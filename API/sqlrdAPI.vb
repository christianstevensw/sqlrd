﻿Imports System.Xml
<System.Serializable()> Public Class sqlrdAPI
    Protected isActivated As Boolean = False


    ''' <summary>
    ''' A structure representing any selectable schedule in the Application
    ''' </summary>
    ''' <remarks></remarks>
    <System.Serializable()> Public Structure sqlrdObject
        Dim objectName As String
        Dim objectLocation As String
        Dim objectType As clsMarsScheduler.enScheduleType
        Dim objectID As Integer
    End Structure
    ''' <summary>
    ''' The path where SQL-RD is installed
    ''' </summary>
    ''' <param name="applicationPath"></param>
    ''' <remarks></remarks>
    Sub New(ByVal applicationPath As String)
        gUser = "SQLRDAdmin"
        gRole = "Administrator"

        RunEditor = False
        MarsGlobal._SetGlobalVars(applicationPath)
        MarsGlobal._GetEdition(True, Nothing)


        clsMarsData.DataItem.OpenMainDataConnection()

        If gnEdition = gEdition.EVALUATION Then
            showDemoMessage()
        End If
    End Sub

    Sub New()
        gUser = "SQLRDAdmin"
        gRole = "Administrator"

        Dim reg As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\{2E1E94E5-EA74-4990-890D-EF4370B2488A}", False)

        If reg Is Nothing Then
            reg = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{2E1E94E5-EA74-4990-890D-EF4370B2488A}", False)
        End If

        If reg Is Nothing Then Throw New Exception("Could not locate SQL-RD on the system. Check that you have installed SQL-RD and that you have Read-Access to the registry")

        Dim installLocation As String = reg.GetValue("InstallLocation")

        RunEditor = False
        MarsGlobal._SetGlobalVars(installLocation)
        MarsGlobal._GetEdition(True, Nothing)


        clsMarsData.DataItem.OpenMainDataConnection()

        If gnEdition = gEdition.EVALUATION Then
            showDemoMessage()
        End If
    End Sub

    Private Sub showDemoMessage()
        Dim message As String = "This application is using a trial version of the SQL-RD developer API. Please contact ChristianSteven Software at www.christiansteven.com to purchase the full version and make this message go away for good"

        MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub
    ''' <summary>
    ''' Closes the connection to the SQL-RD database
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Dispose()
        clsMarsData.DataItem.CloseMainDataConnection()
    End Sub
    ''' <summary>
    ''' Shows the Single Schedule Wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createSingleScheduleWithWizard()
        Dim form As frmSingleScheduleWizard = New frmSingleScheduleWizard

        form.ShowDialog()
    End Sub
    ''' <summary>
    ''' shows the package schedule wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createPackageScheduleWithWizard()
        Dim x As frmPackWizard = New frmPackWizard
        x.ShowDialog()
    End Sub

    ''' <summary>
    ''' shows the automation schedule wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createAutomationScheduleWithWizard()
        Dim x As frmAutoScheduleWizard = New frmAutoScheduleWizard
        x.ShowDialog()
    End Sub
    ''' <summary>
    ''' shows the event-based schedule wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createEventBasedScheduleWithWizard()
        Dim x As frmEventWizard6 = New frmEventWizard6
        x.ShowDialog()
    End Sub
    ''' <summary>
    ''' shows the event-based package wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createEventBasedPackageWithWizard()
        Dim x As frmEventPackage = New frmEventPackage
        x.ShowDialog()
    End Sub

    ''' <summary>
    ''' shows the dynamic schedule wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createDynamicScheduleWithWizard()
        Dim x As New frmDynamicSchedule

        x.Show()
    End Sub
    ''' <summary>
    ''' three guesses as to what this does
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createDynamicPackageWithWizard()
        Dim x As New frmDynamicPackage

        x.ShowDialog()
    End Sub
    ''' <summary>
    ''' shows the data-driven schedule wizard
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createDataDrivenScheduleWithWizard()
        Dim x As frmRDScheduleWizard = New frmRDScheduleWizard
        x.ShowDialog()
    End Sub
    ''' <summary>
    ''' shows the data-driven package schedule
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub createDataDrivenPackageWithWizard()
        Dim x As frmDataDrivenPackage = New frmDataDrivenPackage
        x.ShowDialog()
    End Sub
    ''' <summary>
    ''' opens a preview of the report
    ''' </summary>
    ''' <param name="scheduleName">the name of the schedule (single report based only)</param>
    ''' <param name="scheduleLocation">the folder where the schedule is located</param>
    ''' <remarks></remarks>
    Public Sub previewReport(ByVal scheduleName, ByVal scheduleLocation)
        Dim nID As Integer = getScheduleIdentifier(scheduleName, scheduleLocation, clsMarsScheduler.enScheduleType.REPORT)

        Dim report As clsMarsReport = New clsMarsReport

        report.ViewReport(nID, True)
    End Sub
    ''' <summary>
    ''' Shows the relevant properties window depending on the schedule type
    ''' </summary>
    ''' <param name="scheduleName">the name of the schedule</param>
    ''' <param name="scheduleLocation">the location of the schedule</param>
    ''' <param name="scheduleType">the schedule type</param>
    ''' <remarks></remarks>
    Public Sub editScheduleWithPropertiesWindows(ByVal scheduleName As String, ByVal scheduleLocation As String, ByVal scheduleType As clsMarsScheduler.enScheduleType)
        Dim nID As Integer = getScheduleIdentifier(scheduleName, scheduleLocation, scheduleType)

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                Dim form As frmAutoProp = New frmAutoProp
                form.EditSchedule(nID)
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                Dim form As frmEventProp = New frmEventProp
                form.EditSchedule(nID)
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Dim form As frmEventPackageProp = New frmEventPackageProp

                form.EditPackage(nID)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                Dim form As frmPackageProp = New frmPackageProp
                form.EditPackage(nID)
            Case clsMarsScheduler.enScheduleType.REPORT
                Dim form As frmSingleProp
                form.EditSchedule(nID)
        End Select
    End Sub
    ''' <summary>
    ''' return an ID for the schedule in a specified folder location
    ''' </summary>
    ''' <param name="scheduleName">the name of the schedule</param>
    ''' <param name="folderPath">the location of the folder</param>
    ''' <param name="scheduleType">the schedule type</param>
    ''' <returns>an integer (0 if not found)</returns>
    ''' <remarks></remarks>
    Public Function getScheduleIdentifier(ByVal scheduleName As String, ByVal folderPath As String, ByVal scheduleType As clsMarsScheduler.enScheduleType) As Integer
        Dim SQL As String
        Dim folderID As Integer = getFolderID(folderPath)
        Dim scheduleID As Integer

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                SQL = "SELECT autoid from automationattr WHERE autoname LIKE '" & SQLPrepare(scheduleName) & "' AND parent =" & folderID
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                SQL = "SELECT eventid from eventattr6 WHERE eventname LIKE '" & SQLPrepare(scheduleName) & "' AND parent =" & folderID
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                SQL = "SELECT eventpackid from eventpackageattr WHERE packagename LIKE '" & SQLPrepare(scheduleName) & "' AND parent =" & folderID
            Case clsMarsScheduler.enScheduleType.PACKAGE
                SQL = "SELECT packid from packageattr WHERE packagename LIKE '" & SQLPrepare(scheduleName) & "' AND parent =" & folderID
            Case clsMarsScheduler.enScheduleType.REPORT
                SQL = "SELECT reportid from reportattr WHERE reportname LIKE '" & SQLPrepare(scheduleName) & "' AND parent =" & folderID
        End Select

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            scheduleID = oRs(0).Value
        Else
            scheduleID = 0
        End If

        oRs.Close()

        oRs = Nothing

        Return scheduleID
    End Function
    ''' <summary>
    ''' return an ID identifying the folder
    ''' </summary>
    ''' <param name="folderPath">e.g. Custom\Sales\Region\Continent</param>
    ''' <returns>an integer</returns>
    ''' <remarks></remarks>
    Public Function getFolderID(ByVal folderPath As String) As Integer
        Dim folders() As String = folderPath.Split("\")

        Dim parentid As Integer = 0
        Dim SQL As String
        Dim I As Integer = 0
        Dim oRs As ADODB.Recordset

        For Each s As String In folders
            If s = "" Then Continue For
            '//get the new parent
            SQL = "SELECT folderid FROM folders WHERE parent = " & parentid & " AND folderName LIKE '" & SQLPrepare(s) & "'"

            oRs = clsMarsData.GetData(SQL)

            parentid = oRs(0).Value
        Next

        Return parentid
    End Function
    ''' <summary>
    ''' Executes the provided schedule
    ''' </summary>
    ''' <param name="scheduleName">The name of the schedule</param>
    ''' <param name="location">The folder location in SQL-RD where the schedule is e.g. Daily\Sales\USA </param>
    ''' <param name="type">The type of schedule to be executed</param>
    ''' <param name="errInfoXML">Return any error information that was encountered</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Function executeSchedule(ByVal scheduleName As String, ByVal location As String, ByVal type As clsMarsScheduler.enScheduleType, ByRef errInfoXML As String) As Boolean
        Dim nID As Integer = getScheduleIdentifier(scheduleName, location, type)
        Dim result As Boolean

        Select Case type
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                Dim auto As clsMarsAutoSchedule = New clsMarsAutoSchedule
                result = auto.RunAutomationSchedule(nID)
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                Dim ev As clsMarsEvent = New clsMarsEvent
                result = ev.RunEvents6(nID)
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Dim evp As clsMarsEvent = New clsMarsEvent
                result = evp.RunEventPackage(nID)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                Dim pack As clsMarsReport = New clsMarsReport

                If clsMarsData.IsScheduleDataDriven(nID, "package") Then
                    result = pack.RunDataDrivenPackage(nID)
                ElseIf clsMarsData.IsScheduleDynamic(nID, "package") Then
                    result = pack.RunDynamicPackageSchedule(nID, False)
                Else
                    result = pack.RunPackageSchedule(nID)
                End If
            Case clsMarsScheduler.enScheduleType.REPORT
                Dim report As clsMarsReport = New clsMarsReport

                If clsMarsData.IsScheduleDataDriven(nID, "report") Then
                    result = report.RunDataDrivenSchedule(nID)
                ElseIf clsMarsData.IsScheduleDynamic(nID, "report") Then
                    result = report.RunDynamicSchedule(nID, False)
                Else
                    result = report.RunSingleSchedule(nID)
                End If
        End Select

        If result = False Then
            errInfoXML = "<ErrorDesc>" & gErrorDesc & "</ErrorDesc><ErrorNumber>" & gErrorNumber & "</ErrorNumber><ErrorSource>" & gErrorSource & "</ErrorSource>" & _
                "<Suggestion>" & gErrorSuggest & "</Suggestion><LineNumber>" & gErrorLine & "</LineNumber>"

            Return False
        Else
            Return True
        End If
    End Function

    ''' <summary>
    ''' creates a single schedule
    ''' </summary>
    ''' <param name="reportServer">The path to the SSRS Web Service end point e.g http://myssrsserver/reportserver/reportservice.asmx</param>
    ''' <param name="serverUserID">the userid to log into the SSRS server - leave blank for integrated authentication</param>
    ''' <param name="serverPassword">the password used to log into the server - leave blank for integrated authentication</param>
    ''' <param name="reportPath">the path to the report e.g. \Sales Reports\CommisionReport</param>
    ''' <param name="scheduleName">the name of the schedule</param>
    ''' <param name="scheduleDetails">the schedule object describing when the schedule should run</param>
    ''' <param name="outputDestinations">one or more destination objects</param>
    ''' <param name="scheduleDescription">the schedule description</param>
    ''' <param name="scheduleTags">keywords for the schedule</param>
    ''' <param name="scheduleLocation">the location to store the new schedule</param>
    ''' <param name="parameters">parameters collection for schedule</param>
    ''' <param name="datasources">datasources collection for the schedule</param>
    ''' <param name="errInfo">returns any errors that occurs during the creation</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Private Overloads Function createSingleSchedule(ByVal reportServer As String, ByVal serverUserID As String, ByVal serverPassword As String, ByVal reportPath As String, ByVal scheduleName As String, _
                                         ByVal scheduleDetails As schedule, ByVal outputDestinations() As destination, _
                                         ByVal scheduleDescription As String, ByVal scheduleTags As String, ByVal scheduleLocation As String, ByVal parameters() As reportParameter, _
                                         ByVal datasources() As reportDatasource, ByRef errInfo As Exception) As Boolean
        Dim nReportID As Int32
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim WriteSuccess As Boolean
        Dim nInclude As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport
        Dim folderID As Integer = getFolderID(scheduleLocation)
        Dim SQL As String


        nReportID = clsMarsData.CreateDataID("reportattr", "reportid")

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,LastRefreshed,AutoRefresh,Bursting,Dynamic,RptDatabaseType,AutoCalc,RetryInterval"

        sVals = nReportID & ",0," & _
            "'" & SQLPrepare(reportServer) & "'," & _
            "'" & SQLPrepare(scheduleName) & "'," & _
            folderID & "," & _
            "'" & SQLPrepare(scheduleName) & "'," & _
            3 & "," & _
            0 & "," & _
            0 & "," & _
            "''," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(serverUserID) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(serverPassword)) & "'," & _
            "''," & _
            "''," & _
            0 & "," & _
            0 & "," & _
            "'" & SQLPrepare(reportPath) & "', '" & ConDateTime(Date.Now) & "'," & _
            0 & "," & _
            "0,0," & _
            "'', " & _
            0 & "," & _
            0

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = True Then
            Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            With scheduleDetails
                SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                    "StartTime,Repeat,RepeatInterval," & _
                    "RepeatUntil,Status,ReportID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                    "VALUES(" & _
                    ScheduleID & "," & _
                    "'" & .selectedFrequency & "'," & _
                    "'" & ConDateTime(CTimeZ(.startDate, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDateTime(CTimeZ(.endDate, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDate(CTimeZ(.startDate.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.executionTime, dateConvertType.WRITE)) & "'," & _
                    "'" & CTimeZ(.executionTime, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.repeat) & "," & _
                    "'" & Convert.ToString(.repeatEvery.TotalMinutes).Replace(",", ".") & "'," & _
                    "'" & CTimeZ(.repeatUntil, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.status) & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(scheduleDescription) & "'," & _
                    "'" & SQLPrepare(scheduleTags) & "'," & _
                    "''," & _
                    Convert.ToInt32(False) & "," & _
                    "''," & _
                    "'minutes')"


                WriteSuccess = clsMarsData.WriteData(SQL)
            End With

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Function
            End If

            'save the destination data for the report
            Dim I As Integer = 0

            For Each dest As destination In outputDestinations
                saveDestination(dest, nReportID, 0, I)

                I += 1
            Next

            Dim cols, vals As String

            cols = "parid, reportid, parname, parvalue, partype, parnull, multivalue"

            If parameters IsNot Nothing Then
                For Each par As reportParameter In parameters
                    Dim parameterValues As String = ""

                    For Each s As String In par.parameterValues
                        parameterValues &= s & "|"
                    Next

                    If parameterValues <> "" Then parameterValues = parameterValues.Remove(parameterValues.Length - 1, 1)

                    vals = par.parameterID & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(par.parameterName) & "'," & _
                    "'" & SQLPrepare(parameterValues) & "'," & _
                    "'" & SQLPrepare(par.parameterDataType) & "'," & _
                    Convert.ToInt32(par.setAsNothing) & "," & _
                    Convert.ToInt32(par.multiValue)

                    clsMarsData.DataItem.InsertData("reportparameter", cols, vals, False)
                Next
            End If

            cols = "datasourceid, reportid, datasourcename, rptuserid, rptpassword"

            If datasources IsNot Nothing Then
                For Each ds As reportDatasource In datasources
                    vals = ds.datasourceID & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(ds.loginID) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(ds.password)) & "',"

                    clsMarsData.DataItem.InsertData("reportdatasource", cols, vals, False)
                Next
            End If

        End If

        clsMarsAudit._LogAudit(scheduleName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.CREATE)

    End Function

    ''' <summary>
    ''' Creates a single report schedule 
    ''' </summary>
    ''' <param name="reportObj">The ssrs report object configured with parameters and datasources</param>
    ''' <param name="scheduleName">The name of the schedule</param>
    ''' <param name="scheduleDetails">the schedule object describing when the schedule should run</param>
    ''' <param name="outputDestinations">one or more destination objects</param>
    ''' <param name="scheduleDescription">the schedule description</param>
    ''' <param name="scheduleTags">keywords for the schedule</param>
    ''' <param name="scheduleLocation">the location to store the new schedule</param>
    ''' <param name="errInfo">returns any errors that occurs during the creation</param>
    ''' <returns>Boolean</returns>
    ''' <remarks></remarks>
    Public Overloads Function createSingleSchedule(ByVal reportObj As report, ByVal scheduleName As String, _
                                         ByVal scheduleDetails As schedule, ByVal outputDestinations() As destination, _
                                         ByVal scheduleDescription As String, ByVal scheduleTags As String, ByVal scheduleLocation As String, ByRef errInfo As Exception) As Boolean
        Dim nReportID As Int32
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim WriteSuccess As Boolean
        Dim nInclude As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport
        Dim folderID As Integer = getFolderID(scheduleLocation)
        Dim SQL As String


        nReportID = reportObj.reportID

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,LastRefreshed,AutoRefresh,Bursting,Dynamic,RptDatabaseType,AutoCalc,RetryInterval"

        sVals = nReportID & ",0," & _
            "'" & SQLPrepare(reportObj.serverAddress) & "'," & _
            "'" & SQLPrepare(scheduleName) & "'," & _
            folderID & "," & _
            "'" & SQLPrepare(scheduleName) & "'," & _
            3 & "," & _
            0 & "," & _
            0 & "," & _
            "''," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(reportObj.loginUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(reportObj.loginPassword)) & "'," & _
            "''," & _
            "''," & _
            0 & "," & _
            0 & "," & _
            "'" & SQLPrepare(reportObj.reportPath) & "', '" & ConDateTime(Date.Now) & "'," & _
            0 & "," & _
            "0,0," & _
            "'', " & _
            0 & "," & _
            0

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = True Then
            Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            With scheduleDetails
                SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                    "StartTime,Repeat,RepeatInterval," & _
                    "RepeatUntil,Status,ReportID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                    "VALUES(" & _
                    ScheduleID & "," & _
                    "'" & .selectedFrequency & "'," & _
                    "'" & ConDateTime(CTimeZ(.startDate, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDateTime(CTimeZ(.endDate, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDate(CTimeZ(.startDate.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.executionTime, dateConvertType.WRITE)) & "'," & _
                    "'" & CTimeZ(.executionTime, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.repeat) & "," & _
                    "'" & Convert.ToString(.repeatEvery.TotalMinutes).Replace(",", ".") & "'," & _
                    "'" & CTimeZ(.repeatUntil, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.status) & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(scheduleDescription) & "'," & _
                    "'" & SQLPrepare(scheduleTags) & "'," & _
                    "''," & _
                    Convert.ToInt32(False) & "," & _
                    "''," & _
                    "'minutes')"


                WriteSuccess = clsMarsData.WriteData(SQL)
            End With

            If WriteSuccess = False Then
                clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                Exit Function
            End If

            'save the destination data for the report
            Dim I As Integer = 0

            For Each dest As destination In outputDestinations
                saveDestination(dest, nReportID, 0, I)

                I += 1
            Next

            Dim cols, vals As String

            cols = "parid, reportid, parname, parvalue, partype, parnull, multivalue"

            If reportObj.parameters IsNot Nothing Then
                For Each par As reportParameter In reportObj.parameters
                    Dim parameterValues As String = ""

                    For Each s As String In par.parameterValues
                        parameterValues &= s & "|"
                    Next

                    If parameterValues <> "" Then parameterValues = parameterValues.Remove(parameterValues.Length - 1, 1)

                    vals = par.parameterID & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(par.parameterName) & "'," & _
                    "'" & SQLPrepare(parameterValues) & "'," & _
                    "'" & SQLPrepare(par.parameterDataType) & "'," & _
                    Convert.ToInt32(par.setAsNothing) & "," & _
                    Convert.ToInt32(par.multiValue)

                    clsMarsData.DataItem.InsertData("reportparameter", cols, vals, False)
                Next
            End If

            cols = "datasourceid, reportid, datasourcename, rptuserid, rptpassword"

            If reportObj.datasources IsNot Nothing Then
                For Each ds As reportDatasource In reportObj.datasources
                    vals = ds.datasourceID & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(ds.loginID) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(ds.password)) & "',"

                    clsMarsData.DataItem.InsertData("reportdatasource", cols, vals, False)
                Next
            End If

        End If

        clsMarsAudit._LogAudit(scheduleName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.CREATE)

    End Function
    ''' <summary>
    ''' Creates a package schedule
    ''' </summary>
    ''' <param name="packageName">The name of the package</param>
    ''' <param name="packageLocation">The location to store the package</param>
    ''' <param name="packageDesc">The package description</param>
    ''' <param name="packageTags">The package tags</param>
    ''' <param name="scheduleDetails">The schedule object</param>
    ''' <param name="outputDestinations">One or more destination objects</param>
    ''' <param name="mergePDFs">Merge all the output PDF files in the package into a single package</param>
    ''' <param name="mergedPDFFileName">The name of the merged PDF file</param>
    ''' <param name="mergeExcelFiles">Merge all the output Excel files into a single file</param>
    ''' <param name="mergedExcelFileName">The name of the merged Excel file</param>
    ''' <param name="mergeTextFiles">Merge all the Text output files into a single file</param>
    ''' <param name="mergedTextFileName">The name of the merged text file</param>
    ''' <param name="reports">One or more report objects</param>
    ''' <param name="failAllReportsOnOneFailure">Specifify if the package should be failed if any report fail</param>
    ''' <param name="multiThreaded">should the package be executed using multiple threads</param>
    ''' <param name="errInfo">An exception object of any errors that would cause the package to not get created</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function createPackageSchedule(ByVal packageName As String, ByVal packageLocation As String, ByVal packageDesc As String, ByVal packageTags As String, ByVal scheduleDetails As schedule, ByVal outputDestinations() As destination, ByVal mergePDFs As Boolean, _
                                          ByVal mergedPDFFileName As String, ByVal mergeExcelFiles As Boolean, ByVal mergedExcelFileName As String, ByVal mergeTextFiles As Boolean, _
                                          ByVal mergedTextFileName As String, ByVal reports() As packagedReport, _
                                          ByVal failAllReportsOnOneFailure As Boolean, ByVal multiThreaded As Boolean, ByRef errInfo As Exception)
        Dim SQL As String
        Dim nPackID As Integer = clsMarsData.CreateDataID("packageattr", "packid")
        Dim folderID As Integer = getFolderID(packageLocation)
        Dim WriteSuccess As Boolean

        SQL = "INSERT INTO PackageAttr(PackID,PackageName,Parent,Retry,AssumeFail," & _
            "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName,DateTimeStamp,StampFormat,Dynamic," & _
            "AdjustPackageStamp,AutoCalc,RetryInterval,Multithreaded,MergeText,MergeTextName) VALUES(" & _
            nPackID & "," & _
            "'" & packageName & "'," & _
            folderID & "," & _
            0 & "," & _
            0 & "," & _
            Convert.ToInt32(False) & "," & _
            "'" & gUser & "'," & _
            Convert.ToInt32(failAllReportsOnOneFailure) & "," & _
            Convert.ToInt32(mergePDFs) & "," & _
            Convert.ToInt32(mergeExcelFiles) & "," & _
            "'" & SQLPrepare(mergedPDFFileName) & "'," & _
            "'" & SQLPrepare(mergedExcelFileName) & "'," & _
            Convert.ToInt32(False) & "," & _
            "'',0," & _
            0 & "," & _
            Convert.ToInt32(False) & "," & _
            0 & "," & _
            Convert.ToInt32(multiThreaded) & "," & _
            Convert.ToInt32(mergeTextFiles) & "," & _
            "'" & SQLPrepare(mergedTextFileName) & "')"

        If clsMarsData.WriteData(SQL) = False Then Exit Function

        Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        With scheduleDetails
            SQL = "INSERT INTO ScheduleAttr(ScheduleID,PackID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval," & _
                "RepeatUntil,Status,ReportID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                "VALUES(" & _
                ScheduleID & "," & _
                nPackID & "," & _
                "'" & .selectedFrequency & "'," & _
                "'" & ConDateTime(CTimeZ(.startDate, dateConvertType.WRITE)) & "'," & _
                "'" & ConDateTime(CTimeZ(.endDate, dateConvertType.WRITE)) & "'," & _
                "'" & ConDate(CTimeZ(.startDate.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.executionTime, dateConvertType.WRITE)) & "'," & _
                "'" & CTimeZ(.executionTime, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.repeat) & "," & _
                "'" & Convert.ToString(.repeatEvery.TotalMinutes).Replace(",", ".") & "'," & _
                "'" & CTimeZ(.repeatUntil, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.status) & "," & _
                0 & "," & _
                "'" & SQLPrepare(packageDesc) & "'," & _
                "'" & SQLPrepare(packageTags) & "'," & _
                "''," & _
                Convert.ToInt32(False) & "," & _
                "''," & _
                "'minutes')"

            WriteSuccess = clsMarsData.WriteData(SQL)
        End With

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE Packid = " & nPackID)
            Exit Function
        End If

        'save the destination data for the report
        Dim I As Integer = 0

        For Each dest As destination In outputDestinations
            saveDestination(dest, 0, nPackID, I)

            I += 1
        Next

        '//save the reports here
        Dim sCols, sVals As String

        For Each rpt As packagedReport In reports
            'save the report
            sCols = "ReportID,PackID,DatabasePath,ReportName," & _
                "Parent,ReportTitle," & _
                "Retry," & _
                "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
                "UseSavedData,CachePath,AutoRefresh,PackOrderID,RptDatabaseType,CheckBlank"


            sVals = rpt.reportID & "," & _
            nPackID & "," & _
            "'" & SQLPrepare(rpt.serverAddress) & "'," & _
            "'" & SQLPrepare(rpt.reportName) & "'," & _
            0 & "," & _
            "'" & I & ":" & SQLPrepare(rpt.reportName) & "'," & _
            0 & "," & _
            "''," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(rpt.loginUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(rpt.loginPassword)) & "'," & _
            "''," & _
            "''," & _
            0 & "," & _
            0 & "," & _
            "'" & SQLPrepare(rpt.reportPath) & "'," & _
            0 & "," & _
            I & "," & _
            "''," & _
            Convert.ToInt32(False)

            SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

            'lets save the other report properties
            If clsMarsData.WriteData(SQL) = True Then
                sCols = "AttrID,PackID,ReportID,OutputFormat,CustomExt," & _
                "AppendDateTime,DateTimeFormat,CustomName,Status,DisabledDate,AdjustStamp"

                sVals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
                nPackID & "," & _
                rpt.reportID & "," & _
                "'" & SQLPrepare(rpt.outputFormatString) & "'," & _
                "'" & SQLPrepare(rpt.customExtension) & "'," & _
                Convert.ToInt32(rpt.appendDateStamp) & "," & _
                "'" & SQLPrepare(rpt.dateStampFormat) & "'," & _
                "'" & SQLPrepare(rpt.customOutputFileName) & "'," & _
                Convert.ToInt32(rpt.status) & "," & _
                "'" & ConDateTime(Now) & "'," & _
                rpt.adjustDateStampBy

                SQL = "INSERT INTO PackagedReportAttr(" & sCols & ") " & _
                "VALUES (" & sVals & ")"

                clsMarsData.WriteData(SQL)

                '//save parameters
                sCols = "parid, reportid, parname, parvalue, partype, parnull, multivalue"

                If rpt.parameters IsNot Nothing Then
                    For Each par As reportParameter In rpt.parameters
                        Dim parameterValues As String = ""

                        For Each s As String In par.parameterValues
                            parameterValues &= s & "|"
                        Next

                        If parameterValues <> "" Then parameterValues = parameterValues.Remove(parameterValues.Length - 1, 1)

                        sVals = par.parameterID & "," & _
                        rpt.reportID & "," & _
                        "'" & SQLPrepare(par.parameterName) & "'," & _
                        "'" & SQLPrepare(parameterValues) & "'," & _
                        "'" & SQLPrepare(par.parameterDataType) & "'," & _
                        Convert.ToInt32(par.setAsNothing) & "," & _
                        Convert.ToInt32(par.multiValue)

                        clsMarsData.DataItem.InsertData("reportparameter", sCols, sVals, False)
                    Next
                End If

                '//save datasources
                sCols = "datasourceid, reportid, datasourcename, rptuserid, rptpassword"

                If rpt.datasources IsNot Nothing Then
                    For Each ds As reportDatasource In rpt.datasources
                        sVals = ds.datasourceID & "," & _
                        rpt.reportID & "," & _
                        "'" & SQLPrepare(ds.loginID) & "'," & _
                        "'" & SQLPrepare(_EncryptDBValue(ds.password)) & "',"

                        clsMarsData.DataItem.InsertData("reportdatasource", sCols, sVals, False)
                    Next
                End If
            End If
        Next

        clsMarsAudit._LogAudit(packageName, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.CREATE)

        Return True
    End Function


    Public Function selectObject() As sqlrdObject
        Dim frm As frmFolders = New frmFolders

        Dim obj As sqlrdObject = frm.getAnyObject

        Return obj
    End Function


    Private Function saveDestination(ByVal dest As destination, ByVal reportID As Integer, ByVal packID As Integer, ByVal destOrderID As Integer) As Boolean
        Dim sCols As String = "destinationid,destinationtype,destinationname,"
        Dim svals As String
        Dim destinationID As Integer = dest.destinationID
        Dim SQL As String

        If TypeOf dest Is emailDestination Then

            sCols &= "SendTo,CC,Bcc,Subject,Message,Extras,MailFormat,SMTPServer,SenderName,SenderAddress,"

            With CType(dest, emailDestination)
                svals = destinationID & "," & _
                "'Email'," & _
                "'" & SQLPrepare(.destinationName) & "'," & _
                "'" & SQLPrepare(.emailTo) & "'," & _
                "'" & SQLPrepare(.emailCC) & "'," & _
                "'" & SQLPrepare(.emailBcc) & "'," & _
                "'" & SQLPrepare(.emailSubject) & "'," & _
                "'" & SQLPrepare(.emailBody) & "'," & _
                "'" & SQLPrepare(.customAttachment) & "'," & _
                "'" & .emailFormat.ToString & "'," & _
                "'Default'," & _
                "'" & SQLPrepare(.senderName) & "'," & _
                "'" & SQLPrepare(.senderAddress) & "'"
            End With

        ElseIf TypeOf dest Is diskDestination Then
            Dim paths As String = ""

            For Each item As String In CType(dest, diskDestination).outputFolders
                paths &= item & "|"
            Next

            sCols &= "OutputPath,"

            svals = destinationID & "," & _
            "'Disk'," & _
            "'" & SQLPrepare(dest.destinationName) & "'," & _
            "'" & SQLPrepare(paths) & "'"
        End If

        sCols &= "ReportID,PackID,OutputFormat,Customext,AppendDateTime," & _
        "DateTimeFormat,CustomName,IncludeAttach,Compress,Embed,HTMLSplit," & _
        "DeferDelivery,DeferBy,EncryptZip,EncryptZipLevel,EncryptZipCode,UseDUN,DUNName,SmartID," & _
        "ReadReceipts,AdjustStamp,EnabledStatus,DestOrderID"

        Dim enLevel As Integer = 128

        With dest
            svals &= "," & reportID & "," & _
            packID & "," & _
            "'" & .outputFormatString & "'," & _
            "'" & SQLPrepare(.customExtension) & "'," & _
            Convert.ToInt32(.appendDateStampToFile) & "," & _
            "'" & .dateStampFormat & "'," & _
            "'" & SQLPrepare(.customOutputName) & "'," & _
            Convert.ToInt32(True) & "," & _
            Convert.ToInt32(False) & "," & _
            Convert.ToInt32(False) & "," & _
            Convert.ToInt32(False) & "," & _
            Convert.ToInt32(False) & "," & _
            "'0'," & _
            Convert.ToInt32(False) & "," & _
            Convert.ToInt32(enLevel) & "," & _
            "'" & SQLPrepare(Encrypt("", "preggers")) & "'," & _
            Convert.ToInt32(False) & "," & _
            "'" & SQLPrepare("") & "'," & _
            0 & "," & _
            Convert.ToInt32(False) & "," & _
            .adjustDateStampBy & "," & _
            Convert.ToInt32(True) & "," & _
            destOrderID
        End With

        SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
        "VALUES(" & svals & ")"

        Return clsMarsData.WriteData(SQL)
    End Function

    Public Class destination
        Public Enum destinationFormatType
            PDF = 0
            RTF = 1
            Excel = 2
            Word = 3
            CSV = 4
            TIFF = 5
            Xml = 6
        End Enum


        Dim m_destinationID As Integer
        Sub New()
            m_destinationID = clsMarsData.CreateDataID("destinationattr", "destinationid")
        End Sub

        Public ReadOnly Property destinationID() As Integer
            Get
                Return m_destinationID
            End Get
        End Property


        Private m_destinationName As String
        ''' <summary>
        ''' the name of the destination e.g. Email to Sales Manager
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property destinationName() As String
            Get
                Return m_destinationName
            End Get
            Set(ByVal value As String)
                m_destinationName = value
            End Set
        End Property


        Private m_outputFormat As String
        ''' <summary>
        ''' The output format that the report will be exported to
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property outputFormat() As destinationFormatType
            Get
                Return m_outputFormat
            End Get
            Set(ByVal value As destinationFormatType)
                m_outputFormat = value
            End Set
        End Property

        ''' <summary>
        ''' return a string represantation of the output format
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property outputFormatString()
            Get
                Select Case outputFormat
                    Case destinationFormatType.CSV
                        Return "CSV (*.csv)"
                    Case destinationFormatType.Excel
                        Return "MS Excel 97-2000 (*.xls)"
                    Case destinationFormatType.PDF
                        Return "Acrobat Format (*.pdf)"
                    Case destinationFormatType.RTF
                        Return "Rich Text Format (*.rtf)"
                    Case destinationFormatType.Word
                        Return "MS Word (*.doc)"
                    Case destinationFormatType.TIFF
                        Return "TIFF (*.tif)"
                    Case destinationFormatType.Xml
                        Return "XML (*.xml)"
                End Select
            End Get
        End Property

        Private m_customName As String
        ''' <summary>
        ''' customize the output file name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property customOutputName() As String
            Get
                Return m_customName
            End Get
            Set(ByVal value As String)
                m_customName = value
            End Set
        End Property


        Private m_customExtension As String
        ''' <summary>
        ''' customize the output file extension
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property customExtension() As String
            Get
                Return m_customExtension
            End Get
            Set(ByVal value As String)
                m_customExtension = value
            End Set
        End Property

        Private m_appendDateStamp As Boolean
        ''' <summary>
        ''' append a date/time stamp to the output file name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property appendDateStampToFile() As Boolean
            Get
                Return m_appendDateStamp
            End Get
            Set(ByVal value As Boolean)
                m_appendDateStamp = value
            End Set
        End Property


        Private m_appendedDateTimeFormat As String
        ''' <summary>
        ''' the format of the date/time stamp e.g. yyyy-MM-dd HH:mm:ss
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property dateStampFormat() As String
            Get
                Return m_appendedDateTimeFormat
            End Get
            Set(ByVal value As String)
                m_appendedDateTimeFormat = value
            End Set
        End Property


        Private m_adjustDateStamp As Integer
        ''' <summary>
        ''' adjust the date/time stamp by days e.g. -1 for yesterday or +1 for tommorrow
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property adjustDateStampBy() As Integer
            Get
                Return m_adjustDateStamp
            End Get
            Set(ByVal value As Integer)
                m_adjustDateStamp = value
            End Set
        End Property

    End Class

    Public Class emailDestination
        Inherits destination

        Public Enum emailFormatType
            HTML = 0
            TEXT = 1
        End Enum

        Public ReadOnly Property destinationType() As String
            Get
                Return "Email"
            End Get
        End Property


        Private m_To As String
        ''' <summary>
        ''' the email recipient semi-colon delimited
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property emailTo() As String
            Get
                Return m_To
            End Get
            Set(ByVal value As String)
                m_To = value
            End Set
        End Property


        Private m_CC As String
        ''' <summary>
        ''' the email CC recipient semi-colon delimited
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property emailCC() As String
            Get
                Return m_CC
            End Get
            Set(ByVal value As String)
                m_CC = value
            End Set
        End Property


        Private m_BCC As String
        ''' <summary>
        ''' the email BCC recipient semi-colon delimited
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property emailBcc() As String
            Get
                Return m_BCC
            End Get
            Set(ByVal value As String)
                m_BCC = value
            End Set
        End Property


        Private m_Subject As String
        ''' <summary>
        ''' the email subject
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property emailSubject() As String
            Get
                Return m_Subject
            End Get
            Set(ByVal value As String)
                m_Subject = value
            End Set
        End Property


        Private m_Body As String
        ''' <summary>
        ''' the email body
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property emailBody() As String
            Get
                Return m_Body
            End Get
            Set(ByVal value As String)
                m_Body = value
            End Set
        End Property


        Private m_customAttachment As String
        ''' <summary>
        ''' custom attachments to include with the email destination - semicolon delimited
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property customAttachment() As String
            Get
                Return m_customAttachment
            End Get
            Set(ByVal value As String)
                m_customAttachment = value
            End Set
        End Property

        Private m_emailFormat As emailFormatType
        Public Property emailFormat() As emailFormatType
            Get
                Return m_emailFormat
            End Get
            Set(ByVal value As emailFormatType)
                m_emailFormat = value
            End Set
        End Property


        Private m_senderName As String
        Public Property senderName() As String
            Get
                Return m_senderName
            End Get
            Set(ByVal value As String)
                m_senderName = value
            End Set
        End Property


        Private m_senderAddress As String
        Public Property senderAddress() As String
            Get
                Return m_senderAddress
            End Get
            Set(ByVal value As String)
                m_senderAddress = value
            End Set
        End Property

    End Class

    Public Class diskDestination
        Inherits destination

        Public ReadOnly Property destinationType() As String
            Get
                Return "Disk"
            End Get
        End Property


        Private m_Directorys As String
        ''' <summary>
        ''' a string array of the folders to place the exported report(s)
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property outputFolders() As String
            Get
                Return m_Directorys
            End Get
            Set(ByVal value As String)
                m_Directorys = value
            End Set
        End Property

    End Class

    Public Class reportParameter
        Dim m_parameterID As Integer
        ''' <summary>
        ''' Create a new parameter object
        ''' </summary>
        ''' <param name="name">the parameter name</param>
        ''' <param name="values">the parameter value(s)</param>
        ''' <param name="ignore">set so that the parameter should be skipped</param>
        ''' <param name="null">set the parameter value to nothing</param>
        ''' <remarks></remarks>
        Sub New(ByVal name As String, ByVal values As ArrayList, ByVal ignore As Boolean, ByVal null As Boolean)
            m_parameterID = clsMarsData.CreateDataID("reportparameter", "parid")
            parameterName = name
            parameterValues = values
            ignoreParameter = ignore
            setAsNothing = null
        End Sub

        Public ReadOnly Property parameterID() As Integer
            Get
                Return m_parameterID
            End Get
        End Property

        Private m_parameterName As String
        ''' <summary>
        ''' the parameter name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property parameterName() As String
            Get
                Return m_parameterName
            End Get
            Set(ByVal value As String)
                m_parameterName = value
            End Set
        End Property


        Private m_parameterValue As ArrayList
        ''' <summary>
        ''' an array list containing one or more parameter values
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property parameterValues() As ArrayList
            Get
                Return m_parameterValue
            End Get
            Set(ByVal value As ArrayList)
                m_parameterValue = value
            End Set
        End Property


        Private m_ignoreParameter As Boolean
        ''' <summary>
        ''' set so that the parameter is not processed by the system
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property ignoreParameter() As Boolean
            Get
                Return m_ignoreParameter
            End Get
            Set(ByVal value As Boolean)
                m_ignoreParameter = value
            End Set
        End Property


        Private m_setAsNothing As Boolean
        ''' <summary>
        ''' set the parameter value to Null/Nothing
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property setAsNothing() As Boolean
            Get
                Return m_setAsNothing
            End Get
            Set(ByVal value As Boolean)
                m_setAsNothing = value
            End Set
        End Property


        Private m_multiValue As Boolean
        Public Property multiValue() As Boolean
            Get
                Return m_multiValue
            End Get
            Set(ByVal value As Boolean)
                m_multiValue = value
            End Set
        End Property


        Private m_parameterDataType As String
        Public Property parameterDataType() As String
            Get
                Return m_parameterDataType
            End Get
            Set(ByVal value As String)
                m_parameterDataType = value
            End Set
        End Property

    End Class

    Public Class reportDatasource
        Dim m_datasourceID As Integer
        ''' <summary>
        ''' create a datasource object
        ''' </summary>
        ''' <param name="name">the datasource name</param>
        ''' <param name="login">the login ID</param>
        ''' <param name="loginpassword">the login password</param>
        ''' <remarks></remarks>
        Sub New(ByVal name As String, ByVal login As String, ByVal loginpassword As String)
            m_datasourceID = clsMarsData.CreateDataID("reportdatasource", "datasourceid")
            datasourceName = name
            loginID = login
            password = loginpassword
        End Sub

        Public ReadOnly Property datasourceID() As Integer
            Get
                Return m_datasourceID
            End Get
        End Property

        Private m_datasourceName As String
        ''' <summary>
        ''' the report's datasource name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property datasourceName() As String
            Get
                Return m_datasourceName
            End Get
            Set(ByVal value As String)
                m_datasourceName = value
            End Set
        End Property


        Private m_loginID As String
        ''' <summary>
        ''' userid for the datasource
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property loginID() As String
            Get
                Return m_loginID
            End Get
            Set(ByVal value As String)
                m_loginID = value
            End Set
        End Property


        Private m_password As String
        ''' <summary>
        ''' password for the datasource
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property password() As String
            Get
                Return m_password
            End Get
            Set(ByVal value As String)
                m_password = value
            End Set
        End Property

    End Class

    Public Class schedule
        Dim scheduleID As Integer

        Public Enum scheduleFrequencyType
            Daily = 0
            Weekly = 1
            WeekDays = 2
            Monthly = 3
            Yearly = 4
            None = 5
        End Enum

        Sub New(ByVal frequency As scheduleFrequencyType)
            scheduleID = clsMarsData.CreateDataID("scheduleattr", "scheduleid")
            scheduleFrequency = frequency
        End Sub


        Private m_scheduleFrequency As scheduleFrequencyType
        Public Property scheduleFrequency() As scheduleFrequencyType
            Get
                Return m_scheduleFrequency
            End Get
            Set(ByVal value As scheduleFrequencyType)
                m_scheduleFrequency = value

                If value = scheduleFrequencyType.None Then
                    status = False
                End If
            End Set
        End Property


        Private m_startDate As Date
        Public Property startDate() As Date
            Get
                Return m_startDate
            End Get
            Set(ByVal value As Date)
                m_startDate = value
            End Set
        End Property

        Private m_endDate As Date
        Public Property endDate() As Date
            Get
                Return m_endDate
            End Get
            Set(ByVal value As Date)
                m_endDate = value
            End Set
        End Property


        Private m_noEndDate As Boolean
        Public Property noEndDate() As Boolean
            Get
                Return m_noEndDate
            End Get
            Set(ByVal value As Boolean)
                m_noEndDate = value

                If value = True Then
                    endDate = Date.Now.AddYears(100)
                End If
            End Set
        End Property


        Private m_executionTime As String
        Public Property executionTime() As String
            Get
                If m_executionTime = "" Then
                    Return "08:00:00"
                Else
                    Return m_executionTime
                End If
            End Get
            Set(ByVal value As String)
                m_executionTime = value
            End Set
        End Property


        Private m_repeat As Boolean
        Public Property repeat() As Boolean
            Get
                Return m_repeat
            End Get
            Set(ByVal value As Boolean)
                m_repeat = value
            End Set
        End Property


        Private m_repeatEvery As TimeSpan
        Public Property repeatEvery() As TimeSpan
            Get
                Return m_repeatEvery
            End Get
            Set(ByVal value As TimeSpan)
                m_repeatEvery = value
            End Set
        End Property


        Private m_repeatUntil As String
        Public Property repeatUntil() As String
            Get
                If m_repeatUntil = "" Then
                    Return Date.Now.ToShortTimeString
                Else
                    Return m_repeatUntil
                End If
            End Get
            Set(ByVal value As String)
                m_repeatUntil = value
            End Set
        End Property


        Private m_enabled As Boolean
        Public Property status() As Boolean
            Get
                Return m_enabled
            End Get
            Set(ByVal value As Boolean)
                m_enabled = value
            End Set
        End Property

        Public ReadOnly Property selectedFrequency() As String
            Get
                Select Case scheduleFrequency
                    Case scheduleFrequencyType.Daily
                        Return "Daily"
                    Case scheduleFrequencyType.Monthly
                        Return "Monthly"
                    Case scheduleFrequencyType.None
                        Return "None"
                    Case scheduleFrequencyType.WeekDays
                        Return "Week-Daily"
                    Case scheduleFrequencyType.Weekly
                        Return "Weekly"
                    Case scheduleFrequencyType.Yearly
                        Return "Yearly"
                End Select
            End Get
        End Property

    End Class


    Public Class report
        Dim m_reportID As Integer
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="serverUrl">The SSRS web service endpoint URL</param>
        ''' <param name="pathToReport">The path to the report</param>
        ''' <param name="parameter">an array of parameters for the report - set as Nothing/null if no parameters are to be set</param>
        ''' <param name="datasource">an array of datasources for the report - set as Nothing if no credentials need be set for the datasources</param>
        ''' <param name="serverUser">the SSRS user ID - leave blank for integrated authentication</param>
        ''' <param name="serverPassword">the SSRS password - leave blank for integrated authentication</param>
        ''' <remarks></remarks>
        Sub New(ByVal serverUrl As String, ByVal pathToReport As String, ByVal parameter() As reportParameter, ByVal datasource() As reportDatasource, ByVal serverUser As String, ByVal serverPassword As String)
            m_reportID = clsMarsData.CreateDataID("reportattr", "reportid")
            reportPath = pathToReport
            parameters = parameter
            datasources = datasource
            loginUser = serverUser
            loginPassword = serverPassword
            serverAddress = serverUrl
        End Sub

        Public ReadOnly Property reportID() As String
            Get
                Return m_reportID
            End Get
        End Property


        Private m_reportName As String
        ''' <summary>
        ''' A friendly name for the report
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property reportName() As String
            Get
                Return m_reportName
            End Get
            Set(ByVal value As String)
                m_reportName = value
            End Set
        End Property


        Private m_serverAddress As String
        ''' <summary>
        ''' The SSRS webservice endpoint URL
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property serverAddress() As String
            Get
                Return m_serverAddress
            End Get
            Set(ByVal value As String)
                m_serverAddress = value
            End Set
        End Property


        Private m_reportPath As String
        ''' <summary>
        ''' The path to the report
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property reportPath() As String
            Get
                Return m_reportPath
            End Get
            Set(ByVal value As String)
                m_reportPath = value
            End Set
        End Property


        Private m_serverUser As String
        ''' <summary>
        ''' The SSRS user ID
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property loginUser() As String
            Get
                Return m_serverUser
            End Get
            Set(ByVal value As String)
                m_serverUser = value
            End Set
        End Property


        Private m_serverPassword As String
        ''' <summary>
        ''' The SSRS password
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property loginPassword() As String
            Get
                Return m_serverPassword
            End Get
            Set(ByVal value As String)
                m_serverPassword = value
            End Set
        End Property


        Private m_datasources() As reportDatasource
        ''' <summary>
        ''' An array of datasources - set as nothing if no credentails need to be set for any of the report datasources
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property datasources() As reportDatasource()
            Get
                Return m_datasources
            End Get
            Set(ByVal value() As reportDatasource)
                m_datasources = value
            End Set
        End Property


        Private m_parameters As reportParameter()
        ''' <summary>
        ''' An array of parameters - set as Nothing/null if no parameters are to be set
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property parameters() As reportParameter()
            Get
                Return m_parameters
            End Get
            Set(ByVal value As reportParameter())
                m_parameters = value
            End Set
        End Property
    End Class

    Public Class packagedReport
        Inherits report

        Dim m_reportID As Integer
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="serverUrl">The SSRS web service end point</param>
        ''' <param name="pathToReport">The path to the SSRS report</param>
        ''' <param name="parameter">An array of parameters for the report</param>
        ''' <param name="datasource">An array of datasources for the report</param>
        ''' <param name="serverUser">The user to access the report server - leave blank for integrated authentication</param>
        ''' <param name="serverPassword">The password for the SSRS user - leave blank for integrated authentication</param>
        ''' <param name="packID">The packageID</param>
        ''' <remarks></remarks>
        Sub New(ByVal serverUrl As String, ByVal pathToReport As String, ByVal parameter() As reportParameter, ByVal datasource() As reportDatasource, ByVal serverUser As String, ByVal serverPassword As String)
            MyBase.New(serverUrl, pathToReport, parameter, datasource, serverUser, serverPassword)
            m_reportID = clsMarsData.CreateDataID("reportattr", "reportid")
            reportPath = pathToReport
            parameters = parameter
            datasources = datasource
            loginUser = serverUser
            loginPassword = serverPassword
            serverAddress = serverUrl
        End Sub
        ''' <summary>
        ''' A unique indentifier for the report
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property reportID() As Integer
            Get
                Return m_reportID
            End Get
        End Property


        Private m_outputFormat As sqlrdAPI.destination.destinationFormatType
        ''' <summary>
        ''' The output format that the report will be exported to
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property outputFormat() As sqlrdAPI.destination.destinationFormatType
            Get
                Return m_outputFormat
            End Get
            Set(ByVal value As sqlrdAPI.destination.destinationFormatType)
                m_outputFormat = value
            End Set
        End Property
        ''' <summary>
        ''' A string represantation of the output format that the report will be exported to
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property outputFormatString() As String
            Get
                Select Case outputFormat
                    Case sqlrdAPI.destination.destinationFormatType.CSV
                        Return "CSV (*.csv)"
                    Case sqlrdAPI.destination.destinationFormatType.Excel
                        Return "MS Excel 97-2000 (*.xls)"
                    Case sqlrdAPI.destination.destinationFormatType.PDF
                        Return "Acrobat Format (*.pdf)"
                    Case sqlrdAPI.destination.destinationFormatType.RTF
                        Return "Rich Text Format (*.rtf)"
                    Case sqlrdAPI.destination.destinationFormatType.Word
                        Return "MS Word (*.doc)"
                    Case sqlrdAPI.destination.destinationFormatType.TIFF
                        Return "TIFF (*.tif)"
                    Case sqlrdAPI.destination.destinationFormatType.Xml
                        Return "XML (*.xml)"
                End Select
            End Get
        End Property

        Private m_customExtension As String = ""
        ''' <summary>
        ''' A custom extension for the output file
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property customExtension() As String
            Get
                Return m_customExtension
            End Get
            Set(ByVal value As String)
                m_customExtension = value
            End Set
        End Property


        Private m_appendDateStamp As Boolean = False
        ''' <summary>
        ''' Append a date/time stamp to the output file
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property appendDateStamp() As Boolean
            Get
                Return m_appendDateStamp
            End Get
            Set(ByVal value As Boolean)
                m_appendDateStamp = value
            End Set
        End Property


        Private m_dateStampFormat As String = ""
        ''' <summary>
        ''' The format of the date/time stamp e.g. yyyy-MM-dd
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property dateStampFormat() As String
            Get
                Return m_dateStampFormat
            End Get
            Set(ByVal value As String)
                m_dateStampFormat = value
            End Set
        End Property


        Private m_customName As String = ""
        ''' <summary>
        ''' Customize the output file name
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property customOutputFileName() As String
            Get
                Return m_customName
            End Get
            Set(ByVal value As String)
                m_customName = value
            End Set
        End Property


        Private m_status As Boolean = True
        ''' <summary>
        ''' Specify is the report is enabled or disabled
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property status() As Boolean
            Get
                Return m_status
            End Get
            Set(ByVal value As Boolean)
                m_status = value
            End Set
        End Property


        Private m_adjustDateStampBy As Integer = 0
        ''' <summary>
        ''' Adjust the date/time stamp by n days e.g. -1 for yesterday
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property adjustDateStampBy() As Integer
            Get
                Return m_adjustDateStampBy
            End Get
            Set(ByVal value As Integer)
                m_adjustDateStampBy = value
            End Set
        End Property

    End Class
End Class
