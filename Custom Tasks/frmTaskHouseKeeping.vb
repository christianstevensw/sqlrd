﻿
Public Class frmTaskHouseKeeping
    Dim UserCancel As Boolean
    Dim eventID As Integer = 99999
    Dim eventBased As Boolean = False


    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Property directoryType As String
        Get
            If stabDirectories.SelectedTab.Text = "Local Directory" Then
                Return "LOCAL"
            Else
                Return "REMOTE"
            End If
        End Get
        Set(value As String)
            If value = "LOCAL" Then
                stabDirectories.SelectedTabIndex = 0
            Else
                stabDirectories.SelectedTabIndex = 1
            End If
        End Set
    End Property

    Private Property selectedPath As String
        Get
            If directoryType = "LOCAL" Then
                Return txtLocalDir.Text
            Else
                Return ucFTP.txtDirectory.Text
            End If
        End Get
        Set(value As String)
            If directoryType = "LOCAL" Then
                txtLocalDir.Text = value
            Else
                ucFTP.txtDirectory.Text = value
            End If
        End Set
    End Property
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" And txtName.Visible = True Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf directoryType = "LOCAL" And txtLocalDir.Text = "" Then
            setError(txtLocalDir, "Please select the path to monitor for housekeeping", ErrorIconAlignment.MiddleRight)
            txtLocalDir.Focus()
            Return
        ElseIf directoryType = "REMOTE" And ucFTP.txtDirectory.Text = "" Then
            setError(ucFTP.txtDirectory, "Please select the FTP directory to monitor for housekeeping", ErrorIconAlignment.MiddleRight)
            ucFTP.txtDirectory.Focus()
            Return
        ElseIf cmbAgeUnit.Text = "" Then
            setError(cmbAgeUnit, "Please select the age unit")
            cmbAgeUnit.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFile As String
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID

        '//columns to use
        '//pauseint for age
        '//printername for ageunit
        '//replacefiles for include subfolders
        '//windowstyle for directory type

        sCols = "TaskID,ScheduleID,TaskType,TaskName,FTPServer,FTPPort,FTPUser," & _
        "FTPPassword,FTPDirectory,FTPType,FTPPassive,FtpOptions,pauseint,printername,orderid,replacefiles,windowstyle"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'FolderHouseKeeping'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(ucFTP.txtFTPServer.Text) & "'," & _
            "" & SQLPrepare(ucFTP.txtPort.Text) & "," & _
            "'" & SQLPrepare(ucFTP.txtUserName.Text) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(ucFTP.txtPassword.Text)) & "'," & _
            "'" & SQLPrepare(selectedPath) & "'," & _
            "'" & SQLPrepare(ucFTP.cmbFTPType.Text) & "'," & _
             Convert.ToInt32(ucFTP.chkPassive.Checked) & "," & _
            "'" & SQLPrepare(ucFTP.m_ftpOptions) & "'," & _
            txtAge.Value & "," & _
            "'" & cmbAgeUnit.Text & "'," & _
            oTask.GetOrderID(ScheduleID) & "," & _
             Convert.ToInt32(chkSubFolders.Checked) & "," & _
            "'" & directoryType & "'"


        SQL = "INSERT INTO TASKS (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID

    End Function

    Public Sub editTask(ByVal nTaskID As Integer, Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim sFile As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            directoryType = oRs("windowstyle").Value
            txtAge.Value = oRs("pauseint").Value
            cmbAgeUnit.Text = oRs("printername").Value

            With ucFTP
                .txtFTPServer.Text = oRs("ftpserver").Value
                .txtUserName.Text = oRs("ftpuser").Value
                .txtPassword.Text = _DecryptDBValue(oRs("ftppassword").Value)
                .cmbFTPType.Text = oRs("ftptype").Value
                .chkPassive.Checked = oRs("ftppassive").Value
                .m_ftpOptions = oRs("ftpoptions").Value
                .txtPort.Value = oRs("ftpport").Value
            End With

            chkSubFolders.Checked = oRs("replacefiles").Value
            selectedPath = oRs("ftpdirectory").Value

            oRs.Close()
        End If

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel Then Return

        SQL = "taskname = '" & SQLPrepare(txtName.Text) & "'," & _
            "windowstyle = '" & directoryType & "'," & _
            "pauseint = " & txtAge.Value & "," & _
            "printername = '" & cmbAgeUnit.Text & "'," & _
            "ftpserver = '" & SQLPrepare(ucFTP.txtFTPServer.Text) & "'," & _
            "ftpport =" & ucFTP.txtPort.Value & "," & _
            "ftpuser ='" & SQLPrepare(ucFTP.txtUserName.Text) & "'," & _
            "ftppassword ='" & SQLPrepare(_EncryptDBValue(ucFTP.txtPassword.Text)) & "'," & _
            "ftptype ='" & ucFTP.cmbFTPType.Text & "'," & _
            "ftppassive =" & Convert.ToInt32(ucFTP.chkPassive.Checked) & "," & _
            "ftpoptions ='" & SQLPrepare(ucFTP.m_ftpOptions) & "'," & _
            "ftpdirectory ='" & SQLPrepare(selectedPath) & "'," & _
            "replacefiles = " & Convert.ToInt32(chkSubFolders.Checked)

        SQL = "UPDATE tasks SET " & SQL & " WHERE taskid = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)

    End Sub

    Private Sub frmTaskHouseKeeping_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtLocalDir)

        showInserter(Me, m_eventID)
    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Dim fbd As FolderBrowserDialog = New FolderBrowserDialog

        With fbd
            .Description = "Please select the directory"
            .ShowNewFolderButton = True

            If .ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtLocalDir.Text = .SelectedPath
            End If

        End With
    End Sub
End Class