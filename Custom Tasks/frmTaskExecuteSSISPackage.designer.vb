﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskExecuteSSISPackage
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskExecuteSSISPackage))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabPackLocation = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.txtLocalSSISPackagePath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.stabLocal = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.txtDTSSSISPackagePath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtDTSServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.stabDTS = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtSQLServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbSSISName = New System.Windows.Forms.ComboBox()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.txtServerUserName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtServerPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.stabSQL = New DevComponents.DotNetBar.SuperTabItem()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.lsvVariables = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAddVar = New DevComponents.DotNetBar.ButtonX()
        Me.btnRemoveVar = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtVarName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.txtVarValue = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.tabVariables = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.stabPackLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabPackLocation.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(606, 28)
        Me.FlowLayoutPanel1.TabIndex = 12
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 21)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(490, 21)
        Me.txtName.TabIndex = 5
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 339)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(606, 29)
        Me.FlowLayoutPanel2.TabIndex = 13
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(528, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(447, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        '
        'stabMain
        '
        Me.stabMain.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabMain.ForeColor = System.Drawing.Color.Black
        Me.stabMain.Location = New System.Drawing.Point(0, 28)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 0
        Me.stabMain.Size = New System.Drawing.Size(606, 311)
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabMain.TabIndex = 14
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabVariables, Me.SuperTabItem2})
        Me.stabMain.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.stabPackLocation)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(606, 285)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'stabPackLocation
        '
        Me.stabPackLocation.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabPackLocation.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabPackLocation.ControlBox.MenuBox.Name = ""
        Me.stabPackLocation.ControlBox.Name = ""
        Me.stabPackLocation.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabPackLocation.ControlBox.MenuBox, Me.stabPackLocation.ControlBox.CloseBox})
        Me.stabPackLocation.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabPackLocation.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabPackLocation.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabPackLocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabPackLocation.ForeColor = System.Drawing.Color.Black
        Me.stabPackLocation.Location = New System.Drawing.Point(0, 0)
        Me.stabPackLocation.Name = "stabPackLocation"
        Me.stabPackLocation.ReorderTabsEnabled = True
        Me.stabPackLocation.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabPackLocation.SelectedTabIndex = 0
        Me.stabPackLocation.Size = New System.Drawing.Size(606, 285)
        Me.stabPackLocation.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabPackLocation.TabIndex = 2
        Me.stabPackLocation.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSQL, Me.stabDTS, Me.stabLocal})
        Me.stabPackLocation.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(606, 261)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.stabLocal
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX8)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtLocalSSISPackagePath)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnBrowse)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 6)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(537, 31)
        Me.FlowLayoutPanel3.TabIndex = 10
        '
        'LabelX8
        '
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(3, 3)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(74, 23)
        Me.LabelX8.TabIndex = 7
        Me.LabelX8.Text = "Package Path"
        '
        'txtLocalSSISPackagePath
        '
        Me.txtLocalSSISPackagePath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtLocalSSISPackagePath.Border.Class = "TextBoxBorder"
        Me.txtLocalSSISPackagePath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLocalSSISPackagePath.DisabledBackColor = System.Drawing.Color.White
        Me.txtLocalSSISPackagePath.ForeColor = System.Drawing.Color.Black
        Me.txtLocalSSISPackagePath.Location = New System.Drawing.Point(83, 3)
        Me.txtLocalSSISPackagePath.Name = "txtLocalSSISPackagePath"
        Me.txtLocalSSISPackagePath.ReadOnly = True
        Me.txtLocalSSISPackagePath.Size = New System.Drawing.Size(371, 21)
        Me.txtLocalSSISPackagePath.TabIndex = 8
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(460, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(50, 23)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnBrowse.TabIndex = 9
        Me.btnBrowse.Text = "..."
        '
        'stabLocal
        '
        Me.stabLocal.AttachedControl = Me.SuperTabControlPanel6
        Me.stabLocal.GlobalItem = False
        Me.stabLocal.Name = "stabLocal"
        Me.stabLocal.Text = "Local File"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(543, 261)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.stabDTS
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX7, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDTSSSISPackagePath, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDTSServer, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX6, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(537, 62)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'LabelX7
        '
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(3, 3)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(67, 23)
        Me.LabelX7.TabIndex = 4
        Me.LabelX7.Text = "DTS Server"
        '
        'txtDTSSSISPackagePath
        '
        Me.txtDTSSSISPackagePath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDTSSSISPackagePath.Border.Class = "TextBoxBorder"
        Me.txtDTSSSISPackagePath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDTSSSISPackagePath.DisabledBackColor = System.Drawing.Color.White
        Me.txtDTSSSISPackagePath.ForeColor = System.Drawing.Color.Black
        Me.txtDTSSSISPackagePath.Location = New System.Drawing.Point(76, 32)
        Me.txtDTSSSISPackagePath.Name = "txtDTSSSISPackagePath"
        Me.txtDTSSSISPackagePath.Size = New System.Drawing.Size(439, 21)
        Me.txtDTSSSISPackagePath.TabIndex = 1
        '
        'txtDTSServer
        '
        Me.txtDTSServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDTSServer.Border.Class = "TextBoxBorder"
        Me.txtDTSServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDTSServer.DisabledBackColor = System.Drawing.Color.White
        Me.txtDTSServer.ForeColor = System.Drawing.Color.Black
        Me.txtDTSServer.Location = New System.Drawing.Point(76, 3)
        Me.txtDTSServer.Name = "txtDTSServer"
        Me.txtDTSServer.Size = New System.Drawing.Size(439, 21)
        Me.txtDTSServer.TabIndex = 0
        '
        'LabelX6
        '
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(3, 32)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(67, 23)
        Me.LabelX6.TabIndex = 3
        Me.LabelX6.Text = "Package Path"
        '
        'stabDTS
        '
        Me.stabDTS.AttachedControl = Me.SuperTabControlPanel5
        Me.stabDTS.GlobalItem = False
        Me.stabDTS.Name = "stabDTS"
        Me.stabDTS.Text = "DTS Server"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.TableLayoutPanel3)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(606, 261)
        Me.SuperTabControlPanel4.TabIndex = 1
        Me.SuperTabControlPanel4.TabItem = Me.stabSQL
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.2197!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.7803!))
        Me.TableLayoutPanel3.Controls.Add(Me.txtSQLServer, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cmbSSISName, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX2, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX3, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX4, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtServerUserName, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX5, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.txtServerPassword, 1, 2)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(12, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(528, 117)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'txtSQLServer
        '
        Me.txtSQLServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSQLServer.Border.Class = "TextBoxBorder"
        Me.txtSQLServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSQLServer.DisabledBackColor = System.Drawing.Color.White
        Me.txtSQLServer.ForeColor = System.Drawing.Color.Black
        Me.txtSQLServer.Location = New System.Drawing.Point(152, 3)
        Me.txtSQLServer.Name = "txtSQLServer"
        Me.txtSQLServer.Size = New System.Drawing.Size(355, 21)
        Me.txtSQLServer.TabIndex = 0
        '
        'cmbSSISName
        '
        Me.cmbSSISName.FormattingEnabled = True
        Me.cmbSSISName.Location = New System.Drawing.Point(152, 90)
        Me.cmbSSISName.Name = "cmbSSISName"
        Me.cmbSSISName.Size = New System.Drawing.Size(355, 21)
        Me.cmbSSISName.TabIndex = 3
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 3)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(67, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "SQL Server"
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 90)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(109, 21)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Package Name"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 32)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 23)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "User ID"
        '
        'txtServerUserName
        '
        Me.txtServerUserName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtServerUserName.Border.Class = "TextBoxBorder"
        Me.txtServerUserName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServerUserName.DisabledBackColor = System.Drawing.Color.White
        Me.txtServerUserName.ForeColor = System.Drawing.Color.Black
        Me.txtServerUserName.Location = New System.Drawing.Point(152, 32)
        Me.txtServerUserName.Name = "txtServerUserName"
        Me.txtServerUserName.Size = New System.Drawing.Size(355, 21)
        Me.txtServerUserName.TabIndex = 1
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(3, 61)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(75, 23)
        Me.LabelX5.TabIndex = 0
        Me.LabelX5.Text = "Password"
        '
        'txtServerPassword
        '
        Me.txtServerPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtServerPassword.Border.Class = "TextBoxBorder"
        Me.txtServerPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServerPassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtServerPassword.ForeColor = System.Drawing.Color.Black
        Me.txtServerPassword.Location = New System.Drawing.Point(152, 61)
        Me.txtServerPassword.Name = "txtServerPassword"
        Me.txtServerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtServerPassword.Size = New System.Drawing.Size(355, 21)
        Me.txtServerPassword.TabIndex = 2
        '
        'stabSQL
        '
        Me.stabSQL.AttachedControl = Me.SuperTabControlPanel4
        Me.stabSQL.GlobalItem = False
        Me.stabSQL.Name = "stabSQL"
        Me.stabSQL.Text = "SQL Server"
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvVariables)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnAddVar)
        Me.SuperTabControlPanel3.Controls.Add(Me.btnRemoveVar)
        Me.SuperTabControlPanel3.Controls.Add(Me.TableLayoutPanel2)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(606, 285)
        Me.SuperTabControlPanel3.TabIndex = 2
        Me.SuperTabControlPanel3.TabItem = Me.tabVariables
        '
        'lsvVariables
        '
        Me.lsvVariables.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvVariables.Border.Class = "ListViewBorder"
        Me.lsvVariables.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvVariables.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvVariables.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvVariables.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lsvVariables.ForeColor = System.Drawing.Color.Black
        Me.lsvVariables.FullRowSelect = True
        Me.lsvVariables.GridLines = True
        Me.lsvVariables.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvVariables.HideSelection = False
        Me.lsvVariables.Location = New System.Drawing.Point(0, 86)
        Me.lsvVariables.Name = "lsvVariables"
        Me.lsvVariables.Size = New System.Drawing.Size(606, 199)
        Me.lsvVariables.TabIndex = 2
        Me.lsvVariables.UseCompatibleStateImageBehavior = False
        Me.lsvVariables.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 202
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Value"
        Me.ColumnHeader2.Width = 184
        '
        'btnAddVar
        '
        Me.btnAddVar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddVar.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddVar.Image = CType(resources.GetObject("btnAddVar.Image"), System.Drawing.Image)
        Me.btnAddVar.Location = New System.Drawing.Point(346, 57)
        Me.btnAddVar.Name = "btnAddVar"
        Me.btnAddVar.Size = New System.Drawing.Size(37, 23)
        Me.btnAddVar.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnAddVar.TabIndex = 0
        '
        'btnRemoveVar
        '
        Me.btnRemoveVar.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemoveVar.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemoveVar.Image = CType(resources.GetObject("btnRemoveVar.Image"), System.Drawing.Image)
        Me.btnRemoveVar.Location = New System.Drawing.Point(3, 57)
        Me.btnRemoveVar.Name = "btnRemoveVar"
        Me.btnRemoveVar.Size = New System.Drawing.Size(37, 23)
        Me.btnRemoveVar.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnRemoveVar.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.txtVarName, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX9, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtVarValue, 1, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(606, 51)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'txtVarName
        '
        Me.txtVarName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtVarName.Border.Class = "TextBoxBorder"
        Me.txtVarName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtVarName.DisabledBackColor = System.Drawing.Color.White
        Me.txtVarName.ForeColor = System.Drawing.Color.Black
        Me.txtVarName.Location = New System.Drawing.Point(3, 26)
        Me.txtVarName.Name = "txtVarName"
        Me.txtVarName.Size = New System.Drawing.Size(191, 21)
        Me.txtVarName.TabIndex = 0
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 17)
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = "Name"
        '
        'LabelX9
        '
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(306, 3)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(75, 17)
        Me.LabelX9.TabIndex = 1
        Me.LabelX9.Text = "Value"
        '
        'txtVarValue
        '
        Me.txtVarValue.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtVarValue.Border.Class = "TextBoxBorder"
        Me.txtVarValue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtVarValue.DisabledBackColor = System.Drawing.Color.White
        Me.txtVarValue.ForeColor = System.Drawing.Color.Black
        Me.txtVarValue.Location = New System.Drawing.Point(306, 26)
        Me.txtVarValue.Name = "txtVarValue"
        Me.txtVarValue.Size = New System.Drawing.Size(183, 21)
        Me.txtVarValue.TabIndex = 1
        '
        'tabVariables
        '
        Me.tabVariables.AttachedControl = Me.SuperTabControlPanel3
        Me.tabVariables.GlobalItem = False
        Me.tabVariables.Name = "tabVariables"
        Me.tabVariables.Text = "Variables"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(606, 285)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 149)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'frmTaskExecuteSSISPackage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(606, 368)
        Me.ControlBox = False
        Me.Controls.Add(Me.stabMain)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmTaskExecuteSSISPackage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Execute an SSIS Package"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.stabPackLocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabPackLocation.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabVariables As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabPackLocation As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabSQL As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabDTS As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabLocal As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents cmbSSISName As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSQLServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtServerUserName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtServerPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDTSSSISPackagePath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDTSServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtLocalSSISPackagePath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtVarName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtVarValue As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lsvVariables As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnAddVar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnRemoveVar As DevComponents.DotNetBar.ButtonX
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
End Class
