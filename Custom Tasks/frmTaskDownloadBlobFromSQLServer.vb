﻿Imports System.Data.SqlClient

Public Class frmTaskDownloadBlobFromSQLServer
    Dim ep As ErrorProvider = New ErrorProvider
    Dim con As SqlConnection
    Dim hsDataTypes As Hashtable
    Dim eventID As Integer = 99999
    Dim eventBased As Boolean = False
    Dim cancel As Boolean = False
    Private Property connectionString As String = ""

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return Me.eventBased
        End Get
        Set(ByVal value As Boolean)
            Me.eventBased = value
        End Set
    End Property

    Private WriteOnly Property m_connectionString As String
        Set(value As String)
            '"Data Source=.\crd;Initial Catalog=northwind;Integrated Security=True"
            '"Data Source=.\crd;Initial Catalog=northwind;User ID=sa;Password=cssAdmin12345$"

            For Each s As String In value.Split(New String() {";"}, StringSplitOptions.RemoveEmptyEntries)
                If s = "" Then Continue For

                Dim parameter As String = s.Split("=")(0)
                Dim val As String = s.Split("=")(1)

                Select Case parameter.ToLower
                    Case "data source"
                        txtServerName.Text = val
                    Case "initial catalog"
                        txtDatabaseName.Text = val
                    Case "user id"
                        txtUsername.Text = val
                    Case "password"
                        txtPassword.Text = val
                End Select
            Next
        End Set
    End Property

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Try
            Dim conBuilder As SqlConnectionStringBuilder = New SqlConnectionStringBuilder

            With conBuilder
                .DataSource = txtServerName.Text
                .InitialCatalog = txtDatabaseName.Text

                If txtUsername.Text = "" And txtPassword.Text = "" Then
                    .IntegratedSecurity = True
                Else
                    .UserID = txtUsername.Text
                    .Password = txtPassword.Text
                End If

            End With

            con = New SqlConnection(conBuilder.ConnectionString)

            con.Open()

            '//Retrieve the Table schema info
            Dim tbls As DataTable

            tbls = con.GetSchema("Tables")

            '//For each user table, retrieve all the data and insert into our dataset
            For Each dr As DataRow In tbls.Rows

                Dim tableName As String = dr("TABLE_NAME")
                Dim tableType As String = dr("TABLE_TYPE")

                '//Only get the data for the user tables (non-system)
                If (tableType = "TABLE") Or (tableType = "BASE TABLE") Then
                    cmbTable.Items.Add(tableName)
                End If
            Next

            GroupPanel1.Enabled = True



            connectionString = conBuilder.ConnectionString

            conBuilder = Nothing
            tbls = Nothing
        Catch ex As Exception
            ep.SetError(txtServerName, ex.Message)
            GroupPanel1.Enabled = False
        End Try
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim ofd As SaveFileDialog = New SaveFileDialog
        ofd.Title = "Save file as"
        ofd.Filter = "All Files (*.*)|*.*"

        If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            txtPath.Text = ofd.FileName
        End If
    End Sub

    Private Sub frmTaskDownloadBlobFromSQLServer_Load(sender As Object, e As EventArgs) Handles Me.Load
        setupForDragAndDrop(txtPath)
        setupForDragAndDrop(txtSQL)

        showInserter(Me, m_eventID)

#If DEBUG Then
        txtServerName.Text = ".\crd"
        txtDatabaseName.Text = "Northwind"
#End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        cancel = True

        If con IsNot Nothing Then
            con.Close()
            con = Nothing
        End If

        Close()
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If txtName.Text = "" Then
            setError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf connectionString = "" Then
            setError(txtServerName, "Please configure the SQL Server connection properties")
            txtServerName.Focus()
            Return
        ElseIf cmbTable.Text = "" Then
            setError(cmbTable, "Please the table to insert the data into")
            cmbTable.Focus()
            Return
        ElseIf cmbColumn.Text = "" Then
            setError(cmbColumn, "Please select the column that holds the file")
            cmbColumn.Focus()
            Return
        ElseIf txtSQL.Text = "" Then
            setError(cmbTable, "Please build your selection query")
            cmbTable.Focus()
            Return
        End If

        If con IsNot Nothing Then con.Close() : con = Nothing

        Close()
    End Sub

    Public Function addTask(scheduleID As Integer, showAfterType As Boolean) As Integer
        SuperTabItem2.Visible = showAfterType

        Me.ShowDialog()

        If cancel Then Return 0

        Dim newTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        Dim cols, vals As String

        'Connection String -> ProgramParameters
        'Table Inserted -> ProgramPath
        'Columns for insert -> SendTo
        'filelist ---> the path

        cols = "taskid,taskname,scheduleid,tasktype,programparameters,programpath,sendto,filelist,msg"
        vals = newTaskID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            scheduleID & "," & _
            "'downloadblob'," & _
            "'" & SQLPrepare(_EncryptDBValue(connectionString)) & "'," & _
            "'" & SQLPrepare(cmbTable.Text) & "'," & _
            "'" & SQLPrepare(cmbColumn.Text) & "'," & _
            "'" & SQLPrepare(txtPath.Text) & "'," & _
            "'" & SQLPrepare(txtSQL.Text) & "'"

        clsMarsData.DataItem.InsertData("tasks", cols, vals)

        TaskExecutionPath1.setTaskRunWhen(newTaskID)

        Return newTaskID
    End Function

    Public Sub editTask(taskID As Integer, Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String = "SELECT * FROM Tasks WHERE TaskID = " & taskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table Inserted -> ProgramPath (";" separated)
        'Columns for insert -> SendTo

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            m_connectionString = _DecryptDBValue(oRs("programparameters").Value)

            btnTest_Click(Nothing, Nothing)

            cmbTable.DropDownStyle = ComboBoxStyle.DropDown
            cmbTable.SelectedText = oRs("programpath").Value

            cmbColumn.DropDownStyle = ComboBoxStyle.DropDown
            cmbColumn.Text = oRs("sendto").Value
            txtPath.Text = IsNull(oRs("filelist").Value)

            txtSQL.Text = IsNull(oRs("msg").Value)

            sMode = "advanced"

            GroupPanel3.Enabled = True

            oRs.Close()

            oRs = Nothing
        End If

        TaskExecutionPath1.LoadTaskRunWhen(taskID)

        Me.ShowDialog()

        If cancel = True Then Return

        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                 "ProgramParameters = '" & SQLPrepare(_EncryptDBValue(connectionString)) & "'," & _
                 "ProgramPath = '" & SQLPrepare(cmbTable.Text) & "'," & _
                 "SendTo = '" & SQLPrepare(cmbColumn.Text) & "'," & _
                 "FileList = '" & SQLPrepare(txtPath.Text) & "'," & _
                 "Msg = '" & SQLPrepare(txtSQL.Text) & "'"

        SQL = "UPDATE tasks SET " & SQL & " WHERE taskid = " & taskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(taskID)
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTable.SelectedIndexChanged
        Try
            cmbColumn.Items.Clear()
            cmbColumn.Text = ""

            Dim da As SqlDataAdapter = New SqlDataAdapter("SELECT TOP 1 * FROM [" & cmbTable.Text & "]", con)

            Dim dt As DataTable = New DataTable

            da.Fill(dt)

            hsDataTypes = New Hashtable

            Dim columns As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)

            For Each col As DataColumn In dt.Columns
                cmbColumn.Items.Add(col.ColumnName)
                hsDataTypes.Add(col.ColumnName, col.DataType.ToString)
                columns.Add(col.ColumnName)
            Next

            ucCritBuilder.m_columns = columns
            ucCritBuilder.m_dataTypes = hsDataTypes

            If ucCritBuilder.Panel1.Controls.Count = 0 Then
                ucCritBuilder.addCriteriaControl()
            Else
                For Each uc As ucCriteriaSQL In ucCritBuilder.Panel1.Controls
                    uc.columns = columns
                Next
            End If

            dt = Nothing
            da.Dispose()
            da = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        End Try
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbColumn.SelectedIndexChanged
        '//get the datatype
        Dim dataType As String = hsDataTypes.Item(cmbColumn.Text)

        If dataType.ToLower <> "system.byte[]" Then
            MessageBox.Show("You must pick a column with data type IMAGE or BINARY", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
            cmbColumn.Items.Clear()
        End If
    End Sub

    Private Sub cmbColumn_DropDown(sender As Object, e As EventArgs) Handles cmbColumn.DropDown
        If cmbColumn.Items.Count = 0 And cmbTable.Text <> "" Then
            Dim da As SqlDataAdapter = New SqlDataAdapter("SELECT TOP 1 * FROM " & cmbTable.Text, con)

            Dim dt As DataTable = New DataTable

            da.Fill(dt)

            hsDataTypes = New Hashtable

            For Each col As DataColumn In dt.Columns
                cmbColumn.Items.Add(col.ColumnName)
                hsDataTypes.Add(col.ColumnName, col.DataType.ToString)
            Next

            dt = Nothing
            da.Dispose()
            da = Nothing
        End If
    End Sub

    Property sMode As String
        Get
            Return tabSQL.SelectedTab.Text.ToLower
        End Get
        Set(value As String)
            If value.ToLower = "simple" Then
                tabSQL.SelectedTab = tabSimple
            Else
                tabSQL.SelectedTab = tabAdvanced
            End If
        End Set
    End Property
    Property fullQuery As String
        Get
            Dim query As String = ""

            If sMode = "simple" Then
                query = "SELECT [" & cmbTable.Text & "].[" & cmbColumn.Text & "] FROM " & cmbTable.Text

                If ucCritBuilder.fullCriteria <> "" Then
                    query = query & " WHERE " & ucCritBuilder.fullCriteria
                End If
            End If

            Return query
        End Get
        Set(value As String)
            txtSQL.Text = value

            sMode = "advanced"
        End Set
    End Property
    Private Sub cmdParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParse.Click
        Try
            If sMode = "simple" Then
                txtSQL.Text = fullQuery
            End If

            Dim da As SqlDataAdapter = New SqlDataAdapter(clsMarsParser.Parser.ParseString(txtSQL.Text), con)
            Dim dt As DataTable = New DataTable

            da.Fill(dt)

            da.Dispose()
            da = Nothing

            MessageBox.Show("SQL parsed successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            tabSQL.SelectedTab = tabAdvanced


            GroupPanel3.Enabled = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GroupPanel3.Enabled = False
        End Try

    End Sub

End Class