Public Class frmTaskCopyFile
    Inherits frmTaskMaster
    Dim UserCancel As Boolean = True
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents grpAdvanced As System.Windows.Forms.Panel
    Friend WithEvents chkRecreateFolderStructure As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkRecursive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtSourceFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdBrowseSource As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents General As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Public m_eventID As Integer = 99999
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lsvFiles As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddFiles As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents fbd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents imgFiles As System.Windows.Forms.ImageList
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkReplace As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MyTip As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskCopyFile))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grpAdvanced = New System.Windows.Forms.Panel()
        Me.chkRecreateFolderStructure = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkRecursive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSourceFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBrowseSource = New DevComponents.DotNetBar.ButtonX()
        Me.chkReplace = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.lsvFiles = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddFiles = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.fbd = New System.Windows.Forms.FolderBrowserDialog()
        Me.imgFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.MyTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.rbAdvanced = New System.Windows.Forms.RadioButton()
        Me.rbSimple = New System.Windows.Forms.RadioButton()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.General = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdOK, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdOK, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(447, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.HelpProvider1.SetShowHelp(Me.cmdOK, True)
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        Me.MyTip.SetToolTip(Me.cmdOK, "Save the task")
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(81, 3)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(464, 21)
        Me.txtName.TabIndex = 0
        Me.MyTip.SetToolTip(Me.txtName, "Enter the name for the task")
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label1, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(72, 16)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.HelpProvider1.SetHelpKeyword(Me.cmdCancel, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdCancel, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(528, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.HelpProvider1.SetShowHelp(Me.cmdCancel, True)
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        Me.MyTip.SetToolTip(Me.cmdCancel, "Cancel task creation")
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.grpAdvanced)
        Me.GroupBox1.Controls.Add(Me.chkReplace)
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.lsvFiles)
        Me.GroupBox1.Controls.Add(Me.cmdAddFiles)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox1, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox1, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox1, True)
        Me.GroupBox1.Size = New System.Drawing.Size(600, 232)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.chkRecreateFolderStructure)
        Me.grpAdvanced.Controls.Add(Me.chkRecursive)
        Me.grpAdvanced.Controls.Add(Me.txtSourceFile)
        Me.grpAdvanced.Controls.Add(Me.Label8)
        Me.grpAdvanced.Controls.Add(Me.cmdBrowseSource)
        Me.grpAdvanced.Location = New System.Drawing.Point(8, 16)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(583, 136)
        Me.grpAdvanced.TabIndex = 0
        Me.grpAdvanced.TabStop = True
        '
        'chkRecreateFolderStructure
        '
        '
        '
        '
        Me.chkRecreateFolderStructure.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecreateFolderStructure.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecreateFolderStructure.Location = New System.Drawing.Point(14, 79)
        Me.chkRecreateFolderStructure.Name = "chkRecreateFolderStructure"
        Me.chkRecreateFolderStructure.Size = New System.Drawing.Size(200, 23)
        Me.chkRecreateFolderStructure.TabIndex = 3
        Me.chkRecreateFolderStructure.Text = "Recreate Folder Structure"
        '
        'chkRecursive
        '
        '
        '
        '
        Me.chkRecursive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(14, 58)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.chkRecursive.TabIndex = 2
        Me.chkRecursive.Text = "Recursive"
        '
        'txtSourceFile
        '
        Me.txtSourceFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSourceFile.Border.Class = "TextBoxBorder"
        Me.txtSourceFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSourceFile.DisabledBackColor = System.Drawing.Color.White
        Me.txtSourceFile.ForeColor = System.Drawing.Color.Black
        Me.txtSourceFile.Location = New System.Drawing.Point(14, 32)
        Me.txtSourceFile.Name = "txtSourceFile"
        Me.txtSourceFile.Size = New System.Drawing.Size(520, 21)
        Me.txtSourceFile.TabIndex = 0
        Me.txtSourceFile.Tag = "Memo"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(14, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(89, 16)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "File(s)"
        '
        'cmdBrowseSource
        '
        Me.cmdBrowseSource.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowseSource.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowseSource.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowseSource.Location = New System.Drawing.Point(540, 32)
        Me.cmdBrowseSource.Name = "cmdBrowseSource"
        Me.cmdBrowseSource.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowseSource.TabIndex = 1
        Me.cmdBrowseSource.Text = "..."
        '
        'chkReplace
        '
        '
        '
        '
        Me.chkReplace.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkReplace.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkReplace.Location = New System.Drawing.Point(8, 200)
        Me.chkReplace.Name = "chkReplace"
        Me.chkReplace.Size = New System.Drawing.Size(360, 24)
        Me.chkReplace.TabIndex = 5
        Me.chkReplace.Text = "Replace existing files at destination"
        Me.MyTip.SetToolTip(Me.chkReplace, "Replace existing files at the destination")
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(548, 176)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 21)
        Me.cmdBrowse.TabIndex = 4
        Me.cmdBrowse.Text = "..."
        Me.MyTip.SetToolTip(Me.cmdBrowse, "Browse to the destination directory")
        '
        'lsvFiles
        '
        Me.lsvFiles.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvFiles.Border.Class = "ListViewBorder"
        Me.lsvFiles.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.LabelEdit = True
        Me.lsvFiles.Location = New System.Drawing.Point(8, 16)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(392, 136)
        Me.lsvFiles.TabIndex = 1
        Me.MyTip.SetToolTip(Me.lsvFiles, "Files to copy")
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 380
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(416, 16)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 23)
        Me.cmdAddFiles.TabIndex = 0
        Me.MyTip.SetToolTip(Me.cmdAddFiles, "Add a file to copy")
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(416, 48)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 23)
        Me.cmdRemove.TabIndex = 2
        Me.MyTip.SetToolTip(Me.cmdRemove, "Remove selected file from the list")
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 160)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Destination Folder"
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.DisabledBackColor = System.Drawing.Color.White
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 176)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(534, 21)
        Me.txtPath.TabIndex = 3
        Me.MyTip.SetToolTip(Me.txtPath, "Destination to copy the files to")
        '
        'ofd
        '
        Me.ofd.Multiselect = True
        '
        'imgFiles
        '
        Me.imgFiles.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgFiles.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFiles.TransparentColor = System.Drawing.Color.Transparent
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.rbAdvanced, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.rbAdvanced, System.Windows.Forms.HelpNavigator.Topic)
        Me.rbAdvanced.Location = New System.Drawing.Point(74, 3)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.HelpProvider1.SetShowHelp(Me.rbAdvanced, True)
        Me.rbAdvanced.Size = New System.Drawing.Size(73, 17)
        Me.rbAdvanced.TabIndex = 2
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = False
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.rbSimple, "Copy_File.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.rbSimple, System.Windows.Forms.HelpNavigator.Topic)
        Me.rbSimple.Location = New System.Drawing.Point(3, 3)
        Me.rbSimple.Name = "rbSimple"
        Me.HelpProvider1.SetShowHelp(Me.rbSimple, True)
        Me.rbSimple.Size = New System.Drawing.Size(55, 17)
        Me.rbSimple.TabIndex = 1
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 27)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(606, 288)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 51
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.General, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.rbSimple)
        Me.SuperTabControlPanel1.Controls.Add(Me.rbAdvanced)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(606, 262)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.General
        '
        'General
        '
        Me.General.AttachedControl = Me.SuperTabControlPanel1
        Me.General.GlobalItem = False
        Me.General.Name = "General"
        Me.General.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(606, 262)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 52
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(606, 27)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 315)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(606, 30)
        Me.FlowLayoutPanel2.TabIndex = 52
        '
        'frmTaskCopyFile
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(606, 345)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.MinimizeBox = False
        Me.Name = "frmTaskCopyFile"
        Me.Text = "Copy File"
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.grpAdvanced.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.PerformLayout()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click
        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            If sFile.Length > 0 Then
                sFile = _CreateUNC(sFile)
                lsv = lsvFiles.Items.Add(sFile)
                lsv.ImageIndex = 0
            End If
        Next

        SetError(lsvFiles, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With fbd
            .ShowNewFolderButton = True
            .Description = "Please select the destination folder"
            .ShowDialog()
        End With

        Dim sTemp As String = ""

        sTemp = fbd.SelectedPath

        If sTemp = "" Then Exit Sub

        If sTemp.Substring(sTemp.Length - 1, 1) <> "\" Then sTemp &= "\"

        txtPath.Text = _CreateUNC(sTemp)

        SetError(txtPath, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf rbSimple.Checked = True And lsvFiles.Items.Count = 0 Then
            SetError(lsvFiles, "Please select the file(s) to copy")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf rbAdvanced.Checked = True And txtSourceFile.Text = "" Then
            SetError(txtSourceFile, "Please select the file(s) to copy")
            txtSourceFile.Focus()
            Exit Sub
        ElseIf txtPath.Text = "" Then
            SetError(txtPath, "Please select the destination directory")
            txtPath.Focus()
            Exit Sub
        End If
        UserCancel = False
        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        rbSimple.Checked = True
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,ReplaceFiles,OrderID,CC,Bcc"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'CopyFile'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        Convert.ToInt32(chkReplace.Checked) & "," & _
        oTask.GetOrderID(ScheduleID) & "," & _
        "'" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
        "'" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'"

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID

    End Function

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, "")
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            If sFiles.Contains("|") = True Then

                Me.rbSimple.Checked = True
                Me.rbAdvanced.Checked = False
                Me.grpAdvanced.Enabled = False
                Me.grpAdvanced.Visible = False

                For Each sVal In sFiles.Split("|")
                    If sVal.Length > 0 Then lsvFiles.Items.Add(sVal)
                Next
            Else
                txtSourceFile.Text = sFiles
                Me.rbSimple.Checked = False
                Me.rbAdvanced.Checked = True
                Me.grpAdvanced.Visible = True
                Me.grpAdvanced.Enabled = True
                Me.grpAdvanced.BringToFront()
                Me.lsvFiles.Enabled = False
                Me.cmdAddFiles.Enabled = False
                Me.cmdRemove.Enabled = False

            End If

            txtPath.Text = oRs("programpath").Value

            chkReplace.Checked = oRs("replacefiles").Value
            'cc = recursive, bcc = recreate folder structure
            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))
            chkRecreateFolderStructure.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0))))

        End If

        oRs.Close()

        TaskExecutionPath1.setTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'," & _
            "ReplaceFiles = " & Convert.ToInt32(chkReplace.Checked) & "," & _
            "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
            "Bcc = '" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub frmTaskCopyFile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)
        showInserter(Me, Me.m_eventID, False)
        HelpProvider1.HelpNamespace = gHelpPath

        setupForDragAndDrop(txtPath)
        setupForDragAndDrop(txtSourceFile)

        'txtPath.ContextMenu = Me.mnuInserter
        'txtSourceFile.ContextMenu = Me.mnuInserter
        HelpProvider1.HelpNamespace = gHelpPath
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.HelpButton = True
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
        Me.ControlBox = True
        txtName.Focus()
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            Me.rbSimple.Checked = False
            Me.grpAdvanced.Visible = True
            Me.grpAdvanced.Enabled = True
            Me.grpAdvanced.BringToFront()
            Me.lsvFiles.Enabled = False
            Me.cmdAddFiles.Enabled = False
            Me.cmdRemove.Enabled = False
        End If
    End Sub

    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            Me.rbAdvanced.Checked = False
            Me.grpAdvanced.Visible = False
            Me.grpAdvanced.Enabled = False
            Me.lsvFiles.Enabled = True
            Me.cmdAddFiles.Enabled = True
            Me.cmdRemove.Enabled = True
        End If
    End Sub

    Private Sub cmdBrowseSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseSource.Click
        Dim sFD As OpenFileDialog = New OpenFileDialog
        sFD.Title = "Select file to upload"

        If sFD.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Me.txtSourceFile.Text = _CreateUNC(sFD.FileName)
        End If
    End Sub
End Class
