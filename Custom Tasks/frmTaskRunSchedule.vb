Public Class frmTaskRunSchedule
    Inherits frmTaskMaster
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents tvMars As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskRunSchedule))
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tvMars = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.tvMars, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(528, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 19
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(447, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 18
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(74, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(499, 20)
        Me.txtName.TabIndex = 17
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 20)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.tvMars)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(591, 319)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'tvMars
        '
        Me.tvMars.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvMars.AllowDrop = True
        Me.tvMars.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvMars.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvMars.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvMars.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvMars.Location = New System.Drawing.Point(8, 36)
        Me.tvMars.Name = "tvMars"
        Me.tvMars.NodesConnector = Me.NodeConnector1
        Me.tvMars.NodeStyle = Me.ElementStyle1
        Me.tvMars.PathSeparator = ";"
        Me.tvMars.Size = New System.Drawing.Size(562, 248)
        Me.tvMars.Styles.Add(Me.ElementStyle1)
        Me.tvMars.TabIndex = 3
        Me.tvMars.Text = "AdvTree1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.DisabledBackColor = System.Drawing.Color.White
        Me.txtPath.ForeColor = System.Drawing.Color.Black
        Me.txtPath.Location = New System.Drawing.Point(8, 290)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(562, 20)
        Me.txtPath.TabIndex = 2
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(440, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Please select the schedule to run"
        '
        'imgFolders
        '
        Me.imgFolders.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgFolders.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(606, 351)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 22
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(606, 326)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(606, 351)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 0)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(606, 28)
        Me.FlowLayoutPanel1.TabIndex = 23
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 379)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(606, 29)
        Me.FlowLayoutPanel2.TabIndex = 23
        '
        'frmTaskRunSchedule
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(606, 408)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskRunSchedule"
        Me.Text = "Run a schedule"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.tvMars, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskRunSchedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        tvMars.PathSeparator = "\"
    End Sub
    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Dim oUI As New clsMarsUI

        AppStatus(True)
        oUI.BuildAdvTree(tvMars, True)
        tvMars.Nodes(0).Expand()
        tvMars.SelectedNode = tvMars.Nodes(0)
        AppStatus(False)

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sObject As String
        Dim TaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        Dim nID As Integer

        'Subject -> schedule type
        'PauseInt -> Report or Package ID
        'ProgramPath -> Schedule Name

        sCols = "TaskID, ScheduleID,TaskType,TaskName,Subject,PauseInt,ProgramPath,OrderID"

        sObject = crdxCommon.getScheduleTypeString(tvMars.SelectedNode.Tag)
        nID = tvMars.SelectedNode.DataKey

        sVals = TaskID & "," & _
        ScheduleID & "," & _
        "'RunSchedule'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & sObject & "'," & _
        "" & nID & "," & _
        "'" & SQLPrepare(tvMars.SelectedNode.FullPath) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(TaskID)

        Return TaskID
    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oUI As New clsMarsUI
        Dim nID As Integer
        Dim sObject As String
        Dim sPath As String


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        oUI.BuildAdvTree(tvMars, True)

        If Not oRs Is Nothing And oRs.EOF = False Then

            nID = oRs("pauseint").Value
            sObject = oRs("subject").Value

            txtName.Text = oRs("taskname").Value

            sPath = oRs("programpath").Value

            Try
                txtPath.Text = tvMars.SelectedNode.FullPath
            Catch
                txtPath.Text = sPath
            End Try

            Dim nn As DevComponents.AdvTree.Node = tvMars.FindNodeByDataKey(nID)

            If nn IsNot Nothing Then
                tvMars.SelectedNode = nn
            End If
        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sObject = crdxCommon.getScheduleTypeString(tvMars.SelectedNode.Tag)
        nID = tvMars.SelectedNode.DataKey

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & SQLPrepare(tvMars.SelectedNode.FullPath) & "'," & _
            "Subject = '" & sObject & "'," & _
            "PauseInt = " & nID

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type


        TaskExecutionPath1.setTaskRunWhen(nTaskID)

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If tvMars.SelectedNode Is Nothing Then
            SetError(tvMars, "Please select the schedule to execute")
            tvMars.Focus()
            Return
        ElseIf txtName.Text.Length = 0 Then
            SetError(txtName, "Please provide a name for the task")
            txtName.Focus()
            Return
        ElseIf txtPath.Text = "" Then
            setError(txtPath, "Please select the schedule to execute")
            tvMars.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Private Sub tvMars_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
        SetError(tvMars, "")
        txtPath.Text = tvMars.SelectedNode.FullPath
        tvMars.SelectedNode.Expand()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(txtName, "")
    End Sub

    
    Private Sub tvMars_AfterNodeSelect(sender As Object, e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvMars.AfterNodeSelect
        setError(tvMars, "")
        If crdxCommon.getScheduleTypeString(tvMars.SelectedNode.Tag).ToLower <> "folder" Then
            txtPath.Text = tvMars.SelectedNode.FullPath
        Else
            txtPath.Text = ""
        End If
        tvMars.SelectedNode.Expand()
    End Sub
End Class
