Public Class frmTaskPrint
    Inherits frmTaskMaster
    Dim UserCancel As Boolean = True
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdAddFiles As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPrinterName As System.Windows.Forms.ComboBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lsvFiles As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents imgFiles As System.Windows.Forms.ImageList
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents chkRecursive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtSourceFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdBrowseSource As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskPrint))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkRecursive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSourceFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBrowseSource = New DevComponents.DotNetBar.ButtonX()
        Me.cmbPrinterName = New System.Windows.Forms.ComboBox()
        Me.lsvFiles = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.imgFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdAddFiles = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.rbAdvanced = New System.Windows.Forms.RadioButton()
        Me.rbSimple = New System.Windows.Forms.RadioButton()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdOK, "Print_Document.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdOK, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(323, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.HelpProvider1.SetShowHelp(Me.cmdOK, True)
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "Print_Document.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(72, 3)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(378, 21)
        Me.txtName.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label1, "Print_Document.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(63, 21)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.HelpProvider1.SetHelpKeyword(Me.cmdCancel, "Print_Document.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdCancel, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(404, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.HelpProvider1.SetShowHelp(Me.cmdCancel, True)
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.cmbPrinterName)
        Me.GroupBox1.Controls.Add(Me.lsvFiles)
        Me.GroupBox1.Controls.Add(Me.cmdAddFiles)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox1, "Print_Document.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox1, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox1, True)
        Me.GroupBox1.Size = New System.Drawing.Size(472, 200)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.chkRecursive)
        Me.Panel1.Controls.Add(Me.txtSourceFile)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.cmdBrowseSource)
        Me.Panel1.Location = New System.Drawing.Point(6, 56)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(462, 138)
        Me.Panel1.TabIndex = 1
        '
        'chkRecursive
        '
        '
        '
        '
        Me.chkRecursive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(9, 56)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecursive, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Search and print files from the folder and all its subfolders.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecursive.TabIndex = 2
        Me.chkRecursive.Text = "Recursive"
        '
        'txtSourceFile
        '
        Me.txtSourceFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSourceFile.Border.Class = "TextBoxBorder"
        Me.txtSourceFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSourceFile.DisabledBackColor = System.Drawing.Color.White
        Me.txtSourceFile.ForeColor = System.Drawing.Color.Black
        Me.txtSourceFile.Location = New System.Drawing.Point(9, 33)
        Me.txtSourceFile.Name = "txtSourceFile"
        Me.txtSourceFile.Size = New System.Drawing.Size(323, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtSourceFile, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert Constants and database values.  You can also use w" & _
            "ildcards e.g. my*abc.*", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtSourceFile.TabIndex = 0
        Me.txtSourceFile.Tag = "Memo"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(10, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 66
        Me.Label8.Text = "File(s)"
        '
        'cmdBrowseSource
        '
        Me.cmdBrowseSource.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowseSource.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowseSource.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowseSource.Location = New System.Drawing.Point(343, 32)
        Me.cmdBrowseSource.Name = "cmdBrowseSource"
        Me.cmdBrowseSource.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowseSource.TabIndex = 1
        Me.cmdBrowseSource.Text = "..."
        '
        'cmbPrinterName
        '
        Me.cmbPrinterName.ForeColor = System.Drawing.Color.Blue
        Me.cmbPrinterName.ItemHeight = 13
        Me.cmbPrinterName.Location = New System.Drawing.Point(8, 32)
        Me.cmbPrinterName.Name = "cmbPrinterName"
        Me.cmbPrinterName.Size = New System.Drawing.Size(400, 21)
        Me.cmbPrinterName.TabIndex = 0
        '
        'lsvFiles
        '
        Me.lsvFiles.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvFiles.Border.Class = "ListViewBorder"
        Me.lsvFiles.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.Location = New System.Drawing.Point(8, 56)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(400, 136)
        Me.lsvFiles.SmallImageList = Me.imgFiles
        Me.lsvFiles.TabIndex = 2
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 395
        '
        'imgFiles
        '
        Me.imgFiles.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgFiles.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFiles.TransparentColor = System.Drawing.Color.Transparent
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddFiles.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(416, 56)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 23)
        Me.cmdAddFiles.TabIndex = 1
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(416, 88)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 23)
        Me.cmdRemove.TabIndex = 3
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Printer Name"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.rbAdvanced.Location = New System.Drawing.Point(74, 6)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(73, 17)
        Me.rbAdvanced.TabIndex = 2
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = False
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.BackColor = System.Drawing.Color.Transparent
        Me.rbSimple.Location = New System.Drawing.Point(3, 6)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(55, 17)
        Me.rbSimple.TabIndex = 1
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(482, 259)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 51
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.rbSimple)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Controls.Add(Me.rbAdvanced)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(482, 233)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(482, 259)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 287)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(482, 29)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(482, 28)
        Me.FlowLayoutPanel2.TabIndex = 52
        '
        'frmTaskPrint
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(482, 316)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.MinimizeBox = False
        Me.Name = "frmTaskPrint"
        Me.Text = "Print Task"
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.PerformLayout()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtSourceFile) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.cmbPrinterName) '.ContextMenu = Me.mnuInserter

        showInserter(Me, 99999)

        HelpProvider1.HelpNamespace = gHelpPath
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.HelpButton = True
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
        Me.ControlBox = True
        txtName.Focus()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf cmbPrinterName.Text = "" Then
            SetError(cmbPrinterName, "Please select the output printer")
            cmbPrinterName.Focus()
            Exit Sub
        ElseIf rbSimple.Checked = True And lsvFiles.Items.Count = 0 Then
            SetError(lsvFiles, "Please select the file(s) to copy")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf rbAdvanced.Checked = True And txtSourceFile.Text = "" Then
            SetError(txtSourceFile, "Please select the file(s) to copy")
            txtSourceFile.Focus()
            Exit Sub
        End If
        UserCancel = False
        Me.Close()
    End Sub

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click
        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            lsv = lsvFiles.Items.Add(sFile)
            lsv.ImageIndex = 0
        Next

        SetError(lsvFiles, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        Dim oPrint As New System.Drawing.Printing.PrinterSettings

        Dim strItem As String
        Dim I As Int32 = 0

        For Each strItem In Printing.PrinterSettings.InstalledPrinters
            cmbPrinterName.Items.Add(strItem)
        Next
        rbSimple.Checked = True
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sColumns As String
        Dim sValues As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If
        sColumns = "TaskID,ScheduleID,TaskType,TaskName,PrinterName,FileList,OrderID,CC"

        sValues = nID & "," & _
        ScheduleID & "," & _
        "'Print'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & cmbPrinterName.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        oTask.GetOrderID(ScheduleID) & "," & _
        "'" & Convert.ToInt32(chkRecursive.Checked) & "'"

        SQL = "INSERT INTO Tasks (" & sColumns & ") VALUES (" & sValues & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim oPrint As New System.Drawing.Printing.PrinterSettings

        Dim strItem As String
        Dim I As Int32 = 0


        For Each strItem In Printing.PrinterSettings.InstalledPrinters
            cmbPrinterName.Items.Add(strItem)
        Next

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            cmbPrinterName.Text = oRs("printername").Value

            sFiles = oRs("filelist").Value

            If sFiles.Contains("|") = True Then

                Me.rbSimple.Checked = True
                Me.rbAdvanced.Checked = False

                For Each sVal In sFiles.Split("|")
                    Dim oitem As ListViewItem

                    If sVal.Length > 0 Then
                        oitem = lsvFiles.Items.Add(sVal)
                        oitem.ImageIndex = 0
                    End If
                Next

            Else
                txtSourceFile.Text = sFiles
                Me.rbSimple.Checked = False
                Me.rbAdvanced.Checked = True



            End If

            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))

        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "PrinterName = '" & cmbPrinterName.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'" & "," & _
            "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(txtName, "")
    End Sub

    Private Sub cmbPrinterName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPrinterName.SelectedIndexChanged
        SetError(cmbPrinterName, "")
    End Sub


    Private Sub lsvFiles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvFiles.SelectedIndexChanged

    End Sub

    Private Sub cmdBrowseSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseSource.Click
        With ofd
            .Multiselect = False
            .ShowDialog()
        End With

        txtSourceFile.Text = _CreateUNC(ofd.FileName)
    End Sub

    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            rbAdvanced.Checked = False
            lsvFiles.Enabled = True
            cmdAddFiles.Enabled = True
            cmdRemove.Enabled = True
            Me.Panel1.Enabled = False
            Me.Panel1.Visible = False
        End If
    End Sub

    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbadvanced.Checked = True Then
            rbSimple.Checked = False
            lsvFiles.Enabled = False
            cmdAddFiles.Enabled = False
            cmdRemove.Enabled = False
            Me.Panel1.Enabled = True
            Me.Panel1.Visible = True
            Me.Panel1.BringToFront()
        End If
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.Undo()
            ElseIf TypeOf ctrl Is ComboBox Then
                Dim cmb As ComboBox = CType(ctrl, ComboBox)
            End If
        Catch : End Try

    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.Cut()
            ElseIf TypeOf ctrl Is ComboBox Then
                Dim cmb As ComboBox = CType(ctrl, ComboBox)

                Clipboard.SetText(cmb.SelectedText)

                cmb.SelectedText = ""
            End If
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.Copy()
            ElseIf TypeOf ctrl Is ComboBox Then
                Dim cmb As ComboBox = CType(ctrl, ComboBox)

                Clipboard.SetText(cmb.SelectedText)
            End If
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.Paste()
            ElseIf TypeOf ctrl Is ComboBox Then
                Dim cmb As ComboBox = CType(ctrl, ComboBox)

                cmb.SelectedText = Clipboard.GetText
            End If
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Me.txtSourceFile.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                Dim txt As TextBox = CType(ctrl, TextBox)

                txt.SelectAll()
            ElseIf TypeOf ctrl Is ComboBox Then
                Dim cmb As ComboBox = CType(ctrl, ComboBox)

                cmb.SelectAll()
            End If
        Catch : End Try
    End Sub


    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        'oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        Try

            Dim oItem As New frmDataItems
            oItem.m_EventID = 0

            Try
                Dim ctrl As Control = Me.ActiveControl

                If TypeOf ctrl Is TextBox Then
                    Dim txt As TextBox = CType(ctrl, TextBox)

                    txt.SelectedText = oItem._GetDataItem(0)

                ElseIf TypeOf ctrl Is ComboBox Then
                    Dim cmb As ComboBox = CType(ctrl, ComboBox)

                    cmb.SelectedText = oItem._GetDataItem(0)
                End If
            Catch : End Try
        Catch : End Try
    End Sub
End Class
