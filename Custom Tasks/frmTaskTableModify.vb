Public Class frmTaskTableModify
    Inherits sqlrd.frmTaskMaster
    Dim nStep As Integer = 1
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Private WithEvents txtFinal As FastColoredTextBoxNS.FastColoredTextBox
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTables As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbColumnType As System.Windows.Forms.ComboBox
    Friend WithEvents txtColumnSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbModifyType As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumnName As System.Windows.Forms.ComboBox
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdSkip As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskTableModify))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page3 = New System.Windows.Forms.GroupBox()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.txtColumnSize = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.cmbModifyType = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmbTables = New System.Windows.Forms.ComboBox()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbColumnName = New System.Windows.Forms.ComboBox()
        Me.cmbColumnType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdSkip = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtFinal = New FastColoredTextBoxNS.FastColoredTextBox()
        Me.GroupBox1.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.txtFinal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Black
        Me.txtName.Location = New System.Drawing.Point(80, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(489, 20)
        Me.txtName.TabIndex = 28
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(71, 20)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(591, 253)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.txtFinal)
        Me.Page3.Controls.Add(Me.Label7)
        Me.Page3.Location = New System.Drawing.Point(16, 7)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(569, 208)
        Me.Page3.TabIndex = 8
        Me.Page3.TabStop = False
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(136, 15)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Finalised Query"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(16, 7)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(569, 208)
        Me.Page1.TabIndex = 2
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(301, 146)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 7)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(555, 132)
        Me.UcDSN.TabIndex = 0
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.txtColumnSize)
        Me.Page2.Controls.Add(Me.Label4)
        Me.Page2.Controls.Add(Me.cmbModifyType)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.cmbTables)
        Me.Page2.Controls.Add(Me.Label1)
        Me.Page2.Controls.Add(Me.cmbColumnName)
        Me.Page2.Controls.Add(Me.cmbColumnType)
        Me.Page2.Controls.Add(Me.Label5)
        Me.Page2.Controls.Add(Me.Label6)
        Me.Page2.Location = New System.Drawing.Point(16, 7)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(569, 208)
        Me.Page2.TabIndex = 7
        Me.Page2.TabStop = False
        '
        'txtColumnSize
        '
        Me.txtColumnSize.Enabled = False
        Me.txtColumnSize.Location = New System.Drawing.Point(492, 119)
        Me.txtColumnSize.Name = "txtColumnSize"
        Me.txtColumnSize.Size = New System.Drawing.Size(71, 20)
        Me.txtColumnSize.TabIndex = 5
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Column Name"
        '
        'cmbModifyType
        '
        Me.cmbModifyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModifyType.Enabled = False
        Me.cmbModifyType.ItemHeight = 13
        Me.cmbModifyType.Items.AddRange(New Object() {"ADD COLUMN", "DROP COLUMN"})
        Me.cmbModifyType.Location = New System.Drawing.Point(8, 74)
        Me.cmbModifyType.Name = "cmbModifyType"
        Me.cmbModifyType.Size = New System.Drawing.Size(287, 21)
        Me.cmbModifyType.TabIndex = 3
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(216, 22)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "How would like to modify the table"
        '
        'cmbTables
        '
        Me.cmbTables.ItemHeight = 13
        Me.cmbTables.Location = New System.Drawing.Point(8, 30)
        Me.cmbTables.Name = "cmbTables"
        Me.cmbTables.Size = New System.Drawing.Size(287, 21)
        Me.cmbTables.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the table to modify"
        '
        'cmbColumnName
        '
        Me.cmbColumnName.ItemHeight = 13
        Me.cmbColumnName.Items.AddRange(New Object() {"ADD COLUMN", "DROP COLUMN"})
        Me.cmbColumnName.Location = New System.Drawing.Point(8, 119)
        Me.cmbColumnName.Name = "cmbColumnName"
        Me.cmbColumnName.Size = New System.Drawing.Size(287, 21)
        Me.cmbColumnName.TabIndex = 3
        '
        'cmbColumnType
        '
        Me.cmbColumnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumnType.Enabled = False
        Me.cmbColumnType.ItemHeight = 13
        Me.cmbColumnType.Items.AddRange(New Object() {"NUMBER", "DECIMAL", "TEXT", "MEMO", "DATETIME"})
        Me.cmbColumnType.Location = New System.Drawing.Point(301, 119)
        Me.cmbColumnType.Name = "cmbColumnType"
        Me.cmbColumnType.Size = New System.Drawing.Size(185, 21)
        Me.cmbColumnType.TabIndex = 3
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(301, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Type"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(492, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 15)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Size"
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(16, 223)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 21)
        Me.cmdBack.TabIndex = 6
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(529, 226)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 21)
        Me.cmdNext.TabIndex = 5
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(528, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 31
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(447, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 30
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdSkip
        '
        Me.cmdSkip.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSkip.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(8, 3)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 25)
        Me.cmdSkip.TabIndex = 32
        Me.cmdSkip.Text = "&Skip"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 27)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(606, 286)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 52
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(606, 262)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(606, 286)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(606, 27)
        Me.FlowLayoutPanel1.TabIndex = 53
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label8)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdSkip)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 313)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(606, 32)
        Me.FlowLayoutPanel2.TabIndex = 54
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(89, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(352, 23)
        Me.Label8.TabIndex = 33
        '
        'txtFinal
        '
        Me.txtFinal.AutoScrollMinSize = New System.Drawing.Size(0, 15)
        Me.txtFinal.BackBrush = Nothing
        Me.txtFinal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtFinal.CharHeight = 15
        Me.txtFinal.CharWidth = 8
        Me.txtFinal.CommentPrefix = "--"
        Me.txtFinal.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFinal.DelayedEventsInterval = 500
        Me.txtFinal.DelayedTextChangedInterval = 500
        Me.txtFinal.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.txtFinal.Font = New System.Drawing.Font("Consolas", 10.0!)
        Me.txtFinal.IsReplaceMode = False
        Me.txtFinal.Language = FastColoredTextBoxNS.Language.SQL
        Me.txtFinal.LeftBracket = Global.Microsoft.VisualBasic.ChrW(40)
        Me.txtFinal.Location = New System.Drawing.Point(8, 30)
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.Paddings = New System.Windows.Forms.Padding(0)
        Me.txtFinal.RightBracket = Global.Microsoft.VisualBasic.ChrW(41)
        Me.txtFinal.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(50, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.txtFinal.ShowLineNumbers = False
        Me.txtFinal.Size = New System.Drawing.Size(555, 172)
        Me.txtFinal.TabIndex = 7
        Me.txtFinal.TextAreaBorderColor = System.Drawing.Color.Maroon
        Me.txtFinal.WordWrap = True
        Me.txtFinal.Zoom = 100
        '
        'frmTaskTableModify
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(606, 345)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskTableModify"
        Me.Text = "Modify Table"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.Page2.ResumeLayout(False)
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.txtFinal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmbModifyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbModifyType.SelectedIndexChanged
        If cmbModifyType.Text = "ADD COLUMN" Then
            cmbColumnType.Enabled = True
        Else
            cmbColumnType.Enabled = False
            txtColumnSize.Enabled = False
        End If
    End Sub



    Private Sub cmbColumnName_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbColumnName.DropDown
        If cmbModifyType.Text = "DROP COLUMN" Then
            Dim oData As clsMarsData = New clsMarsData

            oData.GetColumns(cmbColumnName, UcDSN.cmbDSN.Text, cmbTables.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        Else
            cmbColumnName.Items.Clear()
        End If
    End Sub

    Private Sub cmbColumnType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumnType.SelectedIndexChanged
        If cmbColumnType.Text = "TEXT" Then
            txtColumnSize.Enabled = True
        Else
            txtColumnSize.Enabled = False
        End If

    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
                Select Case cmbModifyType.Text
                    Case "ADD COLUMN"
                        If cmbColumnName.Text = "" Then
                            setError(cmbColumnName, "Please enter the column name")
                            cmbColumnName.Focus()
                            Return
                        ElseIf cmbColumnType.Text = "" Then
                            setError(cmbColumnType, "Please select the column type")
                            cmbColumnType.Focus()
                            Return
                        ElseIf txtColumnSize.Text = "0" And cmbColumnType.Text = "TEXT" Then
                            setError(txtColumnSize, "Please select the column size")
                            txtColumnSize.Focus()
                            Return
                        End If
                    Case "DROP COLUMN"
                        If cmbColumnName.Text = "" Then
                            setError(cmbColumnName, "Please enter the column name")
                            cmbColumnName.Focus()
                            Return
                        End If
                End Select

                Page3.BringToFront()

                cmdNext.Enabled = False
                cmdOK.Enabled = True

                If cmbModifyType.Text = "ADD COLUMN" Then
                    Dim sDataType As String

                    Select Case cmbColumnType.Text
                        Case "TEXT"
                            sDataType = "VARCHAR"
                        Case "NUMBER"
                            sDataType = "INTEGER"
                        Case "DECIMAL"
                            sDataType = "FLOAT"
                        Case "MEMO"
                            sDataType = "TEXT"
                    End Select

                    If sDataType = "VARCHAR" Then
                        txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " ADD COLUMN " & cmbColumnName.Text & " " & sDataType & "(" & txtColumnSize.Text & ")"
                    Else
                        txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " ADD COLUMN " & cmbColumnName.Text & " " & sDataType
                    End If
                Else
                    txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " DROP COLUMN " & cmbColumnName.Text
                End If

                txtFinal.Text = txtFinal.Text.ToUpper

        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Select Case nStep
            Case 3
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub
    Public Function AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sWhere As String
        Dim sValues As String
        Dim sModifyType As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'Connection String -> ProgramParameters
        'Table Name -> ProgramPath 
        'Modify Type -> WindowStyle
        'Columns to add/delete (pipe delimited) -> SendTo
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,WindowStyle,SendTo,Msg,OrderID"

        sValues = cmbColumnName.Text & "|" & cmbColumnType.Text & "|" & txtColumnSize.Text & "|"

        If cmbModifyType.Text = "ADD COLUMN" Then
            sModifyType = "DBModifyTable_Add"
        Else
            sModifyType = "DBModifyTable_Drop"
        End If

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'" & sModifyType & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(cmbTables.Text) & "'," & _
            "'" & SQLPrepare(cmbModifyType.Text) & "'," & _
            "'" & SQLPrepare(sValues) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String
        Dim sWhere As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table Name -> ProgramPath 
        'Modify Type -> WindowStyle
        'Columns to add/delete (pipe delimited) -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            cmbTables.Text = oRs("programpath").Value

            sInserts = oRs("sendto").Value

            cmbColumnName.Text = GetDelimitedWord(sInserts, 1, "|")

            cmbColumnType.Text = GetDelimitedWord(sInserts, 2, "|")

            txtColumnSize.Text = GetDelimitedWord(sInserts, 3, "|")


            cmbModifyType.Text = oRs("windowstyle").Value

            txtFinal.Text = oRs("msg").Value

            oRs.Close()

            TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

            Me.ShowDialog()

            If UserCancel = True Then Return

            sInserts = cmbColumnName.Text & "|" & cmbColumnType.Text & "|" & txtColumnSize.Text & "|"

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            'Connection String -> ProgramParameters
            'Table Name -> ProgramPath 
            'Modify Type -> WindowStyle
            'Columns to add/delete (pipe delimited) -> SendTo
            'Final SQL -> Msg

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "ProgramPath = '" & SQLPrepare(cmbTables.Text) & "'," & _
                "WindowStyle = '" & SQLPrepare(cmbModifyType.Text) & "'," & _
                "SendTo = '" & SQLPrepare(sInserts) & "'," & _
                "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)

            TaskExecutionPath1.setTaskRunWhen(nTaskID)
        End If
    End Sub

    Private Sub frmTaskTableModify_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page1.BringToFront()
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdNext.Enabled = True

            Dim oData As clsMarsData = New clsMarsData

            oData.GetTables(cmbTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)
            cmdSkip.Enabled = True
        Else
            cmdNext.Enabled = False
        End If
    End Sub



    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            setError(txtName, "Please enter the name for the task")
            Return
        ElseIf txtFinal.Text = "" Then
            setError(txtFinal, "Please select the table to modify")
            Return
        End If

        Me.Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        setError(txtName, "")
    End Sub

    Private Sub txtFinal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        setError(txtFinal, "")
    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page3.BringToFront()
        cmdNext.Enabled = False
        nStep = 3
        cmdOK.Enabled = True
    End Sub
End Class
