Public Class frmTaskWriteFile
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Dim eventBased As Boolean = False
    Dim eventID As Integer = 99999
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Dim m_Textbox As TextBox = Me.txtText
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOverwrite As System.Windows.Forms.RadioButton
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optAppend As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtText As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskWriteFile))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtText = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optOverwrite = New System.Windows.Forms.RadioButton()
        Me.optAppend = New System.Windows.Forms.RadioButton()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuSpell = New System.Windows.Forms.MenuItem()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ofd = New System.Windows.Forms.SaveFileDialog()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(460, 20)
        Me.txtName.TabIndex = 14
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(423, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 15
        Me.cmdOK.Text = "&OK"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(501, 26)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowse.TabIndex = 18
        Me.cmdBrowse.Text = "..."
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 15)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(504, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 16
        Me.cmdCancel.Text = "&Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtText)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(567, 387)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        '
        'txtText
        '
        Me.txtText.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtText.Border.Class = "TextBoxBorder"
        Me.txtText.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtText.DisabledBackColor = System.Drawing.Color.White
        Me.txtText.ForeColor = System.Drawing.Color.Blue
        Me.txtText.Location = New System.Drawing.Point(8, 67)
        Me.txtText.Multiline = True
        Me.txtText.Name = "txtText"
        Me.txtText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtText.Size = New System.Drawing.Size(533, 256)
        Me.txtText.TabIndex = 20
        Me.txtText.Tag = "memo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.optOverwrite)
        Me.GroupBox2.Controls.Add(Me.optAppend)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 329)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(532, 52)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "If file exists"
        '
        'optOverwrite
        '
        Me.optOverwrite.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optOverwrite.Location = New System.Drawing.Point(8, 22)
        Me.optOverwrite.Name = "optOverwrite"
        Me.optOverwrite.Size = New System.Drawing.Size(144, 23)
        Me.optOverwrite.TabIndex = 0
        Me.optOverwrite.Text = "Overwrite"
        '
        'optAppend
        '
        Me.optAppend.Checked = True
        Me.optAppend.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAppend.Location = New System.Drawing.Point(152, 22)
        Me.optAppend.Name = "optAppend"
        Me.optAppend.Size = New System.Drawing.Size(144, 23)
        Me.optAppend.TabIndex = 0
        Me.optAppend.TabStop = True
        Me.optAppend.Text = "Append"
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.DisabledBackColor = System.Drawing.Color.White
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 26)
        Me.txtPath.MaxLength = 255
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(487, 20)
        Me.txtPath.TabIndex = 14
        Me.txtPath.Tag = "memo"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "File Name"
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Text"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem5, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 10
        Me.MenuItem5.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'ofd
        '
        Me.ofd.OverwritePrompt = False
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(582, 421)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 20
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(582, 396)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(582, 421)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(582, 26)
        Me.FlowLayoutPanel1.TabIndex = 21
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 447)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(582, 32)
        Me.FlowLayoutPanel2.TabIndex = 22
        '
        'frmTaskWriteFile
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(582, 479)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskWriteFile"
        Me.Text = "Write to File"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region


    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        ofd.ShowDialog()

        Dim sTemp As String = ofd.FileName

        If sTemp <> "" Then
            txtPath.Text = _CreateUNC(sTemp)
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf txtPath.Text = "" Then
            SetError(txtPath, "Please select the file")
            txtPath.Focus()
            Exit Sub
        End If

        Me.Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtPath_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.GotFocus
        Me.m_Textbox = txtPath
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,Msg,ReplaceFiles,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'WriteFile'," & _
            "'" & txtName.Text.Replace("'", "''") & "'," & _
            "'" & txtPath.Text.Replace("'", "''") & "'," & _
            "'" & txtText.Text.Replace("'", "''") & "'," & _
            Convert.ToInt32(optOverwrite.Checked) & "," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtPath.Text = oRs("programpath").Value
            txtText.Text = oRs("msg").Value

            If Convert.ToBoolean(oRs("replacefiles").Value) = True Then
                optOverwrite.Checked = True
            Else
                optAppend.Checked = True
            End If

        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Msg = '" & txtText.Text.Replace("'", "''") & "'," & _
            "ReplaceFiles = " & Convert.ToInt32(optOverwrite.Checked)

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub txtText_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtText.GotFocus
        Me.m_Textbox = txtText
    End Sub
    Private Sub txtText_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtText.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub frmTaskWriteFile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtText)
        setupForDragAndDrop(txtPath)

        showInserter(Me, m_eventID, m_eventBased)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.Undo()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.Cut()
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.Copy()
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.Paste()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        If Me.m_Textbox IsNot Nothing Then
            Me.m_Textbox.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.m_eventID)
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        Me.m_Textbox.SelectedText = oItem._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = txtText
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub


End Class
