Public Class frmTaskZip
    Inherits frmTaskMaster
    Dim UserCancel As Boolean = True
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents grpZip As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtZipCode As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbEncryptionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtConfirmCode As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkZipSecurity As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Public m_eventID As Integer = 99999

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvFiles As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAddFiles As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpAdvanced As System.Windows.Forms.Panel
    Friend WithEvents chkRecreateFolderStructure As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkRecursive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtSourceFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdBrowseSource As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskZip))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grpAdvanced = New System.Windows.Forms.Panel()
        Me.chkRecreateFolderStructure = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkRecursive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSourceFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBrowseSource = New DevComponents.DotNetBar.ButtonX()
        Me.lsvFiles = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddFiles = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grpZip = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtZipCode = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.cmbEncryptionLevel = New System.Windows.Forms.ComboBox()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.txtConfirmCode = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkZipSecurity = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.rbAdvanced = New System.Windows.Forms.RadioButton()
        Me.rbSimple = New System.Windows.Forms.RadioButton()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.grpZip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(315, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.grpAdvanced)
        Me.GroupBox1.Controls.Add(Me.lsvFiles)
        Me.GroupBox1.Controls.Add(Me.cmdAddFiles)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 148)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select files to compress"
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.chkRecreateFolderStructure)
        Me.grpAdvanced.Controls.Add(Me.chkRecursive)
        Me.grpAdvanced.Controls.Add(Me.txtSourceFile)
        Me.grpAdvanced.Controls.Add(Me.Label8)
        Me.grpAdvanced.Controls.Add(Me.cmdBrowseSource)
        Me.grpAdvanced.Location = New System.Drawing.Point(8, 15)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(448, 126)
        Me.grpAdvanced.TabIndex = 0
        Me.grpAdvanced.TabStop = True
        '
        'chkRecreateFolderStructure
        '
        '
        '
        '
        Me.chkRecreateFolderStructure.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecreateFolderStructure.Enabled = False
        Me.chkRecreateFolderStructure.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecreateFolderStructure.Location = New System.Drawing.Point(14, 79)
        Me.chkRecreateFolderStructure.Name = "chkRecreateFolderStructure"
        Me.chkRecreateFolderStructure.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecreateFolderStructure, New DevComponents.DotNetBar.SuperTooltipInfo("", "", resources.GetString("chkRecreateFolderStructure.SuperTooltip"), Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecreateFolderStructure.TabIndex = 3
        Me.chkRecreateFolderStructure.Text = "Maintain Folder Structure"
        '
        'chkRecursive
        '
        '
        '
        '
        Me.chkRecursive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(14, 58)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecursive, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Search the folder and all its subfolders.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecursive.TabIndex = 2
        Me.chkRecursive.Text = "Recursive"
        '
        'txtSourceFile
        '
        Me.txtSourceFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSourceFile.Border.Class = "TextBoxBorder"
        Me.txtSourceFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSourceFile.DisabledBackColor = System.Drawing.Color.White
        Me.txtSourceFile.ForeColor = System.Drawing.Color.Black
        Me.txtSourceFile.Location = New System.Drawing.Point(14, 32)
        Me.txtSourceFile.Name = "txtSourceFile"
        Me.txtSourceFile.Size = New System.Drawing.Size(319, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtSourceFile, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert Constants and database values.  You can also use w" & _
            "ildcards e.g. my*abc.*", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtSourceFile.TabIndex = 0
        Me.txtSourceFile.Tag = "Memo"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(11, 13)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "File(s)"
        '
        'cmdBrowseSource
        '
        Me.cmdBrowseSource.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowseSource.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowseSource.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowseSource.Location = New System.Drawing.Point(344, 31)
        Me.cmdBrowseSource.Name = "cmdBrowseSource"
        Me.cmdBrowseSource.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowseSource.TabIndex = 1
        Me.cmdBrowseSource.Text = "..."
        '
        'lsvFiles
        '
        Me.lsvFiles.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvFiles.Border.Class = "ListViewBorder"
        Me.lsvFiles.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.DisabledBackColor = System.Drawing.Color.Empty
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.FullRowSelect = True
        Me.lsvFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvFiles.LabelEdit = True
        Me.lsvFiles.Location = New System.Drawing.Point(8, 15)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(392, 126)
        Me.lsvFiles.TabIndex = 1
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 380
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddFiles.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(416, 15)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 21)
        Me.cmdAddFiles.TabIndex = 0
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(416, 45)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 21)
        Me.cmdRemove.TabIndex = 2
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(362, 20)
        Me.txtName.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(396, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Task Name"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.grpZip)
        Me.GroupBox2.Controls.Add(Me.cmdBrowse)
        Me.GroupBox2.Controls.Add(Me.txtPath)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 180)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(464, 207)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Output zip file"
        '
        'grpZip
        '
        Me.grpZip.Controls.Add(Me.Panel1)
        Me.grpZip.Controls.Add(Me.chkZipSecurity)
        Me.grpZip.Location = New System.Drawing.Point(6, 48)
        Me.grpZip.Name = "grpZip"
        Me.grpZip.Size = New System.Drawing.Size(450, 152)
        Me.grpZip.TabIndex = 16
        Me.grpZip.TabStop = False
        Me.grpZip.Text = "Zip File Encryption"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtZipCode)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.cmbEncryptionLevel)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtConfirmCode)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(8, 43)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(344, 104)
        Me.Panel1.TabIndex = 2
        '
        'txtZipCode
        '
        Me.txtZipCode.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtZipCode.Border.Class = "TextBoxBorder"
        Me.txtZipCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtZipCode.DisabledBackColor = System.Drawing.Color.White
        Me.txtZipCode.ForeColor = System.Drawing.Color.Black
        Me.txtZipCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtZipCode.Location = New System.Drawing.Point(112, 40)
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtZipCode.Size = New System.Drawing.Size(160, 20)
        Me.txtZipCode.TabIndex = 4
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Password"
        '
        'cmbEncryptionLevel
        '
        Me.cmbEncryptionLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEncryptionLevel.ItemHeight = 13
        Me.cmbEncryptionLevel.Items.AddRange(New Object() {"128", "192", "256"})
        Me.cmbEncryptionLevel.Location = New System.Drawing.Point(112, 8)
        Me.cmbEncryptionLevel.Name = "cmbEncryptionLevel"
        Me.cmbEncryptionLevel.Size = New System.Drawing.Size(160, 21)
        Me.cmbEncryptionLevel.TabIndex = 2
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Encryption Level"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 74)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Confirm Password"
        '
        'txtConfirmCode
        '
        Me.txtConfirmCode.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtConfirmCode.Border.Class = "TextBoxBorder"
        Me.txtConfirmCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtConfirmCode.DisabledBackColor = System.Drawing.Color.White
        Me.txtConfirmCode.ForeColor = System.Drawing.Color.Black
        Me.txtConfirmCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtConfirmCode.Location = New System.Drawing.Point(112, 72)
        Me.txtConfirmCode.Name = "txtConfirmCode"
        Me.txtConfirmCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtConfirmCode.Size = New System.Drawing.Size(160, 20)
        Me.txtConfirmCode.TabIndex = 4
        '
        'chkZipSecurity
        '
        '
        '
        '
        Me.chkZipSecurity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkZipSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkZipSecurity.Location = New System.Drawing.Point(6, 18)
        Me.chkZipSecurity.Name = "chkZipSecurity"
        Me.chkZipSecurity.Size = New System.Drawing.Size(320, 24)
        Me.chkZipSecurity.TabIndex = 0
        Me.chkZipSecurity.Text = "Enable zip encryption"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(416, 24)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "..."
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.DisabledBackColor = System.Drawing.Color.White
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 22)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(392, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtPath, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert constants and database values.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtPath.TabIndex = 0
        Me.txtPath.Tag = "Memo"
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'sfd
        '
        Me.sfd.Filter = "Zip Files|*.zip"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.rbAdvanced.Location = New System.Drawing.Point(74, 3)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(74, 17)
        Me.rbAdvanced.TabIndex = 2
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = False
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.BackColor = System.Drawing.Color.Transparent
        Me.rbSimple.Location = New System.Drawing.Point(3, 3)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(56, 17)
        Me.rbSimple.TabIndex = 1
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(474, 409)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 52
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Controls.Add(Me.rbSimple)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel1.Controls.Add(Me.rbAdvanced)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(474, 385)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(474, 409)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(474, 26)
        Me.FlowLayoutPanel1.TabIndex = 53
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 435)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(474, 32)
        Me.FlowLayoutPanel2.TabIndex = 54
        '
        'frmTaskZip
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(474, 467)
        Me.ControlBox = True
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "frmTaskZip"
        Me.Text = "Zip Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.grpAdvanced.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.grpZip.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.PerformLayout()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click
        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            lsv = lsvFiles.Items.Add(_CreateUNC(sFile))
            lsv.ImageIndex = 0
        Next

        SetError(lsvFiles, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With sfd
            .CheckPathExists = True
            .OverwritePrompt = True
            .Title = "Select the destination zip file"
            .ShowDialog()

            If .FileName.Length = 0 Then Return

            txtPath.Text = _CreateUNC(.FileName)
        End With

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please enter the name of the task")
            txtName.Focus()
            Exit Sub
        ElseIf rbSimple.Checked = True And lsvFiles.Items.Count = 0 Then
            SetError(lsvFiles, "Please select the file(s) to zip")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf rbAdvanced.Checked = True And txtSourceFile.Text = "" Then
            SetError(txtSourceFile, "Please select the file(s) to zip")
            txtSourceFile.Focus()
            Exit Sub
        ElseIf txtPath.Text.Length = 0 Then
            SetError(txtPath, "Please select the destination zip file")
            txtPath.Focus()
            Return
        ElseIf chkZipSecurity.Checked Then
            If cmbEncryptionLevel.Text = "" Then
                SetError(cmbEncryptionLevel, "Please select the encryption level")
                cmbEncryptionLevel.Focus()
                Return
            ElseIf txtZipCode.Text = "" Then
                SetError(txtZipCode, "Please enter the encryption password")
                txtZipCode.Focus()
                Return
            ElseIf txtZipCode.Text <> txtConfirmCode.Text Then
                SetError(txtZipCode, "The two passwords do not match, please try again")
                txtZipCode.Text = ""
                txtConfirmCode.Text = ""
                txtZipCode.Focus()
                Return
            End If
        End If

        UserCancel = False
        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
   Optional ByVal ShowAfter As Boolean = True) As Integer
        rbSimple.Checked = True
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If

        '//for FTP encryption options
        '//ftppassive = enable encryption
        '//ftptype = encryption type
        '//ftppassword = zip password

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,OrderID,CC,Bcc,FTPPassive,FTPType,FTPPassword"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'ZipFiles'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        oTask.GetOrderID(ScheduleID) & "," & _
        "'" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
        "'" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
        Convert.ToInt32(chkZipSecurity.Checked) & "," & _
        "'" & cmbEncryptionLevel.Text & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtZipCode.Text)) & "'"

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            If sFiles.Contains("|") = True Then

                Me.rbSimple.Checked = True

                For Each sVal In sFiles.Split("|")
                    If sVal.Length > 0 Then lsvFiles.Items.Add(sVal)
                Next
            Else
                txtSourceFile.Text = sFiles
                Me.rbAdvanced.Checked = True
            End If

            txtPath.Text = oRs("programpath").Value
            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))
            chkRecreateFolderStructure.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0))))

            Try
                chkZipSecurity.Checked = IsNull(oRs("ftppassive").Value, 0)
                cmbEncryptionLevel.Text = IsNull(oRs("ftptype").Value, "")
                txtZipCode.Text = _DecryptDBValue(IsNull(oRs("ftppassword").Value, ""))
                txtConfirmCode.Text = txtZipCode.Text
            Catch : End Try

        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If


        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'" & "," & _
            "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
            "Bcc = '" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
            "FTPPassive =" & Convert.ToInt32(chkZipSecurity.Checked) & "," & _
            "FTPType ='" & cmbEncryptionLevel.Text & "'," & _
            "FTPPassword ='" & SQLPrepare(_EncryptDBValue(txtZipCode.Text)) & "'"


        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub frmTaskZip_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.HelpButton = False
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
        Me.ControlBox = True
        txtName.Focus()

        showInserter(Me, Me.m_eventID)

        setupForDragAndDrop(txtSourceFile) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(txtPath) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(txtConfirmCode) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtZipCode) '.ContextMenu = mnuInserter
    End Sub


    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            rbSimple.Checked = False
            lsvFiles.Enabled = False
            cmdAddFiles.Enabled = False
            cmdRemove.Enabled = False
            grpAdvanced.Enabled = True
            grpAdvanced.Visible = True
        End If
    End Sub

    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            rbAdvanced.Checked = False
            lsvFiles.Enabled = True
            cmdAddFiles.Enabled = True
            cmdRemove.Enabled = True
            grpAdvanced.Enabled = False
            grpAdvanced.Visible = False
        End If
    End Sub

    Private Sub cmdBrowseSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseSource.Click
        With ofd
            .Multiselect = False
            .ShowDialog()
        End With

        txtSourceFile.Text = _CreateUNC(ofd.FileName)
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub chkRecursive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRecursive.CheckedChanged
        If chkRecursive.Checked = True Then
            Me.chkRecreateFolderStructure.Enabled = True
            Me.chkRecreateFolderStructure.Checked = True
        Else
            Me.chkRecreateFolderStructure.Checked = False
            Me.chkRecreateFolderStructure.Enabled = False
        End If
    End Sub


    Private Sub chkZipSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZipSecurity.CheckedChanged
        Panel1.Enabled = chkZipSecurity.Checked
    End Sub
End Class
