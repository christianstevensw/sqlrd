﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskExchangeAppointment
    Inherits sqlrd.frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.txtEndTime = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtStartTime = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnAttendees = New DevComponents.DotNetBar.ButtonX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.mnuImportance = New DevComponents.DotNetBar.RadialMenu()
        Me.High = New DevComponents.DotNetBar.RadialMenuItem()
        Me.Normal = New DevComponents.DotNetBar.RadialMenuItem()
        Me.Low = New DevComponents.DotNetBar.RadialMenuItem()
        Me.Normal2 = New DevComponents.DotNetBar.RadialMenuItem()
        Me.txtReminder = New DevComponents.Editors.IntegerInput()
        Me.txtBody = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkAllDayEvent = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtFileAttachments = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtAttendees = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtLocation = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lblImportance = New DevComponents.DotNetBar.LabelX()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.taskPath = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.grpExchangeWDS = New System.Windows.Forms.GroupBox()
        Me.btnAutoDiscover = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtEWSPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtEWSUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtServerUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtAutodiscover = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.chkAutodiscover = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.mnuDB = New System.Windows.Forms.MenuItem()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.txtReminder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.grpExchangeWDS.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(632, 28)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 21)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        Me.txtName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(77, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(535, 21)
        Me.txtName.TabIndex = 0
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel3)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(632, 421)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 1
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem3, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.txtEndTime)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtStartTime)
        Me.SuperTabControlPanel1.Controls.Add(Me.btnAttendees)
        Me.SuperTabControlPanel1.Controls.Add(Me.btnBrowse)
        Me.SuperTabControlPanel1.Controls.Add(Me.mnuImportance)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtReminder)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtBody)
        Me.SuperTabControlPanel1.Controls.Add(Me.chkAllDayEvent)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtFileAttachments)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtAttendees)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtLocation)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtSubject)
        Me.SuperTabControlPanel1.Controls.Add(Me.lblImportance)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX7)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX8)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX6)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX4)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX10)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX3)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX2)
        Me.SuperTabControlPanel1.Controls.Add(Me.LabelX1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(632, 395)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'txtEndTime
        '
        Me.txtEndTime.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtEndTime.Border.Class = "TextBoxBorder"
        Me.txtEndTime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEndTime.DisabledBackColor = System.Drawing.Color.White
        Me.txtEndTime.ForeColor = System.Drawing.Color.Black
        Me.txtEndTime.Location = New System.Drawing.Point(121, 115)
        Me.txtEndTime.Name = "txtEndTime"
        Me.txtEndTime.Size = New System.Drawing.Size(193, 21)
        Me.txtEndTime.TabIndex = 14
        '
        'txtStartTime
        '
        Me.txtStartTime.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtStartTime.Border.Class = "TextBoxBorder"
        Me.txtStartTime.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStartTime.DisabledBackColor = System.Drawing.Color.White
        Me.txtStartTime.ForeColor = System.Drawing.Color.Black
        Me.txtStartTime.Location = New System.Drawing.Point(121, 86)
        Me.txtStartTime.Name = "txtStartTime"
        Me.txtStartTime.Size = New System.Drawing.Size(193, 21)
        Me.txtStartTime.TabIndex = 14
        '
        'btnAttendees
        '
        Me.btnAttendees.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAttendees.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAttendees.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAttendees.Location = New System.Drawing.Point(12, 59)
        Me.btnAttendees.Name = "btnAttendees"
        Me.btnAttendees.Size = New System.Drawing.Size(103, 21)
        Me.btnAttendees.TabIndex = 13
        Me.btnAttendees.Text = "Attendees (if any)"
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(570, 362)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(48, 21)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnBrowse.TabIndex = 12
        Me.btnBrowse.Text = "..."
        '
        'mnuImportance
        '
        Me.mnuImportance.BackColor = System.Drawing.Color.Transparent
        Me.mnuImportance.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.High, Me.Normal, Me.Low, Me.Normal2})
        Me.mnuImportance.Location = New System.Drawing.Point(121, 328)
        Me.mnuImportance.Name = "mnuImportance"
        Me.mnuImportance.Size = New System.Drawing.Size(28, 28)
        Me.mnuImportance.Symbol = ""
        Me.mnuImportance.TabIndex = 10
        Me.mnuImportance.Text = "RadialMenu1"
        '
        'High
        '
        Me.High.Name = "High"
        Me.High.Text = "High"
        '
        'Normal
        '
        Me.Normal.Name = "Normal"
        Me.Normal.Text = "Normal"
        '
        'Low
        '
        Me.Low.Name = "Low"
        Me.Low.Text = "Low"
        '
        'Normal2
        '
        Me.Normal2.Name = "Normal2"
        Me.Normal2.Text = "Normal"
        '
        'txtReminder
        '
        '
        '
        '
        Me.txtReminder.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtReminder.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtReminder.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtReminder.Location = New System.Drawing.Point(121, 301)
        Me.txtReminder.MinValue = 0
        Me.txtReminder.Name = "txtReminder"
        Me.txtReminder.ShowUpDown = True
        Me.txtReminder.Size = New System.Drawing.Size(80, 21)
        Me.txtReminder.TabIndex = 9
        Me.txtReminder.Value = 15
        '
        'txtBody
        '
        Me.txtBody.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtBody.Border.Class = "TextBoxBorder"
        Me.txtBody.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBody.DisabledBackColor = System.Drawing.Color.White
        Me.txtBody.ForeColor = System.Drawing.Color.Black
        Me.txtBody.Location = New System.Drawing.Point(12, 142)
        Me.txtBody.Multiline = True
        Me.txtBody.Name = "txtBody"
        Me.txtBody.Size = New System.Drawing.Size(606, 152)
        Me.txtBody.TabIndex = 8
        '
        'chkAllDayEvent
        '
        Me.chkAllDayEvent.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAllDayEvent.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAllDayEvent.Location = New System.Drawing.Point(320, 85)
        Me.chkAllDayEvent.Name = "chkAllDayEvent"
        Me.chkAllDayEvent.Size = New System.Drawing.Size(100, 23)
        Me.chkAllDayEvent.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkAllDayEvent.TabIndex = 5
        Me.chkAllDayEvent.Text = "All Day Event"
        '
        'txtFileAttachments
        '
        Me.txtFileAttachments.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFileAttachments.Border.Class = "TextBoxBorder"
        Me.txtFileAttachments.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFileAttachments.DisabledBackColor = System.Drawing.Color.White
        Me.txtFileAttachments.ForeColor = System.Drawing.Color.Black
        Me.txtFileAttachments.Location = New System.Drawing.Point(121, 362)
        Me.txtFileAttachments.Name = "txtFileAttachments"
        Me.txtFileAttachments.Size = New System.Drawing.Size(443, 21)
        Me.txtFileAttachments.TabIndex = 11
        '
        'txtAttendees
        '
        Me.txtAttendees.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAttendees.Border.Class = "TextBoxBorder"
        Me.txtAttendees.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAttendees.DisabledBackColor = System.Drawing.Color.White
        Me.txtAttendees.ForeColor = System.Drawing.Color.Black
        Me.txtAttendees.Location = New System.Drawing.Point(121, 59)
        Me.txtAttendees.Name = "txtAttendees"
        Me.txtAttendees.Size = New System.Drawing.Size(497, 21)
        Me.txtAttendees.TabIndex = 2
        '
        'txtLocation
        '
        Me.txtLocation.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtLocation.Border.Class = "TextBoxBorder"
        Me.txtLocation.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLocation.DisabledBackColor = System.Drawing.Color.White
        Me.txtLocation.ForeColor = System.Drawing.Color.Black
        Me.txtLocation.Location = New System.Drawing.Point(121, 32)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(497, 21)
        Me.txtLocation.TabIndex = 1
        '
        'txtSubject
        '
        Me.txtSubject.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSubject.Border.Class = "TextBoxBorder"
        Me.txtSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSubject.DisabledBackColor = System.Drawing.Color.White
        Me.txtSubject.ForeColor = System.Drawing.Color.Black
        Me.txtSubject.Location = New System.Drawing.Point(121, 6)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(497, 21)
        Me.txtSubject.TabIndex = 0
        '
        'lblImportance
        '
        Me.lblImportance.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblImportance.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblImportance.Location = New System.Drawing.Point(155, 330)
        Me.lblImportance.Name = "lblImportance"
        Me.lblImportance.Size = New System.Drawing.Size(411, 23)
        Me.lblImportance.TabIndex = 0
        Me.lblImportance.Tag = "1"
        Me.lblImportance.Text = "Normal Importance"
        '
        'LabelX7
        '
        Me.LabelX7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(207, 301)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(411, 23)
        Me.LabelX7.TabIndex = 0
        Me.LabelX7.Text = "minutes before event"
        '
        'LabelX8
        '
        Me.LabelX8.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(12, 329)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(75, 23)
        Me.LabelX8.TabIndex = 0
        Me.LabelX8.Text = "Importance"
        '
        'LabelX6
        '
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(12, 300)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(75, 23)
        Me.LabelX6.TabIndex = 0
        Me.LabelX6.Text = "Reminder"
        '
        'LabelX4
        '
        Me.LabelX4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(12, 113)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 23)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "End Time"
        '
        'LabelX10
        '
        Me.LabelX10.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Location = New System.Drawing.Point(12, 362)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(103, 23)
        Me.LabelX10.TabIndex = 0
        Me.LabelX10.Text = "Files (if Any)"
        '
        'LabelX3
        '
        Me.LabelX3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(12, 88)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(75, 23)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Start Time"
        '
        'LabelX2
        '
        Me.LabelX2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(12, 32)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(103, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Location"
        '
        'LabelX1
        '
        Me.LabelX1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(12, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(103, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Subject"
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.taskPath)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(632, 421)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'taskPath
        '
        Me.taskPath.BackColor = System.Drawing.Color.Transparent
        Me.taskPath.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.taskPath.Location = New System.Drawing.Point(12, 14)
        Me.taskPath.Name = "taskPath"
        Me.taskPath.Size = New System.Drawing.Size(322, 143)
        Me.taskPath.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.grpExchangeWDS)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(632, 421)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'grpExchangeWDS
        '
        Me.grpExchangeWDS.BackColor = System.Drawing.Color.Transparent
        Me.grpExchangeWDS.Controls.Add(Me.btnAutoDiscover)
        Me.grpExchangeWDS.Controls.Add(Me.GroupBox1)
        Me.grpExchangeWDS.Controls.Add(Me.txtServerUrl)
        Me.grpExchangeWDS.Controls.Add(Me.txtAutodiscover)
        Me.grpExchangeWDS.Controls.Add(Me.Label5)
        Me.grpExchangeWDS.Controls.Add(Me.chkAutodiscover)
        Me.grpExchangeWDS.Location = New System.Drawing.Point(12, 3)
        Me.grpExchangeWDS.Name = "grpExchangeWDS"
        Me.grpExchangeWDS.Size = New System.Drawing.Size(606, 218)
        Me.grpExchangeWDS.TabIndex = 0
        Me.grpExchangeWDS.TabStop = False
        '
        'btnAutoDiscover
        '
        Me.btnAutoDiscover.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAutoDiscover.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAutoDiscover.Enabled = False
        Me.btnAutoDiscover.Location = New System.Drawing.Point(524, 136)
        Me.btnAutoDiscover.Name = "btnAutoDiscover"
        Me.btnAutoDiscover.Size = New System.Drawing.Size(59, 21)
        Me.btnAutoDiscover.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnAutoDiscover.TabIndex = 3
        Me.btnAutoDiscover.Text = "Go"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtEWSPassword)
        Me.GroupBox1.Controls.Add(Me.txtEWSUserID)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(594, 86)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Authentication"
        '
        'txtEWSPassword
        '
        Me.txtEWSPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtEWSPassword.Border.Class = "TextBoxBorder"
        Me.txtEWSPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEWSPassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtEWSPassword.ForeColor = System.Drawing.Color.Black
        Me.txtEWSPassword.Location = New System.Drawing.Point(90, 56)
        Me.txtEWSPassword.Name = "txtEWSPassword"
        Me.txtEWSPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtEWSPassword.Size = New System.Drawing.Size(487, 21)
        Me.txtEWSPassword.TabIndex = 1
        '
        'txtEWSUserID
        '
        Me.txtEWSUserID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtEWSUserID.Border.Class = "TextBoxBorder"
        Me.txtEWSUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtEWSUserID.DisabledBackColor = System.Drawing.Color.White
        Me.txtEWSUserID.ForeColor = System.Drawing.Color.Black
        Me.txtEWSUserID.Location = New System.Drawing.Point(90, 23)
        Me.txtEWSUserID.Name = "txtEWSUserID"
        Me.txtEWSUserID.Size = New System.Drawing.Size(487, 21)
        Me.txtEWSUserID.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(6, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Password"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(6, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Domain\UserID"
        '
        'txtServerUrl
        '
        Me.txtServerUrl.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtServerUrl.Border.Class = "TextBoxBorder"
        Me.txtServerUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServerUrl.DisabledBackColor = System.Drawing.Color.White
        Me.txtServerUrl.ForeColor = System.Drawing.Color.Black
        Me.txtServerUrl.Location = New System.Drawing.Point(6, 187)
        Me.txtServerUrl.Name = "txtServerUrl"
        Me.txtServerUrl.Size = New System.Drawing.Size(503, 21)
        Me.txtServerUrl.TabIndex = 4
        '
        'txtAutodiscover
        '
        Me.txtAutodiscover.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAutodiscover.Border.Class = "TextBoxBorder"
        Me.txtAutodiscover.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAutodiscover.DisabledBackColor = System.Drawing.Color.White
        Me.txtAutodiscover.Enabled = False
        Me.txtAutodiscover.ForeColor = System.Drawing.Color.Black
        Me.txtAutodiscover.Location = New System.Drawing.Point(6, 136)
        Me.txtAutodiscover.Name = "txtAutodiscover"
        Me.txtAutodiscover.Size = New System.Drawing.Size(503, 21)
        Me.txtAutodiscover.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(6, 171)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(127, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Server Name/Service URL"
        '
        'chkAutodiscover
        '
        '
        '
        '
        Me.chkAutodiscover.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutodiscover.Location = New System.Drawing.Point(8, 115)
        Me.chkAutodiscover.Name = "chkAutodiscover"
        Me.chkAutodiscover.Size = New System.Drawing.Size(305, 22)
        Me.chkAutodiscover.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkAutodiscover.TabIndex = 1
        Me.chkAutodiscover.Text = "Use Exchange Auto-Discovery using this email address"
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Exchange Details"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 449)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(632, 29)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(554, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(473, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'frmTaskExchangeAppointment
        '
        Me.ClientSize = New System.Drawing.Size(632, 478)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskExchangeAppointment"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Exchange Appointment"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.txtReminder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.grpExchangeWDS.ResumeLayout(False)
        Me.grpExchangeWDS.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtLocation As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAllDayEvent As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAttendees As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtBody As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtReminder As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuImportance As DevComponents.DotNetBar.RadialMenu
    Friend WithEvents High As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents Low As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblImportance As DevComponents.DotNetBar.LabelX
    Friend WithEvents Normal As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents Normal2 As DevComponents.DotNetBar.RadialMenuItem
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFileAttachments As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents taskPath As sqlrd.taskExecutionPath
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents btnAttendees As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents grpExchangeWDS As System.Windows.Forms.GroupBox
    Friend WithEvents btnAutoDiscover As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtEWSPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtEWSUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtServerUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtAutodiscover As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAutodiscover As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtEndTime As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtStartTime As DevComponents.DotNetBar.Controls.TextBoxX

End Class
