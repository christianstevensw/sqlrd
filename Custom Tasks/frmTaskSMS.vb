Public Class frmTaskSMS
    Dim oField As TextBox = Me.txtSMSNumber
    Dim eventID As Integer
    Dim eventBased As Boolean = False
    Dim ep As ErrorProvider = New ErrorProvider
    Dim UserCancel As Boolean = True

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return Me.eventBased
        End Get
        Set(ByVal value As Boolean)
            Me.eventBased = value
        End Set
    End Property

    Private Sub txtSMSMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSMsg.GotFocus
        oField = Me.txtSMSMsg
    End Sub

    Private Sub txtSMSMsg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMSMsg.TextChanged
        Try
            Label23.Text = "Characters left: " & 160 - (txtSMSMsg.Text.Length)
        Catch : End Try
    End Sub

    Private Sub cmdSMSTo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSMSTo.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "SMS"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtSMSNumber.Text.EndsWith(";") = False And txtSMSNumber.Text.Length > 0 _
        Then txtSMSNumber.Text &= ";"

        Try
            txtSMSNumber.Text &= sValues(0)
        Catch : End Try
    End Sub

    Private Sub frmTaskSMS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        HelpProvider1.HelpNamespace = gHelpPath

        setupForDragAndDrop(Me.txtSMSMsg) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtSMSNumber) '.ContextMenu = Me.mnuInserter

        showInserter(Me, m_eventID, m_eventBased)


        txtName.Focus()
    End Sub

    Private Sub txtSMSNumber_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSNumber.GotFocus
        oField = Me.txtSMSNumber
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        oField.Undo()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        oField.Copy()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        oField.Cut()
    End Sub
    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next


        Dim oItem As New frmDataItems

        oField.SelectedText = oItem._GetDataItem(Me.eventID)

    End Sub
    Private Sub mnuDefMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.crd")
        Catch : End Try
    End Sub

    Private Sub mnuDefSubject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefSubject.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        oField.SelectAll()
    End Sub

    Private Sub mnuSignature_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSignature.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.crd")
        Catch : End Try
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        oField.Paste()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide the name for this tak")
            txtName.Focus()
            Return
        ElseIf Me.txtSMSNumber.Text = "" Then
            SetError(txtSMSNumber, "Please enter the cell phone number")
            Me.txtSMSNumber.Focus()
            Return
        End If
        UserCancel = False
        Close()
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            Me.txtSMSNumber.Text = oRs("sendto").Value
            Me.txtSMSMsg.Text = oRs("msg").Value
        End If


        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
            "SendTo = '" & SQLPrepare(Me.txtSMSNumber.Text) & "'," & _
            "Msg = '" & SQLPrepare(Me.txtSMSMsg.Text) & "' "

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub
    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,SendTo,Msg,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'SendSMS'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(Me.txtSMSNumber.Text) & "'," & _
            "'" & SQLPrepare(Me.txtSMSMsg.Text) & "', " & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        UserCancel = True
    End Sub
End Class
