﻿Public Class frmTaskExecuteSSISPackage
    Dim eventID As Integer = 99999
    Dim eventBased As Boolean
    Dim userCancel As Boolean

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Private Sub cmbSQLServer_DropDown(sender As Object, e As System.EventArgs) Handles cmbSSISName.DropDown
        If cmbSSISName.Items.Count = 0 Then
            Try
                Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.LoadFrom(IO.Path.Combine(sAppPath, "ssisExecutor.dll"))
                Dim ssis As Object = assembly.CreateInstance("ssisExecutor.ssis")

                For Each package As String In ssis.getSSISPackages(txtSQLServer.Text, txtServerUserName.Text, txtServerPassword.Text)
                    cmbSSISName.Items.Add(package)
                Next
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
            End Try

        End If
    End Sub


    Private Sub ButtonX2_Click(sender As System.Object, e As System.EventArgs) Handles btnAddVar.Click
        If txtVarName.Text = "" Then
            Return
        End If

        Dim it As ListViewItem = lsvVariables.FindItemWithText(txtVarName.Text)

        If it Is Nothing Then
            it = lsvVariables.Items.Add(txtVarName.Text)
            it.SubItems.Add(txtVarValue.Text)
        Else
            it.SubItems(1).Text = txtVarValue.Text
        End If
    End Sub

    Private Sub ButtonX1_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveVar.Click
        Dim arr As ListView.SelectedListViewItemCollection = lsvVariables.SelectedItems

        For Each it As ListViewItem In arr
            it.Remove()
        Next
    End Sub


    Public Property m_ssisLocationType() As String
        Get
            Return stabPackLocation.SelectedTab.Text
        End Get
        Set(ByVal value As String)
            For Each stab As DevComponents.DotNetBar.SuperTabItem In stabPackLocation.Tabs
                If stab.Text = value Then
                    stabPackLocation.SelectedTab = stab
                    Exit For
                End If
            Next
        End Set
    End Property

    Public Property m_variablePairs As String
        Get
            Dim pairs As String = ""

            For Each it As ListViewItem In lsvVariables.Items
                pairs &= it.Text & "=" & it.SubItems(1).Text & PD
            Next

            Return pairs
        End Get
        Set(value As String)
            lsvVariables.Items.Clear()

            Dim pairs() As String = value.Split(PD)

            For Each pair As String In pairs
                If pair.Contains("=") Then
                    Dim it As ListViewItem = lsvVariables.Items.Add(pair.Split("=")(0))
                    it.SubItems.Add(pair.Split("=")(1))
                End If
            Next
        End Set
    End Property

    Public Property m_serverName As String
        Get
            Select Case m_ssisLocationType
                Case "SQL Server"
                    Return txtSQLServer.Text
                Case "DTS Server"
                    Return txtDTSServer.Text
                Case Else
                    Return ""
            End Select
        End Get
        Set(value As String)
            Select Case m_ssisLocationType
                Case "SQL Server"
                    txtSQLServer.Text = value
                Case "DTS Server"
                    txtDTSServer.Text = value
            End Select
        End Set
    End Property

    Public Property m_packageLocation As String
        Get
            Select Case m_ssisLocationType
                Case "SQL Server"
                    Return cmbSSISName.Text
                Case "DTS Server"
                    Return txtDTSSSISPackagePath.Text
                Case Else
                    Return txtLocalSSISPackagePath.Text
            End Select
        End Get
        Set(value As String)
            Select Case m_ssisLocationType
                Case "SQL Server"
                    cmbSSISName.Text = value
                Case "DTS Server"
                    txtDTSSSISPackagePath.Text = value
                Case Else
                    txtLocalSSISPackagePath.Text = value
            End Select
        End Set
    End Property

    Private Sub frmTaskExecuteSSISPackage_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        showInserter(Me, Me.m_eventID, m_eventBased)

        setupForDragAndDrop(txtVarName) '.ContextMenu = Me.mnuInserter)
        setupForDragAndDrop(txtVarValue) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(txtDTSSSISPackagePath) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(cmbSSISName)
        setupForDragAndDrop(txtLocalSSISPackagePath)
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If userCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sUpdates As String
        Dim sWhere As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'programpath > packagepath
        'programparameters > variables
        'ftptype > SSIS location type
        'ftpuser > userid
        'ftppassword > password
        'ftpserver > server

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ProgramParameters,FTPType,FTPUser,FTPPassword,FTPServer,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'ExecuteSSIS'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(m_packageLocation) & "'," & _
            "'" & SQLPrepare(m_variablePairs) & "'," & _
            "'" & SQLPrepare(m_ssisLocationType) & "'," & _
            "'" & SQLPrepare(txtServerUserName.Text) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(txtServerPassword.Text)) & "'," & _
            "'" & SQLPrepare(m_serverName) & "'," & _
            oTask.GetOrderID(ScheduleID)


        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed


        TaskExecutionPath1.setTaskRunWhen(nID)
        'basi
        Return nID
    End Function

    Public Function EditTask(ByVal ntaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM tasks WHERE taskid = " & nTaskID)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            m_ssisLocationType = oRs("ftptype").Value
            m_packageLocation = oRs("programpath").Value
            m_variablePairs = oRs("programparameters").Value
            m_ssisLocationType = oRs("ftptype").Value
            txtServerUserName.Text = oRs("ftpuser").Value
            txtServerPassword.Text = _DecryptDBValue(oRs("ftppassword").Value)
            m_serverName = oRs("ftpserver").Value
        End If

        TaskExecutionPath1.LoadTaskRunWhen(ntaskID)

        Me.ShowDialog()

        If userCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sUpdates As String
        Dim sWhere As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'programpath > packagepath
        'programparameters > variables
        'ftptype > SSIS location type
        'ftpuser > userid
        'ftppassword > password
        'ftpserver > server


        SQL = "UPDATE tasks SET " & _
            "programpath ='" & SQLPrepare(m_packageLocation) & "'," & _
            "programparameters ='" & SQLPrepare(m_variablePairs) & "'," & _
            "ftptype ='" & SQLPrepare(m_ssisLocationType) & "'," & _
            "ftpuser ='" & SQLPrepare(txtServerUserName.Text) & "'," & _
            "ftppassword='" & SQLPrepare(txtServerPassword.Text) & "'," & _
            "ftpserver='" & SQLPrepare(m_serverName) & "' " & _
            "WHERE taskid = " & ntaskID

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed


        TaskExecutionPath1.setTaskRunWhen(nID)
        'basi

    End Function

   
    Private Sub txtVarValue_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtVarValue.KeyDown
        If e.KeyCode = Keys.Enter Then
            ButtonX2_Click(Nothing, Nothing)
            txtVarName.Focus()
            txtVarName.SelectAll()
        End If
    End Sub

    Private Sub txtVarName_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles txtVarName.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtVarValue.Focus()
            txtVarValue.SelectAll()
        End If
    End Sub

  
    Private Sub lsvVariables_KeyDown(sender As Object, e As System.Windows.Forms.KeyEventArgs) Handles lsvVariables.KeyDown
        If e.KeyCode = Keys.Delete Then
            ButtonX1_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the SSIS Package file"
        ofd.Filter = "SSIS Package (*.dtsx)|*.dtsx|All Files (*.*)|*.*"
        ofd.Multiselect = False

        If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            txtLocalSSISPackagePath.Text = ofd.FileName
        End If

    End Sub

    Private Sub cmdOK_Click(sender As System.Object, e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            setError(txtName, "Please provide a name for this task", ErrorIconAlignment.MiddleRight)
            txtName.Focus()
            Return
        End If

        Select Case m_ssisLocationType
            Case "SQL Server"
                If txtSQLServer.Text = "" Then
                    setError(txtSQLServer, "Please enter the SQL Server name or address", ErrorIconAlignment.MiddleRight)
                    txtSQLServer.Focus()
                    stabMain.SelectedTab = tabGeneral
                    Return
                ElseIf cmbSSISName.Text = "" Then
                    setError(cmbSSISName, "Please enter the name or path to the SSIS package")
                    cmbSSISName.Focus()
                    stabMain.SelectedTab = tabGeneral
                    Return
                End If
            Case "DTS Server"
                If txtDTSServer.Text = "" Then
                    setError(txtDTSServer, "Please enter the DTS Server name or address")
                    txtDTSServer.Focus()
                    stabMain.SelectedTab = tabGeneral
                    Return
                ElseIf txtDTSSSISPackagePath.Text = "" Then
                    setError(txtDTSSSISPackagePath, "Please enter the name or path to the SSIS package")
                    txtDTSSSISPackagePath.Focus()
                    stabMain.SelectedTab = tabGeneral
                    Return
                End If
            Case "Local File"
                If txtLocalSSISPackagePath.Text = "" Then
                    setError(txtLocalSSISPackagePath, "Please select the path to the SSIS package")
                    txtLocalSSISPackagePath.Focus()
                    stabMain.SelectedTab = tabGeneral
                    Return
                End If
        End Select

        Close()
    End Sub

    Private Sub cmbSSISName_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbSSISName.SelectedIndexChanged

    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
        Close()
    End Sub
End Class