Public Class frmTaskFTPDownload
    Inherits sqlrd.frmTaskMaster
    'Dim oFtp As ucFTPBrowser
    Dim nStep As Int32 = 0
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Public m_eventID As Integer = 99999
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDirectory As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkOverwrite As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkRecreateFolderStructure As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkRecursive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtFile As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lblPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Button1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcFTPDetails1 As sqlrd.ucFTPDetails
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Dim UserCancel As Boolean = True
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents fbd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskFTPDownload))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.UcFTPDetails1 = New sqlrd.ucFTPDetails()
        Me.lblPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtDirectory = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.chkOverwrite = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtFile = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkRecreateFolderStructure = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkRecursive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Button1 = New DevComponents.DotNetBar.ButtonX()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.fbd = New System.Windows.Forms.FolderBrowserDialog()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.GroupBox1.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(582, 435)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.UcFTPDetails1)
        Me.Page1.Controls.Add(Me.lblPath)
        Me.Page1.Controls.Add(Me.GroupBox3)
        Me.Page1.Controls.Add(Me.Label6)
        Me.Page1.Controls.Add(Me.GroupBox2)
        Me.Page1.Controls.Add(Me.Button1)
        Me.Page1.Location = New System.Drawing.Point(8, 15)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(568, 414)
        Me.Page1.TabIndex = 0
        Me.Page1.TabStop = False
        '
        'UcFTPDetails1
        '
        Me.UcFTPDetails1.BackColor = System.Drawing.Color.Transparent
        Me.UcFTPDetails1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcFTPDetails1.Location = New System.Drawing.Point(13, 10)
        Me.UcFTPDetails1.m_browseFTPForFile = False
        Me.UcFTPDetails1.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
    "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;AsciiMode=0;RetryUpload=0;ClearChanne" & _
    "l=0;"
        Me.UcFTPDetails1.m_ftpSuccess = False
        Me.UcFTPDetails1.m_ShowBrowse = False
        Me.UcFTPDetails1.Name = "UcFTPDetails1"
        Me.UcFTPDetails1.Size = New System.Drawing.Size(549, 190)
        Me.UcFTPDetails1.TabIndex = 39
        '
        'lblPath
        '
        Me.lblPath.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.lblPath.Border.Class = "TextBoxBorder"
        Me.lblPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblPath.DisabledBackColor = System.Drawing.Color.White
        Me.lblPath.ForeColor = System.Drawing.Color.Black
        Me.lblPath.Location = New System.Drawing.Point(13, 217)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(504, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.lblPath, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "For root folder, leave empty or enter a forward slash." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You can right-click and i" & _
            "nsert constants and database values." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Remember:  Most FTP servers are case sen" & _
            "sitive!", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.lblPath.TabIndex = 5
        Me.lblPath.Tag = "Memo"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtDirectory)
        Me.GroupBox3.Controls.Add(Me.cmdBrowse)
        Me.GroupBox3.Controls.Add(Me.chkOverwrite)
        Me.GroupBox3.Location = New System.Drawing.Point(13, 338)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(549, 70)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Download to this folder..."
        '
        'txtDirectory
        '
        Me.txtDirectory.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDirectory.Border.Class = "TextBoxBorder"
        Me.txtDirectory.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDirectory.DisabledBackColor = System.Drawing.Color.White
        Me.txtDirectory.ForeColor = System.Drawing.Color.Black
        Me.txtDirectory.Location = New System.Drawing.Point(6, 22)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(498, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtDirectory, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert Constants and database values.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtDirectory.TabIndex = 0
        Me.txtDirectory.Tag = "Memo"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(509, 23)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "..."
        '
        'chkOverwrite
        '
        '
        '
        '
        Me.chkOverwrite.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOverwrite.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkOverwrite.Location = New System.Drawing.Point(6, 41)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(200, 23)
        Me.chkOverwrite.TabIndex = 2
        Me.chkOverwrite.Text = "Overwrite existing file"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(14, 200)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 38
        Me.Label6.Text = "FTP Path:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtFile)
        Me.GroupBox2.Controls.Add(Me.chkRecreateFolderStructure)
        Me.GroupBox2.Controls.Add(Me.chkRecursive)
        Me.GroupBox2.Location = New System.Drawing.Point(13, 243)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(549, 89)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Selected File"
        '
        'txtFile
        '
        Me.txtFile.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFile.Border.Class = "TextBoxBorder"
        Me.txtFile.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFile.DisabledBackColor = System.Drawing.Color.White
        Me.txtFile.ForeColor = System.Drawing.Color.Black
        Me.txtFile.Location = New System.Drawing.Point(6, 19)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(498, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtFile, New DevComponents.DotNetBar.SuperTooltipInfo("Be Aware", "", "You can right-click and insert Constants and database values.  You can also use w" & _
            "ildcards e.g. my*abc.*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Most FTP servers are case sensitive!", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtFile.TabIndex = 0
        Me.txtFile.Tag = "Memo"
        '
        'chkRecreateFolderStructure
        '
        '
        '
        '
        Me.chkRecreateFolderStructure.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecreateFolderStructure.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecreateFolderStructure.Location = New System.Drawing.Point(6, 62)
        Me.chkRecreateFolderStructure.Name = "chkRecreateFolderStructure"
        Me.chkRecreateFolderStructure.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecreateFolderStructure, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Recreates the FTP directory structure. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "For example, if the selected file is *" & _
            ".* then the entire folder structure, including all files and subfolders, will be" & _
            " recreated in your destination folder.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecreateFolderStructure.TabIndex = 2
        Me.chkRecreateFolderStructure.Text = "Recreate Folder Structure"
        '
        'chkRecursive
        '
        '
        '
        '
        Me.chkRecursive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(6, 42)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecursive, New DevComponents.DotNetBar.SuperTooltipInfo("", "", resources.GetString("chkRecursive.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecursive.TabIndex = 1
        Me.chkRecursive.Text = "Recursive"
        '
        'Button1
        '
        Me.Button1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button1.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(523, 217)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(39, 20)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "..."
        '
        'Page2
        '
        Me.Page2.Location = New System.Drawing.Point(8, 15)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(408, 377)
        Me.Page2.TabIndex = 27
        Me.Page2.TabStop = False
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Black
        Me.txtName.Location = New System.Drawing.Point(8, 15)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(580, 21)
        Me.txtName.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, -3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 21)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(528, 513)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(448, 513)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Gray)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(8, 41)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(595, 466)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 52
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(595, 442)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(595, 442)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 0)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'frmTaskFTPDownload
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(606, 544)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskFTPDownload"
        Me.Text = "FTP Download"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        With fbd
            .ShowNewFolderButton = True
            .Description = "Please select the destination folder"
            .ShowDialog()
        End With

        Dim sTemp As String = ""

        sTemp = fbd.SelectedPath

        If sTemp = "" Then Exit Sub

        If sTemp.Substring(sTemp.Length - 1, 1) <> "\" Then sTemp &= "\"

        txtDirectory.Text = sTemp
    End Sub


    Private Sub frmTaskFTPDownload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If oFtp Is Nothing Then
        '    oFtp = New ucFTPBrowser

        '    oFtp.Left = 8
        '    oFtp.Top = 16

        '    oFtp.Visible = True

        '    Page1.Controls.Add(oFtp)
        'End If

        showInserter(Me, m_eventID)

        Page1.BringToFront()
        FormatForWinXP(Me)
        txtName.Focus()
        
        setupForDragAndDrop(Me.txtDirectory) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtFile) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(lblPath)
    End Sub


    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return

        ElseIf txtFile.Text.Length = 0 Then
            SetError(txtFile, "Please enter the file you wish to download")
            txtFile.Focus()
            Return
        ElseIf txtDirectory.Text.Length = 0 Then
            SetError(txtDirectory, "Please enter the destination path")
            txtDirectory.Focus()
            Return
        ElseIf Me.UcFTPDetails1.validateFtpInfo = False Then
            Return
        End If
        UserCancel = False

        Me.Close()

    End Sub
    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer



        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFile As String
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID

        'note:  cc = recursive, bcc = recreate folder structure
        sCols = "TaskID,ScheduleID,TaskType,TaskName,FTPServer,FTPPort,FTPUser," & _
        "FTPPassword,FTPDirectory,Filelist,ProgramPath, ReplaceFiles,FTPType,FTPPassive,OrderID,CC,Bcc,FtpOptions"

        sFile = txtFile.Text
        If lblPath.Text.StartsWith("/") = False Then
            lblPath.Text = "/" & lblPath.Text

        End If

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'FTPDownload'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
            "" & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
            "'" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(lblPath.Text) & "'," & _
            "'" & SQLPrepare(sFile) & "'," & _
            "'" & SQLPrepare(txtDirectory.Text) & "'," & _
            Convert.ToInt32(chkOverwrite.Checked) & "," & _
            "'" & UcFTPDetails1.cmbFTPType.Text & "'," & _
            Convert.ToInt32(UcFTPDetails1.chkPassive.Checked) & "," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            "'" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
            "'" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
            "'" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"


        SQL = "INSERT INTO TASKS (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID

    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFile As String
        Dim lsv As ListViewItem


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtDirectory.Text = oRs("programpath").Value
            sFile = oRs("filelist").Value
            txtFile.Text = sFile
            chkOverwrite.Checked = Convert.ToBoolean(oRs("replacefiles").Value)
            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))
            chkRecreateFolderStructure.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0))))
            UcFTPDetails1.txtFTPServer.Text = oRs("ftpserver").Value
            UcFTPDetails1.txtUserName.Text = oRs("ftpuser").Value
            UcFTPDetails1.txtPassword.Text = oRs("ftppassword").Value
            lblPath.Text = oRs("ftpdirectory").Value
            UcFTPDetails1.cmbFTPType.Text = IsNull(oRs("ftptype").Value, "FTP")

            Try
                UcFTPDetails1.chkPassive.Checked = IsNull(oRs("ftppassive").Value, 0)
            Catch : End Try

            Try
                Me.UcFTPDetails1.m_ftpOptions = IsNull(oRs("ftpoptions").Value)
            Catch : End Try

            UcFTPDetails1.txtPort.Text = oRs("ftpport").Value
        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Return

        'sFile = oFtp.sFTPItem
        sFile = SQLPrepare(txtFile.Text)

        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
        "FTPServer = '" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
        "FTPPort = " & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
        "FTPuser = '" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
        "FTPPassword = '" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
        "FTPDirectory = '" & SQLPrepare(lblPath.Text) & "'," & _
        "ProgramPath = '" & SQLPrepare(txtDirectory.Text) & "'," & _
        "ReplaceFiles = " & Convert.ToInt32(chkOverwrite.Checked) & "," & _
        "FileList = '" & sFile & "'," & _
        "FTPType = '" & UcFTPDetails1.cmbFTPType.Text & "'" & "," & _
        "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
        "Bcc = '" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
        "FTPPassive =" & Convert.ToInt32(UcFTPDetails1.chkPassive.Checked) & "," & _
        "FtpOptions ='" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub


    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oBrowse As frmFTPBrowser2 = New frmFTPBrowser2
        Dim sReturn As String()

        sReturn = oBrowse.GetFtpInfo(Me.UcFTPDetails1.ConnectFTP)
       
        If sReturn Is Nothing Then

            Return
        Else
            lblPath.Text = sReturn(0)
            txtFile.Text = sReturn(1)
        End If

    End Sub

    Private Sub cmdBrowse_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sFD As FolderBrowserDialog = New FolderBrowserDialog
        sFD.Description = "Select folder to download to"
        sFD.ShowNewFolderButton = True
        If sFD.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Me.txtDirectory.Text = _CreateUNC(sFD.SelectedPath)
        End If
    End Sub

    Private Sub txtFile_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFile.TextChanged
        If txtFile.Text.Contains(" ") = True And txtFile.Text.Contains("<") = False Then
            chkRecursive.Checked = False
            chkRecursive.Enabled = False
            chkRecreateFolderStructure.Checked = False
            chkRecreateFolderStructure.Enabled = False
        Else
            chkRecreateFolderStructure.Enabled = True
            chkRecursive.Enabled = True
        End If
    End Sub

    Private Sub txtUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class
