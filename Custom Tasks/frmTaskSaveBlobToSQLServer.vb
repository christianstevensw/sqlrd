﻿Imports System.Data.SqlClient

Public Class frmTaskSaveBlobToSQLServer
    Dim ep As ErrorProvider = New ErrorProvider
    Dim con As SqlConnection
    Dim hsDataTypes As Hashtable
    Dim eventID As Integer = 99999
    Dim eventBased As Boolean = False
    Dim cancel As Boolean = False
    Private Property connectionString As String = ""

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return Me.eventBased
        End Get
        Set(ByVal value As Boolean)
            Me.eventBased = value
        End Set
    End Property

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
        Try
            Dim conBuilder As SqlConnectionStringBuilder = New SqlConnectionStringBuilder

            With conBuilder
                .DataSource = txtServerName.Text
                .InitialCatalog = txtDatabaseName.Text

                If txtUsername.Text = "" And txtPassword.Text = "" Then
                    .IntegratedSecurity = True
                Else
                    .UserID = txtUsername.Text
                    .Password = txtPassword.Text
                End If

            End With

            con = New SqlConnection(conBuilder.ConnectionString)

            con.Open()

            '//Retrieve the Table schema info
            Dim tbls As DataTable

            tbls = con.GetSchema("Tables")

            '//For each user table, retrieve all the data and insert into our dataset
            For Each dr As DataRow In tbls.Rows

                Dim tableName As String = dr("TABLE_NAME")
                Dim tableType As String = dr("TABLE_TYPE")

                '//Only get the data for the user tables (non-system)
                If (tableType = "TABLE") Or (tableType = "BASE TABLE") Then
                    cmbTable.Items.Add(tableName)
                End If
            Next

            GroupPanel1.Enabled = True

            connectionString = conBuilder.ConnectionString

            conBuilder = Nothing
            tbls = Nothing
        Catch ex As Exception
            ep.SetError(txtServerName, ex.Message)
            GroupPanel1.Enabled = False
        End Try
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTable.SelectedIndexChanged
        Try
            If lsvValues.Items.Count > 0 Then
                Dim res As DialogResult = MessageBox.Show("Changing the table will clear the values you have already entered. Proceed?", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Question)

                If res = Windows.Forms.DialogResult.Cancel Then
                    Return
                End If
            End If

            lsvValues.Items.Clear()
            cmbColumn.Items.Clear()

            Dim da As SqlDataAdapter = New SqlDataAdapter("SELECT TOP 1 * FROM " & cmbTable.Text, con)

            Dim dt As DataTable = New DataTable

            da.Fill(dt)

            hsDataTypes = New Hashtable

            For Each col As DataColumn In dt.Columns
                cmbColumn.Items.Add(col.ColumnName)
                hsDataTypes.Add(col.ColumnName, col.DataType.ToString)
            Next

            dt = Nothing
            da.Dispose()
            da = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        End Try
    End Sub

    Private Sub cmbColumn_DropDown(sender As Object, e As EventArgs) Handles cmbColumn.DropDown
        If cmbColumn.Items.Count = 0 And cmbTable.Text <> "" Then
            Dim da As SqlDataAdapter = New SqlDataAdapter("SELECT TOP 1 * FROM " & cmbTable.Text, con)

            Dim dt As DataTable = New DataTable

            da.Fill(dt)

            hsDataTypes = New Hashtable

            For Each col As DataColumn In dt.Columns
                cmbColumn.Items.Add(col.ColumnName)
                hsDataTypes.Add(col.ColumnName, col.DataType.ToString)
            Next

            dt = Nothing
            da.Dispose()
            da = Nothing
        End If
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbColumn.SelectedIndexChanged
        '//get the datatype
        Dim dataType As String = hsDataTypes.Item(cmbColumn.Text)

        If dataType.ToLower = "system.byte[]" Then
            btnBrowse.Enabled = True
            btnBrowse_Click(Nothing, Nothing)
        End If

    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the file"
        ofd.Filter = "All Files (*.*)|*.*"
        ofd.Multiselect = False

        If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            txtValue.Text = ofd.FileName
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim item As ListViewItem

        item = New ListViewItem(cmbColumn.Text)

        '//get the select columns datatype
        Dim datatype As String = hsDataTypes.Item(cmbColumn.Text)

        If datatype.ToLower = "system.byte[]" Then
            item.SubItems.Add("<byte>" & txtValue.Text & "</byte>")
        Else
            item.SubItems.Add(txtValue.Text)
        End If

        Dim exist As Boolean = False
        Dim it As ListViewItem

        For Each it In lsvValues.Items
            If it.Text.ToLower = cmbColumn.Text.ToLower Then
                exist = True
                Exit For
            End If
        Next

        If exist = False Then
            lsvValues.Items.Add(item)
        Else
            If it IsNot Nothing Then it.Remove()

            lsvValues.Items.Add(item)
        End If

        txtValue.Text = ""
        txtValue.Focus()

        Try
            Dim index As Integer = cmbColumn.SelectedIndex

            cmbColumn.SelectedIndex = index + 1
        Catch ex As Exception
            cmbColumn.SelectedIndex = 0
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        For Each item As ListViewItem In lsvValues.SelectedItems
            item.Remove()
        Next
    End Sub

    Private Sub btnOK_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        If txtName.Text = "" Then
            setError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf connectionString = "" Then
            setError(txtServerName, "Please configure the SQL Server connection properties")
            txtServerName.Focus()
            Return
        ElseIf cmbTable.Text = "" Then
            setError(cmbTable, "Please the table to insert the data into")
            cmbTable.Focus()
            Return
        ElseIf lsvValues.Items.Count = 0 Then
            setError(lsvValues, "You must assign at least one value to the columns in the table")
            txtValue.Focus()
            Return
        End If

        If con IsNot Nothing Then con.Close() : con = Nothing

        Close()
    End Sub

    Private Sub frmTaskSaveBlobToSQLServer_Load(sender As Object, e As EventArgs) Handles Me.Load
        setupForDragAndDrop(txtValue)

        showInserter(Me, m_eventID)
    End Sub

   

    Private Sub txtValue_KeyDown(sender As Object, e As KeyEventArgs) Handles txtValue.KeyDown
        If e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Return Then
            btnAdd_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub lsvValues_KeyDown(sender As Object, e As KeyEventArgs) Handles lsvValues.KeyDown
        If e.KeyCode = Keys.Delete Then
            btnDelete_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub txtServerName_TextChanged(sender As Object, e As EventArgs) Handles txtServerName.TextChanged, txtDatabaseName.TextChanged, _
        txtUsername.TextChanged, txtPassword.TextChanged

        GroupPanel1.Enabled = False
        connectionString = ""
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        cancel = True

        If con IsNot Nothing Then
            con.Close()
            con = Nothing
        End If

        Close()
    End Sub

    Private Property insertPairs As String
        Get
            Dim values As String

            For Each item As ListViewItem In lsvValues.Items
                values &= item.Text & "::" & item.SubItems(1).Text & PD
            Next

            Return values
        End Get
        Set(value As String)
            For Each s As String In value.Split(PD)
                If s = "" Then Continue For

                Dim column As String = s.Split(New String() {"::"}, StringSplitOptions.RemoveEmptyEntries)(0)
                Dim val As String = s.Split(New String() {"::"}, StringSplitOptions.RemoveEmptyEntries)(1)
                Dim item As ListViewItem = New ListViewItem(column)
                item.SubItems.Add(val)
                lsvValues.Items.Add(item)
            Next
        End Set
    End Property

    Private WriteOnly Property m_connectionString As String
        Set(value As String)
            '"Data Source=.\crd;Initial Catalog=northwind;Integrated Security=True"
            '"Data Source=.\crd;Initial Catalog=northwind;User ID=sa;Password=cssAdmin12345$"

            For Each s As String In value.Split(New String() {";"}, StringSplitOptions.RemoveEmptyEntries)
                If s = "" Then Continue For

                Dim parameter As String = s.Split("=")(0)
                Dim val As String = s.Split("=")(1)

                Select Case parameter.ToLower
                    Case "data source"
                        txtServerName.Text = val
                    Case "initial catalog"
                        txtDatabaseName.Text = val
                    Case "user id"
                        txtUsername.Text = val
                    Case "password"
                        txtPassword.Text = val
                End Select
            Next
        End Set
    End Property
    Public Function addTask(scheduleID As Integer, showAfterType As Boolean) As Integer
        SuperTabItem2.Visible = showAfterType

        Me.ShowDialog()

        If cancel Then Return 0

        Dim newTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        Dim cols, vals As String

        'Connection String -> ProgramParameters
        'Table Inserted -> ProgramPath
        'Columns for insert -> SendTo


        cols = "taskid,taskname,scheduleid,tasktype,programparameters,programpath,sendto"
        vals = newTaskID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            scheduleID & "," & _
            "'uploadblob'," & _
            "'" & SQLPrepare(_EncryptDBValue(connectionString)) & "'," & _
            "'" & SQLPrepare(cmbTable.Text) & "'," & _
            "'" & SQLPrepare(insertPairs) & "'"

        clsMarsData.DataItem.InsertData("tasks", cols, vals)

        TaskExecutionPath1.setTaskRunWhen(newTaskID)

        Return newTaskID
    End Function

    Public Sub editTask(taskID As Integer, Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String = "SELECT * FROM Tasks WHERE TaskID = " & taskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table Inserted -> ProgramPath (";" separated)
        'Columns for insert -> SendTo


        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            m_connectionString = _DecryptDBValue(oRs("programparameters").Value)

            btnTest_Click(Nothing, Nothing)

            cmbTable.DropDownStyle = ComboBoxStyle.DropDown
            cmbTable.SelectedText = oRs("programpath").Value

            insertPairs = oRs("sendto").Value

            oRs.Close()

            oRs = Nothing
        End If

        TaskExecutionPath1.LoadTaskRunWhen(taskID)

        Me.ShowDialog()

        If cancel = True Then Return

        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                 "ProgramParameters = '" & SQLPrepare(_EncryptDBValue(connectionString)) & "'," & _
                 "ProgramPath = '" & SQLPrepare(cmbTable.Text) & "'," & _
                 "SendTo = '" & SQLPrepare(insertPairs) & "'"

        SQL = "UPDATE tasks SET " & SQL & " WHERE taskid = " & taskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(taskID)
    End Sub
End Class