﻿Public Class frmTaskUploadToSharepoint
    Dim ep As ErrorProvider = New ErrorProvider
    Dim userCancel As Boolean = True
    Public m_eventID As Integer = 99999
    Public m_eventBased As Boolean
    Dim selectedTxtBox As TextBox

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog

        With ofd
            .Title = "Select the file to upload"
            .Multiselect = True

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                For Each file As String In .FileNames
                    txtFiles.Text &= file & "|"
                Next
            End If
        End With
    End Sub

    Private Sub frmTaskUploadToSharepoint_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setupForDragAndDrop(txtFiles)
        setupForDragAndDrop(txtDocumentLibrary)
        setupForDragAndDrop(txtMetadata)
        showInserter(Me, m_eventID)
    End Sub

    Dim spUsername, spPassword As String

    Private Sub btnSharepointBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSharepointBrowse.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o4_SharePointDestination) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, btnSharepointBrowse, "SharePoint Destination")
            Return
        End If

        Dim sh As clsSharePointHelper = New clsSharePointHelper
        Dim spVal As Boolean
        Dim showmeta As Boolean

        spVal = sh.GetSharePointLib(txtSharepointServer.Text, spUsername, spPassword, txtDocumentLibrary.Text, txtMetadata.Text, True, chkOffice365.Checked)
    End Sub

    Private Sub btnMetadata_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMetadata.Click
        Dim spEditor As frmEditSharePointFolder = New frmEditSharePointFolder

        Dim newLib As String = spEditor.editSharepointLib(txtDocumentLibrary.Text, txtMetadata.Text)

        If newLib IsNot Nothing Then
            txtDocumentLibrary.Text = newLib
        End If
    End Sub


    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If userCancel Then Return 0
        Dim sCols, sVals As String
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID

        '//Msg is used for file metadata, the rest is self explanatory since Sharepoint is just fancy FTP :-)

        sCols = "TaskID,ScheduleID,TaskType,TaskName,FTPServer,FTPUser," & _
       "FTPPassword,FTPDirectory,Msg,Filelist,OrderID,FTPPassive"


        sVals = nID & "," & _
            ScheduleID & "," & _
            "'SharePointUpload'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(txtSharepointServer.Text) & "'," & _
            "'" & SQLPrepare(spUsername) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(spPassword)) & "'," & _
            "'" & SQLPrepare(txtDocumentLibrary.Text) & "'," & _
            "'" & SQLPrepare(txtMetadata.Text) & "'," & _
            "'" & SQLPrepare(txtFiles.Text) & "'," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            Convert.ToInt32(chkOffice365.Checked)


        Dim SQL As String = "INSERT INTO TASKS (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function

    Public Sub editTask(ByVal nTaskID As Integer, Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtSharepointServer.Text = oRs("ftpserver").Value
            spUsername = oRs("ftpuser").Value
            spPassword = _DecryptDBValue(oRs("ftppassword").Value)
            txtDocumentLibrary.Text = oRs("ftpdirectory").Value
            txtFiles.Text = oRs("filelist").Value
            txtMetadata.Text = oRs("msg").Value

            Try
                chkOffice365.Checked = IsNull(oRs("ftppassive").Value)
            Catch ex As Exception
                chkOffice365.Checked = False
            End Try
        End If

        Me.ShowDialog()

        If userCancel = True Then Return

        SQL = "UPDATE Tasks SET " & _
        "taskname ='" & SQLPrepare(txtName.Text) & "'," & _
        "ftpserver = '" & SQLPrepare(txtSharepointServer.Text) & "'," & _
        "ftpuser ='" & SQLPrepare(spUsername) & "'," & _
        "ftppassword ='" & SQLPrepare(_EncryptDBValue(spPassword)) & "'," & _
        "ftpdirectory ='" & SQLPrepare(txtDocumentLibrary.Text) & "'," & _
        "filelist ='" & SQLPrepare(txtFiles.Text) & "'," & _
        "ftppassive = " & Convert.ToInt32(chkOffice365.Checked) & "," & _
        "msg ='" & SQLPrepare(txtMetadata.Text) & "' WHERE taskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please give the task a name")
            txtName.Focus()
            Return
        ElseIf txtSharepointServer.Text = "" Or txtDocumentLibrary.Text = "" Then
            ep.SetError(txtSharepointServer, "Please select the SharePoint server and library")
            txtSharepointServer.Focus()
            Return
        ElseIf txtFiles.Text = "" Then
            ep.SetError(txtFiles, "Please select the files to upload to Sharepoint")
            txtFiles.Focus()
            Return
        End If

        userCancel = False

        Close()
    End Sub

    Private Sub txtDocumentLibrary_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFiles.GotFocus, txtDocumentLibrary.GotFocus, txtSharepointServer.GotFocus, _
        txtMetadata.GotFocus

        selectedTxtBox = CType(sender, TextBox)
    End Sub

    Private Sub txtFiles_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFiles.TextChanged, txtDocumentLibrary.TextChanged, txtName.TextChanged, _
    txtSharepointServer.TextChanged

        ep.SetError(sender, "")

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.Undo()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.Cut()
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.Copy()
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.Paste()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        If selectedTxtBox IsNot Nothing Then
            selectedTxtBox.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        If selectedTxtBox IsNot Nothing Then
            Dim oData As New frmDataItems

            selectedTxtBox.SelectedText = oData._GetDataItem(Me.m_eventID)
        End If
    End Sub
End Class