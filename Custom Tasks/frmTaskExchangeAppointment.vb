﻿Public Class frmTaskExchangeAppointment
    Dim eventID As Integer = 99999
    Dim eventBased As Boolean = False
    Dim userCancel As Boolean

    Public Property m_eventBased() As Boolean
        Get
            Return EventBased
        End Get
        Set(ByVal value As Boolean)
            EventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Private Sub btnAttendees_Click(sender As Object, e As EventArgs) Handles btnAttendees.Click

    End Sub

    Private Sub btnAttendees_MouseUp(sender As Object, e As MouseEventArgs) Handles btnAttendees.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        mnuContacts.Show(btnAttendees, New Point(e.X, e.Y))
    End Sub



    Property eventPriority As String
        Get
            Return lblImportance.Tag
        End Get
        Set(value As String)
            Select Case value.ToLower
                Case 2
                    lblImportance.Tag = 2
                    lblImportance.Text = "High Importance"
                Case 1
                    lblImportance.Tag = 1
                    lblImportance.Text = "Normal Importance"
                Case 0
                    lblImportance.Tag = 0
                    lblImportance.Text = "Low Importance"
            End Select
        End Set
    End Property

    Private Sub High_Click(sender As Object, e As EventArgs) Handles High.Click
        eventPriority = 2
    End Sub

    Private Sub Normal_Click(sender As Object, e As EventArgs) Handles Normal.Click, Normal2.Click
        eventPriority = 1
    End Sub


    Private Sub Low_Click(sender As Object, e As EventArgs) Handles Low.Click
        eventPriority = 0
    End Sub

    Private Sub btnBrowse_Click(sender As Object, e As EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Multiselect = True
        ofd.Title = "Select files to attach"

        If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            For Each s As String In ofd.FileNames
                txtFileAttachments.Text &= s & PD
            Next
        End If
    End Sub
    Private Sub setEmailAutocompleteSource(txt As DevComponents.DotNetBar.Controls.TextBoxX)
        Try
            Dim ac As AutoCompleteStringCollection = New AutoCompleteStringCollection
            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")

            If IO.File.Exists(emailCache) = False Then Return

            Dim dt As DataTable = New DataTable

            dt.ReadXml(emailCache)

            For Each row As DataRow In dt.Rows
                ac.Add(row("emailaddress"))
            Next

            txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            txt.AutoCompleteSource = AutoCompleteSource.CustomSource
            txt.AutoCompleteCustomSource = ac
        Catch : End Try
    End Sub
    Private Sub frmTaskExchangeAppointment_Load(sender As Object, e As EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtAttendees)
        setupForDragAndDrop(txtBody)
        setupForDragAndDrop(txtFileAttachments)
        setupForDragAndDrop(txtLocation)
        setupForDragAndDrop(txtSubject)
        setupForDragAndDrop(txtAutodiscover)
        setupForDragAndDrop(txtServerUrl)
        setupForDragAndDrop(txtEWSUserID)
        setupForDragAndDrop(txtEWSPassword)
        setupForDragAndDrop(txtStartTime)
        setupForDragAndDrop(txtEndTime)

        showInserter(Me, m_eventID, m_eventBased)

        setEmailAutocompleteSource(txtAttendees)
    End Sub

    Private Sub mnuMAPI_Click(sender As Object, e As EventArgs) Handles mnuMAPI.Click
        Dim omsg As clsMarsMessaging = New clsMarsMessaging
        omsg.OutlookAddresses(txtAttendees)

    End Sub

    Private Sub mnuMARS_Click(sender As Object, e As EventArgs) Handles mnuMARS.Click


        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact("to")

        If sValues Is Nothing Then Return

        If txtAttendees.Text.EndsWith(";") = False And txtAttendees.Text.Length > 0 Then txtAttendees.Text &= ";"

        Try
            txtAttendees.Text &= sValues(0)
        Catch : End Try

    End Sub

    Private Sub mnuDB_Click(sender As Object, e As EventArgs) Handles mnuDB.Click

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(Me.m_eventID)

        If sTemp.Length > 0 Then
            If txtAttendees.Text.Length > 0 And txtAttendees.Text.EndsWith(";") = False Then txtAttendees.Text &= ";"
            txtAttendees.Text &= sTemp & ";"
        End If
    End Sub

    Private Sub mnuFile_Click(sender As Object, e As EventArgs) Handles mnuFile.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog

        ofd.Title = "Please select the file to read email addresses from"

        ofd.ShowDialog()

        If ofd.FileName.Length > 0 Then
            If txtAttendees.Text.EndsWith(";") = False And txtAttendees.Text.Length > 0 Then _
            txtAttendees.Text &= ";"

            txtAttendees.Text &= "<[f]" & ofd.FileName & ">;"
        End If
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
   Optional ByVal ShowAfter As Boolean = True) As Integer

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        '//programpath is the location
        '//allday = replacefiles
        '//reminder = pauseint
        '//files = filelist
        '//Msg = body
        '//importance = cpupriority
        'userid = ftpuser
        'password = ftppassword
        'usediscover = ftppassive
        'autodiscoveremail = cc
        'serverurl = ftpserver

        sCols = "TaskID,ScheduleID,TaskType,TaskName," & _
        "subject,programpath,sendto,starttime,endtime,replacefiles,msg,pauseint,cpupriority,filelist,ftpuser,ftppassword,ftppassive,cc,ftpserver,orderid"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'CreateAppointment'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(txtSubject.Text) & "'," & _
            "'" & SQLPrepare(txtLocation.Text) & "'," & _
            "'" & SQLPrepare(txtAttendees.Text) & "'," & _
            "'" & txtStartTime.Text & "'," & _
            "'" & txtEndTime.Text & "'," & _
            Convert.ToInt16(chkAllDayEvent.Checked) & "," & _
            "'" & SQLPrepare(txtBody.Text) & "'," & _
            txtReminder.Value & "," & _
            "'" & eventPriority & "'," & _
            "'" & SQLPrepare(txtFileAttachments.Text) & "'," & _
            "'" & SQLPrepare(txtEWSUserID.Text) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(txtEWSPassword.Text)) & "'," & _
            Convert.ToInt16(chkAutodiscover.Checked) & "," & _
            "'" & SQLPrepare(txtAutodiscover.Text) & "'," & _
            "'" & SQLPrepare(txtServerUrl.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)

        taskPath.setTaskRunWhen(nID)

        Return nID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
   Optional ByVal ShowAfter As Boolean = True)
        Dim oData As clsMarsData = New clsMarsData
        Dim msg As String
        Dim SQL As String

        Sql = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(Sql)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtSubject.Text = IsNull(oRs("subject").Value)
            txtLocation.Text = IsNull(oRs("programpath").Value)
            txtAttendees.Text = IsNull(oRs("sendto").Value)
            chkAllDayEvent.Checked = IsNull(oRs("replacefiles").Value, 0)

            txtStartTime.Text = oRs("starttime").Value
            txtEndTime.Text = oRs("endtime").Value
            chkAllDayEvent.Checked = IsNull(oRs("replacefiles").Value, 0)
            txtBody.Text = IsNull(oRs("msg").Value)
            txtReminder.Value = IsNull(oRs("pauseint").Value, 15)
            eventPriority = IsNull(oRs("cpupriority").Value, 1)
            txtFileAttachments.Text = IsNull(oRs("filelist").Value)

            txtEWSUserID.Text = IsNull(oRs("ftpuser").Value)
            txtEWSPassword.Text = _DecryptDBValue(IsNull(oRs("ftppassword").Value))
            chkAutodiscover.Checked = Convert.ToInt16(oRs("ftppassive").Value)
            txtAutodiscover.Text = IsNull(oRs("cc").Value)
            txtServerUrl.Text = IsNull(oRs("ftpserver").Value)

            oRs.Close()
        End If

        taskPath.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If userCancel = True Then Exit Sub

        SQL = "TaskName ='" & SQLPrepare(txtName.Text) & "'," & _
        "subject='" & SQLPrepare(txtSubject.Text) & "'," & _
        "programpath ='" & SQLPrepare(txtLocation.Text) & "'," & _
        "sendto ='" & SQLPrepare(txtAttendees.Text) & "'," & _
        "starttime ='" & SQLPrepare(txtStartTime.Text) & "'," & _
        "endtime = '" & SQLPrepare(txtEndTime.Text) & "'," & _
        "replacefiles =" & Convert.ToInt16(chkAllDayEvent.Checked) & "," & _
        "msg ='" & SQLPrepare(txtBody.Text) & "'," & _
        "pauseint =" & txtReminder.Value & "," & _
        "cpupriority ='" & eventPriority & "'," & _
        "filelist ='" & SQLPrepare(txtFileAttachments.Text) & "'," & _
        "ftpuser ='" & SQLPrepare(txtEWSUserID.Text) & "'," & _
        "ftppassword = '" & SQLPrepare(_EncryptDBValue(txtEWSPassword.Text)) & "'," & _
        "ftppassive = " & Convert.ToInt16(chkAutodiscover.Checked) & "," & _
        "cc = '" & SQLPrepare(txtAutodiscover.Text) & "'," & _
        "ftpserver ='" & SQLPrepare(txtServerUrl.Text) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        taskPath.setTaskRunWhen(nTaskID)
    End Sub
    Private Sub txtAttendees_LostFocus(sender As Object, e As EventArgs) Handles txtAttendees.LostFocus
        Try
            If CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text = "" Then Return

            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")
            Dim dtCache As DataTable = New DataTable("emailcache")

            If IO.File.Exists(emailCache) Then
                dtCache.ReadXml(emailCache)
            Else
                With dtCache.Columns
                    .Add("emailAddress")
                End With
            End If

            Dim row As DataRow
            Dim rows() As DataRow = dtCache.Select("emailaddress = '" & CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text & "'")

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                row = rows(0)
            Else
                row = dtCache.Rows.Add
                row("emailaddress") = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text
            End If

            dtCache.WriteXml(emailCache, XmlWriteMode.WriteSchema)
        Catch : End Try
    End Sub

    Private Sub cmdCancel_Click(sender As Object, e As EventArgs) Handles cmdCancel.Click
        userCancel = True
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As EventArgs) Handles cmdOK.Click
        If txtName.Text = "" And txtName.Visible = True Then
            setError(txtName, "Please provide a name for this task")
            txtName.Focus()
            SuperTabControl1.SelectedTab = SuperTabItem1
            Exit Sub
        ElseIf txtSubject.Text = "" Then
            setError(txtSubject, "Please enter the subject")
            txtSubject.Focus()
            SuperTabControl1.SelectedTab = SuperTabItem1
            Return
        ElseIf txtServerUrl.Text = "" Then
            setError(txtServerUrl, "Please enter the Exchange Service URL")
            SuperTabControl1.SelectedTab = SuperTabItem3
            txtServerUrl.Focus()
            Return
        ElseIf chkAutodiscover.Checked And txtAutodiscover.Text = "" Then
            setError(txtAutodiscover, "Please enter the auto-discovery email address")
            SuperTabControl1.SelectedTab = SuperTabItem3
            txtAutodiscover.Focus()
            Return
        ElseIf txtStartTime.Text = "" Then
            setError(txtStartTime, "Please provide the event's start time")
            txtStartTime.Focus()
            SuperTabControl1.SelectedTab = SuperTabItem1
            Return
        ElseIf txtEndTime.Text = "" Then
            setError(txtEndTime, "Please provide the event's start time")
            txtEndTime.Focus()
            SuperTabControl1.SelectedTab = SuperTabItem1
            Return
        Else
            Try
                Dim fullstartDate As Date = clsMarsParser.Parser.ParseString(txtStartTime.Text)
                Dim fullendDate As Date = clsMarsParser.Parser.ParseString(txtEndTime.Text)

                If fullendDate < fullstartDate And chkAllDayEvent.Checked = False Then
                    setError(txtStartTime, "Please set an accurate event duration")
                    txtStartTime.Focus()
                    SuperTabControl1.SelectedTab = SuperTabItem1
                    Return
                End If
            Catch : End Try
        End If

        Close()
    End Sub
    Private Function validateAutoDiscoverRedirection()
        Return True
    End Function
    Private Sub btnAutoDiscover_Click(sender As Object, e As EventArgs) Handles btnAutoDiscover.Click
        AppStatus(True)
        Dim service As Microsoft.Exchange.WebServices.Data.ExchangeService = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2007_SP1)

        Dim username, domain As String
        username = txtEWSUserID.Text


        If username.Contains("\") Then
            domain = txtEWSUserID.Text.Split("\")(0)
            username = username.Split("\")(1)
        End If

        If domain = "" Then domain = Environment.UserDomainName

        service.Credentials = New System.Net.NetworkCredential(username, txtEWSPassword.Text, domain)

        Try
            service.AutodiscoverUrl(txtAutodiscover.Text, AddressOf validateAutoDiscoverRedirection)

            txtServerUrl.Text = service.Url.ToString
            txtServerUrl.SelectAll()
            txtServerUrl.Focus()

        Catch ex As Exception
            MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        AppStatus(False)
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub chkAutodiscover_CheckedChanged(sender As Object, e As EventArgs) Handles chkAutodiscover.CheckedChanged
        txtAutodiscover.Enabled = chkAutodiscover.Checked
        btnAutoDiscover.Enabled = chkAutodiscover.Checked
    End Sub

    Private Sub txtStartTime_MouseClick(sender As Object, e As MouseEventArgs) Handles txtStartTime.MouseClick, txtEndTime.MouseClick
        Dim cmb As DevComponents.DotNetBar.Controls.TextBoxX = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX)

        If cmb.Text <> "" Then Return

        Dim datetypeStrings As String() = New String() {"DateTimeParameter", "DateParameter", "crDateField", "crDateTimeField"}

        Dim selekta As frmDateTimePicker = New frmDateTimePicker

        selekta.StartPosition = FormStartPosition.Manual

        selekta.Location = New Point(Cursor.Position.X, Cursor.Position.Y)

        If chkAllDayEvent.Checked = False Then
            selekta.includeTime = True

            If selekta.pickDate(cmb.Text) = DialogResult.OK Then
                cmb.Text = selekta.selectedDateTime
            End If
        Else
            selekta.includeTime = False

            If selekta.pickDate(cmb.Text) = DialogResult.OK Then
                cmb.Text = selekta.selectedDate
            End If
        End If
    End Sub

    Private Sub chkAllDayEvent_CheckedChanged(sender As Object, e As EventArgs) Handles chkAllDayEvent.CheckedChanged
        Try
            If chkAllDayEvent.Checked And txtStartTime.Text <> "" Then
                txtStartTime.Text = ConDate(txtStartTime.Text)
            End If

            If chkAllDayEvent.Checked And txtEndTime.Text <> "" Then
                txtEndTime.Text = ConDate(txtEndTime.Text)
            End If
        Catch : End Try
    End Sub

    Private Sub txtName_LostFocus(sender As Object, e As EventArgs) Handles txtName.LostFocus
        If txtSubject.Text = "" Then
            txtSubject.Text = txtName.Text
        End If
    End Sub


    Private Sub txtSubject_GotFocus(sender As Object, e As EventArgs) Handles txtSubject.GotFocus
        If txtSubject.Text = txtName.Text Then
            txtSubject.SelectAll()
        End If
    End Sub

    Private Sub txtEWSUserID_LostFocus(sender As Object, e As EventArgs) Handles txtEWSUserID.LostFocus
        If txtEWSUserID.Text.Contains("@") And txtAutodiscover.Text = "" Then
            txtAutodiscover.Text = txtEWSUserID.Text
        End If
    End Sub


End Class
