Public Class frmTaskWebBrowse
    Inherits frmTaskMaster
    Dim UserCancel As Boolean
    Friend WithEvents chkDownloadFile As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtSaveTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnSaveTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Dim WithEvents oIE As SHDocVw.InternetExplorer
    Public m_eventID As Integer = 99999
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtURL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkDownloadFile = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnSaveTo = New DevComponents.DotNetBar.ButtonX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtSaveTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtURL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(472, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 19
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(391, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 18
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(433, 20)
        Me.txtName.TabIndex = 17
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 20)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkDownloadFile)
        Me.GroupBox1.Controls.Add(Me.btnSaveTo)
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.txtSaveTo)
        Me.GroupBox1.Controls.Add(Me.txtURL)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(544, 108)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'chkDownloadFile
        '
        Me.chkDownloadFile.AutoSize = True
        '
        '
        '
        Me.chkDownloadFile.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDownloadFile.Location = New System.Drawing.Point(8, 56)
        Me.chkDownloadFile.Name = "chkDownloadFile"
        Me.chkDownloadFile.Size = New System.Drawing.Size(285, 15)
        Me.chkDownloadFile.TabIndex = 22
        Me.chkDownloadFile.Text = "Download file at URL (i.e do not open in web browser)"
        '
        'btnSaveTo
        '
        Me.btnSaveTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSaveTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSaveTo.Enabled = False
        Me.btnSaveTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSaveTo.Location = New System.Drawing.Point(469, 79)
        Me.btnSaveTo.Name = "btnSaveTo"
        Me.btnSaveTo.Size = New System.Drawing.Size(68, 19)
        Me.btnSaveTo.TabIndex = 21
        Me.btnSaveTo.Text = "..."
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(469, 30)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(68, 19)
        Me.cmdBrowse.TabIndex = 21
        Me.cmdBrowse.Text = "..."
        '
        'txtSaveTo
        '
        Me.txtSaveTo.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSaveTo.Border.Class = "TextBoxBorder"
        Me.txtSaveTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSaveTo.DisabledBackColor = System.Drawing.Color.White
        Me.txtSaveTo.Enabled = False
        Me.txtSaveTo.ForeColor = System.Drawing.Color.Blue
        Me.txtSaveTo.Location = New System.Drawing.Point(8, 79)
        Me.txtSaveTo.Name = "txtSaveTo"
        Me.txtSaveTo.Size = New System.Drawing.Size(455, 20)
        Me.txtSaveTo.TabIndex = 17
        '
        'txtURL
        '
        Me.txtURL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtURL.Border.Class = "TextBoxBorder"
        Me.txtURL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtURL.DisabledBackColor = System.Drawing.Color.White
        Me.txtURL.ForeColor = System.Drawing.Color.Blue
        Me.txtURL.Location = New System.Drawing.Point(8, 30)
        Me.txtURL.Name = "txtURL"
        Me.txtURL.Size = New System.Drawing.Size(455, 20)
        Me.txtURL.TabIndex = 17
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Web Address"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(550, 176)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 22
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(550, 151)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(550, 176)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(550, 28)
        Me.FlowLayoutPanel1.TabIndex = 23
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 207)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(550, 29)
        Me.FlowLayoutPanel2.TabIndex = 24
        '
        'frmTaskWebBrowse
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(550, 236)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskWebBrowse"
        Me.Text = "Web Document"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        oIE = New SHDocVw.InternetExplorer

        oIE.Visible = True

        If txtURL.Text <> "" Then
            oIE.Navigate2(txtURL.Text)
        End If
    End Sub

   

    Private Sub oIE_NavigateComplete2(ByVal pDisp As Object, ByRef URL As Object) Handles oIE.NavigateComplete2

        Dim s As String = URL

        txtURL.Text = s


    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True

        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf txtURL.Text = "" Then
            SetError(txtURL, "Please select the web address")
            txtURL.Focus()
            Exit Sub
        ElseIf chkDownloadFile.Checked And txtSaveTo.Text = "" Then
            SetError(txtSaveTo, "Please provide the folder to download the file to")
            txtSaveTo.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ProgramParameters,OrderID"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'WebBrowse'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtURL.Text.Replace("'", "''") & "'," & _
        "'" & SQLPrepare(txtSaveTo.Text) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtURL.Text = oRs("programpath").Value
            txtSaveTo.Text = IsNull(oRs("programparameters").Value, "")

            If txtSaveTo.Text <> "" Then
                chkDownloadFile.Checked = True
            End If
        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtURL.Text.Replace("'", "''") & "'," & _
            "ProgramParameters = '" & SQLPrepare(txtSaveTo.Text) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub


    ' Private Sub txtURL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtURL.TextChanged, txtSaveTo.TextChanged
    '    SetError(sender, "")
    ' End Sub

    Private Sub frmTaskWebBrowse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtURL)
        setupForDragAndDrop(txtSaveTo)

        showInserter(Me, m_eventID)
    End Sub

    Private Sub chkDownloadFile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDownloadFile.CheckedChanged
        txtSaveTo.Enabled = chkDownloadFile.Checked
        btnSaveTo.Enabled = chkDownloadFile.Checked

        If chkDownloadFile.Checked = False Then
            txtSaveTo.Text = ""
        End If
    End Sub

    Private Sub btnSaveTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTo.Click
        If txtURL.Text.Contains("|") Or txtURL.Text.Contains("<[") Then
            Dim fbd As FolderBrowserDialog = New FolderBrowserDialog
            fbd.Description = "Select the folder to download to"

            If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtSaveTo.Text = fbd.SelectedPath

                If txtSaveTo.Text.EndsWith("\") = False Then txtSaveTo.Text = txtSaveTo.Text & "\"
            End If
        Else
            Dim sfd As SaveFileDialog = New SaveFileDialog
            sfd.Title = "Select the folder and file to download the file to"

            If sfd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtSaveTo.Text = sfd.FileName
            End If
        End If

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Undo()
            End If
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Cut()
            End If
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Copy()
            End If
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Paste()
            End If
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).SelectedText = ""
            End If
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim ctrl As Control = Me.ActiveControl

            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).SelectAll()
            End If
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            CType(ctrl, TextBox).SelectedText = oItem._GetDataItem(0)
        End If

    End Sub
End Class
