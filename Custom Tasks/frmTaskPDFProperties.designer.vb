﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskPDFProperties
    Inherits frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabControl2 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dtPDFExpiry = New System.Windows.Forms.DateTimePicker()
        Me.chkPDFExpiry = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoTitle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoAuthor = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoKeywords = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoProducer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem5 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtWatermark = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkPrint = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkCopy = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkEdit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkNotes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFill = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAccess = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAssemble = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFullRes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtOwnerPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUserPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtPDFPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(504, 28)
        Me.FlowLayoutPanel2.TabIndex = 53
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 21)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.DisabledBackColor = System.Drawing.Color.White
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(72, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(396, 21)
        Me.txtName.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 470)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(504, 29)
        Me.FlowLayoutPanel1.TabIndex = 54
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(426, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(345, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(504, 442)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 55
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.SuperTabControl2)
        Me.SuperTabControlPanel1.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(504, 416)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabControl2
        '
        Me.SuperTabControl2.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl2.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl2.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl2.ControlBox.Name = ""
        Me.SuperTabControl2.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl2.ControlBox.MenuBox, Me.SuperTabControl2.ControlBox.CloseBox})
        Me.SuperTabControl2.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl2.Controls.Add(Me.SuperTabControlPanel5)
        Me.SuperTabControl2.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl2.Location = New System.Drawing.Point(7, 38)
        Me.SuperTabControl2.Name = "SuperTabControl2"
        Me.SuperTabControl2.ReorderTabsEnabled = True
        Me.SuperTabControl2.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl2.SelectedTabIndex = 0
        Me.SuperTabControl2.Size = New System.Drawing.Size(490, 369)
        Me.SuperTabControl2.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl2.TabIndex = 9
        Me.SuperTabControl2.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem4, Me.SuperTabItem5})
        Me.SuperTabControl2.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.SuperTabControl2.Text = "stabPDF"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(490, 369)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.SuperTabItem5
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.dtPDFExpiry)
        Me.GroupBox2.Controls.Add(Me.chkPDFExpiry)
        Me.GroupBox2.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox2.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox2.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox2.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtInfoProducer)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(474, 334)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'dtPDFExpiry
        '
        Me.dtPDFExpiry.Enabled = False
        Me.dtPDFExpiry.Location = New System.Drawing.Point(133, 298)
        Me.dtPDFExpiry.Name = "dtPDFExpiry"
        Me.dtPDFExpiry.Size = New System.Drawing.Size(325, 21)
        Me.dtPDFExpiry.TabIndex = 29
        '
        'chkPDFExpiry
        '
        '
        '
        '
        Me.chkPDFExpiry.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPDFExpiry.Location = New System.Drawing.Point(6, 297)
        Me.chkPDFExpiry.Name = "chkPDFExpiry"
        Me.chkPDFExpiry.Size = New System.Drawing.Size(121, 23)
        Me.chkPDFExpiry.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.chkPDFExpiry.TabIndex = 28
        Me.chkPDFExpiry.Text = "PDF Expires On"
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Enabled = False
        Me.txtInfoCreated.Location = New System.Drawing.Point(133, 146)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        Me.txtInfoTitle.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInfoTitle.Border.Class = "TextBoxBorder"
        Me.txtInfoTitle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoTitle.DisabledBackColor = System.Drawing.Color.White
        Me.txtInfoTitle.ForeColor = System.Drawing.Color.Black
        Me.txtInfoTitle.Location = New System.Drawing.Point(133, 18)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        Me.txtInfoAuthor.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInfoAuthor.Border.Class = "TextBoxBorder"
        Me.txtInfoAuthor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoAuthor.DisabledBackColor = System.Drawing.Color.White
        Me.txtInfoAuthor.ForeColor = System.Drawing.Color.Black
        Me.txtInfoAuthor.Location = New System.Drawing.Point(133, 50)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        Me.txtInfoSubject.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInfoSubject.Border.Class = "TextBoxBorder"
        Me.txtInfoSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoSubject.DisabledBackColor = System.Drawing.Color.White
        Me.txtInfoSubject.ForeColor = System.Drawing.Color.Black
        Me.txtInfoSubject.Location = New System.Drawing.Point(133, 114)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        Me.txtInfoKeywords.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInfoKeywords.Border.Class = "TextBoxBorder"
        Me.txtInfoKeywords.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoKeywords.DisabledBackColor = System.Drawing.Color.White
        Me.txtInfoKeywords.ForeColor = System.Drawing.Color.Black
        Me.txtInfoKeywords.Location = New System.Drawing.Point(133, 178)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(325, 107)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(6, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(6, 84)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(6, 178)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(6, 148)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        Me.txtInfoProducer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtInfoProducer.Border.Class = "TextBoxBorder"
        Me.txtInfoProducer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoProducer.DisabledBackColor = System.Drawing.Color.White
        Me.txtInfoProducer.ForeColor = System.Drawing.Color.Black
        Me.txtInfoProducer.Location = New System.Drawing.Point(133, 82)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoProducer.TabIndex = 20
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(6, 116)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'SuperTabItem5
        '
        Me.SuperTabItem5.AttachedControl = Me.SuperTabControlPanel5
        Me.SuperTabItem5.GlobalItem = False
        Me.SuperTabItem5.Name = "SuperTabItem5"
        Me.SuperTabItem5.Text = "File Summary"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Label2)
        Me.SuperTabControlPanel4.Controls.Add(Me.txtWatermark)
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(490, 345)
        Me.SuperTabControlPanel4.TabIndex = 1
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(11, 190)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Include the following watermark/stamp"
        '
        'txtWatermark
        '
        Me.txtWatermark.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtWatermark.Border.Class = "TextBoxBorder"
        Me.txtWatermark.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWatermark.DisabledBackColor = System.Drawing.Color.White
        Me.txtWatermark.ForeColor = System.Drawing.Color.Black
        Me.txtWatermark.Location = New System.Drawing.Point(11, 206)
        Me.txtWatermark.Name = "txtWatermark"
        Me.txtWatermark.Size = New System.Drawing.Size(410, 21)
        Me.txtWatermark.TabIndex = 16
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkPrint)
        Me.GroupBox1.Controls.Add(Me.chkCopy)
        Me.GroupBox1.Controls.Add(Me.chkEdit)
        Me.GroupBox1.Controls.Add(Me.chkNotes)
        Me.GroupBox1.Controls.Add(Me.chkFill)
        Me.GroupBox1.Controls.Add(Me.chkAccess)
        Me.GroupBox1.Controls.Add(Me.chkAssemble)
        Me.GroupBox1.Controls.Add(Me.chkFullRes)
        Me.GroupBox1.Controls.Add(Me.txtOwnerPassword)
        Me.GroupBox1.Controls.Add(Me.txtUserPassword)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(418, 176)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "File Permissions"
        '
        'chkPrint
        '
        '
        '
        '
        Me.chkPrint.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPrint.Checked = True
        Me.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrint.CheckValue = "Y"
        Me.chkPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPrint.Location = New System.Drawing.Point(8, 72)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(88, 24)
        Me.chkPrint.TabIndex = 2
        Me.chkPrint.Text = "Can Print"
        '
        'chkCopy
        '
        '
        '
        '
        Me.chkCopy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCopy.Checked = True
        Me.chkCopy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopy.CheckValue = "Y"
        Me.chkCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCopy.Location = New System.Drawing.Point(8, 96)
        Me.chkCopy.Name = "chkCopy"
        Me.chkCopy.Size = New System.Drawing.Size(88, 24)
        Me.chkCopy.TabIndex = 3
        Me.chkCopy.Text = "Can Copy"
        '
        'chkEdit
        '
        '
        '
        '
        Me.chkEdit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEdit.Checked = True
        Me.chkEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEdit.CheckValue = "Y"
        Me.chkEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEdit.Location = New System.Drawing.Point(8, 120)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(80, 24)
        Me.chkEdit.TabIndex = 4
        Me.chkEdit.Text = "Can Edit"
        '
        'chkNotes
        '
        '
        '
        '
        Me.chkNotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNotes.Checked = True
        Me.chkNotes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNotes.CheckValue = "Y"
        Me.chkNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNotes.Location = New System.Drawing.Point(168, 120)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(104, 24)
        Me.chkNotes.TabIndex = 8
        Me.chkNotes.Text = "Can Add Notes"
        '
        'chkFill
        '
        '
        '
        '
        Me.chkFill.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFill.Checked = True
        Me.chkFill.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFill.CheckValue = "Y"
        Me.chkFill.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFill.Location = New System.Drawing.Point(8, 144)
        Me.chkFill.Name = "chkFill"
        Me.chkFill.Size = New System.Drawing.Size(96, 24)
        Me.chkFill.TabIndex = 5
        Me.chkFill.Text = "Can Fill Fields"
        '
        'chkAccess
        '
        '
        '
        '
        Me.chkAccess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAccess.Checked = True
        Me.chkAccess.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAccess.CheckValue = "Y"
        Me.chkAccess.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAccess.Location = New System.Drawing.Point(168, 144)
        Me.chkAccess.Name = "chkAccess"
        Me.chkAccess.Size = New System.Drawing.Size(176, 24)
        Me.chkAccess.TabIndex = 9
        Me.chkAccess.Text = "Can copy accessibility options"
        '
        'chkAssemble
        '
        '
        '
        '
        Me.chkAssemble.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAssemble.Checked = True
        Me.chkAssemble.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssemble.CheckValue = "Y"
        Me.chkAssemble.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssemble.Location = New System.Drawing.Point(168, 72)
        Me.chkAssemble.Name = "chkAssemble"
        Me.chkAssemble.Size = New System.Drawing.Size(96, 24)
        Me.chkAssemble.TabIndex = 6
        Me.chkAssemble.Text = "Can Assemble"
        '
        'chkFullRes
        '
        '
        '
        '
        Me.chkFullRes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFullRes.Checked = True
        Me.chkFullRes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFullRes.CheckValue = "Y"
        Me.chkFullRes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFullRes.Location = New System.Drawing.Point(168, 96)
        Me.chkFullRes.Name = "chkFullRes"
        Me.chkFullRes.Size = New System.Drawing.Size(152, 24)
        Me.chkFullRes.TabIndex = 7
        Me.chkFullRes.Text = "Can print in full resolution"
        '
        'txtOwnerPassword
        '
        Me.txtOwnerPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtOwnerPassword.Border.Class = "TextBoxBorder"
        Me.txtOwnerPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOwnerPassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtOwnerPassword.ForeColor = System.Drawing.Color.Black
        Me.txtOwnerPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOwnerPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtOwnerPassword.Name = "txtOwnerPassword"
        Me.txtOwnerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOwnerPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtOwnerPassword.TabIndex = 0
        '
        'txtUserPassword
        '
        Me.txtUserPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUserPassword.Border.Class = "TextBoxBorder"
        Me.txtUserPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserPassword.DisabledBackColor = System.Drawing.Color.White
        Me.txtUserPassword.ForeColor = System.Drawing.Color.Black
        Me.txtUserPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtUserPassword.Location = New System.Drawing.Point(168, 40)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtUserPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtUserPassword.TabIndex = 1
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(168, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User Password"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LabelX2.Location = New System.Drawing.Point(8, 24)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(112, 16)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Owner Password"
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(8, 64)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(402, 8)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Tag = "3dline"
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "PDF Security"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtPDFPath)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnBrowse)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(4, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(493, 29)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(59, 21)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "PDF File"
        '
        'txtPDFPath
        '
        Me.txtPDFPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPDFPath.Border.Class = "TextBoxBorder"
        Me.txtPDFPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPDFPath.DisabledBackColor = System.Drawing.Color.White
        Me.txtPDFPath.ForeColor = System.Drawing.Color.Black
        Me.txtPDFPath.Location = New System.Drawing.Point(68, 3)
        Me.txtPDFPath.Name = "txtPDFPath"
        Me.txtPDFPath.Size = New System.Drawing.Size(356, 21)
        Me.txtPDFPath.TabIndex = 1
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(430, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(52, 21)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeMobile2014
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "..."
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(504, 442)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'frmTaskPDFProperties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(504, 499)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskPDFProperties"
        Me.Text = "Edit PDF Properties"
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPDFPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControl2 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem5 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtWatermark As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPrint As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkCopy As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkEdit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNotes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFill As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAccess As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAssemble As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFullRes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtOwnerPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPDFExpiry As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkPDFExpiry As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoTitle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoAuthor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoKeywords As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoProducer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
End Class
