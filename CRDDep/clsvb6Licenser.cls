VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsvb6Licenser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Declare Sub CopyMemory Lib "msvbvm60" Alias "__vbaCopyBytes" ( _
    ByVal ByteLength As Long, _
    ByRef Destination As Any, _
    ByRef Source As Any)
    
Public Function GetLicenseInfo(ByVal license As String, ByVal fullName As String, _
    ByVal companyName As String) As String 'List(Of String)
        On Error GoTo Catch
        Dim num As Variant
        Dim licenseInfo As String

        license = Replace(license, "-", "")

        Dim factorise As String
        
        factorise = fullName & companyName

        Dim nVowels As Integer
        
        nVowels = HowManyVowels(factorise)

        Dim factor As Integer
        
        factor = (Len(factorise) - nVowels) - nVowels

        If factor < 0 Then factor = factor * -1

        num = Hex2Dec(license)

        num = num / factor
        
        licenseInfo = CStr(num)

        Dim custid As String
        
        custid = Mid(licenseInfo, 1, 7)
        
        Dim startDate As String
        
        startDate = Mid(licenseInfo, Len(custid) + 1, 8)

        GetLicenseInfo = custid & "," & startDate
        
        Exit Function
Catch:
        GetLicenseInfo = " , "
        
End Function
    
Private Function Hex2Dec(ByVal HexString As String) As Variant
    Dim X As Integer
    Dim BinStr As String
    
    If Left$(HexString, 2) Like "&[hH]" Then
        HexString = Mid$(HexString, 3)
    End If
  
    If Len(HexString) <= 23 Then
        Const BinValues = "0000000100100011" & _
                      "0100010101100111" & _
                      "1000100110101011" & _
                      "1100110111101111"
        For X = 1 To Len(HexString)
            BinStr = BinStr & Mid$(BinValues, _
               4 * Val("&h" & Mid$(HexString, X, 1)) + 1, 4)
        Next
        
        Hex2Dec = CDec(0)
    
        For X = 0 To Len(BinStr) - 1
            Hex2Dec = Hex2Dec + Val(Mid(BinStr, _
                Len(BinStr) - X, 1)) * 2 ^ X
        Next
    Else
        Hex2Dec = 0
    End If
End Function
Public Function CreateLicense(ByVal fullName As String, ByVal companyName As String, _
    ByVal custid As String, ByVal startDate As String) As String
        On Error GoTo Catch
        Dim sIn As String
        Dim license As String
        Dim num As Variant

        sIn = Trim(custid & startDate)

        num = CVar(sIn)

        Dim factorise As String
            
        factorise = fullName & companyName

        Dim nVowels As Integer
            
        nVowels = HowManyVowels(factorise)

        Dim factor As Integer
            
        factor = (Len(factorise) - nVowels) - nVowels

        If factor < 0 Then
            factor = factor * -1
        End If

        num = num * factor

        license = HexM(num)

        CreateLicense = license
            
        Exit Function
Catch:
        CreateLicense = "XXX"
    End Function
Public Function HowManyVowels(ByVal sIn As String)
    Dim I, X As Integer
    Dim char As String
    
    I = 0
    
    sIn = LCase(sIn)
    
    For X = 1 To Len(sIn)
        char = Mid(sIn, X, 1)
        
        If char = "a" Or char = "e" Or char = "i" Or char = "o" Or char = "u" Then
            I = I + 1
        End If
    Next
    
    HowManyVowels = I
End Function

Function HexM(ByVal Number As Double) As String
    ' Shortcut for simple solution
    If Number = 0 Then
        HexM = "0"
        Exit Function
    End If

    ' Keep track of whether result is negative
    Dim Negative As Boolean
    Negative = Number < 0

    ' Put Number in usable form (and discard fractional amount)
    Number = Abs(Int(Number))

    ' Calculate number of hex digits needed for result
    Dim Digits As Double
    Digits = Int(Log(Number) / Log(16)) + 1 ' That's Log16(Number)

    ' Make room for final value in string result
    HexM = Space$(Digits - CLng(Negative))

    ' Set up result string
    Dim P As Long
    P = StrPtr(HexM) ' P = address of first character in result string

    If Negative Then
        ' Add sign and skip ahead one character
        Mid(HexM, 1, 1) = "-"
        P = P + 2
    End If

    ' Convert...
    Dim I As Long
    Dim Digit As Long ' Value of current digit
    For I = Digits - 1 To 0 Step -1
        Digit = Int(Number / 16 ^ I)
        Number = Number - Digit * 16 ^ I

        Digit = 48 + Digit - 7 * (Digit > 9) ' Calc ASCII value of character
        CopyMemory 1, ByVal P, Digit ' Set character
        P = P + 2 ' Next character
    Next
End Function
