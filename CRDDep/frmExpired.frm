VERSION 5.00
Begin VB.Form frmExpired 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "SQL-RD"
   ClientHeight    =   1425
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7740
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   7740
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRegWiz 
      Caption         =   "&Activate SQL-RD"
      Height          =   375
      Left            =   3000
      TabIndex        =   4
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "&E&xit"
      Height          =   375
      Left            =   6120
      TabIndex        =   3
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton cmdWeb 
      Caption         =   "Purchase SQL-RD"
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   1455
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7455
      Begin VB.Label Label1 
         Caption         =   "Your 30 day trial of SQL-RD has now expired. Please select an option below..."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   7215
      End
   End
End
Attribute VB_Name = "frmExpired"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
    End
End Sub

Private Sub cmdRegWiz_Click()
    Dim sPath As String
    
    sPath = App.Path & "\link.exe"
    
    ShellEx sPath, essSW_SHOWNORMAL, "regwiz"
    
    End
End Sub

Private Sub cmdWeb_Click()
    ShellEx "http://www.christiansteven.com/purchase_sql-rd.htm", essSW_SHOWNORMAL
    End
End Sub

Public Function ShowExpiry()
    Me.Show 1
End Function
