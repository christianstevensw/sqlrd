VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsXML"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Public Function ReadXML(SettingName As String, DefaultValue As String, gConfigFile As String)
    On Error Resume Next
    Dim oDoc As DOMDocument40
    Dim oNodes As IXMLDOMNodeList
    Dim oNode As IXMLDOMNode
    
    Set oDoc = New DOMDocument40
    
    oDoc.Load (gConfigFile)
    
    Set oNodes = oDoc.getElementsByTagName("setting")
    
    For Each oNode In oNodes
        Dim Attr As String
        
        Attr = oNode.Attributes(0).nodeValue
        
        If LCase(Attr) = LCase(SettingName) Then
            DefaultValue = oNode.childNodes(0).Text
            
            Exit For
        End If
    Next
    
    ReadXML = DefaultValue
    
End Function

Public Function SaveXML(SettingName As String, SettingValue As Variant, gConfigFile As String) As Boolean
    On Error GoTo Hell
    Dim oDoc As DOMDocument40
    Dim oNodes As IXMLDOMNodeList
    Dim oNode As IXMLDOMNode
    
    Set oDoc = New DOMDocument40
    
    oDoc.Load (gConfigFile)
    
    Set oNodes = oDoc.getElementsByTagName("setting")
    
    If SettingExists(SettingName, oDoc) = True Then
        For Each oNode In oNodes
            Dim Attr As String
        
            Attr = oNode.Attributes(0).nodeValue
        
            If LCase(Attr) = LCase(SettingName) Then
                oNode.childNodes(0).Text = SettingValue
            
                Exit For
            End If
        Next
    Else
        CreateSetting oDoc, SettingName, SettingValue, gConfigFile
    End If
    
    oDoc.save gConfigFile
    
    SaveXML = True
    Exit Function
Hell:

    SaveXML = False
End Function

Private Function SettingExists(ByVal SettingName As String, ByVal oDoc As DOMDocument40) As Boolean
    Dim oNodes As IXMLDOMNodeList
    Dim oNode As IXMLDOMNode


    Set oNodes = oDoc.getElementsByTagName("setting")

    For Each oNode In oNodes
        Dim Attr As String

        Attr = oNode.Attributes(0).nodeValue

        If LCase(Attr) = LCase(SettingName) Then
            SettingExists = True
        End If
    Next
End Function

Private Function CreateSetting(oDoc As DOMDocument40, SettingName As String, _
InitialValue As Variant, gConfigFile As String) As Boolean
    Dim NewNode As IXMLDOMNode
    Dim NewAttr As IXMLDOMAttribute
    
    Set NewNode = oDoc.createElement("setting")
    
    Set NewAttr = oDoc.createAttribute("name")
    
    NewAttr.nodeValue = SettingName
    
    NewNode.Attributes.setNamedItem NewAttr
    
    Dim ValueNode As IXMLDOMNode
    
    Set ValueNode = oDoc.createElement("value")
    
    NewNode.appendChild ValueNode
    
    ValueNode.Text = InitialValue
    
    Dim ParentNode As IXMLDOMNodeList
    Dim oNode As IXMLDOMNode
    
    Set ParentNode = oDoc.getElementsByTagName("settings")
    
    For Each oNode In ParentNode
       oNode.appendChild NewNode
    Next
    
    oDoc.save gConfigFile
    
End Function



