Attribute VB_Name = "Global"
'for the registry
Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_USERS = &H80000003
Public Const HKEY_CURRENT_CONFIG = &H80000005
Public Const HKEY_DYN_DATA = &H80000006
Public Const REG_SZ = 1 'Unicode nul terminated string
Public Const REG_BINARY = 3 'Free form binary
Public Const REG_DWORD = 4 '32-bit number
Public Const ERROR_SUCCESS = 0&
Public TimeLeft As String
Public Subscription As Boolean
Public oArm As New clsArmadillo
Public oMain As New clsMain
'for armadillo
Declare Sub SECUREBEGIN Lib "armAccess.dll" ()
Declare Sub SECUREBEGIN_A Lib "armAccess.dll" ()
Declare Sub SECUREBEGIN_B Lib "armAccess.dll" ()
Declare Sub SECUREBEGIN_C Lib "armAccess.dll" ()
Declare Sub SECUREEND Lib "armAccess.dll" ()
Declare Sub SECUREEND_A Lib "armAccess.dll" ()
Declare Sub SECUREEND_B Lib "armAccess.dll" ()
Declare Sub SECUREEND_C Lib "armAccess.dll" ()


Public Declare Function RegCloseKey Lib "advapi32.dll" _
        (ByVal hKey As Long) As Long

Public Declare Function RegCreateKey Lib "advapi32.dll" _
        Alias "RegCreateKeyA" (ByVal hKey As Long, ByVal lpSubKey _
        As String, phkResult As Long) As Long

Public Declare Function RegDeleteKey Lib "advapi32.dll" _
        Alias "RegDeleteKeyA" (ByVal hKey As Long, ByVal lpSubKey _
        As String) As Long

Public Declare Function RegDeleteValue Lib "advapi32.dll" _
        Alias "RegDeleteValueA" (ByVal hKey As Long, ByVal _
        lpValueName As String) As Long

Public Declare Function RegOpenKey Lib "advapi32.dll" _
        Alias "RegOpenKeyA" (ByVal hKey As Long, ByVal lpSubKey _
        As String, phkResult As Long) As Long

Public Declare Function RegQueryValueEx Lib "advapi32.dll" _
        Alias "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName _
        As String, ByVal lpReserved As Long, lpType As Long, lpData _
        As Any, lpcbData As Long) As Long

Public Declare Function RegSetValueEx Lib "advapi32.dll" _
        Alias "RegSetValueExA" (ByVal hKey As Long, ByVal _
        lpValueName As String, ByVal Reserved As Long, ByVal _
        dwType As Long, lpData As Any, ByVal cbData As Long) As Long
        
        'Shell Execute
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
        (ByVal hwnd As Long, ByVal lpOperation As String, _
        ByVal lpFile As String, ByVal lpParameters As String, _
        ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long

Private Declare Function ShellExecuteForExplore Lib "shell32.dll" Alias "ShellExecuteA" _
        (ByVal hwnd As Long, ByVal lpOperation As String, _
        ByVal lpFile As String, lpParameters As Any, _
        lpDirectory As Any, ByVal nShowCmd As Long) As Long
        
Public Enum EShellShowConstants
    essSW_HIDE = 0
    essSW_MAXIMIZE = 3
    essSW_MINIMIZE = 6
    essSW_SHOWMAXIMIZED = 3
    essSW_SHOWMINIMIZED = 2
    essSW_SHOWNORMAL = 1
    essSW_SHOWNOACTIVATE = 4
    essSW_SHOWNA = 8
    essSW_SHOWMINNOACTIVE = 7
    essSW_SHOWDEFAULT = 10
    essSW_RESTORE = 9
    essSW_SHOW = 5
End Enum

Private Const ERROR_FILE_NOT_FOUND = 2&
Private Const ERROR_PATH_NOT_FOUND = 3&
Private Const ERROR_BAD_FORMAT = 11&
Private Const SE_ERR_ACCESSDENIED = 5        ' access denied
Private Const SE_ERR_ASSOCINCOMPLETE = 27
Private Const SE_ERR_DDEBUSY = 30
Private Const SE_ERR_DDEFAIL = 29
Private Const SE_ERR_DDETIMEOUT = 28
Private Const SE_ERR_DLLNOTFOUND = 32
Private Const SE_ERR_FNF = 2                ' file not found
Private Const SE_ERR_NOASSOC = 31
Private Const SE_ERR_PNF = 3                ' path not found
Private Const SE_ERR_OOM = 8                ' out of memory
Private Const SE_ERR_SHARE = 26

Public gConfigFile As String
Public oXML As New clsXML

Public Function Encrypt(PlainText, WatchWord)
    'Other Dim Things
    Dim intKey
    Dim intX
    Dim strChar
    Dim EncText
    
    'all Mighty visual Basic lines give us the Key
    For intX = 1 To Len(WatchWord)
        intKey = intKey + Asc(Mid(WatchWord, intX)) / 20
    Next
    
    'the brother of decrypt function.but it earns its
    'living by encrypting the data
    For intX = 1 To Len(PlainText)
        strChar = Mid(PlainText, intX, 1)
        EncText = EncText & Chr(Asc(strChar) Xor intKey)
    Next
    
    'the ever so generous functios should return something.
    Encrypt = EncText
End Function




Public Function SaveSettingString(hKey As Long, strPath _
        As String, strValue As String, strData As String, Optional ComputerName As String) As Boolean
    
    On Error Resume Next
    
    Dim hCurKey As Long
    Dim lRegResult As Long
    
    
    lRegResult = RegCreateKey(hKey, strPath, hCurKey)
        
    lRegResult = RegSetValueEx(hCurKey, strValue, 0, REG_SZ, _
        ByVal strData, Len(strData))
        
    If lRegResult <> ERROR_SUCCESS Then
        'there is a problem
    End If
        
    lRegResult = RegCloseKey(hCurKey)
    
    
    SaveSettingString = True
End Function

Public Sub Main()
    On Error GoTo Catch
    Dim sKey As String
    Dim nEdition As Integer
    Dim configString As String
    Dim fingerPrint As String

    gConfigFile = App.Path & "\sqlrdlive.config"
        
    If Command = "regwiz" Then
        frmRegWiz.Show
    ElseIf Command = "unregwiz" Then
        frmUnReg.Show
    ElseIf Command <> "register" Then
    
        App.TaskVisible = False
            
        If IsExpired = True Then
            frmExpired.Show 1
            End
        End If
    
        nEdition = oMain.GetEdition
        TimeLeft = oMain.GetExpiry
        fingerPrint = "" 'oArm.GetHardwareFingerPrint
        
        Subscription = oMain.IsHP
        
        Dim sComplete As String
        
        sComplete = nEdition & "|" & TimeLeft & "|" & CInt(Subscription) & "|" & fingerPrint
        
        WriteMsg sComplete, App.Path & "\edid.dll"
        End
    End If
    Exit Sub
Catch:
   MsgBox Err.Description
End Sub
Public Sub WriteMsg(Msg As String, Path As String)
    Dim FileName As String
    F = FreeFile
    Open Path For Output As #F
    Print #F, Msg
    Close #F
End Sub
Public Function IsExpired() As Boolean
    Dim sValue As String
    
    sValue = oArm.GetEnvironString("EXPIRED")
    
    If sValue = "" Then
        IsExpired = False
    Else
        IsExpired = True
    End If

End Function
'Public Function GetEdition() As Integer
'    Dim I As Integer
'
'    I = 0
'
'    SECUREBEGIN
'        I = I + 1
'    SECUREEND
'
'    SECUREBEGIN_A
'        I = I + 2
'    SECUREEND_A
'
'    SECUREBEGIN_B
'        I = I + 3
'    SECUREEND_B
'
'    SECUREBEGIN_C
'        I = I + 4
'    SECUREEND_C
'
'    GetEdition = I
'
'End Function

'Public Sub IsHP()
'    Dim sValue As String
'    Dim sKey As String
'
'    sKey = "Software\ChristianSteven\CRD"
'
'    sValue = oArm.GetEnvironString("_CRDHP")
'
'    If sValue = "" Then sValue = "0"
'
'    oXML.SaveXML "HyperProc", sValue, gConfigFile
'    SaveSettingString HKEY_LOCAL_MACHINE, sKey, "HyperProc", sValue
'
'    Subscription = sValue
'End Sub
'Public Sub GetExpiry()
'    TimeLeft = "0"
'    TimeLeft = oArm.GetEnvironString("DAYSLEFT")
'
'    If TimeLeft = "" Then TimeLeft = "0"
'
'End Sub

Public Function GetSettingString(hKey As Long, _
        strPath As String, strValue As String, Optional _
        Default As String, Optional ComputerName As String = "") As String
    
    Dim hCurKey As Long
    Dim lResult As Long
    Dim lValueType As Long
    Dim strBuffer As String
    Dim lDataBufferSize As Long
    Dim intZeroPos As Integer
    Dim lRegResult As Long
    Dim temp As String
    
    If IsRemote = False And (ComputerName = GetPCName Or ComputerName = "") Then
        If Not IsEmpty(Default) Then
            GetSettingString = Default
        Else
            GetSettingString = ""
        End If
        
        lRegResult = RegOpenKey(hKey, strPath, hCurKey)
        lRegResult = RegQueryValueEx(hCurKey, strValue, 0&, _
            lValueType, ByVal 0&, lDataBufferSize)
        
        If lRegResult = ERROR_SUCCESS Then
            
            If lValueType = REG_SZ Then
                
                strBuffer = String(lDataBufferSize, " ")
                lResult = RegQueryValueEx(hCurKey, strValue, 0&, 0&, _
                    ByVal strBuffer, lDataBufferSize)
                
                intZeroPos = InStr(strBuffer, Chr$(0))
                If intZeroPos > 0 Then
                    GetSettingString = Left$(strBuffer, intZeroPos - 1)
                Else
                    GetSettingString = strBuffer
                End If
                
            End If
        Else
            'there is a problem
        End If
        
        lRegResult = RegCloseKey(hCurKey)
    Else
        'GetSettingString = GetValue(hKey, strPath, strValue, ComputerName)
    End If
    
    If GetSettingString = "~blank~" Then GetSettingString = ""
End Function

Public Function ShellEx(ByVal sFile As String, _
            Optional ByVal eShowCmd As EShellShowConstants = essSW_SHOWDEFAULT, _
            Optional ByVal sParameters As String = "", _
            Optional ByVal sDefaultDir As String = "", _
            Optional sOperation As String = "open", _
            Optional Owner As Long = 0 _
            ) As Boolean
        Dim lR As Long
        Dim lErr As Long, sErr As Long
1       If (InStr(UCase$(sFile), ".EXE") <> 0) Then
2           eShowCmd = 0
        End If
        On Error Resume Next
3       If (sParameters = "") And (sDefaultDir = "") Then
4           lR = ShellExecuteForExplore(Owner, sOperation, sFile, 0, 0, essSW_SHOWNORMAL)
5       Else
6           lR = ShellExecute(Owner, sOperation, sFile, sParameters, sDefaultDir, eShowCmd)
        End If
7       If (lR < 0) Or (lR > 32) Then
8           ShellEx = True
9       Else
            ' raise an appropriate error:
10          lErr = vbObjectError + 1048 + lR
            Select Case lR
            Case 0
11              lErr = 7: sErr = "Out of memory"
12              shellErr = "Out of memory"
            Case ERROR_FILE_NOT_FOUND
13              lErr = 53: sErr = "File not found"
14              shellErr = "File not found"
            Case ERROR_PATH_NOT_FOUND
15              lErr = 76: sErr = "Path not found"
16              shellErr = "Path not found"
            Case ERROR_BAD_FORMAT
17              sErr = "The executable file is invalid or corrupt"
18              shellErr = "The executable is invalid or corrupt"
            Case SE_ERR_ACCESSDENIED
19              lErr = 75: sErr = "Path/file access error"
20              shellErr = "Path/file access error"
            Case SE_ERR_ASSOCINCOMPLETE
21              sErr = "This file type does not have a valid file association."
22              shellErr = "This file does not have a valid association"
            Case SE_ERR_DDEBUSY
23              lErr = 285: sErr = "The file could not be opened because the target application is busy. Please try again in a moment."
24              shellErr = "The file could not be opened because the target application is busy. Please try again in a moment."
            Case SE_ERR_DDEFAIL
25              lErr = 285: sErr = "The file could not be opened because the DDE transaction failed. Please try again in a moment."
26              shellErr = "The file could not be opened because the DDE transaction failed. Please try again in a moment."
            Case SE_ERR_DDETIMEOUT
27              lErr = 286: sErr = "The file could not be opened due to time out. Please try again in a moment."
28              shellErr = "The file could not be opened due to a time out. Please try again in a moment"
            Case SE_ERR_DLLNOTFOUND
29              lErr = 48: sErr = "The specified dynamic-link library was not found."
30              shellErr = "The specified dynamic-link library was not found."
            Case SE_ERR_FNF
31              lErr = 53: sErr = "File not found"
32              shellErr = "File not found"
            Case SE_ERR_NOASSOC
33              sErr = "No application is associated with this file type."
34              shellErr = "No application is associated with this file type."
            Case SE_ERR_OOM
35              lErr = 7: sErr = "Out of memory"
36              shellErr = "Out of memory"
            Case SE_ERR_PNF
37              lErr = 76: sErr = "Path not found"
38              shellErr = "Path not found"
            Case SE_ERR_SHARE
39              lErr = 75: sErr = "A sharing violation occurred."
40              shellErr = "A sharing violation occured"
            Case Else
41              sErr = "An error occurred occurred whilst trying to open or print the selected file."
42              shellErr = "An error occurred occurred whilst trying to open or print the selected file."
            End Select
        
            'Err.Raise lErr, , App.Title & ".GShell", sErr
        
43          ShellEx = False
        End If
    
    End Function
