VERSION 5.00
Begin VB.Form frmRegWiz 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Registration & Activation Wizard"
   ClientHeight    =   5670
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7920
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmRegWiz.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5670
   ScaleWidth      =   7920
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   8
      Left            =   120
      TabIndex        =   54
      Top             =   1320
      Width           =   7695
      Begin VB.CommandButton cmdActivate 
         Caption         =   "&Activate"
         Height          =   375
         Left            =   3900
         TabIndex        =   59
         Top             =   1440
         Width           =   2175
      End
      Begin VB.TextBox txtKey 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   2280
         TabIndex        =   58
         Top             =   840
         Width           =   5415
      End
      Begin VB.TextBox txtName 
         BackColor       =   &H00FFFFFF&
         Height          =   315
         Left            =   2280
         TabIndex        =   57
         Top             =   360
         Width           =   5415
      End
      Begin VB.Label Label28 
         Caption         =   "Activation Key"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   56
         Top             =   870
         Width           =   2055
      End
      Begin VB.Label Label27 
         Caption         =   "Name"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   55
         Top             =   390
         Width           =   1095
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   9
      Left            =   120
      TabIndex        =   60
      Top             =   1320
      Width           =   7695
      Begin VB.CheckBox chkRestart 
         Caption         =   "Restart SQL-RD and Scheduler"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   63
         Top             =   1800
         Value           =   1  'Checked
         Width           =   7695
      End
      Begin VB.Label Label29 
         Caption         =   $"frmRegWiz.frx":6852
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1335
         Left            =   0
         TabIndex        =   61
         Top             =   240
         Width           =   7335
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   7
      Left            =   120
      TabIndex        =   39
      Top             =   1320
      Width           =   7695
      Begin VB.CommandButton cmdCopy2 
         Height          =   315
         Left            =   5040
         Picture         =   "frmRegWiz.frx":69C0
         Style           =   1  'Graphical
         TabIndex        =   45
         ToolTipText     =   "Copy to clipboard"
         Top             =   600
         Width           =   615
      End
      Begin VB.CommandButton cmdCopy1 
         Height          =   315
         Left            =   5040
         Picture         =   "frmRegWiz.frx":D212
         Style           =   1  'Graphical
         TabIndex        =   43
         ToolTipText     =   "Copy to clipboard"
         Top             =   120
         Width           =   615
      End
      Begin VB.TextBox txtFingerPrint 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   44
         Top             =   600
         Width           =   2655
      End
      Begin VB.TextBox txtCustNo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   42
         Top             =   120
         Width           =   2655
      End
      Begin VB.Label Label26 
         Caption         =   "2.  Click the provided link to view the Activation Request form."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   53
         Top             =   2160
         Width           =   7695
      End
      Begin VB.Label Label25 
         Caption         =   "3.  In the ""Customer Number"" field, enter your Customer Number (shown above)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   52
         Top             =   2400
         Width           =   7695
      End
      Begin VB.Label Label24 
         Caption         =   "4.  In the ""Hardware Fingerprint"" field, enter the Hardware Fingerprint (shown above)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   51
         Top             =   2640
         Width           =   7695
      End
      Begin VB.Label Label23 
         Caption         =   "5.  Read the notes and submit the form."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   50
         Top             =   2880
         Width           =   7695
      End
      Begin VB.Label Label22 
         Caption         =   "6.  When you have received your activation code, click ""next"" to enter the details."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   49
         Top             =   3120
         Width           =   7695
      End
      Begin VB.Label Label21 
         Caption         =   "1.  Visit http://www.christiansteven.com/members.htm and enter the Member Area."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   48
         Top             =   1920
         Width           =   7695
      End
      Begin VB.Label Label20 
         Caption         =   "How to Activate Manually"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   47
         Top             =   1560
         Width           =   7695
      End
      Begin VB.Label Label19 
         Caption         =   "Sorry, we cannot establish a connection with the server. "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   46
         Top             =   1200
         Width           =   7695
      End
      Begin VB.Label Label18 
         Caption         =   "Hardware Finger-Print"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   41
         Top             =   630
         Width           =   2055
      End
      Begin VB.Label Label17 
         Caption         =   "Customer #"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   40
         Top             =   150
         Width           =   1095
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   3615
      Index           =   6
      Left            =   120
      TabIndex        =   35
      Top             =   1320
      Width           =   7695
      Begin VB.Label Label16 
         Caption         =   $"frmRegWiz.frx":13A64
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   975
         Left            =   0
         TabIndex        =   38
         Top             =   1800
         Width           =   7695
      End
      Begin VB.Label Label15 
         Caption         =   $"frmRegWiz.frx":13B9A
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   735
         Left            =   0
         TabIndex        =   37
         Top             =   840
         Width           =   7695
      End
      Begin VB.Label Label14 
         Caption         =   "You will now be required to activate this installation.  To do this, you will need the Member Area username and password."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   36
         Top             =   120
         Width           =   7695
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   5
      Left            =   120
      TabIndex        =   30
      Top             =   1320
      Width           =   7695
      Begin VB.TextBox txtLastName 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   2400
         TabIndex        =   25
         Top             =   1410
         Width           =   4095
      End
      Begin VB.TextBox txtCustomerID 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   2400
         TabIndex        =   28
         Top             =   2520
         Width           =   4095
      End
      Begin VB.CommandButton cmdReg 
         Caption         =   "Register"
         Height          =   375
         Left            =   2400
         TabIndex        =   29
         Top             =   2880
         Width           =   4095
      End
      Begin VB.TextBox txtFirstName 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   2400
         TabIndex        =   24
         Top             =   1050
         Width           =   4095
      End
      Begin VB.TextBox txtCompany 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Left            =   2400
         TabIndex        =   26
         Top             =   1800
         Width           =   4095
      End
      Begin VB.TextBox txtRegNo 
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00FF0000&
         Height          =   315
         Index           =   0
         Left            =   2400
         TabIndex        =   27
         Top             =   2160
         Width           =   4095
      End
      Begin VB.Label Label31 
         BackStyle       =   0  'Transparent
         Caption         =   "Last Name"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   65
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label30 
         BackStyle       =   0  'Transparent
         Caption         =   "Customer #"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   64
         Top             =   2550
         Width           =   975
      End
      Begin VB.Label Label13 
         Caption         =   "Please enter your registration details below"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   615
         Left            =   0
         TabIndex        =   34
         Top             =   120
         Width           =   7695
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "First Name"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   33
         Top             =   1080
         Width           =   1095
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "Company Name"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   32
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "License #"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   31
         Top             =   2190
         Width           =   1335
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   4
      Left            =   120
      TabIndex        =   20
      Top             =   1320
      Width           =   7695
      Begin VB.Label Label9 
         Caption         =   $"frmRegWiz.frx":13C72
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   855
         Left            =   0
         TabIndex        =   23
         Top             =   1320
         Width           =   7695
      End
      Begin VB.Label Label8 
         Caption         =   $"frmRegWiz.frx":13D73
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   855
         Left            =   0
         TabIndex        =   22
         Top             =   480
         Width           =   7695
      End
      Begin VB.Label Label7 
         Caption         =   "You will now be required to enter your registration details.  "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   21
         Top             =   120
         Width           =   7695
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   3
      Left            =   120
      TabIndex        =   17
      Top             =   1320
      Width           =   7695
      Begin VB.CheckBox chkTerm 
         Caption         =   "I am NOT using Terminal Services or Windows XP Remote Control to control the PC."
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   19
         Top             =   1800
         Width           =   7695
      End
      Begin VB.Label Label6 
         Caption         =   $"frmRegWiz.frx":13E4B
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1335
         Left            =   0
         TabIndex        =   18
         Top             =   120
         Width           =   7695
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Height          =   3615
      Index           =   2
      Left            =   120
      TabIndex        =   14
      Top             =   1320
      Width           =   7695
      Begin VB.CheckBox chkAdmin 
         Caption         =   "I am logged into this PC as the LOCAL Administrator"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   16
         Top             =   1680
         Width           =   7695
      End
      Begin VB.Label Label5 
         Caption         =   $"frmRegWiz.frx":13FF2
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1335
         Left            =   0
         TabIndex        =   15
         Top             =   120
         Width           =   7695
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   3615
      Index           =   1
      Left            =   120
      TabIndex        =   11
      Top             =   1320
      Width           =   7695
      Begin VB.CheckBox chkEmail 
         Caption         =   "I have ensured that my email system can receive all emails from ""christiansteven.com"" irrespective of content"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   13
         Top             =   1320
         Width           =   7695
      End
      Begin VB.Label Label4 
         Caption         =   $"frmRegWiz.frx":141AD
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   0
         TabIndex        =   12
         Top             =   120
         Width           =   7695
      End
   End
   Begin VB.Frame Step 
      BorderStyle     =   0  'None
      Caption         =   "Frame4"
      Height          =   3615
      Index           =   0
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   7695
      Begin VB.Label Label3 
         Caption         =   $"frmRegWiz.frx":1430F
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   0
         TabIndex        =   10
         Top             =   1440
         Width           =   7695
      End
      Begin VB.Label Label2 
         Caption         =   $"frmRegWiz.frx":144C3
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   495
         Left            =   0
         TabIndex        =   9
         Top             =   840
         Width           =   7695
      End
      Begin VB.Label Label1 
         Caption         =   $"frmRegWiz.frx":1457C
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   735
         Left            =   0
         TabIndex        =   8
         Top             =   0
         Width           =   7695
      End
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Height          =   375
      Left            =   6600
      TabIndex        =   6
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "&Finish"
      Height          =   375
      Left            =   6600
      TabIndex        =   62
      Top             =   5160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3720
      TabIndex        =   5
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   5160
      TabIndex        =   4
      Top             =   5160
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      Height          =   135
      Left            =   0
      TabIndex        =   3
      Top             =   4920
      Width           =   7935
   End
   Begin VB.Frame Frame2 
      BackColor       =   &H00FFFFFF&
      Height          =   135
      Left            =   0
      TabIndex        =   1
      Top             =   1080
      Width           =   7935
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7935
      Begin VB.Label lblMessage 
         BackStyle       =   0  'Transparent
         Caption         =   "Message"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   1080
         TabIndex        =   2
         Top             =   390
         Width           =   6735
      End
      Begin VB.Image Image1 
         Height          =   915
         Left            =   0
         Picture         =   "frmRegWiz.frx":1466A
         Top             =   120
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmRegWiz"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim nStep As Integer
Dim sMsg(9) As String
Dim sKey As String
Dim Skip As Boolean

Private Sub chkAdmin_Click()
    cmdNext.Enabled = chkAdmin.Value
End Sub

Private Sub chkEmail_Click()
    cmdNext.Enabled = chkEmail.Value
End Sub

Private Sub chkTerm_Click()
    cmdNext.Enabled = chkTerm.Value
End Sub

Private Sub cmdActivate_Click()

    If oArm.InstallKey(txtName.Text, txtKey.Text) = False Then
        MsgBox "The name/key you have provided does not appear to be valid. Please try again.", _
            vbExclamation, "Registration & Activation Wizard"
        
        cmdNext.Enabled = False
    Else
        cmdNext.Enabled = True
    End If
End Sub

Private Sub cmdBack_Click()
    Select Case nStep
        Case 9
            cmdNext.Visible = True
            cmdFinish.Visible = False
            cmdCancel.Enabled = True
            nStep = nStep - 1
        Case 8
            If Skip = True Then
                nStep = nStep - 2
            Else
                nStep = nStep - 1
            End If
            
            cmdNext.Enabled = True
        Case 7
            nStep = nStep - 1
        Case 6
            nStep = nStep - 1
            cmdNext.Enabled = True
        Case 5
            nStep = nStep - 1
            cmdNext.Enabled = True
        Case 4
            nStep = nStep - 1
            cmdNext.Enabled = chkTerm.Value
        Case 3
            nStep = nStep - 1
            cmdNext.Enabled = chkAdmin.Value
        Case 2
            nStep = nStep - 1
            cmdNext.Enabled = chkEmail.Value
        Case 1
            nStep = nStep - 1
            cmdBack.Enabled = False
            cmdNext.Enabled = True
    End Select
            
    lblMessage.Caption = sMsg(nStep)
    
    Step(nStep).ZOrder
    
End Sub

Private Sub cmdCancel_Click()
    If MsgBox("Exit the Registration & Activation Wizard?", vbYesNo, "Registration & Activation") = vbYes Then
        Unload Me
    End If
End Sub

Private Sub cmdCopy1_Click()
    On Error Resume Next
    Clipboard.Clear
    Clipboard.SetText txtCustNo.Text
End Sub

Private Sub cmdCopy2_Click()
    On Error Resume Next
    Clipboard.Clear
    Clipboard.SetText txtFingerPrint.Text
End Sub

Private Sub cmdFinish_Click()
    On Error Resume Next
    
    Kill App.Path & "\link.dll"
    
    If chkRestart.Value = 1 Then
        Dim sPath As String
    
        sPath = Chr(34) & App.Path & "\sqlrd.exe" & Chr(34)
    
        Shell sPath
    End If
    
    Unload Me
End Sub

Private Sub cmdNext_Click()
    
    Select Case nStep
        Case 0
            cmdNext.Enabled = chkEmail.Value
            cmdBack.Enabled = True
            nStep = nStep + 1
        Case 1
            cmdNext.Enabled = chkAdmin.Value
            nStep = nStep + 1
        Case 2
            cmdNext.Enabled = chkTerm.Value
            nStep = nStep + 1
        Case 3
            nStep = nStep + 1
        Case 4
            cmdNext.Enabled = False
            nStep = nStep + 1
        Case 5
            nStep = nStep + 1
        Case 6
            If frmActivate.GetActivationCode(txtFingerPrint.Text, txtCustNo.Text) = True Then
                nStep = nStep + 2
                cmdNext.Enabled = False
                Skip = True
            Else
                nStep = nStep + 1
                Skip = False
            End If
            
            Screen.MousePointer = vbDefault
        Case 7
            nStep = nStep + 1
            cmdNext.Enabled = False
        Case 8
            cmdFinish.Visible = True
            cmdNext.Visible = False
            cmdCancel.Enabled = False
            nStep = nStep + 1
    End Select
    
    lblMessage.Caption = sMsg(nStep)
    
    Step(nStep).ZOrder

End Sub


Private Sub cmdReg_Click()
On Error GoTo Catch

1      gConfigFile = App.Path & "\sqlrdlive.config"

2    If Len(txtCustomerID.Text) = 0 Then
        MsgBox "Please enter your customer number", vbExclamation, "Registration & Activation Wizard"
        txtCustomerID.SetFocus
        Exit Sub
    End If

3    Dim fullName As String
4    Dim m_licenser As New clsvb6Licenser
    
5    fullName = txtFirstName.Text & txtLastName.Text
        
6    Dim licenseInfo As String
7    licenseInfo = m_licenser.GetLicenseInfo(txtRegNo(0).Text, fullName, txtCompany.Text)

8    Dim nStart As Integer
    
9    nStart = Len(licenseInfo) - 8
    
10    Dim nEnd As Integer
    
11    nEnd = Len(licenseInfo)

12    Dim info
    
13    info = Split(licenseInfo, ",")

14    Dim supportDate As String
    
15    supportDate = info(1)

16    Dim license As String
    
18    license = m_licenser.CreateLicense(fullName, txtCompany.Text, txtCustomerID.Text, supportDate)

        Dim temp As String
        
        temp = Replace(txtRegNo(0).Text, "-", "")
        
19    If Left(temp, 8) <> Left(license, 8) Then
        MsgBox "The license information you have provided is incorrect. Please check to make sure that all fields have been entered correctly.", _
        vbExclamation, "Registration & Activation Wizard"
        
20        Me.txtRegNo(0).SetFocus
21        Me.cmdNext.Enabled = False
        
22        Exit Sub
    Else
23        cmdNext.Enabled = True
        
24        MsgBox "The license number you have entered is CORRECT and has been saved", vbExclamation, _
        "Registration & Activation Wizard"
        
25        Dim XML As New clsXML
        
26        With XML
27            .SaveXML "RegNo", txtRegNo(0).Text, gConfigFile
28            .SaveXML "RegUser", txtFirstName.Text & " " & txtLastName.Text, gConfigFile
29            .SaveXML "RegFirstName", txtFirstName.Text, gConfigFile
30            .SaveXML "RegLastName", txtLastName.Text, gConfigFile
31            .SaveXML "RegCo", txtCompany.Text, gConfigFile
32            .SaveXML "CustNo", txtCustomerID.Text, gConfigFile
33        End With
    End If
    Exit Sub
Catch:
    
End Sub

Private Sub Form_Load()
    gConfigFile = App.Path & "\sqlrdlive.config"

    sKey = "Software\ChristianSteven\SQL-RD"
    
    nStep = 0
    sMsg(0) = "Registration && Activation Wizard"
    sMsg(1) = "Email Check"
    sMsg(2) = "Local Administrator Check"
    sMsg(3) = "Terminal Services Check"
    sMsg(4) = "Registration Details"
    sMsg(5) = "Enter Registration Details"
    sMsg(6) = "Activation"
    sMsg(7) = "Manual Activation"
    sMsg(8) = "Enter Activation Details"
    sMsg(9) = "Finish"
    
    lblMessage.Caption = sMsg(nStep)
    Step(nStep).ZOrder
    
    txtCustNo.Text = oXML.ReadXML("CustNo", "", gConfigFile)
    
    'txtCustNo.Text = GetSettingString(HKEY_LOCAL_MACHINE, sKey, "CustNo", "")
    
    txtCustomerID.Text = txtCustNo.Text
    
    Dim sReg As String
    Dim RegNo() As String
    Dim I As Integer
    Dim XML As New clsXML
    
    sReg = XML.ReadXML("RegNo", "", gConfigFile)
    
    If Len(sReg) > 0 Then
        txtRegNo(0).Text = sReg
        txtFirstName.Text = XML.ReadXML("RegFirstName", "", gConfigFile) 'GetSettingString(HKEY_LOCAL_MACHINE, sKey, "RegUser", "")
        txtLastName.Text = XML.ReadXML("RegLastName", "", gConfigFile)
        txtCompany.Text = XML.ReadXML("RegCo", "", gConfigFile)
        txtCustomerID.Text = XML.ReadXML("CustNo", "", gConfigFile)
    End If
    
    txtFingerPrint.Text = oArm.GetHardwareFingerPrint
    
    XML.SaveXML "HardwareFingerprint", txtFingerPrint.Text, gConfigFile
    
    cmdNext.ZOrder
    
    On Error Resume Next
    Kill App.Path & "\link.dll"
End Sub

Public Function iEnc(PlainText As String, WatchWord As String)
    Dim encFac As Long
    Dim curVal As Long
    Dim I As Integer
    
    For I = 1 To Len(WatchWord)
        encFac = encFac + Asc(Mid(WatchWord, I, 1))
    Next
    
    For I = 1 To Len(PlainText)
        curVal = curVal + Asc(Mid(PlainText, I, 1)) * encFac
    Next
    
    iEnc = curVal
End Function

Private Sub Form_Unload(Cancel As Integer)
    On Error Resume Next
    End
End Sub

Private Sub txtCustomerID_Change()
    txtCustNo.Text = txtCustomerID.Text
End Sub

Private Sub txtRegNo_KeyPress(Index As Integer, KeyAscii As Integer)
     If KeyAscii <> 8 And KeyAscii <> 45 Then
        If Len(txtRegNo(0).Text) = 4 Or Len(txtRegNo(0).Text) = 9 Then
            txtRegNo(0).SelText = "-"
        End If
    End If
End Sub

Private Sub txtRegNo_LostFocus(Index As Integer)
    txtRegNo(0).Text = UCase(txtRegNo(0).Text)
End Sub
