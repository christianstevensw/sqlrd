VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmActivate 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Get Activation Code"
   ClientHeight    =   10785
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   14025
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmActivate.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10785
   ScaleWidth      =   14025
   StartUpPosition =   2  'CenterScreen
   Begin MSWinsockLib.Winsock sock 
      Left            =   0
      Top             =   0
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "&Finish"
      Height          =   375
      Left            =   6420
      TabIndex        =   1
      Top             =   10320
      Width           =   1215
   End
   Begin SHDocVwCtl.WebBrowser wb 
      Height          =   10095
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   13815
      ExtentX         =   24368
      ExtentY         =   17806
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   1
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   "http:///"
   End
End
Attribute VB_Name = "frmActivate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdFinish_Click()
    Me.Hide
End Sub

Private Function CheckNetWork() As Boolean
    Dim sngTimer As Single
    Dim Timer As Single
    
    Screen.MousePointer = vbHourglass
    
    Timer = 30

    With sock
        .Close
        .RemoteHost = "christiansteven.fileburst.com"
        .RemotePort = 21
        .LocalPort = 0
        .Connect
        
        Do
            DoEvents
            sngTimer = sngTimer + 1
        Loop Until .State = sckConnected Or .State = 9

        If .State = sckConnected Then
            .Close
            CheckNetWork = True
        Else
            .Close
            CheckNetWork = False
        End If
    End With
    
    Screen.MousePointer = vbDefault
End Function
Public Function GetDeactivationCode(sUninstallCode As String, sCustNo As String) As Boolean
    On Error GoTo Hell
    
    If CheckNetWork = False Then
        GoTo Hell
    End If
        
    Screen.MousePointer = vbHourglass
    
    wb.Navigate "http://www.christiansteven.com/sql-rd/members/deactivate.htm"
    
    Do
        DoEvents
    Loop Until wb.Busy = False
    
    Dim oBj
    
    For Each oBj In wb.Document.All.tags("input")
        If oBj.name = "custid" Then
            oBj.Value = sCustNo
        ElseIf oBj.name = "Deac_Code" Then
            oBj.Value = sUninstallCode
        End If
    Next
       
    Screen.MousePointer = vbDefault
    
    Me.Show 1
    
    GetDeactivationCode = True
    
    Unload Me
    
    Exit Function
    
Hell:
    Screen.MousePointer = vbDefault
    GetDeactivationCode = False
End Function
Public Function GetActivationCode(sFingerPrint As String, sCustNo As String) As Boolean
    On Error GoTo Hell
    
    If CheckNetWork = False Then
        GoTo Hell
    End If
    
    Screen.MousePointer = vbHourglass
    
    wb.Navigate "http://www.christiansteven.com/sql-rd/members/activate.htm"
    
    Do
        DoEvents
    Loop Until wb.Busy = False
    
    
    Dim oBj
    
    For Each oBj In wb.Document.All.tags("input")
        If oBj.name = "Fingerprint" Then
            oBj.Value = sFingerPrint
        ElseIf oBj.name = "custid" Then
            oBj.Value = sCustNo
        End If
    Next
    
    Screen.MousePointer = vbDefault
    
    Me.Show 1

    GetActivationCode = True
    
    Unload Me
    
    Exit Function
Hell:
    Screen.MousePointer = vbDefault
    GetActivationCode = False
End Function



