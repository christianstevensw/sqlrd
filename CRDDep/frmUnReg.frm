VERSION 5.00
Begin VB.Form frmUnReg 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "License Transfer Wizard"
   ClientHeight    =   5715
   ClientLeft      =   45
   ClientTop       =   360
   ClientWidth     =   7665
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmUnReg.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5715
   ScaleWidth      =   7665
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Step4 
      BorderStyle     =   0  'None
      Height          =   3495
      Left            =   120
      TabIndex        =   20
      Top             =   1200
      Visible         =   0   'False
      Width           =   7455
      Begin VB.Label Label3 
         Caption         =   $"frmUnReg.frx":6852
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   855
         Left            =   0
         TabIndex        =   22
         Top             =   1080
         Width           =   7335
      End
      Begin VB.Label Label2 
         Caption         =   $"frmUnReg.frx":68FE
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   855
         Left            =   0
         TabIndex        =   21
         Top             =   120
         Width           =   7335
      End
   End
   Begin VB.Frame Step3 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3495
      Left            =   120
      TabIndex        =   9
      Top             =   1200
      Visible         =   0   'False
      Width           =   7455
      Begin VB.TextBox txtCustNo 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   120
         Width           =   2655
      End
      Begin VB.TextBox txtUninstallKey 
         BackColor       =   &H8000000F&
         Height          =   315
         Left            =   2280
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   600
         Width           =   2655
      End
      Begin VB.Label Label17 
         Caption         =   "Customer #"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   19
         Top             =   150
         Width           =   1095
      End
      Begin VB.Label Label18 
         Caption         =   "Deactivation Code"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   18
         Top             =   630
         Width           =   2055
      End
      Begin VB.Label Label19 
         Caption         =   "Sorry, we cannot establish a connection with the server. "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   15
         Top             =   1200
         Width           =   7695
      End
      Begin VB.Label Label20 
         Caption         =   "How to Deactivate Manually"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   14
         Top             =   1560
         Width           =   7695
      End
      Begin VB.Label Label21 
         Caption         =   "1.  Visit http://www.christiansteven.com/sql-rd/members/deactivate.htm"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   13
         Top             =   1920
         Width           =   7695
      End
      Begin VB.Label Label23 
         Caption         =   "4.  Read the notes and click ""Deactivate"" to submit the form."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   12
         Top             =   2640
         Width           =   7695
      End
      Begin VB.Label Label24 
         Caption         =   "3.  In the ""Deactivation Code"" field, enter the Hardware Fingerprint (shown above)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   11
         Top             =   2400
         Width           =   7695
      End
      Begin VB.Label Label25 
         Caption         =   "2.  In the ""Customer Number"" field, enter your Customer Number (shown above)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   10
         Top             =   2160
         Width           =   7695
      End
   End
   Begin VB.Frame Step2 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3495
      Left            =   120
      TabIndex        =   3
      Top             =   1200
      Visible         =   0   'False
      Width           =   7455
      Begin VB.CommandButton cmdDeactivate 
         Caption         =   "Deactivate"
         Enabled         =   0   'False
         Height          =   395
         Left            =   2880
         TabIndex        =   8
         Top             =   2160
         Width           =   1455
      End
      Begin VB.CommandButton cmd2 
         Caption         =   "2"
         Enabled         =   0   'False
         Height          =   395
         Left            =   2880
         TabIndex        =   7
         Top             =   1560
         Width           =   1455
      End
      Begin VB.CommandButton cmd3 
         Caption         =   "3"
         Enabled         =   0   'False
         Height          =   395
         Left            =   4440
         TabIndex        =   6
         Top             =   1560
         Width           =   1455
      End
      Begin VB.CommandButton cmd1 
         Caption         =   "1"
         Height          =   395
         Left            =   1320
         TabIndex        =   5
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   $"frmUnReg.frx":69CA
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   735
         Left            =   0
         TabIndex        =   4
         Top             =   120
         Width           =   7455
      End
   End
   Begin VB.Frame Step1 
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   3495
      Left            =   120
      TabIndex        =   1
      Top             =   1200
      Visible         =   0   'False
      Width           =   7455
      Begin VB.CheckBox chkMove 
         Caption         =   "This is the OLD PC where I am moving my SQL-RD license from"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   1680
         Width           =   7095
      End
      Begin VB.Label Label5 
         Caption         =   $"frmUnReg.frx":6A88
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   975
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   7215
      End
   End
   Begin VB.CommandButton cmdNext 
      Caption         =   "&Next >"
      Enabled         =   0   'False
      Height          =   375
      Left            =   6360
      TabIndex        =   23
      Top             =   5160
      Width           =   1215
   End
   Begin VB.Frame Frame3 
      BackColor       =   &H00FFFFFF&
      Height          =   135
      Left            =   0
      TabIndex        =   28
      Top             =   1080
      Width           =   7695
   End
   Begin VB.Frame Frame2 
      Height          =   135
      Left            =   0
      TabIndex        =   27
      Top             =   4800
      Width           =   7695
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   4920
      TabIndex        =   26
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdBack 
      Caption         =   "< &Back"
      Enabled         =   0   'False
      Height          =   375
      Left            =   3480
      TabIndex        =   25
      Top             =   5160
      Width           =   1215
   End
   Begin VB.CommandButton cmdFinish 
      Caption         =   "&Finish"
      Height          =   375
      Left            =   6360
      TabIndex        =   24
      Top             =   5160
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      Height          =   1095
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7695
      Begin VB.Label lblMessage 
         BackStyle       =   0  'Transparent
         Caption         =   "Message"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   1080
         TabIndex        =   29
         Top             =   390
         Width           =   6495
      End
      Begin VB.Image Image1 
         Height          =   915
         Left            =   120
         Picture         =   "frmUnReg.frx":6B46
         Top             =   120
         Width           =   900
      End
   End
End
Attribute VB_Name = "frmUnReg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Const S1 As String = "License Transfer Wizard"
Const S2 As String = "Deactivate License"
Const S3 As String = "Manual Deactivation"
Const S4 As String = "Finish"

Dim nStep As Integer

Private Sub chkMove_Click()
    If chkMove.Value = vbChecked Then
        cmdNext.Enabled = True
    Else
        cmdNext.Enabled = False
    End If
End Sub

Private Sub cmd1_Click()
    cmd2.Enabled = True
End Sub

Private Sub cmd2_Click()
    cmd3.Enabled = True
End Sub

Private Sub cmd3_Click()
    cmdDeactivate.Enabled = True
End Sub

Private Sub cmdBack_Click()
    If Step4.Visible = True Or Step3.Visible = True Then
        Step2.Visible = True
        
        Step4.Visible = False
        Step3.Visible = False
        Step1.Visible = False
        
        lblMessage.Caption = S2
        
        cmdNext.Visible = True
        cmdFinish.Visible = False
    ElseIf Step2.Visible = True Then
        Step1.Visible = True
        
        Step4.Visible = False
        Step3.Visible = False
        Step2.Visible = False
        
        lblMessage.Caption = S1
        cmdBack.Enabled = False
    End If
            
End Sub

Private Sub cmdCancel_Click()
    If MsgBox("Exit the License Transfer Wizard?", vbYesNo, "Registration & Activation") = vbYes Then
        Unload Me
    End If
End Sub

Private Sub cmdDeactivate_Click()
    Dim oArm As New clsArmadillo
    
    Dim unregKey As String
    
    unregKey = oArm.UninstallKey
    
    txtUninstallKey.Text = unregKey
    
    Dim oXML As New clsXML
    
    gConfigFile = App.Path & "\sqlrdlive.config"
    
    txtCustNo.Text = oXML.ReadXML("CustNo", "", gConfigFile)
    
    MsgBox "This installation of SQL-RD has now been deactivated.", vbInformation, "License Transfer Wizard"
    
    cmdNext.Enabled = True
End Sub

Private Sub cmdFinish_Click()
    Unload Me
End Sub

Private Sub cmdNext_Click()
    
    If Step1.Visible = True Then
        Step2.Visible = True
        
        Step1.Visible = False
        Step3.Visible = False
        Step4.Visible = False
        
        lblMessage.Caption = S2
        cmdBack.Enabled = True
        cmdNext.Enabled = False
        
    ElseIf Step2.Visible = True Then
        If frmActivate.GetDeactivationCode(txtUninstallKey.Text, txtCustNo.Text) = True Then
            Step4.Visible = True
            
            Step1.Visible = False
            Step2.Visible = False
            Step3.Visible = False
            
            lblMessage.Caption = S4
        Else
            Step3.Visible = True
            
            Step1.Visible = False
            Step2.Visible = False
            Step4.Visible = False
            
            lblMessage.Caption = S3
        End If
    ElseIf Step3.Visible = True Then
        Step4.Visible = True
        
        Step1.Visible = False
        Step2.Visible = False
        Step3.Visible = False
            
        lblMessage.Caption = S4
    End If
    
    If Step4.Visible = True Then
        cmdNext.Visible = False
        cmdFinish.Visible = True
    End If
                 
End Sub

Private Sub Form_Load()
    Step1.Visible = True
    lblMessage.Caption = S1
End Sub
