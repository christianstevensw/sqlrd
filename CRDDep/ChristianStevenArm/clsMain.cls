VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

'for armadillo
Private Declare Sub SECUREBEGIN Lib "armAccess.dll" ()
Private Declare Sub SECUREBEGIN_A Lib "armAccess.dll" ()
Private Declare Sub SECUREBEGIN_B Lib "armAccess.dll" ()
Private Declare Sub SECUREBEGIN_C Lib "armAccess.dll" ()
Private Declare Sub SECUREEND Lib "armAccess.dll" ()
Private Declare Sub SECUREEND_A Lib "armAccess.dll" ()
Private Declare Sub SECUREEND_B Lib "armAccess.dll" ()
Private Declare Sub SECUREEND_C Lib "armAccess.dll" ()

Dim oArm As New clsArmadillo

Public Sub RegWizard()
    frmRegWiz.Show 1
    End
End Sub
Public Function IsExpired() As Boolean
    Dim sValue As String
    
    sValue = oArm.GetEnvironString("EXPIRED")
    
    If sValue = "" Then
        IsExpired = False
    Else
        IsExpired = True
    End If

End Function

Public Function GetEditionString() As String
    GetEditionString = oArm.GetEnvironString("_EDITION")
End Function
Public Function GetEdition() As Integer
    Dim I As Integer
    
    I = 0
    
    SECUREBEGIN
        I = I + 1
    SECUREEND
    
    SECUREBEGIN_A
        I = I + 2
    SECUREEND_A
    
    SECUREBEGIN_B
        I = I + 3
    SECUREEND_B
        
    SECUREBEGIN_C
        I = I + 4
    SECUREEND_C
    
    GetEdition = I
    
End Function

Public Function IsHP() As Boolean
    Dim sValue As String
    Dim sKey As String
    
    sKey = "Software\ChristianSteven\CRD"
    
    sValue = oArm.GetEnvironString("_CRDHP")
    
    If sValue = "" Then sValue = "0"
    
    If sValue = "0" Then
        IsHP = False
    Else
        IsHP = True
    End If
    
End Function
Public Function GetExpiry() As String
    Dim TimeLeft As String
    
    TimeLeft = "0"
    TimeLeft = oArm.GetEnvironString("DAYSLEFT")
    
    If TimeLeft = "" Then TimeLeft = "0"
    
    GetExpiry = TimeLeft
End Function
