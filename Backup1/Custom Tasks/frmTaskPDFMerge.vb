Public Class frmTaskPDFMerge
    Inherits frmTaskMaster
    Dim UserCancel As Boolean = False
    Dim oField As TextBox
    Dim eventID As Integer = 99999
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TabControl2 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Dim eventBased As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvFiles As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAddFiles As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents MyTip As System.Windows.Forms.ToolTip
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents imgFiles As System.Windows.Forms.ImageList
    Friend WithEvents fbd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents chkReplace As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuAdder As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolder As System.Windows.Forms.MenuItem
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents grpPDF As System.Windows.Forms.GroupBox
    Friend WithEvents chkPDFSecurity As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPrint As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkCopy As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkEdit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNotes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFill As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAccess As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAssemble As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFullRes As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtOwnerPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtWatermark As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoTitle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoAuthor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoKeywords As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoProducer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optMergeFiles As System.Windows.Forms.RadioButton
    Friend WithEvents optMergeFolder As System.Windows.Forms.RadioButton
    Friend WithEvents grpFiles As System.Windows.Forms.GroupBox
    Friend WithEvents grpFolder As System.Windows.Forms.GroupBox
    Friend WithEvents txtPDFFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdPDFBrose As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskPDFMerge))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grpFolder = New System.Windows.Forms.GroupBox()
        Me.txtPDFFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmdPDFBrose = New DevComponents.DotNetBar.ButtonX()
        Me.grpFiles = New System.Windows.Forms.GroupBox()
        Me.lsvFiles = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAddFiles = New DevComponents.DotNetBar.ButtonX()
        Me.optMergeFiles = New System.Windows.Forms.RadioButton()
        Me.optMergeFolder = New System.Windows.Forms.RadioButton()
        Me.chkReplace = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.grpPDF = New System.Windows.Forms.GroupBox()
        Me.chkPDFSecurity = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkPrint = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkCopy = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkEdit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkNotes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFill = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAccess = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAssemble = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFullRes = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtOwnerPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUserPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtWatermark = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoTitle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoAuthor = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoKeywords = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoProducer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.MyTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.imgFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.fbd = New System.Windows.Forms.FolderBrowserDialog()
        Me.sfd = New System.Windows.Forms.SaveFileDialog()
        Me.mnuAdder = New System.Windows.Forms.ContextMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuFolder = New System.Windows.Forms.MenuItem()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.TabControl2 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.grpFolder.SuspendLayout()
        Me.grpFiles.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPDF.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(326, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 20
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.grpFolder)
        Me.GroupBox1.Controls.Add(Me.grpFiles)
        Me.GroupBox1.Controls.Add(Me.optMergeFiles)
        Me.GroupBox1.Controls.Add(Me.optMergeFolder)
        Me.GroupBox1.Controls.Add(Me.chkReplace)
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 7)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 323)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'grpFolder
        '
        Me.grpFolder.Controls.Add(Me.txtPDFFolder)
        Me.grpFolder.Controls.Add(Me.Label6)
        Me.grpFolder.Controls.Add(Me.cmdPDFBrose)
        Me.grpFolder.Location = New System.Drawing.Point(6, 43)
        Me.grpFolder.Name = "grpFolder"
        Me.grpFolder.Size = New System.Drawing.Size(455, 67)
        Me.grpFolder.TabIndex = 24
        Me.grpFolder.TabStop = False
        Me.grpFolder.Visible = False
        '
        'txtPDFFolder
        '
        '
        '
        '
        Me.txtPDFFolder.Border.Class = "TextBoxBorder"
        Me.txtPDFFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPDFFolder.Location = New System.Drawing.Point(6, 33)
        Me.txtPDFFolder.Name = "txtPDFFolder"
        Me.txtPDFFolder.Size = New System.Drawing.Size(394, 21)
        Me.txtPDFFolder.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(5, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(200, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Select the folder containing the PDF files"
        '
        'cmdPDFBrose
        '
        Me.cmdPDFBrose.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdPDFBrose.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdPDFBrose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPDFBrose.Location = New System.Drawing.Point(404, 32)
        Me.cmdPDFBrose.Name = "cmdPDFBrose"
        Me.cmdPDFBrose.Size = New System.Drawing.Size(40, 21)
        Me.cmdPDFBrose.TabIndex = 17
        Me.cmdPDFBrose.Text = "..."
        '
        'grpFiles
        '
        Me.grpFiles.Controls.Add(Me.lsvFiles)
        Me.grpFiles.Controls.Add(Me.cmdRemove)
        Me.grpFiles.Controls.Add(Me.cmdAddFiles)
        Me.grpFiles.Location = New System.Drawing.Point(6, 43)
        Me.grpFiles.Name = "grpFiles"
        Me.grpFiles.Size = New System.Drawing.Size(449, 163)
        Me.grpFiles.TabIndex = 25
        Me.grpFiles.TabStop = False
        '
        'lsvFiles
        '
        '
        '
        '
        Me.lsvFiles.Border.Class = "ListViewBorder"
        Me.lsvFiles.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.Location = New System.Drawing.Point(6, 11)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(392, 147)
        Me.lsvFiles.TabIndex = 6
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 382
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(404, 47)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemove.TabIndex = 5
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddFiles.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(404, 12)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 25)
        Me.cmdAddFiles.TabIndex = 4
        '
        'optMergeFiles
        '
        Me.optMergeFiles.AutoSize = True
        Me.optMergeFiles.Checked = True
        Me.optMergeFiles.Location = New System.Drawing.Point(11, 20)
        Me.optMergeFiles.Name = "optMergeFiles"
        Me.optMergeFiles.Size = New System.Drawing.Size(153, 17)
        Me.optMergeFiles.TabIndex = 20
        Me.optMergeFiles.TabStop = True
        Me.optMergeFiles.Text = "Merge multiple files/folders"
        Me.optMergeFiles.UseVisualStyleBackColor = True
        '
        'optMergeFolder
        '
        Me.optMergeFolder.AutoSize = True
        Me.optMergeFolder.Location = New System.Drawing.Point(170, 20)
        Me.optMergeFolder.Name = "optMergeFolder"
        Me.optMergeFolder.Size = New System.Drawing.Size(171, 17)
        Me.optMergeFolder.TabIndex = 19
        Me.optMergeFolder.Text = "Merge all files in a single folder"
        Me.optMergeFolder.UseVisualStyleBackColor = True
        '
        'chkReplace
        '
        '
        '
        '
        Me.chkReplace.BackgroundStyle.Class = ""
        Me.chkReplace.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkReplace.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkReplace.Location = New System.Drawing.Point(8, 251)
        Me.chkReplace.Name = "chkReplace"
        Me.chkReplace.Size = New System.Drawing.Size(360, 24)
        Me.chkReplace.TabIndex = 18
        Me.chkReplace.Text = "Replace existing files at destination"
        Me.MyTip.SetToolTip(Me.chkReplace, "Replace existing files at the destination")
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(416, 227)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 21)
        Me.cmdBrowse.TabIndex = 17
        Me.cmdBrowse.Text = "..."
        Me.MyTip.SetToolTip(Me.cmdBrowse, "Browse to the destination directory")
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 211)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Destination File"
        '
        'txtPath
        '
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 227)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(392, 21)
        Me.txtPath.TabIndex = 15
        Me.MyTip.SetToolTip(Me.txtPath, "Destination to copy the files to")
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(395, 21)
        Me.txtName.TabIndex = 19
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(407, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 21
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 21)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Task Name"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'grpPDF
        '
        Me.grpPDF.BackColor = System.Drawing.Color.Transparent
        Me.grpPDF.Controls.Add(Me.chkPDFSecurity)
        Me.grpPDF.Controls.Add(Me.GroupBox2)
        Me.grpPDF.Controls.Add(Me.Label5)
        Me.grpPDF.Controls.Add(Me.txtWatermark)
        Me.grpPDF.Location = New System.Drawing.Point(4, 7)
        Me.grpPDF.Name = "grpPDF"
        Me.grpPDF.Size = New System.Drawing.Size(392, 312)
        Me.grpPDF.TabIndex = 24
        Me.grpPDF.TabStop = False
        Me.grpPDF.Text = "PDF Options"
        '
        'chkPDFSecurity
        '
        '
        '
        '
        Me.chkPDFSecurity.BackgroundStyle.Class = ""
        Me.chkPDFSecurity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPDFSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPDFSecurity.Location = New System.Drawing.Point(8, 16)
        Me.chkPDFSecurity.Name = "chkPDFSecurity"
        Me.chkPDFSecurity.Size = New System.Drawing.Size(336, 24)
        Me.chkPDFSecurity.TabIndex = 0
        Me.chkPDFSecurity.Text = "Enable PDF Options "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkPrint)
        Me.GroupBox2.Controls.Add(Me.chkCopy)
        Me.GroupBox2.Controls.Add(Me.chkEdit)
        Me.GroupBox2.Controls.Add(Me.chkNotes)
        Me.GroupBox2.Controls.Add(Me.chkFill)
        Me.GroupBox2.Controls.Add(Me.chkAccess)
        Me.GroupBox2.Controls.Add(Me.chkAssemble)
        Me.GroupBox2.Controls.Add(Me.chkFullRes)
        Me.GroupBox2.Controls.Add(Me.txtOwnerPassword)
        Me.GroupBox2.Controls.Add(Me.txtUserPassword)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.GroupBox9)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(376, 216)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "File Permissions"
        '
        'chkPrint
        '
        '
        '
        '
        Me.chkPrint.BackgroundStyle.Class = ""
        Me.chkPrint.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPrint.Checked = True
        Me.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrint.CheckValue = "Y"
        Me.chkPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPrint.Location = New System.Drawing.Point(8, 88)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(176, 24)
        Me.chkPrint.TabIndex = 2
        Me.chkPrint.Text = "Can Print"
        '
        'chkCopy
        '
        '
        '
        '
        Me.chkCopy.BackgroundStyle.Class = ""
        Me.chkCopy.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCopy.Checked = True
        Me.chkCopy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopy.CheckValue = "Y"
        Me.chkCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCopy.Location = New System.Drawing.Point(8, 120)
        Me.chkCopy.Name = "chkCopy"
        Me.chkCopy.Size = New System.Drawing.Size(176, 24)
        Me.chkCopy.TabIndex = 3
        Me.chkCopy.Text = "Can Copy"
        '
        'chkEdit
        '
        '
        '
        '
        Me.chkEdit.BackgroundStyle.Class = ""
        Me.chkEdit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEdit.Checked = True
        Me.chkEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEdit.CheckValue = "Y"
        Me.chkEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEdit.Location = New System.Drawing.Point(8, 152)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(176, 24)
        Me.chkEdit.TabIndex = 4
        Me.chkEdit.Text = "Can Edit"
        '
        'chkNotes
        '
        '
        '
        '
        Me.chkNotes.BackgroundStyle.Class = ""
        Me.chkNotes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNotes.Checked = True
        Me.chkNotes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNotes.CheckValue = "Y"
        Me.chkNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNotes.Location = New System.Drawing.Point(184, 152)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(176, 24)
        Me.chkNotes.TabIndex = 8
        Me.chkNotes.Text = "Can Add Notes"
        '
        'chkFill
        '
        '
        '
        '
        Me.chkFill.BackgroundStyle.Class = ""
        Me.chkFill.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFill.Checked = True
        Me.chkFill.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFill.CheckValue = "Y"
        Me.chkFill.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFill.Location = New System.Drawing.Point(8, 184)
        Me.chkFill.Name = "chkFill"
        Me.chkFill.Size = New System.Drawing.Size(176, 24)
        Me.chkFill.TabIndex = 5
        Me.chkFill.Text = "Can Fill Fields"
        '
        'chkAccess
        '
        '
        '
        '
        Me.chkAccess.BackgroundStyle.Class = ""
        Me.chkAccess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAccess.Checked = True
        Me.chkAccess.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAccess.CheckValue = "Y"
        Me.chkAccess.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAccess.Location = New System.Drawing.Point(184, 184)
        Me.chkAccess.Name = "chkAccess"
        Me.chkAccess.Size = New System.Drawing.Size(176, 24)
        Me.chkAccess.TabIndex = 9
        Me.chkAccess.Text = "Can copy accessibility options"
        '
        'chkAssemble
        '
        '
        '
        '
        Me.chkAssemble.BackgroundStyle.Class = ""
        Me.chkAssemble.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAssemble.Checked = True
        Me.chkAssemble.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssemble.CheckValue = "Y"
        Me.chkAssemble.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssemble.Location = New System.Drawing.Point(184, 88)
        Me.chkAssemble.Name = "chkAssemble"
        Me.chkAssemble.Size = New System.Drawing.Size(176, 24)
        Me.chkAssemble.TabIndex = 6
        Me.chkAssemble.Text = "Can Assemble"
        '
        'chkFullRes
        '
        '
        '
        '
        Me.chkFullRes.BackgroundStyle.Class = ""
        Me.chkFullRes.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFullRes.Checked = True
        Me.chkFullRes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFullRes.CheckValue = "Y"
        Me.chkFullRes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFullRes.Location = New System.Drawing.Point(184, 120)
        Me.chkFullRes.Name = "chkFullRes"
        Me.chkFullRes.Size = New System.Drawing.Size(176, 24)
        Me.chkFullRes.TabIndex = 7
        Me.chkFullRes.Text = "Can print in full resolution"
        '
        'txtOwnerPassword
        '
        '
        '
        '
        Me.txtOwnerPassword.Border.Class = "TextBoxBorder"
        Me.txtOwnerPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOwnerPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOwnerPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtOwnerPassword.Name = "txtOwnerPassword"
        Me.txtOwnerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOwnerPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtOwnerPassword.TabIndex = 0
        '
        'txtUserPassword
        '
        '
        '
        '
        Me.txtUserPassword.Border.Class = "TextBoxBorder"
        Me.txtUserPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtUserPassword.Location = New System.Drawing.Point(184, 40)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtUserPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtUserPassword.TabIndex = 1
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(184, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User Password"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Owner Password"
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(8, 72)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(360, 8)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Tag = "3dline"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(16, 264)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(256, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Include the following watermark"
        '
        'txtWatermark
        '
        '
        '
        '
        Me.txtWatermark.Border.Class = "TextBoxBorder"
        Me.txtWatermark.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWatermark.Location = New System.Drawing.Point(16, 280)
        Me.txtWatermark.Name = "txtWatermark"
        Me.txtWatermark.Size = New System.Drawing.Size(360, 21)
        Me.txtWatermark.TabIndex = 16
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox3.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox3.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox3.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtInfoProducer)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 4)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(392, 320)
        Me.GroupBox3.TabIndex = 24
        Me.GroupBox3.TabStop = False
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Location = New System.Drawing.Point(136, 120)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.Class = ""
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        '
        '
        '
        Me.txtInfoTitle.Border.Class = "TextBoxBorder"
        Me.txtInfoTitle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoTitle.Location = New System.Drawing.Point(136, 24)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        '
        '
        '
        Me.txtInfoAuthor.Border.Class = "TextBoxBorder"
        Me.txtInfoAuthor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoAuthor.Location = New System.Drawing.Point(136, 48)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        '
        '
        '
        Me.txtInfoSubject.Border.Class = "TextBoxBorder"
        Me.txtInfoSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoSubject.Location = New System.Drawing.Point(136, 96)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        '
        '
        '
        Me.txtInfoKeywords.Border.Class = "TextBoxBorder"
        Me.txtInfoKeywords.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoKeywords.Location = New System.Drawing.Point(136, 144)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(232, 168)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.Class = ""
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.Class = ""
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 72)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.Class = ""
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 144)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.Class = ""
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        '
        '
        '
        Me.txtInfoProducer.Border.Class = "TextBoxBorder"
        Me.txtInfoProducer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoProducer.Location = New System.Drawing.Point(136, 72)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoProducer.TabIndex = 20
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.Class = ""
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'ofd
        '
        Me.ofd.Filter = "PDF Files|*.pdf|All Files|*.*"
        Me.ofd.Title = "Please select PDF Files to merge"
        '
        'imgFiles
        '
        Me.imgFiles.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.imgFiles.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFiles.TransparentColor = System.Drawing.Color.Transparent
        '
        'sfd
        '
        Me.sfd.Filter = "PDF Files|*.pdf"
        Me.sfd.OverwritePrompt = False
        Me.sfd.Title = "Please specify the merged file"
        '
        'mnuAdder
        '
        Me.mnuAdder.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuFolder})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.Text = "PDF File(s)"
        '
        'mnuFolder
        '
        Me.mnuFolder.Index = 1
        Me.mnuFolder.Text = "Folder"
        '
        'fbg
        '
        Me.fbg.Description = "Please select the folder containing PDF Files"
        Me.fbg.ShowNewFolderButton = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'TabControl2
        '
        Me.TabControl2.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl2.CanReorderTabs = True
        Me.TabControl2.Controls.Add(Me.TabControlPanel1)
        Me.TabControl2.Controls.Add(Me.TabControlPanel3)
        Me.TabControl2.Controls.Add(Me.TabControlPanel2)
        Me.TabControl2.Location = New System.Drawing.Point(7, 6)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl2.SelectedTabIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(472, 359)
        Me.TabControl2.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock
        Me.TabControl2.TabIndex = 24
        Me.TabControl2.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl2.Tabs.Add(Me.TabItem1)
        Me.TabControl2.Tabs.Add(Me.TabItem2)
        Me.TabControl2.Tabs.Add(Me.TabItem3)
        Me.TabControl2.Text = "TabControl2"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(472, 336)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.Style.GradientAngle = 90
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem1
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel1
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "General"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.GroupBox3)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(472, 336)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.Style.GradientAngle = 90
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem3
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel3
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "PDF File Summary"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.grpPDF)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(472, 336)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.Style.GradientAngle = 90
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem2
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel2
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "PDF Security"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 27)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(485, 395)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 25
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(485, 395)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 0)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.TabControl2)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(485, 369)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(485, 27)
        Me.FlowLayoutPanel1.TabIndex = 26
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 422)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(485, 30)
        Me.FlowLayoutPanel2.TabIndex = 26
        '
        'frmTaskPDFMerge
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(485, 452)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskPDFMerge"
        Me.Text = "Merge PDF Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpFolder.ResumeLayout(False)
        Me.grpFolder.PerformLayout()
        Me.grpFiles.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPDF.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf lsvFiles.Items.Count = 0 And Me.optMergeFiles.Checked Then
            SetError(lsvFiles, "Please select the file(s) to copy")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf txtPath.Text = "" Then
            SetError(txtPath, "Please specify the merged PDF file name")
            txtPath.Focus()
            Exit Sub
        ElseIf Me.txtPDFFolder.Text = "" And Me.optMergeFolder.Checked Then
            SetError(Me.txtPDFFolder, "Please select the folder to process")
            Me.txtPDFFolder.Focus()
            Exit Sub
        End If

        Me.Close()
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With sfd
            .AddExtension = True
            .DefaultExt = "pdf"
            .ShowDialog()

            If .FileName.Length = 0 Then Return

            txtPath.Text = .FileName
        End With
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        If Me.optMergeFiles.Checked Then
            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = Me.txtPDFFolder.Text
        End If

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,ReplaceFiles,OrderID"

        sVals = nTaskID & "," & _
        ScheduleID & "," & _
        "'MergePDF'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        Convert.ToInt32(chkReplace.Checked) & "," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        sCols = "OptionID,TaskID,PDFPassword,PDFWaterMark,UserPassword,CanPrint," & _
        "CanCopy,CanEdit,CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "PDFSecurity," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer"

        sVals = clsMarsData.CreateDataID("taskoptions", "optionid") & "," & nTaskID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(Me.txtWatermark.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtUserPassword.Text)) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        Convert.ToInt32(chkPDFSecurity.Checked) & "," & _
        "'" & SQLPrepare(Me.txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(Me.txtInfoProducer.Text) & "'"

        SQL = "INSERT INTO TaskOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)

        Return nTaskID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

      

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            If sFiles.Contains("|") = False And sFiles.Contains("*") Then
                Me.txtPDFFolder.Text = sFiles
                Me.optMergeFolder.Checked = True
            Else
                For Each sVal In sFiles.Split("|")
                    If sVal.Length > 0 Then lsvFiles.Items.Add(sVal)
                Next

                Me.optMergeFiles.Checked = True
            End If


            txtPath.Text = oRs("programpath").Value

            chkReplace.Checked = oRs("replacefiles").Value

        End If

        oRs.Close()

        Try
            oRs = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then

                    With Me
                        .txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                        .txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                        .txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                        .chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                        .chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                        .chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                        .chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                        .chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                        .chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                        .chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)
                        .chkPDFSecurity.Checked = Convert.ToBoolean(oRs("PDFsecurity").Value)
                        .txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                        .txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                        .txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                        .txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)

                        Try
                            .txtInfoCreated.Value = Convert.ToDateTime(oRs("infocreated").Value)
                        Catch ex As Exception
                            .txtInfoCreated.Value = Now
                        End Try

                        .txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)
                    End With
                End If
            End If
        Catch : End Try

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""

        If Me.optMergeFiles.Checked Then
            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = Me.txtPDFFolder.Text
        End If

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'," & _
            "ReplaceFiles = " & Convert.ToInt32(chkReplace.Checked)

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        With Me
            SQL = "PDFPassword = '" & SQLPrepare(_EncryptDBValue(.txtOwnerPassword.Text)) & "'," & _
            "PDFWaterMark = '" & SQLPrepare(.txtWatermark.Text) & "'," & _
            "UserPassword = '" & SQLPrepare(_EncryptDBValue(.txtUserPassword.Text)) & "'," & _
            "CanPrint = " & Convert.ToInt32(.chkPrint.Checked) & "," & _
            "CanCopy = " & Convert.ToInt32(.chkCopy.Checked) & "," & _
            "CanEdit = " & Convert.ToInt32(.chkEdit.Checked) & "," & _
            "CanNotes = " & Convert.ToInt32(.chkNotes.Checked) & "," & _
            "CanFill = " & Convert.ToInt32(.chkFill.Checked) & "," & _
            "CanAccess = " & Convert.ToInt32(.chkAccess.Checked) & "," & _
            "CanAssemble = " & Convert.ToInt32(.chkAssemble.Checked) & "," & _
            "CanPrintFull = " & Convert.ToInt32(.chkFullRes.Checked) & "," & _
            "PDFSecurity = " & Convert.ToInt32(.chkPDFSecurity.Checked) & "," & _
            "InfoTitle = '" & SQLPrepare(.txtInfoTitle.Text) & "'," & _
            "InfoAuthor = '" & SQLPrepare(.txtInfoAuthor.Text) & "'," & _
            "InfoSubject = '" & SQLPrepare(.txtInfoSubject.Text) & "'," & _
            "InfoKeyWords = '" & SQLPrepare(.txtInfoKeywords.Text) & "'," & _
            "InfoCreated = '" & ConDateTime(.txtInfoCreated.Value) & "'," & _
            "InfoProducer = '" & SQLPrepare(.txtInfoProducer.Text) & "'"
        End With

        SQL = "UPDATE TaskOptions SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdAddFiles_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAddFiles.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        mnuAdder.Show(cmdAddFiles, New Point(e.X, e.Y))

    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            If sFile.Length > 0 Then
                sFile = _CreateUNC(sFile)
                lsv = lsvFiles.Items.Add(sFile)
                lsv.ImageIndex = 0
            End If
        Next

        SetError(lsvFiles, "")
    End Sub

    Private Sub mnuFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolder.Click
        Dim lsv As ListViewItem
        Dim sPath As String

        With fbg
            .ShowDialog()

            If .SelectedPath.Length = 0 Then Return

            sPath = _CreateUNC(.SelectedPath)

            If sPath.EndsWith("\") = False Then
                sPath &= "\"
            End If

            lsv = lsvFiles.Items.Add(sPath & "*.pdf")

            lsv.ImageIndex = 0
        End With
    End Sub

    Private Sub frmTaskPDFMerge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(Me.txtPDFFolder) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtPath) '.ContextMenu = Me.mnuInserter

        showInserter(Me, m_eventID, m_eventBased)
    End Sub

    Private Sub chkPDFSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPDFSecurity.CheckedChanged
        GroupBox2.Enabled = chkPDFSecurity.Checked
    End Sub

   
    

    Private Sub cmdPDFBrose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPDFBrose.Click

        Dim sPath As String

        With fbg
            .ShowDialog()

            If .SelectedPath.Length = 0 Then Return

            sPath = _CreateUNC(.SelectedPath)

            If sPath.EndsWith("\") = False Then
                sPath &= "\"
            End If

            Me.txtPDFFolder.Text = sPath & "*.pdf"


        End With
    End Sub

    Private Sub optMergeFiles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMergeFiles.CheckedChanged

        Me.grpFiles.Visible = Me.optMergeFiles.Checked

    End Sub

    Private Sub optMergeFolder_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMergeFolder.CheckedChanged
        Me.grpFolder.Visible = Me.optMergeFolder.Checked
    End Sub

    Private Sub txtPath_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.GotFocus, txtPDFFolder.GotFocus
        oField = CType(sender, TextBox)
    End Sub

   
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            oField.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim inserter As frmInserter = New frmInserter(Me.m_eventID)
        inserter.m_EventID = Me.m_eventID
        inserter.GetConstants(Me)
    End Sub
End Class
