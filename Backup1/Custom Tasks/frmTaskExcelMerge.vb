﻿Public Class frmTaskExcelMerge
    Dim userCancel As Boolean = False
    Public m_eventBased As Boolean
    Public m_eventID As Integer = 99999
    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf lsvFiles.Items.Count = 0 Then
            SetError(lsvFiles, "Please select the file(s) to copy")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf txtPath.Text = "" Then
            SetError(txtPath, "Please specify the merged PDF file name")
            txtPath.Focus()
            Exit Sub
        End If

        Me.Close()
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With sfd
            .AddExtension = True
            .DefaultExt = "xls"
            .ShowDialog()

            If .FileName.Length = 0 Then Return

            txtPath.Text = .FileName
        End With
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If userCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        For Each lsv In lsvFiles.Items
            sFiles &= lsv.Text & "|"
        Next


        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,ReplaceFiles,OrderID"

        sVals = nTaskID & "," & _
        ScheduleID & "," & _
        "'MergeXLS'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        Convert.ToInt32(chkReplace.Checked) & "," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        sCols = "OptionID,TaskID,PDFPassword," & _
        "PDFSecurity," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer"

        sVals = clsMarsData.CreateDataID("taskoptions", "optionid") & "," & nTaskID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtPassword.Text)) & "'," & _
        Convert.ToInt32(chkExcelSecurity.Checked) & "," & _
        "'" & SQLPrepare(Me.txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(Me.txtInfoProducer.Text) & "'"

        SQL = "INSERT INTO TaskOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)


        Return nTaskID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            For Each sVal In sFiles.Split("|")
                If sval.Length > 0 Then lsvFiles.Items.Add(sVal)
            Next

            txtPath.Text = oRs("programpath").Value

            chkReplace.Checked = oRs("replacefiles").Value

        End If



        oRs.Close()

        Try
            oRs = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then

                    With Me
                        .txtPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                        .chkExcelSecurity.Checked = Convert.ToBoolean(oRs("PDFsecurity").Value)
                        .txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                        .txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                        .txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                        .txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)

                        Try
                            .txtInfoCreated.Value = Convert.ToDateTime(oRs("infocreated").Value)
                        Catch ex As Exception
                            .txtInfoCreated.Value = Now
                        End Try

                        .txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)
                    End With
                End If
            End If
        Catch : End Try

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""

        For Each lsv In lsvFiles.Items
            sFiles &= lsv.Text & "|"
        Next

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'," & _
            "ReplaceFiles = " & Convert.ToInt32(chkReplace.Checked)

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        With Me
            SQL = "PDFPassword = '" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "'," & _
            "PDFSecurity = " & Convert.ToInt32(.chkExcelSecurity.Checked) & "," & _
            "InfoTitle = '" & SQLPrepare(.txtInfoTitle.Text) & "'," & _
            "InfoAuthor = '" & SQLPrepare(.txtInfoAuthor.Text) & "'," & _
            "InfoSubject = '" & SQLPrepare(.txtInfoSubject.Text) & "'," & _
            "InfoKeyWords = '" & SQLPrepare(.txtInfoKeywords.Text) & "'," & _
            "InfoCreated = '" & ConDateTime(.txtInfoCreated.Value) & "'," & _
            "InfoProducer = '" & SQLPrepare(.txtInfoProducer.Text) & "'"
        End With

        SQL = "UPDATE TaskOptions SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdAddFiles_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAddFiles.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        mnuAdder.Show(cmdAddFiles, New Point(e.X, e.Y))

    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            If sFile.Length > 0 Then
                sFile = _CreateUNC(sFile)
                lsv = lsvFiles.Items.Add(sFile)
                lsv.ImageIndex = 0
            End If
        Next

        SetError(lsvFiles, "")
    End Sub

    Private Sub mnuFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolder.Click
        Dim lsv As ListViewItem
        Dim sPath As String

        With fbg
            .ShowDialog()

            If .SelectedPath.Length = 0 Then Return

            sPath = _CreateUNC(.SelectedPath)

            If sPath.EndsWith("\") = False Then
                sPath &= "\"
            End If

            lsv = lsvFiles.Items.Add(sPath & "*.xls*")

            lsv.ImageIndex = 0
        End With
    End Sub

    Private Sub frmTaskPDFMerge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtPath) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtPassword) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtInfoAuthor) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtInfoKeywords) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtInfoProducer) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtInfoSubject) '.ContextMenu = mnuInserter
        setupForDragAndDrop(txtInfoTitle) '.ContextMenu = mnuInserter


        showInserter(Me, m_eventID, m_eventBased)
    End Sub

    Private Sub chkPDFSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExcelSecurity.CheckedChanged
        GroupBox2.Enabled = chkExcelSecurity.Checked
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Undo()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Cut()
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Paste()
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Copy()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.m_eventID)
        oInsert.m_EventBased = Me.m_eventBased
        'oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        If TypeOf Me.ActiveControl Is TextBox Then
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            Dim oItem As New frmDataItems
            oItem.m_EventID = Me.m_eventID

            txt.SelectedText = oItem._GetDataItem(Me.m_eventID)
        End If
    End Sub
End Class