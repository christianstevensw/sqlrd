Public Class frmTaskDBDelete
    Inherits sqlrd.frmTaskMaster
    Dim sOp As String = " AND "
    Dim nStep As Integer = 1
    Dim UserCancel As Boolean
    Dim eventBased As Boolean = False
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents Options As DevComponents.DotNetBar.SuperTabItem
    Dim eventID As Integer = 99999
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTables As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbColumns As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents optAnd As System.Windows.Forms.RadioButton
    Friend WithEvents optOr As System.Windows.Forms.RadioButton
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbWhereValue As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents lsvWhere As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents MyTip As System.Windows.Forms.ToolTip
    Friend WithEvents cmdSkip As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskDBDelete))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page3 = New System.Windows.Forms.GroupBox()
        Me.txtFinal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.lsvWhere = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.optAnd = New System.Windows.Forms.RadioButton()
        Me.optOr = New System.Windows.Forms.RadioButton()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbWhereValue = New System.Windows.Forms.ComboBox()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmbColumns = New System.Windows.Forms.ComboBox()
        Me.cmbTables = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdSkip = New DevComponents.DotNetBar.ButtonX()
        Me.MyTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Options = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(79, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(340, 21)
        Me.txtName.TabIndex = 17
        Me.MyTip.SetToolTip(Me.txtName, "Enter the name of the task")
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(8, 267)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 22)
        Me.cmdBack.TabIndex = 16
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 21)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Task Name"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(352, 267)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 22)
        Me.cmdNext.TabIndex = 15
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 297)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.txtFinal)
        Me.Page3.Controls.Add(Me.Label6)
        Me.Page3.Location = New System.Drawing.Point(8, 11)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(400, 253)
        Me.Page3.TabIndex = 19
        Me.Page3.TabStop = False
        '
        'txtFinal
        '
        '
        '
        '
        Me.txtFinal.Border.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtFinal.Border.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtFinal.Border.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtFinal.Border.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.txtFinal.Border.Class = "TextBoxBorder"
        Me.txtFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFinal.Location = New System.Drawing.Point(8, 26)
        Me.txtFinal.Multiline = True
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFinal.Size = New System.Drawing.Size(384, 215)
        Me.txtFinal.TabIndex = 3
        Me.txtFinal.Tag = "memo"
        Me.MyTip.SetToolTip(Me.txtFinal, "Your finalized query is shown here. You can ccustomize it here.")
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 11)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Finalised Query"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(8, 11)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(400, 253)
        Me.Page1.TabIndex = 17
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(157, 141)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        Me.MyTip.SetToolTip(Me.cmdValidate, "Connect to the selected ODBC Data Source")
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 7)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 124)
        Me.UcDSN.TabIndex = 0
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvWhere)
        Me.Page2.Controls.Add(Me.cmdAddWhere)
        Me.Page2.Controls.Add(Me.optAnd)
        Me.Page2.Controls.Add(Me.optOr)
        Me.Page2.Controls.Add(Me.cmbOperator)
        Me.Page2.Controls.Add(Me.cmbWhereValue)
        Me.Page2.Controls.Add(Me.cmdRemoveWhere)
        Me.Page2.Controls.Add(Me.cmbColumns)
        Me.Page2.Controls.Add(Me.cmbTables)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.Label3)
        Me.Page2.Location = New System.Drawing.Point(8, 11)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(400, 253)
        Me.Page2.TabIndex = 18
        Me.Page2.TabStop = False
        '
        'lsvWhere
        '
        '
        '
        '
        Me.lsvWhere.Border.Class = "ListViewBorder"
        Me.lsvWhere.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvWhere.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvWhere.Location = New System.Drawing.Point(8, 141)
        Me.lsvWhere.Name = "lsvWhere"
        Me.lsvWhere.Size = New System.Drawing.Size(384, 104)
        Me.lsvWhere.TabIndex = 11
        Me.lsvWhere.UseCompatibleStateImageBehavior = False
        Me.lsvWhere.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Criteria"
        Me.ColumnHeader1.Width = 357
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(280, 111)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 23)
        Me.cmdAddWhere.TabIndex = 10
        Me.MyTip.SetToolTip(Me.cmdAddWhere, "Add the above criteria")
        '
        'optAnd
        '
        Me.optAnd.Checked = True
        Me.optAnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAnd.Location = New System.Drawing.Point(184, 111)
        Me.optAnd.Name = "optAnd"
        Me.optAnd.Size = New System.Drawing.Size(48, 23)
        Me.optAnd.TabIndex = 8
        Me.optAnd.TabStop = True
        Me.optAnd.Text = "And"
        '
        'optOr
        '
        Me.optOr.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optOr.Location = New System.Drawing.Point(232, 111)
        Me.optOr.Name = "optOr"
        Me.optOr.Size = New System.Drawing.Size(40, 23)
        Me.optOr.TabIndex = 7
        Me.optOr.Text = "Or"
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"<", "<=", "<>", "=", ">", ">=", "BEGINS WITH", "CONTAINS", "DOES NOT BEGIN WITH", "DOES NOT CONTAIN", "DOES NOT END WITH", "ENDS WITH", "IS EMPTY", "IS NOT EMPTY"})
        Me.cmbOperator.Location = New System.Drawing.Point(184, 89)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(88, 21)
        Me.cmbOperator.Sorted = True
        Me.cmbOperator.TabIndex = 5
        '
        'cmbWhereValue
        '
        Me.cmbWhereValue.ContextMenu = Me.mnuInserter
        Me.cmbWhereValue.ItemHeight = 13
        Me.cmbWhereValue.Location = New System.Drawing.Point(280, 89)
        Me.cmbWhereValue.Name = "cmbWhereValue"
        Me.cmbWhereValue.Size = New System.Drawing.Size(112, 21)
        Me.cmbWhereValue.TabIndex = 6
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(128, 111)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 23)
        Me.cmdRemoveWhere.TabIndex = 9
        '
        'cmbColumns
        '
        Me.cmbColumns.ItemHeight = 13
        Me.cmbColumns.Location = New System.Drawing.Point(8, 89)
        Me.cmbColumns.Name = "cmbColumns"
        Me.cmbColumns.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumns.TabIndex = 1
        Me.MyTip.SetToolTip(Me.cmbColumns, "Select the column to use in the deletion criteria")
        '
        'cmbTables
        '
        Me.cmbTables.ItemHeight = 13
        Me.cmbTables.Location = New System.Drawing.Point(8, 30)
        Me.cmbTables.Name = "cmbTables"
        Me.cmbTables.Size = New System.Drawing.Size(168, 21)
        Me.cmbTables.TabIndex = 1
        Me.MyTip.SetToolTip(Me.cmbTables, "Select the name of the table to delete the record from")
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(168, 21)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Select the table to delete from"
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(168, 21)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Only delete records where"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(267, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 21
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(348, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 22
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdSkip
        '
        Me.cmdSkip.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSkip.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(14, 3)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 23)
        Me.cmdSkip.TabIndex = 23
        Me.cmdSkip.Text = "&Skip"
        Me.MyTip.SetToolTip(Me.cmdSkip, "Skip to enter a custom query")
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 24
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 29)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(426, 331)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 25
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.Options})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(426, 306)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(426, 331)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.Options
        '
        'Options
        '
        Me.Options.AttachedControl = Me.SuperTabControlPanel2
        Me.Options.GlobalItem = False
        Me.Options.Name = "Options"
        Me.Options.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(426, 29)
        Me.FlowLayoutPanel1.TabIndex = 26
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Controls.Add(Me.Label4)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdSkip)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 360)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(426, 31)
        Me.FlowLayoutPanel2.TabIndex = 26
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(95, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(166, 13)
        Me.Label4.TabIndex = 24
        '
        'frmTaskDBDelete
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(426, 391)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskDBDelete"
        Me.Text = "Delete database record(s)"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.Page2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskDBDelete_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Page1.BringToFront()

        showInserter(Me, Me.m_eventID, m_eventBased)

        setupForDragAndDrop(cmbWhereValue)
        setupForDragAndDrop(txtFinal)
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdNext.Enabled = True

            Dim oData As clsMarsData = New clsMarsData

            oData.GetTables(cmbTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            cmdSkip.Enabled = True
        Else

            cmdNext.Enabled = False
        End If
    End Sub



    Private Sub cmbWhereValue_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWhereValue.DropDown
        Dim oData As clsMarsData = New clsMarsData

        oData.ReturnDistinctValues(cmbWhereValue, UcDSN.cmbDSN.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text, cmbColumns.Text, _
        cmbTables.Text)
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        If cmbOperator.Text.ToLower = "is empty" Or cmbOperator.Text.ToLower = "is not empty" Then
            cmbWhereValue.Enabled = False
        Else
            cmbWhereValue.Enabled = True
        End If
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim sKey As String = ""
        Dim oItem As clsMyList
        Dim nType As Integer

        If cmbColumns.Text = "" Then
            SetError(cmbColumns, "Please select the column name")
            Return
        ElseIf cmbOperator.Text = "" Then
            SetError(cmbOperator, "Please select the operator")
            Return
        End If

        If lsvWhere.Items.Count > 0 Then
            sKey = sOp
        End If

        Select Case cmbOperator.Text.ToLower
            Case "begins with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " LIKE '" & cmbWhereValue.Text & "%'")
            Case "ends with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " LIKE '%" & cmbWhereValue.Text & "'")
            Case "contains"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " LIKE '%" & cmbWhereValue.Text & "%'")
            Case "does not contain"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " NOT LIKE '%" & cmbWhereValue.Text & "%'")
            Case "does not begin with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " NOT LIKE '" & cmbWhereValue.Text & "%'")
            Case "does not end with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbColumns.Text & _
                " NOT LIKE '%" & cmbWhereValue.Text & "'")
            Case "is empty"

                lsvWhere.Items.Add(sKey & _
                "(" & cmbTables.Text & "." & cmbColumns.Text & _
                " IS NULL OR " & _
                cmbTables.Text & "." & cmbColumns.Text & " ='')")
            Case "is not empty"
                cmbWhereValue.Enabled = False
                lsvWhere.Items.Add(sKey & _
                "(" & cmbTables.Text & "." & cmbColumns.Text & _
                " IS NOT NULL AND " & _
                cmbTables.Text & "." & cmbColumns.Text & " <> '')")
            Case Else

                oItem = cmbColumns.Items(cmbColumns.SelectedIndex)

                nType = oItem.ItemData

                If nType = ADODB.DataTypeEnum.adBSTR Or nType = ADODB.DataTypeEnum.adChar Or nType = ADODB.DataTypeEnum.adLongVarChar _
                       Or nType = ADODB.DataTypeEnum.adLongVarWChar Or nType = ADODB.DataTypeEnum.adVarChar Or nType = ADODB.DataTypeEnum.adVarWChar _
                       Or nType = ADODB.DataTypeEnum.adWChar Or nType = ADODB.DataTypeEnum.adDate Or nType = ADODB.DataTypeEnum.adDBDate Or _
                       nType = ADODB.DataTypeEnum.adDBTime Or nType = ADODB.DataTypeEnum.adDBTimeStamp Then

                    lsvWhere.Items.Add(sKey & cmbColumns.Text & cmbOperator.Text & "'" & cmbWhereValue.Text & "'")
                Else
                    lsvWhere.Items.Add(sKey & cmbColumns.Text & cmbOperator.Text & cmbWhereValue.Text)

                End If

        End Select

        SetError(lsvWhere, "")

        Dim lsv As ListViewItem = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub optAnd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAnd.CheckedChanged
        sOp = " AND "
    End Sub

    Private Sub optOr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOr.CheckedChanged
        sOp = " OR "
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvWhere.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvWhere.SelectedItems(0)

        lsv.Remove()

        If lsvWhere.Items.Count = 0 Then Return

        lsv = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Dim sCols As String

        Select Case nStep
            Case 1

                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
               

                Page3.BringToFront()
                cmdNext.Enabled = False
                cmdOK.Enabled = True

                Dim lsv As ListViewItem

                For Each lsv In lsvWhere.Items
                    sCols = sCols & lsv.Text & " "
                Next

                If sCols IsNot Nothing Then
                    If sCols <> "" Then sCols = sCols.Substring(0, sCols.Length - 1)
                End If


                If lsvWhere.Items.Count = 0 Then
                    txtFinal.Text = "DELETE FROM " & cmbTables.Text
                Else
                    txtFinal.Text = "DELETE FROM " & cmbTables.Text & " WHERE " & sCols
                End If



        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Select Case nStep
            Case 3
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Public Function AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sValues As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        'Connection String -> ProgramParameters
        'Table to delete from -> ProgramPath
        'Columns for deletion -> SendTo
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        For Each lsv As ListViewItem In lsvWhere.Items
            If lsv.Text <> "" Then sValues &= lsv.Text & "|"
        Next

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,SendTo,Msg,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'DBDelete'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(cmbTables.Text) & "'," & _
            "'" & SQLPrepare(sValues) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
        'basi

    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please provide a name for this task")
            Return
        ElseIf txtFinal.Text = "" Then
            SetError(txtFinal, "Please build the query for the deletion")
            Return
        End If

        Me.Close()
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sDeletes As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table to delete from -> ProgramPath
        'Columns for delete -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            cmbTables.Text = oRs("programpath").Value

            sDeletes = oRs("sendto").Value

            For Each s As String In sDeletes.Split("|")
                If s.Length > 0 Then
                    Dim lsv As ListViewItem = lsvWhere.Items.Add(s)
                End If
            Next

            txtFinal.Text = oRs("msg").Value

            oRs.Close()

            TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

            Me.ShowDialog()

            If UserCancel = True Then Return

            sDeletes = ""

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            For Each lsv As ListViewItem In lsvWhere.Items
                If lsv.Text <> "" Then sDeletes &= lsv.Text & "|"
            Next

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "ProgramPath = '" & SQLPrepare(cmbTables.Text) & "'," & _
                "SendTo = '" & SQLPrepare(sDeletes) & "'," & _
                "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)


            TaskExecutionPath1.setTaskRunWhen(nTaskID)
        End If
    End Sub

    Private Sub cmbColumns_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbColumns.DropDown
        Dim oData As clsMarsData = New clsMarsData

        oData.GetColumns(cmbColumns, UcDSN.cmbDSN.Text, cmbTables.Text, _
            UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page3.BringToFront()
        cmdNext.Enabled = False
        nStep = 3
        cmdOK.Enabled = True
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.m_eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.m_eventID
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        Dim oField As Object

        oField = Me.ActiveControl

        If (TypeOf oField Is TextBox) Or (TypeOf oField Is ComboBox) Then
            oField.SelectedText = oItem._GetDataItem(Me.m_eventID)
        End If
    End Sub
End Class
