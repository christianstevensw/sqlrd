<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskSMS
    Inherits sqlrd.frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskSMS))
        Me.grpSMS = New System.Windows.Forms.GroupBox()
        Me.Label23 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMSMsg = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSMSNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSMSTo = New DevComponents.DotNetBar.ButtonX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem()
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem()
        Me.mnuSignature = New System.Windows.Forms.MenuItem()
        Me.mnuAttachment = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.mnuSpell = New System.Windows.Forms.MenuItem()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.grpSMS.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpSMS
        '
        Me.grpSMS.BackColor = System.Drawing.Color.Transparent
        Me.grpSMS.Controls.Add(Me.Label23)
        Me.grpSMS.Controls.Add(Me.txtSMSMsg)
        Me.grpSMS.Controls.Add(Me.txtSMSNumber)
        Me.grpSMS.Controls.Add(Me.cmdSMSTo)
        Me.grpSMS.Location = New System.Drawing.Point(3, 3)
        Me.grpSMS.Name = "grpSMS"
        Me.grpSMS.Size = New System.Drawing.Size(414, 320)
        Me.grpSMS.TabIndex = 1
        Me.grpSMS.TabStop = False
        Me.grpSMS.Text = "SMS"
        '
        'Label23
        '
        '
        '
        '
        Me.Label23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(8, 296)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(168, 16)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Characters Left: "
        '
        'txtSMSMsg
        '
        '
        '
        '
        Me.txtSMSMsg.Border.Class = "TextBoxBorder"
        Me.txtSMSMsg.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSMsg.Location = New System.Drawing.Point(8, 56)
        Me.txtSMSMsg.MaxLength = 160
        Me.txtSMSMsg.Multiline = True
        Me.txtSMSMsg.Name = "txtSMSMsg"
        Me.txtSMSMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSMSMsg.Size = New System.Drawing.Size(376, 232)
        Me.txtSMSMsg.TabIndex = 2
        Me.txtSMSMsg.Tag = "memo"
        '
        'txtSMSNumber
        '
        '
        '
        '
        Me.txtSMSNumber.Border.Class = "TextBoxBorder"
        Me.txtSMSNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtSMSNumber.Location = New System.Drawing.Point(88, 24)
        Me.txtSMSNumber.Name = "txtSMSNumber"
        Me.txtSMSNumber.Size = New System.Drawing.Size(296, 20)
        Me.txtSMSNumber.TabIndex = 1
        Me.txtSMSNumber.Tag = "memo"
        '
        'cmdSMSTo
        '
        Me.cmdSMSTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSMSTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSMSTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSMSTo.Image = CType(resources.GetObject("cmdSMSTo.Image"), System.Drawing.Image)
        Me.cmdSMSTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSMSTo.Location = New System.Drawing.Point(8, 24)
        Me.cmdSMSTo.Name = "cmdSMSTo"
        Me.cmdSMSTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdSMSTo.TabIndex = 0
        Me.cmdSMSTo.Text = "To..."
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem13, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuDefMsg, Me.mnuSignature, Me.mnuAttachment})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 5
        Me.mnuDefMsg.Text = "Default Message"
        '
        'mnuSignature
        '
        Me.mnuSignature.Index = 6
        Me.mnuSignature.Text = "Default Signature"
        '
        'mnuAttachment
        '
        Me.mnuAttachment.Index = 7
        Me.mnuAttachment.Text = "Default Attachment"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 10
        Me.MenuItem13.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 20)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(66, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(351, 20)
        Me.txtName.TabIndex = 0
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(263, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 49
        Me.btnOK.Text = "&OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Location = New System.Drawing.Point(344, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 50
        Me.btnCancel.Text = "&Cancel"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 27)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(422, 349)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 51
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.grpSMS)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(422, 325)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(422, 325)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(422, 27)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.btnCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 376)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(422, 30)
        Me.FlowLayoutPanel2.TabIndex = 53
        '
        'frmTaskSMS
        '
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(422, 406)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MinimizeBox = False
        Me.Name = "frmTaskSMS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Send an SMS Text Message"
        Me.grpSMS.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grpSMS As System.Windows.Forms.GroupBox
    Friend WithEvents Label23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMSMsg As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMSNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdSMSTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSignature As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAttachment As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel

End Class
