﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskBuildWorkBook
    Inherits frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskBuildWorkBook))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.btnUp = New DevComponents.DotNetBar.ButtonX()
        Me.btnDown = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.lsvDataSets = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkReplace = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtOutput = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(69, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(330, 21)
        Me.txtName.TabIndex = 20
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 21)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Task Name"
        '
        'btnUp
        '
        Me.btnUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnUp.Location = New System.Drawing.Point(313, 217)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(75, 23)
        Me.btnUp.TabIndex = 18
        '
        'btnDown
        '
        Me.btnDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDown.Location = New System.Drawing.Point(313, 246)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(75, 23)
        Me.btnDown.TabIndex = 19
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(313, 78)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 17
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(313, 49)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 16
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(313, 20)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.TabIndex = 15
        Me.cmdAdd.Text = "&Add"
        '
        'lsvDataSets
        '
        '
        '
        '
        Me.lsvDataSets.Border.Class = "ListViewBorder"
        Me.lsvDataSets.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDataSets.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvDataSets.FullRowSelect = True
        Me.lsvDataSets.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvDataSets.HideSelection = False
        Me.lsvDataSets.LabelEdit = True
        Me.lsvDataSets.Location = New System.Drawing.Point(6, 20)
        Me.lsvDataSets.Name = "lsvDataSets"
        Me.lsvDataSets.Size = New System.Drawing.Size(301, 251)
        Me.lsvDataSets.TabIndex = 0
        Me.lsvDataSets.UseCompatibleStateImageBehavior = False
        Me.lsvDataSets.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 231
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(247, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 55
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(328, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 56
        Me.cmdCancel.Text = "&Cancel"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.btnUp)
        Me.GroupBox1.Controls.Add(Me.lsvDataSets)
        Me.GroupBox1.Controls.Add(Me.btnDown)
        Me.GroupBox1.Controls.Add(Me.cmdAdd)
        Me.GroupBox1.Controls.Add(Me.cmdDelete)
        Me.GroupBox1.Controls.Add(Me.cmdEdit)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(396, 278)
        Me.GroupBox1.TabIndex = 57
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Add Datasets for worksheets"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.chkReplace)
        Me.GroupBox2.Controls.Add(Me.btnBrowse)
        Me.GroupBox2.Controls.Add(Me.txtOutput)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 287)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(396, 82)
        Me.GroupBox2.TabIndex = 58
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Output WorkBook"
        '
        'chkReplace
        '
        Me.chkReplace.AutoSize = True
        '
        '
        '
        Me.chkReplace.BackgroundStyle.Class = ""
        Me.chkReplace.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkReplace.Location = New System.Drawing.Point(77, 53)
        Me.chkReplace.Name = "chkReplace"
        Me.chkReplace.Size = New System.Drawing.Size(118, 16)
        Me.chkReplace.TabIndex = 4
        Me.chkReplace.Text = "Replace if file exists"
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(355, 25)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(33, 23)
        Me.btnBrowse.TabIndex = 3
        Me.btnBrowse.Text = "..."
        '
        'txtOutput
        '
        '
        '
        '
        Me.txtOutput.Border.Class = "TextBoxBorder"
        Me.txtOutput.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtOutput.Location = New System.Drawing.Point(77, 26)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.Size = New System.Drawing.Size(272, 21)
        Me.txtOutput.TabIndex = 2
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(6, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 22)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Save File As"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 59
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 29)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(406, 402)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 60
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(406, 376)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(406, 376)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(406, 29)
        Me.FlowLayoutPanel1.TabIndex = 61
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 431)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(406, 30)
        Me.FlowLayoutPanel2.TabIndex = 61
        '
        'frmTaskBuildWorkBook
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(406, 461)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskBuildWorkBook"
        Me.Text = "Build Workbook"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lsvDataSets As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents btnUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtOutput As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents chkReplace As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
End Class
