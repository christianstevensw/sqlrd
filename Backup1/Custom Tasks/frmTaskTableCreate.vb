Public Class frmTaskTableCreate
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Dim nStep As Integer = 1

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtTable As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtColumnName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbColumnType As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtColumnSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvColumns As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdSkip As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskTableCreate))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.lsvColumns = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.txtColumnSize = New System.Windows.Forms.NumericUpDown()
        Me.cmbColumnType = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtTable = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtColumnName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Page3 = New System.Windows.Forms.GroupBox()
        Me.txtFinal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdSkip = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Page3.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(3, 29)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(345, 20)
        Me.txtName.TabIndex = 26
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 20)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(336, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 24
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(255, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 23
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 305)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(8, 7)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(392, 260)
        Me.Page1.TabIndex = 2
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 143)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 22)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(368, 119)
        Me.UcDSN.TabIndex = 0
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvColumns)
        Me.Page2.Controls.Add(Me.cmdAddWhere)
        Me.Page2.Controls.Add(Me.cmdRemoveWhere)
        Me.Page2.Controls.Add(Me.txtColumnSize)
        Me.Page2.Controls.Add(Me.cmbColumnType)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.txtTable)
        Me.Page2.Controls.Add(Me.Label1)
        Me.Page2.Controls.Add(Me.txtColumnName)
        Me.Page2.Controls.Add(Me.Label4)
        Me.Page2.Controls.Add(Me.Label5)
        Me.Page2.Location = New System.Drawing.Point(8, 7)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(392, 260)
        Me.Page2.TabIndex = 5
        Me.Page2.TabStop = False
        '
        'lsvColumns
        '
        '
        '
        '
        Me.lsvColumns.Border.Class = "ListViewBorder"
        Me.lsvColumns.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvColumns.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvColumns.Location = New System.Drawing.Point(8, 126)
        Me.lsvColumns.Name = "lsvColumns"
        Me.lsvColumns.Size = New System.Drawing.Size(376, 127)
        Me.lsvColumns.TabIndex = 13
        Me.lsvColumns.UseCompatibleStateImageBehavior = False
        Me.lsvColumns.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Column Headers"
        Me.ColumnHeader1.Width = 355
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(344, 97)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdAddWhere.TabIndex = 12
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(248, 97)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdRemoveWhere.TabIndex = 11
        '
        'txtColumnSize
        '
        Me.txtColumnSize.Location = New System.Drawing.Point(296, 74)
        Me.txtColumnSize.Name = "txtColumnSize"
        Me.txtColumnSize.Size = New System.Drawing.Size(88, 20)
        Me.txtColumnSize.TabIndex = 4
        '
        'cmbColumnType
        '
        Me.cmbColumnType.ItemHeight = 13
        Me.cmbColumnType.Items.AddRange(New Object() {"NUMBER", "DECIMAL", "TEXT", "MEMO", "DATETIME"})
        Me.cmbColumnType.Location = New System.Drawing.Point(168, 74)
        Me.cmbColumnType.Name = "cmbColumnType"
        Me.cmbColumnType.Size = New System.Drawing.Size(121, 21)
        Me.cmbColumnType.TabIndex = 3
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Column Name"
        '
        'txtTable
        '
        '
        '
        '
        Me.txtTable.Border.Class = "TextBoxBorder"
        Me.txtTable.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTable.Location = New System.Drawing.Point(8, 30)
        Me.txtTable.Name = "txtTable"
        Me.txtTable.Size = New System.Drawing.Size(376, 20)
        Me.txtTable.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(216, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Table Name"
        '
        'txtColumnName
        '
        '
        '
        '
        Me.txtColumnName.Border.Class = "TextBoxBorder"
        Me.txtColumnName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtColumnName.Location = New System.Drawing.Point(8, 74)
        Me.txtColumnName.Name = "txtColumnName"
        Me.txtColumnName.Size = New System.Drawing.Size(144, 20)
        Me.txtColumnName.TabIndex = 1
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(168, 59)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Column Type"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(296, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 15)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Column Size"
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.txtFinal)
        Me.Page3.Controls.Add(Me.Label6)
        Me.Page3.Location = New System.Drawing.Point(8, 7)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(392, 260)
        Me.Page3.TabIndex = 6
        Me.Page3.TabStop = False
        '
        'txtFinal
        '
        '
        '
        '
        Me.txtFinal.Border.Class = "TextBoxBorder"
        Me.txtFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFinal.Location = New System.Drawing.Point(4, 30)
        Me.txtFinal.Multiline = True
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFinal.Size = New System.Drawing.Size(384, 223)
        Me.txtFinal.TabIndex = 3
        Me.txtFinal.Tag = "memo"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(4, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Finalised Query"
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(8, 275)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 21)
        Me.cmdBack.TabIndex = 4
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(344, 275)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 21)
        Me.cmdNext.TabIndex = 3
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdSkip
        '
        Me.cmdSkip.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSkip.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(8, 3)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 23)
        Me.cmdSkip.TabIndex = 28
        Me.cmdSkip.Text = "&Skip"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 56)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(414, 337)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 52
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(414, 313)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(414, 337)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdSkip)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 393)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(414, 29)
        Me.FlowLayoutPanel1.TabIndex = 53
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(89, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(160, 23)
        Me.Label7.TabIndex = 29
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(414, 56)
        Me.FlowLayoutPanel2.TabIndex = 53
        '
        'frmTaskTableCreate
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(414, 422)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskTableCreate"
        Me.Text = "Create  a new table"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.Page2.ResumeLayout(False)
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Page3.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskTableCreate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page1.BringToFront()
        FormatForWinXP(Me)
    End Sub

    Private Sub cmbColumnType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumnType.SelectedIndexChanged
        If cmbColumnType.Text = "TEXT" Then
            txtColumnSize.Enabled = True
            txtColumnSize.Text = 50
        Else
            txtColumnSize.Enabled = False
            txtColumnSize.Text = 0
        End If

        SetError(cmbColumnType, "")
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click

        Dim sName As String

        If cmbColumnType.Text = "TEXT" And txtColumnSize.Text = "0" Then
            SetError(txtColumnSize, "Please set the correct column size")
            txtColumnSize.Focus()
            Return
        ElseIf txtColumnName.Text = "" Then
            SetError(txtColumnName, "Please enter the column name")
            txtColumnName.Focus()
            Return
        ElseIf cmbColumnType.Text = "" Then
            SetError(cmbColumnType, "Please select the column")
            cmbColumnType.Focus()
            Return
        Else
            For Each lsv As ListViewItem In lsvColumns.Items
                If txtColumnName.Text.ToLower = lsv.Text.ToLower Then
                    SetError(txtColumnName, "The column name already exists in the table definition")
                    txtColumnName.Focus()
                    Return
                End If
            Next
        End If

        If txtColumnName.Text.IndexOf(" ") > 0 Then
            sName = "[" & txtColumnName.Text.ToUpper & "]"
        Else
            sName = txtColumnName.Text.ToUpper
        End If

        Select Case cmbColumnType.Text
            Case "TEXT"
                lsvColumns.Items.Add(sName & " VARCHAR(" & txtColumnSize.Text & ")")
            Case "NUMBER"
                lsvColumns.Items.Add(sName & " INTEGER")
            Case "DECIMAL"
                lsvColumns.Items.Add(sName & " FLOAT")
            Case "DATETIME"
                lsvColumns.Items.Add(sName & " DATETIME")
            Case "MEMO"
                lsvColumns.Items.Add(sName & " TEXT")
        End Select
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf txtFinal.Text = "" Then
            SetError(txtFinal, "Please build the query")
            txtFinal.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvColumns.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvColumns.SelectedItems(0)

        lsv.Remove()
    End Sub

    Public Function AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sValues As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'Connection String -> ProgramParameters
        'New Table Name -> ProgramPath 
        'Columns for Table -> SendTo
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        For Each lsv As ListViewItem In lsvColumns.Items
            If lsv.Text <> "" Then sValues &= lsv.Text & "|"
        Next

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,SendTo,Msg,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'DBCreateTable'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(txtTable.Text) & "'," & _
            "'" & SQLPrepare(sValues) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID

    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        'Connection String -> ProgramParameters
        'New Table Name -> ProgramPath 
        'Columns for Table -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            txtTable.Text = oRs("programpath").Value

            sInserts = oRs("sendto").Value

            For Each s As String In sInserts.Split("|")
                If s.Length > 0 Then
                    Dim lsv As ListViewItem = lsvColumns.Items.Add(s)
                End If
            Next

            txtFinal.Text = oRs("msg").Value

            oRs.Close()

            TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

            Me.ShowDialog()

            If UserCancel = True Then Return

            sInserts = ""

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            For Each lsv As ListViewItem In lsvColumns.Items
                If lsv.Text <> "" Then sInserts &= lsv.Text & "|"
            Next

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "ProgramPath = '" & SQLPrepare(txtTable.Text) & "'," & _
                "SendTo = '" & SQLPrepare(sInserts) & "'," & _
                "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)

            'for editing a task, set its run type
            ' _Delay(2)

            TaskExecutionPath1.setTaskRunWhen(nTaskID)
        End If
    End Sub


    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
                If lsvColumns.Items.Count = 0 Then
                    SetError(lsvColumns, "Please add the table definitions")
                    Return
                End If

                Page3.BringToFront()

                cmdNext.Enabled = False
                cmdOK.Enabled = True

                txtFinal.Text = "CREATE TABLE " & txtTable.Text & " ("

                For Each lsv As ListViewItem In lsvColumns.Items
                    txtFinal.Text &= lsv.Text & ","
                Next

                txtFinal.Text = txtFinal.Text.Substring(0, txtFinal.Text.Length - 1)

                txtFinal.Text &= ")"

                txtFinal.Text = txtFinal.Text.ToUpper

                cmdOK.Enabled = True
        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Select Case nStep
            Case 3
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(txtName, "")
    End Sub

    Private Sub txtColumnName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtColumnName.TextChanged
        SetError(txtColumnName, "")
    End Sub

    Private Sub txtColumnSize_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtColumnSize.ValueChanged
        SetError(txtColumnSize, "")
    End Sub

    Private Sub txtFinal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFinal.TextChanged
        SetError(txtFinal, "")
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdSkip.Enabled = True
            cmdNext.Enabled = True
        Else
            cmdNext.Enabled = False
        End If
    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page3.BringToFront()
        cmdNext.Enabled = False
        nStep = 3
        cmdOK.Enabled = True
    End Sub
End Class
