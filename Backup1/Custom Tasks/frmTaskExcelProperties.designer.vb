﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskExcelProperties
    Inherits frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl2 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoTitle = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoAuthor = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtInfoKeywords = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.txtInfoProducer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem5 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkReadOnly = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtExcelPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(500, 28)
        Me.FlowLayoutPanel2.TabIndex = 54
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 21)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(72, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(414, 21)
        Me.txtName.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label13)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtInfoProducer)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 390)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(500, 29)
        Me.FlowLayoutPanel1.TabIndex = 55
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(422, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(341, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'SuperTabControl2
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl2.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl2.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl2.ControlBox.Name = ""
        Me.SuperTabControl2.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl2.ControlBox.MenuBox, Me.SuperTabControl2.ControlBox.CloseBox})
        Me.SuperTabControl2.Controls.Add(Me.SuperTabControlPanel5)
        Me.SuperTabControl2.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl2.Location = New System.Drawing.Point(6, 38)
        Me.SuperTabControl2.Name = "SuperTabControl2"
        Me.SuperTabControl2.ReorderTabsEnabled = True
        Me.SuperTabControl2.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl2.SelectedTabIndex = 0
        Me.SuperTabControl2.Size = New System.Drawing.Size(488, 291)
        Me.SuperTabControl2.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl2.TabIndex = 56
        Me.SuperTabControl2.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem4, Me.SuperTabItem5})
        Me.SuperTabControl2.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.SuperTabControl2.Text = "stabPDF"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(488, 267)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.SuperTabItem5
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox2.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox2.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox2.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(474, 260)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Enabled = False
        Me.txtInfoCreated.Location = New System.Drawing.Point(133, 114)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.Class = ""
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 20)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        '
        '
        '
        Me.txtInfoTitle.Border.Class = "TextBoxBorder"
        Me.txtInfoTitle.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoTitle.Location = New System.Drawing.Point(133, 18)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        '
        '
        '
        Me.txtInfoAuthor.Border.Class = "TextBoxBorder"
        Me.txtInfoAuthor.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoAuthor.Location = New System.Drawing.Point(133, 50)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        '
        '
        '
        Me.txtInfoSubject.Border.Class = "TextBoxBorder"
        Me.txtInfoSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoSubject.Location = New System.Drawing.Point(133, 82)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(325, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        '
        '
        '
        Me.txtInfoKeywords.Border.Class = "TextBoxBorder"
        Me.txtInfoKeywords.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoKeywords.Location = New System.Drawing.Point(133, 146)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(325, 108)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.Class = ""
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(6, 52)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.Class = ""
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(235, 3)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        Me.Label13.Visible = False
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.Class = ""
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(6, 146)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.Class = ""
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(6, 116)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        '
        '
        '
        Me.txtInfoProducer.Border.Class = "TextBoxBorder"
        Me.txtInfoProducer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtInfoProducer.Location = New System.Drawing.Point(206, 3)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(23, 21)
        Me.txtInfoProducer.TabIndex = 20
        Me.txtInfoProducer.Visible = False
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.Class = ""
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(6, 84)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'SuperTabItem5
        '
        Me.SuperTabItem5.AttachedControl = Me.SuperTabControlPanel5
        Me.SuperTabItem5.GlobalItem = False
        Me.SuperTabItem5.Name = "SuperTabItem5"
        Me.SuperTabItem5.Text = "File Summary"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(488, 267)
        Me.SuperTabControlPanel4.TabIndex = 1
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkReadOnly)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(475, 275)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Workbook Password"
        '
        'chkReadOnly
        '
        '
        '
        '
        Me.chkReadOnly.BackgroundStyle.Class = ""
        Me.chkReadOnly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkReadOnly.Location = New System.Drawing.Point(8, 67)
        Me.chkReadOnly.Name = "chkReadOnly"
        Me.chkReadOnly.Size = New System.Drawing.Size(100, 23)
        Me.chkReadOnly.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkReadOnly.TabIndex = 1
        Me.chkReadOnly.Text = "Read Only"
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(461, 21)
        Me.txtPassword.TabIndex = 0
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.LabelX2.Location = New System.Drawing.Point(8, 24)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(112, 16)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Password"
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Excel Security"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(500, 362)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 57
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel1.Controls.Add(Me.SuperTabControl2)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(500, 336)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtExcelPath)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnBrowse)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(483, 29)
        Me.FlowLayoutPanel3.TabIndex = 1
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(60, 21)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Excel File"
        '
        'txtExcelPath
        '
        '
        '
        '
        Me.txtExcelPath.Border.Class = "TextBoxBorder"
        Me.txtExcelPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExcelPath.Location = New System.Drawing.Point(69, 3)
        Me.txtExcelPath.Name = "txtExcelPath"
        Me.txtExcelPath.Size = New System.Drawing.Size(348, 21)
        Me.txtExcelPath.TabIndex = 1
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(423, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(52, 21)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "..."
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(500, 362)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(11, 0)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 1
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'frmTaskExcelProperties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 419)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskExcelProperties"
        Me.Text = "Edit Excel Properties"
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.SuperTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabControl2 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoTitle As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoAuthor As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtInfoKeywords As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtInfoProducer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents SuperTabItem5 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents chkReadOnly As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtExcelPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
End Class
