﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTaskHouseKeeping
    Inherits frmTaskMaster

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtAge = New DevComponents.Editors.IntegerInput()
        Me.cmbAgeUnit = New System.Windows.Forms.ComboBox()
        Me.chkSubFolders = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.stabDirectories = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ucFTP = New sqlrd.ucFTPDetails()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtLocalDir = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabItem3 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        CType(Me.txtAge, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabDirectories, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabDirectories.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(570, 28)
        Me.FlowLayoutPanel1.TabIndex = 12
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 21)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(84, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(472, 21)
        Me.txtName.TabIndex = 5
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 357)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(570, 29)
        Me.FlowLayoutPanel2.TabIndex = 13
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(492, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 7
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(411, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 6
        Me.cmdOK.Text = "&OK"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(570, 329)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTabControl1.TabIndex = 14
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.FlowLayoutPanel4)
        Me.SuperTabControlPanel1.Controls.Add(Me.stabDirectories)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(570, 303)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.LabelX2)
        Me.FlowLayoutPanel4.Controls.Add(Me.txtAge)
        Me.FlowLayoutPanel4.Controls.Add(Me.cmbAgeUnit)
        Me.FlowLayoutPanel4.Controls.Add(Me.chkSubFolders)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 237)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(418, 59)
        Me.FlowLayoutPanel4.TabIndex = 1
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.Class = ""
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 3)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(120, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Delete files older than"
        '
        'txtAge
        '
        '
        '
        '
        Me.txtAge.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtAge.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAge.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtAge.Location = New System.Drawing.Point(129, 3)
        Me.txtAge.MinValue = 1
        Me.txtAge.Name = "txtAge"
        Me.txtAge.ShowUpDown = True
        Me.txtAge.Size = New System.Drawing.Size(80, 21)
        Me.txtAge.TabIndex = 1
        Me.txtAge.Value = 1
        '
        'cmbAgeUnit
        '
        Me.cmbAgeUnit.FormattingEnabled = True
        Me.cmbAgeUnit.Items.AddRange(New Object() {"Seconds", "Minutes", "Hours", "Days"})
        Me.cmbAgeUnit.Location = New System.Drawing.Point(215, 3)
        Me.cmbAgeUnit.Name = "cmbAgeUnit"
        Me.cmbAgeUnit.Size = New System.Drawing.Size(121, 21)
        Me.cmbAgeUnit.TabIndex = 2
        Me.cmbAgeUnit.Text = "Days"
        '
        'chkSubFolders
        '
        '
        '
        '
        Me.chkSubFolders.BackgroundStyle.Class = ""
        Me.chkSubFolders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSubFolders.Location = New System.Drawing.Point(3, 32)
        Me.chkSubFolders.Name = "chkSubFolders"
        Me.chkSubFolders.Size = New System.Drawing.Size(167, 23)
        Me.chkSubFolders.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSubFolders.TabIndex = 3
        Me.chkSubFolders.Text = "Include subfolders"
        '
        'stabDirectories
        '
        '
        '
        '
        '
        '
        '
        Me.stabDirectories.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabDirectories.ControlBox.MenuBox.Name = ""
        Me.stabDirectories.ControlBox.Name = ""
        Me.stabDirectories.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabDirectories.ControlBox.MenuBox, Me.stabDirectories.ControlBox.CloseBox})
        Me.stabDirectories.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabDirectories.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabDirectories.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabDirectories.Location = New System.Drawing.Point(0, 0)
        Me.stabDirectories.Name = "stabDirectories"
        Me.stabDirectories.ReorderTabsEnabled = True
        Me.stabDirectories.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabDirectories.SelectedTabIndex = 0
        Me.stabDirectories.Size = New System.Drawing.Size(570, 231)
        Me.stabDirectories.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabDirectories.TabIndex = 0
        Me.stabDirectories.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem3, Me.SuperTabItem4})
        Me.stabDirectories.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabDirectories.Text = "SuperTabControl2"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.ucFTP)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(570, 207)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'ucFTP
        '
        Me.ucFTP.BackColor = System.Drawing.SystemColors.Control
        Me.ucFTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFTP.Location = New System.Drawing.Point(3, 3)
        Me.ucFTP.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
    "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;AsciiMode=0;"
        Me.ucFTP.m_ftpSuccess = False
        Me.ucFTP.m_ShowBrowse = True
        Me.ucFTP.Name = "ucFTP"
        Me.ucFTP.Size = New System.Drawing.Size(553, 197)
        Me.ucFTP.TabIndex = 0
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Remote (FTP) Directory"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(570, 207)
        Me.SuperTabControlPanel3.TabIndex = 1
        Me.SuperTabControlPanel3.TabItem = Me.SuperTabItem3
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtLocalDir)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnBrowse)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(3, 12)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(564, 31)
        Me.FlowLayoutPanel3.TabIndex = 0
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(34, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Path"
        '
        'txtLocalDir
        '
        '
        '
        '
        Me.txtLocalDir.Border.Class = "TextBoxBorder"
        Me.txtLocalDir.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLocalDir.Location = New System.Drawing.Point(43, 3)
        Me.txtLocalDir.Name = "txtLocalDir"
        Me.txtLocalDir.Size = New System.Drawing.Size(442, 21)
        Me.txtLocalDir.TabIndex = 1
        '
        'btnBrowse
        '
        Me.btnBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowse.Location = New System.Drawing.Point(491, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(56, 21)
        Me.btnBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnBrowse.TabIndex = 2
        Me.btnBrowse.Text = "..."
        '
        'SuperTabItem3
        '
        Me.SuperTabItem3.AttachedControl = Me.SuperTabControlPanel3
        Me.SuperTabItem3.GlobalItem = False
        Me.SuperTabItem3.Name = "SuperTabItem3"
        Me.SuperTabItem3.Text = "Local Directory"
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(570, 329)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 149)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'frmTaskHouseKeeping
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 386)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Name = "frmTaskHouseKeeping"
        Me.Text = "Folder Housekeeping"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        CType(Me.txtAge, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabDirectories, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabDirectories.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabDirectories As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem3 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtLocalDir As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ucFTP As sqlrd.ucFTPDetails
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAge As DevComponents.Editors.IntegerInput
    Friend WithEvents cmbAgeUnit As System.Windows.Forms.ComboBox
    Friend WithEvents chkSubFolders As DevComponents.DotNetBar.Controls.CheckBoxX
End Class
