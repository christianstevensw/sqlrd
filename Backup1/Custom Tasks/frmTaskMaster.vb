
#If DEBUG Then
Public Class frmTaskMaster
#Else
Public MustInherit Class frmTaskMaster
#End If
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        Me.EnableGlass = False
        setControlStyle(Me)
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.SuspendLayout()
        '
        'frmTaskMaster
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(290, 269)
        Me.ControlBox = False
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmTaskMaster"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Master"
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    'Private Sub frmTaskMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    '    setControlStyle(Me)

    '    Me.EnableGlass = False

    'End Sub

    Private Sub setControlStyle(ByVal cont As Control)
        For Each ctrl As Control In cont.Controls
            If TypeOf ctrl Is DevComponents.DotNetBar.ButtonX Then
                CType(ctrl, DevComponents.DotNetBar.ButtonX).ColorTable = Office2007ColorTable
            ElseIf TypeOf ctrl Is Label Then
                CType(ctrl, Label).BackColor = Color.Transparent
            ElseIf TypeOf ctrl Is GroupBox Or TypeOf ctrl Is Panel Then
                setControlStyle(ctrl)
            End If
        Next
    End Sub
End Class
