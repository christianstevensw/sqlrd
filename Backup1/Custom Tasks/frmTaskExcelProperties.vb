﻿Public Class frmTaskExcelProperties
    Dim eventID As Integer
    Dim eventBased As Boolean
    Dim m_taskID As Integer
    Dim userCancel As Boolean = False

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Using ofd As OpenFileDialog = New OpenFileDialog
            ofd.Title = "Please select the Excel file to manipulate"
            ofd.Filter = "Excel Files|*.xls|Excel 2007 Files|*.xlsx|All Files|*.*"
            ofd.Multiselect = False

            If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtExcelPath.Text = ofd.FileName
            End If
        End Using
    End Sub
    Private Sub frmTaskExcelProperties_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtExcelPath)
        setupForDragAndDrop(txtInfoAuthor)
        setupForDragAndDrop(txtInfoCreated)
        setupForDragAndDrop(txtInfoKeywords)
        setupForDragAndDrop(txtInfoProducer)
        setupForDragAndDrop(txtInfoSubject)
        setupForDragAndDrop(txtInfoTitle)
        setupForDragAndDrop(txtPassword)

        showInserter(Me, m_eventID, m_eventBased)
    End Sub

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As System.EventArgs) Handles cmdOK.Click
        '//lets validate this shiznit
        If txtName.Text = "" Then
            setError(txtName, "Please enter a name for this task")
            txtName.Focus()
            Return
        ElseIf txtExcelPath.Text = "" Then
            setError(txtExcelPath, "Please select the PDF to manipulate")
            txtExcelPath.Focus()
            Return
        End If

        Close()
    End Sub


    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData

        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,OrderID"

        sVals = nTaskID & "," & _
        ScheduleID & "," & _
        "'EditExcel'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtExcelPath.Text.Replace("'", "''") & "'," & _
              oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        sCols = "OptionID,TaskID,PDFPassword," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer,makereadonly"

        sVals = clsMarsData.CreateDataID("taskoptions", "optionid") & "," & nTaskID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtPassword.Text)) & "'," & _
        "'" & SQLPrepare(Me.txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(Me.txtInfoProducer.Text) & "'," & _
        Convert.ToInt32(chkReadOnly.Checked)

        SQL = "INSERT INTO TaskOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)

        Return nTaskID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        m_taskID = nTaskID

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            txtExcelPath.Text = oRs("programpath").Value

        End If

        oRs.Close()

        Try
            oRs = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then

                    With Me
                        .txtPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                        .txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                        .txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                        .txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                        .txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)

                        Try
                            .txtInfoCreated.Value = Convert.ToDateTime(oRs("infocreated").Value)
                        Catch ex As Exception
                            .txtInfoCreated.Value = Now
                        End Try

                        .txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                        chkReadOnly.Checked = Convert.ToInt32(IsNull(oRs("makereadonly").Value, 0))
                    End With
                End If
            End If
        Catch : End Try

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtExcelPath.Text.Replace("'", "''") & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        With Me
            SQL = "PDFPassword = '" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "'," & _
            "InfoTitle = '" & SQLPrepare(.txtInfoTitle.Text) & "'," & _
            "InfoAuthor = '" & SQLPrepare(.txtInfoAuthor.Text) & "'," & _
            "InfoSubject = '" & SQLPrepare(.txtInfoSubject.Text) & "'," & _
            "InfoKeyWords = '" & SQLPrepare(.txtInfoKeywords.Text) & "'," & _
            "InfoCreated = '" & ConDateTime(.txtInfoCreated.Value) & "'," & _
            "InfoProducer = '" & SQLPrepare(.txtInfoProducer.Text) & "'," & _
            "makereadonly = " & Convert.ToInt32(chkReadOnly.Checked)

        End With

        SQL = "UPDATE TaskOptions SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub
End Class