Public Class frmTaskDBUpdate
    Inherits sqlrd.frmTaskMaster
    Dim nStep As Int32
    Dim sOp As String
    Dim UserCancel As Boolean
    Dim eventBased As Boolean = False
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Dim eventID As Integer = 99999

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTables As System.Windows.Forms.ComboBox
    Friend WithEvents cmbUpdateColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbUpdateValue As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddUpdate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvUpdate As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbWhere As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbWhereValue As System.Windows.Forms.ComboBox
    Friend WithEvents optOr As System.Windows.Forms.RadioButton
    Friend WithEvents optAnd As System.Windows.Forms.RadioButton
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvWhere As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Page4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFinal As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdSkip As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskDBUpdate))
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page4 = New System.Windows.Forms.GroupBox()
        Me.txtFinal = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Page3 = New System.Windows.Forms.GroupBox()
        Me.lsvWhere = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.optAnd = New System.Windows.Forms.RadioButton()
        Me.optOr = New System.Windows.Forms.RadioButton()
        Me.cmbWhere = New System.Windows.Forms.ComboBox()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbWhereValue = New System.Windows.Forms.ComboBox()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.lsvUpdate = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmbTables = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmbUpdateColumn = New System.Windows.Forms.ComboBox()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.cmbUpdateValue = New System.Windows.Forms.ComboBox()
        Me.cmdAddUpdate = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemove = New DevComponents.DotNetBar.ButtonX()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdSkip = New DevComponents.DotNetBar.ButtonX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1.SuspendLayout()
        Me.Page4.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.Page2.SuspendLayout()
        Me.Page1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Dock = System.Windows.Forms.DockStyle.Top
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(81, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(386, 21)
        Me.txtName.TabIndex = 9
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 20)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page4)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 253)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'Page4
        '
        Me.Page4.BackColor = System.Drawing.Color.Transparent
        Me.Page4.Controls.Add(Me.txtFinal)
        Me.Page4.Controls.Add(Me.Label7)
        Me.Page4.Location = New System.Drawing.Point(8, 7)
        Me.Page4.Name = "Page4"
        Me.Page4.Size = New System.Drawing.Size(448, 208)
        Me.Page4.TabIndex = 5
        Me.Page4.TabStop = False
        '
        'txtFinal
        '
        '
        '
        '
        Me.txtFinal.Border.Class = "TextBoxBorder"
        Me.txtFinal.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFinal.Location = New System.Drawing.Point(8, 30)
        Me.txtFinal.Multiline = True
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFinal.Size = New System.Drawing.Size(416, 171)
        Me.txtFinal.TabIndex = 1
        Me.txtFinal.Tag = "memo"
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 29)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Completed SQL Script"
        '
        'Page3
        '
        Me.Page3.BackColor = System.Drawing.Color.Transparent
        Me.Page3.Controls.Add(Me.lsvWhere)
        Me.Page3.Controls.Add(Me.cmdAddWhere)
        Me.Page3.Controls.Add(Me.optAnd)
        Me.Page3.Controls.Add(Me.optOr)
        Me.Page3.Controls.Add(Me.cmbWhere)
        Me.Page3.Controls.Add(Me.Label6)
        Me.Page3.Controls.Add(Me.cmbOperator)
        Me.Page3.Controls.Add(Me.cmbWhereValue)
        Me.Page3.Controls.Add(Me.cmdRemoveWhere)
        Me.Page3.Location = New System.Drawing.Point(8, 7)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(448, 208)
        Me.Page3.TabIndex = 4
        Me.Page3.TabStop = False
        '
        'lsvWhere
        '
        '
        '
        '
        Me.lsvWhere.Border.Class = "ListViewBorder"
        Me.lsvWhere.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvWhere.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvWhere.Location = New System.Drawing.Point(8, 89)
        Me.lsvWhere.Name = "lsvWhere"
        Me.lsvWhere.Size = New System.Drawing.Size(416, 112)
        Me.lsvWhere.TabIndex = 5
        Me.lsvWhere.UseCompatibleStateImageBehavior = False
        Me.lsvWhere.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Where"
        Me.ColumnHeader1.Width = 406
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(288, 58)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdAddWhere.TabIndex = 4
        '
        'optAnd
        '
        Me.optAnd.Checked = True
        Me.optAnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAnd.Location = New System.Drawing.Point(184, 52)
        Me.optAnd.Name = "optAnd"
        Me.optAnd.Size = New System.Drawing.Size(48, 22)
        Me.optAnd.TabIndex = 3
        Me.optAnd.TabStop = True
        Me.optAnd.Text = "And"
        '
        'optOr
        '
        Me.optOr.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optOr.Location = New System.Drawing.Point(232, 52)
        Me.optOr.Name = "optOr"
        Me.optOr.Size = New System.Drawing.Size(40, 22)
        Me.optOr.TabIndex = 2
        Me.optOr.Text = "Or"
        '
        'cmbWhere
        '
        Me.cmbWhere.ItemHeight = 13
        Me.cmbWhere.Location = New System.Drawing.Point(8, 30)
        Me.cmbWhere.Name = "cmbWhere"
        Me.cmbWhere.Size = New System.Drawing.Size(160, 21)
        Me.cmbWhere.TabIndex = 1
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(168, 15)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Only update records where"
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"<", "<=", "<>", "=", ">", ">=", "BEGINS WITH", "CONTAINS", "DOES NOT BEGIN WITH", "DOES NOT CONTAIN", "DOES NOT END WITH", "ENDS WITH", "IS EMPTY", "IS NOT EMPTY"})
        Me.cmbOperator.Location = New System.Drawing.Point(184, 30)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(88, 21)
        Me.cmbOperator.Sorted = True
        Me.cmbOperator.TabIndex = 1
        '
        'cmbWhereValue
        '
        Me.cmbWhereValue.ItemHeight = 13
        Me.cmbWhereValue.Location = New System.Drawing.Point(288, 30)
        Me.cmbWhereValue.Name = "cmbWhereValue"
        Me.cmbWhereValue.Size = New System.Drawing.Size(136, 21)
        Me.cmbWhereValue.TabIndex = 1
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(128, 58)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdRemoveWhere.TabIndex = 4
        '
        'Page2
        '
        Me.Page2.BackColor = System.Drawing.Color.Transparent
        Me.Page2.Controls.Add(Me.lsvUpdate)
        Me.Page2.Controls.Add(Me.cmbTables)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.cmbUpdateColumn)
        Me.Page2.Controls.Add(Me.Label3)
        Me.Page2.Controls.Add(Me.Label4)
        Me.Page2.Controls.Add(Me.Label5)
        Me.Page2.Controls.Add(Me.cmbUpdateValue)
        Me.Page2.Controls.Add(Me.cmdAddUpdate)
        Me.Page2.Controls.Add(Me.cmdRemove)
        Me.Page2.Location = New System.Drawing.Point(8, 7)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(448, 208)
        Me.Page2.TabIndex = 3
        Me.Page2.TabStop = False
        '
        'lsvUpdate
        '
        '
        '
        '
        Me.lsvUpdate.Border.Class = "ListViewBorder"
        Me.lsvUpdate.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvUpdate.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lsvUpdate.Location = New System.Drawing.Point(8, 126)
        Me.lsvUpdate.Name = "lsvUpdate"
        Me.lsvUpdate.Size = New System.Drawing.Size(416, 75)
        Me.lsvUpdate.TabIndex = 2
        Me.lsvUpdate.UseCompatibleStateImageBehavior = False
        Me.lsvUpdate.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Set"
        Me.ColumnHeader2.Width = 412
        '
        'cmbTables
        '
        Me.cmbTables.ItemHeight = 13
        Me.cmbTables.Location = New System.Drawing.Point(8, 30)
        Me.cmbTables.Name = "cmbTables"
        Me.cmbTables.Size = New System.Drawing.Size(192, 21)
        Me.cmbTables.TabIndex = 1
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(184, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Table containing records to update"
        '
        'cmbUpdateColumn
        '
        Me.cmbUpdateColumn.ItemHeight = 13
        Me.cmbUpdateColumn.Location = New System.Drawing.Point(8, 74)
        Me.cmbUpdateColumn.Name = "cmbUpdateColumn"
        Me.cmbUpdateColumn.Size = New System.Drawing.Size(192, 21)
        Me.cmbUpdateColumn.TabIndex = 1
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(184, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Update this column"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(208, 74)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(16, 15)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "="
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(232, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(184, 15)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Set its value to"
        '
        'cmbUpdateValue
        '
        Me.cmbUpdateValue.ItemHeight = 13
        Me.cmbUpdateValue.Location = New System.Drawing.Point(232, 74)
        Me.cmbUpdateValue.Name = "cmbUpdateValue"
        Me.cmbUpdateValue.Size = New System.Drawing.Size(192, 21)
        Me.cmbUpdateValue.TabIndex = 1
        '
        'cmdAddUpdate
        '
        Me.cmdAddUpdate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddUpdate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddUpdate.Image = CType(resources.GetObject("cmdAddUpdate.Image"), System.Drawing.Image)
        Me.cmdAddUpdate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddUpdate.Location = New System.Drawing.Point(232, 97)
        Me.cmdAddUpdate.Name = "cmdAddUpdate"
        Me.cmdAddUpdate.Size = New System.Drawing.Size(40, 22)
        Me.cmdAddUpdate.TabIndex = 1
        '
        'cmdRemove
        '
        Me.cmdRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(160, 97)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 22)
        Me.cmdRemove.TabIndex = 1
        '
        'Page1
        '
        Me.Page1.BackColor = System.Drawing.Color.Transparent
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(8, 7)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(448, 208)
        Me.Page1.TabIndex = 2
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(192, 161)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 7)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(432, 148)
        Me.UcDSN.TabIndex = 0
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(8, 223)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 21)
        Me.cmdBack.TabIndex = 1
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(400, 223)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 21)
        Me.cmdNext.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(314, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(395, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 18
        Me.cmdCancel.Text = "&Cancel"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdSkip
        '
        Me.cmdSkip.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSkip.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(13, 3)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 23)
        Me.cmdSkip.TabIndex = 24
        Me.cmdSkip.Text = "&Skip"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 27)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(473, 314)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 25
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(473, 289)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(473, 314)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(473, 27)
        Me.FlowLayoutPanel1.TabIndex = 26
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdSkip)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 341)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(473, 28)
        Me.FlowLayoutPanel2.TabIndex = 27
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(94, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(214, 23)
        Me.LabelX1.TabIndex = 19
        '
        'frmTaskDBUpdate
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(473, 369)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskDBUpdate"
        Me.Text = "Update database records"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page4.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page2.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskDBUpdate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        nStep = 1
        sOp = " AND "

        Page1.BringToFront()
        FormatForWinXP(Me)

        showInserter(Me, Me.m_eventID, m_eventBased)

        setupForDragAndDrop(cmbUpdateValue) '.ContextMenu = Me.mnuInserter)
        setupForDragAndDrop(cmbWhereValue) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(txtFinal) '.ContextMenu = Me.mnuInserter
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdNext.Enabled = True

            Dim oData As clsMarsData = New clsMarsData

            oData.GetTables(cmbTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            cmdSkip.Enabled = True
        Else

            cmdNext.Enabled = False
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
                If lsvUpdate.Items.Count = 0 Then
                    SetError(lsvUpdate, "Please create the columns to update")
                    Return
                End If

                Page3.BringToFront()
            Case 3
                If lsvWhere.Items.Count = 0 Then
                    SetError(lsvWhere, "Please specify the criteria for the update")
                    Return
                End If

                Page4.BringToFront()
                cmdNext.Enabled = False
                cmdOK.Enabled = True

                txtFinal.Text = "UPDATE " & cmbTables.Text & " SET "

                For Each lsv As ListViewItem In lsvUpdate.Items
                    txtFinal.Text &= lsv.Text & ", "
                Next

                txtFinal.Text = txtFinal.Text.Substring(0, txtFinal.Text.Length - 2)

                txtFinal.Text &= " WHERE "

                For Each lsv As ListViewItem In lsvWhere.Items
                    txtFinal.Text &= lsv.Text
                Next

        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 4
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page3.BringToFront()
            Case 3
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdAddUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddUpdate.Click
        Dim nType As Integer
        Dim oItem As clsMyList

        If cmbUpdateColumn.Text = "" Then
            SetError(cmbUpdateColumn, "Please select a column to update")
            Return
        End If

        SetError(lsvUpdate, "")

        oItem = cmbUpdateColumn.Items(cmbUpdateColumn.SelectedIndex)

        nType = oItem.ItemData

        If nType = ADODB.DataTypeEnum.adBSTR Or nType = ADODB.DataTypeEnum.adChar Or nType = ADODB.DataTypeEnum.adLongVarChar _
               Or nType = ADODB.DataTypeEnum.adLongVarWChar Or nType = ADODB.DataTypeEnum.adVarChar Or nType = ADODB.DataTypeEnum.adVarWChar _
               Or nType = ADODB.DataTypeEnum.adWChar Or nType = ADODB.DataTypeEnum.adDate Or nType = ADODB.DataTypeEnum.adDBDate Or _
                nType = ADODB.DataTypeEnum.adDBTime Or nType = ADODB.DataTypeEnum.adDBTimeStamp Then
            lsvUpdate.Items.Add(cmbUpdateColumn.Text & " = '" & SQLPrepare(cmbUpdateValue.Text) & "'")
        Else
            lsvUpdate.Items.Add(cmbUpdateColumn.Text & " = " & cmbUpdateValue.Text)
        End If
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim sKey As String = ""
        Dim oItem As clsMyList
        Dim nType As Integer

        If cmbWhere.Text = "" Then
            SetError(cmbWhere, "Please select the column name")
            Return
        ElseIf cmbOperator.Text = "" Then
            SetError(cmbOperator, "Please select the operator")
            Return
        End If

        If lsvWhere.Items.Count > 0 Then
            sKey = sOp
        End If

        Select Case cmbOperator.Text.ToLower
            Case "begins with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " LIKE '" & SQLPrepare(cmbWhereValue.Text) & "%'")
            Case "ends with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " LIKE '%" & SQLPrepare(cmbWhereValue.Text) & "'")
            Case "contains"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " LIKE '%" & SQLPrepare(cmbWhereValue.Text) & "%'")
            Case "does not contain"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & SQLPrepare(cmbWhereValue.Text) & "%'")
            Case "does not begin with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " NOT LIKE '" & SQLPrepare(cmbWhereValue.Text) & "%'")
            Case "does not end with"
                lsvWhere.Items.Add(sKey & _
                cmbTables.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & SQLPrepare(cmbWhereValue.Text) & "'")
            Case "is empty"

                lsvWhere.Items.Add(sKey & _
                "(" & cmbTables.Text & "." & cmbWhere.Text & _
                " IS NULL OR " & _
                cmbTables.Text & "." & cmbWhere.Text & " ='')")
            Case "is not empty"
                cmbWhereValue.Enabled = False
                lsvWhere.Items.Add(sKey & _
                "(" & cmbTables.Text & "." & cmbWhere.Text & _
                " IS NOT NULL AND " & _
                cmbTables.Text & "." & cmbWhere.Text & " <> '')")
            Case Else

                oItem = cmbWhere.Items(cmbWhere.SelectedIndex)

                nType = oItem.ItemData

                If nType = ADODB.DataTypeEnum.adBSTR Or nType = ADODB.DataTypeEnum.adChar Or nType = ADODB.DataTypeEnum.adLongVarChar _
                       Or nType = ADODB.DataTypeEnum.adLongVarWChar Or nType = ADODB.DataTypeEnum.adVarChar Or nType = ADODB.DataTypeEnum.adVarWChar _
                       Or nType = ADODB.DataTypeEnum.adWChar Or nType = ADODB.DataTypeEnum.adDate Or nType = ADODB.DataTypeEnum.adDBDate Or _
                       nType = ADODB.DataTypeEnum.adDBTime Then

                    lsvWhere.Items.Add(sKey & cmbWhere.Text & cmbOperator.Text & "'" & SQLPrepare(cmbWhereValue.Text) & "'")
                Else
                    lsvWhere.Items.Add(sKey & cmbWhere.Text & cmbOperator.Text & cmbWhereValue.Text)

                End If

        End Select

        SetError(lsvWhere, "")

        Dim lsv As ListViewItem = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If

    End Sub

    Private Sub optAnd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAnd.CheckedChanged
        sOp = " AND "
    End Sub

    Private Sub optOr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOr.CheckedChanged
        sOp = " OR "
    End Sub

    Private Sub cmbUpdateColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbUpdateColumn.SelectedIndexChanged
        SetError(cmbUpdateColumn, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvUpdate.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvUpdate.SelectedItems(0)

        lsvUpdate.Items.Remove(lsv)
    End Sub

    Private Sub cmbWhere_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWhere.SelectedIndexChanged
        SetError(cmbWhere, "")
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        SetError(cmbOperator, "")

        If cmbOperator.Text.ToLower = "is empty" Or cmbOperator.Text.ToLower = "is not empty" Then
            cmbWhereValue.Enabled = False
        Else
            cmbWhereValue.Enabled = True
        End If
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvWhere.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvWhere.SelectedItems(0)

        lsvWhere.Items.Remove(lsv)

        If lsvWhere.Items.Count = 0 Then Return

        lsv = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If

    End Sub

    Private Sub cmbTables_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTables.SelectedIndexChanged
        Dim oData As clsMarsData = New clsMarsData

        oData.GetColumns(cmbUpdateColumn, UcDSN.cmbDSN.Text, cmbTables.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        oData.GetColumns(cmbWhere, UcDSN.cmbDSN.Text, cmbTables.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

    End Sub

    Private Sub cmbUpdateValue_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbUpdateValue.DropDown
        Dim oData As clsMarsData = New clsMarsData

        oData.ReturnDistinctValues(cmbUpdateValue, UcDSN.cmbDSN.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text, cmbUpdateColumn.Text, cmbTables.Text)

    End Sub


    Private Sub cmbWhereValue_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWhereValue.DropDown
        Dim oData As clsMarsData = New clsMarsData

        oData.ReturnDistinctValues(cmbWhereValue, UcDSN.cmbDSN.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text, cmbWhere.Text, cmbTables.Text)
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please enter the name for the task")
            txtName.Focus()
            Return
        ElseIf txtFinal.Text = "" Then
            SetError(txtFinal, "Please create the SQL script for the database update")
            txtFinal.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sUpdates As String
        Dim sWhere As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        'Connection String -> ProgramParameters
        'Table Updated -> ProgramPath
        'Columns for update -> SendTo
        'Where Values -> FileList
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        For Each lsv As ListViewItem In lsvUpdate.Items
            If lsv.Text <> "" Then sUpdates &= lsv.Text & "|"
        Next

        For Each lsv As ListViewItem In lsvWhere.Items
            If lsv.Text <> "" Then sWhere &= lsv.Text & "|"
        Next

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,SendTo," & _
            "FileList,Msg,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'DBUpdate'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(cmbTables.Text) & "'," & _
            "'" & SQLPrepare(sUpdates) & "'," & _
            "'" & SQLPrepare(sWhere) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)


        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed


        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sUpdates As String
        Dim sWhere As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table Updated -> ProgramPath
        'Columns for update -> SendTo
        'Where Values -> FileList
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            cmbTables.Text = oRs("programpath").Value

            sUpdates = oRs("sendto").Value

            For Each s As String In sUpdates.Split("|")
                If s.Length > 0 Then lsvUpdate.Items.Add(s)
            Next

            sWhere = oRs("filelist").Value

            For Each s As String In sWhere.Split("|")
                If s.Length > 0 Then lsvWhere.Items.Add(s)
            Next

            txtFinal.Text = oRs("msg").Value

        End If

        oRs.Close()

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Return

        sUpdates = ""
        sWhere = ""

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
        UcDSN.txtPassword.Text & "|"

        For Each lsv As ListViewItem In lsvUpdate.Items
            sUpdates &= lsv.Text & "|"
        Next

        For Each lsv As ListViewItem In lsvWhere.Items
            sWhere &= lsv.Text & "|"
        Next

        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
            "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
            "ProgramPath = '" & SQLPrepare(cmbTables.Text) & "'," & _
            "SendTo = '" & SQLPrepare(sUpdates) & "'," & _
            "FileList = '" & SQLPrepare(sWhere) & "'," & _
            "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        'for editing a task, set its run type


        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmbWhereValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWhereValue.SelectedIndexChanged

    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page4.BringToFront()
        cmdNext.Enabled = False
        nStep = 4
        cmdOK.Enabled = True
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            Clipboard.SetText(ctrl.SelectedText)

            ctrl.SelectedText = ""
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            Clipboard.SetText(ctrl.SelectedText)
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectedText = Clipboard.GetText
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim ctrl As Object

        ctrl = Me.ActiveControl

        If (TypeOf ctrl Is TextBox) Or (TypeOf ctrl Is ComboBox) Then
            ctrl.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(m_eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.m_eventID
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        Dim oField As Object

        oField = Me.ActiveControl

        If (TypeOf oField Is TextBox) Or (TypeOf oField Is ComboBox) Then
            oField.SelectedText = oItem._GetDataItem(Me.eventID)
        End If
    End Sub


End Class
