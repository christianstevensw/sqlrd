﻿Public Class frmTaskBuildWorkBook
    Public m_taskID As Integer = 99999
    Dim m_eventID As Integer = 99999
    Dim ep As ErrorProvider = New ErrorProvider
    Dim userCancel As Boolean = False


    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim name As String = InputBox("Provide name of this dataset", Application.ProductName)

        If name = "" Then Return

        Dim getSQL As frmDynamicParameter = New frmDynamicParameter

        Dim results() As String = getSQL.ReturnSQLQuery()

        If results IsNot Nothing Then
            Dim conString As String = results(0)
            Dim query As String = results(1)
            Dim datasetid As Integer = clsMarsData.CreateDataID
            Dim cols, vals As String

            Dim dsn As String = conString.Split("|")(0)
            Dim userid As String = conString.Split("|")(1)
            Dim password As String = _EncryptDBValue(conString.Split("|")(2))

            conString = dsn & "|" & userid & "|" & password

            cols = "datasetid,taskid,datasetname,constring,sqltext,ordernumber"
            vals = datasetid & "," & _
            m_taskID & "," & _
            "'" & SQLPrepare(name) & "'," & _
            "'" & SQLPrepare(conString) & "'," & _
            "'" & SQLPrepare(query) & "'," & _
            lsvDataSets.Items.Count + 1

            If clsMarsData.DataItem.InsertData("datasetattr", cols, vals) = True Then
                Dim it As ListViewItem = lsvDataSets.Items.Add(name)
                it.Tag = datasetid
                it.ImageIndex = 0

            End If
        End If
    End Sub

    Private Sub lsvDataSets_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.LabelEditEventArgs) Handles lsvDataSets.AfterLabelEdit
        If lsvDataSets.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = lsvDataSets.SelectedItems(0)
        Dim datasetID As Integer = it.Tag

        If e.Label = "" Then Return

        Dim SQL As String = "UPDATE datasetattr SET datasetname ='" & SQLPrepare(e.Label) & "' WHERE datasetid =" & datasetID

        clsMarsData.WriteData(SQL)
    End Sub

   
    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvDataSets.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected item(s)? This cannot be undone.", Application.ProductName, _
                             MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            '//delete and remove from listview
            For Each it As ListViewItem In lsvDataSets.SelectedItems
                Dim datasetID As Integer = it.Tag
                Dim SQL As String = "DELETE FROM datasetattr WHERE datasetid =" & datasetID
                clsMarsData.WriteData(SQL)
                it.Remove()
            Next

            '//update ordering in the database
            For Each it As ListViewItem In lsvDataSets.Items
                clsMarsData.WriteData("UPDATE datasetattr SET ordernumber =" & it.Index & " WHERE datasetid =" & it.Tag)
            Next
        End If
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If lsvDataSets.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = lsvDataSets.SelectedItems(0)

        Dim getSQL As frmDynamicParameter = New frmDynamicParameter
        Dim SQL As String = "SELECT * FROM datasetattr WHERE datasetid =" & it.Tag
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim query As String = oRs("sqltext").Value
            Dim constring As String = oRs("constring").Value
            Dim dsn As String = constring.Split("|")(0)
            Dim userid As String = constring.Split("|")(1)
            Dim password As String = _DecryptDBValue(constring.Split("|")(2))

            constring = dsn & "|" & userid & "|" & password

            Dim results() As String = getSQL.ReturnSQLQuery(constring, query)

            If results IsNot Nothing Then
                constring = results(0)
                dsn = constring.Split("|")(0)
                userid = constring.Split("|")(1)
                password = _EncryptDBValue(constring.Split("|")(2))

                constring = dsn & "|" & userid & "|" & password

                SQL = "UPDATE datasetattr SET " & _
                "sqltext = '" & SQLPrepare(results(1)) & "'," & _
                "constring = '" & SQLPrepare(constring) & "' " & _
                " WHERE datasetid =" & it.Tag

                clsMarsData.WriteData(SQL)
            End If
        End If
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If lsvDataSets.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvDataSets.SelectedItems(0)

        If oItem.Index = 0 Then Return

        Dim oMove As ListViewItem = lsvDataSets.Items(oItem.Index - 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE datasetattr SET ordernumber =" & newIndex & " WHERE datasetid =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE datasetattr SET ordernumber =" & oldIndex & " WHERE datasetid =" & moveID

        clsMarsData.WriteData(SQL)

        Me.LoadAll()

        For Each item As ListViewItem In lsvDataSets.Items
            If item.Tag = itemID Then
                item.Selected = True
                item.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub loadAll()
        Dim SQL As String = "SELECT * FROM datasetattr WHERE taskid =" & m_taskID & " ORDER BY ordernumber"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        lsvDataSets.Items.Clear()

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim datasetID As Integer = oRs("datasetid").Value
                Dim datasetname As String = oRs("datasetname").Value
                Dim it As ListViewItem = lsvDataSets.Items.Add(datasetname)
                it.Tag = datasetID
                it.ImageIndex = 0
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If lsvDataSets.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvDataSets.SelectedItems(0)

        If oItem.Index = lsvDataSets.Items.Count - 1 Then Return

        Dim oMove As ListViewItem = lsvDataSets.Items(oItem.Index + 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE datasetattr SET ordernumber =" & newIndex & " WHERE datasetid =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE datasetattr SET ordernumber =" & oldIndex & " WHERE datasetid =" & moveID

        clsMarsData.WriteData(SQL)

        Me.loadAll()

        For Each item As ListViewItem In lsvDataSets.Items
            If item.Tag = itemID Then
                item.Selected = True
                item.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf txtOutput.Text = "" Then
            SetError(txtOutput, "Please enter the Workbook path and name")
            txtOutput.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
    End Sub

    Public Function AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        m_taskID = clsMarsData.CreateDataID("tasks", "taskid")

        Me.ShowDialog()

        If userCancel = True Then
            clsMarsData.WriteData("DELETE FROM datasetattr WHERE taskid =" & m_taskID)
            Return 0
        End If

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sValues As String


        sCols = "TaskID,ScheduleID,TaskType,TaskName,FileList,ReplaceFiles,OrderID"

        sVals = m_taskID & "," & _
            ScheduleID & "," & _
            "'buildworkbook'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(txtOutput.Text) & "'," & _
            Convert.ToInt32(chkReplace.Checked) & "," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(m_taskID)

        Return m_taskID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String

        m_taskID = nTaskID

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtOutput.Text = oRs("filelist").Value

            Try
                chkReplace.Checked = IsNull(oRs("replacefiles").Value, 0)
            Catch : End Try

            loadAll()

            TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

            Me.ShowDialog()

            If userCancel = True Then Return


            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                    "ReplaceFiles = " & Convert.ToInt32(chkReplace.Checked) & "," & _
                    "FileList = '" & SQLPrepare(txtOutput.Text) & "' "
                
            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)

            TaskExecutionPath1.setTaskRunWhen(nTaskID)
        End If
    End Sub
    Private Sub frmTaskBuildWorkBook_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        'txtOutput.ContextMenu = mnuInserter
        setupForDragAndDrop(txtOutput)
        showInserter(Me, 99999)

    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim sfd As SaveFileDialog = New SaveFileDialog

        With sfd
            .Filter = "MS Excel 97-2003 (*.xls)|*.xls|MS Excel 2007 (*.xlsx)|*.xlsx"
            .Title = "Save Workbook As"
            .AddExtension = True

            If .ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtOutput.Text = .FileName
            End If
        End With
    End Sub

    Private Sub txtOutput_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOutput.TextChanged

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        txtOutput.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txtOutput.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txtOutput.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txtOutput.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txtOutput.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txtOutput.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        txtOutput.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub
End Class