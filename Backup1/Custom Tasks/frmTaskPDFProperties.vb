﻿Public Class frmTaskPDFProperties
    Dim eventID As Integer
    Dim eventBased As Boolean
    Dim m_taskID As Integer
    Dim userCancel As Boolean = False

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Using ofd As OpenFileDialog = New OpenFileDialog
            ofd.Title = "Please select the PDF file to manipulate"
            ofd.Filter = "PDF Files|*.pdf|All Files|*.*"
            ofd.Multiselect = False

            If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
                txtPDFPath.Text = ofd.FileName
            End If
        End Using
    End Sub

    Private Sub btnPDF_Click(sender As System.Object, e As System.EventArgs)
        Dim pdfStamper As frmPDFStamp = New frmPDFStamp

        With pdfStamper
            .m_Dynamic = False
            .m_eventBased = 0

            txtWatermark.Text = .AddStampDtls(m_taskID, txtWatermark.Text)
        End With
    End Sub

    Private Sub frmTaskPDFProperties_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        setupForDragAndDrop(txtPDFPath)
        setupForDragAndDrop(txtInfoAuthor)
        setupForDragAndDrop(txtInfoCreated)
        setupForDragAndDrop(txtInfoKeywords)
        setupForDragAndDrop(txtInfoProducer)
        setupForDragAndDrop(txtInfoSubject)
        setupForDragAndDrop(txtInfoTitle)
        setupForDragAndDrop(txtUserPassword)
        setupForDragAndDrop(txtOwnerPassword)
        setupForDragAndDrop(txtWatermark)

        showInserter(Me, m_eventID, m_eventBased)
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,OrderID"

        sVals = nTaskID & "," & _
        ScheduleID & "," & _
        "'EditPDF'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPDFPath.Text.Replace("'", "''") & "'," & _
              oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        sCols = "OptionID,TaskID,PDFPassword,PDFWaterMark,UserPassword,CanPrint," & _
        "CanCopy,CanEdit,CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "PDFSecurity," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer,expirepdf,pdfexpirydate"

        sVals = clsMarsData.CreateDataID("taskoptions", "optionid") & "," & nTaskID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(Me.txtWatermark.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtUserPassword.Text)) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        "1," & _
        "'" & SQLPrepare(Me.txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(Me.txtInfoProducer.Text) & "'," & _
        Convert.ToInt32(chkPDFExpiry.Checked) & "," & _
        "'" & ConDate(dtPDFExpiry.Value) & "'"

        SQL = "INSERT INTO TaskOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)

        Return nTaskID
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        m_taskID = nTaskID

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            txtPDFPath.Text = oRs("programpath").Value

        End If

        oRs.Close()

        Try
            oRs = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then

                    With Me
                        .txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                        .txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                        .txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                        .chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                        .chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                        .chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                        .chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                        .chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                        .chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                        .chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)
                        .txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                        .txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                        .txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                        .txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)

                        Try
                            .txtInfoCreated.Value = Convert.ToDateTime(oRs("infocreated").Value)
                        Catch ex As Exception
                            .txtInfoCreated.Value = Now
                        End Try

                        .txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                        chkPDFExpiry.Checked = Convert.ToInt32(IsNull(oRs("expirepdf").Value, 0))
                        dtPDFExpiry.Value = IsNull(oRs("pdfexpirydate").Value, Date.Now)
                    End With
                End If
            End If
        Catch : End Try

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPDFPath.Text.Replace("'", "''") & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        With Me
            SQL = "PDFPassword = '" & SQLPrepare(_EncryptDBValue(.txtOwnerPassword.Text)) & "'," & _
            "PDFWaterMark = '" & SQLPrepare(.txtWatermark.Text) & "'," & _
            "UserPassword = '" & SQLPrepare(_EncryptDBValue(.txtUserPassword.Text)) & "'," & _
            "CanPrint = " & Convert.ToInt32(.chkPrint.Checked) & "," & _
            "CanCopy = " & Convert.ToInt32(.chkCopy.Checked) & "," & _
            "CanEdit = " & Convert.ToInt32(.chkEdit.Checked) & "," & _
            "CanNotes = " & Convert.ToInt32(.chkNotes.Checked) & "," & _
            "CanFill = " & Convert.ToInt32(.chkFill.Checked) & "," & _
            "CanAccess = " & Convert.ToInt32(.chkAccess.Checked) & "," & _
            "CanAssemble = " & Convert.ToInt32(.chkAssemble.Checked) & "," & _
            "CanPrintFull = " & Convert.ToInt32(.chkFullRes.Checked) & "," & _
            "PDFSecurity = 1," & _
            "InfoTitle = '" & SQLPrepare(.txtInfoTitle.Text) & "'," & _
            "InfoAuthor = '" & SQLPrepare(.txtInfoAuthor.Text) & "'," & _
            "InfoSubject = '" & SQLPrepare(.txtInfoSubject.Text) & "'," & _
            "InfoKeyWords = '" & SQLPrepare(.txtInfoKeywords.Text) & "'," & _
            "InfoCreated = '" & ConDateTime(.txtInfoCreated.Value) & "'," & _
            "InfoProducer = '" & SQLPrepare(.txtInfoProducer.Text) & "'," & _
            "ExpirePDF = " & Convert.ToInt32(chkPDFExpiry.Checked) & "," & _
            "PDFExpiryDate ='" & ConDate(dtPDFExpiry.Value) & "'"
        End With

        SQL = "UPDATE TaskOptions SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdCancel_Click(sender As System.Object, e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(sender As Object, e As System.EventArgs) Handles cmdOK.Click
        '//lets validate this shiznit
        If txtName.Text = "" Then
            setError(txtName, "Please enter a name for this task")
            txtName.Focus()
            Return
        ElseIf txtPDFPath.Text = "" Then
            setError(txtPDFPath, "Please select the PDF to manipulate")
            txtPDFPath.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub chkPDFExpiry_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkPDFExpiry.CheckedChanged
        dtPDFExpiry.Enabled = chkPDFExpiry.Checked
    End Sub
End Class