Public Class frmTaskEmail
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oField As TextBox
    Dim sField As String = "to"
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Dim eventBased As Boolean = False
    Friend WithEvents cmbSMTPServer As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents pnSMTP As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents SuperT As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents grpSender As System.Windows.Forms.GroupBox
    Friend WithEvents txtSenderAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSenderName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAttachOrignal As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents TaskExecutionPath1 As sqlrd.taskExecutionPath
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents txtMsg As sqlrd.ucEditorX
    Dim eventID As Integer = 99999

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCC As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdCC As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtBcc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdBCC As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAttach As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtAttach As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents cmdInsert As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSig As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents cmbMailFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAttach As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskEmail))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtMsg = New sqlrd.ucEditorX()
        Me.chkAttachOrignal = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpSender = New System.Windows.Forms.GroupBox()
        Me.txtSenderAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSenderName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label18 = New DevComponents.DotNetBar.LabelX()
        Me.Label17 = New DevComponents.DotNetBar.LabelX()
        Me.pnSMTP = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSMTPServer = New System.Windows.Forms.ComboBox()
        Me.cmbMailFormat = New System.Windows.Forms.ComboBox()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTo = New DevComponents.DotNetBar.ButtonX()
        Me.txtTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCC = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdCC = New DevComponents.DotNetBar.ButtonX()
        Me.txtBcc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdBCC = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAttach = New DevComponents.DotNetBar.ButtonX()
        Me.txtAttach = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem()
        Me.mnuMsg = New System.Windows.Forms.MenuItem()
        Me.mnuSig = New System.Windows.Forms.MenuItem()
        Me.mnuAttach = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.mnuSpell = New System.Windows.Forms.MenuItem()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdInsert = New DevComponents.DotNetBar.ButtonX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.mnuDB = New System.Windows.Forms.MenuItem()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuConstants = New System.Windows.Forms.MenuItem()
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem16 = New System.Windows.Forms.MenuItem()
        Me.MenuItem17 = New System.Windows.Forms.MenuItem()
        Me.MenuItem18 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.SuperT = New DevComponents.DotNetBar.SuperTooltip()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TaskExecutionPath1 = New sqlrd.taskExecutionPath()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox1.SuspendLayout()
        Me.grpSender.SuspendLayout()
        Me.pnSMTP.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(431, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 19
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(77, 3)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(489, 21)
        Me.txtName.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(512, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 20
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 21)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.txtMsg)
        Me.GroupBox1.Controls.Add(Me.chkAttachOrignal)
        Me.GroupBox1.Controls.Add(Me.grpSender)
        Me.GroupBox1.Controls.Add(Me.pnSMTP)
        Me.GroupBox1.Controls.Add(Me.cmbMailFormat)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmdTo)
        Me.GroupBox1.Controls.Add(Me.txtTo)
        Me.GroupBox1.Controls.Add(Me.txtCC)
        Me.GroupBox1.Controls.Add(Me.cmdCC)
        Me.GroupBox1.Controls.Add(Me.txtBcc)
        Me.GroupBox1.Controls.Add(Me.cmdBCC)
        Me.GroupBox1.Controls.Add(Me.cmdAttach)
        Me.GroupBox1.Controls.Add(Me.txtAttach)
        Me.GroupBox1.Controls.Add(Me.txtSubject)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(582, 522)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'txtMsg
        '
        Me.txtMsg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMsg.bodyFormat = sqlrd.ucEditorX.format.TEXT
        Me.txtMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg.Location = New System.Drawing.Point(8, 161)
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(556, 210)
        Me.txtMsg.TabIndex = 30
        '
        'chkAttachOrignal
        '
        Me.chkAttachOrignal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAttachOrignal.AutoSize = True
        '
        '
        '
        Me.chkAttachOrignal.BackgroundStyle.Class = ""
        Me.chkAttachOrignal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAttachOrignal.Location = New System.Drawing.Point(8, 498)
        Me.chkAttachOrignal.Name = "chkAttachOrignal"
        Me.chkAttachOrignal.Size = New System.Drawing.Size(138, 16)
        Me.chkAttachOrignal.TabIndex = 16
        Me.chkAttachOrignal.Text = "Attach original message"
        Me.chkAttachOrignal.Visible = False
        '
        'grpSender
        '
        Me.grpSender.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpSender.Controls.Add(Me.txtSenderAddress)
        Me.grpSender.Controls.Add(Me.txtSenderName)
        Me.grpSender.Controls.Add(Me.Label18)
        Me.grpSender.Controls.Add(Me.Label17)
        Me.grpSender.Location = New System.Drawing.Point(8, 411)
        Me.grpSender.Name = "grpSender"
        Me.grpSender.Size = New System.Drawing.Size(556, 83)
        Me.grpSender.TabIndex = 29
        Me.grpSender.TabStop = False
        Me.grpSender.Text = "Customize sender details (optional)"
        '
        'txtSenderAddress
        '
        Me.txtSenderAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtSenderAddress.Border.Class = "TextBoxBorder"
        Me.txtSenderAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderAddress.Location = New System.Drawing.Point(83, 48)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(467, 21)
        Me.txtSenderAddress.TabIndex = 15
        '
        'txtSenderName
        '
        Me.txtSenderName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtSenderName.Border.Class = "TextBoxBorder"
        Me.txtSenderName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderName.Location = New System.Drawing.Point(83, 19)
        Me.txtSenderName.Name = "txtSenderName"
        Me.txtSenderName.Size = New System.Drawing.Size(467, 21)
        Me.txtSenderName.TabIndex = 14
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        '
        '
        '
        Me.Label18.BackgroundStyle.Class = ""
        Me.Label18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label18.Location = New System.Drawing.Point(4, 50)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(45, 16)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Address:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        '
        '
        '
        Me.Label17.BackgroundStyle.Class = ""
        Me.Label17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label17.Location = New System.Drawing.Point(6, 21)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name:"
        '
        'pnSMTP
        '
        Me.pnSMTP.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.pnSMTP.Controls.Add(Me.Label4)
        Me.pnSMTP.Controls.Add(Me.cmbSMTPServer)
        Me.pnSMTP.Location = New System.Drawing.Point(229, 378)
        Me.pnSMTP.Name = "pnSMTP"
        Me.pnSMTP.Size = New System.Drawing.Size(212, 26)
        Me.pnSMTP.TabIndex = 25
        Me.pnSMTP.Visible = False
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 24)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "SMTP Server"
        '
        'cmbSMTPServer
        '
        Me.cmbSMTPServer.ItemHeight = 13
        Me.cmbSMTPServer.Location = New System.Drawing.Point(85, 3)
        Me.cmbSMTPServer.Name = "cmbSMTPServer"
        Me.cmbSMTPServer.Size = New System.Drawing.Size(121, 21)
        Me.cmbSMTPServer.TabIndex = 28
        '
        'cmbMailFormat
        '
        Me.cmbMailFormat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbMailFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailFormat.ItemHeight = 13
        Me.cmbMailFormat.Items.AddRange(New Object() {"TEXT", "HTML"})
        Me.cmbMailFormat.Location = New System.Drawing.Point(81, 379)
        Me.cmbMailFormat.Name = "cmbMailFormat"
        Me.cmbMailFormat.Size = New System.Drawing.Size(121, 21)
        Me.cmbMailFormat.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(9, 382)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 15)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Mail Format"
        '
        'cmdTo
        '
        Me.cmdTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(8, 15)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdTo.TabIndex = 2
        Me.cmdTo.Text = "To..."
        '
        'txtTo
        '
        Me.txtTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtTo.Border.Class = "TextBoxBorder"
        Me.txtTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTo.ForeColor = System.Drawing.Color.Blue
        Me.txtTo.Location = New System.Drawing.Point(80, 15)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(483, 21)
        Me.txtTo.TabIndex = 3
        Me.txtTo.Tag = "memo"
        '
        'txtCC
        '
        Me.txtCC.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtCC.Border.Class = "TextBoxBorder"
        Me.txtCC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCC.ForeColor = System.Drawing.Color.Blue
        Me.txtCC.Location = New System.Drawing.Point(80, 45)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(483, 21)
        Me.txtCC.TabIndex = 5
        Me.txtCC.Tag = "memo"
        '
        'cmdCC
        '
        Me.cmdCC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCC.Location = New System.Drawing.Point(8, 45)
        Me.cmdCC.Name = "cmdCC"
        Me.cmdCC.Size = New System.Drawing.Size(64, 21)
        Me.cmdCC.TabIndex = 4
        Me.cmdCC.Text = "Cc..."
        '
        'txtBcc
        '
        Me.txtBcc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtBcc.Border.Class = "TextBoxBorder"
        Me.txtBcc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBcc.ForeColor = System.Drawing.Color.Blue
        Me.txtBcc.Location = New System.Drawing.Point(80, 74)
        Me.txtBcc.Name = "txtBcc"
        Me.txtBcc.Size = New System.Drawing.Size(483, 21)
        Me.txtBcc.TabIndex = 7
        Me.txtBcc.Tag = "memo"
        '
        'cmdBCC
        '
        Me.cmdBCC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBCC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBCC.Location = New System.Drawing.Point(8, 74)
        Me.cmdBCC.Name = "cmdBCC"
        Me.cmdBCC.Size = New System.Drawing.Size(64, 22)
        Me.cmdBCC.TabIndex = 6
        Me.cmdBCC.Text = "Bcc..."
        '
        'cmdAttach
        '
        Me.cmdAttach.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAttach.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAttach.Location = New System.Drawing.Point(8, 104)
        Me.cmdAttach.Name = "cmdAttach"
        Me.cmdAttach.Size = New System.Drawing.Size(64, 21)
        Me.cmdAttach.TabIndex = 8
        Me.cmdAttach.Text = "Attach"
        '
        'txtAttach
        '
        Me.txtAttach.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtAttach.Border.Class = "TextBoxBorder"
        Me.txtAttach.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtAttach.Location = New System.Drawing.Point(80, 104)
        Me.txtAttach.Name = "txtAttach"
        Me.txtAttach.Size = New System.Drawing.Size(483, 21)
        Me.txtAttach.TabIndex = 9
        Me.txtAttach.Tag = "memo"
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtSubject.Border.Class = "TextBoxBorder"
        Me.txtSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(80, 134)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(483, 21)
        Me.txtSubject.TabIndex = 10
        Me.txtSubject.Tag = "memo"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 14)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Subject"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem12, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuMsg, Me.mnuSig, Me.mnuAttach})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuMsg
        '
        Me.mnuMsg.Index = 5
        Me.mnuMsg.Text = "Default Message"
        '
        'mnuSig
        '
        Me.mnuSig.Index = 6
        Me.mnuSig.Text = "Default Signature"
        '
        'mnuAttach
        '
        Me.mnuAttach.Index = 7
        Me.mnuAttach.Text = "Default Attachment"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 10
        Me.MenuItem12.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdInsert
        '
        Me.cmdInsert.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdInsert.Image = CType(resources.GetObject("cmdInsert.Image"), System.Drawing.Image)
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(3, 580)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(24, 22)
        Me.cmdInsert.TabIndex = 24
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem8, Me.MenuItem11, Me.MenuItem9, Me.MenuItem10, Me.MenuItem13})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.Text = "Default Subject"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 5
        Me.MenuItem9.Text = "Default Message"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "Default Signature "
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 7
        Me.MenuItem13.Text = "Default Attachment"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'SuperT
        '
        Me.SuperT.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperT.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperT.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControl1.Location = New System.Drawing.Point(0, 28)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(590, 557)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabIndex = 25
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(590, 531)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "General"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TaskExecutionPath1)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(590, 557)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'TaskExecutionPath1
        '
        Me.TaskExecutionPath1.BackColor = System.Drawing.Color.Transparent
        Me.TaskExecutionPath1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TaskExecutionPath1.Location = New System.Drawing.Point(3, 3)
        Me.TaskExecutionPath1.Name = "TaskExecutionPath1"
        Me.TaskExecutionPath1.Size = New System.Drawing.Size(322, 143)
        Me.TaskExecutionPath1.TabIndex = 0
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Options"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtName)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(590, 28)
        Me.FlowLayoutPanel1.TabIndex = 26
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 585)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(590, 29)
        Me.FlowLayoutPanel2.TabIndex = 26
        '
        'frmTaskEmail
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(590, 614)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.cmdInsert)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmTaskEmail"
        Me.Text = "Send an Email"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.grpSender.ResumeLayout(False)
        Me.grpSender.PerformLayout()
        Me.pnSMTP.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAttach.Click
        ofd.Multiselect = True
        ofd.Title = "Select files to attach"
        ofd.ShowDialog()

        If ofd.FileNames.GetLength(0) = 0 Then Return

        If txtAttach.Text.EndsWith(";") = False And txtAttach.Text.Length > 0 Then _
        txtAttach.Text &= ";"

        For Each s As String In ofd.FileNames
            txtAttach.Text &= s & ";"
        Next

    End Sub


    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" And txtName.Visible = True Then
            SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf txtTo.Text = "" Then
            SetError(txtTo, "Please enter the email recipient(s)")
            txtTo.Focus()
            Exit Sub
            'Else
            '    For Each s As String In txtAttach.Text.Split(";")
            '        If s.Length > 0 Then
            '            If (s.IndexOf("*") = -1) And (s.Contains("<[") = False) Then
            '                If IO.File.Exists(s) = False Then
            '                    SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure the file exists and try again.")
            '                    txtAttach.Focus()
            '                    Return
            '                End If
            '            End If
            '        End If

            '    Next
        End If


        Me.Close()

    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True) As Integer
        Me.cmbSMTPServer.Text = "Default"
        Me.cmbMailFormat.SelectedIndex = 1

        If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
            Me.grpSender.Enabled = True
        Else
            Me.grpSender.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName," & _
        "SendTo,Cc,Bcc,Msg,FileList,Subject,ProgramPath,PrinterName,SenderName,SenderAddress,OrderID"

        'program path used for mail format
        'printername used for smtpserver
        Dim msg As String = txtMsg.Text

        If msg Is Nothing Then msg = ""

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'SendMail'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtTo.Text.Replace("'", "''") & "'," & _
        "'" & txtCC.Text.Replace("'", "''") & "'," & _
        "'" & txtBcc.Text.Replace("'", "''") & "'," & _
        "'" & msg.Replace("'", "''") & "'," & _
        "'" & txtAttach.Text.Replace("'", "''") & "'," & _
        "'" & txtSubject.Text.Replace("'", "''") & "'," & _
        "'" & cmbMailFormat.Text & "'," & _
        "'" & SQLPrepare(cmbSMTPServer.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSenderAddress.Text) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)

        TaskExecutionPath1.setTaskRunWhen(nID)

        Return nID

    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim msg As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            cmbMailFormat.Text = IsNull(oRs("programpath").Value, "TEXT")

            msg = IsNull(oRs("msg").Value, "")

            txtName.Text = oRs("taskname").Value
            txtTo.Text = oRs("sendto").Value
            txtCC.Text = oRs("cc").Value
            txtBcc.Text = oRs("bcc").Value
            txtAttach.Text = oRs("filelist").Value
            txtSubject.Text = oRs("subject").Value
            txtMsg.Text = oRs("msg").Value
            cmbSMTPServer.Text = IsNull(oRs("printername").Value, "Default")


            Try
                Me.txtSenderName.Text = IsNull(oRs("sendername").Value)
                Me.txtSenderAddress.Text = IsNull(oRs("senderaddress").Value)
            Catch : End Try
        End If

        oRs.Close()

        If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
            Me.grpSender.Enabled = True
        Else
            Me.grpSender.Enabled = False
        End If

        TaskExecutionPath1.LoadTaskRunWhen(nTaskID)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        msg = txtMsg.Text

        If msg Is Nothing Then msg = ""


        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "SendTo = '" & txtTo.Text.Replace("'", "''") & "'," & _
            "CC = '" & txtCC.Text.Replace("'", "''") & "'," & _
            "Bcc = '" & txtBcc.Text.Replace("'", "''") & "'," & _
            "FileList ='" & txtAttach.Text.Replace("'", "''") & "'," & _
            "Subject = '" & txtSubject.Text.Replace("'", "''") & "'," & _
            "Msg = '" & msg.Replace("'", "''") & "'," & _
            "ProgramPath = '" & cmbMailFormat.Text & "'," & _
            "PrinterName ='" & SQLPrepare(cmbSMTPServer.Text) & "', " & _
            "SenderName ='" & SQLPrepare(Me.txtSenderName.Text) & "', " & _
            "SenderAddress ='" & SQLPrepare(Me.txtSenderAddress.Text) & "' "

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        TaskExecutionPath1.setTaskRunWhen(nTaskID)


    End Sub
    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, "")
    End Sub
    Private Sub setEmailAutocompleteSource(txt As DevComponents.DotNetBar.Controls.TextBoxX)
        Try
            Dim ac As AutoCompleteStringCollection = New AutoCompleteStringCollection
            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")

            If IO.File.Exists(emailCache) = False Then Return

            Dim dt As DataTable = New DataTable

            dt.ReadXml(emailCache)

            For Each row As DataRow In dt.Rows
                ac.Add(row("emailaddress"))
            Next

            txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            txt.AutoCompleteSource = AutoCompleteSource.CustomSource
            txt.AutoCompleteCustomSource = ac
        Catch : End Try
    End Sub

    Private Sub txtTo_LostFocus(sender As Object, e As System.EventArgs) Handles txtTo.LostFocus, txtBcc.LostFocus, txtCC.LostFocus
        Try
            If CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text = "" Then Return

            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")
            Dim dtCache As DataTable = New DataTable("emailcache")

            If IO.File.Exists(emailCache) Then
                dtCache.ReadXml(emailCache)
            Else
                With dtCache.Columns
                    .Add("emailAddress")
                End With
            End If

            Dim row As DataRow
            Dim rows() As DataRow = dtCache.Select("emailaddress = '" & CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text & "'")

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                row = rows(0)
            Else
                row = dtCache.Rows.Add
                row("emailaddress") = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text
            End If

            dtCache.WriteXml(emailCache, XmlWriteMode.WriteSchema)
        Catch : End Try
    End Sub

    'Handles txtTo.GotFocus, txtCC.GotFocus, txtBcc.GotFocus
    '        oField = CType(sender, TextBox)
    '    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        SetError(sender, "")
    End Sub


    Private Sub frmTaskEmail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False

        If MailType = gMailType.GROUPWISE Then
            cmbMailFormat.Text = "TEXT"
            cmbMailFormat.Enabled = False
        ElseIf MailType = gMailType.SMTP Then
            pnSMTP.Visible = True
        End If

        setupForDragAndDrop(Me.txtTo) '.ContextMenu = Me.mnuInserter)
        setupForDragAndDrop(Me.txtCC) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtBcc) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtSenderName) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtSenderAddress) '.ContextMenu = Me.mnuInserter
        setupForDragAndDrop(Me.txtSubject)
        setupForDragAndDrop(Me.txtMsg)
        showInserter(Me, m_eventID, m_eventBased)

        setEmailAutocompleteSource(txtTo)
        setEmailAutocompleteSource(txtCC)
        setEmailAutocompleteSource(txtBcc)
    End Sub

    Private Sub cmdTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdTo.MouseUp

        If e.Button <> MouseButtons.Left Then Return

        oField = txtTo
        sField = "to"

        mnuContacts.Show(cmdTo, New Point(e.X, e.Y))

    End Sub

    Private Sub cmdCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtCC
        sField = "cc"
        mnuContacts.Show(cmdCC, New Point(e.X, e.Y))
    End Sub

    Private Sub cmdBCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdBCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtBcc
        sField = "bcc"
        mnuContacts.Show(cmdBCC, New Point(e.X, e.Y))

    End Sub


    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click

        oMsg.OutlookAddresses(oField)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click


        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact(sField)

        If sValues Is Nothing Then Return

        If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
        Then txtTo.Text &= ";"

        Try
            txtTo.Text &= sValues(0)
        Catch
        End Try

        If txtCC.Text.EndsWith(";") = False And txtCC.Text.Length > 0 _
        Then txtCC.Text &= ";"

        Try
            txtCC.Text &= sValues(1)
        Catch
        End Try

        If txtBcc.Text.EndsWith(";") = False And txtBcc.Text.Length > 0 _
        Then txtBcc.Text &= ";"

        Try
            txtBcc.Text &= sValues(2)
        Catch
        End Try
    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, mnuFile, "Mailing Lists")
            Return
        End If

        ofd.Title = "Please select the file to read email addresses from"

        ofd.ShowDialog()

        If ofd.FileName.Length > 0 Then
            If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
            oField.Text &= ";"

            oField.Text &= "<[f]" & ofd.FileName & ">;"
        End If
    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click
        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, mnuDB, "Mailing Lists")
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(Me.m_eventID)

        If sTemp.Length > 0 Then
            If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"
            oField.Text &= sTemp & ";"
        End If
    End Sub

    Private Sub txtSubject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSubject.TextChanged

    End Sub

    Private Sub txtSubject_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSubject.GotFocus
        oField = txtSubject
    End Sub

    Private Sub txtSubject_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtSubject.MouseUp
        oField = txtSubject
    End Sub

    Private Sub txtMsg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        oField = txtMsg.txtBody
    End Sub

    Private Sub txtMsg_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        oField = txtMsg.txtBody
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch
        End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch
        End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch
        End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch
        End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.m_eventID)
        oInsert.m_EventBased = Me.m_eventBased
        'oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems
        oItem.m_EventID = Me.m_eventID

        oField.SelectedText = oItem._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub cmdInsert_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert.MouseUp
        mnuSubInsert.Show(cmdInsert, New Point(e.X, e.Y))
    End Sub


    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click

    End Sub

    Private Sub mnuMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub mnuSig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSig.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub





    Private Sub mnuDefSubject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefSubject.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    'Handles mnuAttach.Click, MenuItem13.Click
    '    Dim oUI As New clsMarsUI
    '        oField.SelectedText = oUI.ReadRegistry( _
    '         "DefAttach", "")
    '    End Sub

    '    Private Sub cmdTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTo.Click

    '    End Sub

    Private Sub txtAttach_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttach.GotFocus
        If gnEdition >= MarsGlobal.gEdition.ENTERPRISE Then
            oField = txtAttach
        End If
    End Sub

    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        Dim oInsert As New frmInserter(Me.m_eventID)

        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub cmbSMTPServer_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMTPServer.DropDown
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        SQL = "SELECT * FROM SMTPServers"

        cmbSMTPServer.Items.Clear()
        cmbSMTPServer.Items.Add("Default")

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            cmbSMTPServer.Items.Add(oRs("smtpname").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Public Sub AddForwardMail(Optional ByVal conditionID As Integer = 99999)
        Dim SQL As String

        Me.cmbSMTPServer.Text = "Default"
        Me.cmbMailFormat.SelectedIndex = 1
        Label1.Visible = False
        Me.txtName.Visible = False
        GroupBox1.Location = New Point(8, 7)
        Me.Text = "Forward Email"
        Me.chkAttachOrignal.Visible = True

        'tooltips
        Dim info As New DevComponents.DotNetBar.SuperTooltipInfo

        info.HeaderText = "Forward Email"
        info.BodyText = "Modifying this field will replace the value in the originating mail"
        info.HeaderVisible = True

        SuperT.SetSuperTooltip(Me.txtSubject, info)

        'reload info
        SQL = "SELECT * FROM ForwardMailAttr WHERE ConditionID =" & conditionID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Application.DoEvents()
        ElseIf oRs.EOF = True Then
            oRs.Close()
        Else
            txtTo.Text = IsNull(oRs("sendto").Value)
            txtCC.Text = IsNull(oRs("sendcc").Value)
            txtBcc.Text = IsNull(oRs("sendbcc").Value)
            txtSubject.Text = IsNull(oRs("mailsubject").Value)
            txtMsg.Text = IsNull(oRs("mailbody").Value)
            txtAttach.Text = IsNull(oRs("attachments").Value)
            Me.cmbMailFormat.Text = IsNull(oRs("mailformat").Value, "TEXT")
            Me.cmbSMTPServer.Text = IsNull(oRs("mailserver").Value, "Default")

            Try
                Me.txtSenderName.Text = IsNull(oRs("sendername").Value)
                Me.txtSenderAddress.Text = IsNull(oRs("senderaddress").Value)
            Catch : End Try

            Try
                Me.chkAttachOrignal.Checked = IsNull(oRs("attachmsg").Value, 0)
            Catch ex As Exception

            End Try
            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If


        Dim cols As String
        Dim vals As String

        SQL = "DELETE FROM ForwardMailAttr WHERE ConditionID =" & conditionID

        clsMarsData.WriteData(SQL)

        cols = "MailID,ConditionID,SendTo,SendCC,SendBCC,MailSubject,MailBody,Attachments," & _
        "MailFormat,MailServer,SenderName,SenderAddress,AttachMsg"

        vals = clsMarsData.CreateDataID("forwardmailattr", "mailid") & "," & _
        conditionID & "," & _
        "'" & SQLPrepare(Me.txtTo.Text) & "'," & _
        "'" & SQLPrepare(Me.txtCC.Text) & "'," & _
        "'" & SQLPrepare(Me.txtBcc.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtMsg.Text) & "'," & _
        "'" & SQLPrepare(Me.txtAttach.Text) & "'," & _
        "'" & SQLPrepare(Me.cmbMailFormat.Text) & "'," & _
        "'" & SQLPrepare(Me.cmbSMTPServer.Text) & "', " & _
        "'" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSenderAddress.Text) & "'," & _
        Convert.ToInt16(Me.chkAttachOrignal.Checked)

        clsMarsData.DataItem.InsertData("ForwardMailAttr", cols, vals)

    End Sub

    Private Sub mnuInserter_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuInserter.Popup
        Dim textBox As Control = mnuInserter.SourceControl

        Select Case textBox.Name
            Case txtTo.Name, txtCC.Name, txtBcc.Name
                Me.MenuItem12.Visible = False
                Me.mnuSpell.Visible = False
                Me.MenuItem5.Visible = False
                Me.mnuDefSubject.Visible = False
                Me.mnuMsg.Visible = False
                Me.mnuSig.Visible = False
                Me.mnuAttach.Visible = False
            Case Else
                Me.MenuItem12.Visible = True
                Me.mnuSpell.Visible = True
                Me.MenuItem5.Visible = True
                Me.mnuDefSubject.Visible = True
                Me.mnuMsg.Visible = True
                Me.mnuSig.Visible = True
                Me.mnuAttach.Visible = True
        End Select
    End Sub

    Private Sub txtSenderAddress_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSenderAddress.GotFocus, txtSenderName.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub

    Private Sub txtSenderName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSenderName.TextChanged, txtSenderAddress.TextChanged
        Dim txt As DevComponents.DotNetBar.Controls.TextBoxX = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX)

        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, sender, "Multiple SMTP Servers")
            txtSenderName.Text = ""
            txtSenderAddress.Text = ""
        ElseIf clsMarsSecurity._HasGroupAccess("Customize Sender Information (SMTP)") = False And txt.Text <> "" Then
            txt.Text = ""
            txt.Enabled = False
            Return
        End If
    End Sub

    Private Sub cmbMailFormat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMailFormat.SelectedIndexChanged
        If cmbMailFormat.Text.ToLower = "text" Then
            txtMsg.bodyFormat = ucEditorX.format.TEXT
        Else
            txtMsg.bodyFormat = ucEditorX.format.HTML
        End If
    End Sub

    Public Function quickEmailTask()
        SuperTabItem2.Visible = False

        txtName.Text = "Send a Quick Email"
        txtName.Enabled = False
        cmdOK.Text = "Send"
        cmbSMTPServer.Text = "Default"

        Me.ShowDialog()

        If UserCancel = False Then
            Dim msg As clsMarsMessaging = New clsMarsMessaging
            Dim ok As Boolean

            If MailType = gMailType.MAPI Then
                ok = msg.SendMAPI(txtTo.Text, txtSubject.Text, txtMsg.Text, "Single", , 0, , txtCC.Text, txtBcc.Text, False, cmbMailFormat.Text)
            ElseIf MailType = gMailType.SMTP Then
                Dim message As String = txtMsg.Text

                If cmbMailFormat.Text.ToLower.Contains("html") Then
                    message = message.Replace(vbCrLf, "<br>")
                End If

                ok = msg.SendSMTP(txtTo.Text, txtSubject.Text, txtMsg.Text, "Single", , 0, txtAttach.Text, txtCC.Text, txtBcc.Text, , False, cmbMailFormat.Text, False, , cmbMailFormat.Text, cmbSMTPServer.Text,
                              , txtSenderName.Text, txtSenderAddress.Text)
            ElseIf MailType = gMailType.GROUPWISE Then
                ok = msg.SendGROUPWISE(txtTo.Text, txtCC.Text, txtBcc.Text, txtSubject.Text, txtMsg.Text, "", "Single", txtAttach.Text, True, False, "", , , , cmbMailFormat.Text)
            End If

            If ok = True Then
                MessageBox.Show("Email sent successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Email was not sent due to an error", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Function
End Class
