﻿Public Class systemfolder
    Public Sub New()

    End Sub

    Public Function getpackagesDue() As ArrayList
        Dim SQL As String = "SELECT PackageAttr.PackID FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
                       "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & _
                       "Status = 1 AND " & _
                       "DATEDIFF([d], [CurrentDate], NextRun) <=0"

        If gConType = "DAT" Then
            SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim packID As Integer = oRs(0).Value
                Dim pack As Package = New Package(packID)

                results.Add(pack)

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return results
    End Function

    Public Function getreportsDue() As ArrayList
        Dim SQL As String = "SELECT ReportAttr.ReportID FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
                        "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                        "Status = 1 AND " & _
                        "DATEDIFF([d], [CurrentDate],NextRun) <= 0"

        If gConType = "DAT" Then
            SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim reportID As Integer = oRs(0).Value
                Dim rpt As cssreport = New cssreport(reportID)

                results.Add(rpt)

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return results
    End Function

    Public Function getAutomationsDue() As ArrayList
        Dim SQL As String = "SELECT AutomationAttr.AutoID FROM AutomationAttr INNER JOIN ScheduleAttr ON " & _
                        "AutomationAttr.AutoID = ScheduleAttr.AutoID WHERE " & _
                        "Status = 1 AND " & _
                        "DATEDIFF([d], [CurrentDate],NextRun) <= 0"

        If gConType = "DAT" Then
            SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim autoID As Integer = oRs(0).Value
                Dim auto As Automation = New Automation(autoID)

                results.Add(auto)

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return results

    End Function

    Public Function getTodaysFailedPackages() As ArrayList
        Dim SQL As String = "SELECT PackageAttr.PackID FROM (PackageAttr INNER JOIN ScheduleHistory ON " & _
                        "PackageAttr.PackID = ScheduleHistory.PackID) INNER JOIN ScheduleAttr ON " & _
                        "PackageAttr.PackID = ScheduleAttr.PackID WHERE Success =0 AND " & _
                        "DATEDIFF([d], [CurrentDate],EntryDate) = 0"

        If gConType = "DAT" Then
            SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim packID As Integer = oRs(0).Value
                Dim pack As Package = New Package(packID)

                results.Add(pack)

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return results
    End Function

    Public Function getTodaysFailedReports() As ArrayList
        Dim SQL As String = "SELECT ReportAttr.ReportID FROM (ReportAttr INNER JOIN ScheduleHistory ON " & _
                        "ReportAttr.ReportID = ScheduleHistory.ReportID) INNER JOIN " & _
                        "ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                        "Success =0 AND " & _
                        "DATEDIFF([d], [CurrentDate],EntryDate) = 0"

        If gConType = "DAT" Then
            SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim reportID As Integer = oRs(0).Value
                Dim rpt As cssreport = New cssreport(reportID)

                results.Add(rpt)

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return results
    End Function
    
End Class
