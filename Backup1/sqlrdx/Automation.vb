﻿Public Class Automation
    Dim m_autoID As Integer
    Dim m_concernedRow As ADODB.Recordset
    Public m_schedule As Schedule
    Dim addNew As Boolean

    Sub New(ByVal autoID As Integer)
        m_autoID = autoID

        m_schedule = New Schedule(m_autoID, Schedule.parentscheduleType.AUTOMATION)
    End Sub

    Public ReadOnly Property ID As Integer
        Get
            Return m_autoID
        End Get
    End Property
    Private ReadOnly Property concernedRow As ADODB.Recordset
        Get
            If m_concernedRow IsNot Nothing Then
                Return m_concernedRow
            Else
                m_concernedRow = clsMarsData.GetData("SELECT * FROM automationattr WHERE autoid =" & m_autoID)

                If m_concernedRow IsNot Nothing AndAlso m_concernedRow.EOF = False Then
                    Return m_concernedRow
                Else
                    Throw New Exception("Could not locate the automation schedule referenced by ID " & m_autoID)
                    Return Nothing
                End If
            End If
        End Get
    End Property

    Public ReadOnly Property schedulePath As String
        Get
            Dim parent As Integer = Me.autoParent
            Dim folder As folder = New folder(parent)

            Return folder.getFolderPath()
        End Get
    End Property

    
    Public ReadOnly Property AutoName() As String
        Get
            Return concernedRow.Fields("autoname").Value
        End Get
      
    End Property

    Public ReadOnly Property LastRun() As String
        Get
            Try
                Return m_schedule.lastRun
            Catch
                Return ""
            End Try
        End Get
    End Property


    Public ReadOnly Property NextRun() As Date
        Get
            Return m_schedule.nextRun
        End Get
    End Property

    Public ReadOnly Property LastResult() As String
        Get
            Return m_schedule.lastResult
        End Get
    End Property

    Public ReadOnly Property Status() As Boolean
        Get
            Return m_schedule.status
        End Get
    End Property

    Public ReadOnly Property Owner() As String
        Get
            Return concernedRow.Fields("owner").Value
        End Get
    End Property

    Public ReadOnly Property automationImage() As Image
        Get
            If Status = True Then
                Return My.Resources.document_gear2
            Else
                Return MakeGrayscale3(My.Resources.document_gear2)
            End If
        End Get
    End Property

    Public ReadOnly Property largeimageFileUrl() As String
        Get
            If Status = True Then
                Return "largeimages/document_gear.png"
            Else
                Return "largeimages/document_gear_bw.png"
            End If
        End Get
    End Property

    Public ReadOnly Property autoParent() As String
        Get
            Return concernedRow.Fields("parent").Value
        End Get
    End Property

   
End Class
