﻿Public Class destination
    Dim m_destinationID As Integer
    Dim m_concernedRow As ADODB.Recordset
    Dim newDestination As Boolean = False


    Sub New(ByVal destID As Integer)
        m_destinationID = destID
        destinationID = destID
        m_concernedRow = concernedRow
    End Sub

    ''' <summary>
    ''' return a data table of the default destinations with columns id, name, type, format
    ''' ID contains destinationid as well as image
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function getDefualtDestinations()
        Try

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM destinationattr WHERE reportid = 2777 AND packid = 2777 AND smartid = 2777")
            Dim destTable As DataTable = New DataTable("defaultDestinations")

            destTable.Columns.Add("ID")
            destTable.Columns.Add("Name")
            destTable.Columns.Add("Type")
            destTable.Columns.Add("Format")


            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    Dim r As DataRow = destTable.Rows.Add

                    Dim id As Integer = oRs("destinationid").Value
                    Dim dest As destination = New destination(id)
                    Dim newid As String = "<img src='" + "' width='10' height='10' alt='" & id & "'/>"

                    r("id") = newid
                    r("name") = oRs("destinationname").Value
                    r("type") = oRs("destinationtype").Value
                    r("format") = oRs("outputformat").Value

                    oRs.MoveNext()
                Loop
            End If

            Return destTable
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Protected ReadOnly Property concernedRow() As ADODB.Recordset
        Get
            If m_concernedRow Is Nothing Then
                'Get the data from the database

                m_concernedRow = clsMarsData.GetData("SELECT * FROM destinationattr WHERE destinationid =" & m_destinationID)

                'If the row/destination exists then return this row/destination to the user
                If m_concernedRow IsNot Nothing AndAlso m_concernedRow.EOF = False Then

                    Return m_concernedRow
                Else
                    Throw New Exception("Could not find the destination referenced by ID " & m_destinationID)
                End If
            Else
                Return m_concernedRow
            End If
        End Get
    End Property

    Public Property destinationID() As Integer
        Get
            Return concernedRow.Fields("destinationid").Value
        End Get
        Set(value As Integer)
            m_destinationID = value
        End Set
    End Property

    Public ReadOnly Property destinationType() As String
        Get
            Return concernedRow.Fields("destinationtype").Value
        End Get
    End Property

    Public ReadOnly Property destinationName As String
        Get
            Return concernedRow.Fields("destinationname").Value
        End Get
      
    End Property

    Public ReadOnly Property destinationFormat As String
        Get
            Return concernedRow.Fields("outputformat").Value
        End Get
    End Property
    

    Public ReadOnly Property destinationCustomExtension() As String
        Get
            Return (IsNull(concernedRow.Fields("customext").Value, ""))
        End Get
    End Property

    Public ReadOnly Property destinationAppendDT() As Boolean
        Get
            Return IsNull(concernedRow.Fields("appenddatetime").Value, 0)
        End Get
    End Property

    Public ReadOnly Property destinationDTFormat() As String
        Get
            Return IsNull(concernedRow.Fields("datetimeformat").Value, "")
        End Get
    End Property

    Public ReadOnly Property destinationCustomName() As String
        Get
            Return IsNull(concernedRow.Fields("customname").Value, "")
        End Get
    End Property

    Public ReadOnly Property destinationCompress() As Boolean
        Get
            Return concernedRow.Fields("compress").Value
        End Get
    End Property

    Public ReadOnly Property destinationEncryptZip() As Boolean
        Get
            Return concernedRow.Fields("encryptzip").Value
        End Get
    End Property

    Public ReadOnly Property destinationEncryptZipLevel As Integer
        Get
            Return concernedRow.Fields("encryptziplevel").Value
        End Get
    End Property

    Public ReadOnly Property destinationOrderID As Boolean
        Get
            Return concernedRow.Fields("destorderid").Value
        End Get
    End Property

    Public ReadOnly Property destinationStatus As Boolean
        Get
            Return IsNull(concernedRow.Fields("enabledstatus").Value, 1)
        End Get
    End Property

    Public ReadOnly Property destinationReportID As Integer
        Get
            Return concernedRow.Fields("reportid").Value
        End Get
    End Property

    Public ReadOnly Property destinationPackID As Integer
        Get
            Return IsNull(concernedRow.Fields("packid").Value, 0)
        End Get
    End Property


    Public ReadOnly Property destinationAdjustStamp() As Integer
        Get
            Return concernedRow.Fields("adjuststamp").Value
        End Get
    End Property

End Class
