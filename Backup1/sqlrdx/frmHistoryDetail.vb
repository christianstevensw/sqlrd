﻿Public Class frmHistoryDetail
    Dim m_historyID As Integer

    Public Sub showDetails(start As Date, finish As Date, details As String, objectID As Integer)
        dtStart.Value = start
        dtEnd.Value = finish
        m_historyID = objectID
        If executionServer <> "" Then
            details = details & vbCrLf & "Executed on " & executionServer
        End If

        txtDuration.Value = dtEnd.Value.Subtract(dtStart.Value).TotalSeconds
        txtDetails.Text = details



        Me.ShowDialog()
    End Sub

    Private Sub frmHistoryDetail_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub

    Private ReadOnly Property executionServer As String
        Get
            Try
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT serverName FROM schedulehistory WHERE historyid = " & m_historyID)
                Dim serverName As String = ""

                If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                    serverName = IsNull(oRs("servername").Value, "")

                    oRs.Close()
                    oRs = Nothing
                End If

                Return serverName
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property
End Class