﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucPackagedReports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucPackagedReports))
        Me.tvReports = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.btnAdd = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPreview = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUp = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDown = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem1 = New DevComponents.DotNetBar.ButtonItem()
        Me.btnTile = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDetail = New DevComponents.DotNetBar.ButtonItem()
        Me.txtSearch = New DevComponents.DotNetBar.TextBoxItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.menuBar = New DevComponents.DotNetBar.ContextMenuBar()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.Properties = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuStatus = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuPreview = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuRename = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuDelete = New DevComponents.DotNetBar.ButtonItem()
        CType(Me.tvReports, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.menuBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tvReports
        '
        Me.tvReports.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvReports.AllowDrop = True
        Me.tvReports.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvReports.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.menuBar.SetContextMenuEx(Me.tvReports, Me.ButtonItem2)
        Me.tvReports.DragDropNodeCopyEnabled = False
        Me.tvReports.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvReports.Location = New System.Drawing.Point(157, 139)
        Me.tvReports.Name = "tvReports"
        Me.tvReports.NodesConnector = Me.NodeConnector1
        Me.tvReports.NodeStyle = Me.ElementStyle1
        Me.tvReports.PathSeparator = ";"
        Me.tvReports.Size = New System.Drawing.Size(480, 340)
        Me.tvReports.Styles.Add(Me.ElementStyle1)
        Me.tvReports.TabIndex = 0
        Me.tvReports.Text = "AdvTree1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.Class = ""
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.Class = ""
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.Class = ""
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAdd, Me.btnEdit, Me.btnDelete, Me.btnPreview, Me.btnUp, Me.btnDown, Me.ButtonItem1, Me.txtSearch})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(695, 41)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar1.TabIndex = 1
        '
        '
        '
        Me.RibbonBar1.TitleStyle.Class = ""
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.Class = ""
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnAdd
        '
        Me.btnAdd.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.SubItemsExpandWidth = 14
        Me.btnAdd.Text = "Add"
        '
        'btnEdit
        '
        Me.btnEdit.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.SubItemsExpandWidth = 14
        Me.btnEdit.Text = "Design"
        '
        'btnDelete
        '
        Me.btnDelete.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.SubItemsExpandWidth = 14
        Me.btnDelete.Text = "Delete"
        '
        'btnPreview
        '
        Me.btnPreview.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.SubItemsExpandWidth = 14
        Me.btnPreview.Text = "Preview"
        '
        'btnUp
        '
        Me.btnUp.BeginGroup = True
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.SubItemsExpandWidth = 14
        Me.btnUp.Text = "ButtonItem5"
        '
        'btnDown
        '
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.SubItemsExpandWidth = 14
        Me.btnDown.Text = "ButtonItem1"
        '
        'ButtonItem1
        '
        Me.ButtonItem1.Name = "ButtonItem1"
        Me.ButtonItem1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnTile, Me.btnDetail})
        Me.ButtonItem1.SubItemsExpandWidth = 14
        Me.ButtonItem1.Text = "View"
        '
        'btnTile
        '
        Me.btnTile.Name = "btnTile"
        Me.btnTile.Text = "Tile"
        '
        'btnDetail
        '
        Me.btnDetail.Name = "btnDetail"
        Me.btnDetail.Text = "Detail"
        '
        'txtSearch
        '
        Me.txtSearch.BeginGroup = True
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.TextBoxWidth = 250
        Me.txtSearch.WatermarkColor = System.Drawing.SystemColors.GrayText
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = True
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'menuBar
        '
        Me.menuBar.AntiAlias = True
        Me.menuBar.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem2})
        Me.menuBar.Location = New System.Drawing.Point(465, 485)
        Me.menuBar.Name = "menuBar"
        Me.menuBar.Size = New System.Drawing.Size(75, 25)
        Me.menuBar.Stretch = True
        Me.menuBar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.menuBar.TabIndex = 2
        Me.menuBar.TabStop = False
        '
        'ButtonItem2
        '
        Me.ButtonItem2.AutoExpandOnClick = True
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.Properties, Me.mnuStatus, Me.mnuPreview, Me.mnuExecute, Me.mnuRefresh, Me.mnuRename, Me.mnuDelete})
        Me.ButtonItem2.Text = "ButtonItem2"
        '
        'Properties
        '
        Me.Properties.Image = CType(resources.GetObject("Properties.Image"), System.Drawing.Image)
        Me.Properties.Name = "Properties"
        Me.Properties.Text = "Properties"
        '
        'mnuStatus
        '
        Me.mnuStatus.Name = "mnuStatus"
        Me.mnuStatus.Text = "Enabled"
        '
        'mnuPreview
        '
        Me.mnuPreview.BeginGroup = True
        Me.mnuPreview.Image = CType(resources.GetObject("mnuPreview.Image"), System.Drawing.Image)
        Me.mnuPreview.Name = "mnuPreview"
        Me.mnuPreview.Text = "Preview"
        '
        'mnuExecute
        '
        Me.mnuExecute.Image = CType(resources.GetObject("mnuExecute.Image"), System.Drawing.Image)
        Me.mnuExecute.Name = "mnuExecute"
        Me.mnuExecute.Text = "Execute"
        '
        'mnuRefresh
        '
        Me.mnuRefresh.BeginGroup = True
        Me.mnuRefresh.Image = CType(resources.GetObject("mnuRefresh.Image"), System.Drawing.Image)
        Me.mnuRefresh.Name = "mnuRefresh"
        Me.mnuRefresh.Text = "Refresh"
        '
        'mnuRename
        '
        Me.mnuRename.Image = CType(resources.GetObject("mnuRename.Image"), System.Drawing.Image)
        Me.mnuRename.Name = "mnuRename"
        Me.mnuRename.Text = "Rename"
        '
        'mnuDelete
        '
        Me.mnuDelete.BeginGroup = True
        Me.mnuDelete.Image = CType(resources.GetObject("mnuDelete.Image"), System.Drawing.Image)
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Text = "Delete"
        '
        'ucPackagedReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.menuBar)
        Me.Controls.Add(Me.tvReports)
        Me.Controls.Add(Me.RibbonBar1)
        Me.Name = "ucPackagedReports"
        Me.Size = New System.Drawing.Size(695, 521)
        CType(Me.tvReports, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.menuBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tvReports As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnAdd As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPreview As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUp As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDown As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents txtSearch As DevComponents.DotNetBar.TextBoxItem
    Friend WithEvents ButtonItem1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnTile As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDetail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents menuBar As DevComponents.DotNetBar.ContextMenuBar
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Properties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuStatus As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuPreview As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuDelete As DevComponents.DotNetBar.ButtonItem

End Class
