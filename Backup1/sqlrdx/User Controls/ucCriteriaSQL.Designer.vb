﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucCriteriaSQL
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucCriteriaSQL))
        Me.btnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddCriteria = New DevComponents.DotNetBar.ButtonX()
        Me.txtCriteria = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbColumns = New System.Windows.Forms.ComboBox()
        Me.cmbAndOr = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'btnRemove
        '
        Me.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemove.Enabled = False
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(670, 9)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(41, 20)
        Me.btnRemove.TabIndex = 5
        '
        'btnAddCriteria
        '
        Me.btnAddCriteria.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddCriteria.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAddCriteria.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddCriteria.Image = CType(resources.GetObject("btnAddCriteria.Image"), System.Drawing.Image)
        Me.btnAddCriteria.Location = New System.Drawing.Point(623, 9)
        Me.btnAddCriteria.Name = "btnAddCriteria"
        Me.btnAddCriteria.Size = New System.Drawing.Size(41, 20)
        Me.btnAddCriteria.TabIndex = 6
        '
        'txtCriteria
        '
        Me.txtCriteria.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtCriteria.Border.Class = "TextBoxBorder"
        Me.txtCriteria.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCriteria.Location = New System.Drawing.Point(277, 9)
        Me.txtCriteria.Name = "txtCriteria"
        Me.txtCriteria.Size = New System.Drawing.Size(254, 20)
        Me.txtCriteria.TabIndex = 4
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.FormattingEnabled = True
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", ">", "<", ">=", "<=", "CONTAINS", "STARTS WITH", "ENDS WITH"})
        Me.cmbOperator.Location = New System.Drawing.Point(146, 8)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(125, 21)
        Me.cmbOperator.TabIndex = 3
        '
        'cmbColumns
        '
        Me.cmbColumns.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumns.FormattingEnabled = True
        Me.cmbColumns.Items.AddRange(New Object() {"STARTS WITH", "ENDS WITH", "CONTAINS", "DOES NOT CONTAIN"})
        Me.cmbColumns.Location = New System.Drawing.Point(15, 8)
        Me.cmbColumns.Name = "cmbColumns"
        Me.cmbColumns.Size = New System.Drawing.Size(125, 21)
        Me.cmbColumns.TabIndex = 3
        '
        'cmbAndOr
        '
        Me.cmbAndOr.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbAndOr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAndOr.FormattingEnabled = True
        Me.cmbAndOr.Items.AddRange(New Object() {"AND", "OR"})
        Me.cmbAndOr.Location = New System.Drawing.Point(537, 9)
        Me.cmbAndOr.Name = "cmbAndOr"
        Me.cmbAndOr.Size = New System.Drawing.Size(81, 21)
        Me.cmbAndOr.TabIndex = 3
        '
        'ucCriteriaSQL
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAddCriteria)
        Me.Controls.Add(Me.txtCriteria)
        Me.Controls.Add(Me.cmbAndOr)
        Me.Controls.Add(Me.cmbColumns)
        Me.Controls.Add(Me.cmbOperator)
        Me.Name = "ucCriteriaSQL"
        Me.Size = New System.Drawing.Size(743, 39)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddCriteria As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtCriteria As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumns As System.Windows.Forms.ComboBox
    Friend WithEvents cmbAndOr As System.Windows.Forms.ComboBox

End Class
