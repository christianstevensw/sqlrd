﻿Imports Microsoft.Reporting.WinForms
Imports System.Xml
Imports System.Collections.Generic

Public Class ucParametersList
    Public m_serverparametersCollection As New System.Collections.Generic.List(Of ssrsParameter)
    Dim cancel As Boolean
    Dim ofd As New OpenFileDialog
    Dim formatOptions As Object
    Dim m_reportID As String = ""
    Dim serverUser, serverPassword, serverUrl, reportPath, rdlPath As String
    Dim formsAuth As Boolean
    Dim parDefaults As Hashtable
    Dim m_rv As Microsoft.Reporting.WinForms.ReportViewer
    Dim oUI As clsMarsUI = New clsMarsUI
    Public m_serverOnlineStatus As Boolean = True

    Public m_controlFiresEvents As Boolean = False

    ''' <summary>
    ''' This is a list of the parameters names. String
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_ParametersCollection As ArrayList
        Get
            Dim arr As ArrayList = New ArrayList

            For Each ctrl As Control In Me.Controls
                If TypeOf ctrl Is ucParameters Then
                    arr.Add(CType(ctrl, ucParameters).m_parameterName)
                End If
            Next

            Return arr
        End Get
    End Property
    ''' <summary>
    ''' This is a collections of ucParameters in the list
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_ParameterControlsCollection As System.Collections.Generic.List(Of ucParameters)
        Get
            Dim arr As System.Collections.Generic.List(Of ucParameters) = New System.Collections.Generic.List(Of ucParameters)

            For Each ctrl As Control In Me.Controls
                If TypeOf ctrl Is ucParameters Then
                    arr.Add(ctrl)
                End If
            Next

            Return arr
        End Get
    End Property

    Public Sub generateParametersDataTable(ByRef dt As DataTable)
        If dt Is Nothing Then
            dt = New DataTable

            With dt.Columns
                .Add("Name")
                .Add("Type")
                .Add("MultiValue")
                .Add("Label")
                .Add("Value")
                .Add("AvailableValues")
            End With
        End If

        For Each par As ucParameters In Me.m_ParameterControlsCollection
            Dim rows() As DataRow = dt.Select("name = '" & SQLPrepare(par.m_parameterName) & "'")

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                Continue For
            Else
                Dim dr As DataRow = dt.Rows.Add

                dr("name") = par.m_parameterName
                dr("type") = par.m_parameterType
                dr("multivalue") = par.m_allowMultipleValues
                dr("label") = par.m_ParameterPrompt
                dr("value") = par.m_parameterValue
                dr("availablevalues") = par.m_availableValues
            End If
        Next
    End Sub


    Public parameterControls As New ArrayList


    Public Property m_serverUrl As String
        Get
            Return serverUrl
        End Get
        Set(value As String)
            serverUrl = value
        End Set
    End Property

    Public Property m_serverUser As String
        Get
            Return serverUser
        End Get
        Set(value As String)
            serverUser = value
        End Set
    End Property

    Public Property m_serverPassword() As String
        Get
            Return serverPassword
        End Get
        Set(ByVal value As String)
            serverPassword = value
        End Set
    End Property

    Public Property m_reportPath As String
        Get
            Return reportPath
        End Get
        Set(value As String)
            reportPath = value
        End Set
    End Property

    Public Property m_rdlPath() As String
        Get
            Return rdlPath
        End Get
        Set(ByVal value As String)
            rdlPath = value
        End Set
    End Property

    Public Property m_formsAuth As Boolean
        Get
            Return formsAuth
        End Get
        Set(value As Boolean)
            formsAuth = value
        End Set
    End Property

    Private Sub ucParametersList_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.BackColor = Color.Transparent

    End Sub

    Public Function getParameterValidValues(ByVal index As Integer, ByVal parameterName As String) As System.Collections.Generic.List(Of KeyValuePair(Of String, String))
        Dim hs As System.Collections.Generic.List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
        Dim f As String = "parameterslisting.debug"
        Dim oPars() As Object

        If m_serverOnlineStatus = False Then Return hs

        clsMarsDebug.writeToDebug(f, "Server Status: Online", True)

        Try

            Dim pValues As IList = Nothing
            Dim p As ReportParameterInfo

            clsMarsDebug.writeToDebug(f, "Creating report instance", True)

            createReportInstance()

            If m_rv Is Nothing Then
                Return Nothing
            End If
            'get the parameter inde
            clsMarsDebug.writeToDebug(f, "Parameter Index: " & index, True)

            If index = 0 Then 'its the first parameter so we can just read the available values
                Dim ps As ReportParameterInfoCollection

                If m_serverOnlineStatus = True Then
                    ps = m_rv.ServerReport.GetParameters() 'get the parameters collection from the serverreport
                End If

                If ps IsNot Nothing Then

                    For Each p In ps 'loop through the parameters

                        If p.Name = parameterName Then
                            pValues = p.ValidValues

                            If p.ValidValues IsNot Nothing Then
                                For Each val As ValidValue In pValues
                                    hs.Add(New KeyValuePair(Of String, String)(val.Label, val.Value))
                                Next
                            End If

                            Exit For
                        End If
                    Next
                End If
            Else
                clsMarsDebug.writeToDebug(f, "Setting previous parameter values", True)

                'we need to set the previous parameters first before getting the available values for this one
                Dim reportPars() As ReportParameter = Nothing
                ReDim reportPars(index - 1)

                For I As Integer = 0 To index - 1
                    ReDim Preserve oPars(I)

                    reportPars(I) = New ReportParameter

                    reportPars(I).Name = parameterControls(I).m_parameterName

                    clsMarsDebug.writeToDebug(f, "Parameter Name:" & parameterControls(I).m_parameterName, True)

                    Dim value As String = ""

                    Try
                        value = clsMarsParser.Parser.ParseString(parameterControls(I).m_rawparameterValue)
                    Catch : End Try

                    clsMarsDebug.writeToDebug(f, "Parameter Label: " & value, True)
                    '//test to see if the parameters has values

                    If value.Contains("|") = False Then   'does the value contain multiple values?
                        If value.ToLower = "[sql-rdnull]" Then
                            reportPars(I).Values.Clear()
                            reportPars(I).Values.Add(Nothing)
                        ElseIf value.ToLower = "[sql-rddefault]" Then
                            clsMarsDebug.writeToDebug(f, "Default value specified for the parameter", True)
                            reportPars(I).Values.Clear()
                        ElseIf value.ToLower = "[selectall]" Then
                            clsMarsDebug.writeToDebug(f, "selecting all the values", True)

                            Dim tmp As Hashtable = New Hashtable
                            Dim values As ArrayList = New ArrayList
                            Dim rpt As clsMarsReport = New clsMarsReport

                            If I = 0 Then
                                values = rpt.getParameterValidValues(m_rv, reportPars(I).Name, tmp, m_formsAuth)

                                Dim pvalue As String

                                For Each val As String In values
                                    pvalue &= val & "|"
                                Next

                                oPars(I) = pvalue
                            Else
                                For x As Integer = 0 To I - 1
                                    tmp.Add(reportPars(x).Name, oPars(x))
                                Next

                                values = rpt.getParameterValidValues(m_rv, reportPars(I).Name, tmp, m_formsAuth)

                                Dim pvalue As String

                                For Each val As String In values
                                    pvalue &= val & "|"
                                Next

                                oPars(I) = pvalue
                            End If

                            Dim tempValues As String = ""

                            For Each str As String In values

                                If str.Length > 0 Then reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(str))

                                tempValues &= str & "|"
                            Next
                        Else
                            reportPars(I).Values.Add(getserverParameterValue(reportPars(I).Name, value))
                            'reportPars(I).Values.Add(value)
                        End If
                    Else
                        For Each s As String In value.Split("|")
                            If s.ToLower = "[sql-rdnull]" Then
                                reportPars(I).Values.Clear()
                                reportPars(I).Values.Add(Nothing)
                            ElseIf s.ToLower = "[sql-rddefault]" Then
                                clsMarsDebug.writeToDebug(f, "Default value specified for the parameter", True)
                                reportPars(I).Values.Clear()
                            ElseIf s.ToLower = "[selectall]" Then
                                clsMarsDebug.writeToDebug(f, "selecting all the values", True)

                                Dim tmp As Hashtable = New Hashtable
                                Dim values As ArrayList = New ArrayList
                                Dim rpt As clsMarsReport = New clsMarsReport

                                If I = 0 Then
                                    values = rpt.getParameterValidValues(m_rv, reportPars(I).Name, tmp, m_formsAuth)

                                    Dim pvalue As String

                                    For Each val As String In values
                                        pvalue &= val & "|"
                                    Next

                                    oPars(I) = pvalue
                                Else
                                    For x As Integer = 0 To I - 1
                                        tmp.Add(reportPars(x).Name, oPars(x))
                                    Next

                                    values = rpt.getParameterValidValues(m_rv, reportPars(I).Name, tmp, m_formsAuth)

                                    Dim pvalue As String

                                    For Each val As String In values
                                        pvalue &= val & "|"
                                    Next

                                    oPars(I) = pvalue
                                End If

                                Dim tempValues As String = ""

                                For Each str As String In values
                                    If str.Length > 0 Then reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(str))

                                    tempValues &= str & "|"
                                Next


                            ElseIf s <> "" Then
                                reportPars(I).Values.Add(getserverParameterValue(reportPars(I).Name, s))
                                'reportPars(I).Values.Add(s)

                                clsMarsDebug.writeToDebug(f, "Actual Value: " & s, True)
                            End If
                        Next
                    End If
                Next

                'lets set the parameters that we have values for
                If m_serverOnlineStatus = False Then
                    Return hs
                End If

                clsMarsDebug.writeToDebug(f, "Sending values to the server", True)

                m_rv.ServerReport.SetParameters(reportPars)

                'now we can get the values of our target parameter i.e. at parIndex
                Dim ps As ReportParameterInfoCollection = m_rv.ServerReport.GetParameters

                For Each p In ps
                    If p.Name = parameterName Then
                        pValues = p.ValidValues

                        If pValues IsNot Nothing Then
                            For Each val As ValidValue In pValues
                                hs.Add(New KeyValuePair(Of String, String)(val.Label, val.Value))
                            Next
                        End If

                        Exit For
                    End If
                Next

                Dim par As ssrsParameter

                For Each par In m_serverparametersCollection '//see if we already have the available values ready
                    If par.parameterName.ToLower = parameterName.ToLower Then
                        par.parameterAvailableValues = hs

                        Exit For
                    End If
                Next

            End If
        Catch ex As Exception
            clsMarsDebug.writeToDebug(f, "Error setting parameters " & ex.ToString, True)
        End Try

        Return hs
    End Function

    Public Function getParameterValuesFromRDL(ByVal sFile As String, parameterName As String, parameterType As String, multiValue As Boolean) As List(Of KeyValuePair(Of String, String))

        Dim I As Integer = 0
        Dim x As Integer = 0
        Dim defaultValues As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
        'do we show hidden parameters
        Dim showHidden As Boolean = True

        Dim oXML As New Xml.XmlDocument

        oXML.Load(sFile)

        Dim oNodes As XmlNodeList

        oNodes = oXML.GetElementsByTagName("ReportParameter")

        For Each oNode As XmlNode In oNodes
            Dim hidden As Boolean = False

            Dim parName As String = oNode.Attributes("Name").Value

            If parName.ToLower = parameterName.ToLower Then
                Try
                    hidden = oNode.Item("Hidden").InnerText
                Catch ex As Exception
                    hidden = False
                End Try

                If hidden = True And showHidden = False Then Continue For

                Try
                    Dim oChild As XmlNode

                    oChild = oNode.Item("ValidValues").ChildNodes.Item(0) 'oNode.ChildNodes(2).ChildNodes(0)

                    For Each o As XmlNode In oChild.ChildNodes
                        Dim value, label As String

                        For Each ch As XmlNode In o.ChildNodes
                            If ch.Name.ToLower = "value" Then
                                value = ch.InnerText
                            ElseIf ch.Name.ToLower = "label" Then
                                label = ch.InnerText
                            End If
                        Next

                        If value Is Nothing Then
                            value = label
                        End If

                        'label = o.ChildNodes(0).InnerText


                        'Try
                        '    value = o.ChildNodes(1).InnerText
                        'Catch
                        '    value = o.ChildNodes(0).InnerText
                        'End Try

                        ' MsgBox("adding label: " & label & " value: " & value)

                        defaultValues.Add(New KeyValuePair(Of String, String)(label, value))   '(label, value)
                    Next

                    'we need to update the m_serverParameters datatable with the new found values

                    'delete the existing values for this parameter
                    'If m_serverParametersTable Is Nothing Then
                    '    m_serverParametersTable = (New clsMarsReport).getserverReportParameters(m_serverUrl, m_reportPath, serverUser, serverPassword, "")
                    'End If
                    Dim par As ssrsParameter

                    For Each par In m_serverparametersCollection
                        If par.parameterName.ToLower = parameterName.ToLower Then
                            Exit For
                        End If
                    Next

                    If par IsNot Nothing Then
                        par.parameterAvailableValues = defaultValues
                    End If
                Catch : End Try
            End If

            I += 1
        Next

        Return defaultValues
    End Function

    Private Sub createReportInstance(Optional formsAuth As Boolean = False)
        'set up the report viewer with the report we are accessing
        Dim rv As Microsoft.Reporting.WinForms.ReportViewer

        rv = New Microsoft.Reporting.WinForms.ReportViewer

        'm_rv = Nothing

        ' m_rv = New Microsoft.Reporting.WinForms.ReportViewer
        rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

        Dim actualURL As String = ""

        For I As Integer = 0 To m_serverUrl.Split("/").GetUpperBound(0) - 1
            actualURL &= m_serverUrl.Split("/")(I) & "/"
        Next

        rv.ServerReport.ReportServerUrl = New Uri(actualURL)
        rv.ServerReport.ReportPath = m_reportPath

        If serverUser <> "" Then
            Dim userDomain As String = ""
            Dim tmpUser As String = serverUser

            getDomainAndUserFromString(tmpUser, userDomain)

            Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)
            rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred

            '//forms auth too
            Try
                If m_formsAuth Then rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, tmpUser, serverPassword, userDomain)
            Catch : End Try

            '//test by getting parameters
            Try
                '  rv.ServerReport.GetParameters()
            Catch ex As Exception
                If ex.Message.ToLower.Contains("logon") Then
                    rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, tmpUser, serverPassword, userDomain)
                End If
            End Try
        Else
            rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultNetworkCredentials
        End If

        '// datasources
        m_rv = rv

        setDatasourceCredentials()
    End Sub

    Public Sub setDatasourceCredentials()
        '  If m_reportID = 0 Then m_reportID = m_reportID

        Dim SQL As String = "SELECT * FROM reportdatasource WHERE reportid = " & m_reportID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim reportDS() As Microsoft.Reporting.WinForms.DataSourceCredentials
        Dim I As Integer = 0

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim dsName As String = oRs("datasourcename").Value
                Dim userid As String = oRs("rptuserid").Value
                Dim password As String = oRs("rptpassword").Value
                Dim dsAlreadySet As Boolean = False

                password = _DecryptDBValue(password)

                If reportDS IsNot Nothing Then
                    For Each ds As Microsoft.Reporting.WinForms.DataSourceCredentials In reportDS
                        If ds.Name.ToLower = dsName.ToLower Then
                            dsAlreadySet = True
                            Exit For
                        End If
                    Next
                End If

                If userid <> "default" And password <> "default" And dsAlreadySet = False Then
                    ReDim Preserve reportDS(I)

                    reportDS(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                    With reportDS(I)
                        .Name = dsName
                        .UserId = userid
                        .Password = password
                    End With

                    I += 1
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing

            If m_rv IsNot Nothing AndAlso reportDS IsNot Nothing Then m_rv.ServerReport.SetDataSourceCredentials(reportDS)
        End If
    End Sub
    Public Function getserverParameterValue(ByVal parameterName As String, ByVal label As String) As String
        Try
            Dim ssrsReport As clsMarsReport = New clsMarsReport

            If m_serverparametersCollection Is Nothing OrElse m_serverparametersCollection.Count = 0 Then m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_formsAuth, , m_reportID)

            '  m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", , m_reportID)


            Dim par As ssrsParameter

            For Each par In m_serverparametersCollection
                If par.parameterName.ToLower = parameterName.ToLower Then
                    Try
                        Dim hs As List(Of KeyValuePair(Of String, String)) = par.parameterAvailableValues
                        Dim value As String

                        For Each pair As KeyValuePair(Of String, String) In hs
                            If pair.Key.ToLower = label.ToLower Then
                                value = pair.Value
                                Exit For
                            End If
                        Next

                        If value Is Nothing Then
                            value = label
                        End If

                        'Dim value As String = hs.Item(label)

                        'If value Is Nothing Then value = label
                        Return value
                    Catch ex As Exception
                        Return label
                    End Try
                End If
            Next

            Return label
        Catch
            Return label
        End Try
    End Function
    Public Function getserverParameterLabel(parameterName As String, ByVal parameterIndex As Integer, ByVal value As String, ByRef valuesList As List(Of KeyValuePair(Of String, String))) As String
        Try
            Dim ssrsReport As clsMarsReport = New clsMarsReport

            If m_serverparametersCollection Is Nothing Then m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_serverOnlineStatus, m_reportID)

            If m_serverparametersCollection.Count = 0 And m_serverOnlineStatus = True Then m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_serverOnlineStatus, m_reportID)

            Dim par As ssrsParameter

            For Each par In m_serverparametersCollection
                If par.parameterName.ToLower = parameterName.ToLower Then
                    Dim hs As List(Of KeyValuePair(Of String, String)) = getParameterValidValues(parameterIndex, parameterName)

                    For Each pair As KeyValuePair(Of String, String) In hs
                        If pair.Value.ToLower = value.ToLower Then
                            valuesList = hs
                            Return pair.Key
                        End If
                    Next


                    'For Each de As DictionaryEntry In hs
                    '    If de.Value.ToString.ToLower = value.ToLower Then
                    '        Return de.Key
                    '    End If
                    'Next
                End If
            Next

            Return value
        Catch
            Return value
        End Try
    End Function

    Public Function getserverParameterLabel(ByVal parameterName As String, ByVal value As String) As String
        Try
            Dim ssrsReport As clsMarsReport = New clsMarsReport

            If m_serverparametersCollection Is Nothing Then m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_serverOnlineStatus, m_reportID)

            If m_serverparametersCollection.Count = 0 And m_serverOnlineStatus = True Then m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_serverOnlineStatus, m_reportID)

            Dim par As ssrsParameter

            For Each par In m_serverparametersCollection
                If par.parameterName.ToLower = parameterName.ToLower Then
                    Dim hs As List(Of KeyValuePair(Of String, String)) = par.parameterAvailableValues

                    For Each pair As KeyValuePair(Of String, String) In hs
                        If pair.Value.ToLower = value.ToLower Then
                            Return pair.Key
                        End If
                    Next

                    'For Each de As DictionaryEntry In hs
                    '    If de.Value.ToString.ToLower = value.ToLower Then
                    '        Return de.Key
                    '    End If
                    'Next
                End If
            Next

            Return value
        Catch
            Return value
        End Try
    End Function
    Public Shared Function getParameterFields(ByVal sFile As String) As String()
        Dim sValue() As String
        Dim I As Integer = 0
        Dim x As Integer = 0
        Dim sDefValues As String
        'do we show hidden parameters
        Dim showHidden As Boolean = False

        Dim oXML As New Xml.XmlDocument

        oXML.Load(sFile)

        Dim oNodes As XmlNodeList

        oNodes = oXML.GetElementsByTagName("ReportParameter")

        For Each oNode As XmlNode In oNodes
            Dim hidden As Boolean = False

            Try
                hidden = oNode.Item("Hidden").InnerText
            Catch ex As Exception
                hidden = False
            End Try

            If hidden = True And showHidden = False Then GoTo skip

            sDefValues = String.Empty

            ReDim Preserve sValue(I)
            Dim multiValue As String = "false"
            Dim prompt As String = ""

            Try
                multiValue = oNode.Item("MultiValue").InnerText
            Catch ex As Exception
                multiValue = "false"
            End Try

            Try
                For Each n As XmlNode In oNode.ChildNodes
                    If n.Name.ToLower = "prompt" Then
                        prompt = n.InnerText
                        Exit For
                    End If
                Next
            Catch ex As Exception
                prompt = ""
            End Try

            sValue(I) = oNode.Attributes(0).Value & "|" & oNode.ChildNodes(0).InnerText & "|" & multiValue & "|" & prompt
skip:
            I += 1
        Next

        Return sValue
    End Function

    Public Shared Function getParameterValues(ByVal sFile As String) As String()
        Dim I As Integer = 0
        Dim x As Integer = 0
        Dim sDefValues() As String
        'do we show hidden parameters
        Dim showHidden As Boolean = False

        Dim oXML As New Xml.XmlDocument

        oXML.Load(sFile)

        Dim oNodes As XmlNodeList

        oNodes = oXML.GetElementsByTagName("ReportParameter")

        For Each oNode As XmlNode In oNodes
            Dim hidden As Boolean = False

            Try
                hidden = oNode.Item("Hidden").InnerText
            Catch ex As Exception
                hidden = False
            End Try

            If hidden = True And showHidden = False Then GoTo skip

            ReDim Preserve sDefValues(I)

            sDefValues(I) = ""

            Try
                Dim oChild As XmlNode

                oChild = oNode.Item("ValidValues").ChildNodes.Item(0) 'oNode.ChildNodes(2).ChildNodes(0)

                For Each o As XmlNode In oChild.ChildNodes
                    sDefValues(I) &= o.ChildNodes(0).InnerText & "|"
                Next
            Catch : End Try
skip:
            I += 1
        Next

        Return sDefValues
    End Function

    Public Function createRDLFile(Optional formsAuth As Boolean = False) As String
        Try
            Dim f As String = "parameterslisting.debug"

            Dim srsVersion As String = clsMarsReport.m_serverVersion(m_serverUrl, m_serverUser, m_serverPassword, formsAuth)

            Dim oRpt

            clsMarsDebug.writeToDebug(f, "getting rdl", True)

            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(m_serverUrl)
            Else
                oRpt = New rsClients.rsClient(m_serverUrl)
            End If



            If m_serverUrl.ToLower.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(m_serverUrl)
            End If

            clsMarsDebug.writeToDebug(f, "connecting to server", True)

            oRpt.Url = m_serverUrl

            clsMarsDebug.writeToDebug(f, "authenticating", True)

            If serverUser = "" And serverPassword = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            Else
                Dim userDomain As String = ""
                Dim tmpUser As String = serverUser

                getDomainAndUserFromString(tmpUser, userDomain)

                If m_formsAuth = False Then
                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)

                    oRpt.Credentials = oCred
                Else
                    Try
                        oRpt.LogonUser(tmpUser, serverPassword, Nothing)
                    Catch : End Try
                End If
            End If

            Dim sPath As String = reportPath

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".rdl"
            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                Try
                    IO.File.Delete(sRDLPath)
                Catch
                    sRDLPath = clsMarsReport.m_rdltempPath & clsMarsData.CreateDataID & ".rdl"
                End Try
            End If

            clsMarsDebug.writeToDebug(f, "downloading rdl to " & sRDLPath, True)

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is ReportServer_2010.ReportingService2010 Then

                repDef = oRpt.GetItemDefinition(reportPath)
            Else
                repDef = oRpt.GetReportDefinition(reportPath)
            End If

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()



            clsMarsDebug.writeToDebug(f, "RDL downloaded successfully", True)

            Return sRDLPath
        Catch ex As Exception

            clsMarsDebug.writeToDebug("parameterslisting.debug", "error downloading rdl " & ex.Message, True)
        End Try
    End Function
    Public Sub loadParameters(formsAuth As Boolean)
        '//get the parameters
        ' Dim sPars() As String
        ' Dim sParDefaults As String()
        Dim I As Integer = 0
        Dim ssrsReport As clsMarsReport = New clsMarsReport
        Dim topPos As Integer = 3

        If m_serverUrl = "" Or m_reportPath = "" Then
            MessageBox.Show("The parameters control has not be initialized properly. Please make sure that the server, reportpath and rdl have been set", Application.ProductName, _
                              MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        m_reportID = 99999

        m_formsAuth = formsAuth

        parameterControls.Clear()

        If m_rdlPath = "" Or IO.File.Exists(m_rdlPath) = False Then
            m_rdlPath = createRDLFile(m_formsAuth)
        End If

        clsMarsUI.MainUI.BusyProgress(15, "Connecting to server to read parameters", False)

        m_serverparametersCollection = New System.Collections.Generic.List(Of ssrsParameter)

        m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, m_rdlPath, formsAuth, , m_reportID)


        ' sPars = getParameterFields(rdlPath)
        '  sParDefaults = getParameterValues(rdlPath)

        Me.AutoScroll = True
        Me.Controls.Clear()

        parDefaults = New Hashtable

        g_parameterList = New ArrayList

        clsMarsUI.MainUI.BusyProgress(50, "Building controls", False)

        If m_serverparametersCollection IsNot Nothing Then
            For Each par As ssrsParameter In m_serverparametersCollection
                Dim parControl As ucParameters = New ucParameters(par.parameterName, par.parameterType, par.parameterAllowMultipleValue, "", Me, "")

                '//set up the parameters control
                With parControl
                    .Name = par.parameterName & I
                    .m_ParameterPrompt = IIf(par.parameterPrompt = "", par.parameterName, par.parameterPrompt)
                    .m_parameterType = par.parameterType
                    .m_parameterIndex = I
                    .Left = 3
                    .m_rdlFilePath = m_rdlPath
                    '.Top = topPos
                    .Tag = 0
                    .m_parameterParent = Me
                End With

                '//add it to the list
                Me.Controls.Add(parControl)
                parControl.Visible = True
                parControl.Dock = DockStyle.Top
                parControl.BringToFront()

                If g_parameterList.Contains(par.parameterName) = False Then
                    g_parameterList.Add(par.parameterName)
                End If

                ' topPos = parControl.Height + 277


                parameterControls.Add(parControl)

                '//get the default values if they exist
                Dim hs As List(Of KeyValuePair(Of String, String)) = par.parameterAvailableValues

                If hs IsNot Nothing Then
                    If parControl.m_allowMultipleValues = True Then
                        If hs.Count > 0 Then
                            parControl.m_hasDefaultValues = True

                            For Each pair As KeyValuePair(Of String, String) In hs
                                Dim it As ListViewItem = parControl.lsvValues.Items.Add(pair.Key)
                                it.Tag = pair.Value
                            Next

                            'For Each de As DictionaryEntry In hs
                            '    Dim it As ListViewItem = parControl.lsvValues.Items.Add(de.Key)
                            '    it.Tag = de.Value
                            'Next
                        Else
                            parControl.m_hasDefaultValues = False
                        End If
                    Else
                        For Each pair As KeyValuePair(Of String, String) In hs
                            parControl.txtValue.Items.Add(pair.Key)
                        Next

                        'For Each de As DictionaryEntry In hs
                        '    parControl.txtValue.Items.Add(de.Key)
                        'Next

                        If parControl.txtValue.Items.Count > 0 Then parControl.txtValue.DropDownStyle = ComboBoxStyle.DropDown
                    End If
                End If

                Dim defaultParameter As clsDefaultParameter = New clsDefaultParameter(par.parameterName)

                If defaultParameter.hasValue = True Then
                    parControl.m_parameterValue = newHashTable(defaultParameter.parameterValue, defaultParameter.parameterValue)

                    If parControl.AdminCanEditDefaultParameters = False Then
                        parControl.chkNull.Enabled = False
                        parControl.chkIgnore.Enabled = False
                        parControl.lsvValues.Enabled = False
                        parControl.FlowLayoutPanel1.Enabled = False
                    End If
                End If

                If defaultParameter.hasValue And parControl.AdminCanEditDefaultParameters = False Then
                    parControl.txtValue.Enabled = False
                    parControl.btnAddValue.Enabled = False
                    parControl.btnGetValues.Enabled = False
                    parControl.lsvValues.Enabled = False
                    parControl.FlowLayoutPanel1.Enabled = False
                End If

                I += 1
            Next
        End If

        clsMarsUI.MainUI.BusyProgress(75, "Cleaning up...", False)

        clsMarsUI.MainUI.BusyProgress(100, "", True)
        m_controlFiresEvents = True
    End Sub

    Public Sub addHandlers()
        For Each par As ucParameters In Me.parameterControls
            AddHandler par.lsvValues.ItemChecked, AddressOf par.lsvValues_ItemChecked
        Next
    End Sub

    Public Sub saveParameters(reportID As Integer)
        Try
            Dim sCols As String
            Dim sVals As String
            Dim SQL As String
            Dim parsList As ArrayList = New ArrayList

            clsMarsData.WriteData("DELETE FROM reportparameter WHERE reportid = " & reportID)

            sCols = "ParID,ReportID,ParName,ParValue,ParType,ParNull,MultiValue,parIndex,parvaluelabel"

            For Each ctrl As Control In Me.Controls

                If TypeOf ctrl Is ucParameters Then
                    parsList.Add(ctrl)
                End If
            Next


            For Each ctrl As ucParameters In Me.m_ParameterControlsCollection
                Dim par As ucParameters = CType(ctrl, ucParameters)
                Dim labels, values As String

                labels = ""
                values = ""

                For Each de As DictionaryEntry In par.m_parameterValue
                    labels &= de.Key & "|"
                    values &= de.Value & "|"
                Next

                If labels.EndsWith("|") Then labels = labels.Remove(labels.Length - 1, 1)
                If values.EndsWith("|") Then values = values.Remove(values.Length - 1, 1)

                Dim parValue As String = values ' getserverParameterValue(par.m_parameterName, par.m_parameterValue)
                Dim parValueLabel As String = labels ' par.m_parameterValue

                sVals = clsMarsData.CreateDataID("ReportParameter", "ParID") & "," & _
                reportID & "," & _
                "'" & SQLPrepare(par.m_parameterName) & "'," & _
                "'" & SQLPrepare(parValue) & "'," & _
                "'" & par.m_parameterType & "'," & _
                Convert.ToInt32(par.m_ParameterNull) & "," & _
                "'" & par.m_allowMultipleValues & "'," & _
                par.m_parameterIndex & "," & _
                "'" & SQLPrepare(parValueLabel) & "'"

                SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                clsMarsData.WriteData(SQL)

            Next
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GetParameterAvailableValues()
        Try
            Dim oRpt

            oRpt = New rsClients.rsClient(clsMarsParser.Parser.ParseString(m_serverUrl))

            oUI.BusyProgress(10, "Connecting to server...")

            oRpt.Url = clsMarsParser.Parser.ParseString(m_serverUrl)

            If serverUser = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            Else
                Dim userDomain As String = ""
                Dim tmpUser As String = serverUser

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)

                oRpt.Credentials = oCred
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & Guid.NewGuid.ToString & ".rdl"
            Dim repDef() As Byte

            oUI.BusyProgress(40, "Getting server definition...")

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(Me.m_reportPath)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            oUI.BusyProgress(70, "Loading values...")

            clsMarsReport._GetParameterValues(sRDLPath)

            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            parDefaults = New Hashtable

            oUI.BusyProgress(90, "Populating parameters...")

            If sPars IsNot Nothing Then
                For Each s As String In sPars

                    parDefaults.Add(s.Split("|")(0), sParDefaults(I))

                    I += 1
                Next
            End If

            '  UcDest.m_ParameterList = Me.m_ParametersList

            oUI.BusyProgress(, , True)
        Catch
            oUI.BusyProgress(, , True)
        End Try

    End Sub
    'Private Sub loadParametersIntoDataTable()
    '    Try
    '        m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(m_serverUrl, m_reportPath, m_serverUser, m_serverPassword, "")

    '        Me.GetParameterAvailableValues()
    '    Catch
    '        oUI.BusyProgress(, , True)
    '    End Try
    'End Sub

    Private Function getserverParameterPrompt(parameterName As String) As String
        Try
            Dim par As ssrsParameter

            For Each par In m_serverparametersCollection
                If par.parameterName.ToLower = parameterName.ToLower Then
                    Return par.parameterPrompt
                End If
            Next

            Return parameterName
        Catch
            Return parameterName
        End Try
    End Function
    ''' <summary>
    ''' this function loads the parameters from the database
    ''' </summary>
    ''' <param name="reportID"></param>
    ''' <remarks></remarks>
    Public Sub loadParameters(reportID As Integer)
        '//get the parameters
        clsMarsUI.MainUI.BusyProgress(20, "Loading parameters, please wait...")

        Me.SuspendLayout()
        Dim sPars() As String
        Dim sParDefaults As String()
        Dim I As Integer = 0
        Dim ssrsReport As clsMarsReport = New clsMarsReport
        Dim topPos As Integer = 3
        Dim SQL As String = "SELECT * FROM reportParameter WHERE reportID = " & reportID & " ORDER BY parindex ASC"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        m_reportID = reportID

        m_formsAuth = (New cssreport(reportID)).usesFormsAuth

        If m_serverUrl = "" Or m_reportPath = "" Then
            MessageBox.Show("The parameters control has not be initialized properly. Please make sure that the server, reportpath and rdl have been set", Application.ProductName, _
                              MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        clsMarsUI.MainUI.BusyProgress(30, "Checking server status, please wait...")

        m_serverOnlineStatus = False ' ssrsReport.isServerOnline(m_serverUrl)

        clsMarsUI.MainUI.BusyProgress(50, "Getting report definition, please wait...")

        If m_serverOnlineStatus = True Then
            If m_rdlPath = "" Then
                m_rdlPath = createRDLFile(m_formsAuth)
            End If
        Else
            m_rdlPath = ""
        End If

        Me.Controls.Clear()
        parameterControls.Clear()

        '//load the parameters
        Dim pars As ArrayList = New ArrayList
        g_parameterList = New ArrayList

        Me.AutoScroll = True

        clsMarsUI.MainUI.BusyProgress(70, "Getting parameters list from server, please wait...")

        If m_serverOnlineStatus = True Then
            Try
                If m_serverparametersCollection Is Nothing Then
                    m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_formsAuth, , m_reportID)
                ElseIf m_serverparametersCollection.Count = 0 Then
                    m_serverparametersCollection = ssrsReport.getserverReportParametersCollection(m_serverUrl, m_reportPath, serverUser, serverPassword, "", m_formsAuth, m_reportID)
                End If
            Catch : End Try
        Else
            m_serverparametersCollection = New System.Collections.Generic.List(Of ssrsParameter)
        End If

        clsMarsUI.MainUI.BusyProgress(90, "Drawing parameters to screen, please wait...")

        Try
            Do While oRs.EOF = False
                Dim parameterName As String = oRs("parname").Value
                Dim parameterValue As String = oRs("parvalue").Value
                Dim parameterMultiValue = oRs("multivalue").Value
                Dim parameterType As String = oRs("partype").Value
                Dim parameterValueLabel As String = IsNull(oRs("parvaluelabel").Value, parameterValue)
                Dim parameterPrompt As String = getserverParameterPrompt(parameterName)

                Dim parControl As ucParameters = New ucParameters(parameterName, parameterType, parameterMultiValue, "", Me, "")

                With parControl
                    .Name = parameterName & I
                    .m_ParameterPrompt = IIf(parameterPrompt = "", parameterName, parameterPrompt)
                    .m_parameterType = parameterType
                    .m_parameterIndex = I
                    .Left = 3
                    .Tag = 0
                    .m_parameterParent = Me
                    .m_rdlFilePath = m_rdlPath
                    '.m_parameterValue = parameterValue
                End With

                If g_parameterList.Contains(parameterName) = False Then
                    g_parameterList.Add(parameterName)
                End If

                Me.Controls.Add(parControl)

                Dim defaultParameter As clsDefaultParameter = New clsDefaultParameter(parameterName)

                If defaultParameter.hasValue Then
                    parameterValue = defaultParameter.parameterValue
                End If

                Dim hsT As Hashtable = New Hashtable

                If parameterValue.Contains("|") Then
                    For y As Integer = 0 To parameterValue.Split("|").GetUpperBound(0)
                        Dim value, label As String
                        value = parameterValue.Split("|")(y)

                        Try
                            label = parameterValueLabel.Split("|")(y)
                        Catch ex As Exception
                            label = value
                        End Try

                        If label = "" Then label = value

                        hsT.Add(label, value)
                    Next
                Else
                    If parameterValueLabel = "" Then parameterValueLabel = parameterValue

                    hsT.Add(parameterValueLabel, parameterValue)
                End If

                parControl.m_parameterValue = hsT

                If parameterValue <> "[SelectAll]" And parameterValue <> "[SQL-RDDefault]" And parameterValue <> "[SQL-RDNull]" Then
                    Dim hs As List(Of KeyValuePair(Of String, String)) = parControl.m_staticValuesList

                    If hs Is Nothing OrElse hs.Count = 0 Then hs = getParameterValidValues(I, parControl.m_parameterName)


                    If hs IsNot Nothing Then
                        If parControl.m_allowMultipleValues = True Then
                            If hs.Count > 0 Then
                                parControl.m_hasDefaultValues = True

                                For Each pair As KeyValuePair(Of String, String) In hs
                                    Dim exist As Boolean = False

                                    For Each x As ListViewItem In parControl.lsvValues.Items
                                        If x.Text.ToLower = pair.Key.ToString.ToLower Then
                                            exist = True
                                            Exit For
                                        End If
                                    Next

                                    If Not exist Then
                                        Dim it As ListViewItem = parControl.lsvValues.Items.Add(pair.Key)
                                        it.Tag = pair.Value
                                    End If
                                Next
                            Else
                                parControl.m_hasDefaultValues = False
                            End If
                        Else

                            For Each pair As KeyValuePair(Of String, String) In hs
                                Dim exist As Boolean = False

                                For Each x As ListViewItem In parControl.lsvValues.Items
                                    If x.Text.ToLower = pair.Key.ToString.ToLower Then
                                        exist = True
                                        Exit For
                                    End If
                                Next

                                If Not exist Then parControl.txtValue.Items.Add(pair.Key)
                            Next

                            If parControl.txtValue.Items.Count > 0 Then parControl.txtValue.DropDownStyle = ComboBoxStyle.DropDown
                        End If
                    End If
                End If

                If defaultParameter.hasValue = True Then
                    '  parControl.m_parameterValue = defaultParameter.parameterValue we have this already set from the database

                    If parControl.AdminCanEditDefaultParameters = False Then
                        parControl.chkNull.Enabled = False
                        parControl.chkIgnore.Enabled = False
                        parControl.lsvValues.Enabled = False
                        parControl.FlowLayoutPanel1.Enabled = False
                    End If
                End If

                If defaultParameter.hasValue And parControl.AdminCanEditDefaultParameters = False Then
                    parControl.txtValue.Enabled = False
                    parControl.btnAddValue.Enabled = False
                    parControl.btnGetValues.Enabled = False
                    parControl.lsvValues.Enabled = False
                    parControl.FlowLayoutPanel1.Enabled = False
                End If

                parControl.Dock = DockStyle.Top


                parameterControls.Add(parControl)

                parControl.BringToFront()

                I += 1
                oRs.MoveNext()
            Loop

            clsMarsUI.MainUI.BusyProgress(95, "Sorting parameters, please wait...")

            clsMarsUI.MainUI.BusyProgress(100, "", True)

            m_serverOnlineStatus = ssrsReport.isServerOnline(m_serverUrl)

            oRs.Close()
        Catch : End Try

        m_controlFiresEvents = True
        Me.ResumeLayout()



    End Sub
End Class
