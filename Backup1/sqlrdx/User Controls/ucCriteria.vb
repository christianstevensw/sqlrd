﻿Public Class ucCriteria

    Public Property criteria As String
        Get
            If cmbOperator.Text = "" Then Return ""

            Return cmbOperator.Text & "::" & txtCriteria.Text
        End Get
        Set(value As String)
            If value.Contains("::") = False Then Return

            Dim tmp() As String = value.Split("::")

            cmbOperator.Text = tmp(0)
            txtCriteria.Text = tmp(2)
        End Set
    End Property
End Class
