﻿Imports DevComponents.AdvTree

Public Class ucPackagedReports
    Dim pack As Package

    Dim m_IsDataDriven As Boolean
    Dim m_IsDynamic As Boolean
    Dim ofd As OpenFileDialog = New OpenFileDialog
    Dim oUI As clsMarsUI = New clsMarsUI

    Public Property viewStyle() As eView
        Get
            Return tvReports.View
        End Get
        Set(ByVal value As eView)
            tvReports.View = value
        End Set
    End Property

    Public Sub loadReports(packID As Integer, isDataDriven As Boolean, isDynamic As Boolean)
        m_IsDataDriven = isDataDriven
        m_IsDynamic = isDynamic

        Select Case viewStyle
            Case eView.Tile
                loadReportTiles(packID)
            Case Else
                loadReportDetails(packID)
        End Select
    End Sub

    Private Sub loadReportTiles(packid As Integer)
        tvReports.Nodes.Clear()
        tvReports.Columns.Clear()
        tvReports.GridColumnLines = False

        pack = New Package(packid)

        Dim reportImage As Image = Nothing

        For Each rpt As cssreport In pack.getReportsinPackage
            Dim reportNode As Node = New Node(rpt.reportName)
            reportNode.DataKey = rpt.ID
            reportNode.Tag = "packagedreport"

            If reportImage Is Nothing Then
                reportImage = resizeImage(rpt.reportImage, 32, 32)
            End If

            reportNode.Image = reportImage

            tvReports.Nodes.Add(reportNode)
        Next
    End Sub

    Private Sub loadReportDetails(packid As Integer)
        tvReports.BeginUpdate()
        tvReports.Nodes.Clear()
        tvReports.Columns.Clear()


        Dim columns As String() = New String() {"Report Name", "Output Format"}

        For Each s As String In columns
            Dim cHeader As ColumnHeader = New ColumnHeader(s)
            cHeader.MinimumWidth = 64
            cHeader.Width.Absolute = (tvReports.Width / 2) - 10
            tvReports.Columns.Add(cHeader)
        Next

        pack = New Package(packid)

        Dim reportImage As Image = Nothing

        For Each rpt As cssreport In pack.getReportsinPackage
            Dim reportNode As Node = New Node(rpt.reportName)
            reportNode.DataKey = rpt.ID
            reportNode.Tag = "packagedreport"

            If reportImage Is Nothing Then
                reportImage = resizeImage(rpt.reportImage, 24, 24)
            End If


            For Each ch As ColumnHeader In tvReports.Columns
                If ch.Text = "Report Name" Then
                    'do nothing 
                Else
                    reportNode.Cells.Add(New Cell(rpt.packedReportFormat))
                End If
            Next

            reportNode.Image = reportImage

            tvReports.Nodes.Add(reportNode)
        Next

        tvReports.EndUpdate()
    End Sub

    Private Sub btnAdd_Click(sender As System.Object, e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim oForm As frmPackedReport
            Dim sReturn() As String
            Dim rs As ADODB.Recordset
            Dim SQL As String
            Dim nCount As Integer


            With ofd
                .Title = "Please select the Crystal Reports to add to the package"
                .Filter = "Crystal Reports|*.rpt"
                .Multiselect = True
                .InitialDirectory = oUI.ReadRegistry("ReportsLoc", sAppPath & "\Samples")
                .ShowDialog()
                If .FileNames.Length = 0 Then Return
            End With

            For Each s As String In ofd.FileNames


                SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & pack.ID

                rs = clsMarsData.GetData(SQL)

                If rs Is Nothing Then
                    nCount = 1
                Else
                    If rs.EOF = False Then
                        Dim sTemp As String = rs.Fields(0).Value

                        nCount = sTemp

                        nCount += 1
                    Else
                        nCount = 1
                    End If
                End If

                rs.Close()

                oForm = New frmPackedReport

                oForm.IsDataDriven = m_IsDataDriven

                sReturn = oForm.AddReport(nCount, pack.ID)
            Next

            loadReports(pack.ID, m_IsDataDriven, m_IsDynamic)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub

    Private Sub btnEdit_Click(sender As System.Object, e As System.EventArgs) Handles btnEdit.Click
        If tvReports.SelectedNode IsNot Nothing Then
            Try

                MessageBox.Show("Please remember to refresh the schedule (right-click then select 'Refresh') once you have made your changes so that they are " &
                             "reflected in SQL-RD", Application.ProductName, MessageBoxButtons.OK)


                Dim rpt As cssreport = New cssreport(tvReports.SelectedNode.DataKey)

                Process.Start(rpt.reportLocation)
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please install Crystal Reports")
            End Try
        End If
    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click, mnuDelete.Click
        If tvReports.SelectedNode IsNot Nothing Then

            Dim SQL As String
            Dim sWhere As String
            Dim Response As DialogResult

            Response = MessageBox.Show("Delete the selected report from the package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If Response = DialogResult.Yes Then

                oUI.DeleteReport(tvReports.SelectedNode.DataKey, pack.ID)

                tvReports.Nodes.Remove(tvReports.SelectedNode)

            End If
        End If
    End Sub

    Private Sub btnPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click, mnuPreview.Click
        If tvReports.SelectedNode IsNot Nothing Then
            Dim reportID As Integer = tvReports.SelectedNode.DataKey
            Dim prev As clsMarsReport = New clsMarsReport

            prev.ViewReport(reportID, True)
        End If
    End Sub

    Private Sub btnUp_Click(sender As System.Object, e As System.EventArgs) Handles btnUp.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        Dim selectedNode As Node = tvReports.SelectedNode
        Dim SelTag As Object = selectedNode.DataKey

        If selectedNode.Index = 0 Then Return

        Dim nextNode As Node = tvReports.Nodes(selectedNode.Index - 1)

        Dim OldID, NewID As Integer

        NewID = nextNode.Index + 1
        OldID = selectedNode.Index + 1

        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up

        NewName = NewID & ":" & selectedNode.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "'," & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & selectedNode.DataKey

        clsMarsData.WriteData(SQL)

        selectedNode.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & nextNode.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & nextNode.DataKey

        clsMarsData.WriteData(SQL)

        nextNode.Text = NewName

        Me.loadReports(pack.ID, m_IsDataDriven, m_IsDynamic)

        Dim newNode As Node = tvReports.FindNodeByDataKey(SelTag)

        If newNode IsNot Nothing Then
            tvReports.SelectedNode = newNode
            newNode.EnsureVisible()
        End If
    End Sub

    Private Sub btnDown_Click(sender As System.Object, e As System.EventArgs) Handles btnDown.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        Dim selectedNode As Node = tvReports.SelectedNode
        Dim SelTag As Object = selectedNode.DataKey

        If selectedNode.Index = tvReports.Nodes.Count - 1 Then Return

        Dim nextNode As Node = tvReports.Nodes(selectedNode.Index + 1)

        Dim OldID, NewID As Integer

        NewID = nextNode.Index + 1  'Text.Split(":")(0)
        OldID = selectedNode.Index + 1 'Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & selectedNode.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & selectedNode.DataKey

        clsMarsData.WriteData(SQL)

        selectedNode.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & nextNode.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & nextNode.DataKey

        clsMarsData.WriteData(SQL)

        nextNode.Text = NewName

        Me.loadReports(pack.ID, m_IsDataDriven, m_IsDynamic)

        Dim newNode As Node = tvReports.FindNodeByDataKey(SelTag)

        If newNode IsNot Nothing Then
            tvReports.SelectedNode = newNode
            newNode.EnsureVisible()
        End If
    End Sub

    Private Sub btnTile_Click(sender As System.Object, e As System.EventArgs) Handles btnTile.Click
        tvReports.View = eView.Tile
        loadReportTiles(pack.ID)
    End Sub

    Private Sub btnDetail_Click(sender As System.Object, e As System.EventArgs) Handles btnDetail.Click
        tvReports.View = eView.Tree
        loadReportDetails(pack.ID)
    End Sub

    Private Sub txtSearch_TextChanged(sender As Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            If txtSearch.Text = "" Then
                For Each nx As Node In tvReports.Nodes
                    For Each ch As Node In nx.Nodes
                        ch.Visible = True
                    Next
                Next
            Else
                For Each nx As Node In tvReports.Nodes

                    If nx.Text.ToLower.Contains(txtSearch.Text.ToLower) = False Then
                        nx.Visible = False
                    Else
                        nx.Visible = True
                    End If

                Next
            End If
        Catch : End Try
    End Sub

    Private Sub tvReports_AfterNodeDrop(sender As Object, e As DevComponents.AdvTree.TreeDragDropEventArgs) Handles tvReports.AfterNodeDrop
        Dim id As Integer
        Dim table As String
        Dim parentColumn As String
        Dim keyColumn As String
        Dim type As String
        Dim otype As clsMarsScheduler.enScheduleType


        For Each nn As Node In e.Nodes
            type = nn.Tag

            If type <> "packagedreport" And type <> "report" Then Continue For

            id = nn.DataKey

            Dim SQL As String
            Dim newID As Integer = pack.maxPackOrderID + 1
            Dim newName As String
            Dim oldName As String = nn.Text
            Dim oRs As ADODB.Recordset

            If type = "packagedreport" Then
                newName = newID & ":" & oldName.Split(":")(1)
            Else
                newName = newID & ":" & oldName
            End If

            SQL = "UPDATE ReportAttr SET " & _
            "PackID =" & pack.ID & "," & _
            "ReportTitle ='" & SQLPrepare(newName) & "'," & _
            "PackOrderID =" & newID & " WHERE ReportID =" & nn.DataKey

            clsMarsData.WriteData(SQL)
        Next

        _Delay(1)



        loadReports(pack.ID, m_IsDataDriven, m_IsDynamic)

        Return

        For Each nn As Node In e.Nodes
            Dim nx As Node = tvReports.FindNodeByDataKey(nn.DataKey)

            If nx IsNot Nothing Then
                tvReports.SelectedNodes.Add(nx)
                nx.EnsureVisible()
            End If
        Next
    End Sub

    Private Sub tvReports_BeforeNodeDrop(sender As Object, e As DevComponents.AdvTree.TreeDragDropEventArgs) Handles tvReports.BeforeNodeDrop

    End Sub

    Private Sub tvReports_CellEditEnding(sender As Object, e As DevComponents.AdvTree.CellEditEventArgs) Handles tvReports.CellEditEnding
        On Error Resume Next

        If tvReports.SelectedNode Is Nothing Then Return

        Dim node As DevComponents.AdvTree.Node = tvReports.SelectedNode

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim nID As Integer = node.DataKey
        Dim sSelType As String = node.Parent.Text
        Dim sCurrentNode As String
        Dim oRs As New ADODB.Recordset
        Dim sEvent As String
        Dim sType As String
        Dim sName As String
        Dim nParent As Integer
        Dim oUI As clsMarsUI = New clsMarsUI


        If e.NewText Is Nothing Then Exit Sub

        If e.NewText.Length = 0 Then e.Cancel = True : Return

        Dim nCount As Integer
        Dim snewName As String = e.NewText

        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            e.Cancel = True
            Return
        End If

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & pack.ID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            nCount = 1
        Else
            If oRs.EOF = False Then
                Dim sTemp As String = oRs.Fields(0).Value

                nCount = sTemp

                nCount += 1
            Else
                nCount = 1
            End If
        End If

        snewName = nCount & ":" & snewName

        oRs.Close()


        SQL = "UPDATE ReportAttr SET ReportTitle = '" & SQLPrepare(snewName) & "' WHERE " & _
                            "ReportID = " & nID

        clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.RENAME)

        clsMarsData.WriteData(SQL)

        tvReports.SelectedNode.Text = snewName
    End Sub

    Private Sub Properties_Click(sender As System.Object, e As System.EventArgs) Handles Properties.Click, tvReports.DoubleClick
        Try
            Dim selectedNode As Node
            Dim sNewName As String = ""
            Dim sVals As String()

            If tvReports.SelectedNodes.Count = 0 Then Exit Sub

            Dim frmPackProp As frmPackedReport = New frmPackedReport

            selectedNode = tvReports.SelectedNode

            frmPackProp.IsDataDriven = m_IsDataDriven

            sVals = frmPackProp.EditReport(selectedNode.DataKey)

            If Not sVals Is Nothing Then
                Dim oItem As Node = tvReports.SelectedNode

                oItem.Text = sVals(0)

                If tvReports.View = eView.Tree Then oItem.Cells(1).Text = sVals(1)
            End If

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        End Try
    End Sub

    Private Sub mnuStatus_Click(sender As System.Object, e As System.EventArgs) Handles mnuStatus.Click
        If tvReports.SelectedNode IsNot Nothing Then
            Dim rpt As cssreport = New cssreport(tvReports.SelectedNode.DataKey)
            Dim currentStatus As Boolean = rpt.packedStatus

            rpt.packedStatus = Not currentStatus

            If rpt.packedStatus = False Then
                tvReports.SelectedNode.Image = MakeGrayscale3(tvReports.SelectedNode.Image)
            Else
                Dim imageSize As Size

                If tvReports.View = eView.Tile Then
                    imageSize = New Size(32, 32)
                Else
                    imageSize = New Size(24, 24)
                End If

                tvReports.SelectedNode.Image = resizeImage(rpt.reportImage, imageSize)
            End If

            mnuStatus.Checked = rpt.packedStatus
        End If
    End Sub


    Private Sub mnuExecute_Click(sender As System.Object, e As System.EventArgs) Handles mnuExecute.Click
        Dim nPackID As Integer
        Dim nReportID As Integer

        If tvReports.SelectedNodes.Count = 0 Then Return

        nReportID = tvReports.SelectedNode.DataKey

        nPackID = pack.ID

        Dim oT As New clsMarsThreading

        oT.xReportID = nReportID
        oT.xPackID = nPackID


        AppStatus(True)
        oT.PackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(tvReports.SelectedNode.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.EXECUTE)

    End Sub

    Private Sub mnuRefresh_Click(sender As System.Object, e As System.EventArgs) Handles mnuRefresh.Click
        If tvReports.SelectedNodes.Count = 0 Then Return

        Dim reportid As Integer = tvReports.SelectedNode.DataKey

        Dim oReport As New clsMarsReport

        If oReport.RefreshReport(reportid) = True Then
            MessageBox.Show("Report refreshed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please reselect the source reports and then try refreshing the schedule again.")
        End If
    End Sub

    Private Sub mnuRename_Click(sender As System.Object, e As System.EventArgs) Handles mnuRename.Click
        If tvReports.SelectedNode IsNot Nothing Then
            tvReports.SelectedNode.BeginEdit()
        End If
    End Sub


    Private Sub ucPackagedReports_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        tvReports.Dock = DockStyle.Fill
    End Sub

    Private Sub ButtonItem2_PopupOpen(sender As Object, e As DevComponents.DotNetBar.PopupOpenEventArgs) Handles ButtonItem2.PopupOpen
        If tvReports.SelectedNode IsNot Nothing Then
            mnuStatus.Checked = New cssreport(tvReports.SelectedNode.DataKey).packedStatus
        End If
    End Sub

    
End Class
