﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucScheduleHistory
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucScheduleHistory))
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdClear = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRefresh = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.grSort = New System.Windows.Forms.GroupBox()
        Me.optAsc = New System.Windows.Forms.RadioButton()
        Me.optDesc = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.tvDetails = New DevComponents.AdvTree.AdvTree()
        Me.ColumnHeader1 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader2 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader3 = New DevComponents.AdvTree.ColumnHeader()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.pnlInfo = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.lblInfo = New DevComponents.DotNetBar.LabelX()
        Me.tvSchedules = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.pgLoading = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.ContextMenuBar1 = New DevComponents.DotNetBar.ContextMenuBar()
        Me.history = New DevComponents.DotNetBar.ButtonItem()
        Me.btnLoadAll = New DevComponents.DotNetBar.ButtonItem()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.grSort.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.tvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlInfo.SuspendLayout()
        CType(Me.tvSchedules, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdClear)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdRefresh)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 864)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1356, 29)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'cmdClear
        '
        Me.cmdClear.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdClear.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdClear.TabIndex = 15
        Me.cmdClear.Text = "Clear"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 14
        Me.cmdRefresh.Text = "Refresh"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.grSort)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(1356, 53)
        Me.FlowLayoutPanel2.TabIndex = 2
        Me.FlowLayoutPanel2.Visible = False
        '
        'grSort
        '
        Me.grSort.BackColor = System.Drawing.Color.Transparent
        Me.grSort.Controls.Add(Me.optAsc)
        Me.grSort.Controls.Add(Me.optDesc)
        Me.grSort.Location = New System.Drawing.Point(3, 3)
        Me.grSort.Name = "grSort"
        Me.grSort.Size = New System.Drawing.Size(272, 48)
        Me.grSort.TabIndex = 12
        Me.grSort.TabStop = False
        Me.grSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 1
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 0
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 53)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1356, 811)
        Me.Panel1.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.ContextMenuBar1)
        Me.Panel2.Controls.Add(Me.tvDetails)
        Me.Panel2.Controls.Add(Me.pgLoading)
        Me.Panel2.Controls.Add(Me.ExpandableSplitter1)
        Me.Panel2.Controls.Add(Me.pnlInfo)
        Me.Panel2.Controls.Add(Me.tvSchedules)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1356, 811)
        Me.Panel2.TabIndex = 5
        '
        'tvDetails
        '
        Me.tvDetails.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvDetails.AllowDrop = True
        Me.tvDetails.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvDetails.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvDetails.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvDetails.Columns.Add(Me.ColumnHeader1)
        Me.tvDetails.Columns.Add(Me.ColumnHeader2)
        Me.tvDetails.Columns.Add(Me.ColumnHeader3)
        Me.ContextMenuBar1.SetContextMenuEx(Me.tvDetails, Me.history)
        Me.tvDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvDetails.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvDetails.ExpandLineColor = System.Drawing.Color.White
        Me.tvDetails.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvDetails.Location = New System.Drawing.Point(182, 32)
        Me.tvDetails.Name = "tvDetails"
        Me.tvDetails.NodesConnector = Me.NodeConnector1
        Me.tvDetails.NodeStyle = Me.ElementStyle1
        Me.tvDetails.PathSeparator = ";"
        Me.tvDetails.Size = New System.Drawing.Size(1174, 765)
        Me.tvDetails.Styles.Add(Me.ElementStyle1)
        Me.tvDetails.TabIndex = 8
        Me.tvDetails.Text = "AdvTree1"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Editable = False
        Me.ColumnHeader1.Name = "ColumnHeader1"
        Me.ColumnHeader1.Text = "Started"
        Me.ColumnHeader1.Width.Absolute = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Name = "ColumnHeader2"
        Me.ColumnHeader2.Text = "Finished"
        Me.ColumnHeader2.Width.Absolute = 150
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Name = "ColumnHeader3"
        Me.ColumnHeader3.Text = "Details"
        Me.ColumnHeader3.Width.Absolute = 450
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.Class = ""
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(200, Byte), Integer), CType(CType(103, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(226, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(31, Byte), Integer), CType(CType(57, Byte), Integer), CType(CType(120, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(220, Byte), Integer), CType(CType(232, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(179, 32)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 779)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007
        Me.ExpandableSplitter1.TabIndex = 7
        Me.ExpandableSplitter1.TabStop = False
        '
        'pnlInfo
        '
        Me.pnlInfo.BackColor = System.Drawing.SystemColors.Info
        Me.pnlInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlInfo.Controls.Add(Me.ReflectionImage1)
        Me.pnlInfo.Controls.Add(Me.lblInfo)
        Me.pnlInfo.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlInfo.Location = New System.Drawing.Point(179, 0)
        Me.pnlInfo.Name = "pnlInfo"
        Me.pnlInfo.Size = New System.Drawing.Size(1177, 32)
        Me.pnlInfo.TabIndex = 5
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(3, 3)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.ReflectionEnabled = False
        Me.ReflectionImage1.Size = New System.Drawing.Size(56, 23)
        Me.ReflectionImage1.TabIndex = 0
        '
        'lblInfo
        '
        '
        '
        '
        Me.lblInfo.BackgroundStyle.Class = ""
        Me.lblInfo.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblInfo.Location = New System.Drawing.Point(65, 3)
        Me.lblInfo.Name = "lblInfo"
        Me.lblInfo.Size = New System.Drawing.Size(633, 22)
        Me.lblInfo.TabIndex = 1
        '
        'tvSchedules
        '
        Me.tvSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvSchedules.AllowDrop = True
        Me.tvSchedules.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.tvSchedules.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvSchedules.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvSchedules.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvSchedules.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvSchedules.ExpandLineColor = System.Drawing.Color.White
        Me.tvSchedules.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvSchedules.Location = New System.Drawing.Point(0, 0)
        Me.tvSchedules.Name = "tvSchedules"
        Me.tvSchedules.NodesConnector = Me.NodeConnector2
        Me.tvSchedules.NodeStyle = Me.ElementStyle2
        Me.tvSchedules.PathSeparator = ";"
        Me.tvSchedules.Size = New System.Drawing.Size(179, 811)
        Me.tvSchedules.Styles.Add(Me.ElementStyle2)
        Me.tvSchedules.TabIndex = 6
        Me.tvSchedules.Text = "AdvTree1"
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle2
        '
        Me.ElementStyle2.Class = ""
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'pgLoading
        '
        '
        '
        '
        Me.pgLoading.BackgroundStyle.Class = ""
        Me.pgLoading.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pgLoading.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pgLoading.Location = New System.Drawing.Point(182, 797)
        Me.pgLoading.Name = "pgLoading"
        Me.pgLoading.Size = New System.Drawing.Size(1174, 14)
        Me.pgLoading.TabIndex = 16
        '
        'ContextMenuBar1
        '
        Me.ContextMenuBar1.AntiAlias = True
        Me.ContextMenuBar1.DockSide = DevComponents.DotNetBar.eDockSide.Document
        Me.ContextMenuBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.history})
        Me.ContextMenuBar1.Location = New System.Drawing.Point(0, 0)
        Me.ContextMenuBar1.Name = "ContextMenuBar1"
        Me.ContextMenuBar1.Size = New System.Drawing.Size(97, 25)
        Me.ContextMenuBar1.Stretch = True
        Me.ContextMenuBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ContextMenuBar1.TabIndex = 17
        Me.ContextMenuBar1.TabStop = False
        Me.ContextMenuBar1.Text = "ContextMenuBar1"
        '
        'history
        '
        Me.history.AutoExpandOnClick = True
        Me.history.Name = "history"
        Me.history.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnLoadAll})
        Me.history.Text = "ButtonItem1"
        '
        'btnLoadAll
        '
        Me.btnLoadAll.Name = "btnLoadAll"
        Me.btnLoadAll.Text = "Load All History"
        '
        'ucScheduleHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.FlowLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "ucScheduleHistory"
        Me.Size = New System.Drawing.Size(1356, 893)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.grSort.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.tvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlInfo.ResumeLayout(False)
        CType(Me.tvSchedules, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdClear As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRefresh As DevComponents.DotNetBar.ButtonX
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents grSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlInfo As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents lblInfo As DevComponents.DotNetBar.LabelX
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvSchedules As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents tvDetails As DevComponents.AdvTree.AdvTree
    Friend WithEvents ColumnHeader1 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader2 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader3 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents pgLoading As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents ContextMenuBar1 As DevComponents.DotNetBar.ContextMenuBar
    Friend WithEvents history As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnLoadAll As DevComponents.DotNetBar.ButtonItem

End Class
