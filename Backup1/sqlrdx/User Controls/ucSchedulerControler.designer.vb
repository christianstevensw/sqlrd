﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSchedulerControler
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucSchedulerControler))
        Me.lblSchedulerStatus = New DevComponents.DotNetBar.LabelX()
        Me.tblSchedulerStatus = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnStopScheduler = New DevComponents.DotNetBar.ButtonX()
        Me.imageSchedulerstatus = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnStartScheduler = New DevComponents.DotNetBar.ButtonX()
        Me.tblSchedulerStatus.SuspendLayout()
        Me.FlowLayoutPanel11.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblSchedulerStatus
        '
        '
        '
        '
        Me.lblSchedulerStatus.BackgroundStyle.Class = ""
        Me.lblSchedulerStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tblSchedulerStatus.SetColumnSpan(Me.lblSchedulerStatus, 2)
        Me.lblSchedulerStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSchedulerStatus.Location = New System.Drawing.Point(3, 258)
        Me.lblSchedulerStatus.Name = "lblSchedulerStatus"
        Me.lblSchedulerStatus.Size = New System.Drawing.Size(186, 14)
        Me.lblSchedulerStatus.TabIndex = 19
        Me.lblSchedulerStatus.Text = "Scheduler Status: "
        Me.lblSchedulerStatus.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'tblSchedulerStatus
        '
        Me.tblSchedulerStatus.ColumnCount = 2
        Me.tblSchedulerStatus.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblSchedulerStatus.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblSchedulerStatus.Controls.Add(Me.lblSchedulerStatus, 0, 2)
        Me.tblSchedulerStatus.Controls.Add(Me.FlowLayoutPanel11, 0, 1)
        Me.tblSchedulerStatus.Controls.Add(Me.imageSchedulerstatus, 0, 0)
        Me.tblSchedulerStatus.Controls.Add(Me.FlowLayoutPanel1, 1, 1)
        Me.tblSchedulerStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblSchedulerStatus.Location = New System.Drawing.Point(0, 0)
        Me.tblSchedulerStatus.Name = "tblSchedulerStatus"
        Me.tblSchedulerStatus.RowCount = 3
        Me.tblSchedulerStatus.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.0!))
        Me.tblSchedulerStatus.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.tblSchedulerStatus.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblSchedulerStatus.Size = New System.Drawing.Size(192, 275)
        Me.tblSchedulerStatus.TabIndex = 18
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.Controls.Add(Me.btnStopScheduler)
        Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Right
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(9, 207)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(84, 45)
        Me.FlowLayoutPanel11.TabIndex = 14
        '
        'btnStopScheduler
        '
        Me.btnStopScheduler.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnStopScheduler.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnStopScheduler.Image = CType(resources.GetObject("btnStopScheduler.Image"), System.Drawing.Image)
        Me.btnStopScheduler.Location = New System.Drawing.Point(3, 3)
        Me.btnStopScheduler.Name = "btnStopScheduler"
        Me.btnStopScheduler.Size = New System.Drawing.Size(75, 33)
        Me.btnStopScheduler.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnStopScheduler.TabIndex = 0
        '
        'imageSchedulerstatus
        '
        '
        '
        '
        Me.imageSchedulerstatus.BackgroundStyle.Class = ""
        Me.imageSchedulerstatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.imageSchedulerstatus.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.tblSchedulerStatus.SetColumnSpan(Me.imageSchedulerstatus, 2)
        Me.imageSchedulerstatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.imageSchedulerstatus.Image = CType(resources.GetObject("imageSchedulerstatus.Image"), System.Drawing.Image)
        Me.imageSchedulerstatus.Location = New System.Drawing.Point(3, 3)
        Me.imageSchedulerstatus.Name = "imageSchedulerstatus"
        Me.imageSchedulerstatus.Size = New System.Drawing.Size(186, 198)
        Me.imageSchedulerstatus.TabIndex = 15
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnStartScheduler)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(99, 207)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(82, 45)
        Me.FlowLayoutPanel1.TabIndex = 20
        '
        'btnStartScheduler
        '
        Me.btnStartScheduler.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnStartScheduler.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnStartScheduler.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnStartScheduler.Image = CType(resources.GetObject("btnStartScheduler.Image"), System.Drawing.Image)
        Me.btnStartScheduler.Location = New System.Drawing.Point(4, 3)
        Me.btnStartScheduler.Name = "btnStartScheduler"
        Me.btnStartScheduler.Size = New System.Drawing.Size(75, 33)
        Me.btnStartScheduler.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnStartScheduler.TabIndex = 0
        '
        'ucSchedulerControler
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.tblSchedulerStatus)
        Me.Name = "ucSchedulerControler"
        Me.Size = New System.Drawing.Size(192, 275)
        Me.tblSchedulerStatus.ResumeLayout(False)
        Me.FlowLayoutPanel11.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSchedulerStatus As DevComponents.DotNetBar.LabelX
    Friend WithEvents tblSchedulerStatus As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel11 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnStopScheduler As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnStartScheduler As DevComponents.DotNetBar.ButtonX
    Friend WithEvents imageSchedulerstatus As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel

End Class
