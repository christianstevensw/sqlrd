﻿Public Class ucSchedule
    Dim ep As ErrorProvider = New ErrorProvider
    Dim isLoaded As Boolean = False
    Public Enum modeEnum
        SETUP = 0
        EDIT = 1
    End Enum


    Private m_scheduleID As Integer
    Public Property scheduleID() As Integer
        Get
            Return m_scheduleID
        End Get
        Set(ByVal value As Integer)
            m_scheduleID = value
        End Set
    End Property

    Public ReadOnly Property m_EnableRepeat(ByVal scheduleID As Integer) As Boolean
        Get
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            SQL = "SELECT OtherUnit FROM ScheduleOptions WHERE ScheduleID =" & scheduleID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then
                Return True
            ElseIf oRs.EOF = True Then
                Return True
            Else
                Dim unit As String = IsNull(oRs("otherunit").Value, "")

                Select Case unit
                    Case "Seconds", "Minutes", "Hours"
                        Return False
                    Case Else
                        Return True
                End Select
            End If
        End Get
    End Property
 

    Public ReadOnly Property m_endDate() As String
        Get
            If chkEndDate.Checked = False Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                dtEndDate.Value = d
            End If

            Return ConDate(CTimeZ(dtEndDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public Property m_nextRun() As String
        Get
            Return ConDate(CTimeZ(dtNextRunDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(dtNextRunTime.Value, dateConvertType.WRITE))
        End Get
        Set(ByVal value As String)
            dtNextRunDate.Value = (CTimeZ(value, dateConvertType.READ))
            dtNextRunTime.Value = (CTimeZ(value, dateConvertType.READ))
        End Set
    End Property

    Public ReadOnly Property m_startTime() As String
        Get
            Return ConTime(CTimeZ(dtScheduleTime.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_startDate() As String
        Get
            Return ConDateTime(CTimeZ(dtStartDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_repeatUntil() As String
        Get

            Dim s As String = ConTime(CTimeZ(dtRepeatUntil.Value, dateConvertType.WRITE))

            Return s
        End Get
    End Property

    Private m_mode As modeEnum
    Public Property mode() As modeEnum
        Get
            Return m_mode
        End Get
        Set(ByVal value As modeEnum)
            m_mode = value

            If value = modeEnum.SETUP Then
                isLoaded = True
                dtNextRunDate.Enabled = False
                dtNextRunTime.Enabled = False
            Else
                dtStartDate.Enabled = True
                dtNextRunDate.Enabled = True
                dtNextRunTime.Enabled = False
            End If
        End Set
    End Property

    Private frequency As String
    Public Property sFrequency() As String
        Get
            Dim value As String = stabFrequencyOptions.SelectedTab.Text

            If value = "Custom Calendar" Then
                Return "Custom"
            ElseIf value = "Week Days" Then
                Return "Week-Daily"
            ElseIf value = "Annual" Then
                Return "Yearly"
            Else
                Return value
            End If
        End Get
        Set(ByVal value As String)
            If value = "Custom" Or value = "Custom Calendar" Then
                value = "Custom Calendar"
            ElseIf value.ToLower = "week-daily" Or value.ToLower = "weekdays" Or value.ToLower = "week days" Then
                value = "Week Days"
            ElseIf value.ToLower = "Yearly" Then
                value = "Annual"

            End If

            For Each tab As DevComponents.DotNetBar.SuperTabItem In stabFrequencyOptions.Tabs
                If String.Compare(value, tab.Text, True) = 0 Then
                    stabFrequencyOptions.SelectedTab = tab
                    Exit For
                End If
            Next
        End Set
    End Property

    Public Property m_RepeatUnit() As String
        Get
            Return cmbRepeatUnit.Text
        End Get
        Set(ByVal value As String)
            cmbRepeatUnit.SelectedIndex = cmbRepeatUnit.Items.IndexOf(value)
        End Set
    End Property

    Public Property scheduleStatus() As Boolean
        Get
            Return chkstatus.Checked
        End Get
        Set(ByVal value As Boolean)
            chkstatus.Checked = value
        End Set
    End Property

    Private Sub rbtnDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim selectedRad As RadioButton = CType(sender, RadioButton)

        If selectedRad.Checked Then
            '//make all tabs visible
            For Each tb As DevComponents.DotNetBar.SuperTabItem In stabFrequencyOptions.Tabs
                tb.Visible = True
            Next

            Select Case selectedRad.Text
                Case "Daily"
                    stabFrequencyOptions.SelectedTab = tabDaily
                Case "Weekly"
                    stabFrequencyOptions.SelectedTab = tabWeekly
                Case "Week Days"
                    stabFrequencyOptions.SelectedTab = tabWeekDays
                Case "Monthly"
                    stabFrequencyOptions.SelectedTab = tabMonthly
                Case "Annual"
                    stabFrequencyOptions.SelectedTab = tabYearly
                Case "Custom Calendar"
                    stabFrequencyOptions.SelectedTab = tabCalendar
                Case "Other"
                    stabFrequencyOptions.SelectedTab = tabOther
                Case "None"
                    stabFrequencyOptions.SelectedTab = tabNone
            End Select

            For Each tb As DevComponents.DotNetBar.SuperTabItem In stabFrequencyOptions.Tabs
                If tb.IsSelected = False Then
                    tb.Visible = False
                Else
                    tb.Visible = True
                End If
            Next
        End If
    End Sub

    Public Function isAllDataValid() As Boolean
        If dtStartDate.Value > dtEndDate.Value Then
            setError(ep, dtStartDate, "The start data cannot be after the end date")
            Return False
        ElseIf chkUseExceptionCalendar.Checked And cmbExceptionCalendar.Text = "" Then
            setError(ep, cmbExceptionCalendar, "Select an exception calendar to use")
            Return False
        ElseIf chkUseExceptionCalendar.Checked And cmbExceptionCalendar.Text = "[New...]" Then
            setError(ep, cmbExceptionCalendar, "Select an exception calendar to use")
            Return False
        ElseIf cmbRepeatUnit.Text = "" And chkRepeat.Checked Then
            setError(ep, cmbRepeatUnit, "Select the repeat frequency")
            Return False
        ElseIf chkUseMonthlyOptions.Checked Then
            If cmbMonthDayName.Text = "" Then
                setError(ep, cmbMonthDayName, "Select the day name")
                Return False
            ElseIf cmbMonthDayNumber.Text = "" Then
                setError(ep, cmbMonthDayNumber, "Select the day number")
                Return False
            End If
        ElseIf stabFrequencyOptions.SelectedTab Is tabCalendar Then
            If cmbCustomCalendarName.Text = "" Or cmbCustomCalendarName.Text = "[New...]" Then
                setError(ep, cmbCustomCalendarName, "Select the custom calendar name")
                Return False
            End If
        ElseIf stabFrequencyOptions.SelectedTab Is tabOther Then
            If cmbOtherUnit.Text = "" Then
                setError(ep, cmbOtherUnit, "Select the unit for other frequency")
                Return False
            End If

        End If

        '//lets see if this group has black out times
        Dim SQL As String = "SELECT operationhrsid FROM groupattr g INNER JOIN groupblackoutattr b ON g.groupid = b.groupid WHERE g.groupname ='" & SQLPrepare(gRole) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                Dim opID As Integer = oRs(0).Value
                Dim opHours As clsOperationalHours = New clsOperationalHours(opID)
                Dim checkDate As Date = New Date(dtStartDate.Value.Year, dtStartDate.Value.Month, dtStartDate.Value.Day, dtScheduleTime.Value.Hour, dtScheduleTime.Value.Minute, dtScheduleTime.Value.Second)

                For I As Integer = 0 To 6
                    If opHours.withinOperationHours(checkDate, opHours.operationHoursName) Then
                        ep.SetError(dtScheduleTime, "The specified execution time is not allowed for the Group '" & gRole & "' as its within the '" & opHours.operationHoursName & "' period")
                        dtScheduleTime.Focus()
                        oRs.Close()
                        Return False
                    End If

                    checkDate = clsMarsScheduler.globalItem.GetNextRun(99999, sFrequency, checkDate, dtScheduleTime.Value, chkRepeat.Checked, txtRepeatInterval.Value, dtRepeatUntil.Value, _
                                                                       cmbCustomCalendarName.Text, , True, , cmbRepeatUnit.Text, True)
                Next
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
        Return True
    End Function



    Private Sub chkEndDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEndDate.CheckedChanged
        If chkEndDate.Checked = False Then
            dtEndDate.Value = Date.Now.AddYears(999)
            dtEndDate.Enabled = False
        Else
            dtEndDate.Enabled = True
        End If
    End Sub

    Private Sub chkRepeat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeat.CheckedChanged
        If m_mode = modeEnum.SETUP Then
            If chkRepeat.Checked Then
                cmbRepeatUnit.Text = "Minutes"
            End If
        End If

        txtRepeatInterval.Enabled = chkRepeat.Checked
        cmbRepeatUnit.Enabled = chkRepeat.Checked
        dtRepeatUntil.Enabled = chkRepeat.Checked
    End Sub

    Private Sub ucSchedule_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If m_mode = modeEnum.SETUP Then
            dtStartDate.Value = Now
            dtScheduleTime.Value = Now
            dtEndDate.Value = Date.Now.AddYears(999)
            chkstatus.Checked = True

            Dim iscollabo As Boolean

            Try
                iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
            Catch ex As Exception
                iscollabo = False
            End Try


            If iscollabo = True Then
                cmbCollaboration.Items.Clear()
                cmbCollaboration.Items.Add("DEFAULT")
                chkstatus.Text = "Enable this schedule and execute it on "
                cmbCollaboration.Enabled = True
                cmbCollaboration.Visible = True
                If cmbCollaboration.Text = "" Then cmbCollaboration.Text = "DEFAULT"
            Else
                chkstatus.Text = "Enable this schedule"
                cmbCollaboration.Text = "DEFAULT"
                cmbCollaboration.Enabled = False
                cmbCollaboration.Visible = False
            End If
        Else
            Dim iscollabo As Boolean

            Try
                iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
            Catch ex As Exception
                iscollabo = False
            End Try

            If iscollabo = True Then
                cmbCollaboration.Items.Clear()
                cmbCollaboration.Items.Add("DEFAULT")
                chkstatus.Text = "Enable this schedule and execute it on "
                cmbCollaboration.Enabled = True
                cmbCollaboration.Visible = True
                If cmbCollaboration.Text = "" Then cmbCollaboration.Text = "DEFAULT"
            Else
                chkstatus.Text = "Enable this schedule"
                cmbCollaboration.Text = "DEFAULT"
                cmbCollaboration.Enabled = False
                cmbCollaboration.Visible = False
            End If

        End If

    End Sub


    Private collaborationServerID As Integer
    Public Property m_collaborationServerID() As Integer
        Get
            If cmbCollaboration.Text = "DEFAULT" Or cmbCollaboration.Text = "" Then
                Return 0
            Else
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID from collaboratorsAttr WHERE serverName ='" & SQLPrepare(cmbCollaboration.Text) & "'")

                If oRs Is Nothing Then
                    Return 0
                ElseIf oRs.EOF = True Then
                    Return 0
                Else
                    Return IsNull(oRs(0).Value, 0)
                End If
            End If
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                cmbCollaboration.Text = "DEFAULT"
            Else
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT serverName from collaboratorsAttr WHERE collaboID =" & value)

                If oRs Is Nothing Then
                    cmbCollaboration.Text = "DEFAULT"
                ElseIf oRs.EOF = True Then
                    cmbCollaboration.Text = "DEFAULT"
                Else
                    cmbCollaboration.Text = IsNull(oRs(0).Value, "DEFAULT")
                End If
            End If
        End Set
    End Property


    Private Sub chkUseExceptionCalendar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseExceptionCalendar.CheckedChanged
        cmbExceptionCalendar.Enabled = chkUseExceptionCalendar.Checked
    End Sub

    Private Sub dtStartDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtStartDate.ValueChanged
        If isLoaded = False Then Return

        If mode = modeEnum.SETUP Then
            dtNextRunDate.Value = dtStartDate.Value
        Else
            If isLoaded And dtStartDate.Value.Subtract(Date.Now).TotalDays < 0 Then
                MessageBox.Show("You cannot modify the start date to be in the past", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                dtStartDate.Value = Date.Now
                Return
            End If

            dtNextRunDate.Value = dtStartDate.Value
        End If
    End Sub

    Private Sub dtScheduleTime_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtScheduleTime.ValueChanged

        dtNextRunTime.Value = dtScheduleTime.Value

        If stabFrequencyOptions.SelectedTab.Text = "Daily" And m_mode = modeEnum.SETUP Then
            Dim dt As Date = New Date(dtNextRunDate.Value.Year, dtNextRunDate.Value.Month, dtNextRunDate.Value.Day, dtNextRunTime.Value.Hour, dtNextRunTime.Value.Minute, dtNextRunTime.Value.Second)
            Dim totalMins As Integer = Date.Now.Subtract(dt).TotalMinutes

            If totalMins > 0 Then '//its in the past
                dtNextRunDate.Value = Date.Now.AddDays(1)
            End If
        End If
    End Sub

    Public Function saveScheduleOptions(ByVal scheduleID As Integer) As Date
        Select Case stabFrequencyOptions.SelectedTab.Text.ToLower
            Case "daily"
                Return setdailyOptions(scheduleID)
            Case "weekly"
                Return setWeeklyOptions(scheduleID)
            Case "monthly"
                Return setMonthlyOptions(scheduleID)
            Case "other"
                Return setotherOptions(scheduleID, cmbOtherUnit.Text)
            Case "working day"
                Return setWorkingDayOptions(scheduleID)
            Case Else
                Return Now
        End Select
    End Function

    Public Sub loadScheduleOptions(ByVal scheduleID As Integer)
        Select Case stabFrequencyOptions.SelectedTab.Text.ToLower
            Case "daily"
                getdailyOptions(scheduleID)
            Case "weekly"
                getWeeklyOptions(scheduleID)
            Case "monthly"
                getMonthlyOptions(scheduleID)
            Case "working day"
                getWorkingDayOptions(scheduleID)
            Case "other"
                getotherOptions(scheduleID)
            Case Else
                Return
        End Select
    End Sub
    Public Function getotherOptions(ByVal ScheduleID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer


        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Other'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Me.txtOtherFrequency.Value = oRs("otherfrequency").Value
                Me.cmbOtherUnit.Text = oRs("otherunit").Value
            End If

            oRs.Close()
        End If


    End Function
    Public Function setotherOptions(ByVal ScheduleID As Integer, ByRef selectedUnit As String) As Date
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        Dim sCols As String
        Dim sVals As String

        sCols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType,OtherFrequency,OtherUnit"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        "''," & _
        txtRepeatWeekly.Text & "," & _
        ScheduleID & "," & _
        "'Other'," & _
        Me.txtOtherFrequency.Value & "," & _
        "'" & Me.cmbOtherUnit.Text & "'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oSchedule As New clsMarsScheduler

        selectedUnit = Me.cmbOtherUnit.Text

        Return oSchedule.GetOther(Me.txtOtherFrequency.Value, Me.cmbOtherUnit.Text)

    End Function

    Private Property nthWorkingDay As String
        Get
            If optLastWorkingDay.Checked Then
                Return "Last"
            Else
                Return txtNthWorkingDay.Value
            End If
        End Get
        Set(value As String)
            If value = "Last" Then
                optLastWorkingDay.Checked = True
            Else
                optNthWorkingDay.Checked = True
                txtNthWorkingDay.Value = CInt(value)
            End If
        End Set
    End Property
    Public Overloads Function setWorkingDayOptions(ByVal ScheduleID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        Dim sCols As String = "OptionID,MonthsIn,ScheduleID,ScheduleType"

        Dim sVals As String = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        "'" & nthWorkingDay & "'," & _
        ScheduleID & "," & _
        "'Working Day'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oSchedule As New clsMarsScheduler

        Return oSchedule.getnthWorkingDay(nthWorkingDay, Date.Now.AddMonths(1).Month, Date.Now.AddMonths(1).Year)

    End Function

    Public Overloads Function getWorkingDayOptions(ByVal ScheduleID As Integer)
        Dim SQL As String = "SELECT * FROM scheduleoptions WHERE scheduleid =" & ScheduleID & " AND scheduletype ='Working Day'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            nthWorkingDay = IsNull(oRs("monthsin").Value, "1")

            oRs.Close()

            oRs = Nothing
        End If
    End Function
    Public Overloads Function setWeeklyOptions(ByVal ScheduleID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        Dim chkDays(6) As DevComponents.DotNetBar.Controls.CheckBoxX

        chkDays(0) = chkSunday
        chkDays(1) = chkMonday
        chkDays(2) = chkTuesday
        chkDays(3) = chkWednesday
        chkDays(4) = chkThursday
        chkDays(5) = chkFriday
        chkDays(6) = chkSaturday

       

        Dim sCols As String
        Dim sVals As String
        Dim sDays As String

        For I As Integer = 0 To chkDays.GetUpperBound(0)
            If chkDays(I).Checked = True Then
                sDays &= I & "|"
            End If
        Next

        sCols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        "'" & sDays & "'," & _
        txtRepeatWeekly.Text & "," & _
        ScheduleID & "," & _
        "'Weekly'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oSchedule As New clsMarsScheduler

        Return oSchedule.GetNextWeek(sDays)

    End Function

    Public Overloads Function getWeeklyOptions(ByVal ScheduleID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        ignoreCheckingEvent = True

        Dim chkDays(6) As DevComponents.DotNetBar.Controls.CheckBoxX

        chkDays(0) = chkSunday
        chkDays(1) = chkMonday
        chkDays(2) = chkTuesday
        chkDays(3) = chkWednesday
        chkDays(4) = chkThursday
        chkDays(5) = chkFriday
        chkDays(6) = chkSaturday

        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Weekly'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                txtRepeatWeekly.Text = oRs("ncount").Value

                Dim sValues As String = oRs("monthsin").Value

                '//reset them all
                For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkDays
                    chk.Checked = False
                Next

                For I As Integer = 0 To sValues.Split("|").GetUpperBound(0)
                    Try
                        nIndex = sValues.Split("|")(I)
                        chkDays(nIndex).Checked = True
                    Catch : End Try
                Next
            Else
                For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkDays
                    chk.Checked = True
                Next
            End If
            oRs.Close()
        End If

        ignoreCheckingEvent = False
    End Function
    Public Overloads Function getdailyOptions(ByVal ScheduleID As Integer)

        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String

        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Daily'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then

                txtDailyRepeat.Text = oRs("ncount").Value

            End If
            oRs.Close()
        End If

    End Function

    Public Overloads Function setdailyOptions(ByVal ScheduleID As Integer)
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String
        Dim sCols As String
        Dim sVals As String

        sCols = "OptionID,nCount,ScheduleID,ScheduleType"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        txtDailyRepeat.Text & "," & _
        ScheduleID & "," & _
        "'Daily'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)


        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

    End Function
    Public Function getMonthlyOptions(ByVal scheduleID As Integer)
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim I As Integer
        Dim sWhere As String
        Dim nIndex As Integer
        Dim oItem As clsMyList

        Dim chkMonths(11) As DevComponents.DotNetBar.Controls.CheckBoxX
        chkMonths(0) = chkJanuary
        chkMonths(1) = chkFebruary
        chkMonths(2) = chkMarch
        chkMonths(3) = chkApril
        chkMonths(4) = chkMay
        chkMonths(5) = chkJune
        chkMonths(6) = chkJuly
        chkMonths(7) = chkAugust
        chkMonths(8) = chkSeptember
        chkMonths(9) = chkOctober
        chkMonths(10) = chkNovember
        chkMonths(11) = chkDecember

        sWhere = " WHERE ScheduleID = " & scheduleID & " AND ScheduleType = 'Monthly'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        Dim preNum, preDay As String

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                With oRs
                    Select Case oRs("weekno").Value
                        Case 1
                            PreNum = "First"
                        Case 2
                            PreNum = "Second"
                        Case 3
                            PreNum = "Third"
                        Case 4
                            PreNum = "Fourth"
                        Case 5
                            PreNum = "Last"
                    End Select

                    If oRs("dayno").Value = 7 Then
                        preDay = "Day"
                    Else
                        Dim d As Integer

                        d = oRs("dayno").Value

                        If d < 7 Then
                            preDay = WeekdayName(d + 1, False, vbSunday)
                        Else
                            preDay = "Day"
                        End If
                    End If

                    Dim selectedMonths As String = oRs("monthsin").Value

                    '//reset all the months
                    For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkMonths
                        chk.Checked = False
                    Next

                    For I = 0 To selectedMonths.Split("|").GetUpperBound(0)
                        Try
                            nIndex = selectedMonths.Split("|")(I)
                            chkMonths(nIndex - 1).Checked = True
                        Catch : End Try
                    Next

                    Try
                        chkUseMonthlyOptions.Checked = Convert.ToBoolean(.Fields("useoptions").Value)
                    Catch
                        chkUseMonthlyOptions.Checked = True
                    End Try

                    If preNum <> "" Then cmbMonthDayNumber.Text = preNum

                    If preDay <> "" Then cmbMonthDayName.Text = preDay
                End With
            Else
                For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkMonths
                    chk.Checked = True
                Next
            End If

            oRs.Close()
        End If

    End Function

    'Public Overloads Function MonthlyOptions(ByVal ScheduleIDs() As Integer) As Date
    '    Try
    '        Dim oRs As ADODB.Recordset
    '        Dim oData As New clsMarsData
    '        Dim SQL As String
    '        Dim I As Integer
    '        Dim sWhere As String
    '        Dim nIndex As Integer
    '        Dim oItem As clsMyList

    '        oFreqy = enFreqy.Monthly

    '        grpMonthly.BringToFront()

    '        chkMonths(0) = chkJan
    '        chkMonths(1) = chkFeb
    '        chkMonths(2) = chkMar
    '        chkMonths(3) = chkApr
    '        chkMonths(4) = chkMay
    '        chkMonths(5) = chkJun
    '        chkMonths(6) = chkJul
    '        chkMonths(7) = chkAug
    '        chkMonths(8) = chkSep
    '        chkMonths(9) = chkOct
    '        chkMonths(10) = chkNov
    '        chkMonths(11) = chkDec

    '        Me.ShowDialog()

    '        If UserCancel = True Then Return Now.Date
    '        Dim sMonths As String
    '        Dim nWeekNumber As Integer
    '        Dim nDayName As Integer

    '        Dim sCols As String
    '        Dim sVals As String
    '        sMonths = ""

    '        sCols = "OptionID,WeekNo,DayNo,MonthsIn,ScheduleID,ScheduleType,UseOptions"

    '        For I = 0 To chkMonths.GetUpperBound(0)
    '            If chkMonths(I).Checked = True Then
    '                sMonths &= I + 1 & "|"
    '            End If
    '        Next

    '        oItem = cmbWeekNumber.Items(cmbWeekNumber.SelectedIndex)

    '        nWeekNumber = oItem.ItemData

    '        oItem = cmbDayName.Items(cmbDayName.SelectedIndex)

    '        nDayName = oItem.ItemData

    '        For Each scheduleID As Integer In ScheduleIDs
    '            If chkEnableOptions.Checked = False Then
    '                clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)
    '                Return Now.Date
    '            End If

    '            sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
    '                nWeekNumber & "," & _
    '                nDayName & "," & _
    '                "'" & sMonths & "'," & _
    '                scheduleID & "," & _
    '                "'Monthly'," & _
    '                Convert.ToInt32(chkEnableOptions.Checked)

    '            SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

    '            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)

    '            If chkEnableOptions.Checked Then clsMarsData.WriteData(SQL)
    '        Next

    '        If chkEnableOptions.Checked Then
    '            Dim oSchedule As New clsMarsScheduler

    '            Dim NewDate As Date

    '            NewDate = oSchedule.GetNextMonth(sMonths, Now.Month - 1)

    '            NewDate = oSchedule.GetNthDayOfMonth(NewDate, _
    '                   nWeekNumber, nDayName)

    '            Return NewDate
    '        Else
    '            Return Now.Date
    '        End If

    '    Catch : End Try
    'End Function
    Public Function setMonthlyOptions(ByVal ScheduleID As Integer) As Date
        Try
            Dim SQL As String
            Dim chkMonths(11) As DevComponents.DotNetBar.Controls.CheckBoxX
            chkMonths(0) = chkJanuary
            chkMonths(1) = chkFebruary
            chkMonths(2) = chkMarch
            chkMonths(3) = chkApril
            chkMonths(4) = chkMay
            chkMonths(5) = chkJune
            chkMonths(6) = chkJuly
            chkMonths(7) = chkAugust
            chkMonths(8) = chkSeptember
            chkMonths(9) = chkOctober
            chkMonths(10) = chkNovember
            chkMonths(11) = chkDecember

            If chkUseMonthlyOptions.Checked = False Then
                clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
                Return Now.Date
            End If

            Dim sCols As String
            Dim sVals As String
            Dim sMonths As String

            sCols = "OptionID,WeekNo,DayNo,MonthsIn,ScheduleID,ScheduleType,UseOptions"

            For I As Integer = 0 To chkMonths.GetUpperBound(0)
                If chkMonths(I).Checked = True Then
                    sMonths &= I + 1 & "|"
                End If
            Next

            Dim nWeekNumber As Integer
            Dim nDayName As Integer
            Dim weekNumbers As Hashtable = New Hashtable

            weekNumbers.Add("First", 1)
            weekNumbers.Add("Second", 2)
            weekNumbers.Add("Third", 3)
            weekNumbers.Add("Fourth", 4)
            weekNumbers.Add("Last", 5)


            nWeekNumber = weekNumbers.Item(cmbMonthDayNumber.Text)

            Dim dayNumbers As Hashtable = New Hashtable
           
            dayNumbers.Add("Monday", 1)
            dayNumbers.Add("Tuesday", 2)
            dayNumbers.Add("Wednesday", 3)
            dayNumbers.Add("Thursday", 4)
            dayNumbers.Add("Friday", 5)
            dayNumbers.Add("Saturday", 6)
            dayNumbers.Add("Sunday", 0)
            dayNumbers.Add("Day", 7)

            nDayName = dayNumbers.Item(cmbMonthDayName.Text)

            sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                nWeekNumber & "," & _
                nDayName & "," & _
                "'" & sMonths & "'," & _
                ScheduleID & "," & _
                "'Monthly'," & _
                Convert.ToInt32(chkUseMonthlyOptions.Checked)

            Sql = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

            If chkUseMonthlyOptions.Checked Then
                clsMarsData.WriteData(SQL)

                Dim oSchedule As New clsMarsScheduler

                Dim NewDate As Date

                NewDate = oSchedule.GetNextMonth(sMonths, Now.Month - 1)

                NewDate = oSchedule.GetNthDayOfMonth(NewDate, _
                       nWeekNumber, nDayName)

                Return NewDate

            Else
                Return Now.Date
            End If

        Catch : End Try
    End Function

    Public Function saveSchedule(ByVal nID As Integer, ByVal scheduleType As clsMarsScheduler.enScheduleType, ByVal desc As String, ByVal tags As String) As Integer
        Dim SQL As String
        Dim key As String
        Dim nScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        scheduleID = nScheduleID

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                key = "autoid"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                key = "eventpackid"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                key = "packid"
            Case clsMarsScheduler.enScheduleType.REPORT
                key = "reportid"
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                key = "smartid"
            Case Else
                Return 0
        End Select

        If sFrequency.ToLower = "none" Then
            chkstatus.Checked = False
        End If

        With Me
            SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
         "StartTime,Repeat,RepeatInterval," & _
         "RepeatUntil,Status," & key & ",Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit,collaborationServer) " & _
         "VALUES(" & _
         scheduleID & "," & _
         "'" & Me.sFrequency & "'," & _
         "'" & ConDateTime(CTimeZ(.dtStartDate.Value, dateConvertType.WRITE)) & "'," & _
         "'" & ConDateTime(CTimeZ(.dtEndDate.Value, dateConvertType.WRITE)) & "'," & _
         "'" & ConDate(CTimeZ(.dtStartDate.Value.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.dtScheduleTime.Value, dateConvertType.WRITE)) & "'," & _
         "'" & CTimeZ(.dtScheduleTime.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
         Convert.ToInt32(.chkRepeat.Checked) & "," & _
         "'" & Convert.ToString(.txtRepeatInterval.Value).Replace(",", ".") & "'," & _
         "'" & CTimeZ(.dtRepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
         Convert.ToInt32(.chkstatus.Checked) & "," & _
         nID & "," & _
         "'" & SQLPrepare(desc) & "'," & _
         "'" & SQLPrepare(tags) & "'," & _
         "'" & SQLPrepare(.cmbCustomCalendarName.Text) & "'," & _
         Convert.ToInt32(.chkUseExceptionCalendar.Checked) & "," & _
         "'" & SQLPrepare(.cmbExceptionCalendar.Text) & "'," & _
         "'" & .m_RepeatUnit & "'," & _
         m_collaborationServerID & ")"
        End With

        If clsMarsData.WriteData(SQL) = False Then
            Return 0
        End If

        Me.saveScheduleOptions(nScheduleID)

        Return nScheduleID
    End Function

    Public Function updateSchedule(ByVal nID As Integer, ByVal scheduleType As clsMarsScheduler.enScheduleType, ByVal desc As String, ByVal tags As String) As Boolean
        Dim SQL As String
        Dim key As String
        Dim nScheduleID As Integer

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                key = "autoid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , nID)
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                key = "eventpackid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                key = "packid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, nID)
            Case clsMarsScheduler.enScheduleType.REPORT
                key = "reportid"
                nScheduleID = clsMarsScheduler.GetScheduleID(nID)
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                key = "smartid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , , nID)
            Case Else
                Return True
        End Select

        scheduleID = nScheduleID

        With Me
            If sFrequency.ToLower = "none" Then
                chkstatus.Checked = False
            End If

            If .chkEndDate.Checked = False Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .dtEndDate.Value = d
            End If

            SQL = "UPDATE ScheduleAttr SET " & _
      "Frequency = '" & .sFrequency & "', " & _
      "EndDate = '" & .m_endDate & "', " & _
      "StartDate = '" & .m_startDate & "'," & _
      "NextRun = '" & .m_nextRun & "', " & _
      "StartTime = '" & .m_startTime & "', " & _
      "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
      "RepeatInterval = '" & txtRepeatInterval.Value & "'," & _
      "Status = " & Convert.ToInt32(.chkstatus.Checked) & "," & _
      "RepeatUntil = '" & .m_repeatUntil & "', " & _
      "Description = '" & SQLPrepare(desc) & "'," & _
      "KeyWord = '" & SQLPrepare(tags) & "', " & _
      "CalendarName = '" & SQLPrepare(.cmbCustomCalendarName.Text) & "', " & _
      "UseException = " & Convert.ToInt32(.chkUseExceptionCalendar.Checked) & "," & _
      "ExceptionCalendar ='" & SQLPrepare(.cmbExceptionCalendar.Text) & "', " & _
      "RepeatUnit ='" & .m_RepeatUnit & "', " & _
      "collaborationServer = " & m_collaborationServerID
        End With

        SQL &= " WHERE " & key & " =" & nID

        clsMarsData.WriteData(SQL)

        saveScheduleOptions(nScheduleID)

    End Function

    Public Sub loadScheduleInfo(ByVal nID As Integer, ByVal scheduleType As clsMarsScheduler.enScheduleType, ByRef txtDesc As TextBox, ByRef txtKeyword As TextBox, Optional ByRef oTask As ucTasks = Nothing)
        isLoaded = False
        Dim SQL As String
        Dim key As String
        Dim nScheduleID As Integer

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                key = "autoid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , nID)
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                key = "eventpackid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)
            Case clsMarsScheduler.enScheduleType.PACKAGE
                key = "packid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, nID)
            Case clsMarsScheduler.enScheduleType.REPORT
                key = "reportid"
                nScheduleID = clsMarsScheduler.GetScheduleID(nID)
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                key = "smartid"
                nScheduleID = clsMarsScheduler.GetScheduleID(, , , nID)

                If nScheduleID = 0 Then
                    chkstatus.Checked = False
                    Return
                End If
            Case Else
                Return
        End Select

        If nScheduleID = 0 Then Return

        scheduleID = nScheduleID

        SQL = "SELECT * FROM ScheduleAttr WHERE scheduleID = " & nScheduleID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        If oRs.EOF = False Then

            sFrequency = oRs("frequency").Value

            If sFrequency = "Custom Calendar" Or sFrequency = "Custom" Then
                cmbCustomCalendarName.Text = oRs("calendarname").Value
            End If
            ' m_fireOthers = True
            chkRepeat.Enabled = m_EnableRepeat(scheduleID)

            Try
                If oRs("status").Value = 1 Then
                    chkstatus.Checked = True
                Else
                    chkstatus.Checked = False
                End If
            Catch ex As Exception
                chkstatus.Checked = True
            End Try

            Try
                m_RepeatUnit = IsNull(oRs("repeatunit").Value, "hours")
            Catch ex As Exception
                m_RepeatUnit = "hours"
            End Try

            If oTask IsNot Nothing Then
                oTask.ScheduleID = nScheduleID
                oTask.LoadTasks()
            End If

            Try
                Dim d As Date

                d = oRs("enddate").Value

                If d.Year >= 3004 Then
                    dtEndDate.Enabled = False
                    chkEndDate.Checked = True
                End If

            Catch : End Try

            Try
                Dim repeatUntil As String = IsNull(oRs("repeatuntil").Value, Now)

                If repeatUntil.Length > 10 Then
                    dtRepeatUntil.Value = CTimeZ(ConDateTime(repeatUntil), dateConvertType.READ)
                Else
                    dtRepeatUntil.Value = CTimeZ(Now.Date & " " & repeatUntil, dateConvertType.READ)
                End If
            Catch : End Try

            loadScheduleOptions(nScheduleID)

            m_nextRun = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
            dtEndDate.Value = CTimeZ(oRs("enddate").Value, dateConvertType.READ)
            dtScheduleTime.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & oRs("starttime").Value), dateConvertType.READ)

            Try
                dtNextRunTime.Value = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
            Catch : End Try

            chkRepeat.Checked = Convert.ToBoolean(oRs("repeat").Value)
            txtRepeatInterval.Text = IsNonEnglishRegionRead(oRs("repeatinterval").Value)

            dtStartDate.Value = CTimeZ(ConDate(oRs("startdate").Value), dateConvertType.READ)

            cmbExceptionCalendar.Text = IsNull(oRs("exceptioncalendar").Value)

            Try
                chkUseExceptionCalendar.Checked = Convert.ToBoolean(oRs("useexception").Value)
            Catch
                chkUseExceptionCalendar.Checked = False
            End Try
        End If

        If txtRepeatInterval.Text = "" Then txtRepeatInterval.Text = 0.25

        txtDesc.Text = IsNull(oRs("description").Value)
        If txtKeyword IsNot Nothing Then txtKeyword.Text = IsNull(oRs("keyword").Value)

        oRs.Close()



        '//loadcollaboration ish
        Dim iscollabo As Boolean

        Try
            iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
        Catch ex As Exception
            iscollabo = False
        End Try


        If iscollabo = True Then
            cmbCollaboration.Items.Clear()
            cmbCollaboration.Items.Add("DEFAULT")
            chkstatus.Text = "Enable this schedule and execute it on "
            cmbCollaboration.Enabled = True

            oRs = clsMarsData.GetData("SELECT collaboID, serverName FROM collaboratorsAttr")


            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    cmbCollaboration.Items.Add(oRs("serverName").Value.ToString.ToUpper)
                    oRs.MoveNext()
                Loop

                oRs.Close()
                oRs = Nothing
            End If

            cmbCollaboration.Text = clsCollaboration.getCollaborationForSchedule(scheduleID)
        Else
            chkstatus.Text = "Enable this schedule"
            cmbCollaboration.Text = "DEFAULT"
            cmbCollaboration.Enabled = False
        End If

        isLoaded = True
    End Sub

    Private Sub cmbExceptionCalendar_DropDown(sender As Object, e As System.EventArgs) Handles cmbExceptionCalendar.DropDown
        cmbExceptionCalendar.Items.Clear()

        cmbExceptionCalendar.Items.Add("[New...]")

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbExceptionCalendar.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

  
    Private Sub cmbCustomCalendarName_DropDown(sender As Object, e As System.EventArgs) Handles cmbCustomCalendarName.DropDown
        cmbCustomCalendarName.Items.Clear()

        cmbCustomCalendarName.Items.Add("[New...]")

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbCustomCalendarName.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub chkstatus_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkstatus.CheckedChanged
        Dim iscollabo As Boolean

        Try
            iscollabo = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0))
        Catch ex As Exception
            iscollabo = False
        End Try

        If chkstatus.Checked Then
            If iscollabo = True Then
                cmbCollaboration.Enabled = True
            Else
                cmbCollaboration.Text = "DEFAULT"
                cmbCollaboration.Enabled = False
            End If
        Else
            cmbCollaboration.Text = "DEFAULT"
            cmbCollaboration.Enabled = False
        End If
    End Sub

    Dim ignoreCheckingEvent As Boolean = False

    Private Sub chkMonday_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkMonday.CheckedChanged, chkTuesday.CheckedChanged, chkWednesday.CheckedChanged, chkThursday.CheckedChanged,
        chkFriday.CheckedChanged, chkSaturday.CheckedChanged, chkSunday.CheckedChanged

        If ignoreCheckingEvent Then Return

        Try
            Dim chkDays(6) As DevComponents.DotNetBar.Controls.CheckBoxX

            chkDays(0) = chkSunday
            chkDays(1) = chkMonday
            chkDays(2) = chkTuesday
            chkDays(3) = chkWednesday
            chkDays(4) = chkThursday
            chkDays(5) = chkFriday
            chkDays(6) = chkSaturday

            Dim sDays As String
            Dim allUnchecked As Boolean = True

            For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkDays
                If chk.Checked Then
                    allUnchecked = False
                End If
            Next

            If allUnchecked Then
                chkMonday.Checked = True
            End If


            For I As Integer = 0 To chkDays.GetUpperBound(0)
                If chkDays(I).Checked = True Then
                    sDays &= I & "|"
                End If
            Next

            Dim oSchedule As New clsMarsScheduler

            If m_mode = modeEnum.EDIT Then
                dtNextRunDate.Value = oSchedule.GetNextWeek(sDays)
            End If

            dtStartDate.Value = oSchedule.GetNextWeek(sDays)
        Catch : End Try
    End Sub

    Private Sub chkJanuary_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkJanuary.CheckedChanged, chkFebruary.CheckedChanged, chkMarch.CheckedChanged, chkApril.CheckedChanged,
        chkMay.CheckedChanged, chkJune.CheckedChanged, chkJuly.CheckedChanged, chkAugust.CheckedChanged,
        chkSeptember.CheckedChanged, chkOctober.CheckedChanged, chkNovember.CheckedChanged, chkDecember.CheckedChanged, cmbMonthDayName.SelectedIndexChanged, cmbMonthDayNumber.SelectedIndexChanged

        If cmbMonthDayName.Text = "" Then Return
        If cmbMonthDayNumber.Text = "" Then Return

        Try
            Dim sMonths As String

            Dim chkMonths(11) As DevComponents.DotNetBar.Controls.CheckBoxX
            chkMonths(0) = chkJanuary
            chkMonths(1) = chkFebruary
            chkMonths(2) = chkMarch
            chkMonths(3) = chkApril
            chkMonths(4) = chkMay
            chkMonths(5) = chkJune
            chkMonths(6) = chkJuly
            chkMonths(7) = chkAugust
            chkMonths(8) = chkSeptember
            chkMonths(9) = chkOctober
            chkMonths(10) = chkNovember
            chkMonths(11) = chkDecember

            Dim allUnchecked As Boolean = True

            For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkMonths
                If chk.Checked Then
                    allUnchecked = False
                End If
            Next

            If allUnchecked Then
                chkJanuary.Checked = True
            End If

            Dim I As Integer

            For I = 0 To chkMonths.GetUpperBound(0)
                If chkMonths(I).Checked = True Then
                    sMonths &= I + 1 & "|"
                End If
            Next


            Dim oSchedule As New clsMarsScheduler

            Dim nWeekNumber As Integer

            Select Case cmbMonthDayNumber.Text
                Case "First"
                    nWeekNumber = 1
                Case "Second"
                    nWeekNumber = 2
                Case "Third"
                    nWeekNumber = 3
                Case "Fourth"
                    nWeekNumber = 4
                Case "Last"
                    nWeekNumber = 5
                Case Else
                    Return
            End Select

            Dim nDayName As Integer

            Select Case cmbMonthDayName.Text
                Case "Day"
                    nDayName = 7
                Case "Sunday"
                    nDayName = 0
                Case "Monday"
                    nDayName = 1
                Case "Tuesday"
                    nDayName = 2
                Case "Wednesday"
                    nDayName = 3
                Case "Thursday"
                    nDayName = 4
                Case "Friday"
                    nDayName = 5
                Case "Saturday"
                    nDayName = 6
                Case Else
                    Return
            End Select

            Dim NewDate As Date

            NewDate = oSchedule.GetNextMonth(sMonths, Now.Month - 1)

            NewDate = oSchedule.GetNthDayOfMonth(NewDate, _
                   nWeekNumber, nDayName)

            dtStartDate.Value = NewDate

            If m_mode = modeEnum.EDIT Then
                dtNextRunDate.Value = NewDate
            End If
        Catch : End Try
    End Sub

    Private Sub cmbOtherUnit_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbOtherUnit.SelectedIndexChanged
        Dim oSchedule As New clsMarsScheduler

        Dim selectedUnit As String = Me.cmbOtherUnit.Text

        dtStartDate.Value = oSchedule.GetOther(Me.txtOtherFrequency.Value, Me.cmbOtherUnit.Text)
    End Sub

    Private Sub chkUseMonthlyOptions_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkUseMonthlyOptions.CheckedChanged
        monthlyOption1.Enabled = chkUseMonthlyOptions.Checked
        monthlyOptions2.Enabled = chkUseMonthlyOptions.Checked
    End Sub

    Private Sub stabFrequencyOptions_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabFrequencyOptions.SelectedTabChanged
        If stabFrequencyOptions.SelectedTab.Text = "None" Then
            chkstatus.Checked = False
            chkstatus.Enabled = False
        ElseIf stabFrequencyOptions.SelectedTab.Text = "Other" Then
            chkRepeat.Checked = False
            chkRepeat.Enabled = False
        Else
            chkstatus.Checked = True
            chkstatus.Enabled = True
            chkRepeat.Enabled = True
        End If
    End Sub

    Private Sub cmbCustomCalendarName_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbCustomCalendarName.SelectedIndexChanged, cmbExceptionCalendar.SelectedIndexChanged
        Dim cmb As ComboBox = CType(sender, ComboBox)

        If cmb.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar

                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    If cmb.Name = "cmbCustomCalendarName" Then
                        cmbCustomCalendarName_DropDown(sender, e)
                        cmbCustomCalendarName.Text = temp
                    Else
                        cmbExceptionCalendar_DropDown(sender, e)
                        cmbExceptionCalendar.Text = temp
                    End If
                End If
            End If
        Else
            If cmb.Name = "cmbCustomCalendarName" Then
                Dim oS As New clsMarsScheduler

                If m_mode = modeEnum.SETUP Then
                    dtStartDate.Value = oS.GetCustomNextRunDate(cmbCustomCalendarName.Text, Now.Date.AddDays(-1))
                    dtEndDate.Value = oS.GetCustomEndDate(cmbCustomCalendarName.Text)
                Else
                    dtNextRunDate.Value = oS.GetCustomNextRunDate(cmbCustomCalendarName.Text, Now.Date.AddDays(-1))
                    dtEndDate.Value = oS.GetCustomEndDate(cmbCustomCalendarName.Text)
                End If
            End If
            End If
    End Sub

    Private Sub cmbCollaboration_DropDown(sender As Object, e As System.EventArgs) Handles cmbCollaboration.DropDown

        If cmbCollaboration.Items.Count <= 1 Then
            cmbCollaboration.Items.Clear()
            cmbCollaboration.Items.Add("DEFAULT")

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboID, serverName FROM collaboratorsAttr")

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    cmbCollaboration.Items.Add(oRs("serverName").Value.ToString.ToUpper)
                    oRs.MoveNext()
                Loop

                oRs.Close()
                oRs = Nothing
            End If
        End If
    End Sub

   

    Private Sub LabelX14_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub RadioButton1_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles optNthWorkingDay.CheckedChanged, optLastWorkingDay.CheckedChanged
        txtNthWorkingDay.Enabled = optNthWorkingDay.Checked

        Dim sched As clsMarsScheduler = New clsMarsScheduler

        If optNthWorkingDay.Checked Then
            Dim tmp As Date = sched.getnthWorkingDay(txtNthWorkingDay.Value, Date.Now.Month, Date.Now.Year)

            If tmp < Date.Now Then
                tmp = sched.getnthWorkingDay(txtNthWorkingDay.Value, Date.Now.AddMonths(1).Month, Date.Now.AddMonths(1).Year)
            End If

            dtNextRunDate.Value = tmp
        ElseIf optLastWorkingDay.Checked Then
            Dim tmp As Date = sched.getnthWorkingDay("last", Date.Now.Month, Date.Now.Year)

            If tmp < Date.Now Then
                tmp = sched.getnthWorkingDay("last", Date.Now.AddMonths(1).Month, Date.Now.AddMonths(1).Year)
            End If

            dtNextRunDate.Value = tmp
        End If
    End Sub

    Private Sub txtNthWorkingDay_ValueChanged(sender As System.Object, e As System.EventArgs) Handles txtNthWorkingDay.ValueChanged
        Dim sched As clsMarsScheduler = New clsMarsScheduler

        If optNthWorkingDay.Checked Then
            Dim tmp As Date = sched.getnthWorkingDay(txtNthWorkingDay.Value, Date.Now.Month, Date.Now.Year)

            If tmp < Date.Now Then
                tmp = sched.getnthWorkingDay(txtNthWorkingDay.Value, Date.Now.AddMonths(1).Month, Date.Now.AddMonths(1).Year)
            End If

            dtNextRunDate.Value = tmp
        End If
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class
