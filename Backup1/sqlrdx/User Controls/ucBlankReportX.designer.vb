﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucBlankReportX
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkCheckBlankReport = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.stabBlankReports = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcBlankReportTasks = New sqlrd.ucTasks()
        Me.SuperTabItem9 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbBlankFormat = New System.Windows.Forms.ComboBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtMinSize = New System.Windows.Forms.NumericUpDown()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnBuild = New DevComponents.DotNetBar.ButtonX()
        Me.txtQuery = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.optFilesize = New System.Windows.Forms.RadioButton()
        Me.optSQL = New System.Windows.Forms.RadioButton()
        Me.SuperTabItem8 = New DevComponents.DotNetBar.SuperTabItem()
        Me.chkIgnoreReport = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.chkAllReports = New DevComponents.DotNetBar.Controls.CheckBoxX()
        CType(Me.stabBlankReports, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabBlankReports.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.txtMinSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkCheckBlankReport
        '
        Me.chkCheckBlankReport.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkCheckBlankReport.BackgroundStyle.Class = ""
        Me.chkCheckBlankReport.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel1.SetColumnSpan(Me.chkCheckBlankReport, 2)
        Me.chkCheckBlankReport.Location = New System.Drawing.Point(3, 3)
        Me.chkCheckBlankReport.Name = "chkCheckBlankReport"
        Me.chkCheckBlankReport.Size = New System.Drawing.Size(340, 23)
        Me.chkCheckBlankReport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkCheckBlankReport.TabIndex = 5
        Me.chkCheckBlankReport.Text = "Check if the report is blank"
        '
        'stabBlankReports
        '
        Me.stabBlankReports.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        '
        '
        '
        Me.stabBlankReports.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabBlankReports.ControlBox.MenuBox.Name = ""
        Me.stabBlankReports.ControlBox.Name = ""
        Me.stabBlankReports.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabBlankReports.ControlBox.MenuBox, Me.stabBlankReports.ControlBox.CloseBox})
        Me.stabBlankReports.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabBlankReports.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabBlankReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabBlankReports.Enabled = False
        Me.stabBlankReports.Location = New System.Drawing.Point(0, 59)
        Me.stabBlankReports.Name = "stabBlankReports"
        Me.stabBlankReports.ReorderTabsEnabled = True
        Me.stabBlankReports.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabBlankReports.SelectedTabIndex = 0
        Me.stabBlankReports.Size = New System.Drawing.Size(591, 239)
        Me.stabBlankReports.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabBlankReports.TabIndex = 5
        Me.stabBlankReports.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem8, Me.SuperTabItem9})
        Me.stabBlankReports.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.WinMediaPlayer12
        Me.stabBlankReports.Text = "Actions"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.UcBlankReportTasks)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(591, 239)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.SuperTabItem9
        '
        'UcBlankReportTasks
        '
        Me.UcBlankReportTasks.BackColor = System.Drawing.Color.White
        Me.UcBlankReportTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcBlankReportTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlankReportTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcBlankReportTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcBlankReportTasks.m_defaultTaks = False
        Me.UcBlankReportTasks.m_eventBased = False
        Me.UcBlankReportTasks.m_eventID = 99999
        Me.UcBlankReportTasks.m_forExceptionHandling = True
        Me.UcBlankReportTasks.m_showAfterType = False
        Me.UcBlankReportTasks.m_showExpanded = True
        Me.UcBlankReportTasks.Name = "UcBlankReportTasks"
        Me.UcBlankReportTasks.Size = New System.Drawing.Size(591, 239)
        Me.UcBlankReportTasks.TabIndex = 0
        '
        'SuperTabItem9
        '
        Me.SuperTabItem9.AttachedControl = Me.SuperTabControlPanel6
        Me.SuperTabItem9.GlobalItem = False
        Me.SuperTabItem9.Name = "SuperTabItem9"
        Me.SuperTabItem9.Text = "Actions"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.Panel2)
        Me.SuperTabControlPanel8.Controls.Add(Me.Panel1)
        Me.SuperTabControlPanel8.Controls.Add(Me.optFilesize)
        Me.SuperTabControlPanel8.Controls.Add(Me.optSQL)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(591, 215)
        Me.SuperTabControlPanel8.TabIndex = 1
        Me.SuperTabControlPanel8.TabItem = Me.SuperTabItem8
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.GroupBox2)
        Me.Panel2.Location = New System.Drawing.Point(7, 26)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(386, 140)
        Me.Panel2.TabIndex = 4
        Me.Panel2.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel2)
        Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(377, 81)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmbBlankFormat)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(3, 44)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(358, 28)
        Me.FlowLayoutPanel2.TabIndex = 6
        Me.FlowLayoutPanel2.Visible = False
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.Class = ""
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(187, 20)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Export Report to"
        '
        'cmbBlankFormat
        '
        Me.cmbBlankFormat.Enabled = False
        Me.cmbBlankFormat.FormattingEnabled = True
        Me.cmbBlankFormat.Items.AddRange(New Object() {"XML (*.xml)"})
        Me.cmbBlankFormat.Location = New System.Drawing.Point(196, 3)
        Me.cmbBlankFormat.Name = "cmbBlankFormat"
        Me.cmbBlankFormat.Size = New System.Drawing.Size(150, 21)
        Me.cmbBlankFormat.TabIndex = 7
        Me.cmbBlankFormat.Text = "XML (*.xml)"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label3)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtMinSize)
        Me.FlowLayoutPanel1.Controls.Add(Me.Label4)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 13)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(358, 25)
        Me.FlowLayoutPanel1.TabIndex = 5
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(187, 20)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Assume report is blank if size is under"
        '
        'txtMinSize
        '
        Me.txtMinSize.Location = New System.Drawing.Point(196, 3)
        Me.txtMinSize.Maximum = New Decimal(New Integer() {1410065407, 2, 0, 0})
        Me.txtMinSize.Name = "txtMinSize"
        Me.txtMinSize.Size = New System.Drawing.Size(65, 20)
        Me.txtMinSize.TabIndex = 1
        Me.txtMinSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtMinSize.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(267, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(29, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "bytes"
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.btnBuild)
        Me.Panel1.Controls.Add(Me.txtQuery)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(7, 26)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(387, 140)
        Me.Panel1.TabIndex = 2
        Me.Panel1.Visible = False
        '
        'btnBuild
        '
        Me.btnBuild.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBuild.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuild.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBuild.Location = New System.Drawing.Point(289, 114)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 0
        Me.btnBuild.Text = "Build"
        '
        'txtQuery
        '
        Me.txtQuery.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtQuery.Border.Class = "TextBoxBorder"
        Me.txtQuery.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtQuery.Location = New System.Drawing.Point(6, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(358, 89)
        Me.txtQuery.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(57, 15)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "SQL Query"
        '
        'optFilesize
        '
        Me.optFilesize.AutoSize = True
        Me.optFilesize.Location = New System.Drawing.Point(96, 3)
        Me.optFilesize.Name = "optFilesize"
        Me.optFilesize.Size = New System.Drawing.Size(95, 17)
        Me.optFilesize.TabIndex = 0
        Me.optFilesize.Text = "File size check"
        Me.optFilesize.UseVisualStyleBackColor = True
        '
        'optSQL
        '
        Me.optSQL.AutoSize = True
        Me.optSQL.Checked = True
        Me.optSQL.Location = New System.Drawing.Point(13, 3)
        Me.optSQL.Name = "optSQL"
        Me.optSQL.Size = New System.Drawing.Size(77, 17)
        Me.optSQL.TabIndex = 0
        Me.optSQL.TabStop = True
        Me.optSQL.Text = "SQL Query"
        Me.optSQL.UseVisualStyleBackColor = True
        '
        'SuperTabItem8
        '
        Me.SuperTabItem8.AttachedControl = Me.SuperTabControlPanel8
        Me.SuperTabItem8.GlobalItem = False
        Me.SuperTabItem8.Name = "SuperTabItem8"
        Me.SuperTabItem8.Text = "Method"
        '
        'chkIgnoreReport
        '
        Me.chkIgnoreReport.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkIgnoreReport.BackgroundStyle.Class = ""
        Me.chkIgnoreReport.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkIgnoreReport.Enabled = False
        Me.chkIgnoreReport.Location = New System.Drawing.Point(41, 32)
        Me.chkIgnoreReport.Name = "chkIgnoreReport"
        Me.chkIgnoreReport.Size = New System.Drawing.Size(302, 23)
        Me.chkIgnoreReport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkIgnoreReport.TabIndex = 5
        Me.chkIgnoreReport.Text = "Ignore the report and subsequent tasks"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.chkCheckBlankReport, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkIgnoreReport, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkAllReports, 2, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(591, 59)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'chkAllReports
        '
        '
        '
        '
        Me.chkAllReports.BackgroundStyle.Class = ""
        Me.chkAllReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAllReports.Enabled = False
        Me.chkAllReports.Location = New System.Drawing.Point(349, 32)
        Me.chkAllReports.Name = "chkAllReports"
        Me.chkAllReports.Size = New System.Drawing.Size(226, 23)
        Me.chkAllReports.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAllReports.TabIndex = 6
        Me.chkAllReports.Text = "Only run tasks if ALL reports are blank"
        Me.chkAllReports.Visible = False
        '
        'ucBlankReportX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.stabBlankReports)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ucBlankReportX"
        Me.Size = New System.Drawing.Size(591, 298)
        CType(Me.stabBlankReports, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabBlankReports.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.SuperTabControlPanel8.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.txtMinSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkCheckBlankReport As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents stabBlankReports As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents optFilesize As System.Windows.Forms.RadioButton
    Friend WithEvents optSQL As System.Windows.Forms.RadioButton
    Friend WithEvents SuperTabItem8 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcBlankReportTasks As ucTasks
    Friend WithEvents SuperTabItem9 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnBuild As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtQuery As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtMinSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkIgnoreReport As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbBlankFormat As System.Windows.Forms.ComboBox
    Friend WithEvents chkAllReports As DevComponents.DotNetBar.Controls.CheckBoxX

End Class
