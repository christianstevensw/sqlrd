﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucEditorX
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucEditorX))
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.btnBold = New System.Windows.Forms.ToolStripButton()
        Me.btnItalic = New System.Windows.Forms.ToolStripButton()
        Me.btnUnderline = New System.Windows.Forms.ToolStripButton()
        Me.btnStrike = New System.Windows.Forms.ToolStripButton()
        Me.btnFont = New System.Windows.Forms.ToolStripButton()
        Me.btnParagraph = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnLink = New System.Windows.Forms.ToolStripButton()
        Me.btnImage = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnPreview = New System.Windows.Forms.ToolStripButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.htmlBox = New SpiceLogic.WinHTMLEditor.HTMLEditor()
        Me.txtBody = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ToolStripButton1 = New System.Windows.Forms.ToolStripButton()
        Me.ToolStrip1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.htmlBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.htmlBox.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnBold, Me.btnItalic, Me.btnUnderline, Me.btnStrike, Me.btnFont, Me.btnParagraph, Me.ToolStripSeparator1, Me.btnLink, Me.btnImage, Me.ToolStripSeparator2, Me.btnPreview})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(273, 25)
        Me.ToolStrip1.TabIndex = 0
        Me.ToolStrip1.Text = "ToolStrip1"
        Me.ToolStrip1.Visible = False
        '
        'btnBold
        '
        Me.btnBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnBold.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBold.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnBold.Name = "btnBold"
        Me.btnBold.Size = New System.Drawing.Size(23, 22)
        Me.btnBold.Text = "B"
        Me.btnBold.ToolTipText = "Bold"
        '
        'btnItalic
        '
        Me.btnItalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnItalic.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnItalic.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnItalic.Name = "btnItalic"
        Me.btnItalic.Size = New System.Drawing.Size(23, 22)
        Me.btnItalic.Text = "I"
        Me.btnItalic.ToolTipText = "Italics"
        '
        'btnUnderline
        '
        Me.btnUnderline.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnUnderline.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUnderline.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnUnderline.Name = "btnUnderline"
        Me.btnUnderline.Size = New System.Drawing.Size(23, 22)
        Me.btnUnderline.Text = "U"
        Me.btnUnderline.ToolTipText = "Underline"
        '
        'btnStrike
        '
        Me.btnStrike.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnStrike.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Strikeout, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStrike.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnStrike.Name = "btnStrike"
        Me.btnStrike.Size = New System.Drawing.Size(23, 22)
        Me.btnStrike.Text = "S"
        Me.btnStrike.ToolTipText = "Strikethrough"
        '
        'btnFont
        '
        Me.btnFont.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnFont.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFont.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnFont.Name = "btnFont"
        Me.btnFont.Size = New System.Drawing.Size(23, 22)
        Me.btnFont.Text = "F"
        Me.btnFont.ToolTipText = "Font"
        '
        'btnParagraph
        '
        Me.btnParagraph.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.btnParagraph.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnParagraph.Name = "btnParagraph"
        Me.btnParagraph.Size = New System.Drawing.Size(23, 22)
        Me.btnParagraph.Text = "P"
        Me.btnParagraph.ToolTipText = "Paragraph"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(6, 25)
        '
        'btnLink
        '
        Me.btnLink.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnLink.Image = CType(resources.GetObject("btnLink.Image"), System.Drawing.Image)
        Me.btnLink.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnLink.Name = "btnLink"
        Me.btnLink.Size = New System.Drawing.Size(23, 22)
        Me.btnLink.Text = "ToolStripButton5"
        Me.btnLink.ToolTipText = "hyperlink"
        '
        'btnImage
        '
        Me.btnImage.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnImage.Image = CType(resources.GetObject("btnImage.Image"), System.Drawing.Image)
        Me.btnImage.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImage.Name = "btnImage"
        Me.btnImage.Size = New System.Drawing.Size(23, 22)
        Me.btnImage.Text = "ToolStripButton6"
        Me.btnImage.ToolTipText = "Insert image link"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(6, 25)
        '
        'btnPreview
        '
        Me.btnPreview.CheckOnClick = True
        Me.btnPreview.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnPreview.Image = CType(resources.GetObject("btnPreview.Image"), System.Drawing.Image)
        Me.btnPreview.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(23, 22)
        Me.btnPreview.Text = "ToolStripButton7"
        Me.btnPreview.ToolTipText = "Preview HTML"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.htmlBox)
        Me.Panel1.Controls.Add(Me.txtBody)
        Me.Panel1.Controls.Add(Me.ToolStrip1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1410, 505)
        Me.Panel1.TabIndex = 1
        '
        'htmlBox
        '
        Me.htmlBox.AutoScrollMargin = New System.Drawing.Size(0, 0)
        Me.htmlBox.AutoScrollMinSize = New System.Drawing.Size(0, 0)
        Me.htmlBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.htmlBox.BackgroundImagePath = ""
        Me.htmlBox.BaseUrl = ""
        Me.htmlBox.BodyColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.htmlBox.BodyHtml = Nothing
        Me.htmlBox.BodyStyle = Nothing
        Me.htmlBox.Charset = "unicode"
        Me.htmlBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.htmlBox.DefaultForeColor = System.Drawing.Color.Black
        Me.htmlBox.DocumentHtml = resources.GetString("htmlBox.DocumentHtml")
        Me.htmlBox.DocumentTitle = ""
        Me.htmlBox.EditorContextMenuStrip = Nothing
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_New", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "New", CType(resources.GetObject("htmlBox.FactoryToolbarItems"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "New", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Open", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Open", CType(resources.GetObject("htmlBox.FactoryToolbarItems1"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Open", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Save", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Save", CType(resources.GetObject("htmlBox.FactoryToolbarItems2"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Save", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator1", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Name", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Font Name", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Verdana", 8.0!), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(160, 24)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Size", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Font Size", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator2", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Cut", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Cut", CType(resources.GetObject("htmlBox.FactoryToolbarItems3"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Cut", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Copy", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Copy", CType(resources.GetObject("htmlBox.FactoryToolbarItems4"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Copy", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Paste", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Paste", CType(resources.GetObject("htmlBox.FactoryToolbarItems5"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Paste", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator3", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Bold", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Bold", CType(resources.GetObject("htmlBox.FactoryToolbarItems6"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Bold", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Italic", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Italic", CType(resources.GetObject("htmlBox.FactoryToolbarItems7"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Italic", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Underline", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Underline", CType(resources.GetObject("htmlBox.FactoryToolbarItems8"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Underline", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Horizontal Rule", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Horizontal Rule", CType(resources.GetObject("htmlBox.FactoryToolbarItems9"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Horizontal Rule", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Format Reset", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Format Reset", CType(resources.GetObject("htmlBox.FactoryToolbarItems10"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Format Reset", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Undo", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Undo", CType(resources.GetObject("htmlBox.FactoryToolbarItems11"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Undo", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Redo", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Redo", CType(resources.GetObject("htmlBox.FactoryToolbarItems12"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Redo", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Edit Source", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Edit Source", CType(resources.GetObject("htmlBox.FactoryToolbarItems13"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Edit Source", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Spell Check", SpiceLogic.WinHTMLEditor.ToolStripHosts.TopStrip, True, True, True, True, "Spell Check", CType(resources.GetObject("htmlBox.FactoryToolbarItems14"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Spell Check", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Title Insert", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Title Insert", Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(105, 29)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Text Highlight Color", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Text Highlight Color", CType(resources.GetObject("htmlBox.FactoryToolbarItems15"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Text Highlight Color", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Font Color", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Font Color", CType(resources.GetObject("htmlBox.FactoryToolbarItems16"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Font Color", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator4", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_HyperLink", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "HyperLink", CType(resources.GetObject("htmlBox.FactoryToolbarItems17"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "HyperLink", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Image", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Image", CType(resources.GetObject("htmlBox.FactoryToolbarItems18"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Image", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator5", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Insert Ordered List", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Insert Ordered List", CType(resources.GetObject("htmlBox.FactoryToolbarItems19"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Insert Ordered List", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Insert Unordered List", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Insert Unordered List", CType(resources.GetObject("htmlBox.FactoryToolbarItems20"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Insert Unordered List", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator6", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Left", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Left", CType(resources.GetObject("htmlBox.FactoryToolbarItems21"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Left", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Center", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Center", CType(resources.GetObject("htmlBox.FactoryToolbarItems22"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Center", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Right", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Right", CType(resources.GetObject("htmlBox.FactoryToolbarItems23"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Right", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(26, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator7", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Table", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Table", CType(resources.GetObject("htmlBox.FactoryToolbarItems24"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Table", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator8", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Outdent", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Outdent", CType(resources.GetObject("htmlBox.FactoryToolbarItems25"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Outdent", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Indent", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Indent", CType(resources.GetObject("htmlBox.FactoryToolbarItems26"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Indent", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Separator9", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, Nothing, Nothing, System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.None, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.SizeToFit, System.Drawing.Color.Empty, System.Windows.Forms.RightToLeft.No, Nothing, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(0, 0)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Strike through", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Strike through", CType(resources.GetObject("htmlBox.FactoryToolbarItems27"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Strike through", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(24, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_SuperScript", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "SuperScript", CType(resources.GetObject("htmlBox.FactoryToolbarItems28"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "SuperScript", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Subscript", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Subscript", CType(resources.GetObject("htmlBox.FactoryToolbarItems29"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Subscript", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(27, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Body Style", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Body Style", CType(resources.GetObject("htmlBox.FactoryToolbarItems30"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Body Style", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(34, 26)))
        Me.htmlBox.FactoryToolbarItems.Add(New SpiceLogic.WinHTMLEditor.FactoryItem("FACTORY_Print", SpiceLogic.WinHTMLEditor.ToolStripHosts.BottomStrip, True, True, True, True, "Print", CType(resources.GetObject("htmlBox.FactoryToolbarItems31"), System.Drawing.Image), System.Drawing.Color.Empty, Nothing, System.Windows.Forms.ImageLayout.None, System.Windows.Forms.ToolStripItemDisplayStyle.Image, New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte)), System.Drawing.Color.Black, System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripItemImageScaling.None, System.Drawing.Color.Magenta, System.Windows.Forms.RightToLeft.No, "Print", System.Drawing.ContentAlignment.MiddleCenter, System.Windows.Forms.ToolStripTextDirection.Horizontal, System.Windows.Forms.TextImageRelation.ImageBeforeText, System.Windows.Forms.ToolStripItemAlignment.Left, New System.Drawing.Size(23, 26)))
        Me.htmlBox.LicenseKey = "85CE-3CBE-0E7B-3CF2-0285-AF1E-96FA-3CAE"
        Me.htmlBox.Location = New System.Drawing.Point(31, 270)
        Me.htmlBox.Name = "htmlBox"
        Me.htmlBox.SacrificeOptimizationForLongHtml = False
        Me.htmlBox.ScrollBars = SpiceLogic.WinHTMLEditor.ScrollBarVisibility.[Default]
        Me.htmlBox.Size = New System.Drawing.Size(635, 182)
        '
        '
        '
        Me.htmlBox.SpellCheckDictionary.DictionaryFile = "en-US.dic"
        '
        '
        '
        Me.htmlBox.SpellChecker.Dictionary = Me.htmlBox.SpellCheckDictionary
        Me.htmlBox.TabIndex = 2
        '
        'htmlBox.HtmlEditorToolbar1
        '
        Me.htmlBox.Toolbar1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripButton1})
        Me.htmlBox.Toolbar1.Location = New System.Drawing.Point(0, 0)
        Me.htmlBox.Toolbar1.Name = "HtmlEditorToolbar1"
        Me.htmlBox.Toolbar1.Size = New System.Drawing.Size(635, 27)
        Me.htmlBox.Toolbar1.TabIndex = 0
        Me.htmlBox.Toolbar1.Text = "toolStrip1"
        '
        'htmlBox.HtmlEditorToolbar2
        '
        Me.htmlBox.Toolbar2.Location = New System.Drawing.Point(0, 27)
        Me.htmlBox.Toolbar2.Name = "HtmlEditorToolbar2"
        Me.htmlBox.Toolbar2.Size = New System.Drawing.Size(635, 29)
        Me.htmlBox.Toolbar2.TabIndex = 1
        Me.htmlBox.Toolbar2.Text = "toolStrip2"
        Me.htmlBox.ToolbarContextMenuStrip = Nothing
        Me.htmlBox.ToolbarCursor = System.Windows.Forms.Cursors.Default
        '
        'txtBody
        '
        '
        '
        '
        Me.txtBody.Border.Class = "TextBoxBorder"
        Me.txtBody.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBody.Location = New System.Drawing.Point(105, 39)
        Me.txtBody.Multiline = True
        Me.txtBody.Name = "txtBody"
        Me.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBody.Size = New System.Drawing.Size(273, 225)
        Me.txtBody.TabIndex = 1
        '
        'ToolStripButton1
        '
        Me.ToolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripButton1.Image = CType(resources.GetObject("ToolStripButton1.Image"), System.Drawing.Image)
        Me.ToolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripButton1.Name = "ToolStripButton1"
        Me.ToolStripButton1.Size = New System.Drawing.Size(23, 24)
        Me.ToolStripButton1.Text = "HTML"
        '
        'ucEditorX
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucEditorX"
        Me.Size = New System.Drawing.Size(1410, 505)
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.htmlBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.htmlBox.ResumeLayout(False)
        Me.htmlBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents btnBold As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnItalic As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnUnderline As System.Windows.Forms.ToolStripButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnStrike As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnLink As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImage As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnPreview As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtBody As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnParagraph As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnFont As System.Windows.Forms.ToolStripButton
    Friend WithEvents htmlBox As SpiceLogic.WinHTMLEditor.HTMLEditor
    Friend WithEvents ToolStripButton1 As System.Windows.Forms.ToolStripButton

End Class

