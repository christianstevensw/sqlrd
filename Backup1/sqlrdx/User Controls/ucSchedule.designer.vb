﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucSchedule
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.stabFrequencyOptions = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtDailyRepeat = New System.Windows.Forms.NumericUpDown()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.tabDaily = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkWednesday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkTuesday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSaturday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFriday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSunday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkThursday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMonday = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtRepeatWeekly = New System.Windows.Forms.NumericUpDown()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.tabWeekly = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabWeekDays = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabYearly = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.monthlyOption1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.cmbMonthDayNumber = New System.Windows.Forms.ComboBox()
        Me.cmbMonthDayName = New System.Windows.Forms.ComboBox()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.monthlyOptions2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.chkJanuary = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFebruary = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMarch = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkApril = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMay = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkJune = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSeptember = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAugust = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkJuly = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkOctober = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkNovember = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkDecember = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkUseMonthlyOptions = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.tabMonthly = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.txtNthWorkingDay = New DevComponents.Editors.IntegerInput()
        Me.optLastWorkingDay = New System.Windows.Forms.RadioButton()
        Me.optNthWorkingDay = New System.Windows.Forms.RadioButton()
        Me.LabelX13 = New DevComponents.DotNetBar.LabelX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabNone = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.cmbCustomCalendarName = New System.Windows.Forms.ComboBox()
        Me.tabCalendar = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel()
        Me.LabelX11 = New DevComponents.DotNetBar.LabelX()
        Me.txtOtherFrequency = New System.Windows.Forms.NumericUpDown()
        Me.cmbOtherUnit = New System.Windows.Forms.ComboBox()
        Me.tabOther = New DevComponents.DotNetBar.SuperTabItem()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.chkEndDate = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.dtStartDate = New System.Windows.Forms.DateTimePicker()
        Me.dtEndDate = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkUseExceptionCalendar = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmbExceptionCalendar = New System.Windows.Forms.ComboBox()
        Me.dtScheduleTime = New System.Windows.Forms.DateTimePicker()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.chkRepeat = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtRepeatInterval = New System.Windows.Forms.NumericUpDown()
        Me.cmbRepeatUnit = New System.Windows.Forms.ComboBox()
        Me.dtRepeatUntil = New System.Windows.Forms.DateTimePicker()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.dtNextRunTime = New System.Windows.Forms.DateTimePicker()
        Me.dtNextRunDate = New System.Windows.Forms.DateTimePicker()
        Me.chkstatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmbCollaboration = New System.Windows.Forms.ComboBox()
        Me.LabelX12 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.stabFrequencyOptions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabFrequencyOptions.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        CType(Me.txtDailyRepeat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.txtRepeatWeekly, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.monthlyOption1.SuspendLayout()
        Me.monthlyOptions2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        CType(Me.txtNthWorkingDay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.FlowLayoutPanel6.SuspendLayout()
        CType(Me.txtOtherFrequency, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.txtRepeatInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.FlowLayoutPanel8.SuspendLayout()
        Me.FlowLayoutPanel9.SuspendLayout()
        Me.FlowLayoutPanel10.SuspendLayout()
        Me.SuspendLayout()
        '
        'stabFrequencyOptions
        '
        '
        '
        '
        '
        '
        '
        Me.stabFrequencyOptions.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabFrequencyOptions.ControlBox.MenuBox.Name = ""
        Me.stabFrequencyOptions.ControlBox.Name = ""
        Me.stabFrequencyOptions.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabFrequencyOptions.ControlBox.MenuBox, Me.stabFrequencyOptions.ControlBox.CloseBox})
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel9)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabFrequencyOptions.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabFrequencyOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabFrequencyOptions.Location = New System.Drawing.Point(0, 0)
        Me.stabFrequencyOptions.Name = "stabFrequencyOptions"
        Me.stabFrequencyOptions.ReorderTabsEnabled = True
        Me.stabFrequencyOptions.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabFrequencyOptions.SelectedTabIndex = 0
        Me.stabFrequencyOptions.Size = New System.Drawing.Size(574, 157)
        Me.stabFrequencyOptions.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabFrequencyOptions.TabIndex = 1
        Me.stabFrequencyOptions.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabDaily, Me.tabWeekly, Me.tabWeekDays, Me.SuperTabItem1, Me.tabMonthly, Me.tabYearly, Me.tabCalendar, Me.tabOther, Me.tabNone})
        Me.stabFrequencyOptions.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabFrequencyOptions.Text = "Working Day"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 23)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(574, 134)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabDaily
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.FlowLayoutPanel2)
        Me.GroupBox1.Location = New System.Drawing.Point(4, 3)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox1.Size = New System.Drawing.Size(566, 48)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.LabelX3)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtDailyRepeat)
        Me.FlowLayoutPanel2.Controls.Add(Me.LabelX5)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(13, 13)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(362, 30)
        Me.FlowLayoutPanel2.TabIndex = 1
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelX3.Location = New System.Drawing.Point(3, 2)
        Me.LabelX3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(75, 21)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Repeat every "
        '
        'txtDailyRepeat
        '
        Me.txtDailyRepeat.Location = New System.Drawing.Point(84, 2)
        Me.txtDailyRepeat.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtDailyRepeat.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtDailyRepeat.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtDailyRepeat.Name = "txtDailyRepeat"
        Me.txtDailyRepeat.Size = New System.Drawing.Size(58, 21)
        Me.txtDailyRepeat.TabIndex = 1
        Me.txtDailyRepeat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDailyRepeat.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelX5.Location = New System.Drawing.Point(148, 2)
        Me.LabelX5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(75, 21)
        Me.LabelX5.TabIndex = 2
        Me.LabelX5.Text = " days"
        '
        'tabDaily
        '
        Me.tabDaily.AttachedControl = Me.SuperTabControlPanel1
        Me.tabDaily.GlobalItem = False
        Me.tabDaily.Name = "tabDaily"
        Me.tabDaily.Text = "Daily"
        Me.tabDaily.Tooltip = "Daily"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabWeekly
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.FlowLayoutPanel3)
        Me.GroupBox2.Location = New System.Drawing.Point(4, 3)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox2.Size = New System.Drawing.Size(566, 127)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkWednesday)
        Me.GroupBox3.Controls.Add(Me.chkTuesday)
        Me.GroupBox3.Controls.Add(Me.chkSaturday)
        Me.GroupBox3.Controls.Add(Me.chkFriday)
        Me.GroupBox3.Controls.Add(Me.chkSunday)
        Me.GroupBox3.Controls.Add(Me.chkThursday)
        Me.GroupBox3.Controls.Add(Me.chkMonday)
        Me.GroupBox3.Location = New System.Drawing.Point(10, 40)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.GroupBox3.Size = New System.Drawing.Size(547, 75)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "on"
        '
        'chkWednesday
        '
        '
        '
        '
        Me.chkWednesday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkWednesday.Checked = True
        Me.chkWednesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWednesday.CheckValue = "Y"
        Me.chkWednesday.Location = New System.Drawing.Point(191, 18)
        Me.chkWednesday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(92, 19)
        Me.chkWednesday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkWednesday.TabIndex = 0
        Me.chkWednesday.Text = "Wednesday"
        '
        'chkTuesday
        '
        '
        '
        '
        Me.chkTuesday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkTuesday.Checked = True
        Me.chkTuesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTuesday.CheckValue = "Y"
        Me.chkTuesday.Location = New System.Drawing.Point(90, 18)
        Me.chkTuesday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(95, 19)
        Me.chkTuesday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkTuesday.TabIndex = 0
        Me.chkTuesday.Text = "Tuesday"
        '
        'chkSaturday
        '
        '
        '
        '
        Me.chkSaturday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSaturday.Checked = True
        Me.chkSaturday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSaturday.CheckValue = "Y"
        Me.chkSaturday.Location = New System.Drawing.Point(6, 41)
        Me.chkSaturday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkSaturday.Name = "chkSaturday"
        Me.chkSaturday.Size = New System.Drawing.Size(73, 19)
        Me.chkSaturday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSaturday.TabIndex = 0
        Me.chkSaturday.Text = "Saturday"
        '
        'chkFriday
        '
        '
        '
        '
        Me.chkFriday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFriday.Checked = True
        Me.chkFriday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFriday.CheckValue = "Y"
        Me.chkFriday.Location = New System.Drawing.Point(396, 18)
        Me.chkFriday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Size = New System.Drawing.Size(100, 19)
        Me.chkFriday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkFriday.TabIndex = 0
        Me.chkFriday.Text = "Friday"
        '
        'chkSunday
        '
        '
        '
        '
        Me.chkSunday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSunday.Checked = True
        Me.chkSunday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSunday.CheckValue = "Y"
        Me.chkSunday.Location = New System.Drawing.Point(90, 41)
        Me.chkSunday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkSunday.Name = "chkSunday"
        Me.chkSunday.Size = New System.Drawing.Size(100, 19)
        Me.chkSunday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSunday.TabIndex = 0
        Me.chkSunday.Text = "Sunday"
        '
        'chkThursday
        '
        '
        '
        '
        Me.chkThursday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkThursday.Checked = True
        Me.chkThursday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkThursday.CheckValue = "Y"
        Me.chkThursday.Location = New System.Drawing.Point(290, 18)
        Me.chkThursday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Size = New System.Drawing.Size(100, 19)
        Me.chkThursday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkThursday.TabIndex = 0
        Me.chkThursday.Text = "Thursday"
        '
        'chkMonday
        '
        '
        '
        '
        Me.chkMonday.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMonday.Checked = True
        Me.chkMonday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMonday.CheckValue = "Y"
        Me.chkMonday.Location = New System.Drawing.Point(6, 18)
        Me.chkMonday.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Size = New System.Drawing.Size(78, 19)
        Me.chkMonday.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkMonday.TabIndex = 0
        Me.chkMonday.Text = "Monday"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX6)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtRepeatWeekly)
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX7)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(13, 15)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(362, 22)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'LabelX6
        '
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelX6.Location = New System.Drawing.Point(3, 2)
        Me.LabelX6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(75, 21)
        Me.LabelX6.TabIndex = 0
        Me.LabelX6.Text = "Repeat every "
        '
        'txtRepeatWeekly
        '
        Me.txtRepeatWeekly.Location = New System.Drawing.Point(84, 2)
        Me.txtRepeatWeekly.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtRepeatWeekly.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtRepeatWeekly.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtRepeatWeekly.Name = "txtRepeatWeekly"
        Me.txtRepeatWeekly.Size = New System.Drawing.Size(58, 21)
        Me.txtRepeatWeekly.TabIndex = 1
        Me.txtRepeatWeekly.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRepeatWeekly.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LabelX7
        '
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LabelX7.Location = New System.Drawing.Point(148, 2)
        Me.LabelX7.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(75, 21)
        Me.LabelX7.TabIndex = 2
        Me.LabelX7.Text = "weeks"
        '
        'tabWeekly
        '
        Me.tabWeekly.AttachedControl = Me.SuperTabControlPanel2
        Me.tabWeekly.GlobalItem = False
        Me.tabWeekly.Name = "tabWeekly"
        Me.tabWeekly.Text = "Weekly"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabWeekDays
        '
        'tabWeekDays
        '
        Me.tabWeekDays.AttachedControl = Me.SuperTabControlPanel3
        Me.tabWeekDays.GlobalItem = False
        Me.tabWeekDays.Name = "tabWeekDays"
        Me.tabWeekDays.Text = "Week Days"
        Me.tabWeekDays.Tooltip = "Week Days"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabYearly
        '
        'tabYearly
        '
        Me.tabYearly.AttachedControl = Me.SuperTabControlPanel5
        Me.tabYearly.GlobalItem = False
        Me.tabYearly.Name = "tabYearly"
        Me.tabYearly.Text = "Annual"
        Me.tabYearly.Tooltip = "Annual"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.monthlyOption1)
        Me.SuperTabControlPanel4.Controls.Add(Me.monthlyOptions2)
        Me.SuperTabControlPanel4.Controls.Add(Me.chkUseMonthlyOptions)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabMonthly
        '
        'monthlyOption1
        '
        Me.monthlyOption1.BackColor = System.Drawing.Color.Transparent
        Me.monthlyOption1.Controls.Add(Me.LabelX8)
        Me.monthlyOption1.Controls.Add(Me.cmbMonthDayNumber)
        Me.monthlyOption1.Controls.Add(Me.cmbMonthDayName)
        Me.monthlyOption1.Controls.Add(Me.LabelX9)
        Me.monthlyOption1.Enabled = False
        Me.monthlyOption1.Location = New System.Drawing.Point(4, 23)
        Me.monthlyOption1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.monthlyOption1.Name = "monthlyOption1"
        Me.monthlyOption1.Size = New System.Drawing.Size(566, 25)
        Me.monthlyOption1.TabIndex = 2
        '
        'LabelX8
        '
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(3, 2)
        Me.LabelX8.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(30, 19)
        Me.LabelX8.TabIndex = 0
        Me.LabelX8.Text = "The "
        '
        'cmbMonthDayNumber
        '
        Me.cmbMonthDayNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonthDayNumber.FormattingEnabled = True
        Me.cmbMonthDayNumber.Items.AddRange(New Object() {"First", "Second", "Third", "Fourth", "Last"})
        Me.cmbMonthDayNumber.Location = New System.Drawing.Point(39, 2)
        Me.cmbMonthDayNumber.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbMonthDayNumber.Name = "cmbMonthDayNumber"
        Me.cmbMonthDayNumber.Size = New System.Drawing.Size(121, 21)
        Me.cmbMonthDayNumber.TabIndex = 1
        '
        'cmbMonthDayName
        '
        Me.cmbMonthDayName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMonthDayName.FormattingEnabled = True
        Me.cmbMonthDayName.Items.AddRange(New Object() {"Day", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"})
        Me.cmbMonthDayName.Location = New System.Drawing.Point(166, 2)
        Me.cmbMonthDayName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbMonthDayName.Name = "cmbMonthDayName"
        Me.cmbMonthDayName.Size = New System.Drawing.Size(121, 21)
        Me.cmbMonthDayName.TabIndex = 2
        Me.cmbMonthDayName.Tag = "nosort"
        '
        'LabelX9
        '
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(293, 2)
        Me.LabelX9.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(129, 19)
        Me.LabelX9.TabIndex = 3
        Me.LabelX9.Text = " of  the  month"
        '
        'monthlyOptions2
        '
        Me.monthlyOptions2.BackColor = System.Drawing.Color.Transparent
        Me.monthlyOptions2.Controls.Add(Me.TableLayoutPanel1)
        Me.monthlyOptions2.Enabled = False
        Me.monthlyOptions2.Location = New System.Drawing.Point(4, 50)
        Me.monthlyOptions2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.monthlyOptions2.Name = "monthlyOptions2"
        Me.monthlyOptions2.Padding = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.monthlyOptions2.Size = New System.Drawing.Size(566, 89)
        Me.monthlyOptions2.TabIndex = 1
        Me.monthlyOptions2.TabStop = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813!))
        Me.TableLayoutPanel1.Controls.Add(Me.chkJanuary, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkFebruary, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMarch, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkApril, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkMay, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkJune, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkSeptember, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.chkAugust, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkJuly, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkOctober, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.chkNovember, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkDecember, 3, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 15)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(554, 68)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'chkJanuary
        '
        '
        '
        '
        Me.chkJanuary.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJanuary.Checked = True
        Me.chkJanuary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkJanuary.CheckValue = "Y"
        Me.chkJanuary.Location = New System.Drawing.Point(3, 2)
        Me.chkJanuary.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkJanuary.Name = "chkJanuary"
        Me.chkJanuary.Size = New System.Drawing.Size(100, 17)
        Me.chkJanuary.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkJanuary.TabIndex = 0
        Me.chkJanuary.Text = "January"
        '
        'chkFebruary
        '
        '
        '
        '
        Me.chkFebruary.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFebruary.Checked = True
        Me.chkFebruary.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFebruary.CheckValue = "Y"
        Me.chkFebruary.Location = New System.Drawing.Point(3, 24)
        Me.chkFebruary.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkFebruary.Name = "chkFebruary"
        Me.chkFebruary.Size = New System.Drawing.Size(100, 17)
        Me.chkFebruary.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkFebruary.TabIndex = 0
        Me.chkFebruary.Text = "February"
        '
        'chkMarch
        '
        '
        '
        '
        Me.chkMarch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMarch.Checked = True
        Me.chkMarch.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMarch.CheckValue = "Y"
        Me.chkMarch.Location = New System.Drawing.Point(3, 46)
        Me.chkMarch.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMarch.Name = "chkMarch"
        Me.chkMarch.Size = New System.Drawing.Size(100, 17)
        Me.chkMarch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkMarch.TabIndex = 0
        Me.chkMarch.Text = "March"
        '
        'chkApril
        '
        '
        '
        '
        Me.chkApril.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkApril.Checked = True
        Me.chkApril.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkApril.CheckValue = "Y"
        Me.chkApril.Location = New System.Drawing.Point(141, 2)
        Me.chkApril.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkApril.Name = "chkApril"
        Me.chkApril.Size = New System.Drawing.Size(100, 17)
        Me.chkApril.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkApril.TabIndex = 0
        Me.chkApril.Text = "April"
        '
        'chkMay
        '
        '
        '
        '
        Me.chkMay.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMay.Checked = True
        Me.chkMay.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMay.CheckValue = "Y"
        Me.chkMay.Location = New System.Drawing.Point(141, 24)
        Me.chkMay.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkMay.Name = "chkMay"
        Me.chkMay.Size = New System.Drawing.Size(100, 17)
        Me.chkMay.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkMay.TabIndex = 0
        Me.chkMay.Text = "May"
        '
        'chkJune
        '
        '
        '
        '
        Me.chkJune.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJune.Checked = True
        Me.chkJune.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkJune.CheckValue = "Y"
        Me.chkJune.Location = New System.Drawing.Point(141, 46)
        Me.chkJune.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkJune.Name = "chkJune"
        Me.chkJune.Size = New System.Drawing.Size(100, 17)
        Me.chkJune.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkJune.TabIndex = 0
        Me.chkJune.Text = "June"
        '
        'chkSeptember
        '
        '
        '
        '
        Me.chkSeptember.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSeptember.Checked = True
        Me.chkSeptember.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSeptember.CheckValue = "Y"
        Me.chkSeptember.Location = New System.Drawing.Point(279, 46)
        Me.chkSeptember.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkSeptember.Name = "chkSeptember"
        Me.chkSeptember.Size = New System.Drawing.Size(100, 17)
        Me.chkSeptember.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkSeptember.TabIndex = 0
        Me.chkSeptember.Text = "September"
        '
        'chkAugust
        '
        '
        '
        '
        Me.chkAugust.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAugust.Checked = True
        Me.chkAugust.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAugust.CheckValue = "Y"
        Me.chkAugust.Location = New System.Drawing.Point(279, 24)
        Me.chkAugust.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkAugust.Name = "chkAugust"
        Me.chkAugust.Size = New System.Drawing.Size(100, 17)
        Me.chkAugust.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAugust.TabIndex = 0
        Me.chkAugust.Text = "August"
        '
        'chkJuly
        '
        '
        '
        '
        Me.chkJuly.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJuly.Checked = True
        Me.chkJuly.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkJuly.CheckValue = "Y"
        Me.chkJuly.Location = New System.Drawing.Point(279, 2)
        Me.chkJuly.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkJuly.Name = "chkJuly"
        Me.chkJuly.Size = New System.Drawing.Size(100, 17)
        Me.chkJuly.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkJuly.TabIndex = 0
        Me.chkJuly.Text = "July"
        '
        'chkOctober
        '
        '
        '
        '
        Me.chkOctober.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOctober.Checked = True
        Me.chkOctober.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkOctober.CheckValue = "Y"
        Me.chkOctober.Location = New System.Drawing.Point(417, 2)
        Me.chkOctober.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkOctober.Name = "chkOctober"
        Me.chkOctober.Size = New System.Drawing.Size(100, 17)
        Me.chkOctober.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkOctober.TabIndex = 0
        Me.chkOctober.Text = "October"
        '
        'chkNovember
        '
        '
        '
        '
        Me.chkNovember.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNovember.Checked = True
        Me.chkNovember.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNovember.CheckValue = "Y"
        Me.chkNovember.Location = New System.Drawing.Point(417, 24)
        Me.chkNovember.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkNovember.Name = "chkNovember"
        Me.chkNovember.Size = New System.Drawing.Size(100, 17)
        Me.chkNovember.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkNovember.TabIndex = 0
        Me.chkNovember.Text = "November"
        '
        'chkDecember
        '
        '
        '
        '
        Me.chkDecember.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDecember.Checked = True
        Me.chkDecember.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDecember.CheckValue = "Y"
        Me.chkDecember.Location = New System.Drawing.Point(417, 46)
        Me.chkDecember.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkDecember.Name = "chkDecember"
        Me.chkDecember.Size = New System.Drawing.Size(100, 17)
        Me.chkDecember.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkDecember.TabIndex = 0
        Me.chkDecember.Text = "December"
        '
        'chkUseMonthlyOptions
        '
        Me.chkUseMonthlyOptions.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkUseMonthlyOptions.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUseMonthlyOptions.Location = New System.Drawing.Point(4, 3)
        Me.chkUseMonthlyOptions.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkUseMonthlyOptions.Name = "chkUseMonthlyOptions"
        Me.chkUseMonthlyOptions.Size = New System.Drawing.Size(234, 19)
        Me.chkUseMonthlyOptions.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkUseMonthlyOptions.TabIndex = 0
        Me.chkUseMonthlyOptions.Text = "Use the following options"
        '
        'tabMonthly
        '
        Me.tabMonthly.AttachedControl = Me.SuperTabControlPanel4
        Me.tabMonthly.GlobalItem = False
        Me.tabMonthly.Name = "tabMonthly"
        Me.tabMonthly.Text = "Monthly"
        Me.tabMonthly.Tooltip = "Monthly"
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.txtNthWorkingDay)
        Me.SuperTabControlPanel9.Controls.Add(Me.optLastWorkingDay)
        Me.SuperTabControlPanel9.Controls.Add(Me.optNthWorkingDay)
        Me.SuperTabControlPanel9.Controls.Add(Me.LabelX13)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.SuperTabItem1
        '
        'txtNthWorkingDay
        '
        '
        '
        '
        Me.txtNthWorkingDay.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtNthWorkingDay.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNthWorkingDay.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtNthWorkingDay.Location = New System.Drawing.Point(71, 45)
        Me.txtNthWorkingDay.MinValue = 1
        Me.txtNthWorkingDay.Name = "txtNthWorkingDay"
        Me.txtNthWorkingDay.ShowUpDown = True
        Me.txtNthWorkingDay.Size = New System.Drawing.Size(72, 21)
        Me.txtNthWorkingDay.TabIndex = 2
        Me.txtNthWorkingDay.Value = 1
        '
        'optLastWorkingDay
        '
        Me.optLastWorkingDay.AutoSize = True
        Me.optLastWorkingDay.BackColor = System.Drawing.Color.Transparent
        Me.optLastWorkingDay.Location = New System.Drawing.Point(157, 47)
        Me.optLastWorkingDay.Name = "optLastWorkingDay"
        Me.optLastWorkingDay.Size = New System.Drawing.Size(45, 17)
        Me.optLastWorkingDay.TabIndex = 1
        Me.optLastWorkingDay.Text = "Last"
        Me.optLastWorkingDay.UseVisualStyleBackColor = False
        '
        'optNthWorkingDay
        '
        Me.optNthWorkingDay.AutoSize = True
        Me.optNthWorkingDay.BackColor = System.Drawing.Color.Transparent
        Me.optNthWorkingDay.Checked = True
        Me.optNthWorkingDay.Location = New System.Drawing.Point(24, 47)
        Me.optNthWorkingDay.Name = "optNthWorkingDay"
        Me.optNthWorkingDay.Size = New System.Drawing.Size(41, 17)
        Me.optNthWorkingDay.TabIndex = 1
        Me.optNthWorkingDay.TabStop = True
        Me.optNthWorkingDay.Text = "nth"
        Me.optNthWorkingDay.UseVisualStyleBackColor = False
        '
        'LabelX13
        '
        Me.LabelX13.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX13.Location = New System.Drawing.Point(7, 12)
        Me.LabelX13.Name = "LabelX13"
        Me.LabelX13.Size = New System.Drawing.Size(284, 23)
        Me.LabelX13.TabIndex = 0
        Me.LabelX13.Text = "Specify the 'nth' or 'last' working day of the month"
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel9
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "Working Day"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabNone
        '
        'tabNone
        '
        Me.tabNone.AttachedControl = Me.SuperTabControlPanel8
        Me.tabNone.GlobalItem = False
        Me.tabNone.Name = "tabNone"
        Me.tabNone.Text = "None"
        Me.tabNone.Tooltip = "None"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.FlowLayoutPanel5)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabCalendar
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel5.Controls.Add(Me.LabelX10)
        Me.FlowLayoutPanel5.Controls.Add(Me.cmbCustomCalendarName)
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(4, 3)
        Me.FlowLayoutPanel5.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(566, 27)
        Me.FlowLayoutPanel5.TabIndex = 0
        '
        'LabelX10
        '
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Location = New System.Drawing.Point(3, 2)
        Me.LabelX10.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(89, 19)
        Me.LabelX10.TabIndex = 0
        Me.LabelX10.Text = "Calendar Name"
        '
        'cmbCustomCalendarName
        '
        Me.cmbCustomCalendarName.FormattingEnabled = True
        Me.cmbCustomCalendarName.Location = New System.Drawing.Point(98, 2)
        Me.cmbCustomCalendarName.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbCustomCalendarName.Name = "cmbCustomCalendarName"
        Me.cmbCustomCalendarName.Size = New System.Drawing.Size(262, 21)
        Me.cmbCustomCalendarName.TabIndex = 1
        '
        'tabCalendar
        '
        Me.tabCalendar.AttachedControl = Me.SuperTabControlPanel6
        Me.tabCalendar.GlobalItem = False
        Me.tabCalendar.Name = "tabCalendar"
        Me.tabCalendar.Text = "Custom Calendar"
        Me.tabCalendar.Tooltip = "Custom Calendar"
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.FlowLayoutPanel6)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(574, 157)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabOther
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel6.Controls.Add(Me.LabelX11)
        Me.FlowLayoutPanel6.Controls.Add(Me.txtOtherFrequency)
        Me.FlowLayoutPanel6.Controls.Add(Me.cmbOtherUnit)
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(4, 3)
        Me.FlowLayoutPanel6.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(566, 27)
        Me.FlowLayoutPanel6.TabIndex = 0
        '
        'LabelX11
        '
        '
        '
        '
        Me.LabelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX11.Location = New System.Drawing.Point(3, 2)
        Me.LabelX11.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX11.Name = "LabelX11"
        Me.LabelX11.Size = New System.Drawing.Size(112, 19)
        Me.LabelX11.TabIndex = 0
        Me.LabelX11.Text = "Run schedule every "
        '
        'txtOtherFrequency
        '
        Me.txtOtherFrequency.Location = New System.Drawing.Point(121, 2)
        Me.txtOtherFrequency.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtOtherFrequency.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtOtherFrequency.Name = "txtOtherFrequency"
        Me.txtOtherFrequency.Size = New System.Drawing.Size(54, 21)
        Me.txtOtherFrequency.TabIndex = 2
        Me.txtOtherFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOtherFrequency.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbOtherUnit
        '
        Me.cmbOtherUnit.FormattingEnabled = True
        Me.cmbOtherUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months", "Years"})
        Me.cmbOtherUnit.Location = New System.Drawing.Point(181, 2)
        Me.cmbOtherUnit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbOtherUnit.Name = "cmbOtherUnit"
        Me.cmbOtherUnit.Size = New System.Drawing.Size(141, 21)
        Me.cmbOtherUnit.TabIndex = 1
        Me.cmbOtherUnit.Text = "Minutes"
        '
        'tabOther
        '
        Me.tabOther.AttachedControl = Me.SuperTabControlPanel7
        Me.tabOther.GlobalItem = False
        Me.tabOther.Name = "tabOther"
        Me.tabOther.Text = "Other"
        Me.tabOther.Tooltip = "Other"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.02091!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.26132!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.24042!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.82578!))
        Me.TableLayoutPanel2.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.chkEndDate, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.dtStartDate, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.dtEndDate, 3, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(574, 24)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(62, 18)
        Me.LabelX1.TabIndex = 3
        Me.LabelX1.Text = "Start Date"
        '
        'chkEndDate
        '
        '
        '
        '
        Me.chkEndDate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEndDate.Checked = True
        Me.chkEndDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEndDate.CheckValue = "Y"
        Me.chkEndDate.Location = New System.Drawing.Point(215, 2)
        Me.chkEndDate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkEndDate.Name = "chkEndDate"
        Me.chkEndDate.Size = New System.Drawing.Size(69, 19)
        Me.chkEndDate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkEndDate.TabIndex = 5
        Me.chkEndDate.Text = "End Date"
        '
        'dtStartDate
        '
        Me.dtStartDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtStartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtStartDate.Location = New System.Drawing.Point(71, 2)
        Me.dtStartDate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtStartDate.Name = "dtStartDate"
        Me.dtStartDate.Size = New System.Drawing.Size(136, 21)
        Me.dtStartDate.TabIndex = 10
        '
        'dtEndDate
        '
        Me.dtEndDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtEndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtEndDate.Location = New System.Drawing.Point(290, 2)
        Me.dtEndDate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtEndDate.Name = "dtEndDate"
        Me.dtEndDate.Size = New System.Drawing.Size(136, 21)
        Me.dtEndDate.TabIndex = 10
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.stabFrequencyOptions)
        Me.Panel1.Location = New System.Drawing.Point(3, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(574, 157)
        Me.Panel1.TabIndex = 3
        '
        'chkUseExceptionCalendar
        '
        '
        '
        '
        Me.chkUseExceptionCalendar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUseExceptionCalendar.Location = New System.Drawing.Point(205, 3)
        Me.chkUseExceptionCalendar.Name = "chkUseExceptionCalendar"
        Me.chkUseExceptionCalendar.Size = New System.Drawing.Size(129, 23)
        Me.chkUseExceptionCalendar.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkUseExceptionCalendar.TabIndex = 4
        Me.chkUseExceptionCalendar.Text = "Exception calendar"
        '
        'cmbExceptionCalendar
        '
        Me.cmbExceptionCalendar.Enabled = False
        Me.cmbExceptionCalendar.FormattingEnabled = True
        Me.cmbExceptionCalendar.Location = New System.Drawing.Point(340, 3)
        Me.cmbExceptionCalendar.Name = "cmbExceptionCalendar"
        Me.cmbExceptionCalendar.Size = New System.Drawing.Size(133, 21)
        Me.cmbExceptionCalendar.TabIndex = 5
        '
        'dtScheduleTime
        '
        Me.dtScheduleTime.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtScheduleTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtScheduleTime.Location = New System.Drawing.Point(104, 2)
        Me.dtScheduleTime.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtScheduleTime.Name = "dtScheduleTime"
        Me.dtScheduleTime.ShowUpDown = True
        Me.dtScheduleTime.Size = New System.Drawing.Size(95, 21)
        Me.dtScheduleTime.TabIndex = 10
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 2)
        Me.LabelX2.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(95, 25)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Schedule time"
        '
        'chkRepeat
        '
        '
        '
        '
        Me.chkRepeat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRepeat.Location = New System.Drawing.Point(3, 3)
        Me.chkRepeat.Name = "chkRepeat"
        Me.chkRepeat.Size = New System.Drawing.Size(95, 20)
        Me.chkRepeat.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkRepeat.TabIndex = 4
        Me.chkRepeat.Text = "Repeat every"
        '
        'txtRepeatInterval
        '
        Me.txtRepeatInterval.DecimalPlaces = 2
        Me.txtRepeatInterval.Enabled = False
        Me.txtRepeatInterval.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.txtRepeatInterval.Location = New System.Drawing.Point(104, 2)
        Me.txtRepeatInterval.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtRepeatInterval.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.txtRepeatInterval.Minimum = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.txtRepeatInterval.Name = "txtRepeatInterval"
        Me.txtRepeatInterval.Size = New System.Drawing.Size(55, 21)
        Me.txtRepeatInterval.TabIndex = 5
        Me.txtRepeatInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRepeatInterval.Value = New Decimal(New Integer() {25, 0, 0, 131072})
        '
        'cmbRepeatUnit
        '
        Me.cmbRepeatUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbRepeatUnit.Enabled = False
        Me.cmbRepeatUnit.FormattingEnabled = True
        Me.cmbRepeatUnit.Items.AddRange(New Object() {"minutes", "hours"})
        Me.cmbRepeatUnit.Location = New System.Drawing.Point(165, 2)
        Me.cmbRepeatUnit.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmbRepeatUnit.Name = "cmbRepeatUnit"
        Me.cmbRepeatUnit.Size = New System.Drawing.Size(133, 21)
        Me.cmbRepeatUnit.TabIndex = 6
        '
        'dtRepeatUntil
        '
        Me.dtRepeatUntil.Enabled = False
        Me.dtRepeatUntil.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtRepeatUntil.Location = New System.Drawing.Point(364, 2)
        Me.dtRepeatUntil.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtRepeatUntil.Name = "dtRepeatUntil"
        Me.dtRepeatUntil.ShowUpDown = True
        Me.dtRepeatUntil.Size = New System.Drawing.Size(92, 21)
        Me.dtRepeatUntil.TabIndex = 11
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 2)
        Me.LabelX4.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(95, 21)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Next to run on"
        '
        'dtNextRunTime
        '
        Me.dtNextRunTime.Enabled = False
        Me.dtNextRunTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtNextRunTime.Location = New System.Drawing.Point(205, 2)
        Me.dtNextRunTime.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtNextRunTime.Name = "dtNextRunTime"
        Me.dtNextRunTime.ShowUpDown = True
        Me.dtNextRunTime.Size = New System.Drawing.Size(92, 21)
        Me.dtNextRunTime.TabIndex = 10
        '
        'dtNextRunDate
        '
        Me.dtNextRunDate.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtNextRunDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtNextRunDate.Location = New System.Drawing.Point(104, 2)
        Me.dtNextRunDate.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dtNextRunDate.Name = "dtNextRunDate"
        Me.dtNextRunDate.Size = New System.Drawing.Size(95, 21)
        Me.dtNextRunDate.TabIndex = 10
        '
        'chkstatus
        '
        '
        '
        '
        Me.chkstatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkstatus.Location = New System.Drawing.Point(3, 2)
        Me.chkstatus.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.chkstatus.Name = "chkstatus"
        Me.chkstatus.Size = New System.Drawing.Size(331, 22)
        Me.chkstatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkstatus.TabIndex = 8
        Me.chkstatus.Text = "Enable this schedule and execute it on "
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.chkstatus)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmbCollaboration)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(3, 104)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(484, 26)
        Me.FlowLayoutPanel1.TabIndex = 9
        '
        'cmbCollaboration
        '
        Me.cmbCollaboration.FormattingEnabled = True
        Me.cmbCollaboration.Items.AddRange(New Object() {"DEFAULT"})
        Me.cmbCollaboration.Location = New System.Drawing.Point(340, 3)
        Me.cmbCollaboration.Name = "cmbCollaboration"
        Me.cmbCollaboration.Size = New System.Drawing.Size(133, 21)
        Me.cmbCollaboration.TabIndex = 9
        Me.cmbCollaboration.Text = "DEFAULT"
        Me.cmbCollaboration.Visible = False
        '
        'LabelX12
        '
        '
        '
        '
        Me.LabelX12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX12.Location = New System.Drawing.Point(304, 3)
        Me.LabelX12.Name = "LabelX12"
        Me.LabelX12.Size = New System.Drawing.Size(54, 20)
        Me.LabelX12.TabIndex = 7
        Me.LabelX12.Text = "until"
        Me.LabelX12.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.Controls.Add(Me.LabelX2)
        Me.FlowLayoutPanel4.Controls.Add(Me.dtScheduleTime)
        Me.FlowLayoutPanel4.Controls.Add(Me.chkUseExceptionCalendar)
        Me.FlowLayoutPanel4.Controls.Add(Me.cmbExceptionCalendar)
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(484, 28)
        Me.FlowLayoutPanel4.TabIndex = 10
        '
        'FlowLayoutPanel8
        '
        Me.FlowLayoutPanel8.Controls.Add(Me.chkRepeat)
        Me.FlowLayoutPanel8.Controls.Add(Me.txtRepeatInterval)
        Me.FlowLayoutPanel8.Controls.Add(Me.cmbRepeatUnit)
        Me.FlowLayoutPanel8.Controls.Add(Me.LabelX12)
        Me.FlowLayoutPanel8.Controls.Add(Me.dtRepeatUntil)
        Me.FlowLayoutPanel8.Location = New System.Drawing.Point(3, 72)
        Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
        Me.FlowLayoutPanel8.Size = New System.Drawing.Size(484, 26)
        Me.FlowLayoutPanel8.TabIndex = 10
        '
        'FlowLayoutPanel9
        '
        Me.FlowLayoutPanel9.Controls.Add(Me.LabelX4)
        Me.FlowLayoutPanel9.Controls.Add(Me.dtNextRunDate)
        Me.FlowLayoutPanel9.Controls.Add(Me.dtNextRunTime)
        Me.FlowLayoutPanel9.Location = New System.Drawing.Point(3, 37)
        Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
        Me.FlowLayoutPanel9.Size = New System.Drawing.Size(484, 29)
        Me.FlowLayoutPanel9.TabIndex = 10
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.Controls.Add(Me.FlowLayoutPanel4)
        Me.FlowLayoutPanel10.Controls.Add(Me.FlowLayoutPanel9)
        Me.FlowLayoutPanel10.Controls.Add(Me.FlowLayoutPanel8)
        Me.FlowLayoutPanel10.Controls.Add(Me.FlowLayoutPanel1)
        Me.FlowLayoutPanel10.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(3, 194)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(574, 141)
        Me.FlowLayoutPanel10.TabIndex = 11
        '
        'ucSchedule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.FlowLayoutPanel10)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucSchedule"
        Me.Size = New System.Drawing.Size(584, 390)
        CType(Me.stabFrequencyOptions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabFrequencyOptions.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        CType(Me.txtDailyRepeat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.txtRepeatWeekly, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.monthlyOption1.ResumeLayout(False)
        Me.monthlyOptions2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.SuperTabControlPanel9.PerformLayout()
        CType(Me.txtNthWorkingDay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel6.ResumeLayout(False)
        CType(Me.txtOtherFrequency, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtRepeatInterval, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel8.ResumeLayout(False)
        Me.FlowLayoutPanel9.ResumeLayout(False)
        Me.FlowLayoutPanel10.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stabFrequencyOptions As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabNone As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDaily As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabOther As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabCalendar As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabYearly As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabMonthly As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabWeekDays As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabWeekly As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkEndDate As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkUseExceptionCalendar As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbExceptionCalendar As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkRepeat As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtRepeatInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbRepeatUnit As System.Windows.Forms.ComboBox
    Friend WithEvents chkstatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents monthlyOption1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbMonthDayNumber As System.Windows.Forms.ComboBox
    Friend WithEvents cmbMonthDayName As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents monthlyOptions2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkJanuary As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFebruary As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMarch As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkApril As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMay As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkJune As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSeptember As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAugust As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkJuly As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkOctober As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNovember As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkDecember As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkUseMonthlyOptions As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkWednesday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkTuesday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSaturday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFriday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSunday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkThursday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMonday As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtRepeatWeekly As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDailyRepeat As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbCustomCalendarName As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents LabelX11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtOtherFrequency As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbOtherUnit As System.Windows.Forms.ComboBox
    Friend WithEvents dtScheduleTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtRepeatUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtNextRunTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtStartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtEndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtNextRunDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmbCollaboration As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel8 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel9 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel10 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents txtNthWorkingDay As DevComponents.Editors.IntegerInput
    Friend WithEvents optLastWorkingDay As System.Windows.Forms.RadioButton
    Friend WithEvents optNthWorkingDay As System.Windows.Forms.RadioButton
    Friend WithEvents LabelX13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class
