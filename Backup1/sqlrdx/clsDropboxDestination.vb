﻿Imports chilombo.frmMainView
Public Class clsDropboxDestination
    Dim m_destinationID As Integer
    Dim concernedRow As ADODB.Recordset = Nothing

    Sub New(ByVal destinationID As Integer)
        m_destinationID = destinationID
    End Sub
    Private ReadOnly Property m_concernedRow() As ADODB.Recordset
        Get
            If concernedRow Is Nothing Then
                Dim row As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM destinationattr WHERE destinationid = " & m_destinationID)
                concernedRow = row
                Return row
            Else
                Return concernedRow
            End If
        End Get
    End Property

    Public Function putFiles(ByRef errInfo As Exception, exportedFiles As System.Collections.Generic.List(Of String)) As Boolean
        Try
            Dim outputDir As String = clsMarsParser.Parser.ParseString(m_concernedRow.Fields("ftppath").Value)

            Dim da As dropboxAccount = New dropboxAccount(m_concernedRow.Fields("ftpserver").Value)
            Dim token, secret As String

            token = da.token
            secret = da.secret

            Dim db As clsDropbox = New clsDropbox(token, secret)
            Dim err As Exception = Nothing

            For Each s As String In exportedFiles

                If s.EndsWith("\") Then
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(s)
                    Dim dir As String = di.Name
                    Dim moveTo As String = outputDir & dir

                    If IO.Directory.Exists(moveTo) Then
                        Dim di2 As IO.DirectoryInfo = New IO.DirectoryInfo(moveTo)
                        di2.Delete(True)
                    End If

                    di.MoveTo(moveTo)
                    System.Threading.Thread.Sleep(250)
                Else
                    If db.uploadFileToDropbox(IO.Path.GetFileName(s), s, outputDir, err) = False Then
                        Throw err
                    End If

                    System.Threading.Thread.Sleep(250)
                End If
            Next

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            errInfo = ex
            Return False
        End Try
    End Function

    Public Function putFiles(ByRef errInfo As Exception, exportedFiles As System.Collections.Generic.List(Of String), overrideDirectory As String) As Boolean
        Try
            Dim outputDir As String = clsMarsParser.Parser.ParseString(overrideDirectory) ' clsMarsParser.Parser.ParseString(m_concernedRow.Fields("ftppath").Value)

            Dim da As dropboxAccount = New dropboxAccount(m_concernedRow.Fields("ftpserver").Value)
            Dim token, secret As String

            token = da.token
            secret = da.secret

            Dim db As clsDropbox = New clsDropbox(token, secret)
            Dim err As Exception = Nothing

            For Each s As String In exportedFiles

                If s.EndsWith("\") Then
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(s)
                    Dim dir As String = di.Name
                    Dim moveTo As String = outputDir & dir

                    If IO.Directory.Exists(moveTo) Then
                        Dim di2 As IO.DirectoryInfo = New IO.DirectoryInfo(moveTo)
                        di2.Delete(True)
                    End If

                    di.MoveTo(moveTo)
                    System.Threading.Thread.Sleep(250)
                Else
                    If db.uploadFileToDropbox(IO.Path.GetFileName(s), s, outputDir, err) = False Then
                        Throw err
                    End If

                    System.Threading.Thread.Sleep(250)
                End If
            Next

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            errInfo = ex
            Return False
        End Try
    End Function
End Class
