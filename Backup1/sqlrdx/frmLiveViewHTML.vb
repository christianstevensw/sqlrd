﻿Public Class frmLiveViewHTML
    Dim cancel As Boolean = False
    Public m_eventID As Integer
    Public m_eventBased As Boolean

    Public Function editHTML(rawHTML As String) As String
        rawHTML = rawHTML.Replace("&lt;", "<").Replace("&gt;", ">")

        rawHTML = rawHTML.Replace("%3C", "<").Replace("%3E", ">")

        rawHTML = rawHTML.Replace("%5B", "[").Replace("%5D", "]")

        txtHTML.Text = rawHTML

        Me.ShowDialog()

        If cancel = False Then
            Return txtHTML.Text
        Else
            Return Nothing
        End If
    End Function

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Close()
    End Sub

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        cancel = True
        Close()
    End Sub

    Private Sub frmLiveViewHTML_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        setupForDragAndDrop(txtHTML)
        showInserter(Me, m_eventID, m_eventBased)
    End Sub
End Class