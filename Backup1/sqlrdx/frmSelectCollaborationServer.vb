﻿Public Class frmSelectCollaborationServer
    Dim m_selectedServer As Integer = 0
    Dim cancel As Boolean = True
    Public Property selectedServer() As Integer
        Get
            Return m_selectedServer
        End Get
        Set(ByVal value As Integer)
            m_selectedServer = value
        End Set
    End Property

    Sub loadServers()
        Dim SQL As String = "SELECT * FROM collaboratorsattr"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim btn As Button = New Button
                btn.Text = oRs("servername").Value
                btn.TextAlign = ContentAlignment.MiddleCenter
                btn.Tag = oRs("collaboid").Value
                btn.Visible = True
                btn.Width = pnlServers.Width - 10
                btn.Height = 23
                AddHandler btn.Click, AddressOf btn_click

                pnlServers.Controls.Add(btn)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Sub btn_click(ByVal sender As Object, ByVal e As EventArgs)
        cancel = False

        Dim btn As Button = CType(sender, Button)
        Dim collaboid As Integer = btn.Tag

        selectedServer = collaboid

        Close()
    End Sub

    Private Sub frmSelectCollaborationServer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub

    Public Function showSelector() As DialogResult
        loadServers()

        Me.ShowDialog()

        If cancel Then
            Return Windows.Forms.DialogResult.Cancel
        Else
            Return Windows.Forms.DialogResult.OK
        End If
    End Function
End Class