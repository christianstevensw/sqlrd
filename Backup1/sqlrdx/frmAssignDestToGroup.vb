﻿Public Class frmAssignDestToGroup
    Dim m_cancel As Boolean = False
    Dim m_destinationID As Integer

    Sub prepareImageList()
        ImageList1.ImageSize = New Size(16, 16)
        ImageList1.Images.Add(My.Resources.users_family)
    End Sub


    Private Sub _LoadGroups()
        prepareImageList()

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM GroupAttr WHERE destinationAccessType = 'DefaultOnly'"

        oRs = clsMarsData.GetData(SQL)

        lsvGroups.Items.Clear()

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oItem = New ListViewItem

                For Each o As ListViewItem In lsvGroups.Items
                    If o.Text = oRs("groupname").Value Then
                        GoTo skip
                    End If
                Next

                oItem.Text = oRs("groupname").Value
                oItem.SubItems.Add(oRs("groupdesc").Value)
                oItem.Tag = oRs("groupid").Value
                oItem.ImageIndex = 0

                Dim grp As usergroup = New usergroup(Convert.ToInt32(oItem.Tag))
                Dim allowedDests As ArrayList = grp.assignedDestinations

                If allowedDests.Contains(m_destinationID) Then
                    oItem.Checked = True
                End If

                lsvGroups.Items.Add(oItem)
skip:
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Public Sub assignDestinationToGroup(ByVal destinationID As Integer)
        m_destinationID = destinationID

        _LoadGroups()

        Me.ShowDialog()

        If m_cancel Then Return

        For Each it As ListViewItem In lsvGroups.Items
            Dim groupID As Integer = it.Tag
            Dim grp As usergroup = New usergroup(groupID)

            If it.Checked Then
                grp.assignADestination(destinationID)
            Else
                grp.revokeADestination(destinationID)
            End If

            grp.dispose()
        Next

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        m_cancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Private Sub frmAssignDestToGroup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub
End Class