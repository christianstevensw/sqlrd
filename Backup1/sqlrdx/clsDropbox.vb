﻿Imports OAuthProtocol.OAuth
Imports OAuthProtocol
Imports Dropbox.Api
Imports System.Collections.Generic

Public Class clsDropbox
    Private Const consumerKey As String = "ct4mzfn2wmq3fhm"
    Private Const consumerSecret As String = "rpkqdsxnu9uhvj5"
    Dim m_token, m_secret As String
    Dim m_accessToken As OAuthToken
    Dim m_dbApi As DropboxApi

    Public Enum ITEMTYPE
        FILE = 0
        FOLDER = 1
    End Enum
    Sub New(token As String, secret As String)
        m_token = token
        m_secret = secret

        m_accessToken = New OAuthToken(m_token, m_secret)
        m_dbApi = New DropboxApi(consumerKey, consumerSecret, m_accessToken)


    End Sub

    Public Shared Function GetAccessToken(ByRef accountName As String) As OAuthToken

        Dim oauth = New OAuth()
        Dim requestToken = oauth.GetRequestToken(New Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret)
        Dim authorizeUri = oauth.GetAuthorizeUri(New Uri(DropboxRestApi.AuthorizeBaseUri), requestToken)

        ' Dim browser As frmBrowser = New frmBrowser

        'browser.ShowDialog(authorizeUri.AbsoluteUri)

        'Dim p As Process = New Process
        ' p.StartInfo.FileName = "chrome"
        'p.StartInfo.Arguments = authorizeUri.AbsoluteUri
        'p.StartInfo.UseShellExecute = True
        'p.Start()

        'p.WaitForExit()
        Process.Start(authorizeUri.AbsoluteUri)


        Dim token As OAuthToken
        Dim tokenGot As Boolean
        Dim errInfo As Exception
        Dim timeOutExpired As Boolean
        Dim startedTime As Date = Date.Now

        Do
            Try
                token = oauth.GetAccessToken(New Uri(DropboxRestApi.BaseUri), consumerKey, consumerSecret, requestToken)

                tokenGot = True
            Catch ex As Exception
                errInfo = ex
                tokenGot = False
            End Try

            If Date.Now.Subtract(startedTime).TotalSeconds > 120 Then
                Throw errInfo
            End If
        Loop Until tokenGot = True

        Dim api As New DropboxApi(consumerKey, consumerSecret, token)
        Dim ac As Account = api.GetAccountInfo
        Dim q As Dropbox.Api.Quota = ac.Quota

        accountName = ac.DisplayName

        Return token
    End Function

    Public Function uploadFileToDropbox(targetFileName As String, fileToUpload As String, dropBoxPath As String, ByRef err As Exception) As Boolean
        Try
            Dim file As Dropbox.Api.FileSystemInfo = m_dbApi.UploadFile(dropBoxPath, targetFileName, fileToUpload)
            Return True
        Catch ex As Exception
            err = ex
            Return False
        End Try
    End Function

    Public Function getDropboxFolders(path As String) As List(Of String)
        Dim contents As Dropbox.Api.File = m_dbApi.GetFiles(path, "")
        Dim foldersList As List(Of String) = New List(Of String)

        For Each item As Dropbox.Api.File In contents.Contents

            If item.IsDirectory Then
                foldersList.Add(item.Path)
            End If

        Next

        Return foldersList
    End Function

    Public Function getDropboxFiles(path As String) As List(Of String)
        Dim contents As Dropbox.Api.File = m_dbApi.GetFiles(path, "")
        Dim filesList As List(Of String) = New List(Of String)

        For Each item As Dropbox.Api.File In contents.Contents

            If Not item.IsDirectory Then
                filesList.Add(item.Path)
            End If

        Next

        Return filesList
    End Function

    Public Shared Function getAbsoluteItemName(path As String)
        Dim num As Integer = path.Split("/").GetUpperBound(0)
        Return path.Split("/")(num)
    End Function

    Public Shared Function getAllStoredAccounts() As List(Of String)
        getAllStoredAccounts = New List(Of String)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM dropboxattr")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                getAllStoredAccounts.Add(oRs("accountname").Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

    End Function

End Class

Public Class dropboxAccount
    Dim m_concernedRow As ADODB.Recordset

    Sub New(accountName As String)
        m_concernedRow = clsMarsData.GetData("SELECT * FROM dropboxAttr WHERE accountName = '" & SQLPrepare(accountName) & "'") ' clsDataAccess.selectFirstRow(g_settingsData.dropboxAccount, "dropboxName", accountName)
    End Sub

    Public ReadOnly Property token As String
        Get
            If m_concernedRow.EOF = True Then
                Throw New Exception("The provided Dropbox account could not be found in CRD. Make sure that you have added it in Options > Cloud Storage")
            End If

            Return _DecryptDBValue(IsNull(m_concernedRow("tokenValue").Value))
        End Get

    End Property

    Public ReadOnly Property secret As String
        Get
            If m_concernedRow.EOF = True Then
                Throw New Exception("The provided Dropbox account could not be found in CRD. Make sure that you have added it in Options > Cloud Storage")
            End If

            Return _DecryptDBValue(IsNull(m_concernedRow("secretValue").Value))
        End Get
    End Property

    Public Shared Function addDropboxAccount(accountName As String, token As String, secret As String, ByRef err As Exception) As Boolean
        Try
            Dim rs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM dropboxAttr WHERE accountName = '" & SQLPrepare(accountName) & "'")

            If rs IsNot Nothing AndAlso rs.EOF = False Then
                Throw New Exception("The name provided is already in use. Please use a different name")
            Else
                Dim cols, vals As String
                cols = "accountname, tokenvalue,secretvalue"
                vals = "'" & SQLPrepare(accountName) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(token)) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(secret)) & "'"

                clsMarsData.DataItem.InsertData("dropboxattr", cols, vals, True)
            End If

            Return True
        Catch ex As Exception
            err = ex
            Return False
        End Try
    End Function

    Public Shared Function deleteDropboxAccount(accountName As String) As Boolean
        Try
            clsMarsData.WriteData("DELETE FROM dropboxattr WHERE accountname ='" & SQLPrepare(accountName) & "'")

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Return False
        End Try
    End Function
End Class
