﻿Namespace ChristianStevenMessaging
    Public Class messageQ
        Dim m_qDT As DataTable
        Dim m_qRow As DataRow
        Dim m_qName As String
        Shared m_qDB As String
        ''' <summary>
        ''' Return the path to the  messages database
        ''' </summary>
        ''' <param name="qName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function validateMessageQ(ByVal qName As String) As String

            '//Create Database
            Dim SQLconnect As New SQLite.SQLiteConnection()
            Dim messageQPath As String = sAppDataPath & "\MessageQ\" & qName & "\"

            clsMarsParser.Parser.ParseDirectory(messageQPath)

            Dim messageQDB As String = messageQPath & qName & ".dbx"


            If IO.File.Exists(messageQDB) = False Then
                'Database Doesn't Exist so Created at Path
                SQLconnect.ConnectionString = "Data Source=" & messageQDB & ";"
                SQLconnect.Open()

                Dim SQLcommand As SQLite.SQLiteCommand

                SQLcommand = SQLconnect.CreateCommand
                '// create taskHistory table
                Dim cmdText As String = "create table messageattr (messageid TEXT, qid, messagebody TEXT, unread INTEGER, entrydate TEXT, processid INTEGER);"

                SQLcommand.CommandText = cmdText
                SQLcommand.ExecuteNonQuery()

                '//create the reportHistory table
                cmdText = "create table messageqattr (qid TEXT, qname TEXT);"
                SQLcommand.CommandText = cmdText
                SQLcommand.ExecuteNonQuery()

                SQLcommand.Dispose()
                SQLconnect.Close()

            End If

            Return messageQDB
        End Function

        <DebuggerStepThrough> _
        Public Shared Function readFromQDB(query As String) As DataTable
            Dim SQLconnect As New SQLite.SQLiteConnection()

            If m_qDB Is Nothing Then
                Dim tmp As messageQ = New messageQ("progress", True)
            End If

            SQLconnect.ConnectionString = "Data Source=" & m_qDB & ";"

            Dim ad As SQLite.SQLiteDataAdapter = New SQLite.SQLiteDataAdapter(query, SQLconnect)
            Dim dt As DataTable = New DataTable("messages")

            ad.Fill(dt) '//the datatable with all the data

            ad.Dispose()
            ad = Nothing

            SQLconnect.Close()
            SQLconnect.Dispose()
            SQLconnect = Nothing
            Return dt
        End Function

        <DebuggerStepThrough> _
        Public Shared Function writeToQDB(query As String) As Boolean
            Try
                Dim SQLconnect As New SQLite.SQLiteConnection()

                SQLconnect.ConnectionString = "Data Source=" & m_qDB & ";"

                SQLconnect.Open()

                Dim cmd As SQLite.SQLiteCommand = New SQLite.SQLiteCommand(query, SQLconnect)

                cmd.ExecuteNonQuery()

                SQLconnect.Close()
                SQLconnect.Dispose()
                SQLconnect = Nothing
                cmd.Dispose()
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
        '//creates an instance of the desired message Q.
        Sub New(QName As String, createIfNotExist As Boolean)

            m_qDB = validateMessageQ(QName)

            m_qDT = readFromQDB("SELECT * FROM messageQAttr WHERE qName = '" & SQLPrepare(QName) & "'")

            If m_qDT IsNot Nothing AndAlso m_qDT.Rows.Count > 0 Then
                m_qName = QName
                m_qRow = m_qDT.Rows(0)
            Else
                If createIfNotExist = False Then
                    Throw New Exception("A message queue by that name does not exist. Please create one first")
                Else
                    Dim qerror As Exception

                    If createNewMessageQ(QName, qerror) = False Then
                        If qerror IsNot Nothing Then
                            Throw qerror
                        End If
                    Else
                        m_qName = QName

                        m_qDT = readFromQDB("SELECT * FROM messageQAttr WHERE qName = '" & SQLPrepare(QName) & "'")

                        If m_qDT Is Nothing Then
                            Throw New Exception("Error creating message queue")
                        ElseIf m_qDT.Rows.Count = 0 Then
                            Throw New Exception("Error creating message queue")
                        Else
                            m_qRow = m_qDT.Rows(0)
                        End If
                    End If
                End If
            End If
        End Sub

        Public Shared Sub cleanOldMessagesAsync()
            Dim oT As Threading.Thread = New Threading.Thread(AddressOf cleanOldMessages)
            oT.Start()
        End Sub
        ''' <summary>
        ''' returns the number of MARS editors
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function cleanOldMessages() As Integer
            '//check if any mars editors are running
            Dim runCount As Integer = 0

            Try

                For Each p As Process In Process.GetProcessesByName("sqlrd")
                    Dim title As String = IsNull(p.MainWindowTitle, "")

                    If title.ToLower.Contains("process") Or title.ToLower.Contains("system") Then
                        runCount += 1
                    End If
                Next

                'Dim test As DataTable = readFromQDB("SELECT * FROM messageattr")

                If runCount > 0 Then
                    Dim minutesAgo As String = Date.Now.AddMinutes(-5).ToString("yyyy-MM-dd HH:mm:ss")
                    writeToQDB("DELETE FROM messageattr WHERE entrydate < '" & minutesAgo & "';")

                    'test = readFromQDB("SELECT * FROM messageattr")
                Else
                    Try
                        Dim fi As IO.FileInfo = New IO.FileInfo(m_qDB)
                        If fi.Length > 3072 Then
                            writeToQDB("DELETE FROM messageattr; VACUUM;")
                        End If
                    Catch ex As Exception
                        writeToQDB("DELETE FROM messageattr; VACUUM;")
                    End Try
                End If
            Catch : End Try

            Dim SQL As String = "SELECT DISTINCT processid from messageattr" '//the left over ones - see the ones to keep
            Dim dt As DataTable = readFromQDB(SQL)

            If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                For Each r As DataRow In dt.Rows
                    Dim pid As Integer = r("processid")

                    Try
                        Dim proc As Process = Process.GetProcessById(pid)
                        Dim procName As String = proc.ProcessName

                        If procName.ToLower <> "sqlrd" Then
                            writeToQDB("DELETE FROM messageattr WHERE processid=" & pid)
                        End If
                    Catch e As Exception
                        writeToQDB("DELETE FROM messageattr WHERE processid=" & pid)
                    End Try

                Next

                dt.Dispose()
                dt = Nothing
            End If

            Return runCount
        End Function

        Public Shared Function createNewMessageQ(qname As String, ByRef errorInfo As Exception) As Boolean
            Dim SQL As String = "SELECT * FROM messageQAttr WHERE qname = '" & SQLPrepare(qname) & "'"
            Dim recordset As DataTable = readFromQDB(SQL)

            If recordset IsNot Nothing AndAlso recordset.Rows.Count > 0 Then
                errorInfo = New Exception("A message queue with that name already exists")
                Return False
            Else
                Dim vals, cols As String

                cols = "qid, qname"

                vals = "'" & Guid.NewGuid.ToString & "',  '" & SQLPrepare(qname) & "'"

                SQL = "INSERT INTO messageqattr (" & cols & ") VALUES (" & vals & ")"

                writeToQDB(SQL)

                Return True
            End If
        End Function


        Public ReadOnly Property qID As String
            Get

                Return m_qRow("qid")
            End Get
        End Property


        Public ReadOnly Property messageQName() As String
            Get
                Return m_qName
            End Get
        End Property

        Public Function createNewMessage(messageBody As String) As Boolean
            If cleanOldMessages() > 0 Then
                Dim msg As message = New message(qID, messageBody)
            End If
        End Function

        ''' <summary>
        ''' An arraylist of type message
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getUnreadMessages() As ArrayList
            cleanOldMessages()

            Dim dt As DataTable = readFromQDB("SELECT messageid FROM messageattr WHERE unread =1 AND qid = '" & qID & "' ORDER BY entryDate ASC")
            Dim unreadMessages As ArrayList = New ArrayList

            If dt IsNot Nothing Then
                For Each r As DataRow In dt.Rows
                    Dim msgID As String = r("messageid")
                    Dim msg As message = New message(msgID)

                    unreadMessages.Add(msg)
                Next

                dt = Nothing
            End If

            Return unreadMessages
        End Function

        ''' <summary>
        ''' An arraylist of type message
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getReadMessages() As ArrayList
            Dim dt As DataTable = readFromQDB("SELECT messageid FROM messageattr WHERE unread =0  AND qid = '" & qID & "' ORDER BY entryDate DESC")
            Dim unreadMessages As ArrayList = New ArrayList

            If dt IsNot Nothing Then
                For Each r As DataRow In dt.Rows
                    Dim msgID As String = r("messageid")
                    Dim msg As message = New message(msgID)

                    unreadMessages.Add(msg)
                Next

                dt = Nothing
            End If

            Return unreadMessages
        End Function

        Public Sub dispose()
            If m_qDT IsNot Nothing Then
                Try
                    m_qDT.Dispose()
                    m_qDT = Nothing
                Catch : End Try

                m_qDT = Nothing
            End If
        End Sub
    End Class

    Public Class message
        Dim m_messageDT As DataTable
        Dim m_messageRow As DataRow
        Private m_messageID As String

        ''' <summary>
        ''' retrieves an already existing message
        ''' </summary>
        ''' <param name="messageID"></param>
        ''' <remarks></remarks>
        Public Sub New(messageID As String)

            m_messageDT = messageQ.readFromQDB("SELECT * FROM messageattr WHERE messageid = '" & messageID & "'")

            If m_messageDT IsNot Nothing AndAlso m_messageDT.Rows.Count > 0 Then
                m_messageID = messageID
                m_messageRow = m_messageDT.Rows(0)
            Else
                Throw New Exception("Sorry but that message was not found")
            End If
        End Sub

        ''' <summary>
        ''' creates a new message
        ''' </summary>
        ''' <param name="qid"></param>
        ''' <param name="messageBody"></param>
        ''' <remarks></remarks>
        Public Sub New(qid As String, messageBody As String)

            Dim msgID As String = Guid.NewGuid.ToString
            Dim cols, vals As String

            cols = "messageID, qid, messageBody, Unread, entrydate, processid"

            vals = "'" & msgID & "'," & _
                "'" & qid & "'," & _
                "'" & SQLPrepare(messageBody) & "'," & _
                1 & "," & _
                "'" & ConDateTime(Now) & "'," & _
                g_ProcessID

            Dim query As String = "INSERT INTO messageattr (" & cols & ") VALUES (" & vals & ")"

            If messageQ.writeToQDB(query) = True Then
                messageID = msgID
                m_messageDT = messageQ.readFromQDB("SELECT * FROM messageattr WHERE messageid = '" & msgID & "'")

                If m_messageDT IsNot Nothing AndAlso m_messageDT.Rows.Count > 0 Then
                    m_messageRow = m_messageDT.Rows(0)
                End If
            Else
                Throw New Exception("Error creating message")
            End If

        End Sub


        Public Property unread() As Boolean
            Get
                Return m_messageRow("unread")
            End Get
            Set(ByVal value As Boolean)
                messageQ.writeToQDB("UPDATE messageattr SET unread = " & Convert.ToInt32(value) & " WHERE messageid = '" & m_messageID & "'")
            End Set
        End Property

        Public Function deleteMessage()
            messageQ.writeToQDB("DELETE FROM messageAttr WHERE messageid ='" & messageID & "'")
        End Function


        Public Property messageBody() As String
            Get
                Return m_messageRow("messagebody")
            End Get
            Set(ByVal value As String)
                messageQ.writeToQDB("UPDATE messageattr SET messagebody = '" & SQLPrepare(value) & "' WHERE messageid = '" & m_messageID & "'")
            End Set
        End Property


        Public Property messageID() As String
            Get
                Return m_messageID
            End Get
            Set(ByVal value As String)
                m_messageID = value
            End Set
        End Property


        Public ReadOnly Property entryDate() As Date
            Get
                Return m_messageRow("entrydate")
            End Get
        End Property


        Public Sub dispose()
            If m_messageDT IsNot Nothing Then
                Try
                    m_messageDT.Dispose()
                Catch : End Try
                m_messageDT = Nothing
            End If
        End Sub
    End Class
End Namespace
