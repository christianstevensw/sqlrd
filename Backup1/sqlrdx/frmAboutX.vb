﻿Public Class frmAboutX
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub LoadPic()
        On Error Resume Next
        Dim LastPic As String
        Dim oUI As clsMarsUI = New clsMarsUI

        LastPic = oUI.ReadRegistry("LastPic", "0", , , True)

        Select Case LastPic
            Case "0", "10"
                ' lblVersion.Top = 217
                ' lblEdition.Top = 217
                ReflectionImage1.Image = Image.FromFile(getAssetLocation("sqlrd7sarah.png"))
                oUI.SaveRegistry("LastPic", "1", , , True)
            Case "1"
                ' lblVersion.Top = 227
                ' lblEdition.Top = 227
                ReflectionImage1.Image = Image.FromFile(getAssetLocation("sqlrd7helen.png"))
                oUI.SaveRegistry("LastPic", "0", , , True)
        End Select
    End Sub

    Private Sub frmAboutX_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        LoadPic()

        LabelX1.Text = "Maximum  daily  outputs (peak  usage): " & outputTracker.getMaxPer24HourPeriod

        Dim licenseFile As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ChristianSteven\" & Application.ProductName & "\" & Application.ProductName & ".licx")

        If IO.File.Exists(licenseFile) Then
            Dim lic As licensex = New licensex(licenseFile)

            txtCustID2.Text = lic.customerNumber
            txtCustomerName.Text = lic.companyName
            txtProduct.Text = lic.product
            txtEdition.Text = lic.edition
            txtOutputlevel.Text = lic.outputlevel
            txtLicense2.Text = lic.licenseNumber
            txtWebLics.Text = lic.webLicenses
        Else
            txtCustID2.Text = clsMarsUI.MainUI.ReadRegistry("CustNo", "")
            txtCustomerName.Text = ""
            txtProduct.Text = ""
            txtEdition.Text = ""
            txtOutputlevel.Text = ""
            txtLicense2.Text = ""
            txtWebLics.Text = ""
        End If

        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & "sqlrd.exe")

        lblVersion.Text = "Version " & oFile.FileMajorPart & "." & oFile.FileMinorPart & " " & oFile.Comments

        lblEdition.Text = gsEdition & " Edition"
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        clsMarsUI.MainUI.SaveRegistry("CustNo", txtCustID2.Text, , , True)
        Close()
    End Sub

    Dim imageHolder As Image

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click, ButtonX1.Click

        Dim licenseFile As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ChristianSteven\" & Application.ProductName & "\" & Application.ProductName & ".licx")

        If txtLicense2.ReadOnly Then
            imageHolder = btnEdit.Image
            txtLicense2.ReadOnly = False
            btnEdit.Image = ButtonX1.Image
            txtLicense2.Focus()
            txtLicense2.SelectAll()
        Else
            Dim sec As clsLicenser = New clsLicenser

            If sec.CheckLicense(txtLicense2.Text, StrReverse(txtCustID2.Text), StrReverse(txtCustID2.Text), "CSS", txtCustID2.Text) = True Then
                clsMarsUI.MainUI.SaveRegistry("RegNo", txtLicense2.Text, False, , True)

                Dim lic As licensex = New licensex(licenseFile)
                lic.licenseNumber = txtLicense2.Text
                lic.saveLicense()
                btnEdit.Image = imageHolder
                txtLicense2.ReadOnly = True

                MessageBox.Show("The license has been updated successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("The license number you have entered is not valid.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                btnEdit.Image = imageHolder
                txtLicense2.ReadOnly = True
                txtLicense2.Text = clsMarsUI.MainUI.ReadRegistry("RegNo", "")
            End If
        End If


    End Sub
End Class