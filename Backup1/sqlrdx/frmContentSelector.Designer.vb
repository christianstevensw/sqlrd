﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmContentSelector
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.chkMessaging = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkUserDefaults = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSystemPaths = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAudit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkGeneral = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.DividerLabel1 = New sqlrd.DividerLabel()
        Me.btnProceed = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(448, 27)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the settings data to copy from the main server (the server where th" & _
    "e schedule is installed)"
        '
        'chkMessaging
        '
        Me.chkMessaging.AutoSize = True
        '
        '
        '
        Me.chkMessaging.BackgroundStyle.Class = ""
        Me.chkMessaging.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMessaging.Location = New System.Drawing.Point(144, 62)
        Me.chkMessaging.Name = "chkMessaging"
        Me.chkMessaging.Size = New System.Drawing.Size(133, 15)
        Me.chkMessaging.TabIndex = 1
        Me.chkMessaging.Text = "Messaging information"
        '
        'chkUserDefaults
        '
        Me.chkUserDefaults.AutoSize = True
        '
        '
        '
        Me.chkUserDefaults.BackgroundStyle.Class = ""
        Me.chkUserDefaults.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUserDefaults.Location = New System.Drawing.Point(15, 62)
        Me.chkUserDefaults.Name = "chkUserDefaults"
        Me.chkUserDefaults.Size = New System.Drawing.Size(87, 15)
        Me.chkUserDefaults.TabIndex = 1
        Me.chkUserDefaults.Text = "User defaults"
        '
        'chkSystemPaths
        '
        Me.chkSystemPaths.AutoSize = True
        '
        '
        '
        Me.chkSystemPaths.BackgroundStyle.Class = ""
        Me.chkSystemPaths.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSystemPaths.Location = New System.Drawing.Point(15, 85)
        Me.chkSystemPaths.Name = "chkSystemPaths"
        Me.chkSystemPaths.Size = New System.Drawing.Size(90, 15)
        Me.chkSystemPaths.TabIndex = 1
        Me.chkSystemPaths.Text = "System Paths"
        '
        'chkAudit
        '
        Me.chkAudit.AutoSize = True
        '
        '
        '
        Me.chkAudit.BackgroundStyle.Class = ""
        Me.chkAudit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAudit.Location = New System.Drawing.Point(144, 39)
        Me.chkAudit.Name = "chkAudit"
        Me.chkAudit.Size = New System.Drawing.Size(72, 15)
        Me.chkAudit.TabIndex = 1
        Me.chkAudit.Text = "Audit Trail"
        '
        'chkGeneral
        '
        Me.chkGeneral.AutoSize = True
        '
        '
        '
        Me.chkGeneral.BackgroundStyle.Class = ""
        Me.chkGeneral.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkGeneral.Location = New System.Drawing.Point(15, 39)
        Me.chkGeneral.Name = "chkGeneral"
        Me.chkGeneral.Size = New System.Drawing.Size(107, 15)
        Me.chkGeneral.TabIndex = 1
        Me.chkGeneral.Text = " General Settings"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(-12, 107)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(497, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 2
        '
        'btnProceed
        '
        Me.btnProceed.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnProceed.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnProceed.Location = New System.Drawing.Point(176, 123)
        Me.btnProceed.Name = "btnProceed"
        Me.btnProceed.Size = New System.Drawing.Size(75, 23)
        Me.btnProceed.TabIndex = 3
        Me.btnProceed.Text = "&Proceed"
        '
        'frmContentSelector
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 152)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnProceed)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.chkGeneral)
        Me.Controls.Add(Me.chkAudit)
        Me.Controls.Add(Me.chkSystemPaths)
        Me.Controls.Add(Me.chkUserDefaults)
        Me.Controls.Add(Me.chkMessaging)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmContentSelector"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Migrate Settings Data from Server"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkMessaging As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkUserDefaults As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSystemPaths As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAudit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkGeneral As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents btnProceed As DevComponents.DotNetBar.ButtonX
End Class
