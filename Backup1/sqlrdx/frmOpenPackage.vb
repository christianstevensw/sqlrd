﻿Public Class frmOpenPackage
    Public Sub openPackage(packageName As String, packID As Integer, isDataDriven As Boolean, isDynamic As Boolean)

        ucReports.loadReports(packID, isDataDriven, isDynamic)

        Me.Text = "Package - " & packageName

        Me.Show()
    End Sub
End Class