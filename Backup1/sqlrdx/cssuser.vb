﻿Public Class cssusers

    Public Class normalUser
        Dim oRs As ADODB.Recordset
        Dim m_userID As String
        Sub New(userid As String)
            Dim SQL As String = "SELECT * FROM crdusers WHERE userid = '" & SQLPrepare(userid) & "'"
            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing OrElse oRs.EOF = True Then
                Throw New Exception("Could not initialize user '" & userid & "'")
            Else
                m_userID = userid
            End If
        End Sub

        Public ReadOnly Property firstName As String
            Get
                Return IsNull(oRs("firstname").Value, "")
            End Get
        End Property

        Public ReadOnly Property lastName As String
            Get
                Return IsNull(oRs("lastName").Value, "")
            End Get
        End Property

        Public ReadOnly Property userID As String
            Get
                Return m_userID
            End Get
        End Property

        Public ReadOnly Property password As String
            Get
                Return _DecryptDBValue(IsNull(oRs("password").Value, ""))
            End Get
        End Property

        Public ReadOnly Property userRole As String
            Get
                Return IsNull(oRs("userrole").Value, "")
            End Get
        End Property

        Public ReadOnly Property lastModifiedDate As String
            Get
                Return IsNull(oRs("lastmoddate").Value, Date.Now)
            End Get
        End Property

        Public ReadOnly Property lastModifiedBy As String
            Get
                Return IsNull(oRs("lastModBy").Value, "SQLRDAdmin")
            End Get
        End Property

        Public ReadOnly Property webEnabled As Boolean
            Get
                Return Convert.ToInt32(IsNull(oRs("lokienabled").Value, 0))
            End Get
        End Property

        Public ReadOnly Property autoLogin As Boolean
            Get
                Return Convert.ToInt32(IsNull(oRs("autologin").Value, 0))
            End Get
        End Property

        Sub dispose()
            oRs.Close()
            oRs = Nothing
        End Sub
    End Class

    Public Shared Function userCanAccessReport(reportid As Integer) As Boolean
        Dim SQL As String = String.Format("SELECT ReportID FROM UserView WHERE UserID = '{0}' AND reportid ={1}", SQLPrepare(gUser), reportid)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            userCanAccessReport = False
        Else
            userCanAccessReport = True
        End If

        SQL = String.Format("SELECT owner FROM reportattr WHERE reportid ={0}", reportid)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim tmp As String = IsNull(oRs("owner").Value)

            If tmp.ToLower = gUser.ToLower Then
                userCanAccessReport = True
            End If
        Else
            userCanAccessReport = False
        End If

        oRs.Close()
    End Function

    Public Shared Function userCanAccessPackage(packid As Integer) As Boolean
        Dim SQL As String = String.Format("SELECT packid FROM UserView WHERE UserID = '{0}' AND packid ={1}", SQLPrepare(gUser), packid)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            userCanAccessPackage = False
        Else
            userCanAccessPackage = True
        End If

        SQL = String.Format("SELECT owner FROM packageattr WHERE packid ={0}", packid)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim tmp As String = IsNull(oRs("owner").Value)

            If tmp.ToLower = gUser.ToLower Then
                userCanAccessPackage = True
            End If
        Else
            userCanAccessPackage = False
        End If

        oRs.Close()
    End Function

    Public Shared Function userCanAccessAutomation(autoid As Integer) As Boolean
        Dim SQL As String = String.Format("SELECT autoid FROM UserView WHERE UserID = '{0}' AND autoid ={1}", SQLPrepare(gUser), autoid)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            userCanAccessAutomation = False
        Else
            userCanAccessAutomation = True
        End If

        SQL = String.Format("SELECT owner FROM automationattr WHERE autoid ={0}", autoid)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim tmp As String = IsNull(oRs("owner").Value)

            If tmp.ToLower = gUser.ToLower Then
                userCanAccessAutomation = True
            End If
        Else
            userCanAccessAutomation = False
        End If

        oRs.Close()
    End Function

    Public Shared Function userCanAccessEvent(eventid As Integer) As Boolean
        Dim SQL As String = String.Format("SELECT eventid FROM UserView WHERE UserID = '{0}' AND eventid ={1}", SQLPrepare(gUser), eventid)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            userCanAccessEvent = False
        Else
            userCanAccessEvent = True
        End If

        SQL = String.Format("SELECT owner FROM eventattr6 WHERE eventid ={0}", eventid)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim tmp As String = IsNull(oRs("owner").Value)

            If tmp.ToLower = gUser.ToLower Then
                userCanAccessEvent = True
            End If
        Else
            userCanAccessEvent = False
        End If

        oRs.Close()
    End Function

    Public Shared Function userCanAccessEventPackage(packid As Integer) As Boolean
        Dim SQL As String = String.Format("SELECT eventpackid FROM UserView WHERE UserID = '{0}' AND eventpackid ={1}", SQLPrepare(gUser), packid)

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            userCanAccessEventPackage = False
        Else
            userCanAccessEventPackage = True
        End If

        SQL = String.Format("SELECT owner FROM eventpackageattr WHERE eventpackid ={0}", packid)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim tmp As String = IsNull(oRs("owner").Value)

            If tmp.ToLower = gUser.ToLower Then
                userCanAccessEventPackage = True
            End If
        Else
            userCanAccessEventPackage = False
        End If

        oRs.Close()
    End Function

    Public Class NTDomainUser
        Dim oRs As ADODB.Recordset
        Dim m_userID As String

        Sub New(userid As String)
            Dim SQL As String = "SELECT * FROM domainattr WHERE domainname = '" & SQLPrepare(userid) & "'"
            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing OrElse oRs.EOF = True Then
                Throw New Exception("Could not initialize user '" & userid & "'")
            Else
                m_userID = userid
            End If
        End Sub

        Public ReadOnly Property domainName As String
            Get
                Return m_userID.Split("\")(0)
            End Get
        End Property

        Public ReadOnly Property userID As String
            Get
                Return m_userID
            End Get
        End Property

        Public ReadOnly Property userRole As String
            Get
                Return IsNull(oRs("userrole").Value, "")
            End Get
        End Property

        Public ReadOnly Property lastModifiedDate As String
            Get
                Return IsNull(oRs("lastmoddate").Value, Date.Now)
            End Get
        End Property

        Public ReadOnly Property lastModifiedBy As String
            Get
                Return IsNull(oRs("lastModBy").Value, "SQLRDAdmin")
            End Get
        End Property

        Public ReadOnly Property webEnabled As Boolean
            Get
                Return Convert.ToInt32(IsNull(oRs("lokienabled").Value, 0))
            End Get
        End Property

        Sub dispose()
            oRs.Close()
            oRs = Nothing
        End Sub
    End Class
End Class