﻿Public Class EventBasedPackage
    Dim m_eventpackid As Integer
    Dim m_concernedRow As ADODB.Recordset
    Public m_schedule As Schedule
    Sub New(ByVal eventpackid As Integer)
        m_eventpackid = eventpackid

        m_schedule = New Schedule(eventpackid, Schedule.parentscheduleType.EVENTBASEDPACKAGE)
    End Sub

    Public ReadOnly Property concernedRow() As ADODB.Recordset
        Get
            If m_concernedRow Is Nothing Then
                m_concernedRow = clsMarsData.GetData("SELECT * FROM eventpackageattr WHERE eventpackid =" & m_eventpackid)
               

                If m_concernedRow IsNot Nothing AndAlso m_concernedRow.EOF = False Then
                    Return m_concernedRow
                Else
                    Throw New Exception("Could not locate the Event-Based Package referenced by ID: " & m_eventpackid)
                    Return Nothing
                End If
            Else
                Return m_concernedRow
            End If
        End Get
    End Property

    Public ReadOnly Property packageParent As Integer
        Get
            Return concernedRow.Fields("parent").Value
        End Get
    End Property

    Public ReadOnly Property schedulePath As String
        Get
            Dim parent As Integer = Me.packageParent
            Dim folder As folder = New folder(parent)

            Return folder.getFolderPath()
        End Get
    End Property

    Public ReadOnly Property ID As Integer
        Get
            Return m_eventpackid
        End Get
    End Property
    Public ReadOnly Property eventPackageName() As String
        Get
            Return concernedRow.Fields("packagename").Value
        End Get
    End Property

    Public ReadOnly Property LastRun() As String
        Get
            Return m_schedule.lastRun
        End Get
    End Property

    Public ReadOnly Property NextRun() As String
        Get
            Return m_schedule.nextRun
        End Get
    End Property

    Public ReadOnly Property LastResult() As String
        Get
            Return m_schedule.lastResult
        End Get
    End Property

    Public ReadOnly Property Status() As Boolean
        Get
            Return m_schedule.status
        End Get
    End Property

    Public ReadOnly Property Owner() As String
        Get
            Return concernedRow.Fields("owner").Value
        End Get
    End Property

    Public ReadOnly Property packageImage() As Image
        Get
            If Status = True Then
                Return My.Resources.cube_molecule2
            Else
                Return MakeGrayscale3(My.Resources.cube_molecule2)
            End If
        End Get
    End Property

    Public ReadOnly Property largeimageFileURL() As String
        Get
            If Status = True Then
                Return "largeimages/packages/cube_molecule.png"
            Else
                Return "largeimages/packages/cube_molecule_bw.png"
            End If
        End Get
    End Property

    Public ReadOnly Property events() As EventBased()
        Get
            Dim evs() As EventBased = Nothing
            Dim I As Integer = 0
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM eventattr6 WHERE packid =" & m_eventpackid)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve evs(I)

                    evs(I) = New EventBased(oRs("eventid").Value)

                    I += 1

                    oRs.MoveNext()
                Loop

                oRs.Close()
                Return evs
            Else
                Return Nothing
            End If
        End Get
    End Property


    Public ReadOnly Property totalEvents As Integer
        Get
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) AS [total] FROM eventattr6 WHERE packid = " & m_eventpackid)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Dim count As Integer = oRs(0).Value

                oRs.Close()

                oRs = Nothing

                Return count
            Else
                Return 0
            End If
        End Get
    End Property
End Class
