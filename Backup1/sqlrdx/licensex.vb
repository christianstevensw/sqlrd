﻿Public Class licensex

    Private dt As DataTable
    Private concernedRow As DataRow

    Sub New(licenseFile As String)
        Try
            Dim zip As Chilkat.Zip = New Chilkat.Zip
            zip.UnlockComponent("STEVENZIP_HEvTVomkpRxI")

            Dim ok As Boolean = zip.OpenZip(licenseFile)

            zip.DecryptPassword = "inc0rr3ct"

            If ok = False Then
                Throw New Exception(zip.LastErrorText)
            End If

            Dim saveLoc As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ChristianSteven\" & Application.ProductName & "\" & Guid.NewGuid.ToString)

            clsMarsParser.Parser.ParseDirectory(saveLoc) '//create it if it doesnt exist

            Dim i As Integer = zip.Unzip(saveLoc)

            If i = -1 Then
                Throw New Exception(zip.LastErrorText)
            End If

            dt = New DataTable("licenseAttr")

            dt.ReadXml(IO.Directory.GetFiles(saveLoc)(0))

            Dim rows() As DataRow = dt.Select()

            If rows Is Nothing Then
                Throw New Exception("Invalid license file exception")
            ElseIf rows.Length = 0 Then
                Throw New Exception("Invalid license file exception")
            Else
                concernedRow = rows(0)
            End If

            Try
                IO.Directory.Delete(saveLoc, True)
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Public ReadOnly Property webLicenses As Integer
        Get
            Return concernedRow("weblicenses")
        End Get
    End Property
    Public ReadOnly Property customerNumber As String
        Get
            Return concernedRow("customernumber")
        End Get
    End Property

    Public ReadOnly Property companyName As String
        Get
            Return concernedRow("companyname")
        End Get
    End Property

    Public ReadOnly Property product As String
        Get
            Return concernedRow("product")
        End Get
    End Property

    Public ReadOnly Property edition As String
        Get
            Return concernedRow("edition")
        End Get
    End Property

    Public ReadOnly Property outputlevel As String
        Get
            Return concernedRow("outputlevel")
        End Get
    End Property
    Public Property licenseNumber As String
        Get
            Return concernedRow("licenseNumber")
        End Get
        Set(value As String)
            concernedRow("licenseNumber") = value
        End Set
    End Property
    Public Function saveLicense()
        Dim saveLoc As String = IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ChristianSteven\" & Application.ProductName)

        clsMarsParser.Parser.ParseDirectory(saveLoc) '//create it if it doesnt exist

        '//create the datatable
        Dim dt As DataTable = New DataTable("licenseAttr")

        With dt.Columns
            .Add("customernumber")
            .Add("companyname")
            .Add("product")
            .Add("edition")
            .Add("outputlevel")
            .Add("licensenumber")
            .Add("weblicenses")
        End With

        'fill that shit up
        Dim r As DataRow = dt.Rows.Add

        r("customernumber") = concernedRow("customernumber")
        r("companyname") = concernedRow("companyname")
        r("product") = concernedRow("product")
        r("edition") = concernedRow("edition")
        r("outputlevel") = concernedRow("outputlevel")
        r("licensenumber") = concernedRow("licensenumber")
        r("weblicenses") = concernedRow("weblicenses")

        '//save it to disk
        Dim fileName As String = Guid.NewGuid.ToString & ".xml"
        Dim fullPath As String = IO.Path.Combine(saveLoc, fileName)

        dt.WriteXml(fullPath, XmlWriteMode.WriteSchema)

        '//zip that shit up
        Dim zip As Chilkat.Zip = New Chilkat.Zip
        Dim ok As Boolean

        ok = zip.UnlockComponent("STEVENZIP_HEvTVomkpRxI")

        If ok = False Then
            Throw New Exception(zip.LastErrorText)
        End If

        Dim zipFile As String = IO.Path.Combine(saveLoc, Application.ProductName & ".licx")

        If IO.File.Exists(zipFile) Then
            IO.File.Delete(zipFile)
        End If

        ok = zip.NewZip(zipFile)

        If ok = False Then
            Throw New Exception(zip.LastErrorText)
        End If

        zip.DiscardPaths = True
        zip.AppendOneFileOrDir(fullPath, False)

        zip.PasswordProtect = False
        zip.Encryption = 4
        zip.EncryptPassword = "inc0rr3ct"

        ok = zip.WriteZipAndClose()

        If ok = False Then
            Throw New Exception(zip.LastErrorText)
        End If

        dt.Dispose()

        Try
            IO.File.Delete(fullPath)
        Catch : End Try
    End Function
   
End Class
