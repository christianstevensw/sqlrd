﻿Public Class outputTracker
    Public Shared Function calculateOutput(numRecords As Integer, numDestinations As Integer, numReports As Integer) As Integer
        Return numRecords * numDestinations * numReports
    End Function

    Public Shared Sub addOutput(count As Integer)
        Dim SQL As String = "SELECT * FROM outputTracker WHERE column1 = '" & SQLPrepare(_EncryptDBValue(ConDate(Date.Now))) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim cols, vals As String

        cols = "column1,column2"

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                vals = "'" & SQLPrepare(_EncryptDBValue(ConDate(Date.Now))) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(count)) & "'"

                clsMarsData.DataItem.InsertData("outputTracker", cols, vals)
            Else
                Dim currentCount As Integer = _DecryptDBValue(oRs(1).Value) '//get the current count

                currentCount += count '//add one more

                SQL = "UPDATE outputTracker SET column2 = '" & SQLPrepare(_EncryptDBValue(currentCount)) & "' WHERE column1 ='" & SQLPrepare(_EncryptDBValue(ConDate(Date.Now))) & "'"

                clsMarsData.WriteData(SQL)

                '//check if we have exceeded the max allowed
                If IO.File.Exists(sAppDataPath & "\sqlrd.licx") Then
                    Try
                        Dim lic As licensex = New licensex(sAppDataPath & "\sqlrd.licx")

                        If currentCount > lic.outputlevel Then
                            '//send a warning email
                            ' Dim msg As clsMarsMessaging = New clsMarsMessaging
                            ' Dim sendTo As String = clsMarsUI.MainUI.ReadRegistry("AlertWho", "")

                            Dim isOutofMaint As Boolean = False

                            processOutofMaintViolations(lic, isOutofMaint)

                            If isOutofMaint = False Then
                                Dim outputExceededAlertLastSent As String = clsMarsUI.MainUI.ReadRegistry("outputExceededAlertLastSent", "1977-02-27")

                                If outputExceededAlertLastSent <> ConDate(Date.Now) Then
                                    _ErrorHandle("Your SQL-RD system has exceeded the maximum allowed daily outputs (" & lic.outputlevel & "). Please arrange with your account manager to have this limit increased." & vbCrLf & _
                                        "Thank you for using SQL-RD.", 10225, "OutputManager", 0, "", , True)

                                    clsMarsUI.MainUI.SaveRegistry("outputExceededAlertLastSent", ConDate(Date.Now))
                                End If
                            End If
                        End If
                    Catch : End Try
                End If
            End If

            oRs.Close()

            oRs = Nothing
        End If
    End Sub

    Shared Sub processOutofMaintViolations(lic As licensex, ByRef isOutOfMaint As Boolean)
        Dim licenseNumber As String = lic.licenseNumber
        Dim licer As clsLicenser = New clsLicenser

        '//violation statuses
        '// 1 = first warning issued
        '// 11 = second warning issued
        '// 111 = third and last warning issued

        Dim licenseInfo As String = licer.GetLicenseInfo(licenseNumber, StrReverse(lic.customerNumber) & "" & StrReverse(lic.customerNumber), "CSS")
        Dim startDateStr As String = licenseInfo.Split(",")(1)
        Dim startDate As Date = New Date(startDateStr.Substring(0, 4), startDateStr.Substring(4, 2), startDateStr.Substring(6, 2))

        If Date.Now.Subtract(startDate).TotalDays > 366 Then '//maintenance has lasped

            isOutOfMaint = True

            Dim violationStatus As Integer = clsMarsUI.MainUI.ReadRegistry("Viostat", 1, True)

            Select Case violationStatus
                Case 1
                    _ErrorHandle("Your SQL-RD system has exceeded the maximum allowed daily outputs (" & lic.outputlevel & "). Please contact us to renew your support maintenance. If this occurs two more times, all your schedules will unfortunately be disabled." & vbCrLf & _
                                        "Thank you for using SQL-RD.", 10225, "OutputManager", 0, "", , True)

                    clsMarsUI.MainUI.SaveRegistry("Viostat", 11, True)
                Case 11
                    _ErrorHandle("Your SQL-RD system has exceeded the maximum allowed daily outputs (" & lic.outputlevel & "). Please contact us to renew your support maintenance. If this occurs one more time, all your schedules will unfortunately be disabled." & vbCrLf & _
                                        "Thank you for using SQL-RD.", 10225, "OutputManager", 0, "", , True)

                    clsMarsUI.MainUI.SaveRegistry("Viostat", 111, True)
                Case 111
                    _ErrorHandle("Your SQL-RD system has exceeded the maximum allowed daily outputs (" & lic.outputlevel & "). Please contact us to renew your support maintenance. All your schedules will now be disabled." & vbCrLf & _
                                                            "Thank you for using SQL-RD.", 10225, "OutputManager", 0, "", , True)

                    clsMarsUI.MainUI.SaveRegistry("Viostat", 1, True)

                    clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 0")
                    clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 0")
            End Select
        Else
            isOutOfMaint = False
        End If
    End Sub
    Public Shared ReadOnly Property getMaxPer24HourPeriod As Integer
        Get
            Dim SQL As String = "SELECT * FROM outputTracker"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Dim dt As DataTable = New DataTable("tracker")
                dt.Columns.Add("entrydate")
                dt.Columns.Add("count", Type.GetType("System.Int32"))

                Do While oRs.EOF = False
                    Dim r As DataRow = dt.Rows.Add
                    r("entrydate") = _DecryptDBValue(oRs("column1").Value)
                    r("count") = _DecryptDBValue(oRs("column2").Value)

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = Nothing

                Dim rows() As DataRow = dt.Select("", "COUNT DESC")

                If rows IsNot Nothing AndAlso rows.Length > 0 Then
                    Dim r As DataRow = rows(0)
                    Dim max As Integer = r("count")

                    Return max
                Else
                    Return 0
                End If

            Else
                Return 0
            End If
        End Get
    End Property

End Class
