﻿Public Class frmReportOptionsView
    Dim userCancel As Boolean

    Public Sub packageOptions(format As String, Optional packageID As Integer = 99999)

        ucOptions.FlowLayoutPanel1.Visible = False

        ucOptions.loadPackageOptions(format, packageID)

        If format = "XLS" Then
            ucOptions.ExcelGeneralSuperTab.Enabled = True
            ucOptions.txtWorksheetName.Enabled = False
        End If



        Me.ShowDialog()

        If userCancel Then Return

        ucOptions.savePackageOptions(format, packageID)
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Close()
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub frmReportOptionsView_Load(sender As Object, e As EventArgs) Handles Me.Load
        showInserter(Me, 99999)
    End Sub
End Class