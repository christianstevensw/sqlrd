﻿
Public Class FileUtilities

    Private Const MAX_STRING_BUFFER_SIZE = 256         'A good starting size  
    Private Const NO_ERROR As Int32 = 0                'Indicates success  
    Private Const ERROR_MORE_DATA As Int32 = 234       'The buffer isn't large enough for the return value.  
    Private Const ERROR_NOT_CONNECTED As Int32 = 2250  'The drive type queried is not a mapped drive.  
    Private Const ERROR_BAD_DEVICE As Int32 = 1200     'No such device was found.  


    'Detailed information about the WNetGetConnection API function is here:  
    'http://msdn.microsoft.com/en-us/library/windows/desktop/aa385453%28v=vs.85%29.aspx  
    Private Declare Ansi Function WNetGetConnection _
                         Lib "mpr.dll" _
                         Alias "WNetGetConnectionA" (ByVal lpszLocalName As String, _
                                                     ByVal lpszRemoteName As System.Text.StringBuilder, _
                                                     ByRef cbRemoteName As Int32) As Int32

    '*********************************************************************************************************************  
    '''<summary>  
    '''  Converts a path to a UNC path. If the path is already UNC it returns it untouched.  
    '''</summary>  
    Public Shared Function ToUNCPath(ByVal path As String) As String

        If Not path.StartsWith("\\") Then

            Dim pathInfo As String() = path.Split(":"c)
            Dim driveLetter As String = pathInfo(0) & ":"
            Dim bufferLen As Int32 = MAX_STRING_BUFFER_SIZE

            Do

                Dim buffer As System.Text.StringBuilder = New System.Text.StringBuilder(bufferLen)
                Dim status As Int32 = WNetGetConnection(driveLetter, buffer, bufferLen)

                Select Case status
                    Case NO_ERROR
                        'the path might not have a remainder if it is just a drive letter (for example C:)  
                        If pathInfo.Length = 2 Then
                            'the remainder should already contain a backslash since we split on :  
                            Return buffer.ToString & pathInfo(1)
                        Else
                            Return buffer.ToString
                        End If

                    Case ERROR_MORE_DATA
                        'This means we need to allocate more space to the string buffer.  
                        'bufferLen has been modified by WNetGetConnection to contain the correct length  
                        'so retry the attempt with the current value of bufferLen.  

                    Case ERROR_NOT_CONNECTED
                        'this means the drive is not a mapped drive. Return it right back.  
                        Return path

                    Case Else
                        Throw New InvalidOperationException("Error: " & status.ToString & ", the path: " & path & " is not valid.")
                End Select

                If status = NO_ERROR Then
                    'the path might not have a remainder if it is just a drive letter (for example C:)  
                    If pathInfo.Length = 2 Then
                        'the remainder should already contain a backslash since we split on :  
                        Return buffer.ToString & pathInfo(1)
                    Else
                        Return buffer.ToString
                    End If
                Else
                    Throw New InvalidOperationException("Error:" & status.ToString & ", the path: " & path & " is not valid.")
                End If

            Loop

        Else
            Return path
        End If

    End Function

End Class

