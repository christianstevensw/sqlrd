﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmOpenPackage
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOpenPackage))
        Me.ucReports = New sqlrd.ucPackagedReports()
        Me.SuspendLayout()
        '
        'ucReports
        '
        Me.ucReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucReports.Location = New System.Drawing.Point(0, 0)
        Me.ucReports.Name = "ucReports"
        Me.ucReports.Size = New System.Drawing.Size(831, 619)
        Me.ucReports.TabIndex = 0
        Me.ucReports.viewStyle = DevComponents.AdvTree.eView.Tree
        '
        'frmOpenPackage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(831, 619)
        Me.Controls.Add(Me.ucReports)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmOpenPackage"
        Me.Text = "Package - "
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ucReports As sqlrd.ucPackagedReports
End Class
