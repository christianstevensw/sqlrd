﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddUser
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddUser))
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.grpNormalUser = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.txtFirstName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtLastName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtVerifyPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbSecurityRole = New System.Windows.Forms.ComboBox()
        Me.chkAutoLogon = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkRIA = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnAssignSchedules = New DevComponents.DotNetBar.ButtonX()
        Me.grpNTUser = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX7 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX8 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX9 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX10 = New DevComponents.DotNetBar.LabelX()
        Me.chkAddEntireGroup = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmbNTDomain = New System.Windows.Forms.ComboBox()
        Me.cmbNTGroup = New System.Windows.Forms.ComboBox()
        Me.cmbNTUserName = New System.Windows.Forms.ComboBox()
        Me.cmbNTUserRole = New System.Windows.Forms.ComboBox()
        Me.chkNTRIA = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnNTAssignSchedules = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabNT = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabCRD = New DevComponents.DotNetBar.SuperTabItem()
        Me.grpNormalUser.SuspendLayout()
        Me.grpNTUser.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(453, 3)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(138, 274)
        Me.ReflectionImage1.TabIndex = 0
        '
        'grpNormalUser
        '
        Me.grpNormalUser.BackColor = System.Drawing.Color.Transparent
        Me.grpNormalUser.ColumnCount = 2
        Me.grpNormalUser.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.grpNormalUser.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.grpNormalUser.Controls.Add(Me.LabelX1, 0, 0)
        Me.grpNormalUser.Controls.Add(Me.LabelX2, 0, 1)
        Me.grpNormalUser.Controls.Add(Me.LabelX3, 0, 2)
        Me.grpNormalUser.Controls.Add(Me.LabelX4, 0, 3)
        Me.grpNormalUser.Controls.Add(Me.LabelX5, 0, 4)
        Me.grpNormalUser.Controls.Add(Me.LabelX6, 0, 5)
        Me.grpNormalUser.Controls.Add(Me.txtFirstName, 1, 0)
        Me.grpNormalUser.Controls.Add(Me.txtLastName, 1, 1)
        Me.grpNormalUser.Controls.Add(Me.txtUserID, 1, 2)
        Me.grpNormalUser.Controls.Add(Me.txtPassword, 1, 3)
        Me.grpNormalUser.Controls.Add(Me.txtVerifyPassword, 1, 4)
        Me.grpNormalUser.Controls.Add(Me.cmbSecurityRole, 1, 5)
        Me.grpNormalUser.Controls.Add(Me.chkAutoLogon, 1, 6)
        Me.grpNormalUser.Controls.Add(Me.chkRIA, 1, 7)
        Me.grpNormalUser.Controls.Add(Me.btnAssignSchedules, 1, 8)
        Me.grpNormalUser.Location = New System.Drawing.Point(3, 3)
        Me.grpNormalUser.Name = "grpNormalUser"
        Me.grpNormalUser.RowCount = 9
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.grpNormalUser.Size = New System.Drawing.Size(435, 274)
        Me.grpNormalUser.TabIndex = 1
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(211, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "First Name"
        '
        'LabelX2
        '
        Me.LabelX2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 32)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(211, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Last Name"
        '
        'LabelX3
        '
        Me.LabelX3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 61)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(211, 23)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "User ID"
        '
        'LabelX4
        '
        Me.LabelX4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 90)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(211, 23)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Password"
        '
        'LabelX5
        '
        Me.LabelX5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(3, 119)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(211, 23)
        Me.LabelX5.TabIndex = 0
        Me.LabelX5.Text = "Verify Password"
        '
        'LabelX6
        '
        Me.LabelX6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(3, 148)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(211, 23)
        Me.LabelX6.TabIndex = 0
        Me.LabelX6.Text = "Security Role"
        '
        'txtFirstName
        '
        Me.txtFirstName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtFirstName.Border.Class = "TextBoxBorder"
        Me.txtFirstName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFirstName.Location = New System.Drawing.Point(220, 3)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(212, 20)
        Me.txtFirstName.TabIndex = 1
        '
        'txtLastName
        '
        Me.txtLastName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtLastName.Border.Class = "TextBoxBorder"
        Me.txtLastName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLastName.Location = New System.Drawing.Point(220, 32)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(212, 20)
        Me.txtLastName.TabIndex = 1
        '
        'txtUserID
        '
        Me.txtUserID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.Location = New System.Drawing.Point(220, 61)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(212, 20)
        Me.txtUserID.TabIndex = 1
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.Location = New System.Drawing.Point(220, 90)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(212, 20)
        Me.txtPassword.TabIndex = 1
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'txtVerifyPassword
        '
        Me.txtVerifyPassword.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtVerifyPassword.Border.Class = "TextBoxBorder"
        Me.txtVerifyPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtVerifyPassword.Location = New System.Drawing.Point(220, 119)
        Me.txtVerifyPassword.Name = "txtVerifyPassword"
        Me.txtVerifyPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtVerifyPassword.Size = New System.Drawing.Size(212, 20)
        Me.txtVerifyPassword.TabIndex = 1
        Me.txtVerifyPassword.UseSystemPasswordChar = True
        '
        'cmbSecurityRole
        '
        Me.cmbSecurityRole.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbSecurityRole.FormattingEnabled = True
        Me.cmbSecurityRole.Location = New System.Drawing.Point(220, 148)
        Me.cmbSecurityRole.Name = "cmbSecurityRole"
        Me.cmbSecurityRole.Size = New System.Drawing.Size(212, 21)
        Me.cmbSecurityRole.TabIndex = 2
        '
        'chkAutoLogon
        '
        '
        '
        '
        Me.chkAutoLogon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoLogon.Location = New System.Drawing.Point(220, 177)
        Me.chkAutoLogon.Name = "chkAutoLogon"
        Me.chkAutoLogon.Size = New System.Drawing.Size(200, 23)
        Me.chkAutoLogon.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAutoLogon.TabIndex = 3
        Me.chkAutoLogon.Text = "Automatically logon with this user"
        '
        'chkRIA
        '
        '
        '
        '
        Me.chkRIA.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRIA.Location = New System.Drawing.Point(220, 206)
        Me.chkRIA.Name = "chkRIA"
        Me.chkRIA.Size = New System.Drawing.Size(200, 23)
        Me.chkRIA.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkRIA.TabIndex = 3
        Me.chkRIA.Text = "RIA (Web) access enabled"
        '
        'btnAssignSchedules
        '
        Me.btnAssignSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAssignSchedules.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAssignSchedules.Location = New System.Drawing.Point(220, 235)
        Me.btnAssignSchedules.Name = "btnAssignSchedules"
        Me.btnAssignSchedules.Size = New System.Drawing.Size(212, 34)
        Me.btnAssignSchedules.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAssignSchedules.TabIndex = 4
        Me.btnAssignSchedules.Text = "Assigned Schedules"
        '
        'grpNTUser
        '
        Me.grpNTUser.BackColor = System.Drawing.Color.Transparent
        Me.grpNTUser.ColumnCount = 2
        Me.grpNTUser.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.grpNTUser.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.grpNTUser.Controls.Add(Me.LabelX7, 0, 0)
        Me.grpNTUser.Controls.Add(Me.LabelX8, 0, 1)
        Me.grpNTUser.Controls.Add(Me.LabelX9, 0, 3)
        Me.grpNTUser.Controls.Add(Me.LabelX10, 0, 4)
        Me.grpNTUser.Controls.Add(Me.chkAddEntireGroup, 1, 2)
        Me.grpNTUser.Controls.Add(Me.cmbNTDomain, 1, 0)
        Me.grpNTUser.Controls.Add(Me.cmbNTGroup, 1, 1)
        Me.grpNTUser.Controls.Add(Me.cmbNTUserName, 1, 3)
        Me.grpNTUser.Controls.Add(Me.cmbNTUserRole, 1, 4)
        Me.grpNTUser.Controls.Add(Me.chkNTRIA, 1, 5)
        Me.grpNTUser.Controls.Add(Me.btnNTAssignSchedules, 1, 6)
        Me.grpNTUser.Location = New System.Drawing.Point(3, 3)
        Me.grpNTUser.Name = "grpNTUser"
        Me.grpNTUser.RowCount = 7
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 14.28571!))
        Me.grpNTUser.Size = New System.Drawing.Size(435, 237)
        Me.grpNTUser.TabIndex = 2
        '
        'LabelX7
        '
        Me.LabelX7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX7.Location = New System.Drawing.Point(3, 3)
        Me.LabelX7.Name = "LabelX7"
        Me.LabelX7.Size = New System.Drawing.Size(211, 27)
        Me.LabelX7.TabIndex = 0
        Me.LabelX7.Text = "Domain Name"
        '
        'LabelX8
        '
        Me.LabelX8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX8.Location = New System.Drawing.Point(3, 36)
        Me.LabelX8.Name = "LabelX8"
        Me.LabelX8.Size = New System.Drawing.Size(211, 27)
        Me.LabelX8.TabIndex = 0
        Me.LabelX8.Text = "Group Name"
        '
        'LabelX9
        '
        Me.LabelX9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX9.Location = New System.Drawing.Point(3, 102)
        Me.LabelX9.Name = "LabelX9"
        Me.LabelX9.Size = New System.Drawing.Size(211, 27)
        Me.LabelX9.TabIndex = 0
        Me.LabelX9.Text = "User Name"
        '
        'LabelX10
        '
        Me.LabelX10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX10.Location = New System.Drawing.Point(3, 135)
        Me.LabelX10.Name = "LabelX10"
        Me.LabelX10.Size = New System.Drawing.Size(211, 27)
        Me.LabelX10.TabIndex = 0
        Me.LabelX10.Text = "Security Role"
        '
        'chkAddEntireGroup
        '
        '
        '
        '
        Me.chkAddEntireGroup.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAddEntireGroup.Location = New System.Drawing.Point(220, 69)
        Me.chkAddEntireGroup.Name = "chkAddEntireGroup"
        Me.chkAddEntireGroup.Size = New System.Drawing.Size(200, 23)
        Me.chkAddEntireGroup.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAddEntireGroup.TabIndex = 3
        Me.chkAddEntireGroup.Text = "Add the entire group"
        '
        'cmbNTDomain
        '
        Me.cmbNTDomain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbNTDomain.FormattingEnabled = True
        Me.cmbNTDomain.Location = New System.Drawing.Point(220, 3)
        Me.cmbNTDomain.Name = "cmbNTDomain"
        Me.cmbNTDomain.Size = New System.Drawing.Size(212, 21)
        Me.cmbNTDomain.TabIndex = 2
        '
        'cmbNTGroup
        '
        Me.cmbNTGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbNTGroup.FormattingEnabled = True
        Me.cmbNTGroup.Location = New System.Drawing.Point(220, 36)
        Me.cmbNTGroup.Name = "cmbNTGroup"
        Me.cmbNTGroup.Size = New System.Drawing.Size(212, 21)
        Me.cmbNTGroup.TabIndex = 2
        '
        'cmbNTUserName
        '
        Me.cmbNTUserName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbNTUserName.FormattingEnabled = True
        Me.cmbNTUserName.Location = New System.Drawing.Point(220, 102)
        Me.cmbNTUserName.Name = "cmbNTUserName"
        Me.cmbNTUserName.Size = New System.Drawing.Size(212, 21)
        Me.cmbNTUserName.TabIndex = 2
        '
        'cmbNTUserRole
        '
        Me.cmbNTUserRole.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbNTUserRole.FormattingEnabled = True
        Me.cmbNTUserRole.Location = New System.Drawing.Point(220, 135)
        Me.cmbNTUserRole.Name = "cmbNTUserRole"
        Me.cmbNTUserRole.Size = New System.Drawing.Size(212, 21)
        Me.cmbNTUserRole.TabIndex = 2
        '
        'chkNTRIA
        '
        '
        '
        '
        Me.chkNTRIA.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNTRIA.Location = New System.Drawing.Point(220, 168)
        Me.chkNTRIA.Name = "chkNTRIA"
        Me.chkNTRIA.Size = New System.Drawing.Size(200, 23)
        Me.chkNTRIA.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkNTRIA.TabIndex = 3
        Me.chkNTRIA.Text = "RIA (Web) access enabled"
        '
        'btnNTAssignSchedules
        '
        Me.btnNTAssignSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNTAssignSchedules.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnNTAssignSchedules.Location = New System.Drawing.Point(220, 201)
        Me.btnNTAssignSchedules.Name = "btnNTAssignSchedules"
        Me.btnNTAssignSchedules.Size = New System.Drawing.Size(212, 33)
        Me.btnNTAssignSchedules.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnNTAssignSchedules.TabIndex = 4
        Me.btnNTAssignSchedules.Text = "Assigned Schedules"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 315)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(595, 30)
        Me.FlowLayoutPanel1.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(517, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "Cancel"
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(436, 3)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "OK"
        '
        'SuperTabControl1
        '
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel2)
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel1)
        Me.SuperTabControl1.Location = New System.Drawing.Point(1, 3)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = True
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.SuperTabControl1.SelectedTabIndex = 0
        Me.SuperTabControl1.Size = New System.Drawing.Size(446, 309)
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.SuperTabControl1.TabIndex = 4
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabCRD, Me.tabNT})
        Me.SuperTabControl1.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.grpNTUser)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(446, 284)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabNT
        '
        'tabNT
        '
        Me.tabNT.AttachedControl = Me.SuperTabControlPanel2
        Me.tabNT.GlobalItem = False
        Me.tabNT.Name = "tabNT"
        Me.tabNT.Text = "Windows User"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.grpNormalUser)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 25)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(446, 284)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabCRD
        '
        'tabCRD
        '
        Me.tabCRD.AttachedControl = Me.SuperTabControlPanel1
        Me.tabCRD.GlobalItem = False
        Me.tabCRD.Name = "tabCRD"
        Me.tabCRD.Text = "SQL-RD User"
        '
        'frmAddUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(595, 345)
        Me.ControlBox = False
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAddUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Information"
        Me.grpNormalUser.ResumeLayout(False)
        Me.grpNTUser.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents grpNormalUser As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFirstName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtLastName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtVerifyPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbSecurityRole As System.Windows.Forms.ComboBox
    Friend WithEvents chkAutoLogon As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNTRIA As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkRIA As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnAssignSchedules As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpNTUser As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAddEntireGroup As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbNTDomain As System.Windows.Forms.ComboBox
    Friend WithEvents cmbNTGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cmbNTUserName As System.Windows.Forms.ComboBox
    Friend WithEvents cmbNTUserRole As System.Windows.Forms.ComboBox
    Friend WithEvents btnNTAssignSchedules As DevComponents.DotNetBar.ButtonX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabNT As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabCRD As DevComponents.DotNetBar.SuperTabItem
End Class
