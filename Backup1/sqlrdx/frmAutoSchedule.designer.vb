﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAutoSchedule
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoSchedule))
        Me.automationScheduleTab = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcTasks = New sqlrd.ucTasks()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcSet = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.txtparentFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtScheduleName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtTags = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnSelectFolder = New DevComponents.DotNetBar.ButtonX()
        Me.txtDescription = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnFinish = New DevComponents.DotNetBar.ButtonX()
        Me.btnNext = New DevComponents.DotNetBar.ButtonX()
        CType(Me.automationScheduleTab, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.automationScheduleTab.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'automationScheduleTab
        '
        Me.automationScheduleTab.CloseButtonOnTabsAlwaysDisplayed = False
        Me.automationScheduleTab.CloseButtonPosition = DevComponents.DotNetBar.eTabCloseButtonPosition.Left
        '
        '
        '
        '
        '
        '
        Me.automationScheduleTab.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.automationScheduleTab.ControlBox.MenuBox.Name = ""
        Me.automationScheduleTab.ControlBox.Name = ""
        Me.automationScheduleTab.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.automationScheduleTab.ControlBox.MenuBox, Me.automationScheduleTab.ControlBox.CloseBox})
        Me.automationScheduleTab.Controls.Add(Me.SuperTabControlPanel7)
        Me.automationScheduleTab.Controls.Add(Me.SuperTabControlPanel2)
        Me.automationScheduleTab.Controls.Add(Me.SuperTabControlPanel1)
        Me.automationScheduleTab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.automationScheduleTab.Location = New System.Drawing.Point(0, 0)
        Me.automationScheduleTab.Name = "automationScheduleTab"
        Me.automationScheduleTab.ReorderTabsEnabled = False
        Me.automationScheduleTab.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.automationScheduleTab.SelectedTabIndex = 3
        Me.automationScheduleTab.Size = New System.Drawing.Size(706, 409)
        Me.automationScheduleTab.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.automationScheduleTab.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.automationScheduleTab.TabIndex = 2
        Me.automationScheduleTab.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabTasks})
        Me.automationScheduleTab.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.automationScheduleTab.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.UcTasks)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(120, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(586, 409)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabTasks
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.White
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = False
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(586, 409)
        Me.UcTasks.TabIndex = 0
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel7
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Custom Tasks"
        Me.tabTasks.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.UcSet)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(111, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(595, 409)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'UcSet
        '
        Me.UcSet.BackColor = System.Drawing.Color.Transparent
        Me.UcSet.Font = New System.Drawing.Font("Trebuchet MS", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSet.Location = New System.Drawing.Point(0, 0)
        Me.UcSet.m_collaborationServerID = 0
        Me.UcSet.m_nextRun = "2013-08-22 14:22:39"
        Me.UcSet.m_RepeatUnit = ""
        Me.UcSet.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.UcSet.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.UcSet.Name = "UcSet"
        Me.UcSet.scheduleID = 0
        Me.UcSet.scheduleStatus = True
        Me.UcSet.sFrequency = "Daily"
        Me.UcSet.Size = New System.Drawing.Size(595, 500)
        Me.UcSet.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.ReflectionImage1)
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(111, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(595, 367)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'ReflectionImage1
        '
        Me.ReflectionImage1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(412, 74)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(122, 203)
        Me.ReflectionImage1.TabIndex = 1
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.61693!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.38307!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX5, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtparentFolder, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtScheduleName, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTags, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btnSelectFolder, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDescription, 1, 3)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(10, 10)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(514, 344)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'LabelX1
        '
        Me.LabelX1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(103, 25)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Parent folder"
        '
        'LabelX3
        '
        Me.LabelX3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 34)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(103, 25)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Schedule Name"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 65)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(103, 20)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Description (optional)"
        '
        'LabelX5
        '
        Me.LabelX5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(3, 321)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(103, 20)
        Me.LabelX5.TabIndex = 0
        Me.LabelX5.Text = "Keywords (optional)"
        '
        'txtparentFolder
        '
        '
        '
        '
        Me.txtparentFolder.Border.Class = "TextBoxBorder"
        Me.txtparentFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtparentFolder.Location = New System.Drawing.Point(112, 3)
        Me.txtparentFolder.Name = "txtparentFolder"
        Me.txtparentFolder.ReadOnly = True
        Me.txtparentFolder.Size = New System.Drawing.Size(280, 20)
        Me.txtparentFolder.TabIndex = 0
        '
        'txtScheduleName
        '
        '
        '
        '
        Me.txtScheduleName.Border.Class = "TextBoxBorder"
        Me.txtScheduleName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtScheduleName.Location = New System.Drawing.Point(112, 34)
        Me.txtScheduleName.Name = "txtScheduleName"
        Me.txtScheduleName.Size = New System.Drawing.Size(280, 20)
        Me.txtScheduleName.TabIndex = 4
        '
        'txtTags
        '
        '
        '
        '
        Me.txtTags.Border.Class = "TextBoxBorder"
        Me.txtTags.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTags.Location = New System.Drawing.Point(112, 321)
        Me.txtTags.Name = "txtTags"
        Me.txtTags.Size = New System.Drawing.Size(280, 20)
        Me.txtTags.TabIndex = 6
        '
        'btnSelectFolder
        '
        Me.btnSelectFolder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSelectFolder.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSelectFolder.Location = New System.Drawing.Point(398, 3)
        Me.btnSelectFolder.Name = "btnSelectFolder"
        Me.btnSelectFolder.Size = New System.Drawing.Size(51, 23)
        Me.btnSelectFolder.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnSelectFolder.TabIndex = 1
        Me.btnSelectFolder.Text = "..."
        '
        'txtDescription
        '
        '
        '
        '
        Me.txtDescription.Border.Class = "TextBoxBorder"
        Me.txtDescription.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtDescription, 2)
        Me.txtDescription.Location = New System.Drawing.Point(112, 65)
        Me.txtDescription.Multiline = True
        Me.txtDescription.Name = "txtDescription"
        Me.txtDescription.Size = New System.Drawing.Size(284, 250)
        Me.txtDescription.TabIndex = 5
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnCancel)
        Me.Panel1.Controls.Add(Me.btnFinish)
        Me.Panel1.Controls.Add(Me.btnNext)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 409)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(706, 34)
        Me.Panel1.TabIndex = 3
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(460, 6)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "&Cancel"
        '
        'btnFinish
        '
        Me.btnFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnFinish.Enabled = False
        Me.btnFinish.Location = New System.Drawing.Point(622, 6)
        Me.btnFinish.Name = "btnFinish"
        Me.btnFinish.Size = New System.Drawing.Size(75, 23)
        Me.btnFinish.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnFinish.TabIndex = 0
        Me.btnFinish.Text = "&Finish"
        '
        'btnNext
        '
        Me.btnNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnNext.Location = New System.Drawing.Point(541, 6)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 23)
        Me.btnNext.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnNext.TabIndex = 0
        Me.btnNext.Text = "&Next"
        '
        'frmAutoSchedule
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(706, 443)
        Me.ControlBox = False
        Me.Controls.Add(Me.automationScheduleTab)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmAutoSchedule"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Automation Schedule"
        CType(Me.automationScheduleTab, System.ComponentModel.ISupportInitialize).EndInit()
        Me.automationScheduleTab.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents automationScheduleTab As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtparentFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtScheduleName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDescription As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtTags As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnSelectFolder As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents UcSet As sqlrd.ucSchedule
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnNext As DevComponents.DotNetBar.ButtonX
End Class
