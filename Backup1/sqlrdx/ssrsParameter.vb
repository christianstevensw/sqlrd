﻿Imports System.Collections.Generic

Public Class ssrsParameter
    ''' <summary>
    ''' Defines an instance of an SSRS parameter and all its properties. This class is not concerned with what the values are set to.
    ''' </summary>
    ''' <param name="name"></param>
    ''' <param name="type"></param>
    ''' <remarks></remarks>
    Sub New(name As String, type As String)
        parameterName = name
        parameterType = type
    End Sub

    Public Property parameterName As String

    Public Property parameterType As String

    Public Property parameterPrompt As String

    Public Property parameterAllowMultipleValue As Boolean

    Public Property parameterHidden As Boolean

    Public Property parameterAvailableValues As System.Collections.Generic.List(Of KeyValuePair(Of String, String))

End Class
