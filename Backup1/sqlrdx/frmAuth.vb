﻿Public Class frmAuth
    Dim cancel As Boolean = True

    Public Function getAuthInfo(ByRef username As String, ByRef password As String, ByRef domain As String) As DialogResult
        txtUsername.Text = username
        txtPassword.Text = password
        txtDomain.Text = domain

        Me.ShowDialog()

        If cancel = False Then
            username = txtUsername.Text
            password = txtPassword.Text
            domain = txtDomain.Text

            Return DialogResult.OK
        Else
            Return DialogResult.Cancel
        End If
    End Function

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        cancel = False
        Close()
    End Sub

    Private Sub frmAuth_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub
End Class