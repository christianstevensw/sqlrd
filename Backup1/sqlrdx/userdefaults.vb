﻿Public Class userdefaults
    Dim oRs As ADODB.Recordset

    Sub New()
        oRs = clsMarsData.GetData("SELECT * FROM userdefaultsattr")

        If oRs Is Nothing Then
            Throw New Exception("Could not initialize UserDefaults class")
        End If
    End Sub
    Public Shared Property shareUserDefaults() As Boolean
        Get
            Return Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ShareUserDefaults", 0)))
        End Get
        Set(ByVal value As Boolean)
            clsMarsUI.MainUI.SaveRegistry("ShareUserDefaults", Convert.ToInt32(value))
        End Set
    End Property

    'txtDefSubject.Text = IsNull(ors("emailsubject").Value)
    '       txtDefAttach.Text = IsNull(ors("emailattachment").Value)
    '       txtDefaultEmail.Text = IsNull(ors("emailmessage").Value)
    '       txtSig.Text = IsNull(ors("emailsignature").Value)
    '       UcLogin.txtUserID.Text = IsNull(ors("dbuserid").Value)
    '       UcLogin.txtPassword.Text = IsNull(ors("dbpassword").Value)

    Public ReadOnly Property emailSubject() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("emailsubject").Value) Else Return ""
        End Get
    End Property

    Public ReadOnly Property emailAttachment() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("emailattachment").Value) Else Return ""
        End Get
    End Property

    Public ReadOnly Property emailMessage() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("emailmessage").Value) Else Return ""
        End Get
    End Property

    Public ReadOnly Property emailSignature() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("emailsignature").Value) Else Return ""
        End Get
    End Property

    Public ReadOnly Property UserID() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("dbuserid").Value) Else Return ""
        End Get
    End Property

    Public ReadOnly Property Password() As String
        Get
            If oRs.EOF = False Then Return IsNull(oRs("dbpassword").Value) Else Return ""
        End Get
    End Property

    Sub Dispose()
        Try
            oRs.Close()
            oRs = Nothing
        Catch :  End Try
    End Sub
End Class
