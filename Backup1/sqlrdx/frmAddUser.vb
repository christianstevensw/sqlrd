﻿Public Class frmAddUser
    Dim cancel As Boolean = False
    Dim oEdit As Boolean

    Private m_userMode As e_mode

    Public Property userMode() As e_mode
        Get
            Return m_userMode
        End Get
        Set(ByVal value As e_mode)
            m_userMode = value

            Select Case value
                Case e_mode.NORMAL
                    tabCRD.Visible = True
                    tabNT.Visible = False
                Case e_mode.NT
                    tabNT.Visible = True
                    tabCRD.Visible = False

                    cmbNTDomain.Text = Environment.UserDomainName
            End Select
        End Set
    End Property



    Public Enum e_mode
        NORMAL = 0
        NT = 1
    End Enum

    Private Sub btnCancel_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        cancel = True
        Close()
    End Sub

    Public Sub addUser()
        oEdit = False

        Me.ShowDialog()
    End Sub

    Public Sub editUser(userid As String)
        oEdit = True

        Dim user As cssusers.normalUser = New cssusers.normalUser(userid)

        txtFirstName.Text = user.firstName
        txtLastName.Text = user.lastName
        txtUserID.Text = userid
        txtUserID.Enabled = False
        txtPassword.Text = user.password
        txtVerifyPassword.Text = txtPassword.Text
        cmbSecurityRole.Text = user.userRole
        chkAutoLogon.Checked = user.autoLogin
        chkRIA.Checked = user.webEnabled

        If cmbSecurityRole.Text.ToLower <> "administrator" Then
            btnAssignSchedules.Enabled = True
        Else
            btnAssignSchedules.Enabled = False
        End If

        user.dispose()

        Me.ShowDialog()
    End Sub

    Public Sub editNTUser(userid As String)
        oEdit = True

        Dim user As cssusers.NTDomainUser = New cssusers.NTDomainUser(userid)

        cmbNTDomain.Text = user.domainName
        cmbNTGroup.Enabled = False
        chkAddEntireGroup.Enabled = False
        cmbNTUserName.Text = userid.Split("\")(1)
        cmbNTUserName.Enabled = False
        cmbNTUserRole.Text = user.userRole
        chkNTRIA.Checked = user.webEnabled


        If cmbNTUserRole.Text.ToLower <> "administrator" Then
            btnNTAssignSchedules.Enabled = True
        Else
            btnNTAssignSchedules.Enabled = False
        End If


        Me.ShowDialog()
    End Sub

    Public Sub addNTUser()
        oEdit = False
        Me.ShowDialog()
    End Sub
    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Dim user As clsMarsUsers = New clsMarsUsers

        Select Case userMode
            Case e_mode.NORMAL
                If validateControl(txtFirstName, "Please enter the first name") = False Then
                    Return
                ElseIf validateControl(txtLastName, "Please enter the last name") = False Then
                    Return
                ElseIf validateControl(txtUserID, "Please enter the userid") = False Then
                    Return
                ElseIf validateControl(txtPassword, "Please enter the password") = False Then
                    Return
                ElseIf validateControl(cmbSecurityRole, "Please select the security role") = False Then
                    Return
                ElseIf validateControl(txtUserID, "You cannot use this user id as its used by the system", "sqlrdadmin") = False Then
                    Return
                ElseIf txtPassword.Text <> txtVerifyPassword.Text Then
                    setError(txtPassword, "The passwords provided do not match. You must enter them again")
                    txtPassword.Text = ""
                    txtVerifyPassword.Text = ""
                    txtPassword.Focus()
                    Return
                Else
                    If user.AddUser(txtUserID.Text, txtPassword.Text, txtFirstName.Text, txtLastName.Text, cmbSecurityRole.Text, chkAutoLogon.Checked, chkRIA.Checked) = True Then
                        Close()
                    Else
                        MessageBox.Show("Failed to add the user. Please check that the user does not already exist in the system", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                End If
            Case e_mode.NT
                If validateControl(cmbNTDomain, "Please select the domain") = False Then
                    Return
                ElseIf validateControl(cmbNTUserName, "Please select the domain user") = False Then
                    Return
                ElseIf validateControl(cmbNTUserRole, "Please select the security role") = False Then
                    Return
                End If

                Dim SQL As String
                Dim sCols As String
                Dim sVals As String
                Dim fullDomainName As String = cmbNTDomain.Text & "\" & cmbNTUserName.Text

                If oEdit = False Then
                    sCols = "DomainID,DomainName,UserRole,LastModDate,LastModBy,lokiEnabled,UserNumber"

                    sVals = clsMarsData.CreateDataID("domainattr", "domainid") & "," & _
                    "'" & fullDomainName & "'," & _
                    "'" & SQLPrepare(cmbNTUserRole.Text) & "'," & _
                    "'" & ConDateTime(Date.Now) & "'," & _
                    "'" & gUser & "'," & _
                    Convert.ToInt32(chkNTRIA.Checked) & "," & _
                    clsMarsData.GetNewUserNumber("DomainAttr")

                    SQL = "INSERT INTO DomainAttr (" & sCols & ") VALUES (" & sVals & ")"
                Else
                    SQL = "UPDATE DomainAttr SET " & _
                    "UserRole ='" & SQLPrepare(cmbNTUserRole.Text) & "'," & _
                    "LastModDate='" & ConDateTime(Date.Now) & "'," & _
                    "LastModBy ='" & gUser & "', " & _
                    "LokiEnabled = " & Convert.ToInt32(chkNTRIA.Checked) & " " & _
                    "WHERE DomainName ='" & fullDomainName & "'"
                End If

                If clsMarsData.WriteData(SQL) = True Then
                    If chkAddEntireGroup.Checked And cmbNTGroup.Text <> "" Then
                        If Not clsMarsData.IsDuplicate("groupattr", "groupname", cmbNTGroup.Text, False) Then
                            Dim nGroupID As Integer = clsMarsData.CreateDataID("groupattr", "groupid")

                            sCols = "GroupID,GroupName,GroupDesc"

                            sVals = nGroupID & "," & _
                            "'" & SQLPrepare(cmbNTGroup.Text) & "'," & _
                            "''"

                            SQL = "INSERT INTO GroupAttr (" & sCols & ") VALUES (" & sVals & ")"

                            clsMarsData.WriteData(SQL)

                        End If
                    End If
                End If
        End Select


        Close()
    End Sub

    Private Function validateControl(sender As Control, message As String, Optional checkValue As String = "") As Boolean
        If sender.Text.ToLower = checkValue.ToLower Then
            setError(sender, message, ErrorIconAlignment.MiddleLeft)
            sender.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cmbSecurityRole_DropDown(sender As Object, e As System.EventArgs) Handles cmbSecurityRole.DropDown, cmbNTUserRole.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oCombo As ComboBox

        oCombo = CType(sender, ComboBox)

        oCombo.Items.Clear()

        SQL = "SELECT GroupName FROM GroupAttr"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oCombo.Items.Add(oRs(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Dim s As String() = New String() {"Administrator", "User"}

        oCombo.Items.AddRange(s)


    End Sub

    Private Sub cmbNTDomain_DropDown(sender As Object, e As System.EventArgs) Handles cmbNTDomain.DropDown
        If cmbNTDomain.Items.Count = 0 Then
            cmbNTDomain.Items.Add(Environment.UserDomainName)
        End If
    End Sub

    Private Sub cmbNTGroup_DropDown(sender As System.Object, e As System.EventArgs) Handles cmbNTGroup.DropDown
        Try
            If cmbNTGroup.Items.Count > 0 Or cmbNTDomain.Text.Length = 0 Then Return

            AppStatus(True)

            Dim oDomain, Member As Object

            oDomain = GetObject("WinNT://" & cmbNTDomain.Text)

            For Each Member In oDomain
                If Member.Class = "Group" Then
                    cmbNTGroup.Items.Add(Member.Name)
                End If
            Next

            AppStatus(False)
        Catch : End Try
    End Sub

    Private Sub cmbNTGroup_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cmbNTGroup.SelectedIndexChanged
        Try

            Dim oGroup, Member As Object
            Dim sMyParent As String

            If cmbNTGroup.Text.Length = 0 Or cmbNTDomain.Text.Length = 0 Then Return

            oGroup = GetObject("WinNT://" & cmbNTDomain.Text & "/" & cmbNTGroup.Text)

            cmbNTUserName.Items.Clear()

            For Each Member In oGroup.Members
                sMyParent = Member.Parent
                sMyParent = Microsoft.VisualBasic.Right(sMyParent, Len(sMyParent) - InStrRev(sMyParent, "/"))

                If Member.Class = "User" Then
                    cmbNTUserName.Items.Add(Member.Name)
                End If
            Next

            setError(sender, "")
        Catch : End Try
    End Sub

    Private Sub chkAddEntireGroup_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAddEntireGroup.CheckedChanged
        If chkAddEntireGroup.Checked Then
            cmbNTUserName.Text = cmbNTGroup.Text & ".*"
            cmbNTUserName.Enabled = False

            If cmbNTUserRole.Text = "" And cmbNTGroup.Text <> "" Then cmbNTUserRole.Text = cmbNTGroup.Text

            chkNTRIA.Checked = False
            chkNTRIA.Enabled = False
        Else
            cmbNTUserName.Enabled = True
            If cmbNTUserName.Text.Contains("*") Then cmbNTUserName.Text = ""

            chkNTRIA.Enabled = True
        End If
    End Sub

    Private Sub chkNTRIA_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkNTRIA.CheckedChanged, chkRIA.CheckedChanged
        Dim message As String = ""
        Dim chk As DevComponents.DotNetBar.Controls.CheckBoxX = CType(sender, DevComponents.DotNetBar.Controls.CheckBoxX)

        If chk.Checked = True Then
            If clsMarsUsers.canEnableUserForLoki(message, sender) = False Then
                If message <> "" Then MessageBox.Show(message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                chk.Checked = False
                Return
            End If
        End If
    End Sub

    Private Sub btnAssignSchedules_Click(sender As System.Object, e As System.EventArgs) Handles btnAssignSchedules.Click
        If txtUserID.Text = "" Then Return

        If cmbNTUserRole.Text = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(txtUserID.Text)
    End Sub

    Private Sub txtUserID_TextChanged(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserID.KeyPress
        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) Then
            e.Handled = True
        End If
    End Sub

    Private Sub btnNTAssignSchedules_Click(sender As System.Object, e As System.EventArgs) Handles btnNTAssignSchedules.Click
        If cmbNTUserName.Text = "" Then Return

        If cmbNTUserRole.Text.ToLower = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(cmbNTDomain.Text & "\" & cmbNTUserName.Text)
    End Sub

    Private Sub cmbSecurityRole_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbSecurityRole.SelectedIndexChanged
        If cmbSecurityRole.Text.ToLower = "administrator" Then
            btnAssignSchedules.Enabled = False
        Else
            btnAssignSchedules.Enabled = True
        End If
    End Sub

    Private Sub cmbSecurityRole_Validated(sender As Object, e As System.EventArgs) Handles cmbSecurityRole.Validated
        Dim items As ComboBox.ObjectCollection = Me.cmbSecurityRole.Items

        If items.Contains(cmbSecurityRole.Text) = False Then
            Me.cmbSecurityRole.Text = ""
        End If
    End Sub

    Private Sub cmbNTUserRole_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbNTUserRole.SelectedIndexChanged
        If cmbNTUserRole.Text.ToLower = "administrator" Then
            btnNTAssignSchedules.Enabled = False
        Else
            btnNTAssignSchedules.Enabled = True
        End If
    End Sub

    Private Sub cmbNTUserRole_Validated(sender As Object, e As System.EventArgs) Handles cmbNTUserRole.Validated
        Dim items As ComboBox.ObjectCollection = Me.cmbNTUserRole.Items

        If items.Contains(cmbNTUserRole.Text) = False And chkAddEntireGroup.Checked = False Then
            Me.cmbNTUserRole.Text = ""
        End If
    End Sub

    Private Sub LabelX2_Click(sender As System.Object, e As System.EventArgs) Handles LabelX2.Click

    End Sub

    Private Sub frmAddUser_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub
End Class