﻿Public Class frmContentSelector
    Dim m_workstationID As String
    Dim m_connection As ADODB.Connection

    Public Sub selectContent(ByVal connection As ADODB.Connection)
        If hideKhov Then Return

        m_connection = connection

        If connection Is Nothing Then
            Return
        ElseIf connection.State <> 1 Then
            Return
        End If

        Me.ShowDialog()
    End Sub
    Private Sub copySetting(ByVal settingName As String, ByVal defaultValue As String)
        Dim serverPath As String = "\\" & m_workstationID & "\ChristianSteven\SQLRD"
        Dim configPath As String = "\\" & m_workstationID & "\ChristianSteven\SQLRD\sqlrdlive.config"

        clsMarsUI.MainUI.SaveRegistry(settingName, clsMarsUI.MainUI.ReadRegistry(settingName, defaultValue, False, configPath, True), False, , True)
    End Sub
    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        Try
            Dim oRs As ADODB.Recordset = New ADODB.Recordset ' clsMarsData.GetData("SELECT workstationid FROM schedulerattr")

            oRs.Open("SELECT workstationid FROM schedulerattr", m_connection, ADODB.CursorTypeEnum.adOpenForwardOnly)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                m_workstationID = oRs(0).Value

                oRs.Close()
            Else
                MessageBox.Show("No scheduler is set up. Configuration data cannot be migrated until a scheduler is set up on the main server", Application.ProductName, _
                                  MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Close()
                Return
            End If

            Dim serverPath As String = "\\" & m_workstationID & "\ChristianSteven\SQLRD\"
            Dim configPath As String = "\\" & m_workstationID & "\ChristianSteven\SQLRD\sqlrdlive.config"

            '//login type is always copied
            copySetting("unifiedlogin", 0)

            If chkGeneral.Checked Then
                copySetting("Poll", 30)
                copySetting("CustNo", "")
                copySetting("AlertWho", "")
                copySetting("DoNotAutoCompact", 1)
                copySetting("RunOutOfProcess", 0)
                copySetting("DoNotUNC", 0)
                copySetting("ShowHiddenParameters", 0)
                copySetting("ErrorByLog", 1)
                copySetting("ErrorByMail", 0)
                copySetting("ErrorBySMS", 0)
                copySetting("SMSAlertWho", "")
            End If

            If chkUserDefaults.Checked Then
                'user defaults
                IO.File.Copy(serverPath & "defmsg.sqlrd", sAppPath & "defmsg.sqlrd", True)
                IO.File.Copy(serverPath & "defsig.sqlrd", sAppPath & "defsig.sqlrd", True)
                IO.File.Copy(serverPath & "url.dat", sAppPath & "url.dat", True)

                copySetting("DefSubject", "")
                copySetting("DefAttach", "")
                copySetting("Archive", 0)
                copySetting("ArchiveAfter", 30)
                copySetting("DefDateTimeStamp", "")
                copySetting("DefDBUser", "")
                copySetting("DefPassword", "")
                copySetting("ReportsLoc", "")
            End If

            If chkSystemPaths.Checked Then
                'system paths
                '//copySetting ("","")
                copySetting("CachedDataPath", "")
                copySetting("SnapshotsPath", "")
                copySetting("CachePath", "")
                copySetting("SentMessagesFolder", "")
                copySetting("ConvertToMsg", 0)
            End If

            If chkAudit.Checked Then
                copySetting("AuditDSN", "")
                copySetting("AuditUserName", "")
                copySetting("AuditPassword", "")
                copySetting("AuditTrail", 0)
            End If

            If chkMessaging.Checked Then
                copySetting("MailType", "")
                copySetting("MAPIProfile", "")
                copySetting("MAPIPassword", "")
                copySetting("MAPIType", "")

                copySetting("EmailRetry", 0)
                copySetting("SMTPUserID", "")
                copySetting("SMTPPassword", "")
                copySetting("SMTPServer", "")
                copySetting("SMTPSenderAddress", "")
                copySetting("SMTPSenderName", "")
                copySetting("SMTPTimeout", 60)
                copySetting("SMTPPort", 25)

                copySetting("SQL-RDSender", "")
                copySetting("SQL-RDAddress", "")
                copySetting("GWUser", "")
                copySetting("GWPassword", "")
                copySetting("GWProxy", "")
                copySetting("GWPOIP", "")
                copySetting("GWPOPort", "")


                copySetting("CharSetEncoding", "")
                copySetting("SMSDevice", "")
                copySetting("SMSDataFormat", "")
                copySetting("SMSSpeed", "")
                copySetting("SMSSInit", "")
                copySetting("SMSDeviceType", "")
                copySetting("SMSCNumber", "")
                copySetting("SMSCProtocol", "")
                copySetting("SMSCPassword", "")
                copySetting("SMSSender", "")
                copySetting("UseUnicode", "")
            End If

            Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "The process will continue without copying the data.")
            Close()
        End Try
    End Sub

  
    Private Sub frmContentSelector_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub
End Class