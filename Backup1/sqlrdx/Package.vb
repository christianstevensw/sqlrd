﻿Public Class Package
    Dim m_packid As Integer
    Dim m_concernedRow As ADODB.Recordset
    Public m_schedule As Schedule
    Dim m_newPackage As Boolean

    Sub New(ByVal packid As Integer)
        m_packid = packid

        m_schedule = New Schedule(packid, Schedule.parentscheduleType.PACKAGE)
    End Sub

    Sub New(ByVal packid As Integer, ByVal newPackage As Boolean)
        m_packid = packid
        m_newPackage = newPackage
    End Sub

    Public ReadOnly Property ID As Integer
        Get
            Return m_packid
        End Get
    End Property
    Public ReadOnly Property maxPackOrderID As Integer
        Get
            Dim SQL As String = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & ID

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            Dim maxOrderID As Integer = 0

            If oRs IsNot Nothing Then
                maxOrderID = IsNull(oRs(0).Value, 0)
            Else
                maxOrderID = 0
            End If

            Return maxOrderID
        End Get
    End Property
    Private ReadOnly Property packagedReports() As DataTable
        Get
            Dim listingTable As DataTable = New DataTable("packagedreports")

            With listingTable.Columns
                .Add("ID")
                .Add("Name")
                .Add("Format")
            End With

            Return listingTable
        End Get
    End Property

    Public ReadOnly Property schedulePath As String
        Get
            Dim parent As Integer = Me.parent
            Dim folder As folder = New folder(parent)

            Return folder.getFolderPath()
        End Get
    End Property
    Public ReadOnly Property concernedRow() As ADODB.Recordset
        Get
            If m_concernedRow Is Nothing Then

                m_concernedRow = clsMarsData.GetData("SELECT * FROM packageattr WHERE packid =" & m_packid)

                If m_concernedRow IsNot Nothing AndAlso m_concernedRow.EOF = False Then
                    Return m_concernedRow
                Else
                    Throw New Exception("Could not locate Package schedule referenced by ID: " & m_packid)
                    Return Nothing
                End If
            Else
                Return m_concernedRow
            End If
        End Get
    End Property
    Public ReadOnly Property isDynamic() As Boolean
        Get
            Return IsNull(concernedRow.Fields("dynamic").Value, 0)
        End Get
       
    End Property

    Public ReadOnly Property isDataDriven() As Boolean
        Get
            Return IsNull(concernedRow.Fields("isdatadriven").Value, 0)
        End Get
      
    End Property

    Public ReadOnly Property m_status() As Boolean
        Get
            Return m_schedule.status
        End Get
    End Property

    Public ReadOnly Property staticDest() As Boolean
        Get
            Return Convert.ToInt32(IsNull(concernedRow.Fields("staticdest").Value, 0))
        End Get
      
    End Property

    Public ReadOnly Property selectedImageFileUrl() As String
        Get
            Dim imageFile As String = "box.png"

            If m_status = True Then
                If isDynamic = True Then
                    imageFile = "box_new.png"
                ElseIf isDataDriven = True Then
                    imageFile = "data_driven_box.png"
                Else
                    imageFile = "box.png"
                End If
            Else
                If isDynamic = True Then
                    imageFile = "box_new_bw.png"
                ElseIf isDataDriven = True Then
                    imageFile = "data_driven_box_bw.png"
                Else
                    imageFile = "box_bw.png"
                End If
            End If

            Return "images/packages/" + imageFile
        End Get
    End Property

    Public ReadOnly Property packageImage() As Image
        Get
            Dim pImage As Image = My.Resources.box

            If isDynamic = True Then
                pImage = My.Resources.dynamic_package1
            ElseIf isDataDriven = True Then
                pImage = My.Resources.data_driven_package1
            Else
                pImage = My.Resources.box2
            End If

            If m_status = False Then

                pImage = MakeGrayscale3(pImage)
                
            End If

            Return pImage
        End Get
    End Property

    Public ReadOnly Property largeImageFileUrl() As String
        Get
            Dim imageFile As String = "box.png"

            If m_status = True Then
                If isDynamic = True Then
                    imageFile = "box_new.png"
                ElseIf isDataDriven = True Then
                    imageFile = "data_driven_box.png"
                Else
                    imageFile = "box.png"
                End If
            Else
                If isDynamic = True Then
                    imageFile = "box_new_bw.png"
                ElseIf isDataDriven = True Then
                    imageFile = "data_driven_box_bw.png"
                Else
                    imageFile = "box_closed_bw.png"
                End If
            End If

            Return "largeimages/packages/" + imageFile
        End Get
    End Property
    Public Property packageName() As String
        Get
            Return concernedRow.Fields("packagename").Value
        End Get
        Set(ByVal value As String)
            concernedRow.Fields("packagename").Value = value
        End Set
    End Property
    Public ReadOnly Property packageType() As String
        Get
            If isDataDriven = True Then
                Return "Data-Driven Package"
            ElseIf isDynamic = True Then
                Return "Dynamic Package"
            Else
                Return "Package Schedule"
            End If
        End Get
    End Property
    Public ReadOnly Property owner() As String
        Get
            Return concernedRow.Fields("owner").Value
        End Get
       
    End Property

    Public ReadOnly Property lastRun() As String
        Get
            Return m_schedule.lastRun
        End Get
    End Property

    Public ReadOnly Property lastResult() As String
        Get
            Return m_schedule.lastResult
        End Get
    End Property

    Public ReadOnly Property nextRun() As Date
        Get
            Return m_schedule.nextRun
        End Get
    End Property

    Public ReadOnly Property parent() As Integer
        Get
            Return concernedRow.Fields("parent").Value
        End Get
       
    End Property
    Public ReadOnly Property reportParent() As Integer
        Get
            Return concernedRow.Fields("parent").Value
        End Get
       
    End Property
    Public ReadOnly Property autofailafter() As Integer
        Get
            Return concernedRow.Fields("assumefail").Value
        End Get
       
    End Property

    Public ReadOnly Property autocalculculateduration() As Boolean
        Get
            Return IsNull(concernedRow.Fields("autocalc").Value, True)
        End Get
        
    End Property
    Public ReadOnly Property retryinterval() As Integer
        Get
            Return IsNull(concernedRow.Fields("retryinterval").Value, 5)
        End Get
        
    End Property

    Public ReadOnly Property retrytimes() As Integer
        Get
            Return concernedRow.Fields("retry").Value
        End Get
       
    End Property

    Public ReadOnly Property checkreportshavedata() As Boolean
        Get
            Return concernedRow.Fields("checkblank").Value
        End Get
    End Property

   
    Public ReadOnly Property failonanyfailure() As Boolean
        Get
            Return concernedRow.Fields("failonone").Value
        End Get
      
    End Property

    Public ReadOnly Property mergePDFFiles() As Boolean
        Get
            Return concernedRow.Fields("mergepdf").Value
        End Get
       
    End Property

    Public ReadOnly Property mergedPDFName() As String
        Get
            Return concernedRow.Fields("mergepdfname").Value
        End Get
       
    End Property

    Public ReadOnly Property mergeExcelFiles() As Boolean
        Get
            Return concernedRow.Fields("mergexl").Value
        End Get
       
    End Property

    Public ReadOnly Property mergedExcelName() As String
        Get
            Return concernedRow.Fields("mergexlname").Value
        End Get
       
    End Property

    Public ReadOnly Property multiThreaded() As Boolean
        Get
            Return concernedRow.Fields("multithreaded").Value
        End Get
        
    End Property

    Public ReadOnly Property dateStamp() As Boolean
        Get
            Return concernedRow.Fields("datatimestamp").Value
        End Get
       
    End Property

    Public ReadOnly Property datetimeStampFormat() As String
        Get
            Return concernedRow.Fields("stampformat").Value
        End Get
       
    End Property


    Public ReadOnly Property destinations() As destination()
        Get
            Dim dests() As destination = Nothing
            Dim I As Integer = 0
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM destinationattr WHERE packid =" & m_packid)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve dests(I)

                    dests(I) = New destination(oRs("destinationid").Value)

                    I += 1

                    oRs.MoveNext()
                Loop

                oRs.Close()
                Return dests
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Function getReportsinPackage() As ArrayList
        Dim reports As ArrayList = New ArrayList
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM reportattr WHERE packid = " & m_packid & " ORDER BY packorderid ASC")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim rpt As cssreport = New cssreport(oRs("reportid").Value, m_packid)
                reports.Add(rpt)
                oRs.MoveNext()
            Loop

            oRs.Close()
            oRs = Nothing
        End If

        Return reports
    End Function

    Public Function totalReports() As Integer
        Return totalReportsLive()
    End Function

    Public Function totalReportsLive() As Integer
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM reportattr WHERE packid = " & m_packid)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim count As Integer = oRs(0).Value

            oRs.Close()

            Return count
        End If
    End Function

    Public Function totalReportsEnabledLive() As Integer
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM packagedreportattr WHERE packid = " & m_packid & " AND status =1")

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim count As Integer = oRs(0).Value

            oRs.Close()

            Return count
        End If
    End Function

    Public ReadOnly Property runExceptionTasksIfAllReportsAreBlank As Boolean
        Get
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT allreportsblankonly FROM blankreportalert WHERE packid =" & m_packid)
            Dim bool As Boolean

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                bool = IsNull(oRs("allreportsblankonly").Value, 0)
            End If

            Return bool
        End Get
    End Property
End Class
