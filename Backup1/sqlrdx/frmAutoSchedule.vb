﻿Public Class frmAutoSchedule
    Dim ep As ErrorProvider = New ErrorProvider
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        closeFlag = False

        Select Case automationScheduleTab.SelectedTab.Name
            Case "tabGeneral"
                If txtparentFolder.Text = "" Then
                    Dim txt As TextBox = txtparentFolder
                    ep.SetIconAlignment(txt, ErrorIconAlignment.BottomLeft)
                    SetError(txt, "Please select the folder to store this schedule")
                    txt = Nothing
                    Return
                ElseIf txtScheduleName.Text = "" Then
                    Dim txt As TextBox = txtScheduleName
                    ep.SetIconAlignment(txt, ErrorIconAlignment.BottomLeft)
                    SetError(txt, "Please specify the schedule name")
                    txt = Nothing
                    Return
                Else
                    Dim fld As folder = New folder(txtparentFolder.Tag)

                    If fld.doesFolderContain(txtScheduleName.Text, clsMarsScheduler.enScheduleType.REPORT) = True Then
                        Dim txt As TextBox = txtScheduleName
                        ep.SetIconAlignment(txt, ErrorIconAlignment.BottomLeft)
                        SetError(txt, "The specified schedule name is already in use. Please use a different name")
                        txt = Nothing
                        Return
                    End If

                    fld = Nothing
                End If
            Case "tabSchedule"
                If UcSet.isAllDataValid = False Then
                    Return
                Else
                    btnFinish.Enabled = True
                    btnNext.Enabled = False
                End If

        End Select


        Try
            '//get the current tab index
            Dim index As Integer = automationScheduleTab.SelectedTabIndex

            If index = automationScheduleTab.Tabs.Count - 1 Then
                btnNext.Enabled = False
                btnFinish.Enabled = True
            Else
                Dim nextTab As DevComponents.DotNetBar.SuperTabItem = automationScheduleTab.Tabs(index + 1)

                nextTab.Visible = True

                automationScheduleTab.SelectedTab = nextTab
            End If

        Catch : End Try
    End Sub

    Private Sub btnSelectFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectFolder.Click
        Dim folders As frmFolders = New frmFolders
        Dim folderPath As String
        Dim folderid As Integer

        If folders.GetFolder("Please select where to store this schedule", folderPath, folderid) Then
            txtparentFolder.Text = folderPath
            txtparentFolder.Tag = folderid
        End If
    End Sub

    Dim closeFlag As Boolean = False

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(btnCancel, spi)
            sp.ShowTooltip(btnCancel)
            closeFlag = True
        Else
            Close()
        End If
    End Sub

    Private Sub btnFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinish.Click
        If UcTasks.tvTasks.Nodes.Count = 0 Then
            MessageBox.Show("Please add a custom task", Application.ProductName, MessageBoxButtons.OK)
            Return
        End If

        btnFinish.Enabled = False

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim ExecuteOk As Boolean

        Dim AutoID As Integer

        AutoID = clsMarsData.CreateDataID("automationattr", "autoid")

        sCols = "AutoID, AutoName, Parent,Owner"

        sVals = AutoID & ", '" & SQLPrepare(txtScheduleName.Text) & "'," & _
            txtparentFolder.Tag & "," & _
            "'" & gUser & "'"

        SQL = "INSERT INTO AutomationAttr (" & sCols & ") VALUES (" & sVals & ")"

        ExecuteOk = clsMarsData.WriteData(SQL)


        If ExecuteOk = False Then GoTo RollbackTrans

        Dim ScheduleID As Int64 = UcSet.saveSchedule(AutoID, clsMarsScheduler.enScheduleType.AUTOMATION, txtDescription.Text, txtTags.Text)

        If ScheduleID = 0 Then GoTo RollbackTrans

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        If gUser IsNot Nothing Then
            If gUser.ToLower <> "admin" Then
                Dim oUser As New clsMarsUsers

                oUser.AssignView(AutoID, gUser, clsMarsUsers.enViewType.ViewAutomation)
            End If
        End If

        Close()

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtparentFolder.Tag
        frmMainWin.itemToSelect.itemName = txtScheduleName.Text


        clsMarsAudit._LogAudit(txtScheduleName.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.CREATE)
        Return
RollbackTrans:
        clsMarsData.WriteData("DELETE FROM AutomationAttr WHERE AutoID =" & AutoID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE AutoID =" & AutoID)
        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID =" & ScheduleID)
        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
        btnFinish.Enabled = True
    End Sub

  
    Private Sub frmAutoSchedule_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        If gParentID > 0 And gParent.Length > 0 Then
            Dim fld As folder = New folder(gParentID)
            txtparentFolder.Text = fld.getFolderPath
            txtparentFolder.Tag = gParentID
        End If
    End Sub

    Private Sub txtScheduleName_TextChanged(sender As Object, e As EventArgs) Handles txtScheduleName.TextChanged
        gScheduleName = txtScheduleName.Text
    End Sub
End Class