﻿Public Class frmCollaborators
    Dim cacheUser, cachePassword, cacheDomain As String

    Private Function getandkillRemoteProcs(ByVal pcname As String, ByRef username As String, ByRef password As String, ByRef domain As String) As Boolean

        Cursor.Current = Cursors.WaitCursor

        Dim procs() As String = New String() {"sqlrd.exe", "sqlrdapp.exe", "sqlrdsvc.exe"}
        Dim auther As frmAuth = New frmAuth

        If cacheUser <> "" Then
            username = cacheUser
            password = cachePassword
            domain = cacheDomain
        Else
            username = Environment.UserName
            domain = Environment.UserDomainName
            password = ""
        End If


        If auther.getAuthInfo(username, password, domain) = DialogResult.Cancel Then
            Return False
        End If

        cacheUser = username
        cachePassword = password
        cacheDomain = domain

        Try

            For Each proc As String In procs
                Dim remoteProc As remoteProcessController.controller = New remoteProcessController.controller
                Dim errInfo As Exception
                Dim dt As DataTable

                dt = remoteProc.ConnectToRemoteMachine(domain, pcname, username, password, True, errInfo)

                If dt Is Nothing Then
                    If errInfo Is Nothing Then errInfo = New Exception("Cannot access remote machine. Access is denied")
                    Throw errInfo
                End If

                Dim num As Integer = dt.Select("processName LIKE '%" & SQLPrepare(proc) & "%'").Length

                If num > 0 Then
                    If remoteProc.terminateRemoteProcess(proc, domain, pcname, username, password, True, errInfo) = False Then
                        If errInfo Is Nothing Then errInfo = New Exception("Cannot terminate process " & proc & ". Access denied.")
                        Throw errInfo
                    End If
                End If
            Next

            Return True
        Catch ex As Exception
            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl(), "Please make sure you have full access rights to the machine")
            Return False
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Function
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the SQL-RD configuration file on the machine you would like to add to the Collaboration ring"
        ofd.Filter = "sqlrdlive.config|sqlrdlive.config"


        If ofd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Dim fullPath As String = ofd.FileName '\\mallard\blah\blah

            If fullPath.StartsWith("\\") = False Then
                MessageBox.Show("Please use a UNC path", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return

            End If

            Dim pcname As String = fullPath.Split("\")(2)

            If lsvCollabo.FindItemWithText(pcname) Is Nothing Then

                If MessageBox.Show("The server " & pcname & " will be added to your Collaboration group and will no longer process local schedules. It will connect to your central SQL-RD database and " & _
                                    "process schedules assigned to it." & vbCrLf & _
                                    "The SQL-RD editor and scheduler on the remote server will be terminated. Proceed?", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.Cancel Then
                    Return
                End If


                '//check that the user is not already part of another collaboration ring
                Dim iscollabo As Boolean

                Try
                    iscollabo = clsMarsUI.MainUI.ReadRegistry("collaboration", 0, , fullPath, True)
                Catch
                    iscollabo = False
                End Try

                If iscollabo Then
                    Dim leader As String = clsMarsUI.MainUI.ReadRegistry("collaborationLeader", "", True)
                    MessageBox.Show("The server you are attempting to add to Collaboration is already enabled for collaboration under " & leader.ToUpper, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If

                Dim username, password, domain As String

                If getandkillRemoteProcs(pcname, username, password, domain) = False Then
                    Dim proceed As DialogResult = MessageBox.Show("SQL-RD could not access the process list on the remote server. Please make sure that you have closed SQL-RD and the scheduler on the remote server before proceeding." & vbCrLf & _
                                                                  "Click OK to proceed or Cancel to abort.", _
                                                                   Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)

                    If proceed = Windows.Forms.DialogResult.Cancel Then Return
                End If

                Dim cols, vals As String
                Dim collaboID As Integer = clsMarsData.CreateDataID


                cols = "collaboID, serverName, serverPath, username, userpassword, domain"
                vals = collaboID & "," & _
                    "'" & SQLPrepare(pcname) & "'," & _
                    "'" & SQLPrepare(fullPath) & "'," & _
                    "'" & SQLPrepare(username) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(password)) & "'," & _
                    "'" & SQLPrepare(domain) & "'"

                If clsMarsData.DataItem.InsertData("collaboratorsattr", cols, vals) = True Then
                    clsSettingsManager.Appconfig.SaveSettingValue("collaboration", 1, fullPath, False)
                    clsSettingsManager.Appconfig.SaveSettingValue("collaborationID", collaboID, fullPath, False)
                    clsSettingsManager.Appconfig.SaveSettingValue("collaborationLeader", Environment.MachineName, fullPath, False)

                    Dim conType, conString, conString2 As String
                    conType = clsMarsUI.MainUI.ReadRegistry("conType", "")
                    conString = clsMarsUI.MainUI.ReadRegistry("conString", "")
                    conString2 = clsMarsUI.MainUI.ReadRegistry("conString2", "")

                    clsSettingsManager.Appconfig.SaveSettingValue("conType", conType, fullPath, False)
                    clsSettingsManager.Appconfig.SaveSettingValue("conString", conString, fullPath, False)
                    clsSettingsManager.Appconfig.SaveSettingValue("conString2", conString2, fullPath, False)

                    Dim it As ListViewItem = lsvCollabo.Items.Add(pcname)
                    it.Tag = collaboID
                    it.SubItems.Add(fullPath)
                End If
            End If
        End If


    End Sub

    Sub loadServers()
        Dim SQL As String = "SELECT * FROM collaboratorsattr"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim it As ListViewItem = lsvCollabo.Items.Add(oRs("servername").Value)
                it.Tag = oRs("collaboid").Value
                it.SubItems.Add(oRs("serverpath").Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
        
    End Sub
    Private Sub ButtonX2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lsvCollabo.SelectedItems.Count = 0 Then Return

        Dim confirm As DialogResult = MessageBox.Show("Are you sure you would like to remove this server from Collaboration? All schedules using this will revert to using the default server.", _
                                                        Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If confirm = DialogResult.OK Then
            
            
            Dim it As ListViewItem = lsvCollabo.SelectedItems(0)
            Dim fullPath As String = it.SubItems(1).Text

            If getandkillRemoteProcs(it.Text, "", "", "") = False Then
                Dim proceed As DialogResult = MessageBox.Show("SQL-RD could not access the process list on the remote server. Please make sure that you have closed SQL-RD and the scheduler on the remote server before proceeding." & vbCrLf & _
                                                                   "Click OK to proceed or Cancel to abort.", _
                                                                    Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Warning)

                If proceed = Windows.Forms.DialogResult.Cancel Then Return
            End If

            Dim SQL As String = "DELETE FROM collaboratorsattr WHERE collaboID =" & it.Tag

            If clsMarsData.WriteData(SQL) = True Then
                it.Remove()

                clsSettingsManager.Appconfig.SaveSettingValue("collaboration", 0, fullPath, False)
                clsSettingsManager.Appconfig.SaveSettingValue("collaborationID", "", fullPath, False)
                clsSettingsManager.Appconfig.SaveSettingValue("collaborationLeader", "", fullPath, False)

                SQL = "UPDATE scheduleattr SET collaborationServer = 0 WHERE collaborationServer =" & it.Tag

                clsMarsData.WriteData(SQL)

                MessageBox.Show("The server has now been removed from Collaboration.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Dim isloaded As Boolean

    Private Sub frmCollaborators_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)

        loadServers()

        Dim iscollabo As Boolean = clsMarsUI.MainUI.ReadRegistry("collaboration", 0)

        chkCollaborationStatus.Checked = iscollabo

        isloaded = True
    End Sub



    Private Sub sbtnCollaboration_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCollaborationStatus.CheckedChanged


        If chkCollaborationStatus.Checked = False And isloaded = True Then
            Dim res As DialogResult = MessageBox.Show("Are you sure you would like to disable Collaboration? All the servers will be removed immediately.", Application.ProductName, _
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            Dim hasErrored As Boolean

            If res = Windows.Forms.DialogResult.Yes Then
                For Each it As ListViewItem In lsvCollabo.Items
                    Dim fullPath As String = it.SubItems(1).Text

                    If getandkillRemoteProcs(it.Text, "", "", "") = False Then
                        hasErrored = True
                        Continue For
                    End If


                    Dim SQL As String = "DELETE FROM collaboratorsattr WHERE collaboID =" & it.Tag

                    If clsMarsData.WriteData(SQL) = True Then
                        it.Remove()

                        clsSettingsManager.Appconfig.SaveSettingValue("collaboration", 0, fullPath, False)
                        clsSettingsManager.Appconfig.SaveSettingValue("collaborationID", "", fullPath, False)
                        clsSettingsManager.Appconfig.SaveSettingValue("collaborationLeader", "", fullPath, False)

                        SQL = "UPDATE scheduleattr SET collaborationServer = 0 WHERE collaborationServer =" & it.Tag

                        clsMarsData.WriteData(SQL)

                    End If
                Next

                If hasErrored = False Then
                    MessageBox.Show("Collaboration has now been switched off.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    clsMarsUI.MainUI.SaveRegistry("collaboration", 0)
                    btnAdd.Enabled = chkCollaborationStatus.Checked
                    btnDelete.Enabled = chkCollaborationStatus.Checked
                Else
                    MessageBox.Show("Collaboration could not be switched off as one or more server could not be removed. Please stop SQL-RD on the remote servers and try again.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    chkCollaborationStatus.Checked = True
                End If
            Else
                chkCollaborationStatus.Checked = True
            End If
        Else
            clsMarsUI.MainUI.SaveRegistry("collaboration", 1)
            clsMarsUI.MainUI.SaveRegistry("collaborationLeader", Environment.MachineName)

            btnAdd.Enabled = chkCollaborationStatus.Checked
            btnDelete.Enabled = chkCollaborationStatus.Checked
        End If


    End Sub
End Class