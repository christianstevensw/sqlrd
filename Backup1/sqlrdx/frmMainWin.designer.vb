﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainWin
    Inherits DevComponents.DotNetBar.Office2007RibbonForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMainWin))
        Me.StyleManager1 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.RibbonControl1 = New DevComponents.DotNetBar.RibbonControl()
        Me.RibbonPanel4 = New DevComponents.DotNetBar.RibbonPanel()
        Me.stabDatabase = New DevComponents.DotNetBar.RibbonBar()
        Me.btnExportData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAutomationScheduleData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleScheduleData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackageScheduleData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEventBasedData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEventPackageData = New DevComponents.DotNetBar.ButtonItem()
        Me.btnLoginInfo = New DevComponents.DotNetBar.ButtonItem()
        Me.btnConsole = New DevComponents.DotNetBar.ButtonItem()
        Me.stabMigrate = New DevComponents.DotNetBar.RibbonBar()
        Me.btnMigrateToLocal = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMigratetoODBC = New DevComponents.DotNetBar.ButtonItem()
        Me.stabSwitch = New DevComponents.DotNetBar.RibbonBar()
        Me.btnSwitchToLocal = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSwitchToODBC = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel1 = New DevComponents.DotNetBar.RibbonPanel()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.btnDateAndTime = New DevComponents.DotNetBar.LabelItem()
        Me.stabSearch = New DevComponents.DotNetBar.RibbonBar()
        Me.txtSearch = New DevComponents.DotNetBar.TextBoxItem()
        Me.swFullSystemSearch = New DevComponents.DotNetBar.SwitchButtonItem()
        Me.stabView = New DevComponents.DotNetBar.RibbonBar()
        Me.btnDashboardA = New DevComponents.DotNetBar.ButtonItem()
        Me.btnOutlook = New DevComponents.DotNetBar.ButtonItem()
        Me.btnViewStyle = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDetailStyle = New DevComponents.DotNetBar.ButtonItem()
        Me.btnTileStyle = New DevComponents.DotNetBar.ButtonItem()
        Me.slSize = New DevComponents.DotNetBar.SliderItem()
        Me.btnUnifyFolderStyles = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDetails = New DevComponents.DotNetBar.ButtonItem()
        Me.btnRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnTheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnBlackTheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnBluetheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSilvertheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnVistatheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnWin7theme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnFutureTheme = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMetroTheme = New DevComponents.DotNetBar.ButtonItem()
        Me.stabSystem = New DevComponents.DotNetBar.RibbonBar()
        Me.btnAddressBook = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer5 = New DevComponents.DotNetBar.ItemContainer()
        Me.btnCustomCals = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDataItems = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUserConstants = New DevComponents.DotNetBar.ButtonItem()
        Me.stabSchedules = New DevComponents.DotNetBar.RibbonBar()
        Me.btnSingleSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDynamicSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDynamicPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEventBased = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEventBasedPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDataDriven = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDataDrivenPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnBurstingPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAutomationSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel2 = New DevComponents.DotNetBar.RibbonPanel()
        Me.stabBackRestore = New DevComponents.DotNetBar.RibbonBar()
        Me.btnBackup = New DevComponents.DotNetBar.ButtonItem()
        Me.btnRestore = New DevComponents.DotNetBar.ButtonItem()
        Me.stabRemoteAdmin = New DevComponents.DotNetBar.RibbonBar()
        Me.btnRemoteConnect = New DevComponents.DotNetBar.ButtonItem()
        Me.btnRemoteDisconnect = New DevComponents.DotNetBar.ButtonItem()
        Me.stabManage = New DevComponents.DotNetBar.RibbonBar()
        Me.btnDashboard = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSystemMonitorLarge = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCollaboration = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUserManager = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSMTPServers = New DevComponents.DotNetBar.ButtonItem()
        Me.btnExportSchedules = New DevComponents.DotNetBar.ButtonItem()
        Me.btnOperationHours = New DevComponents.DotNetBar.ButtonItem()
        Me.RibbonPanel3 = New DevComponents.DotNetBar.RibbonPanel()
        Me.stabFeatures = New DevComponents.DotNetBar.RibbonBar()
        Me.btnUpdateFeatures = New DevComponents.DotNetBar.ButtonItem()
        Me.btnValidateFeatures = New DevComponents.DotNetBar.ButtonItem()
        Me.stabActivation = New DevComponents.DotNetBar.RibbonBar()
        Me.btnActivate = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDeactivate = New DevComponents.DotNetBar.ButtonItem()
        Me.stabSupport = New DevComponents.DotNetBar.RibbonBar()
        Me.btnSupportFiles = New DevComponents.DotNetBar.ButtonItem()
        Me.btnLogCall = New DevComponents.DotNetBar.ButtonItem()
        Me.stabHelp = New DevComponents.DotNetBar.RibbonBar()
        Me.btnAboutToo = New DevComponents.DotNetBar.ButtonItem()
        Me.btnHelp = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDemos = New DevComponents.DotNetBar.ButtonItem()
        Me.btnforums = New DevComponents.DotNetBar.ButtonItem()
        Me.btnKbase = New DevComponents.DotNetBar.ButtonItem()
        Me.btnUpdateCheck = New DevComponents.DotNetBar.ButtonItem()
        Me.btnProfServices = New DevComponents.DotNetBar.ButtonItem()
        Me.Office2007StartButton1 = New DevComponents.DotNetBar.Office2007StartButton()
        Me.SuperTabControl1 = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.SuperTabItem4 = New DevComponents.DotNetBar.SuperTabItem()
        Me.btnOpenReport = New DevComponents.DotNetBar.ButtonItem()
        Me.btnQuickEmail = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem8 = New DevComponents.DotNetBar.ButtonItem()
        Me.btnOptions1 = New DevComponents.DotNetBar.ButtonItem()
        Me.btnLogOut = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem10 = New DevComponents.DotNetBar.ButtonItem()
        Me.ItemContainer1 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer2 = New DevComponents.DotNetBar.ItemContainer()
        Me.ItemContainer3 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem2 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem3 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem5 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem7 = New DevComponents.DotNetBar.ButtonItem()
        Me.GalleryContainer1 = New DevComponents.DotNetBar.GalleryContainer()
        Me.ItemContainer4 = New DevComponents.DotNetBar.ItemContainer()
        Me.ButtonItem12 = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem13 = New DevComponents.DotNetBar.ButtonItem()
        Me.rtabCreate = New DevComponents.DotNetBar.RibbonTabItem()
        Me.rtabSystem = New DevComponents.DotNetBar.RibbonTabItem()
        Me.rtabResources = New DevComponents.DotNetBar.RibbonTabItem()
        Me.rtabConfiguration = New DevComponents.DotNetBar.RibbonTabItem()
        Me.btnNewFolder = New DevComponents.DotNetBar.ButtonItem()
        Me.btnNewSmartFolder = New DevComponents.DotNetBar.ButtonItem()
        Me.btnOptions = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSystemMonitor = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPasteItem = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAbout = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmallHelp = New DevComponents.DotNetBar.ButtonItem()
        Me.QatCustomizeItem1 = New DevComponents.DotNetBar.QatCustomizeItem()
        Me.ComboItem1 = New DevComponents.Editors.ComboItem()
        Me.ComboItem2 = New DevComponents.Editors.ComboItem()
        Me.ComboItem3 = New DevComponents.Editors.ComboItem()
        Me.ComboItem4 = New DevComponents.Editors.ComboItem()
        Me.ComboItem5 = New DevComponents.Editors.ComboItem()
        Me.ComboItem6 = New DevComponents.Editors.ComboItem()
        Me.ComboItem7 = New DevComponents.Editors.ComboItem()
        Me.ComboItem8 = New DevComponents.Editors.ComboItem()
        Me.StyleManager2 = New DevComponents.DotNetBar.StyleManager(Me.components)
        Me.tvExplorer = New DevComponents.AdvTree.AdvTree()
        Me.Node5 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.ElementStyle3 = New DevComponents.DotNetBar.ElementStyle()
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.tvMain = New DevComponents.AdvTree.AdvTree()
        Me.ElementStyle4 = New DevComponents.DotNetBar.ElementStyle()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.Cell1 = New DevComponents.AdvTree.Cell()
        Me.Cell2 = New DevComponents.AdvTree.Cell()
        Me.Cell3 = New DevComponents.AdvTree.Cell()
        Me.DotNetBarManager1 = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.DockSite4 = New DevComponents.DotNetBar.DockSite()
        Me.Bar1 = New DevComponents.DotNetBar.Bar()
        Me.PanelDockContainer2 = New DevComponents.DotNetBar.PanelDockContainer()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.lblProgress = New DevComponents.DotNetBar.LabelX()
        Me.pgx = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.DockContainerItem4 = New DevComponents.DotNetBar.DockContainerItem()
        Me.DockSite1 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite2 = New DevComponents.DotNetBar.DockSite()
        Me.dnbRight = New DevComponents.DotNetBar.Bar()
        Me.PanelDockContainer3 = New DevComponents.DotNetBar.PanelDockContainer()
        Me.advProps = New DevComponents.DotNetBar.AdvPropertyGrid()
        Me.UcSchedulerControler1 = New sqlrd.ucSchedulerControler()
        Me.UcScheduleCount1 = New sqlrd.ucScheduleCount()
        Me.PanelDockContainer1 = New DevComponents.DotNetBar.PanelDockContainer()
        Me.ucHistory = New sqlrd.ucScheduleHistory()
        Me.DockContainerItem3 = New DevComponents.DotNetBar.DockContainerItem()
        Me.DockContainerItem1 = New DevComponents.DotNetBar.DockContainerItem()
        Me.DockSite8 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite5 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite6 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite7 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite3 = New DevComponents.DotNetBar.DockSite()
        Me.pg = New DevComponents.DotNetBar.Controls.CircularProgress()
        Me.DockContainerItem2 = New DevComponents.DotNetBar.DockContainerItem()
        Me.ContextMenuBar1 = New DevComponents.DotNetBar.ContextMenuBar()
        Me.mnuFolders = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderNew = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderOpenInNewWindow = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderMove = New DevComponents.DotNetBar.ButtonItem()
        Me.btnFolderRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem11 = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderDisable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderEnable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnfolderExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleContext = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleProperties = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleCopy = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSinglePaste = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleEnabled = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMoveSingle = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSinglePreview = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleTools = New DevComponents.DotNetBar.ButtonItem()
        Me.btnReportDesign = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSingleTest = New DevComponents.DotNetBar.ButtonItem()
        Me.btnConvertToPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAdHocEmail = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCreateSingleShortcut = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAdHocView = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackageContext = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuPackageProperties = New DevComponents.DotNetBar.ButtonItem()
        Me.btnOpenPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackAddReport = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackCopy = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackPaste = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackEnabled = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMovePackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackRefreshPack = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackTools = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackTest = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackSplit = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackAdhoc = New DevComponents.DotNetBar.ButtonItem()
        Me.btnpackShortcut = New DevComponents.DotNetBar.ButtonItem()
        Me.btnAutomationSchedules = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuAutoProp = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoCopy = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoPaste = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoEnabled = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMoveAuto = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnautoShortcut = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventBasedSchedules = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuEventProp = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventCopy = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventPaste = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventEnabled = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMoveEvent = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventShortcut = New DevComponents.DotNetBar.ButtonItem()
        Me.btnEventBasedPacks = New DevComponents.DotNetBar.ButtonItem()
        Me.mnueventPackProps = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackAdd = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackAddNew = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackAddExisting = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackCopy = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackPaste = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackEnabled = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMoveEventPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackOpen = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackTools = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackSplit = New DevComponents.DotNetBar.ButtonItem()
        Me.btneventpackShortcut = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuMultipleItems = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltProperties = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltMove = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltConvertToPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltMoveToPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltStatus = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltEnable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnmltDisable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnCollaborationServer = New DevComponents.DotNetBar.ButtonItem()
        Me.cmbCollaborationServer = New DevComponents.DotNetBar.ComboBoxItem()
        Me.mnuSmartFolders = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartProperties = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartViewReport = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartAllContents = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartDisable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartEnable = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSmartExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudesktopMenu = New DevComponents.DotNetBar.ButtonItem()
        Me.mnuDesktopPaste = New DevComponents.DotNetBar.ButtonItem()
        Me.ButtonItem4 = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewFolder = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewSmartFolder = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewSingleSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewDynamicSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewDynamicPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewEventSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewEventPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewDataDriven = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewDataDrivenPackage = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewBurstingSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.mnudtNewAutomationSchedule = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedReportMenu = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackagedProperties = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedRename = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedStatus = New DevComponents.DotNetBar.ButtonItem()
        Me.btnMovePackedReport = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedRefresh = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedPreview = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedExecute = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedDelete = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedAdhoc = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPackedDesign = New DevComponents.DotNetBar.ButtonItem()
        Me.crumb = New DevComponents.DotNetBar.CrumbBar()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tmSchedulerInfo = New System.Windows.Forms.Timer(Me.components)
        Me.tmCheckUpdates = New System.Windows.Forms.Timer(Me.components)
        Me.RibbonControl1.SuspendLayout()
        Me.RibbonPanel4.SuspendLayout()
        Me.RibbonPanel1.SuspendLayout()
        Me.RibbonPanel2.SuspendLayout()
        Me.RibbonPanel3.SuspendLayout()
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControl1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        CType(Me.tvExplorer, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockSite4.SuspendLayout()
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Bar1.SuspendLayout()
        Me.PanelDockContainer2.SuspendLayout()
        Me.DockSite2.SuspendLayout()
        CType(Me.dnbRight, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.dnbRight.SuspendLayout()
        Me.PanelDockContainer3.SuspendLayout()
        CType(Me.advProps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelDockContainer1.SuspendLayout()
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'StyleManager1
        '
        Me.StyleManager1.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager1.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(237, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(0, Byte), Integer)))
        '
        'RibbonControl1
        '
        Me.RibbonControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.RibbonControl1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonControl1.CaptionVisible = True
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel3)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel4)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel1)
        Me.RibbonControl1.Controls.Add(Me.RibbonPanel2)
        Me.RibbonControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonControl1.ForeColor = System.Drawing.Color.Black
        Me.RibbonControl1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.Office2007StartButton1, Me.rtabCreate, Me.rtabSystem, Me.rtabResources, Me.rtabConfiguration})
        Me.RibbonControl1.KeyTipsFont = New System.Drawing.Font("Tahoma", 7.0!)
        Me.RibbonControl1.Location = New System.Drawing.Point(5, 1)
        Me.RibbonControl1.Name = "RibbonControl1"
        Me.RibbonControl1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 2)
        Me.RibbonControl1.QuickToolbarItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnNewFolder, Me.btnNewSmartFolder, Me.btnOptions, Me.btnSystemMonitor, Me.btnPasteItem, Me.btnAbout, Me.btnSmallHelp, Me.QatCustomizeItem1})
        Me.RibbonControl1.Size = New System.Drawing.Size(1596, 154)
        Me.RibbonControl1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonControl1.SystemText.MaximizeRibbonText = "&Maximize the Ribbon"
        Me.RibbonControl1.SystemText.MinimizeRibbonText = "Mi&nimize the Ribbon"
        Me.RibbonControl1.SystemText.QatAddItemText = "&Add to Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatCustomizeMenuLabel = "<b>Customize Quick Access Toolbar</b>"
        Me.RibbonControl1.SystemText.QatCustomizeText = "&Customize Quick Access Toolbar..."
        Me.RibbonControl1.SystemText.QatDialogAddButton = "&Add >>"
        Me.RibbonControl1.SystemText.QatDialogCancelButton = "Cancel"
        Me.RibbonControl1.SystemText.QatDialogCaption = "Customize Quick Access Toolbar"
        Me.RibbonControl1.SystemText.QatDialogCategoriesLabel = "&Choose commands from:"
        Me.RibbonControl1.SystemText.QatDialogOkButton = "OK"
        Me.RibbonControl1.SystemText.QatDialogPlacementCheckbox = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatDialogRemoveButton = "&Remove"
        Me.RibbonControl1.SystemText.QatPlaceAboveRibbonText = "&Place Quick Access Toolbar above the Ribbon"
        Me.RibbonControl1.SystemText.QatPlaceBelowRibbonText = "&Place Quick Access Toolbar below the Ribbon"
        Me.RibbonControl1.SystemText.QatRemoveItemText = "&Remove from Quick Access Toolbar"
        Me.RibbonControl1.TabGroupHeight = 14
        Me.RibbonControl1.TabIndex = 0
        Me.RibbonControl1.Text = "RibbonControl1"
        '
        'RibbonPanel4
        '
        Me.RibbonPanel4.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel4.Controls.Add(Me.stabDatabase)
        Me.RibbonPanel4.Controls.Add(Me.stabMigrate)
        Me.RibbonPanel4.Controls.Add(Me.stabSwitch)
        Me.RibbonPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel4.Location = New System.Drawing.Point(0, 54)
        Me.RibbonPanel4.Name = "RibbonPanel4"
        Me.RibbonPanel4.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel4.Size = New System.Drawing.Size(1596, 98)
        '
        '
        '
        Me.RibbonPanel4.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel4.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel4.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel4.TabIndex = 4
        Me.RibbonPanel4.Visible = False
        '
        'stabDatabase
        '
        Me.stabDatabase.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabDatabase.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabDatabase.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabDatabase.ContainerControlProcessDialogKey = True
        Me.stabDatabase.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabDatabase.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnExportData, Me.btnLoginInfo, Me.btnConsole})
        Me.stabDatabase.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabDatabase.Location = New System.Drawing.Point(196, 0)
        Me.stabDatabase.Name = "stabDatabase"
        Me.stabDatabase.Size = New System.Drawing.Size(179, 95)
        Me.stabDatabase.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabDatabase.TabIndex = 2
        Me.stabDatabase.Text = "Database"
        '
        '
        '
        Me.stabDatabase.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabDatabase.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnExportData
        '
        Me.btnExportData.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnExportData.Image = CType(resources.GetObject("btnExportData.Image"), System.Drawing.Image)
        Me.btnExportData.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnExportData.Name = "btnExportData"
        Me.btnExportData.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAutomationScheduleData, Me.btnSingleScheduleData, Me.btnPackageScheduleData, Me.btnEventBasedData, Me.btnEventPackageData})
        Me.btnExportData.SubItemsExpandWidth = 14
        Me.btnExportData.Text = "Reports"
        '
        'btnAutomationScheduleData
        '
        Me.btnAutomationScheduleData.Image = CType(resources.GetObject("btnAutomationScheduleData.Image"), System.Drawing.Image)
        Me.btnAutomationScheduleData.Name = "btnAutomationScheduleData"
        Me.btnAutomationScheduleData.Text = "Automation Schedule Data"
        '
        'btnSingleScheduleData
        '
        Me.btnSingleScheduleData.Image = CType(resources.GetObject("btnSingleScheduleData.Image"), System.Drawing.Image)
        Me.btnSingleScheduleData.Name = "btnSingleScheduleData"
        Me.btnSingleScheduleData.Text = "Single Schedule Data"
        '
        'btnPackageScheduleData
        '
        Me.btnPackageScheduleData.Image = CType(resources.GetObject("btnPackageScheduleData.Image"), System.Drawing.Image)
        Me.btnPackageScheduleData.Name = "btnPackageScheduleData"
        Me.btnPackageScheduleData.Text = "Package Schedule Data"
        '
        'btnEventBasedData
        '
        Me.btnEventBasedData.Image = CType(resources.GetObject("btnEventBasedData.Image"), System.Drawing.Image)
        Me.btnEventBasedData.Name = "btnEventBasedData"
        Me.btnEventBasedData.Text = "Event-Based Data"
        '
        'btnEventPackageData
        '
        Me.btnEventPackageData.Image = CType(resources.GetObject("btnEventPackageData.Image"), System.Drawing.Image)
        Me.btnEventPackageData.Name = "btnEventPackageData"
        Me.btnEventPackageData.Text = "Event-Based Packages"
        '
        'btnLoginInfo
        '
        Me.btnLoginInfo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnLoginInfo.Enabled = False
        Me.btnLoginInfo.Image = CType(resources.GetObject("btnLoginInfo.Image"), System.Drawing.Image)
        Me.btnLoginInfo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnLoginInfo.Name = "btnLoginInfo"
        Me.btnLoginInfo.SubItemsExpandWidth = 14
        Me.btnLoginInfo.Text = "Login Information"
        '
        'btnConsole
        '
        Me.btnConsole.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnConsole.Image = CType(resources.GetObject("btnConsole.Image"), System.Drawing.Image)
        Me.btnConsole.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnConsole.Name = "btnConsole"
        Me.btnConsole.SubItemsExpandWidth = 14
        Me.btnConsole.Text = "Console"
        '
        'stabMigrate
        '
        Me.stabMigrate.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabMigrate.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabMigrate.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabMigrate.ContainerControlProcessDialogKey = True
        Me.stabMigrate.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabMigrate.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnMigrateToLocal, Me.btnMigratetoODBC})
        Me.stabMigrate.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabMigrate.Location = New System.Drawing.Point(97, 0)
        Me.stabMigrate.Name = "stabMigrate"
        Me.stabMigrate.Size = New System.Drawing.Size(99, 95)
        Me.stabMigrate.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabMigrate.TabIndex = 1
        Me.stabMigrate.Text = "Migrate"
        '
        '
        '
        Me.stabMigrate.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabMigrate.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnMigrateToLocal
        '
        Me.btnMigrateToLocal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnMigrateToLocal.Enabled = False
        Me.btnMigrateToLocal.Image = CType(resources.GetObject("btnMigrateToLocal.Image"), System.Drawing.Image)
        Me.btnMigrateToLocal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnMigrateToLocal.Name = "btnMigrateToLocal"
        Me.btnMigrateToLocal.SubItemsExpandWidth = 14
        Me.btnMigrateToLocal.Text = "To LOCAL"
        '
        'btnMigratetoODBC
        '
        Me.btnMigratetoODBC.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnMigratetoODBC.Enabled = False
        Me.btnMigratetoODBC.Image = CType(resources.GetObject("btnMigratetoODBC.Image"), System.Drawing.Image)
        Me.btnMigratetoODBC.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnMigratetoODBC.Name = "btnMigratetoODBC"
        Me.btnMigratetoODBC.SubItemsExpandWidth = 14
        Me.btnMigratetoODBC.Text = "To ODBC"
        '
        'stabSwitch
        '
        Me.stabSwitch.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabSwitch.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSwitch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabSwitch.ContainerControlProcessDialogKey = True
        Me.stabSwitch.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabSwitch.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSwitchToLocal, Me.btnSwitchToODBC})
        Me.stabSwitch.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabSwitch.Location = New System.Drawing.Point(3, 0)
        Me.stabSwitch.Name = "stabSwitch"
        Me.stabSwitch.Size = New System.Drawing.Size(94, 95)
        Me.stabSwitch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSwitch.TabIndex = 0
        Me.stabSwitch.Text = "Switch"
        '
        '
        '
        Me.stabSwitch.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSwitch.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnSwitchToLocal
        '
        Me.btnSwitchToLocal.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSwitchToLocal.Enabled = False
        Me.btnSwitchToLocal.Image = CType(resources.GetObject("btnSwitchToLocal.Image"), System.Drawing.Image)
        Me.btnSwitchToLocal.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSwitchToLocal.Name = "btnSwitchToLocal"
        Me.btnSwitchToLocal.SubItemsExpandWidth = 14
        Me.btnSwitchToLocal.Text = "To LOCAL"
        '
        'btnSwitchToODBC
        '
        Me.btnSwitchToODBC.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSwitchToODBC.Enabled = False
        Me.btnSwitchToODBC.Image = CType(resources.GetObject("btnSwitchToODBC.Image"), System.Drawing.Image)
        Me.btnSwitchToODBC.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSwitchToODBC.Name = "btnSwitchToODBC"
        Me.btnSwitchToODBC.SubItemsExpandWidth = 14
        Me.btnSwitchToODBC.Text = "To ODBC"
        '
        'RibbonPanel1
        '
        Me.RibbonPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel1.Controls.Add(Me.RibbonBar1)
        Me.RibbonPanel1.Controls.Add(Me.stabSearch)
        Me.RibbonPanel1.Controls.Add(Me.stabView)
        Me.RibbonPanel1.Controls.Add(Me.stabSystem)
        Me.RibbonPanel1.Controls.Add(Me.stabSchedules)
        Me.RibbonPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel1.Location = New System.Drawing.Point(0, 54)
        Me.RibbonPanel1.Name = "RibbonPanel1"
        Me.RibbonPanel1.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel1.Size = New System.Drawing.Size(1596, 98)
        '
        '
        '
        Me.RibbonPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel1.TabIndex = 1
        Me.RibbonPanel1.Visible = False
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Left
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnDateAndTime})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(1382, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(187, 95)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar1.TabIndex = 5
        Me.RibbonBar1.Text = "Date && Time"
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnDateAndTime
        '
        Me.btnDateAndTime.Image = CType(resources.GetObject("btnDateAndTime.Image"), System.Drawing.Image)
        Me.btnDateAndTime.Name = "btnDateAndTime"
        Me.btnDateAndTime.Stretch = True
        Me.btnDateAndTime.Text = "11/11/2011 12:34:56 PM "
        '
        'stabSearch
        '
        Me.stabSearch.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabSearch.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSearch.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabSearch.ContainerControlProcessDialogKey = True
        Me.stabSearch.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabSearch.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.txtSearch, Me.swFullSystemSearch})
        Me.stabSearch.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabSearch.Location = New System.Drawing.Point(1070, 0)
        Me.stabSearch.Name = "stabSearch"
        Me.stabSearch.Size = New System.Drawing.Size(312, 95)
        Me.stabSearch.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSearch.TabIndex = 4
        Me.stabSearch.Text = "Search"
        '
        '
        '
        Me.stabSearch.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSearch.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'txtSearch
        '
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.TextBoxWidth = 200
        Me.txtSearch.WatermarkColor = System.Drawing.SystemColors.GrayText
        '
        'swFullSystemSearch
        '
        Me.swFullSystemSearch.Name = "swFullSystemSearch"
        Me.swFullSystemSearch.Text = "Full System Search"
        '
        'stabView
        '
        Me.stabView.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabView.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabView.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabView.ContainerControlProcessDialogKey = True
        Me.stabView.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabView.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnDashboardA, Me.btnOutlook, Me.btnViewStyle, Me.btnDetails, Me.btnRefresh, Me.btnTheme})
        Me.stabView.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabView.Location = New System.Drawing.Point(779, 0)
        Me.stabView.Name = "stabView"
        Me.stabView.Size = New System.Drawing.Size(291, 95)
        Me.stabView.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabView.TabIndex = 3
        Me.stabView.Text = "View"
        '
        '
        '
        Me.stabView.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabView.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnDashboardA
        '
        Me.btnDashboardA.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDashboardA.Image = CType(resources.GetObject("btnDashboardA.Image"), System.Drawing.Image)
        Me.btnDashboardA.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDashboardA.Name = "btnDashboardA"
        Me.btnDashboardA.SubItemsExpandWidth = 14
        Me.btnDashboardA.Text = "Dashboard"
        '
        'btnOutlook
        '
        Me.btnOutlook.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOutlook.Image = CType(resources.GetObject("btnOutlook.Image"), System.Drawing.Image)
        Me.btnOutlook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnOutlook.Name = "btnOutlook"
        Me.btnOutlook.SubItemsExpandWidth = 14
        Me.btnOutlook.Text = "Outlook"
        '
        'btnViewStyle
        '
        Me.btnViewStyle.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnViewStyle.Image = CType(resources.GetObject("btnViewStyle.Image"), System.Drawing.Image)
        Me.btnViewStyle.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnViewStyle.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.btnViewStyle.Name = "btnViewStyle"
        Me.btnViewStyle.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnDetailStyle, Me.btnTileStyle, Me.slSize, Me.btnUnifyFolderStyles})
        Me.btnViewStyle.SubItemsExpandWidth = 14
        Me.btnViewStyle.Text = "Style"
        '
        'btnDetailStyle
        '
        Me.btnDetailStyle.AutoCheckOnClick = True
        Me.btnDetailStyle.BeginGroup = True
        Me.btnDetailStyle.Name = "btnDetailStyle"
        Me.btnDetailStyle.Text = "Details"
        '
        'btnTileStyle
        '
        Me.btnTileStyle.BeginGroup = True
        Me.btnTileStyle.Name = "btnTileStyle"
        Me.btnTileStyle.Text = "Tiles"
        '
        'slSize
        '
        Me.slSize.LabelPosition = DevComponents.DotNetBar.eSliderLabelPosition.Top
        Me.slSize.Name = "slSize"
        Me.slSize.Step = 25
        Me.slSize.Text = "Tile"
        Me.slSize.Value = 0
        '
        'btnUnifyFolderStyles
        '
        Me.btnUnifyFolderStyles.Image = CType(resources.GetObject("btnUnifyFolderStyles.Image"), System.Drawing.Image)
        Me.btnUnifyFolderStyles.Name = "btnUnifyFolderStyles"
        Me.btnUnifyFolderStyles.Text = "Apply style to all folders"
        '
        'btnDetails
        '
        Me.btnDetails.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDetails.Image = CType(resources.GetObject("btnDetails.Image"), System.Drawing.Image)
        Me.btnDetails.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDetails.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.btnDetails.Name = "btnDetails"
        Me.btnDetails.SubItemsExpandWidth = 14
        Me.btnDetails.Text = "Select Details"
        '
        'btnRefresh
        '
        Me.btnRefresh.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnRefresh.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.SubItemsExpandWidth = 14
        Me.btnRefresh.Text = "Refresh"
        '
        'btnTheme
        '
        Me.btnTheme.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnTheme.Image = CType(resources.GetObject("btnTheme.Image"), System.Drawing.Image)
        Me.btnTheme.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnTheme.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.btnTheme.Name = "btnTheme"
        Me.btnTheme.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnBlackTheme, Me.btnBluetheme, Me.btnSilvertheme, Me.btnVistatheme, Me.btnWin7theme, Me.btnFutureTheme, Me.btnMetroTheme})
        Me.btnTheme.SubItemsExpandWidth = 14
        Me.btnTheme.Text = "Theme"
        '
        'btnBlackTheme
        '
        Me.btnBlackTheme.AutoCheckOnClick = True
        Me.btnBlackTheme.Name = "btnBlackTheme"
        Me.btnBlackTheme.Text = "Black theme"
        '
        'btnBluetheme
        '
        Me.btnBluetheme.AutoCheckOnClick = True
        Me.btnBluetheme.Name = "btnBluetheme"
        Me.btnBluetheme.Text = "Blue theme"
        '
        'btnSilvertheme
        '
        Me.btnSilvertheme.AutoCheckOnClick = True
        Me.btnSilvertheme.Name = "btnSilvertheme"
        Me.btnSilvertheme.Text = "Silver theme"
        '
        'btnVistatheme
        '
        Me.btnVistatheme.AutoCheckOnClick = True
        Me.btnVistatheme.Name = "btnVistatheme"
        Me.btnVistatheme.Text = "Vista theme"
        '
        'btnWin7theme
        '
        Me.btnWin7theme.AutoCheckOnClick = True
        Me.btnWin7theme.Name = "btnWin7theme"
        Me.btnWin7theme.Text = "Win7 theme"
        '
        'btnFutureTheme
        '
        Me.btnFutureTheme.Name = "btnFutureTheme"
        Me.btnFutureTheme.Text = "Future theme"
        '
        'btnMetroTheme
        '
        Me.btnMetroTheme.Name = "btnMetroTheme"
        Me.btnMetroTheme.Text = "Metro theme"
        '
        'stabSystem
        '
        Me.stabSystem.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabSystem.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSystem.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabSystem.ContainerControlProcessDialogKey = True
        Me.stabSystem.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabSystem.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAddressBook, Me.ItemContainer5})
        Me.stabSystem.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabSystem.Location = New System.Drawing.Point(613, 0)
        Me.stabSystem.Name = "stabSystem"
        Me.stabSystem.Size = New System.Drawing.Size(166, 95)
        Me.stabSystem.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSystem.TabIndex = 1
        Me.stabSystem.Text = "Data"
        '
        '
        '
        Me.stabSystem.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSystem.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnAddressBook
        '
        Me.btnAddressBook.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnAddressBook.Image = CType(resources.GetObject("btnAddressBook.Image"), System.Drawing.Image)
        Me.btnAddressBook.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnAddressBook.Name = "btnAddressBook"
        Me.btnAddressBook.SubItemsExpandWidth = 14
        Me.btnAddressBook.Text = "Address Book"
        '
        'ItemContainer5
        '
        '
        '
        '
        Me.ItemContainer5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer5.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer5.Name = "ItemContainer5"
        Me.ItemContainer5.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnCustomCals, Me.btnDataItems, Me.btnUserConstants})
        '
        '
        '
        Me.ItemContainer5.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnCustomCals
        '
        Me.btnCustomCals.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnCustomCals.Image = CType(resources.GetObject("btnCustomCals.Image"), System.Drawing.Image)
        Me.btnCustomCals.Name = "btnCustomCals"
        Me.btnCustomCals.SubItemsExpandWidth = 14
        Me.btnCustomCals.Text = "Custom Calendars"
        '
        'btnDataItems
        '
        Me.btnDataItems.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDataItems.Image = CType(resources.GetObject("btnDataItems.Image"), System.Drawing.Image)
        Me.btnDataItems.Name = "btnDataItems"
        Me.btnDataItems.SubItemsExpandWidth = 14
        Me.btnDataItems.Text = "Data Items"
        '
        'btnUserConstants
        '
        Me.btnUserConstants.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnUserConstants.Image = CType(resources.GetObject("btnUserConstants.Image"), System.Drawing.Image)
        Me.btnUserConstants.Name = "btnUserConstants"
        Me.btnUserConstants.SubItemsExpandWidth = 14
        Me.btnUserConstants.Text = "User Constants"
        '
        'stabSchedules
        '
        Me.stabSchedules.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabSchedules.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSchedules.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabSchedules.ContainerControlProcessDialogKey = True
        Me.stabSchedules.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabSchedules.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSingleSchedule, Me.btnPackage, Me.btnDynamicSchedule, Me.btnDynamicPackage, Me.btnEventBased, Me.btnEventBasedPackage, Me.btnDataDriven, Me.btnDataDrivenPackage, Me.btnBurstingPackage, Me.btnAutomationSchedule})
        Me.stabSchedules.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabSchedules.Location = New System.Drawing.Point(3, 0)
        Me.stabSchedules.Name = "stabSchedules"
        Me.stabSchedules.Size = New System.Drawing.Size(610, 95)
        Me.stabSchedules.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSchedules.TabIndex = 0
        Me.stabSchedules.Text = "Create a Schedule"
        '
        '
        '
        Me.stabSchedules.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSchedules.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnSingleSchedule
        '
        Me.btnSingleSchedule.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSingleSchedule.Image = CType(resources.GetObject("btnSingleSchedule.Image"), System.Drawing.Image)
        Me.btnSingleSchedule.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSingleSchedule.Name = "btnSingleSchedule"
        Me.btnSingleSchedule.SubItemsExpandWidth = 14
        Me.btnSingleSchedule.Text = "Single"
        '
        'btnPackage
        '
        Me.btnPackage.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnPackage.Image = CType(resources.GetObject("btnPackage.Image"), System.Drawing.Image)
        Me.btnPackage.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnPackage.Name = "btnPackage"
        Me.btnPackage.SubItemsExpandWidth = 14
        Me.btnPackage.Text = "Package"
        '
        'btnDynamicSchedule
        '
        Me.btnDynamicSchedule.BeginGroup = True
        Me.btnDynamicSchedule.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDynamicSchedule.Image = CType(resources.GetObject("btnDynamicSchedule.Image"), System.Drawing.Image)
        Me.btnDynamicSchedule.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDynamicSchedule.Name = "btnDynamicSchedule"
        Me.btnDynamicSchedule.SubItemsExpandWidth = 14
        Me.btnDynamicSchedule.Text = "Dynamic"
        '
        'btnDynamicPackage
        '
        Me.btnDynamicPackage.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDynamicPackage.Image = CType(resources.GetObject("btnDynamicPackage.Image"), System.Drawing.Image)
        Me.btnDynamicPackage.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDynamicPackage.Name = "btnDynamicPackage"
        Me.btnDynamicPackage.SubItemsExpandWidth = 14
        Me.btnDynamicPackage.Text = "Dynamic Package"
        '
        'btnEventBased
        '
        Me.btnEventBased.BeginGroup = True
        Me.btnEventBased.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnEventBased.Image = CType(resources.GetObject("btnEventBased.Image"), System.Drawing.Image)
        Me.btnEventBased.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnEventBased.Name = "btnEventBased"
        Me.btnEventBased.SubItemsExpandWidth = 14
        Me.btnEventBased.Text = "Event-Based"
        '
        'btnEventBasedPackage
        '
        Me.btnEventBasedPackage.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnEventBasedPackage.Image = CType(resources.GetObject("btnEventBasedPackage.Image"), System.Drawing.Image)
        Me.btnEventBasedPackage.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnEventBasedPackage.Name = "btnEventBasedPackage"
        Me.btnEventBasedPackage.SubItemsExpandWidth = 14
        Me.btnEventBasedPackage.Text = "Event-Based Package"
        '
        'btnDataDriven
        '
        Me.btnDataDriven.BeginGroup = True
        Me.btnDataDriven.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDataDriven.Image = CType(resources.GetObject("btnDataDriven.Image"), System.Drawing.Image)
        Me.btnDataDriven.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDataDriven.Name = "btnDataDriven"
        Me.btnDataDriven.SubItemsExpandWidth = 14
        Me.btnDataDriven.Text = "Data-Driven"
        '
        'btnDataDrivenPackage
        '
        Me.btnDataDrivenPackage.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDataDrivenPackage.Image = CType(resources.GetObject("btnDataDrivenPackage.Image"), System.Drawing.Image)
        Me.btnDataDrivenPackage.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDataDrivenPackage.Name = "btnDataDrivenPackage"
        Me.btnDataDrivenPackage.SubItemsExpandWidth = 14
        Me.btnDataDrivenPackage.Text = "Data-Driven Package"
        '
        'btnBurstingPackage
        '
        Me.btnBurstingPackage.BeginGroup = True
        Me.btnBurstingPackage.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnBurstingPackage.Image = CType(resources.GetObject("btnBurstingPackage.Image"), System.Drawing.Image)
        Me.btnBurstingPackage.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnBurstingPackage.Name = "btnBurstingPackage"
        Me.btnBurstingPackage.SubItemsExpandWidth = 14
        Me.btnBurstingPackage.Text = "Bursting"
        Me.btnBurstingPackage.Visible = False
        '
        'btnAutomationSchedule
        '
        Me.btnAutomationSchedule.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnAutomationSchedule.Image = CType(resources.GetObject("btnAutomationSchedule.Image"), System.Drawing.Image)
        Me.btnAutomationSchedule.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnAutomationSchedule.Name = "btnAutomationSchedule"
        Me.btnAutomationSchedule.SubItemsExpandWidth = 14
        Me.btnAutomationSchedule.Text = "Automation"
        '
        'RibbonPanel2
        '
        Me.RibbonPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel2.Controls.Add(Me.stabBackRestore)
        Me.RibbonPanel2.Controls.Add(Me.stabRemoteAdmin)
        Me.RibbonPanel2.Controls.Add(Me.stabManage)
        Me.RibbonPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel2.Location = New System.Drawing.Point(0, 53)
        Me.RibbonPanel2.Name = "RibbonPanel2"
        Me.RibbonPanel2.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel2.Size = New System.Drawing.Size(1596, 99)
        '
        '
        '
        Me.RibbonPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel2.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel2.TabIndex = 2
        Me.RibbonPanel2.Visible = False
        '
        'stabBackRestore
        '
        Me.stabBackRestore.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabBackRestore.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabBackRestore.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabBackRestore.ContainerControlProcessDialogKey = True
        Me.stabBackRestore.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabBackRestore.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnBackup, Me.btnRestore})
        Me.stabBackRestore.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabBackRestore.Location = New System.Drawing.Point(536, 0)
        Me.stabBackRestore.Name = "stabBackRestore"
        Me.stabBackRestore.Size = New System.Drawing.Size(105, 96)
        Me.stabBackRestore.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabBackRestore.TabIndex = 2
        Me.stabBackRestore.Text = "Backup &&  Restore"
        '
        '
        '
        Me.stabBackRestore.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabBackRestore.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnBackup
        '
        Me.btnBackup.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnBackup.Image = CType(resources.GetObject("btnBackup.Image"), System.Drawing.Image)
        Me.btnBackup.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnBackup.Name = "btnBackup"
        Me.btnBackup.SubItemsExpandWidth = 14
        Me.btnBackup.Text = "Backup"
        '
        'btnRestore
        '
        Me.btnRestore.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnRestore.Image = CType(resources.GetObject("btnRestore.Image"), System.Drawing.Image)
        Me.btnRestore.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnRestore.Name = "btnRestore"
        Me.btnRestore.SubItemsExpandWidth = 14
        Me.btnRestore.Text = "Restore"
        '
        'stabRemoteAdmin
        '
        Me.stabRemoteAdmin.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabRemoteAdmin.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabRemoteAdmin.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabRemoteAdmin.ContainerControlProcessDialogKey = True
        Me.stabRemoteAdmin.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabRemoteAdmin.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnRemoteConnect, Me.btnRemoteDisconnect})
        Me.stabRemoteAdmin.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabRemoteAdmin.Location = New System.Drawing.Point(415, 0)
        Me.stabRemoteAdmin.Name = "stabRemoteAdmin"
        Me.stabRemoteAdmin.Size = New System.Drawing.Size(121, 96)
        Me.stabRemoteAdmin.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabRemoteAdmin.TabIndex = 1
        Me.stabRemoteAdmin.Text = "Remote Adminstration"
        '
        '
        '
        Me.stabRemoteAdmin.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabRemoteAdmin.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnRemoteConnect
        '
        Me.btnRemoteConnect.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnRemoteConnect.Image = CType(resources.GetObject("btnRemoteConnect.Image"), System.Drawing.Image)
        Me.btnRemoteConnect.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnRemoteConnect.Name = "btnRemoteConnect"
        Me.btnRemoteConnect.SubItemsExpandWidth = 14
        Me.btnRemoteConnect.Text = "Connect"
        '
        'btnRemoteDisconnect
        '
        Me.btnRemoteDisconnect.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnRemoteDisconnect.Enabled = False
        Me.btnRemoteDisconnect.Image = CType(resources.GetObject("btnRemoteDisconnect.Image"), System.Drawing.Image)
        Me.btnRemoteDisconnect.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnRemoteDisconnect.Name = "btnRemoteDisconnect"
        Me.btnRemoteDisconnect.SubItemsExpandWidth = 14
        Me.btnRemoteDisconnect.Text = "Disconnect"
        '
        'stabManage
        '
        Me.stabManage.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabManage.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabManage.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabManage.ContainerControlProcessDialogKey = True
        Me.stabManage.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabManage.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnDashboard, Me.btnSystemMonitorLarge, Me.btnCollaboration, Me.btnUserManager, Me.btnSMTPServers, Me.btnExportSchedules, Me.btnOperationHours})
        Me.stabManage.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabManage.Location = New System.Drawing.Point(3, 0)
        Me.stabManage.Name = "stabManage"
        Me.stabManage.Size = New System.Drawing.Size(412, 96)
        Me.stabManage.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabManage.TabIndex = 0
        Me.stabManage.Text = "Manage"
        '
        '
        '
        Me.stabManage.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabManage.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnDashboard
        '
        Me.btnDashboard.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDashboard.Image = CType(resources.GetObject("btnDashboard.Image"), System.Drawing.Image)
        Me.btnDashboard.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDashboard.Name = "btnDashboard"
        Me.btnDashboard.SubItemsExpandWidth = 14
        Me.btnDashboard.Text = "Dashboard"
        '
        'btnSystemMonitorLarge
        '
        Me.btnSystemMonitorLarge.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSystemMonitorLarge.Image = CType(resources.GetObject("btnSystemMonitorLarge.Image"), System.Drawing.Image)
        Me.btnSystemMonitorLarge.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSystemMonitorLarge.Name = "btnSystemMonitorLarge"
        Me.btnSystemMonitorLarge.SubItemsExpandWidth = 14
        Me.btnSystemMonitorLarge.Text = "System Monitor"
        '
        'btnCollaboration
        '
        Me.btnCollaboration.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnCollaboration.Image = CType(resources.GetObject("btnCollaboration.Image"), System.Drawing.Image)
        Me.btnCollaboration.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnCollaboration.Name = "btnCollaboration"
        Me.btnCollaboration.SubItemsExpandWidth = 14
        Me.btnCollaboration.Text = "Collaboration"
        '
        'btnUserManager
        '
        Me.btnUserManager.BeginGroup = True
        Me.btnUserManager.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnUserManager.Image = CType(resources.GetObject("btnUserManager.Image"), System.Drawing.Image)
        Me.btnUserManager.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUserManager.Name = "btnUserManager"
        Me.btnUserManager.SubItemsExpandWidth = 14
        Me.btnUserManager.Text = "User Manager"
        '
        'btnSMTPServers
        '
        Me.btnSMTPServers.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSMTPServers.Image = CType(resources.GetObject("btnSMTPServers.Image"), System.Drawing.Image)
        Me.btnSMTPServers.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSMTPServers.Name = "btnSMTPServers"
        Me.btnSMTPServers.SubItemsExpandWidth = 14
        Me.btnSMTPServers.Text = "SMTP Servers"
        '
        'btnExportSchedules
        '
        Me.btnExportSchedules.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnExportSchedules.Image = CType(resources.GetObject("btnExportSchedules.Image"), System.Drawing.Image)
        Me.btnExportSchedules.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnExportSchedules.Name = "btnExportSchedules"
        Me.btnExportSchedules.SubItemsExpandWidth = 14
        Me.btnExportSchedules.Text = "Export Schedules"
        '
        'btnOperationHours
        '
        Me.btnOperationHours.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOperationHours.Image = CType(resources.GetObject("btnOperationHours.Image"), System.Drawing.Image)
        Me.btnOperationHours.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnOperationHours.Name = "btnOperationHours"
        Me.btnOperationHours.SubItemsExpandWidth = 14
        Me.btnOperationHours.Text = "Operation Hours"
        '
        'RibbonPanel3
        '
        Me.RibbonPanel3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonPanel3.Controls.Add(Me.stabFeatures)
        Me.RibbonPanel3.Controls.Add(Me.stabActivation)
        Me.RibbonPanel3.Controls.Add(Me.stabSupport)
        Me.RibbonPanel3.Controls.Add(Me.stabHelp)
        Me.RibbonPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.RibbonPanel3.Location = New System.Drawing.Point(0, 54)
        Me.RibbonPanel3.Name = "RibbonPanel3"
        Me.RibbonPanel3.Padding = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.RibbonPanel3.Size = New System.Drawing.Size(1596, 98)
        '
        '
        '
        Me.RibbonPanel3.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonPanel3.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonPanel3.TabIndex = 3
        '
        'stabFeatures
        '
        Me.stabFeatures.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabFeatures.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabFeatures.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabFeatures.ContainerControlProcessDialogKey = True
        Me.stabFeatures.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabFeatures.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnUpdateFeatures, Me.btnValidateFeatures})
        Me.stabFeatures.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabFeatures.Location = New System.Drawing.Point(561, 0)
        Me.stabFeatures.Name = "stabFeatures"
        Me.stabFeatures.Size = New System.Drawing.Size(119, 95)
        Me.stabFeatures.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabFeatures.TabIndex = 3
        Me.stabFeatures.Text = "Features"
        '
        '
        '
        Me.stabFeatures.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabFeatures.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnUpdateFeatures
        '
        Me.btnUpdateFeatures.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnUpdateFeatures.Image = CType(resources.GetObject("btnUpdateFeatures.Image"), System.Drawing.Image)
        Me.btnUpdateFeatures.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUpdateFeatures.Name = "btnUpdateFeatures"
        Me.btnUpdateFeatures.SubItemsExpandWidth = 14
        Me.btnUpdateFeatures.Text = "Upgrade Features"
        '
        'btnValidateFeatures
        '
        Me.btnValidateFeatures.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnValidateFeatures.Image = CType(resources.GetObject("btnValidateFeatures.Image"), System.Drawing.Image)
        Me.btnValidateFeatures.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnValidateFeatures.Name = "btnValidateFeatures"
        Me.btnValidateFeatures.SubItemsExpandWidth = 14
        Me.btnValidateFeatures.Text = "Validate Features"
        '
        'stabActivation
        '
        Me.stabActivation.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabActivation.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabActivation.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabActivation.ContainerControlProcessDialogKey = True
        Me.stabActivation.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabActivation.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnActivate, Me.btnDeactivate})
        Me.stabActivation.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabActivation.Location = New System.Drawing.Point(449, 0)
        Me.stabActivation.Name = "stabActivation"
        Me.stabActivation.Size = New System.Drawing.Size(112, 95)
        Me.stabActivation.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabActivation.TabIndex = 2
        Me.stabActivation.Text = "Activation"
        '
        '
        '
        Me.stabActivation.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabActivation.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnActivate
        '
        Me.btnActivate.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnActivate.Image = CType(resources.GetObject("btnActivate.Image"), System.Drawing.Image)
        Me.btnActivate.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnActivate.Name = "btnActivate"
        Me.btnActivate.SubItemsExpandWidth = 14
        Me.btnActivate.Text = "Activate"
        '
        'btnDeactivate
        '
        Me.btnDeactivate.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDeactivate.Image = CType(resources.GetObject("btnDeactivate.Image"), System.Drawing.Image)
        Me.btnDeactivate.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDeactivate.Name = "btnDeactivate"
        Me.btnDeactivate.SubItemsExpandWidth = 14
        Me.btnDeactivate.Text = "Deactivate"
        '
        'stabSupport
        '
        Me.stabSupport.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabSupport.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSupport.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabSupport.ContainerControlProcessDialogKey = True
        Me.stabSupport.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabSupport.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSupportFiles, Me.btnLogCall})
        Me.stabSupport.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabSupport.Location = New System.Drawing.Point(355, 0)
        Me.stabSupport.Name = "stabSupport"
        Me.stabSupport.Size = New System.Drawing.Size(94, 95)
        Me.stabSupport.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSupport.TabIndex = 1
        Me.stabSupport.Text = "Get Support"
        '
        '
        '
        Me.stabSupport.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabSupport.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnSupportFiles
        '
        Me.btnSupportFiles.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSupportFiles.Image = CType(resources.GetObject("btnSupportFiles.Image"), System.Drawing.Image)
        Me.btnSupportFiles.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnSupportFiles.Name = "btnSupportFiles"
        Me.btnSupportFiles.SubItemsExpandWidth = 14
        Me.btnSupportFiles.Text = "Support Files"
        '
        'btnLogCall
        '
        Me.btnLogCall.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnLogCall.Image = CType(resources.GetObject("btnLogCall.Image"), System.Drawing.Image)
        Me.btnLogCall.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnLogCall.Name = "btnLogCall"
        Me.btnLogCall.SubItemsExpandWidth = 14
        Me.btnLogCall.Text = "Log a Call"
        '
        'stabHelp
        '
        Me.stabHelp.AutoOverflowEnabled = True
        '
        '
        '
        Me.stabHelp.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabHelp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.stabHelp.ContainerControlProcessDialogKey = True
        Me.stabHelp.Dock = System.Windows.Forms.DockStyle.Left
        Me.stabHelp.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAboutToo, Me.btnHelp, Me.btnDemos, Me.btnforums, Me.btnKbase, Me.btnUpdateCheck, Me.btnProfServices})
        Me.stabHelp.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.stabHelp.Location = New System.Drawing.Point(3, 0)
        Me.stabHelp.Name = "stabHelp"
        Me.stabHelp.Size = New System.Drawing.Size(352, 95)
        Me.stabHelp.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabHelp.TabIndex = 0
        Me.stabHelp.Text = "Help  &&  Information"
        '
        '
        '
        Me.stabHelp.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.stabHelp.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'btnAboutToo
        '
        Me.btnAboutToo.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnAboutToo.Image = CType(resources.GetObject("btnAboutToo.Image"), System.Drawing.Image)
        Me.btnAboutToo.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnAboutToo.Name = "btnAboutToo"
        Me.btnAboutToo.SubItemsExpandWidth = 14
        Me.btnAboutToo.Text = "About SQL-RD"
        '
        'btnHelp
        '
        Me.btnHelp.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.SubItemsExpandWidth = 14
        Me.btnHelp.Text = "Help"
        '
        'btnDemos
        '
        Me.btnDemos.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnDemos.Image = CType(resources.GetObject("btnDemos.Image"), System.Drawing.Image)
        Me.btnDemos.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnDemos.Name = "btnDemos"
        Me.btnDemos.SubItemsExpandWidth = 14
        Me.btnDemos.Text = "Demos"
        '
        'btnforums
        '
        Me.btnforums.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnforums.Image = CType(resources.GetObject("btnforums.Image"), System.Drawing.Image)
        Me.btnforums.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnforums.Name = "btnforums"
        Me.btnforums.SubItemsExpandWidth = 14
        Me.btnforums.Text = "User Forums"
        '
        'btnKbase
        '
        Me.btnKbase.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnKbase.Image = CType(resources.GetObject("btnKbase.Image"), System.Drawing.Image)
        Me.btnKbase.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnKbase.Name = "btnKbase"
        Me.btnKbase.SubItemsExpandWidth = 14
        Me.btnKbase.Text = "Browse KBase"
        '
        'btnUpdateCheck
        '
        Me.btnUpdateCheck.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnUpdateCheck.Image = CType(resources.GetObject("btnUpdateCheck.Image"), System.Drawing.Image)
        Me.btnUpdateCheck.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnUpdateCheck.Name = "btnUpdateCheck"
        Me.btnUpdateCheck.SubItemsExpandWidth = 14
        Me.btnUpdateCheck.Text = "Check for Updates"
        '
        'btnProfServices
        '
        Me.btnProfServices.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnProfServices.Image = CType(resources.GetObject("btnProfServices.Image"), System.Drawing.Image)
        Me.btnProfServices.ImagePosition = DevComponents.DotNetBar.eImagePosition.Top
        Me.btnProfServices.Name = "btnProfServices"
        Me.btnProfServices.SubItemsExpandWidth = 14
        Me.btnProfServices.Text = "Prof. Services"
        '
        'Office2007StartButton1
        '
        Me.Office2007StartButton1.AutoExpandOnClick = True
        Me.Office2007StartButton1.BackstageTab = Me.SuperTabControl1
        Me.Office2007StartButton1.CanCustomize = False
        Me.Office2007StartButton1.HotTrackingStyle = DevComponents.DotNetBar.eHotTrackingStyle.Image
        Me.Office2007StartButton1.Image = CType(resources.GetObject("Office2007StartButton1.Image"), System.Drawing.Image)
        Me.Office2007StartButton1.ImageFixedSize = New System.Drawing.Size(16, 16)
        Me.Office2007StartButton1.ImagePaddingHorizontal = 0
        Me.Office2007StartButton1.ImagePaddingVertical = 1
        Me.Office2007StartButton1.Name = "Office2007StartButton1"
        Me.Office2007StartButton1.ShowSubItems = False
        Me.Office2007StartButton1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer1})
        Me.Office2007StartButton1.Text = "&File"
        '
        'SuperTabControl1
        '
        Me.SuperTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SuperTabControl1.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.SuperTabControl1.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.SuperTabControl1.ControlBox.MenuBox.Name = ""
        Me.SuperTabControl1.ControlBox.Name = ""
        Me.SuperTabControl1.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabControl1.ControlBox.MenuBox, Me.SuperTabControl1.ControlBox.CloseBox})
        Me.SuperTabControl1.ControlBox.Visible = False
        Me.SuperTabControl1.Controls.Add(Me.SuperTabControlPanel4)
        Me.SuperTabControl1.ForeColor = System.Drawing.Color.Black
        Me.SuperTabControl1.ItemPadding.Left = 6
        Me.SuperTabControl1.ItemPadding.Right = 4
        Me.SuperTabControl1.ItemPadding.Top = 4
        Me.SuperTabControl1.Location = New System.Drawing.Point(5, 53)
        Me.SuperTabControl1.Name = "SuperTabControl1"
        Me.SuperTabControl1.ReorderTabsEnabled = False
        Me.SuperTabControl1.SelectedTabFont = New System.Drawing.Font("Segoe UI", 9.75!)
        Me.SuperTabControl1.SelectedTabIndex = 4
        Me.SuperTabControl1.Size = New System.Drawing.Size(1596, 815)
        Me.SuperTabControl1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.SuperTabControl1.TabFont = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SuperTabControl1.TabHorizontalSpacing = 16
        Me.SuperTabControl1.TabIndex = 7
        Me.SuperTabControl1.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnOpenReport, Me.btnQuickEmail, Me.ButtonItem8, Me.SuperTabItem4, Me.btnOptions1, Me.btnLogOut, Me.ButtonItem10})
        Me.SuperTabControl1.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.SuperTabControl1.TabVerticalSpacing = 8
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.BackgroundImage = CType(resources.GetObject("SuperTabControlPanel4.BackgroundImage"), System.Drawing.Image)
        Me.SuperTabControlPanel4.BackgroundImagePosition = DevComponents.DotNetBar.eStyleBackgroundImage.BottomRight
        Me.SuperTabControlPanel4.Controls.Add(Me.WebBrowser1)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(236, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(1360, 815)
        Me.SuperTabControlPanel4.TabIndex = 4
        Me.SuperTabControlPanel4.TabItem = Me.SuperTabItem4
        '
        'WebBrowser1
        '
        Me.WebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser1.Location = New System.Drawing.Point(0, 0)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.Size = New System.Drawing.Size(1360, 815)
        Me.WebBrowser1.TabIndex = 0
        '
        'SuperTabItem4
        '
        Me.SuperTabItem4.AttachedControl = Me.SuperTabControlPanel4
        Me.SuperTabItem4.GlobalItem = False
        Me.SuperTabItem4.Image = CType(resources.GetObject("SuperTabItem4.Image"), System.Drawing.Image)
        Me.SuperTabItem4.KeyTips = "H"
        Me.SuperTabItem4.Name = "SuperTabItem4"
        Me.SuperTabItem4.Text = "Community"
        '
        'btnOpenReport
        '
        Me.btnOpenReport.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOpenReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.btnOpenReport.Image = CType(resources.GetObject("btnOpenReport.Image"), System.Drawing.Image)
        Me.btnOpenReport.ImagePaddingHorizontal = 18
        Me.btnOpenReport.ImagePaddingVertical = 10
        Me.btnOpenReport.KeyTips = "O"
        Me.btnOpenReport.Name = "btnOpenReport"
        Me.btnOpenReport.Stretch = True
        Me.btnOpenReport.Text = "Open"
        Me.btnOpenReport.Visible = False
        '
        'btnQuickEmail
        '
        Me.btnQuickEmail.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnQuickEmail.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.btnQuickEmail.Image = CType(resources.GetObject("btnQuickEmail.Image"), System.Drawing.Image)
        Me.btnQuickEmail.ImagePaddingHorizontal = 18
        Me.btnQuickEmail.ImagePaddingVertical = 10
        Me.btnQuickEmail.Name = "btnQuickEmail"
        Me.btnQuickEmail.Stretch = True
        Me.btnQuickEmail.Text = "Quick Email"
        Me.btnQuickEmail.Tooltip = "Send a quick email using SQL-RD"
        '
        'ButtonItem8
        '
        Me.ButtonItem8.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem8.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.ButtonItem8.Image = CType(resources.GetObject("ButtonItem8.Image"), System.Drawing.Image)
        Me.ButtonItem8.ImagePaddingHorizontal = 18
        Me.ButtonItem8.ImagePaddingVertical = 10
        Me.ButtonItem8.KeyTips = "C"
        Me.ButtonItem8.Name = "ButtonItem8"
        Me.ButtonItem8.Stretch = True
        Me.ButtonItem8.Text = "Close"
        '
        'btnOptions1
        '
        Me.btnOptions1.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOptions1.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.btnOptions1.Image = CType(resources.GetObject("btnOptions1.Image"), System.Drawing.Image)
        Me.btnOptions1.ImagePaddingHorizontal = 18
        Me.btnOptions1.ImagePaddingVertical = 10
        Me.btnOptions1.KeyTips = "T"
        Me.btnOptions1.Name = "btnOptions1"
        Me.btnOptions1.Stretch = True
        Me.btnOptions1.Text = "Options"
        '
        'btnLogOut
        '
        Me.btnLogOut.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnLogOut.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.btnLogOut.Image = CType(resources.GetObject("btnLogOut.Image"), System.Drawing.Image)
        Me.btnLogOut.ImagePaddingHorizontal = 18
        Me.btnLogOut.ImagePaddingVertical = 10
        Me.btnLogOut.Name = "btnLogOut"
        Me.btnLogOut.Stretch = True
        Me.btnLogOut.Text = "Log Out"
        '
        'ButtonItem10
        '
        Me.ButtonItem10.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem10.ColorTable = DevComponents.DotNetBar.eButtonColor.Blue
        Me.ButtonItem10.Image = CType(resources.GetObject("ButtonItem10.Image"), System.Drawing.Image)
        Me.ButtonItem10.ImagePaddingHorizontal = 18
        Me.ButtonItem10.ImagePaddingVertical = 10
        Me.ButtonItem10.KeyTips = "X"
        Me.ButtonItem10.Name = "ButtonItem10"
        Me.ButtonItem10.Stretch = True
        Me.ButtonItem10.Text = "Exit"
        '
        'ItemContainer1
        '
        '
        '
        '
        Me.ItemContainer1.BackgroundStyle.Class = "RibbonFileMenuContainer"
        Me.ItemContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer1.Name = "ItemContainer1"
        Me.ItemContainer1.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer2, Me.ItemContainer4})
        '
        '
        '
        Me.ItemContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer2
        '
        '
        '
        '
        Me.ItemContainer2.BackgroundStyle.Class = "RibbonFileMenuTwoColumnContainer"
        Me.ItemContainer2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer2.ItemSpacing = 0
        Me.ItemContainer2.Name = "ItemContainer2"
        Me.ItemContainer2.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ItemContainer3, Me.GalleryContainer1})
        '
        '
        '
        Me.ItemContainer2.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer3
        '
        '
        '
        '
        Me.ItemContainer3.BackgroundStyle.Class = "RibbonFileMenuColumnOneContainer"
        Me.ItemContainer3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer3.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.ItemContainer3.MinimumSize = New System.Drawing.Size(120, 0)
        Me.ItemContainer3.Name = "ItemContainer3"
        Me.ItemContainer3.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem2, Me.ButtonItem3, Me.ButtonItem5, Me.ButtonItem7})
        '
        '
        '
        Me.ItemContainer3.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem2
        '
        Me.ButtonItem2.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem2.Image = CType(resources.GetObject("ButtonItem2.Image"), System.Drawing.Image)
        Me.ButtonItem2.Name = "ButtonItem2"
        Me.ButtonItem2.Text = "&New..."
        '
        'ButtonItem3
        '
        Me.ButtonItem3.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem3.Image = CType(resources.GetObject("ButtonItem3.Image"), System.Drawing.Image)
        Me.ButtonItem3.Name = "ButtonItem3"
        Me.ButtonItem3.SubItemsExpandWidth = 24
        Me.ButtonItem3.Text = "&Open Report..."
        '
        'ButtonItem5
        '
        Me.ButtonItem5.BeginGroup = True
        Me.ButtonItem5.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem5.Image = CType(resources.GetObject("ButtonItem5.Image"), System.Drawing.Image)
        Me.ButtonItem5.Name = "ButtonItem5"
        Me.ButtonItem5.SubItemsExpandWidth = 24
        Me.ButtonItem5.Text = "&Log Out"
        '
        'ButtonItem7
        '
        Me.ButtonItem7.BeginGroup = True
        Me.ButtonItem7.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem7.Image = CType(resources.GetObject("ButtonItem7.Image"), System.Drawing.Image)
        Me.ButtonItem7.Name = "ButtonItem7"
        Me.ButtonItem7.SubItemsExpandWidth = 24
        Me.ButtonItem7.Text = "&Close"
        '
        'GalleryContainer1
        '
        '
        '
        '
        Me.GalleryContainer1.BackgroundStyle.Class = "RibbonFileMenuColumnTwoContainer"
        Me.GalleryContainer1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GalleryContainer1.EnableGalleryPopup = False
        Me.GalleryContainer1.LayoutOrientation = DevComponents.DotNetBar.eOrientation.Vertical
        Me.GalleryContainer1.MinimumSize = New System.Drawing.Size(180, 240)
        Me.GalleryContainer1.MultiLine = False
        Me.GalleryContainer1.Name = "GalleryContainer1"
        Me.GalleryContainer1.PopupUsesStandardScrollbars = False
        '
        '
        '
        Me.GalleryContainer1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ItemContainer4
        '
        '
        '
        '
        Me.ItemContainer4.BackgroundStyle.Class = "RibbonFileMenuBottomContainer"
        Me.ItemContainer4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ItemContainer4.HorizontalItemAlignment = DevComponents.DotNetBar.eHorizontalItemsAlignment.Right
        Me.ItemContainer4.Name = "ItemContainer4"
        Me.ItemContainer4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.ButtonItem12, Me.ButtonItem13})
        '
        '
        '
        Me.ItemContainer4.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'ButtonItem12
        '
        Me.ButtonItem12.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem12.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem12.Image = CType(resources.GetObject("ButtonItem12.Image"), System.Drawing.Image)
        Me.ButtonItem12.Name = "ButtonItem12"
        Me.ButtonItem12.SubItemsExpandWidth = 24
        Me.ButtonItem12.Text = "Opt&ions"
        '
        'ButtonItem13
        '
        Me.ButtonItem13.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.ButtonItem13.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.ButtonItem13.Image = CType(resources.GetObject("ButtonItem13.Image"), System.Drawing.Image)
        Me.ButtonItem13.Name = "ButtonItem13"
        Me.ButtonItem13.SubItemsExpandWidth = 24
        Me.ButtonItem13.Text = "E&xit"
        '
        'rtabCreate
        '
        Me.rtabCreate.Name = "rtabCreate"
        Me.rtabCreate.Panel = Me.RibbonPanel1
        Me.rtabCreate.Text = "Home"
        '
        'rtabSystem
        '
        Me.rtabSystem.Name = "rtabSystem"
        Me.rtabSystem.Panel = Me.RibbonPanel2
        Me.rtabSystem.Text = "System"
        '
        'rtabResources
        '
        Me.rtabResources.Checked = True
        Me.rtabResources.Name = "rtabResources"
        Me.rtabResources.Panel = Me.RibbonPanel3
        Me.rtabResources.Text = "Resources"
        '
        'rtabConfiguration
        '
        Me.rtabConfiguration.Name = "rtabConfiguration"
        Me.rtabConfiguration.Panel = Me.RibbonPanel4
        Me.rtabConfiguration.Text = "Configuration"
        '
        'btnNewFolder
        '
        Me.btnNewFolder.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnNewFolder.Image = CType(resources.GetObject("btnNewFolder.Image"), System.Drawing.Image)
        Me.btnNewFolder.Name = "btnNewFolder"
        Me.btnNewFolder.Text = "New Folder"
        '
        'btnNewSmartFolder
        '
        Me.btnNewSmartFolder.Image = CType(resources.GetObject("btnNewSmartFolder.Image"), System.Drawing.Image)
        Me.btnNewSmartFolder.Name = "btnNewSmartFolder"
        Me.btnNewSmartFolder.Text = "New Smart Folder"
        Me.btnNewSmartFolder.Tooltip = "New Smart Folder"
        '
        'btnOptions
        '
        Me.btnOptions.BeginGroup = True
        Me.btnOptions.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnOptions.Image = CType(resources.GetObject("btnOptions.Image"), System.Drawing.Image)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.Text = "Options"
        '
        'btnSystemMonitor
        '
        Me.btnSystemMonitor.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.btnSystemMonitor.Image = CType(resources.GetObject("btnSystemMonitor.Image"), System.Drawing.Image)
        Me.btnSystemMonitor.Name = "btnSystemMonitor"
        Me.btnSystemMonitor.Text = "System Monitor"
        '
        'btnPasteItem
        '
        Me.btnPasteItem.Enabled = False
        Me.btnPasteItem.Image = CType(resources.GetObject("btnPasteItem.Image"), System.Drawing.Image)
        Me.btnPasteItem.Name = "btnPasteItem"
        Me.btnPasteItem.Text = "Paste"
        Me.btnPasteItem.Tooltip = "Paste Item"
        '
        'btnAbout
        '
        Me.btnAbout.Image = CType(resources.GetObject("btnAbout.Image"), System.Drawing.Image)
        Me.btnAbout.Name = "btnAbout"
        Me.btnAbout.Text = "About SQL-RD"
        '
        'btnSmallHelp
        '
        Me.btnSmallHelp.Image = CType(resources.GetObject("btnSmallHelp.Image"), System.Drawing.Image)
        Me.btnSmallHelp.Name = "btnSmallHelp"
        Me.btnSmallHelp.Text = "Help"
        '
        'QatCustomizeItem1
        '
        Me.QatCustomizeItem1.Name = "QatCustomizeItem1"
        '
        'ComboItem1
        '
        Me.ComboItem1.Text = "ClassicBlue"
        '
        'ComboItem2
        '
        Me.ComboItem2.Text = "ClassicSilver"
        '
        'ComboItem3
        '
        Me.ComboItem3.Text = "ClassicBlack"
        '
        'ComboItem4
        '
        Me.ComboItem4.Text = "NewBlue"
        '
        'ComboItem5
        '
        Me.ComboItem5.Text = "NewSilver"
        '
        'ComboItem6
        '
        Me.ComboItem6.Text = "NewBlack"
        '
        'ComboItem7
        '
        Me.ComboItem7.Text = "Vista"
        '
        'ComboItem8
        '
        Me.ComboItem8.Text = "Seven"
        '
        'StyleManager2
        '
        Me.StyleManager2.ManagerStyle = DevComponents.DotNetBar.eStyle.Metro
        Me.StyleManager2.MetroColorParameters = New DevComponents.DotNetBar.Metro.ColorTables.MetroColorGeneratorParameters(System.Drawing.Color.White, System.Drawing.Color.FromArgb(CType(CType(237, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(0, Byte), Integer)))
        '
        'tvExplorer
        '
        Me.tvExplorer.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvExplorer.AllowDrop = True
        Me.tvExplorer.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvExplorer.BackgroundStyle.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tvExplorer.BackgroundStyle.BackColor2 = System.Drawing.Color.AliceBlue
        Me.tvExplorer.BackgroundStyle.BackColorGradientAngle = 45
        Me.tvExplorer.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvExplorer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ContextMenuBar1.SetContextMenuEx(Me.tvExplorer, Me.mnuFolders)
        Me.tvExplorer.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvExplorer.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvExplorer.ExpandLineColor = System.Drawing.Color.White
        Me.tvExplorer.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tvExplorer.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvExplorer.Location = New System.Drawing.Point(5, 155)
        Me.tvExplorer.Name = "tvExplorer"
        Me.tvExplorer.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node5})
        Me.tvExplorer.NodesConnector = Me.NodeConnector1
        Me.tvExplorer.NodeStyle = Me.ElementStyle1
        Me.tvExplorer.PathSeparator = ";"
        Me.tvExplorer.Size = New System.Drawing.Size(168, 651)
        Me.tvExplorer.Styles.Add(Me.ElementStyle1)
        Me.tvExplorer.Styles.Add(Me.ElementStyle3)
        Me.tvExplorer.TabIndex = 1
        Me.tvExplorer.Text = "AdvTree1"
        '
        'Node5
        '
        Me.Node5.Expanded = True
        Me.Node5.ImageIndex = 1
        Me.Node5.Name = "Node5"
        Me.Node5.Text = "Loading..."
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle3
        '
        Me.ElementStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(236, Byte), Integer))
        Me.ElementStyle3.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(213, Byte), Integer), CType(CType(164, Byte), Integer), CType(CType(187, Byte), Integer))
        Me.ElementStyle3.BackColorGradientAngle = 90
        Me.ElementStyle3.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderBottomWidth = 1
        Me.ElementStyle3.BorderColor = System.Drawing.Color.DarkGray
        Me.ElementStyle3.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderLeftWidth = 1
        Me.ElementStyle3.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderRightWidth = 1
        Me.ElementStyle3.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle3.BorderTopWidth = 1
        Me.ElementStyle3.CornerDiameter = 4
        Me.ElementStyle3.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle3.Description = "Magenta"
        Me.ElementStyle3.Name = "ElementStyle3"
        Me.ElementStyle3.PaddingBottom = 1
        Me.ElementStyle3.PaddingLeft = 1
        Me.ElementStyle3.PaddingRight = 1
        Me.ElementStyle3.PaddingTop = 1
        Me.ElementStyle3.TextColor = System.Drawing.Color.Black
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandableSplitter1.ExpandableControl = Me.tvExplorer
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.ForeColor = System.Drawing.Color.Black
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.White
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.White
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(173, 155)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(4, 651)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007
        Me.ExpandableSplitter1.TabIndex = 2
        Me.ExpandableSplitter1.TabStop = False
        '
        'tvMain
        '
        Me.tvMain.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvMain.AllowDrop = True
        Me.tvMain.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvMain.BackgroundStyle.BackColor = System.Drawing.Color.White
        Me.tvMain.BackgroundStyle.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(231, Byte), Integer), CType(CType(227, Byte), Integer), CType(CType(231, Byte), Integer))
        Me.tvMain.BackgroundStyle.BackColorGradientAngle = 45
        Me.tvMain.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvMain.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ContextMenuBar1.SetContextMenuEx(Me.tvMain, Me.btnSingleContext)
        Me.tvMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvMain.DragDropNodeCopyEnabled = False
        Me.tvMain.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvMain.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tvMain.GroupNodeStyle = Me.ElementStyle4
        Me.tvMain.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvMain.Location = New System.Drawing.Point(0, 22)
        Me.tvMain.MultiSelect = True
        Me.tvMain.MultiSelectRule = DevComponents.AdvTree.eMultiSelectRule.AnyNode
        Me.tvMain.Name = "tvMain"
        Me.tvMain.NodesConnector = Me.NodeConnector2
        Me.tvMain.NodeStyle = Me.ElementStyle2
        Me.tvMain.PathSeparator = ";"
        Me.tvMain.Size = New System.Drawing.Size(1157, 629)
        Me.tvMain.Styles.Add(Me.ElementStyle2)
        Me.tvMain.Styles.Add(Me.ElementStyle4)
        Me.tvMain.TabIndex = 3
        Me.tvMain.Text = "AdvTree2"
        Me.tvMain.View = DevComponents.AdvTree.eView.Tile
        '
        'ElementStyle4
        '
        Me.ElementStyle4.BackColor = System.Drawing.Color.FromArgb(CType(CType(221, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.ElementStyle4.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(138, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(228, Byte), Integer))
        Me.ElementStyle4.BackColorGradientAngle = 90
        Me.ElementStyle4.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle4.BorderBottomWidth = 1
        Me.ElementStyle4.BorderColor = System.Drawing.Color.DarkGray
        Me.ElementStyle4.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle4.BorderLeftWidth = 1
        Me.ElementStyle4.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle4.BorderRightWidth = 1
        Me.ElementStyle4.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.ElementStyle4.BorderTopWidth = 1
        Me.ElementStyle4.CornerDiameter = 4
        Me.ElementStyle4.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle4.Description = "Blue"
        Me.ElementStyle4.Name = "ElementStyle4"
        Me.ElementStyle4.PaddingBottom = 1
        Me.ElementStyle4.PaddingLeft = 1
        Me.ElementStyle4.PaddingRight = 1
        Me.ElementStyle4.PaddingTop = 1
        Me.ElementStyle4.TextColor = System.Drawing.Color.Black
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'Cell1
        '
        Me.Cell1.Name = "Cell1"
        Me.Cell1.StyleMouseOver = Nothing
        '
        'Cell2
        '
        Me.Cell2.Name = "Cell2"
        Me.Cell2.StyleMouseOver = Nothing
        '
        'Cell3
        '
        Me.Cell3.Name = "Cell3"
        Me.Cell3.StyleMouseOver = Nothing
        '
        'DotNetBarManager1
        '
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlY)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.DotNetBarManager1.BottomDockSite = Me.DockSite4
        Me.DotNetBarManager1.EnableFullSizeDock = False
        Me.DotNetBarManager1.LeftDockSite = Me.DockSite1
        Me.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.DotNetBarManager1.ParentForm = Me
        Me.DotNetBarManager1.RightDockSite = Me.DockSite2
        Me.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.DotNetBarManager1.ToolbarBottomDockSite = Me.DockSite8
        Me.DotNetBarManager1.ToolbarLeftDockSite = Me.DockSite5
        Me.DotNetBarManager1.ToolbarRightDockSite = Me.DockSite6
        Me.DotNetBarManager1.ToolbarTopDockSite = Me.DockSite7
        Me.DotNetBarManager1.TopDockSite = Me.DockSite3
        '
        'DockSite4
        '
        Me.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite4.Controls.Add(Me.Bar1)
        Me.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite4.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer(New DevComponents.DotNetBar.DocumentBaseContainer() {CType(New DevComponents.DotNetBar.DocumentBarContainer(Me.Bar1, 1596, 62), DevComponents.DotNetBar.DocumentBaseContainer)}, DevComponents.DotNetBar.eOrientation.Vertical)
        Me.DockSite4.Location = New System.Drawing.Point(5, 806)
        Me.DockSite4.Name = "DockSite4"
        Me.DockSite4.Size = New System.Drawing.Size(1596, 65)
        Me.DockSite4.TabIndex = 12
        Me.DockSite4.TabStop = False
        '
        'Bar1
        '
        Me.Bar1.AccessibleDescription = "DotNetBar Bar (Bar1)"
        Me.Bar1.AccessibleName = "DotNetBar Bar"
        Me.Bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.Bar1.AntiAlias = True
        Me.Bar1.AutoCreateCaptionMenu = False
        Me.Bar1.AutoHide = True
        Me.Bar1.AutoSyncBarCaption = True
        Me.Bar1.CloseSingleTab = True
        Me.Bar1.Controls.Add(Me.PanelDockContainer2)
        Me.Bar1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.DockContainerItem4})
        Me.Bar1.LayoutType = DevComponents.DotNetBar.eLayoutType.DockContainer
        Me.Bar1.Location = New System.Drawing.Point(0, 3)
        Me.Bar1.Name = "Bar1"
        Me.Bar1.Size = New System.Drawing.Size(1596, 62)
        Me.Bar1.Stretch = True
        Me.Bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.Bar1.TabIndex = 0
        Me.Bar1.TabStop = False
        Me.Bar1.Text = "Status"
        '
        'PanelDockContainer2
        '
        Me.PanelDockContainer2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelDockContainer2.Controls.Add(Me.btnCancel)
        Me.PanelDockContainer2.Controls.Add(Me.lblProgress)
        Me.PanelDockContainer2.Controls.Add(Me.pgx)
        Me.PanelDockContainer2.Location = New System.Drawing.Point(3, 3)
        Me.PanelDockContainer2.Name = "PanelDockContainer2"
        Me.PanelDockContainer2.Size = New System.Drawing.Size(1590, 56)
        Me.PanelDockContainer2.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelDockContainer2.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.PanelDockContainer2.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.PanelDockContainer2.Style.GradientAngle = 90
        Me.PanelDockContainer2.TabIndex = 0
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground
        Me.btnCancel.Location = New System.Drawing.Point(516, 22)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(47, 23)
        Me.btnCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        '
        'lblProgress
        '
        Me.lblProgress.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblProgress.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblProgress.Location = New System.Drawing.Point(10, 12)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(499, 18)
        Me.lblProgress.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.lblProgress.TabIndex = 1
        Me.lblProgress.Text = "Ready"
        '
        'pgx
        '
        '
        '
        '
        Me.pgx.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pgx.Location = New System.Drawing.Point(9, 33)
        Me.pgx.Name = "pgx"
        Me.pgx.Size = New System.Drawing.Size(500, 13)
        Me.pgx.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.pgx.TabIndex = 0
        Me.pgx.Text = "ProgressBarX1"
        Me.pgx.Visible = False
        '
        'DockContainerItem4
        '
        Me.DockContainerItem4.Control = Me.PanelDockContainer2
        Me.DockContainerItem4.Name = "DockContainerItem4"
        Me.DockContainerItem4.Text = "Status"
        '
        'DockSite1
        '
        Me.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite1.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite1.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.DockSite1.Location = New System.Drawing.Point(177, 155)
        Me.DockSite1.Name = "DockSite1"
        Me.DockSite1.Size = New System.Drawing.Size(0, 651)
        Me.DockSite1.TabIndex = 9
        Me.DockSite1.TabStop = False
        '
        'DockSite2
        '
        Me.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite2.Controls.Add(Me.dnbRight)
        Me.DockSite2.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite2.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer(New DevComponents.DotNetBar.DocumentBaseContainer() {CType(New DevComponents.DotNetBar.DocumentBarContainer(Me.dnbRight, 264, 651), DevComponents.DotNetBar.DocumentBaseContainer)}, DevComponents.DotNetBar.eOrientation.Horizontal)
        Me.DockSite2.Location = New System.Drawing.Point(1334, 155)
        Me.DockSite2.Name = "DockSite2"
        Me.DockSite2.Size = New System.Drawing.Size(267, 651)
        Me.DockSite2.TabIndex = 10
        Me.DockSite2.TabStop = False
        '
        'dnbRight
        '
        Me.dnbRight.AccessibleDescription = "DotNetBar Bar (dnbRight)"
        Me.dnbRight.AccessibleName = "DotNetBar Bar"
        Me.dnbRight.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.dnbRight.AutoSyncBarCaption = True
        Me.dnbRight.BarType = DevComponents.DotNetBar.eBarType.DockWindow
        Me.dnbRight.CanCustomize = False
        Me.dnbRight.CanDockRight = False
        Me.dnbRight.CanDockTop = False
        Me.dnbRight.CloseSingleTab = True
        Me.dnbRight.Controls.Add(Me.PanelDockContainer3)
        Me.dnbRight.Controls.Add(Me.PanelDockContainer1)
        Me.dnbRight.FadeEffect = True
        Me.dnbRight.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dnbRight.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Caption
        Me.dnbRight.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.DockContainerItem3, Me.DockContainerItem1})
        Me.dnbRight.LayoutType = DevComponents.DotNetBar.eLayoutType.DockContainer
        Me.dnbRight.Location = New System.Drawing.Point(3, 0)
        Me.dnbRight.Name = "dnbRight"
        Me.dnbRight.SelectedDockTab = 0
        Me.dnbRight.Size = New System.Drawing.Size(264, 651)
        Me.dnbRight.Stretch = True
        Me.dnbRight.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dnbRight.TabIndex = 0
        Me.dnbRight.TabStop = False
        Me.dnbRight.Text = "System"
        '
        'PanelDockContainer3
        '
        Me.PanelDockContainer3.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelDockContainer3.Controls.Add(Me.advProps)
        Me.PanelDockContainer3.Controls.Add(Me.UcSchedulerControler1)
        Me.PanelDockContainer3.Controls.Add(Me.UcScheduleCount1)
        Me.PanelDockContainer3.Location = New System.Drawing.Point(3, 23)
        Me.PanelDockContainer3.Name = "PanelDockContainer3"
        Me.PanelDockContainer3.Size = New System.Drawing.Size(258, 600)
        Me.PanelDockContainer3.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelDockContainer3.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.PanelDockContainer3.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.PanelDockContainer3.Style.GradientAngle = 90
        Me.PanelDockContainer3.TabIndex = 2
        '
        'advProps
        '
        Me.advProps.Dock = System.Windows.Forms.DockStyle.Fill
        Me.advProps.GridLinesColor = System.Drawing.Color.WhiteSmoke
        Me.advProps.Location = New System.Drawing.Point(0, 305)
        Me.advProps.Name = "advProps"
        Me.advProps.Size = New System.Drawing.Size(258, 253)
        Me.advProps.TabIndex = 1
        Me.advProps.Text = "AdvPropertyGrid1"
        '
        'UcSchedulerControler1
        '
        Me.UcSchedulerControler1.BackColor = System.Drawing.Color.Transparent
        Me.UcSchedulerControler1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcSchedulerControler1.Location = New System.Drawing.Point(0, 0)
        Me.UcSchedulerControler1.Name = "UcSchedulerControler1"
        Me.UcSchedulerControler1.Size = New System.Drawing.Size(258, 305)
        Me.UcSchedulerControler1.TabIndex = 0
        '
        'UcScheduleCount1
        '
        Me.UcScheduleCount1.BackColor = System.Drawing.Color.Transparent
        Me.UcScheduleCount1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UcScheduleCount1.Location = New System.Drawing.Point(0, 558)
        Me.UcScheduleCount1.Name = "UcScheduleCount1"
        Me.UcScheduleCount1.numberOfReports = 5
        Me.UcScheduleCount1.Size = New System.Drawing.Size(258, 42)
        Me.UcScheduleCount1.TabIndex = 2
        Me.UcScheduleCount1.Visible = False
        '
        'PanelDockContainer1
        '
        Me.PanelDockContainer1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.PanelDockContainer1.Controls.Add(Me.ucHistory)
        Me.PanelDockContainer1.Location = New System.Drawing.Point(3, 23)
        Me.PanelDockContainer1.Name = "PanelDockContainer1"
        Me.PanelDockContainer1.Size = New System.Drawing.Size(258, 600)
        Me.PanelDockContainer1.Style.Alignment = System.Drawing.StringAlignment.Center
        Me.PanelDockContainer1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.PanelDockContainer1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.PanelDockContainer1.Style.GradientAngle = 90
        Me.PanelDockContainer1.TabIndex = 0
        '
        'ucHistory
        '
        Me.ucHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucHistory.Location = New System.Drawing.Point(0, 0)
        Me.ucHistory.m_filter = False
        Me.ucHistory.m_historyLimit = 0
        Me.ucHistory.m_objectID = 0
        Me.ucHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.NONE
        Me.ucHistory.m_showControls = False
        Me.ucHistory.m_showSchedulesList = False
        Me.ucHistory.m_sortOrder = " ASC "
        Me.ucHistory.Name = "ucHistory"
        Me.ucHistory.Size = New System.Drawing.Size(258, 600)
        Me.ucHistory.TabIndex = 0
        '
        'DockContainerItem3
        '
        Me.DockContainerItem3.Control = Me.PanelDockContainer3
        Me.DockContainerItem3.Name = "DockContainerItem3"
        Me.DockContainerItem3.Text = "System"
        '
        'DockContainerItem1
        '
        Me.DockContainerItem1.Control = Me.PanelDockContainer1
        Me.DockContainerItem1.Name = "DockContainerItem1"
        Me.DockContainerItem1.Text = "History (latest 10)"
        '
        'DockSite8
        '
        Me.DockSite8.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite8.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite8.Location = New System.Drawing.Point(5, 871)
        Me.DockSite8.Name = "DockSite8"
        Me.DockSite8.Size = New System.Drawing.Size(1596, 0)
        Me.DockSite8.TabIndex = 16
        Me.DockSite8.TabStop = False
        '
        'DockSite5
        '
        Me.DockSite5.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite5.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite5.Location = New System.Drawing.Point(5, 1)
        Me.DockSite5.Name = "DockSite5"
        Me.DockSite5.Size = New System.Drawing.Size(0, 870)
        Me.DockSite5.TabIndex = 13
        Me.DockSite5.TabStop = False
        '
        'DockSite6
        '
        Me.DockSite6.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite6.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite6.Location = New System.Drawing.Point(1601, 1)
        Me.DockSite6.Name = "DockSite6"
        Me.DockSite6.Size = New System.Drawing.Size(0, 870)
        Me.DockSite6.TabIndex = 14
        Me.DockSite6.TabStop = False
        '
        'DockSite7
        '
        Me.DockSite7.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite7.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite7.Location = New System.Drawing.Point(5, 1)
        Me.DockSite7.Name = "DockSite7"
        Me.DockSite7.Size = New System.Drawing.Size(1596, 0)
        Me.DockSite7.TabIndex = 15
        Me.DockSite7.TabStop = False
        '
        'DockSite3
        '
        Me.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite3.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite3.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.DockSite3.Location = New System.Drawing.Point(5, 1)
        Me.DockSite3.Name = "DockSite3"
        Me.DockSite3.Size = New System.Drawing.Size(1596, 0)
        Me.DockSite3.TabIndex = 11
        Me.DockSite3.TabStop = False
        '
        'pg
        '
        '
        '
        '
        Me.pg.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pg.Location = New System.Drawing.Point(3, 3)
        Me.pg.Name = "pg"
        Me.pg.ProgressBarType = DevComponents.DotNetBar.eCircularProgressType.Dot
        Me.pg.ProgressColor = System.Drawing.Color.Green
        Me.pg.ProgressTextVisible = True
        Me.pg.Size = New System.Drawing.Size(252, 147)
        Me.pg.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.pg.TabIndex = 0
        '
        'DockContainerItem2
        '
        Me.DockContainerItem2.Name = "DockContainerItem2"
        Me.DockContainerItem2.Text = "DockContainerItem2"
        '
        'ContextMenuBar1
        '
        Me.ContextMenuBar1.AntiAlias = True
        Me.ContextMenuBar1.Font = New System.Drawing.Font("Segoe UI", 9.0!)
        Me.ContextMenuBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSingleContext, Me.btnPackageContext, Me.btnAutomationSchedules, Me.btneventBasedSchedules, Me.btnEventBasedPacks, Me.mnuFolders, Me.mnuMultipleItems, Me.mnuSmartFolders, Me.mnudesktopMenu, Me.btnPackedReportMenu})
        Me.ContextMenuBar1.Location = New System.Drawing.Point(384, 4)
        Me.ContextMenuBar1.Name = "ContextMenuBar1"
        Me.ContextMenuBar1.Size = New System.Drawing.Size(644, 47)
        Me.ContextMenuBar1.Stretch = True
        Me.ContextMenuBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.ContextMenuBar1.TabIndex = 17
        Me.ContextMenuBar1.TabStop = False
        Me.ContextMenuBar1.Text = "ContextMenuBar1"
        '
        'mnuFolders
        '
        Me.mnuFolders.AutoExpandOnClick = True
        Me.mnuFolders.Name = "mnuFolders"
        Me.mnuFolders.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnfolderNew, Me.btnfolderOpenInNewWindow, Me.btnfolderMove, Me.btnFolderRename, Me.btnfolderDelete, Me.ButtonItem11})
        Me.mnuFolders.Text = "folders"
        '
        'btnfolderNew
        '
        Me.btnfolderNew.Image = CType(resources.GetObject("btnfolderNew.Image"), System.Drawing.Image)
        Me.btnfolderNew.Name = "btnfolderNew"
        Me.btnfolderNew.Text = "New Folder"
        '
        'btnfolderOpenInNewWindow
        '
        Me.btnfolderOpenInNewWindow.BeginGroup = True
        Me.btnfolderOpenInNewWindow.Image = CType(resources.GetObject("btnfolderOpenInNewWindow.Image"), System.Drawing.Image)
        Me.btnfolderOpenInNewWindow.Name = "btnfolderOpenInNewWindow"
        Me.btnfolderOpenInNewWindow.Text = "Open in New Window"
        '
        'btnfolderMove
        '
        Me.btnfolderMove.BeginGroup = True
        Me.btnfolderMove.Image = CType(resources.GetObject("btnfolderMove.Image"), System.Drawing.Image)
        Me.btnfolderMove.Name = "btnfolderMove"
        Me.btnfolderMove.Text = "Move"
        '
        'btnFolderRename
        '
        Me.btnFolderRename.Image = CType(resources.GetObject("btnFolderRename.Image"), System.Drawing.Image)
        Me.btnFolderRename.Name = "btnFolderRename"
        Me.btnFolderRename.Text = "Rename"
        '
        'btnfolderDelete
        '
        Me.btnfolderDelete.Image = CType(resources.GetObject("btnfolderDelete.Image"), System.Drawing.Image)
        Me.btnfolderDelete.Name = "btnfolderDelete"
        Me.btnfolderDelete.Text = "Delete"
        '
        'ButtonItem11
        '
        Me.ButtonItem11.BeginGroup = True
        Me.ButtonItem11.Name = "ButtonItem11"
        Me.ButtonItem11.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnfolderRefresh, Me.btnfolderDisable, Me.btnfolderEnable, Me.btnfolderExecute})
        Me.ButtonItem11.Text = "All Contents"
        '
        'btnfolderRefresh
        '
        Me.btnfolderRefresh.Image = CType(resources.GetObject("btnfolderRefresh.Image"), System.Drawing.Image)
        Me.btnfolderRefresh.Name = "btnfolderRefresh"
        Me.btnfolderRefresh.Text = "Refresh"
        '
        'btnfolderDisable
        '
        Me.btnfolderDisable.Image = CType(resources.GetObject("btnfolderDisable.Image"), System.Drawing.Image)
        Me.btnfolderDisable.Name = "btnfolderDisable"
        Me.btnfolderDisable.Text = "Disable"
        '
        'btnfolderEnable
        '
        Me.btnfolderEnable.Image = CType(resources.GetObject("btnfolderEnable.Image"), System.Drawing.Image)
        Me.btnfolderEnable.Name = "btnfolderEnable"
        Me.btnfolderEnable.Text = "Enable"
        '
        'btnfolderExecute
        '
        Me.btnfolderExecute.Image = CType(resources.GetObject("btnfolderExecute.Image"), System.Drawing.Image)
        Me.btnfolderExecute.Name = "btnfolderExecute"
        Me.btnfolderExecute.Text = "Execute"
        '
        'btnSingleContext
        '
        Me.btnSingleContext.AutoExpandOnClick = True
        Me.btnSingleContext.Name = "btnSingleContext"
        Me.btnSingleContext.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSingleProperties, Me.btnSingleCopy, Me.btnSinglePaste, Me.btnSingleRename, Me.btnSingleEnabled, Me.btnMoveSingle, Me.btnSingleRefresh, Me.btnSinglePreview, Me.btnSingleExecute, Me.btnSingleDelete, Me.btnSingleTools})
        Me.btnSingleContext.Text = "singleSchedules"
        '
        'btnSingleProperties
        '
        Me.btnSingleProperties.Image = CType(resources.GetObject("btnSingleProperties.Image"), System.Drawing.Image)
        Me.btnSingleProperties.Name = "btnSingleProperties"
        Me.btnSingleProperties.Text = "Properties"
        '
        'btnSingleCopy
        '
        Me.btnSingleCopy.BeginGroup = True
        Me.btnSingleCopy.Image = CType(resources.GetObject("btnSingleCopy.Image"), System.Drawing.Image)
        Me.btnSingleCopy.Name = "btnSingleCopy"
        Me.btnSingleCopy.Text = "Copy"
        '
        'btnSinglePaste
        '
        Me.btnSinglePaste.Enabled = False
        Me.btnSinglePaste.Image = CType(resources.GetObject("btnSinglePaste.Image"), System.Drawing.Image)
        Me.btnSinglePaste.Name = "btnSinglePaste"
        Me.btnSinglePaste.Text = "Paste"
        '
        'btnSingleRename
        '
        Me.btnSingleRename.Image = CType(resources.GetObject("btnSingleRename.Image"), System.Drawing.Image)
        Me.btnSingleRename.Name = "btnSingleRename"
        Me.btnSingleRename.Text = "Rename"
        '
        'btnSingleEnabled
        '
        Me.btnSingleEnabled.BeginGroup = True
        Me.btnSingleEnabled.Checked = True
        Me.btnSingleEnabled.Name = "btnSingleEnabled"
        Me.btnSingleEnabled.Text = "Enabled"
        '
        'btnMoveSingle
        '
        Me.btnMoveSingle.Name = "btnMoveSingle"
        Me.btnMoveSingle.Text = "Move"
        '
        'btnSingleRefresh
        '
        Me.btnSingleRefresh.BeginGroup = True
        Me.btnSingleRefresh.Image = CType(resources.GetObject("btnSingleRefresh.Image"), System.Drawing.Image)
        Me.btnSingleRefresh.Name = "btnSingleRefresh"
        Me.btnSingleRefresh.Text = "Refresh Schedule"
        Me.btnSingleRefresh.Visible = False
        '
        'btnSinglePreview
        '
        Me.btnSinglePreview.Image = CType(resources.GetObject("btnSinglePreview.Image"), System.Drawing.Image)
        Me.btnSinglePreview.Name = "btnSinglePreview"
        Me.btnSinglePreview.Text = "Preview"
        '
        'btnSingleExecute
        '
        Me.btnSingleExecute.Image = CType(resources.GetObject("btnSingleExecute.Image"), System.Drawing.Image)
        Me.btnSingleExecute.Name = "btnSingleExecute"
        Me.btnSingleExecute.Text = "Execute"
        '
        'btnSingleDelete
        '
        Me.btnSingleDelete.Image = CType(resources.GetObject("btnSingleDelete.Image"), System.Drawing.Image)
        Me.btnSingleDelete.Name = "btnSingleDelete"
        Me.btnSingleDelete.Text = "Delete"
        '
        'btnSingleTools
        '
        Me.btnSingleTools.BeginGroup = True
        Me.btnSingleTools.Name = "btnSingleTools"
        Me.btnSingleTools.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnReportDesign, Me.btnSingleTest, Me.btnConvertToPackage, Me.btnAdHocEmail, Me.btnCreateSingleShortcut, Me.btnAdHocView})
        Me.btnSingleTools.Text = "Tools"
        '
        'btnReportDesign
        '
        Me.btnReportDesign.Image = CType(resources.GetObject("btnReportDesign.Image"), System.Drawing.Image)
        Me.btnReportDesign.Name = "btnReportDesign"
        Me.btnReportDesign.Text = "Report Design"
        Me.btnReportDesign.Visible = False
        '
        'btnSingleTest
        '
        Me.btnSingleTest.BeginGroup = True
        Me.btnSingleTest.Image = CType(resources.GetObject("btnSingleTest.Image"), System.Drawing.Image)
        Me.btnSingleTest.Name = "btnSingleTest"
        Me.btnSingleTest.Text = "Test"
        '
        'btnConvertToPackage
        '
        Me.btnConvertToPackage.Enabled = False
        Me.btnConvertToPackage.Image = CType(resources.GetObject("btnConvertToPackage.Image"), System.Drawing.Image)
        Me.btnConvertToPackage.Name = "btnConvertToPackage"
        Me.btnConvertToPackage.Text = "Convert To Package"
        '
        'btnAdHocEmail
        '
        Me.btnAdHocEmail.BeginGroup = True
        Me.btnAdHocEmail.Image = CType(resources.GetObject("btnAdHocEmail.Image"), System.Drawing.Image)
        Me.btnAdHocEmail.Name = "btnAdHocEmail"
        Me.btnAdHocEmail.Text = "Ad-Hoc Email to Recipients"
        '
        'btnCreateSingleShortcut
        '
        Me.btnCreateSingleShortcut.Image = CType(resources.GetObject("btnCreateSingleShortcut.Image"), System.Drawing.Image)
        Me.btnCreateSingleShortcut.Name = "btnCreateSingleShortcut"
        Me.btnCreateSingleShortcut.Text = "Create Shortcut"
        '
        'btnAdHocView
        '
        Me.btnAdHocView.BeginGroup = True
        Me.btnAdHocView.Image = CType(resources.GetObject("btnAdHocView.Image"), System.Drawing.Image)
        Me.btnAdHocView.Name = "btnAdHocView"
        Me.btnAdHocView.Text = "Ad-Hoc View"
        '
        'btnPackageContext
        '
        Me.btnPackageContext.AutoExpandOnClick = True
        Me.btnPackageContext.Name = "btnPackageContext"
        Me.btnPackageContext.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuPackageProperties, Me.btnOpenPackage, Me.btnpackAddReport, Me.btnpackCopy, Me.btnpackPaste, Me.btnpackRename, Me.btnpackEnabled, Me.btnMovePackage, Me.btnpackRefreshPack, Me.btnpackExecute, Me.btnpackDelete, Me.btnpackTools})
        Me.btnPackageContext.Text = "packageSchedules"
        '
        'mnuPackageProperties
        '
        Me.mnuPackageProperties.Image = CType(resources.GetObject("mnuPackageProperties.Image"), System.Drawing.Image)
        Me.mnuPackageProperties.Name = "mnuPackageProperties"
        Me.mnuPackageProperties.Text = "Properties"
        '
        'btnOpenPackage
        '
        Me.btnOpenPackage.BeginGroup = True
        Me.btnOpenPackage.Image = CType(resources.GetObject("btnOpenPackage.Image"), System.Drawing.Image)
        Me.btnOpenPackage.Name = "btnOpenPackage"
        Me.btnOpenPackage.Text = "Open"
        '
        'btnpackAddReport
        '
        Me.btnpackAddReport.Image = CType(resources.GetObject("btnpackAddReport.Image"), System.Drawing.Image)
        Me.btnpackAddReport.Name = "btnpackAddReport"
        Me.btnpackAddReport.Text = "Add Report"
        '
        'btnpackCopy
        '
        Me.btnpackCopy.BeginGroup = True
        Me.btnpackCopy.Image = CType(resources.GetObject("btnpackCopy.Image"), System.Drawing.Image)
        Me.btnpackCopy.Name = "btnpackCopy"
        Me.btnpackCopy.Text = "Copy"
        '
        'btnpackPaste
        '
        Me.btnpackPaste.Enabled = False
        Me.btnpackPaste.Image = CType(resources.GetObject("btnpackPaste.Image"), System.Drawing.Image)
        Me.btnpackPaste.Name = "btnpackPaste"
        Me.btnpackPaste.Text = "Paste"
        '
        'btnpackRename
        '
        Me.btnpackRename.Image = CType(resources.GetObject("btnpackRename.Image"), System.Drawing.Image)
        Me.btnpackRename.Name = "btnpackRename"
        Me.btnpackRename.Text = "Rename"
        '
        'btnpackEnabled
        '
        Me.btnpackEnabled.BeginGroup = True
        Me.btnpackEnabled.Checked = True
        Me.btnpackEnabled.Name = "btnpackEnabled"
        Me.btnpackEnabled.Text = "Enabled"
        '
        'btnMovePackage
        '
        Me.btnMovePackage.Name = "btnMovePackage"
        Me.btnMovePackage.Text = "Move"
        '
        'btnpackRefreshPack
        '
        Me.btnpackRefreshPack.BeginGroup = True
        Me.btnpackRefreshPack.Image = CType(resources.GetObject("btnpackRefreshPack.Image"), System.Drawing.Image)
        Me.btnpackRefreshPack.Name = "btnpackRefreshPack"
        Me.btnpackRefreshPack.Text = "Refresh Package"
        Me.btnpackRefreshPack.Visible = False
        '
        'btnpackExecute
        '
        Me.btnpackExecute.Image = CType(resources.GetObject("btnpackExecute.Image"), System.Drawing.Image)
        Me.btnpackExecute.Name = "btnpackExecute"
        Me.btnpackExecute.Text = "Execute"
        '
        'btnpackDelete
        '
        Me.btnpackDelete.Image = CType(resources.GetObject("btnpackDelete.Image"), System.Drawing.Image)
        Me.btnpackDelete.Name = "btnpackDelete"
        Me.btnpackDelete.Text = "Delete"
        '
        'btnpackTools
        '
        Me.btnpackTools.BeginGroup = True
        Me.btnpackTools.Name = "btnpackTools"
        Me.btnpackTools.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnpackTest, Me.btnpackSplit, Me.btnpackAdhoc, Me.btnpackShortcut})
        Me.btnpackTools.Text = "Tools"
        '
        'btnpackTest
        '
        Me.btnpackTest.Image = CType(resources.GetObject("btnpackTest.Image"), System.Drawing.Image)
        Me.btnpackTest.Name = "btnpackTest"
        Me.btnpackTest.Text = "Test Package"
        '
        'btnpackSplit
        '
        Me.btnpackSplit.BeginGroup = True
        Me.btnpackSplit.Image = CType(resources.GetObject("btnpackSplit.Image"), System.Drawing.Image)
        Me.btnpackSplit.Name = "btnpackSplit"
        Me.btnpackSplit.Text = "Split into Single Schedules"
        '
        'btnpackAdhoc
        '
        Me.btnpackAdhoc.Image = CType(resources.GetObject("btnpackAdhoc.Image"), System.Drawing.Image)
        Me.btnpackAdhoc.Name = "btnpackAdhoc"
        Me.btnpackAdhoc.Text = "Ad-hoc Email Recipients"
        '
        'btnpackShortcut
        '
        Me.btnpackShortcut.Image = CType(resources.GetObject("btnpackShortcut.Image"), System.Drawing.Image)
        Me.btnpackShortcut.Name = "btnpackShortcut"
        Me.btnpackShortcut.Text = "Create Shortcut"
        '
        'btnAutomationSchedules
        '
        Me.btnAutomationSchedules.AutoExpandOnClick = True
        Me.btnAutomationSchedules.Name = "btnAutomationSchedules"
        Me.btnAutomationSchedules.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuAutoProp, Me.btnautoCopy, Me.btnautoPaste, Me.btnautoRename, Me.btnautoEnabled, Me.btnMoveAuto, Me.btnautoExecute, Me.btnautoDelete, Me.btnautoShortcut})
        Me.btnAutomationSchedules.Text = "automationSchedules"
        '
        'mnuAutoProp
        '
        Me.mnuAutoProp.Image = CType(resources.GetObject("mnuAutoProp.Image"), System.Drawing.Image)
        Me.mnuAutoProp.Name = "mnuAutoProp"
        Me.mnuAutoProp.Text = "Properties"
        '
        'btnautoCopy
        '
        Me.btnautoCopy.BeginGroup = True
        Me.btnautoCopy.Image = CType(resources.GetObject("btnautoCopy.Image"), System.Drawing.Image)
        Me.btnautoCopy.Name = "btnautoCopy"
        Me.btnautoCopy.Text = "Copy"
        '
        'btnautoPaste
        '
        Me.btnautoPaste.Enabled = False
        Me.btnautoPaste.Image = CType(resources.GetObject("btnautoPaste.Image"), System.Drawing.Image)
        Me.btnautoPaste.Name = "btnautoPaste"
        Me.btnautoPaste.Text = "Paste"
        '
        'btnautoRename
        '
        Me.btnautoRename.Image = CType(resources.GetObject("btnautoRename.Image"), System.Drawing.Image)
        Me.btnautoRename.Name = "btnautoRename"
        Me.btnautoRename.Text = "Rename"
        '
        'btnautoEnabled
        '
        Me.btnautoEnabled.BeginGroup = True
        Me.btnautoEnabled.Checked = True
        Me.btnautoEnabled.Name = "btnautoEnabled"
        Me.btnautoEnabled.Text = "Enabled"
        '
        'btnMoveAuto
        '
        Me.btnMoveAuto.Name = "btnMoveAuto"
        Me.btnMoveAuto.Text = "Move"
        '
        'btnautoExecute
        '
        Me.btnautoExecute.BeginGroup = True
        Me.btnautoExecute.Image = CType(resources.GetObject("btnautoExecute.Image"), System.Drawing.Image)
        Me.btnautoExecute.Name = "btnautoExecute"
        Me.btnautoExecute.Text = "Execute"
        '
        'btnautoDelete
        '
        Me.btnautoDelete.Image = CType(resources.GetObject("btnautoDelete.Image"), System.Drawing.Image)
        Me.btnautoDelete.Name = "btnautoDelete"
        Me.btnautoDelete.Text = "Delete"
        '
        'btnautoShortcut
        '
        Me.btnautoShortcut.BeginGroup = True
        Me.btnautoShortcut.Image = CType(resources.GetObject("btnautoShortcut.Image"), System.Drawing.Image)
        Me.btnautoShortcut.Name = "btnautoShortcut"
        Me.btnautoShortcut.Text = "Create Shortcut"
        '
        'btneventBasedSchedules
        '
        Me.btneventBasedSchedules.AutoExpandOnClick = True
        Me.btneventBasedSchedules.Name = "btneventBasedSchedules"
        Me.btneventBasedSchedules.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuEventProp, Me.btneventCopy, Me.btneventPaste, Me.btneventRename, Me.btneventEnabled, Me.btnMoveEvent, Me.btneventExecute, Me.btneventDelete, Me.btneventShortcut})
        Me.btneventBasedSchedules.Text = "eventBasedSchedules"
        '
        'mnuEventProp
        '
        Me.mnuEventProp.Image = CType(resources.GetObject("mnuEventProp.Image"), System.Drawing.Image)
        Me.mnuEventProp.Name = "mnuEventProp"
        Me.mnuEventProp.Text = "Properties"
        '
        'btneventCopy
        '
        Me.btneventCopy.BeginGroup = True
        Me.btneventCopy.Image = CType(resources.GetObject("btneventCopy.Image"), System.Drawing.Image)
        Me.btneventCopy.Name = "btneventCopy"
        Me.btneventCopy.Text = "Copy"
        '
        'btneventPaste
        '
        Me.btneventPaste.Enabled = False
        Me.btneventPaste.Image = CType(resources.GetObject("btneventPaste.Image"), System.Drawing.Image)
        Me.btneventPaste.Name = "btneventPaste"
        Me.btneventPaste.Text = "Paste"
        '
        'btneventRename
        '
        Me.btneventRename.Image = CType(resources.GetObject("btneventRename.Image"), System.Drawing.Image)
        Me.btneventRename.Name = "btneventRename"
        Me.btneventRename.Text = "Rename"
        '
        'btneventEnabled
        '
        Me.btneventEnabled.BeginGroup = True
        Me.btneventEnabled.Checked = True
        Me.btneventEnabled.Name = "btneventEnabled"
        Me.btneventEnabled.Text = "Enabled"
        '
        'btnMoveEvent
        '
        Me.btnMoveEvent.Name = "btnMoveEvent"
        Me.btnMoveEvent.Text = "Move"
        '
        'btneventExecute
        '
        Me.btneventExecute.BeginGroup = True
        Me.btneventExecute.Image = CType(resources.GetObject("btneventExecute.Image"), System.Drawing.Image)
        Me.btneventExecute.Name = "btneventExecute"
        Me.btneventExecute.Text = "Execute"
        '
        'btneventDelete
        '
        Me.btneventDelete.Image = CType(resources.GetObject("btneventDelete.Image"), System.Drawing.Image)
        Me.btneventDelete.Name = "btneventDelete"
        Me.btneventDelete.Text = "Delete"
        '
        'btneventShortcut
        '
        Me.btneventShortcut.BeginGroup = True
        Me.btneventShortcut.Image = CType(resources.GetObject("btneventShortcut.Image"), System.Drawing.Image)
        Me.btneventShortcut.Name = "btneventShortcut"
        Me.btneventShortcut.Text = "Create Shortcut"
        '
        'btnEventBasedPacks
        '
        Me.btnEventBasedPacks.AutoExpandOnClick = True
        Me.btnEventBasedPacks.Name = "btnEventBasedPacks"
        Me.btnEventBasedPacks.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnueventPackProps, Me.btneventpackAdd, Me.btneventpackCopy, Me.btneventpackPaste, Me.btneventpackRename, Me.btneventpackEnabled, Me.btnMoveEventPackage, Me.btneventpackOpen, Me.btneventpackExecute, Me.btneventpackDelete, Me.btneventpackTools})
        Me.btnEventBasedPacks.Text = "eventBasedPacks"
        '
        'mnueventPackProps
        '
        Me.mnueventPackProps.Image = CType(resources.GetObject("mnueventPackProps.Image"), System.Drawing.Image)
        Me.mnueventPackProps.Name = "mnueventPackProps"
        Me.mnueventPackProps.Text = "Properties"
        '
        'btneventpackAdd
        '
        Me.btneventpackAdd.BeginGroup = True
        Me.btneventpackAdd.Name = "btneventpackAdd"
        Me.btneventpackAdd.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btneventpackAddNew, Me.btneventpackAddExisting})
        Me.btneventpackAdd.Text = "Add Schedule"
        '
        'btneventpackAddNew
        '
        Me.btneventpackAddNew.Name = "btneventpackAddNew"
        Me.btneventpackAddNew.Text = "New"
        '
        'btneventpackAddExisting
        '
        Me.btneventpackAddExisting.Name = "btneventpackAddExisting"
        Me.btneventpackAddExisting.Text = "Existing"
        '
        'btneventpackCopy
        '
        Me.btneventpackCopy.BeginGroup = True
        Me.btneventpackCopy.Image = CType(resources.GetObject("btneventpackCopy.Image"), System.Drawing.Image)
        Me.btneventpackCopy.Name = "btneventpackCopy"
        Me.btneventpackCopy.Text = "Copy"
        '
        'btneventpackPaste
        '
        Me.btneventpackPaste.Enabled = False
        Me.btneventpackPaste.Image = CType(resources.GetObject("btneventpackPaste.Image"), System.Drawing.Image)
        Me.btneventpackPaste.Name = "btneventpackPaste"
        Me.btneventpackPaste.Text = "Paste"
        '
        'btneventpackRename
        '
        Me.btneventpackRename.Image = CType(resources.GetObject("btneventpackRename.Image"), System.Drawing.Image)
        Me.btneventpackRename.Name = "btneventpackRename"
        Me.btneventpackRename.Text = "Rename"
        '
        'btneventpackEnabled
        '
        Me.btneventpackEnabled.BeginGroup = True
        Me.btneventpackEnabled.Name = "btneventpackEnabled"
        Me.btneventpackEnabled.Text = "Enabled"
        '
        'btnMoveEventPackage
        '
        Me.btnMoveEventPackage.Name = "btnMoveEventPackage"
        Me.btnMoveEventPackage.Text = "Move"
        '
        'btneventpackOpen
        '
        Me.btneventpackOpen.BeginGroup = True
        Me.btneventpackOpen.Image = CType(resources.GetObject("btneventpackOpen.Image"), System.Drawing.Image)
        Me.btneventpackOpen.Name = "btneventpackOpen"
        Me.btneventpackOpen.Text = "Open"
        '
        'btneventpackExecute
        '
        Me.btneventpackExecute.Image = CType(resources.GetObject("btneventpackExecute.Image"), System.Drawing.Image)
        Me.btneventpackExecute.Name = "btneventpackExecute"
        Me.btneventpackExecute.Text = "Execute"
        '
        'btneventpackDelete
        '
        Me.btneventpackDelete.Image = CType(resources.GetObject("btneventpackDelete.Image"), System.Drawing.Image)
        Me.btneventpackDelete.Name = "btneventpackDelete"
        Me.btneventpackDelete.Text = "Delete"
        '
        'btneventpackTools
        '
        Me.btneventpackTools.BeginGroup = True
        Me.btneventpackTools.Name = "btneventpackTools"
        Me.btneventpackTools.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btneventpackSplit, Me.btneventpackShortcut})
        Me.btneventpackTools.Text = "Tools"
        '
        'btneventpackSplit
        '
        Me.btneventpackSplit.Image = CType(resources.GetObject("btneventpackSplit.Image"), System.Drawing.Image)
        Me.btneventpackSplit.Name = "btneventpackSplit"
        Me.btneventpackSplit.Text = "Split Package"
        '
        'btneventpackShortcut
        '
        Me.btneventpackShortcut.Image = CType(resources.GetObject("btneventpackShortcut.Image"), System.Drawing.Image)
        Me.btneventpackShortcut.Name = "btneventpackShortcut"
        Me.btneventpackShortcut.Text = "Create Shortcut"
        '
        'mnuMultipleItems
        '
        Me.mnuMultipleItems.AutoExpandOnClick = True
        Me.mnuMultipleItems.Name = "mnuMultipleItems"
        Me.mnuMultipleItems.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnmltProperties, Me.btnmltMove, Me.btnmltRefresh, Me.btnmltExecute, Me.btnmltDelete, Me.btnmltConvertToPackage, Me.btnmltMoveToPackage, Me.btnmltStatus, Me.btnCollaborationServer})
        Me.mnuMultipleItems.Text = "multipleItems"
        '
        'btnmltProperties
        '
        Me.btnmltProperties.Image = CType(resources.GetObject("btnmltProperties.Image"), System.Drawing.Image)
        Me.btnmltProperties.Name = "btnmltProperties"
        Me.btnmltProperties.Text = "Properties"
        '
        'btnmltMove
        '
        Me.btnmltMove.BeginGroup = True
        Me.btnmltMove.Image = CType(resources.GetObject("btnmltMove.Image"), System.Drawing.Image)
        Me.btnmltMove.Name = "btnmltMove"
        Me.btnmltMove.Text = "Move"
        '
        'btnmltRefresh
        '
        Me.btnmltRefresh.Image = CType(resources.GetObject("btnmltRefresh.Image"), System.Drawing.Image)
        Me.btnmltRefresh.Name = "btnmltRefresh"
        Me.btnmltRefresh.Text = "Refresh"
        Me.btnmltRefresh.Visible = False
        '
        'btnmltExecute
        '
        Me.btnmltExecute.Image = CType(resources.GetObject("btnmltExecute.Image"), System.Drawing.Image)
        Me.btnmltExecute.Name = "btnmltExecute"
        Me.btnmltExecute.Text = "Execute"
        '
        'btnmltDelete
        '
        Me.btnmltDelete.Image = CType(resources.GetObject("btnmltDelete.Image"), System.Drawing.Image)
        Me.btnmltDelete.Name = "btnmltDelete"
        Me.btnmltDelete.Text = "Delete"
        '
        'btnmltConvertToPackage
        '
        Me.btnmltConvertToPackage.BeginGroup = True
        Me.btnmltConvertToPackage.Image = CType(resources.GetObject("btnmltConvertToPackage.Image"), System.Drawing.Image)
        Me.btnmltConvertToPackage.Name = "btnmltConvertToPackage"
        Me.btnmltConvertToPackage.Text = "Convert To Package"
        '
        'btnmltMoveToPackage
        '
        Me.btnmltMoveToPackage.Image = CType(resources.GetObject("btnmltMoveToPackage.Image"), System.Drawing.Image)
        Me.btnmltMoveToPackage.Name = "btnmltMoveToPackage"
        Me.btnmltMoveToPackage.Text = "Move to Package"
        '
        'btnmltStatus
        '
        Me.btnmltStatus.BeginGroup = True
        Me.btnmltStatus.Name = "btnmltStatus"
        Me.btnmltStatus.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnmltEnable, Me.btnmltDisable})
        Me.btnmltStatus.Text = "Status"
        '
        'btnmltEnable
        '
        Me.btnmltEnable.Image = CType(resources.GetObject("btnmltEnable.Image"), System.Drawing.Image)
        Me.btnmltEnable.Name = "btnmltEnable"
        Me.btnmltEnable.Text = "Enable"
        '
        'btnmltDisable
        '
        Me.btnmltDisable.Image = CType(resources.GetObject("btnmltDisable.Image"), System.Drawing.Image)
        Me.btnmltDisable.Name = "btnmltDisable"
        Me.btnmltDisable.Text = "Disable"
        '
        'btnCollaborationServer
        '
        Me.btnCollaborationServer.Image = CType(resources.GetObject("btnCollaborationServer.Image"), System.Drawing.Image)
        Me.btnCollaborationServer.Name = "btnCollaborationServer"
        Me.btnCollaborationServer.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.cmbCollaborationServer})
        Me.btnCollaborationServer.Text = "Collaboration Server"
        '
        'cmbCollaborationServer
        '
        Me.cmbCollaborationServer.ComboWidth = 120
        Me.cmbCollaborationServer.DropDownHeight = 106
        Me.cmbCollaborationServer.ItemHeight = 14
        Me.cmbCollaborationServer.Name = "cmbCollaborationServer"
        '
        'mnuSmartFolders
        '
        Me.mnuSmartFolders.AutoExpandOnClick = True
        Me.mnuSmartFolders.Name = "mnuSmartFolders"
        Me.mnuSmartFolders.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSmartProperties, Me.btnSmartViewReport, Me.btnSmartRename, Me.btnSmartDelete, Me.btnSmartAllContents})
        Me.mnuSmartFolders.Text = "smartfolder"
        '
        'btnSmartProperties
        '
        Me.btnSmartProperties.Image = CType(resources.GetObject("btnSmartProperties.Image"), System.Drawing.Image)
        Me.btnSmartProperties.Name = "btnSmartProperties"
        Me.btnSmartProperties.Text = "Properties"
        '
        'btnSmartViewReport
        '
        Me.btnSmartViewReport.BeginGroup = True
        Me.btnSmartViewReport.Image = CType(resources.GetObject("btnSmartViewReport.Image"), System.Drawing.Image)
        Me.btnSmartViewReport.Name = "btnSmartViewReport"
        Me.btnSmartViewReport.Text = "View as Report"
        '
        'btnSmartRename
        '
        Me.btnSmartRename.BeginGroup = True
        Me.btnSmartRename.Image = CType(resources.GetObject("btnSmartRename.Image"), System.Drawing.Image)
        Me.btnSmartRename.Name = "btnSmartRename"
        Me.btnSmartRename.Text = "Rename"
        '
        'btnSmartDelete
        '
        Me.btnSmartDelete.Image = CType(resources.GetObject("btnSmartDelete.Image"), System.Drawing.Image)
        Me.btnSmartDelete.Name = "btnSmartDelete"
        Me.btnSmartDelete.Text = "Delete"
        '
        'btnSmartAllContents
        '
        Me.btnSmartAllContents.BeginGroup = True
        Me.btnSmartAllContents.Name = "btnSmartAllContents"
        Me.btnSmartAllContents.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnSmartRefresh, Me.btnSmartDisable, Me.btnSmartEnable, Me.btnSmartExecute})
        Me.btnSmartAllContents.Text = "All Contents"
        '
        'btnSmartRefresh
        '
        Me.btnSmartRefresh.Image = CType(resources.GetObject("btnSmartRefresh.Image"), System.Drawing.Image)
        Me.btnSmartRefresh.Name = "btnSmartRefresh"
        Me.btnSmartRefresh.Text = "Refresh"
        '
        'btnSmartDisable
        '
        Me.btnSmartDisable.Image = CType(resources.GetObject("btnSmartDisable.Image"), System.Drawing.Image)
        Me.btnSmartDisable.Name = "btnSmartDisable"
        Me.btnSmartDisable.Text = "Disable"
        '
        'btnSmartEnable
        '
        Me.btnSmartEnable.Image = CType(resources.GetObject("btnSmartEnable.Image"), System.Drawing.Image)
        Me.btnSmartEnable.Name = "btnSmartEnable"
        Me.btnSmartEnable.Text = "Enable"
        '
        'btnSmartExecute
        '
        Me.btnSmartExecute.Image = CType(resources.GetObject("btnSmartExecute.Image"), System.Drawing.Image)
        Me.btnSmartExecute.Name = "btnSmartExecute"
        Me.btnSmartExecute.Text = "Execute"
        '
        'mnudesktopMenu
        '
        Me.mnudesktopMenu.AutoExpandOnClick = True
        Me.mnudesktopMenu.Name = "mnudesktopMenu"
        Me.mnudesktopMenu.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnuDesktopPaste, Me.ButtonItem4})
        Me.mnudesktopMenu.Text = "desktopMenu"
        '
        'mnuDesktopPaste
        '
        Me.mnuDesktopPaste.Enabled = False
        Me.mnuDesktopPaste.Image = CType(resources.GetObject("mnuDesktopPaste.Image"), System.Drawing.Image)
        Me.mnuDesktopPaste.Name = "mnuDesktopPaste"
        Me.mnuDesktopPaste.Text = "Paste"
        '
        'ButtonItem4
        '
        Me.ButtonItem4.BeginGroup = True
        Me.ButtonItem4.Name = "ButtonItem4"
        Me.ButtonItem4.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.mnudtNewFolder, Me.mnudtNewSmartFolder, Me.mnudtNewSingleSchedule, Me.mnudtNewPackage, Me.mnudtNewDynamicSchedule, Me.mnudtNewDynamicPackage, Me.mnudtNewEventSchedule, Me.mnudtNewEventPackage, Me.mnudtNewDataDriven, Me.mnudtNewDataDrivenPackage, Me.mnudtNewBurstingSchedule, Me.mnudtNewAutomationSchedule})
        Me.ButtonItem4.Text = "New"
        '
        'mnudtNewFolder
        '
        Me.mnudtNewFolder.Image = CType(resources.GetObject("mnudtNewFolder.Image"), System.Drawing.Image)
        Me.mnudtNewFolder.Name = "mnudtNewFolder"
        Me.mnudtNewFolder.Text = "Folder"
        '
        'mnudtNewSmartFolder
        '
        Me.mnudtNewSmartFolder.Image = CType(resources.GetObject("mnudtNewSmartFolder.Image"), System.Drawing.Image)
        Me.mnudtNewSmartFolder.Name = "mnudtNewSmartFolder"
        Me.mnudtNewSmartFolder.Text = "Smart Folder"
        '
        'mnudtNewSingleSchedule
        '
        Me.mnudtNewSingleSchedule.BeginGroup = True
        Me.mnudtNewSingleSchedule.Image = CType(resources.GetObject("mnudtNewSingleSchedule.Image"), System.Drawing.Image)
        Me.mnudtNewSingleSchedule.Name = "mnudtNewSingleSchedule"
        Me.mnudtNewSingleSchedule.Text = "Single Schedule"
        '
        'mnudtNewPackage
        '
        Me.mnudtNewPackage.Image = CType(resources.GetObject("mnudtNewPackage.Image"), System.Drawing.Image)
        Me.mnudtNewPackage.Name = "mnudtNewPackage"
        Me.mnudtNewPackage.Text = "Package Schedule"
        '
        'mnudtNewDynamicSchedule
        '
        Me.mnudtNewDynamicSchedule.BeginGroup = True
        Me.mnudtNewDynamicSchedule.Image = CType(resources.GetObject("mnudtNewDynamicSchedule.Image"), System.Drawing.Image)
        Me.mnudtNewDynamicSchedule.Name = "mnudtNewDynamicSchedule"
        Me.mnudtNewDynamicSchedule.Text = "Dynamic Schedule"
        '
        'mnudtNewDynamicPackage
        '
        Me.mnudtNewDynamicPackage.Image = CType(resources.GetObject("mnudtNewDynamicPackage.Image"), System.Drawing.Image)
        Me.mnudtNewDynamicPackage.Name = "mnudtNewDynamicPackage"
        Me.mnudtNewDynamicPackage.Text = "Dynamic Package"
        '
        'mnudtNewEventSchedule
        '
        Me.mnudtNewEventSchedule.BeginGroup = True
        Me.mnudtNewEventSchedule.Image = CType(resources.GetObject("mnudtNewEventSchedule.Image"), System.Drawing.Image)
        Me.mnudtNewEventSchedule.Name = "mnudtNewEventSchedule"
        Me.mnudtNewEventSchedule.Text = "Event-Based Schedule"
        '
        'mnudtNewEventPackage
        '
        Me.mnudtNewEventPackage.Image = CType(resources.GetObject("mnudtNewEventPackage.Image"), System.Drawing.Image)
        Me.mnudtNewEventPackage.Name = "mnudtNewEventPackage"
        Me.mnudtNewEventPackage.Text = "Event-Based Package"
        '
        'mnudtNewDataDriven
        '
        Me.mnudtNewDataDriven.BeginGroup = True
        Me.mnudtNewDataDriven.Image = CType(resources.GetObject("mnudtNewDataDriven.Image"), System.Drawing.Image)
        Me.mnudtNewDataDriven.Name = "mnudtNewDataDriven"
        Me.mnudtNewDataDriven.Text = "Data-Driven Schedule"
        '
        'mnudtNewDataDrivenPackage
        '
        Me.mnudtNewDataDrivenPackage.Image = CType(resources.GetObject("mnudtNewDataDrivenPackage.Image"), System.Drawing.Image)
        Me.mnudtNewDataDrivenPackage.Name = "mnudtNewDataDrivenPackage"
        Me.mnudtNewDataDrivenPackage.Text = "Data-Driven Package"
        '
        'mnudtNewBurstingSchedule
        '
        Me.mnudtNewBurstingSchedule.BeginGroup = True
        Me.mnudtNewBurstingSchedule.Image = CType(resources.GetObject("mnudtNewBurstingSchedule.Image"), System.Drawing.Image)
        Me.mnudtNewBurstingSchedule.Name = "mnudtNewBurstingSchedule"
        Me.mnudtNewBurstingSchedule.Text = "Bursting Schedule"
        Me.mnudtNewBurstingSchedule.Visible = False
        '
        'mnudtNewAutomationSchedule
        '
        Me.mnudtNewAutomationSchedule.BeginGroup = True
        Me.mnudtNewAutomationSchedule.Image = CType(resources.GetObject("mnudtNewAutomationSchedule.Image"), System.Drawing.Image)
        Me.mnudtNewAutomationSchedule.Name = "mnudtNewAutomationSchedule"
        Me.mnudtNewAutomationSchedule.Text = "Automation Schedule"
        '
        'btnPackedReportMenu
        '
        Me.btnPackedReportMenu.AutoExpandOnClick = True
        Me.btnPackedReportMenu.Name = "btnPackedReportMenu"
        Me.btnPackedReportMenu.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnPackagedProperties, Me.btnPackedRename, Me.btnPackedStatus, Me.btnMovePackedReport, Me.btnPackedRefresh, Me.btnPackedPreview, Me.btnPackedExecute, Me.btnPackedDelete, Me.btnPackedAdhoc, Me.btnPackedDesign})
        Me.btnPackedReportMenu.Text = "packedReports"
        '
        'btnPackagedProperties
        '
        Me.btnPackagedProperties.Image = CType(resources.GetObject("btnPackagedProperties.Image"), System.Drawing.Image)
        Me.btnPackagedProperties.Name = "btnPackagedProperties"
        Me.btnPackagedProperties.Text = "Properties"
        '
        'btnPackedRename
        '
        Me.btnPackedRename.Image = CType(resources.GetObject("btnPackedRename.Image"), System.Drawing.Image)
        Me.btnPackedRename.Name = "btnPackedRename"
        Me.btnPackedRename.Text = "Rename"
        '
        'btnPackedStatus
        '
        Me.btnPackedStatus.BeginGroup = True
        Me.btnPackedStatus.Checked = True
        Me.btnPackedStatus.Name = "btnPackedStatus"
        Me.btnPackedStatus.Text = "Enabled"
        '
        'btnMovePackedReport
        '
        Me.btnMovePackedReport.Name = "btnMovePackedReport"
        Me.btnMovePackedReport.Text = "Move"
        '
        'btnPackedRefresh
        '
        Me.btnPackedRefresh.BeginGroup = True
        Me.btnPackedRefresh.Image = CType(resources.GetObject("btnPackedRefresh.Image"), System.Drawing.Image)
        Me.btnPackedRefresh.Name = "btnPackedRefresh"
        Me.btnPackedRefresh.Text = "Refresh Report"
        Me.btnPackedRefresh.Visible = False
        '
        'btnPackedPreview
        '
        Me.btnPackedPreview.Image = CType(resources.GetObject("btnPackedPreview.Image"), System.Drawing.Image)
        Me.btnPackedPreview.Name = "btnPackedPreview"
        Me.btnPackedPreview.Text = "Preview"
        '
        'btnPackedExecute
        '
        Me.btnPackedExecute.Image = CType(resources.GetObject("btnPackedExecute.Image"), System.Drawing.Image)
        Me.btnPackedExecute.Name = "btnPackedExecute"
        Me.btnPackedExecute.Text = "Execute"
        '
        'btnPackedDelete
        '
        Me.btnPackedDelete.Image = CType(resources.GetObject("btnPackedDelete.Image"), System.Drawing.Image)
        Me.btnPackedDelete.Name = "btnPackedDelete"
        Me.btnPackedDelete.Text = "Delete"
        '
        'btnPackedAdhoc
        '
        Me.btnPackedAdhoc.BeginGroup = True
        Me.btnPackedAdhoc.Image = CType(resources.GetObject("btnPackedAdhoc.Image"), System.Drawing.Image)
        Me.btnPackedAdhoc.Name = "btnPackedAdhoc"
        Me.btnPackedAdhoc.Text = "Ad-Hoc View"
        '
        'btnPackedDesign
        '
        Me.btnPackedDesign.Image = CType(resources.GetObject("btnPackedDesign.Image"), System.Drawing.Image)
        Me.btnPackedDesign.Name = "btnPackedDesign"
        Me.btnPackedDesign.Text = "Report Design"
        Me.btnPackedDesign.Visible = False
        '
        'crumb
        '
        Me.crumb.AutoSize = True
        '
        '
        '
        Me.crumb.BackgroundStyle.BackColor = System.Drawing.Color.FromArgb(CType(CType(248, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.crumb.BackgroundStyle.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.crumb.BackgroundStyle.BorderBottomWidth = 1
        Me.crumb.BackgroundStyle.BorderColor = System.Drawing.Color.FromArgb(CType(CType(83, Byte), Integer), CType(CType(89, Byte), Integer), CType(CType(94, Byte), Integer))
        Me.crumb.BackgroundStyle.BorderColor2 = System.Drawing.Color.FromArgb(CType(CType(169, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(191, Byte), Integer))
        Me.crumb.BackgroundStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.crumb.BackgroundStyle.BorderLeftWidth = 1
        Me.crumb.BackgroundStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.crumb.BackgroundStyle.BorderRightWidth = 1
        Me.crumb.BackgroundStyle.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.crumb.BackgroundStyle.BorderTopWidth = 1
        Me.crumb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.crumb.ContainerControlProcessDialogKey = True
        Me.crumb.Dock = System.Windows.Forms.DockStyle.Top
        Me.crumb.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.crumb.Location = New System.Drawing.Point(0, 0)
        Me.crumb.Name = "crumb"
        Me.crumb.PathSeparator = ";"
        Me.crumb.Size = New System.Drawing.Size(1157, 22)
        Me.crumb.TabIndex = 18
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.tvMain)
        Me.Panel1.Controls.Add(Me.crumb)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(177, 155)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1157, 651)
        Me.Panel1.TabIndex = 19
        '
        'tmSchedulerInfo
        '
        Me.tmSchedulerInfo.Enabled = True
        Me.tmSchedulerInfo.Interval = 5000
        '
        'tmCheckUpdates
        '
        Me.tmCheckUpdates.Interval = 5000
        '
        'frmMainWin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1606, 873)
        Me.Controls.Add(Me.SuperTabControl1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.ContextMenuBar1)
        Me.Controls.Add(Me.DockSite2)
        Me.Controls.Add(Me.DockSite1)
        Me.Controls.Add(Me.ExpandableSplitter1)
        Me.Controls.Add(Me.tvExplorer)
        Me.Controls.Add(Me.RibbonControl1)
        Me.Controls.Add(Me.DockSite3)
        Me.Controls.Add(Me.DockSite4)
        Me.Controls.Add(Me.DockSite5)
        Me.Controls.Add(Me.DockSite6)
        Me.Controls.Add(Me.DockSite7)
        Me.Controls.Add(Me.DockSite8)
        Me.DoubleBuffered = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMainWin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD SEVEN"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.RibbonControl1.ResumeLayout(False)
        Me.RibbonControl1.PerformLayout()
        Me.RibbonPanel4.ResumeLayout(False)
        Me.RibbonPanel1.ResumeLayout(False)
        Me.RibbonPanel2.ResumeLayout(False)
        Me.RibbonPanel3.ResumeLayout(False)
        CType(Me.SuperTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControl1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        CType(Me.tvExplorer, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tvMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockSite4.ResumeLayout(False)
        CType(Me.Bar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Bar1.ResumeLayout(False)
        Me.PanelDockContainer2.ResumeLayout(False)
        Me.DockSite2.ResumeLayout(False)
        CType(Me.dnbRight, System.ComponentModel.ISupportInitialize).EndInit()
        Me.dnbRight.ResumeLayout(False)
        Me.PanelDockContainer3.ResumeLayout(False)
        CType(Me.advProps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelDockContainer1.ResumeLayout(False)
        CType(Me.ContextMenuBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents StyleManager1 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents RibbonControl1 As DevComponents.DotNetBar.RibbonControl
    Friend WithEvents RibbonPanel1 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents stabSchedules As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnSingleSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDynamicSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDynamicPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAutomationSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEventBased As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEventBasedPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnBurstingPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDataDriven As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDataDrivenPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel2 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents Office2007StartButton1 As DevComponents.DotNetBar.Office2007StartButton
    Friend WithEvents ItemContainer1 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer2 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ItemContainer3 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem3 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem5 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem7 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents GalleryContainer1 As DevComponents.DotNetBar.GalleryContainer
    Friend WithEvents ItemContainer4 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents ButtonItem12 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem13 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents rtabCreate As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents rtabSystem As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents btnNewFolder As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents QatCustomizeItem1 As DevComponents.DotNetBar.QatCustomizeItem
    Friend WithEvents StyleManager2 As DevComponents.DotNetBar.StyleManager
    Friend WithEvents stabSystem As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnDataItems As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUserConstants As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCustomCals As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabBackRestore As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnBackup As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnRestore As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabRemoteAdmin As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnRemoteConnect As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnRemoteDisconnect As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabManage As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnUserManager As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSMTPServers As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOperationHours As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnExportSchedules As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOptions As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSystemMonitor As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAddressBook As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonPanel3 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents stabFeatures As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnUpdateFeatures As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnValidateFeatures As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabActivation As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnActivate As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDeactivate As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabSupport As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnSupportFiles As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnLogCall As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabHelp As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnHelp As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDemos As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnforums As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnKbase As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents rtabResources As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents btnAbout As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents tvExplorer As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node5 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvMain As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents RibbonPanel4 As DevComponents.DotNetBar.RibbonPanel
    Friend WithEvents stabDatabase As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnExportData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleScheduleData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackageScheduleData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEventBasedData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEventPackageData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnLoginInfo As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabMigrate As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnMigrateToLocal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMigratetoODBC As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabSwitch As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnSwitchToLocal As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSwitchToODBC As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents rtabConfiguration As DevComponents.DotNetBar.RibbonTabItem
    Friend WithEvents ButtonItem2 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabView As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnDetails As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnTheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ComboItem1 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem2 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem3 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem4 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem5 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem6 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem7 As DevComponents.Editors.ComboItem
    Friend WithEvents ComboItem8 As DevComponents.Editors.ComboItem
    Friend WithEvents Cell1 As DevComponents.AdvTree.Cell
    Friend WithEvents Cell2 As DevComponents.AdvTree.Cell
    Friend WithEvents Cell3 As DevComponents.AdvTree.Cell
    Friend WithEvents SuperTabControl1 As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem4 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents btnOpenReport As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem8 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOptions1 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem10 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnViewStyle As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ElementStyle3 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents btnDetailStyle As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnBlackTheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnBluetheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSilvertheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnVistatheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnWin7theme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents stabSearch As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents txtSearch As DevComponents.DotNetBar.TextBoxItem
    Friend WithEvents ElementStyle4 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ItemContainer5 As DevComponents.DotNetBar.ItemContainer
    Friend WithEvents btnLogOut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents slSize As DevComponents.DotNetBar.SliderItem
    Friend WithEvents DotNetBarManager1 As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents DockSite4 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite1 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite2 As DevComponents.DotNetBar.DockSite
    Friend WithEvents dnbRight As DevComponents.DotNetBar.Bar
    Friend WithEvents PanelDockContainer1 As DevComponents.DotNetBar.PanelDockContainer
    Friend WithEvents DockContainerItem1 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents DockSite3 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite5 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite6 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite7 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite8 As DevComponents.DotNetBar.DockSite
    Friend WithEvents ContextMenuBar1 As DevComponents.DotNetBar.ContextMenuBar
    Friend WithEvents btnSingleContext As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleProperties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackageContext As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuPackageProperties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAutomationSchedules As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuAutoProp As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventBasedSchedules As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuEventProp As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnEventBasedPacks As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnueventPackProps As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ucHistory As sqlrd.ucScheduleHistory
    Friend WithEvents btnCollaboration As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuFolders As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderOpenInNewWindow As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnNewSmartFolder As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOutlook As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents crumb As DevComponents.DotNetBar.CrumbBar
    Friend WithEvents btnSingleCopy As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSinglePaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleEnabled As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSinglePreview As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleTools As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSingleTest As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnConvertToPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAdHocEmail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCreateSingleShortcut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnAdHocView As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackAddReport As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackCopy As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackPaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackEnabled As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackRefreshPack As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackTools As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackTest As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackSplit As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackAdhoc As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnpackShortcut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoCopy As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoPaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoEnabled As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnautoShortcut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventCopy As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventPaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventEnabled As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventShortcut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackAdd As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackAddNew As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackAddExisting As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackCopy As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackPaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackEnabled As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackOpen As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackTools As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackSplit As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btneventpackShortcut As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderNew As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderMove As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnFolderRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem11 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderDisable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderEnable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnfolderExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents DockContainerItem2 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents pg As DevComponents.DotNetBar.Controls.CircularProgress
    Friend WithEvents btnAutomationScheduleData As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents PanelDockContainer3 As DevComponents.DotNetBar.PanelDockContainer
    Friend WithEvents UcSchedulerControler1 As sqlrd.ucSchedulerControler
    Friend WithEvents DockContainerItem3 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents advProps As DevComponents.DotNetBar.AdvPropertyGrid
    Friend WithEvents tmSchedulerInfo As System.Windows.Forms.Timer
    Friend WithEvents btnPasteItem As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuMultipleItems As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltProperties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltMove As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltConvertToPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltMoveToPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltStatus As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltEnable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnmltDisable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuSmartFolders As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartViewReport As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartProperties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartAllContents As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartDisable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartEnable As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSmartExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnTileStyle As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSystemMonitorLarge As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudesktopMenu As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnuDesktopPaste As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ButtonItem4 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewFolder As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewSmartFolder As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewSingleSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewDynamicSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewDynamicPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewEventSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewEventPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewDataDriven As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewDataDrivenPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewBurstingSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents mnudtNewAutomationSchedule As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnFutureTheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnCollaborationServer As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents cmbCollaborationServer As DevComponents.DotNetBar.ComboBoxItem
    Friend WithEvents btnSmallHelp As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents Bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents PanelDockContainer2 As DevComponents.DotNetBar.PanelDockContainer
    Friend WithEvents DockContainerItem4 As DevComponents.DotNetBar.DockContainerItem
    Friend WithEvents pgx As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents lblProgress As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnAboutToo As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnQuickEmail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnReportDesign As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnOpenPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUpdateCheck As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents tmCheckUpdates As System.Windows.Forms.Timer
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnPackedReportMenu As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackagedProperties As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedRename As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedStatus As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedRefresh As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedPreview As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedExecute As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedDelete As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedAdhoc As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPackedDesign As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents UcScheduleCount1 As sqlrd.ucScheduleCount
    Friend WithEvents btnDashboard As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDashboardA As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents btnDateAndTime As DevComponents.DotNetBar.LabelItem
    Friend WithEvents btnProfServices As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnUnifyFolderStyles As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnConsole As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMetroTheme As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents swFullSystemSearch As DevComponents.DotNetBar.SwitchButtonItem
    Friend WithEvents btnMoveSingle As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMovePackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMoveAuto As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMoveEvent As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMoveEventPackage As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnMovePackedReport As DevComponents.DotNetBar.ButtonItem

End Class
