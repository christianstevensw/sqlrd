Imports System.Threading
Imports Microsoft.Win32
Imports DevComponents.DotNetBar
Imports SaveListView
Imports System.Drawing
Imports System.Collections
Imports System.Configuration
Imports System.ComponentModel
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports C1.Win.C1Schedule
Imports C1.C1Schedule
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Or crystal_ver = 11.5 Or CRYSTAL_VER >= 11.6 Then
Imports My.Crystal11
#End If

#If CRYSTAL_VER >= 11.6 Then
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
#End If
Public Class frmWindow
    Inherits DevComponents.DotNetBar.Office2007Form

    Dim oUI As clsMarsUI = New clsMarsUI
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Private m_SortingColumn As ColumnHeader
    Dim WithEvents oTextBoxItem As TextBoxItem
    Public gItemList As ListViewItem()
    Dim sortBy As Integer = 0
    Dim sortOrder As SortOrder = SortOrder.Ascending
    Dim columns As Hashtable = Nothing
    Dim processOutlook As Boolean = False
    Dim oCharter As frmScheduleView = New frmScheduleView
    Dim ep As ErrorProvider = New ErrorProvider
    Dim m_selctedFolderItem As String = ""
#Region "Friends?"
    Friend WithEvents tbNav As DevComponents.DotNetBar.TabControl
    Friend WithEvents exSplitter As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents btnSingle As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamic As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamicPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAutomation As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEvent As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnBursting As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton2 As DevComponents.DotNetBar.BubbleButton
#If CRYSTAL_VER < 11.6 Then
    Public oRpt As Object
#Else
    Public oRpt As object
#End If

    Friend IsLoaded As Boolean = False
    Friend IsFormLoaded As Boolean = False
    Dim oMulti As Boolean = False
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tbExpose As DevComponents.DotNetBar.TabItem
    Friend WithEvents c1sched As C1.Win.C1Schedule.C1Schedule
    Friend WithEvents lsvItems As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents optMonth As System.Windows.Forms.RadioButton
    Friend WithEvents optWeek As System.Windows.Forms.RadioButton
    Friend WithEvents optDay As System.Windows.Forms.RadioButton
    Friend WithEvents btnRefresh As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lblInterval As DevComponents.DotNetBar.LabelX
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents gantt As C1.Win.C1Chart.C1Chart
    Friend WithEvents optGantt As System.Windows.Forms.RadioButton
    Friend WithEvents grpGantt As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents optMaximum As System.Windows.Forms.RadioButton
    Friend WithEvents optMinimum As System.Windows.Forms.RadioButton
    Friend WithEvents optAverage As System.Windows.Forms.RadioButton
    Friend WithEvents optMedian As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents dtStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnRedraw As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tippy As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFilter As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents navOutlook As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupControlContainer1 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroupControlContainer2 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroupControlContainer3 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroup2 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroup3 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroup4 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupControlContainer4 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents oCal As C1.Win.C1Schedule.C1Calendar
    Dim oMultiRes As DialogResult
#End Region
    Private Delegate Sub DeleteDesktopItem_A(ByVal sender As System.Object, ByVal e As System.EventArgs, ByRef shouldReturn As Boolean)

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWindow))
        Dim ListViewItem22 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Single Schedules"}, "document_chart.png", System.Drawing.Color.Black, System.Drawing.Color.NavajoWhite, Nothing)
        Dim ListViewItem23 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Package Schedules"}, "box.png", System.Drawing.Color.Black, System.Drawing.Color.PaleGreen, Nothing)
        Dim ListViewItem24 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Dynamic Schedules"}, 7, System.Drawing.Color.Empty, System.Drawing.Color.MediumOrchid, Nothing)
        Dim ListViewItem25 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Dynamic Packages"}, 13, System.Drawing.Color.Empty, System.Drawing.Color.Goldenrod, Nothing)
        Dim ListViewItem26 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Automation Schedules"}, "document_gear.png", System.Drawing.Color.Black, System.Drawing.Color.LightSteelBlue, Nothing)
        Dim ListViewItem27 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Event-Based Packages"}, "cube_molecule.png", System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer)), System.Drawing.Color.Yellow, Nothing)
        Dim ListViewItem28 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Data-Driven Schedules"}, 17, System.Drawing.Color.Empty, System.Drawing.Color.LightSkyBlue, Nothing)
        Me.tbNav = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.navOutlook = New DevExpress.XtraNavBar.NavBarControl()
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarGroupControlContainer1 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.NavBarGroupControlContainer2 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.lsvItems = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.NavBarGroupControlContainer3 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.optGantt = New System.Windows.Forms.RadioButton()
        Me.optDay = New System.Windows.Forms.RadioButton()
        Me.optMonth = New System.Windows.Forms.RadioButton()
        Me.optWeek = New System.Windows.Forms.RadioButton()
        Me.NavBarGroupControlContainer4 = New DevExpress.XtraNavBar.NavBarGroupControlContainer()
        Me.lblInterval = New DevComponents.DotNetBar.LabelX()
        Me.btnRefresh = New DevComponents.DotNetBar.ButtonX()
        Me.TrackBar1 = New System.Windows.Forms.TrackBar()
        Me.NavBarGroup2 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarGroup3 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.NavBarGroup4 = New DevExpress.XtraNavBar.NavBarGroup()
        Me.tbExpose = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.exSplitter = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grpGantt = New System.Windows.Forms.Panel()
        Me.gantt = New C1.Win.C1Chart.C1Chart()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtFilter = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnRedraw = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.dtStart = New System.Windows.Forms.DateTimePicker()
        Me.dtEnd = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.optMaximum = New System.Windows.Forms.RadioButton()
        Me.optMinimum = New System.Windows.Forms.RadioButton()
        Me.optAverage = New System.Windows.Forms.RadioButton()
        Me.optMedian = New System.Windows.Forms.RadioButton()
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton()
        Me.BubbleButton2 = New DevComponents.DotNetBar.BubbleButton()
        Me.tippy = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSingle = New DevComponents.DotNetBar.BubbleButton()
        Me.btnPackage = New DevComponents.DotNetBar.BubbleButton()
        Me.btnDynamic = New DevComponents.DotNetBar.BubbleButton()
        Me.btnDynamicPackage = New DevComponents.DotNetBar.BubbleButton()
        Me.btnAutomation = New DevComponents.DotNetBar.BubbleButton()
        Me.btnEvent = New DevComponents.DotNetBar.BubbleButton()
        Me.btnBursting = New DevComponents.DotNetBar.BubbleButton()
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbNav.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.navOutlook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.navOutlook.SuspendLayout()
        Me.NavBarGroupControlContainer2.SuspendLayout()
        Me.NavBarGroupControlContainer3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.NavBarGroupControlContainer4.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.grpGantt.SuspendLayout()
        CType(Me.gantt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'tbNav
        '
        Me.tbNav.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.tbNav.CanReorderTabs = True
        Me.tbNav.Controls.Add(Me.TabControlPanel4)
        Me.tbNav.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbNav.Location = New System.Drawing.Point(0, 0)
        Me.tbNav.Name = "tbNav"
        Me.tbNav.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbNav.SelectedTabIndex = 0
        Me.tbNav.Size = New System.Drawing.Size(256, 716)
        Me.tbNav.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Dock
        Me.tbNav.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbNav.TabIndex = 0
        Me.tbNav.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbNav.Tabs.Add(Me.tbExpose)
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.Panel2)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(29, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(227, 716)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(232, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbExpose
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.navOutlook)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(1, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(225, 714)
        Me.Panel2.TabIndex = 0
        '
        'navOutlook
        '
        Me.navOutlook.ActiveGroup = Me.NavBarGroup1
        Me.navOutlook.Appearance.GroupBackground.BackColor = System.Drawing.Color.White
        Me.navOutlook.Appearance.GroupBackground.Options.UseBackColor = True
        Me.navOutlook.BackColor = System.Drawing.Color.White
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer2)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer3)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer1)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer4)
        Me.navOutlook.Dock = System.Windows.Forms.DockStyle.Fill
        Me.navOutlook.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1, Me.NavBarGroup2, Me.NavBarGroup3, Me.NavBarGroup4})
        Me.navOutlook.Location = New System.Drawing.Point(0, 0)
        Me.navOutlook.Name = "navOutlook"
        Me.navOutlook.Size = New System.Drawing.Size(225, 714)
        Me.navOutlook.TabIndex = 27
        Me.navOutlook.Text = "NavBarControl1"
        Me.navOutlook.View = New DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("The Asphalt World")
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = "Calendar"
        Me.NavBarGroup1.ControlContainer = Me.NavBarGroupControlContainer1
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupClientHeight = 160
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'NavBarGroupControlContainer1
        '
        Me.NavBarGroupControlContainer1.BackgroundImage = CType(resources.GetObject("NavBarGroupControlContainer1.BackgroundImage"), System.Drawing.Image)
        Me.NavBarGroupControlContainer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.NavBarGroupControlContainer1.Name = "NavBarGroupControlContainer1"
        Me.NavBarGroupControlContainer1.Size = New System.Drawing.Size(223, 156)
        Me.NavBarGroupControlContainer1.TabIndex = 0
        '
        'NavBarGroupControlContainer2
        '
        Me.NavBarGroupControlContainer2.Controls.Add(Me.lsvItems)
        Me.NavBarGroupControlContainer2.Name = "NavBarGroupControlContainer2"
        Me.NavBarGroupControlContainer2.Size = New System.Drawing.Size(223, 156)
        Me.NavBarGroupControlContainer2.TabIndex = 1
        '
        'lsvItems
        '
        Me.lsvItems.BackColor = System.Drawing.Color.White
        Me.lsvItems.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lsvItems.CheckBoxes = True
        Me.lsvItems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvItems.FullRowSelect = True
        Me.lsvItems.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem22.Checked = True
        ListViewItem22.StateImageIndex = 1
        ListViewItem22.Tag = "Report"
        ListViewItem23.Checked = True
        ListViewItem23.StateImageIndex = 1
        ListViewItem23.Tag = "Package"
        ListViewItem24.Checked = True
        ListViewItem24.StateImageIndex = 1
        ListViewItem24.Tag = "Dynamic"
        ListViewItem25.Checked = True
        ListViewItem25.StateImageIndex = 1
        ListViewItem25.Tag = "Dynamic Package"
        ListViewItem26.Checked = True
        ListViewItem26.StateImageIndex = 1
        ListViewItem26.Tag = "Automation"
        ListViewItem27.Checked = True
        ListViewItem27.StateImageIndex = 1
        ListViewItem27.Tag = "EventPackage"
        ListViewItem28.Checked = True
        ListViewItem28.StateImageIndex = 1
        ListViewItem28.Tag = "Data-Driven"
        Me.lsvItems.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem22, ListViewItem23, ListViewItem24, ListViewItem25, ListViewItem26, ListViewItem27, ListViewItem28})
        Me.lsvItems.Location = New System.Drawing.Point(6, 0)
        Me.lsvItems.Name = "lsvItems"
        Me.lsvItems.Size = New System.Drawing.Size(179, 154)
        Me.lsvItems.TabIndex = 1
        Me.lsvItems.UseCompatibleStateImageBehavior = False
        Me.lsvItems.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "View"
        Me.ColumnHeader1.Width = 160
        '
        'NavBarGroupControlContainer3
        '
        Me.NavBarGroupControlContainer3.Controls.Add(Me.TableLayoutPanel1)
        Me.NavBarGroupControlContainer3.Name = "NavBarGroupControlContainer3"
        Me.NavBarGroupControlContainer3.Size = New System.Drawing.Size(223, 116)
        Me.NavBarGroupControlContainer3.TabIndex = 2
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.optGantt, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.optDay, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optMonth, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.optWeek, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(223, 116)
        Me.TableLayoutPanel1.TabIndex = 27
        '
        'optGantt
        '
        Me.optGantt.AutoSize = True
        Me.optGantt.BackColor = System.Drawing.Color.Transparent
        Me.optGantt.Location = New System.Drawing.Point(3, 90)
        Me.optGantt.Name = "optGantt"
        Me.optGantt.Size = New System.Drawing.Size(80, 17)
        Me.optGantt.TabIndex = 3
        Me.optGantt.TabStop = True
        Me.optGantt.Text = "Gantt chart"
        Me.optGantt.UseVisualStyleBackColor = False
        '
        'optDay
        '
        Me.optDay.AutoSize = True
        Me.optDay.BackColor = System.Drawing.Color.Transparent
        Me.optDay.Checked = True
        Me.optDay.Location = New System.Drawing.Point(3, 3)
        Me.optDay.Name = "optDay"
        Me.optDay.Size = New System.Drawing.Size(69, 17)
        Me.optDay.TabIndex = 0
        Me.optDay.TabStop = True
        Me.optDay.Text = "Day View"
        Me.optDay.UseVisualStyleBackColor = False
        '
        'optMonth
        '
        Me.optMonth.AutoSize = True
        Me.optMonth.BackColor = System.Drawing.Color.Transparent
        Me.optMonth.Location = New System.Drawing.Point(3, 61)
        Me.optMonth.Name = "optMonth"
        Me.optMonth.Size = New System.Drawing.Size(80, 17)
        Me.optMonth.TabIndex = 2
        Me.optMonth.Text = "Month View"
        Me.optMonth.UseVisualStyleBackColor = False
        '
        'optWeek
        '
        Me.optWeek.AutoSize = True
        Me.optWeek.BackColor = System.Drawing.Color.Transparent
        Me.optWeek.Location = New System.Drawing.Point(3, 32)
        Me.optWeek.Name = "optWeek"
        Me.optWeek.Size = New System.Drawing.Size(77, 17)
        Me.optWeek.TabIndex = 1
        Me.optWeek.Text = "Week View"
        Me.optWeek.UseVisualStyleBackColor = False
        '
        'NavBarGroupControlContainer4
        '
        Me.NavBarGroupControlContainer4.Controls.Add(Me.lblInterval)
        Me.NavBarGroupControlContainer4.Controls.Add(Me.btnRefresh)
        Me.NavBarGroupControlContainer4.Controls.Add(Me.TrackBar1)
        Me.NavBarGroupControlContainer4.Name = "NavBarGroupControlContainer4"
        Me.NavBarGroupControlContainer4.Size = New System.Drawing.Size(223, 106)
        Me.NavBarGroupControlContainer4.TabIndex = 3
        '
        'lblInterval
        '
        Me.lblInterval.AutoSize = True
        Me.lblInterval.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.lblInterval.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblInterval.Location = New System.Drawing.Point(3, 51)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(72, 16)
        Me.lblInterval.TabIndex = 2
        Me.lblInterval.Text = "Thirty Minutes"
        '
        'btnRefresh
        '
        Me.btnRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRefresh.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRefresh.Location = New System.Drawing.Point(63, 74)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(99, 23)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Text = "Refresh"
        '
        'TrackBar1
        '
        Me.TrackBar1.BackColor = System.Drawing.Color.White
        Me.TrackBar1.LargeChange = 1
        Me.TrackBar1.Location = New System.Drawing.Point(6, 3)
        Me.TrackBar1.Maximum = 6
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(217, 45)
        Me.TrackBar1.TabIndex = 1
        '
        'NavBarGroup2
        '
        Me.NavBarGroup2.Caption = "Filter"
        Me.NavBarGroup2.ControlContainer = Me.NavBarGroupControlContainer2
        Me.NavBarGroup2.Expanded = True
        Me.NavBarGroup2.GroupClientHeight = 160
        Me.NavBarGroup2.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup2.Name = "NavBarGroup2"
        '
        'NavBarGroup3
        '
        Me.NavBarGroup3.Caption = "View Style"
        Me.NavBarGroup3.ControlContainer = Me.NavBarGroupControlContainer3
        Me.NavBarGroup3.Expanded = True
        Me.NavBarGroup3.GroupClientHeight = 120
        Me.NavBarGroup3.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup3.Name = "NavBarGroup3"
        '
        'NavBarGroup4
        '
        Me.NavBarGroup4.Caption = "Time Interval"
        Me.NavBarGroup4.ControlContainer = Me.NavBarGroupControlContainer4
        Me.NavBarGroup4.Expanded = True
        Me.NavBarGroup4.GroupClientHeight = 110
        Me.NavBarGroup4.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup4.Name = "NavBarGroup4"
        '
        'tbExpose
        '
        Me.tbExpose.AttachedControl = Me.TabControlPanel4
        Me.tbExpose.Image = CType(resources.GetObject("tbExpose.Image"), System.Drawing.Image)
        Me.tbExpose.Name = "tbExpose"
        Me.tbExpose.Text = "SQL-RD Outlook"
        '
        'exSplitter
        '
        Me.exSplitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.exSplitter.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.exSplitter.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.ExpandableControl = Me.tbNav
        Me.exSplitter.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.exSplitter.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.ExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.GripDarkColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.exSplitter.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.exSplitter.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.exSplitter.HotBackColor2 = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.exSplitter.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.exSplitter.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.exSplitter.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.HotExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.exSplitter.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.exSplitter.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.exSplitter.Location = New System.Drawing.Point(256, 0)
        Me.exSplitter.Name = "exSplitter"
        Me.exSplitter.Size = New System.Drawing.Size(5, 716)
        Me.exSplitter.TabIndex = 22
        Me.exSplitter.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.grpGantt)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(261, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(876, 716)
        Me.Panel1.TabIndex = 25
        '
        'grpGantt
        '
        Me.grpGantt.Controls.Add(Me.gantt)
        Me.grpGantt.Controls.Add(Me.Panel4)
        Me.grpGantt.Location = New System.Drawing.Point(41, 28)
        Me.grpGantt.Name = "grpGantt"
        Me.grpGantt.Size = New System.Drawing.Size(787, 487)
        Me.grpGantt.TabIndex = 26
        Me.grpGantt.Visible = False
        '
        'gantt
        '
        Me.gantt.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.gantt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gantt.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gantt.Location = New System.Drawing.Point(0, 0)
        Me.gantt.Name = "gantt"
        Me.gantt.PropBag = resources.GetString("gantt.PropBag")
        Me.gantt.Size = New System.Drawing.Size(787, 402)
        Me.gantt.TabIndex = 25
        Me.tippy.SetToolTip(Me.gantt, "Double-click me to save the chart to image file")
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.Panel4.Controls.Add(Me.GroupBox5)
        Me.Panel4.Controls.Add(Me.GroupBox3)
        Me.Panel4.Controls.Add(Me.GroupBox4)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 402)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(787, 85)
        Me.Panel4.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label3)
        Me.GroupBox5.Controls.Add(Me.txtFilter)
        Me.GroupBox5.Location = New System.Drawing.Point(532, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(210, 71)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Filter Schedules"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(7, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Name contains"
        '
        'txtFilter
        '
        '
        '
        '
        Me.txtFilter.Border.Class = "TextBoxBorder"
        Me.txtFilter.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFilter.Location = New System.Drawing.Point(6, 38)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(198, 21)
        Me.txtFilter.TabIndex = 0
        Me.tippy.SetToolTip(Me.txtFilter, "Use "";"" for multiple filters and press ""Enter"" to confirm")
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnRedraw)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.dtStart)
        Me.GroupBox3.Controls.Add(Me.dtEnd)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(282, 71)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Please select the time range to use"
        '
        'btnRedraw
        '
        Me.btnRedraw.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRedraw.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRedraw.Location = New System.Drawing.Point(190, 38)
        Me.btnRedraw.Name = "btnRedraw"
        Me.btnRedraw.Size = New System.Drawing.Size(75, 23)
        Me.btnRedraw.TabIndex = 4
        Me.btnRedraw.Text = "&Zoom"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(17, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start Time"
        '
        'dtStart
        '
        Me.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtStart.Location = New System.Drawing.Point(20, 40)
        Me.dtStart.Name = "dtStart"
        Me.dtStart.ShowUpDown = True
        Me.dtStart.Size = New System.Drawing.Size(71, 21)
        Me.dtStart.TabIndex = 0
        Me.dtStart.Value = New Date(2007, 5, 21, 0, 0, 0, 0)
        '
        'dtEnd
        '
        Me.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtEnd.Location = New System.Drawing.Point(100, 40)
        Me.dtEnd.Name = "dtEnd"
        Me.dtEnd.ShowUpDown = True
        Me.dtEnd.Size = New System.Drawing.Size(71, 21)
        Me.dtEnd.TabIndex = 1
        Me.dtEnd.Value = New Date(2007, 5, 21, 23, 59, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(97, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "End Time"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.optMaximum)
        Me.GroupBox4.Controls.Add(Me.optMinimum)
        Me.GroupBox4.Controls.Add(Me.optAverage)
        Me.GroupBox4.Controls.Add(Me.optMedian)
        Me.GroupBox4.Location = New System.Drawing.Point(291, 8)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(235, 71)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Value Type"
        '
        'optMaximum
        '
        Me.optMaximum.AutoSize = True
        Me.optMaximum.Location = New System.Drawing.Point(129, 42)
        Me.optMaximum.Name = "optMaximum"
        Me.optMaximum.Size = New System.Drawing.Size(103, 17)
        Me.optMaximum.TabIndex = 3
        Me.optMaximum.Text = "Maximum values"
        Me.optMaximum.UseVisualStyleBackColor = True
        '
        'optMinimum
        '
        Me.optMinimum.AutoSize = True
        Me.optMinimum.Location = New System.Drawing.Point(129, 21)
        Me.optMinimum.Name = "optMinimum"
        Me.optMinimum.Size = New System.Drawing.Size(99, 17)
        Me.optMinimum.TabIndex = 2
        Me.optMinimum.Text = "Minimum values"
        Me.optMinimum.UseVisualStyleBackColor = True
        '
        'optAverage
        '
        Me.optAverage.AutoSize = True
        Me.optAverage.Location = New System.Drawing.Point(7, 42)
        Me.optAverage.Name = "optAverage"
        Me.optAverage.Size = New System.Drawing.Size(100, 17)
        Me.optAverage.TabIndex = 1
        Me.optAverage.Text = "Average values"
        Me.optAverage.UseVisualStyleBackColor = True
        '
        'optMedian
        '
        Me.optMedian.AutoSize = True
        Me.optMedian.Checked = True
        Me.optMedian.Location = New System.Drawing.Point(7, 21)
        Me.optMedian.Name = "optMedian"
        Me.optMedian.Size = New System.Drawing.Size(93, 17)
        Me.optMedian.TabIndex = 0
        Me.optMedian.TabStop = True
        Me.optMedian.Text = "Median values"
        Me.optMedian.UseVisualStyleBackColor = True
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Name = "BubbleButton1"
        '
        'BubbleButton2
        '
        Me.BubbleButton2.Name = "BubbleButton2"
        '
        'btnSingle
        '
        Me.btnSingle.Image = CType(resources.GetObject("btnSingle.Image"), System.Drawing.Image)
        Me.btnSingle.ImageLarge = CType(resources.GetObject("btnSingle.ImageLarge"), System.Drawing.Image)
        Me.btnSingle.Name = "btnSingle"
        Me.btnSingle.TooltipText = "Single Schedule"
        '
        'btnPackage
        '
        Me.btnPackage.Image = CType(resources.GetObject("btnPackage.Image"), System.Drawing.Image)
        Me.btnPackage.ImageLarge = CType(resources.GetObject("btnPackage.ImageLarge"), System.Drawing.Image)
        Me.btnPackage.Name = "btnPackage"
        Me.btnPackage.TooltipText = "Packaged Reports Schedule"
        '
        'btnDynamic
        '
        Me.btnDynamic.Image = CType(resources.GetObject("btnDynamic.Image"), System.Drawing.Image)
        Me.btnDynamic.ImageLarge = CType(resources.GetObject("btnDynamic.ImageLarge"), System.Drawing.Image)
        Me.btnDynamic.Name = "btnDynamic"
        Me.btnDynamic.TooltipText = "Dynamic Schedule"
        '
        'btnDynamicPackage
        '
        Me.btnDynamicPackage.Image = CType(resources.GetObject("btnDynamicPackage.Image"), System.Drawing.Image)
        Me.btnDynamicPackage.ImageLarge = CType(resources.GetObject("btnDynamicPackage.ImageLarge"), System.Drawing.Image)
        Me.btnDynamicPackage.Name = "btnDynamicPackage"
        Me.btnDynamicPackage.TooltipText = "Dynamic Package Schedule"
        '
        'btnAutomation
        '
        Me.btnAutomation.Image = CType(resources.GetObject("btnAutomation.Image"), System.Drawing.Image)
        Me.btnAutomation.ImageLarge = CType(resources.GetObject("btnAutomation.ImageLarge"), System.Drawing.Image)
        Me.btnAutomation.Name = "btnAutomation"
        Me.btnAutomation.TooltipText = "Automation Schedule"
        '
        'btnEvent
        '
        Me.btnEvent.Image = CType(resources.GetObject("btnEvent.Image"), System.Drawing.Image)
        Me.btnEvent.ImageLarge = CType(resources.GetObject("btnEvent.ImageLarge"), System.Drawing.Image)
        Me.btnEvent.Name = "btnEvent"
        Me.btnEvent.TooltipText = "Event-Based Schedule"
        '
        'btnBursting
        '
        Me.btnBursting.Image = CType(resources.GetObject("btnBursting.Image"), System.Drawing.Image)
        Me.btnBursting.ImageLarge = CType(resources.GetObject("btnBursting.ImageLarge"), System.Drawing.Image)
        Me.btnBursting.Name = "btnBursting"
        Me.btnBursting.TooltipText = "Bursting Schedule"
        '
        'frmWindow
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(1137, 716)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.exSplitter)
        Me.Controls.Add(Me.tbNav)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmWindow"
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbNav.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.navOutlook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.navOutlook.ResumeLayout(False)
        Me.NavBarGroupControlContainer2.ResumeLayout(False)
        Me.NavBarGroupControlContainer3.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.NavBarGroupControlContainer4.ResumeLayout(False)
        Me.NavBarGroupControlContainer4.PerformLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.grpGantt.ResumeLayout(False)
        CType(Me.gantt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Enum enCalcType As Integer
        MEDIAN = 0
        AVERAGE = 1
        MINIMUM = 2
        MAXIMUM = 3
    End Enum

    Private Enum scheduleType
        SINGLES = 0
        PACKAGE = 1
        SINGLEDYNAMIC = 2
        PACKAGEDYNAMIC = 3
        AUTOMATION = 4
        EVENTBASED = 5
        BURSTING = 6
        EVENTPACKAGE = 7
    End Enum

    Private ReadOnly Property m_excludeTypes() As String()
        Get
            Try
                Dim results As String()
                Dim I As Integer = 0

                For Each item As ListViewItem In Me.lsvItems.Items
                    If item.Checked = False Then
                        ReDim Preserve results(I)

                        results(I) = item.Tag

                        I += 1
                    End If
                Next

                Return results
            Catch
                Return Nothing
            End Try
        End Get
    End Property

    Private ReadOnly Property m_CalcType() As enCalcType
        Get
            If Me.optAverage.Checked Then
                Return enCalcType.AVERAGE
            ElseIf Me.optMedian.Checked Then
                Return enCalcType.MEDIAN
            ElseIf Me.optMinimum.Checked Then
                Return enCalcType.MINIMUM
            ElseIf Me.optMaximum.Checked Then
                Return enCalcType.MAXIMUM
            End If
        End Get

    End Property

    Private Sub optDay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDay.CheckedChanged, optWeek.CheckedChanged, optMonth.CheckedChanged, optGantt.CheckedChanged
        If IsLoaded = False Then Return

        If optGantt.Checked = True Then
            c1sched.Visible = False
            grpGantt.Visible = True
            grpGantt.Dock = DockStyle.Fill
        Else
            grpGantt.Visible = False

            c1sched.Visible = True
            c1sched.Dock = DockStyle.Fill
        End If

        If optDay.Checked Then
            c1sched.ViewType = ScheduleViewEnum.DayView
        ElseIf optWeek.Checked Then
            c1sched.ViewType = ScheduleViewEnum.WeekView
        ElseIf optMonth.Checked = True Then
            c1sched.ViewType = ScheduleViewEnum.MonthView
        ElseIf optGantt.Checked = True Then
            Dim tit As C1.Win.C1Chart.Title = gantt.Header
            Dim selDates() As Date = oCal.SelectedDates

            tit.Text = selDates(0)

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes)
        End If

        TrackBar1.Enabled = optDay.Checked
        lblInterval.Enabled = TrackBar1.Enabled
    End Sub

    Private Sub c1sched_AppointmentChanged(ByVal sender As Object, ByVal e As C1.C1Schedule.AppointmentEventArgs) Handles c1sched.AppointmentChanged
        If Me.processOutlook = True Then
            Dim apt As C1.C1Schedule.Appointment = e.Appointment
            Dim lbl As C1.C1Schedule.Label = apt.Label
            Dim nID As Integer = apt.Tag
            Dim type As String = lbl.Text
            Dim newStart As Date = CTimeZ(apt.Start, dateConvertType.WRITE)
            Dim scheduleID As Integer

            Select Case type
                Case "Report"
                    scheduleID = clsMarsScheduler.GetScheduleID(nID)
                Case "Package"
                    scheduleID = clsMarsScheduler.GetScheduleID(, nID)
                Case "Automation"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , nID)
                Case "EventPackage"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)
                Case Else
                    Return
            End Select

            Dim SQL As String = "UPDATE ScheduleAttr SET " & _
            "StartTime = '" & ConTime(newStart) & "'," & _
            "NextRun = '" & ConDate(newStart) & " " & ConTime(newStart) & "' " & _
            "WHERE ScheduleID = " & scheduleID

            If clsMarsData.WriteData(SQL) = True Then
                MessageBox.Show("The schedule has been modified successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub


    Private Sub c1sched_BeforeAppointmentShow(ByVal sender As Object, ByVal e As C1.C1Schedule.CancelAppointmentEventArgs) Handles c1sched.BeforeAppointmentShow
        e.Cancel = True

        Dim apt As C1.C1Schedule.Appointment = e.Appointment

        Dim nID As Integer = apt.Tag
        Dim lbl As C1.C1Schedule.Label = apt.Label
        Dim type As String = lbl.Text

        Select Case type
            Case "Report"
                Dim oProp As frmSingleProp = New frmSingleProp

                oProp.EditSchedule(nID)
            Case "Package"
                Dim oProp As frmPackageProp = New frmPackageProp

                oProp.EditPackage(nID)
            Case "Automation"
                Dim oProp As frmAutoProp = New frmAutoProp

                oProp.EditSchedule(nID)
            Case "EventPackage"
                Dim oProp As frmEventPackageProp = New frmEventPackageProp

                oProp.EditPackage(nID)
        End Select


    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If Me.optGantt.Checked = False Then
            Me.processOutlook = False
            clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType, Me.m_excludeTypes)
            Me.processOutlook = True
        Else
            Dim selDates() As Date = oCal.SelectedDates

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub



    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        Select Case TrackBar1.Value
            Case 0
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FiveMinutes
                Me.lblInterval.Text = "Five Minutes"
            Case 1
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.SixMinutes
                Me.lblInterval.Text = "Six Minutes"
            Case 2
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TenMinutes
                Me.lblInterval.Text = "Ten Minutes"
            Case 3
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FifteenMinutes
                Me.lblInterval.Text = "Fifteen Minutes"
            Case 4
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TwentyMinutes
                Me.lblInterval.Text = "Twenty Minutes"
            Case 5
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.ThirtyMinutes
                Me.lblInterval.Text = "Thirty Minutes"
            Case 6
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.OneHour
                Me.lblInterval.Text = "One Hour"
        End Select



    End Sub

    Private Sub lsvItems_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvItems.ItemChecked
        If optGantt.Checked = False Then
            If processOutlook = True Then
                Me.processOutlook = False
                clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType, Me.m_excludeTypes)
                Me.processOutlook = True
            End If
        Else
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.drawChart(gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes)
        End If

    End Sub

    Private Sub iCal_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
        Dim selDates() As Date = oCal.SelectedDates

        If optGantt.Checked = False Then
            Try

                c1sched.GoToDate(selDates(0))
            Catch : End Try
        Else
            Dim tit As C1.Win.C1Chart.Title = gantt.Header

            tit.Text = selDates(0)

            oCharter.drawChart(gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If

    End Sub


    Private Sub btnRedraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRedraw.Click
        If dtStart.Value > dtEnd.Value Then
            SetError(dtEnd, "Please select a valid time range")
        Else
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.setChartRange(Me.gantt, selDates(0), Me.dtStart.Value, Me.dtEnd.Value, m_CalcType, Me.m_excludeTypes, _
            txtFilter.Text)
        End If
    End Sub

    Private Sub optMedian_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMedian.CheckedChanged, optAverage.CheckedChanged, optMinimum.CheckedChanged, optMaximum.CheckedChanged
        If Me.processOutlook = True Then
            Dim selDates() As Date = oCal.SelectedDates

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub

    Private Sub dtEnd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtEnd.KeyDown
        If e.KeyCode = Keys.Return Or e.KeyCode = Keys.Enter Then
            Me.btnRedraw_Click(sender, e)
        End If
    End Sub

    Private Sub dtStart_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtStart.ValueChanged, dtEnd.ValueChanged
        SetError(dtEnd, "")
    End Sub

    Private Sub gantt_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gantt.DoubleClick
        Dim img As System.Drawing.Image = gantt.GetImage()
        Dim sfd As New SaveFileDialog

        With sfd
            .CheckPathExists = True
            .OverwritePrompt = True
            .Title = "Save Gantt Chart image as..."
            .Filter = "Portable Network Graphics Image|*.png"
            .DefaultExt = "png"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                img.Save(.FileName)
            End If
        End With

    End Sub

    Private Sub txtFilter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFilter.KeyDown
        If (e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Return) Then
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub

    <DebuggerStepThrough()> _
    Public Sub IsBusy(ByVal busy As Boolean)
        If busy = True Then
            Me.Cursor = Cursors.WaitCursor
            Me.MdiParent.Cursor = Cursors.WaitCursor
        Else
            Me.Cursor = Cursors.Default
            Me.MdiParent.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub initOutlookView()
10:     Try
            'sort out the calendar
20:         Me.oCal = New C1.Win.C1Schedule.C1Calendar
30:         Me.NavBarGroupControlContainer1.Controls.Add(Me.oCal)
40:         Me.oCal.BoldedDates = New Date(-1) {}
50:         Me.oCal.BorderStyle = System.Windows.Forms.BorderStyle.None
60:         Me.oCal.Location = New System.Drawing.Point(7, 3)
70:         Me.oCal.Name = "oCal"

80:         Me.oCal.ShowWeekNumbers = False
90:         Me.oCal.Size = New System.Drawing.Size(214, 140)
100:        Me.oCal.TabIndex = 0
110:        Me.oCal.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Blue

120:        oUI.BusyProgress(10, "Loading Outlook view...")
130:        Me.c1sched = New C1.Win.C1Schedule.C1Schedule

140:        CType(Me.c1sched, System.ComponentModel.ISupportInitialize).BeginInit()
150:        CType(Me.c1sched.DataStorage.AppointmentStorage, System.ComponentModel.ISupportInitialize).BeginInit()
160:        CType(Me.c1sched.DataStorage.CategoryStorage, System.ComponentModel.ISupportInitialize).BeginInit()
170:        CType(Me.c1sched.DataStorage.ContactStorage, System.ComponentModel.ISupportInitialize).BeginInit()
180:        CType(Me.c1sched.DataStorage.LabelStorage, System.ComponentModel.ISupportInitialize).BeginInit()
190:        CType(Me.c1sched.DataStorage.ResourceStorage, System.ComponentModel.ISupportInitialize).BeginInit()
200:        CType(Me.c1sched.DataStorage.StatusStorage, System.ComponentModel.ISupportInitialize).BeginInit()

210:        Me.oCal.Schedule = Me.c1sched

220:        oUI.BusyProgress(40, "Configuring Outlook view...")
230:        Me.c1sched.CalendarInfo.DateFormatString = "M/d/yyyy"
            Me.c1sched.CalendarInfo.EndDayTime = System.TimeSpan.Parse("19:00:00")
240:        Me.c1sched.CalendarInfo.FirstDate = Date.Now.AddMonths(-12)
250:        Me.c1sched.CalendarInfo.LastDate = Date.Now.AddMonths(12)
            Me.c1sched.CalendarInfo.StartDayTime = System.TimeSpan.Parse("07:00:00")
260:        Me.c1sched.CalendarInfo.TimeFormatString = "tt"
            Me.c1sched.CalendarInfo.TimeScale = System.TimeSpan.Parse("00:30:00")
270:        Me.c1sched.CalendarInfo.WorkDays.AddRange(New System.DayOfWeek() {System.DayOfWeek.Monday, System.DayOfWeek.Tuesday, System.DayOfWeek.Wednesday, System.DayOfWeek.Thursday, System.DayOfWeek.Friday})
            '
            '
            '
280:        oUI.BusyProgress(60, "Finalizing Outlook view...")
290:        Me.c1sched.EditOptions = CType((C1.Win.C1Schedule.EditOptions.AllowDrag Or C1.Win.C1Schedule.EditOptions.AllowAppointmentEdit), C1.Win.C1Schedule.EditOptions)
300:        Me.c1sched.Location = New System.Drawing.Point(27, 28)
310:        Me.c1sched.Name = "c1sched"
320:        Me.c1sched.ShowContextMenu = False
330:        Me.c1sched.ShowReminderForm = False
340:        Me.c1sched.Size = New System.Drawing.Size(271, 185)
350:        Me.c1sched.TabIndex = 24
360:        Me.c1sched.Visible = False
370:        Me.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Black

380:        Me.Panel1.Controls.Add(Me.c1sched)

390:        CType(Me.c1sched.DataStorage.AppointmentStorage, System.ComponentModel.ISupportInitialize).EndInit()
400:        CType(Me.c1sched.DataStorage.CategoryStorage, System.ComponentModel.ISupportInitialize).EndInit()
410:        CType(Me.c1sched.DataStorage.ContactStorage, System.ComponentModel.ISupportInitialize).EndInit()
420:        CType(Me.c1sched.DataStorage.LabelStorage, System.ComponentModel.ISupportInitialize).EndInit()
430:        CType(Me.c1sched.DataStorage.ResourceStorage, System.ComponentModel.ISupportInitialize).EndInit()
440:        CType(Me.c1sched.DataStorage.StatusStorage, System.ComponentModel.ISupportInitialize).EndInit()
450:        CType(Me.c1sched, System.ComponentModel.ISupportInitialize).EndInit()

460:        Dim themeName As String = clsMarsUI.MainUI.ReadRegistry("AppTheme", "Glass")
            Dim theme As clsMarsUI.APPTHEMES


470:        oUI.BusyProgress(80, "Loading Outlook theme...")


560:        'clsMarsUI.MainUI.SetAppTheme(theme)
570:    Catch ex As Exception
580:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
590:    Finally
600:        oUI.BusyProgress(100, "", True)
        End Try
    End Sub



    Private Sub frmWindow_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Me.c1sched Is Nothing Then Me.initOutlookView()

        If optGantt.Checked = False Then
            c1sched.Visible = True
            c1sched.Dock = DockStyle.Fill

            clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType)

            TrackBar1.Value = 5
        Else
            Me.grpGantt.Visible = True
            Me.grpGantt.Dock = DockStyle.Fill
            Dim selDates() As Date = oCal.SelectedDates

            oCharter.setChartRange(Me.gantt, selDates(0), Me.dtStart.Value, Me.dtEnd.Value, Me.m_CalcType, Me.m_excludeTypes)
        End If

        Me.processOutlook = True
        IsLoaded = True

    End Sub


End Class
