Public Class frmTestDestination
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.UcDest = New sqlrd.ucDestination()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(0, 16)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(416, 362)
        Me.UcDest.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(257, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(338, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label2.Location = New System.Drawing.Point(0, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(276, 16)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Please select a destination to test with and then click Ok"
        Me.Label2.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 378)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(416, 30)
        Me.FlowLayoutPanel1.TabIndex = 13
        '
        'frmTestDestination
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(416, 408)
        Me.ControlBox = False
        Me.Controls.Add(Me.UcDest)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.Label2)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmTestDestination"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Destination for Testing"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmTestDestination_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        With UcDest
            .nReportID = 2777
            .nPackID = 2777
            .nSmartID = 2777
            .cmdImport.Visible = False
            .LoadAll()
        End With
    End Sub
    Public Function _ImportDestination() As Integer
        UcDest.DefaultDestinations = True
        UcDest.m_CanDisable = False

        Me.ShowDialog()

        If UserCancel = True Then Return 0

        Return UcDest.lsvDestination.SelectedNode.Tag
    End Function
    Public Sub _SelectDestination(Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0)
        Label2.Visible = True
        '  Me.UcDest.lsvDestination. = False
        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim oReport As New clsMarsReport
        Dim ok As Boolean

        If nReportID > 0 Then
            ok = oReport.RunSinglewithDefault(nReportID, UcDest.lsvDestination.SelectedNode.Tag)

            If ok = True Then
                MessageBox.Show("Schedule test completed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
            End If
        ElseIf nPackID > 0 Then

            ok = oReport.RunPackageWithDefault(nPackID, UcDest.lsvDestination.SelectedNode.Tag)

            If ok = True Then
                MessageBox.Show("Package test completed successfully!", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
            End If
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If UcDest.lsvDestination.SelectedNodes.Count = 0 Then
            MessageBox.Show("Please select a destination to test this schedule with", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub
End Class
