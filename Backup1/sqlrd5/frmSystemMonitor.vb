Imports Microsoft.Win32
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Public Class frmSystemMonitor
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As clsMarsData = New clsMarsData
    Dim oUI As New clsMarsUI
    Dim ep As New ErrorProvider
    Dim oPersist As String
    Dim oSchedule As New clsMarsScheduler
    Dim IsLoaded As Boolean = False
    Dim sortOrder As SortOrder = sortOrder.Ascending
    Dim logsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim sortBy As Integer = 1
    Dim logsortBy As Integer = 1
    Dim errlogsortBy As Integer = 1
    Dim errlogsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim retrylogsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim retrylogSortBy As Integer = 1
    Dim emailLogRow() As Integer
    Public m_eventID As Integer = 99999
    Dim oTs As ArrayList = New ArrayList
    Dim tcpListener As TcpListener
    Dim gItemList() As ListViewItem
    Dim supportMode As Boolean = False
    Dim gerrItemList() As ListViewItem
    Public standAlone As Boolean = False
    Dim WithEvents progressTable As DataTable
    Dim WithEvents emailLogFilterMan As DgvFilterPopup.DgvFilterManager
    Dim emailLogFilter As Hashtable = New Hashtable
    Dim currentErrorLogPage As Integer = 1
    Dim errorLogPageCount As Integer = -1
#Region "Friends"
    Friend WithEvents TabControlPanel12 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPackageFilter As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdPackClear As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdPackRefresh As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpPackageSort As System.Windows.Forms.GroupBox
    Friend WithEvents optPackageAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optPackageDesc As System.Windows.Forms.RadioButton
    Friend WithEvents txtKeepPack As System.Windows.Forms.NumericUpDown
    Friend WithEvents tvPackage As System.Windows.Forms.TreeView
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel7 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel8 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel9 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel10 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel9 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lsvReadReciepts As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents TableLayoutPanel10 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbFilterRR As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader29 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader31 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader32 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader33 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader34 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader35 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader30 As System.Windows.Forms.ColumnHeader
    Friend WithEvents muRR As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuReceived As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPending As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TableLayoutPanel11 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDeleteOlder As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbValueUnit As System.Windows.Forms.ComboBox
    Friend WithEvents cmnuEPClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuEPClearAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents grpBackup As System.Windows.Forms.GroupBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents grpSimple As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeep As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtBackup As System.Windows.Forms.DateTimePicker
    Friend WithEvents optDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optYearly As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents txtZipName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents UcUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents Label21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkZip As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnPackageHistory As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader36 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuClearEmailLog As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteSelectedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmScheduleMonCleaner As System.Timers.Timer
    Friend WithEvents ColumnHeader37 As System.Windows.Forms.ColumnHeader
    Friend WithEvents bgEmailLog As System.ComponentModel.BackgroundWorker
    Friend WithEvents tmEmailLog As System.Timers.Timer
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents txtSearch As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lsvErrorLog As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader40 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader41 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader42 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txterrLine As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txterrSource As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txterrNo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txterrSearch As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label27 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ColumnHeader43 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader44 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader45 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader46 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader47 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtErrorlog As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txterrSuggest As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TableLayoutPanel14 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lsvRetry As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader48 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader49 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader50 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnRefreshRetry As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuRetryTracker As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCopy As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuProcMan As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuProcProperties As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProcLoc As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProcEnd As System.Windows.Forms.MenuItem
    Friend WithEvents dgvEmailLog As System.Windows.Forms.DataGridView
    Friend WithEvents btnRefreshEmailLog As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label30 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeepErrorLogs As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label31 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkRefreshEmailLog As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents optReportHistory As System.Windows.Forms.RadioButton
    Friend WithEvents optPackageHistory As System.Windows.Forms.RadioButton
    Friend WithEvents optAutomationHistory As System.Windows.Forms.RadioButton
    Friend WithEvents optEventHistory As System.Windows.Forms.RadioButton
    Friend WithEvents optEventPackageHistory As System.Windows.Forms.RadioButton
    Friend WithEvents optSmartFolderHistory As System.Windows.Forms.RadioButton
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeepScheduleHistory As System.Windows.Forms.NumericUpDown
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnMoreHistoryData As DevComponents.DotNetBar.ButtonX
    Friend WithEvents historyViewer As sqlrd.ucScheduleHistory
    Friend WithEvents slRefreshInterval As DevComponents.DotNetBar.Controls.Slider
    Friend WithEvents pnlErrorLog As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents stabSystemMonitor As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel11 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabScheduleRetries As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel10 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabScheduleHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabDeferredDelivery As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabAuditTrail As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabScheduleRefresh As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabBackup As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabDefferedDelivery As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabErrorLog As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabEmailQueue As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabScheduleManager As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabEmailLog As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents lsvTaskManager As DevComponents.AdvTree.AdvTree
    Friend WithEvents ColumnHeader17 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader18 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents tvProcessing As DevComponents.AdvTree.AdvTree
    Friend WithEvents ColumnHeader19 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader20 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader38 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader39 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader57 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader58 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents txtSearchSchedules As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents RibbonBar1 As DevComponents.DotNetBar.RibbonBar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ControlContainerItem1 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents ControlContainerItem2 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem2 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents ControlContainerItem3 As DevComponents.DotNetBar.ControlContainerItem
    Friend WithEvents LabelItem3 As DevComponents.DotNetBar.LabelItem
    Friend WithEvents tvObjects As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector3 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle3 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents FlowLayoutPanel11 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ColumnHeader51 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader52 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader53 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader54 As System.Windows.Forms.ColumnHeader
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents ColumnHeader55 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pageNav As DevComponents.DotNetBar.Controls.PageNavigator
    Friend WithEvents btnPurgeHistory As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel
    Friend WithEvents advProp As DevComponents.DotNetBar.AdvPropertyGrid
#End Region
    Private m_SortingColumn As ColumnHeader

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents lsvLog As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvEmailQueue As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveOne As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveAll As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdSendNow As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuTaskMan As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuRemove As System.Windows.Forms.MenuItem
    Friend WithEvents cmbFilter As System.Windows.Forms.ComboBox
    Friend WithEvents lsvDefer As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optDeferAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDeferDesc As System.Windows.Forms.RadioButton
    Friend WithEvents mnuDefer As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuDeferRemove As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDeferDeliver As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Button1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdSaveBackup As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkBackups As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents tmScheduleMon As System.Timers.Timer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdAddSchedule As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvSchedules As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtzRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtRunTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents optrDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optrWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optrMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optrYearly As System.Windows.Forms.RadioButton
    Friend WithEvents mnuRefresh As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents tmEmailQueue As System.Timers.Timer
    Friend WithEvents chkAutoRefresh As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmnuSingleClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuSingleClearAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents cmnuPackageClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuPackageClear As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents cmnuAutoClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuAutoClear As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents chkOneEmailPerSchedule As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdErrorLog As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtKeepLogs As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExecute As System.Windows.Forms.MenuItem
    Friend WithEvents lsvAudit As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader23 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader26 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader27 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader28 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveSchedule As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSystemMonitor))
        Me.mnuTaskMan = New System.Windows.Forms.ContextMenu()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.mnuRemove = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuExecute = New System.Windows.Forms.MenuItem()
        Me.muRR = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuReceived = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuPending = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDefer = New System.Windows.Forms.ContextMenu()
        Me.MenuItem14 = New System.Windows.Forms.MenuItem()
        Me.mnuDeferDeliver = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuDeferRemove = New System.Windows.Forms.MenuItem()
        Me.tmScheduleMon = New System.Timers.Timer()
        Me.mnuRefresh = New System.Windows.Forms.ContextMenu()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.tmEmailQueue = New System.Timers.Timer()
        Me.cmnuSingleClear = New System.Windows.Forms.ContextMenu()
        Me.mnuSingleClearAll = New System.Windows.Forms.MenuItem()
        Me.mnuSingleClearSel = New System.Windows.Forms.MenuItem()
        Me.cmnuPackageClear = New System.Windows.Forms.ContextMenu()
        Me.mnuPackageClear = New System.Windows.Forms.MenuItem()
        Me.mnuPackageClearSel = New System.Windows.Forms.MenuItem()
        Me.cmnuAutoClear = New System.Windows.Forms.ContextMenu()
        Me.mnuAutoClear = New System.Windows.Forms.MenuItem()
        Me.mnuAutoClearSel = New System.Windows.Forms.MenuItem()
        Me.TabControlPanel12 = New DevComponents.DotNetBar.TabControlPanel()
        Me.tvPackage = New System.Windows.Forms.TreeView()
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdPackClear = New DevComponents.DotNetBar.ButtonX()
        Me.cmdPackRefresh = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtKeepPack = New System.Windows.Forms.NumericUpDown()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.btnPackageHistory = New DevComponents.DotNetBar.ButtonX()
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel()
        Me.grpPackageSort = New System.Windows.Forms.GroupBox()
        Me.optPackageAsc = New System.Windows.Forms.RadioButton()
        Me.optPackageDesc = New System.Windows.Forms.RadioButton()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.chkPackageFilter = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lsvRetry = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader48 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader49 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader50 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuRetryTracker = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel14 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnRefreshRetry = New DevComponents.DotNetBar.ButtonX()
        Me.lsvReadReciepts = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader29 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader31 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader32 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader33 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader34 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader35 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader30 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.cmbFilterRR = New System.Windows.Forms.ComboBox()
        Me.lsvAudit = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader23 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader25 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader26 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader27 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader28 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lsvSchedules = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader14 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader15 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader16 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader21 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader22 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader24 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmdRemoveSchedule = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAddSchedule = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.tvObjects = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector3 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle3 = New DevComponents.DotNetBar.ElementStyle()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtzRpt = New System.Windows.Forms.NumericUpDown()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.dtRunTime = New System.Windows.Forms.DateTimePicker()
        Me.optrDaily = New System.Windows.Forms.RadioButton()
        Me.optrWeekly = New System.Windows.Forms.RadioButton()
        Me.optrMonthly = New System.Windows.Forms.RadioButton()
        Me.optrYearly = New System.Windows.Forms.RadioButton()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkAutoRefresh = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.grpBackup = New System.Windows.Forms.GroupBox()
        Me.grpAdvanced = New System.Windows.Forms.GroupBox()
        Me.txtZipName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.UcUpdate = New sqlrd.ucScheduleUpdate()
        Me.Label21 = New DevComponents.DotNetBar.LabelX()
        Me.chkZip = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label19 = New DevComponents.DotNetBar.LabelX()
        Me.grpSimple = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtInterval = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.dtBackup = New System.Windows.Forms.DateTimePicker()
        Me.optDaily = New System.Windows.Forms.RadioButton()
        Me.optWeekly = New System.Windows.Forms.RadioButton()
        Me.optMonthly = New System.Windows.Forms.RadioButton()
        Me.optYearly = New System.Windows.Forms.RadioButton()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeep = New System.Windows.Forms.NumericUpDown()
        Me.rbAdvanced = New System.Windows.Forms.RadioButton()
        Me.rbSimple = New System.Windows.Forms.RadioButton()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSaveBackup = New DevComponents.DotNetBar.ButtonX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Button1 = New DevComponents.DotNetBar.ButtonX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.chkBackups = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lsvDefer = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader55 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader51 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader52 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader53 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader54 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.optDeferAsc = New System.Windows.Forms.RadioButton()
        Me.optDeferDesc = New System.Windows.Forms.RadioButton()
        Me.btnCopy = New DevComponents.DotNetBar.ButtonX()
        Me.txterrSuggest = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtErrorlog = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txterrLine = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label29 = New DevComponents.DotNetBar.LabelX()
        Me.Label28 = New DevComponents.DotNetBar.LabelX()
        Me.Label26 = New DevComponents.DotNetBar.LabelX()
        Me.txterrSource = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label25 = New DevComponents.DotNetBar.LabelX()
        Me.txterrNo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label24 = New DevComponents.DotNetBar.LabelX()
        Me.lsvErrorLog = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader42 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader40 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader41 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader43 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader44 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader45 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader46 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader47 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdErrorLog = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label27 = New DevComponents.DotNetBar.LabelX()
        Me.txterrSearch = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label30 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeepErrorLogs = New System.Windows.Forms.NumericUpDown()
        Me.Label31 = New DevComponents.DotNetBar.LabelX()
        Me.lsvEmailQueue = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader37 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label17 = New DevComponents.DotNetBar.LabelX()
        Me.txtDeleteOlder = New System.Windows.Forms.NumericUpDown()
        Me.cmbValueUnit = New System.Windows.Forms.ComboBox()
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdSendNow = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveOne = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveAll = New DevComponents.DotNetBar.ButtonX()
        Me.chkOneEmailPerSchedule = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.dgvEmailLog = New System.Windows.Forms.DataGridView()
        Me.lsvLog = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader36 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuClearEmailLog = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteSelectedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DeleteAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnRefreshEmailLog = New DevComponents.DotNetBar.ButtonX()
        Me.txtKeepLogs = New System.Windows.Forms.NumericUpDown()
        Me.Label20 = New DevComponents.DotNetBar.LabelX()
        Me.txtSearch = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label23 = New DevComponents.DotNetBar.LabelX()
        Me.chkRefreshEmailLog = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSearchSchedules = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.slRefreshInterval = New DevComponents.DotNetBar.Controls.Slider()
        Me.cmbFilter = New System.Windows.Forms.ComboBox()
        Me.mnuProcMan = New System.Windows.Forms.ContextMenu()
        Me.mnuProcProperties = New System.Windows.Forms.MenuItem()
        Me.mnuProcLoc = New System.Windows.Forms.MenuItem()
        Me.MenuItem16 = New System.Windows.Forms.MenuItem()
        Me.mnuProcEnd = New System.Windows.Forms.MenuItem()
        Me.cmnuEPClear = New System.Windows.Forms.ContextMenu()
        Me.mnuEPClearAll = New System.Windows.Forms.MenuItem()
        Me.mnuEPClearSel = New System.Windows.Forms.MenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.tvProcessing = New DevComponents.AdvTree.AdvTree()
        Me.ColumnHeader19 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader20 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader38 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader39 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader57 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader58 = New DevComponents.AdvTree.ColumnHeader()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.advProp = New DevComponents.DotNetBar.AdvPropertyGrid()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.lsvTaskManager = New DevComponents.AdvTree.AdvTree()
        Me.ColumnHeader17 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader18 = New DevComponents.AdvTree.ColumnHeader()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.pnlErrorLog = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnMoreHistoryData = New DevComponents.DotNetBar.ButtonX()
        Me.btnPurgeHistory = New DevComponents.DotNetBar.ButtonX()
        Me.txtKeepScheduleHistory = New System.Windows.Forms.NumericUpDown()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.optReportHistory = New System.Windows.Forms.RadioButton()
        Me.optPackageHistory = New System.Windows.Forms.RadioButton()
        Me.optAutomationHistory = New System.Windows.Forms.RadioButton()
        Me.optEventHistory = New System.Windows.Forms.RadioButton()
        Me.optEventPackageHistory = New System.Windows.Forms.RadioButton()
        Me.optSmartFolderHistory = New System.Windows.Forms.RadioButton()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.tmScheduleMonCleaner = New System.Timers.Timer()
        Me.bgEmailLog = New System.ComponentModel.BackgroundWorker()
        Me.tmEmailLog = New System.Timers.Timer()
        Me.stabSystemMonitor = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabEmailLog = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.RibbonBar1 = New DevComponents.DotNetBar.RibbonBar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.ControlContainerItem1 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem3 = New DevComponents.DotNetBar.LabelItem()
        Me.ControlContainerItem2 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.LabelItem2 = New DevComponents.DotNetBar.LabelItem()
        Me.ControlContainerItem3 = New DevComponents.DotNetBar.ControlContainerItem()
        Me.stabScheduleManager = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabBackup = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel10 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.historyViewer = New sqlrd.ucScheduleHistory()
        Me.stabScheduleHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.FlowLayoutPanel11 = New System.Windows.Forms.Panel()
        Me.stabScheduleRefresh = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.pageNav = New DevComponents.DotNetBar.Controls.PageNavigator()
        Me.stabErrorLog = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabDefferedDelivery = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabDeferredDelivery = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel11 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabScheduleRetries = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabEmailQueue = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.stabAuditTrail = New DevComponents.DotNetBar.SuperTabItem()
        Me.muRR.SuspendLayout()
        CType(Me.tmScheduleMon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEmailQueue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel12.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.txtKeepPack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.grpPackageSort.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.mnuRetryTracker.SuspendLayout()
        Me.TableLayoutPanel14.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.tvObjects, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtzRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel10.SuspendLayout()
        Me.grpBackup.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        Me.grpSimple.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKeep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel9.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.FlowLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        CType(Me.txtKeepErrorLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel11.SuspendLayout()
        CType(Me.txtDeleteOlder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel5.SuspendLayout()
        CType(Me.dgvEmailLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuClearEmailLog.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        CType(Me.txtKeepLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.tvProcessing, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tvProcessing.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupPanel1.SuspendLayout()
        CType(Me.advProp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.lsvTaskManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlErrorLog.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        CType(Me.txtKeepScheduleHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.tmScheduleMonCleaner, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEmailLog, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabSystemMonitor, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabSystemMonitor.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.RibbonBar1.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel10.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.FlowLayoutPanel11.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        Me.SuperTabControlPanel11.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuTaskMan
        '
        Me.mnuTaskMan.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem11, Me.MenuItem12, Me.MenuItem13, Me.mnuRemove, Me.MenuItem3, Me.mnuExecute})
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 0
        Me.MenuItem11.Tag = "MenuItem11"
        Me.MenuItem11.Text = "Properties"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 1
        Me.MenuItem12.Tag = "MenuItem12"
        Me.MenuItem12.Text = "Show in Explorer"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 2
        Me.MenuItem13.Text = "-"
        '
        'mnuRemove
        '
        Me.mnuRemove.Index = 3
        Me.mnuRemove.Tag = "mnuRemove"
        Me.mnuRemove.Text = "Remove From Queue"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 4
        Me.MenuItem3.Text = "-"
        '
        'mnuExecute
        '
        Me.mnuExecute.Index = 5
        Me.mnuExecute.Text = "Execute Schedule"
        '
        'muRR
        '
        Me.muRR.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReceived, Me.ToolStripSeparator1, Me.mnuPending, Me.ToolStripSeparator2, Me.mnuDelete})
        Me.muRR.Name = "muRR"
        Me.muRR.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.muRR.Size = New System.Drawing.Size(172, 82)
        '
        'mnuReceived
        '
        Me.mnuReceived.Name = "mnuReceived"
        Me.mnuReceived.Size = New System.Drawing.Size(171, 22)
        Me.mnuReceived.Text = "Mark as 'Received'"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(168, 6)
        '
        'mnuPending
        '
        Me.mnuPending.Name = "mnuPending"
        Me.mnuPending.Size = New System.Drawing.Size(171, 22)
        Me.mnuPending.Text = "Mark as 'Pending'"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(168, 6)
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(171, 22)
        Me.mnuDelete.Text = "Delete"
        '
        'mnuDefer
        '
        Me.mnuDefer.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem14, Me.mnuDeferDeliver, Me.MenuItem1, Me.mnuDeferRemove})
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 0
        Me.MenuItem14.Text = "Open File(s)"
        '
        'mnuDeferDeliver
        '
        Me.mnuDeferDeliver.Index = 1
        Me.mnuDeferDeliver.Text = "Deliver Now"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.Text = "-"
        '
        'mnuDeferRemove
        '
        Me.mnuDeferRemove.Index = 3
        Me.mnuDeferRemove.Text = "Remove"
        '
        'tmScheduleMon
        '
        Me.tmScheduleMon.Interval = 2000.0R
        Me.tmScheduleMon.SynchronizingObject = Me
        '
        'mnuRefresh
        '
        Me.mnuRefresh.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Delete"
        '
        'tmEmailQueue
        '
        Me.tmEmailQueue.Interval = 10000.0R
        Me.tmEmailQueue.SynchronizingObject = Me
        '
        'cmnuSingleClear
        '
        Me.cmnuSingleClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSingleClearAll, Me.mnuSingleClearSel})
        '
        'mnuSingleClearAll
        '
        Me.mnuSingleClearAll.Index = 0
        Me.mnuSingleClearAll.Text = "Clear All"
        '
        'mnuSingleClearSel
        '
        Me.mnuSingleClearSel.Index = 1
        Me.mnuSingleClearSel.Text = "Clear Selected"
        '
        'cmnuPackageClear
        '
        Me.cmnuPackageClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuPackageClear, Me.mnuPackageClearSel})
        '
        'mnuPackageClear
        '
        Me.mnuPackageClear.Index = 0
        Me.mnuPackageClear.Text = "Clear All"
        '
        'mnuPackageClearSel
        '
        Me.mnuPackageClearSel.Index = 1
        Me.mnuPackageClearSel.Text = "Clear Selected"
        '
        'cmnuAutoClear
        '
        Me.cmnuAutoClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAutoClear, Me.mnuAutoClearSel})
        '
        'mnuAutoClear
        '
        Me.mnuAutoClear.Index = 0
        Me.mnuAutoClear.Text = "Clear All"
        '
        'mnuAutoClearSel
        '
        Me.mnuAutoClearSel.Index = 1
        Me.mnuAutoClearSel.Text = "Clear Selected"
        '
        'TabControlPanel12
        '
        Me.TabControlPanel12.Controls.Add(Me.tvPackage)
        Me.TabControlPanel12.Controls.Add(Me.FlowLayoutPanel7)
        Me.TabControlPanel12.Controls.Add(Me.FlowLayoutPanel4)
        Me.TabControlPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel12.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel12.Name = "TabControlPanel12"
        Me.TabControlPanel12.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel12.Size = New System.Drawing.Size(255, 443)
        Me.TabControlPanel12.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(244, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(232, Byte), Integer))
        Me.TabControlPanel12.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel12.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel12.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel12.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel12.TabIndex = 2
        '
        'tvPackage
        '
        Me.tvPackage.BackColor = System.Drawing.Color.White
        Me.tvPackage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvPackage.HideSelection = False
        Me.tvPackage.Indent = 19
        Me.tvPackage.ItemHeight = 16
        Me.tvPackage.LineColor = System.Drawing.Color.Empty
        Me.tvPackage.Location = New System.Drawing.Point(1, 56)
        Me.tvPackage.Name = "tvPackage"
        Me.tvPackage.Size = New System.Drawing.Size(253, 352)
        Me.tvPackage.TabIndex = 0
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdPackClear)
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdPackRefresh)
        Me.FlowLayoutPanel7.Controls.Add(Me.TableLayoutPanel4)
        Me.FlowLayoutPanel7.Controls.Add(Me.btnPackageHistory)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(1, 408)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(253, 34)
        Me.FlowLayoutPanel7.TabIndex = 20
        '
        'cmdPackClear
        '
        Me.cmdPackClear.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdPackClear.Image = CType(resources.GetObject("cmdPackClear.Image"), System.Drawing.Image)
        Me.cmdPackClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPackClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdPackClear.Name = "cmdPackClear"
        Me.cmdPackClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdPackClear.TabIndex = 7
        '
        'cmdPackRefresh
        '
        Me.cmdPackRefresh.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdPackRefresh.Image = CType(resources.GetObject("cmdPackRefresh.Image"), System.Drawing.Image)
        Me.cmdPackRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPackRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdPackRefresh.Name = "cmdPackRefresh"
        Me.cmdPackRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdPackRefresh.TabIndex = 6
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.txtKeepPack, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label16, 0, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 32)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(267, 27)
        Me.TableLayoutPanel4.TabIndex = 21
        '
        'txtKeepPack
        '
        Me.txtKeepPack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepPack.Location = New System.Drawing.Point(136, 4)
        Me.txtKeepPack.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepPack.Name = "txtKeepPack"
        Me.txtKeepPack.Size = New System.Drawing.Size(56, 20)
        Me.txtKeepPack.TabIndex = 15
        Me.txtKeepPack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(3, 3)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(127, 21)
        Me.Label16.TabIndex = 14
        '
        'btnPackageHistory
        '
        Me.btnPackageHistory.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPackageHistory.Image = CType(resources.GetObject("btnPackageHistory.Image"), System.Drawing.Image)
        Me.btnPackageHistory.Location = New System.Drawing.Point(3, 65)
        Me.btnPackageHistory.Name = "btnPackageHistory"
        Me.btnPackageHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnPackageHistory.TabIndex = 22
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel4.Controls.Add(Me.grpPackageSort)
        Me.FlowLayoutPanel4.Controls.Add(Me.GroupBox9)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(253, 55)
        Me.FlowLayoutPanel4.TabIndex = 19
        '
        'grpPackageSort
        '
        Me.grpPackageSort.BackColor = System.Drawing.Color.Transparent
        Me.grpPackageSort.Controls.Add(Me.optPackageAsc)
        Me.grpPackageSort.Controls.Add(Me.optPackageDesc)
        Me.grpPackageSort.Location = New System.Drawing.Point(3, 3)
        Me.grpPackageSort.Name = "grpPackageSort"
        Me.grpPackageSort.Size = New System.Drawing.Size(261, 44)
        Me.grpPackageSort.TabIndex = 1
        Me.grpPackageSort.TabStop = False
        '
        'optPackageAsc
        '
        Me.optPackageAsc.Checked = True
        Me.optPackageAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPackageAsc.Location = New System.Drawing.Point(16, 16)
        Me.optPackageAsc.Name = "optPackageAsc"
        Me.optPackageAsc.Size = New System.Drawing.Size(120, 24)
        Me.optPackageAsc.TabIndex = 1
        Me.optPackageAsc.TabStop = True
        Me.optPackageAsc.Tag = "ASC"
        Me.optPackageAsc.Text = "Ascending Order"
        '
        'optPackageDesc
        '
        Me.optPackageDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPackageDesc.Location = New System.Drawing.Point(144, 16)
        Me.optPackageDesc.Name = "optPackageDesc"
        Me.optPackageDesc.Size = New System.Drawing.Size(120, 24)
        Me.optPackageDesc.TabIndex = 0
        Me.optPackageDesc.Tag = "DESC"
        Me.optPackageDesc.Text = "Descending Order"
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox9.Controls.Add(Me.chkPackageFilter)
        Me.GroupBox9.Location = New System.Drawing.Point(3, 53)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(261, 44)
        Me.GroupBox9.TabIndex = 11
        Me.GroupBox9.TabStop = False
        '
        'chkPackageFilter
        '
        '
        '
        '
        Me.chkPackageFilter.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPackageFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPackageFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkPackageFilter.Name = "chkPackageFilter"
        Me.chkPackageFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkPackageFilter.TabIndex = 0
        '
        'lsvRetry
        '
        Me.lsvRetry.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvRetry.Border.Class = "ListViewBorder"
        Me.lsvRetry.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvRetry.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader48, Me.ColumnHeader49, Me.ColumnHeader50})
        Me.lsvRetry.ContextMenuStrip = Me.mnuRetryTracker
        Me.lsvRetry.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvRetry.ForeColor = System.Drawing.Color.Black
        Me.lsvRetry.FullRowSelect = True
        Me.lsvRetry.HideSelection = False
        Me.lsvRetry.Location = New System.Drawing.Point(0, 0)
        Me.lsvRetry.Name = "lsvRetry"
        Me.lsvRetry.Size = New System.Drawing.Size(974, 625)
        Me.lsvRetry.TabIndex = 0
        Me.lsvRetry.UseCompatibleStateImageBehavior = False
        Me.lsvRetry.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "Name"
        Me.ColumnHeader48.Width = 348
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Last Retry"
        Me.ColumnHeader49.Width = 155
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Retry #"
        Me.ColumnHeader50.Width = 86
        '
        'mnuRetryTracker
        '
        Me.mnuRetryTracker.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.mnuRetryTracker.Name = "mnuRetryTracker"
        Me.mnuRetryTracker.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuRetryTracker.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'TableLayoutPanel14
        '
        Me.TableLayoutPanel14.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel14.ColumnCount = 2
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Controls.Add(Me.btnRefreshRetry, 0, 0)
        Me.TableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel14.Location = New System.Drawing.Point(0, 625)
        Me.TableLayoutPanel14.Name = "TableLayoutPanel14"
        Me.TableLayoutPanel14.RowCount = 1
        Me.TableLayoutPanel14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Size = New System.Drawing.Size(974, 29)
        Me.TableLayoutPanel14.TabIndex = 0
        '
        'btnRefreshRetry
        '
        Me.btnRefreshRetry.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRefreshRetry.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRefreshRetry.Image = CType(resources.GetObject("btnRefreshRetry.Image"), System.Drawing.Image)
        Me.btnRefreshRetry.Location = New System.Drawing.Point(3, 3)
        Me.btnRefreshRetry.Name = "btnRefreshRetry"
        Me.btnRefreshRetry.Size = New System.Drawing.Size(75, 23)
        Me.btnRefreshRetry.TabIndex = 0
        '
        'lsvReadReciepts
        '
        Me.lsvReadReciepts.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvReadReciepts.Border.Class = "ListViewBorder"
        Me.lsvReadReciepts.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvReadReciepts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader29, Me.ColumnHeader31, Me.ColumnHeader32, Me.ColumnHeader33, Me.ColumnHeader34, Me.ColumnHeader35, Me.ColumnHeader30})
        Me.lsvReadReciepts.ContextMenuStrip = Me.muRR
        Me.lsvReadReciepts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvReadReciepts.ForeColor = System.Drawing.Color.Black
        Me.lsvReadReciepts.FullRowSelect = True
        Me.lsvReadReciepts.GridLines = True
        Me.lsvReadReciepts.HideSelection = False
        Me.lsvReadReciepts.Location = New System.Drawing.Point(0, 29)
        Me.lsvReadReciepts.Name = "lsvReadReciepts"
        Me.lsvReadReciepts.Size = New System.Drawing.Size(974, 625)
        Me.lsvReadReciepts.TabIndex = 1
        Me.lsvReadReciepts.UseCompatibleStateImageBehavior = False
        Me.lsvReadReciepts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader29
        '
        Me.ColumnHeader29.Text = "Schedule Name"
        Me.ColumnHeader29.Width = 111
        '
        'ColumnHeader31
        '
        Me.ColumnHeader31.Text = "Email Subject"
        Me.ColumnHeader31.Width = 118
        '
        'ColumnHeader32
        '
        Me.ColumnHeader32.Text = "Sent To"
        Me.ColumnHeader32.Width = 87
        '
        'ColumnHeader33
        '
        Me.ColumnHeader33.Text = "Date Sent"
        Me.ColumnHeader33.Width = 80
        '
        'ColumnHeader34
        '
        Me.ColumnHeader34.Text = "Next Check"
        Me.ColumnHeader34.Width = 95
        '
        'ColumnHeader35
        '
        Me.ColumnHeader35.Text = "Status"
        Me.ColumnHeader35.Width = 99
        '
        'ColumnHeader30
        '
        Me.ColumnHeader30.Text = "Last Checked"
        Me.ColumnHeader30.Width = 95
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel10.ColumnCount = 2
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel10.Controls.Add(Me.Label15, 0, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.cmbFilterRR, 1, 0)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 1
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(974, 29)
        Me.TableLayoutPanel10.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.Location = New System.Drawing.Point(3, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 23)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Filter"
        '
        'cmbFilterRR
        '
        Me.cmbFilterRR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFilterRR.FormattingEnabled = True
        Me.cmbFilterRR.Items.AddRange(New Object() {"Received", "Pending", "None", "Delivered"})
        Me.cmbFilterRR.Location = New System.Drawing.Point(54, 3)
        Me.cmbFilterRR.Name = "cmbFilterRR"
        Me.cmbFilterRR.Size = New System.Drawing.Size(121, 21)
        Me.cmbFilterRR.TabIndex = 1
        '
        'lsvAudit
        '
        Me.lsvAudit.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvAudit.Border.Class = "ListViewBorder"
        Me.lsvAudit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvAudit.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader23, Me.ColumnHeader25, Me.ColumnHeader26, Me.ColumnHeader27, Me.ColumnHeader28})
        Me.lsvAudit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvAudit.ForeColor = System.Drawing.Color.Black
        Me.lsvAudit.FullRowSelect = True
        Me.lsvAudit.GridLines = True
        Me.lsvAudit.HideSelection = False
        Me.lsvAudit.Location = New System.Drawing.Point(0, 0)
        Me.lsvAudit.Name = "lsvAudit"
        Me.lsvAudit.Size = New System.Drawing.Size(974, 654)
        Me.lsvAudit.TabIndex = 0
        Me.lsvAudit.UseCompatibleStateImageBehavior = False
        Me.lsvAudit.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "Entry Date"
        Me.ColumnHeader23.Width = 148
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "User ID"
        Me.ColumnHeader25.Width = 66
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Object Type"
        Me.ColumnHeader26.Width = 93
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Object Name"
        Me.ColumnHeader27.Width = 161
        '
        'ColumnHeader28
        '
        Me.ColumnHeader28.Text = "Event"
        Me.ColumnHeader28.Width = 146
        '
        'lsvSchedules
        '
        Me.lsvSchedules.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvSchedules.Border.Class = "ListViewBorder"
        Me.lsvSchedules.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16, Me.ColumnHeader21, Me.ColumnHeader22, Me.ColumnHeader24})
        Me.lsvSchedules.ContextMenu = Me.mnuRefresh
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSchedules.ForeColor = System.Drawing.Color.Navy
        Me.lsvSchedules.FullRowSelect = True
        Me.lsvSchedules.GridLines = True
        Me.lsvSchedules.Location = New System.Drawing.Point(384, 69)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(590, 585)
        Me.lsvSchedules.TabIndex = 6
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Type"
        Me.ColumnHeader14.Width = 74
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Name"
        Me.ColumnHeader15.Width = 117
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Frequency"
        Me.ColumnHeader16.Width = 74
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Last Run"
        Me.ColumnHeader21.Width = 87
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Due Next"
        Me.ColumnHeader22.Width = 116
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "Repeat Interval"
        Me.ColumnHeader24.Width = 95
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel9.ColumnCount = 1
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Controls.Add(Me.cmdRemoveSchedule, 0, 1)
        Me.TableLayoutPanel9.Controls.Add(Me.cmdAddSchedule, 0, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Left
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(307, 69)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 2
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(77, 585)
        Me.TableLayoutPanel9.TabIndex = 10
        '
        'cmdRemoveSchedule
        '
        Me.cmdRemoveSchedule.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveSchedule.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveSchedule.Image = CType(resources.GetObject("cmdRemoveSchedule.Image"), System.Drawing.Image)
        Me.cmdRemoveSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveSchedule.Location = New System.Drawing.Point(3, 295)
        Me.cmdRemoveSchedule.Name = "cmdRemoveSchedule"
        Me.cmdRemoveSchedule.Size = New System.Drawing.Size(71, 23)
        Me.cmdRemoveSchedule.TabIndex = 5
        '
        'cmdAddSchedule
        '
        Me.cmdAddSchedule.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddSchedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAddSchedule.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddSchedule.Image = CType(resources.GetObject("cmdAddSchedule.Image"), System.Drawing.Image)
        Me.cmdAddSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddSchedule.Location = New System.Drawing.Point(3, 266)
        Me.cmdAddSchedule.Name = "cmdAddSchedule"
        Me.cmdAddSchedule.Size = New System.Drawing.Size(71, 23)
        Me.cmdAddSchedule.TabIndex = 5
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.tvObjects)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox5.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(307, 385)
        Me.GroupBox5.TabIndex = 8
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Select object to refresh"
        '
        'tvObjects
        '
        Me.tvObjects.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvObjects.AllowDrop = True
        Me.tvObjects.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvObjects.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvObjects.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvObjects.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvObjects.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvObjects.Location = New System.Drawing.Point(3, 17)
        Me.tvObjects.MultiSelect = True
        Me.tvObjects.Name = "tvObjects"
        Me.tvObjects.NodesConnector = Me.NodeConnector3
        Me.tvObjects.NodeStyle = Me.ElementStyle3
        Me.tvObjects.PathSeparator = ";"
        Me.tvObjects.Size = New System.Drawing.Size(301, 365)
        Me.tvObjects.Styles.Add(Me.ElementStyle3)
        Me.tvObjects.TabIndex = 0
        Me.tvObjects.Text = "AdvTree1"
        '
        'NodeConnector3
        '
        Me.NodeConnector3.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle3
        '
        Me.ElementStyle3.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle3.Name = "ElementStyle3"
        Me.ElementStyle3.TextColor = System.Drawing.SystemColors.ControlText
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtzRpt)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.dtRunTime)
        Me.GroupBox4.Controls.Add(Me.optrDaily)
        Me.GroupBox4.Controls.Add(Me.optrWeekly)
        Me.GroupBox4.Controls.Add(Me.optrMonthly)
        Me.GroupBox4.Controls.Add(Me.optrYearly)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox4.Location = New System.Drawing.Point(0, 385)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(307, 200)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Refresh the schedule every"
        '
        'txtzRpt
        '
        Me.txtzRpt.Location = New System.Drawing.Point(112, 142)
        Me.txtzRpt.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtzRpt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtzRpt.Name = "txtzRpt"
        Me.txtzRpt.Size = New System.Drawing.Size(80, 21)
        Me.txtzRpt.TabIndex = 3
        Me.txtzRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtzRpt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label10
        '
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 120)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "At"
        '
        'dtRunTime
        '
        Me.dtRunTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtRunTime.Location = New System.Drawing.Point(112, 120)
        Me.dtRunTime.Name = "dtRunTime"
        Me.dtRunTime.ShowUpDown = True
        Me.dtRunTime.Size = New System.Drawing.Size(80, 21)
        Me.dtRunTime.TabIndex = 1
        '
        'optrDaily
        '
        Me.optrDaily.Checked = True
        Me.optrDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrDaily.Location = New System.Drawing.Point(8, 24)
        Me.optrDaily.Name = "optrDaily"
        Me.optrDaily.Size = New System.Drawing.Size(104, 24)
        Me.optrDaily.TabIndex = 0
        Me.optrDaily.TabStop = True
        Me.optrDaily.Text = "Day"
        '
        'optrWeekly
        '
        Me.optrWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrWeekly.Location = New System.Drawing.Point(8, 48)
        Me.optrWeekly.Name = "optrWeekly"
        Me.optrWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optrWeekly.TabIndex = 0
        Me.optrWeekly.Text = "Week"
        '
        'optrMonthly
        '
        Me.optrMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrMonthly.Location = New System.Drawing.Point(8, 72)
        Me.optrMonthly.Name = "optrMonthly"
        Me.optrMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optrMonthly.TabIndex = 0
        Me.optrMonthly.Text = "Month"
        '
        'optrYearly
        '
        Me.optrYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrYearly.Location = New System.Drawing.Point(8, 96)
        Me.optrYearly.Name = "optrYearly"
        Me.optrYearly.Size = New System.Drawing.Size(104, 24)
        Me.optrYearly.TabIndex = 0
        Me.optrYearly.Text = "Year"
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 144)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 16)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "And repeat every "
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(200, 144)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 16)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "days"
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel10.Controls.Add(Me.chkAutoRefresh)
        Me.FlowLayoutPanel10.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel10.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(974, 69)
        Me.FlowLayoutPanel10.TabIndex = 9
        '
        'chkAutoRefresh
        '
        Me.chkAutoRefresh.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAutoRefresh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoRefresh.Location = New System.Drawing.Point(3, 3)
        Me.chkAutoRefresh.Name = "chkAutoRefresh"
        Me.chkAutoRefresh.Size = New System.Drawing.Size(408, 24)
        Me.chkAutoRefresh.TabIndex = 10
        Me.chkAutoRefresh.Text = "Enable schedule auto-refresh"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(616, 32)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = resources.GetString("Label7.Text")
        Me.Label7.WordWrap = True
        '
        'grpBackup
        '
        Me.grpBackup.BackColor = System.Drawing.Color.Transparent
        Me.grpBackup.Controls.Add(Me.grpAdvanced)
        Me.grpBackup.Controls.Add(Me.grpSimple)
        Me.grpBackup.Controls.Add(Me.txtKeep)
        Me.grpBackup.Controls.Add(Me.rbAdvanced)
        Me.grpBackup.Controls.Add(Me.rbSimple)
        Me.grpBackup.Controls.Add(Me.txtPath)
        Me.grpBackup.Controls.Add(Me.cmdSaveBackup)
        Me.grpBackup.Controls.Add(Me.Label6)
        Me.grpBackup.Controls.Add(Me.Button1)
        Me.grpBackup.Controls.Add(Me.Label8)
        Me.grpBackup.Controls.Add(Me.Label9)
        Me.grpBackup.Enabled = False
        Me.grpBackup.Location = New System.Drawing.Point(15, 42)
        Me.grpBackup.Name = "grpBackup"
        Me.grpBackup.Size = New System.Drawing.Size(552, 476)
        Me.grpBackup.TabIndex = 1
        Me.grpBackup.TabStop = False
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.txtZipName)
        Me.grpAdvanced.Controls.Add(Me.UcUpdate)
        Me.grpAdvanced.Controls.Add(Me.Label21)
        Me.grpAdvanced.Controls.Add(Me.chkZip)
        Me.grpAdvanced.Controls.Add(Me.Label19)
        Me.grpAdvanced.Location = New System.Drawing.Point(20, 36)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(506, 369)
        Me.grpAdvanced.TabIndex = 2
        Me.grpAdvanced.TabStop = False
        '
        'txtZipName
        '
        Me.txtZipName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtZipName.Border.Class = "TextBoxBorder"
        Me.txtZipName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtZipName.Enabled = False
        Me.txtZipName.ForeColor = System.Drawing.Color.Black
        Me.txtZipName.Location = New System.Drawing.Point(86, 335)
        Me.txtZipName.Name = "txtZipName"
        Me.txtZipName.Size = New System.Drawing.Size(311, 21)
        Me.txtZipName.TabIndex = 2
        '
        'UcUpdate
        '
        Me.UcUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcUpdate.Location = New System.Drawing.Point(6, 9)
        Me.UcUpdate.m_collaborationServerID = 0
        Me.UcUpdate.m_RepeatUnit = ""
        Me.UcUpdate.Name = "UcUpdate"
        Me.UcUpdate.Size = New System.Drawing.Size(493, 299)
        Me.UcUpdate.TabIndex = 0
        '
        'Label21
        '
        '
        '
        '
        Me.Label21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(398, 339)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(62, 17)
        Me.Label21.TabIndex = 13
        Me.Label21.Text = ".zip"
        '
        'chkZip
        '
        Me.chkZip.AutoSize = True
        '
        '
        '
        Me.chkZip.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkZip.Location = New System.Drawing.Point(17, 314)
        Me.chkZip.Name = "chkZip"
        Me.chkZip.Size = New System.Drawing.Size(135, 16)
        Me.chkZip.TabIndex = 1
        Me.chkZip.Text = "Compress (Zip) Backup"
        '
        'Label19
        '
        '
        '
        '
        Me.Label19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(45, 339)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 17)
        Me.Label19.TabIndex = 12
        Me.Label19.Text = "Name"
        '
        'grpSimple
        '
        Me.grpSimple.Controls.Add(Me.GroupBox3)
        Me.grpSimple.Location = New System.Drawing.Point(20, 36)
        Me.grpSimple.Name = "grpSimple"
        Me.grpSimple.Size = New System.Drawing.Size(506, 369)
        Me.grpSimple.TabIndex = 2
        Me.grpSimple.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtInterval)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.dtBackup)
        Me.GroupBox3.Controls.Add(Me.optDaily)
        Me.GroupBox3.Controls.Add(Me.optWeekly)
        Me.GroupBox3.Controls.Add(Me.optMonthly)
        Me.GroupBox3.Controls.Add(Me.optYearly)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 15)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(490, 342)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Perform a system backup every"
        '
        'txtInterval
        '
        Me.txtInterval.Location = New System.Drawing.Point(159, 201)
        Me.txtInterval.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtInterval.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtInterval.Name = "txtInterval"
        Me.txtInterval.Size = New System.Drawing.Size(80, 21)
        Me.txtInterval.TabIndex = 5
        Me.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInterval.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(31, 173)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "At"
        '
        'dtBackup
        '
        Me.dtBackup.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtBackup.Location = New System.Drawing.Point(159, 171)
        Me.dtBackup.Name = "dtBackup"
        Me.dtBackup.ShowUpDown = True
        Me.dtBackup.Size = New System.Drawing.Size(80, 21)
        Me.dtBackup.TabIndex = 4
        '
        'optDaily
        '
        Me.optDaily.Checked = True
        Me.optDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDaily.Location = New System.Drawing.Point(31, 35)
        Me.optDaily.Name = "optDaily"
        Me.optDaily.Size = New System.Drawing.Size(104, 24)
        Me.optDaily.TabIndex = 0
        Me.optDaily.TabStop = True
        Me.optDaily.Text = "Day"
        '
        'optWeekly
        '
        Me.optWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekly.Location = New System.Drawing.Point(31, 67)
        Me.optWeekly.Name = "optWeekly"
        Me.optWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optWeekly.TabIndex = 1
        Me.optWeekly.Text = "Week"
        '
        'optMonthly
        '
        Me.optMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optMonthly.Location = New System.Drawing.Point(31, 99)
        Me.optMonthly.Name = "optMonthly"
        Me.optMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optMonthly.TabIndex = 2
        Me.optMonthly.Text = "Month"
        '
        'optYearly
        '
        Me.optYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optYearly.Location = New System.Drawing.Point(31, 131)
        Me.optYearly.Name = "optYearly"
        Me.optYearly.Size = New System.Drawing.Size(104, 24)
        Me.optYearly.TabIndex = 3
        Me.optYearly.Text = "Year"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(31, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "And repeat every "
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(247, 203)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "days"
        '
        'txtKeep
        '
        Me.txtKeep.Location = New System.Drawing.Point(235, 444)
        Me.txtKeep.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtKeep.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtKeep.Name = "txtKeep"
        Me.txtKeep.Size = New System.Drawing.Size(80, 21)
        Me.txtKeep.TabIndex = 5
        Me.txtKeep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKeep.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.Location = New System.Drawing.Point(78, 14)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(73, 17)
        Me.rbAdvanced.TabIndex = 1
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = True
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.Checked = True
        Me.rbSimple.Location = New System.Drawing.Point(15, 13)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(55, 17)
        Me.rbSimple.TabIndex = 0
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.ForeColor = System.Drawing.Color.Black
        Me.txtPath.Location = New System.Drawing.Point(106, 411)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(312, 21)
        Me.txtPath.TabIndex = 3
        Me.txtPath.Tag = "memo"
        '
        'cmdSaveBackup
        '
        Me.cmdSaveBackup.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSaveBackup.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSaveBackup.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSaveBackup.Location = New System.Drawing.Point(442, 444)
        Me.cmdSaveBackup.Name = "cmdSaveBackup"
        Me.cmdSaveBackup.Size = New System.Drawing.Size(48, 23)
        Me.cmdSaveBackup.TabIndex = 6
        Me.cmdSaveBackup.Text = "Sa&ve"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(34, 413)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Backup Path"
        '
        'Button1
        '
        Me.Button1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button1.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(442, 411)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(48, 21)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "..."
        '
        'Label8
        '
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(106, 446)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Only keep backups for "
        '
        'Label9
        '
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(322, 446)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 16)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "days"
        '
        'chkBackups
        '
        Me.chkBackups.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkBackups.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkBackups.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkBackups.Location = New System.Drawing.Point(15, 12)
        Me.chkBackups.Name = "chkBackups"
        Me.chkBackups.Size = New System.Drawing.Size(464, 24)
        Me.chkBackups.TabIndex = 0
        Me.chkBackups.Text = "Enable scheduled system backups"
        '
        'lsvDefer
        '
        Me.lsvDefer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvDefer.Border.Class = "ListViewBorder"
        Me.lsvDefer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDefer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader55, Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13, Me.ColumnHeader51, Me.ColumnHeader52, Me.ColumnHeader53, Me.ColumnHeader54})
        Me.lsvDefer.ContextMenu = Me.mnuDefer
        Me.lsvDefer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvDefer.ForeColor = System.Drawing.Color.Black
        Me.lsvDefer.FullRowSelect = True
        Me.lsvDefer.GridLines = True
        Me.lsvDefer.Location = New System.Drawing.Point(0, 59)
        Me.lsvDefer.Name = "lsvDefer"
        Me.lsvDefer.Size = New System.Drawing.Size(974, 595)
        Me.lsvDefer.TabIndex = 0
        Me.lsvDefer.UseCompatibleStateImageBehavior = False
        Me.lsvDefer.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader55
        '
        Me.ColumnHeader55.Text = "Schedule Name"
        Me.ColumnHeader55.Width = 140
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Report Name"
        Me.ColumnHeader11.Width = 140
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "File Path"
        Me.ColumnHeader12.Width = 323
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Due Date"
        Me.ColumnHeader13.Width = 126
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "Delivery To"
        Me.ColumnHeader51.Width = 160
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Cc"
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "Bcc"
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Subject"
        Me.ColumnHeader54.Width = 160
        '
        'FlowLayoutPanel9
        '
        Me.FlowLayoutPanel9.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel9.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel9.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
        Me.FlowLayoutPanel9.Size = New System.Drawing.Size(974, 59)
        Me.FlowLayoutPanel9.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.optDeferAsc)
        Me.GroupBox2.Controls.Add(Me.optDeferDesc)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(272, 48)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sort"
        '
        'optDeferAsc
        '
        Me.optDeferAsc.Checked = True
        Me.optDeferAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDeferAsc.Location = New System.Drawing.Point(16, 16)
        Me.optDeferAsc.Name = "optDeferAsc"
        Me.optDeferAsc.Size = New System.Drawing.Size(120, 24)
        Me.optDeferAsc.TabIndex = 1
        Me.optDeferAsc.TabStop = True
        Me.optDeferAsc.Tag = "ASC"
        Me.optDeferAsc.Text = "Ascending Order"
        '
        'optDeferDesc
        '
        Me.optDeferDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDeferDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDeferDesc.Name = "optDeferDesc"
        Me.optDeferDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDeferDesc.TabIndex = 0
        Me.optDeferDesc.Tag = "DESC"
        Me.optDeferDesc.Text = "Descending Order"
        '
        'btnCopy
        '
        Me.btnCopy.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCopy.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCopy.Location = New System.Drawing.Point(102, 471)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(75, 23)
        Me.btnCopy.TabIndex = 6
        Me.btnCopy.Text = "Copy Error"
        '
        'txterrSuggest
        '
        Me.txterrSuggest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrSuggest.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txterrSuggest.Border.Class = "TextBoxBorder"
        Me.txterrSuggest.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txterrSuggest.ForeColor = System.Drawing.Color.Black
        Me.txterrSuggest.Location = New System.Drawing.Point(102, 376)
        Me.txterrSuggest.Multiline = True
        Me.txterrSuggest.Name = "txterrSuggest"
        Me.txterrSuggest.Size = New System.Drawing.Size(504, 86)
        Me.txterrSuggest.TabIndex = 3
        '
        'txtErrorlog
        '
        Me.txtErrorlog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtErrorlog.BackColor = System.Drawing.SystemColors.ControlLightLight
        '
        '
        '
        Me.txtErrorlog.Border.Class = "TextBoxBorder"
        Me.txtErrorlog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtErrorlog.ForeColor = System.Drawing.Color.Black
        Me.txtErrorlog.Location = New System.Drawing.Point(102, 84)
        Me.txtErrorlog.Multiline = True
        Me.txtErrorlog.Name = "txtErrorlog"
        Me.txtErrorlog.Size = New System.Drawing.Size(504, 286)
        Me.txtErrorlog.TabIndex = 4
        '
        'txterrLine
        '
        Me.txterrLine.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrLine.BackColor = System.Drawing.SystemColors.ControlLightLight
        '
        '
        '
        Me.txterrLine.Border.Class = "TextBoxBorder"
        Me.txterrLine.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txterrLine.ForeColor = System.Drawing.Color.Black
        Me.txterrLine.Location = New System.Drawing.Point(102, 57)
        Me.txterrLine.Name = "txterrLine"
        Me.txterrLine.ReadOnly = True
        Me.txterrLine.Size = New System.Drawing.Size(504, 21)
        Me.txterrLine.TabIndex = 2
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        '
        '
        '
        Me.Label29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label29.Location = New System.Drawing.Point(3, 84)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(84, 16)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Error Description"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        '
        '
        '
        Me.Label28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label28.Location = New System.Drawing.Point(3, 376)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(56, 16)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Suggestion"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        '
        '
        '
        Me.Label26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label26.Location = New System.Drawing.Point(3, 57)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(64, 16)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Line Number"
        '
        'txterrSource
        '
        Me.txterrSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrSource.BackColor = System.Drawing.SystemColors.ControlLightLight
        '
        '
        '
        Me.txterrSource.Border.Class = "TextBoxBorder"
        Me.txterrSource.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txterrSource.ForeColor = System.Drawing.Color.Black
        Me.txterrSource.Location = New System.Drawing.Point(102, 30)
        Me.txterrSource.Name = "txterrSource"
        Me.txterrSource.ReadOnly = True
        Me.txterrSource.Size = New System.Drawing.Size(504, 21)
        Me.txterrSource.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        '
        '
        '
        Me.Label25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label25.Location = New System.Drawing.Point(3, 30)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(63, 16)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Error Source"
        '
        'txterrNo
        '
        Me.txterrNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrNo.BackColor = System.Drawing.SystemColors.ControlLightLight
        '
        '
        '
        Me.txterrNo.Border.Class = "TextBoxBorder"
        Me.txterrNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txterrNo.ForeColor = System.Drawing.Color.Black
        Me.txterrNo.Location = New System.Drawing.Point(102, 3)
        Me.txterrNo.Name = "txterrNo"
        Me.txterrNo.ReadOnly = True
        Me.txterrNo.Size = New System.Drawing.Size(504, 21)
        Me.txterrNo.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        '
        '
        '
        Me.Label24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label24.Location = New System.Drawing.Point(3, 3)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(69, 16)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Error Number"
        '
        'lsvErrorLog
        '
        Me.lsvErrorLog.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvErrorLog.Border.Class = "ListViewBorder"
        Me.lsvErrorLog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvErrorLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader42, Me.ColumnHeader40, Me.ColumnHeader41, Me.ColumnHeader43, Me.ColumnHeader44, Me.ColumnHeader45, Me.ColumnHeader46, Me.ColumnHeader47})
        Me.lsvErrorLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvErrorLog.ForeColor = System.Drawing.Color.Black
        Me.lsvErrorLog.FullRowSelect = True
        Me.lsvErrorLog.GridLines = True
        Me.lsvErrorLog.HideSelection = False
        Me.lsvErrorLog.Location = New System.Drawing.Point(0, 0)
        Me.lsvErrorLog.MultiSelect = False
        Me.lsvErrorLog.Name = "lsvErrorLog"
        Me.lsvErrorLog.Size = New System.Drawing.Size(321, 600)
        Me.lsvErrorLog.TabIndex = 0
        Me.lsvErrorLog.UseCompatibleStateImageBehavior = False
        Me.lsvErrorLog.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "PC Name"
        Me.ColumnHeader42.Width = 100
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Entry Date"
        Me.ColumnHeader40.Width = 100
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "Schedule Name"
        Me.ColumnHeader41.Width = 100
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = ""
        Me.ColumnHeader43.Width = 0
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = ""
        Me.ColumnHeader44.Width = 0
        '
        'ColumnHeader45
        '
        Me.ColumnHeader45.Text = ""
        Me.ColumnHeader45.Width = 0
        '
        'ColumnHeader46
        '
        Me.ColumnHeader46.Text = ""
        Me.ColumnHeader46.Width = 0
        '
        'ColumnHeader47
        '
        Me.ColumnHeader47.Text = ""
        Me.ColumnHeader47.Width = 0
        '
        'FlowLayoutPanel8
        '
        Me.FlowLayoutPanel8.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel8.Controls.Add(Me.cmdErrorLog)
        Me.FlowLayoutPanel8.Controls.Add(Me.TableLayoutPanel5)
        Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel8.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
        Me.FlowLayoutPanel8.Size = New System.Drawing.Size(974, 37)
        Me.FlowLayoutPanel8.TabIndex = 4
        '
        'cmdErrorLog
        '
        Me.cmdErrorLog.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdErrorLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdErrorLog.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdErrorLog.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdErrorLog.Location = New System.Drawing.Point(3, 3)
        Me.cmdErrorLog.Name = "cmdErrorLog"
        Me.cmdErrorLog.Size = New System.Drawing.Size(75, 29)
        Me.cmdErrorLog.TabIndex = 2
        Me.cmdErrorLog.Text = "Clear Log"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 6
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 161.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.Label27, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txterrSearch, 2, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Label30, 3, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtKeepErrorLogs, 4, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.Label31, 5, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(84, 3)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(584, 29)
        Me.TableLayoutPanel5.TabIndex = 3
        '
        'Label27
        '
        Me.Label27.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label27.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label27.Image = Global.sqlrd.My.Resources.Resources.view
        Me.Label27.Location = New System.Drawing.Point(3, 3)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(72, 23)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "Search"
        '
        'txterrSearch
        '
        Me.txterrSearch.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txterrSearch.Border.Class = "TextBoxBorder"
        Me.txterrSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txterrSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txterrSearch.ForeColor = System.Drawing.Color.Black
        Me.txterrSearch.Location = New System.Drawing.Point(81, 3)
        Me.txterrSearch.Name = "txterrSearch"
        Me.txterrSearch.Size = New System.Drawing.Size(161, 21)
        Me.txterrSearch.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label30.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label30.Location = New System.Drawing.Point(248, 3)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(94, 23)
        Me.Label30.TabIndex = 1
        Me.Label30.Text = "Keep entries for"
        '
        'txtKeepErrorLogs
        '
        Me.txtKeepErrorLogs.Location = New System.Drawing.Point(348, 3)
        Me.txtKeepErrorLogs.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtKeepErrorLogs.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtKeepErrorLogs.Name = "txtKeepErrorLogs"
        Me.txtKeepErrorLogs.Size = New System.Drawing.Size(72, 21)
        Me.txtKeepErrorLogs.TabIndex = 2
        Me.txtKeepErrorLogs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKeepErrorLogs.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'Label31
        '
        Me.Label31.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label31.Location = New System.Drawing.Point(426, 3)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(42, 23)
        Me.Label31.TabIndex = 1
        Me.Label31.Text = "days"
        '
        'lsvEmailQueue
        '
        Me.lsvEmailQueue.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvEmailQueue.Border.Class = "ListViewBorder"
        Me.lsvEmailQueue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvEmailQueue.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader5, Me.ColumnHeader8, Me.ColumnHeader6, Me.ColumnHeader10, Me.ColumnHeader9, Me.ColumnHeader37})
        Me.lsvEmailQueue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvEmailQueue.ForeColor = System.Drawing.Color.Black
        Me.lsvEmailQueue.FullRowSelect = True
        Me.lsvEmailQueue.GridLines = True
        Me.lsvEmailQueue.Location = New System.Drawing.Point(0, 63)
        Me.lsvEmailQueue.Name = "lsvEmailQueue"
        Me.lsvEmailQueue.Size = New System.Drawing.Size(974, 591)
        Me.lsvEmailQueue.TabIndex = 1
        Me.lsvEmailQueue.UseCompatibleStateImageBehavior = False
        Me.lsvEmailQueue.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Schedule Name"
        Me.ColumnHeader7.Width = 98
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Addresses"
        Me.ColumnHeader5.Width = 86
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "CC"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Subject"
        Me.ColumnHeader6.Width = 86
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Last Attempt"
        Me.ColumnHeader10.Width = 91
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Last Result"
        Me.ColumnHeader9.Width = 105
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Path"
        Me.ColumnHeader37.Width = 240
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel11.ColumnCount = 3
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel11.Controls.Add(Me.Label17, 0, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.txtDeleteOlder, 1, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.cmbValueUnit, 2, 0)
        Me.TableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(0, 32)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 1
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(974, 31)
        Me.TableLayoutPanel11.TabIndex = 8
        '
        'Label17
        '
        '
        '
        '
        Me.Label17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label17.Location = New System.Drawing.Point(3, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(244, 22)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Remove items from the queue that are older than"
        '
        'txtDeleteOlder
        '
        Me.txtDeleteOlder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtDeleteOlder.Location = New System.Drawing.Point(253, 3)
        Me.txtDeleteOlder.Name = "txtDeleteOlder"
        Me.txtDeleteOlder.Size = New System.Drawing.Size(68, 21)
        Me.txtDeleteOlder.TabIndex = 1
        Me.txtDeleteOlder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDeleteOlder.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'cmbValueUnit
        '
        Me.cmbValueUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValueUnit.FormattingEnabled = True
        Me.cmbValueUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days"})
        Me.cmbValueUnit.Location = New System.Drawing.Point(327, 3)
        Me.cmbValueUnit.Name = "cmbValueUnit"
        Me.cmbValueUnit.Size = New System.Drawing.Size(121, 21)
        Me.cmbValueUnit.TabIndex = 2
        Me.cmbValueUnit.Tag = "unsorted"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdSendNow)
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdRemoveOne)
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdRemoveAll)
        Me.FlowLayoutPanel5.Controls.Add(Me.chkOneEmailPerSchedule)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(974, 32)
        Me.FlowLayoutPanel5.TabIndex = 7
        '
        'cmdSendNow
        '
        Me.cmdSendNow.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSendNow.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSendNow.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSendNow.Location = New System.Drawing.Point(3, 3)
        Me.cmdSendNow.Name = "cmdSendNow"
        Me.cmdSendNow.Size = New System.Drawing.Size(104, 23)
        Me.cmdSendNow.TabIndex = 4
        Me.cmdSendNow.Text = "Send Now"
        '
        'cmdRemoveOne
        '
        Me.cmdRemoveOne.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveOne.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveOne.Location = New System.Drawing.Point(113, 3)
        Me.cmdRemoveOne.Name = "cmdRemoveOne"
        Me.cmdRemoveOne.Size = New System.Drawing.Size(104, 23)
        Me.cmdRemoveOne.TabIndex = 2
        Me.cmdRemoveOne.Text = "Remove Selected"
        '
        'cmdRemoveAll
        '
        Me.cmdRemoveAll.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveAll.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveAll.Location = New System.Drawing.Point(223, 3)
        Me.cmdRemoveAll.Name = "cmdRemoveAll"
        Me.cmdRemoveAll.Size = New System.Drawing.Size(104, 23)
        Me.cmdRemoveAll.TabIndex = 3
        Me.cmdRemoveAll.Text = "Remove All"
        '
        'chkOneEmailPerSchedule
        '
        Me.chkOneEmailPerSchedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkOneEmailPerSchedule.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkOneEmailPerSchedule.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOneEmailPerSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkOneEmailPerSchedule.Location = New System.Drawing.Point(333, 3)
        Me.chkOneEmailPerSchedule.Name = "chkOneEmailPerSchedule"
        Me.chkOneEmailPerSchedule.Size = New System.Drawing.Size(231, 24)
        Me.chkOneEmailPerSchedule.TabIndex = 5
        Me.chkOneEmailPerSchedule.Text = "Only send one email alert per email failure"
        '
        'dgvEmailLog
        '
        Me.dgvEmailLog.AllowUserToAddRows = False
        Me.dgvEmailLog.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue
        Me.dgvEmailLog.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvEmailLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvEmailLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmailLog.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvEmailLog.Location = New System.Drawing.Point(90, 339)
        Me.dgvEmailLog.Name = "dgvEmailLog"
        Me.dgvEmailLog.ReadOnly = True
        Me.dgvEmailLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmailLog.Size = New System.Drawing.Size(240, 150)
        Me.dgvEmailLog.TabIndex = 23
        Me.dgvEmailLog.VirtualMode = True
        Me.dgvEmailLog.Visible = False
        '
        'lsvLog
        '
        Me.lsvLog.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvLog.Border.Class = "ListViewBorder"
        Me.lsvLog.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader36})
        Me.lsvLog.ContextMenuStrip = Me.mnuClearEmailLog
        Me.lsvLog.ForeColor = System.Drawing.Color.Black
        Me.lsvLog.FullRowSelect = True
        Me.lsvLog.GridLines = True
        Me.lsvLog.HideSelection = False
        Me.lsvLog.Location = New System.Drawing.Point(162, 96)
        Me.lsvLog.Name = "lsvLog"
        Me.lsvLog.Size = New System.Drawing.Size(322, 213)
        Me.lsvLog.TabIndex = 0
        Me.lsvLog.UseCompatibleStateImageBehavior = False
        Me.lsvLog.View = System.Windows.Forms.View.Details
        Me.lsvLog.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 99
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Sent To"
        Me.ColumnHeader2.Width = 85
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Subject"
        Me.ColumnHeader3.Width = 172
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Date Sent"
        Me.ColumnHeader4.Width = 110
        '
        'ColumnHeader36
        '
        Me.ColumnHeader36.Text = "File Name"
        Me.ColumnHeader36.Width = 218
        '
        'mnuClearEmailLog
        '
        Me.mnuClearEmailLog.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.DeleteSelectedToolStripMenuItem, Me.DeleteAllToolStripMenuItem})
        Me.mnuClearEmailLog.Name = "mnuClearEmailLog"
        Me.mnuClearEmailLog.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuClearEmailLog.Size = New System.Drawing.Size(155, 70)
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.OpenToolStripMenuItem.Text = "&Open"
        Me.OpenToolStripMenuItem.Visible = False
        '
        'DeleteSelectedToolStripMenuItem
        '
        Me.DeleteSelectedToolStripMenuItem.Name = "DeleteSelectedToolStripMenuItem"
        Me.DeleteSelectedToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.DeleteSelectedToolStripMenuItem.Text = "Delete &Selected"
        '
        'DeleteAllToolStripMenuItem
        '
        Me.DeleteAllToolStripMenuItem.Name = "DeleteAllToolStripMenuItem"
        Me.DeleteAllToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.DeleteAllToolStripMenuItem.Text = "Delete &All"
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel8.ColumnCount = 6
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel8.Controls.Add(Me.btnRefreshEmailLog, 4, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.txtKeepLogs, 1, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Label20, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.txtSearch, 3, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Label23, 2, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.chkRefreshEmailLog, 5, 0)
        Me.TableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 1
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(974, 30)
        Me.TableLayoutPanel8.TabIndex = 22
        '
        'btnRefreshEmailLog
        '
        Me.btnRefreshEmailLog.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRefreshEmailLog.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRefreshEmailLog.Image = CType(resources.GetObject("btnRefreshEmailLog.Image"), System.Drawing.Image)
        Me.btnRefreshEmailLog.Location = New System.Drawing.Point(699, 3)
        Me.btnRefreshEmailLog.Name = "btnRefreshEmailLog"
        Me.btnRefreshEmailLog.Size = New System.Drawing.Size(43, 21)
        Me.btnRefreshEmailLog.TabIndex = 3
        Me.btnRefreshEmailLog.Visible = False
        '
        'txtKeepLogs
        '
        Me.txtKeepLogs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepLogs.Location = New System.Drawing.Point(134, 3)
        Me.txtKeepLogs.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepLogs.Name = "txtKeepLogs"
        Me.txtKeepLogs.Size = New System.Drawing.Size(56, 21)
        Me.txtKeepLogs.TabIndex = 18
        Me.txtKeepLogs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(3, 3)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(125, 21)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "Keep history for (days)"
        '
        'txtSearch
        '
        Me.txtSearch.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSearch.Border.Class = "TextBoxBorder"
        Me.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSearch.ForeColor = System.Drawing.Color.Black
        Me.txtSearch.Location = New System.Drawing.Point(261, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(432, 21)
        Me.txtSearch.TabIndex = 19
        '
        'Label23
        '
        Me.Label23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label23.Image = Global.sqlrd.My.Resources.Resources.view
        Me.Label23.Location = New System.Drawing.Point(196, 3)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(59, 30)
        Me.Label23.TabIndex = 20
        Me.Label23.Text = "Search"
        '
        'chkRefreshEmailLog
        '
        Me.chkRefreshEmailLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkRefreshEmailLog.AutoSize = True
        '
        '
        '
        Me.chkRefreshEmailLog.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRefreshEmailLog.Checked = True
        Me.chkRefreshEmailLog.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRefreshEmailLog.CheckValue = "Y"
        Me.chkRefreshEmailLog.Location = New System.Drawing.Point(748, 3)
        Me.chkRefreshEmailLog.Name = "chkRefreshEmailLog"
        Me.chkRefreshEmailLog.Size = New System.Drawing.Size(83, 16)
        Me.chkRefreshEmailLog.TabIndex = 21
        Me.chkRefreshEmailLog.Text = "Auto-refresh"
        '
        'txtSearchSchedules
        '
        Me.txtSearchSchedules.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSearchSchedules.Border.Class = "TextBoxBorder"
        Me.txtSearchSchedules.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSearchSchedules.ForeColor = System.Drawing.Color.Black
        Me.txtSearchSchedules.Location = New System.Drawing.Point(643, 5)
        Me.txtSearchSchedules.Name = "txtSearchSchedules"
        Me.txtSearchSchedules.Size = New System.Drawing.Size(165, 21)
        Me.txtSearchSchedules.TabIndex = 9
        '
        'slRefreshInterval
        '
        Me.slRefreshInterval.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.slRefreshInterval.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.slRefreshInterval.LabelVisible = False
        Me.slRefreshInterval.LabelWidth = 68
        Me.slRefreshInterval.Location = New System.Drawing.Point(334, 3)
        Me.slRefreshInterval.Maximum = 60
        Me.slRefreshInterval.Minimum = 2
        Me.slRefreshInterval.Name = "slRefreshInterval"
        Me.slRefreshInterval.Size = New System.Drawing.Size(263, 26)
        Me.slRefreshInterval.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.slRefreshInterval.TabIndex = 7
        Me.slRefreshInterval.Text = "Refresh Interval"
        Me.slRefreshInterval.Value = 2
        '
        'cmbFilter
        '
        Me.cmbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFilter.ItemHeight = 13
        Me.cmbFilter.Items.AddRange(New Object() {"None", "Due Today - All", "Due Today - Next 5 Minutes", "Due Today - Next 30 Minutes", "Due Today - Next 60 Minutes", "Due in Next 24 Hours"})
        Me.cmbFilter.Location = New System.Drawing.Point(32, 5)
        Me.cmbFilter.Name = "cmbFilter"
        Me.cmbFilter.Size = New System.Drawing.Size(202, 21)
        Me.cmbFilter.TabIndex = 3
        '
        'mnuProcMan
        '
        Me.mnuProcMan.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuProcProperties, Me.mnuProcLoc, Me.MenuItem16, Me.mnuProcEnd})
        '
        'mnuProcProperties
        '
        Me.mnuProcProperties.Index = 0
        Me.mnuProcProperties.Text = "Properties"
        '
        'mnuProcLoc
        '
        Me.mnuProcLoc.Index = 1
        Me.mnuProcLoc.Text = "Show in Explorer"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 2
        Me.MenuItem16.Text = "-"
        '
        'mnuProcEnd
        '
        Me.mnuProcEnd.Index = 3
        Me.mnuProcEnd.Text = "Cancel Execution"
        '
        'cmnuEPClear
        '
        Me.cmnuEPClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEPClearAll, Me.mnuEPClearSel})
        '
        'mnuEPClearAll
        '
        Me.mnuEPClearAll.Index = 0
        Me.mnuEPClearAll.Text = "Clear All"
        '
        'mnuEPClearSel
        '
        Me.mnuEPClearSel.Index = 1
        Me.mnuEPClearSel.Text = "Clear Selected"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.tvProcessing)
        Me.Panel1.Controls.Add(Me.ExpandableSplitter1)
        Me.Panel1.Controls.Add(Me.lsvTaskManager)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 74)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(962, 580)
        Me.Panel1.TabIndex = 8
        '
        'tvProcessing
        '
        Me.tvProcessing.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvProcessing.AllowDrop = True
        Me.tvProcessing.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvProcessing.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvProcessing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvProcessing.Columns.Add(Me.ColumnHeader19)
        Me.tvProcessing.Columns.Add(Me.ColumnHeader20)
        Me.tvProcessing.Columns.Add(Me.ColumnHeader38)
        Me.tvProcessing.Columns.Add(Me.ColumnHeader39)
        Me.tvProcessing.Columns.Add(Me.ColumnHeader57)
        Me.tvProcessing.Columns.Add(Me.ColumnHeader58)
        Me.tvProcessing.Controls.Add(Me.Panel3)
        Me.tvProcessing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvProcessing.DragDropEnabled = False
        Me.tvProcessing.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvProcessing.Location = New System.Drawing.Point(325, 0)
        Me.tvProcessing.MultiSelect = True
        Me.tvProcessing.Name = "tvProcessing"
        Me.tvProcessing.NodesConnector = Me.NodeConnector2
        Me.tvProcessing.NodeStyle = Me.ElementStyle2
        Me.tvProcessing.PathSeparator = ";"
        Me.tvProcessing.Size = New System.Drawing.Size(637, 580)
        Me.tvProcessing.Styles.Add(Me.ElementStyle2)
        Me.tvProcessing.TabIndex = 9
        Me.tvProcessing.Text = "AdvTree1"
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Name = "ColumnHeader19"
        Me.ColumnHeader19.Text = "Name"
        Me.ColumnHeader19.Width.Absolute = 150
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Name = "ColumnHeader20"
        Me.ColumnHeader20.Text = "Started at"
        Me.ColumnHeader20.Width.Absolute = 150
        '
        'ColumnHeader38
        '
        Me.ColumnHeader38.Name = "ColumnHeader38"
        Me.ColumnHeader38.Text = "Server Name"
        Me.ColumnHeader38.Width.Absolute = 150
        '
        'ColumnHeader39
        '
        Me.ColumnHeader39.Name = "ColumnHeader39"
        Me.ColumnHeader39.Text = "Status"
        Me.ColumnHeader39.Width.Absolute = 150
        '
        'ColumnHeader57
        '
        Me.ColumnHeader57.Name = "ColumnHeader57"
        Me.ColumnHeader57.Text = "Process ID"
        Me.ColumnHeader57.Width.Absolute = 150
        '
        'ColumnHeader58
        '
        Me.ColumnHeader58.Name = "ColumnHeader58"
        Me.ColumnHeader58.Text = "Duration"
        Me.ColumnHeader58.Width.Absolute = 150
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.GroupPanel1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 441)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(637, 139)
        Me.Panel3.TabIndex = 1
        '
        'GroupPanel1
        '
        Me.GroupPanel1.BackColor = System.Drawing.Color.White
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.GroupPanel1.Controls.Add(Me.advProp)
        Me.GroupPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupPanel1.Location = New System.Drawing.Point(0, 0)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(637, 139)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorGradientType = DevComponents.DotNetBar.eGradientType.Radial
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 0
        '
        'advProp
        '
        Me.advProp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.advProp.GridLinesColor = System.Drawing.Color.WhiteSmoke
        Me.advProp.Location = New System.Drawing.Point(0, 0)
        Me.advProp.Name = "advProp"
        Me.advProp.Size = New System.Drawing.Size(635, 137)
        Me.advProp.TabIndex = 0
        Me.advProp.Text = "AdvPropertyGrid1"
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.Color.FromArgb(CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer), CType(CType(242, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.ForeColor = System.Drawing.Color.Black
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.White
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(135, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.White
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(321, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(4, 580)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Office2007
        Me.ExpandableSplitter1.TabIndex = 7
        Me.ExpandableSplitter1.TabStop = False
        '
        'lsvTaskManager
        '
        Me.lsvTaskManager.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.lsvTaskManager.AllowDrop = True
        Me.lsvTaskManager.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.lsvTaskManager.BackgroundStyle.Class = "TreeBorderKey"
        Me.lsvTaskManager.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvTaskManager.Columns.Add(Me.ColumnHeader17)
        Me.lsvTaskManager.Columns.Add(Me.ColumnHeader18)
        Me.lsvTaskManager.Dock = System.Windows.Forms.DockStyle.Left
        Me.lsvTaskManager.DragDropEnabled = False
        Me.lsvTaskManager.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.lsvTaskManager.Location = New System.Drawing.Point(0, 0)
        Me.lsvTaskManager.MultiSelect = True
        Me.lsvTaskManager.Name = "lsvTaskManager"
        Me.lsvTaskManager.NodesConnector = Me.NodeConnector1
        Me.lsvTaskManager.NodeStyle = Me.ElementStyle1
        Me.lsvTaskManager.PathSeparator = ";"
        Me.lsvTaskManager.Size = New System.Drawing.Size(321, 580)
        Me.lsvTaskManager.Styles.Add(Me.ElementStyle1)
        Me.lsvTaskManager.TabIndex = 8
        Me.lsvTaskManager.Text = "AdvTree1"
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Editable = False
        Me.ColumnHeader17.Name = "ColumnHeader17"
        Me.ColumnHeader17.Text = "Name"
        Me.ColumnHeader17.Width.Absolute = 150
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Editable = False
        Me.ColumnHeader18.Name = "ColumnHeader18"
        Me.ColumnHeader18.Text = "Due to Run"
        Me.ColumnHeader18.Width.Absolute = 150
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'pnlErrorLog
        '
        Me.pnlErrorLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.pnlErrorLog.BackColor = System.Drawing.Color.Transparent
        Me.pnlErrorLog.ColumnCount = 2
        Me.pnlErrorLog.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.30094!))
        Me.pnlErrorLog.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.69906!))
        Me.pnlErrorLog.Controls.Add(Me.Label24, 0, 0)
        Me.pnlErrorLog.Controls.Add(Me.txterrNo, 1, 0)
        Me.pnlErrorLog.Controls.Add(Me.txtErrorlog, 1, 3)
        Me.pnlErrorLog.Controls.Add(Me.btnCopy, 1, 5)
        Me.pnlErrorLog.Controls.Add(Me.Label25, 0, 1)
        Me.pnlErrorLog.Controls.Add(Me.txterrSource, 1, 1)
        Me.pnlErrorLog.Controls.Add(Me.Label26, 0, 2)
        Me.pnlErrorLog.Controls.Add(Me.txterrLine, 1, 2)
        Me.pnlErrorLog.Controls.Add(Me.Label29, 0, 3)
        Me.pnlErrorLog.Controls.Add(Me.Label28, 0, 4)
        Me.pnlErrorLog.Controls.Add(Me.txterrSuggest, 1, 4)
        Me.pnlErrorLog.Location = New System.Drawing.Point(325, 37)
        Me.pnlErrorLog.Name = "pnlErrorLog"
        Me.pnlErrorLog.RowCount = 6
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.pnlErrorLog.Size = New System.Drawing.Size(609, 497)
        Me.pnlErrorLog.TabIndex = 8
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.btnMoreHistoryData)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnPurgeHistory)
        Me.FlowLayoutPanel3.Controls.Add(Me.txtKeepScheduleHistory)
        Me.FlowLayoutPanel3.Controls.Add(Me.LabelX1)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 625)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(974, 29)
        Me.FlowLayoutPanel3.TabIndex = 2
        '
        'btnMoreHistoryData
        '
        Me.btnMoreHistoryData.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnMoreHistoryData.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnMoreHistoryData.Location = New System.Drawing.Point(896, 3)
        Me.btnMoreHistoryData.Name = "btnMoreHistoryData"
        Me.btnMoreHistoryData.Size = New System.Drawing.Size(75, 23)
        Me.btnMoreHistoryData.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnMoreHistoryData.TabIndex = 0
        Me.btnMoreHistoryData.Text = "More Data..."
        '
        'btnPurgeHistory
        '
        Me.btnPurgeHistory.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPurgeHistory.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPurgeHistory.Location = New System.Drawing.Point(815, 3)
        Me.btnPurgeHistory.Name = "btnPurgeHistory"
        Me.btnPurgeHistory.Size = New System.Drawing.Size(75, 23)
        Me.btnPurgeHistory.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPurgeHistory.TabIndex = 3
        Me.btnPurgeHistory.Text = "Purge Now"
        '
        'txtKeepScheduleHistory
        '
        Me.txtKeepScheduleHistory.Location = New System.Drawing.Point(755, 3)
        Me.txtKeepScheduleHistory.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtKeepScheduleHistory.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtKeepScheduleHistory.Name = "txtKeepScheduleHistory"
        Me.txtKeepScheduleHistory.Size = New System.Drawing.Size(54, 21)
        Me.txtKeepScheduleHistory.TabIndex = 2
        Me.txtKeepScheduleHistory.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKeepScheduleHistory.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(557, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(192, 23)
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = "Clear history thats older than (days)"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.optReportHistory)
        Me.FlowLayoutPanel1.Controls.Add(Me.optPackageHistory)
        Me.FlowLayoutPanel1.Controls.Add(Me.optAutomationHistory)
        Me.FlowLayoutPanel1.Controls.Add(Me.optEventHistory)
        Me.FlowLayoutPanel1.Controls.Add(Me.optEventPackageHistory)
        Me.FlowLayoutPanel1.Controls.Add(Me.optSmartFolderHistory)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(974, 32)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'optReportHistory
        '
        Me.optReportHistory.AutoSize = True
        Me.optReportHistory.Location = New System.Drawing.Point(3, 3)
        Me.optReportHistory.Name = "optReportHistory"
        Me.optReportHistory.Size = New System.Drawing.Size(109, 17)
        Me.optReportHistory.TabIndex = 0
        Me.optReportHistory.Text = "Report Schedules"
        Me.optReportHistory.UseVisualStyleBackColor = True
        '
        'optPackageHistory
        '
        Me.optPackageHistory.AutoSize = True
        Me.optPackageHistory.Location = New System.Drawing.Point(118, 3)
        Me.optPackageHistory.Name = "optPackageHistory"
        Me.optPackageHistory.Size = New System.Drawing.Size(116, 17)
        Me.optPackageHistory.TabIndex = 0
        Me.optPackageHistory.Text = "Package Schedules"
        Me.optPackageHistory.UseVisualStyleBackColor = True
        '
        'optAutomationHistory
        '
        Me.optAutomationHistory.AutoSize = True
        Me.optAutomationHistory.Location = New System.Drawing.Point(240, 3)
        Me.optAutomationHistory.Name = "optAutomationHistory"
        Me.optAutomationHistory.Size = New System.Drawing.Size(131, 17)
        Me.optAutomationHistory.TabIndex = 0
        Me.optAutomationHistory.Text = "Automation Schedules"
        Me.optAutomationHistory.UseVisualStyleBackColor = True
        '
        'optEventHistory
        '
        Me.optEventHistory.AutoSize = True
        Me.optEventHistory.Location = New System.Drawing.Point(377, 3)
        Me.optEventHistory.Name = "optEventHistory"
        Me.optEventHistory.Size = New System.Drawing.Size(137, 17)
        Me.optEventHistory.TabIndex = 0
        Me.optEventHistory.Text = "Event-Based Schedules"
        Me.optEventHistory.UseVisualStyleBackColor = True
        '
        'optEventPackageHistory
        '
        Me.optEventPackageHistory.AutoSize = True
        Me.optEventPackageHistory.Location = New System.Drawing.Point(520, 3)
        Me.optEventPackageHistory.Name = "optEventPackageHistory"
        Me.optEventPackageHistory.Size = New System.Drawing.Size(134, 17)
        Me.optEventPackageHistory.TabIndex = 0
        Me.optEventPackageHistory.Text = "Event-Based Packages"
        Me.optEventPackageHistory.UseVisualStyleBackColor = True
        '
        'optSmartFolderHistory
        '
        Me.optSmartFolderHistory.AutoSize = True
        Me.optSmartFolderHistory.Location = New System.Drawing.Point(660, 3)
        Me.optSmartFolderHistory.Name = "optSmartFolderHistory"
        Me.optSmartFolderHistory.Size = New System.Drawing.Size(91, 17)
        Me.optSmartFolderHistory.TabIndex = 0
        Me.optSmartFolderHistory.Text = "Smart Folders"
        Me.optSmartFolderHistory.UseVisualStyleBackColor = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem4, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.MenuItem5, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem8})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 5
        Me.MenuItem5.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 9
        Me.MenuItem8.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem9, Me.MenuItem10, Me.mnuDatabase})
        Me.MenuItem8.Text = "Insert"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 0
        Me.MenuItem9.Text = "Constants"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 1
        Me.MenuItem10.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'tmScheduleMonCleaner
        '
        Me.tmScheduleMonCleaner.Enabled = True
        Me.tmScheduleMonCleaner.Interval = 500.0R
        Me.tmScheduleMonCleaner.SynchronizingObject = Me
        '
        'bgEmailLog
        '
        '
        'tmEmailLog
        '
        Me.tmEmailLog.Interval = 30000.0R
        Me.tmEmailLog.SynchronizingObject = Me
        '
        'stabSystemMonitor
        '
        Me.stabSystemMonitor.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabSystemMonitor.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabSystemMonitor.ControlBox.MenuBox.Name = ""
        Me.stabSystemMonitor.ControlBox.Name = ""
        Me.stabSystemMonitor.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSystemMonitor.ControlBox.MenuBox, Me.stabSystemMonitor.ControlBox.CloseBox})
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel10)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel9)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel11)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabSystemMonitor.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabSystemMonitor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabSystemMonitor.ForeColor = System.Drawing.Color.Black
        Me.stabSystemMonitor.Location = New System.Drawing.Point(0, 0)
        Me.stabSystemMonitor.Name = "stabSystemMonitor"
        Me.stabSystemMonitor.ReorderTabsEnabled = True
        Me.stabSystemMonitor.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabSystemMonitor.SelectedTabIndex = 0
        Me.stabSystemMonitor.ShowFocusRectangle = True
        Me.stabSystemMonitor.Size = New System.Drawing.Size(1115, 654)
        Me.stabSystemMonitor.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabSystemMonitor.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabSystemMonitor.TabIndex = 4
        Me.stabSystemMonitor.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabScheduleManager, Me.stabEmailLog, Me.stabEmailQueue, Me.stabErrorLog, Me.stabDefferedDelivery, Me.stabBackup, Me.stabScheduleRefresh, Me.stabAuditTrail, Me.stabDeferredDelivery, Me.stabScheduleHistory, Me.stabScheduleRetries})
        Me.stabSystemMonitor.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabSystemMonitor.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.dgvEmailLog)
        Me.SuperTabControlPanel2.Controls.Add(Me.TableLayoutPanel8)
        Me.SuperTabControlPanel2.Controls.Add(Me.lsvLog)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.stabEmailLog
        '
        'stabEmailLog
        '
        Me.stabEmailLog.AttachedControl = Me.SuperTabControlPanel2
        Me.stabEmailLog.GlobalItem = False
        Me.stabEmailLog.Image = CType(resources.GetObject("stabEmailLog.Image"), System.Drawing.Image)
        Me.stabEmailLog.Name = "stabEmailLog"
        Me.stabEmailLog.Text = "Email Log"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Panel1)
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel1.Controls.Add(Me.RibbonBar1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(153, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(962, 654)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.stabScheduleManager
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LabelX3, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 42)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(962, 32)
        Me.TableLayoutPanel1.TabIndex = 11
        '
        'LabelX2
        '
        Me.LabelX2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 3)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(316, 26)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Waiting to Execute"
        Me.LabelX2.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'LabelX3
        '
        Me.LabelX3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(325, 3)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(879, 26)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Currently Executing"
        Me.LabelX3.TextAlignment = System.Drawing.StringAlignment.Center
        '
        'RibbonBar1
        '
        Me.RibbonBar1.AutoOverflowEnabled = True
        '
        '
        '
        Me.RibbonBar1.BackgroundMouseOverStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.RibbonBar1.ContainerControlProcessDialogKey = True
        Me.RibbonBar1.Controls.Add(Me.cmbFilter)
        Me.RibbonBar1.Controls.Add(Me.slRefreshInterval)
        Me.RibbonBar1.Controls.Add(Me.txtSearchSchedules)
        Me.RibbonBar1.Dock = System.Windows.Forms.DockStyle.Top
        Me.RibbonBar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1, Me.ControlContainerItem1, Me.LabelItem3, Me.ControlContainerItem2, Me.LabelItem2, Me.ControlContainerItem3})
        Me.RibbonBar1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.RibbonBar1.Location = New System.Drawing.Point(0, 0)
        Me.RibbonBar1.Name = "RibbonBar1"
        Me.RibbonBar1.Size = New System.Drawing.Size(962, 42)
        Me.RibbonBar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.RibbonBar1.TabIndex = 10
        '
        '
        '
        Me.RibbonBar1.TitleStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.RibbonBar1.TitleStyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        'LabelItem1
        '
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Text = "Filter"
        '
        'ControlContainerItem1
        '
        Me.ControlContainerItem1.AllowItemResize = False
        Me.ControlContainerItem1.Control = Me.cmbFilter
        Me.ControlContainerItem1.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem1.Name = "ControlContainerItem1"
        '
        'LabelItem3
        '
        Me.LabelItem3.BeginGroup = True
        Me.LabelItem3.Name = "LabelItem3"
        Me.LabelItem3.Text = "    Refresh Interval"
        '
        'ControlContainerItem2
        '
        Me.ControlContainerItem2.AllowItemResize = False
        Me.ControlContainerItem2.Control = Me.slRefreshInterval
        Me.ControlContainerItem2.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem2.Name = "ControlContainerItem2"
        '
        'LabelItem2
        '
        Me.LabelItem2.BeginGroup = True
        Me.LabelItem2.Image = CType(resources.GetObject("LabelItem2.Image"), System.Drawing.Image)
        Me.LabelItem2.ImagePosition = DevComponents.DotNetBar.eImagePosition.Right
        Me.LabelItem2.Name = "LabelItem2"
        Me.LabelItem2.Text = "    "
        '
        'ControlContainerItem3
        '
        Me.ControlContainerItem3.AllowItemResize = False
        Me.ControlContainerItem3.Control = Me.txtSearchSchedules
        Me.ControlContainerItem3.MenuVisibility = DevComponents.DotNetBar.eMenuVisibility.VisibleAlways
        Me.ControlContainerItem3.Name = "ControlContainerItem3"
        '
        'stabScheduleManager
        '
        Me.stabScheduleManager.AttachedControl = Me.SuperTabControlPanel1
        Me.stabScheduleManager.GlobalItem = False
        Me.stabScheduleManager.Image = CType(resources.GetObject("stabScheduleManager.Image"), System.Drawing.Image)
        Me.stabScheduleManager.ItemAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        Me.stabScheduleManager.Name = "stabScheduleManager"
        Me.stabScheduleManager.Text = "Schedule Manager"
        Me.stabScheduleManager.TextAlignment = DevComponents.DotNetBar.eItemAlignment.Center
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.grpBackup)
        Me.SuperTabControlPanel6.Controls.Add(Me.chkBackups)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.stabBackup
        '
        'stabBackup
        '
        Me.stabBackup.AttachedControl = Me.SuperTabControlPanel6
        Me.stabBackup.GlobalItem = False
        Me.stabBackup.Image = CType(resources.GetObject("stabBackup.Image"), System.Drawing.Image)
        Me.stabBackup.Name = "stabBackup"
        Me.stabBackup.Text = "Schedule Backup"
        '
        'SuperTabControlPanel10
        '
        Me.SuperTabControlPanel10.Controls.Add(Me.historyViewer)
        Me.SuperTabControlPanel10.Controls.Add(Me.FlowLayoutPanel1)
        Me.SuperTabControlPanel10.Controls.Add(Me.FlowLayoutPanel3)
        Me.SuperTabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel10.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel10.Name = "SuperTabControlPanel10"
        Me.SuperTabControlPanel10.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel10.TabIndex = 0
        Me.SuperTabControlPanel10.TabItem = Me.stabScheduleHistory
        '
        'historyViewer
        '
        Me.historyViewer.BackColor = System.Drawing.Color.Transparent
        Me.historyViewer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.historyViewer.Location = New System.Drawing.Point(0, 32)
        Me.historyViewer.m_filter = False
        Me.historyViewer.m_historyLimit = 0
        Me.historyViewer.m_objectID = 0
        Me.historyViewer.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.NONE
        Me.historyViewer.m_showControls = False
        Me.historyViewer.m_showSchedulesList = True
        Me.historyViewer.m_sortOrder = " ASC "
        Me.historyViewer.Name = "historyViewer"
        Me.historyViewer.Size = New System.Drawing.Size(974, 593)
        Me.historyViewer.TabIndex = 3
        '
        'stabScheduleHistory
        '
        Me.stabScheduleHistory.AttachedControl = Me.SuperTabControlPanel10
        Me.stabScheduleHistory.GlobalItem = False
        Me.stabScheduleHistory.Image = CType(resources.GetObject("stabScheduleHistory.Image"), System.Drawing.Image)
        Me.stabScheduleHistory.Name = "stabScheduleHistory"
        Me.stabScheduleHistory.Text = "History View"
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.lsvSchedules)
        Me.SuperTabControlPanel7.Controls.Add(Me.TableLayoutPanel9)
        Me.SuperTabControlPanel7.Controls.Add(Me.FlowLayoutPanel11)
        Me.SuperTabControlPanel7.Controls.Add(Me.FlowLayoutPanel10)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.stabScheduleRefresh
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel11.Controls.Add(Me.GroupBox5)
        Me.FlowLayoutPanel11.Controls.Add(Me.GroupBox4)
        Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Left
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(0, 69)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(307, 585)
        Me.FlowLayoutPanel11.TabIndex = 11
        '
        'stabScheduleRefresh
        '
        Me.stabScheduleRefresh.AttachedControl = Me.SuperTabControlPanel7
        Me.stabScheduleRefresh.GlobalItem = False
        Me.stabScheduleRefresh.Image = CType(resources.GetObject("stabScheduleRefresh.Image"), System.Drawing.Image)
        Me.stabScheduleRefresh.Name = "stabScheduleRefresh"
        Me.stabScheduleRefresh.Text = "Schedule Refresh"
        Me.stabScheduleRefresh.Visible = False
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Panel2)
        Me.SuperTabControlPanel4.Controls.Add(Me.pnlErrorLog)
        Me.SuperTabControlPanel4.Controls.Add(Me.FlowLayoutPanel8)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.stabErrorLog
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lsvErrorLog)
        Me.Panel2.Controls.Add(Me.pageNav)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel2.Location = New System.Drawing.Point(0, 37)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(321, 617)
        Me.Panel2.TabIndex = 9
        '
        'pageNav
        '
        '
        '
        '
        Me.pageNav.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pageNav.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pageNav.Location = New System.Drawing.Point(0, 600)
        Me.pageNav.Name = "pageNav"
        Me.pageNav.Size = New System.Drawing.Size(321, 17)
        Me.pageNav.TabIndex = 1
        Me.pageNav.Text = "PageNavigator1"
        '
        'stabErrorLog
        '
        Me.stabErrorLog.AttachedControl = Me.SuperTabControlPanel4
        Me.stabErrorLog.GlobalItem = False
        Me.stabErrorLog.Image = CType(resources.GetObject("stabErrorLog.Image"), System.Drawing.Image)
        Me.stabErrorLog.Name = "stabErrorLog"
        Me.stabErrorLog.Text = "Error Log"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.lsvDefer)
        Me.SuperTabControlPanel5.Controls.Add(Me.FlowLayoutPanel9)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.stabDefferedDelivery
        '
        'stabDefferedDelivery
        '
        Me.stabDefferedDelivery.AttachedControl = Me.SuperTabControlPanel5
        Me.stabDefferedDelivery.GlobalItem = False
        Me.stabDefferedDelivery.Image = CType(resources.GetObject("stabDefferedDelivery.Image"), System.Drawing.Image)
        Me.stabDefferedDelivery.Name = "stabDefferedDelivery"
        Me.stabDefferedDelivery.Text = "Deferred Delivery"
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.lsvReadReciepts)
        Me.SuperTabControlPanel9.Controls.Add(Me.TableLayoutPanel10)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.stabDeferredDelivery
        '
        'stabDeferredDelivery
        '
        Me.stabDeferredDelivery.AttachedControl = Me.SuperTabControlPanel9
        Me.stabDeferredDelivery.GlobalItem = False
        Me.stabDeferredDelivery.Image = CType(resources.GetObject("stabDeferredDelivery.Image"), System.Drawing.Image)
        Me.stabDeferredDelivery.Name = "stabDeferredDelivery"
        Me.stabDeferredDelivery.Text = "Read Receipts"
        '
        'SuperTabControlPanel11
        '
        Me.SuperTabControlPanel11.Controls.Add(Me.lsvRetry)
        Me.SuperTabControlPanel11.Controls.Add(Me.TableLayoutPanel14)
        Me.SuperTabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel11.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel11.Name = "SuperTabControlPanel11"
        Me.SuperTabControlPanel11.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel11.TabIndex = 0
        Me.SuperTabControlPanel11.TabItem = Me.stabScheduleRetries
        '
        'stabScheduleRetries
        '
        Me.stabScheduleRetries.AttachedControl = Me.SuperTabControlPanel11
        Me.stabScheduleRetries.GlobalItem = False
        Me.stabScheduleRetries.Image = CType(resources.GetObject("stabScheduleRetries.Image"), System.Drawing.Image)
        Me.stabScheduleRetries.Name = "stabScheduleRetries"
        Me.stabScheduleRetries.Text = "Retry View"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.lsvEmailQueue)
        Me.SuperTabControlPanel3.Controls.Add(Me.TableLayoutPanel11)
        Me.SuperTabControlPanel3.Controls.Add(Me.FlowLayoutPanel5)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.stabEmailQueue
        '
        'stabEmailQueue
        '
        Me.stabEmailQueue.AttachedControl = Me.SuperTabControlPanel3
        Me.stabEmailQueue.GlobalItem = False
        Me.stabEmailQueue.Image = CType(resources.GetObject("stabEmailQueue.Image"), System.Drawing.Image)
        Me.stabEmailQueue.Name = "stabEmailQueue"
        Me.stabEmailQueue.Text = "Email Queue"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.lsvAudit)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(974, 654)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.stabAuditTrail
        '
        'stabAuditTrail
        '
        Me.stabAuditTrail.AttachedControl = Me.SuperTabControlPanel8
        Me.stabAuditTrail.GlobalItem = False
        Me.stabAuditTrail.Image = CType(resources.GetObject("stabAuditTrail.Image"), System.Drawing.Image)
        Me.stabAuditTrail.Name = "stabAuditTrail"
        Me.stabAuditTrail.Text = "Audit Trail"
        '
        'frmSystemMonitor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(1115, 654)
        Me.Controls.Add(Me.stabSystemMonitor)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(1131, 0)
        Me.Name = "frmSystemMonitor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD System Monitor"
        Me.muRR.ResumeLayout(False)
        CType(Me.tmScheduleMon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEmailQueue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel12.ResumeLayout(False)
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.txtKeepPack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.grpPackageSort.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.mnuRetryTracker.ResumeLayout(False)
        Me.TableLayoutPanel14.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.tvObjects, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtzRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel10.ResumeLayout(False)
        Me.grpBackup.ResumeLayout(False)
        Me.grpBackup.PerformLayout()
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        Me.grpSimple.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.txtInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKeep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel9.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.FlowLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        CType(Me.txtKeepErrorLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel11.ResumeLayout(False)
        CType(Me.txtDeleteOlder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        CType(Me.dgvEmailLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuClearEmailLog.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        CType(Me.txtKeepLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.tvProcessing, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tvProcessing.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupPanel1.ResumeLayout(False)
        CType(Me.advProp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.lsvTaskManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlErrorLog.ResumeLayout(False)
        Me.pnlErrorLog.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        CType(Me.txtKeepScheduleHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.tmScheduleMonCleaner, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEmailLog, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabSystemMonitor, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabSystemMonitor.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.RibbonBar1.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel10.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.FlowLayoutPanel11.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.SuperTabControlPanel11.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub logDataFromClient(ByVal message As String, pid As Integer)
        '//  .Add("ProcessID")
        '// .Add("Status")

        Dim rows() As DataRow = Me.progressTable.Select("processid =" & pid)

        For Each row As DataRow In rows
            row("Status") = message
        Next

    End Sub

    Public Sub ListenForMessages()
        Try
            Dim messageQ As ChristianStevenMessaging.messageQ

            Try
                messageQ = New ChristianStevenMessaging.messageQ("progress", False)
            Catch
                Return
            End Try

            Dim unreadMessages As ArrayList = messageQ.getUnreadMessages()


            For Each msg As ChristianStevenMessaging.message In unreadMessages

                Dim ID As String = msg.messageBody.Split("|")(0)
                Dim message As String = msg.messageBody.Split("|")(1)
                Dim value As String = msg.messageBody.Split("|")(2)
                Dim processID As Integer = msg.messageBody.Split("|")(3)

                message = message & " (" & value & "%)"

                updateCell(message, processID)

                msg.deleteMessage()

                msg.dispose()

                Threading.Thread.Sleep(100)

                ''we also update or insert the progressid in the progressTable data table
                'Dim rows() As DataRow = Me.progressTable.Select("processid =" & processID)

                'If rows IsNot Nothing Then
                '    If rows.Length = 0 Then
                '        Dim row As DataRow = Me.progressTable.Rows.Add

                '        row("processid") = processID
                '        row("status") = message
                '    Else
                '        Dim row As DataRow = rows(0)

                '        row("status") = message
                '    End If
                'End If

                'clean up dead processes
                Try
                    For Each row As DataRow In Me.progressTable.Rows
                        Dim procID As Integer = row("processid")

                        Try
                            Dim proc As Process = Process.GetProcessById(procID)
                        Catch ex As Exception
                            row.Delete()
                        End Try
                    Next
                Catch : End Try

                '  logDataFromClient(message, processID)

                ''  msg.deleteMessage()

                '   msg.dispose()

                '  System.Threading.Thread.Sleep(250)
            Next

            For Each msg As ChristianStevenMessaging.message In messageQ.getReadMessages
                msg.deleteMessage()
            Next

            ' System.Threading.Thread.Sleep(250)

        Catch : End Try
    End Sub
    Public Sub Listen()

        Try
            tcpListener.Start()
        Catch
            Return
        End Try

        'Accept the pending client connection and return             'a TcpClient initialized for communication. 
        Dim tcpClient As TcpClient

        Try
            tcpClient = tcpListener.AcceptTcpClient()
        Catch
            Return
        End Try

        Try

            Dim networkStream As NetworkStream = tcpClient.GetStream()

            ' Read the stream into a byte array
            Dim bytes(tcpClient.ReceiveBufferSize) As Byte

            networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))

            Dim clientdata As String = Encoding.ASCII.GetString(bytes)

            'Close TcpListener and TcpClient.
            logDataFromClient(clientdata)

            tcpClient.Close()
            tcpListener.Stop()

            Dim recycledThread As Boolean = False

            For Each t As Threading.Thread In oTs
                If t.IsAlive = False Then
                    recycledThread = True
                    t = New Threading.Thread(AddressOf Listen)
                    t.Start()
                    Exit For
                End If
            Next

            If recycledThread = False Then
                Dim oT As New Threading.Thread(AddressOf Listen)

                oT.Start()

                oTs.Add(oT)
            End If
        Catch : End Try

    End Sub

    Private Sub logDataFromClient(ByVal data As String)
        Try
            Dim sID As String = data.Split("|")(0) ' item ID
            Dim msg As String = data.Split("|")(1) ' status
            Dim value As Integer = data.Split("|")(2) ' progress amount 
            Dim processID As Integer = data.Split("|")(3) ' the process id 
            Dim itemFound As Boolean = False


            Dim sType As String = sID.Split(":")(0)
            Dim nID As Integer = sID.Split(":")(1)
            Dim SQL As String
            Dim sWhere As String = ""
            Dim sMsg As String = msg & " (" & value & "%)"



            'we also update or insert the progressid in the progressTable data table
            Dim rows() As DataRow = Me.progressTable.Select("processid =" & processID)

            If rows IsNot Nothing Then
                If rows.Length = 0 Then
                    Dim row As DataRow = Me.progressTable.Rows.Add

                    row("processid") = processID
                    row("status") = sMsg
                Else
                    Dim row As DataRow = rows(0)

                    row("status") = sMsg
                End If
            End If

            'clean up dead processes
            Try
                For Each row As DataRow In Me.progressTable.Rows
                    Dim procID As Integer = row("processid")

                    Try
                        Dim proc As Process = Process.GetProcessById(procID)
                    Catch ex As Exception
                        row.Delete()
                    End Try
                Next
            Catch : End Try


        Catch : End Try
    End Sub

    Private Function GetProcessID(ByVal nID As Integer, ByVal nType As clsMarsScheduler.enScheduleType) As Integer
        Dim column As String = ""

        Select Case nType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                column = "AutoID"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                column = "EventID"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                column = "EventPackID"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                column = "PackID"
            Case clsMarsScheduler.enScheduleType.REPORT
                column = "ReportID"
            Case Else
                Return 0
        End Select

        Dim SQL As String = "SELECT ProcessID FROM ThreadManager WHERE " & column & " = " & nID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return 0

        If oRs.EOF = False Then
            Dim processID As Integer = oRs(0).Value
            oRs.Close()
            Return processID
        Else
            oRs.Close()
            Return 0
        End If

    End Function

    Private Sub frmSystemMonitor_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Enter


        If IsLoaded = False Then Return

        Select Case stabSystemMonitor.SelectedTab.Text.ToLower
            Case "schedule manager"
                Me.tmScheduleMon.Enabled = True
                Me.tmScheduleMonCleaner.Enabled = True
            Case "email log"
                Me.tmEmailLog.Enabled = True
            Case "email queue"
                Me.tmEmailQueue.Enabled = False
        End Select


    End Sub
    Private Sub frmSystemMonitor_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            For Each t As Threading.Thread In oTs
                Try
                    ''console.writeline(t.IsAlive)
                    t.Abort()
                    t = Nothing
                Catch : End Try
            Next

            tcpListener.Stop()
            tcpListener = Nothing

            clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile, False)

            gSysmonInstance = Nothing

            Me.tmEmailQueue.Stop()
            Me.tmScheduleMon.Stop()
            Me.tmEmailLog.Stop()
            Me.tmScheduleMonCleaner.Stop()

            Me.progressTable.Dispose()
        Catch : End Try
    End Sub



    Private Sub frmSystemMonitor_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        Me.tmEmailQueue.Enabled = False
        Me.tmScheduleMon.Enabled = False
        Me.tmEmailLog.Enabled = False
        Me.tmScheduleMonCleaner.Enabled = False
    End Sub

    Private Sub frmSystemMonitor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        setupForDragAndDrop(txtZipName)
        setupForDragAndDrop(txtPath)

        tvProcessing.CheckForIllegalCrossThreadCalls = False

        Try
            tcpListener = New TcpListener(IPAddress.Any, 2003)
        Catch : End Try


        Application.DoEvents()



        FormatForWinXP(Me)
        Me.txtZipName.ContextMenu = Me.mnuInserter
        Me.txtPath.ContextMenu = Me.mnuInserter

        cmbFilter.Text = "None"

        Try
            Dim n As Integer

            n = oUI.ReadRegistry("AutoBackUp", 0)

            chkBackups.Checked = Convert.ToBoolean(n)
        Catch
            chkBackups.Checked = False
        End Try


        lsvTaskManager.Columns(1).Sort()
        lsvTaskManager.ContextMenu = mnuTaskMan
        tvProcessing.ContextMenu = mnuProcMan
        tmScheduleMon.Enabled = True

        Try
            chkAutoRefresh.Checked = oUI.ReadRegistry("AutoRefresh", 0)
        Catch ex As Exception
            chkAutoRefresh.Checked = False
        End Try



        Dim Audit As Boolean

        Try
            Audit = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AuditTrail", 0)))
        Catch ex As Exception
            Audit = False
        End Try

        If Audit = False Then
            stabAuditTrail.Visible = False
        End If

        Try
            chkOneEmailPerSchedule.Checked = oUI.ReadRegistry("OneEmailPerFailure", 0)
        Catch ex As Exception
            chkOneEmailPerSchedule.Checked = False
        End Try

        Dim size As String = clsMarsUI.MainUI.ReadRegistry("SysMonSize", "879,419")

        Me.Width = size.Split(",")(0)
        Me.Height = size.Split(",")(1)

        Try
            Dim value As String = oUI.ReadRegistry("EmailQueueDeleteAfter", "30 Days")
            Me.txtDeleteOlder.Value = value.Split(" ")(0)
            Me.cmbValueUnit.Text = value.Split(" ")(1)
        Catch : End Try

        Try
            txtKeepScheduleHistory.Value = oUI.ReadRegistry("KeepSingle", 14)
        Catch
            txtKeepScheduleHistory.Value = 14
        End Try

        Try
            txtKeepErrorLogs.Value = oUI.ReadRegistry("KeepErrorLogs", 30)
        Catch ex As Exception
            txtKeepErrorLogs.Value = 30
        End Try

        Me.progressTable = New DataTable

        With Me.progressTable.Columns
            .Add("ProcessID")
            .Add("Status")
        End With

        'RestoreColumns(Me.lsvTaskManager)
        'RestoreColumns(Me.lsvProcessing)

        IsLoaded = True

        Dim oT As System.Threading.Thread

        oT = New Threading.Thread(AddressOf Me.ListenForMessages)

        oT.Start()

        oTs.Add(oT)

        ''set up the scheduler  control buttons
        'Try
        '    Dim svc As clsServiceController = New clsServiceController

        '    If svc.m_serviceType <> clsServiceController.enum_svcType.NONE Then
        '        If svc.m_serviceStatus = clsServiceController.enum_svcStatus.RUNNING Then
        '            Me.btnStop.Enabled = True
        '            Me.btnResume.Enabled = False
        '        Else
        '            Me.btnStop.Enabled = False
        '            Me.btnResume.Enabled = True
        '        End If
        '    Else
        '        GroupBox12.Enabled = False
        '    End If
        'Catch : End Try

        Me.slRefreshInterval.Value = clsMarsUI.MainUI.ReadRegistry("TaskManagerRefreshInt", 30)
        Me.tmScheduleMon.Interval = Me.slRefreshInterval.Value * 1000

        pageNav.TodayTooltip = "Go to Page 1"
        pageNav.PreviousPageTooltip = ""
        pageNav.NextPageTooltip = "Go to Page 2"

    End Sub



    Private Function cleanListView()
        Try
            For Each it As DevComponents.AdvTree.Node In Me.lsvTaskManager.Nodes


                Dim type As String = it.Tag
                Dim nID As Integer = it.DataKey
                Dim SQL, Col As String

                Select Case type.ToLower
                    Case "report"
                        Col = "ReportID"
                    Case "package"
                        Col = "PackID"
                    Case "automation"
                        Col = "AutoID"
                    Case "event-package"
                        Col = "EventPackID"
                    Case "event"
                        Col = "EventID"
                    Case "backup"
                        Col = "AutoID"
                End Select

                SQL = "SELECT * FROM TaskManager WHERE " & Col & " = " & nID

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs.EOF = True Then
                    it.Remove()
                End If

                oRs.Close()
            Next
        Catch : End Try
    End Function

    Private Function getTaskStatus(ByVal processID As Integer) As String
        Try
            Dim rows() As DataRow = Me.progressTable.Select("processid =" & processID)
            Dim status As String = "Executing..."

            If rows IsNot Nothing Then
                If rows.Length > 0 Then
                    Dim row As DataRow = rows(0)

                    status = IsNull(row("status"), "Executing...")
                End If
            End If


            'Dim SQL As String = "SELECT status FROM TaskStatusTracker WHERE ProcessID =" & processID
            'Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)


            'If oRs IsNot Nothing Then
            '    If oRs.EOF = False Then
            '        status = IsNull(oRs("status").Value, "Executing...")
            '    End If

            '    oRs.Close()
            'End If

            Return status
        Catch
            Return "Executing..."
        End Try
    End Function

    'add proceses that are executing
    Private Sub ProcessMonitor()

10:     If IsLoaded = False Then Return


        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nodeItem As DevComponents.AdvTree.Node
        Dim I As Integer
        Dim nCheck As Integer = 0
        Dim sUnit As String = "d"
        

220:    If gConType = "DAT" Then
230:        sUnit = "'" & sUnit & "'"
        End If

240:    Me.tvProcessing.BeginUpdate()

250:
        Dim imageSize As Size = New Size(16, 16)
        'add all the items that are being processed

260:    Try

            'back ups
280:        SQL = "SELECT * FROM TaskManager WHERE AutoID = 99999"

290:        oRs = clsMarsData.GetData(SQL)

300:        If oRs IsNot Nothing Then
310:            If oRs.EOF = False Then

                    If tvProcessing.FindNodeByDataKey(99999) Is Nothing Then
                        nodeItem = New DevComponents.AdvTree.Node("System Maintenance")
320:
330:                    nodeItem.Tag = "backup"
340:                    nodeItem.Image = resizeImage(Image.FromFile(getAssetLocation("workstation.png")), imageSize)
                        nodeItem.DataKey = 99999

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

350:                    With nodeItem.Cells
360:                        .Add(New DevComponents.AdvTree.Cell(Convert.ToString(val.ToString("yyyy-MM-dd HH:mm:ss")))) 'start time
370:                        .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value))) 'pc name
380:                        .Add(New DevComponents.AdvTree.Cell("Collecting data...")) 'status
390:                        .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value, ""))) 'process id
400:                        .Add(New DevComponents.AdvTree.Cell(duration)) 'process duration
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If
                End If

410:            oRs.Close()
            End If

            'schedule retries
420:        SQL = "SELECT * FROM TaskManager WHERE AutoID = 99998"

430:        oRs = clsMarsData.GetData(SQL)

440:        If oRs IsNot Nothing Then
450:            If oRs.EOF = False Then

                    If tvProcessing.FindNodeByDataKey(99998) Is Nothing Then
                        nodeItem = New DevComponents.AdvTree.Node("Retrying failed schedules...")

                        nodeItem.Tag = "retry"
                        nodeItem.Image = resizeImage(Image.FromFile(getAssetLocation("retry.png")), imageSize)
                        nodeItem.DataKey = 99998

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

                        With nodeItem.Cells
                            .Add(New DevComponents.AdvTree.Cell(Convert.ToString(val.ToString("yyyy-MM-dd HH:mm:ss")))) 'start time
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value))) 'pc name
                            .Add(New DevComponents.AdvTree.Cell("Collecting data...")) 'status
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value, ""))) 'process id
                            .Add(New DevComponents.AdvTree.Cell(duration)) 'process duration
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If
                End If

550:            oRs.Close()
            End If

            'single based schedules
560:        SQL = "SELECT ReportAttr.ReportID,PCName,TaskManager.Status AS Status, " & _
                  "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                  "ReportAttr INNER JOIN TaskManager ON ReportAttr.ReportID = TaskManager.ReportID"

570:        oRs = clsMarsData.GetData(SQL)

580:        If Not oRs Is Nothing Then
590:            Do While oRs.EOF = False
                    Dim nID As Integer = oRs(0).Value

600:                If tvProcessing.FindNodeByDataKey(nID) Is Nothing Then
610:                    Dim rpt As cssreport = New cssreport(nID)
                        nodeItem = New DevComponents.AdvTree.Node(rpt.reportName)
620:                    nodeItem.Tag = "report"
                        nodeItem.DataKey = nID
                        nodeItem.Image = resizeImage(rpt.reportImage, imageSize)

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

710:                    With nodeItem.Cells
720:                        .Add(New DevComponents.AdvTree.Cell(Convert.ToString((val.ToString("yyyy-MM-dd HH:mm:ss")))))
730:                        .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value)))
740:                        .Add(New DevComponents.AdvTree.Cell(Me.getTaskStatus(IsNull(oRs("processid").Value)))) '(IsNull(oRs("status").Value))
750:                        .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value)))
760:                        .Add(New DevComponents.AdvTree.Cell(duration))
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If
770:                oRs.MoveNext()
780:            Loop
790:            oRs.Close()
            End If


            'packages
800:        SQL = "SELECT PackageAttr.PackID,PCName,TaskManager.Status AS Status, " & _
                  "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                  "PackageAttr INNER JOIN TaskManager ON PackageAttr.PackID = TaskManager.PackID"

810:        oRs = clsMarsData.GetData(SQL)

820:        If Not oRs Is Nothing Then
830:            Do While oRs.EOF = False

840:                Dim nID As Integer = oRs(0).Value

                    If tvProcessing.FindNodeByDataKey(nID) Is Nothing Then
                        Dim pack As Package = New Package(nID)
                        nodeItem = New DevComponents.AdvTree.Node(pack.packageName)
                        nodeItem.Tag = "package"
                        nodeItem.DataKey = nID
                        nodeItem.Image = resizeImage(pack.packageImage, imageSize)

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

                        With nodeItem.Cells
                            .Add(New DevComponents.AdvTree.Cell(Convert.ToString((val.ToString("yyyy-MM-dd HH:mm:ss")))))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value)))
                            .Add(New DevComponents.AdvTree.Cell(Me.getTaskStatus(IsNull(oRs("processid").Value)))) '(IsNull(oRs("status").Value))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value)))
                            .Add(New DevComponents.AdvTree.Cell(duration))
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If

1000:               oRs.MoveNext()
1010:           Loop
1020:           oRs.Close()
            End If


            'event based packages
1030:       SQL = "SELECT EventPackageAttr.EventPackID,PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "EventPackageAttr INNER JOIN TaskManager ON EventPackageAttr.EventPackID = TaskManager.EventPackID"


1040:       oRs = clsMarsData.GetData(SQL)

1050:       If Not oRs Is Nothing Then
1060:           Do While oRs.EOF = False
                    Dim nID As Integer = oRs(0).Value

                    If tvProcessing.FindNodeByDataKey(nID) Is Nothing Then
                        Dim eventPack As EventBasedPackage = New EventBasedPackage(nID)
                        nodeItem = New DevComponents.AdvTree.Node(eventPack.eventPackageName)
                        nodeItem.Tag = "eventpackage"
                        nodeItem.DataKey = nID
                        nodeItem.Image = resizeImage(eventPack.packageImage, imageSize)

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

                        With nodeItem.Cells
                            .Add(New DevComponents.AdvTree.Cell(Convert.ToString((val.ToString("yyyy-MM-dd HH:mm:ss")))))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value)))
                            .Add(New DevComponents.AdvTree.Cell(Me.getTaskStatus(IsNull(oRs("processid").Value)))) '(IsNull(oRs("status").Value))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value)))
                            .Add(New DevComponents.AdvTree.Cell(duration))
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If

1190:               oRs.MoveNext()
1200:           Loop
1210:           oRs.Close()
            End If

            'automation schedules
1220:       SQL = "SELECT  AutomationAttr.AutoID, PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "AutomationAttr INNER JOIN TaskManager ON AutomationAttr.AutoID = TaskManager.AutoID"

1230:       oRs = clsMarsData.GetData(SQL)

1240:       If oRs IsNot Nothing Then
1250:           Do While oRs.EOF = False
                    Dim nID As Integer = oRs(0).Value

                    If tvProcessing.FindNodeByDataKey(nID) Is Nothing Then
                        Dim auto As Automation = New Automation(nID)
                        nodeItem = New DevComponents.AdvTree.Node(auto.AutoName)
                        nodeItem.Tag = "automation"
                        nodeItem.DataKey = nID
                        nodeItem.Image = resizeImage(auto.automationImage, imageSize)

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

                        With nodeItem.Cells
                            .Add(New DevComponents.AdvTree.Cell(Convert.ToString((val.ToString("yyyy-MM-dd HH:mm:ss")))))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value)))
                            .Add(New DevComponents.AdvTree.Cell(Me.getTaskStatus(IsNull(oRs("processid").Value)))) '(IsNull(oRs("status").Value))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value)))
                            .Add(New DevComponents.AdvTree.Cell(duration))
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If

1370:               oRs.MoveNext()
1380:           Loop

1390:           oRs.Close()
            End If

            'event-based schedules
1400:       SQL = "SELECT EventAttr6.EventID, PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "EventAttr6 INNER JOIN TaskManager ON EventAttr6.EventID = TaskManager.EventID WHERE (EventAttr6.PackID =0 OR EventAttr6.PackID IS NULL)"

1410:       oRs = clsMarsData.GetData(SQL)

1420:       If oRs IsNot Nothing Then
1430:           Do While oRs.EOF = False
                    Dim nID As Integer = oRs(0).Value

                    If tvProcessing.FindNodeByDataKey(nID) Is Nothing Then
                        Dim evt As EventBased = New EventBased(nID)
                        nodeItem = New DevComponents.AdvTree.Node(evt.eventName)
                        nodeItem.Tag = "event"
                        nodeItem.DataKey = nID
                        nodeItem.Image = resizeImage(evt.eventImage, imageSize)

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

                        With nodeItem.Cells
                            .Add(New DevComponents.AdvTree.Cell(Convert.ToString((val.ToString("yyyy-MM-dd HH:mm:ss")))))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("pcname").Value)))
                            .Add(New DevComponents.AdvTree.Cell(Me.getTaskStatus(IsNull(oRs("processid").Value)))) '(IsNull(oRs("status").Value))
                            .Add(New DevComponents.AdvTree.Cell(IsNull(oRs("processid").Value)))
                            .Add(New DevComponents.AdvTree.Cell(duration))
                        End With

                        tvProcessing.Nodes.Add(nodeItem)
                    End If

1550:               oRs.MoveNext()
1560:           Loop

1570:           oRs.Close()
            End If


        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            Me.tvProcessing.EndUpdate()
        End Try

    End Sub
    Public Sub ScheduleMonitor()
        'Return
        clsMarsData.WriteData("DELETE FROM TaskManager WHERE monitorid IS NULL", False)

10:     If IsLoaded = False Then Return

        Try
            Dim SQL() As String
            Dim oRs As ADODB.Recordset
            Dim itemNode As DevComponents.AdvTree.Node
            Dim I As Integer
            Dim nCheck As Integer = 0
            Dim KwikExit As Boolean = False
            Dim sUnit As String = "d"
            Dim selectedNode As DevComponents.AdvTree.Node = lsvTaskManager.SelectedNode

            Dim sfilter As String = cmbFilter.Text

20:         Select Case sfilter
                Case "None"
30:                 sUnit = "d"
40:                 nCheck = 0
50:             Case "Being Processed"
60:                 KwikExit = True
70:             Case "Due Today -All"
80:                 sUnit = "d"
90:                 nCheck = 0
100:            Case "Due Today - Next 5 Minutes"
110:                sUnit = "n"
120:                nCheck = 5
130:            Case "Due Today - Next 30 Minutes"
140:                sUnit = "n"
150:                nCheck = 30
160:            Case "Due Today - Next 60 Minutes"
170:                sUnit = "n"
180:                nCheck = 60
190:            Case "Due in Next 24 Hours"
200:                sUnit = "n"
210:                nCheck = 1440
            End Select

220:        If gConType = "DAT" Then
230:            sUnit = "'" & sUnit & "'"
            End If

240:        Me.lsvTaskManager.BeginUpdate()

250:        Me.lsvTaskManager.Nodes.Clear()

            ReDim SQL(3)

            'now add the schedules which are waiting to get processed
1600:       SQL(0) = "SELECT reportattr.reportid FROM ReportAttr, ScheduleAttr WHERE " & _
                 "(ReportAttr.ReportID = ScheduleAttr.ReportID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
                 "ReportAttr.ReportID NOT IN (SELECT ReportID FROM TaskManager) AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE'"


1610:       SQL(1) = "SELECT packageattr.packid FROM PackageAttr, ScheduleAttr WHERE " & _
                 "(PackageAttr.PackID = ScheduleAttr.PackID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
                 "PackageAttr.PackID NOT IN (SELECT PackID FROM TaskManager) AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE'"

1620:       SQL(2) = "SELECT AutomationAttr.AutoID FROM AutomationAttr, ScheduleAttr WHERE " & _
                 "(AutomationAttr.AutoID = ScheduleAttr.AutoID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE' AND " & _
                 "AutomationAttr.AutoID NOT IN (SELECT AutoID FROM TaskManager)"

1630:       SQL(3) = "SELECT EventPackageAttr.EventPackID FROM EventPackageAttr, ScheduleAttr WHERE " & _
                 "(EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE' AND " & _
                 "EventPackageAttr.EventPackID NOT IN (SELECT EventPackID FROM TaskManager)"


1640:       For I = 0 To 3

1650:           If gConType = "DAT" Then
1660:               SQL(I) = SQL(I).Replace("[CurrentDate]", "Now()")
1670:           Else
1680:               SQL(I) = SQL(I).Replace("[CurrentDate]", "GetDate()")
                End If

1690:           oRs = clsMarsData.GetData(SQL(I))

                Dim iconSize As Size = New Size(16, 16)

1700:           If Not oRs Is Nothing Then
1710:               Do While oRs.EOF = False
                        Dim idvalue As String = oRs.Fields(0).Value



1730:                   If I = 0 Then
                            Dim rpt As cssreport = New cssreport(idvalue)

                            itemNode = New DevComponents.AdvTree.Node(rpt.reportName)
                            itemNode.Tag = "report"
                            itemNode.DataKey = idvalue
                            itemNode.Image = resizeImage(rpt.reportImage, iconSize)


                            itemNode.Cells.Add(New DevComponents.AdvTree.Cell(rpt.m_schedule.nexttorun.ToString("yyyy-MM-dd HH:mm:ss")))

                            lsvTaskManager.Nodes.Add(itemNode)
1800:                   ElseIf I = 1 Then
                            Dim pack As Package = New Package(idvalue)

                            itemNode = New DevComponents.AdvTree.Node(pack.packageName)
                            itemNode.Tag = "package"
                            itemNode.DataKey = idvalue
                            itemNode.Image = resizeImage(pack.packageImage, iconSize)

                            itemNode.Cells.Add(New DevComponents.AdvTree.Cell(pack.m_schedule.nexttorun.ToString("yyyy-MM-dd HH:mm:ss")))
                            lsvTaskManager.Nodes.Add(itemNode)
1870:                   ElseIf I = 2 Then
                            Dim auto As Automation = New Automation(idvalue)

                            itemNode = New DevComponents.AdvTree.Node(auto.AutoName)
                            itemNode.Tag = "automation"
                            itemNode.DataKey = idvalue
                            itemNode.Image = resizeImage(auto.automationImage, iconSize)

                            itemNode.Cells.Add(New DevComponents.AdvTree.Cell(auto.m_schedule.nexttorun.ToString("yyyy-MM-dd HH:mm:ss")))
                            lsvTaskManager.Nodes.Add(itemNode)
1890:                   ElseIf I = 3 Then
                            Dim eventpack As EventBasedPackage = New EventBasedPackage(idvalue)

                            itemNode = New DevComponents.AdvTree.Node(eventpack.eventPackageName)
                            itemNode.Tag = "eventpackage"
                            itemNode.DataKey = idvalue
                            itemNode.Image = resizeImage(eventpack.packageImage, iconSize)

                            itemNode.Cells.Add(New DevComponents.AdvTree.Cell(eventpack.m_schedule.nexttorun.ToString("yyyy-MM-dd HH:mm:ss")))
                            lsvTaskManager.Nodes.Add(itemNode)
                        End If

1980:                   oRs.MoveNext()
1990:               Loop

2000:               oRs.Close()
                End If
2010:       Next

2080:       Try
2090:           If selectedNode IsNot Nothing Then
                    lsvTaskManager.SelectedNode = selectedNode
                End If
2150:       Catch : End Try

            If lsvTaskManager.Columns.IsSorted = False Then
                lsvTaskManager.Columns(1).SortDirection = DevComponents.AdvTree.eSortDirection.Ascending
                lsvTaskManager.Columns(1).Sort()
            End If
KWIK_EXIT:
2160:       Me.lsvTaskManager.EndUpdate()
            'scheduler info
2170:       Try
                Dim serviceType As String = gServiceType

2180:           If serviceType = "" Then serviceType = oUI.ReadRegistry("SQL-RDService", "NONE")

                Dim scheduleInfo As String = ""
                Dim ebInfo As String = ""

2190:           Select Case serviceType.ToLower
                    Case "windowsnt"
2200:                   scheduleInfo = ReadTextFromFile(sAppPath & "ntservice.info")

2210:                   If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Last Poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim
                        End If
2220:               Case "windowsapp"
2230:                   scheduleInfo = ReadTextFromFile(sAppPath & "bgservice.info")

2240:                   If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Last Poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim & " seconds"
                        End If
2250:               Case "none"
2260:                   scheduleInfo = "No scheduler is configured. You can configure the scheduler from Options -> Scheduler"
                End Select

2270:           '  Me.lblScheduler.Text = "SCHEDULER INFO" & vbCrLf & vbCrLf & scheduleInfo & vbCrLf & ebInfo
            Catch : End Try
2280:   Catch ex As Exception

        Finally
2290:       Try
2300:           Me.lsvTaskManager.EndUpdate()
            Catch : End Try
        End Try

    End Sub


    Public Sub EmailQueue()
        If IsLoaded = False Then Return
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim lsv As ListViewItem

            SQL = "SELECT * FROM SendWait"

            oRs = clsMarsData.GetData(SQL)

            lsvEmailQueue.Items.Clear()

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    lsv = lsvEmailQueue.Items.Add(oRs("sname").Value)

                    With lsv
                        .Tag = oRs("queuenumber").Value

                        With .SubItems
                            .Add(oRs("addresses").Value)
                            .Add(oRs("sendcc").Value)
                            .Add(oRs("subject").Value)

                            Dim val As Date = CTimeZ(oRs("lastattempt").Value, dateConvertType.READ)
                            .Add(Convert.ToString(val))
                            .Add(oRs("lastresult").Value)

                            Try
                                .Add(IsNull(oRs("messagefile").Value))
                            Catch : End Try
                        End With
                    End With

                    oRs.MoveNext()
                Loop
            End If

            oRs.Close()

            oData.DisposeRecordset(oRs)
            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Sub LoadEmailLog()

        If gConType = "DAT" Then
            Me.lsvLog.Visible = True
            Me.dgvEmailLog.Visible = False
            Me.lsvLog.Dock = DockStyle.Fill
            LoadEmailLogLSV()
            txtSearch.Visible = True
        Else
            Me.dgvEmailLog.Visible = True
            Me.lsvLog.Visible = False
            Me.dgvEmailLog.Dock = DockStyle.Fill
            Me.txtSearch.Visible = False
            Label23.Visible = False
            LoadEmailLogDGV()
        End If
    End Sub


    Public Sub LoadEmailLogDGV()
        Dim SQL As String = "SELECT logid AS ID, schedulename AS Schedule, sendto AS Recipient, Subject, datesent AS [Date], messagefile AS [File] FROM EmailLog ORDER BY logid DESC"

        '//add an unbound column for the icon
        dgvEmailLog.CellBorderStyle = DataGridViewCellBorderStyle.SingleVertical

        clsMarsUI.BusyProgress(10, "Loading email log")

        If Me.dgvEmailLog.DataSource Is Nothing Then
            Try
                Dim col As DataGridViewImageColumn = New DataGridViewImageColumn()
                col.DefaultCellStyle.BackColor = Color.White
                col.DefaultCellStyle.ForeColor = Color.Black
                col.DefaultCellStyle.SelectionBackColor = Color.White
                col.DefaultCellStyle.SelectionForeColor = Color.Black
                col.HeaderText = ""

                Me.dgvEmailLog.Columns.Add(col)

                clsMarsUI.BusyProgress(40, "Loading email log")

                Dim bs As BindingSource
                Dim ds As DataSet = New DataSet

                clsMarsUI.BusyProgress(60, "Loading email log")

                clsMarsData.fillDataset(ds, SQL)

                bs = New BindingSource(ds, ds.Tables(0).TableName)

                Me.dgvEmailLog.VirtualMode = True
                Me.dgvEmailLog.RowCount = 500
                Me.dgvEmailLog.DataSource = bs

                emailLogFilterMan = New DgvFilterPopup.DgvFilterManager(Me.dgvEmailLog)

                Me.dgvEmailLog.Refresh()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            End Try
        Else
            Try
                clsMarsUI.BusyProgress(10, "Updating email log")

                'select the row
                Dim bs As BindingSource = Me.dgvEmailLog.DataSource
                Dim ds As DataSet = New DataSet

                clsMarsUI.BusyProgress(50, "Updating email log")

                clsMarsData.fillDataset(ds, SQL)
                bs.DataSource = ds
                bs.DataMember = ds.Tables(0).TableName
                bs.ResetBindings(False)

                clsMarsUI.BusyProgress(90, "Updating email log")

                Try
                    For Each n As Integer In emailLogRow
                        Me.dgvEmailLog.Rows(n).Selected = True
                    Next

                    dgvEmailLog.FirstDisplayedScrollingRowIndex = emailLogRow(emailLogRow.GetUpperBound(0))
                Catch : End Try

                '//reset the filter
                'emailLogFilterMan.
                For Each entry As DictionaryEntry In emailLogFilter
                    Me.emailLogFilterMan.Item(entry.Key) = entry.Value
                Next
            Catch : End Try
        End If

        dgvEmailLog.AllowUserToResizeColumns = True

        clsMarsUI.BusyProgress(100, "Loading email log", True)

    End Sub
    Public Sub LoadEmailLogLSV()
10:     If IsLoaded = False Then Return


20:     If lsvLog.Items.Count = 0 Then
30:         gItemList = Nothing

40:         Try
                Dim SQL As String
                Dim oRs As ADODB.Recordset
                Dim lsv As ListViewItem

50:             lsvLog.BeginUpdate()

60:             SQL = "SELECT * FROM EmailLog ORDER BY DateSent DESC"

70:             oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                Dim nTotal As Integer = oRs.RecordCount
                Dim I As Integer = 1

80:             If Not oRs Is Nothing Then
90:                 Do While oRs.EOF = False
100:                    clsMarsUI.BusyProgress(((I / nTotal) * 100), "Loading email log...")

110:                    ReDim Preserve gItemList(I - 1)

120:                    lsv = lsvLog.Items.Add(oRs("schedulename").Value)

130:                    With lsv
140:                        .Tag = oRs("logid").Value

150:                        With .SubItems
160:                            .Add(IsNull(oRs("sendto").Value, ""))
170:                            .Add(IsNull(oRs("subject").Value, ""))

                                Dim val As Date = CTimeZ(IsNull(oRs("datesent").Value, Now), dateConvertType.READ)

180:                            .Add(Convert.ToString(val))

190:                            Try
200:                                .Add(IsNull(oRs("messagefile").Value, ""))
210:                            Catch ex As Exception
220:                                .Add("")
                                End Try
                            End With

230:                        .ImageIndex = 4
                        End With

240:                    gItemList(I - 1) = lsv

250:                    oRs.MoveNext()

260:                    I += 1
270:                Loop

280:                oRs.Close()
                End If

290:            oData.DisposeRecordset(oRs)
300:            lsvLog.EndUpdate()

310:            Exit Sub
320:        Catch ex As Exception
330:            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
340:        Finally
350:            clsMarsUI.BusyProgress(, , True)
            End Try
360:    Else
370:        Try
                'create a list of existing log ids to exclude from the query
                Dim logList As String = ""

                For Each it As ListViewItem In Me.lsvLog.Items
                    logList &= it.Tag & ","
                Next

                If logList = "" Then Return

                'remove the last comma
                logList = logList.Remove(logList.Length - 1, 1)
                logList = "(" & logList & ")"

                Dim SQL As String = "SELECT * FROM EmailLog WHERE LogID NOT IN " & logList & " ORDER BY DateSent DESC"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)
                Dim nTotal As Integer = oRs.RecordCount
                Dim I As Integer = 1

380:            If oRs IsNot Nothing Then
390:                Do While oRs.EOF = False
                        Dim sTag As String = oRs("logid").Value
                        Dim found As Boolean = False

                        clsMarsUI.BusyProgress((I / nTotal) * 100, "Refreshing Email log...")


460:                    ReDim Preserve gItemList(gItemList.Length)

470:                    lsvLog.BeginUpdate()

                        Dim lsv As ListViewItem = lsvLog.Items.Add(oRs("schedulename").Value)

480:                    With lsv
490:                        .Tag = oRs("logid").Value

500:                        With .SubItems
510:                            .Add(IsNull(oRs("sendto").Value))
520:                            .Add(IsNull(oRs("subject").Value))

                                Dim val As Date = CTimeZ(IsNull(oRs("datesent").Value, Now), dateConvertType.READ)
530:                            .Add(Convert.ToString(val))

540:                            Try
550:                                .Add(IsNull(oRs("messagefile").Value, ""))
560:                            Catch ex As Exception
570:                                .Add("")
                                End Try
                            End With

580:                        .ImageIndex = 4
                        End With

590:                    gItemList(gItemList.Length - 1) = lsv

                        Dim sortString As String = " ASC"

600:                    If logsortOrder = Windows.Forms.SortOrder.Ascending Then
610:                        sortString = " ASC"
620:                    Else
630:                        sortString = " DESC"
                        End If

                        Dim isDate As Boolean = False

640:                    If logsortBy = 3 Then isDate = True

                        Try
650:                        clsMarsUI.sortListView(Me.lsvLog, sortString, logsortBy, isDate)
                        Catch : End Try

660:                    lsvLog.EndUpdate()

                        I += 1
670:                    oRs.MoveNext()
680:                Loop

                    clsMarsUI.BusyProgress(, , True)

690:                oRs.Close()
                End If
700:        Catch ex As Exception
710:            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Finally
                clsMarsUI.BusyProgress(, , True)
            End Try
        End If
    End Sub



    Private Sub cmdRemoveOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveOne.Click
        If lsvEmailQueue.SelectedItems.Count = 0 Then Exit Sub

        Dim SQL As String
        Dim lsv As ListViewItem

        For Each lsv In lsvEmailQueue.SelectedItems
            Try
                Dim path As String = lsv.SubItems(6).Text

                IO.File.Delete(path)
            Catch : End Try

            SQL = "DELETE FROM SendWait WHERE QueueNumber = " & lsv.Tag

            clsMarsData.WriteData(SQL)

            lsvEmailQueue.Items.Remove(lsv)
        Next
    End Sub

    Private Sub cmdRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveAll.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim Response As DialogResult

        Response = MessageBox.Show("Clear all mails from the queue?", Application.ProductName, MessageBoxButtons.YesNo)

        If Response = Windows.Forms.DialogResult.Yes Then
            For Each lsv As ListViewItem In Me.lsvEmailQueue.Items
                Try
                    Dim path As String = lsv.SubItems(6).Text

                    Try
                        IO.File.Delete(path)
                    Catch : End Try
                Catch : End Try
            Next

            SQL = "DELETE FROM SendWait"

            clsMarsData.WriteData(SQL)
        End If

        lsvEmailQueue.Items.Clear()
    End Sub

    Private Sub cmdSendNow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSendNow.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim lsv As ListViewItem
        Dim oMsg As clsMarsMessaging = New clsMarsMessaging
        Dim Success As Boolean

        'database values
        Dim Addresses As String
        Dim Subject As String
        Dim Body As String
        Dim SendType As String
        Dim Attach As String
        Dim NumAttach As Integer
        Dim Extras As String
        Dim SendCC As String
        Dim SendBcc As String
        Dim sName As String
        Dim InBody As String
        Dim sFormat As String
        Dim sFiles As String

        If lsvEmailQueue.SelectedItems.Count = 0 Then Exit Sub

        For Each lsv In lsvEmailQueue.SelectedItems
            SQL = "SELECT * FROM SendWait WHERE QueueNumber = " & lsv.Tag

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                Addresses = IsNull(oRs("addresses").Value)
                Subject = IsNull(oRs("subject").Value)
                Body = IsNull(oRs("body").Value)
                SendType = IsNull(oRs("sendtype").Value)
                Attach = IsNull(oRs("attach").Value)
                NumAttach = IsNull(oRs("numattach").Value)
                Extras = IsNull(oRs("extras").Value)
                SendCC = IsNull(oRs("sendcc").Value)
                SendBcc = IsNull(oRs("sendbcc").Value)
                sName = IsNull(oRs("sname").Value)
                InBody = IsNull(oRs("inbody").Value)
                sFormat = IsNull(oRs("sformat").Value)
                sFiles = IsNull(oRs("filelist").Value)
                rptFileNames = sFiles.Split("|")
            End If

            Select Case MailType
                Case MarsGlobal.gMailType.MAPI
                    Success = clsMarsMessaging.SendMAPI(Addresses, Subject, Body, SendType, Attach, _
                    NumAttach, Extras, SendCC, SendBcc, Convert.ToBoolean(InBody), _
                    sFormat, sName, False)

                Case MarsGlobal.gMailType.SMTP, MarsGlobal.gMailType.SQLRDMAIL
                    Dim objSender As clsMarsMessaging = New clsMarsMessaging

                    Success = objSender.SendSMTP(Addresses, Subject, Body, SendType, Attach, _
                    NumAttach, Extras, SendCC, SendBcc, sName, Convert.ToBoolean(InBody), _
                    sFormat, False)
            End Select

            If Success = True Then
                clsMarsData.WriteData("DELETE FROM SendWait WHERE QueueNumber =" & lsv.Tag)

                lsvEmailQueue.Items.Remove(lsv)
            End If
        Next

    End Sub




    Private Sub cmdClearLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim SQL As String
        SQL = "DELETE FROM EmailLog"

        clsMarsData.WriteData(SQL)

        lsvLog.Items.Clear()

        Dim emailPath As String = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages")

        If IO.Directory.Exists(emailPath) = True Then
            For Each s As String In IO.Directory.GetFiles(emailPath)
                Try
                    IO.File.Delete(s)
                Catch : End Try
            Next
        End If
    End Sub

    Private Sub killThread(ByVal nID As Integer, ByVal type As clsMarsScheduler.enScheduleType)
        Try

            Dim colName As String

            Select Case type
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    colName = "AutoID"
                Case clsMarsScheduler.enScheduleType.EVENTBASED
                    colName = "EventID"
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    colName = "EventPackID"
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    colName = "PackID"
                Case clsMarsScheduler.enScheduleType.REPORT
                    colName = "ReportID"
                Case Else
                    Return
            End Select

            Dim SQL As String = "SELECT ProcessID FROM ThreadManager WHERE " & colName & " = " & nID
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim procID As Integer = IsNull(oRs("processid").Value, 0)

                    Try
                        Dim proc As Process = Process.GetProcessById(procID, Environment.MachineName)

                        proc.Kill()
                    Catch : End Try
                End If

                oRs.Close()
            End If

        Catch : End Try
    End Sub

    Private Sub _RemoveFromQueue(ByVal sName As String, ByVal nID As Integer, ByVal sType As String, ByVal Log As Boolean)
        Try
            Dim SQL As String
            Dim ScheduleID As Integer
            Dim oRs As ADODB.Recordset
            Dim NextRun As Date

            '    If lsvTaskManager.SelectedNode Is Nothing Then Return

            Select Case sType.ToLower
                Case "report"

                    sType = "single report"

                    If clsMarsData.IsDuplicate("TaskManager", "ReportID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.REPORT)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(nID)

                    Dim sched As Schedule = New Schedule(nID, Schedule.parentscheduleType.REPORT)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = sched.nexttorun 'lsvTaskManager.SelectedNode.Cells(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                Else
                                    Force2Moro = True
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try


                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))



                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                             ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE ReportID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE ReportID = " & nID, False)
                        End If
                    End If
                Case "package"

                    If clsMarsData.IsDuplicate("TaskManager", "PackID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.PACKAGE)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(, nID)

                    Dim sched As Schedule = New Schedule(nID, Schedule.parentscheduleType.PACKAGE)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = sched.nexttorun  ' lsvTaskManager.SelectedNode.Cells(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                            ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE PackID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE PackID = " & nID, False)
                        End If
                    End If
                Case "automation", "backup", "retry"
                    If clsMarsData.IsDuplicate("TaskManager", "AutoID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.AUTOMATION)
                    End If

                    Dim sched As Schedule = New Schedule(nID, Schedule.parentscheduleType.AUTOMATION)

                    If nID = 99999 Or nID = 9998 Then GoTo ClearData

                    oSchedule.TaskManager("Remove", , , nID)
                    oSchedule.ThreadManager(nID, "Remove", clsMarsScheduler.enScheduleType.AUTOMATION)

                    ScheduleID = clsMarsScheduler.GetScheduleID(, , nID)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = sched.nexttorun 'lsvTaskManager.SelectedNode.Cells(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                             ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            If Log = True Then
                                oSchedule.SetScheduleHistory(False, "User remove schedule from queue", , nID)
                            End If

ClearData:
                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE AutoID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE AutoID = " & nID, False)
                        End If
                    End If
                Case "event-package", "eventpackage"
                    If clsMarsData.IsDuplicate("TaskManager", "EventPackID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.EVENTPACKAGE)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)

                    oSchedule.TaskManager("Remove", , , , Log, nID)

                    Dim sched As Schedule = New Schedule(nID, Schedule.parentscheduleType.EVENTBASEDPACKAGE)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = sched.nexttorun  'lsvTaskManager.SelectedNode.Cells(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                            ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE EventPackID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE EventPackID = " & nID, False)
                        End If
                    End If
                Case "event"
                    If clsMarsData.IsDuplicate("TaskManager", "EventID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.EVENTBASED)
                    End If

                    clsMarsScheduler.globalItem.TaskManager("Remove", , , , Log, , nID)

                    clsMarsData.WriteData("DELETE FROM ThreadManager WHERE EventID =" & nID, False)
                    clsMarsData.WriteData("DELETE FROM TaskManager WHERE EventID = " & nID, False)

                    clsMarsEvent.SaveEventHistory(nID, False, "Schedule execution was manually cancelled by user")
            End Select

            Dim errorMsg As String = sType & " schedule:".ToUpper & " " & sName & ": Schedule manually remove from queue."

            _ErrorHandle(errorMsg, -214790210, "_RemoveFromQueue", 0, "", True, True)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        Finally
            _Delay(1)
        End Try
    End Sub
    Private Sub mnuRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemove.Click, mnuProcEnd.Click
        Dim lsv As DevComponents.AdvTree.AdvTree
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "mnuremove", True) = 0 Then
            lsv = lsvTaskManager
        Else
            lsv = tvProcessing
        End If

        If lsv.SelectedNodes.Count = 0 Then Return

        Dim sType As String
        Dim nID As Integer
        Dim selectedItems As ArrayList = New ArrayList

        For Each oItem As DevComponents.AdvTree.Node In lsv.SelectedNodes
            selectedItems.Add(oItem)
        Next

        For Each oItem As DevComponents.AdvTree.Node In selectedItems
            sType = oItem.Tag
            nID = oItem.DataKey

            _RemoveFromQueue(oItem.Text, nID, sType, True)

            oItem.Remove()
        Next


    End Sub

    Private Sub cmbFilter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFilter.SelectedIndexChanged
        ScheduleMonitor()
    End Sub

    Public Sub DeferedList(Optional ByVal SortOrder As String = "ASC")
        If IsLoaded = False Then Return

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem
        Dim destType As String

        SQL = "SELECT * FROM DeferDeliveryAttr d INNER JOIN destinationAttr x ON d.destinationid = x.destinationid ORDER BY DueDate " & SortOrder

        oRs = clsMarsData.GetData(SQL)

        lsvDefer.Items.Clear()

        lsvDefer.BeginUpdate()

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem
                destType = oRs("destinationtype").Value
                Dim destID As Integer = oRs("destinationid").Value

                With oItem
                    .Text = clsMarsScheduler.getScheduleNameFromDestination(destID) 'oRs("filename").Value
                    .Tag = oRs("deferid").Value

                    With .SubItems
                        .Add(oRs("filename").Value)
                        .Add(oRs("filepath").Value)
                        .Add(Convert.ToString(CTimeZ(oRs("duedate").Value, dateConvertType.READ)))



                        SQL = "SELECT * FROM deferdeliveryDetailAttr WHERE deferid = " & oItem.Tag

                        Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                        If oRs1 IsNot Nothing AndAlso oRs1.EOF = False Then
                            If destType = "Email" Then
                                .Add(oRs1("sendto").Value)
                                .Add(oRs1("sendcc").Value)
                                .Add(oRs1("sendbcc").Value)
                                .Add(oRs1("subject").Value)
                            ElseIf destType = "Disk" Then
                                .Add(oRs1("deliveryDevice").Value)
                                .Add(oRs1("sendcc").Value)
                                .Add(oRs1("sendbcc").Value)
                                .Add(oRs1("subject").Value)
                            ElseIf destType = "FTP" Then
                                .Add(oRs1("ftpserver").Value)
                                .Add(oRs1("sendcc").Value)
                                .Add(oRs1("sendbcc").Value)
                                .Add(oRs1("subject").Value)
                            Else
                                .Add(oRs1("deliveryDevice").Value)
                                .Add(oRs1("sendcc").Value)
                                .Add(oRs1("sendbcc").Value)
                                .Add(oRs1("subject").Value)
                            End If

                            oRs1.Close()
                            oRs1 = Nothing
                        End If
                    End With

                    lsvDefer.Items.Add(oItem)
                End With

                oRs.MoveNext()
            Loop

            lsvDefer.EndUpdate()

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub optDeferAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDeferAsc.CheckedChanged
        If IsLoaded = False Then Return

        If optDeferAsc.Checked = True Then
            DeferedList("ASC")
        End If
    End Sub

    Private Sub optDeferDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDeferDesc.CheckedChanged
        If IsLoaded = False Then Return

        If optDeferDesc.Checked = True Then
            DeferedList("DESC")
        End If
    End Sub

    Private Sub mnuDeferDeliver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeferDeliver.Click
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim oReport As New clsMarsReport
        Dim Ok As Boolean

        Dim nReportID As Integer = 0
        Dim nPackID As Integer = 0
        Dim oTask As New clsMarsTask
        Dim oUI As New clsMarsUI
        Dim nScheduleID As Integer
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        For Each oItem As ListViewItem In lsvDefer.SelectedItems
            oUI.BusyProgress(60, "Performing the requested action")

            SQL = "SELECT x.ReportID,x.PackID FROM DeferDeliveryAttr d INNER JOIN DestinationAttr x ON d.DestinationID = " & _
                                        "x.DestinationID WHERE d.DeferID =" & oItem.Tag

            oRs = clsMarsData.GetData(SQL)

            Try
                If oRs.EOF = False Then
                    nReportID = oRs.Fields(0).Value
                    nPackID = oRs.Fields(1).Value
                End If

                oRs.Close()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try

            Ok = oReport.DeliverDefered(lsvDefer.SelectedItems(0).Tag)

            If Ok = True Then
                oUI.BusyProgress(75, "Getting schedule properties...")

                If nReportID > 0 Then
                    nScheduleID = clsMarsScheduler.GetScheduleID(nReportID)
                Else
                    nScheduleID = clsMarsScheduler.GetScheduleID(, nPackID)
                End If

                oUI.BusyProgress(90, "Setting schedule history...")

                oSchedule.SetScheduleHistory(True, "Deferred delivery peformed successfully", nReportID, nPackID)

                oUI.BusyProgress(95, "Performing custom actions...")

                oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

                oUI.BusyProgress(100, "Cleaning up...")

                clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr WHERE DeferID =" & oItem.Tag, False)

                oItem.Remove()

                oUI.BusyProgress(, , True)

                MessageBox.Show("Report delivered successfully!", Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nReportID)
            End If
        Next
    End Sub

    Private Sub mnuDeferRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeferRemove.Click
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim oRes As DialogResult

        oRes = MessageBox.Show("Remove the selected delivery from the list?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Yes Then
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            Dim nScheduleID As Integer
            Dim nReportID As Integer

            For Each oItem As ListViewItem In lsvDefer.SelectedItems

                SQL = "SELECT x.ReportID FROM DeferDeliveryAttr d INNER JOIN DestinationAttr x ON d.DestinationID = " & _
                "x.DestinationID WHERE d.DeferID =" & oItem.Tag

                oRs = clsMarsData.GetData(SQL)

                Try
                    If oRs.EOF = False Then
                        nReportID = oRs.Fields(0).Value
                    End If

                    oRs.Close()

                    nScheduleID = clsMarsScheduler.GetScheduleID(nReportID)

                    oSchedule.SetScheduleHistory(False, "Deferred delivery removed from queue by user", nReportID)

                    clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr WHERE DeferID =" & oItem.Tag, False)

                    oItem.Remove()

                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            Next
        End If
    End Sub

    Private Sub optDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optDaily.Checked = True Then
            Label5.Text = "days"
            Label9.Text = "days"
        End If
    End Sub

    Private Sub optWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optWeekly.Checked = True Then
            Label5.Text = "weeks"
            Label9.Text = "weeks"
        End If
    End Sub

    Private Sub optMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optMonthly.Checked = True Then
            Label5.Text = "months"
            Label9.Text = "months"
        End If
    End Sub

    Private Sub optYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optYearly.Checked = True Then
            Label5.Text = "years"
            Label9.Text = "years"
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim fbd As New FolderBrowserDialog

        fbd.Description = "Please select the path to backup to (a subfolder will be created for each backup)"

        fbd.ShowNewFolderButton = False

        fbd.ShowDialog()

        If fbd.SelectedPath.Length = 0 Then
            Return
        Else
            txtPath.Text = _CreateUNC(fbd.SelectedPath)
        End If

    End Sub

    Private Sub cmdSaveBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveBackup.Click
        ' for status
        '0 = no scheduling
        '1 = simple
        '2 = advanced
        '3 = advanced with zip

        Dim sPath As String = sAppPath & "backup.xml"
        Dim SQL As String

        If txtPath.Text.Length = 0 Then
            setError(txtPath, "Please select the backup path")
            txtPath.Focus()
            Return
        End If

        If System.IO.File.Exists(sPath) Then
            Try : System.IO.File.Delete(sPath) : Catch : End Try
        End If
        'if simple is selected
        'If rbSimple.Checked = True Then

        Dim oRs As New ADODB.Recordset

        With oRs
            With .Fields()
                .Append("Frequency", ADODB.DataTypeEnum.adVarChar, 50)
                .Append("RunTime", ADODB.DataTypeEnum.adVarChar, 50)
                .Append("RepeatEvery", ADODB.DataTypeEnum.adInteger)
                .Append("BackupPath", ADODB.DataTypeEnum.adVarWChar, 255)
                .Append("NextRun", ADODB.DataTypeEnum.adDate, 50)
                .Append("KeepFor", ADODB.DataTypeEnum.adInteger)
            End With

            .Open()

            .AddNew()

            If optDaily.Checked = True Then
                .Fields("frequency").Value = "Daily"
            ElseIf optWeekly.Checked = True Then
                .Fields("frequency").Value = "Weekly"
            ElseIf optMonthly.Checked = True Then
                .Fields("frequency").Value = "Monthly"
            ElseIf optYearly.Checked = True Then
                .Fields("frequency").Value = "Yearly"
            End If

            .Fields("runtime").Value = dtBackup.Value.ToString("HH:mm:ss")
            .Fields("nextrun").Value = Now.Date & " " & dtBackup.Value.ToString("HH:mm:ss")
            .Fields("repeatevery").Value = txtInterval.Value
            .Fields("backuppath").Value = txtPath.Text
            .Fields("KeepFor").Value = txtKeep.Value

            .Update()

            clsMarsData.CreateXML(oRs, sPath)

            ''update the scheduleattr table
            'SQL = "Delete from ScheduleAttr where ScheduleID = 11"
            'clsMarsData.WriteData(SQL)

            'Dim sCols As String = "ScheduleID,ReportID,PackID,AutoID"
            'Dim sVals As String = "11,0,0,0"
            'Dim SQL2 As String = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"
            'clsMarsData.WriteData(SQL2)

            'MessageBox.Show("Scheduled backup saved successfully", Application.ProductName, MessageBoxButtons.OK, _
            'MessageBoxIcon.Information)
        End With
        'End If
        'if advanced is selected


        'If rbAdvanced.Checked = True Then
        SQL = "Delete from ScheduleAttr where ScheduleID = 11"
        clsMarsData.WriteData(SQL)
        Dim sCols As String = "ScheduleID,ReportID,PackID,AutoID"
        Dim sVals As String = "11,0,0,0"
        Dim SQL2 As String = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"
        clsMarsData.WriteData(SQL2)

        Dim nRepeat As String
        If UcUpdate.chkRepeat.Checked = False Then
            nRepeat = "0"
        Else
            nRepeat = Convert.ToString(UcUpdate.cmbRpt.Value).Replace(",", ".")
        End If

        With UcUpdate
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            Dim sStatus As Int32
            If Me.chkBackups.Checked = False Then
                sStatus = 0
            ElseIf Me.chkBackups.Checked = True And rbSimple.Checked = True Then
                sStatus = 1
            ElseIf chkBackups.Checked = True And rbAdvanced.Checked = True And chkZip.Checked = False Then
                sStatus = 2
            ElseIf chkBackups.Checked = True And rbAdvanced.Checked = True And chkZip.Checked = True Then
                sStatus = 3
            End If

            SQL = "UPDATE ScheduleAttr SET " & _
                "Frequency = '" & .sFrequency & "', " & _
                "EndDate = '" & .m_endDate & "', " & _
                "StartDate = '" & .m_startDate & "'," & _
                "NextRun = '" & .m_nextRun & "', " & _
                "StartTime = '" & .m_startTime & "', " & _
                "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "RepeatInterval = '" & nRepeat & "'," & _
                "Status = " & sStatus & "," & _
                "RepeatUntil = '" & .m_repeatUntil & "'," & _
                "BPath = '" & SQLPrepare(txtPath.Text) & "'," & _
                "BZipName = '" & SQLPrepare(txtZipName.Text) & "', " & _
                "CalendarName = '" & SQLPrepare(.cmbCustom.Text) & "', " & _
                "UseException = " & Convert.ToInt32(.chkException.Checked) & "," & _
                "ExceptionCalendar ='" & SQLPrepare(.cmbException.Text) & "', " & _
                "RepeatUnit ='" & UcUpdate.m_RepeatUnit & "' " & _
                "where scheduleid = 11"

            clsMarsData.WriteData(SQL)

        End With

        MessageBox.Show("Scheduled backup saved successfully", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)

        'End If
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        setError(sender, String.Empty)
    End Sub

    Private Sub chkBackups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBackups.CheckedChanged


        Me.grpBackup.Enabled = chkBackups.Checked
        cmdSaveBackup.Enabled = chkBackups.Checked

        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("AutoBackUp", Convert.ToInt32(chkBackups.Checked))

        If chkBackups.Checked = False Then
            clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 0 WHERE ScheduleID =11")
        Else
            If Me.rbAdvanced.Checked = True Then
                clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 1 WHERE ScheduleID =11")
            End If
        End If
    End Sub

    Private Sub tmScheduleMon_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmScheduleMon.Elapsed
        tmScheduleMon.Stop()


        Dim selItem As DevComponents.AdvTree.Node

        If lsvTaskManager.SelectedNodes.Count > 0 Then
            selItem = Me.lsvTaskManager.SelectedNodes(0)
        End If

        ScheduleMonitor() '//get schedules that are due

        Me.ProcessMonitor()

        ListenForMessages() '//get the live status

        updateDuration()


        If selItem IsNot Nothing Then
            Dim nn As DevComponents.AdvTree.Node = lsvTaskManager.FindNodeByDataKey(selItem.DataKey)

            If nn IsNot Nothing Then
                lsvTaskManager.SelectedNode = nn
                nn.EnsureVisible()
            End If
        End If

        Dim sched As schedulerInfo = New schedulerInfo

        advProp.SelectedObject = sched

        tmScheduleMon.Start()
    End Sub



    Private Sub RestoreColumns(ByVal lsv As ListView)
        Try
            Dim keyFile As String = sAppPath & lsv.Name & ".cols"

            If IO.File.Exists(keyFile) = False Then Return

            Dim dt As DataTable = New DataTable("Columns")

            dt.ReadXml(keyFile)

            For I As Integer = 0 To lsv.Columns.Count - 1
                Dim rows() As DataRow = dt.Select("ColumnID =" & I)

                If rows IsNot Nothing AndAlso rows.Length > 0 Then
                    Dim row As DataRow = rows(0)

                    lsv.Columns(I).Width = row("ColumnWidth")
                End If
            Next
        Catch : End Try
    End Sub


    Private Sub cmdAddSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddSchedule.Click
        If tvObjects.SelectedNode Is Nothing Then Return

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim sFreq As String
        Dim sType As String
        Dim nID As Integer
        Dim nLast As Date
        Dim nNext As Date
        Dim ObjectID As Integer

        For Each nn As DevComponents.AdvTree.Node In tvObjects.SelectedNodes
            sType = getScheduleTypeString(nn.Tag)
            nID = nn.DataKey

            Select Case sType.ToLower
                Case "folder", "desktop", "report", "package"
                    If optrDaily.Checked = True Then
                        sFreq = "Daily"
                    ElseIf optrWeekly.Checked = True Then
                        sFreq = "Weekly"
                    ElseIf optrMonthly.Checked = True Then
                        sFreq = "Monthly"
                    ElseIf optrYearly.Checked = True Then
                        sFreq = "Yearly"
                    End If

                    nLast = ConDate(Now.Date) & " " & ConTime(dtRunTime.Value)

                    nNext = CTimeZ(nLast, dateConvertType.WRITE)
                Case Else
                    MessageBox.Show("The selected object (" & nn.Text & ") does not require refreshing. Please select a folder, report or package", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return
            End Select


            sCols = "RefreshID,ObjectType,Frequency,ObjectName,ObjectID,NextRun,RepeatEvery,StartTime"

            ObjectID = clsMarsData.CreateDataID("refresherattr", "refreshid")

            sVals = ObjectID & "," & _
            "'" & sType & "'," & _
            "'" & sFreq & "'," & _
            "'" & SQLPrepare(tvObjects.SelectedNode.Text) & "'," & _
            nID & "," & _
            "'" & ConDateTime(nNext) & "'," & _
            txtzRpt.Value & "," & _
            "'" & ConTime(CTimeZ(dtRunTime.Value, dateConvertType.WRITE)) & "'"

            SQL = "INSERT INTO  RefresherAttr (" & sCols & ") VALUES (" & sVals & ")"

            If clsMarsData.WriteData(SQL) = True Then
                Dim oItem As New ListViewItem

                oItem.Text = sType
                oItem.Tag = ObjectID

                With oItem.SubItems
                    .Add(tvObjects.SelectedNode.Text)
                    .Add(sFreq)
                    .Add("")
                    .Add(CType(nNext, String))
                    .Add(txtzRpt.Value)
                End With

                lsvSchedules.Items.Add(oItem)
            End If
        Next
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Return

        For Each olsv As ListViewItem In lsvSchedules.SelectedItems
            Dim nID As Integer = olsv.Tag

            Dim SQL As String = "DELETE FROM RefresherAttr WHERE RefreshID =" & nID

            If clsMarsData.WriteData(SQL) = True Then
                olsv.Remove()
            End If
        Next
    End Sub

    Private Sub optrDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrDaily.CheckedChanged
        If optrDaily.Checked = True Then
            Label12.Text = "days"
        End If
    End Sub

    Private Sub optrWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrWeekly.CheckedChanged
        If optrWeekly.Checked = True Then
            Label12.Text = "weeks"
        End If
    End Sub

    Private Sub optrMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrMonthly.CheckedChanged
        If optrMonthly.Checked = True Then
            Label12.Text = "months"
        End If
    End Sub

    Private Sub optrYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrYearly.CheckedChanged
        If optrYearly.Checked = True Then
            Label12.Text = "years"
        End If
    End Sub


    Private Sub lsvSchedules_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvSchedules.ColumnClick

        Me.lsvSchedules.ListViewItemSorter = New ListViewComparer(e.Column)

        'Dim new_sorting_column As ColumnHeader = _
        'lsvSchedules.Columns(e.Column)

        '' Figure out the new sorting order.
        'Dim sort_order As System.Windows.Forms.SortOrder
        'If m_SortingColumn Is Nothing Then
        '    ' New column. Sort ascending.
        '    sort_order = SortOrder.Ascending
        'Else
        '    ' See if this is the same column.
        '    If new_sorting_column.Equals(m_SortingColumn) Then
        '        ' Same column. Switch the sort order.
        '        If m_SortingColumn.Text.StartsWith("> ") Then
        '            sort_order = SortOrder.Descending
        '        Else
        '            sort_order = SortOrder.Ascending
        '        End If
        '    Else
        '        ' New column. Sort ascending.
        '        sort_order = SortOrder.Ascending
        '    End If

        '    ' Remove the old sort indicator.
        '    m_SortingColumn.Text = _
        '        m_SortingColumn.Text.Substring(2)
        'End If

        '' Display the new sort order.
        'm_SortingColumn = new_sorting_column
        'If sort_order = SortOrder.Ascending Then
        '    m_SortingColumn.Text = "> " & m_SortingColumn.Text
        'Else
        '    m_SortingColumn.Text = "< " & m_SortingColumn.Text
        'End If

        '' Create a comparer.
        'lsvSchedules.ListViewItemSorter = New _
        '    ListViewComparer(e.Column, sort_order)

        '' Sort.
        'lsvSchedules.Sort()
    End Sub

    Private Sub tmEmailQueue_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmEmailQueue.Elapsed
        EmailQueue()
    End Sub





    Private Sub chkPackageFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPackageFilter.CheckedChanged
        If optPackageAsc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " ASC ", _
            chkPackageFilter.Checked)
        Else
            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " DESC ", _
            chkPackageFilter.Checked)
        End If
    End Sub



    Private Sub frmSystemMonitor_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.tmEmailQueue.Enabled = False
        Me.tmScheduleMon.Enabled = False
    End Sub

    Private Sub chkAutoRefresh_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoRefresh.CheckedChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("AutoRefresh", Convert.ToInt32(chkAutoRefresh.Checked))

        ' Panel1.Enabled = chkAutoRefresh.Checked
    End Sub





    Private Sub mnuPackageClearSel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageClearSel.Click
        If tvPackage.SelectedNode Is Nothing Then Return

        Dim nID As Integer

        Try
            Int32.Parse(tvPackage.SelectedNode.Tag)

            nID = tvPackage.SelectedNode.Tag
        Catch ex As Exception
            Return
        End Try

        Dim SQL As String

        SQL = "DELETE FROM ScheduleHistory WHERE PackID =" & nID

        clsMarsData.WriteData(SQL)

        Try
            tvPackage.SelectedNode.Nodes.Clear()
        Catch : End Try
    End Sub

    Private Sub cmdPackClear_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdPackClear.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        cmnuPackageClear.Show(cmdPackClear, New Point(e.X, e.Y))
    End Sub




    Private Sub chkOneEmailPerSchedule_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOneEmailPerSchedule.CheckedChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("OneEmailPerFailure", Convert.ToInt32(chkOneEmailPerSchedule.Checked))
    End Sub

    Private Sub txtKeepSingle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepScheduleHistory.ValueChanged
        Dim oUI As New clsMarsUI
        Dim key As String = "KeepSingle"

        If optAutomationHistory.Checked Then
            key = "KeepAuto"
        ElseIf optEventHistory.Checked Then
            key = "KeepEvent"
        ElseIf optEventPackageHistory.Checked Then
            key = "KeepEventPackage"
        ElseIf optReportHistory.Checked Then
            key = "KeepSingle"
        ElseIf optPackageHistory.Checked Then
            key = "KeepPackage"
        Else
            Return
        End If

        oUI.SaveRegistry(key, txtKeepScheduleHistory.Value)

        clsMarsData.DataItem.ClearHistory()
    End Sub



    Private Sub cmdErrorLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdErrorLog.Click
        If MessageBox.Show("Clear the entire error log? This cannot be undone.", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            clsMarsData.WriteData("DELETE FROM EventLogAttr")

            Me.lsvErrorLog.Items.Clear()

            For Each ctrl As Control In pnlErrorLog.Controls
                If TypeOf ctrl Is TextBox Then
                    Dim txt As TextBox = CType(ctrl, TextBox)

                    txt.Text = ""
                End If
            Next
        End If
    End Sub

    Private Sub txtKeepLogs_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepLogs.TextChanged, txtKeepLogs.ValueChanged
        oUI.SaveRegistry("KeepLogs", txtKeepLogs.Value)
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExecute.Click
        Dim sType As String
        Dim nID As Int32
        Dim oItem As DevComponents.AdvTree.Node
        Dim oT As New clsMarsThreading

        Try
            If lsvTaskManager.SelectedNodes.Count = 0 Then Return

            AppStatus(True)

            For Each oItem In lsvTaskManager.SelectedNodes
                sType = oItem.Tag
                nID = oItem.DataKey



                Select Case sType.ToLower
                    Case "report"
                        oT.xReportID = nID

                        oSchedule.TaskManager("Add", nID)

                        Dim oRs As ADODB.Recordset
                        Dim SQL As String

                        SQL = "SELECT Dynamic, Bursting FROM ReportAttr WHERE ReportID =" & nID

                        oRs = clsMarsData.GetData(SQL)

                        If Not oRs Is Nothing Then
                            If oRs.EOF = False Then
                                If IsNull(oRs(0).Value, 0) = "1" Then
                                    oT.DynamicScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EXECUTE)
                                ElseIf IsNull(oRs(1).Value, 0) = "1" Then
                                    oT.BurstingScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EXECUTE)
                                Else
                                    oT.SingleScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EXECUTE)
                                End If
                            End If
                        End If
                    Case "package"
                        oT.xPackID = nID

                        oSchedule.TaskManager("Add", , nID)

                        oT.PackageScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EXECUTE)
                    Case "automation"
                        oT.xAutoID = nID

                        oT.AutoScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EXECUTE)
                    Case "event-package", "eventpackage"
                        oT.xPackID = nID

                        oT.EventPackageScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.EXECUTE)
                    Case "event"
                        MessageBox.Show("The selected schedule is already executing!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                End Select
                Try
                    _RemoveFromQueue(oItem.Text, nID, sType, False)
                Catch : End Try
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        Finally
            AppStatus(False)
        End Try
    End Sub



    Private Sub cmdRemoveSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveSchedule.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Function getRowRange(page As Integer) As String
        Dim startRow As Integer = page * 100
        Dim endRow As Integer = startRow + 100

        Dim rangeSQL As String = "WHERE rownumber BETWEEN " & startRow & " AND " & endRow

        Return rangeSQL
    End Function

    Private Function getPagesInErrorLog() As Integer

        If errorLogPageCount = -1 Then
            Dim SQL As String = "SELECT COUNT(*) FROM eventlogattr WHERE errorseverity =0"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim count As Integer = 0

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                count = oRs(0).Value
                oRs.Close()
                oRs = Nothing
            End If

            Dim raw As Double = count / 100
            Dim pageCount As Integer = Math.Round(raw + 0.4)

            errorLogPageCount = pageCount

            Return pageCount
        Else
            Return errorLogPageCount
        End If
    End Function

    Private Sub readErrorLog(Optional page As Integer = 1)
        Try

            If page < 1 Then page = 1

            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim I As Integer = 0

            Dim exceptionRange As Integer = (page - 1) * 100

            If exceptionRange > 0 Then
                If supportMode = False Then
                    SQL = "SELECT TOP 100 * FROM eventlogattr WHERE eventid NOT IN (SELECT TOP " & exceptionRange & " eventid FROM eventlogattr WHERE errorseverity = 0 ORDER BY entrydate DESC) AND errorseverity = 0 ORDER BY entrydate DESC"
                Else
                    SQL = "SELECT TOP 100 * FROM eventlogattr WHERE eventid NOT IN (SELECT TOP " & exceptionRange & " eventid FROM eventlogattr ORDER BY entrydate DESC) ORDER BY entrydate DESC"
                End If
            Else
                If supportMode = False Then
                    SQL = "SELECT TOP 100 * FROM eventlogattr WHERE errorseverity = 0 ORDER BY entrydate DESC"
                Else
                    SQL = "SELECT TOP 100 * FROM eventlogattr ORDER BY entrydate DESC"
                End If
            End If

            Dim errInfo As Exception

            oRs = clsMarsData.GetData(SQL, , errInfo)

            If oRs Is Nothing Then
                If errInfo IsNot Nothing Then Throw errInfo
            End If

            gerrItemList = Nothing

            Me.lsvErrorLog.Items.Clear()

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim oItem As ListViewItem = New ListViewItem
                ReDim Preserve Me.gerrItemList(I)

                With oItem
                    .Text = IsNull(oRs("pcname").Value, "")
                    .Tag = oRs("eventid").Value

                    With .SubItems
                        .Add(CType(oRs("entrydate").Value, String)) '1
                        .Add(IsNull(oRs("schedulename").Value, "")) '2
                        .Add(IsNull(oRs("errordesc").Value, "")) '3
                        .Add(IsNull(oRs("errornumber").Value, 0)) '4
                        .Add(IsNull(oRs("errorsource").Value, "")) '5
                        .Add(IsNull(oRs("errorline").Value, "")) '6 
                        .Add(IsNull(oRs("errorsuggestion").Value)) '7
                    End With
                End With

                Me.lsvErrorLog.Items.Add(oItem)

                Me.gerrItemList(I) = oItem

                I += 1
                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub



    Private Sub LoadScheduleRetries()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sCol As String

10:     SQL = "SELECT * FROM RetryTracker r INNER JOIN ScheduleAttr s on r.ScheduleID = s.ScheduleID"

20:     oRs = clsMarsData.GetData(SQL)

30:     Me.lsvRetry.Items.Clear()

40:     Me.lsvRetry.BeginUpdate()

50:     Try
60:         If oRs IsNot Nothing Then
70:             Do While oRs.EOF = False
80:                 Dim item As ListViewItem = New ListViewItem
                    Dim imageIndex As Integer = 0
                    Dim scheduleName As String = ""
                    Dim retryID As Integer = oRs("retryid").Value
                    Dim scheduleID As Integer
                    Dim retryNumber, retryCount As Integer
                    Dim nID As Integer = 0
                    Dim lastRetry As String = ConDateTime(oRs("entrydate").Value)

                    Dim reportID, packID, autoID, eventPackID As Integer

90:                 reportID = IsNull(oRs("reportid").Value, 0)
100:                packID = IsNull(oRs("packid").Value, 0)
110:                autoID = IsNull(oRs("autoid").Value, 0)
120:                eventPackID = IsNull(oRs("eventpackid").Value, 0)
130:                retryNumber = IsNull(oRs("retrynumber").Value, 1)

140:                If reportID <> 0 Then
150:                    scheduleName = clsMarsScheduler.globalItem.GetScheduleName(reportID, clsMarsScheduler.enScheduleType.REPORT)
160:                    If clsMarsData.IsScheduleDataDriven(reportID, "report") Then
170:                        imageIndex = 10
180:                    ElseIf clsMarsData.IsScheduleDynamic(reportID, "report") Then
190:                        imageIndex = 5
200:                    ElseIf clsMarsData.IsScheduleBursting(reportID) Then
210:                        imageIndex = 4
220:                    Else
230:                        imageIndex = 1
                        End If

240:                    scheduleID = clsMarsScheduler.GetScheduleID(reportID)

250:                    SQL = "SELECT Retry FROM ReportAttr WHERE ReportID =" & reportID

                        sCol = "ReportID"
                        nID = reportID
260:                ElseIf packID <> 0 Then
270:                    scheduleName = clsMarsScheduler.globalItem.GetScheduleName(packID, clsMarsScheduler.enScheduleType.PACKAGE)

280:                    If clsMarsData.IsScheduleDataDriven(packID, "package") Then
290:                        imageIndex = 9
300:                    ElseIf clsMarsData.IsScheduleDynamic(packID, "package") Then
310:                        imageIndex = 3
320:                    Else
330:                        imageIndex = 0
                        End If

340:                    scheduleID = clsMarsScheduler.GetScheduleID(, packID)

350:                    SQL = "SELECT Retry FROM PackageAttr WHERE PackID =" & packID

                        sCol = "PackID"
                        nID = packID
360:                Else
370:                    GoTo skip
                    End If

380:                item.Text = scheduleName
390:                item.ImageIndex = imageIndex

                    Dim rsRetry As ADODB.Recordset = clsMarsData.GetData(SQL)
                    Dim lastResult As String = ""

400:                If rsRetry IsNot Nothing Then
410:                    If rsRetry.EOF = False Then
420:                        retryCount = IsNull(rsRetry("retry").Value, 3)
                        End If

430:                    rsRetry.Close()
                    End If

                    Dim rsHis As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM ScheduleHistory WHERE " & sCol & " = " & nID & _
                    " AND Success = 0 ORDER BY EntryDate DESC")

440:                If rsHis IsNot Nothing Then
450:                    If rsHis.EOF = False Then
460:                        lastResult = IsNull(rsHis("errmsg").Value)
470:                        item.SubItems.Add(lastRetry)
480:                        item.SubItems.Add(retryNumber & " of " & retryCount)
490:                        item.SubItems.Add(lastResult)
500:                        item.Tag = retryID
510:                        Me.lsvRetry.Items.Add(item)
                        End If
                    End If
skip:
520:                oRs.MoveNext()
530:            Loop

540:            oRs.Close()
            End If

550:        SQL = "SELECT * FROM RetryTracker r INNER JOIN EventAttr6 e ON r.EventID = e.EventID"

560:        oRs = clsMarsData.GetData(SQL)

570:        If oRs IsNot Nothing Then
580:            Do While oRs.EOF = False
590:                Dim item As ListViewItem = New ListViewItem
                    Dim eventName As String = oRs("eventname").Value
                    Dim eventID As Integer = clsMarsData.GetColumnIndex("eventid", oRs)
                    Dim retryID = oRs("retryID").Value
                    Dim retryNumber, retryCount As Integer
                    Dim lastResult As String = ""
                    Dim lastRetry As String = ConDateTime(oRs("entrydate").Value)

600:                eventID = oRs(eventID).Value
610:                retryCount = oRs("retrycount").Value
620:                retryNumber = oRs("retrynumber").Value

630:                item.ImageIndex = 7
640:                item.Tag = retryID
650:                item.Text = eventName

                    Dim rsHis As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM EventHistory WHERE " & _
           "EventID=" & eventID & " AND  Status =0 ORDER BY LastFired DESC")

660:                If rsHis IsNot Nothing Then
670:                    If rsHis.EOF = False Then
680:                        lastResult = IsNull(rsHis("errmsg").Value)
690:                        item.SubItems.Add(lastRetry)
700:                        item.SubItems.Add(retryNumber & " of " & retryCount)
710:                        item.SubItems.Add(lastResult)

720:                        Me.lsvRetry.Items.Add(item)
                        End If
                    End If

730:                oRs.MoveNext()
740:            Loop

750:            oRs.Close()
            End If

            Dim sortString As String

            If Me.retrylogsortOrder = Windows.Forms.SortOrder.Ascending Then
                sortString = " ASC "
            Else
                sortString = " DESC "
            End If

            Try
                clsMarsUI.sortListView(Me.lsvRetry, sortString, retrylogSortBy, IIf(Me.retrylogSortBy = 1, True, False), False)
            Catch ex As Exception

            End Try
760:    Catch ex As Exception
770:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 1)
780:    Finally
790:        Me.lsvRetry.EndUpdate()
        End Try

    End Sub

    Private Sub ViewScheduledBackups()
        Try
            Dim oRs As New ADODB.Recordset
            Dim sPath As String = sAppPath & "backup.xml"

            If System.IO.File.Exists(sPath) Then
                oRs = oData.GetXML(sPath)

                Select Case CType(oRs("frequency").Value, String).ToLower
                    Case "daily"
                        optDaily.Checked = True
                    Case "weekly"
                        optWeekly.Checked = True
                    Case "monthly"
                        optMonthly.Checked = True
                    Case "yearly"
                        optYearly.Checked = True
                End Select

                dtBackup.Value = ConDate(Now.Date) & " " & ConTime(oRs("runtime").Value)
                txtInterval.Value = oRs("repeatevery").Value
                txtPath.Text = oRs("backuppath").Value

                Try
                    txtKeep.Text = oRs("keepfor").Value
                Catch ex As Exception
                    txtKeep.Text = 5
                End Try
            End If


            'Pull the backup information for ScheduleAttr
            Dim SQL1 As String = "Select * from ScheduleAttr where ScheduleID = 11"
            Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL1)
            Try

                If Not oRs1 Is Nothing Then
                    If oRs1.EOF = True Then
                        UcUpdate.EndDate.Value = Date.Now.AddYears(10)

                    Else
                        Select Case oRs1("frequency").Value.ToLower
                            Case "daily"
                                UcUpdate.optDaily.Checked = True
                            Case "weekly"
                                UcUpdate.optWeekly.Checked = True
                            Case "week-daily"
                                UcUpdate.optWeekDaily.Checked = True
                            Case "monthly"
                                UcUpdate.optMonthly.Checked = True
                            Case "yearly"
                                UcUpdate.optYearly.Checked = True
                            Case "custom"
                                UcUpdate.optCustom.Checked = True
                                UcUpdate.cmbCustom.Text = IsNull(oRs1("calendarname").Value)
                            Case "other"
                                UcUpdate.optOther.Checked = True
                            Case "none"
                                UcUpdate.optNone.Checked = True
                        End Select

                        Try
                            If IsNull(oRs1("status").Value, 5) = 1 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = True
                                Me.rbAdvanced.Checked = False
                                Me.grpAdvanced.Visible = False
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            ElseIf IsNull(oRs1("status").Value, 5) = 2 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = False
                                Me.rbAdvanced.Checked = True
                                Me.grpAdvanced.Visible = True
                                Me.grpAdvanced.BringToFront()
                                Me.grpSimple.Visible = False
                                Me.chkZip.Checked = False
                                Me.txtZipName.Enabled = False
                            ElseIf IsNull(oRs1("status").Value, 5) = 3 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = False
                                Me.rbAdvanced.Checked = True
                                Me.grpAdvanced.Visible = True
                                Me.grpAdvanced.BringToFront()
                                Me.grpSimple.Visible = False
                                Me.chkZip.Checked = True
                                Me.txtZipName.Enabled = True
                            ElseIf IsNull(oRs1("status").Value, 5) = 0 Then
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            ElseIf IsNull(oRs1("status").Value, 5) = 5 Then
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            End If
                        Catch ex As Exception
                            Me.chkBackups.Checked = True
                        End Try

                        grpBackup.Enabled = chkBackups.Checked
                        cmdSaveBackup.Enabled = chkBackups.Checked

                        Try
                            UcUpdate.m_RepeatUnit = IsNull(oRs1("repeatunit").Value, "hours")
                        Catch ex As Exception
                            UcUpdate.m_RepeatUnit = "hours"
                        End Try

                        ' UcUpdate.btnApply = Me.cmdSaveBackup

                        UcUpdate.ScheduleID = 11


                        Try
                            Dim d As Date

                            d = oRs1("enddate").Value

                            If d.Year >= 3004 Then
                                UcUpdate.EndDate.Enabled = False
                                UcUpdate.chkNoEnd.Checked = True
                            End If

                        Catch : End Try

                        Try
                            Dim repeatUntil As String = IsNull(oRs1("repeatuntil").Value, Now)

                            If repeatUntil.Length > 8 Then
                                UcUpdate.RepeatUntil.Value = ConDateTime(CTimeZ(repeatUntil, dateConvertType.READ))
                            Else
                                UcUpdate.RepeatUntil.Value = CTimeZ(Now.Date & " " & repeatUntil, dateConvertType.READ)
                            End If
                        Catch : End Try

                        UcUpdate.NextRun.Value = CTimeZ(oRs1("nextrun").Value, dateConvertType.READ)
                        UcUpdate.EndDate.Value = CTimeZ(oRs1("enddate").Value, dateConvertType.READ)
                        UcUpdate.RunAt.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & oRs1("starttime").Value), dateConvertType.READ)
                        UcUpdate.NextRunTime.Value = CTimeZ(oRs1("nextrun").Value, dateConvertType.READ)
                        UcUpdate.chkRepeat.Checked = Convert.ToBoolean(oRs1("repeat").Value)
                        UcUpdate.cmbRpt.Text = IsNonEnglishRegionRead(oRs1("repeatinterval").Value)

                        UcUpdate.StartDate.Value = ConDate(oRs1("startdate").Value)

                        UcUpdate.cmbException.Text = IsNull(oRs1("exceptioncalendar").Value)

                        Try
                            UcUpdate.chkException.Checked = Convert.ToBoolean(oRs1("useexception").Value)
                        Catch
                            UcUpdate.chkException.Checked = False
                        End Try
                    End If

                    If UcUpdate.cmbRpt.Text = "" Then UcUpdate.cmbRpt.Text = 0.25

                    Me.txtZipName.Text = IsNull(oRs1("bzipname").Value)


                    oRs1.Close()

                End If
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub
    Private Sub ViewRefreshSchedule()
        Dim oUI As New clsMarsUI
        Dim oItem As ListViewItem

        oUI.BuildAdvTree(tvObjects, True)

        Dim oRs As ADODB.Recordset

        Dim SQL As String

        SQL = "SELECT * FROM RefresherAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvSchedules.Items.Clear()

        Do While oRs.EOF = False
            oItem = lsvSchedules.Items.Add(oRs("objecttype").Value)

            With oItem
                With .SubItems
                    .Add(oRs("objectname").Value)
                    .Add(oRs("frequency").Value)
                    .Add(CType(IsNull(oRs("lastrun").Value), String))

                    Try
                        .Add(CType(CTimeZ(oRs("nextrun").Value, dateConvertType.READ), String))
                    Catch
                        .Add(ConDateTime(Now))
                    End Try

                    .Add(CType(oRs("repeatevery").Value, String))
                End With

                .Tag = oRs("refreshid").Value
            End With
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Private Sub ListReceipts(Optional ByVal filter As String = "")
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim imagePos As Integer = 0
            Dim scheduleName As String = ""

            lsvReadReciepts.Items.Clear()


            SQL = "SELECT * FROM ReadReceiptsLog r INNER JOIN DestinationAttr d ON r.DestinationID = d.DestinationID"

            If filter.Length > 0 And filter <> "None" Then
                SQL &= " WHERE RRStatus ='" & filter & "'"
            End If

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                SQL = ""

                Dim item As ListViewItem = New ListViewItem

                Dim nReportID As Integer = IsNull(oRs("reportid").Value, 0)
                Dim nPackID As Integer = IsNull(oRs("packid").Value, 0)

                If nReportID > 0 Then
                    imagePos = 0

                    SQL = "SELECT ReportTitle FROM ReportAttr WHERE ReportID =" & nReportID
                ElseIf nPackID > 0 Then

                    SQL = "SELECT PackageName FROM PackageAttr WHERE PackID =" & nPackID

                    imagePos = 1
                End If

                If SQL.Length = 0 Then Continue Do

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs1 Is Nothing Then Continue Do

                If oRs1.EOF = True Then Continue Do

                scheduleName = oRs1.Fields(0).Value

                oRs1.Close()

                item.Text = scheduleName
                item.ImageIndex = imagePos

                With item.SubItems
                    .Add(oRs("emailsubject").Value)
                    .Add(oRs("sentto").Value)
                    .Add(CType(CTimeZ(oRs("entrydate").Value, dateConvertType.READ), String))
                    .Add(CType(CTimeZ(oRs("nextcheck").Value, dateConvertType.READ), String))
                    .Add(oRs("rrstatus").Value)
                    .Add(IsNull(CTimeZ(oRs("lastcheck").Value, dateConvertType.READ), ""))
                End With

                item.Tag = oRs("logid").Value

                lsvReadReciepts.Items.Add(item)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub mnuReceived_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReceived.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus ='Received' WHERE LogID =" & logid) = True Then

                item.SubItems(5).Text = "Received"

            Else
                Exit For
            End If
        Next

        Me.ListReceipts(cmbFilterRR.Text)
    End Sub

    Private Sub mnuPending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPending.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus ='Pending' WHERE LogID =" & logid) = True Then

                item.SubItems(5).Text = "Pending"

            Else
                Exit For
            End If
        Next

        Me.ListReceipts(cmbFilterRR.Text)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("DELETE FROM ReadReceiptsLog WHERE LogID =" & logid) = True Then

                item.Remove()
            Else
                Exit For
            End If
        Next
    End Sub


    Private Sub cmbFilterRR_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFilterRR.SelectedIndexChanged

        Me.ListReceipts(cmbFilterRR.Text)


    End Sub


    Private Sub frmSystemMonitor_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        On Error Resume Next
        clsMarsUI.MainUI.SaveRegistry("SysMonSize", Me.Width & "," & Me.Height)
    End Sub

    Private Sub txtDeleteOlder_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDeleteOlder.ValueChanged, txtDeleteOlder.TextChanged, cmbValueUnit.SelectedIndexChanged
        If IsLoaded = False Then Return

        clsMarsUI.MainUI.SaveRegistry("EmailQueueDeleteAfter", txtDeleteOlder.Text & " " & Me.cmbValueUnit.Text)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub menuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub


    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            grpAdvanced.Visible = False
            grpSimple.Visible = True
            grpSimple.BringToFront()
            oUI.SaveRegistry("AdvancedBackupSchedule", 0)
        End If

    End Sub

    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            grpAdvanced.Visible = True
            grpSimple.Visible = False
            grpAdvanced.BringToFront()
            Label9.Text = "days"
            oUI.SaveRegistry("AdvancedBackupSchedule", 1)
        End If
    End Sub

    Private Sub chkZip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZip.CheckedChanged
        If chkZip.Checked = True Then
            Me.txtZipName.Enabled = True
        End If
        If chkZip.Checked = False Then
            Me.txtZipName.Enabled = False
        End If
    End Sub




    Private Sub lsvLog_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvLog.ColumnClick

        Me.lsvLog.BeginUpdate()
        logsortBy = e.Column

        Dim sortString As String = " ASC"

        If logsortOrder = Windows.Forms.SortOrder.Ascending Then
            logsortOrder = Windows.Forms.SortOrder.Descending
            sortString = " DESC"
        Else
            logsortOrder = Windows.Forms.SortOrder.Ascending
            sortString = " ASC"
        End If

        clsMarsUI.sortListView(Me.lsvLog, sortString, logsortBy, IIf(logsortBy = 3, True, False))

        Me.lsvLog.EndUpdate()
        Me.tmScheduleMon.Start()
    End Sub

    Private Sub lsvLog_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvLog.DoubleClick
        If lsvLog.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvLog.SelectedItems(0)
        Dim path As String = ""

        Try
            path = oItem.SubItems(4).Text
        Catch ex As Exception
            Return
        End Try

        Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

        If path <> "" Then
            Try
                If convertToMsg = False Then
                    Process.Start(path)
                Else
                    path = clsMarsMessaging.convertToMsg(path)

                    If path IsNot Nothing Then Process.Start(path)
                End If
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6565)
            End Try
        End If

    End Sub


    Private Sub DeleteSelectedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteSelectedToolStripMenuItem.Click
        Dim res As DialogResult = MessageBox.Show("Delete the selected messages?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            Dim items As ArrayList = New ArrayList

            If lsvLog.Visible = True Then
                For Each item As ListViewItem In Me.lsvLog.SelectedItems
                    Dim nID As Integer = item.Tag
                    Dim filePath As String = item.SubItems(4).Text

                    If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & nID) = True Then
                        Try
                            If filePath <> "" Then
                                IO.File.Delete(filePath)
                            End If
                        Catch : End Try

                        items.Add(item)
                    End If
                Next

                For Each item As ListViewItem In items
                    Me.lsvLog.Items.Remove(item)
                Next

                gItemList = Nothing
                Dim I As Integer = 0

                For Each item As ListViewItem In lsvLog.Items
                    ReDim Preserve gItemList(I)

                    gItemList(I) = item

                    I += 1
                Next
            Else
                For Each r As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                    Dim emailID As Integer = r.Cells(0).Value
                    Dim filePath As String = r.Cells(5).Value

                    If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & emailID) = True Then
                        Try
                            If filePath <> "" Then
                                IO.File.Delete(filePath)
                            End If
                        Catch : End Try
                    End If
                Next

                Me.LoadEmailLogDGV()
            End If
        End If
    End Sub

    Private Sub DeleteAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteAllToolStripMenuItem.Click
        Dim res As DialogResult = MessageBox.Show("Delete all the messages in the log?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            If clsMarsData.WriteData("DELETE FROM EmailLog") = True Then
                Dim fileDir As String = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages\")

                If IO.Directory.Exists(fileDir) Then
                    For Each s As String In IO.Directory.GetFiles(fileDir)
                        Try
                            IO.File.Delete(s)
                        Catch : End Try
                    Next
                End If

                Me.lsvLog.Items.Clear()
            End If
        End If
    End Sub

    Private Sub OpenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenToolStripMenuItem.Click
        If Me.lsvLog.Visible = True Then
            For Each item As ListViewItem In Me.lsvLog.SelectedItems
                Dim path As String = item.SubItems(4).Text

                If path <> "" Then
                    Try
                        Process.Start(path)
                    Catch ex As Exception
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6625)
                    End Try
                End If
            Next
        End If

    End Sub


    Private Sub tmScheduleMonCleaner_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmScheduleMonCleaner.Elapsed
        Try
            Dim itemsToRemove As ArrayList = New ArrayList

            For Each it As DevComponents.AdvTree.Node In Me.tvProcessing.Nodes
                Dim procID As Integer = it.Cells(4).Text

                'check the process exists
                Try
                    Dim proc As Process = Process.GetProcessById(procID)
                Catch
                    clsMarsData.WriteData("DELETE FROM TaskManager WHERE ProcessID =" & procID & " AND PCName ='" & SQLPrepare(Environment.MachineName) & "'")

                    If String.Compare(it.Cells(2).Text, Environment.MachineName, True) = 0 Then
                        itemsToRemove.Add(it)
                    End If
                End Try

                'check the ID exists in the TaskManager table

                Dim type As String = it.Tag
                Dim ID As Integer = it.DataKey
                Dim idCol As String = ""

                Dim SQL As String

                Select Case type
                    Case "report"
                        idCol = "reportid"
                    Case "package"
                        idCol = "packid"
                    Case "event-package", "eventpackage"
                        idCol = "eventpackid"
                    Case "event"
                        idCol = "eventid"
                    Case "automation", "backup", "retry"
                        idCol = "autoid"
                End Select

                SQL = "SELECT * FROM taskmanager WHERE " & idCol & " = " & ID

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = True Then
                        it.Remove()
                        clsMarsData.WriteData("DELETE FROM TaskManager WHERE " & idCol & "=" & ID)
                    End If

                    oRs.Close()
                End If
            Next

            For Each it As DevComponents.AdvTree.Node In itemsToRemove
                it.Remove()
            Next
        Catch : End Try
    End Sub

    Private Sub lsvEmailQueue_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvEmailQueue.DoubleClick

        If lsvEmailQueue.SelectedItems.Count = 0 Then Return

        For Each lsv As ListViewItem In Me.lsvEmailQueue.SelectedItems
            Try
                Dim path As String = lsv.SubItems(6).Text

                Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

                If convertToMsg = False Then
                    If path <> "" Then Process.Start(path)
                Else
                    If path <> "" Then
                        path = clsMarsMessaging.convertToMsg(path)

                        If path IsNot Nothing Then
                            Process.Start(path)
                        End If
                    End If
                End If
            Catch : End Try
        Next
    End Sub


    Private Sub bgScheduleMon_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgEmailLog.DoWork
        Me.ScheduleMonitor()
    End Sub

    Private Sub tmEmailLog_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmEmailLog.Elapsed


        Me.tmEmailLog.Stop()
        LoadEmailLog()
        Me.tmEmailLog.Start()
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click, mnuProcProperties.Click
        Dim tag As String
        Dim type As String
        Dim nID As Integer
        Dim lsv As DevComponents.AdvTree.AdvTree
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "menuitem11", True) = 0 Then
            If lsvTaskManager.SelectedNode Is Nothing Then Return

            Dim tv As DevComponents.AdvTree.Node = lsvTaskManager.SelectedNode
            type = tv.Tag
            nID = tv.DataKey
        Else
            lsv = Me.tvProcessing

            If lsv.SelectedNodes.Count = 0 Then Return

            Dim item As DevComponents.AdvTree.Node = lsv.SelectedNodes(0)

            tag = item.Tag
            type = item.Tag
            nID = item.DataKey
        End If

        Select Case type.ToLower
            Case "report"
                Dim prop As frmSingleProp = New frmSingleProp

                prop.EditSchedule(nID)
            Case "package"
                Dim prop As frmPackageProp = New frmPackageProp

                prop.EditPackage(nID)
            Case "event"
                Dim prop As frmEventProp = New frmEventProp

                prop.EditSchedule(nID)
            Case "event-package", "eventpackage"
                Dim prop As frmEventPackageProp = New frmEventPackageProp

                prop.EditPackage(nID)
            Case "automation"
                Dim prop As frmAutoProp = New frmAutoProp

                prop.EditSchedule(nID)
        End Select
    End Sub





    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text <> "" Then
            tmEmailLog.Stop()
            lsvLog.BeginUpdate()
            lsvLog.Items.Clear()

            For Each it As ListViewItem In gItemList
                If it IsNot Nothing Then
                    lsvLog.Items.Add(it)
                End If
            Next

            For Each it As ListViewItem In Me.lsvLog.Items
                Dim search(4) As String

                For I As Integer = 0 To 4
                    search(I) = it.SubItems(I).Text.ToLower
                Next

                Dim match As Boolean = False

                For I As Integer = 0 To 4
                    If search(I).Contains(txtSearch.Text.ToLower) = True Then
                        match = True
                    End If
                Next

                If match = False Then
                    it.Remove()
                End If
            Next

            lsvLog.EndUpdate()
        Else
            Me.tmEmailLog.Stop()
            lsvLog.BeginUpdate()
            lsvLog.Items.Clear()

            LoadEmailLog()
            lsvLog.EndUpdate()
            Me.tmEmailLog.Start()
        End If
    End Sub

    Private Sub lsvErrorLog_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvErrorLog.ColumnClick
        If Me.lsvErrorLog.Items.Count = 0 Then Return

        Dim oItem As ListViewItem

        If Me.lsvErrorLog.SelectedItems.Count <> 0 Then
            oItem = Me.lsvErrorLog.SelectedItems(0)
        End If

        If Me.errlogsortOrder = Windows.Forms.SortOrder.Descending Then
            clsMarsUI.sortListView(Me.lsvErrorLog, " ASC", e.Column, IIf(e.Column = 1, True, False), False)
            errlogsortOrder = Windows.Forms.SortOrder.Ascending
        Else
            clsMarsUI.sortListView(Me.lsvErrorLog, " DESC", e.Column, IIf(e.Column = 1, True, False), False)
            errlogsortOrder = Windows.Forms.SortOrder.Descending
        End If

        If oItem Is Nothing Then
            Me.lsvErrorLog.Items(0).Selected = True
        Else
            For Each oIt As ListViewItem In Me.lsvErrorLog.Items
                If oIt.Equals(oItem) Then
                    oIt.Selected = True
                    oIt.EnsureVisible()
                End If
            Next
        End If
    End Sub

    Private Sub lsvErrorLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvErrorLog.SelectedIndexChanged
        If Me.lsvErrorLog.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvErrorLog.SelectedItems(0)

        '.Add(CType(oRs("entrydate").Value, String)) '1
        '.Add(IsNull(oRs("schedulename").Value, "")) '2
        '.Add(IsNull(oRs("errordesc").Value, "")) '3
        '.Add(IsNull(oRs("errornumber").Value, 0)) '4
        '.Add(IsNull(oRs("errorsource").Value, "")) '5
        '.Add(IsNull(oRs("errorline").Value, "")) '6 
        '.Add(IsNull(oRs("errorsuggestion").Value)) '7

        Me.txterrNo.Text = oItem.SubItems(4).Text
        Me.txterrSource.Text = oItem.SubItems(5).Text
        Me.txterrLine.Text = oItem.SubItems(6).Text
        Me.txtErrorlog.Text = oItem.SubItems(3).Text
        Me.txterrSuggest.Text = oItem.SubItems(7).Text

    End Sub

    Sub searchError()
        If txterrSearch.Text = "support1001350" Then
            Me.supportMode = True
            Me.readErrorLog()
            Return
        End If

        If txterrSearch.Text <> "" And txterrSearch.Text.Length < 3 Then Return

        If txterrSearch.Text <> "" Then

            Me.lsvErrorLog.BeginUpdate()

            lsvErrorLog.Items.Clear()

            For Each it As ListViewItem In gerrItemList
                If it IsNot Nothing Then
                    lsvErrorLog.Items.Add(it)
                End If
            Next
            '-----
            'Dim nTotal As Integer = lsvErrorLog.Items.Count
            'Dim counter As Integer = 1
            'Dim foundItem As ListViewItem
            'Dim foundItems As ArrayList = New ArrayList

            'Do
            '    foundItem = lsvErrorLog.FindItemWithText(txterrSearch.Text, True, 0)

            '    If foundItem IsNot Nothing Then
            '        foundItems.Add(foundItem)
            '        lsvErrorLog.Items.Remove(foundItem)
            '        ''console.writeline(lsvErrorLog.Items.Count)
            '    End If

            '    clsMarsUI.BusyProgress((counter / nTotal) * 100, "Searching...")
            '    counter += 1
            'Loop Until foundItem Is Nothing

            'clsMarsUI.BusyProgress(, , True)

            'lsvErrorLog.Items.Clear()

            'For Each it As ListViewItem In foundItems
            '    lsvErrorLog.Items.Add(it)
            'Next

            'lsvErrorLog.EndUpdate()
            '------
            If Me.lsvErrorLog.Items.Count > 0 Then
                For Each it As ListViewItem In Me.lsvErrorLog.Items

                    clsMarsUI.BusyProgress((it.Index / lsvErrorLog.Items.Count) * 100, "searching...")

                    Dim search(7) As String

                    For I As Integer = 0 To 7
                        search(I) = it.SubItems(I).Text.ToLower
                    Next

                    Dim match As Boolean = False

                    For I As Integer = 0 To 7
                        If search(I).Contains(txterrSearch.Text.ToLower) = True Then
                            match = True
                        End If
                    Next

                    If match = False Then
                        it.Remove()
                    End If
                Next
            End If
            lsvErrorLog.EndUpdate()

            clsMarsUI.BusyProgress(, , True)
        Else

            Me.lsvErrorLog.BeginUpdate()
            lsvErrorLog.Items.Clear()

            Me.readErrorLog()
            lsvErrorLog.EndUpdate()

        End If

        If Me.lsvErrorLog.Items.Count > 0 Then
            Me.lsvErrorLog.Items(0).Selected = True
        Else
            For Each ctrl As Control In pnlErrorLog.Controls
                If TypeOf ctrl Is TextBox Then
                    Dim txt As TextBox = CType(ctrl, TextBox)

                    txt.Text = ""
                End If
            Next
        End If

        lsvErrorLog.Refresh()
    End Sub
    Private Sub txterrSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txterrSearch.TextChanged


        searchError()

    End Sub


    Private Sub lsvEmailQueue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvEmailQueue.SelectedIndexChanged

    End Sub

    Private Sub lsvLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvLog.SelectedIndexChanged

    End Sub



    Private Sub btnRefreshRetry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshRetry.Click
        Me.btnRefreshRetry.Enabled = False
        Me.LoadScheduleRetries()
        Me.btnRefreshRetry.Enabled = True
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Dim arr As ArrayList = New ArrayList

        For Each item As ListViewItem In Me.lsvRetry.SelectedItems
            Dim retryID As Integer = item.Tag
            Dim SQL As String = "DELETE FROM RetryTracker WHERE RetryID =" & retryID

            If clsMarsData.WriteData(SQL, False, False) = True Then
                arr.Add(retryID)
            End If
        Next

        For Each n As Integer In arr
            For Each item As ListViewItem In Me.lsvRetry.Items
                If item.Tag = n Then
                    item.Remove()
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub lsvRetry_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvRetry.ColumnClick
        Try
            Me.lsvRetry.BeginUpdate()
            Me.retrylogSortBy = e.Column

            Dim sortString As String = " ASC"

            If retrylogsortOrder = Windows.Forms.SortOrder.Ascending Then
                retrylogsortOrder = Windows.Forms.SortOrder.Descending
                sortString = " DESC"
            Else
                retrylogsortOrder = Windows.Forms.SortOrder.Ascending
                sortString = " ASC"
            End If

            clsMarsUI.sortListView(Me.lsvRetry, sortString, e.Column, IIf(e.Column = 1, True, False))

            Me.lsvRetry.EndUpdate()
        Catch : End Try
    End Sub


    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            If Me.lsvErrorLog.SelectedItems.Count = 0 Then Return

            Dim txtCopy As New TextBox
            txtCopy.Multiline = True

            txtCopy.Text = "Error #: " & Me.txterrNo.Text & vbCrLf & _
            "Schedule Name: " & Me.lsvErrorLog.SelectedItems(0).SubItems(2).Text & vbCrLf & _
            "Error Source: " & Me.txterrSource.Text & vbCrLf & _
            "Error Line: " & Me.txterrLine.Text & vbCrLf & _
            "Error Suggestion: " & Me.txterrSuggest.Text & vbCrLf & _
            "Error Description: " & Me.txtErrorlog.Text

            txtCopy.SelectAll()

            txtCopy.Copy()
        Catch : End Try
    End Sub

    Private Delegate Sub updateCellDelegate(status As String, procID As Integer)
    Private Delegate Sub updateDurationDelegate()

    Private Sub updateDuration()
        Try
            If InvokeRequired = False Then
                For Each it As DevComponents.AdvTree.Node In Me.tvProcessing.Nodes

                    'update the duration
                    Try
                        Dim start As Date = it.Cells(1).Text
                        Dim duration As Double = Now.Subtract(start).TotalMinutes
                        duration = Math.Round(duration, 1)

                        it.Cells(5).Text = duration

                        Application.DoEvents()
                    Catch : End Try
                Next
            Else
                BeginInvoke(New updateDurationDelegate(AddressOf updateDuration))
            End If
        Catch : End Try
    End Sub
    Private Sub updateCell(status As String, procID As Integer)
        Try
            If InvokeRequired = False Then

                ' tvProcessing.BeginUpdate()
                For Each it As DevComponents.AdvTree.Node In Me.tvProcessing.Nodes

                    If it.Cells(4).Text = procID Then

                        it.Cells(3).Text = status

                        ''update the duration
                        'Try
                        '    Dim start As Date = it.Cells(1).Text
                        '    Dim duration As Double = Now.Subtract(start).TotalMinutes
                        '    duration = Math.Round(duration, 1)

                        '    it.Cells(5).Text = duration

                        '    Application.DoEvents()

                        '    Exit For
                        'Catch : End Try
                    End If
                Next

                ' tvProcessing.EndUpdate()
            Else
                ' BeginInvoke(New updateHistoryDelegate(AddressOf updateHistory), New Object() {nID, type})
                BeginInvoke(New updateCellDelegate(AddressOf updateCell), New Object() {status, procID})
            End If
        Catch : End Try
    End Sub



    Private Sub progressTable_ColumnChanged(ByVal sender As Object, ByVal e As System.Data.DataColumnChangeEventArgs) Handles progressTable.ColumnChanged
        'Me.tmScheduleMon.Stop()
        Try
            Dim row As DataRow = e.Row

            If e.Column.ColumnName.ToLower = "status" Then
                Dim procID As Integer = row("processid")

                updateCell(row("status"), procID)

            End If
        Catch ex As Exception

        End Try
    End Sub


    Private Sub Splitter2_Move(ByVal sender As Object, ByVal e As System.EventArgs)
        '    If Me.IsLoaded = True Then clsMarsUI.MainUI.SaveRegistry("TaskManagerLayOut", Me.GroupBox14.Height)
    End Sub

    Private Sub dgvEmailLog_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmailLog.CellClick
        Try
            Dim I As Integer = 0
            ReDim emailLogRow(I)

            For Each row As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                ReDim Preserve emailLogRow(I)
                Me.emailLogRow(I) = row.Index
                I += 1
            Next
        Catch : End Try
    End Sub

    Private Sub dgvEmailLog_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmailLog.CellDoubleClick
        Try
            Dim r As DataGridViewRow = Me.dgvEmailLog.Rows(e.RowIndex)
            Dim path As String = r.Cells(6).Value

            Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

            If path <> "" Then
                Try
                    If convertToMsg = False Then
                        Process.Start(path)
                    Else
                        path = clsMarsMessaging.convertToMsg(path)

                        If path IsNot Nothing Then Process.Start(path)
                    End If
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6565)
                End Try
            End If
        Catch : End Try
    End Sub

    Private Sub dgvEmailLog_CellValueNeeded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValueEventArgs) Handles dgvEmailLog.CellValueNeeded
        If e.RowIndex >= 0 AndAlso e.ColumnIndex = 0 Then
            e.Value = My.Resources.mail
        End If
    End Sub

    Private Sub dgvEmailLog_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvEmailLog.KeyUp
        If e.KeyCode = Keys.Delete Or e.KeyCode = Keys.Back Then
            Me.tmEmailLog.Stop()
            For Each r As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                Dim emailID As Integer = r.Cells(1).Value
                Dim filePath As String = r.Cells(6).Value

                If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & emailID) = True Then
                    Try
                        If filePath <> "" Then
                            IO.File.Delete(filePath)
                        End If
                    Catch : End Try
                End If
            Next

            Me.LoadEmailLogDGV()
            Me.tmEmailLog.Start()
        End If
    End Sub


    Private Sub emailLogFilterMan_PopupShowing(ByVal sender As Object, ByVal e As DgvFilterPopup.ColumnFilterEventArgs) Handles emailLogFilterMan.PopupShowing
        Me.tmEmailLog.Stop()
        Me.btnRefreshEmailLog.Visible = True
    End Sub

    Private Sub btnRefreshEmailLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshEmailLog.Click
        Me.btnRefreshEmailLog.Enabled = False
        Me.LoadEmailLogDGV()
        Me.btnRefreshEmailLog.Enabled = True
    End Sub

    Private Sub txtKeepErrorLogs_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepErrorLogs.ValueChanged
        If IsLoaded = True Then clsMarsUI.MainUI.SaveRegistry("KeepErrorLogs", txtKeepErrorLogs.Value)
    End Sub

    Private Sub chkRefreshEmailLog_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRefreshEmailLog.CheckedChanged
        tmEmailLog.Enabled = chkRefreshEmailLog.Checked
    End Sub

    Private Sub slRefreshInterval_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles slRefreshInterval.ValueChanged
        Me.tmScheduleMon.Interval = slRefreshInterval.Value * 1000

        Me.ToolTip1.SetToolTip(slRefreshInterval, slRefreshInterval.Value & " seconds")

        Me.ToolTip1.Show(slRefreshInterval.Value & " seconds", slRefreshInterval, 3000)

        clsMarsUI.MainUI.SaveRegistry("TaskManagerRefreshInt", slRefreshInterval.Value)
    End Sub

    Private Sub lsvTaskManager_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvTaskManager.Click

    End Sub



    Private Sub txtSearchSchedules_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearchSchedules.TextChanged
        If txtSearchSchedules.Text = "" Then
            For Each nn As DevComponents.AdvTree.Node In lsvTaskManager.Nodes
                nn.Visible = True
            Next

            slRefreshInterval.Enabled = True
            tmScheduleMon.Start()
        Else
            tmScheduleMon.Stop()
            slRefreshInterval.Enabled = False
            For Each nn As DevComponents.AdvTree.Node In lsvTaskManager.Nodes
                If nn.Text.ToLower.Contains(txtSearchSchedules.Text.ToLower) Then
                    nn.Visible = True
                Else
                    nn.Visible = False
                End If
            Next
        End If
    End Sub

    Private Sub optAutomationHistory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optPackageHistory.CheckedChanged, optReportHistory.CheckedChanged, optAutomationHistory.CheckedChanged, optEventHistory.CheckedChanged, optEventPackageHistory.CheckedChanged, optSmartFolderHistory.CheckedChanged

        Dim scheduleType As clsMarsScheduler.enScheduleType = clsMarsScheduler.enScheduleType.REPORT

        If optAutomationHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.AUTOMATION
        ElseIf optEventHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.EVENTBASED
        ElseIf optEventPackageHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.EVENTPACKAGE
        ElseIf optPackageHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.PACKAGE
        ElseIf optReportHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.REPORT
        ElseIf optSmartFolderHistory.Checked Then
            scheduleType = clsMarsScheduler.enScheduleType.SMARTFOLDER
        Else
            Return
        End If

        historyViewer.m_objectID = 0
        historyViewer.m_scheduleType = scheduleType

        historyViewer.reset()

        If scheduleType = clsMarsScheduler.enScheduleType.EVENTBASED Then
            historyViewer.DrawEventHistory()
        Else
            historyViewer.DrawScheduleHistory()
        End If

    End Sub

    Private Sub btnMoreHistoryData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoreHistoryData.Click
        Dim type As clsMarsScheduler.enScheduleType

        If optAutomationHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.AUTOMATION
        ElseIf optEventHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.EVENTBASED
        ElseIf optEventPackageHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.EVENTPACKAGE
        ElseIf optPackageHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.PACKAGE
        ElseIf optReportHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.REPORT
        ElseIf optSmartFolderHistory.Checked Then
            type = clsMarsScheduler.enScheduleType.SMARTFOLDER
        Else
            Return
        End If

        Dim historyReport As frmHistoryReport = New frmHistoryReport

        historyReport.viewReportHistory(type)

    End Sub


    Private Sub tvProcessing_ColumnResized(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvProcessing.ColumnResized
        If IsLoaded = True Then
            IsLoaded = False

            Dim tv As DevComponents.AdvTree.AdvTree = tvProcessing

            Dim keyFile As String = tv.Name & ".cols"

            Try
                Dim dt As DataTable = New DataTable("Columns")

                With dt.Columns
                    .Add("ColumnID")
                    .Add("ColumnWidth")
                End With


                Dim hs As Hashtable = New Hashtable

                For I As Integer = 0 To tv.Columns.Count - 1
                    Dim row As DataRow = dt.Rows.Add
                    row("ColumnID") = I
                    row("ColumnWidth") = tv.Columns(I).Width
                Next

                If IO.File.Exists(sAppPath & keyFile) Then
                    Try
                        IO.File.Delete(sAppPath & keyFile)
                    Catch : End Try
                End If

                dt.WriteXml(sAppPath & keyFile, XmlWriteMode.WriteSchema)

                dt.Dispose()
            Catch
            Finally
                IsLoaded = True
            End Try
        End If
    End Sub

    Private Sub lsvTaskManager_KeyDown1(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvTaskManager.KeyDown
        If e.KeyCode = Keys.F7 Then
            If Me.tmScheduleMon.Enabled = True Then
                Me.tmScheduleMon.Enabled = False
            Else
                Me.tmScheduleMon.Enabled = True
            End If
        End If
    End Sub

    Dim inserter As frmInserter = New frmInserter(99999)


    Private Sub stabSystemMonitor_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabSystemMonitor.SelectedTabChanged
        Try
            Select Case e.NewValue.Text.ToLower
                Case "schedule manager"
                    tmEmailQueue.Enabled = False
                    Me.tmEmailLog.Enabled = False
                    Me.tmScheduleMon.Enabled = True
                    Me.tmScheduleMonCleaner.Enabled = True
                    inserter.Hide()
                Case "email queue"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmEmailLog.Enabled = False
                    tmEmailQueue.Enabled = True
                    Me.tmScheduleMonCleaner.Enabled = False
                    inserter.Hide()
                Case "email log"
                    Me.tmScheduleMon.Enabled = False
                    tmEmailQueue.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False

                    Dim sort As String = clsMarsUI.MainUI.ReadRegistry("EmailLogSort", "ASC")

                    LoadEmailLog()

                    txtKeepLogs.Value = oUI.ReadRegistry("KeepLogs", 14)

                    Me.tmEmailLog.Enabled = True
                    inserter.Hide()
                Case "error log"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    tmEmailQueue.Enabled = False
                    tmEmailLog.Enabled = False

                    If System.IO.File.Exists(sAppPath & "eventlog.dat") Then
                        Me.readErrorLog()
                        If Me.lsvErrorLog.Items.Count > 0 Then
                            Me.lsvErrorLog.Items(0).Selected = True
                        End If
                    End If
                    inserter.Hide()
                Case "deferred delivery"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    tmEmailQueue.Enabled = False

                    DeferedList("ASC")
                    inserter.Hide()
                Case "schedule backup"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    tmEmailQueue.Enabled = False
                    tmEmailLog.Enabled = False

                    ViewScheduledBackups()

                    If inserter IsNot Nothing AndAlso inserter.IsDisposed = False Then
                        inserter.GetConstants(Me)
                    End If
                Case "schedule refresh"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    tmEmailQueue.Enabled = False
                    tmEmailLog.Enabled = False

                    ViewRefreshSchedule()
                    inserter.Hide()
                Case "audit trail"
                    Me.tmScheduleMon.Enabled = False
                    tmEmailQueue.Enabled = False
                    tmEmailLog.Enabled = False

                    Dim oAudit As New clsMarsAudit

                    oAudit._ViewTrail(Me.lsvAudit)
                    inserter.Hide()
                Case "read receipts"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    tmEmailQueue.Enabled = False
                    tmEmailLog.Enabled = False

                    Me.cmbFilterRR.SelectedIndex = 0
                    ListReceipts()
                    inserter.Hide()
                Case "retry view"
                    Me.tmScheduleMon.Enabled = False
                    Me.tmScheduleMonCleaner.Enabled = False
                    Me.tmEmailLog.Enabled = False
                    tmEmailQueue.Enabled = False

                    LoadScheduleRetries()

                    inserter.Hide()
            End Select
        Catch : End Try
    End Sub

    Private Sub lsvTaskManager_Resize(sender As Object, e As System.EventArgs) Handles lsvTaskManager.Resize
        LabelX2.Width = lsvTaskManager.Width
    End Sub

    Private Sub mnuProcLoc_Click(sender As Object, e As System.EventArgs) Handles mnuProcLoc.Click, MenuItem12.Click
        Dim tag, type, table, SQL, col As String
        Dim nID, nParent As Integer
        Dim lsv As DevComponents.AdvTree.AdvTree
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "menuitem12", True) = 0 Then
            lsv = Me.lsvTaskManager
        Else
            lsv = Me.tvProcessing
        End If

        If lsv.SelectedNodes.Count = 0 Then Return

        Dim item As DevComponents.AdvTree.Node = lsv.SelectedNode

        type = item.Tag
        nID = item.DataKey

        Select Case type
            Case "report"
                table = "ReportAttr"
                col = "ReportID"
            Case "package"
                table = "PackageAttr"
                col = "PackID"
            Case "event"
                table = "EventAttr6"
                col = "EventID"
            Case "event-package"
                table = "EventPackageAttr"
                col = "EventPackID"
            Case "automation"
                table = "AutomationAttr"
                col = "AutoID"
        End Select

        SQL = "SELECT Parent FROM " & table & " WHERE " & col & " = " & nID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                nParent = oRs(0).Value

                Dim folderTag As String = "Folder:" & nParent

                If selectedForm Is Nothing Then Return

                selectedForm.BringToFront()

                Dim folderNode As DevComponents.AdvTree.Node = selectedForm.tvExplorer.FindNodeByDataKey(nParent)

                If folderNode IsNot Nothing Then
                    selectedForm.tvExplorer.SelectedNode = folderNode
                    folderNode.EnsureVisible()

                    Dim itemNode As DevComponents.AdvTree.Node = selectedForm.tvMain.FindNodeByDataKey(nID)

                    If itemNode IsNot Nothing Then
                        selectedForm.tvMain.SelectedNode = itemNode
                        itemNode.EnsureVisible()
                    End If
                End If
            End If

            oRs.Close()
        End If
    End Sub

    Private Sub MenuItem14_Click(sender As System.Object, e As System.EventArgs) Handles MenuItem14.Click
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim files As String = lsvDefer.SelectedItems(0).SubItems(2).Text

        For Each File As String In files.Split(";")
            If File <> "" Then
                Process.Start(File)
            End If
        Next
    End Sub


    Private Sub lsvDefer_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDefer.DoubleClick
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim files As String = lsvDefer.SelectedItems(0).SubItems(2).Text

        For Each File As String In files.Split(";")
            If File <> "" Then
                Process.Start(File)
            End If
        Next
    End Sub

    Private Sub pageNav_NavigateNextPage(sender As Object, e As System.EventArgs) Handles pageNav.NavigateNextPage
        currentErrorLogPage += 1 '//add one to the current page


        If currentErrorLogPage >= getPagesInErrorLog() Then currentErrorLogPage = getPagesInErrorLog()

        pageNav.NextPageTooltip = "Go to Page " & IIf(currentErrorLogPage >= getPagesInErrorLog(), currentErrorLogPage, currentErrorLogPage + 1) '//the next page caption is the newpage + 1

        pageNav.PreviousPageTooltip = "Go to Page " & IIf(currentErrorLogPage = 1, 1, currentErrorLogPage - 1) '//the previous page

        pageNav.TodayTooltip = "Go to Page 1 (Current Page: " & currentErrorLogPage & " of " & getPagesInErrorLog() & ")"

        readErrorLog(currentErrorLogPage)
    End Sub

    Private Sub pageNav_NavigatePreviousPage(sender As Object, e As System.EventArgs) Handles pageNav.NavigatePreviousPage
        currentErrorLogPage -= 1 '//subtract one from the current page

        If currentErrorLogPage < 1 Then currentErrorLogPage = 1

        pageNav.PreviousPageTooltip = "Go to Page " & IIf(currentErrorLogPage = 1, 1, currentErrorLogPage - 1)

        pageNav.NextPageTooltip = "Goto Page " & currentErrorLogPage + 1

        pageNav.TodayTooltip = "Go to Page 1 (Current Page: " & currentErrorLogPage & " of " & getPagesInErrorLog() & ")"

        readErrorLog(currentErrorLogPage)
    End Sub

    Private Sub pageNav_NavigateToday(sender As Object, e As System.EventArgs) Handles pageNav.NavigateToday
        currentErrorLogPage = 1

        pageNav.TodayTooltip = "Page 1 of " & getPagesInErrorLog()
        pageNav.PreviousPageTooltip = ""
        pageNav.NextPageTooltip = "Go to Page 2"
        readErrorLog(1)
    End Sub


    Private Sub btnPurgeHistory_Click(sender As System.Object, e As System.EventArgs) Handles btnPurgeHistory.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to purge ALL schedule history now? This cannot be undone.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)

        If res = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM schedulehistory")
            clsMarsData.WriteData("DELETE FROM eventhistory")
            clsMarsData.WriteData("DELETE FROM historydetailattr")

            historyViewer.reset()
        End If
    End Sub
End Class
