Imports DevComponents
Imports DevComponents.DotNetBar
Imports Microsoft.Win32

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Friend Class frmDynamicSchedule
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oErr As New clsMarsUI
    Dim nStep As Integer = 0
    Dim oData As New clsMarsData
    Dim sLink As String = String.Empty
    Dim oMsg As New clsMarsMessaging
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim sTempKey As String = ""
    Dim oUI As New clsMarsUI
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim formsAuth As Boolean
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable
    Dim ep As New ErrorProvider
    Const S1 As String = "Step 1: Report Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Key Parameter"
    Const S4 As String = "Step 4: Parameter to Database Linking"
    Const S5 As String = "Step 5: Report Destination"
    Const S6 As String = "Step 6: Report Options"
    Const S7 As String = "Step 7: Exception Handling"
    Const S8 As String = "Step 8: Custom Tasks"
    Dim HasCancelled As Boolean = False
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents txtUrl As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvDatasources As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents UcErrorHandler1 As sqlrd.ucErrorHandler
    Friend WithEvents grpResume As System.Windows.Forms.GroupBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAutoResume As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabKeyParameter As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabLinking As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReportOptions As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabExceptions As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents UcSet As sqlrd.ucSchedule
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents UcParsList As sqlrd.ucParametersList
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents UcBlank As sqlrd.ucBlankReportX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdDbLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtDBLoc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdParameters As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdvanced As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdValidate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks1 As sqlrd.ucTasks
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbDestination As System.Windows.Forms.ComboBox
    Friend WithEvents HelpTip As System.Windows.Forms.ToolTip
    Friend WithEvents HelpLink As System.Windows.Forms.LinkLabel
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFormula As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents cmdSubreports As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents grpDynamicDest As System.Windows.Forms.GroupBox
    Friend WithEvents chkStaticDest As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDynamicSchedule))
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.txtUrl = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDbLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtDBLoc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.UcSet = New sqlrd.ucSchedule()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDestination = New System.Windows.Forms.ComboBox()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.UcParsList = New sqlrd.ucParametersList()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.HelpLink = New System.Windows.Forms.LinkLabel()
        Me.optDynamic = New System.Windows.Forms.RadioButton()
        Me.optStatic = New System.Windows.Forms.RadioButton()
        Me.cmbKey = New System.Windows.Forms.ComboBox()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.cmdSubreports = New DevComponents.DotNetBar.ButtonX()
        Me.cmdParameters = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdvanced = New DevComponents.DotNetBar.ButtonX()
        Me.txtFormula = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkStaticDest = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpDynamicDest = New System.Windows.Forms.GroupBox()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.cmdValidate = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.Step5 = New System.Windows.Forms.Panel()
        Me.UcDest = New sqlrd.ucDestination()
        Me.Step8 = New System.Windows.Forms.Panel()
        Me.UcTasks1 = New sqlrd.ucTasks()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.optOnce = New System.Windows.Forms.RadioButton()
        Me.optAll = New System.Windows.Forms.RadioButton()
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.Step6 = New System.Windows.Forms.Panel()
        Me.grpResume = New System.Windows.Forms.GroupBox()
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.chkAutoResume = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.lsvDatasources = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Step7 = New System.Windows.Forms.Panel()
        Me.UcBlank = New sqlrd.ucBlankReportX()
        Me.UcErrorHandler1 = New sqlrd.ucErrorHandler()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.HelpTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabKeyParameter = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReportOptions = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabExceptions = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabLinking = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.grpDynamicDest.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.grpResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.mnuDatasources.SuspendLayout()
        Me.Step7.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.Transparent
        Me.Step1.Controls.Add(Me.ReflectionImage1)
        Me.Step1.Controls.Add(Me.txtUrl)
        Me.Step1.Controls.Add(Me.Label6)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label10)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdDbLoc)
        Me.Step1.Controls.Add(Me.txtDBLoc)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step1, "Dynamic_Schedule.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Step1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step1.Location = New System.Drawing.Point(0, 0)
        Me.Step1.Name = "Step1"
        Me.HelpProvider1.SetShowHelp(Me.Step1, True)
        Me.Step1.Size = New System.Drawing.Size(677, 441)
        Me.Step1.TabIndex = 0
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(518, 124)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(133, 211)
        Me.ReflectionImage1.TabIndex = 27
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtUrl.Border.Class = "TextBoxBorder"
        Me.txtUrl.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtUrl, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtUrl, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtUrl.Location = New System.Drawing.Point(115, 47)
        Me.txtUrl.Name = "txtUrl"
        Me.HelpProvider1.SetShowHelp(Me.txtUrl, True)
        Me.txtUrl.Size = New System.Drawing.Size(383, 21)
        Me.txtUrl.TabIndex = 2
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label6, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(5, 50)
        Me.Label6.Name = "Label6"
        Me.HelpProvider1.SetShowHelp(Me.Label6, True)
        Me.Label6.Size = New System.Drawing.Size(283, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Report  Service URL"
        '
        'txtDesc
        '
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(114, 128)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(384, 182)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label1, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 128)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label10
        '
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label10, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label10, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(5, 319)
        Me.Label10.Name = "Label10"
        Me.HelpProvider1.SetShowHelp(Me.Label10, True)
        Me.Label10.Size = New System.Drawing.Size(102, 16)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(114, 314)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(384, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDbLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDbLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(442, 74)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdDbLoc, True)
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 4
        Me.cmdDbLoc.Text = "..."
        Me.HelpTip.SetToolTip(Me.cmdDbLoc, "Browse")
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDBLoc.Border.Class = "TextBoxBorder"
        Me.txtDBLoc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtDBLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDBLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDBLoc.Location = New System.Drawing.Point(115, 74)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtDBLoc, True)
        Me.txtDBLoc.Size = New System.Drawing.Size(317, 21)
        Me.txtDBLoc.TabIndex = 3
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(442, 21)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 1
        Me.cmdLoc.Text = "...."
        Me.HelpTip.SetToolTip(Me.cmdLoc, "Browse")
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(115, 21)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(317, 21)
        Me.txtFolder.TabIndex = 0
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(5, 24)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(5, 76)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Report  Location"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(115, 101)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(383, 21)
        Me.txtName.TabIndex = 5
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 104)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(654, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(573, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.Transparent
        Me.Step2.Controls.Add(Me.UcSet)
        Me.Step2.Controls.Add(Me.FlowLayoutPanel3)
        Me.Step2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Dynamic_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 0)
        Me.Step2.Name = "Step2"
        Me.Step2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(677, 441)
        Me.Step2.TabIndex = 0
        '
        'UcSet
        '
        Me.UcSet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcSet.Location = New System.Drawing.Point(0, 30)
        Me.UcSet.m_collaborationServerID = 0
        Me.UcSet.m_nextRun = "2012-08-01 11:38:14"
        Me.UcSet.m_RepeatUnit = ""
        Me.UcSet.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.UcSet.Name = "UcSet"
        Me.UcSet.scheduleID = 0
        Me.UcSet.scheduleStatus = True
        Me.UcSet.sFrequency = "Daily"
        Me.UcSet.Size = New System.Drawing.Size(677, 411)
        Me.UcSet.TabIndex = 4
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmbDestination)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(677, 30)
        Me.FlowLayoutPanel3.TabIndex = 3
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 3)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 21)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Destination"
        '
        'cmbDestination
        '
        Me.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestination.ItemHeight = 13
        Me.cmbDestination.Items.AddRange(New Object() {"Disk", "Email", "Fax", "FTP", "Printer", "SMS"})
        Me.cmbDestination.Location = New System.Drawing.Point(109, 3)
        Me.cmbDestination.Name = "cmbDestination"
        Me.cmbDestination.Size = New System.Drawing.Size(160, 21)
        Me.cmbDestination.Sorted = True
        Me.cmbDestination.TabIndex = 1
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.GroupBox3)
        Me.Step3.Controls.Add(Me.GroupBox2)
        Me.Step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step3, "Dynamic_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.Step3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step3.Location = New System.Drawing.Point(0, 0)
        Me.Step3.Name = "Step3"
        Me.HelpProvider1.SetShowHelp(Me.Step3, True)
        Me.Step3.Size = New System.Drawing.Size(677, 441)
        Me.Step3.TabIndex = 16
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.UcParsList)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 152)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(657, 283)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Report Parameters"
        '
        'UcParsList
        '
        Me.UcParsList.BackColor = System.Drawing.Color.Transparent
        Me.UcParsList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcParsList.Location = New System.Drawing.Point(3, 17)
        Me.UcParsList.m_formsAuth = False
        Me.UcParsList.m_rdlPath = Nothing
        Me.UcParsList.m_reportPath = Nothing
        Me.UcParsList.m_serverPassword = Nothing
        Me.UcParsList.m_serverUrl = Nothing
        Me.UcParsList.m_serverUser = Nothing
        Me.UcParsList.Name = "UcParsList"
        Me.UcParsList.Size = New System.Drawing.Size(651, 263)
        Me.UcParsList.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.HelpLink)
        Me.GroupBox2.Controls.Add(Me.optDynamic)
        Me.GroupBox2.Controls.Add(Me.optStatic)
        Me.GroupBox2.Controls.Add(Me.cmbKey)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(657, 136)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Parameters"
        '
        'HelpLink
        '
        Me.HelpLink.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.HelpLink.LinkArea = New System.Windows.Forms.LinkArea(0, 19)
        Me.HelpLink.Location = New System.Drawing.Point(248, 48)
        Me.HelpLink.Name = "HelpLink"
        Me.HelpLink.Size = New System.Drawing.Size(100, 16)
        Me.HelpLink.TabIndex = 1
        Me.HelpLink.TabStop = True
        Me.HelpLink.Text = "Click Here for Help"
        '
        'optDynamic
        '
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(8, 104)
        Me.optDynamic.Name = "optDynamic"
        Me.optDynamic.Size = New System.Drawing.Size(352, 24)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.Text = "Populate key parameter with data from a database using a query"
        '
        'optStatic
        '
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(8, 80)
        Me.optStatic.Name = "optStatic"
        Me.optStatic.Size = New System.Drawing.Size(224, 16)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate key parameter with static data"
        '
        'cmbKey
        '
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(8, 48)
        Me.cmbKey.Name = "cmbKey"
        Me.cmbKey.Size = New System.Drawing.Size(232, 21)
        Me.cmbKey.TabIndex = 0
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(416, 32)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Please select the report parameter that will be used to look up information from " & _
    "your database"
        '
        'cmdSubreports
        '
        Me.cmdSubreports.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSubreports.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSubreports.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSubreports.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdSubreports.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSubreports.Location = New System.Drawing.Point(188, 418)
        Me.cmdSubreports.Name = "cmdSubreports"
        Me.cmdSubreports.Size = New System.Drawing.Size(75, 23)
        Me.cmdSubreports.TabIndex = 8
        Me.cmdSubreports.Text = "Subreports"
        Me.cmdSubreports.Visible = False
        '
        'cmdParameters
        '
        Me.cmdParameters.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdParameters.BackColor = System.Drawing.SystemColors.Control
        Me.cmdParameters.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdParameters.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParameters.Location = New System.Drawing.Point(361, 418)
        Me.cmdParameters.Name = "cmdParameters"
        Me.cmdParameters.Size = New System.Drawing.Size(75, 23)
        Me.cmdParameters.TabIndex = 1
        Me.cmdParameters.Text = "Parameters"
        Me.cmdParameters.Visible = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdvanced.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdvanced.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdAdvanced.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdvanced.Location = New System.Drawing.Point(28, 418)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdvanced.TabIndex = 6
        Me.cmdAdvanced.Text = "Advanced"
        Me.cmdAdvanced.Visible = False
        '
        'txtFormula
        '
        '
        '
        '
        Me.txtFormula.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFormula.Location = New System.Drawing.Point(679, 326)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(32, 15)
        Me.txtFormula.TabIndex = 15
        Me.txtFormula.Visible = False
        '
        'chkStaticDest
        '
        Me.chkStaticDest.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkStaticDest.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(15, 3)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 0
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        '
        'grpDynamicDest
        '
        Me.grpDynamicDest.BackColor = System.Drawing.Color.Transparent
        Me.grpDynamicDest.Controls.Add(Me.Label9)
        Me.grpDynamicDest.Controls.Add(Me.cmdValidate)
        Me.grpDynamicDest.Controls.Add(Me.UcDSN)
        Me.grpDynamicDest.Controls.Add(Me.cmbColumn)
        Me.grpDynamicDest.Controls.Add(Me.cmbValue)
        Me.grpDynamicDest.Controls.Add(Me.Label15)
        Me.grpDynamicDest.Controls.Add(Me.cmdTest)
        Me.grpDynamicDest.Controls.Add(Me.Label13)
        Me.grpDynamicDest.Controls.Add(Me.cmbTable)
        Me.grpDynamicDest.Location = New System.Drawing.Point(4, 33)
        Me.grpDynamicDest.Name = "grpDynamicDest"
        Me.grpDynamicDest.Size = New System.Drawing.Size(566, 349)
        Me.grpDynamicDest.TabIndex = 1
        Me.grpDynamicDest.TabStop = False
        '
        'Label9
        '
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(384, 32)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Please specify the table and column that will be used to gather the xxx from the " & _
    "database"
        '
        'cmdValidate
        '
        Me.cmdValidate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdValidate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 207)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 56)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 145)
        Me.UcDSN.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 273)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 3
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 321)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 4
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 305)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(360, 16)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Please select the column that holds the xxx"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(272, 321)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 21)
        Me.cmdTest.TabIndex = 5
        Me.cmdTest.Text = "Test"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 257)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(392, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Please select the table and column that must equal the key parameter"
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 273)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 2
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.Color.Transparent
        Me.Step5.Controls.Add(Me.UcDest)
        Me.Step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step5, "Dynamic_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Step5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step5.Location = New System.Drawing.Point(0, 0)
        Me.Step5.Name = "Step5"
        Me.HelpProvider1.SetShowHelp(Me.Step5, True)
        Me.Step5.Size = New System.Drawing.Size(677, 441)
        Me.Step5.TabIndex = 18
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(677, 441)
        Me.UcDest.TabIndex = 0
        '
        'Step8
        '
        Me.Step8.BackColor = System.Drawing.Color.Transparent
        Me.Step8.Controls.Add(Me.UcTasks1)
        Me.Step8.Controls.Add(Me.GroupBox1)
        Me.Step8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step8, "Dynamic_Schedule.htm#Step8")
        Me.HelpProvider1.SetHelpNavigator(Me.Step8, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step8.Location = New System.Drawing.Point(0, 0)
        Me.Step8.Name = "Step8"
        Me.HelpProvider1.SetShowHelp(Me.Step8, True)
        Me.Step8.Size = New System.Drawing.Size(677, 441)
        Me.Step8.TabIndex = 21
        '
        'UcTasks1
        '
        Me.UcTasks1.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks1.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks1.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks1.m_defaultTaks = False
        Me.UcTasks1.m_eventBased = False
        Me.UcTasks1.m_eventID = 99999
        Me.UcTasks1.m_forExceptionHandling = False
        Me.UcTasks1.m_showAfterType = True
        Me.UcTasks1.m_showExpanded = True
        Me.UcTasks1.Name = "UcTasks1"
        Me.UcTasks1.Size = New System.Drawing.Size(677, 367)
        Me.UcTasks1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optOnce)
        Me.GroupBox1.Controls.Add(Me.optAll)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.GroupBox1.Location = New System.Drawing.Point(0, 367)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(677, 74)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 0
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(735, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.rpt"
        Me.ofd.Filter = "Crystal Reports|*.rpt|All Files|*.*"
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.Color.Transparent
        Me.Step6.Controls.Add(Me.grpResume)
        Me.Step6.Controls.Add(Me.GroupBox6)
        Me.Step6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step6, "Dynamic_Schedule.htm#Step6")
        Me.HelpProvider1.SetHelpNavigator(Me.Step6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step6.Location = New System.Drawing.Point(0, 0)
        Me.Step6.Name = "Step6"
        Me.HelpProvider1.SetShowHelp(Me.Step6, True)
        Me.Step6.Size = New System.Drawing.Size(677, 441)
        Me.Step6.TabIndex = 26
        '
        'grpResume
        '
        Me.grpResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpResume.Controls.Add(Me.Label5)
        Me.grpResume.Controls.Add(Me.Label8)
        Me.grpResume.Controls.Add(Me.chkAutoResume)
        Me.grpResume.Location = New System.Drawing.Point(8, 356)
        Me.grpResume.Name = "grpResume"
        Me.grpResume.Size = New System.Drawing.Size(561, 74)
        Me.grpResume.TabIndex = 18
        Me.grpResume.TabStop = False
        Me.grpResume.Text = "Resume with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(213, 41)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 2
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(257, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "minutes"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(164, 16)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Expire dynamic cached data after"
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        '
        '
        '
        Me.chkAutoResume.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoResume.Location = New System.Drawing.Point(6, 20)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(267, 16)
        Me.chkAutoResume.TabIndex = 0
        Me.chkAutoResume.Text = "Resume failed/errored schedules with cached data"
        Me.HelpTip.SetToolTip(Me.chkAutoResume, "If the schedule fails mid-flight, next time it runs, it will resume from where it" & _
        " left off instead of starting from the beginning")
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.lsvDatasources)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(561, 342)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Report Datasources"
        '
        'lsvDatasources
        '
        '
        '
        '
        Me.lsvDatasources.Border.Class = "ListViewBorder"
        Me.lsvDatasources.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.FullRowSelect = True
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.Location = New System.Drawing.Point(6, 16)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.lsvDatasources.Size = New System.Drawing.Size(549, 316)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
            " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 1
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Datasource Name"
        Me.ColumnHeader3.Width = 274
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Name"
        Me.ColumnHeader4.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'Step7
        '
        Me.Step7.BackColor = System.Drawing.Color.Transparent
        Me.Step7.Controls.Add(Me.UcBlank)
        Me.Step7.Controls.Add(Me.UcErrorHandler1)
        Me.Step7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.Step7, "Dynamic_Schedule.htm#Step7")
        Me.HelpProvider1.SetHelpNavigator(Me.Step7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step7.Location = New System.Drawing.Point(0, 0)
        Me.Step7.Name = "Step7"
        Me.HelpProvider1.SetShowHelp(Me.Step7, True)
        Me.Step7.Size = New System.Drawing.Size(677, 441)
        Me.Step7.TabIndex = 27
        '
        'UcBlank
        '
        Me.UcBlank.BackColor = System.Drawing.Color.Transparent
        Me.UcBlank.blankID = 0
        Me.UcBlank.blankType = "AlertandReport"
        Me.UcBlank.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.UcBlank.Location = New System.Drawing.Point(0, 75)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_showAllReportsBlankForTasks = False
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(677, 366)
        Me.UcBlank.TabIndex = 3
        '
        'UcErrorHandler1
        '
        Me.UcErrorHandler1.BackColor = System.Drawing.Color.Transparent
        Me.UcErrorHandler1.Location = New System.Drawing.Point(13, 4)
        Me.UcErrorHandler1.m_showFailOnOne = False
        Me.UcErrorHandler1.Name = "UcErrorHandler1"
        Me.UcErrorHandler1.Size = New System.Drawing.Size(541, 65)
        Me.UcErrorHandler1.TabIndex = 2
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'stabMain
        '
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel9)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabMain.Location = New System.Drawing.Point(0, 0)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 0
        Me.stabMain.Size = New System.Drawing.Size(813, 441)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabMain.TabIndex = 51
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabReportOptions, Me.tabKeyParameter, Me.tabLinking, Me.tabDestinations, Me.tabExceptions, Me.tabTasks})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabKeyParameter
        '
        'tabKeyParameter
        '
        Me.tabKeyParameter.AttachedControl = Me.SuperTabControlPanel3
        Me.tabKeyParameter.GlobalItem = False
        Me.tabKeyParameter.Image = CType(resources.GetObject("tabKeyParameter.Image"), System.Drawing.Image)
        Me.tabKeyParameter.Name = "tabKeyParameter"
        Me.tabKeyParameter.Text = "Key Parameter"
        Me.tabKeyParameter.Visible = False
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Step6)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabReportOptions
        '
        'tabReportOptions
        '
        Me.tabReportOptions.AttachedControl = Me.SuperTabControlPanel4
        Me.tabReportOptions.GlobalItem = False
        Me.tabReportOptions.Image = CType(resources.GetObject("tabReportOptions.Image"), System.Drawing.Image)
        Me.tabReportOptions.Name = "tabReportOptions"
        Me.tabReportOptions.Text = "Datasources"
        Me.tabReportOptions.Visible = False
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.Step7)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabExceptions
        '
        'tabExceptions
        '
        Me.tabExceptions.AttachedControl = Me.SuperTabControlPanel8
        Me.tabExceptions.GlobalItem = False
        Me.tabExceptions.Image = CType(resources.GetObject("tabExceptions.Image"), System.Drawing.Image)
        Me.tabExceptions.Name = "tabExceptions"
        Me.tabExceptions.Text = "Exception Handling"
        Me.tabExceptions.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.Visible = False
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.Step5)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabDestinations
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel7
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Destinations"
        Me.tabDestinations.Visible = False
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.Step8)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel9
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Custom Tasks"
        Me.tabTasks.Visible = False
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.chkStaticDest)
        Me.SuperTabControlPanel5.Controls.Add(Me.grpDynamicDest)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(677, 441)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabLinking
        '
        'tabLinking
        '
        Me.tabLinking.AttachedControl = Me.SuperTabControlPanel5
        Me.tabLinking.GlobalItem = False
        Me.tabLinking.Image = CType(resources.GetObject("tabLinking.Image"), System.Drawing.Image)
        Me.tabLinking.Name = "tabLinking"
        Me.tabLinking.Text = "Linking"
        Me.tabLinking.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 441)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(813, 32)
        Me.FlowLayoutPanel1.TabIndex = 52
        '
        'frmDynamicSchedule
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(813, 473)
        Me.Controls.Add(Me.stabMain)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.cmdSubreports)
        Me.Controls.Add(Me.cmdParameters)
        Me.Controls.Add(Me.cmdAdvanced)
        Me.Controls.Add(Me.txtFormula)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDynamicSchedule"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dynamic Schedule Wizard"
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.grpDynamicDest.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.Step8.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Step6.ResumeLayout(False)
        Me.grpResume.ResumeLayout(False)
        Me.grpResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.mnuDatasources.ResumeLayout(False)
        Me.Step7.ResumeLayout(False)
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private ReadOnly Property m_ParametersList() As ArrayList
        Get
            Dim list As ArrayList = New ArrayList

            For Each item As String In UcParsList.m_ParametersCollection
                list.Add(item)
            Next

            Return list
        End Get
    End Property

   
    Private Sub frmDynamicSchedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s2_DynamicSched) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, sender, getFeatDesc(featureCodes.s2_DynamicSched))
            Close()
            Return
        End If

        FormatForWinXP(Me)
        'Step1.Visible = True
        'Step2.Visible = False
        'Step3.Visible = False
        ''Step4.Visible = False
        'Step5.Visible = False
        'Step6.Visible = False
        'Step7.Visible = False
        'Step8.Visible = False
        'Step1.BringToFront()
        txtFolder.Focus()
        'lblStep.Text = S1

        UcDest.isDynamic = True

        If gParentID > 0 And gParent.Length > 0 Then
            Dim fld As folder = New folder(gParentID)
            txtFolder.Text = fld.getFolderPath
            txtFolder.Tag = gParentID
        End If

        txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

        oData.CleanDB()
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            browseAndPickReportFromServer(oRpt, ServerUser, ServerPassword, txtUrl, txtDBLoc, txtName, txtDesc, txtKeyWord, cmdNext, UcParsList, lsvDatasources, formsAuth)

            cmbKey.Items.Clear()

            For Each par As ucParameters In UcParsList.m_ParameterControlsCollection
                If par.m_allowMultipleValues = False Then
                    cmbKey.Items.Add(par.m_parameterName)
                End If
            Next

            Return

            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword, formsAuth)

            'If srsVersion >= "2009" Then
            '    oRpt = New rsClients.rsClient2010(txtUrl.Text)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(txtUrl.Text)
            Else
                oRpt = New rsClients.rsClient(txtUrl.Text)
            End If

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion >= "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            If txtUrl.Text.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf txtUrl.Text.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(txtUrl.Text)
            End If

            oRpt.Url = txtUrl.Text

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLoc.Text)

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdNext.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False And (TypeOf oRpt Is ReportServer.ReportingService Or TypeOf oRpt Is ReportServer_2005.ReportingService2005 Or _
                                                              TypeOf oRpt Is ReportServer_2010.ReportingService2010) Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"

            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is rsClients.rsClient2010 Then
                repDef = oRpt.GetItemDefinition(txtDBLoc.Text)
            Else
                repDef = oRpt.GetReportDefinition(txtDBLoc.Text)
            End If


            fsReport.Write(repDef, 0, repDef.LongLength)

            fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLoc.Text, ServerUser, _
            ServerPassword, sRDLPath, formsAuth)

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            With UcParsList
                .m_serverUrl = txtUrl.Text
                .m_reportPath = txtDBLoc.Text
                .m_serverUser = ServerUser
                .m_serverPassword = ServerPassword
                .m_rdlPath = sRDLPath
                .m_formsAuth = formsAuth
            End With

            UcParsList.loadParameters(formsAuth)

            cmbKey.Items.Clear()

            ParDefaults = New Hashtable

            If sPars IsNot Nothing Then
                For Each s As String In sPars
                    Dim parameterName As String = s.Split("|")(0)
                    Dim parameterType As String = s.Split("|")(1)
                    Dim tmp As String = s.Split("|")(2)


                    ParDefaults.Add(parameterName, sParDefaults(I))
                    cmbKey.Items.Add(parameterName)
                    I += 1
                Next
            End If

            UcDest.m_parameterList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdNext.Enabled = True

            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLoc.Text = ""
                txtName.Text = ""
                txtDesc.Text = ""
            End If
        End Try
    End Sub


    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Return
                ElseIf txtFolder.Text.Length = 0 Then
                    ep.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Return
                ElseIf txtDBLoc.Text.Length = 0 Then
                    ep.SetError(cmdDbLoc, "Please select the report")
                    cmdDbLoc.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT, , True) = False Then
                    ep.SetError(txtName, "A dynamic schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

               
                cmbDestination.Focus()
                tabSchedule.Visible = True
                stabMain.SelectedTab = tabSchedule
               
            Case "Schedule"
                If cmbDestination.Text.Length = 0 Then
                    ep.SetError(cmbDestination, "Please select the destination")
                    cmbDestination.Focus()
                    Return
                ElseIf UcSet.isAllDataValid = False Then
                    Return
                End If

                Select Case cmbDestination.Text
                    Case "Email"
                        Label11.Text = Label11.Text.Replace("information", "the email address")
                        Label9.Text = Label9.Text.Replace("xxx", "email address")
                        Label15.Text = Label15.Text.Replace("xxx", "email address")
                    Case "Printer"
                        Label11.Text = Label11.Text.Replace("informaton", "the printer")
                        Label9.Text = Label9.Text.Replace("xxx", "printer")
                        Label15.Text = Label15.Text.Replace("xxx", "printer")
                    Case "Disk"
                        Label11.Text = Label11.Text.Replace("informaton", "the directory")
                        Label9.Text = Label9.Text.Replace("xxx", "directory")
                        Label15.Text = Label15.Text.Replace("xxx", "directory")
                    Case "FTP"
                        Label11.Text = Label11.Text.Replace("information", "the FTP Server")
                        Label9.Text = Label9.Text.Replace("xxx", "FTP Server")
                        Label15.Text = Label15.Text.Replace("xxx", "FTP Server")
                    Case "SMS"
                        Label11.Text = Label11.Text.Replace("information", "the cell phone number")
                        Label9.Text = Label9.Text.Replace("xxx", "cell phone number")
                        Label15.Text = Label15.Text.Replace("xxx", "cell phone number")
                    Case "Fax"
                        Label11.Text = Label11.Text.Replace("information", "the fax number")
                        Label9.Text = Label9.Text.Replace("xxx", "fax number")
                        Label15.Text = Label15.Text.Replace("xxx", "fax number")
                End Select

                tabReportOptions.Visible = True
                stabMain.SelectedTab = tabReportOptions

            Case "Datasources"
                tabKeyParameter.Visible = True
                stabMain.SelectedTab = tabKeyParameter
            Case "Key Parameter"
                If cmbKey.Text.Length = 0 Then
                    ep.SetError(cmbKey, "Please select the key parameter")
                    cmbKey.Focus()
                    Return
                ElseIf optStatic.Checked = False And optDynamic.Checked = False Then
                    ep.SetError(optStatic, "Please select how the above key parameter is populated")
                    'optStatic.Focus()
                    Return
                Else
                    For Each o As ucParameters In UcParsList.Controls
                        Dim s As String = o.m_rawParameterValue

                        If s = "" Then
                            Dim res As DialogResult = MessageBox.Show("One or more parameters have been left blank. Edit parameters?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                            If res = Windows.Forms.DialogResult.Yes Then
                                Return
                            Else
                                Exit For
                            End If
                        End If
                    Next
                End If

                ' Step4.BringToFront()
                'Step4.Visible = True
                'Step3.Visible = False
                chkStaticDest.Focus()
                tabLinking.Visible = True
                stabMain.SelectedTab = tabLinking
                'lblStep.Text = S4
            Case "Linking"
                If chkStaticDest.Checked = False Then
                    If cmbTable.Text.Length = 0 Then
                        ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                        cmbTable.Focus()
                        Return
                    ElseIf cmbColumn.Text.Length = 0 Then
                        ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                        cmbColumn.Focus()
                        Return
                    ElseIf cmbValue.Text.Length = 0 Then
                        ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                        cmbValue.Focus()
                        Return
                    ElseIf cmbValue.Text = "<Advanced>" And sLink = String.Empty Then
                        ep.SetError(cmbValue, "Please set up the query for the value")
                        cmbValue.Focus()
                        Return
                    End If
                End If

                'Step5.BringToFront()
                'Step5.Visible = True
                tabDestinations.Visible = True
                stabMain.SelectedTab = tabDestinations
                'Step4.Visible = False
                'lblStep.Text = S5
                Dim senderX As Object

                Select Case cmbDestination.Text
                    Case "Disk"
                        senderX = UcDest.btnDisk
                    Case "Email"
                        senderX = UcDest.btnEmail
                    Case "Fax"
                        senderX = UcDest.btnFax
                    Case "FTP"
                        senderX = UcDest.btnFTP
                    Case "Printer"
                        senderX = UcDest.btnPrinter
                    Case "SharePoint"
                        senderX = UcDest.btnSharepoint
                    Case "SMS"
                        senderX = UcDest.btnSMS
                    Case Else
                        senderX = sender
                End Select

                UcDest.cmdAdd_Click(senderX, e)
            Case "Destinations"
                If UcDest.lsvDestination.Nodes.Count = 0 Then
                    UcDest.ep.SetError(UcDest.lsvDestination, "Please add a destination for the report(s)")
                    Return
                End If

               
                tabExceptions.Visible = True
                stabMain.SelectedTab = tabExceptions

                UcErrorHandler1.cmbRetry.Focus()
                tabExceptions.Visible = True
                stabMain.SelectedTab = tabExceptions
            Case "Exception Handling"

                tabTasks.Visible = True
                stabMain.SelectedTab = tabTasks
                cmdNext.Enabled = False
                cmdFinish.Enabled = True
            Case "Custom Tasks"

                'Step8.BringToFront()
                'Step8.Visible = True
                'Step7.Visible = False
                'lblStep.Text = S8


        End Select

        nStep += 1
    End Sub

    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        Return

        Dim oVal As New frmKeyParameter

        For Each oItem As ucParameters In UcParsList.Controls
            If oItem.m_parameterName = cmbKey.Text Then
                Try
                    sVal = oItem.m_rawParameterValue
                Catch : End Try
                Exit For
            End If
        Next

        Dim label As String = ""

        sVal = oVal._GetParameterValue(cmbKey.Text, sVal, ParDefaults.Item(cmbKey.Text), m_serverParametersTable, label)

        If sVal.Length = 0 Then Return

        For Each oItem As ucParameters In UcParsList.Controls
            If oItem.m_parameterName = cmbKey.Text Then
                oItem.Enabled = True

                Dim hs As Hashtable = New Hashtable()
                hs.Add(label, sVal)

                oItem.m_parameterValue = hs

                Exit For
            End If
        Next

        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID=99999")

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
        "Query,ReportID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(sVal) & "'," & _
        "'_'," & _
        "'_'," & _
        99999

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "report")

        If sParameter.Length = 0 Then Return

        For Each oItem As ucParameters In UcParsList.Controls
            If oItem.m_parameterName = cmbKey.Text Then
                oItem.txtValue.DropDownStyle = ComboBoxStyle.DropDown

                Dim hs As Hashtable = New Hashtable
                hs.Add(sParameter, sParameter)
                oItem.m_parameterValue = hs
                oItem.Enabled = False
                Exit For
            End If
        Next
    End Sub

    Private Sub optStatic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optStatic.CheckedChanged
        ep.SetError(optStatic, "")
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1226)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        gTables(0) = cmbTable.Text

        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    'Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Select Case nStep
    '        Case 7
    '            Step7.BringToFront()
    '            Step7.Visible = True
    '            ''lblStep.Text = S7
    '            cmdFinish.Visible = False
    '            cmdNext.Visible = True
    '        Case 6
    '            Step6.BringToFront()
    '            Step6.Visible = True
    '            ''lblStep.Text = S6

    '        Case 5
    '            Step5.BringToFront()
    '            Step5.Visible = True
    '            ''lblStep.Text = S5
    '        Case 4
    '            'Step4.BringToFront()
    '            'Step4.Visible = True
    '            ''lblStep.Text = S4
    '        Case 3
    '            Step3.BringToFront()
    '            Step3.Visible = True
    '            ''lblStep.Text = S3
    '        Case 2
    '            Step2.BringToFront()
    '            Step2.Visible = True
    '            ''lblStep.Text = S2
    '        Case 1
    '            Step1.BringToFront()
    '            Step1.Visible = True
    '            ''lblStep.Text = S1
    '            'cmdBack.Enabled = False
    '    End Select

    '    nStep -= 1
    'End Sub

    Private Sub Step7_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Step8.Paint

    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged

        gsCon = UcDSN.cmbDSN.Text & "|" & _
        UcDSN.txtUserID.Text & "|" & _
        UcDSN.txtPassword.Text & "|"

        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            sLink = oForm._AdvancedDynamicLinking(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim WriteSuccess As Boolean
        Dim nID As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport
        Dim dynamicTasks As Integer

        If optAll.Checked = True Then
            dynamicTasks = 0
        Else
            dynamicTasks = 1
        End If

        If UcSet.chkEndDate.Checked = True Then UcSet.dtEndDate.Value = Now.AddYears(1000)

        oErr.BusyProgress(30, "Saving report data...")

        'save the report 
        cmdFinish.Enabled = False

        nID = clsMarsData.CreateDataID("reportattr", "reportid")

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula," & _
            "Dynamic,Owner," & _
            "RptUserID,RptPassword,RptServer,RptDatabase,UseLogin,UseSavedData," & _
            "CachePath, LastRefreshed,StaticDest,DynamicTasks,RptDatabaseType," & _
            "AutoCalc,RetryInterval, rptFormsAuth"


        sVals = nID & ",0," & _
            "'" & SQLPrepare(txtUrl.Text) & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            UcErrorHandler1.cmbRetry.Text & "," & _
            UcErrorHandler1.m_autoFailAfter & "," & _
            Convert.ToInt32(UcBlank.chkCheckBlankReport.Checked) & "," & _
            "'" & SQLPrepare(txtFormula.Text) & "',1," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(ServerUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
            "''," & _
            "''," & _
            "0,0," & _
            "'" & SQLPrepare(txtDBLoc.Text) & "', " & _
            "'" & ConDateTime(Date.Now) & "'," & _
            Convert.ToInt32(chkStaticDest.Checked) & "," & _
            dynamicTasks & "," & _
            "''," & _
            Convert.ToInt32(UcErrorHandler1.chkAutoCalc.Checked) & "," & _
            UcErrorHandler1.txtRetryInterval.Value & "," & _
            Convert.ToInt32(formsAuth)


        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)


        If WriteSuccess = False Then GoTo RollbackTran

        oErr.BusyProgress(50, "Saving destination data...")

        SQL = "UPDATE DestinationAttr SET PackID =0, SmartID = 0, ReportID = " & nID & " WHERE ReportID = 99999"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = False Then GoTo RollbackTran

        oErr.BusyProgress(60, "Saving schedule data...")

        Dim ScheduleID As Integer

        ScheduleID = UcSet.saveSchedule(nID, clsMarsScheduler.enScheduleType.REPORT, txtDesc.Text, txtKeyWord.Text)


        oErr.BusyProgress(80, "Saving custom tasks...")


        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DynamicAttr SET ReportID = " & nID & ", PackID = 0, " & _
       "AutoResume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
       "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
       " WHERE ReportID = " & 99999

        clsMarsData.WriteData(SQL)

        oErr.BusyProgress(95, "Saving dynamic queries...")

        If cmbValue.Text = "<Advanced>" Then
            SQL = "UPDATE DynamicLink SET ReportID = " & nID & " WHERE ReportID = 99999"
        Else

            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID = 99999")

            sLink = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & _
                " FROM " & cmbTable.Text

            sCols = "LinkID,ReportID,LinkSQL,ConString,KeyColumn,KeyParameter,ValueColumn"

            sVals = clsMarsData.CreateDataID("dynamiclink", "linkid") & "," & _
            nID & "," & _
            "'" & SQLPrepare(sLink) & "'," & _
            "'" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
            "'" & SQLPrepare(cmbKey.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbValue.Text) & "'"

            SQL = "INSERT INTO DynamicLink (" & sCols & ") VALUES (" & sVals & ")"

        End If

        clsMarsData.WriteData(SQL)

        'save parameters
        UcParsList.saveParameters(nID)

        'update subreport parameters
        SQL = "UPDATE SubReportParameters SET ReportID =" & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportLogin SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET ReportID = 0 WHERE ReportID =99999")

        'set report table logins
        SQL = "UPDATE ReportTable SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportTable SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportDatasource SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        'save blank report alert information if available
        UcBlank.saveInfo(nID, clsMarsScheduler.enScheduleType.REPORT)

        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewSingle)
        End If

        frmMainWin.itemToSelect = New genericExplorerObject
        frmMainWin.itemToSelect.folderID = txtFolder.Tag
        frmMainWin.itemToSelect.itemName = txtName.Text

        Close()

        oErr.BusyProgress(, , True)

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.CREATE)
        Exit Sub

RollbackTran:

        clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID = " & nID)
        cmdFinish.Enabled = True
    End Sub

    Dim closeFlag As Boolean

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            If closeFlag = False Then
                Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
                Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                                   "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                                   Nothing,
                                                                                                                   DevComponents.DotNetBar.eTooltipColor.Office2003)
                sp.SetSuperTooltip(sender, spi)
                sp.ShowTooltip(sender)
                closeFlag = True
            Else
                Dim oCleaner As clsMarsData = New clsMarsData
                oCleaner.CleanDB()
                Me.Close()
                HasCancelled = True
            End If
        Catch : End Try
    End Sub

    Private Sub HelpLink_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles HelpLink.LinkClicked
        Dim sKeyMsg As String

        sKeyMsg = "Dim KeyMsg As String"

        sKeyMsg = "In a Dynamic Schedule, you tell SQL-RD to" & vbCrLf & _
            "- Reel through a list of possible values for a 'Key' parameter." & vbCrLf & _
            "- For each value in the list, SQL-RD must generate a unique report using that " & _
            "value as the parameter value, and then email the report to a recipient, " & _
            "or export it to a specified folder." & vbCrLf & _
            "- For email, the recipient email address is read from a database " & _
            "(or can be entered as a static value)" & vbCrLf & _
            "- For folder exports, the destination folder is read from a database " & _
            "(or can be entered as a static value)" & vbCrLf & vbCrLf & _
            "The Key Parameter" & vbCrLf & vbCrLf & _
            "The Key Parameter is used by SQL-RD to determine:" & vbCrLf & _
            "1. The list of parameter values to reel through" & vbCrLf & _
            "2. The corresponding recipient email address or destination output folder, " & _
            "for each value in the above list." & vbCrLf & vbCrLf

        sKeyMsg &= "Example " & vbCrLf & vbCrLf & _
            "I want to send a report to all sales people telling them the number of " & _
            "cars they sold.  Each sales person should get a unique report showing only that " & _
            "individual�s sales. " & _
            "The report should be emailed to that individual only." & vbCrLf & vbCrLf & _
            "In my database:" & vbCrLf & _
            "1. RepNo = Sales person�s id number" & vbCrLf & _
            "2. CarsSold = Number of cars sold" & vbCrLf & _
            "3. RepEmail = Rep�s email address" & vbCrLf & vbCrLf & _
            "In the MS Access report:" & vbCrLf & _
            "1. Create a parameter called {RepNumber}" & vbCrLf & _
            "2. In the Select Expert, set RepNo = {RepNumber}" & vbCrLf & _
            "3. In the report, show the field CarsSold." & vbCrLf & _
            "In the above report, if I manually enter a sales person�s number," & _
            "it will show me the number of cars sold." & vbCrLf & vbCrLf & _
            "The Key Parameter in the above example is {RepNumber} because " & _
            "I will instruct SQL-RD with the list of possible values that SQL-RD " & _
            "will enter into this parameter and generate reports. " & _
            "E.g. select RepNo where state = �Kansas� will give SQL-RD a list of all sales people in Kansas." & _
            "- I will then instruct how, using each value, SQL-RD can determine the email address to send the report to. " & _
            "E.g. select RepEmail where RepNo = {?RepNumber} will give SQL-RD the RepEmail for each RepNo that is in the above list"

        Dim m_AlertOnLoad As AlertCustom = New AlertCustom

        Dim r As Rectangle = Screen.GetWorkingArea(Me)
        m_AlertOnLoad.Location = New Point(r.Right - m_AlertOnLoad.Width, r.Bottom - m_AlertOnLoad.Height)
        m_AlertOnLoad.AutoClose = True
        m_AlertOnLoad.Label1.Text = sKeyMsg
        m_AlertOnLoad.AutoCloseTimeOut = 60
        m_AlertOnLoad.AlertAnimation = eAlertAnimation.BottomToTop
        m_AlertOnLoad.AlertAnimationDuration = 300
        m_AlertOnLoad.Show(False)
    End Sub

    Private Sub cmbDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDestination.SelectedIndexChanged
        If cmbDestination.Text.ToLower = "sms" And gnEdition < gEdition.ENTERPRISEPROPLUS Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.o1_SMS))
            cmbDestination.SelectedIndex = 0
            Return
        End If

        UcDest.DynamicDestination = cmbDestination.Text

    End Sub

    Private Sub optDynamic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDynamic.CheckedChanged
        ep.SetError(optStatic, "")
    End Sub

    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        'If Not ParValue.StartsWith("'") And Not ParValue.EndsWith("'") Then
        '    Try
        '        Int32.Parse(ParValue)
        '    Catch ex As Exception
        '        ParValue = "'" & SQLPrepare(ParValue) & "'"
        '    End Try
        'End If

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE ReportID= 99999"

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)


                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub



    Private Sub cmbKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbKey.SelectedIndexChanged
        If UcParsList.m_ParametersCollection.Count = 0 Then Return

        ep.SetError(sender, "")

        '//check if the parameter requires multiple values
        For Each par As ucParameters In UcParsList.Controls
            If par.m_parameterName = cmbKey.Text Then
                If par.m_allowMultipleValues = True Then
                    MessageBox.Show("A parameter that accepts multiple values cannot be used as a Key Parameter in a dynamic schedule", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    cmbKey.Text = ""
                    Return
                End If
            End If
        Next

        If cmbKey.Text.Length > 0 And sTempKey.Length > 0 Then
            MessageBox.Show("You are changing the key parameter. All your parameter values will be reset", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If

        sTempKey = cmbKey.Text

        For Each oitem As ucParameters In UcParsList.Controls
            Try
                oitem.m_parameterValue = newHashTable(String.Empty, String.Empty)
            Catch : End Try
        Next
    End Sub

    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        grpDynamicDest.Enabled = Not chkStaticDest.Checked
        UcDest.StaticDest = chkStaticDest.Checked
        UcDest.m_CanDisable = chkStaticDest.Checked
    End Sub
    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(99999, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub


    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If

    End Sub

    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub


    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLoc.TextChanged
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 200)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            'Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            'Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            'Me.cmdDbLoc.Image = ImageList1.Images(2)
            'Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If
    End Sub
    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub

    Dim m_inserter As frmInserter = New frmInserter(99999)

    Private Sub stabMain_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabMain.SelectedTabChanged
        cmdNext.Enabled = True

        Select Case e.NewValue.Text
            Case "Report", "Key Parameter"
                If m_inserter.IsDisposed Then
                    m_inserter = New frmInserter(0)
                End If

                Dim ssrsReports As clsMarsReport = New clsMarsReport

                UcParsList.m_serverparametersCollection = ssrsReports.getserverReportParametersCollection(txtUrl.Text, txtDBLoc.Text, ServerUser, ServerPassword, "", formsAuth, , 99999)

                m_inserter.GetConstants(Me)
            Case Else
                m_inserter.Hide()
        End Select
    End Sub
End Class
