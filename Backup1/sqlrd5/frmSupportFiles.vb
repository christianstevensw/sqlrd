Public Class frmSupportFiles
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim ep As ErrorProvider = New ErrorProvider
    Friend WithEvents grpExcept As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox4 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox3 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox2 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox1 As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents CheckBox5 As DevComponents.DotNetBar.Controls.CheckBoxX
    Dim UserCancel As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtLoc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkEncrypt As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtExt As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkExt As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.grpExcept = New System.Windows.Forms.GroupBox()
        Me.CheckBox5 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox4 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox3 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox2 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.CheckBox1 = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.txtExt = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkEncrypt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkExt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtLoc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox2.SuspendLayout()
        Me.grpExcept.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.grpExcept)
        Me.GroupBox2.Controls.Add(Me.cmdBrowse)
        Me.GroupBox2.Controls.Add(Me.txtExt)
        Me.GroupBox2.Controls.Add(Me.chkEncrypt)
        Me.GroupBox2.Controls.Add(Me.chkExt)
        Me.GroupBox2.Controls.Add(Me.txtLoc)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(4, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(480, 232)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'grpExcept
        '
        Me.grpExcept.Controls.Add(Me.CheckBox5)
        Me.grpExcept.Controls.Add(Me.CheckBox4)
        Me.grpExcept.Controls.Add(Me.CheckBox3)
        Me.grpExcept.Controls.Add(Me.CheckBox2)
        Me.grpExcept.Controls.Add(Me.CheckBox1)
        Me.grpExcept.Enabled = False
        Me.grpExcept.Location = New System.Drawing.Point(8, 132)
        Me.grpExcept.Name = "grpExcept"
        Me.grpExcept.Size = New System.Drawing.Size(456, 94)
        Me.grpExcept.TabIndex = 6
        Me.grpExcept.TabStop = False
        Me.grpExcept.Text = "Do not export the following tables"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        '
        '
        '
        Me.CheckBox5.BackgroundStyle.Class = ""
        Me.CheckBox5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox5.Location = New System.Drawing.Point(152, 43)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(84, 16)
        Me.CheckBox5.TabIndex = 0
        Me.CheckBox5.Tag = "sendwait"
        Me.CheckBox5.Text = "Email Queue"
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        '
        '
        '
        Me.CheckBox4.BackgroundStyle.Class = ""
        Me.CheckBox4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox4.Location = New System.Drawing.Point(152, 20)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(117, 16)
        Me.CheckBox4.TabIndex = 0
        Me.CheckBox4.Tag = "readreceiptslog"
        Me.CheckBox4.Text = "Read Receipts Logs"
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        '
        '
        '
        Me.CheckBox3.BackgroundStyle.Class = ""
        Me.CheckBox3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox3.Location = New System.Drawing.Point(6, 66)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(126, 16)
        Me.CheckBox3.TabIndex = 0
        Me.CheckBox3.Tag = "reportdurationtracker,reportduration"
        Me.CheckBox3.Text = "Report Duration Logs"
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        '
        '
        '
        Me.CheckBox2.BackgroundStyle.Class = ""
        Me.CheckBox2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox2.Location = New System.Drawing.Point(6, 43)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(104, 16)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Tag = "historydetailattr, schedulehistory"
        Me.CheckBox2.Text = "Schedule History"
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        '
        '
        '
        Me.CheckBox1.BackgroundStyle.Class = ""
        Me.CheckBox1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.CheckBox1.Location = New System.Drawing.Point(6, 20)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(70, 16)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Tag = "emaillog"
        Me.CheckBox1.Text = "Email Log"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(408, 32)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(56, 23)
        Me.cmdBrowse.TabIndex = 5
        Me.cmdBrowse.Text = "..."
        '
        'txtExt
        '
        '
        '
        '
        Me.txtExt.Border.Class = "TextBoxBorder"
        Me.txtExt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtExt.Enabled = False
        Me.txtExt.Location = New System.Drawing.Point(160, 104)
        Me.txtExt.Name = "txtExt"
        Me.txtExt.Size = New System.Drawing.Size(96, 21)
        Me.txtExt.TabIndex = 4
        '
        'chkEncrypt
        '
        '
        '
        '
        Me.chkEncrypt.BackgroundStyle.Class = ""
        Me.chkEncrypt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEncrypt.Location = New System.Drawing.Point(8, 59)
        Me.chkEncrypt.Name = "chkEncrypt"
        Me.chkEncrypt.Size = New System.Drawing.Size(464, 37)
        Me.chkEncrypt.TabIndex = 3
        Me.chkEncrypt.Text = "Encrypt files (do not encrypt if your email security does not permit  you to send" & _
    " encrypted files)"
        '
        'chkExt
        '
        '
        '
        '
        Me.chkExt.BackgroundStyle.Class = ""
        Me.chkExt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkExt.Location = New System.Drawing.Point(8, 101)
        Me.chkExt.Name = "chkExt"
        Me.chkExt.Size = New System.Drawing.Size(152, 24)
        Me.chkExt.TabIndex = 2
        Me.chkExt.Text = "Change file extension"
        '
        'txtLoc
        '
        '
        '
        '
        Me.txtLoc.Border.Class = "TextBoxBorder"
        Me.txtLoc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLoc.Location = New System.Drawing.Point(8, 32)
        Me.txtLoc.Name = "txtLoc"
        Me.txtLoc.Size = New System.Drawing.Size(376, 21)
        Me.txtLoc.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Output location for support files"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(332, 238)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(412, 238)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "&Cancel"
        '
        'frmSupportFiles
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(496, 267)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmSupportFiles"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Support Files"
        Me.GroupBox2.ResumeLayout(False)
        Me.grpExcept.ResumeLayout(False)
        Me.grpExcept.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmSupportFiles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim fbd As FolderBrowserDialog = New FolderBrowserDialog

        With fbd
            .Description = "Please select the output folder"
            .ShowNewFolderButton = True

            If .ShowDialog = DialogResult.OK Then
                txtLoc.Text = .SelectedPath

                If txtLoc.Text.EndsWith("\") = False Then
                    txtLoc.Text &= "\"
                End If
            End If
        End With
    End Sub

    Private Sub chkExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExt.CheckedChanged
        txtExt.Enabled = chkExt.Checked
        SetError(txtExt, "")
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtLoc.Text.Length = 0 Then
            SetError(txtLoc, "Please select the path to save the support files")
            txtLoc.Focus()
            Return
        ElseIf chkExt.Checked And txtExt.Text.Length = 0 Then
            SetError(txtExt, "Please provide the extension for the support files")
            txtExt.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub txtLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoc.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExt.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Public Sub _CreateSupportFiles()
        If gConType <> "DAT" Then
            Me.grpExcept.Enabled = True
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return
        Dim ok As Boolean
        Dim sFile As String = txtLoc.Text
        Dim oUI As New clsMarsUI

        Dim srcDSN As String
        Dim conString As String
        Dim dsn As String
        Dim user As String
        Dim pwd As String

        Try
            If gConType = "ODBC" Then
                conString = oUI.ReadRegistry("ConString", "", True)
                If conString.Length > 0 Then
                    dsn = conString.Split(";")(4).Split("=")(1)
                    user = conString.Split(";")(3).Split("=")(1)
                    pwd = conString.Split(";")(1).Split("=")(1)
                    srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
                End If



                Dim exceptTables As ArrayList = New ArrayList

                For Each ctrl As Control In Me.grpExcept.Controls
                    Dim chk As DevComponents.DotNetBar.Controls.CheckBoxX = CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX)

                    If chk.Checked Then
                        For Each s As String In chk.Tag.ToString.Split(",")
                            If s <> "" Then exceptTables.Add(chk.Tag)
                        Next
                    End If
                Next

                If exceptTables.Count = 0 Then exceptTables = Nothing

                oUI.BusyProgress(20, "Exporting data to file...")

                'ok = clsMigration.MigrateSystem(clsMigration.m_datConString, "", "", exceptTables)
                Dim dm As New DataMigrator(DataMigrator.dbType.ODBC)
                Dim errInfo As Exception = Nothing

                ok = dm.Export(srcDSN, sAppPath & "sqlrdlive.def", errInfo, exceptTables)

                If ok = False Then
                    If errInfo Is Nothing Then
                        Throw New Exception("Could not create def file")
                    Else
                        Throw errInfo
                    End If
                End If
            ElseIf gConType = "LOCAL" Then
                Dim exceptTables As ArrayList = New ArrayList

                For Each ctrl As Control In Me.grpExcept.Controls
                    Dim chk As DevComponents.DotNetBar.Controls.CheckBoxX = CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX)

                    If chk.Checked Then
                        For Each s As String In chk.Tag.ToString.Split(",")
                            If s <> "" Then exceptTables.Add(chk.Tag)
                        Next
                    End If
                Next

                If exceptTables.Count = 0 Then exceptTables = Nothing

                oUI.BusyProgress(20, "Exporting data to file...")

                'ok = clsMigration.MigrateSystem(clsMigration.m_datConString, "", "", exceptTables)
                Dim dm As New DataMigrator(DataMigrator.dbType.LOCALSQL)
                Dim errInfo As Exception = Nothing

                ok = dm.Export("", sAppPath & "sqlrdlive.def", errInfo, exceptTables)

                If ok = False Then
                    If errInfo Is Nothing Then
                        Throw New Exception("Could not create def file")
                    Else
                        Throw errInfo
                    End If
                End If
            Else
                ok = True
            End If

            Dim FilezToZip As String
            Dim sRegFile As String

            oUI.BusyProgress(60, "Adding files to archive...")


            FilezToZip = sAppPath & "sqlrdlive.dat"

            If IO.File.Exists(sAppPath & "crderror.log") = True Then
                FilezToZip &= "|" & sAppPath & "crderror.log"
            End If

            If IO.File.Exists(sAppPath & "sqlrdlive.def") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdlive.def"
            End If

            If oUI.ExportRegistry(sKey, sAppPath & "crdreg.txt") = True Then
                FilezToZip &= "|" & sAppPath & "crdreg.txt"
            End If

            Dim oTools As New clsSystemTools

            If oTools._CreateSysInfoFile(sAppPath & "crdinfo.txt") = True Then
                FilezToZip &= "|" & sAppPath & "crdinfo.txt"
            End If

            If IO.File.Exists(gConfigFile) = True Then
                FilezToZip &= "|" & gConfigFile
            End If

            'If IO.File.Exists(sAppPath & "eventlog.dat") Then
            '    FilezToZip &= "|" & sAppPath & "eventlog.dat"
            'End If

            For Each s As String In IO.Directory.GetFiles(sAppPath)
                If s.EndsWith(".debug") Or s.EndsWith(".log") Then
                    FilezToZip &= "|" & s
                End If
            Next

            If IO.Directory.Exists(sAppPath & "\Logs") Then
                For Each s As String In IO.Directory.GetFiles(sAppPath & "\Logs")
                    FilezToZip &= "|" & s
                Next
            End If


            oUI.BusyProgress(80, "Reading customer information...")

            Dim nCustID As String

            nCustID = oUI.ReadRegistry("CustNo", 0)

            Dim oTask As New clsMarsTask

            Dim ZipFile As String = "SQL-RD_" & nCustID

            oUI.BusyProgress(90, "Zipping up files...")

            If chkEncrypt.Checked Then
                oTask._ZipFiles(sFile & ZipFile, FilezToZip, True, "SQLRDAdmin")
            Else
                oTask._ZipFiles(sFile & ZipFile, FilezToZip)
            End If

            If chkExt.Checked Then

                If txtExt.Text.StartsWith(".") = False Then
                    txtExt.Text = "." & txtExt.Text
                End If

                oUI.BusyProgress(90, "Renaming file...")

                Dim originalFile As String = sFile & ZipFile & ".zip"
                Dim newFile As String = sFile & ZipFile & txtExt.Text

                IO.File.Move(originalFile, newFile)
            End If

            oUI.BusyProgress(, , True)

            MessageBox.Show("Support files created successfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
        End Try
    End Sub


    Public Sub _CreateSupportFiles(saveDirectory As String)
        If gConType <> "DAT" Then
            Me.grpExcept.Enabled = True
        End If


        Dim ok As Boolean
        Dim sFile As String = saveDirectory
        Dim oUI As New clsMarsUI

        Dim srcDSN As String
        Dim conString As String
        Dim dsn As String
        Dim user As String
        Dim pwd As String

        Try
            If gConType = "ODBC" Then
                conString = oUI.ReadRegistry("ConString", "", True)
                If conString.Length > 0 Then
                    dsn = conString.Split(";")(4).Split("=")(1)
                    user = conString.Split(";")(3).Split("=")(1)
                    pwd = conString.Split(";")(1).Split("=")(1)
                    srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
                End If

                Dim exceptTables As ArrayList = New ArrayList

                '//check all the boxes
                For Each ctrl As Control In Me.grpExcept.Controls
                    CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX).Checked = True
                Next

                For Each ctrl As Control In Me.grpExcept.Controls
                    Dim chk As DevComponents.DotNetBar.Controls.CheckBoxX = CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX)

                    If chk.Checked Then
                        For Each s As String In chk.Tag.ToString.Split(",")
                            If s <> "" Then exceptTables.Add(chk.Tag)
                        Next
                    End If
                Next

                If exceptTables.Count = 0 Then exceptTables = Nothing

                oUI.BusyProgress(20, "Exporting data to file...")

                'ok = clsMigration.MigrateSystem(clsMigration.m_datConString, "", "", exceptTables)
                Dim dm As New DataMigrator(DataMigrator.dbType.ODBC)
                Dim errInfo As Exception = Nothing

                ok = dm.Export(srcDSN, sAppPath & "sqlrdlive.def", errInfo, exceptTables)

                If ok = False Then
                    If errInfo Is Nothing Then
                        Throw New Exception("Could not create def file")
                    Else
                        Throw errInfo
                    End If
                End If
            ElseIf gConType = "LOCAL" Then
                Dim exceptTables As ArrayList = New ArrayList

                '//check all the boxes
                For Each ctrl As Control In Me.grpExcept.Controls
                    CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX).Checked = True
                Next

                For Each ctrl As Control In Me.grpExcept.Controls
                    Dim chk As DevComponents.DotNetBar.Controls.CheckBoxX = CType(ctrl, DevComponents.DotNetBar.Controls.CheckBoxX)

                    If chk.Checked Then
                        For Each s As String In chk.Tag.ToString.Split(",")
                            If s <> "" Then exceptTables.Add(chk.Tag)
                        Next
                    End If
                Next

                If exceptTables.Count = 0 Then exceptTables = Nothing

                oUI.BusyProgress(20, "Exporting data to file...")

                'ok = clsMigration.MigrateSystem(clsMigration.m_datConString, "", "", exceptTables)
                Dim dm As New DataMigrator(DataMigrator.dbType.LOCALSQL)
                Dim errInfo As Exception

                ok = dm.Export("", sAppPath & "sqlrdlive.def", errInfo, exceptTables)

                If ok = False Then
                    If errInfo Is Nothing Then
                        Throw New Exception("Could not create def file")
                    Else
                        Throw errInfo
                    End If
                End If
            Else
                ok = True
            End If

            Dim FilezToZip As String
            Dim sRegFile As String

            oUI.BusyProgress(60, "Adding files to archive...")


            FilezToZip = sAppPath & "sqlrdlive.dat"

            If IO.File.Exists(sAppPath & "crderror.log") = True Then
                FilezToZip &= "|" & sAppPath & "crderror.log"
            End If

            If IO.File.Exists(sAppPath & "sqlrdlive.def") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdlive.def"
            End If

            If oUI.ExportRegistry(sKey, sAppPath & "crdreg.txt") = True Then
                FilezToZip &= "|" & sAppPath & "crdreg.txt"
            End If

            Dim oTools As New clsSystemTools

            If oTools._CreateSysInfoFile(sAppPath & "crdinfo.txt") = True Then
                FilezToZip &= "|" & sAppPath & "crdinfo.txt"
            End If

            If IO.File.Exists(gConfigFile) = True Then
                FilezToZip &= "|" & gConfigFile
            End If

            'If IO.File.Exists(sAppPath & "eventlog.dat") Then
            '    FilezToZip &= "|" & sAppPath & "eventlog.dat"
            'End If

            For Each s As String In IO.Directory.GetFiles(sAppPath)
                If s.EndsWith(".debug") Then
                    FilezToZip &= "|" & s
                End If
            Next

            oUI.BusyProgress(80, "Reading customer information...")

            Dim nCustID As String

            nCustID = oUI.ReadRegistry("CustNo", 0)

            Dim oTask As New clsMarsTask

            Dim ZipFile As String = "SQL-RD_" & nCustID & "_" & Date.Now.ToString("yyyyMMdd")

            oUI.BusyProgress(90, "Zipping up files...")

            If chkEncrypt.Checked Then
                oTask._ZipFiles(sFile & ZipFile, FilezToZip, True, "SQL-RDAdmin")
            Else
                oTask._ZipFiles(sFile & ZipFile, FilezToZip)
            End If

            If chkExt.Checked Then

                If txtExt.Text.StartsWith(".") = False Then
                    txtExt.Text = "." & txtExt.Text
                End If

                oUI.BusyProgress(90, "Renaming file...")

                Dim originalFile As String = sFile & ZipFile & ".zip"
                Dim newFile As String = sFile & ZipFile & txtExt.Text

                IO.File.Move(originalFile, newFile)
            End If

            oUI.BusyProgress(, , True)



        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
        End Try
    End Sub
End Class
