Friend Class frmSetTableLogin
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Friend WithEvents txtDSN As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Dim ep As New ErrorProvider
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents UcLogin As sqlrd.ucLoginDetails
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.UcLogin = New sqlrd.ucLoginDetails()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.txtDSN = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.SuspendLayout()
        '
        'UcLogin
        '
        Me.UcLogin.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcLogin.Location = New System.Drawing.Point(4, 52)
        Me.UcLogin.Name = "UcLogin"
        Me.UcLogin.Size = New System.Drawing.Size(318, 70)
        Me.UcLogin.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(159, 128)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(247, 128)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'txtDSN
        '
        '
        '
        '
        Me.txtDSN.Border.Class = "TextBoxBorder"
        Me.txtDSN.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDSN.Location = New System.Drawing.Point(12, 25)
        Me.txtDSN.Name = "txtDSN"
        Me.txtDSN.ReadOnly = True
        Me.txtDSN.Size = New System.Drawing.Size(301, 21)
        Me.txtDSN.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(9, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 16)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Datasource Name"
        '
        'frmSetTableLogin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(325, 155)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDSN)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.UcLogin)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmSetTableLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Set Table Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmSetTableLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Function getDatasourceDetails(ByVal Edit As Boolean, Optional ByVal DSN As String = "", Optional ByVal UserID As String = "", _
    Optional ByVal Password As String = "") As Hashtable
        If Edit = True Then
            Me.txtDSN.ReadOnly = True
        Else
            Me.txtDSN.ReadOnly = False
        End If

        Me.txtDSN.Text = DSN
        Me.UcLogin.txtUserID.Text = UserID
        Me.UcLogin.txtPassword.Text = Password

        Me.ShowDialog()

        Dim result As Hashtable = New Hashtable

        If UserCancel = True Then Return Nothing

        result.Add("Name", Me.txtDSN.Text)
        result.Add("UserID", Me.UcLogin.txtUserID.Text)
        result.Add("Password", Me.UcLogin.txtPassword.Text)

        Return result
    End Function
    Public Function SaveLoginInfo(ByVal nReportID As Integer, ByVal sName As String, _
    Optional ByVal nDataID As Integer = 0) As String()

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValues(1) As String

        If nDataID > 0 Then
            SQL = "SELECT * FROM ReportDatasource WHERE DatasourceID =" & nDataID

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    With UcLogin
                        .txtUserID.Text = oRs("rptuserid").Value
                        .txtPassword.Text = _DecryptDBValue(oRs("rptpassword").Value)
                    End With
                End If

                oRs.Close()
            End If
        Else
            Try
                Dim oUI As New clsMarsUI
                Dim oSec As New clsMarsSecurity

                Me.UcLogin.txtUserID.Text = oUI.ReadRegistry("DefDBUser", "")
                UcLogin.txtPassword.Text = oUI.ReadRegistry("DefDBPassword", "", True)
            Catch : End Try
        End If

        Me.txtDSN.Text = sName

        Me.ShowDialog()

        If UserCancel = True Then
            Return Nothing
        End If

        With UcLogin
            If nDataID > 0 Then
                SQL = "RptUserID = '" & SQLPrepare(.txtUserID.Text) & "'," & _
                "RptPassword = '" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "' "

                SQL = "UPDATE ReportDatasource SET " & SQL & " WHERE DatasourceID = " & nDataID
            Else
                Dim sCols As String
                Dim sVals As String

                nDataID = clsMarsData.CreateDataID("reportdatasource", "datasourceid")

                sCols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"

                sVals = nDataID & "," & _
                nReportID & "," & _
                "'" & SQLPrepare(sName) & "'," & _
                "'" & SQLPrepare(.txtUserID.Text) & "'," & _
                "'" & SQLPrepare(_EncryptDBValue(.txtPassword.Text)) & "'"

                SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"
            End If
        End With

        If clsMarsData.WriteData(SQL) = True Then
            sValues(0) = UcLogin.txtUserID.Text
            sValues(1) = nDataID
            Return sValues
        Else
            Return Nothing
        End If

    End Function


    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        With UcLogin
            If .txtUserID.Text.Length = 0 Then
                ep.SetError(.txtUserID, "Please specify the user id")
                .txtUserID.Focus()
                Return
            ElseIf .txtPassword.Text.Length = 0 Then
                ep.SetError(.txtPassword, "Please specify the password")
                .txtPassword.Focus()
                Return
            ElseIf Me.txtDSN.Text = "" Then
                ep.SetError(Me.txtDSN, "Please enter the Datasource name")
                Me.txtDSN.Focus()
                Return
            End If
        End With

        Close()
    End Sub
End Class
