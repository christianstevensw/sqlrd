Public Class frmScheduleOptions
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean
    Dim chkMonths(11) As DevComponents.DotNetBar.Controls.CheckBoxX
    Dim chkDays(6) As DevComponents.DotNetBar.Controls.CheckBoxX
    Dim oFreqy As enFreqy
    Dim ep As New ErrorProvider
    Dim PreDay As String = String.Empty
    Friend WithEvents grpOthers As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtOtherFrequency As System.Windows.Forms.NumericUpDown
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmbOtherUnit As System.Windows.Forms.ComboBox
    Dim PreNum As String = String.Empty

    Private Enum enFreqy
        Daily = 1
        Weekly = 2
        Monthly = 3
        Other = 4
    End Enum
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpMonthly As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkJan As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFeb As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMar As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkApr As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkJun As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkJul As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAug As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMay As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSep As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkNov As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkDec As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkOct As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbWeekNumber As System.Windows.Forms.ComboBox
    Friend WithEvents cmbDayName As System.Windows.Forms.ComboBox
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpDaily As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents grpWeekly As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtWeeklyRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents chkMon As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkTue As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkWed As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkThu As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkFri As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSat As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSun As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkEnableOptions As DevComponents.DotNetBar.Controls.CheckBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.grpMonthly = New System.Windows.Forms.GroupBox()
        Me.chkEnableOptions = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbWeekNumber = New System.Windows.Forms.ComboBox()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDayName = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkJan = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFeb = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMar = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkApr = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkJun = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkJul = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAug = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMay = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSep = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkNov = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkDec = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkOct = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.grpDaily = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtRpt = New System.Windows.Forms.NumericUpDown()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.grpWeekly = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.chkMon = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkTue = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkWed = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkThu = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkFri = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSat = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSun = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtWeeklyRpt = New System.Windows.Forms.NumericUpDown()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.grpOthers = New System.Windows.Forms.GroupBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.txtOtherFrequency = New System.Windows.Forms.NumericUpDown()
        Me.cmbOtherUnit = New System.Windows.Forms.ComboBox()
        Me.grpMonthly.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.grpDaily.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpWeekly.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtWeeklyRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpOthers.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.txtOtherFrequency, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpMonthly
        '
        Me.grpMonthly.Controls.Add(Me.chkEnableOptions)
        Me.grpMonthly.Controls.Add(Me.GroupBox2)
        Me.grpMonthly.Controls.Add(Me.GroupBox1)
        Me.grpMonthly.Location = New System.Drawing.Point(8, 8)
        Me.grpMonthly.Name = "grpMonthly"
        Me.grpMonthly.Size = New System.Drawing.Size(408, 256)
        Me.grpMonthly.TabIndex = 0
        Me.grpMonthly.TabStop = False
        '
        'chkEnableOptions
        '
        '
        '
        '
        Me.chkEnableOptions.BackgroundStyle.Class = ""
        Me.chkEnableOptions.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEnableOptions.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEnableOptions.Location = New System.Drawing.Point(8, 16)
        Me.chkEnableOptions.Name = "chkEnableOptions"
        Me.chkEnableOptions.Size = New System.Drawing.Size(360, 24)
        Me.chkEnableOptions.TabIndex = 2
        Me.chkEnableOptions.Text = "Use the options below"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbWeekNumber)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cmbDayName)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 56)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Monthly"
        '
        'cmbWeekNumber
        '
        Me.cmbWeekNumber.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbWeekNumber.ItemHeight = 13
        Me.cmbWeekNumber.Location = New System.Drawing.Point(40, 24)
        Me.cmbWeekNumber.Name = "cmbWeekNumber"
        Me.cmbWeekNumber.Size = New System.Drawing.Size(121, 21)
        Me.cmbWeekNumber.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "The"
        '
        'cmbDayName
        '
        Me.cmbDayName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDayName.ItemHeight = 13
        Me.cmbDayName.Location = New System.Drawing.Point(176, 24)
        Me.cmbDayName.Name = "cmbDayName"
        Me.cmbDayName.Size = New System.Drawing.Size(121, 21)
        Me.cmbDayName.TabIndex = 1
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(304, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "of the month"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkJan)
        Me.GroupBox1.Controls.Add(Me.chkFeb)
        Me.GroupBox1.Controls.Add(Me.chkMar)
        Me.GroupBox1.Controls.Add(Me.chkApr)
        Me.GroupBox1.Controls.Add(Me.chkJun)
        Me.GroupBox1.Controls.Add(Me.chkJul)
        Me.GroupBox1.Controls.Add(Me.chkAug)
        Me.GroupBox1.Controls.Add(Me.chkMay)
        Me.GroupBox1.Controls.Add(Me.chkSep)
        Me.GroupBox1.Controls.Add(Me.chkNov)
        Me.GroupBox1.Controls.Add(Me.chkDec)
        Me.GroupBox1.Controls.Add(Me.chkOct)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(8, 96)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(392, 152)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Months"
        '
        'chkJan
        '
        '
        '
        '
        Me.chkJan.BackgroundStyle.Class = ""
        Me.chkJan.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJan.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkJan.Location = New System.Drawing.Point(40, 24)
        Me.chkJan.Name = "chkJan"
        Me.chkJan.Size = New System.Drawing.Size(104, 24)
        Me.chkJan.TabIndex = 0
        Me.chkJan.Text = "January"
        '
        'chkFeb
        '
        '
        '
        '
        Me.chkFeb.BackgroundStyle.Class = ""
        Me.chkFeb.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFeb.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFeb.Location = New System.Drawing.Point(40, 56)
        Me.chkFeb.Name = "chkFeb"
        Me.chkFeb.Size = New System.Drawing.Size(104, 24)
        Me.chkFeb.TabIndex = 0
        Me.chkFeb.Text = "Febraury"
        '
        'chkMar
        '
        '
        '
        '
        Me.chkMar.BackgroundStyle.Class = ""
        Me.chkMar.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMar.Location = New System.Drawing.Point(40, 88)
        Me.chkMar.Name = "chkMar"
        Me.chkMar.Size = New System.Drawing.Size(104, 24)
        Me.chkMar.TabIndex = 0
        Me.chkMar.Text = "March"
        '
        'chkApr
        '
        '
        '
        '
        Me.chkApr.BackgroundStyle.Class = ""
        Me.chkApr.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkApr.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkApr.Location = New System.Drawing.Point(40, 120)
        Me.chkApr.Name = "chkApr"
        Me.chkApr.Size = New System.Drawing.Size(104, 24)
        Me.chkApr.TabIndex = 0
        Me.chkApr.Text = "April"
        '
        'chkJun
        '
        '
        '
        '
        Me.chkJun.BackgroundStyle.Class = ""
        Me.chkJun.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJun.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkJun.Location = New System.Drawing.Point(160, 56)
        Me.chkJun.Name = "chkJun"
        Me.chkJun.Size = New System.Drawing.Size(104, 24)
        Me.chkJun.TabIndex = 0
        Me.chkJun.Text = "June"
        '
        'chkJul
        '
        '
        '
        '
        Me.chkJul.BackgroundStyle.Class = ""
        Me.chkJul.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkJul.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkJul.Location = New System.Drawing.Point(160, 88)
        Me.chkJul.Name = "chkJul"
        Me.chkJul.Size = New System.Drawing.Size(104, 24)
        Me.chkJul.TabIndex = 0
        Me.chkJul.Text = "July"
        '
        'chkAug
        '
        '
        '
        '
        Me.chkAug.BackgroundStyle.Class = ""
        Me.chkAug.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAug.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAug.Location = New System.Drawing.Point(160, 120)
        Me.chkAug.Name = "chkAug"
        Me.chkAug.Size = New System.Drawing.Size(104, 24)
        Me.chkAug.TabIndex = 0
        Me.chkAug.Text = "August"
        '
        'chkMay
        '
        '
        '
        '
        Me.chkMay.BackgroundStyle.Class = ""
        Me.chkMay.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMay.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMay.Location = New System.Drawing.Point(160, 24)
        Me.chkMay.Name = "chkMay"
        Me.chkMay.Size = New System.Drawing.Size(104, 24)
        Me.chkMay.TabIndex = 0
        Me.chkMay.Text = "May"
        '
        'chkSep
        '
        '
        '
        '
        Me.chkSep.BackgroundStyle.Class = ""
        Me.chkSep.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSep.Location = New System.Drawing.Point(280, 24)
        Me.chkSep.Name = "chkSep"
        Me.chkSep.Size = New System.Drawing.Size(104, 24)
        Me.chkSep.TabIndex = 0
        Me.chkSep.Text = "September"
        '
        'chkNov
        '
        '
        '
        '
        Me.chkNov.BackgroundStyle.Class = ""
        Me.chkNov.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkNov.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNov.Location = New System.Drawing.Point(280, 88)
        Me.chkNov.Name = "chkNov"
        Me.chkNov.Size = New System.Drawing.Size(104, 24)
        Me.chkNov.TabIndex = 0
        Me.chkNov.Text = "November"
        '
        'chkDec
        '
        '
        '
        '
        Me.chkDec.BackgroundStyle.Class = ""
        Me.chkDec.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDec.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDec.Location = New System.Drawing.Point(280, 120)
        Me.chkDec.Name = "chkDec"
        Me.chkDec.Size = New System.Drawing.Size(104, 24)
        Me.chkDec.TabIndex = 0
        Me.chkDec.Text = "December"
        '
        'chkOct
        '
        '
        '
        '
        Me.chkOct.BackgroundStyle.Class = ""
        Me.chkOct.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOct.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkOct.Location = New System.Drawing.Point(280, 56)
        Me.chkOct.Name = "chkOct"
        Me.chkOct.Size = New System.Drawing.Size(104, 24)
        Me.chkOct.TabIndex = 0
        Me.chkOct.Text = "October"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(256, 272)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(344, 272)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'grpDaily
        '
        Me.grpDaily.Controls.Add(Me.GroupBox3)
        Me.grpDaily.Location = New System.Drawing.Point(8, 8)
        Me.grpDaily.Name = "grpDaily"
        Me.grpDaily.Size = New System.Drawing.Size(408, 256)
        Me.grpDaily.TabIndex = 4
        Me.grpDaily.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtRpt)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(392, 100)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Daily"
        '
        'txtRpt
        '
        Me.txtRpt.Location = New System.Drawing.Point(96, 30)
        Me.txtRpt.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtRpt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtRpt.Name = "txtRpt"
        Me.txtRpt.Size = New System.Drawing.Size(56, 21)
        Me.txtRpt.TabIndex = 1
        Me.txtRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRpt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Repeat every "
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(168, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(72, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "days"
        '
        'grpWeekly
        '
        Me.grpWeekly.Controls.Add(Me.GroupBox4)
        Me.grpWeekly.Location = New System.Drawing.Point(8, 8)
        Me.grpWeekly.Name = "grpWeekly"
        Me.grpWeekly.Size = New System.Drawing.Size(408, 256)
        Me.grpWeekly.TabIndex = 5
        Me.grpWeekly.TabStop = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox5)
        Me.GroupBox4.Controls.Add(Me.txtWeeklyRpt)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(392, 232)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Weekly"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chkMon)
        Me.GroupBox5.Controls.Add(Me.chkTue)
        Me.GroupBox5.Controls.Add(Me.chkWed)
        Me.GroupBox5.Controls.Add(Me.chkThu)
        Me.GroupBox5.Controls.Add(Me.chkFri)
        Me.GroupBox5.Controls.Add(Me.chkSat)
        Me.GroupBox5.Controls.Add(Me.chkSun)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 56)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(376, 152)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "on"
        '
        'chkMon
        '
        '
        '
        '
        Me.chkMon.BackgroundStyle.Class = ""
        Me.chkMon.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMon.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMon.Location = New System.Drawing.Point(80, 24)
        Me.chkMon.Name = "chkMon"
        Me.chkMon.Size = New System.Drawing.Size(104, 24)
        Me.chkMon.TabIndex = 0
        Me.chkMon.Text = "Monday"
        '
        'chkTue
        '
        '
        '
        '
        Me.chkTue.BackgroundStyle.Class = ""
        Me.chkTue.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkTue.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkTue.Location = New System.Drawing.Point(80, 56)
        Me.chkTue.Name = "chkTue"
        Me.chkTue.Size = New System.Drawing.Size(104, 24)
        Me.chkTue.TabIndex = 0
        Me.chkTue.Text = "Tuesday"
        '
        'chkWed
        '
        '
        '
        '
        Me.chkWed.BackgroundStyle.Class = ""
        Me.chkWed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkWed.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkWed.Location = New System.Drawing.Point(80, 88)
        Me.chkWed.Name = "chkWed"
        Me.chkWed.Size = New System.Drawing.Size(104, 24)
        Me.chkWed.TabIndex = 0
        Me.chkWed.Text = "Wednesday"
        '
        'chkThu
        '
        '
        '
        '
        Me.chkThu.BackgroundStyle.Class = ""
        Me.chkThu.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkThu.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkThu.Location = New System.Drawing.Point(80, 120)
        Me.chkThu.Name = "chkThu"
        Me.chkThu.Size = New System.Drawing.Size(104, 24)
        Me.chkThu.TabIndex = 0
        Me.chkThu.Text = "Thursday"
        '
        'chkFri
        '
        '
        '
        '
        Me.chkFri.BackgroundStyle.Class = ""
        Me.chkFri.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFri.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFri.Location = New System.Drawing.Point(208, 24)
        Me.chkFri.Name = "chkFri"
        Me.chkFri.Size = New System.Drawing.Size(104, 24)
        Me.chkFri.TabIndex = 0
        Me.chkFri.Text = "Friday"
        '
        'chkSat
        '
        '
        '
        '
        Me.chkSat.BackgroundStyle.Class = ""
        Me.chkSat.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSat.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSat.Location = New System.Drawing.Point(208, 56)
        Me.chkSat.Name = "chkSat"
        Me.chkSat.Size = New System.Drawing.Size(104, 24)
        Me.chkSat.TabIndex = 0
        Me.chkSat.Text = "Saturday"
        '
        'chkSun
        '
        '
        '
        '
        Me.chkSun.BackgroundStyle.Class = ""
        Me.chkSun.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSun.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSun.Location = New System.Drawing.Point(208, 88)
        Me.chkSun.Name = "chkSun"
        Me.chkSun.Size = New System.Drawing.Size(104, 24)
        Me.chkSun.TabIndex = 0
        Me.chkSun.Text = "Sunday"
        '
        'txtWeeklyRpt
        '
        Me.txtWeeklyRpt.Location = New System.Drawing.Point(88, 24)
        Me.txtWeeklyRpt.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtWeeklyRpt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtWeeklyRpt.Name = "txtWeeklyRpt"
        Me.txtWeeklyRpt.Size = New System.Drawing.Size(56, 21)
        Me.txtWeeklyRpt.TabIndex = 0
        Me.txtWeeklyRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtWeeklyRpt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(160, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(128, 17)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "weeks"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 17)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Repeat every"
        '
        'grpOthers
        '
        Me.grpOthers.Controls.Add(Me.GroupBox6)
        Me.grpOthers.Location = New System.Drawing.Point(8, 8)
        Me.grpOthers.Name = "grpOthers"
        Me.grpOthers.Size = New System.Drawing.Size(408, 256)
        Me.grpOthers.TabIndex = 6
        Me.grpOthers.TabStop = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 20)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(392, 182)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Other frequency"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label8)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtOtherFrequency)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmbOtherUnit)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(5, 22)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(381, 27)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.Class = ""
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(104, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Run schedule every "
        '
        'txtOtherFrequency
        '
        Me.txtOtherFrequency.Location = New System.Drawing.Point(113, 3)
        Me.txtOtherFrequency.Maximum = New Decimal(New Integer() {21016, 0, 0, 0})
        Me.txtOtherFrequency.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtOtherFrequency.Name = "txtOtherFrequency"
        Me.txtOtherFrequency.Size = New System.Drawing.Size(67, 21)
        Me.txtOtherFrequency.TabIndex = 1
        Me.txtOtherFrequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtOtherFrequency.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbOtherUnit
        '
        Me.cmbOtherUnit.FormattingEnabled = True
        Me.cmbOtherUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months", "Years"})
        Me.cmbOtherUnit.Location = New System.Drawing.Point(186, 3)
        Me.cmbOtherUnit.Name = "cmbOtherUnit"
        Me.cmbOtherUnit.Size = New System.Drawing.Size(151, 21)
        Me.cmbOtherUnit.TabIndex = 2
        Me.cmbOtherUnit.Tag = "nosort"
        '
        'frmScheduleOptions
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(426, 301)
        Me.ControlBox = False
        Me.Controls.Add(Me.grpOthers)
        Me.Controls.Add(Me.grpWeekly)
        Me.Controls.Add(Me.grpMonthly)
        Me.Controls.Add(Me.grpDaily)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmScheduleOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Schedule Options"
        Me.grpMonthly.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.grpDaily.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.txtRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpWeekly.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.txtWeeklyRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpOthers.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        CType(Me.txtOtherFrequency, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmScheduleOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)

        With cmbWeekNumber
            .Items.Add(New clsMyList("First", 1))
            .Items.Add(New clsMyList("Second", 2))
            .Items.Add(New clsMyList("Third", 3))
            .Items.Add(New clsMyList("Fourth", 4))
            .Items.Add(New clsMyList("Last", 5))
        End With

        With cmbDayName
            .Items.Add(New clsMyList("Monday", 1))
            .Items.Add(New clsMyList("Tuesday", 2))
            .Items.Add(New clsMyList("Wednesday", 3))
            .Items.Add(New clsMyList("Thursday", 4))
            .Items.Add(New clsMyList("Friday", 5))
            .Items.Add(New clsMyList("Saturday", 6))
            .Items.Add(New clsMyList("Sunday", 0))
            .Items.Add(New clsMyList("Day", 7))
        End With

        If oFreqy = enFreqy.Monthly Then
            If PreNum.Length > 0 Then cmbWeekNumber.Text = PreNum

            If PreDay.Length > 0 Then cmbDayName.Text = PreDay
        End If
    End Sub
    Public Function OtherOptions(ByVal ScheduleID As Integer, ByRef selectedUnit As String)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        oFreqy = enFreqy.Other

        grpOthers.BringToFront()

        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Other'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Me.txtOtherFrequency.Value = oRs("otherfrequency").Value
                Me.cmbOtherUnit.Text = oRs("otherunit").Value
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return Now.Date

        Dim sCols As String
        Dim sVals As String

        sCols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType,OtherFrequency,OtherUnit"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        "''," & _
        txtWeeklyRpt.Text & "," & _
        ScheduleID & "," & _
        "'Other'," & _
        Me.txtOtherFrequency.Value & "," & _
        "'" & Me.cmbOtherUnit.Text & "'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oSchedule As New clsMarsScheduler

        selectedUnit = Me.cmbOtherUnit.Text

        Return oSchedule.GetOther(Me.txtOtherFrequency.Value, Me.cmbOtherUnit.Text)

    End Function

    Public Overloads Function WeeklyOptions(ByVal ScheduleIDs() As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        oFreqy = enFreqy.Weekly

        grpWeekly.BringToFront()

        chkDays(0) = chkSun
        chkDays(1) = chkMon
        chkDays(2) = chkTue
        chkDays(3) = chkWed
        chkDays(4) = chkThu
        chkDays(5) = chkFri
        chkDays(6) = chkSat


        Me.ShowDialog()

        If UserCancel = True Then Return Now.Date
        Dim sDays As String


        Dim sCols As String
        Dim sVals As String

        sDays = ""

        For I As Integer = 0 To chkDays.GetUpperBound(0)
            If chkDays(I).Checked = True Then
                sDays &= I & "|"
            End If
        Next

        sCols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType"

        For Each scheduleID As Integer In ScheduleIDs
            sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
            "'" & sDays & "'," & _
            txtWeeklyRpt.Text & "," & _
            scheduleID & "," & _
            "'Weekly'"

            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)

            SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

            clsMarsData.WriteData(SQL)
        Next

        Dim oSchedule As New clsMarsScheduler

        Return oSchedule.GetNextWeek(sDays)

    End Function
    Public Overloads Function WeeklyOptions(ByVal ScheduleID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sWhere As String
        Dim nIndex As Integer

        oFreqy = enFreqy.Weekly

        grpWeekly.BringToFront()

        chkDays(0) = chkSun
        chkDays(1) = chkMon
        chkDays(2) = chkTue
        chkDays(3) = chkWed
        chkDays(4) = chkThu
        chkDays(5) = chkFri
        chkDays(6) = chkSat

        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Weekly'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                txtWeeklyRpt.Text = oRs("ncount").Value

                Dim sValues As String = oRs("monthsin").Value

                For I As Integer = 1 To FindOccurence(oRs("monthsin").Value, "|")
                    nIndex = GetDelimitedWord(oRs("MonthsIn").Value, I, _
                     "|")
                    chkDays(nIndex).Checked = True
                Next
            Else
                For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkDays
                    chk.Checked = True
                Next
            End If
            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return Now.Date

        Dim sCols As String
        Dim sVals As String
        Dim sDays As String

        For I As Integer = 0 To chkDays.GetUpperBound(0)
            If chkDays(I).Checked = True Then
                sDays &= I & "|"
            End If
        Next

        sCols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        "'" & sDays & "'," & _
        txtWeeklyRpt.Text & "," & _
        ScheduleID & "," & _
        "'Weekly'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oSchedule As New clsMarsScheduler

        Return oSchedule.GetNextWeek(sDays)

    End Function

    Public Overloads Function DailyOptions(ByVal scheduleIDs() As Integer)
        oFreqy = enFreqy.Daily

        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String

        grpDaily.BringToFront()


        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty


        Dim sCols As String
        Dim sVals As String

        sCols = "OptionID,nCount,ScheduleID,ScheduleType"

        For Each scheduleID As Integer In scheduleIDs
            sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
            txtRpt.Text & "," & _
            scheduleID & "," & _
            "'Daily'"

            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)


            SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

            clsMarsData.WriteData(SQL)
        Next
    End Function
    Public Overloads Function DailyOptions(ByVal ScheduleID As Integer)
        oFreqy = enFreqy.Daily

        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String

        grpDaily.BringToFront()

        sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Daily'"

        SQL = "SELECT * FROM ScheduleOptions " & sWhere

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then

                txtRpt.Text = oRs("ncount").Value

            End If
            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Dim sCols As String
        Dim sVals As String

        sCols = "OptionID,nCount,ScheduleID,ScheduleType"

        sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
        txtRpt.Text & "," & _
        ScheduleID & "," & _
        "'Daily'"

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)


        SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

    End Function
    Public Overloads Function MonthlyOptions(ByVal ScheduleIDs() As Integer) As Date
        Try
            Dim oRs As ADODB.Recordset
            Dim oData As New clsMarsData
            Dim SQL As String
            Dim I As Integer
            Dim sWhere As String
            Dim nIndex As Integer
            Dim oItem As clsMyList

            oFreqy = enFreqy.Monthly

            grpMonthly.BringToFront()

            chkMonths(0) = chkJan
            chkMonths(1) = chkFeb
            chkMonths(2) = chkMar
            chkMonths(3) = chkApr
            chkMonths(4) = chkMay
            chkMonths(5) = chkJun
            chkMonths(6) = chkJul
            chkMonths(7) = chkAug
            chkMonths(8) = chkSep
            chkMonths(9) = chkOct
            chkMonths(10) = chkNov
            chkMonths(11) = chkDec

            Me.ShowDialog()

            If UserCancel = True Then Return Now.Date
            Dim sMonths As String
            Dim nWeekNumber As Integer
            Dim nDayName As Integer

            Dim sCols As String
            Dim sVals As String
            sMonths = ""

            sCols = "OptionID,WeekNo,DayNo,MonthsIn,ScheduleID,ScheduleType,UseOptions"

            For I = 0 To chkMonths.GetUpperBound(0)
                If chkMonths(I).Checked = True Then
                    sMonths &= I + 1 & "|"
                End If
            Next

            oItem = cmbWeekNumber.Items(cmbWeekNumber.SelectedIndex)

            nWeekNumber = oItem.ItemData

            oItem = cmbDayName.Items(cmbDayName.SelectedIndex)

            nDayName = oItem.ItemData

            For Each scheduleID As Integer In ScheduleIDs
                If chkEnableOptions.Checked = False Then
                    clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)
                    Return Now.Date
                End If

                sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                    nWeekNumber & "," & _
                    nDayName & "," & _
                    "'" & sMonths & "'," & _
                    scheduleID & "," & _
                    "'Monthly'," & _
                    Convert.ToInt32(chkEnableOptions.Checked)

                SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

                clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & scheduleID)

                If chkEnableOptions.Checked Then clsMarsData.WriteData(SQL)
            Next

            If chkEnableOptions.Checked Then
                Dim oSchedule As New clsMarsScheduler

                Dim NewDate As Date

                NewDate = oSchedule.GetNextMonth(sMonths, Now.Month - 1)

                NewDate = oSchedule.GetNthDayOfMonth(NewDate, _
                       nWeekNumber, nDayName)

                Return NewDate
            Else
                Return Now.Date
            End If

        Catch : End Try
    End Function
    Public Overloads Function MonthlyOptions(ByVal ScheduleID As Integer) As Date
        Try
            Dim oRs As ADODB.Recordset
            Dim oData As New clsMarsData
            Dim SQL As String
            Dim I As Integer
            Dim sWhere As String
            Dim nIndex As Integer
            Dim oItem As clsMyList

            oFreqy = enFreqy.Monthly

            grpMonthly.BringToFront()

            chkMonths(0) = chkJan
            chkMonths(1) = chkFeb
            chkMonths(2) = chkMar
            chkMonths(3) = chkApr
            chkMonths(4) = chkMay
            chkMonths(5) = chkJun
            chkMonths(6) = chkJul
            chkMonths(7) = chkAug
            chkMonths(8) = chkSep
            chkMonths(9) = chkOct
            chkMonths(10) = chkNov
            chkMonths(11) = chkDec

            sWhere = " WHERE ScheduleID = " & ScheduleID & " AND ScheduleType = 'Monthly'"

            SQL = "SELECT * FROM ScheduleOptions " & sWhere

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    With oRs
                        Select Case oRs("weekno").Value
                            Case 1
                                PreNum = "First"
                            Case 2
                                PreNum = "Second"
                            Case 3
                                PreNum = "Third"
                            Case 4
                                PreNum = "Fourth"
                            Case 5
                                PreNum = "Last"
                        End Select

                        If oRs("dayno").Value = 7 Then
                            PreDay = "Day"
                        Else
                            Dim d As Integer

                            d = oRs("dayno").Value + 1

                            PreDay = WeekdayName(d, False, vbSunday)
                        End If

                        For I = 1 To FindOccurence(oRs("monthsin").Value, "|")
                            nIndex = GetDelimitedWord(oRs("MonthsIn").Value, I, _
                             "|") - 1
                            chkMonths(nIndex).Checked = True
                        Next

                        Try
                            chkEnableOptions.Checked = Convert.ToBoolean(.Fields("useoptions").Value)
                        Catch
                            chkEnableOptions.Checked = True
                        End Try

                    End With
                Else
                    For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkMonths
                        chk.Checked = True
                    Next
                End If

                oRs.Close()
            End If

            Me.ShowDialog()

            If UserCancel = True Then Return Now.Date

            If chkEnableOptions.Checked = False Then
                clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
                Return Now.Date
            End If

            Dim sCols As String
            Dim sVals As String
            Dim sMonths As String

            sCols = "OptionID,WeekNo,DayNo,MonthsIn,ScheduleID,ScheduleType,UseOptions"

            For I = 0 To chkMonths.GetUpperBound(0)
                If chkMonths(I).Checked = True Then
                    sMonths &= I + 1 & "|"
                End If
            Next

            Dim nWeekNumber As Integer
            Dim nDayName As Integer

            oItem = cmbWeekNumber.Items(cmbWeekNumber.SelectedIndex)

            nWeekNumber = oItem.ItemData

            oItem = cmbDayName.Items(cmbDayName.SelectedIndex)

            nDayName = oItem.ItemData

            sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                nWeekNumber & "," & _
                nDayName & "," & _
                "'" & sMonths & "'," & _
                ScheduleID & "," & _
                "'Monthly'," & _
                Convert.ToInt32(chkEnableOptions.Checked)

            SQL = "INSERT INTO ScheduleOptions (" & sCols & ") VALUES (" & sVals & ")"

            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)

            If chkEnableOptions.Checked Then
                clsMarsData.WriteData(SQL)


                Dim oSchedule As New clsMarsScheduler

                Dim NewDate As Date

                NewDate = oSchedule.GetNextMonth(sMonths, Now.Month - 1)

                NewDate = oSchedule.GetNthDayOfMonth(NewDate, _
                       nWeekNumber, nDayName)

                Return NewDate

            Else
                Return Now.Date
            End If

        Catch : End Try
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        Select Case oFreqy
            Case enFreqy.Monthly
                If cmbWeekNumber.Text.Length = 0 And chkEnableOptions.Checked Then
                    SetError(cmbWeekNumber, "Please select the week number")
                    cmbWeekNumber.Focus()
                    Return
                ElseIf cmbDayName.Text.Length = 0 And chkEnableOptions.Checked Then
                    SetError(cmbDayName, "Please select the day when to run")
                    cmbDayName.Focus()
                    Return
                Else
                    Dim I As Integer = 0
                    Dim n As Integer

                    Try
                        For I = 0 To 11
                            If chkMonths(I).Checked = True Then
                                n += 1
                            End If
                        Next
                    Catch ex As Exception
                        n = 0
                    End Try

                    If n = 0 Then
                        SetError(chkJan, "Please select the months when to run")
                        chkJan.Focus()
                        Return
                    End If
                End If
            Case enFreqy.Daily
                If txtRpt.Text.Length = 0 Then
                    SetError(txtRpt, "Please enter the repeat interval")
                    txtRpt.Focus()
                    Return
                End If

                Try
                    Int32.Parse(txtRpt.Text)
                Catch ex As Exception
                    SetError(txtRpt, "Please select a valid number ")
                    txtRpt.Focus()
                    Return
                End Try
            Case enFreqy.Weekly
                Dim n As Integer

                If txtWeeklyRpt.Text.Length = 0 Then
                    SetError(txtWeeklyRpt, "Please enter the repeat interval")
                    txtWeeklyRpt.Focus()
                    Return
                End If

                Try
                    Int32.Parse(txtWeeklyRpt.Text)
                Catch ex As Exception
                    SetError(txtWeeklyRpt, "Please select a valid number ")
                    txtWeeklyRpt.Focus()
                    Return
                End Try

                For Each chk As DevComponents.DotNetBar.Controls.CheckBoxX In chkDays
                    If chk.Checked = True Then
                        n += 1
                    End If
                Next

                If n = 0 Then
                    SetError(chkMon, "Please select at least one day to run on")
                    chkMon.Focus()
                    Return
                End If
            Case enFreqy.Other
                If Me.cmbOtherUnit.Text = "" Then
                    SetError(Me.cmbOtherUnit, "Please select the repeat unit")
                    cmbOtherUnit.Focus()
                    Return
                End If
        End Select

        Close()
    End Sub

    Private Sub cmbWeekNumber_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWeekNumber.SelectedIndexChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub cmbDayName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDayName.SelectedIndexChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub chkJan_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkJan.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkFeb_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFeb.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkMar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMar.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkApr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkApr.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkMay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMay.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkJun_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkJun.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkJul_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkJul.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkAug_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAug.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkSep_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSep.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkOct_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOct.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkNov_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNov.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub chkDec_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDec.CheckedChanged
        SetError(chkJan, String.Empty)
    End Sub

    Private Sub txtWeeklyRpt_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWeeklyRpt.ValueChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub chkMon_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMon.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkFri_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFri.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkTue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTue.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkSat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSat.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkWed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkWed.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkSun_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSun.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkThu_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkThu.CheckedChanged
        SetError(chkMon, String.Empty)
    End Sub

    Private Sub chkEnableOptions_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEnableOptions.CheckedChanged
        GroupBox2.Enabled = chkEnableOptions.Checked
        GroupBox1.Enabled = chkEnableOptions.Checked
    End Sub

    Private Sub cmbOtherUnit_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOtherUnit.SelectedIndexChanged
        SetError(sender, "")
    End Sub
End Class
