Imports Microsoft.WindowsAPICodePack.Taskbar
Imports Microsoft.WindowsAPICodePack.Shell
Public Class frmError
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim countDown As Integer = 30
    Dim m_autoCloseAfter As Integer = 30
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtErrorNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtModuleName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtLineNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdCopy As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtErrorDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFix As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents txtCopy As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents tmClose As System.Windows.Forms.Timer
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmError))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtErrorDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.txtCopy = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCopy = New DevComponents.DotNetBar.ButtonX()
        Me.txtErrorNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtModuleName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtLineNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.txtFix = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.tmClose = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtErrorDesc)
        Me.GroupBox1.Controls.Add(Me.ReflectionImage1)
        Me.GroupBox1.Controls.Add(Me.txtCopy)
        Me.GroupBox1.Controls.Add(Me.cmdOK)
        Me.GroupBox1.Controls.Add(Me.cmdCopy)
        Me.GroupBox1.Controls.Add(Me.txtErrorNumber)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtModuleName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtLineNumber)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtFix)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(538, 408)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtErrorDesc
        '
        Me.txtErrorDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtErrorDesc.Border.Class = "TextBoxBorder"
        Me.txtErrorDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtErrorDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtErrorDesc.ForeColor = System.Drawing.Color.Blue
        Me.txtErrorDesc.Location = New System.Drawing.Point(8, 184)
        Me.txtErrorDesc.Multiline = True
        Me.txtErrorDesc.Name = "txtErrorDesc"
        Me.txtErrorDesc.ReadOnly = True
        Me.txtErrorDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtErrorDesc.Size = New System.Drawing.Size(360, 80)
        Me.txtErrorDesc.TabIndex = 5
        Me.txtErrorDesc.Tag = "red"
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(374, 72)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(142, 296)
        Me.ReflectionImage1.TabIndex = 7
        '
        'txtCopy
        '
        '
        '
        '
        Me.txtCopy.Border.Class = ""
        Me.txtCopy.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCopy.Location = New System.Drawing.Point(193, 382)
        Me.txtCopy.Name = "txtCopy"
        Me.txtCopy.Size = New System.Drawing.Size(100, 14)
        Me.txtCopy.TabIndex = 6
        Me.txtCopy.Visible = False
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(304, 376)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(64, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCopy
        '
        Me.cmdCopy.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCopy.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCopy.Image = CType(resources.GetObject("cmdCopy.Image"), System.Drawing.Image)
        Me.cmdCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCopy.Location = New System.Drawing.Point(8, 376)
        Me.cmdCopy.Name = "cmdCopy"
        Me.cmdCopy.Size = New System.Drawing.Size(120, 23)
        Me.cmdCopy.TabIndex = 0
        Me.cmdCopy.Text = "&Copy to Clipboard"
        '
        'txtErrorNumber
        '
        '
        '
        '
        Me.txtErrorNumber.Border.Class = "TextBoxBorder"
        Me.txtErrorNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtErrorNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtErrorNumber.Location = New System.Drawing.Point(104, 72)
        Me.txtErrorNumber.Name = "txtErrorNumber"
        Me.txtErrorNumber.ReadOnly = True
        Me.txtErrorNumber.Size = New System.Drawing.Size(264, 21)
        Me.txtErrorNumber.TabIndex = 2
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Error Number:"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(360, 56)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Apologies for the inconvenience, but SQL-RD has encountered an exception. Please " & _
    "find the details below:"
        Me.Label1.WordWrap = True
        '
        'txtModuleName
        '
        '
        '
        '
        Me.txtModuleName.Border.Class = "TextBoxBorder"
        Me.txtModuleName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtModuleName.ForeColor = System.Drawing.Color.Blue
        Me.txtModuleName.Location = New System.Drawing.Point(104, 104)
        Me.txtModuleName.Name = "txtModuleName"
        Me.txtModuleName.ReadOnly = True
        Me.txtModuleName.Size = New System.Drawing.Size(264, 21)
        Me.txtModuleName.TabIndex = 3
        '
        'Label3
        '
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Module Name:"
        '
        'txtLineNumber
        '
        '
        '
        '
        Me.txtLineNumber.Border.Class = "TextBoxBorder"
        Me.txtLineNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLineNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtLineNumber.Location = New System.Drawing.Point(104, 136)
        Me.txtLineNumber.Name = "txtLineNumber"
        Me.txtLineNumber.ReadOnly = True
        Me.txtLineNumber.Size = New System.Drawing.Size(264, 21)
        Me.txtLineNumber.TabIndex = 4
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Line Number:"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Error Description:"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 272)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Suggestion for Fix"
        '
        'txtFix
        '
        Me.txtFix.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.txtFix.Border.Class = "TextBoxBorder"
        Me.txtFix.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFix.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFix.ForeColor = System.Drawing.Color.Blue
        Me.txtFix.Location = New System.Drawing.Point(8, 288)
        Me.txtFix.Multiline = True
        Me.txtFix.Name = "txtFix"
        Me.txtFix.ReadOnly = True
        Me.txtFix.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFix.Size = New System.Drawing.Size(360, 80)
        Me.txtFix.TabIndex = 6
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Select All"
        '
        'tmClose
        '
        Me.tmClose.Interval = 1000
        '
        'frmError
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(552, 416)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmError"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Something has gone wrong"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Delegate Function Copier() As Boolean

#If DEBUG Then
    Dim errorDescs As String() = New String() {"Ooops!", "Dang It!", "OMGWTH!", "Something has gone terribly wrong", "Hmmm, thats not right", "Its Borked!", _
                                               "I just can't do it Captain! I do not have the power!", "I think we need more duct tape", "Sorry Captain, its a no go"}
#Else
    Dim errorDescs As String() = New String() {"Oops!", "Ooops!", "Ooooops!"}
#End If

    Private Function CopytoClipboard() As Boolean
        Try
            Dim sMsg As String

            sMsg = "Error Number: " & txtErrorNumber.Text & Environment.NewLine & _
                "Module Name: " & txtModuleName.Text & Environment.NewLine & _
                "Line Number: " & txtLineNumber.Text & Environment.NewLine & _
                "Error Description: " & txtErrorDesc.Text & Environment.NewLine & _
                "Suggestion: " & txtFix.Text & Environment.NewLine

            txtCopy.Text = sMsg

            txtCopy.Focus()
            txtCopy.SelectAll()
            txtCopy.Copy()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub cmdCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCopy.Click
        Dim newCopier As New Copier(AddressOf CopytoClipboard)

        Me.Invoke(newCopier)
    End Sub

    Public Sub ShowError(ByVal nError As Long, ByVal sModule As String, ByVal sDesc As String, _
    Optional ByVal sLine As String = "Unavailable", Optional ByVal sFix As String = "Unavailable")
        Dim randommonth As Integer
        Dim randomdate As Integer
        Dim thisyear As Integer = Date.Now.Year
        Dim rnd As Random = New Random

        randommonth = rnd.Next(1, 12)
        randomdate = rnd.Next(1, 28)

        Dim testdate As Date = New Date(thisyear, randommonth, randomdate)

        If Date.Now.Subtract(testdate).TotalDays = 0 Then
            errorDescs = New String() {"Ooops!", "Dang It!", "OMGWTH!", "Something has gone terribly wrong", "Hmmm, thats not right", "Its Borked!", _
                                               "I just can't do it Captain! I do not have the power!", "I think we need more duct tape", "Sorry Captain, its a no go"}
        End If

        txtErrorNumber.Text = nError
        txtModuleName.Text = sModule
        txtLineNumber.Text = sLine
        txtErrorDesc.Text = sDesc
        txtFix.Text = sFix

        Beep()

        Dim entry As Integer = rnd.Next(errorDescs.GetUpperBound(0))

        Me.Text = errorDescs(entry)


        Dim autoClose As Boolean
        Dim autoCloseAfter As Integer

        autoClose = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AutoCloseErrors", 0)))
        autoCloseAfter = clsMarsUI.MainUI.ReadRegistry("AutoCloseErrorValue", 30)

        If autoClose = True Then
            tmClose.Enabled = True
            Me.m_autoCloseAfter = autoCloseAfter
            Me.countDown = Me.m_autoCloseAfter
        End If

        Me.ShowDialog()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Close()
    End Sub

    Private Sub frmError_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.NoProgress)
        Catch : End Try
    End Sub

    Private Sub frmError_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'FormatForWinXP(Me)
        Try
            TaskbarManager.Instance.SetProgressState(TaskbarProgressBarState.Error)
        Catch : End Try
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        txtErrorDesc.SelectAll()
    End Sub

    Private Sub tmClose_Tick(sender As System.Object, e As System.EventArgs) Handles tmClose.Tick
        If countDown >= 0 Then
            Me.cmdOK.Text = "&OK (" & countDown & ")"
            countDown -= 1
        Else
            Me.cmdOK_Click(sender, e)
        End If
    End Sub
End Class
