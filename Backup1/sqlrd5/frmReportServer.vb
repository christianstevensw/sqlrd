Imports DevComponents.AdvTree
Friend Class frmReportServer
    Inherits DevComponents.DotNetBar.Office2007Form
    Public m_oRpt As Object 'rsClients.rsClient 'EDIT

    Public showOwner As Boolean = False
    Dim UserCancel As Boolean = False
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtserverPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtserverUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnGo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpReports As System.Windows.Forms.GroupBox
    Dim oUI As New clsMarsUI
    Friend WithEvents txtPath As DevComponents.DotNetBar.Controls.TextBoxX
    Dim m_serverUrl As String
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Dim readOnlyMode As Boolean = False
    Friend WithEvents pgServer As DevComponents.DotNetBar.Controls.ProgressBarX
    Friend WithEvents tvFolders As DevComponents.AdvTree.AdvTree
    Friend WithEvents Node1 As DevComponents.AdvTree.Node
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkFormsAuth As DevComponents.DotNetBar.Controls.CheckBoxX
    Dim firstAttempt As Boolean = True

    Public Property m_ReadOnly() As Boolean
        Get
            Return Me.readOnlyMode
        End Get
        Set(ByVal value As Boolean)
            Me.readOnlyMode = value
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportServer))
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtserverPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtserverUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnGo = New DevComponents.DotNetBar.ButtonX()
        Me.grpReports = New System.Windows.Forms.GroupBox()
        Me.tvFolders = New DevComponents.AdvTree.AdvTree()
        Me.Node1 = New DevComponents.AdvTree.Node()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.pgServer = New DevComponents.DotNetBar.Controls.ProgressBarX()
        Me.txtPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkFormsAuth = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpReports.SuspendLayout()
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(280, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(361, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 25)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Domain Name\User ID"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(174, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Password"
        '
        'txtserverPassword
        '
        '
        '
        '
        Me.txtserverPassword.Border.Class = "TextBoxBorder"
        Me.txtserverPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtserverPassword.Location = New System.Drawing.Point(174, 34)
        Me.txtserverPassword.Name = "txtserverPassword"
        Me.txtserverPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtserverPassword.Size = New System.Drawing.Size(165, 21)
        Me.txtserverPassword.TabIndex = 1
        '
        'txtserverUser
        '
        '
        '
        '
        Me.txtserverUser.Border.Class = "TextBoxBorder"
        Me.txtserverUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtserverUser.Location = New System.Drawing.Point(3, 34)
        Me.txtserverUser.Name = "txtserverUser"
        Me.txtserverUser.Size = New System.Drawing.Size(165, 21)
        Me.txtserverUser.TabIndex = 0
        '
        'btnGo
        '
        Me.btnGo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnGo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnGo.Location = New System.Drawing.Point(345, 34)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(71, 23)
        Me.btnGo.TabIndex = 2
        Me.btnGo.Text = "Connect"
        '
        'grpReports
        '
        Me.grpReports.Controls.Add(Me.tvFolders)
        Me.grpReports.Controls.Add(Me.pgServer)
        Me.grpReports.Controls.Add(Me.txtPath)
        Me.grpReports.Location = New System.Drawing.Point(5, 151)
        Me.grpReports.Name = "grpReports"
        Me.grpReports.Size = New System.Drawing.Size(429, 315)
        Me.grpReports.TabIndex = 0
        Me.grpReports.TabStop = False
        Me.grpReports.Text = "Please select the report"
        '
        'tvFolders
        '
        Me.tvFolders.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvFolders.AllowDrop = True
        Me.tvFolders.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvFolders.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvFolders.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvFolders.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvFolders.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvFolders.Location = New System.Drawing.Point(10, 20)
        Me.tvFolders.MultiNodeDragDropAllowed = False
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.Nodes.AddRange(New DevComponents.AdvTree.Node() {Me.Node1})
        Me.tvFolders.NodesConnector = Me.NodeConnector1
        Me.tvFolders.NodeStyle = Me.ElementStyle1
        Me.tvFolders.PathSeparator = "/"
        Me.tvFolders.Size = New System.Drawing.Size(411, 243)
        Me.tvFolders.Styles.Add(Me.ElementStyle1)
        Me.tvFolders.TabIndex = 3
        '
        'Node1
        '
        Me.Node1.Expanded = True
        Me.Node1.Name = "Node1"
        Me.Node1.Text = "Node1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'pgServer
        '
        '
        '
        '
        Me.pgServer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.pgServer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pgServer.Location = New System.Drawing.Point(3, 296)
        Me.pgServer.Name = "pgServer"
        Me.pgServer.Size = New System.Drawing.Size(423, 16)
        Me.pgServer.TabIndex = 2
        Me.pgServer.Text = "ProgressBarX1"
        '
        'txtPath
        '
        '
        '
        '
        Me.txtPath.Border.Class = "TextBoxBorder"
        Me.txtPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPath.Location = New System.Drawing.Point(10, 270)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(411, 21)
        Me.txtPath.TabIndex = 1
        Me.txtPath.Tag = "memo"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnGo, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtserverUser, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtserverPassword, 1, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 46)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(421, 69)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(422, 38)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Please enter the credentials used to access your report server (if any) and then " & _
    "click the ""Connect"" button to browse the server for the required report"
        Me.Label1.WordWrap = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 475)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(439, 31)
        Me.FlowLayoutPanel1.TabIndex = 14
        '
        'chkFormsAuth
        '
        '
        '
        '
        Me.chkFormsAuth.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkFormsAuth.Location = New System.Drawing.Point(15, 122)
        Me.chkFormsAuth.Name = "chkFormsAuth"
        Me.chkFormsAuth.Size = New System.Drawing.Size(371, 23)
        Me.chkFormsAuth.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkFormsAuth.TabIndex = 15
        Me.chkFormsAuth.Text = "Server uses custom/forms authentication"
        '
        'frmReportServer
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(439, 506)
        Me.Controls.Add(Me.chkFormsAuth)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.grpReports)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportServer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reports Server Browser"
        Me.grpReports.ResumeLayout(False)
        CType(Me.tvFolders, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmReportServer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

    End Sub
    ''' <summary>
    ''' Sharepoint integrated mode
    ''' </summary>
    ''' <param name="oRpt"></param>
    ''' <param name="sUrl"></param>
    ''' <param name="serverUser"></param>
    ''' <param name="serverPassword"></param>
    ''' <param name="currentPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function newGetReports(ByRef oRpt As ReportServer_2006.ReportingService2006, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "", Optional ByRef formsAuth As Boolean = False) As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice2006.asmx"
            Else
                sUrl &= "reportservice2006.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword
        chkFormsAuth.Checked = formsAuth

        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text
        formsAuth = chkFormsAuth.Checked

        If txtPath.Text = "" Then Return Nothing

        If txtPath.Text.StartsWith("/") Then txtPath.Text = txtPath.Text.Remove(0, 1)

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function
    ''' <summary>
    ''' SSRS 2005/2008 native mode
    ''' </summary>
    ''' <param name="oRpt"></param>
    ''' <param name="sUrl"></param>
    ''' <param name="serverUser"></param>
    ''' <param name="serverPassword"></param>
    ''' <param name="currentPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function newGetReports(ByRef oRpt As rsClients.rsClient2008, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "", Optional formsAuth As Boolean = False) As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice2005.asmx"
            Else
                sUrl &= "reportservice2005.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword
        chkFormsAuth.Checked = formsAuth
        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text
        formsAuth = chkFormsAuth.Checked

        If txtPath.Text = "" Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function

    ''' <summary>
    ''' SSRS 2008/2006 native and sharepoint mode
    ''' </summary>
    ''' <param name="oRpt"></param>
    ''' <param name="sUrl"></param>
    ''' <param name="serverUser"></param>
    ''' <param name="serverPassword"></param>
    ''' <param name="currentPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function newGetReports(ByRef oRpt As rsClients.rsClient2010, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "", Optional ByRef formsAuth As Boolean = False) As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice2010.asmx"
            Else
                sUrl &= "reportservice2010.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword
        chkFormsAuth.Checked = formsAuth

        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text
        formsAuth = chkFormsAuth.Checked

        If txtPath.Text = "" Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function
    ''' <summary>
    ''' SSRS 2000
    ''' </summary>
    ''' <param name="oRpt"></param>
    ''' <param name="sUrl"></param>
    ''' <param name="serverUser"></param>
    ''' <param name="serverPassword"></param>
    ''' <param name="currentPath"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overloads Function newGetReports(ByRef oRpt As rsClients.rsClient, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "", Optional formsAuth As Boolean = False) As String 'EDIT
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice.asmx"
            Else
                sUrl &= "reportservice.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword
        chkFormsAuth.Checked = formsAuth

        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text
        formsAuth = chkFormsAuth.Checked
        If txtPath.Text = "" Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function

    'this function is no longer used
    Public Function _GetReport(ByVal sUrl As String, Optional ByVal cred As System.Net.NetworkCredential = Nothing) As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice.asmx"
            Else
                sUrl &= "reportservice.asmx"
            End If
        End If

        m_oRpt = New rsClients.rsClient(sUrl)

        m_oRpt.Url = sUrl

        If cred Is Nothing Then
            m_oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
        Else
            m_oRpt.Credentials = cred
        End If

        AppStatus(True)

        If LoadServer(sUrl) = False Then
            Return Nothing
        End If

        AppStatus(False)

        oUI.BusyProgress(, , True)

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        If tvFolders.SelectedNode Is Nothing Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function
    Public Function LoadServer(ByVal sUrl As String) As Boolean

        Try
            tvFolders.Nodes.Clear()

            Dim oNode As DevComponents.AdvTree.Node
            Dim oMaster As Node
            Dim sName As String
            Dim sPath As String
            Dim NodeFound As Boolean
            Dim I As Integer = 1

            Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)
            oUI.BusyProgress(10, "Loading folders...")

            oMaster = New Node(sUrl)
            oMaster.Image = resizeImage(My.Resources.server_document, 16, 16)
            oMaster.Tag = "master"
            tvFolders.Nodes.Add(oMaster)

            Dim oArr As ArrayList = ReadFolders()

            For Each o As Object In oArr

                oUI.BusyProgress((I / oArr.Count) * 100, "Loading folders...")

                I += 1

                sName = Convert.ToString(o).Split("|")(0)
                sPath = Convert.ToString(o).Split("|")(1)

                If FindOccurence(sPath, "/") = 1 Then
                    oNode = New Node(sName)
                    oNode.Image = folderImage
                    oMaster.Nodes.Add(oNode)
                    oNode.Tag = "folder"
                Else
                    Dim StartNode As Node = tvFolders.Nodes(0)
                    Dim x As Node

                    For Each s As String In sPath.Split("/")
                        If s.Length > 0 Then

                            NodeFound = False

                            For Each x In StartNode.Nodes
                                If x.Text = s And x.Tag = "folder" Then
                                    NodeFound = True
                                    Exit For
                                End If
                            Next

                            If NodeFound = False Then
                                oNode = New Node(s)
                                oNode.Image = folderImage
                                StartNode.Nodes.Add(oNode)
                                oNode.Tag = "folder"
                                StartNode = oNode
                            Else
                                StartNode = x
                            End If
                        End If
                    Next

                End If

            Next

            AddReports(oMaster)

            oMaster.Expand()

            Return True
        Catch ex As Exception
            Dim suggest As String

            suggest = "1. Please provide a valid user name and password" & vbCrLf & _
            "2. Check to make sure that the report server is online" & vbCrLf & _
            "3. Check your network connection and make sure that you can connect to the report server"

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), suggest)
            Return False
        End Try

    End Function

    Private Sub AddReports(ByVal oNode As Node)
        For Each o As Node In oNode.Nodes
            If o.Tag = "folder" Then
                AddReports(o)
            End If
        Next

        Dim reportImage As Image = resizeImage(My.Resources.document_chart2, 16, 16)
        Dim oChild As Node
        Dim sPath As String = oNode.FullPath.Replace(tvFolders.Nodes(0).Text, "")

        If sPath.Length = 0 Then Return

        Dim oArr As ArrayList = ReadReports(sPath)
        Dim I As Integer = 1

        For Each x As Object In oArr

            oUI.BusyProgress((I / oArr.Count) * 100, "Loading reports...")

            I += 1

            oChild = New Node(x)
            oChild.Image = reportImage
            oNode.Nodes.Add(oChild)
            oChild.Tag = "report"
        Next
    End Sub
    Private Function ReadFolders() As ArrayList
        Dim srcFolderCond As ReportServer.SearchCondition
        Dim srchResults As ReportServer.CatalogItem()
        Dim srchResult As ReportServer.CatalogItem
        Dim aryResults As New ArrayList

        With m_oRpt

            srcFolderCond = New ReportServer.SearchCondition

            With srcFolderCond
                .ConditionSpecified = False
                .Condition = ReportServer.ConditionEnum.Equals
                .Name = "Name"
            End With

            srchResults = .FindItems("/", ReportServer.BooleanOperatorEnum.And, New ReportServer.SearchCondition() {srcFolderCond})

            For Each srchResult In srchResults

                If srchResult.Type = ReportServer.ItemTypeEnum.Folder Then
                    If srchResult.Name <> "/" Then aryResults.Add(srchResult.Name & "|" & srchResult.Path)
                End If
            Next
        End With

        Return aryResults
    End Function

    Private Function ReadReports(ByVal Folder As String) As ArrayList
        Dim srcFolderCond As ReportServer.SearchCondition
        Dim srchResults As ReportServer.CatalogItem()
        Dim srchResult As ReportServer.CatalogItem
        Dim aryResults As New ArrayList

        With m_oRpt

            srcFolderCond = New ReportServer.SearchCondition
            With srcFolderCond
                .ConditionSpecified = False
                .Condition = ReportServer.ConditionEnum.Equals
                .Name = "Name"
            End With

            'srchResults = .FindItems(Folder, ReportServer.BooleanOperatorEnum.And, New ReportServer.SearchCondition() {srcFolderCond})

            srchResults = .ListChildren(Folder, False)

            For Each srchResult In srchResults
                If srchResult.Type = ReportServer.ItemTypeEnum.Report Then
                    Dim PathOnly As String = ""

                    For I As Integer = 0 To srchResult.Path.Split("/").GetUpperBound(0) - 1
                        PathOnly &= srchResult.Path.Split("/")(I) & "/"
                    Next


                    If PathOnly = Folder & "/" Then
                        If showOwner = True Then
                            aryResults.Add(srchResult.CreatedBy & "." & srchResult.Name)
                        Else
                            aryResults.Add(srchResult.Name)
                        End If
                    End If

                End If
            Next
        End With

        Return aryResults
    End Function



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtPath.Text = "" Then
            MessageBox.Show("Please select a report", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        ElseIf Me.m_ReadOnly = True Then
            Try
                Dim cat As ReportServer.CatalogItem() = Me.m_oRpt.ListChildren("/", False)
            Catch ex As Exception
                MessageBox.Show("The user name or password you have provided for the report server is invalid. Please try again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End Try
        End If

        Close()
    End Sub

    Private Sub tvFolders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        If tvFolders.SelectedNode Is Nothing Then
            cmdOK.Enabled = False
            Return
        End If

        Select Case tvFolders.SelectedNode.Tag
            Case "folder", "master"
                Return
            Case "report"
                cmdOK_Click(sender, e)
            Case Else
                Return
        End Select
    End Sub
    Function replaceServerNameWithIP(url As String) As String
        Try
            Dim server As String = url.Split("/")(2) 'http://myserver
            Dim ipAddressses As System.Net.IPAddress() = System.Net.Dns.GetHostAddresses(server)
            Dim serverIP As String = ""

            If ipAddressses IsNot Nothing Then
                For Each ipAddress As System.Net.IPAddress In ipAddressses
                    serverIP = ipAddress.ToString
                Next
            End If

            If serverIP <> "" Then
                url = url.Replace(server, serverIP)
            End If

            Return url
        Catch ex As Exception
            Return url
        End Try
    End Function

    Dim proxyAttempt As Boolean = False

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click

        Try
            AppStatus(True)
            tvFolders.Nodes.Clear()

            Dim reportImage As Image = resizeImage(My.Resources.document_chart2, 16, 16)
            Dim oMaster As Node
            Dim I As Integer = 0
            Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)
            Dim logFile As String = "serverBrowser.debug"

            If tvFolders.Nodes.Count = 0 Then
                oMaster = New Node(m_serverUrl)
                oMaster.Image = resizeImage(My.Resources.server_document, 16, 16)
                oMaster.Tag = "master"
                tvFolders.Nodes.Add(oMaster)
            End If

            clsMarsDebug.writeToDebug(logFile, "setting up server credentials", False)

            If Me.txtserverUser.Text = "" Then
                clsMarsDebug.writeToDebug(logFile, "Using Windows default credentials", True)

                m_oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials

                If chkFormsAuth.Checked Then
                    Try
                        '//for forms auth
                        Dim netCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential

                        netCredentials = System.Net.CredentialCache.DefaultCredentials

                        m_oRpt.LogonUser(netCredentials.UserName, netCredentials.Password, Nothing)
                    Catch : End Try
                End If
            Else
                Dim domain As String = Nothing
                Dim tmpUser As String = txtserverUser.Text

                getDomainAndUserFromString(tmpUser, domain)

                'If chkFormsAuth.Checked = False Then
                clsMarsDebug.writeToDebug(logFile, "Using credentials: " & domain & "\" & tmpUser, True)

                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, Me.txtserverPassword.Text, domain)
                m_oRpt.Credentials = cred
                'Else
                '//forms auth
                clsMarsDebug.writeToDebug(logFile, "Using forms authentication for " & domain & "\" & tmpUser, True)
                Try
                    m_oRpt.LogonUser(tmpUser, Me.txtserverPassword.Text, domain)

                    Try

                        If (m_oRpt.CheckAuthorized()) Then
                            Dim type As ReportServer.ItemTypeEnum = m_oRpt.GetItemType("/")

                            Console.WriteLine(type)

                        End If

                    Catch ex As Exception
                        Console.WriteLine("Exception on call to GetItemType.")
                    End Try

                    m_oRpt.SessionHeaderValue = New ReportServer.SessionHeader()
                Catch : End Try
            End If
            'End If

            pgServer.Visible = True

            If oMaster IsNot Nothing Then
                Dim catItems As Object

                If TypeOf m_oRpt Is rsClients.rsClient Then
                    catItems = New ReportServer.CatalogItem
                ElseIf TypeOf m_oRpt Is rsClients.rsClient2008 Then
                    catItems = New ReportServer_2005.CatalogItem
                ElseIf TypeOf m_oRpt Is rsClients.rsClient2010 Then
                    catItems = New ReportServer_2010.CatalogItem
                Else
                    catItems = New ReportServer_2006.CatalogItem
                End If

                clsMarsDebug.writeToDebug(logFile, "Enumerating catalog items", True)

                If TypeOf catItems Is ReportServer_2006.CatalogItem Then
                    catItems = m_oRpt.ListChildren("/") ', False)
                ElseIf TypeOf catItems Is ReportServer_2010.CatalogItem Then
                    catItems = m_oRpt.ListChildren("/", False)
                Else
                    catItems = m_oRpt.ListChildren("/", False)
                End If



                Dim count As Integer = catItems.Length

                clsMarsDebug.writeToDebug(logFile, "Found " & count & " catalog items", True)

                If TypeOf m_oRpt Is ReportServer_2010.ReportingService2010 Then
                    For Each catitem As Object In catItems
                        clsMarsDebug.writeToDebug(logFile, "Catalog Item Name: " & catitem.Name, True)
                        'clsMarsDebug.writeToDebug(logFile, "Catalog Item Type: " & catitem.TypeName, True)

                        If catitem.TypeName = "Folder" Or catitem.TypeName = "Site" Then 'a folder or a site

                            Dim itemPath As String = catitem.path

                            If itemPath.StartsWith("/") Then itemPath = itemPath.Remove(0, 1)

                            Dim child As Node = New Node(itemPath)
                            child.Image = folderImage
                            oMaster.Nodes.Add(child)
                            child.Tag = "folder"
                        ElseIf catitem.Typename = "2" Or catitem.TypeName.ToLower = "report" Then
                            Dim child As Node = New Node(catitem.Name)
                            child.Image = reportImage
                            oMaster.Nodes.Add(child)
                            child.Tag = "report"
                        End If

                        pgServer.Value = (I / count) * 100
                        Application.DoEvents()
                        System.Threading.Thread.Sleep(25)
                        I += 1
                    Next
                Else
                    For Each catitem As Object In catItems
                        clsMarsDebug.writeToDebug(logFile, "Catalog Item Name: " & catitem.Name, True)
                        'clsMarsDebug.writeToDebug(logFile, "Catalog Item Type: " & catitem.TypeName, True)

                        If catitem.Type = 1 Or catitem.Type = 6 Then 'a folder or a site

                            Dim itemPath As String = catitem.path

                            If itemPath.StartsWith("/") Then itemPath = itemPath.Remove(0, 1)

                            Dim child As Node = New Node(itemPath)
                            child.Image = folderImage
                            oMaster.Nodes.Add(child)
                            child.Tag = "folder"
                        ElseIf catitem.Type = 2 Then '//a report
                            Dim child As Node = New Node(catitem.Name)
                            child.Image = reportImage
                            oMaster.Nodes.Add(child)
                            child.Tag = "report"
                        End If

                        pgServer.Value = (I / count) * 100
                        Application.DoEvents()
                        System.Threading.Thread.Sleep(25)
                        I += 1
                    Next
                End If
            End If

            pgServer.Value = 0
            pgServer.Visible = False

            Me.grpReports.Enabled = Not (Me.m_ReadOnly)

            If oMaster IsNot Nothing Then oMaster.Expand()
        Catch ex As Exception
            If firstAttempt = False Then
                If ex.Message.Contains("407") And proxyAttempt = False Then
                    proxyAttempt = True

                    m_oRpt.url = replaceServerNameWithIP(m_oRpt.URL)

                    btnGo_Click(Nothing, Nothing)
                Else
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End If
            End If

            tvFolders.Nodes.Clear()
        Finally
            AppStatus(False)
        End Try
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        If txtPath.Text.Length = 0 Then
            cmdOK.Enabled = False
        Else
            cmdOK.Enabled = True
        End If
    End Sub

    Private Sub tvFolders_AfterNodeSelect(sender As Object, e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvFolders.AfterNodeSelect
        Try
            Dim reportImage As Image = resizeImage(My.Resources.document_chart2, 16, 16)
            Dim folderImage As Image = resizeImage(My.Resources.Folder_256x2561, 16, 16)
            Dim logFile As String = "serverBrowser.debug"

            If tvFolders.SelectedNode Is Nothing Then
                cmdOK.Enabled = False
                Return
            ElseIf Me.m_ReadOnly = True Then
                cmdOK.Enabled = True
                Me.grpReports.Enabled = False
                Return
            End If

            Select Case tvFolders.SelectedNode.Tag
                Case "folder", "master"
                    'cmdOK.Enabled = False
                    txtPath.Text = ""
                Case "report"
                    'cmdOK.Enabled = True
                    txtPath.Text = tvFolders.SelectedNode.FullPath.Replace(Me.m_serverUrl, "")

                    If txtPath.Text.StartsWith("/") Then txtPath.Text = txtPath.Text.Remove(0, 1)
                    Return
                Case Else
                    'cmdOK.Enabled = False
                    txtPath.Text = ""
                    Return
            End Select

            Dim path As String = tvFolders.SelectedNode.FullPath.Replace(tvFolders.Nodes(0).Text, "")
            Dim selNode As Node = tvFolders.SelectedNode

            If path = "" Then Return

            If selNode.Tag <> "folder" Then
                Return
            End If

            If selNode.Nodes.Count > 0 Then Return

            Dim catItems As Object

            If TypeOf m_oRpt Is rsClients.rsClient Then
                catItems = New ReportServer.CatalogItem
            ElseIf TypeOf m_oRpt Is rsClients.rsClient2008 Then
                catItems = New ReportServer_2005.CatalogItem
            ElseIf TypeOf m_oRpt Is rsClients.rsClient2010 Then
                catItems = New ReportServer_2010.CatalogItem
            Else
                catItems = New ReportServer_2006.CatalogItem
            End If

            clsMarsDebug.writeToDebug(logFile, "Getting subitems from fullpath...", True)

            If TypeOf m_oRpt Is ReportServer_2006.ReportingService2006 Then
                Try
                    Dim fullPath As String = tvFolders.SelectedNode.FullPath
                    fullPath = fullPath.Replace(tvFolders.Nodes(0).Text & "/", "")

                    clsMarsDebug.writeToDebug(logFile, "Full path: " & fullPath, True)

                    catItems = m_oRpt.ListChildren(fullPath)
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
                    Return
                End Try
            ElseIf TypeOf m_oRpt Is ReportServer_2010.ReportingService2010 Then
                Try
                    Dim fullPath As String = tvFolders.SelectedNode.FullPath
                    fullPath = fullPath.Replace(tvFolders.Nodes(0).Text & "/", "")

                    clsMarsDebug.writeToDebug(logFile, "Full path: " & fullPath, True)

                    catItems = m_oRpt.ListChildren(fullPath, False)

                Catch ex As Exception
                    If ex.Message.Contains("slash") Then
                        Try
                            Dim fullPath As String = tvFolders.SelectedNode.FullPath
                            fullPath = fullPath.Replace(tvFolders.Nodes(0).Text & "/", "")

                            clsMarsDebug.writeToDebug(logFile, "Full path: " & fullPath, True)

                            If fullPath.StartsWith("/") = False Then fullPath = "/" & fullPath

                            catItems = m_oRpt.ListChildren(fullPath, False)
                        Catch exx As Exception
                            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
                            Return
                        End Try
                    Else
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
                        Return
                    End If
                End Try
            Else
                catItems = m_oRpt.ListChildren(path, False)
            End If


            Me.grpReports.Enabled = False
            'tvFolders.BeginUpdate()

            Dim I As Integer = 0
            Dim count As Integer

            pgServer.Visible = True

            Dim items As DataTable = New DataTable("ServerItems")

            items.Columns.Add("Name")
            items.Columns.Add("Type")

            If TypeOf m_oRpt Is ReportServer_2010.ReportingService2010 Then

                For Each catitem As Object In catItems
                    clsMarsDebug.writeToDebug(logFile, "Catalog Item Name: " & catitem.Name, True)
                    ' clsMarsDebug.writeToDebug(logFile, "Catalog Item Type: " & catitem.TypeName, True)

                    Dim row As DataRow

                    If catitem.Typename = "Folder" Or catitem.Typename = "Site" Then 'a folder
                        row = items.Rows.Add
                        row.Item("Name") = catitem.Name
                        row.Item("Type") = "folder"
                        'ElseIf catitem.Type = 2 Or catitem.Type = 4 Then 'a report/linked report
                    ElseIf catitem.TypeName = "Report" Then 'report
                        row = items.Rows.Add
                        row.Item("Name") = catitem.Name
                        row.Item("Type") = "report"
                    End If
                Next
            Else
                For Each catitem As Object In catItems
                    clsMarsDebug.writeToDebug(logFile, "Catalog Item Name: " & catitem.Name, True)
                    'clsMarsDebug.writeToDebug(logFile, "Catalog Item Type: " & catitem.TypeName, True)

                    Dim row As DataRow

                    If catitem.Type = 1 Or catitem.Type = 6 Then 'a folder
                        row = items.Rows.Add
                        row.Item("Name") = catitem.Name
                        row.Item("Type") = "folder"
                        'ElseIf catitem.Type = 2 Or catitem.Type = 4 Then 'a report/linked report
                    ElseIf catitem.Type = 2 Then 'report
                        row = items.Rows.Add
                        row.Item("Name") = catitem.Name
                        row.Item("Type") = "report"
                    End If
                Next
            End If

            Dim foundRows() As DataRow = items.Select("", "TYPE ASC")
            count = foundRows.GetLength(0)

            For Each row As DataRow In foundRows
                Dim childNode As Node = New Node(row.Item("Name"))
                childNode.Tag = row.Item("Type")

                If childNode.Tag = "report" Then
                    childNode.Image = reportImage
                Else
                    childNode.Image = folderImage
                End If

                selNode.Nodes.Add(childNode)

                pgServer.Value = (I / count) * 100
                Application.DoEvents()
                System.Threading.Thread.Sleep(25)
                I += 1
            Next


            pgServer.Value = 0
            pgServer.Visible = False
            selNode.Expand()
            Me.grpReports.Enabled = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub tvFolders_DoubleClick1(sender As Object, e As System.EventArgs) Handles tvFolders.DoubleClick
        If tvFolders.SelectedNode Is Nothing Then
            cmdOK.Enabled = False
            Return
        End If

        Select Case tvFolders.SelectedNode.Tag
            Case "folder", "master"
                Return
            Case "report"
                cmdOK_Click(sender, e)
            Case Else
                Return
        End Select
    End Sub
End Class
