Public Class frmDBResults
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvResults As DevComponents.DotNetBar.Controls.ListViewEx
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDBResults))
        Me.lsvResults = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.SuspendLayout()
        '
        'lsvResults
        '
        Me.lsvResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.lsvResults.Border.Class = "ListViewBorder"
        Me.lsvResults.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvResults.FullRowSelect = True
        Me.lsvResults.Location = New System.Drawing.Point(8, 8)
        Me.lsvResults.Name = "lsvResults"
        Me.lsvResults.Size = New System.Drawing.Size(557, 382)
        Me.lsvResults.TabIndex = 0
        Me.lsvResults.UseCompatibleStateImageBehavior = False
        Me.lsvResults.View = System.Windows.Forms.View.Details
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(490, 396)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'frmDBResults
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(577, 422)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.lsvResults)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmDBResults"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Query Results"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmDBResults_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub _ShowResults(ByVal oRs As ADODB.Recordset)

        lsvResults.Items.Clear()

        Try
            oRs.Requery()

            For Each field As ADODB.Field In oRs.Fields
                Dim col As ColumnHeader = New ColumnHeader()
                col.Text = field.Name

                lsvResults.Columns.Add(col)

                col.Width = lsvResults.Width / oRs.Fields.Count
            Next

            If oRs.EOF = True Then
                MessageBox.Show("The query did not return any values", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)
                Return
            End If

            Dim count As Integer = 0

            Do While oRs.EOF = False
                Dim listItem As ListViewItem
                Dim value As String

                listItem = lsvResults.Items.Add(IsNull(oRs(0).Value))

                For Each field As ADODB.Field In oRs.Fields
                    If field.Name <> oRs.Fields(0).Name Then

                        Try
                            listItem.SubItems.Add(IsNull(field.Value))
                        Catch ex As Exception
                            If field.Type = ADODB.DataTypeEnum.adBinary Then
                                listItem.SubItems.Add("<Binary Data>")
                            End If
                        End Try
                    End If
                Next

                count += 1

                oRs.MoveNext()
            Loop

            Try
                lsvResults.Columns(0).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lsvResults.Columns(1).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
                lsvResults.Columns(2).AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent)
            Catch : End Try

            oRs.Close()

            Me.Text = "Query Results (" & count & " records found)"

            Me.ShowDialog()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, "frmDBresults._ShowResults", _
            _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Private Sub lsvResults_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvResults.Resize
        Try
            For Each col As ColumnHeader In lsvResults.Columns
                If col.Index > 4 Then col.Width = lsvResults.Width / lsvResults.Columns.Count
            Next
        Catch : End Try
    End Sub

    Private Sub lsvResults_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvResults.SelectedIndexChanged

    End Sub
End Class
