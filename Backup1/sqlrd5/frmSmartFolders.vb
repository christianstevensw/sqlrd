Public Class frmSmartFolders
    Inherits DevComponents.DotNetBar.Office2007Form
    Const S1 As String = "Step 1: Smart Folder Name"
    Friend WithEvents stabSmartFolder As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDefinition As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Const S2 As String = "Step 2: Smart Folder Definition"
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvCriteria As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSmartFolders))
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lsvCriteria = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.stabSmartFolder = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDefinition = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Step1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.stabSmartFolder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabSmartFolder.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.Transparent
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Location = New System.Drawing.Point(3, 3)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(424, 352)
        Me.Step1.TabIndex = 1
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(16, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(392, 21)
        Me.txtName.TabIndex = 1
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        '
        'txtDesc
        '
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.Location = New System.Drawing.Point(16, 96)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(392, 184)
        Me.txtDesc.TabIndex = 1
        Me.txtDesc.Tag = "memo"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Description (optional)"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(583, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 14
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(421, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 15
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(502, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 20
        Me.cmdNext.Text = "&Next"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.Color.Transparent
        Me.Step2.Controls.Add(Me.GroupBox2)
        Me.Step2.Controls.Add(Me.cmbAnyAll)
        Me.Step2.Controls.Add(Me.Label4)
        Me.Step2.Controls.Add(Me.Label5)
        Me.Step2.Location = New System.Drawing.Point(3, 3)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(432, 352)
        Me.Step2.TabIndex = 21
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lsvCriteria)
        Me.GroupBox2.Controls.Add(Me.cmdAddWhere)
        Me.GroupBox2.Controls.Add(Me.cmdRemoveWhere)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbOperator)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 48)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 296)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'lsvCriteria
        '
        '
        '
        '
        Me.lsvCriteria.Border.Class = "ListViewBorder"
        Me.lsvCriteria.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvCriteria.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvCriteria.FullRowSelect = True
        Me.lsvCriteria.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvCriteria.Location = New System.Drawing.Point(8, 72)
        Me.lsvCriteria.Name = "lsvCriteria"
        Me.lsvCriteria.Size = New System.Drawing.Size(384, 216)
        Me.lsvCriteria.TabIndex = 15
        Me.lsvCriteria.UseCompatibleStateImageBehavior = False
        Me.lsvCriteria.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Criteria"
        Me.ColumnHeader1.Width = 210
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = ""
        Me.ColumnHeader2.Width = 168
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(352, 40)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 14
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(256, 40)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 13
        '
        'cmbColumn
        '
        Me.cmbColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Items.AddRange(New Object() {"Bcc", "Cc", "Description", "EndDate", "ErrMsg", "FolderName", "Frequency", "Keyword", "Message", "NextRun", "ReportTitle", "ScheduleName", "ScheduleType", "SendTo", "StartDate", "StartTime", "Status", "Subject"})
        Me.cmbColumn.Location = New System.Drawing.Point(8, 16)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(112, 21)
        Me.cmbColumn.Sorted = True
        Me.cmbColumn.TabIndex = 8
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", ">=", "<=", ">", "<", "STARTS WITH", "ENDS WITH", "CONTAINS", "DOES NOT CONTAIN"})
        Me.cmbOperator.Location = New System.Drawing.Point(136, 16)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(104, 21)
        Me.cmbOperator.TabIndex = 8
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbValue.Location = New System.Drawing.Point(256, 16)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(136, 21)
        Me.cmbValue.TabIndex = 8
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.ItemHeight = 13
        Me.cmbAnyAll.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(64, 14)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 8
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Match"
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(136, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(288, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "of these conditions:"
        '
        'stabSmartFolder
        '
        '
        '
        '
        '
        '
        '
        Me.stabSmartFolder.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabSmartFolder.ControlBox.MenuBox.Name = ""
        Me.stabSmartFolder.ControlBox.Name = ""
        Me.stabSmartFolder.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSmartFolder.ControlBox.MenuBox, Me.stabSmartFolder.ControlBox.CloseBox})
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabSmartFolder.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabSmartFolder.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabSmartFolder.Location = New System.Drawing.Point(0, 0)
        Me.stabSmartFolder.Name = "stabSmartFolder"
        Me.stabSmartFolder.ReorderTabsEnabled = True
        Me.stabSmartFolder.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabSmartFolder.SelectedTabIndex = 1
        Me.stabSmartFolder.Size = New System.Drawing.Size(661, 363)
        Me.stabSmartFolder.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabSmartFolder.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabSmartFolder.TabIndex = 26
        Me.stabSmartFolder.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabDefinition})
        Me.stabSmartFolder.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabSmartFolder.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(98, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(563, 363)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabDefinition
        '
        'tabDefinition
        '
        Me.tabDefinition.AttachedControl = Me.SuperTabControlPanel2
        Me.tabDefinition.GlobalItem = False
        Me.tabDefinition.Image = CType(resources.GetObject("tabDefinition.Image"), System.Drawing.Image)
        Me.tabDefinition.Name = "tabDefinition"
        Me.tabDefinition.Text = "Definition"
        Me.tabDefinition.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.ReflectionImage1)
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(90, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(571, 363)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'ReflectionImage1
        '
        Me.ReflectionImage1.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(427, 99)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(144, 230)
        Me.ReflectionImage1.TabIndex = 2
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 365)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(661, 30)
        Me.FlowLayoutPanel1.TabIndex = 27
        '
        'frmSmartFolders
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(661, 395)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabSmartFolder)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmSmartFolders"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Smart Folder Wizard"
        Me.Step1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.stabSmartFolder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabSmartFolder.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim nStep As Integer = 0
    Private Sub frmSmartFolders_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Step1.BringToFront()

        cmbAnyAll.Text = "All"
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn.SelectedIndexChanged
        Dim oData As New clsMarsData
        Dim SQL(3) As String
        Dim oRs As ADODB.Recordset

        cmbValue.Items.Clear()
        cmbOperator.Items.Clear()

        SetError(sender, String.Empty)

        Select Case cmbColumn.Text
            Case "Status", "ScheduleType"
                With cmbOperator.Items
                    .Add("=")
                End With
            Case "Message", "Description", "Keyword", "ScheduleName", "ReportTitle", "SendTo", _
                "Cc", "Bcc", "Subject", "ErrMsg"
                With cmbOperator.Items
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                    .Add("IS EMPTY")
                    .Add("IS NOT EMPTY")
                End With
            Case "ScheduleType", "Frequency", "DestinationType", "PrinterName", "OutputFormat", _
                 "FTPServer", _
                 "FTPUserName", "FolderName"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
            Case "StartDate", "EndDate", "NextRun"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                End With
            Case Else
                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
        End Select

        Select Case cmbColumn.Text.ToLower
            Case "frequency"
                With cmbValue.Items
                    .Add("Daily")
                    .Add("Weekly")
                    .Add("Monthly")
                    .Add("Yearly")
                    .Add("Custom")
                    .Add("None")
                End With
            Case "status"
                With cmbValue.Items
                    .Add("0")
                    .Add("1")
                End With
            Case "destinationtype"
                With cmbValue.Items
                    .Add("Email")
                    .Add("Disk")
                    .Add("Printer")
                    .Add("FTP")
                End With
            Case "printername"
                Dim strItem As String
                Dim oPrint As New System.Drawing.Printing.PrinterSettings

                For Each strItem In Printing.PrinterSettings.InstalledPrinters
                    cmbValue.Items.Add(strItem)
                Next
            Case "outputformat"
                With cmbValue.Items
                    .Add("Acrobat Format (*.pdf)")
                    .Add("Crystal Reports (*.rpt)")
                    .Add("CSV (*.csv)")
                    .Add("Data Interchange Format (*.dif)")
                    .Add("dBase II (*.dbf)")
                    .Add("dBase III (*.dbf)")
                    .Add("dBase IV (*.dbf)")
                    .Add("HTML (*.htm)")
                    .Add("Lotus 1-2-3 (*.wk1)")
                    .Add("Lotus 1-2-3 (*.wk3)")
                    .Add("Lotus 1-2-3 (*.wk4)")
                    .Add("Lotus 1-2-3 (*.wks)")
                    .Add("MS Excel - Data Only (*.xls)")
                    .Add("MS Excel 7 (*.xls)")
                    .Add("MS Excel 8 (*.xls)")
                    .Add("MS Excel 97-2000 (*.xls)")
                    .Add("MS Word (*.doc)")
                    .Add("ODBC (*.odbc)")
                    .Add("Rich Text Format (*.rtf)")
                    .Add("Tab Seperated (*.txt)")
                    .Add("Text (*.txt)")
                    .Add("TIFF (*.tif)")
                    .Add("XML (*.xml)")
                End With
            Case "schedulename"
                SQL(0) = "SELECT DISTINCT ReportTitle FROM ReportAttr"
                SQL(1) = "SELECT DISTINCT PackageName FROM PackageAttr"
                SQL(2) = "SELECT DISTINCT AutoName FROM AutomationAttr"
                SQL(3) = "SELECT DISTINCT EventName FROM EventAttr"

                For Each s As String In SQL
                    oRs = clsMarsData.GetData(s)

                    If Not oRs Is Nothing Then
                        Do While oRs.EOF = False
                            cmbValue.Items.Add(oRs.Fields(0).Value)
                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If
                Next
            Case "foldername"
                SQL(0) = "SELECT DISTINCT FolderName FROM Folders"

                oRs = clsMarsData.GetData(SQL(0))

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        cmbValue.Items.Add(oRs(0).Value)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()

                End If
            Case "scheduletype"
                With cmbValue.Items
                    .Add("Single Schedule")
                    .Add("Package Schedule")
                    .Add("Automation Schedule")
                    .Add("Event-Based Schedule")
                    .Add("Event-Based Packages")
                    .Add("Dynamic Schedules")
                    .Add("Dynamic Packages")
                    .Add("Data-Driven Schedules")
                    .Add("Data-Driven Packages")
                    .Add("Bursting Schedules")
                End With
        End Select
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim olsv As ListViewItem
        Dim sCriteria As String

        If cmbColumn.Text.Length = 0 Then
            SetError(cmbColumn, "Please select the column for the criteria")
            cmbColumn.Focus()
            Return
        ElseIf cmbOperator.Text.Length = 0 Then
            SetError(cmbOperator, "Please select the operator the criteria")
            cmbOperator.Focus()
            Return
        End If

        olsv = New ListViewItem
        olsv.Text = cmbColumn.Text

        Select Case cmbOperator.Text.ToLower
            Case "starts with"
                sCriteria = "LIKE '" & cmbValue.Text & "%'"
            Case "ends with"
                sCriteria = "LIKE '%" & cmbValue.Text & "'"
            Case "contains"
                sCriteria = "LIKE '%" & cmbValue.Text & "%'"
            Case "does not contain"
                sCriteria = " NOT LIKE '%" & cmbValue.Text & "%'"
            Case "is empty"
                sCriteria = " IS EMPTY"
            Case "is not empty"
                sCriteria = " IS NOT EMPTY"
            Case Else
                sCriteria = cmbOperator.Text & " '" & cmbValue.Text & "'"
        End Select

        olsv.SubItems.Add(sCriteria)

        lsvCriteria.Items.Add(olsv)
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvCriteria.SelectedItems.Count = 0 Then Return

        lsvCriteria.SelectedItems(0).Remove()
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Dim oData As New clsMarsData

        Select Case stabSmartFolder.SelectedTab.Text
            Case "General"
                If txtName.Text.Length = 0 Then
                    setError(txtName, "Please enter the name for the smart folder")
                    txtName.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("SmartFolders", "SmartName", _
                    txtName.Text, True) Then

                    setError(txtName, "A smart folder with this name already exists")
                    txtName.Focus()

                    Return
                End If

                tabDefinition.Visible = True
                stabSmartFolder.SelectedTab = tabDefinition
            Case "Definition"
                If lsvCriteria.Items.Count = 0 Then
                    SetError(lsvCriteria, "Please create the selection criteria")
                    Return
                End If

                cmdNext.Enabled = False
                cmdFinish.Enabled = True
        End Select
    End Sub



    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click


        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim Success As Boolean
        Dim nID As Integer
        Dim I As Integer = 1

        cmdFinish.Enabled = False

        nID = clsMarsData.CreateDataID("smartfolders", "smartid")

        sCols = "SmartID,SmartName,SmartDesc,SmartType,Owner"

        sVals = nID & ",'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(txtDesc.Text) & "'," & _
            "'" & cmbAnyAll.Text & "'," & _
            "'" & gUser & "'"

        SQL = "INSERT INTO SmartFolders (" & sCols & ") VALUES (" & sVals & ")"


        Success = clsMarsData.WriteData(SQL)

        If Success = False Then Return

        sCols = "SmartDefID,SmartID,SmartColumn, SmartCriteria"

        For Each olsv As ListViewItem In lsvCriteria.Items
            sVals = clsMarsData.CreateDataID("smartfolderattr", "smartdefid") & "," & _
            nID & "," & _
            "'" & SQLPrepare(olsv.Text) & "'," & _
            "'" & SQLPrepare(olsv.SubItems(1).Text) & "'"

            SQL = "INSERT INTO SmartFolderAttr (" & sCols & ") VALUES (" & sVals & ")"

            Success = clsMarsData.WriteData(SQL)

            If Success = False Then GoTo Hell
            I = I + 1
        Next

        Dim oUser As New clsMarsUsers

        oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewSmartFolder)

        On Error Resume Next

        Dim oForm As Object
        Dim oUI As New clsMarsUI

        oForm = oWindow(nWindowCurrent)

        If Not oForm Is Nothing Then

            oUI.AddSmartFolders(oForm.tvSmartFolders, True)

            oForm.tbNav.SelectedTabIndex = 2

            oForm.lsvWindow.Items.Clear()

            oUI.FindNode("SmartFolder:" & nID, oForm.tvSmartFolders)

            'For Each oNode As TreeNode In oForm.tvSmartFolders.Nodes
            '    If oNode.Text = txtName.Text Then
            '        oForm.tvSmartFolders.SelectedNode = oNode
            '        Exit For
            '    End If
            'Next
        End If

        Close()

        Return
Hell:
        clsMarsData.WriteData("DELETE FROM SmartFolders WHERE SmartID = " & nID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        SetError(sender, String.Empty)

        If cmbOperator.Text.ToLower.IndexOf("empty") > -1 Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        SetError(sender, String.Empty)
    End Sub
    Dim closeFlag As Boolean = False
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                               Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(cmdCancel, spi)
            sp.ShowTooltip(cmdCancel)
            closeFlag = True
        Else
            Close()
        End If
    End Sub
End Class
