Imports DevComponents.DotNetBar
Public Class frmAddressBook
    Inherits Office2007Form
    Dim oData As New clsMarsData
    Friend WithEvents tbNav As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGroups As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbContacts As DevComponents.DotNetBar.TabItem
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents DockSite1 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite2 As DevComponents.DotNetBar.DockSite
    Friend WithEvents DockSite3 As DevComponents.DotNetBar.DockSite
    Friend WithEvents bar1 As DevComponents.DotNetBar.Bar
    Friend WithEvents item_369 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents item_383 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents item_384 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents item_385 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents item_386 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents item_387 As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents DockSite4 As DevComponents.DotNetBar.DockSite
    Friend WithEvents statusBar2 As DevComponents.DotNetBar.Bar
    Friend WithEvents LabelItem1 As DevComponents.DotNetBar.LabelItem
    Dim oList As ListView
    Dim imgList As ImageList
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents lsvContacts As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents lsvDetails As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents sfg As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ofg As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lsvGroups As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtMan As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents barLeftDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barRightDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barTopDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barBottomDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddressBook))
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.lsvContacts = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lsvGroups = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lsvDetails = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.sfg = New System.Windows.Forms.SaveFileDialog()
        Me.ofg = New System.Windows.Forms.OpenFileDialog()
        Me.dtMan = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.barBottomDockSite = New DevComponents.DotNetBar.DockSite()
        Me.barLeftDockSite = New DevComponents.DotNetBar.DockSite()
        Me.barRightDockSite = New DevComponents.DotNetBar.DockSite()
        Me.DockSite4 = New DevComponents.DotNetBar.DockSite()
        Me.statusBar2 = New DevComponents.DotNetBar.Bar()
        Me.LabelItem1 = New DevComponents.DotNetBar.LabelItem()
        Me.DockSite1 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite2 = New DevComponents.DotNetBar.DockSite()
        Me.DockSite3 = New DevComponents.DotNetBar.DockSite()
        Me.bar1 = New DevComponents.DotNetBar.Bar()
        Me.item_369 = New DevComponents.DotNetBar.ButtonItem()
        Me.item_383 = New DevComponents.DotNetBar.ButtonItem()
        Me.item_384 = New DevComponents.DotNetBar.ButtonItem()
        Me.item_385 = New DevComponents.DotNetBar.ButtonItem()
        Me.item_386 = New DevComponents.DotNetBar.ButtonItem()
        Me.item_387 = New DevComponents.DotNetBar.ButtonItem()
        Me.barTopDockSite = New DevComponents.DotNetBar.DockSite()
        Me.tbNav = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel()
        Me.tbContacts = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel()
        Me.tbGroups = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.DockSite4.SuspendLayout()
        CType(Me.statusBar2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.DockSite3.SuspendLayout()
        CType(Me.bar1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbNav.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'lsvContacts
        '
        Me.lsvContacts.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.lsvContacts.Border.Class = "ListViewBorder"
        Me.lsvContacts.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvContacts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvContacts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvContacts.FullRowSelect = True
        Me.lsvContacts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvContacts.Location = New System.Drawing.Point(1, 1)
        Me.lsvContacts.Name = "lsvContacts"
        Me.lsvContacts.Size = New System.Drawing.Size(189, 490)
        Me.lsvContacts.TabIndex = 1
        Me.lsvContacts.UseCompatibleStateImageBehavior = False
        Me.lsvContacts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Contact"
        Me.ColumnHeader1.Width = 154
        '
        'lsvGroups
        '
        Me.lsvGroups.BackColor = System.Drawing.Color.WhiteSmoke
        '
        '
        '
        Me.lsvGroups.Border.Class = "ListViewBorder"
        Me.lsvGroups.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvGroups.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5})
        Me.lsvGroups.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvGroups.FullRowSelect = True
        Me.lsvGroups.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvGroups.Location = New System.Drawing.Point(1, 1)
        Me.lsvGroups.Name = "lsvGroups"
        Me.lsvGroups.Size = New System.Drawing.Size(189, 490)
        Me.lsvGroups.TabIndex = 0
        Me.lsvGroups.UseCompatibleStateImageBehavior = False
        Me.lsvGroups.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ColumnHeader"
        Me.ColumnHeader5.Width = 153
        '
        'lsvDetails
        '
        '
        '
        '
        Me.lsvDetails.Border.Class = "ListViewBorder"
        Me.lsvDetails.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvDetails.Location = New System.Drawing.Point(221, 25)
        Me.lsvDetails.Name = "lsvDetails"
        Me.lsvDetails.Size = New System.Drawing.Size(653, 492)
        Me.lsvDetails.TabIndex = 6
        Me.lsvDetails.UseCompatibleStateImageBehavior = False
        Me.lsvDetails.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Contact Name"
        Me.ColumnHeader6.Width = 156
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Email Address"
        Me.ColumnHeader2.Width = 236
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Phone Number"
        Me.ColumnHeader3.Width = 114
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Fax Number"
        Me.ColumnHeader4.Width = 121
        '
        'sfg
        '
        Me.sfg.Filter = "Address Book Export|*.adbx|Text File|*.txt|XML File|*.xml"
        '
        'ofg
        '
        Me.ofg.Filter = "Address Book Export|*.adbx|Text File|*.txt|XML File|*.xml"
        '
        'dtMan
        '
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.dtMan.BottomDockSite = Me.barBottomDockSite
        Me.dtMan.LeftDockSite = Me.barLeftDockSite
        Me.dtMan.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.dtMan.ParentForm = Me
        Me.dtMan.RightDockSite = Me.barRightDockSite
        Me.dtMan.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.dtMan.ToolbarBottomDockSite = Me.DockSite4
        Me.dtMan.ToolbarLeftDockSite = Me.DockSite1
        Me.dtMan.ToolbarRightDockSite = Me.DockSite2
        Me.dtMan.ToolbarTopDockSite = Me.DockSite3
        Me.dtMan.TopDockSite = Me.barTopDockSite
        '
        'barBottomDockSite
        '
        Me.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barBottomDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barBottomDockSite.Location = New System.Drawing.Point(0, 517)
        Me.barBottomDockSite.Name = "barBottomDockSite"
        Me.barBottomDockSite.Size = New System.Drawing.Size(874, 0)
        Me.barBottomDockSite.TabIndex = 10
        Me.barBottomDockSite.TabStop = False
        '
        'barLeftDockSite
        '
        Me.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left
        Me.barLeftDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barLeftDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barLeftDockSite.Name = "barLeftDockSite"
        Me.barLeftDockSite.Size = New System.Drawing.Size(0, 492)
        Me.barLeftDockSite.TabIndex = 7
        Me.barLeftDockSite.TabStop = False
        '
        'barRightDockSite
        '
        Me.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right
        Me.barRightDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barRightDockSite.Location = New System.Drawing.Point(874, 25)
        Me.barRightDockSite.Name = "barRightDockSite"
        Me.barRightDockSite.Size = New System.Drawing.Size(0, 492)
        Me.barRightDockSite.TabIndex = 8
        Me.barRightDockSite.TabStop = False
        '
        'DockSite4
        '
        Me.DockSite4.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite4.Controls.Add(Me.statusBar2)
        Me.DockSite4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.DockSite4.Location = New System.Drawing.Point(0, 517)
        Me.DockSite4.Name = "DockSite4"
        Me.DockSite4.Size = New System.Drawing.Size(874, 20)
        Me.DockSite4.TabIndex = 17
        Me.DockSite4.TabStop = False
        '
        'statusBar2
        '
        Me.statusBar2.AccessibleDescription = "DotNetBar Bar (statusBar2)"
        Me.statusBar2.AccessibleName = "DotNetBar Bar"
        Me.statusBar2.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar
        Me.statusBar2.DockSide = DevComponents.DotNetBar.eDockSide.Bottom
        Me.statusBar2.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.ResizeHandle
        Me.statusBar2.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.LabelItem1})
        Me.statusBar2.ItemSpacing = 2
        Me.statusBar2.Location = New System.Drawing.Point(0, 0)
        Me.statusBar2.Name = "statusBar2"
        Me.statusBar2.Size = New System.Drawing.Size(874, 19)
        Me.statusBar2.Stretch = True
        Me.statusBar2.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.statusBar2.TabIndex = 0
        Me.statusBar2.TabStop = False
        Me.statusBar2.Text = "Status"
        '
        'LabelItem1
        '
        Me.LabelItem1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelItem1.GlobalName = "LabelItem1"
        Me.LabelItem1.Name = "LabelItem1"
        Me.LabelItem1.Width = 136
        '
        'DockSite1
        '
        Me.DockSite1.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite1.Dock = System.Windows.Forms.DockStyle.Left
        Me.DockSite1.Location = New System.Drawing.Point(0, 25)
        Me.DockSite1.Name = "DockSite1"
        Me.DockSite1.Size = New System.Drawing.Size(0, 492)
        Me.DockSite1.TabIndex = 14
        Me.DockSite1.TabStop = False
        '
        'DockSite2
        '
        Me.DockSite2.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite2.Dock = System.Windows.Forms.DockStyle.Right
        Me.DockSite2.Location = New System.Drawing.Point(874, 25)
        Me.DockSite2.Name = "DockSite2"
        Me.DockSite2.Size = New System.Drawing.Size(0, 492)
        Me.DockSite2.TabIndex = 15
        Me.DockSite2.TabStop = False
        '
        'DockSite3
        '
        Me.DockSite3.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.DockSite3.Controls.Add(Me.bar1)
        Me.DockSite3.Dock = System.Windows.Forms.DockStyle.Top
        Me.DockSite3.Location = New System.Drawing.Point(0, 0)
        Me.DockSite3.Name = "DockSite3"
        Me.DockSite3.Size = New System.Drawing.Size(874, 25)
        Me.DockSite3.TabIndex = 16
        Me.DockSite3.TabStop = False
        '
        'bar1
        '
        Me.bar1.AccessibleDescription = "DotNetBar Bar (bar1)"
        Me.bar1.AccessibleName = "DotNetBar Bar"
        Me.bar1.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar
        Me.bar1.DockSide = DevComponents.DotNetBar.eDockSide.Top
        Me.bar1.GrabHandleStyle = DevComponents.DotNetBar.eGrabHandleStyle.Office2003
        Me.bar1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.item_369, Me.item_383, Me.item_384, Me.item_385, Me.item_386, Me.item_387})
        Me.bar1.Location = New System.Drawing.Point(0, 0)
        Me.bar1.Name = "bar1"
        Me.bar1.Size = New System.Drawing.Size(421, 25)
        Me.bar1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.bar1.TabIndex = 0
        Me.bar1.TabStop = False
        Me.bar1.Text = "My Bar"
        '
        'item_369
        '
        Me.item_369.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_369.GlobalName = "item_369"
        Me.item_369.Image = CType(resources.GetObject("item_369.Image"), System.Drawing.Image)
        Me.item_369.Name = "item_369"
        Me.item_369.Text = "New Contact"
        Me.item_369.Tooltip = "Create a new contact"
        '
        'item_383
        '
        Me.item_383.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_383.GlobalName = "item_383"
        Me.item_383.Image = CType(resources.GetObject("item_383.Image"), System.Drawing.Image)
        Me.item_383.Name = "item_383"
        Me.item_383.Text = "New Group"
        Me.item_383.Tooltip = "Create a group of contacts"
        '
        'item_384
        '
        Me.item_384.BeginGroup = True
        Me.item_384.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_384.GlobalName = "item_384"
        Me.item_384.Image = CType(resources.GetObject("item_384.Image"), System.Drawing.Image)
        Me.item_384.Name = "item_384"
        Me.item_384.Text = "Delete"
        Me.item_384.Tooltip = "Delete the selected contact"
        '
        'item_385
        '
        Me.item_385.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_385.GlobalName = "item_385"
        Me.item_385.Image = CType(resources.GetObject("item_385.Image"), System.Drawing.Image)
        Me.item_385.Name = "item_385"
        Me.item_385.Text = "Edit"
        Me.item_385.Tooltip = "Edit the selected contact"
        '
        'item_386
        '
        Me.item_386.BeginGroup = True
        Me.item_386.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_386.GlobalName = "item_386"
        Me.item_386.Image = CType(resources.GetObject("item_386.Image"), System.Drawing.Image)
        Me.item_386.Name = "item_386"
        Me.item_386.Text = "Import"
        Me.item_386.Tooltip = "Import addresses into the address book"
        '
        'item_387
        '
        Me.item_387.ButtonStyle = DevComponents.DotNetBar.eButtonStyle.ImageAndText
        Me.item_387.GlobalName = "item_387"
        Me.item_387.Image = CType(resources.GetObject("item_387.Image"), System.Drawing.Image)
        Me.item_387.Name = "item_387"
        Me.item_387.Text = "Export"
        Me.item_387.Tooltip = "Export contacts to external file"
        '
        'barTopDockSite
        '
        Me.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top
        Me.barTopDockSite.DocumentDockContainer = New DevComponents.DotNetBar.DocumentDockContainer()
        Me.barTopDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barTopDockSite.Name = "barTopDockSite"
        Me.barTopDockSite.Size = New System.Drawing.Size(874, 0)
        Me.barTopDockSite.TabIndex = 9
        Me.barTopDockSite.TabStop = False
        '
        'tbNav
        '
        Me.tbNav.BackColor = System.Drawing.Color.FromArgb(CType(CType(207, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.tbNav.CanReorderTabs = True
        Me.tbNav.Controls.Add(Me.TabControlPanel1)
        Me.tbNav.Controls.Add(Me.TabControlPanel2)
        Me.tbNav.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbNav.Location = New System.Drawing.Point(0, 25)
        Me.tbNav.Name = "tbNav"
        Me.tbNav.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbNav.SelectedTabIndex = 0
        Me.tbNav.Size = New System.Drawing.Size(216, 492)
        Me.tbNav.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock
        Me.tbNav.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbNav.TabIndex = 12
        Me.tbNav.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbNav.Tabs.Add(Me.tbContacts)
        Me.tbNav.Tabs.Add(Me.tbGroups)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.lsvContacts)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(191, 492)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbContacts
        '
        'tbContacts
        '
        Me.tbContacts.AttachedControl = Me.TabControlPanel1
        Me.tbContacts.Image = CType(resources.GetObject("tbContacts.Image"), System.Drawing.Image)
        Me.tbContacts.Name = "tbContacts"
        Me.tbContacts.Text = "Contacts"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.lsvGroups)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(191, 492)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(132, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(189, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbGroups
        '
        'tbGroups
        '
        Me.tbGroups.AttachedControl = Me.TabControlPanel2
        Me.tbGroups.Image = CType(resources.GetObject("tbGroups.Image"), System.Drawing.Image)
        Me.tbGroups.Name = "tbGroups"
        Me.tbGroups.Text = "Groups"
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tbNav
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(207, Byte), Integer), CType(CType(139, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(216, 25)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(5, 492)
        Me.ExpandableSplitter1.TabIndex = 13
        Me.ExpandableSplitter1.TabStop = False
        '
        'frmAddressBook
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(874, 537)
        Me.Controls.Add(Me.lsvDetails)
        Me.Controls.Add(Me.ExpandableSplitter1)
        Me.Controls.Add(Me.tbNav)
        Me.Controls.Add(Me.barLeftDockSite)
        Me.Controls.Add(Me.barRightDockSite)
        Me.Controls.Add(Me.barTopDockSite)
        Me.Controls.Add(Me.barBottomDockSite)
        Me.Controls.Add(Me.DockSite1)
        Me.Controls.Add(Me.DockSite2)
        Me.Controls.Add(Me.DockSite3)
        Me.Controls.Add(Me.DockSite4)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAddressBook"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Address Book"
        Me.DockSite4.ResumeLayout(False)
        CType(Me.statusBar2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.DockSite3.ResumeLayout(False)
        CType(Me.bar1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbNav.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub OpenContact(ByVal nID As Integer, ByVal sType As String)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sEmail As String = ""
        Dim sFax As String
        Dim sPhone As String
        Dim sName As String
        Dim oRs1 As ADODB.Recordset

        '_Delay(1)

        SQL = "SELECT * FROM ContactDetail c INNER JOIN ContactAttr x ON " & _
                "c.ContactID = x.ContactID WHERE c.ContactID =" & nID

        oRs = clsMarsData.GetData(SQL)

        lsvDetails.Items.Clear()
        lsvDetails.SmallImageList = imgList
        lsvDetails.LargeImageList = imgList

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            sName = oRs("contactname").Value
            sEmail = oRs("emailaddress").Value
            sPhone = IsNull(oRs("phonenumber").Value)
            sFax = IsNull(oRs("faxnumber").Value)

            If sEmail.IndexOf("@") < 0 Then
                sName = sEmail
                oRs1 = clsMarsData.GetData("SELECT * FROM " & _
                "ContactDetail c INNER JOIN ContactAttr x ON " & _
                "c.ContactID = x.ContactID WHERE " & _
                "x.ContactName ='" & SQLPrepare(sEmail) & "'")

                Try

                    If oRs1.EOF = False Then
                        sEmail = oRs1("emailaddress").Value
                        sPhone = oRs1("phonenumber").Value
                        sFax = oRs1("faxnumber").Value
                    End If

                    oRs1.Close()
                Catch ex As Exception
                End Try
            End If

            With oItem
                .ImageIndex = 0
                .Text = sName
                .Tag = oRs("detailid").Value

                With .SubItems
                    .Add(sEmail)
                    .Add(sPhone)
                    .Add(sFax)
                End With
            End With

            lsvDetails.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub
    Private Sub DrawContacts()
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        imglist = New ImageList

        imgList.Images.Add(Image.FromFile(IO.Path.Combine(sAppPath, "assets\user.png")))
        imgList.Images.Add(Image.FromFile(IO.Path.Combine(sAppPath, "assets\group.png")))


        SQL = "SELECT * FROM ContactAttr WHERE ContactType ='contact' " & _
        "ORDER BY ContactName ASC"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        lsvContacts.SmallImageList = imgList
        lsvContacts.LargeImageList = imgList

        lsvContacts.Items.Clear()

        Application.DoEvents()

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            With oItem
                .Text = oRs("contactname").Value
                .Tag = oRs("contacttype").Value & ":" & oRs("contactid").Value

                If oRs("contacttype").Value = "contact" Then
                    .ImageIndex = 0
                Else
                    .ImageIndex = 1
                End If
            End With

            lsvContacts.Items.Add(oItem)
            oRs.MoveNext()
        Loop

        oRs.Close()

        SQL = "SELECT * FROM ContactAttr WHERE ContactType ='group' " & _
        "ORDER BY ContactName ASC"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        lsvGroups.Items.Clear()
        lsvGroups.SmallImageList = imgList
        lsvGroups.LargeImageList = imgList

        Application.DoEvents()

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            With oItem
                .Text = oRs("contactname").Value
                .Tag = oRs("contacttype").Value & ":" & oRs("contactid").Value
                If oRs("contacttype").Value = "contact" Then
                    .ImageIndex = 0
                Else
                    .ImageIndex = 1
                End If
            End With

            lsvGroups.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Public Sub DeleteContact(ByVal nID As Integer, ByVal sType As String)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sMsg As String
        Dim Exist As Boolean = False
        Dim sName As String

        If sType.ToLower = "contact" Then
            sName = lsvContacts.SelectedItems(0).Text
        Else
            sName = lsvGroups.SelectedItems(0).Text
        End If

        SQL = "SELECT r.ReportTitle, d.ReportID FROM DestinationAttr d " & _
        "INNER JOIN ReportAttr r ON d.ReportID = r.ReportID WHERE d.SendTo LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
        "d.Cc LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
        "d.Bcc LIKE '%" & SQLPrepare(sName) & ">%'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Exist = True
                sMsg = "The following schedules currently reference this contact/group:" & vbCrLf & _
                "----------------------------------------------------------------------" & vbCrLf
            End If

            Do While oRs.EOF = False
                sMsg &= oRs(0).Value & vbCrLf
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        SQL = "SELECT p.PackageName, d.PackID FROM DestinationAttr d " & _
                "INNER JOIN PackageAttr p ON d.PackID = p.PackID WHERE d.SendTo LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
                "d.Cc LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
                "d.Bcc LIKE '%" & SQLPrepare(sName) & ">%'"


        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Exist = True
                sMsg &= vbCrLf & "The following packages currently reference this contact/group:" & vbCrLf & _
                "----------------------------------------------------------------------" & vbCrLf
            End If

            Do While oRs.EOF = False
                sMsg &= oRs(0).Value & vbCrLf
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        If Exist = True Then
            MessageBox.Show(sMsg & vbCrLf & vbCrLf & "Please remove the references from the above schedules/packages and then try again.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        clsMarsData.WriteData("DELETE FROM ContactDetail WHERE ContactID = " & nID)
        clsMarsData.WriteData("DELETE FROM ContactAttr WHERE ContactID = " & nID)
        clsMarsData.WriteData("DELETE FROM ContactDetail WHERE EmailAddress  LIKE '" & SQLPrepare(sName) & "'")

        oList.SelectedItems(0).Remove()
        lsvDetails.Items.Clear()
    End Sub


    Private Sub frmAddressBook_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)
        DrawContacts()

        oList = lsvContacts

    End Sub

    Private Sub dtMan_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtMan.ItemClick
        Try
            Dim oItem As BaseItem
            Dim nID As Integer
            Dim sType As String
            Dim oRes As DialogResult

            oItem = CType(sender, BaseItem)

            Select Case oItem.Text.ToLower
                Case "new contact"
                    Dim oContact As New frmAddContact
                    oContact.AddContact()

                Case "new group"
                    Dim oGroup As New frmAddContact
                    oGroup.AddGroup()
                Case "delete"
                    If oList.SelectedItems.Count = 0 Then Return

                    sType = oList.SelectedItems(0).Tag.split(":")(0)

                    If oList.SelectedItems.Count = 1 Then

                        nID = oList.SelectedItems(0).Tag.split(":")(1)

                        oRes = MessageBox.Show("Delete the " & sType & " '" & _
                        oList.SelectedItems(0).Text & "'?", _
                        Application.ProductName, MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then
                            DeleteContact(nID, sType)
                        End If
                    Else
                        oRes = MessageBox.Show("Delete the selected" & sType & "s " & _
                        "from the address book?", Application.ProductName, _
                        MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then

                            Dim oUI As New clsMarsUI

                            oUI.BusyProgress(50, "Deleting...")
                            For Each lsv As ListViewItem In oList.SelectedItems
                                nID = lsv.Tag.split(":")(1)

                                DeleteContact(nID, sType)
                            Next

                            oUI.BusyProgress(80, "Ceaning up...")

                            oUI.BusyProgress(, , True)
                        End If
                    End If
                Case "edit"

                    If oList.SelectedItems.Count = 0 Then Return

                    nID = oList.SelectedItems(0).Tag.split(":")(1)
                    sType = oList.SelectedItems(0).Tag.split(":")(0)

                    Dim nSel As Integer = oList.SelectedItems(0).Index

                    Dim oForm As New frmAddContact

                    oForm.isEdit = True

                    Select Case sType
                        Case "contact"
                            oForm.EditContact(nID)
                        Case "group"
                            oForm.EditGroup(nID)
                    End Select

                    OpenContact(nID, sType)
                Case "export"
                    ExportAddressBook()
                Case "import"
                    ImportContacts()
            End Select

            If oList.Name.ToLower = "lsvcontacts" Then
                tbNav.SelectedTabIndex = 0
            Else
                tbNav.SelectedTabIndex = 1
            End If

            DrawContacts()
        Catch : End Try
    End Sub
    Private Sub ImportContacts()
        Dim oUI As New clsMarsUI
        Dim oData As New clsMarsData
        Dim sName As String
        Dim sAddress As String
        Dim sAddressEntry As String
        Dim sPhone As String
        Dim sFax As String
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer
        Dim oRes As DialogResult



        ofg.Title = "Please select the file to import"

        If ofg.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return

        If ofg.FileName.Length = 0 Then Return

        If IO.Path.GetExtension(ofg.FileName).ToLower = ".adbx" Then
            importAddressBookFromADBX(ofg.FileName)
        Else
            Dim sChar As String = InputBox("Please enter the character used as the field delimiter", Application.ProductName)
            If sChar.Length = 0 Then Exit Sub

            Dim sImport As String = ReadTextFromFile(ofg.FileName)

            If sImport.EndsWith(sChar) = False Then
                sImport &= sChar
            End If

            Dim nCount As Integer = FindOccurence(sImport, Environment.NewLine) + 1

            For I As Integer = 1 To nCount

                oUI.BusyProgress((I / nCount) * 100, "Importing...")

                sAddressEntry = GetDelimitedWord(sImport, I, Environment.NewLine)

                sName = GetDelimitedWord(sAddressEntry, 1, sChar).Trim
                sAddress = GetDelimitedWord(sAddressEntry, 2, sChar).Trim
                sPhone = GetDelimitedWord(sAddressEntry, 3, sChar).Trim
                sFax = GetDelimitedWord(sAddressEntry, 4, sChar).Trim

                If sAddress.Length > 0 And sName.Length > 1 Then

                    If clsMarsData.IsDuplicate("ContactAttr", "ContactName", _
                    sName, False) = True Then
Hell:
                        oRes = MessageBox.Show("The name '" & sName & "' already " & _
                        "exists in the address book." & Environment.NewLine & _
                        "Press 'Yes' to overwrite it, 'No' to rename it or " & _
                        "'Cancel' to skip this entry'", Application.ProductName, _
                         MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

                        Select Case oRes

                            Case DialogResult.Yes
                                clsMarsData.WriteData("DELETE FROM ContactAttr " & _
                                "WHERE ContactName ='" & SQLPrepare(sName) & "'")
                            Case DialogResult.No
                                sName = _
                                InputBox("Please enter the new name", _
                                Application.ProductName)

                                If sName.Length = 0 Then
                                    GoTo Here
                                ElseIf clsMarsData.IsDuplicate("ContactAttr", _
                                "ContactName", sName, False) = True Then
                                    GoTo Hell
                                End If
                            Case DialogResult.Cancel
                                GoTo Here
                        End Select

                    End If

                    nID = clsMarsData.CreateDataID("contactattr", "contactid")

                    sCols = "ContactID, ContactName,ContactType"

                    sVals = nID & "," & _
                    "'" & SQLPrepare(sName) & "'," & _
                    "'contact'"

                    SQL = "INSERT INTO ContactAttr (" & sCols & ") VALUES(" & sVals & ")"

                    If clsMarsData.WriteData(SQL) = True Then
                        sCols = "DetailID,ContactID,EmailAddress,PhoneNumber,FaxNumber"

                        sVals = clsMarsData.CreateDataID("contactdetail", "detailid") & "," & _
                        nID & "," & _
                        "'" & SQLPrepare(sAddress) & "'," & _
                        "'" & SQLPrepare(sPhone) & "'," & _
                        "'" & SQLPrepare(sFax) & "'"

                        SQL = "INSERT INTO ContactDetail(" & sCols & ") " & _
                        "VALUES (" & sVals & ")"

                        clsMarsData.WriteData(SQL)

                    End If


                End If
Here:
            Next
        End If
        oUI.BusyProgress(, , True)
        DrawContacts()
    End Sub

    Private Sub importAddressBookFromADBX(fileName As String)
        Try
            Dim ds As DataSet = New DataSet("Contacts")
            ds.ReadXml(fileName)

            Dim dtContacts As DataTable = ds.Tables("ContactAttr")
            Dim dtContactDetails As DataTable = ds.Tables("ContactDetail")

            Dim cols, vals, SQL As String
            Dim i As Integer = 1

            For Each contactrow As DataRow In dtContacts.Rows

                clsMarsUI.BusyProgress((i / dtContacts.Rows.Count) * 100, "Importing " & contactrow("contactname"))

                cols = "contactid, contactname, contacttype"

                Dim newID As Integer = clsMarsData.CreateDataID("contactattr", "contactid")
                Dim oldID As Integer = contactrow("contactid")

                vals = newID & "," & _
                    "'" & SQLPrepare(contactrow("contactname")) & "'," & _
                    "'" & SQLPrepare(contactrow("contacttype")) & "'"

                If clsMarsData.DataItem.InsertData("contactattr", cols, vals) Then
                    cols = "detailid, contactid, emailaddress, phonenumber, faxnumber"

                    Dim detailrows() As DataRow = dtContactDetails.Select("contactid =" & oldID)

                    If detailrows IsNot Nothing AndAlso detailrows.Length > 0 Then
                        For Each detailrow As DataRow In detailrows
                            Dim detailID As Integer = clsMarsData.CreateDataID("contactdetail", "detailid")

                            vals = detailID & "," & _
                                newID & "," & _
                                "'" & SQLPrepare(IsNull(detailrow("emailaddress"))) & "'," & _
                                "'" & SQLPrepare(IsNull(detailrow("phonenumber"))) & "'," & _
                                "'" & SQLPrepare(IsNull(detailrow("faxnumber"))) & "'"

                            clsMarsData.DataItem.InsertData("contactdetail", cols, vals)
                        Next
                    End If
                End If

                i += 1
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        Finally
            clsMarsUI.MainUI.BusyProgress(, , True)
        End Try

    End Sub
    Private Sub exportAddressBookToADBX(fileName As String)
        Try
            Cursor.Current = Cursors.WaitCursor

            Dim ds As DataSet = New DataSet("Contacts")

            Dim conString As String

            If gConType = "DAT" Then
                Dim con As OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection
                Dim conB As Data.OleDb.OleDbConnectionStringBuilder = New System.Data.OleDb.OleDbConnectionStringBuilder

                With CType(conB, System.Data.OleDb.OleDbConnectionStringBuilder)
                    .DataSource = IO.Path.Combine(sAppPath, "sqlrdlive.dat")
                    .Provider = "Microsoft.Jet.OLEDB.4.0"
                    .FileName = IO.Path.Combine(sAppPath, "sqlrdlive.dat")
                    con.ConnectionString = .ConnectionString
                End With

                con.Open()

                Dim da As OleDb.OleDbDataAdapter = New OleDb.OleDbDataAdapter("SELECT * from contactattr", con)

                da.Fill(ds, "ContactAttr")

                da = New OleDb.OleDbDataAdapter("select * from contactdetail", CType(con, OleDb.OleDbConnection))

                da.Fill(ds, "ContactDetail")

                con.Close()
                con.Dispose()
                da.Dispose()
            ElseIf gConType = "LOCAL" Then
                Dim con As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(clsMigration.m_localConStringdotNET)
                con.ConnectionString = clsMigration.m_localConStringdotNET

                con.Open()

                Dim da As SqlClient.SqlDataAdapter = New SqlClient.SqlDataAdapter("SELECT * from contactattr", con)

                da.Fill(ds, "ContactAttr")

                da = New SqlClient.SqlDataAdapter("select * from contactdetail", con)

                da.Fill(ds, "ContactDetail")

                con.Close()
                con.Dispose()
                da.Dispose()
            ElseIf gConType = "ODBC" Then
                Dim dsn, uid, password As String
                conString = clsMarsUI.MainUI.ReadRegistry("ConString2", "")

                If conString = "" Then
                    conString = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)
                    dsn = conString.Split(";")(4)
                    dsn = dsn.Split("=")(1)
                    uid = conString.Split(";")(3)
                    uid = uid.Split("=")(1)
                    password = conString.Split(";")(1)
                    password = password.Split("=")(1)
                Else
                    dsn = conString.Split("|")(0)
                    uid = conString.Split("|")(1)
                    password = _DecryptDBValue(conString.Split("|")(2))
                End If

                Dim con As Odbc.OdbcConnection = New System.Data.Odbc.OdbcConnection("Dsn=" & dsn & ";Uid=" & uid & ";Pwd=" & password)

                con.Open()

                Dim da As Odbc.OdbcDataAdapter = New Odbc.OdbcDataAdapter("SELECT * from contactattr", con)

                da.Fill(ds, "ContactAttr")

                da = New Odbc.OdbcDataAdapter("select * from contactdetail", con)

                da.Fill(ds, "ContactDetail")

                con.Close()
                con.Dispose()
                da.Dispose()
            End If

            If ds IsNot Nothing Then
                ds.WriteXml(fileName, XmlWriteMode.WriteSchema)
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    Private Sub ExportAddressBook()
        Dim sAddress As String
        Dim oRs As ADODB.Recordset
        Dim I As Integer
        Dim oUi As New clsMarsUI
        Dim sName As String
        Dim xmlBody As String = "<?xml version='1.0' encoding='utf-8'?>" & vbCrLf & "<AddressBook>" & vbCrLf

        sfg.OverwritePrompt = True
        sfg.Title = "Save export file as..."

        If sfg.ShowDialog() = Windows.Forms.DialogResult.Cancel Then Return

        sAddress = sfg.FileName

        If sAddress.Length = 0 Then Exit Sub

        If IO.Path.GetExtension(sfg.FileName).ToLower = ".adbx" Then
            exportAddressBookToADBX(sfg.FileName)
            MessageBox.Show("Contacts exported successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Else

            Dim SQL As String = "SELECT * FROM  ContactAttr c INNER JOIN " & _
            "ContactDetail x ON c.ContactID = x.ContactID"

            oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

            If oRs Is Nothing Then Return

            With oRs
                'sName = "ContactName,Email,Phone Number,Fax Number" & Environment.NewLine

                Do While .EOF = False
                    I += I
                    oUi.BusyProgress((I / .RecordCount) * 100, "Exporting...", False)

                    Dim contactid As Integer = oRs(0).Value
                    Dim contactName As String = oRs("contactname").Value
                    Dim emailAddress As String = IsNull(oRs("emailAddress").Value)
                    Dim phoneNumber As String = IsNull(oRs("phonenumber").Value)
                    Dim faxNumber As String = IsNull(oRs("faxNumber").Value)

                    xmlBody &= "<Address ID='" & contactid & "'>" & vbCrLf & _
                    "<ContactName>" & contactName & "</ContactName>" & vbCrLf & _
                    "<EmailAddress>" & emailAddress & "</EmailAddress>" & vbCrLf & _
                    "<PhoneNumber>" & phoneNumber & "</PhoneNumber>" & vbCrLf & _
                    "<FaxNumber>" & faxNumber & "</FaxNumber>" & vbCrLf & _
                    "</Address>" & vbCrLf

                    sName &= oRs("contactname").Value & "," & _
                        oRs("emailaddress").Value & "," & _
                        IsNull(oRs("phonenumber").Value) & "," & _
                        IsNull(oRs("faxnumber").Value) & Environment.NewLine

                    .MoveNext()
                Loop

                xmlBody &= "</AddressBook>"

                .Close()

                oUi.BusyProgress(, , True)

            End With

            If sAddress.EndsWith(".txt") Then
                SaveTextToFile(sName, sAddress)
            Else
                SaveTextToFile(xmlBody, sAddress, , , False)
            End If

            MessageBox.Show("Contacts exported successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
    Private Sub lsvContacts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvContacts.SelectedIndexChanged
        Dim nID As Integer
        Dim sType As String

        If lsvContacts.SelectedItems.Count = 0 Then Return

        nID = lsvContacts.SelectedItems(0).Tag.Split(":")(1)
        sType = lsvContacts.SelectedItems(0).Tag.Split(":")(0)

        OpenContact(nID, sType)

    End Sub

    Private Sub lsvGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvGroups.SelectedIndexChanged
        Dim nID As Integer
        Dim sType As String

        If lsvGroups.SelectedItems.Count = 0 Then Return

        nID = lsvGroups.SelectedItems(0).Tag.Split(":")(1)
        sType = lsvGroups.SelectedItems(0).Tag.Split(":")(0)

        OpenContact(nID, sType)

    End Sub

    Private Sub pnContacts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oList = lsvContacts

        lsvDetails.Items.Clear()

        lsvContacts_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub pnGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oList = lsvGroups
        lsvDetails.Items.Clear()
        lsvGroups_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub lsvContacts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvContacts.DoubleClick
        Dim nID As Integer
        Dim sType As String


        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
        DrawContacts()
    End Sub

    Private Sub lsvGroups_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvGroups.DoubleClick
        Dim nID As Integer
        Dim sType As String


        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
        DrawContacts()
    End Sub

    Private Sub lsvDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDetails.DoubleClick
        Dim nID As Integer
        Dim sType As String

        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
    End Sub


    Private Sub tbNav_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tbNav.SelectedTabChanged
        On Error Resume Next
        Select Case e.NewTab.Name.ToLower
            Case "tbcontacts"
                If lsvGroups.SelectedItems.Count > 0 Then lsvGroups.SelectedItems(0).Selected = False
                If lsvContacts.Items.Count > 0 Then lsvContacts.Items(0).Selected = True

                oList = lsvContacts

                If lsvContacts.Items.Count = 0 Then Me.lsvDetails.Items.Clear()
            Case "tbgroups"
                If lsvContacts.SelectedItems.Count > 0 Then lsvContacts.SelectedItems(0).Selected = False

                If lsvGroups.Items.Count > 0 Then lsvGroups.Items(0).Selected = True

                oList = lsvGroups

                If lsvGroups.Items.Count = 0 Then Me.lsvDetails.Items.Clear()
        End Select
    End Sub

    Private Sub item_387_Click(sender As System.Object, e As System.EventArgs) Handles item_387.Click

    End Sub
End Class
