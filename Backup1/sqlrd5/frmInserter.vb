#If CRYSTAL_VER = 8 Then
    imports My.Crystal85

#ElseIf CRYSTAL_VER = 9 Then
    imports My.Crystal9 

#ElseIf CRYSTAL_VER = 10 Then
    imports My.Crystal10

#ElseIf CRYSTAL_VER = 11 Or CRYSTAL_VER = 11.5 Or CRYSTAL_VER >= 11.6 Then
Imports My.Crystal11
#End If

Imports DevComponents.DotNetBar
Public Class frmInserter
    Inherits DevComponents.DotNetBar.Office2007Form
    Public sCon As String
    Public sTables() As String
    Dim oData As New clsMarsData
    Dim ep As New ErrorProvider
    Public UserCancel As Boolean
    Private HidePars As Boolean = False
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.TableLayoutPanel
    Dim HideUserConstants As Boolean = False
    Dim eventBased As Boolean = False
    Friend WithEvents Panel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label3 As LabelX
    Friend WithEvents txtCustom As System.Windows.Forms.TextBox
    Dim eventID As Integer = 99999
    Friend WithEvents Panel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As LabelX
    Friend WithEvents cmbDateFormat As System.Windows.Forms.ComboBox
    Dim mode As e_Mode
    Dim m_Container As ContainerControl
    Friend WithEvents pnMsgFormat As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents optText As System.Windows.Forms.RadioButton
    Friend WithEvents optHTML As System.Windows.Forms.RadioButton
    Friend WithEvents txtIndex As System.Windows.Forms.NumericUpDown
    Friend WithEvents pnAttachIndex As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label5 As LabelX
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents tvInserter As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Dim Dialog As Boolean = False
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Dim defaultnodeImage As Image = resizeImage(My.Resources.components, New Size(16, 16))
    Friend WithEvents pnlExtract As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtLineNumber As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCharsToRead As DevComponents.Editors.IntegerInput
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtStartIndex As DevComponents.Editors.IntegerInput
    Public m_parameterList As ArrayList
    Friend WithEvents pnlFiscalWeek As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtFiscalWeek As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX

    Dim loaded As Boolean


#Region " Windows Form Designer generated code "

    Public Sub New(ByVal m_EventID As Integer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        eventID = m_EventID
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdSelect As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInserter))
        Me.Panel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New DevComponents.DotNetBar.LabelX()
        Me.pnAttachIndex = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtIndex = New System.Windows.Forms.NumericUpDown()
        Me.pnMsgFormat = New System.Windows.Forms.FlowLayoutPanel()
        Me.optText = New System.Windows.Forms.RadioButton()
        Me.optHTML = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDateFormat = New System.Windows.Forms.ComboBox()
        Me.Panel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtCustom = New System.Windows.Forms.TextBox()
        Me.cmdSelect = New System.Windows.Forms.Button()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.tvInserter = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.pnlFiscalWeek = New System.Windows.Forms.TableLayoutPanel()
        Me.txtFiscalWeek = New System.Windows.Forms.NumericUpDown()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.pnlExtract = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.txtLineNumber = New DevComponents.Editors.IntegerInput()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtCharsToRead = New DevComponents.Editors.IntegerInput()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.txtStartIndex = New DevComponents.Editors.IntegerInput()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnAttachIndex.SuspendLayout()
        CType(Me.txtIndex, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnMsgFormat.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.tvInserter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.pnlFiscalWeek.SuspendLayout()
        CType(Me.txtFiscalWeek, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlExtract.SuspendLayout()
        CType(Me.txtLineNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCharsToRead, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtStartIndex, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.ColumnCount = 2
        Me.Panel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.32258!))
        Me.Panel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.67742!))
        Me.Panel1.Controls.Add(Me.txtAdjustStamp, 1, 0)
        Me.Panel1.Controls.Add(Me.Label26, 0, 0)
        Me.Panel1.Location = New System.Drawing.Point(6, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RowCount = 1
        Me.Panel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Panel1.Size = New System.Drawing.Size(248, 27)
        Me.Panel1.TabIndex = 8
        Me.Panel1.Visible = False
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAdjustStamp.Location = New System.Drawing.Point(196, 3)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 20
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label26.Location = New System.Drawing.Point(3, 3)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(136, 21)
        Me.Label26.TabIndex = 19
        Me.Label26.Text = "Adjust constant by (days)"
        '
        'pnAttachIndex
        '
        Me.pnAttachIndex.Controls.Add(Me.Label5)
        Me.pnAttachIndex.Controls.Add(Me.txtIndex)
        Me.pnAttachIndex.Location = New System.Drawing.Point(6, 20)
        Me.pnAttachIndex.Name = "pnAttachIndex"
        Me.pnAttachIndex.Size = New System.Drawing.Size(156, 30)
        Me.pnAttachIndex.TabIndex = 5
        Me.pnAttachIndex.Visible = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(3, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(89, 16)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Attachment Index"
        '
        'txtIndex
        '
        Me.txtIndex.Location = New System.Drawing.Point(98, 3)
        Me.txtIndex.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.txtIndex.Name = "txtIndex"
        Me.txtIndex.Size = New System.Drawing.Size(44, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtIndex, New DevComponents.DotNetBar.SuperTooltipInfo("The position of the attachment in the email", "", resources.GetString("txtIndex.SuperTooltip"), Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.txtIndex.TabIndex = 4
        Me.txtIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIndex.Value = New Decimal(New Integer() {1, 0, 0, -2147483648})
        '
        'pnMsgFormat
        '
        Me.pnMsgFormat.Controls.Add(Me.optText)
        Me.pnMsgFormat.Controls.Add(Me.optHTML)
        Me.pnMsgFormat.Location = New System.Drawing.Point(6, 20)
        Me.pnMsgFormat.Name = "pnMsgFormat"
        Me.pnMsgFormat.Size = New System.Drawing.Size(200, 24)
        Me.pnMsgFormat.TabIndex = 7
        Me.pnMsgFormat.Visible = False
        '
        'optText
        '
        Me.optText.AutoSize = True
        Me.optText.Location = New System.Drawing.Point(3, 3)
        Me.optText.Name = "optText"
        Me.optText.Size = New System.Drawing.Size(49, 17)
        Me.optText.TabIndex = 1
        Me.optText.Text = "TEXT"
        Me.optText.UseVisualStyleBackColor = True
        '
        'optHTML
        '
        Me.optHTML.AutoSize = True
        Me.optHTML.Location = New System.Drawing.Point(58, 3)
        Me.optHTML.Name = "optHTML"
        Me.optHTML.Size = New System.Drawing.Size(51, 17)
        Me.optHTML.TabIndex = 0
        Me.optHTML.TabStop = True
        Me.optHTML.Text = "HTML"
        Me.optHTML.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.ColumnCount = 2
        Me.Panel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.Panel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.Panel3.Controls.Add(Me.Label4, 0, 0)
        Me.Panel3.Controls.Add(Me.cmbDateFormat, 1, 0)
        Me.Panel3.Location = New System.Drawing.Point(6, 54)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.RowCount = 1
        Me.Panel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Panel3.Size = New System.Drawing.Size(245, 28)
        Me.Panel3.TabIndex = 9
        Me.Panel3.Visible = False
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 22)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Date/Time Format"
        '
        'cmbDateFormat
        '
        Me.cmbDateFormat.FormattingEnabled = True
        Me.cmbDateFormat.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "hh:mm:ss", "HHmm", "HHmmss", "MM", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "MMMM", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyy-MM-dd", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss"})
        Me.cmbDateFormat.Location = New System.Drawing.Point(110, 3)
        Me.cmbDateFormat.Name = "cmbDateFormat"
        Me.cmbDateFormat.Size = New System.Drawing.Size(135, 21)
        Me.cmbDateFormat.Sorted = True
        Me.cmbDateFormat.TabIndex = 9
        '
        'Panel2
        '
        Me.Panel2.ColumnCount = 2
        Me.Panel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.Panel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.Panel2.Controls.Add(Me.Label3, 0, 0)
        Me.Panel2.Controls.Add(Me.txtCustom, 1, 0)
        Me.Panel2.Location = New System.Drawing.Point(6, 20)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.RowCount = 1
        Me.Panel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.Panel2.Size = New System.Drawing.Size(245, 28)
        Me.Panel2.TabIndex = 7
        Me.Panel2.Visible = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(3, 3)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 28)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Custom Field Descriptor"
        '
        'txtCustom
        '
        Me.txtCustom.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCustom.Location = New System.Drawing.Point(145, 3)
        Me.txtCustom.Name = "txtCustom"
        Me.txtCustom.Size = New System.Drawing.Size(97, 21)
        Me.txtCustom.TabIndex = 8
        '
        'cmdSelect
        '
        Me.cmdSelect.Image = CType(resources.GetObject("cmdSelect.Image"), System.Drawing.Image)
        Me.cmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSelect.Location = New System.Drawing.Point(439, 424)
        Me.cmdSelect.Name = "cmdSelect"
        Me.cmdSelect.Size = New System.Drawing.Size(75, 23)
        Me.cmdSelect.TabIndex = 1
        Me.cmdSelect.Text = "&Insert"
        Me.cmdSelect.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(439, 456)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Done"
        Me.cmdCancel.Visible = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.SuperTooltip1.TooltipDuration = 5
        '
        'tvInserter
        '
        Me.tvInserter.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvInserter.AllowDrop = True
        Me.tvInserter.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvInserter.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvInserter.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvInserter.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvInserter.ExpandButtonType = DevComponents.AdvTree.eExpandButtonType.Triangle
        Me.tvInserter.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvInserter.Location = New System.Drawing.Point(0, 0)
        Me.tvInserter.Name = "tvInserter"
        Me.tvInserter.NodesConnector = Me.NodeConnector1
        Me.tvInserter.NodeStyle = Me.ElementStyle1
        Me.tvInserter.PathSeparator = ";"
        Me.tvInserter.Size = New System.Drawing.Size(324, 389)
        Me.tvInserter.Styles.Add(Me.ElementStyle1)
        Me.SuperTooltip1.SetSuperTooltip(Me.tvInserter, New DevComponents.DotNetBar.SuperTooltipInfo("Inserts", "", "Drag and drop ""inserts"" from here to any field that has rounded corners." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Double-" & _
            "click on a Data Item or User Defined Constant to edit its definition.", CType(resources.GetObject("tvInserter.SuperTooltip"), System.Drawing.Image), Nothing, DevComponents.DotNetBar.eTooltipColor.Gray))
        Me.tvInserter.TabIndex = 3
        Me.tvInserter.Text = "AdvTree1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.Color.Transparent
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.pnlFiscalWeek)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Controls.Add(Me.Panel2)
        Me.GroupBox2.Controls.Add(Me.Panel3)
        Me.GroupBox2.Controls.Add(Me.pnlExtract)
        Me.GroupBox2.Controls.Add(Me.pnAttachIndex)
        Me.GroupBox2.Controls.Add(Me.pnMsgFormat)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(304, 95)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Options (if any)"
        '
        'pnlFiscalWeek
        '
        Me.pnlFiscalWeek.ColumnCount = 2
        Me.pnlFiscalWeek.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.0!))
        Me.pnlFiscalWeek.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.pnlFiscalWeek.Controls.Add(Me.txtFiscalWeek, 1, 0)
        Me.pnlFiscalWeek.Controls.Add(Me.LabelX4, 0, 0)
        Me.pnlFiscalWeek.Location = New System.Drawing.Point(6, 54)
        Me.pnlFiscalWeek.Name = "pnlFiscalWeek"
        Me.pnlFiscalWeek.RowCount = 1
        Me.pnlFiscalWeek.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlFiscalWeek.Size = New System.Drawing.Size(248, 30)
        Me.pnlFiscalWeek.TabIndex = 10
        '
        'txtFiscalWeek
        '
        Me.txtFiscalWeek.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFiscalWeek.Location = New System.Drawing.Point(196, 3)
        Me.txtFiscalWeek.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.txtFiscalWeek.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtFiscalWeek.Name = "txtFiscalWeek"
        Me.txtFiscalWeek.Size = New System.Drawing.Size(49, 21)
        Me.txtFiscalWeek.TabIndex = 1
        Me.txtFiscalWeek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtFiscalWeek.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 3)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(75, 19)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "Start Month"
        '
        'pnlExtract
        '
        Me.pnlExtract.ColumnCount = 2
        Me.pnlExtract.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlExtract.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.pnlExtract.Controls.Add(Me.LabelX1, 0, 0)
        Me.pnlExtract.Controls.Add(Me.txtLineNumber, 1, 0)
        Me.pnlExtract.Controls.Add(Me.LabelX2, 0, 2)
        Me.pnlExtract.Controls.Add(Me.txtCharsToRead, 1, 2)
        Me.pnlExtract.Controls.Add(Me.LabelX3, 0, 1)
        Me.pnlExtract.Controls.Add(Me.txtStartIndex, 1, 1)
        Me.pnlExtract.Location = New System.Drawing.Point(6, 20)
        Me.pnlExtract.Name = "pnlExtract"
        Me.pnlExtract.RowCount = 3
        Me.pnlExtract.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.pnlExtract.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.pnlExtract.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.pnlExtract.Size = New System.Drawing.Size(170, 75)
        Me.pnlExtract.TabIndex = 5
        Me.pnlExtract.Visible = False
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 18)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Line Number"
        '
        'txtLineNumber
        '
        '
        '
        '
        Me.txtLineNumber.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtLineNumber.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLineNumber.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtLineNumber.Location = New System.Drawing.Point(88, 3)
        Me.txtLineNumber.MinValue = 1
        Me.txtLineNumber.Name = "txtLineNumber"
        Me.txtLineNumber.ShowUpDown = True
        Me.txtLineNumber.Size = New System.Drawing.Size(79, 21)
        Me.txtLineNumber.TabIndex = 1
        Me.txtLineNumber.Value = 1
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 53)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 19)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Chars to read"
        '
        'txtCharsToRead
        '
        '
        '
        '
        Me.txtCharsToRead.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtCharsToRead.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCharsToRead.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtCharsToRead.Location = New System.Drawing.Point(88, 53)
        Me.txtCharsToRead.MinValue = 1
        Me.txtCharsToRead.Name = "txtCharsToRead"
        Me.txtCharsToRead.ShowUpDown = True
        Me.txtCharsToRead.Size = New System.Drawing.Size(79, 21)
        Me.txtCharsToRead.TabIndex = 2
        Me.txtCharsToRead.Value = 1
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 28)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(75, 14)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "Start Index"
        '
        'txtStartIndex
        '
        '
        '
        '
        Me.txtStartIndex.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtStartIndex.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStartIndex.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtStartIndex.Location = New System.Drawing.Point(88, 28)
        Me.txtStartIndex.MinValue = 1
        Me.txtStartIndex.Name = "txtStartIndex"
        Me.txtStartIndex.ShowUpDown = True
        Me.txtStartIndex.Size = New System.Drawing.Size(79, 21)
        Me.txtStartIndex.TabIndex = 2
        Me.txtStartIndex.Value = 1
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.GroupBox2)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 389)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(324, 105)
        Me.Panel4.TabIndex = 6
        '
        'frmInserter
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(324, 494)
        Me.Controls.Add(Me.tvInserter)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.cmdSelect)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmInserter"
        Me.ShowInTaskbar = False
        Me.Text = "Insert"
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnAttachIndex.ResumeLayout(False)
        Me.pnAttachIndex.PerformLayout()
        CType(Me.txtIndex, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnMsgFormat.ResumeLayout(False)
        Me.pnMsgFormat.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.tvInserter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.pnlFiscalWeek.ResumeLayout(False)
        CType(Me.txtFiscalWeek, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlExtract.ResumeLayout(False)
        CType(Me.txtLineNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCharsToRead, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtStartIndex, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Enum e_Mode As Integer
        DATA = 0
        CONSTANT = 1
    End Enum
    Public Property m_HideUserConstants() As Boolean
        Get
            Return HideUserConstants
        End Get
        Set(ByVal value As Boolean)
            HideUserConstants = value
        End Set
    End Property
    Public Property m_HideParameters() As Boolean
        Get
            Return HidePars
        End Get
        Set(ByVal value As Boolean)
            HidePars = True
        End Set
    End Property

    Public Property m_EventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = True
        End Set
    End Property

    Public Property m_EventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Private Property m_ReturnType() As e_Mode
        Get
            Return mode
        End Get
        Set(ByVal value As e_Mode)
            mode = value
        End Set
    End Property


    Private Sub frmInserter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)

        Dim recallInserterState As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("RecallInserterState", 1))

        If recallInserterState Then
            Dim loc As String = clsMarsUI.MainUI.ReadRegistry("inserterLocation", "")

            If loc <> "" Then
                Dim x, y As Integer

                x = loc.Split(",")(0)
                y = loc.Split(",")(1)

                If IsOnScreen(Me, x, y) Then
                    Me.Location = New Point(x, y)
                End If

            End If

            Dim formState As FormWindowState = clsMarsUI.MainUI.ReadRegistry("inserterState", 0)
        End If

        Dim size As String = clsMarsUI.MainUI.ReadRegistry("inserterSize", "")

        If size <> "" Then
            Dim width, height As Integer

            width = size.Split(",")(0)
            height = size.Split(",")(1)

            Me.Width = width
            Me.Height = height
        End If


        loaded = True
    End Sub

    Public Function IsOnScreen(form As Form, x As Integer, y As Integer) As Boolean
        Dim screens As Screen() = Screen.AllScreens
        For Each screen__1 As Screen In screens
            Dim formTopLeft As New Point(x, y)

            If screen__1.WorkingArea.Contains(formTopLeft) Then
                Return True
            End If
        Next

        Return False
    End Function


    Public Function GetDatabaseField(ByVal sTables() As String, ByVal sDataCon As String, ByVal owner As Form) As String
        sCon = sDataCon

        Try
            For Each s As String In sTables
                If s.Length > 0 Then addNode(s)
            Next
        Catch
        End Try

        Me.m_ReturnType = e_Mode.DATA

        Me.Owner = owner
        Me.Show()

        If UserCancel = True Then Return String.Empty

        '        Return "<[x]" & cmbType.Text & "." & cmbName.Text & ">"

    End Function

    Private Sub addNode(ByVal title As String, Optional ByVal nodeImage As Image = Nothing, Optional ByVal parent As DevComponents.AdvTree.Node = Nothing, Optional dataKey As String = "")
        Dim tnode As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(title)

        If nodeImage Is Nothing Then
            nodeImage = defaultnodeImage
        End If

        tnode.DataKey = dataKey
        tnode.Image = nodeImage

        If parent IsNot Nothing Then
            parent.Nodes.Add(tnode)

            parent.Nodes.Sort()
        Else
            tvInserter.Nodes.Add(tnode)
        End If
    End Sub
    Public Overloads Function GetConstants() As String
        Try
            tvInserter.Nodes.Clear()

            Dim insertTypes As String() = New String() {"SQL-RD Constants", "Event-Based Data", "Data Items"}

            Dim nodeImage As Image = resizeImage(My.Resources.components, New Size(16, 16))

            For Each s As String In insertTypes
                addNode(s, nodeImage)
            Next

            If Me.m_HideUserConstants = False Then
                addNode("User Defined Constants", nodeImage)
            End If

            If Me.m_HideParameters = False And greportItem IsNot Nothing Then
                addNode("Parameters", nodeImage)
            End If

            If frmRDScheduleWizard.m_DataFields IsNot Nothing Then
                addNode("Data-Driven Data", nodeImage)
            End If



            Me.m_ReturnType = e_Mode.CONSTANT
            Me.Owner = Owner

            If Container IsNot Nothing Then
                m_Container = Container
            End If

            Dialog = True

            Me.ShowDialog()

            If UserCancel = True Then Return String.Empty
        Catch : End Try
    End Function
    Public Overloads Function GetConstants(ByVal owner As Form, Optional ByVal container As ContainerControl = Nothing) As String
        Try
            tvInserter.Nodes.Clear()


            addNode("SQL-RD Constants")

            If Me.m_HideUserConstants = False Then
                addNode("User Defined Constants")
            End If

            If Me.m_HideParameters = False Then
                addNode("Parameters")
            End If

            addNode("Event-Based Data")

            If frmRDScheduleWizard.m_DataFields IsNot Nothing Then
                addNode("Data-Driven Data")
            End If

           

            addNode("Data Items")

            addNode("User Defaults")

            If gTables IsNot Nothing And gsCon IsNot Nothing Then addNode("Dynamic Schedule Table Fields")

            Dim selectedNodeText As String = clsMarsUI.MainUI.ReadRegistry("lastInsertNode", "")

            If selectedNodeText <> "" Then
                Dim selectedNode As DevComponents.AdvTree.Node = tvInserter.FindNodeByText(selectedNodeText)

                If selectedNode IsNot Nothing Then
                    tvInserter.SelectedNode = selectedNode
                    selectedNode.EnsureVisible()
                End If
            End If

            Me.m_ReturnType = e_Mode.CONSTANT
            Me.Owner = owner

            If container IsNot Nothing Then
                m_Container = container
            End If

            Dialog = False

            Me.Show()

            If UserCancel = True Then Return String.Empty
        Catch : End Try
    End Function
    Private Sub addChildNodes()
        Dim nodeImage As Image = resizeImage(My.Resources.component_yellow, 16, 16)

        For Each tnode As DevComponents.AdvTree.Node In tvInserter.Nodes
            If tnode.Parent Is Nothing Then
                Select Case tnode.Text
                    Case "User Defaults"
                        With tnode
                            .Nodes.Clear()
                            addNode("Default Subject", nodeImage, tnode)
                            addNode("Default Message", nodeImage, tnode)
                            addNode("Default Attachment", nodeImage, tnode)
                            addNode("Default Signature", nodeImage, tnode)
                        End With
                   
                    Case "SQL-RD Constants"
                        With tnode
                            .Nodes.Clear()
                            addNode("CurrentDate", nodeImage, tnode)
                            addNode("CurrentTime", nodeImage, tnode)
                            addNode("CurrentDateTime", nodeImage, tnode)
                            addNode("CurrentMonth", nodeImage, tnode)
                            addNode("CurrentMonthName", nodeImage, tnode)
                            addNode("CurrentShortMonthName", nodeImage, tnode)
                            addNode("CurrentScheduleName", nodeImage, tnode)
                            addNode("CurrentYear", nodeImage, tnode)
                            addNode("CurrentDay", nodeImage, tnode)
                            addNode("CurrentShortWeekDayName", nodeImage, tnode)
                            addNode("CurrentWeekDayName", nodeImage, tnode)
                            addNode("Monday Last Week", nodeImage, tnode)
                            addNode("Tuesday Last Week", nodeImage, tnode)
                            addNode("Wednesday Last Week", nodeImage, tnode)
                            addNode("Thursday Last Week", nodeImage, tnode)
                            addNode("Friday Last Week", nodeImage, tnode)
                            addNode("Saturday Last Week", nodeImage, tnode)
                            addNode("Sunday Last Week", nodeImage, tnode)
                            addNode("Monday This Week", nodeImage, tnode)
                            addNode("Tuesday This Week", nodeImage, tnode)
                            addNode("Wednesday This Week", nodeImage, tnode)
                            addNode("Thursday This Week", nodeImage, tnode)
                            addNode("Friday This Week", nodeImage, tnode)
                            addNode("Saturday This Week", nodeImage, tnode)
                            addNode("Sunday This Week", nodeImage, tnode)
                            addNode("Month Start Last Month", nodeImage, tnode)
                            addNode("Month End Last Month", nodeImage, tnode)
                            addNode("First Day of this Month", nodeImage, tnode)
                            addNode("Year Start Last Year", nodeImage, tnode)
                            addNode("Start This Year", nodeImage, tnode)
                            addNode("End Last Year", nodeImage, tnode)
                            addNode("Exported File Name", nodeImage, tnode)
                            addNode("Key Parameter Value", nodeImage, tnode)
                            addNode("Current Group Value", nodeImage, tnode)
                            addNode("Yesterday", nodeImage, tnode)
                            addNode("Last Day of the Month", nodeImage, tnode)
                            addNode("First Day of Next Month", nodeImage, tnode)
                            addNode("Current Temp File Path", nodeImage, tnode)
                            addNode("LastRun", nodeImage, tnode)
                            addNode("LastResult", nodeImage, tnode)
                            addNode("LatestRunTime", nodeImage, tnode)
                            addNode("LatestResult", nodeImage, tnode)
                            addNode("Schedule Owner", nodeImage, tnode)
                            addNode("FiscalWeek", nodeImage, tnode)
                        End With

                        Panel2.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Case "User Defined Constants"
                        With tnode
                            .Nodes.Clear()

                            Dim oRs As ADODB.Recordset
                            Dim SQL As String

                            SQL = "SELECT * FROM FormulaAttr"

                            oRs = clsMarsData.GetData(SQL)

                            Try
                                Do While oRs.EOF = False
                                    addNode(oRs("FormulaName").Value, nodeImage, tnode)
                                    oRs.MoveNext()
                                Loop
                                oRs.Close()
                            Catch ex As Exception

                            End Try
                        End With
                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Case "Parameters"

                        tnode.Nodes.Clear()

                        If g_parameterList IsNot Nothing Then
                            For Each s As String In g_parameterList
                                addNode(s, nodeImage, tnode)
                            Next
                        End If
                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Case "Event-Based Data"
                        tnode.Nodes.Clear()

                        GetEventData(nodeImage, tnode)

                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Case "Data-Driven Data"
                        tnode.Nodes.Clear()

                        If frmRDScheduleWizard.m_DataFields IsNot Nothing Then
                            For Each entry As DictionaryEntry In frmRDScheduleWizard.m_DataFields
                                addNode(entry.Value, nodeImage, tnode)
                            Next
                        End If
                    Case "Data Items"

                        tnode.Nodes.Clear()

                        Dim SQL As String = "SELECT DISTINCT ItemName FROM DataItems"
                        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                        If oRs IsNot Nothing Then
                            Do While oRs.EOF = False
                                addNode(oRs(0).Value, nodeImage, tnode)
                                oRs.MoveNext()
                            Loop

                            oRs.Close()
                        End If

                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Case Else
                        tnode.Nodes.Clear()

                        Dim sUser As String
                        Dim sDsn As String
                        Dim sPassword As String

                        sDsn = sCon.Split("|")(0)
                        sUser = sCon.Split("|")(1)
                        sPassword = sCon.Split("|")(2)

                        Dim cmb As ComboBox = New ComboBox

                        oData.GetColumns(cmb, sDsn, tnode.Text, sUser, sPassword)

                        For Each it As String In cmb.Items
                            addNode(it, nodeImage, tnode)
                        Next

                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                End Select
            End If
        Next
    End Sub

    Private Sub addChildNodes(ByVal tnode As DevComponents.AdvTree.Node)
        Dim nodeImage As Image = resizeImage(My.Resources.component_yellow, 16, 16)

        If tnode Is Nothing Then Return

        If tnode.Parent Is Nothing Then
            Select Case tnode.Text
                Case "SQL-RD Constants"
                    With tnode
                        .Nodes.Clear()
                        addNode("CurrentDate", nodeImage, tnode)
                        addNode("CurrentTime", nodeImage, tnode)
                        addNode("CurrentDateTime", nodeImage, tnode)
                        addNode("CurrentMonth", nodeImage, tnode)
                        addNode("CurrentMonthName", nodeImage, tnode)
                        addNode("CurrentShortMonthName", nodeImage, tnode)
                        addNode("CurrentScheduleName", nodeImage, tnode)
                        addNode("CurrentYear", nodeImage, tnode)
                        addNode("CurrentDay", nodeImage, tnode)
                        addNode("CurrentShortWeekDayName", nodeImage, tnode)
                        addNode("CurrentWeekDayName", nodeImage, tnode)
                        addNode("Monday Last Week", nodeImage, tnode)
                        addNode("Tuesday Last Week", nodeImage, tnode)
                        addNode("Wednesday Last Week", nodeImage, tnode)
                        addNode("Thursday Last Week", nodeImage, tnode)
                        addNode("Friday Last Week", nodeImage, tnode)
                        addNode("Saturday Last Week", nodeImage, tnode)
                        addNode("Sunday Last Week", nodeImage, tnode)
                        addNode("Monday This Week", nodeImage, tnode)
                        addNode("Tuesday This Week", nodeImage, tnode)
                        addNode("Wednesday This Week", nodeImage, tnode)
                        addNode("Thursday This Week", nodeImage, tnode)
                        addNode("Friday This Week", nodeImage, tnode)
                        addNode("Saturday This Week", nodeImage, tnode)
                        addNode("Sunday This Week", nodeImage, tnode)
                        addNode("Month Start Last Month", nodeImage, tnode)
                        addNode("Month End Last Month", nodeImage, tnode)
                        addNode("First Day of this Month", nodeImage, tnode)
                        addNode("Year Start Last Year", nodeImage, tnode)
                        addNode("Start This Year", nodeImage, tnode)
                        addNode("End Last Year", nodeImage, tnode)
                        addNode("Exported File Name", nodeImage, tnode)
                        addNode("Key Parameter Value", nodeImage, tnode)
                        addNode("Current Group Value", nodeImage, tnode)
                        addNode("Yesterday", nodeImage, tnode)
                        addNode("Last Day of the Month", nodeImage, tnode)
                        addNode("First Day of Next Month", nodeImage, tnode)
                        addNode("Current Temp File Path", nodeImage, tnode)
                        addNode("LastRun", nodeImage, tnode)
                        addNode("LastResult", nodeImage, tnode)
                        addNode("LatestRunTime", nodeImage, tnode)
                        addNode("LatestResult", nodeImage, tnode)
                        addNode("Schedule Owner", nodeImage, tnode)
                        addNode("FiscalWeek", nodeImage, tnode)
                    End With

                    Panel2.Visible = False
                    Me.pnMsgFormat.Visible = False

                Case "User Defined Constants"
                    With tnode
                        .Nodes.Clear()

                        Dim oRs As ADODB.Recordset
                        Dim SQL As String

                        SQL = "SELECT * FROM FormulaAttr ORDER BY FormulaName"

                        oRs = clsMarsData.GetData(SQL)

                        addNode("...New...", nodeImage, tnode, 0)

                        Try
                            Do While oRs.EOF = False
                                addNode(oRs("FormulaName").Value, nodeImage, tnode, oRs("formulaid").Value)
                                oRs.MoveNext()
                            Loop
                            oRs.Close()
                        Catch ex As Exception

                        End Try
                    End With
                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False

                Case "Parameters"
                    tnode.Nodes.Clear()

                    Try
                        If g_parameterList IsNot Nothing Then
                            For Each s As String In g_parameterList
                                addNode(s, nodeImage, tnode)
                            Next
                        End If
                    Catch : End Try

                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False

                Case "Event-Based Data"
                    tnode.Nodes.Clear()

                    GetEventData(nodeImage, tnode)

                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False
                Case "Dynamic Schedule Table Fields"
                    Try
                        If gsCon = "||" Then Return

                        Dim sUser As String
                        Dim sDsn As String
                        Dim sPassword As String

                        sDsn = gsCon.Split("|")(0)
                        sUser = gsCon.Split("|")(1)
                        sPassword = gsCon.Split("|")(2)

                        For Each table As String In gTables
                            Dim columns As ArrayList = oData.GetColumnsFromTable(table, sDsn, sUser, sPassword)

                            If columns IsNot Nothing Then
                                For Each col As String In columns
                                    addNode(table & "." & col, nodeImage, tnode)
                                Next
                            End If
                        Next
                    Catch : End Try

                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False
                Case "User Defaults"
                    tnode.Nodes.Clear()
                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False

                    addNode("Default Subject", nodeImage, tnode)
                    addNode("Default Attachment", nodeImage, tnode)
                    addNode("Default Message", nodeImage, tnode)
                    addNode("Default Signature", nodeImage, tnode)

                Case "Data-Driven Data"
                    tnode.Nodes.Clear()

                    If frmRDScheduleWizard.m_DataFields IsNot Nothing Then
                        For Each entry As DictionaryEntry In frmRDScheduleWizard.m_DataFields
                            addNode(entry.Value, nodeImage, tnode)
                        Next
                    End If
                    Return
                Case "Data Items"

                    tnode.Nodes.Clear()

                    Dim SQL As String = "SELECT ItemID, ItemName FROM DataItems ORDER BY ItemName"
                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    addNode("...New...", nodeImage, tnode, 0)

                    If oRs IsNot Nothing Then
                        Do While oRs.EOF = False
                            addNode(oRs("itemname").Value, nodeImage, tnode, oRs("itemid").Value)
                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If

                    Panel1.Visible = False
                    Panel2.Visible = False
                    Panel3.Visible = False
                    Me.pnMsgFormat.Visible = False

                Case Else
                    Try
                        If sCon Is Nothing Then Return

                        tnode.Nodes.Clear()

                        Dim sUser As String
                        Dim sDsn As String
                        Dim sPassword As String

                        sDsn = sCon.Split("|")(0)
                        sUser = sCon.Split("|")(1)
                        sPassword = sCon.Split("|")(2)

                        Dim columns As ArrayList = oData.GetColumnsFromTable(tnode.Text, sDsn, sUser, sPassword)

                        If columns IsNot Nothing Then
                            For Each column As String In columns
                                addNode(column, nodeImage, tnode)
                            Next
                        End If

                        Panel1.Visible = False
                        Panel2.Visible = False
                        Panel3.Visible = False
                        Me.pnMsgFormat.Visible = False
                    Catch : End Try
            End Select
        End If


        tnode.Expand()
    End Sub
    Private Sub GetEventData(ByVal nodeImage As Image, ByVal tnode As DevComponents.AdvTree.Node)
        If tnode Is Nothing Then Return

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim nID As Integer = 0

            If clsMarsEvent.currentEventID = 0 Then
                nID = eventID
            Else
                nID = clsMarsEvent.currentEventID
            End If

            SQL = "SELECT * FROM EventConditions WHERE EventID = " & nID

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim conditionType As String = oRs("conditiontype").Value
                    Dim conditionName As String = oRs("conditionname").Value

                    Select Case conditionType.ToLower
                        Case "data received"
                            tnode.Nodes.Clear()
                            addNode(conditionName & ";ReceivedData", nodeImage, tnode)
                            addNode(conditionName & ";Custom", nodeImage, tnode)
                            addNode(conditionName & ";Extract(LineNumber, StartIndex, CharsToRead)", nodeImage, tnode)
                        Case "database record exists", "database record has changed"
                            Dim oRs1 As ADODB.Recordset = New ADODB.Recordset
                            Dim oCon As ADODB.Connection = New ADODB.Connection
                            Dim connectionString As String
                            Dim user As String
                            Dim password As String
                            Dim dsn As String

                            SQL = oRs("searchcriteria").Value
                            connectionString = oRs("connectionstring").Value

                            dsn = connectionString.Split("|")(0)
                            user = connectionString.Split("|")(1)
                            password = connectionString.Split("|")(2)

                            password = _DecryptDBValue(password)

                            oCon.Open(dsn, user, password)

                            oRs1.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic)

                            For Each field As ADODB.Field In oRs1.Fields
                                addNode(conditionName & ";[" & field.Name & "]", nodeImage, tnode)
                            Next

                            'cmbName.Items.Add(conditionName & ";Custom")

                            oRs1.Close()

                            oCon.Close()
                        Case "file exists", "file has been modified"
                            tnode.Nodes.Clear()

                            addNode(conditionName & ";FileName", nodeImage, tnode)
                            addNode(conditionName & ";FileDirectory", nodeImage, tnode)
                            addNode(conditionName & ";FilePath", nodeImage, tnode)
                            addNode(conditionName & ";DateModified", nodeImage, tnode)
                            addNode(conditionName & ";DateCreated", nodeImage, tnode)

                        Case "process exists", "window is present"
                            tnode.Nodes.Clear()
                            addNode(conditionName & ";ProcessName", nodeImage, tnode)
                            addNode(conditionName & ";MainWindowTitle", nodeImage, tnode)
                            addNode(conditionName & ";TotalProcessorTime", nodeImage, tnode)
                            addNode(conditionName & ";UserProcessorTime", nodeImage, tnode)
                            addNode(conditionName & ";MaxWorkingSet", nodeImage, tnode)
                            addNode(conditionName & ";MinWorkingSet", nodeImage, tnode)
                            addNode(conditionName & ";ID")

                        Case "unread email is present"
                            tnode.Nodes.Clear()
                            addNode(conditionName & ";From", nodeImage, tnode)
                            addNode(conditionName & ";To", nodeImage, tnode)
                            addNode(conditionName & ";Cc", nodeImage, tnode)
                            addNode(conditionName & ";Bcc", nodeImage, tnode)
                            addNode(conditionName & ";Subject", nodeImage, tnode)
                            addNode(conditionName & ";Message", nodeImage, tnode)
                            addNode(conditionName & ";DateSent", nodeImage, tnode)
                            addNode(conditionName & ";Attachments", nodeImage, tnode)
                            addNode(conditionName & ";AttachmentsPath", nodeImage, tnode)
                            addNode(conditionName & ";Custom", nodeImage, tnode)

                    End Select

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, ex.TargetSite.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub tvInserter_AfterSelect(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub txtCustom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustom.TextChanged
        Dim tnode As DevComponents.AdvTree.Node = tvInserter.SelectedNode

        If tnode Is Nothing Then Return

        Dim conditionStart As String = tnode.Text.Split(";")(0)

        If txtCustom.Text.Length > 0 Then
            tnode.Text = conditionStart & ";" & txtCustom.Text
        Else
            tnode.Text = conditionStart & ";Custom"
        End If
    End Sub

    Private Sub tvInserter_AfterNodeSelect(ByVal sender As System.Object, ByVal e As DevComponents.AdvTree.AdvTreeNodeEventArgs) Handles tvInserter.AfterNodeSelect
        Dim tnode As DevComponents.AdvTree.Node = tvInserter.SelectedNode

        If tnode Is Nothing Then Return

        If tnode.Parent Is Nothing Then
            GroupBox2.Visible = False
        Else
            GroupBox2.Visible = True
        End If

        If tnode.Text = "[New/Edit...]" Then

            Dim oNew As New frmFormulaEditor
            Dim sConstant As String = ""

            sConstant = oNew.GetName()


            tvInserter.Nodes.Clear()
            addNode("[New/Edit...]", , tnode)

            Dim oRs As ADODB.Recordset
            Dim SQL As String

            SQL = "SELECT * FROM FormulaAttr"

            oRs = clsMarsData.GetData(SQL)

            Try
                Do While oRs.EOF = False
                    addNode(oRs("FormulaName").Value, , tnode)
                    oRs.MoveNext()
                Loop
                oRs.Close()
            Catch ex As Exception

            End Try


            If sConstant.Length > 0 Then
                tnode.Text = sConstant
            End If
        Else

            pnlExtract.Visible = False
            pnlFiscalWeek.Visible = False

            If tnode.Nodes.Count = 0 Then addChildNodes(tnode)

            If tnode.Text.IndexOf(";Custom") > -1 Then
                If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
                    _NeedUpgrade(gEdition.CORPORATE, tnode, "Advanced Events Pack")
                    tvInserter.SelectedNode = tvInserter.Nodes(0)
                    Return
                End If

                Panel1.Visible = False
                Panel2.Visible = True
                Panel2.BringToFront()
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
                Me.pnAttachIndex.Visible = False
            ElseIf tnode.Text.IndexOf(";Extract") > -1 Then
                If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
                    _NeedUpgrade(gEdition.CORPORATE, tnode, "Advanced Events Pack")
                    tvInserter.SelectedNode = tvInserter.Nodes(0)
                    Return
                End If

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                pnlExtract.Visible = True
                pnlExtract.BringToFront()
            ElseIf tnode.Text.IndexOf(";Message") > -1 Then
                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = True
                Me.pnAttachIndex.Visible = False
            ElseIf tnode.Text.IndexOf(";Attachments") > -1 Then
                Me.pnAttachIndex.Visible = True
                Panel2.Visible = False
                Panel3.Visible = False
                Panel1.Visible = False
                Me.pnMsgFormat.Visible = False
            Else
                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
                Me.pnAttachIndex.Visible = False

                cmbDateFormat.Text = clsMarsUI.MainUI.ReadRegistry("InserterDateFormat", "")

                Select Case tnode.Text
                    Case "CurrentScheduleName", "Exported File Name", "Key Parameter Value", "Current Group Value", "Current Temp File Path", "Schedule Owner"
                        txtAdjustStamp.Enabled = False
                        txtAdjustStamp.Value = 0
                        Panel1.Visible = False
                    Case Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) = False Then
                            txtAdjustStamp.Enabled = False
                            txtAdjustStamp.Value = 0
                            Panel1.Visible = False
                        End If

                        Select Case tnode.Text
                            Case "CurrentWeekDayName", "CurrentDay"
                                Label26.Text = "Adjust value by (days)"
                                Panel3.Visible = False
                                Panel1.Visible = True
                            Case "CurrentTime", "CurrentDateTime"
                                Label26.Text = "Adjust value by (minutes)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                            Case "CurrentMonthName", "CurrentShortMonthName"
                                Label26.Text = "Adjust value by (months)"
                                Panel1.Visible = True
                            Case "CurrentYear"
                                Label26.Text = "Adjust value by (years)"
                                Panel1.Visible = True
                            Case "CurrentMonth"
                                Label26.Text = "Adjust value by (months)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                            Case "FiscalWeek"
                                Label26.Text = "Adjust start date by (days)"
                                Panel1.Visible = True
                                Panel3.Visible = False
                                pnlFiscalWeek.Visible = True
                            Case Else
                                Label26.Text = "Adjust value by (days)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                        End Select

                        If gnEdition >= gEdition.ENTERPRISE Then txtAdjustStamp.Enabled = True
                End Select

                If tnode.Parent IsNot Nothing Then
                    If tnode.Parent.Text <> "SQL-RD Constants" Then
                        Panel1.Visible = False
                        Panel3.Visible = False
                    End If


                    clsMarsUI.MainUI.SaveRegistry("lastInsertNode", tnode.Parent.Text)
                Else
                    clsMarsUI.MainUI.SaveRegistry("lastInsertNode", tnode.Text)
                End If
            End If
        End If
    End Sub


    Private Sub tvInserter_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvInserter.MouseMove
        Dim tnode As DevComponents.AdvTree.Node = tvInserter.SelectedNode

        If tnode Is Nothing Then Return
        If tnode.Parent Is Nothing Then Return

        Dim tparent As DevComponents.AdvTree.Node = tnode.Parent

        If e.Button = Windows.Forms.MouseButtons.Left And tnode.Text <> "...New..." Then
            Dim data As String = ""

            If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.i1_Inserts) = False And _
                IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s3_EventBasedScheds) = False And _
                IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s5_DataDrivenSched) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, tvInserter, "Inserts")

                Return
            End If

            Select Case Me.m_ReturnType
                Case e_Mode.CONSTANT
                    If tparent.Text = "SQL-RD Constants" Then
                        Dim value As String

                        value = tnode.Text

                        If txtAdjustStamp.Value <> 0 Then
                            value &= ";" & txtAdjustStamp.Value
                        End If

                        If Panel3.Visible = True Then
                            If cmbDateFormat.Text = "" Then cmbDateFormat.Text = "yyyy-MM-dd"

                            value &= ";" & cmbDateFormat.Text
                        End If


                        data = "<[m]" & value & ">"
                    ElseIf tparent.Text = "User Defaults" Then
                        Dim oUI As clsMarsUI = New clsMarsUI
                        Dim ud As userdefaults = New userdefaults

                        Select Case tnode.Text
                            Case "Default Subject"
                                If ud.shareUserDefaults Then
                                    data = ud.emailSubject
                                Else
                                    data = oUI.ReadRegistry("DefSubject", "")
                                End If
                            Case "Default Message"
                                If ud.shareUserDefaults Then
                                    data = ud.emailMessage
                                Else
                                    data = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
                                End If
                            Case "Default Signature"
                                If ud.shareUserDefaults Then
                                    data = ud.emailSignature
                                Else
                                    data = ReadTextFromFile(sAppPath & "defsig.sqlrd")
                                End If
                            Case "Default Attachment"
                                If ud.shareUserDefaults Then
                                    data = ud.emailAttachment
                                Else
                                    data = oUI.ReadRegistry("DefAttach", "")
                                End If
                            Case Else
                                data = ""
                        End Select
                    ElseIf tparent.Text = "User Defined Constants" Then

                        data = "<[u]" & tnode.Text & ">"
                    ElseIf tparent.Text = "Parameters" Then
                        data = "<[p]" & tnode.Text & ">"
                    ElseIf tparent.Text = "Event-Based Data" Then
                        Dim value As String = tnode.Text

                        If Me.pnMsgFormat.Visible = True Then
                            If optHTML.Checked = True Then
                                value &= ";HTML"
                            ElseIf optText.Checked = True Then
                                value &= ";TEXT"
                            End If
                        ElseIf Me.pnAttachIndex.Visible = True Then
                            value &= ";" & Me.txtIndex.Value
                        ElseIf pnlExtract.Visible Then
                            value = value.Replace("LineNumber", 1).Replace("CharsToRead", 1).Replace("StartIndex", 1)
                        End If

                        data = "<[e]" & value & ">"

                    ElseIf tparent.Text = "Dynamic Schedule Table Fields" Then
                        Dim value As String = tnode.Text

                        data = "<[x]" & value & ">"
                    ElseIf tparent.Text = "Data-Driven Data" Then
                        Dim value As String = tnode.Text

                        data = "<[r]" & value & ">"
                    ElseIf tparent.Text = "Crystal Report Field" Then
                        Dim value As String = tnode.Text

                        data = "<[c]" & value & ">"
                    ElseIf tparent.Text = "Data Items" Then
                        Dim value As String = tnode.Text


                        data = "<[d]" & value & ">"
                    Else

                        data = tnode.Text
                    End If


                Case e_Mode.DATA

                    data = "<[x]" & tparent.Text & "." & tnode.Text & ">"

            End Select

            tvInserter.DoDragDrop(data, DragDropEffects.Copy)
        End If


    End Sub




    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub tvInserter_Click(sender As System.Object, e As System.EventArgs) Handles tvInserter.Click

    End Sub

    Private Sub frmInserter_Move(sender As Object, e As System.EventArgs) Handles Me.Move
        If loaded Then clsMarsUI.MainUI.SaveRegistry("inserterLocation", Me.Location.X & "," & Me.Location.Y)
    End Sub

    Private Sub tvInserter_DoubleClick(sender As Object, e As System.EventArgs) Handles tvInserter.DoubleClick
        If tvInserter.SelectedNode Is Nothing Then Return

        If tvInserter.SelectedNode.Parent Is Nothing Then Return

        Dim nodeImage As Image = resizeImage(My.Resources.component_yellow, 16, 16)

        Select Case tvInserter.SelectedNode.Parent.Text
            Case "Data Items"
                Dim dataItemName As String = tvInserter.SelectedNode.Text
                Dim dataItemID As String = tvInserter.SelectedNode.DataKey

                If dataItemID = "" Then Return

                Dim editor As frmEditDataItem = New frmEditDataItem

                If dataItemID > 0 Then
                    dataItemName = editor.EditDataItems(dataItemID)

                    If dataItemName <> "" Then tvInserter.SelectedNode.Text = dataItemName
                Else
                    dataItemName = editor.AddDataItem()

                    If dataItemName IsNot Nothing Then
                        Dim newNode As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(dataItemName)
                        newNode.Image = nodeimage
                        newNode.DataKey = editor.newDataItemID

                        tvInserter.SelectedNode.Parent.Nodes.Add(newNode)
                        tvInserter.SelectedNode = newNode
                        newNode.EnsureVisible()
                    End If
                End If

            Case "User Defined Constants"
                Dim formulaID As String = tvInserter.SelectedNode.DataKey

                If formulaID = "" Then Return

                Dim formEditor As frmFormulaEditor = New frmFormulaEditor

                If formulaID > 0 Then
                    Dim name As String = formEditor.EditConstant(formulaID)

                    If name IsNot Nothing Then
                        tvInserter.SelectedNode.Text = name
                    End If
                Else
                    Dim name As String = formEditor.AddConstant(eventID)

                    If name IsNot Nothing Then
                        Dim newNode As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node(name)
                        newNode.Image = nodeimage
                        newNode.DataKey = formEditor.newDataItemID
                        tvInserter.SelectedNode.Parent.Nodes.Add(newNode)
                    End If
                End If
        End Select
    End Sub

    Private Sub frmInserter_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        If loaded And Me.WindowState = FormWindowState.Minimized Then
            Dim res As DialogResult = MessageBox.Show("You just minimized the Inserts screen. Would you like SQL-RD to keep it minimized in the future?", Application.ProductName, MessageBoxButtons.YesNo)

            If res = Windows.Forms.DialogResult.Yes Then
                clsMarsUI.MainUI.SaveRegistry("RecallInserterState", 1)
                clsMarsUI.MainUI.SaveRegistry("inserterState", Convert.ToInt16(Me.WindowState))
            Else
                clsMarsUI.MainUI.SaveRegistry("RecallInserterState", 0)
            End If
        End If
    End Sub

    Private Sub frmInserter_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        If loaded Then clsMarsUI.MainUI.SaveRegistry("inserterSize", Me.Width & "," & Me.Height)
    End Sub


    Private Sub txtLineNumber_ValueChanged(sender As System.Object, e As System.EventArgs) Handles txtLineNumber.ValueChanged, txtCharsToRead.ValueChanged, txtStartIndex.ValueChanged
        Dim tnode As DevComponents.AdvTree.Node = tvInserter.SelectedNode

        If tnode Is Nothing Then Return

        Dim conditionStart As String = tnode.Text.Split(";")(0)

        tnode.Text = conditionStart & ";Extract(" & txtLineNumber.Value & "," & txtStartIndex.Value & "," & txtCharsToRead.Value & ")"

    End Sub

    Private Sub cmbDateFormat_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cmbDateFormat.SelectedIndexChanged
        If cmbDateFormat.Text <> "" Then
            clsMarsUI.MainUI.SaveRegistry("InserterDateFormat", cmbDateFormat.Text)
        End If
    End Sub

    Private Sub tvInserter_GiveFeedback(sender As Object, e As GiveFeedbackEventArgs) Handles tvInserter.GiveFeedback
        e.UseDefaultCursors = e.Effect <> DragDropEffects.Copy
    End Sub
End Class
