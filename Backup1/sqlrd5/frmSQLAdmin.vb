Public Class frmSQLAdmin
    Inherits DevComponents.DotNetBar.Office2007Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDBName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdGo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSQLAdmin))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdGo = New DevComponents.DotNetBar.ButtonX()
        Me.txtDBName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdGo)
        Me.GroupBox1.Controls.Add(Me.txtDBName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmdTest)
        Me.GroupBox1.Controls.Add(Me.UcDSN)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 264)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmdGo
        '
        Me.cmdGo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdGo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdGo.Enabled = False
        Me.cmdGo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdGo.Location = New System.Drawing.Point(167, 224)
        Me.cmdGo.Name = "cmdGo"
        Me.cmdGo.Size = New System.Drawing.Size(75, 23)
        Me.cmdGo.TabIndex = 4
        Me.cmdGo.Text = "Go"
        '
        'txtDBName
        '
        '
        '
        '
        Me.txtDBName.Border.Class = "TextBoxBorder"
        Me.txtDBName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDBName.Location = New System.Drawing.Point(8, 184)
        Me.txtDBName.Name = "txtDBName"
        Me.txtDBName.ReadOnly = True
        Me.txtDBName.Size = New System.Drawing.Size(392, 21)
        Me.txtDBName.TabIndex = 3
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 168)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Database Name"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(167, 136)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdTest.TabIndex = 1
        Me.cmdTest.Text = "Connect"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(400, 112)
        Me.UcDSN.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(476, 291)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 15
        Me.cmdCancel.Text = "&Done"
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(435, 12)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 264)
        Me.ReflectionImage1.TabIndex = 16
        '
        'frmSQLAdmin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(564, 318)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSQLAdmin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Migrate to SQL Server"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub frmSQLAdmin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        If UcDSN._Validate = True Then
            cmdGo.Enabled = True
            Try
                Dim o As New ADODB.Connection

                o.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

                txtDBName.Text = o.DefaultDatabase

                o.Close()
            Catch : End Try
        Else
            cmdGo.Enabled = False
        End If
    End Sub

    Public Function _CreateDataTables(Optional ByVal sCon As String = "", Optional ByVal ToLocalSQL As Boolean = False) As Boolean
        Dim SQL As String
        Dim sAr() As String
        Dim oUI As New clsMarsUI
        Dim I As Integer = 1
        Dim oData As New clsMarsData

        AppStatus(True)

        Dim oCombo As New ComboBox
        Dim sTable As String
        Dim oItems As ComboBox.ObjectCollection


        Dim oC As New ADODB.Connection

        If sCon.Length = 0 Then
            With UcDSN
                oData.GetTables(oCombo, sqlrd.sCon) '.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
                oC.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
            End With
        Else
            If ToLocalSQL = False Then
                oData.GetTables(oCombo, sCon)
                oC.Open(sCon)
            Else
                oC.Open(sCon)
            End If
        End If

        If ToLocalSQL = False Then
            oItems = oCombo.Items

            For I = 0 To oItems.Count - 1
                sTable = oItems.Item(I)

                Try
                    oC.Execute("DROP TABLE " & sTable)
                Catch : End Try
            Next
        End If

        _Delay(5)

        SQL = ReadTextFromFile(sAppPath & "sqlrd.sql")

        If SQL.Length = 0 Then Return False

        sAr = Split(SQL, "GO")

        For Each s As String In sAr

            oUI.BusyProgress(((I / sAr.GetUpperBound(0)) * 100), "Creating tables...")

            If s.Length > 0 Then
                Try
                    oC.Execute(s)
                Catch ex As Exception
                    If ex.Message.IndexOf("There is already an object named") = -1 Then
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                        _GetLineNumber(ex.StackTrace))
                        Return False
                    End If
                End Try
            End If

            I += 1
        Next

        'update the tables

        Dim sLoc As String

        If ToLocalSQL = False Then
            sLoc = "Provider=MSDASQL.1;Password=" & UcDSN.txtPassword.Text & ";Persist Security Info=True;User ID=" & UcDSN.txtUserID.Text & ";Data Source=" & UcDSN.cmbDSN.Text
        Else
            sLoc = sCon
        End If

        oData.UpgradeCRDDb(sLoc)

        _Delay(5)

        oC.Close()

        AppStatus(False)

        Return True
    End Function
    Public Function _ReturntoDatz(ByVal SetSys As Boolean, Optional ByVal sLivePath As String = "") As Boolean
        Dim oCombo As New ComboBox
        Dim oData As New clsMarsData
        Dim sPath As String
        Dim oC As New ADODB.Connection
        Dim I As Integer
        Dim oUI As New clsMarsUI
        Dim oRs As ADODB.Recordset
        Dim sValue

        Try
            If sLivePath.Length = 0 Then
                If IO.File.Exists(sAppPath & "sqlrdlive.dat") = False Then
                    IO.File.Copy(sAppPath & "sqlrd.dat", sAppPath & "sqlrdlive.dat", True)
                End If

                sLivePath = sAppPath & "sqlrdlive.dat"
            Else
                If IO.File.Exists(sLivePath & "\sqlrdlive.dat") = False Then
                    IO.File.Copy(sAppPath & "sqlrd.dat", sLivePath & "\sqlrdlive.dat", True)
                End If

                sLivePath = sLivePath & "\sqlrdlive.dat"
            End If

            sPath = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sLivePath & ";Persist Security Info=False"

            oData.GetTables(oCombo, sPath)

            Dim oItems As ComboBox.ObjectCollection
            Dim sTable As String
            Dim SQL As String

            oItems = oCombo.Items

            oC.Open(sPath)

            'drop the tables

            For I = 0 To oItems.Count - 1
                sTable = oItems.Item(I)

                SQL = "DROP TABLE " & sTable

                Try : oC.Execute(SQL) : Catch : End Try
            Next

            'recreate the tables
            SQL = ReadTextFromFile(sAppPath & "sqlrd.sql")

            If SQL.Length = 0 Then Return False

            Dim sAr() As String

            sAr = Split(SQL, "GO")

            For Each s As String In sAr

                oUI.BusyProgress(((I / sAr.GetUpperBound(0)) * 100), "Creating tables...")

                If s.Trim.Length > 0 Then
                    Try
                        s = s.Replace("DEFAULT 0", String.Empty)

                        s = s.ToLower.Replace("[int]", "int").Replace("[varchar]", "varchar"). _
                        Replace("[text]", "text").Replace("[datetime]", "datetime"). _
                        Replace("[float]", "float")

                        s = s.ToLower.Replace("null", "")

                        oC.Execute(s)
                    Catch ex As Exception
                        If ex.Message.IndexOf("There is already an object named") = -1 Then
                            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                            _GetLineNumber(ex.StackTrace))
                            Return False
                        End If
                    End Try
                End If

                I += 1
            Next

            System.Threading.Thread.Sleep(3000)

            'upgrade the tables
            oData.UpgradeCRDDb(sPath)

            System.Threading.Thread.Sleep(3000)

            'copy the data
            For I = 0 To oItems.Count - 1

                SQL = ""

                sTable = oItems.Item(I)

                If sTable.ToLower = "tmpfolders" Then
                    GoTo NextRec
                End If

                oRs = clsMarsData.GetData("SELECT * FROM " & sTable)

                If oRs Is Nothing Then GoTo NextRec

                oUI.BusyProgress(((I / oItems.Count) * 100), "Copying table " & sTable & "...")

                Do While oRs.EOF = False
                    SQL = ""

                    For x As Integer = 0 To oRs.Fields.Count - 1
                        sValue = SQLPrepare(IsNull(oRs.Fields(x).Value))

                        If oRs.Fields(x).Type = ADODB.DataTypeEnum.adDate Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBDate Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBTime Or _
                        oRs.Fields(x).Type = 135 Then
                            Try
                                Date.Parse(sValue)

                                If oRs.Fields(x).Name <> "RepeatUntil" Then
                                    sValue = ConDate(sValue) & " " & ConTime(sValue)
                                End If

                            Catch
                                sValue = ConDateTime(Now)
                            End Try

                            sValue = "'" & sValue & "'"

                        ElseIf oRs.Fields(x).Type = ADODB.DataTypeEnum.adBigInt Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adDouble Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adInteger Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adNumeric Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adSingle Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adSmallInt Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adTinyInt Or _
                        oRs.Fields(x).Type = ADODB.DataTypeEnum.adVarNumeric Then

                            Try
                                Int32.Parse(sValue)
                            Catch ex As Exception
                                sValue = 0
                            End Try
                        Else
                            sValue = "'" & sValue & "'"
                        End If

                        SQL &= sValue & ","
                    Next

                    SQL = SQL.Substring(0, SQL.Length - 1)

                    SQL = "INSERT INTO " & sTable & " VALUES (" & SQL & ")"

                    Try
                        oC.Execute(SQL)
                    Catch : End Try

                    oRs.MoveNext()
                Loop

                oRs.Close()
NextRec:
            Next

            oC.Close()

            oUI.BusyProgress(90, "Saving settings...")

            oUI.BusyProgress(100, , True)

            If SetSys = True Then
                oUI.SaveRegistry("ConType", "DAT")
                MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Information)

                Process.Start(sAppPath & assemblyName)

                Application.Exit()
            End If

            Return True

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function
    Public Function _CopyData(Optional ByVal sCon As String = "") As Boolean
        Dim oData As New clsMarsData
        Dim oCombo As New ComboBox
        Dim oC As New ADODB.Connection
        Dim oItems As ComboBox.ObjectCollection
        Dim oRs As ADODB.Recordset
        Dim sTable As String
        Dim oUI As New clsMarsUI
        Dim sValue

        If sCon.Length = 0 Then
            oData.GetTables(oCombo, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            oC.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        Else
            oData.GetTables(oCombo, sCon)
            oC.Open(sCon)
        End If

        Dim SQL As String

        oItems = oCombo.Items

        For I As Integer = 0 To oItems.Count - 1

            SQL = ""

            sTable = oItems.Item(I)

            oRs = clsMarsData.GetData("SELECT * FROM " & sTable)

            If oRs Is Nothing Then GoTo NextRec

            oUI.BusyProgress(((I / oItems.Count) * 100), "Copying table " & sTable & "...")

            Do While oRs.EOF = False
                SQL = ""

                For x As Integer = 0 To oRs.Fields.Count - 1
                    sValue = SQLPrepare(IsNull(oRs.Fields(x).Value))

                    If oRs.Fields(x).Type = ADODB.DataTypeEnum.adDate Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBDate Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDBTime Then
                        Try
                            Date.Parse(sValue)

                            If oRs.Fields(x).Name <> "RepeatUntil" Then
                                sValue = ConDate(sValue) & " " & ConTime(sValue)
                            End If
                        Catch
                            sValue = ConDateTime(Now)
                        End Try
                    ElseIf oRs.Fields(x).Type = ADODB.DataTypeEnum.adBigInt Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDouble Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adDouble Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adInteger Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adNumeric Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adSingle Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adSmallInt Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adTinyInt Or _
                    oRs.Fields(x).Type = ADODB.DataTypeEnum.adVarNumeric Then

                        Try
                            Int32.Parse(sValue)
                        Catch ex As Exception
                            sValue = 0
                        End Try
                    End If

                    SQL &= "'" & sValue & "',"
                Next

                SQL = SQL.Substring(0, SQL.Length - 1)

                SQL = "INSERT INTO " & sTable & " VALUES (" & SQL & ")"

                Try
                    oC.Execute(SQL)
                Catch ex As Exception
                    ''console.writeline(ex.Message)
                End Try

                oRs.MoveNext()
            Loop

            oRs.Close()
NextRec:
        Next

        oUI.BusyProgress(, , True)


        Return True

    End Function

    Private Delegate Function MigrateSystem_A(ByVal dsn As String, ByVal user As String, ByVal password As String, ByVal exceptTables As ArrayList) As Boolean


    Private Sub cmdGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdGo.Click
        Try
            Dim oUI As New clsMarsUI
            Dim ConString As String
            Dim oRes As DialogResult

            IO.File.Delete(sAppPath & "crdbuild.dll")

            oRes = MessageBox.Show("All tables in the selected database WILL be deleted. Are you sure you want to proceed with this operation?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning)

            If oRes = DialogResult.No Then
                Return
            End If

            clsServiceController.itemGlobal.StopScheduler()

            cmdGo.Enabled = False
            cmdTest.Enabled = False
            cmdCancel.Enabled = False

            AppStatus(True)

            oUI.BusyProgress(25, "Creating tables...")

            Dim oInvoke As New MigrateSystem_A(AddressOf clsMigration.MigrateSystem)
            Dim result As System.IAsyncResult

            result = oInvoke.BeginInvoke(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text, Nothing, Nothing, Nothing)

            Do While result.IsCompleted = False
                Application.DoEvents()
            Loop

            'If clsMigration.MigrateSystem(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text) = True Then

            
            If clsMigration.m_Result = True Then

                oUI.BusyProgress(90, "Saving settings...")

                oUI.SaveRegistry("ConType", "ODBC", False, , True)

                ConString = "Provider=MSDASQL.1;Password=" & UcDSN.txtPassword.Text & _
                ";Persist Security Info=True;User ID=" & UcDSN.txtUserID.Text & _
                ";Data Source=" & UcDSN.cmbDSN.Text

                oUI.SaveRegistry("ConString", ConString, True, , True)

                oUI.BusyProgress(100, , True)

                MessageBox.Show("SQL-RD will now exit so that you can log into the new system", Application.ProductName, _
                MessageBoxButtons.OK, MessageBoxIcon.Information)

                Process.Start(sAppPath & assemblyName)

                clsServiceController.itemGlobal.StartScheduler()

                End
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        Finally
            cmdGo.Enabled = True
            cmdTest.Enabled = True
            cmdCancel.Enabled = True

            AppStatus(False)
        End Try

    End Sub

    Public Function _CreateDatabase(ByVal instanceName As String, Optional ByVal dataLocation As String = "") As Boolean
        Dim SQLPath As String
        Dim Migrate As Boolean = False

        Try

            Migrate = True

            Dim Retry As Integer = 1
retry:

            Dim oCon As New ADODB.Connection
            Dim sCon As String

            If instanceName = "" Then Return False

            sCon = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=master;Data Source=" & instanceName

            Try
                oCon.Open(sCon)
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

            'SQLPath = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles)

            If dataLocation = "" Then
                Dim fbd As FolderBrowserDialog = New FolderBrowserDialog

                With fbd
                    .ShowNewFolderButton = True
                    .Description = "Please select the folder to store your Data files"
                    If .ShowDialog = Windows.Forms.DialogResult.Cancel Then Return False
                End With


                SQLPath = fbd.SelectedPath '&= "\Microsoft SQL Server\MSSQL\Data\"
            Else
                SQLPath = dataLocation
            End If

            If SQLPath.EndsWith("\") = False Then SQLPath &= "\"

            Try
                oCon.Execute("DROP DATABASE [SQL-RD]")
            Catch : End Try

            Dim DataFile As String = SQLPath & "Live_Data.mdf"
            Dim LogFile As String = SQLPath & "Live_Log.ldf"
            Dim SQL As String

            Try
                If IO.File.Exists(DataFile) Then IO.File.Delete(DataFile)
                If IO.File.Exists(LogFile) Then IO.File.Delete(LogFile)
            Catch : End Try

            SQL = "CREATE DATABASE [SQL-RD] " & vbCrLf & _
            "ON PRIMARY " & vbCrLf & _
            "(NAME = N'Live_Data'," & vbCrLf & _
            "FILENAME = N'" & DataFile & "'," & vbCrLf & _
            "SIZE = 10," & vbCrLf & _
            "MAXSIZE = 100," & vbCrLf & _
            "FILEGROWTH = 20 ) " & vbCrLf & _
            "LOG ON " & vbCrLf & _
            "( NAME = 'Live_Log'," & vbCrLf & _
            "FILENAME = N'" & LogFile & "'," & vbCrLf & _
            "SIZE = 5MB, " & vbCrLf & _
            "MAXSIZE = 25MB, " & vbCrLf & _
            "FILEGROWTH = 5MB )"

            oCon.Execute(SQL)

            oCon.Close()

            _Delay(5)

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        End Try
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
