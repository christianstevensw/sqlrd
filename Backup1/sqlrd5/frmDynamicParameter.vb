Public Class frmDynamicParameter
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As New clsMarsData
    Dim sOp As String
    Dim UserCancel As Boolean
    Dim sMode As String = "simple"
    Dim ForDynamic As Boolean = False

    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Dim showStar As Boolean = False
    Friend WithEvents stabSQL As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabAdvanced As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSimple As DevComponents.DotNetBar.SuperTabItem
    Public m_eventID As Integer = 0
    Dim m_inserter As frmInserter = New frmInserter(m_eventID)
    Public m_ParameterList As ArrayList
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Public Property m_showStar() As Boolean
        Get
            Return showStar
        End Get
        Set(ByVal value As Boolean)
            showStar = True
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents lsvWhere As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents optAnd As System.Windows.Forms.RadioButton
    Friend WithEvents optOr As System.Windows.Forms.RadioButton
    Friend WithEvents cmbWhere As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRemoveWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents grpSimple As System.Windows.Forms.GroupBox
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdParse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdvanced As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDynamicParameter))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.stabSQL = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.grpSimple = New System.Windows.Forms.GroupBox()
        Me.lsvWhere = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.optAnd = New System.Windows.Forms.RadioButton()
        Me.optOr = New System.Windows.Forms.RadioButton()
        Me.cmbWhere = New System.Windows.Forms.ComboBox()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.cmdRemoveWhere = New DevComponents.DotNetBar.ButtonX()
        Me.tabSimple = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.grpAdvanced = New System.Windows.Forms.GroupBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtQuery = New System.Windows.Forms.TextBox()
        Me.tabAdvanced = New DevComponents.DotNetBar.SuperTabItem()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.cmdConnect = New DevComponents.DotNetBar.ButtonX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdParse = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdvanced = New DevComponents.DotNetBar.ButtonX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.GroupBox1.SuspendLayout()
        CType(Me.stabSQL, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabSQL.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.grpSimple.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.stabSQL)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(471, 507)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'stabSQL
        '
        '
        '
        '
        '
        '
        '
        Me.stabSQL.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabSQL.ControlBox.MenuBox.Name = ""
        Me.stabSQL.ControlBox.Name = ""
        Me.stabSQL.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSQL.ControlBox.MenuBox, Me.stabSQL.ControlBox.CloseBox})
        Me.stabSQL.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabSQL.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabSQL.Location = New System.Drawing.Point(8, 236)
        Me.stabSQL.Name = "stabSQL"
        Me.stabSQL.ReorderTabsEnabled = True
        Me.stabSQL.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabSQL.SelectedTabIndex = 0
        Me.stabSQL.Size = New System.Drawing.Size(457, 264)
        Me.stabSQL.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabSQL.TabIndex = 51
        Me.stabSQL.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabSimple, Me.tabAdvanced})
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.grpSimple)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 26)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(457, 238)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabSimple
        '
        'grpSimple
        '
        Me.grpSimple.BackColor = System.Drawing.Color.Transparent
        Me.grpSimple.Controls.Add(Me.lsvWhere)
        Me.grpSimple.Controls.Add(Me.cmdAddWhere)
        Me.grpSimple.Controls.Add(Me.optAnd)
        Me.grpSimple.Controls.Add(Me.optOr)
        Me.grpSimple.Controls.Add(Me.cmbWhere)
        Me.grpSimple.Controls.Add(Me.Label6)
        Me.grpSimple.Controls.Add(Me.cmbOperator)
        Me.grpSimple.Controls.Add(Me.cmbValue)
        Me.grpSimple.Controls.Add(Me.cmdRemoveWhere)
        Me.grpSimple.Location = New System.Drawing.Point(3, 3)
        Me.grpSimple.Name = "grpSimple"
        Me.grpSimple.Size = New System.Drawing.Size(448, 230)
        Me.grpSimple.TabIndex = 2
        Me.grpSimple.TabStop = False
        '
        'lsvWhere
        '
        '
        '
        '
        Me.lsvWhere.Border.Class = "ListViewBorder"
        Me.lsvWhere.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvWhere.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvWhere.Location = New System.Drawing.Point(8, 88)
        Me.lsvWhere.Name = "lsvWhere"
        Me.lsvWhere.Size = New System.Drawing.Size(432, 134)
        Me.lsvWhere.TabIndex = 4
        Me.lsvWhere.UseCompatibleStateImageBehavior = False
        Me.lsvWhere.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Where"
        Me.ColumnHeader1.Width = 428
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(288, 56)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 3
        '
        'optAnd
        '
        Me.optAnd.Checked = True
        Me.optAnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAnd.Location = New System.Drawing.Point(184, 56)
        Me.optAnd.Name = "optAnd"
        Me.optAnd.Size = New System.Drawing.Size(48, 24)
        Me.optAnd.TabIndex = 6
        Me.optAnd.TabStop = True
        Me.optAnd.Text = "And"
        '
        'optOr
        '
        Me.optOr.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optOr.Location = New System.Drawing.Point(232, 56)
        Me.optOr.Name = "optOr"
        Me.optOr.Size = New System.Drawing.Size(40, 24)
        Me.optOr.TabIndex = 7
        Me.optOr.Text = "Or"
        '
        'cmbWhere
        '
        Me.cmbWhere.ItemHeight = 13
        Me.cmbWhere.Location = New System.Drawing.Point(8, 32)
        Me.cmbWhere.Name = "cmbWhere"
        Me.cmbWhere.Size = New System.Drawing.Size(160, 21)
        Me.cmbWhere.TabIndex = 0
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(168, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Only return records where"
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"<", "<=", "<>", "=", ">", ">=", "BEGINS WITH", "CONTAINS", "DOES NOT BEGIN WITH", "DOES NOT CONTAIN", "DOES NOT END WITH", "ENDS WITH", "IS EMPTY", "IS NOT EMPTY"})
        Me.cmbOperator.Location = New System.Drawing.Point(184, 32)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(88, 21)
        Me.cmbOperator.Sorted = True
        Me.cmbOperator.TabIndex = 1
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(288, 32)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(152, 21)
        Me.cmbValue.TabIndex = 2
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveWhere.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(128, 56)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 5
        '
        'tabSimple
        '
        Me.tabSimple.AttachedControl = Me.SuperTabControlPanel1
        Me.tabSimple.GlobalItem = False
        Me.tabSimple.Name = "tabSimple"
        Me.tabSimple.Text = "Simple"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.grpAdvanced)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(457, 264)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabAdvanced
        '
        'grpAdvanced
        '
        Me.grpAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.grpAdvanced.Controls.Add(Me.Label2)
        Me.grpAdvanced.Controls.Add(Me.txtQuery)
        Me.grpAdvanced.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grpAdvanced.Location = New System.Drawing.Point(0, 0)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(457, 264)
        Me.grpAdvanced.TabIndex = 1
        Me.grpAdvanced.TabStop = False
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "SQL Query"
        '
        'txtQuery
        '
        Me.txtQuery.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtQuery.Location = New System.Drawing.Point(3, 64)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(451, 197)
        Me.txtQuery.TabIndex = 0
        Me.txtQuery.Tag = "memo"
        '
        'tabAdvanced
        '
        Me.tabAdvanced.AttachedControl = Me.SuperTabControlPanel2
        Me.tabAdvanced.GlobalItem = False
        Me.tabAdvanced.Name = "tabAdvanced"
        Me.tabAdvanced.Text = "Advanced"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmbTable)
        Me.GroupBox2.Controls.Add(Me.cmdConnect)
        Me.GroupBox2.Controls.Add(Me.UcDSN)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(457, 214)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(16, 184)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(192, 21)
        Me.cmbTable.TabIndex = 2
        '
        'cmdConnect
        '
        Me.cmdConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdConnect.BackColor = System.Drawing.Color.Transparent
        Me.cmdConnect.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(197, 139)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 23)
        Me.cmdConnect.TabIndex = 1
        Me.cmdConnect.Text = "Connect"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(432, 120)
        Me.UcDSN.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(232, 184)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(192, 21)
        Me.cmbColumn.TabIndex = 3
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 166)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(320, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Select the table and the column that holds the required values"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.Enabled = False
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(485, 8)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 25)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(485, 40)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdParse
        '
        Me.cmdParse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdParse.BackColor = System.Drawing.Color.Transparent
        Me.cmdParse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdParse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParse.Location = New System.Drawing.Point(404, 513)
        Me.cmdParse.Name = "cmdParse"
        Me.cmdParse.Size = New System.Drawing.Size(75, 23)
        Me.cmdParse.TabIndex = 1
        Me.cmdParse.Text = "Parse"
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdvanced.BackColor = System.Drawing.Color.Transparent
        Me.cmdAdvanced.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdvanced.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdvanced.Location = New System.Drawing.Point(485, 72)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.Size = New System.Drawing.Size(75, 25)
        Me.cmdAdvanced.TabIndex = 2
        Me.cmdAdvanced.Text = "&Advanced"
        Me.cmdAdvanced.Visible = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'frmDynamicParameter
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(568, 539)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdParse)
        Me.Controls.Add(Me.cmdAdvanced)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmDynamicParameter"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Get values from database"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.stabSQL, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabSQL.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.grpSimple.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click

        If UcDSN.Validate = True Then
            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text)
            cmdParse.Enabled = True
            cmbTable.Enabled = True

            If Me.m_showStar = False Then cmbColumn.Enabled = True
        Else
            cmdParse.Enabled = False
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            _ErrorHandle("Failed to connect to selected datasource", -2147636225, "ucDSN.Validate(frmDynamicParameter.cmdConnect_Click)", 372)
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        cmbColumn.Items.Clear()
        cmbWhere.Items.Clear()

        If showStar = False Then
            oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, _
            cmbTable.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        Else
            cmbColumn.Items.Add("*")
            cmbColumn.SelectedIndex = 0
        End If

        oData.GetColumns(cmbWhere, UcDSN.cmbDSN.Text, _
                cmbTable.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        SetError(cmbTable, "")
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        SetError(cmbOperator, "")

        If cmbOperator.Text.ToLower = "is empty" Or cmbOperator.Text.ToLower = "is not empty" Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim sKey As String = ""
        Dim oItem As clsMyList
        Dim nType As Integer

        If cmbWhere.Text = "" Then
            SetError(cmbWhere, "Please select the column name")
            Return
        ElseIf cmbOperator.Text = "" Then
            SetError(cmbOperator, "Please select the operator")
            Return
        End If

        If lsvWhere.Items.Count > 0 Then
            sKey = sOp
        End If

        Select Case cmbOperator.Text.ToLower
            Case "begins with"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " LIKE '" & cmbValue.Text & "%'")
            Case "ends with"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " LIKE '%" & cmbValue.Text & "'")
            Case "contains"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " LIKE '%" & cmbValue.Text & "%'")
            Case "does not contain"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & cmbValue.Text & "%'")
            Case "does not begin with"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '" & cmbValue.Text & "%'")
            Case "does not end with"
                lsvWhere.Items.Add(sKey & _
                cmbTable.Text & "." & cmbWhere.Text & _
                " NOT LIKE '%" & cmbValue.Text & "'")
            Case "is empty"

                lsvWhere.Items.Add(sKey & _
                "(" & cmbTable.Text & "." & cmbWhere.Text & _
                " IS NULL OR " & _
                cmbTable.Text & "." & cmbWhere.Text & " ='')")
            Case "is not empty"
                cmbValue.Enabled = False
                lsvWhere.Items.Add(sKey & _
                "(" & cmbTable.Text & "." & cmbWhere.Text & _
                " IS NOT NULL AND " & _
                cmbTable.Text & "." & cmbWhere.Text & " <> '')")
            Case Else
                Try
                    oItem = cmbWhere.Items(cmbWhere.SelectedIndex)

                    nType = oItem.ItemData

                    If nType = ADODB.DataTypeEnum.adBSTR Or nType = ADODB.DataTypeEnum.adChar Or nType = ADODB.DataTypeEnum.adLongVarChar _
                           Or nType = ADODB.DataTypeEnum.adLongVarWChar Or nType = ADODB.DataTypeEnum.adVarChar Or nType = ADODB.DataTypeEnum.adVarWChar _
                           Or nType = ADODB.DataTypeEnum.adWChar Then

                        lsvWhere.Items.Add(sKey & cmbWhere.Text & cmbOperator.Text & "'" & cmbValue.Text & "'")
                    Else
                        lsvWhere.Items.Add(sKey & cmbWhere.Text & cmbOperator.Text & cmbValue.Text)

                    End If
                Catch ex As Exception
                    _ErrorHandle("Please select a valid column for your selection criteria", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Select a column from the dropdown list.")
                    Return
                End Try
        End Select

        SetError(lsvWhere, "")

        Dim lsv As ListViewItem = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub optAnd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAnd.CheckedChanged
        sOp = " AND "
    End Sub

    Private Sub optOr_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOr.CheckedChanged
        sOp = " OR "
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvWhere.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvWhere.SelectedItems(0)

        lsvWhere.Items.Remove(lsv)

        If lsvWhere.Items.Count = 0 Then Return

        lsv = lsvWhere.Items(0)

        If lsv.Text.ToLower.StartsWith(" and ") Then
            lsv.Text = lsv.Text.Substring(5, lsv.Text.Length - 5)
        ElseIf lsv.Text.ToLower.StartsWith(" or ") Then
            lsv.Text = lsv.Text.Substring(4, lsv.Text.Length - 4)
        End If
    End Sub

    Private Sub frmDynamicParameter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        txtQuery.ContextMenu = Me.mnuInserter
        cmbValue.ContextMenu = Me.mnuInserter

        setupForDragAndDrop(txtQuery)
        setupForDragAndDrop(cmbValue)

        m_inserter.GetConstants(Me)
    End Sub

    Public Function ReturnSQLQuery(Optional ByVal sCon As String = "", _
    Optional ByVal sQuery As String = "") As String()

        If sCon.Length > 0 Then
            UcDSN.cmbDSN.Text = sCon.Split("|")(0)
            UcDSN.txtUserID.Text = sCon.Split("|")(1)
            UcDSN.txtPassword.Text = sCon.Split("|")(2)
        End If

        If sQuery.Length > 0 Then
            txtQuery.Text = sQuery
            grpAdvanced.BringToFront()
            cmdAdvanced.Text = "Simple"
            sMode = "Advanced"
            stabSQL.SelectedTab = tabAdvanced
            txtQuery.Text = sQuery
        End If

        Me.ShowDialog()


        If UserCancel = True Then Return Nothing

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|"

        Dim sVals(1) As String

        sVals(0) = sCon
        sVals(1) = txtQuery.Text

        Return sVals
    End Function

    Public Function AddParameterDynamicQuery(ByVal sKey As String, ByVal scheduleType As String, _
    Optional ByVal nReportID As Integer = 99999, Optional ByVal nPackID As Integer = 99999) As String

        ForDynamic = True

        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sCon As String

        If scheduleType.ToLower = "report" Then
            SQL = "SELECT * FROM DynamicAttr WHERE ReportID =" & nReportID
        Else
            SQL = "SELECT * FROM DynamicAttr WHERE PackID =" & nPackID
        End If

        SQL &= " AND KeyParameter = '" & SQLPrepare(sKey) & "'"

        'SQL = "SELECT * FROM DynamicAttr WHERE ReportID =" & nReportID & " AND " & _
        '"PackID = " & nPackID & _
        '" AND KeyParameter ='" & SQLPrepare(sKey) & "'"

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                sCon = oRs("constring").Value

                UcDSN.cmbDSN.Text = sCon.Split("|")(0)
                UcDSN.txtUserID.Text = sCon.Split("|")(1)
                UcDSN.txtPassword.Text = sCon.Split("|")(2)

                txtQuery.Text = oRs("query").Value

                grpAdvanced.BringToFront()
                cmdAdvanced.Text = "Simple"
                sMode = "advanced"
                stabSQL.SelectedTab = tabAdvanced
            End If
        Catch ex As Exception

        End Try

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty


        Dim sCols As String
        Dim sVals As String
        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|"

        If scheduleType.ToLower = "report" Then
            clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID =" & nReportID)
            nPackID = 0
        Else
            clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE PackID=" & nPackID)
            nReportID = 0
        End If

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString,Query,ReportID,PackID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(sKey) & "'," & _
        "'" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
        "'" & SQLPrepare(sCon) & "'," & _
        "'" & SQLPrepare(txtQuery.Text) & "'," & _
        nReportID & "," & _
        nPackID

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Return "[" & cmbTable.Text & "." & cmbColumn.Text & "]"
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If ForDynamic = True Then
            If cmbTable.Text.Length = 0 Then
                SetError(cmbTable, "Please select the table that holds the key column")
                cmbTable.Focus()
                Return
            ElseIf cmbColumn.Text.Length = 0 Then
                SetError(cmbColumn, "Please select the key column")
                cmbColumn.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Private Sub cmdParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParse.Click


        If sMode = "simple" Or txtQuery.Text.Length = 0 Then
            Dim start As String

            If cmbColumn.Text = "*" Then
                start = "SELECT "
            Else
                start = "SELECT DISTINCT "
            End If

            txtQuery.Text = start & cmbTable.Text & "." & _
            cmbColumn.Text & " FROM " & cmbTable.Text

            If lsvWhere.Items.Count > 0 Then

                txtQuery.Text &= " WHERE "

                For Each oitem As ListViewItem In lsvWhere.Items
                    txtQuery.Text &= oitem.Text
                Next
            End If
        End If

        If txtQuery.Text.Contains("<[d") Or txtQuery.Text.Contains("<[p") Or txtQuery.Text.Contains("<[e") Or _
               txtQuery.Text.Contains("<[r") Or txtQuery.Text.Contains("<[c") Or txtQuery.Text.ToLower.Contains("<[m]key parameter value>") Then
            MessageBox.Show("SQL parsed successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = True
            Return
        End If


        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        If txtQuery.Text.ToLower.IndexOf("where") > -1 Then
            Dim whereEnd As Integer = txtQuery.Text.ToLower.IndexOf("where") + 6

            If txtQuery.Text.Substring(whereEnd, 1) <> "(" Then

                txtQuery.Text = txtQuery.Text.Insert(whereEnd, "(")

                If txtQuery.Text.ToLower.Contains("group by") Then
                    Dim whereGroup As Integer = txtQuery.Text.ToLower.IndexOf("group by")

                    txtQuery.Text = txtQuery.Text.Insert(whereGroup, ") ")
                ElseIf txtQuery.Text.ToLower.Contains("order by") Then
                    Dim whereOrder As Integer = txtQuery.Text.ToLower.IndexOf("order by")

                    txtQuery.Text = txtQuery.Text.Insert(whereOrder, ") ")
                Else
                    txtQuery.Text &= ")"
                End If

            End If
        End If

        Try
            Dim sql As String = clsMarsParser.Parser.ParseString(txtQuery.Text, False, True)

            oCon.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            oRs.Open(sql, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, ADODB.CommandTypeEnum.adCmdText)

            Dim oRes As DialogResult = MessageBox.Show("SQL parsed successfully! View Results?", _
            Application.ProductName, MessageBoxButtons.YesNo, _
            MessageBoxIcon.Question)

            If oRes = Windows.Forms.DialogResult.Yes Then
                Dim oResult As New frmDBResults

                oResult._ShowResults(oRs)
            End If

            cmdOK.Enabled = True

            Try
                If cmbColumn.Text.Length = 0 And cmbTable.Text.Length = 0 Then
                    Dim start As Integer
                    Dim ends As Integer

                    If txtQuery.Text.ToLower.IndexOf("distinct") > -1 Then
                        start = txtQuery.Text.ToLower.IndexOf("distinct") + "distinct".Length
                    Else
                        start = txtQuery.Text.ToLower.IndexOf("select") + "select".Length
                    End If

                    ends = txtQuery.Text.ToLower.IndexOf("from")

                    Dim fieldInfo As String

                    fieldInfo = txtQuery.Text.Substring(start, ends - start).Trim

                    If fieldInfo.IndexOf(".") > -1 Then
                        cmbTable.Text = fieldInfo.Split(".")(0)

                        If cmbTable.Text.Contains(" ") And cmbTable.Text.StartsWith("[") = False Then
                            Dim spaceCount As Integer = cmbTable.Text.Split(" ").GetUpperBound(0)

                            cmbTable.Text = cmbTable.Text.Split(" ")(spaceCount)
                        End If

                        cmbTable.Text = cmbTable.Text.Replace("[", "").Replace("]", "")

                        cmbColumn.Text = fieldInfo.Split(".")(1)

                        cmbColumn.Text = cmbColumn.Text.Replace("[", "").Replace("]", "")
                    End If
                End If
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            cmdOK.Enabled = True
        End Try

    End Sub


    Private Sub cmdAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdvanced.Click
        If sMode = "simple" Then
            grpAdvanced.BringToFront()
            cmdAdvanced.Text = "&Simple"

            txtQuery.Text = "SELECT DISTINCT " & cmbTable.Text & "." & _
            cmbColumn.Text & " FROM " & cmbTable.Text

            If lsvWhere.Items.Count > 0 Then

                txtQuery.Text &= " WHERE "

                For Each oitem As ListViewItem In lsvWhere.Items
                    txtQuery.Text &= oitem.Text
                Next
            End If

            sMode = "advanced"
        Else
            grpSimple.BringToFront()
            cmdAdvanced.Text = "&Advanced"
            sMode = "simple"
        End If
    End Sub

    Private Sub cmbWhere_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWhere.SelectedIndexChanged
        oData.ReturnDistinctValues(cmbValue, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text, cmbWhere.Text, cmbTable.Text)
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn.SelectedIndexChanged
        SetError(cmbColumn, "")
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.Cut()
        ElseIf TypeOf ctrl Is ComboBox Then
            Dim o As ComboBox = CType(ctrl, ComboBox)

            Clipboard.SetText(o.SelectedText)
            o.SelectedText = ""
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.Copy()
        ElseIf TypeOf ctrl Is ComboBox Then
            Dim o As ComboBox = CType(ctrl, ComboBox)

            Clipboard.SetText(o.SelectedText)
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.Paste()
        ElseIf TypeOf ctrl Is ComboBox Then
            Dim o As ComboBox = CType(ctrl, ComboBox)

            o.SelectedText = Clipboard.GetText
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.SelectedText = ""
        ElseIf TypeOf ctrl Is ComboBox Then
            Dim o As ComboBox = CType(ctrl, ComboBox)

            o.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.SelectAll()
        ElseIf TypeOf ctrl Is ComboBox Then
            Dim o As ComboBox = CType(ctrl, ComboBox)

            o.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim inserter As frmInserter = New frmInserter(Me.m_eventID)
        inserter.m_EventID = Me.m_eventID
        inserter.GetConstants(Me)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next
        Dim ctrl As Control = Me.ActiveControl

        If TypeOf ctrl Is TextBox Then
            Dim o As TextBox = CType(ctrl, TextBox)

            o.Undo()
        End If
    End Sub

    Private Sub stabSQL_SelectedTabChanged(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabSQL.SelectedTabChanged
        Select Case stabSQL.SelectedTab.Text
            Case "Simple"
                If txtQuery.Text <> "" Then
                    Dim tmp As String = txtQuery.Text
                    Dim proceed As DialogResult = MessageBox.Show("Changing to Simple mode will overwrite your current query. Proceed?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                    If proceed = Windows.Forms.DialogResult.No Then
                        txtQuery.Text = tmp
                        stabSQL.SelectedTab = tabAdvanced
                        'e.Cancel = True
                        Return
                    End If
                End If

                grpSimple.BringToFront()
                cmdAdvanced.Text = "&Advanced"
                sMode = "simple"
            Case "Advanced"
                grpAdvanced.BringToFront()
                cmdAdvanced.Text = "&Simple"

                If sMode <> "advanced" Then
                    txtQuery.Text = "SELECT DISTINCT " & cmbTable.Text & "." & _
                    cmbColumn.Text & " FROM " & cmbTable.Text

                    If lsvWhere.Items.Count > 0 Then

                        txtQuery.Text &= " WHERE "

                        For Each oitem As ListViewItem In lsvWhere.Items
                            txtQuery.Text &= oitem.Text
                        Next
                    End If
                End If

                sMode = "advanced"
        End Select
    End Sub
End Class
