Public Class frmAutoProp
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim ScheduleID As Integer
    Friend WithEvents tabProperties As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabHistory As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ucUpdate As sqlrd.ucSchedule
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents scheduleHistory As sqlrd.ucScheduleHistory
    Dim OkayToClose As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents txtID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents imgTools As System.Windows.Forms.ImageList
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents imgAuto As System.Windows.Forms.ImageList
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoProp))
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.imgAuto = New System.Windows.Forms.ImageList(Me.components)
        Me.imgTools = New System.Windows.Forms.ImageList(Me.components)
        Me.UcTasks = New sqlrd.ucTasks()
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tabProperties = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.scheduleHistory = New sqlrd.ucScheduleHistory()
        Me.tabHistory = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ucUpdate = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(600, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(519, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(438, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtDesc
        '
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtDesc, 2)
        Me.txtDesc.Location = New System.Drawing.Point(134, 57)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(344, 129)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(3, 57)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 16)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 192)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 16)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Keywords (Optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(134, 192)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(344, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(484, 3)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(134, 3)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(344, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Location"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(134, 30)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(344, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Schedule Name"
        '
        'txtID
        '
        '
        '
        '
        Me.txtID.Border.Class = "TextBoxBorder"
        Me.txtID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtID.Location = New System.Drawing.Point(27, 404)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 21)
        Me.txtID.TabIndex = 1
        Me.txtID.Visible = False
        '
        'imgAuto
        '
        Me.imgAuto.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgAuto.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgAuto.TransparentColor = System.Drawing.Color.Transparent
        '
        'imgTools
        '
        Me.imgTools.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgTools.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgTools.TransparentColor = System.Drawing.Color.Transparent
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = False
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(584, 367)
        Me.UcTasks.TabIndex = 0
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'tabProperties
        '
        '
        '
        '
        '
        '
        '
        Me.tabProperties.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.tabProperties.ControlBox.MenuBox.Name = ""
        Me.tabProperties.ControlBox.Name = ""
        Me.tabProperties.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabProperties.ControlBox.MenuBox, Me.tabProperties.ControlBox.CloseBox})
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel2)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel1)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel3)
        Me.tabProperties.Controls.Add(Me.SuperTabControlPanel4)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.ReorderTabsEnabled = True
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(678, 367)
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.tabProperties.TabIndex = 51
        Me.tabProperties.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabHistory, Me.tabTasks})
        Me.tabProperties.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.tabProperties.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.scheduleHistory)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(584, 367)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabHistory
        '
        'scheduleHistory
        '
        Me.scheduleHistory.BackColor = System.Drawing.Color.Transparent
        Me.scheduleHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.scheduleHistory.Location = New System.Drawing.Point(0, 0)
        Me.scheduleHistory.m_filter = False
        Me.scheduleHistory.m_historyLimit = 0
        Me.scheduleHistory.m_objectID = 0
        Me.scheduleHistory.m_scheduleType = sqlrd.clsMarsScheduler.enScheduleType.NONE
        Me.scheduleHistory.m_showControls = False
        Me.scheduleHistory.m_showSchedulesList = False
        Me.scheduleHistory.m_sortOrder = " ASC "
        Me.scheduleHistory.Name = "scheduleHistory"
        Me.scheduleHistory.Size = New System.Drawing.Size(584, 367)
        Me.scheduleHistory.TabIndex = 0
        '
        'tabHistory
        '
        Me.tabHistory.AttachedControl = Me.SuperTabControlPanel3
        Me.tabHistory.GlobalItem = False
        Me.tabHistory.Image = CType(resources.GetObject("tabHistory.Image"), System.Drawing.Image)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.Text = "History"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.TableLayoutPanel1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(87, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(591, 367)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.txtKeyWord, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDesc, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFolder, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdLoc, 2, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(553, 218)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.UcTasks)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(584, 367)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel4
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Tasks"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.ucUpdate)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(94, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(584, 367)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'ucUpdate
        '
        Me.ucUpdate.BackColor = System.Drawing.Color.Transparent
        Me.ucUpdate.Dock = System.Windows.Forms.DockStyle.Top
        Me.ucUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucUpdate.Location = New System.Drawing.Point(0, 0)
        Me.ucUpdate.m_collaborationServerID = 0
        Me.ucUpdate.m_nextRun = "2011-05-10 10:40:01"
        Me.ucUpdate.m_RepeatUnit = ""
        Me.ucUpdate.mode = sqlrd.ucSchedule.modeEnum.EDIT
        Me.ucUpdate.Name = "ucUpdate"
        Me.ucUpdate.scheduleID = 0
        Me.ucUpdate.scheduleStatus = True
        Me.ucUpdate.sFrequency = "Daily"
        Me.ucUpdate.Size = New System.Drawing.Size(584, 364)
        Me.ucUpdate.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 370)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(678, 33)
        Me.FlowLayoutPanel3.TabIndex = 52
        '
        'frmAutoProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(678, 403)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.txtID)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAutoProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Automation Schedule Properties"
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAutoProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

    End Sub

    Public Sub EditSchedule(ByVal nAutoID As Integer)

        If gnEdition < gEdition.GOLD Then
            _NeedUpgrade(gEdition.GOLD, Me, "Automation Schedules")
            Return
        End If

        Dim oData As New clsMarsData
        Dim SQL As String

        UcTasks.ShowAfterType = False
        UcTasks.oAuto = True
        UcTasks.tvTasks.ColumnsVisible = False

        SQL = "SELECT * FROM AutomationAttr WHERE AutoID = " & nAutoID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("autoname").Value

            Me.Text = "Automation Schedule Properties - " & txtName.Text

            txtFolder.Tag = oRs("parent").Value
            txtID.Text = oRs("autoid").Value
            oRs.Close()
        End If

        SQL = "SELECT FolderName FROM Folders WHERE FolderID =" & txtFolder.Tag

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtFolder.Text = oRs.Fields(0).Value
        End If

        oRs.Close()

        ucUpdate.mode = ucSchedule.modeEnum.EDIT
        ucUpdate.loadScheduleInfo(txtID.Text, clsMarsScheduler.enScheduleType.AUTOMATION, txtDesc, txtKeyWord, UcTasks)

        Me.ShowDialog()
    End Sub


    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        If UcTasks.tvTasks.Nodes.Count = 0 Then
            setError(UcTasks.tvTasks, "Please add a custom action to the schedule")
            OkayToClose = False
            tabProperties.SelectedTabIndex = 3
            Return
        ElseIf txtName.Text.Length = 0 Then
            setError(txtName, "Please enter a name for this schedule")
            OkayToClose = False
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf txtFolder.Text.Length = 0 Then
            setError(txtFolder, "Please select the parent folder for the schedule")
            OkayToClose = False
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text) = False Then
            setError(txtName, "An Automation schedule with this name already exists in this folder. Please use a different name")
            OkayToClose = False
            txtName.Focus()
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf Me.ucUpdate.isAllDataValid() = False Then
            OkayToClose = False
            Return
        End If

        Dim oData As New clsMarsData

        Dim SQL As String

        SQL = "UPDATE AutomationAttr SET " & _
            "AutoName = '" & SQLPrepare(txtName.Text) & "'," & _
            "Parent = " & txtFolder.Tag & " WHERE AutoID = " & txtID.Text

        clsMarsData.WriteData(SQL)

        Dim nRepeat As String = "0"

        Try
            nRepeat = Convert.ToString(ucUpdate.txtRepeatInterval.Value).Replace(",", ".")
        Catch ex As Exception
            ''console.writeline(ex.Message)
            nRepeat = "0"
        End Try

        ucUpdate.updateSchedule(txtID.Text, clsMarsScheduler.enScheduleType.AUTOMATION, txtDesc.Text, txtKeyWord.Text)

        setAndShowMessageOnControl(cmdApply, "Saved!", "", "Your schedule has been updated successfully")

        OkayToClose = True


    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)

        If OkayToClose = False Then Return

        Close()

        Dim oUI As New clsMarsUI



        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub



    Private Sub tabProperties_SelectedTabChanged(sender As System.Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        If e.NewValue.Text = "History" Then
            With scheduleHistory
                .m_objectID = txtID.Text
                .m_scheduleType = clsMarsScheduler.enScheduleType.AUTOMATION
                .m_showControls = True
                .DrawScheduleHistory()
            End With

        End If
    End Sub
End Class
