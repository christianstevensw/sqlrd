Imports Microsoft.Win32
Imports System.IO
Imports System.ServiceProcess
Imports ActiveDs
Imports System.Net.Sockets
Imports System.Windows.Forms

Friend Class frmOptions
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oReg As RegistryKey = Registry.LocalMachine
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Dim WithEvents oSMTP As New Quiksoft.EasyMail.SMTP.SMTP
    Dim UpdateAutoCompact As Boolean = False
    Dim ShowCompactMsg As Boolean = False
    Dim isLoaded As Boolean = False
    Dim skipValidation As Boolean = False
    Dim m_tempSMTPValues As Hashtable
    Dim threadWarning As Boolean = False
    Dim keepTemp As String = ""
    Friend WithEvents chkUniversal As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label17 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPollInt As System.Windows.Forms.NumericUpDown
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmbSMTPTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRefresh As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkDTRefresh As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label25 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents grpGroupwise As System.Windows.Forms.GroupBox
    Friend WithEvents Label28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtgwUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label41 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label45 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label46 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtgwPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwProxy As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwPOIP As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwPOPort As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtThreadCount As System.Windows.Forms.NumericUpDown
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkUNC As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkOutofProcess As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPollIntEB As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label47 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label48 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPriorityEB As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents chkUseRelTime As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label49 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtSMSInit As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label50 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label51 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkDisableCRParsing As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label52 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnSentMessages As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtSentMessages As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnMAPIMore As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkDelayRestart As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label53 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDelayBy As System.Windows.Forms.NumericUpDown
    Dim sCrystalKey As String = ""
    Friend WithEvents btnSMTPAdvanced As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkConvertToMsg As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents chkShowHiddenParameters As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpThreading As System.Windows.Forms.GroupBox
    Friend WithEvents Label54 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtThreadInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label55 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label56 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNumConnections As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkPrecheckconditions As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents optFileLastModified As System.Windows.Forms.RadioButton
    Friend WithEvents optFileCreated As System.Windows.Forms.RadioButton
    Friend WithEvents Label57 As DevComponents.DotNetBar.LabelX
    Friend WithEvents lsvParameterDefaults As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents txtParameterDefaultValue As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtDefaultParameterName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label59 As DevComponents.DotNetBar.LabelX
    Friend WithEvents optParameterSpecifiedValue As System.Windows.Forms.RadioButton
    Friend WithEvents optParameterNull As System.Windows.Forms.RadioButton
    Friend WithEvents optParameterDefault As System.Windows.Forms.RadioButton
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label58 As DevComponents.DotNetBar.LabelX
    Friend WithEvents optParameterAllValues As System.Windows.Forms.RadioButton
    Friend WithEvents btnRemoveParameterDefault As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddParameterDefault As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents Button1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDBLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label20 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDefSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDefaultEmail As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSig As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdSpell As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label31 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtDefAttach As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents UcLogin As sqlrd.ucLoginDetails
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvServers As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdRptLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtReportLoc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkAllowAdminToChangeValues As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkShareUserDefaults As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnBlackout As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkBlackout As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents stabOptions As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel11 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDefaultParameters As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel10 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel9 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabAuditTrail As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel8 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSystemPaths As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDefaults As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabHouseKeeping As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabScheduler As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabMessaging As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TabControl3 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel12 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel13 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents grpSMSByDevice As System.Windows.Forms.GroupBox
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents optSMSByRedOxygen As System.Windows.Forms.RadioButton
    Friend WithEvents optSMSByDevice As System.Windows.Forms.RadioButton
    Friend WithEvents grpSMSByRedOxygen As System.Windows.Forms.GroupBox
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtROAccountID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtROEmail As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtROPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents TabControl4 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel14 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel15 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControl2 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel18 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem8 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel17 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem7 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel16 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem6 As DevComponents.DotNetBar.TabItem
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkAutoclose As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAutoclose As DevComponents.Editors.IntegerInput
    Friend WithEvents chkUseAsBackupScheduler As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAutoEmbedImages As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkAutoUpdates As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbKeepTempUnit As System.Windows.Forms.ComboBox
    Friend WithEvents txtKeepTemps As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelX6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkLoadSSRSParametersAutomatically As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents SuperTabControlPanel12 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents btnDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddCloud As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddDropbox As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents lsvCloud As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Dim m_scheduleStatus As clsServiceController.enum_svcStatus
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpSMTP As System.Windows.Forms.GroupBox
    Friend WithEvents grpMAPI As System.Windows.Forms.GroupBox
    Friend WithEvents cmdMailTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdClearSettings As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkThreading As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtErrorAlertWho As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optErrorLog As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents optErrorMail As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtCustomerNo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMTPUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMTPPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMTPServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMTPSenderAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSMTPSenderName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMAPIPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbMAPIProfile As System.Windows.Forms.ComboBox
    Friend WithEvents chkEmailRetry As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbMailType As System.Windows.Forms.ComboBox
    Friend WithEvents cmdServiceApply As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtWinDomain As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtWinUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtWinPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optNoScheduling As System.Windows.Forms.RadioButton
    Friend WithEvents optBackgroundApp As System.Windows.Forms.RadioButton
    Friend WithEvents optNTService As System.Windows.Forms.RadioButton
    Friend WithEvents cmdServiceStart As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRegister As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvComponents As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents grpNTService As System.Windows.Forms.GroupBox
    Friend WithEvents grpServices As System.Windows.Forms.GroupBox
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents txtArchive As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkArchive As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpCRDMail As System.Windows.Forms.GroupBox
    Friend WithEvents txtCRDSender As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCRDAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdActivate As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents chkCheckService As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdSMTPMore As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkCompact As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents dtAutoCompact As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label32 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkAutoCompact As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbSMSDevice As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label35 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label36 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label37 As DevComponents.DotNetBar.LabelX

    Friend WithEvents Label40 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMSNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbSMSProtocol As System.Windows.Forms.ComboBox
    Friend WithEvents txtSMSPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optUseGSM As System.Windows.Forms.RadioButton
    Friend WithEvents cmbSMSFormat As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSMSSpeed As System.Windows.Forms.ComboBox
    Friend WithEvents optUseSMSC As System.Windows.Forms.RadioButton
    Friend WithEvents txtSMSSender As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkUnicode As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpSMSC As System.Windows.Forms.GroupBox
    Friend WithEvents optErrorSMS As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdAlertWho As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAlertSMS As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtAlertSMS As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents Label39 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbEncoding As System.Windows.Forms.ComboBox
    Friend WithEvents txtReportCache As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label42 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCachedData As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label43 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSnapShots As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdReportCache As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCachedData As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdSnapshots As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTempFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdTempFolder As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label38 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvPaths As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label44 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ucDSN As sqlrd.ucDSN
    Friend WithEvents chkAudit As DevComponents.DotNetBar.Controls.CheckBoxX


    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Collaboration Data Objects"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Compression Support"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Extended MAPI Support"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"MAPI Profiles Enumerator"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Me.cmdApply = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.chkAllowAdminToChangeValues = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Button1 = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox18 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label59 = New DevComponents.DotNetBar.LabelX()
        Me.txtParameterDefaultValue = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optParameterSpecifiedValue = New System.Windows.Forms.RadioButton()
        Me.optParameterNull = New System.Windows.Forms.RadioButton()
        Me.optParameterDefault = New System.Windows.Forms.RadioButton()
        Me.optParameterAllValues = New System.Windows.Forms.RadioButton()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label58 = New DevComponents.DotNetBar.LabelX()
        Me.txtDefaultParameterName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnRemoveParameterDefault = New DevComponents.DotNetBar.ButtonX()
        Me.lsvParameterDefaults = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnAddParameterDefault = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox12 = New System.Windows.Forms.GroupBox()
        Me.Label44 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.chkAudit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox14 = New System.Windows.Forms.GroupBox()
        Me.chkConvertToMsg = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSentMessages = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnSentMessages = New DevComponents.DotNetBar.ButtonX()
        Me.Label52 = New DevComponents.DotNetBar.LabelX()
        Me.cmdReportCache = New DevComponents.DotNetBar.ButtonX()
        Me.txtReportCache = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtTempFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdTempFolder = New DevComponents.DotNetBar.ButtonX()
        Me.Label42 = New DevComponents.DotNetBar.LabelX()
        Me.txtCachedData = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSnapshots = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCachedData = New DevComponents.DotNetBar.ButtonX()
        Me.txtSnapShots = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label17 = New DevComponents.DotNetBar.LabelX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.Label43 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.cmdDBLoc = New DevComponents.DotNetBar.ButtonX()
        Me.Label20 = New DevComponents.DotNetBar.LabelX()
        Me.txtDefSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label18 = New DevComponents.DotNetBar.LabelX()
        Me.txtDefaultEmail = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtSig = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSpell = New DevComponents.DotNetBar.ButtonX()
        Me.Label31 = New DevComponents.DotNetBar.LabelX()
        Me.txtDefAttach = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lsvServers = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.GroupBox13 = New System.Windows.Forms.GroupBox()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.cmdRptLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtReportLoc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.lsvComponents = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdRegister = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.chkArchive = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label22 = New DevComponents.DotNetBar.LabelX()
        Me.txtArchive = New System.Windows.Forms.NumericUpDown()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.chkAutoCompact = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.dtAutoCompact = New System.Windows.Forms.DateTimePicker()
        Me.Label32 = New DevComponents.DotNetBar.LabelX()
        Me.optFileLastModified = New System.Windows.Forms.RadioButton()
        Me.optFileCreated = New System.Windows.Forms.RadioButton()
        Me.Label57 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.lsvPaths = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.grpThreading = New System.Windows.Forms.GroupBox()
        Me.txtThreadInterval = New System.Windows.Forms.NumericUpDown()
        Me.Label54 = New DevComponents.DotNetBar.LabelX()
        Me.txtThreadCount = New System.Windows.Forms.NumericUpDown()
        Me.Label55 = New DevComponents.DotNetBar.LabelX()
        Me.Label21 = New DevComponents.DotNetBar.LabelX()
        Me.chkThreading = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox16 = New System.Windows.Forms.GroupBox()
        Me.Label56 = New DevComponents.DotNetBar.LabelX()
        Me.txtNumConnections = New System.Windows.Forms.NumericUpDown()
        Me.chkPrecheckconditions = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.cmbPollIntEB = New System.Windows.Forms.NumericUpDown()
        Me.cmbPriority = New System.Windows.Forms.ComboBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label25 = New DevComponents.DotNetBar.LabelX()
        Me.cmbPollInt = New System.Windows.Forms.NumericUpDown()
        Me.Label47 = New DevComponents.DotNetBar.LabelX()
        Me.Label48 = New DevComponents.DotNetBar.LabelX()
        Me.cmbPriorityEB = New System.Windows.Forms.ComboBox()
        Me.chkUniversal = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnBlackout = New DevComponents.DotNetBar.ButtonX()
        Me.chkBlackout = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label53 = New DevComponents.DotNetBar.LabelX()
        Me.txtDelayBy = New System.Windows.Forms.NumericUpDown()
        Me.chkDelayRestart = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkCheckService = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdServiceApply = New DevComponents.DotNetBar.ButtonX()
        Me.grpServices = New System.Windows.Forms.GroupBox()
        Me.chkUseAsBackupScheduler = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpNTService = New System.Windows.Forms.GroupBox()
        Me.txtWinDomain = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.txtWinUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtWinPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.optNoScheduling = New System.Windows.Forms.RadioButton()
        Me.optBackgroundApp = New System.Windows.Forms.RadioButton()
        Me.optNTService = New System.Windows.Forms.RadioButton()
        Me.cmdServiceStart = New DevComponents.DotNetBar.ButtonX()
        Me.grpSMTP = New System.Windows.Forms.GroupBox()
        Me.btnSMTPAdvanced = New DevComponents.DotNetBar.ButtonX()
        Me.txtSMTPPort = New System.Windows.Forms.NumericUpDown()
        Me.cmbSMTPTimeout = New System.Windows.Forms.NumericUpDown()
        Me.cmdSMTPMore = New DevComponents.DotNetBar.ButtonX()
        Me.Label19 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label49 = New DevComponents.DotNetBar.LabelX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPSenderAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPSenderName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpMAPI = New System.Windows.Forms.GroupBox()
        Me.btnMAPIMore = New DevComponents.DotNetBar.ButtonX()
        Me.txtMAPIPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbMAPIProfile = New System.Windows.Forms.ComboBox()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.grpGroupwise = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label28 = New DevComponents.DotNetBar.LabelX()
        Me.txtgwUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label29 = New DevComponents.DotNetBar.LabelX()
        Me.Label41 = New DevComponents.DotNetBar.LabelX()
        Me.Label45 = New DevComponents.DotNetBar.LabelX()
        Me.Label46 = New DevComponents.DotNetBar.LabelX()
        Me.txtgwPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwProxy = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwPOIP = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwPOPort = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpCRDMail = New System.Windows.Forms.GroupBox()
        Me.cmdActivate = New DevComponents.DotNetBar.ButtonX()
        Me.Label23 = New DevComponents.DotNetBar.LabelX()
        Me.txtCRDAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCRDSender = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label24 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox11 = New System.Windows.Forms.GroupBox()
        Me.cmbEncoding = New System.Windows.Forms.ComboBox()
        Me.Label39 = New DevComponents.DotNetBar.LabelX()
        Me.cmbMailType = New System.Windows.Forms.ComboBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.chkEmailRetry = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdMailTest = New DevComponents.DotNetBar.ButtonX()
        Me.cmdClearSettings = New DevComponents.DotNetBar.ButtonX()
        Me.Label51 = New DevComponents.DotNetBar.LabelX()
        Me.Label50 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMSInit = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkUnicode = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpSMSC = New System.Windows.Forms.GroupBox()
        Me.txtSMSNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label36 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSMSProtocol = New System.Windows.Forms.ComboBox()
        Me.Label37 = New DevComponents.DotNetBar.LabelX()
        Me.Label38 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMSPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label40 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMSSender = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optUseGSM = New System.Windows.Forms.RadioButton()
        Me.cmbSMSDevice = New System.Windows.Forms.ComboBox()
        Me.Label33 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSMSFormat = New System.Windows.Forms.ComboBox()
        Me.Label34 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSMSSpeed = New System.Windows.Forms.ComboBox()
        Me.Label35 = New DevComponents.DotNetBar.LabelX()
        Me.optUseSMSC = New System.Windows.Forms.RadioButton()
        Me.GroupBox17 = New System.Windows.Forms.GroupBox()
        Me.chkShowHiddenParameters = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox15 = New System.Windows.Forms.GroupBox()
        Me.chkUseRelTime = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkOutofProcess = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkUNC = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtRefresh = New System.Windows.Forms.NumericUpDown()
        Me.chkDTRefresh = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtCustomerNo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.chkLoadSSRSParametersAutomatically = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkAutoUpdates = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkShareUserDefaults = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkCompact = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkAutoclose = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.txtAutoclose = New DevComponents.Editors.IntegerInput()
        Me.cmdAlertWho = New DevComponents.DotNetBar.ButtonX()
        Me.txtErrorAlertWho = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optErrorMail = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.optErrorSMS = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdAlertSMS = New DevComponents.DotNetBar.ButtonX()
        Me.txtAlertSMS = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkDisableCRParsing = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.optErrorLog = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.stabOptions = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel12 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.btnDelete = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddCloud = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddDropbox = New DevComponents.DotNetBar.ButtonItem()
        Me.lsvCloud = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel11 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDefaultParameters = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel10 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel8 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.cmbKeepTempUnit = New System.Windows.Forms.ComboBox()
        Me.txtKeepTemps = New System.Windows.Forms.NumericUpDown()
        Me.LabelX6 = New DevComponents.DotNetBar.LabelX()
        Me.tabSystemPaths = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabScheduler = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TabControl3 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel12 = New DevComponents.DotNetBar.TabControlPanel()
        Me.chkAutoEmbedImages = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel13 = New DevComponents.DotNetBar.TabControlPanel()
        Me.optSMSByRedOxygen = New System.Windows.Forms.RadioButton()
        Me.optSMSByDevice = New System.Windows.Forms.RadioButton()
        Me.grpSMSByRedOxygen = New System.Windows.Forms.GroupBox()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.txtROAccountID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtROEmail = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtROPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpSMSByDevice = New System.Windows.Forms.GroupBox()
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.tabMessaging = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel9 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabAuditTrail = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TabControl2 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel16 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem6 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel18 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem8 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel17 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem7 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.tabDefaults = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TabControl4 = New DevComponents.DotNetBar.TabControl()
        Me.TabControlPanel14 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel15 = New DevComponents.DotNetBar.TabControlPanel()
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.tabHouseKeeping = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcTasks = New sqlrd.ucTasks()
        Me.ucDSN = New sqlrd.ucDSN()
        Me.UcDest = New sqlrd.ucDestination()
        Me.UcLogin = New sqlrd.ucLoginDetails()
        Me.GroupBox18.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        CType(Me.txtArchive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.grpThreading.SuspendLayout()
        CType(Me.txtThreadInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtThreadCount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox16.SuspendLayout()
        CType(Me.txtNumConnections, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.cmbPollIntEB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbPollInt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtDelayBy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpServices.SuspendLayout()
        Me.grpNTService.SuspendLayout()
        Me.grpSMTP.SuspendLayout()
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpMAPI.SuspendLayout()
        Me.grpGroupwise.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.grpCRDMail.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.grpSMSC.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        CType(Me.txtRefresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtAutoclose, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.stabOptions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabOptions.SuspendLayout()
        Me.SuperTabControlPanel12.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel11.SuspendLayout()
        Me.SuperTabControlPanel10.SuspendLayout()
        Me.SuperTabControlPanel8.SuspendLayout()
        CType(Me.txtKeepTemps, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        CType(Me.TabControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl3.SuspendLayout()
        Me.TabControlPanel12.SuspendLayout()
        Me.TabControlPanel13.SuspendLayout()
        Me.grpSMSByRedOxygen.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.grpSMSByDevice.SuspendLayout()
        Me.SuperTabControlPanel9.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl2.SuspendLayout()
        Me.TabControlPanel16.SuspendLayout()
        Me.TabControlPanel18.SuspendLayout()
        Me.TabControlPanel17.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        CType(Me.TabControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl4.SuspendLayout()
        Me.TabControlPanel14.SuspendLayout()
        Me.TabControlPanel15.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(772, 3)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 4
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(691, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(610, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = False
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'chkAllowAdminToChangeValues
        '
        Me.chkAllowAdminToChangeValues.AutoSize = True
        Me.chkAllowAdminToChangeValues.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAllowAdminToChangeValues.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAllowAdminToChangeValues.Location = New System.Drawing.Point(3, 433)
        Me.chkAllowAdminToChangeValues.Name = "chkAllowAdminToChangeValues"
        Me.chkAllowAdminToChangeValues.Size = New System.Drawing.Size(365, 16)
        Me.chkAllowAdminToChangeValues.TabIndex = 9
        Me.chkAllowAdminToChangeValues.Text = "Allow Administrators to change these values on the parameters screen"
        '
        'Button1
        '
        Me.Button1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button1.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.Button1.Location = New System.Drawing.Point(417, 433)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(97, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Apply to Existing"
        '
        'GroupBox18
        '
        Me.GroupBox18.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox18.Controls.Add(Me.FlowLayoutPanel2)
        Me.GroupBox18.Controls.Add(Me.FlowLayoutPanel1)
        Me.GroupBox18.Location = New System.Drawing.Point(3, 9)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(511, 166)
        Me.GroupBox18.TabIndex = 2
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "Define parameter"
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.Controls.Add(Me.Label59)
        Me.FlowLayoutPanel2.Controls.Add(Me.txtParameterDefaultValue)
        Me.FlowLayoutPanel2.Controls.Add(Me.optParameterSpecifiedValue)
        Me.FlowLayoutPanel2.Controls.Add(Me.optParameterNull)
        Me.FlowLayoutPanel2.Controls.Add(Me.optParameterDefault)
        Me.FlowLayoutPanel2.Controls.Add(Me.optParameterAllValues)
        Me.FlowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(244, 17)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(236, 144)
        Me.FlowLayoutPanel2.TabIndex = 2
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        '
        '
        '
        Me.Label59.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label59.Location = New System.Drawing.Point(3, 3)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(29, 16)
        Me.Label59.TabIndex = 0
        Me.Label59.Text = "Value"
        '
        'txtParameterDefaultValue
        '
        Me.txtParameterDefaultValue.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtParameterDefaultValue.Border.Class = "TextBoxBorder"
        Me.txtParameterDefaultValue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtParameterDefaultValue.ForeColor = System.Drawing.Color.Black
        Me.txtParameterDefaultValue.Location = New System.Drawing.Point(3, 25)
        Me.txtParameterDefaultValue.Name = "txtParameterDefaultValue"
        Me.txtParameterDefaultValue.Size = New System.Drawing.Size(228, 21)
        Me.txtParameterDefaultValue.TabIndex = 3
        '
        'optParameterSpecifiedValue
        '
        Me.optParameterSpecifiedValue.AutoSize = True
        Me.optParameterSpecifiedValue.Checked = True
        Me.optParameterSpecifiedValue.Location = New System.Drawing.Point(3, 52)
        Me.optParameterSpecifiedValue.Name = "optParameterSpecifiedValue"
        Me.optParameterSpecifiedValue.Size = New System.Drawing.Size(142, 17)
        Me.optParameterSpecifiedValue.TabIndex = 4
        Me.optParameterSpecifiedValue.TabStop = True
        Me.optParameterSpecifiedValue.Text = "Set as to specified value"
        Me.optParameterSpecifiedValue.UseVisualStyleBackColor = True
        '
        'optParameterNull
        '
        Me.optParameterNull.AutoSize = True
        Me.optParameterNull.Location = New System.Drawing.Point(3, 75)
        Me.optParameterNull.Name = "optParameterNull"
        Me.optParameterNull.Size = New System.Drawing.Size(75, 17)
        Me.optParameterNull.TabIndex = 5
        Me.optParameterNull.Text = "Set as Null"
        Me.optParameterNull.UseVisualStyleBackColor = True
        '
        'optParameterDefault
        '
        Me.optParameterDefault.AutoSize = True
        Me.optParameterDefault.Location = New System.Drawing.Point(3, 98)
        Me.optParameterDefault.Name = "optParameterDefault"
        Me.optParameterDefault.Size = New System.Drawing.Size(93, 17)
        Me.optParameterDefault.TabIndex = 6
        Me.optParameterDefault.Text = "Set as Default"
        Me.optParameterDefault.UseVisualStyleBackColor = True
        '
        'optParameterAllValues
        '
        Me.optParameterAllValues.AutoSize = True
        Me.optParameterAllValues.Location = New System.Drawing.Point(3, 121)
        Me.optParameterAllValues.Name = "optParameterAllValues"
        Me.optParameterAllValues.Size = New System.Drawing.Size(116, 17)
        Me.optParameterAllValues.TabIndex = 7
        Me.optParameterAllValues.Text = "All Available Values"
        Me.optParameterAllValues.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.Label58)
        Me.FlowLayoutPanel1.Controls.Add(Me.txtDefaultParameterName)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(7, 17)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(234, 111)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'Label58
        '
        '
        '
        '
        Me.Label58.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label58.Dock = System.Windows.Forms.DockStyle.Left
        Me.Label58.Location = New System.Drawing.Point(3, 3)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(45, 13)
        Me.Label58.TabIndex = 0
        Me.Label58.Text = "Name"
        '
        'txtDefaultParameterName
        '
        Me.txtDefaultParameterName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDefaultParameterName.Border.Class = "TextBoxBorder"
        Me.txtDefaultParameterName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefaultParameterName.ForeColor = System.Drawing.Color.Black
        Me.txtDefaultParameterName.Location = New System.Drawing.Point(3, 22)
        Me.txtDefaultParameterName.Name = "txtDefaultParameterName"
        Me.txtDefaultParameterName.Size = New System.Drawing.Size(227, 21)
        Me.txtDefaultParameterName.TabIndex = 1
        '
        'btnRemoveParameterDefault
        '
        Me.btnRemoveParameterDefault.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemoveParameterDefault.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemoveParameterDefault.Enabled = False
        Me.btnRemoveParameterDefault.Image = CType(resources.GetObject("btnRemoveParameterDefault.Image"), System.Drawing.Image)
        Me.btnRemoveParameterDefault.Location = New System.Drawing.Point(3, 183)
        Me.btnRemoveParameterDefault.Name = "btnRemoveParameterDefault"
        Me.btnRemoveParameterDefault.Size = New System.Drawing.Size(97, 23)
        Me.btnRemoveParameterDefault.TabIndex = 3
        '
        'lsvParameterDefaults
        '
        Me.lsvParameterDefaults.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvParameterDefaults.Border.Class = "ListViewBorder"
        Me.lsvParameterDefaults.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvParameterDefaults.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader8, Me.ColumnHeader9})
        Me.lsvParameterDefaults.ForeColor = System.Drawing.Color.Black
        Me.lsvParameterDefaults.FullRowSelect = True
        Me.lsvParameterDefaults.HideSelection = False
        Me.lsvParameterDefaults.Location = New System.Drawing.Point(3, 212)
        Me.lsvParameterDefaults.Name = "lsvParameterDefaults"
        Me.lsvParameterDefaults.Size = New System.Drawing.Size(511, 215)
        Me.lsvParameterDefaults.TabIndex = 0
        Me.lsvParameterDefaults.UseCompatibleStateImageBehavior = False
        Me.lsvParameterDefaults.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Parameter Name"
        Me.ColumnHeader8.Width = 250
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Default Value"
        Me.ColumnHeader9.Width = 250
        '
        'btnAddParameterDefault
        '
        Me.btnAddParameterDefault.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddParameterDefault.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddParameterDefault.Enabled = False
        Me.btnAddParameterDefault.Image = CType(resources.GetObject("btnAddParameterDefault.Image"), System.Drawing.Image)
        Me.btnAddParameterDefault.Location = New System.Drawing.Point(416, 180)
        Me.btnAddParameterDefault.Name = "btnAddParameterDefault"
        Me.btnAddParameterDefault.Size = New System.Drawing.Size(97, 23)
        Me.btnAddParameterDefault.TabIndex = 8
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox12.Controls.Add(Me.Label44)
        Me.GroupBox12.Controls.Add(Me.cmdTest)
        Me.GroupBox12.Controls.Add(Me.ucDSN)
        Me.GroupBox12.Enabled = False
        Me.GroupBox12.Location = New System.Drawing.Point(14, 39)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(488, 368)
        Me.GroupBox12.TabIndex = 2
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Audit Trail Database Setup"
        '
        'Label44
        '
        '
        '
        '
        Me.Label44.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label44.Location = New System.Drawing.Point(8, 32)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(456, 16)
        Me.Label44.TabIndex = 0
        Me.Label44.Text = "Please provide the Data Source Name that will be used to store Audit trail data"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.Location = New System.Drawing.Point(200, 176)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdTest.TabIndex = 1
        Me.cmdTest.Text = "Connect"
        '
        'chkAudit
        '
        Me.chkAudit.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAudit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAudit.Location = New System.Drawing.Point(14, 9)
        Me.chkAudit.Name = "chkAudit"
        Me.chkAudit.Size = New System.Drawing.Size(240, 24)
        Me.chkAudit.TabIndex = 0
        Me.chkAudit.Text = "Enable audit trial"
        '
        'GroupBox14
        '
        Me.GroupBox14.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox14.Controls.Add(Me.chkConvertToMsg)
        Me.GroupBox14.Controls.Add(Me.txtSentMessages)
        Me.GroupBox14.Controls.Add(Me.btnSentMessages)
        Me.GroupBox14.Controls.Add(Me.Label52)
        Me.GroupBox14.Location = New System.Drawing.Point(14, 189)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(503, 84)
        Me.GroupBox14.TabIndex = 9
        Me.GroupBox14.TabStop = False
        '
        'chkConvertToMsg
        '
        Me.chkConvertToMsg.AutoSize = True
        Me.chkConvertToMsg.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkConvertToMsg.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkConvertToMsg.Location = New System.Drawing.Point(5, 59)
        Me.chkConvertToMsg.Name = "chkConvertToMsg"
        Me.chkConvertToMsg.Size = New System.Drawing.Size(294, 16)
        Me.chkConvertToMsg.TabIndex = 8
        Me.chkConvertToMsg.Text = "Convert files to ""MSG"" format for viewing in MS Outlook"
        '
        'txtSentMessages
        '
        Me.txtSentMessages.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSentMessages.Border.Class = "TextBoxBorder"
        Me.txtSentMessages.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSentMessages.ForeColor = System.Drawing.Color.Black
        Me.txtSentMessages.Location = New System.Drawing.Point(6, 32)
        Me.txtSentMessages.Name = "txtSentMessages"
        Me.txtSentMessages.Size = New System.Drawing.Size(419, 21)
        Me.txtSentMessages.TabIndex = 6
        '
        'btnSentMessages
        '
        Me.btnSentMessages.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSentMessages.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSentMessages.Image = CType(resources.GetObject("btnSentMessages.Image"), System.Drawing.Image)
        Me.btnSentMessages.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSentMessages.Location = New System.Drawing.Point(433, 30)
        Me.btnSentMessages.Name = "btnSentMessages"
        Me.btnSentMessages.Size = New System.Drawing.Size(52, 21)
        Me.btnSentMessages.TabIndex = 7
        '
        'Label52
        '
        Me.Label52.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label52.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label52.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label52.Location = New System.Drawing.Point(3, 16)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(216, 16)
        Me.Label52.TabIndex = 0
        Me.Label52.Text = "Sent messages Repository (SMTP)"
        '
        'cmdReportCache
        '
        Me.cmdReportCache.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdReportCache.Image = CType(resources.GetObject("cmdReportCache.Image"), System.Drawing.Image)
        Me.cmdReportCache.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdReportCache.Location = New System.Drawing.Point(445, 329)
        Me.cmdReportCache.Name = "cmdReportCache"
        Me.cmdReportCache.Size = New System.Drawing.Size(52, 21)
        Me.cmdReportCache.TabIndex = 7
        Me.cmdReportCache.Visible = False
        '
        'txtReportCache
        '
        Me.txtReportCache.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtReportCache.Border.Class = "TextBoxBorder"
        Me.txtReportCache.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtReportCache.ForeColor = System.Drawing.Color.Black
        Me.txtReportCache.Location = New System.Drawing.Point(14, 329)
        Me.txtReportCache.Name = "txtReportCache"
        Me.txtReportCache.Size = New System.Drawing.Size(424, 21)
        Me.txtReportCache.TabIndex = 6
        Me.txtReportCache.Visible = False
        '
        'txtTempFolder
        '
        Me.txtTempFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtTempFolder.Border.Class = "TextBoxBorder"
        Me.txtTempFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTempFolder.ForeColor = System.Drawing.Color.Black
        Me.txtTempFolder.Location = New System.Drawing.Point(15, 118)
        Me.txtTempFolder.Name = "txtTempFolder"
        Me.txtTempFolder.Size = New System.Drawing.Size(424, 21)
        Me.txtTempFolder.TabIndex = 4
        '
        'cmdTempFolder
        '
        Me.cmdTempFolder.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTempFolder.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTempFolder.Image = CType(resources.GetObject("cmdTempFolder.Image"), System.Drawing.Image)
        Me.cmdTempFolder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTempFolder.Location = New System.Drawing.Point(447, 119)
        Me.cmdTempFolder.Name = "cmdTempFolder"
        Me.cmdTempFolder.Size = New System.Drawing.Size(52, 21)
        Me.cmdTempFolder.TabIndex = 5
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label42.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label42.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label42.Location = New System.Drawing.Point(15, 9)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(216, 16)
        Me.Label42.TabIndex = 0
        Me.Label42.Text = "Cached Data Folder"
        '
        'txtCachedData
        '
        Me.txtCachedData.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCachedData.Border.Class = "TextBoxBorder"
        Me.txtCachedData.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCachedData.ForeColor = System.Drawing.Color.Black
        Me.txtCachedData.Location = New System.Drawing.Point(15, 25)
        Me.txtCachedData.Name = "txtCachedData"
        Me.txtCachedData.Size = New System.Drawing.Size(424, 21)
        Me.txtCachedData.TabIndex = 0
        '
        'cmdSnapshots
        '
        Me.cmdSnapshots.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSnapshots.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSnapshots.Image = CType(resources.GetObject("cmdSnapshots.Image"), System.Drawing.Image)
        Me.cmdSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSnapshots.Location = New System.Drawing.Point(447, 73)
        Me.cmdSnapshots.Name = "cmdSnapshots"
        Me.cmdSnapshots.Size = New System.Drawing.Size(52, 21)
        Me.cmdSnapshots.TabIndex = 3
        '
        'cmdCachedData
        '
        Me.cmdCachedData.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCachedData.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCachedData.Image = CType(resources.GetObject("cmdCachedData.Image"), System.Drawing.Image)
        Me.cmdCachedData.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCachedData.Location = New System.Drawing.Point(447, 25)
        Me.cmdCachedData.Name = "cmdCachedData"
        Me.cmdCachedData.Size = New System.Drawing.Size(52, 21)
        Me.cmdCachedData.TabIndex = 1
        '
        'txtSnapShots
        '
        Me.txtSnapShots.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSnapShots.Border.Class = "TextBoxBorder"
        Me.txtSnapShots.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSnapShots.ForeColor = System.Drawing.Color.Black
        Me.txtSnapShots.Location = New System.Drawing.Point(15, 73)
        Me.txtSnapShots.Name = "txtSnapShots"
        Me.txtSnapShots.Size = New System.Drawing.Size(424, 21)
        Me.txtSnapShots.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(11, 314)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(216, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Cached Reports folder"
        Me.Label17.Visible = False
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(13, 102)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(216, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Temporary output folder"
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label43.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label43.Location = New System.Drawing.Point(15, 57)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(216, 16)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Snapshots Folder"
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.cmdDBLoc)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.txtDefSubject)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.txtDefaultEmail)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtSig)
        Me.GroupBox3.Controls.Add(Me.cmdSpell)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txtDefAttach)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 7)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(464, 352)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'cmdDBLoc
        '
        Me.cmdDBLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDBLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDBLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDBLoc.Location = New System.Drawing.Point(392, 72)
        Me.cmdDBLoc.Name = "cmdDBLoc"
        Me.cmdDBLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDBLoc.TabIndex = 3
        Me.cmdDBLoc.Text = "..."
        '
        'Label20
        '
        '
        '
        '
        Me.Label20.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(8, 17)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(216, 16)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Default Email Subject"
        '
        'txtDefSubject
        '
        Me.txtDefSubject.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDefSubject.Border.Class = "TextBoxBorder"
        Me.txtDefSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtDefSubject.Location = New System.Drawing.Point(8, 33)
        Me.txtDefSubject.Name = "txtDefSubject"
        Me.txtDefSubject.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefSubject.Size = New System.Drawing.Size(440, 21)
        Me.txtDefSubject.TabIndex = 0
        Me.txtDefSubject.Tag = "memo"
        '
        'Label18
        '
        '
        '
        '
        Me.Label18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(8, 96)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(216, 16)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Default Email Message"
        '
        'txtDefaultEmail
        '
        Me.txtDefaultEmail.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDefaultEmail.Border.Class = "TextBoxBorder"
        Me.txtDefaultEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefaultEmail.ForeColor = System.Drawing.Color.Blue
        Me.txtDefaultEmail.Location = New System.Drawing.Point(8, 112)
        Me.txtDefaultEmail.Multiline = True
        Me.txtDefaultEmail.Name = "txtDefaultEmail"
        Me.txtDefaultEmail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefaultEmail.Size = New System.Drawing.Size(440, 120)
        Me.txtDefaultEmail.TabIndex = 0
        Me.txtDefaultEmail.Tag = "memo"
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 240)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(216, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Default Email Signature"
        '
        'txtSig
        '
        Me.txtSig.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSig.Border.Class = "TextBoxBorder"
        Me.txtSig.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSig.ForeColor = System.Drawing.Color.Blue
        Me.txtSig.Location = New System.Drawing.Point(8, 256)
        Me.txtSig.Multiline = True
        Me.txtSig.Name = "txtSig"
        Me.txtSig.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSig.Size = New System.Drawing.Size(440, 56)
        Me.txtSig.TabIndex = 0
        Me.txtSig.Tag = "memo"
        '
        'cmdSpell
        '
        Me.cmdSpell.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSpell.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSpell.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSpell.Location = New System.Drawing.Point(8, 320)
        Me.cmdSpell.Name = "cmdSpell"
        Me.cmdSpell.Size = New System.Drawing.Size(88, 23)
        Me.cmdSpell.TabIndex = 2
        Me.cmdSpell.Text = "Spell Check"
        '
        'Label31
        '
        '
        '
        '
        Me.Label31.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(8, 56)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(216, 16)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Default Attachment"
        '
        'txtDefAttach
        '
        Me.txtDefAttach.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDefAttach.Border.Class = "TextBoxBorder"
        Me.txtDefAttach.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtDefAttach.Location = New System.Drawing.Point(8, 72)
        Me.txtDefAttach.Name = "txtDefAttach"
        Me.txtDefAttach.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefAttach.Size = New System.Drawing.Size(376, 21)
        Me.txtDefAttach.TabIndex = 0
        Me.txtDefAttach.Tag = "memo"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.lsvServers)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 76)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(461, 214)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Previous Servers"
        '
        'lsvServers
        '
        Me.lsvServers.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvServers.Border.Class = "ListViewBorder"
        Me.lsvServers.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvServers.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7})
        Me.lsvServers.ForeColor = System.Drawing.Color.Black
        Me.lsvServers.FullRowSelect = True
        Me.lsvServers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvServers.HideSelection = False
        Me.lsvServers.Location = New System.Drawing.Point(14, 15)
        Me.lsvServers.Name = "lsvServers"
        Me.lsvServers.Size = New System.Drawing.Size(433, 193)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvServers, New DevComponents.DotNetBar.SuperTooltipInfo("Previous Report Servers", "", "Items in this list are automagically added by SQL-RD as you create schedules. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "T" & _
            "o remove an item, simply select it and press the 'Delete' button on your keyboar" & _
            "d.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.lsvServers.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.lsvServers, "Items in this list are automagically added by SQL-RD as you create schedules. ")
        Me.lsvServers.UseCompatibleStateImageBehavior = False
        Me.lsvServers.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Width = 426
        '
        'GroupBox13
        '
        Me.GroupBox13.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox13.Controls.Add(Me.cmbDateTime)
        Me.GroupBox13.Location = New System.Drawing.Point(7, 298)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(464, 56)
        Me.GroupBox13.TabIndex = 1
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Default Date/Time Stamp"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(8, 24)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(208, 21)
        Me.cmbDateTime.TabIndex = 0
        '
        'GroupBox10
        '
        Me.GroupBox10.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox10.Controls.Add(Me.cmdRptLoc)
        Me.GroupBox10.Controls.Add(Me.txtReportLoc)
        Me.GroupBox10.Location = New System.Drawing.Point(4, 10)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(464, 56)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Default Report Location"
        '
        'cmdRptLoc
        '
        Me.cmdRptLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRptLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRptLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRptLoc.Location = New System.Drawing.Point(400, 24)
        Me.cmdRptLoc.Name = "cmdRptLoc"
        Me.cmdRptLoc.Size = New System.Drawing.Size(52, 21)
        Me.cmdRptLoc.TabIndex = 4
        Me.cmdRptLoc.Text = "..."
        Me.cmdRptLoc.Visible = False
        '
        'txtReportLoc
        '
        Me.txtReportLoc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtReportLoc.Border.Class = "TextBoxBorder"
        Me.txtReportLoc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtReportLoc.ForeColor = System.Drawing.Color.Black
        Me.txtReportLoc.Location = New System.Drawing.Point(8, 24)
        Me.txtReportLoc.Name = "txtReportLoc"
        Me.txtReportLoc.Size = New System.Drawing.Size(376, 21)
        Me.txtReportLoc.TabIndex = 1
        '
        'lsvComponents
        '
        Me.lsvComponents.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvComponents.Border.Class = "ListViewBorder"
        Me.lsvComponents.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvComponents.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvComponents.ForeColor = System.Drawing.Color.Blue
        Me.lsvComponents.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvComponents.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8})
        Me.lsvComponents.Location = New System.Drawing.Point(4, 7)
        Me.lsvComponents.Name = "lsvComponents"
        Me.lsvComponents.Size = New System.Drawing.Size(456, 160)
        Me.lsvComponents.TabIndex = 0
        Me.lsvComponents.UseCompatibleStateImageBehavior = False
        Me.lsvComponents.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 250
        '
        'cmdRegister
        '
        Me.cmdRegister.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRegister.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRegister.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRegister.Location = New System.Drawing.Point(332, 173)
        Me.cmdRegister.Name = "cmdRegister"
        Me.cmdRegister.Size = New System.Drawing.Size(128, 23)
        Me.cmdRegister.TabIndex = 1
        Me.cmdRegister.Text = "Register Component"
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox9.Controls.Add(Me.chkArchive)
        Me.GroupBox9.Controls.Add(Me.Label22)
        Me.GroupBox9.Controls.Add(Me.txtArchive)
        Me.GroupBox9.Location = New System.Drawing.Point(4, 202)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(456, 48)
        Me.GroupBox9.TabIndex = 13
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Archiving"
        '
        'chkArchive
        '
        '
        '
        '
        Me.chkArchive.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkArchive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkArchive.Location = New System.Drawing.Point(8, 16)
        Me.chkArchive.Name = "chkArchive"
        Me.chkArchive.Size = New System.Drawing.Size(264, 24)
        Me.chkArchive.TabIndex = 7
        Me.chkArchive.Text = "Archive schedules that have been disabled for"
        '
        'Label22
        '
        '
        '
        '
        Me.Label22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(344, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 14)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Days"
        '
        'txtArchive
        '
        Me.txtArchive.Enabled = False
        Me.txtArchive.Location = New System.Drawing.Point(272, 16)
        Me.txtArchive.Maximum = New Decimal(New Integer() {366, 0, 0, 0})
        Me.txtArchive.Name = "txtArchive"
        Me.txtArchive.Size = New System.Drawing.Size(48, 21)
        Me.txtArchive.TabIndex = 8
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox8.Controls.Add(Me.chkAutoCompact)
        Me.GroupBox8.Controls.Add(Me.dtAutoCompact)
        Me.GroupBox8.Controls.Add(Me.Label32)
        Me.GroupBox8.Location = New System.Drawing.Point(4, 263)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(456, 80)
        Me.GroupBox8.TabIndex = 12
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Auto-Compact"
        '
        'chkAutoCompact
        '
        '
        '
        '
        Me.chkAutoCompact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoCompact.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoCompact.Location = New System.Drawing.Point(8, 16)
        Me.chkAutoCompact.Name = "chkAutoCompact"
        Me.chkAutoCompact.Size = New System.Drawing.Size(232, 24)
        Me.chkAutoCompact.TabIndex = 9
        Me.chkAutoCompact.Text = "Auto-compact and repair system files at :"
        '
        'dtAutoCompact
        '
        Me.dtAutoCompact.Enabled = False
        Me.dtAutoCompact.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtAutoCompact.Location = New System.Drawing.Point(248, 16)
        Me.dtAutoCompact.Name = "dtAutoCompact"
        Me.dtAutoCompact.ShowUpDown = True
        Me.dtAutoCompact.Size = New System.Drawing.Size(72, 21)
        Me.dtAutoCompact.TabIndex = 10
        Me.dtAutoCompact.Value = New Date(2005, 1, 7, 18, 30, 0, 0)
        '
        'Label32
        '
        '
        '
        '
        Me.Label32.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(8, 40)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(424, 32)
        Me.Label32.TabIndex = 11
        Me.Label32.Text = "CAUTION: Auto-compact reqires that the SQL-RD editor is closed. The system will c" & _
    "lose itself down at this time."
        '
        'optFileLastModified
        '
        Me.optFileLastModified.AutoSize = True
        Me.optFileLastModified.BackColor = System.Drawing.Color.Transparent
        Me.optFileLastModified.Location = New System.Drawing.Point(43, 471)
        Me.optFileLastModified.Name = "optFileLastModified"
        Me.optFileLastModified.Size = New System.Drawing.Size(147, 17)
        Me.optFileLastModified.TabIndex = 4
        Me.optFileLastModified.Text = "When it was last modified"
        Me.optFileLastModified.UseVisualStyleBackColor = False
        '
        'optFileCreated
        '
        Me.optFileCreated.AutoSize = True
        Me.optFileCreated.BackColor = System.Drawing.Color.Transparent
        Me.optFileCreated.Checked = True
        Me.optFileCreated.Location = New System.Drawing.Point(43, 441)
        Me.optFileCreated.Name = "optFileCreated"
        Me.optFileCreated.Size = New System.Drawing.Size(124, 17)
        Me.optFileCreated.TabIndex = 4
        Me.optFileCreated.TabStop = True
        Me.optFileCreated.Text = "When it was created"
        Me.optFileCreated.UseVisualStyleBackColor = False
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label57.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label57.Location = New System.Drawing.Point(10, 419)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(131, 16)
        Me.Label57.TabIndex = 3
        Me.Label57.Text = "Calculate a file's age using"
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Controls.Add(Me.lsvPaths)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Location = New System.Drawing.Point(4, 7)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(456, 406)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = ""
        Me.GroupBox4.Text = "Folder  Housekeeping"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.Location = New System.Drawing.Point(400, 16)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(48, 23)
        Me.cmdAdd.TabIndex = 1
        '
        'lsvPaths
        '
        Me.lsvPaths.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvPaths.Border.Class = "ListViewBorder"
        Me.lsvPaths.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvPaths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lsvPaths.ForeColor = System.Drawing.Color.Black
        Me.lsvPaths.FullRowSelect = True
        Me.lsvPaths.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvPaths.HideSelection = False
        Me.lsvPaths.Location = New System.Drawing.Point(8, 16)
        Me.lsvPaths.Name = "lsvPaths"
        Me.lsvPaths.Size = New System.Drawing.Size(376, 381)
        Me.lsvPaths.TabIndex = 0
        Me.lsvPaths.UseCompatibleStateImageBehavior = False
        Me.lsvPaths.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Path"
        Me.ColumnHeader4.Width = 228
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Delete items older than..."
        Me.ColumnHeader5.Width = 143
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.Location = New System.Drawing.Point(400, 48)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(48, 23)
        Me.cmdDelete.TabIndex = 1
        '
        'grpThreading
        '
        Me.grpThreading.BackColor = System.Drawing.Color.Transparent
        Me.grpThreading.Controls.Add(Me.txtThreadInterval)
        Me.grpThreading.Controls.Add(Me.Label54)
        Me.grpThreading.Controls.Add(Me.txtThreadCount)
        Me.grpThreading.Controls.Add(Me.Label55)
        Me.grpThreading.Controls.Add(Me.Label21)
        Me.grpThreading.Controls.Add(Me.chkThreading)
        Me.grpThreading.Enabled = False
        Me.grpThreading.Location = New System.Drawing.Point(3, 334)
        Me.grpThreading.Name = "grpThreading"
        Me.grpThreading.Size = New System.Drawing.Size(416, 80)
        Me.grpThreading.TabIndex = 56
        Me.grpThreading.TabStop = False
        Me.grpThreading.Text = "Multi-threading"
        '
        'txtThreadInterval
        '
        Me.txtThreadInterval.Location = New System.Drawing.Point(279, 44)
        Me.txtThreadInterval.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtThreadInterval.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtThreadInterval.Name = "txtThreadInterval"
        Me.txtThreadInterval.ReadOnly = True
        Me.txtThreadInterval.Size = New System.Drawing.Size(44, 21)
        Me.txtThreadInterval.TabIndex = 5
        Me.txtThreadInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtThreadInterval.Value = New Decimal(New Integer() {15, 0, 0, 0})
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        '
        '
        '
        Me.Label54.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label54.Location = New System.Drawing.Point(7, 46)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(76, 16)
        Me.Label54.TabIndex = 4
        Me.Label54.Text = "Thread interval"
        '
        'txtThreadCount
        '
        Me.txtThreadCount.Enabled = False
        Me.txtThreadCount.Location = New System.Drawing.Point(279, 18)
        Me.txtThreadCount.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me.txtThreadCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtThreadCount.Name = "txtThreadCount"
        Me.txtThreadCount.ReadOnly = True
        Me.txtThreadCount.Size = New System.Drawing.Size(44, 21)
        Me.txtThreadCount.TabIndex = 1
        Me.txtThreadCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtThreadCount.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'Label55
        '
        '
        '
        '
        Me.Label55.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label55.Location = New System.Drawing.Point(327, 48)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(56, 12)
        Me.Label55.TabIndex = 3
        Me.Label55.Text = "seconds"
        '
        'Label21
        '
        '
        '
        '
        Me.Label21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label21.Location = New System.Drawing.Point(327, 16)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 21)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "threads"
        '
        'chkThreading
        '
        '
        '
        '
        Me.chkThreading.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkThreading.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkThreading.Location = New System.Drawing.Point(10, 17)
        Me.chkThreading.Name = "chkThreading"
        Me.chkThreading.Size = New System.Drawing.Size(263, 24)
        Me.chkThreading.TabIndex = 0
        Me.chkThreading.Text = "Enable Multi-Threading technology and use up to "
        '
        'GroupBox16
        '
        Me.GroupBox16.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox16.Controls.Add(Me.Label56)
        Me.GroupBox16.Controls.Add(Me.txtNumConnections)
        Me.GroupBox16.Controls.Add(Me.chkPrecheckconditions)
        Me.GroupBox16.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox16.Location = New System.Drawing.Point(3, 208)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(414, 119)
        Me.GroupBox16.TabIndex = 4
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Polling Intervals (Seconds)"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        '
        '
        '
        Me.Label56.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label56.Location = New System.Drawing.Point(330, 94)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(60, 16)
        Me.Label56.TabIndex = 15
        Me.Label56.Text = "connections"
        '
        'txtNumConnections
        '
        Me.txtNumConnections.Enabled = False
        Me.txtNumConnections.Location = New System.Drawing.Point(275, 90)
        Me.txtNumConnections.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtNumConnections.Name = "txtNumConnections"
        Me.txtNumConnections.Size = New System.Drawing.Size(41, 21)
        Me.txtNumConnections.TabIndex = 14
        Me.txtNumConnections.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtNumConnections.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        'chkPrecheckconditions
        '
        Me.chkPrecheckconditions.AutoSize = True
        '
        '
        '
        Me.chkPrecheckconditions.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPrecheckconditions.Location = New System.Drawing.Point(8, 92)
        Me.chkPrecheckconditions.Name = "chkPrecheckconditions"
        Me.chkPrecheckconditions.Size = New System.Drawing.Size(228, 16)
        Me.chkPrecheckconditions.TabIndex = 13
        Me.chkPrecheckconditions.Text = "Pre-check database conditions using up to"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPollIntEB, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPriority, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label25, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPollInt, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label47, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label48, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPriorityEB, 3, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(8, 17)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(399, 62)
        Me.TableLayoutPanel2.TabIndex = 12
        '
        'cmbPollIntEB
        '
        Me.cmbPollIntEB.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbPollIntEB.Location = New System.Drawing.Point(86, 34)
        Me.cmbPollIntEB.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.cmbPollIntEB.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbPollIntEB.Name = "cmbPollIntEB"
        Me.cmbPollIntEB.Size = New System.Drawing.Size(109, 21)
        Me.cmbPollIntEB.TabIndex = 2
        Me.cmbPollIntEB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbPollIntEB.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"Real Time", "High", "Above Normal", "Normal", "Below Normal", "Low"})
        Me.cmbPriority.Location = New System.Drawing.Point(281, 3)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(103, 21)
        Me.cmbPriority.TabIndex = 1
        Me.cmbPriority.Tag = "unsorted"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 25)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Standard"
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label25.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(201, 3)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(74, 25)
        Me.Label25.TabIndex = 4
        Me.Label25.Text = "CPU Priority"
        '
        'cmbPollInt
        '
        Me.cmbPollInt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbPollInt.Location = New System.Drawing.Point(86, 3)
        Me.cmbPollInt.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.cmbPollInt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbPollInt.Name = "cmbPollInt"
        Me.cmbPollInt.Size = New System.Drawing.Size(109, 21)
        Me.cmbPollInt.TabIndex = 0
        Me.cmbPollInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbPollInt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label47
        '
        Me.Label47.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label47.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label47.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label47.Location = New System.Drawing.Point(3, 34)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(77, 25)
        Me.Label47.TabIndex = 0
        Me.Label47.Text = "Event-Based"
        '
        'Label48
        '
        Me.Label48.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label48.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label48.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label48.Location = New System.Drawing.Point(201, 34)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(74, 25)
        Me.Label48.TabIndex = 4
        Me.Label48.Text = "CPU Priority"
        '
        'cmbPriorityEB
        '
        Me.cmbPriorityEB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriorityEB.FormattingEnabled = True
        Me.cmbPriorityEB.Items.AddRange(New Object() {"Real Time", "High", "Above Normal", "Normal", "Below Normal", "Low"})
        Me.cmbPriorityEB.Location = New System.Drawing.Point(281, 34)
        Me.cmbPriorityEB.Name = "cmbPriorityEB"
        Me.cmbPriorityEB.Size = New System.Drawing.Size(103, 21)
        Me.cmbPriorityEB.TabIndex = 3
        Me.cmbPriorityEB.Tag = "unsorted"
        '
        'chkUniversal
        '
        Me.chkUniversal.AutoSize = True
        Me.chkUniversal.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkUniversal.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUniversal.Location = New System.Drawing.Point(907, 860)
        Me.chkUniversal.Name = "chkUniversal"
        Me.chkUniversal.Size = New System.Drawing.Size(198, 16)
        Me.chkUniversal.TabIndex = 55
        Me.chkUniversal.TabStop = False
        Me.chkUniversal.Text = "Scheduler should use Universal time"
        Me.chkUniversal.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.btnBlackout)
        Me.GroupBox2.Controls.Add(Me.chkBlackout)
        Me.GroupBox2.Controls.Add(Me.Label53)
        Me.GroupBox2.Controls.Add(Me.txtDelayBy)
        Me.GroupBox2.Controls.Add(Me.chkDelayRestart)
        Me.GroupBox2.Controls.Add(Me.chkCheckService)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 417)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 104)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Miscellaneous"
        '
        'btnBlackout
        '
        Me.btnBlackout.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBlackout.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBlackout.Enabled = False
        Me.btnBlackout.Location = New System.Drawing.Point(295, 74)
        Me.btnBlackout.Name = "btnBlackout"
        Me.btnBlackout.Size = New System.Drawing.Size(44, 23)
        Me.btnBlackout.TabIndex = 8
        Me.btnBlackout.Text = "..."
        '
        'chkBlackout
        '
        Me.chkBlackout.AutoSize = True
        '
        '
        '
        Me.chkBlackout.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkBlackout.Location = New System.Drawing.Point(14, 77)
        Me.chkBlackout.Name = "chkBlackout"
        Me.chkBlackout.Size = New System.Drawing.Size(200, 16)
        Me.chkBlackout.TabIndex = 7
        Me.chkBlackout.Text = "Use Blackout times for the scheduler"
        '
        'Label53
        '
        '
        '
        '
        Me.Label53.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label53.Location = New System.Drawing.Point(344, 45)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(44, 21)
        Me.Label53.TabIndex = 6
        Me.Label53.Text = "minutes"
        '
        'txtDelayBy
        '
        Me.txtDelayBy.Enabled = False
        Me.txtDelayBy.Location = New System.Drawing.Point(295, 45)
        Me.txtDelayBy.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.txtDelayBy.Name = "txtDelayBy"
        Me.txtDelayBy.Size = New System.Drawing.Size(44, 21)
        Me.txtDelayBy.TabIndex = 5
        Me.txtDelayBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDelayBy.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkDelayRestart
        '
        Me.chkDelayRestart.AutoSize = True
        '
        '
        '
        Me.chkDelayRestart.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDelayRestart.Location = New System.Drawing.Point(14, 47)
        Me.chkDelayRestart.Name = "chkDelayRestart"
        Me.chkDelayRestart.Size = New System.Drawing.Size(278, 16)
        Me.chkDelayRestart.TabIndex = 4
        Me.chkDelayRestart.Text = "On editor start-up, delay restarting the scheduler for "
        '
        'chkCheckService
        '
        Me.chkCheckService.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkCheckService.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCheckService.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCheckService.Location = New System.Drawing.Point(14, 17)
        Me.chkCheckService.Name = "chkCheckService"
        Me.chkCheckService.Size = New System.Drawing.Size(399, 24)
        Me.chkCheckService.TabIndex = 0
        Me.chkCheckService.Text = "Do not check and restart the scheduler on editor start-up (not recommended)"
        '
        'cmdServiceApply
        '
        Me.cmdServiceApply.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdServiceApply.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdServiceApply.Enabled = False
        Me.cmdServiceApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdServiceApply.Location = New System.Drawing.Point(422, 14)
        Me.cmdServiceApply.Name = "cmdServiceApply"
        Me.cmdServiceApply.Size = New System.Drawing.Size(96, 23)
        Me.cmdServiceApply.TabIndex = 7
        Me.cmdServiceApply.Text = "Apply Setting"
        '
        'grpServices
        '
        Me.grpServices.BackColor = System.Drawing.Color.Transparent
        Me.grpServices.Controls.Add(Me.chkUseAsBackupScheduler)
        Me.grpServices.Controls.Add(Me.grpNTService)
        Me.grpServices.Controls.Add(Me.optNoScheduling)
        Me.grpServices.Controls.Add(Me.optBackgroundApp)
        Me.grpServices.Controls.Add(Me.optNTService)
        Me.grpServices.Location = New System.Drawing.Point(3, 5)
        Me.grpServices.Name = "grpServices"
        Me.grpServices.Size = New System.Drawing.Size(413, 201)
        Me.grpServices.TabIndex = 0
        Me.grpServices.TabStop = False
        '
        'chkUseAsBackupScheduler
        '
        '
        '
        '
        Me.chkUseAsBackupScheduler.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUseAsBackupScheduler.Enabled = False
        Me.chkUseAsBackupScheduler.Location = New System.Drawing.Point(9, 171)
        Me.chkUseAsBackupScheduler.Name = "chkUseAsBackupScheduler"
        Me.chkUseAsBackupScheduler.Size = New System.Drawing.Size(374, 23)
        Me.chkUseAsBackupScheduler.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkUseAsBackupScheduler.TabIndex = 5
        Me.chkUseAsBackupScheduler.Text = "Use as a backup scheduler server (Failover)"
        '
        'grpNTService
        '
        Me.grpNTService.Controls.Add(Me.txtWinDomain)
        Me.grpNTService.Controls.Add(Me.Label13)
        Me.grpNTService.Controls.Add(Me.txtWinUser)
        Me.grpNTService.Controls.Add(Me.txtWinPassword)
        Me.grpNTService.Controls.Add(Me.Label15)
        Me.grpNTService.Controls.Add(Me.Label14)
        Me.grpNTService.Location = New System.Drawing.Point(11, 98)
        Me.grpNTService.Name = "grpNTService"
        Me.grpNTService.Size = New System.Drawing.Size(380, 72)
        Me.grpNTService.TabIndex = 3
        Me.grpNTService.TabStop = False
        Me.grpNTService.Text = "NT Service Logon Credentials"
        '
        'txtWinDomain
        '
        Me.txtWinDomain.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtWinDomain.Border.Class = "TextBoxBorder"
        Me.txtWinDomain.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWinDomain.ForeColor = System.Drawing.Color.Blue
        Me.txtWinDomain.Location = New System.Drawing.Point(132, 17)
        Me.txtWinDomain.Name = "txtWinDomain"
        Me.txtWinDomain.Size = New System.Drawing.Size(102, 21)
        Me.txtWinDomain.TabIndex = 0
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 19)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(122, 16)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Domain\User Name"
        '
        'txtWinUser
        '
        Me.txtWinUser.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtWinUser.Border.Class = "TextBoxBorder"
        Me.txtWinUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWinUser.ForeColor = System.Drawing.Color.Blue
        Me.txtWinUser.Location = New System.Drawing.Point(255, 18)
        Me.txtWinUser.Name = "txtWinUser"
        Me.txtWinUser.Size = New System.Drawing.Size(118, 21)
        Me.txtWinUser.TabIndex = 1
        '
        'txtWinPassword
        '
        Me.txtWinPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtWinPassword.Border.Class = "TextBoxBorder"
        Me.txtWinPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtWinPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtWinPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtWinPassword.Location = New System.Drawing.Point(132, 45)
        Me.txtWinPassword.Name = "txtWinPassword"
        Me.txtWinPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtWinPassword.Size = New System.Drawing.Size(241, 21)
        Me.txtWinPassword.TabIndex = 2
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 47)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 16)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Windows Password"
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(240, 19)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(17, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "\"
        '
        'optNoScheduling
        '
        Me.optNoScheduling.Checked = True
        Me.optNoScheduling.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNoScheduling.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNoScheduling.Location = New System.Drawing.Point(8, 16)
        Me.optNoScheduling.Name = "optNoScheduling"
        Me.optNoScheduling.Size = New System.Drawing.Size(352, 24)
        Me.optNoScheduling.TabIndex = 0
        Me.optNoScheduling.TabStop = True
        Me.optNoScheduling.Text = "No Scheduling required"
        '
        'optBackgroundApp
        '
        Me.optBackgroundApp.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optBackgroundApp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.optBackgroundApp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optBackgroundApp.Location = New System.Drawing.Point(8, 41)
        Me.optBackgroundApp.Name = "optBackgroundApp"
        Me.optBackgroundApp.Size = New System.Drawing.Size(352, 24)
        Me.optBackgroundApp.TabIndex = 1
        Me.optBackgroundApp.Text = "Use background scheduling application (recommended)"
        '
        'optNTService
        '
        Me.optNTService.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNTService.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNTService.Location = New System.Drawing.Point(8, 66)
        Me.optNTService.Name = "optNTService"
        Me.optNTService.Size = New System.Drawing.Size(352, 24)
        Me.optNTService.TabIndex = 2
        Me.optNTService.Text = "Use NT Service (advanced users on Windows NT, 2000 or later)"
        '
        'cmdServiceStart
        '
        Me.cmdServiceStart.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdServiceStart.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdServiceStart.Enabled = False
        Me.cmdServiceStart.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdServiceStart.Location = New System.Drawing.Point(422, 43)
        Me.cmdServiceStart.Name = "cmdServiceStart"
        Me.cmdServiceStart.Size = New System.Drawing.Size(96, 23)
        Me.cmdServiceStart.TabIndex = 8
        Me.cmdServiceStart.Text = "Start Service"
        '
        'grpSMTP
        '
        Me.grpSMTP.BackColor = System.Drawing.Color.Transparent
        Me.grpSMTP.Controls.Add(Me.btnSMTPAdvanced)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPort)
        Me.grpSMTP.Controls.Add(Me.cmbSMTPTimeout)
        Me.grpSMTP.Controls.Add(Me.cmdSMTPMore)
        Me.grpSMTP.Controls.Add(Me.Label19)
        Me.grpSMTP.Controls.Add(Me.txtSMTPUserID)
        Me.grpSMTP.Controls.Add(Me.Label8)
        Me.grpSMTP.Controls.Add(Me.Label9)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPassword)
        Me.grpSMTP.Controls.Add(Me.Label49)
        Me.grpSMTP.Controls.Add(Me.Label10)
        Me.grpSMTP.Controls.Add(Me.txtSMTPServer)
        Me.grpSMTP.Controls.Add(Me.Label11)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderAddress)
        Me.grpSMTP.Controls.Add(Me.Label12)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderName)
        Me.grpSMTP.Location = New System.Drawing.Point(6, 54)
        Me.grpSMTP.Name = "grpSMTP"
        Me.grpSMTP.Size = New System.Drawing.Size(424, 216)
        Me.grpSMTP.TabIndex = 2
        Me.grpSMTP.TabStop = False
        Me.grpSMTP.Text = "SMTP Mail"
        '
        'btnSMTPAdvanced
        '
        Me.btnSMTPAdvanced.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSMTPAdvanced.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSMTPAdvanced.Location = New System.Drawing.Point(230, 184)
        Me.btnSMTPAdvanced.Name = "btnSMTPAdvanced"
        Me.btnSMTPAdvanced.Size = New System.Drawing.Size(66, 21)
        Me.btnSMTPAdvanced.TabIndex = 9
        Me.btnSMTPAdvanced.Text = "Advanced"
        '
        'txtSMTPPort
        '
        Me.txtSMTPPort.Location = New System.Drawing.Point(310, 88)
        Me.txtSMTPPort.Maximum = New Decimal(New Integer() {80000, 0, 0, 0})
        Me.txtSMTPPort.Name = "txtSMTPPort"
        Me.txtSMTPPort.Size = New System.Drawing.Size(58, 21)
        Me.txtSMTPPort.TabIndex = 8
        Me.txtSMTPPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSMTPPort.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'cmbSMTPTimeout
        '
        Me.cmbSMTPTimeout.Location = New System.Drawing.Point(136, 184)
        Me.cmbSMTPTimeout.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.cmbSMTPTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbSMTPTimeout.Name = "cmbSMTPTimeout"
        Me.cmbSMTPTimeout.Size = New System.Drawing.Size(65, 21)
        Me.cmbSMTPTimeout.TabIndex = 7
        Me.cmbSMTPTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbSMTPTimeout.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmdSMTPMore
        '
        Me.cmdSMTPMore.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSMTPMore.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSMTPMore.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSMTPMore.Location = New System.Drawing.Point(302, 184)
        Me.cmdSMTPMore.Name = "cmdSMTPMore"
        Me.cmdSMTPMore.Size = New System.Drawing.Size(66, 21)
        Me.cmdSMTPMore.TabIndex = 6
        Me.cmdSMTPMore.Text = "Add more"
        '
        'Label19
        '
        '
        '
        '
        Me.Label19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 186)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(96, 16)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "SMTP Timeout"
        '
        'txtSMTPUserID
        '
        Me.txtSMTPUserID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMTPUserID.Border.Class = "TextBoxBorder"
        Me.txtSMTPUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPUserID.Location = New System.Drawing.Point(136, 24)
        Me.txtSMTPUserID.Name = "txtSMTPUserID"
        Me.txtSMTPUserID.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPUserID.TabIndex = 0
        '
        'Label8
        '
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "User ID"
        '
        'Label9
        '
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Password"
        '
        'txtSMTPPassword
        '
        Me.txtSMTPPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMTPPassword.Border.Class = "TextBoxBorder"
        Me.txtSMTPPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMTPPassword.Location = New System.Drawing.Point(136, 56)
        Me.txtSMTPPassword.Name = "txtSMTPPassword"
        Me.txtSMTPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMTPPassword.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPPassword.TabIndex = 1
        '
        'Label49
        '
        '
        '
        '
        Me.Label49.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label49.Location = New System.Drawing.Point(261, 90)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(43, 16)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "Port"
        '
        'Label10
        '
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Server Name"
        '
        'txtSMTPServer
        '
        Me.txtSMTPServer.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMTPServer.Border.Class = "TextBoxBorder"
        Me.txtSMTPServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPServer.Location = New System.Drawing.Point(136, 88)
        Me.txtSMTPServer.Name = "txtSMTPServer"
        Me.txtSMTPServer.Size = New System.Drawing.Size(120, 21)
        Me.txtSMTPServer.TabIndex = 2
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 120)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Sender Address"
        '
        'txtSMTPSenderAddress
        '
        Me.txtSMTPSenderAddress.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMTPSenderAddress.Border.Class = "TextBoxBorder"
        Me.txtSMTPSenderAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderAddress.Location = New System.Drawing.Point(136, 120)
        Me.txtSMTPSenderAddress.Name = "txtSMTPSenderAddress"
        Me.txtSMTPSenderAddress.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPSenderAddress.TabIndex = 3
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 152)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Sender Name"
        '
        'txtSMTPSenderName
        '
        Me.txtSMTPSenderName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMTPSenderName.Border.Class = "TextBoxBorder"
        Me.txtSMTPSenderName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderName.Location = New System.Drawing.Point(136, 152)
        Me.txtSMTPSenderName.Name = "txtSMTPSenderName"
        Me.txtSMTPSenderName.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPSenderName.TabIndex = 4
        '
        'grpMAPI
        '
        Me.grpMAPI.BackColor = System.Drawing.Color.Transparent
        Me.grpMAPI.Controls.Add(Me.btnMAPIMore)
        Me.grpMAPI.Controls.Add(Me.txtMAPIPassword)
        Me.grpMAPI.Controls.Add(Me.cmbMAPIProfile)
        Me.grpMAPI.Controls.Add(Me.Label6)
        Me.grpMAPI.Controls.Add(Me.Label7)
        Me.grpMAPI.Location = New System.Drawing.Point(6, 54)
        Me.grpMAPI.Name = "grpMAPI"
        Me.grpMAPI.Size = New System.Drawing.Size(424, 184)
        Me.grpMAPI.TabIndex = 2
        Me.grpMAPI.TabStop = False
        Me.grpMAPI.Text = "MAPI Mail"
        '
        'btnMAPIMore
        '
        Me.btnMAPIMore.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnMAPIMore.Location = New System.Drawing.Point(310, 116)
        Me.btnMAPIMore.Name = "btnMAPIMore"
        Me.btnMAPIMore.Size = New System.Drawing.Size(58, 23)
        Me.btnMAPIMore.TabIndex = 2
        Me.btnMAPIMore.Text = "More..."
        '
        'txtMAPIPassword
        '
        Me.txtMAPIPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMAPIPassword.Border.Class = "TextBoxBorder"
        Me.txtMAPIPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMAPIPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtMAPIPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtMAPIPassword.Location = New System.Drawing.Point(8, 88)
        Me.txtMAPIPassword.Name = "txtMAPIPassword"
        Me.txtMAPIPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtMAPIPassword.Size = New System.Drawing.Size(360, 21)
        Me.txtMAPIPassword.TabIndex = 1
        '
        'cmbMAPIProfile
        '
        Me.cmbMAPIProfile.ForeColor = System.Drawing.Color.Blue
        Me.cmbMAPIProfile.ItemHeight = 13
        Me.cmbMAPIProfile.Location = New System.Drawing.Point(8, 40)
        Me.cmbMAPIProfile.Name = "cmbMAPIProfile"
        Me.cmbMAPIProfile.Size = New System.Drawing.Size(360, 21)
        Me.cmbMAPIProfile.TabIndex = 0
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "MAPI Profile"
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Password"
        '
        'grpGroupwise
        '
        Me.grpGroupwise.BackColor = System.Drawing.Color.Transparent
        Me.grpGroupwise.Controls.Add(Me.TableLayoutPanel1)
        Me.grpGroupwise.Location = New System.Drawing.Point(6, 54)
        Me.grpGroupwise.Name = "grpGroupwise"
        Me.grpGroupwise.Size = New System.Drawing.Size(424, 216)
        Me.grpGroupwise.TabIndex = 11
        Me.grpGroupwise.TabStop = False
        Me.grpGroupwise.Text = "Groupwise"
        Me.grpGroupwise.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Label28, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwUser, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label29, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label41, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label45, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label46, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPassword, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwProxy, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPOIP, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPOPort, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(22, 30)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(344, 135)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        '
        '
        '
        Me.Label28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label28.Location = New System.Drawing.Point(3, 3)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(39, 16)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "User ID"
        '
        'txtgwUser
        '
        Me.txtgwUser.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtgwUser.Border.Class = "TextBoxBorder"
        Me.txtgwUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwUser.ForeColor = System.Drawing.Color.Black
        Me.txtgwUser.Location = New System.Drawing.Point(84, 3)
        Me.txtgwUser.Name = "txtgwUser"
        Me.txtgwUser.Size = New System.Drawing.Size(219, 21)
        Me.txtgwUser.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        '
        '
        '
        Me.Label29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label29.Location = New System.Drawing.Point(3, 30)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 16)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Password"
        '
        'Label41
        '
        Me.Label41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        '
        '
        '
        Me.Label41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label41.Location = New System.Drawing.Point(3, 57)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(44, 16)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Proxy ID"
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        '
        '
        '
        Me.Label45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label45.Location = New System.Drawing.Point(3, 84)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(75, 16)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Server Address"
        '
        'Label46
        '
        Me.Label46.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label46.AutoSize = True
        '
        '
        '
        Me.Label46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label46.Location = New System.Drawing.Point(3, 111)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(57, 16)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Server Port"
        '
        'txtgwPassword
        '
        Me.txtgwPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtgwPassword.Border.Class = "TextBoxBorder"
        Me.txtgwPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPassword.ForeColor = System.Drawing.Color.Black
        Me.txtgwPassword.Location = New System.Drawing.Point(84, 30)
        Me.txtgwPassword.Name = "txtgwPassword"
        Me.txtgwPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtgwPassword.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPassword.TabIndex = 1
        '
        'txtgwProxy
        '
        Me.txtgwProxy.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtgwProxy.Border.Class = "TextBoxBorder"
        Me.txtgwProxy.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwProxy.ForeColor = System.Drawing.Color.Black
        Me.txtgwProxy.Location = New System.Drawing.Point(84, 57)
        Me.txtgwProxy.Name = "txtgwProxy"
        Me.txtgwProxy.Size = New System.Drawing.Size(219, 21)
        Me.txtgwProxy.TabIndex = 1
        '
        'txtgwPOIP
        '
        Me.txtgwPOIP.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtgwPOIP.Border.Class = "TextBoxBorder"
        Me.txtgwPOIP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPOIP.ForeColor = System.Drawing.Color.Black
        Me.txtgwPOIP.Location = New System.Drawing.Point(84, 84)
        Me.txtgwPOIP.Name = "txtgwPOIP"
        Me.txtgwPOIP.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPOIP.TabIndex = 1
        '
        'txtgwPOPort
        '
        Me.txtgwPOPort.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtgwPOPort.Border.Class = "TextBoxBorder"
        Me.txtgwPOPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPOPort.ForeColor = System.Drawing.Color.Black
        Me.txtgwPOPort.Location = New System.Drawing.Point(84, 111)
        Me.txtgwPOPort.Name = "txtgwPOPort"
        Me.txtgwPOPort.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPOPort.TabIndex = 1
        '
        'grpCRDMail
        '
        Me.grpCRDMail.BackColor = System.Drawing.Color.Transparent
        Me.grpCRDMail.Controls.Add(Me.cmdActivate)
        Me.grpCRDMail.Controls.Add(Me.Label23)
        Me.grpCRDMail.Controls.Add(Me.txtCRDAddress)
        Me.grpCRDMail.Controls.Add(Me.txtCRDSender)
        Me.grpCRDMail.Controls.Add(Me.Label24)
        Me.grpCRDMail.Location = New System.Drawing.Point(6, 54)
        Me.grpCRDMail.Name = "grpCRDMail"
        Me.grpCRDMail.Size = New System.Drawing.Size(424, 216)
        Me.grpCRDMail.TabIndex = 4
        Me.grpCRDMail.TabStop = False
        Me.grpCRDMail.Text = "SQL-RD Mail"
        Me.grpCRDMail.Visible = False
        '
        'cmdActivate
        '
        Me.cmdActivate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdActivate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdActivate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdActivate.Location = New System.Drawing.Point(264, 88)
        Me.cmdActivate.Name = "cmdActivate"
        Me.cmdActivate.Size = New System.Drawing.Size(104, 23)
        Me.cmdActivate.TabIndex = 3
        Me.cmdActivate.Text = "Activate Account"
        '
        'Label23
        '
        '
        '
        '
        Me.Label23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(8, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(100, 16)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Sender Name"
        '
        'txtCRDAddress
        '
        Me.txtCRDAddress.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCRDAddress.Border.Class = "TextBoxBorder"
        Me.txtCRDAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCRDAddress.ForeColor = System.Drawing.Color.Black
        Me.txtCRDAddress.Location = New System.Drawing.Point(136, 54)
        Me.txtCRDAddress.Name = "txtCRDAddress"
        Me.txtCRDAddress.Size = New System.Drawing.Size(232, 21)
        Me.txtCRDAddress.TabIndex = 1
        '
        'txtCRDSender
        '
        Me.txtCRDSender.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCRDSender.Border.Class = "TextBoxBorder"
        Me.txtCRDSender.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCRDSender.ForeColor = System.Drawing.Color.Black
        Me.txtCRDSender.Location = New System.Drawing.Point(136, 24)
        Me.txtCRDSender.Name = "txtCRDSender"
        Me.txtCRDSender.Size = New System.Drawing.Size(232, 21)
        Me.txtCRDSender.TabIndex = 0
        '
        'Label24
        '
        '
        '
        '
        Me.Label24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(8, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(100, 16)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Sender Address"
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox11.Controls.Add(Me.cmbEncoding)
        Me.GroupBox11.Controls.Add(Me.Label39)
        Me.GroupBox11.Location = New System.Drawing.Point(4, 338)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(424, 64)
        Me.GroupBox11.TabIndex = 5
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Text Character Encoding"
        '
        'cmbEncoding
        '
        Me.cmbEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEncoding.ItemHeight = 13
        Me.cmbEncoding.Items.AddRange(New Object() {"US-ASCII", "ISO-2022-JP"})
        Me.cmbEncoding.Location = New System.Drawing.Point(136, 32)
        Me.cmbEncoding.Name = "cmbEncoding"
        Me.cmbEncoding.Size = New System.Drawing.Size(224, 21)
        Me.cmbEncoding.TabIndex = 0
        '
        'Label39
        '
        '
        '
        '
        Me.Label39.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label39.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label39.Location = New System.Drawing.Point(8, 32)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(100, 16)
        Me.Label39.TabIndex = 2
        Me.Label39.Text = "Encoding Type"
        '
        'cmbMailType
        '
        Me.cmbMailType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailType.ForeColor = System.Drawing.Color.Blue
        Me.cmbMailType.ItemHeight = 13
        Me.cmbMailType.Items.AddRange(New Object() {"MAPI", "SMTP", "SQLRDMAIL", "GROUPWISE", "NONE"})
        Me.cmbMailType.Location = New System.Drawing.Point(140, 12)
        Me.cmbMailType.Name = "cmbMailType"
        Me.cmbMailType.Size = New System.Drawing.Size(232, 21)
        Me.cmbMailType.TabIndex = 0
        Me.cmbMailType.Tag = "unsorted"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(4, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Mail Type"
        '
        'chkEmailRetry
        '
        Me.chkEmailRetry.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkEmailRetry.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEmailRetry.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmailRetry.Location = New System.Drawing.Point(6, 276)
        Me.chkEmailRetry.Name = "chkEmailRetry"
        Me.chkEmailRetry.Size = New System.Drawing.Size(152, 24)
        Me.chkEmailRetry.TabIndex = 1
        Me.chkEmailRetry.Text = "Try re-sending failed emails"
        '
        'cmdMailTest
        '
        Me.cmdMailTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMailTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdMailTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMailTest.Location = New System.Drawing.Point(326, 276)
        Me.cmdMailTest.Name = "cmdMailTest"
        Me.cmdMailTest.Size = New System.Drawing.Size(104, 23)
        Me.cmdMailTest.TabIndex = 3
        Me.cmdMailTest.Text = "Test Settings"
        '
        'cmdClearSettings
        '
        Me.cmdClearSettings.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdClearSettings.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdClearSettings.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClearSettings.Location = New System.Drawing.Point(206, 276)
        Me.cmdClearSettings.Name = "cmdClearSettings"
        Me.cmdClearSettings.Size = New System.Drawing.Size(104, 23)
        Me.cmdClearSettings.TabIndex = 2
        Me.cmdClearSettings.Text = "Clear Settings"
        '
        'Label51
        '
        '
        '
        '
        Me.Label51.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label51.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label51.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label51.Location = New System.Drawing.Point(386, 48)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(41, 16)
        Me.SuperTooltip1.SetSuperTooltip(Me.Label51, New DevComponents.DotNetBar.SuperTooltipInfo("", "", resources.GetString("Label51.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.Label51.TabIndex = 9
        Me.Label51.Text = "Help"
        '
        'Label50
        '
        '
        '
        '
        Me.Label50.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label50.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label50.Location = New System.Drawing.Point(78, 49)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(101, 14)
        Me.Label50.TabIndex = 8
        Me.Label50.Text = "Initialization String"
        '
        'txtSMSInit
        '
        Me.txtSMSInit.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMSInit.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSInit.ForeColor = System.Drawing.Color.Black
        Me.txtSMSInit.Location = New System.Drawing.Point(179, 45)
        Me.txtSMSInit.Name = "txtSMSInit"
        Me.txtSMSInit.Size = New System.Drawing.Size(206, 15)
        Me.txtSMSInit.TabIndex = 7
        Me.txtSMSInit.Text = "AT+MS=V22B"
        '
        'chkUnicode
        '
        '
        '
        '
        Me.chkUnicode.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUnicode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUnicode.Location = New System.Drawing.Point(79, 313)
        Me.chkUnicode.Name = "chkUnicode"
        Me.chkUnicode.Size = New System.Drawing.Size(360, 41)
        Me.chkUnicode.TabIndex = 6
        Me.chkUnicode.Text = "Use Unicode (to support Arabic, Chinese and other wide style character sets"
        '
        'grpSMSC
        '
        Me.grpSMSC.Controls.Add(Me.txtSMSNumber)
        Me.grpSMSC.Controls.Add(Me.Label36)
        Me.grpSMSC.Controls.Add(Me.cmbSMSProtocol)
        Me.grpSMSC.Controls.Add(Me.Label37)
        Me.grpSMSC.Controls.Add(Me.Label38)
        Me.grpSMSC.Controls.Add(Me.txtSMSPassword)
        Me.grpSMSC.Controls.Add(Me.Label40)
        Me.grpSMSC.Controls.Add(Me.txtSMSSender)
        Me.grpSMSC.Location = New System.Drawing.Point(79, 161)
        Me.grpSMSC.Name = "grpSMSC"
        Me.grpSMSC.Size = New System.Drawing.Size(304, 144)
        Me.grpSMSC.TabIndex = 5
        Me.grpSMSC.TabStop = False
        Me.grpSMSC.Text = "SMSC Details"
        '
        'txtSMSNumber
        '
        Me.txtSMSNumber.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMSNumber.Border.Class = "TextBoxBorder"
        Me.txtSMSNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSNumber.ForeColor = System.Drawing.Color.Black
        Me.txtSMSNumber.Location = New System.Drawing.Point(120, 24)
        Me.txtSMSNumber.Name = "txtSMSNumber"
        Me.txtSMSNumber.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSNumber.TabIndex = 4
        '
        'Label36
        '
        '
        '
        '
        Me.Label36.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(8, 24)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(88, 16)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "SMSC Number "
        '
        'cmbSMSProtocol
        '
        Me.cmbSMSProtocol.ItemHeight = 13
        Me.cmbSMSProtocol.Items.AddRange(New Object() {"TAP", "UCP"})
        Me.cmbSMSProtocol.Location = New System.Drawing.Point(120, 56)
        Me.cmbSMSProtocol.Name = "cmbSMSProtocol"
        Me.cmbSMSProtocol.Size = New System.Drawing.Size(160, 21)
        Me.cmbSMSProtocol.TabIndex = 1
        '
        'Label37
        '
        '
        '
        '
        Me.Label37.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label37.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label37.Location = New System.Drawing.Point(8, 56)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(96, 16)
        Me.Label37.TabIndex = 0
        Me.Label37.Text = "Provider Protocol"
        '
        'Label38
        '
        '
        '
        '
        Me.Label38.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(8, 80)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(104, 16)
        Me.Label38.TabIndex = 0
        Me.Label38.Text = "Password (optional)"
        '
        'txtSMSPassword
        '
        Me.txtSMSPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMSPassword.Border.Class = "TextBoxBorder"
        Me.txtSMSPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSPassword.ForeColor = System.Drawing.Color.Black
        Me.txtSMSPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMSPassword.Location = New System.Drawing.Point(120, 80)
        Me.txtSMSPassword.Name = "txtSMSPassword"
        Me.txtSMSPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMSPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSPassword.TabIndex = 4
        '
        'Label40
        '
        '
        '
        '
        Me.Label40.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label40.Location = New System.Drawing.Point(8, 117)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(104, 16)
        Me.Label40.TabIndex = 0
        Me.Label40.Text = "Sender Number"
        '
        'txtSMSSender
        '
        Me.txtSMSSender.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtSMSSender.Border.Class = "TextBoxBorder"
        Me.txtSMSSender.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSSender.ForeColor = System.Drawing.Color.Black
        Me.txtSMSSender.Location = New System.Drawing.Point(120, 112)
        Me.txtSMSSender.Name = "txtSMSSender"
        Me.txtSMSSender.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSSender.TabIndex = 4
        '
        'optUseGSM
        '
        Me.optUseGSM.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optUseGSM.Location = New System.Drawing.Point(79, 113)
        Me.optUseGSM.Name = "optUseGSM"
        Me.optUseGSM.Size = New System.Drawing.Size(312, 24)
        Me.optUseGSM.TabIndex = 2
        Me.optUseGSM.Text = "Send through GSM phone or SMS device"
        '
        'cmbSMSDevice
        '
        Me.cmbSMSDevice.ItemHeight = 13
        Me.cmbSMSDevice.Location = New System.Drawing.Point(79, 17)
        Me.cmbSMSDevice.Name = "cmbSMSDevice"
        Me.cmbSMSDevice.Size = New System.Drawing.Size(306, 21)
        Me.cmbSMSDevice.TabIndex = 1
        '
        'Label33
        '
        '
        '
        '
        Me.Label33.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(36, 20)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(41, 16)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Device"
        '
        'cmbSMSFormat
        '
        Me.cmbSMSFormat.ItemHeight = 13
        Me.cmbSMSFormat.Items.AddRange(New Object() {"Default", "8,n,1", "7,e,1"})
        Me.cmbSMSFormat.Location = New System.Drawing.Point(79, 81)
        Me.cmbSMSFormat.Name = "cmbSMSFormat"
        Me.cmbSMSFormat.Size = New System.Drawing.Size(96, 21)
        Me.cmbSMSFormat.TabIndex = 1
        '
        'Label34
        '
        '
        '
        '
        Me.Label34.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(5, 84)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(72, 16)
        Me.Label34.TabIndex = 0
        Me.Label34.Text = "Data Format"
        '
        'cmbSMSSpeed
        '
        Me.cmbSMSSpeed.ItemHeight = 13
        Me.cmbSMSSpeed.Items.AddRange(New Object() {"Default", "1200", "2400", "9600", "19200", "38400", "57600", "115200"})
        Me.cmbSMSSpeed.Location = New System.Drawing.Point(231, 81)
        Me.cmbSMSSpeed.Name = "cmbSMSSpeed"
        Me.cmbSMSSpeed.Size = New System.Drawing.Size(104, 21)
        Me.cmbSMSSpeed.TabIndex = 1
        '
        'Label35
        '
        '
        '
        '
        Me.Label35.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(188, 84)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(40, 16)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Speed"
        '
        'optUseSMSC
        '
        Me.optUseSMSC.Checked = True
        Me.optUseSMSC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optUseSMSC.Location = New System.Drawing.Point(79, 137)
        Me.optUseSMSC.Name = "optUseSMSC"
        Me.optUseSMSC.Size = New System.Drawing.Size(312, 24)
        Me.optUseSMSC.TabIndex = 2
        Me.optUseSMSC.TabStop = True
        Me.optUseSMSC.Text = "Send through service provider"
        '
        'GroupBox17
        '
        Me.GroupBox17.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox17.Controls.Add(Me.chkShowHiddenParameters)
        Me.GroupBox17.Location = New System.Drawing.Point(12, 406)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(481, 59)
        Me.GroupBox17.TabIndex = 8
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Reports"
        '
        'chkShowHiddenParameters
        '
        Me.chkShowHiddenParameters.AutoSize = True
        '
        '
        '
        Me.chkShowHiddenParameters.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkShowHiddenParameters.Location = New System.Drawing.Point(8, 26)
        Me.chkShowHiddenParameters.Name = "chkShowHiddenParameters"
        Me.chkShowHiddenParameters.Size = New System.Drawing.Size(176, 16)
        Me.chkShowHiddenParameters.TabIndex = 0
        Me.chkShowHiddenParameters.Text = "Show hidden report parameters"
        '
        'GroupBox15
        '
        Me.GroupBox15.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox15.Controls.Add(Me.chkUseRelTime)
        Me.GroupBox15.Controls.Add(Me.chkOutofProcess)
        Me.GroupBox15.Controls.Add(Me.chkUNC)
        Me.GroupBox15.Controls.Add(Me.txtRefresh)
        Me.GroupBox15.Controls.Add(Me.chkDTRefresh)
        Me.GroupBox15.Controls.Add(Me.Label3)
        Me.GroupBox15.Controls.Add(Me.txtCustomerNo)
        Me.GroupBox15.Location = New System.Drawing.Point(12, 4)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(477, 163)
        Me.GroupBox15.TabIndex = 7
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "User Experience"
        '
        'chkUseRelTime
        '
        Me.chkUseRelTime.AutoSize = True
        '
        '
        '
        Me.chkUseRelTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUseRelTime.Location = New System.Drawing.Point(7, 120)
        Me.chkUseRelTime.Name = "chkUseRelTime"
        Me.chkUseRelTime.Size = New System.Drawing.Size(110, 16)
        Me.chkUseRelTime.TabIndex = 4
        Me.chkUseRelTime.Text = "Use Relative Time"
        '
        'chkOutofProcess
        '
        '
        '
        '
        Me.chkOutofProcess.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOutofProcess.Location = New System.Drawing.Point(7, 98)
        Me.chkOutofProcess.Name = "chkOutofProcess"
        Me.chkOutofProcess.Size = New System.Drawing.Size(389, 17)
        Me.chkOutofProcess.TabIndex = 7
        Me.chkOutofProcess.Text = "Use the Process Watcher for manual executions"
        '
        'chkUNC
        '
        Me.chkUNC.AutoSize = True
        '
        '
        '
        Me.chkUNC.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkUNC.Checked = True
        Me.chkUNC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUNC.CheckValue = "Y"
        Me.chkUNC.Location = New System.Drawing.Point(7, 76)
        Me.chkUNC.Name = "chkUNC"
        Me.chkUNC.Size = New System.Drawing.Size(190, 16)
        Me.chkUNC.TabIndex = 5
        Me.chkUNC.Text = "Only convert remote paths to UNC"
        '
        'txtRefresh
        '
        Me.txtRefresh.Enabled = False
        Me.txtRefresh.Location = New System.Drawing.Point(239, 52)
        Me.txtRefresh.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtRefresh.Minimum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtRefresh.Name = "txtRefresh"
        Me.txtRefresh.Size = New System.Drawing.Size(40, 21)
        Me.txtRefresh.TabIndex = 4
        Me.txtRefresh.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'chkDTRefresh
        '
        Me.chkDTRefresh.AutoSize = True
        '
        '
        '
        Me.chkDTRefresh.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDTRefresh.Location = New System.Drawing.Point(7, 54)
        Me.chkDTRefresh.Name = "chkDTRefresh"
        Me.chkDTRefresh.Size = New System.Drawing.Size(229, 16)
        Me.chkDTRefresh.TabIndex = 2
        Me.chkDTRefresh.Text = "Automatically refresh desktop every (secs)"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(5, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Customer #"
        '
        'txtCustomerNo
        '
        Me.txtCustomerNo.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtCustomerNo.Border.Class = "TextBoxBorder"
        Me.txtCustomerNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomerNo.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomerNo.Location = New System.Drawing.Point(84, 20)
        Me.txtCustomerNo.Name = "txtCustomerNo"
        Me.txtCustomerNo.Size = New System.Drawing.Size(195, 21)
        Me.txtCustomerNo.TabIndex = 1
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Controls.Add(Me.chkLoadSSRSParametersAutomatically)
        Me.GroupBox7.Controls.Add(Me.chkAutoUpdates)
        Me.GroupBox7.Controls.Add(Me.chkShareUserDefaults)
        Me.GroupBox7.Controls.Add(Me.chkCompact)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 277)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(478, 123)
        Me.GroupBox7.TabIndex = 6
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Misc"
        '
        'chkLoadSSRSParametersAutomatically
        '
        '
        '
        '
        Me.chkLoadSSRSParametersAutomatically.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkLoadSSRSParametersAutomatically.Location = New System.Drawing.Point(7, 96)
        Me.chkLoadSSRSParametersAutomatically.Name = "chkLoadSSRSParametersAutomatically"
        Me.chkLoadSSRSParametersAutomatically.Size = New System.Drawing.Size(362, 23)
        Me.chkLoadSSRSParametersAutomatically.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkLoadSSRSParametersAutomatically.TabIndex = 3
        Me.chkLoadSSRSParametersAutomatically.Text = "Load SSRS cascading parameters automatically"
        '
        'chkAutoUpdates
        '
        Me.chkAutoUpdates.AutoSize = True
        '
        '
        '
        Me.chkAutoUpdates.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoUpdates.Location = New System.Drawing.Point(8, 75)
        Me.chkAutoUpdates.Name = "chkAutoUpdates"
        Me.chkAutoUpdates.Size = New System.Drawing.Size(177, 16)
        Me.chkAutoUpdates.TabIndex = 2
        Me.chkAutoUpdates.Text = "Automatically check for updates"
        '
        'chkShareUserDefaults
        '
        Me.chkShareUserDefaults.AutoSize = True
        '
        '
        '
        Me.chkShareUserDefaults.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkShareUserDefaults.Location = New System.Drawing.Point(8, 49)
        Me.chkShareUserDefaults.Name = "chkShareUserDefaults"
        Me.chkShareUserDefaults.Size = New System.Drawing.Size(196, 16)
        Me.chkShareUserDefaults.TabIndex = 2
        Me.chkShareUserDefaults.Text = "Sync User Defaults across all clients"
        '
        'chkCompact
        '
        '
        '
        '
        Me.chkCompact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCompact.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCompact.Location = New System.Drawing.Point(8, 20)
        Me.chkCompact.Name = "chkCompact"
        Me.chkCompact.Size = New System.Drawing.Size(456, 24)
        Me.chkCompact.TabIndex = 1
        Me.chkCompact.Text = "Do not prompt me to compact the system when it grows over 50MB"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkAutoclose)
        Me.GroupBox1.Controls.Add(Me.LabelX2)
        Me.GroupBox1.Controls.Add(Me.txtAutoclose)
        Me.GroupBox1.Controls.Add(Me.cmdAlertWho)
        Me.GroupBox1.Controls.Add(Me.txtErrorAlertWho)
        Me.GroupBox1.Controls.Add(Me.optErrorMail)
        Me.GroupBox1.Controls.Add(Me.optErrorSMS)
        Me.GroupBox1.Controls.Add(Me.cmdAlertSMS)
        Me.GroupBox1.Controls.Add(Me.txtAlertSMS)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 168)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 100)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Error Handling"
        '
        'chkAutoclose
        '
        '
        '
        '
        Me.chkAutoclose.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoclose.Location = New System.Drawing.Point(9, 70)
        Me.chkAutoclose.Name = "chkAutoclose"
        Me.chkAutoclose.Size = New System.Drawing.Size(207, 23)
        Me.chkAutoclose.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAutoclose.TabIndex = 13
        Me.chkAutoclose.Text = "Close ""Oops"" error messages after"
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(313, 70)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 12
        Me.LabelX2.Text = "seconds"
        '
        'txtAutoclose
        '
        '
        '
        '
        Me.txtAutoclose.BackgroundStyle.Class = "DateTimeInputBackground"
        Me.txtAutoclose.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAutoclose.ButtonFreeText.Shortcut = DevComponents.DotNetBar.eShortcut.F2
        Me.txtAutoclose.Enabled = False
        Me.txtAutoclose.Location = New System.Drawing.Point(224, 72)
        Me.txtAutoclose.MinValue = 5
        Me.txtAutoclose.Name = "txtAutoclose"
        Me.txtAutoclose.ShowUpDown = True
        Me.txtAutoclose.Size = New System.Drawing.Size(80, 21)
        Me.txtAutoclose.TabIndex = 11
        Me.txtAutoclose.Value = 30
        '
        'cmdAlertWho
        '
        Me.cmdAlertWho.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAlertWho.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAlertWho.Enabled = False
        Me.cmdAlertWho.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAlertWho.Location = New System.Drawing.Point(168, 21)
        Me.cmdAlertWho.Name = "cmdAlertWho"
        Me.cmdAlertWho.Size = New System.Drawing.Size(48, 23)
        Me.cmdAlertWho.TabIndex = 2
        Me.cmdAlertWho.Text = "To..."
        '
        'txtErrorAlertWho
        '
        Me.txtErrorAlertWho.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtErrorAlertWho.Border.Class = "TextBoxBorder"
        Me.txtErrorAlertWho.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtErrorAlertWho.Enabled = False
        Me.txtErrorAlertWho.ForeColor = System.Drawing.Color.Blue
        Me.txtErrorAlertWho.Location = New System.Drawing.Point(224, 21)
        Me.txtErrorAlertWho.Name = "txtErrorAlertWho"
        Me.txtErrorAlertWho.Size = New System.Drawing.Size(232, 21)
        Me.txtErrorAlertWho.TabIndex = 3
        '
        'optErrorMail
        '
        Me.optErrorMail.AutoSize = True
        '
        '
        '
        Me.optErrorMail.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.optErrorMail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorMail.Location = New System.Drawing.Point(8, 20)
        Me.optErrorMail.Name = "optErrorMail"
        Me.optErrorMail.Size = New System.Drawing.Size(91, 16)
        Me.optErrorMail.TabIndex = 1
        Me.optErrorMail.Text = "Send an email"
        '
        'optErrorSMS
        '
        Me.optErrorSMS.AutoSize = True
        '
        '
        '
        Me.optErrorSMS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.optErrorSMS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorSMS.Location = New System.Drawing.Point(8, 44)
        Me.optErrorSMS.Name = "optErrorSMS"
        Me.optErrorSMS.Size = New System.Drawing.Size(131, 16)
        Me.optErrorSMS.TabIndex = 4
        Me.optErrorSMS.Text = "Send an SMS Message"
        '
        'cmdAlertSMS
        '
        Me.cmdAlertSMS.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAlertSMS.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAlertSMS.Enabled = False
        Me.cmdAlertSMS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAlertSMS.Location = New System.Drawing.Point(168, 45)
        Me.cmdAlertSMS.Name = "cmdAlertSMS"
        Me.cmdAlertSMS.Size = New System.Drawing.Size(48, 23)
        Me.cmdAlertSMS.TabIndex = 5
        Me.cmdAlertSMS.Text = "To..."
        '
        'txtAlertSMS
        '
        Me.txtAlertSMS.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAlertSMS.Border.Class = "TextBoxBorder"
        Me.txtAlertSMS.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAlertSMS.Enabled = False
        Me.txtAlertSMS.ForeColor = System.Drawing.Color.Blue
        Me.txtAlertSMS.Location = New System.Drawing.Point(224, 46)
        Me.txtAlertSMS.Name = "txtAlertSMS"
        Me.txtAlertSMS.Size = New System.Drawing.Size(232, 21)
        Me.txtAlertSMS.TabIndex = 6
        '
        'chkDisableCRParsing
        '
        Me.chkDisableCRParsing.AutoSize = True
        '
        '
        '
        Me.chkDisableCRParsing.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDisableCRParsing.Location = New System.Drawing.Point(143, 984)
        Me.chkDisableCRParsing.Name = "chkDisableCRParsing"
        Me.chkDisableCRParsing.Size = New System.Drawing.Size(202, 16)
        Me.chkDisableCRParsing.TabIndex = 2
        Me.chkDisableCRParsing.Text = "Disable Crystal Reports fields parsing"
        Me.ToolTip1.SetToolTip(Me.chkDisableCRParsing, "Disables the ability to insert Crystal Reports fields into emails etc. Having thi" & _
        "s option checked can speed up report exporting.")
        Me.chkDisableCRParsing.Visible = False
        '
        'optErrorLog
        '
        '
        '
        '
        Me.optErrorLog.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.optErrorLog.Checked = True
        Me.optErrorLog.CheckState = System.Windows.Forms.CheckState.Checked
        Me.optErrorLog.CheckValue = "Y"
        Me.optErrorLog.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorLog.Location = New System.Drawing.Point(18, 984)
        Me.optErrorLog.Name = "optErrorLog"
        Me.optErrorLog.Size = New System.Drawing.Size(160, 24)
        Me.optErrorLog.TabIndex = 0
        Me.optErrorLog.Text = "Write to error log file"
        Me.optErrorLog.Visible = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.SuperTooltip1.TooltipDuration = 30
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(310, 88)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {80000, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(58, 20)
        Me.NumericUpDown1.TabIndex = 8
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown1.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuCut
        '
        Me.mnuCut.Index = 0
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 2
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 3
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 5
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 6
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 7
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'stabOptions
        '
        Me.stabOptions.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabOptions.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabOptions.ControlBox.MenuBox.Name = ""
        Me.stabOptions.ControlBox.Name = ""
        Me.stabOptions.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabOptions.ControlBox.MenuBox, Me.stabOptions.ControlBox.CloseBox})
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel12)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel11)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel10)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel8)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel9)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabOptions.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabOptions.ForeColor = System.Drawing.Color.Black
        Me.stabOptions.Location = New System.Drawing.Point(0, 0)
        Me.stabOptions.Name = "stabOptions"
        Me.stabOptions.ReorderTabsEnabled = True
        Me.stabOptions.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabOptions.SelectedTabIndex = 1
        Me.stabOptions.Size = New System.Drawing.Size(850, 544)
        Me.stabOptions.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabOptions.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabOptions.TabIndex = 11
        Me.stabOptions.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabMessaging, Me.tabScheduler, Me.tabHouseKeeping, Me.tabDefaults, Me.tabDestinations, Me.tabSystemPaths, Me.tabAuditTrail, Me.tabTasks, Me.tabDefaultParameters, Me.SuperTabItem2})
        Me.stabOptions.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabOptions.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel12
        '
        Me.SuperTabControlPanel12.Controls.Add(Me.btnDelete)
        Me.SuperTabControlPanel12.Controls.Add(Me.btnAddCloud)
        Me.SuperTabControlPanel12.Controls.Add(Me.lsvCloud)
        Me.SuperTabControlPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel12.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel12.Name = "SuperTabControlPanel12"
        Me.SuperTabControlPanel12.Size = New System.Drawing.Size(709, 544)
        Me.SuperTabControlPanel12.TabIndex = 0
        Me.SuperTabControlPanel12.TabItem = Me.SuperTabItem2
        '
        'btnDelete
        '
        Me.btnDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDelete.Location = New System.Drawing.Point(561, 43)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(74, 23)
        Me.btnDelete.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnDelete.TabIndex = 5
        Me.btnDelete.Text = "Delete"
        '
        'btnAddCloud
        '
        Me.btnAddCloud.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddCloud.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddCloud.Location = New System.Drawing.Point(561, 14)
        Me.btnAddCloud.Name = "btnAddCloud"
        Me.btnAddCloud.Size = New System.Drawing.Size(74, 23)
        Me.btnAddCloud.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAddCloud.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnAddDropbox})
        Me.btnAddCloud.TabIndex = 4
        Me.btnAddCloud.Text = "Add"
        '
        'btnAddDropbox
        '
        Me.btnAddDropbox.GlobalItem = False
        Me.btnAddDropbox.Image = CType(resources.GetObject("btnAddDropbox.Image"), System.Drawing.Image)
        Me.btnAddDropbox.Name = "btnAddDropbox"
        Me.btnAddDropbox.Text = "Dropbox Account"
        '
        'lsvCloud
        '
        '
        '
        '
        Me.lsvCloud.Border.Class = "ListViewBorder"
        Me.lsvCloud.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvCloud.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader2})
        Me.lsvCloud.Location = New System.Drawing.Point(9, 14)
        Me.lsvCloud.Name = "lsvCloud"
        Me.lsvCloud.Size = New System.Drawing.Size(543, 501)
        Me.lsvCloud.TabIndex = 3
        Me.lsvCloud.UseCompatibleStateImageBehavior = False
        Me.lsvCloud.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Account Name"
        Me.ColumnHeader6.Width = 214
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Account Type"
        Me.ColumnHeader2.Width = 319
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel12
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Image = CType(resources.GetObject("SuperTabItem2.Image"), System.Drawing.Image)
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Cloud Storage"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox17)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox15)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox7)
        Me.SuperTabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(709, 544)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel11
        '
        Me.SuperTabControlPanel11.Controls.Add(Me.Button1)
        Me.SuperTabControlPanel11.Controls.Add(Me.chkAllowAdminToChangeValues)
        Me.SuperTabControlPanel11.Controls.Add(Me.GroupBox18)
        Me.SuperTabControlPanel11.Controls.Add(Me.btnRemoveParameterDefault)
        Me.SuperTabControlPanel11.Controls.Add(Me.lsvParameterDefaults)
        Me.SuperTabControlPanel11.Controls.Add(Me.btnAddParameterDefault)
        Me.SuperTabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel11.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel11.Name = "SuperTabControlPanel11"
        Me.SuperTabControlPanel11.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel11.TabIndex = 0
        Me.SuperTabControlPanel11.TabItem = Me.tabDefaultParameters
        '
        'tabDefaultParameters
        '
        Me.tabDefaultParameters.AttachedControl = Me.SuperTabControlPanel11
        Me.tabDefaultParameters.GlobalItem = False
        Me.tabDefaultParameters.Image = CType(resources.GetObject("tabDefaultParameters.Image"), System.Drawing.Image)
        Me.tabDefaultParameters.Name = "tabDefaultParameters"
        Me.tabDefaultParameters.Text = "Default Parameters"
        '
        'SuperTabControlPanel10
        '
        Me.SuperTabControlPanel10.Controls.Add(Me.UcTasks)
        Me.SuperTabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel10.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel10.Name = "SuperTabControlPanel10"
        Me.SuperTabControlPanel10.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel10.TabIndex = 0
        Me.SuperTabControlPanel10.TabItem = Me.tabTasks
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel10
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Default Tasks"
        '
        'SuperTabControlPanel8
        '
        Me.SuperTabControlPanel8.Controls.Add(Me.cmbKeepTempUnit)
        Me.SuperTabControlPanel8.Controls.Add(Me.txtKeepTemps)
        Me.SuperTabControlPanel8.Controls.Add(Me.LabelX6)
        Me.SuperTabControlPanel8.Controls.Add(Me.GroupBox14)
        Me.SuperTabControlPanel8.Controls.Add(Me.Label42)
        Me.SuperTabControlPanel8.Controls.Add(Me.cmdReportCache)
        Me.SuperTabControlPanel8.Controls.Add(Me.Label43)
        Me.SuperTabControlPanel8.Controls.Add(Me.txtReportCache)
        Me.SuperTabControlPanel8.Controls.Add(Me.Label16)
        Me.SuperTabControlPanel8.Controls.Add(Me.txtTempFolder)
        Me.SuperTabControlPanel8.Controls.Add(Me.Label17)
        Me.SuperTabControlPanel8.Controls.Add(Me.cmdTempFolder)
        Me.SuperTabControlPanel8.Controls.Add(Me.txtSnapShots)
        Me.SuperTabControlPanel8.Controls.Add(Me.cmdCachedData)
        Me.SuperTabControlPanel8.Controls.Add(Me.txtCachedData)
        Me.SuperTabControlPanel8.Controls.Add(Me.cmdSnapshots)
        Me.SuperTabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel8.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel8.Name = "SuperTabControlPanel8"
        Me.SuperTabControlPanel8.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel8.TabIndex = 0
        Me.SuperTabControlPanel8.TabItem = Me.tabSystemPaths
        '
        'cmbKeepTempUnit
        '
        Me.cmbKeepTempUnit.FormattingEnabled = True
        Me.cmbKeepTempUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months"})
        Me.cmbKeepTempUnit.Location = New System.Drawing.Point(372, 142)
        Me.cmbKeepTempUnit.Name = "cmbKeepTempUnit"
        Me.cmbKeepTempUnit.Size = New System.Drawing.Size(77, 21)
        Me.cmbKeepTempUnit.TabIndex = 13
        Me.cmbKeepTempUnit.Tag = "nosort"
        Me.cmbKeepTempUnit.Text = "Days"
        '
        'txtKeepTemps
        '
        Me.txtKeepTemps.Location = New System.Drawing.Point(331, 143)
        Me.txtKeepTemps.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.txtKeepTemps.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtKeepTemps.Name = "txtKeepTemps"
        Me.txtKeepTemps.Size = New System.Drawing.Size(36, 21)
        Me.txtKeepTemps.TabIndex = 11
        Me.txtKeepTemps.Value = New Decimal(New Integer() {7, 0, 0, 0})
        '
        'LabelX6
        '
        Me.LabelX6.AutoSize = True
        Me.LabelX6.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.LabelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX6.Location = New System.Drawing.Point(24, 145)
        Me.LabelX6.Name = "LabelX6"
        Me.LabelX6.Size = New System.Drawing.Size(294, 16)
        Me.LabelX6.TabIndex = 12
        Me.LabelX6.Text = "Delete items from the temporary folder that are older than "
        '
        'tabSystemPaths
        '
        Me.tabSystemPaths.AttachedControl = Me.SuperTabControlPanel8
        Me.tabSystemPaths.GlobalItem = False
        Me.tabSystemPaths.Image = CType(resources.GetObject("tabSystemPaths.Image"), System.Drawing.Image)
        Me.tabSystemPaths.Name = "tabSystemPaths"
        Me.tabSystemPaths.Text = "System Paths"
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdServiceApply)
        Me.SuperTabControlPanel3.Controls.Add(Me.cmdServiceStart)
        Me.SuperTabControlPanel3.Controls.Add(Me.GroupBox2)
        Me.SuperTabControlPanel3.Controls.Add(Me.grpThreading)
        Me.SuperTabControlPanel3.Controls.Add(Me.grpServices)
        Me.SuperTabControlPanel3.Controls.Add(Me.GroupBox16)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabScheduler
        '
        'tabScheduler
        '
        Me.tabScheduler.AttachedControl = Me.SuperTabControlPanel3
        Me.tabScheduler.GlobalItem = False
        Me.tabScheduler.Image = CType(resources.GetObject("tabScheduler.Image"), System.Drawing.Image)
        Me.tabScheduler.Name = "tabScheduler"
        Me.tabScheduler.Text = "Scheduler"
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.TabControl3)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabMessaging
        '
        'TabControl3
        '
        Me.TabControl3.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl3.CanReorderTabs = True
        Me.TabControl3.Controls.Add(Me.TabControlPanel12)
        Me.TabControl3.Controls.Add(Me.TabControlPanel13)
        Me.TabControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl3.ForeColor = System.Drawing.Color.Black
        Me.TabControl3.Location = New System.Drawing.Point(0, 0)
        Me.TabControl3.Name = "TabControl3"
        Me.TabControl3.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl3.SelectedTabIndex = 0
        Me.TabControl3.Size = New System.Drawing.Size(681, 530)
        Me.TabControl3.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock
        Me.TabControl3.TabIndex = 9
        Me.TabControl3.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl3.Tabs.Add(Me.TabItem2)
        Me.TabControl3.Tabs.Add(Me.TabItem3)
        Me.TabControl3.Text = "TabControl3"
        '
        'TabControlPanel12
        '
        Me.TabControlPanel12.Controls.Add(Me.grpSMTP)
        Me.TabControlPanel12.Controls.Add(Me.chkAutoEmbedImages)
        Me.TabControlPanel12.Controls.Add(Me.GroupBox11)
        Me.TabControlPanel12.Controls.Add(Me.grpCRDMail)
        Me.TabControlPanel12.Controls.Add(Me.chkEmailRetry)
        Me.TabControlPanel12.Controls.Add(Me.cmdMailTest)
        Me.TabControlPanel12.Controls.Add(Me.grpGroupwise)
        Me.TabControlPanel12.Controls.Add(Me.cmdClearSettings)
        Me.TabControlPanel12.Controls.Add(Me.grpMAPI)
        Me.TabControlPanel12.Controls.Add(Me.Label5)
        Me.TabControlPanel12.Controls.Add(Me.cmbMailType)
        Me.TabControlPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel12.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel12.Name = "TabControlPanel12"
        Me.TabControlPanel12.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel12.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel12.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel12.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel12.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel12.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel12.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel12.Style.GradientAngle = 90
        Me.TabControlPanel12.TabIndex = 1
        Me.TabControlPanel12.TabItem = Me.TabItem2
        '
        'chkAutoEmbedImages
        '
        Me.chkAutoEmbedImages.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkAutoEmbedImages.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAutoEmbedImages.Location = New System.Drawing.Point(6, 309)
        Me.chkAutoEmbedImages.Name = "chkAutoEmbedImages"
        Me.chkAutoEmbedImages.Size = New System.Drawing.Size(421, 23)
        Me.chkAutoEmbedImages.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkAutoEmbedImages.TabIndex = 12
        Me.chkAutoEmbedImages.Text = "Automatically embed related images into HTML emails"
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel12
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Email Settings"
        '
        'TabControlPanel13
        '
        Me.TabControlPanel13.Controls.Add(Me.optSMSByRedOxygen)
        Me.TabControlPanel13.Controls.Add(Me.optSMSByDevice)
        Me.TabControlPanel13.Controls.Add(Me.grpSMSByRedOxygen)
        Me.TabControlPanel13.Controls.Add(Me.grpSMSByDevice)
        Me.TabControlPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel13.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel13.Name = "TabControlPanel13"
        Me.TabControlPanel13.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel13.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel13.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel13.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel13.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel13.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel13.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel13.Style.GradientAngle = 90
        Me.TabControlPanel13.TabIndex = 2
        Me.TabControlPanel13.TabItem = Me.TabItem3
        '
        'optSMSByRedOxygen
        '
        Me.optSMSByRedOxygen.AutoSize = True
        Me.optSMSByRedOxygen.BackColor = System.Drawing.Color.Transparent
        Me.optSMSByRedOxygen.Location = New System.Drawing.Point(230, 16)
        Me.optSMSByRedOxygen.Name = "optSMSByRedOxygen"
        Me.optSMSByRedOxygen.Size = New System.Drawing.Size(231, 17)
        Me.optSMSByRedOxygen.TabIndex = 5
        Me.optSMSByRedOxygen.Text = "Use RedOxygen Web Service  to send SMS"
        Me.optSMSByRedOxygen.UseVisualStyleBackColor = False
        '
        'optSMSByDevice
        '
        Me.optSMSByDevice.AutoSize = True
        Me.optSMSByDevice.BackColor = System.Drawing.Color.Transparent
        Me.optSMSByDevice.Checked = True
        Me.optSMSByDevice.Location = New System.Drawing.Point(4, 16)
        Me.optSMSByDevice.Name = "optSMSByDevice"
        Me.optSMSByDevice.Size = New System.Drawing.Size(188, 17)
        Me.optSMSByDevice.TabIndex = 4
        Me.optSMSByDevice.TabStop = True
        Me.optSMSByDevice.Text = "Use hardware device to send SMS"
        Me.optSMSByDevice.UseVisualStyleBackColor = False
        '
        'grpSMSByRedOxygen
        '
        Me.grpSMSByRedOxygen.BackColor = System.Drawing.Color.Transparent
        Me.grpSMSByRedOxygen.Controls.Add(Me.LabelX5)
        Me.grpSMSByRedOxygen.Controls.Add(Me.TableLayoutPanel3)
        Me.grpSMSByRedOxygen.Location = New System.Drawing.Point(4, 42)
        Me.grpSMSByRedOxygen.Name = "grpSMSByRedOxygen"
        Me.grpSMSByRedOxygen.Size = New System.Drawing.Size(519, 413)
        Me.grpSMSByRedOxygen.TabIndex = 3
        Me.grpSMSByRedOxygen.TabStop = False
        Me.grpSMSByRedOxygen.Text = "SMS using RedOxygen"
        Me.grpSMSByRedOxygen.Visible = False
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(9, 129)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(511, 43)
        Me.LabelX5.TabIndex = 1
        Me.LabelX5.Text = "You may learn more and sign up with the <font color=""#ED1C24""><b>RedOxygen</b></f" & _
    "ont> SMS service at <a href='htp://www.redoxygen.com'>http://www.redoxygen.com</" & _
    "a>"
        Me.LabelX5.WordWrap = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.50685!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.49315!))
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX1, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX3, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.LabelX4, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.txtROAccountID, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.txtROEmail, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtROPassword, 1, 2)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(9, 23)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(511, 100)
        Me.TableLayoutPanel3.TabIndex = 0
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(3, 3)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 1
        Me.LabelX1.Text = "Account ID"
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 36)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(154, 23)
        Me.LabelX3.TabIndex = 1
        Me.LabelX3.Text = "Account Email Address"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 69)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(154, 23)
        Me.LabelX4.TabIndex = 1
        Me.LabelX4.Text = "Account Password"
        '
        'txtROAccountID
        '
        Me.txtROAccountID.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtROAccountID.Border.Class = "TextBoxBorder"
        Me.txtROAccountID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtROAccountID.ForeColor = System.Drawing.Color.Black
        Me.txtROAccountID.Location = New System.Drawing.Point(163, 3)
        Me.txtROAccountID.Name = "txtROAccountID"
        Me.txtROAccountID.Size = New System.Drawing.Size(243, 21)
        Me.txtROAccountID.TabIndex = 0
        '
        'txtROEmail
        '
        Me.txtROEmail.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtROEmail.Border.Class = "TextBoxBorder"
        Me.txtROEmail.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtROEmail.ForeColor = System.Drawing.Color.Black
        Me.txtROEmail.Location = New System.Drawing.Point(163, 36)
        Me.txtROEmail.Name = "txtROEmail"
        Me.txtROEmail.Size = New System.Drawing.Size(243, 21)
        Me.txtROEmail.TabIndex = 1
        '
        'txtROPassword
        '
        Me.txtROPassword.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtROPassword.Border.Class = "TextBoxBorder"
        Me.txtROPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtROPassword.ForeColor = System.Drawing.Color.Black
        Me.txtROPassword.Location = New System.Drawing.Point(163, 69)
        Me.txtROPassword.Name = "txtROPassword"
        Me.txtROPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtROPassword.Size = New System.Drawing.Size(243, 21)
        Me.txtROPassword.TabIndex = 2
        '
        'grpSMSByDevice
        '
        Me.grpSMSByDevice.BackColor = System.Drawing.Color.Transparent
        Me.grpSMSByDevice.Controls.Add(Me.Label51)
        Me.grpSMSByDevice.Controls.Add(Me.Label33)
        Me.grpSMSByDevice.Controls.Add(Me.Label50)
        Me.grpSMSByDevice.Controls.Add(Me.optUseSMSC)
        Me.grpSMSByDevice.Controls.Add(Me.txtSMSInit)
        Me.grpSMSByDevice.Controls.Add(Me.Label35)
        Me.grpSMSByDevice.Controls.Add(Me.chkUnicode)
        Me.grpSMSByDevice.Controls.Add(Me.cmbSMSSpeed)
        Me.grpSMSByDevice.Controls.Add(Me.grpSMSC)
        Me.grpSMSByDevice.Controls.Add(Me.Label34)
        Me.grpSMSByDevice.Controls.Add(Me.optUseGSM)
        Me.grpSMSByDevice.Controls.Add(Me.cmbSMSFormat)
        Me.grpSMSByDevice.Controls.Add(Me.cmbSMSDevice)
        Me.grpSMSByDevice.Location = New System.Drawing.Point(4, 42)
        Me.grpSMSByDevice.Name = "grpSMSByDevice"
        Me.grpSMSByDevice.Size = New System.Drawing.Size(519, 413)
        Me.grpSMSByDevice.TabIndex = 0
        Me.grpSMSByDevice.TabStop = False
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel13
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "SMS Settings"
        '
        'tabMessaging
        '
        Me.tabMessaging.AttachedControl = Me.SuperTabControlPanel2
        Me.tabMessaging.GlobalItem = False
        Me.tabMessaging.Image = CType(resources.GetObject("tabMessaging.Image"), System.Drawing.Image)
        Me.tabMessaging.Name = "tabMessaging"
        Me.tabMessaging.Text = "Messaging"
        '
        'SuperTabControlPanel9
        '
        Me.SuperTabControlPanel9.Controls.Add(Me.GroupBox12)
        Me.SuperTabControlPanel9.Controls.Add(Me.chkAudit)
        Me.SuperTabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel9.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel9.Name = "SuperTabControlPanel9"
        Me.SuperTabControlPanel9.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel9.TabIndex = 0
        Me.SuperTabControlPanel9.TabItem = Me.tabAuditTrail
        '
        'tabAuditTrail
        '
        Me.tabAuditTrail.AttachedControl = Me.SuperTabControlPanel9
        Me.tabAuditTrail.GlobalItem = False
        Me.tabAuditTrail.Image = CType(resources.GetObject("tabAuditTrail.Image"), System.Drawing.Image)
        Me.tabAuditTrail.Name = "tabAuditTrail"
        Me.tabAuditTrail.Text = "Audit Trail"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.UcDest)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabDestinations
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel6
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Default Destinations"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.TabControl2)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabDefaults
        '
        'TabControl2
        '
        Me.TabControl2.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl2.CanReorderTabs = True
        Me.TabControl2.Controls.Add(Me.TabControlPanel16)
        Me.TabControl2.Controls.Add(Me.TabControlPanel18)
        Me.TabControl2.Controls.Add(Me.TabControlPanel17)
        Me.TabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl2.ForeColor = System.Drawing.Color.Black
        Me.TabControl2.Location = New System.Drawing.Point(0, 0)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl2.SelectedTabIndex = 2
        Me.TabControl2.Size = New System.Drawing.Size(681, 530)
        Me.TabControl2.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock
        Me.TabControl2.TabIndex = 0
        Me.TabControl2.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl2.Tabs.Add(Me.TabItem6)
        Me.TabControl2.Tabs.Add(Me.TabItem7)
        Me.TabControl2.Tabs.Add(Me.TabItem8)
        Me.TabControl2.Text = "TabControl2"
        '
        'TabControlPanel16
        '
        Me.TabControlPanel16.Controls.Add(Me.GroupBox3)
        Me.TabControlPanel16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel16.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel16.Name = "TabControlPanel16"
        Me.TabControlPanel16.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel16.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel16.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel16.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel16.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel16.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel16.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel16.Style.GradientAngle = 90
        Me.TabControlPanel16.TabIndex = 1
        Me.TabControlPanel16.TabItem = Me.TabItem6
        '
        'TabItem6
        '
        Me.TabItem6.AttachedControl = Me.TabControlPanel16
        Me.TabItem6.Name = "TabItem6"
        Me.TabItem6.Text = "Messaging Defaults"
        '
        'TabControlPanel18
        '
        Me.TabControlPanel18.Controls.Add(Me.GroupBox13)
        Me.TabControlPanel18.Controls.Add(Me.GroupBox5)
        Me.TabControlPanel18.Controls.Add(Me.GroupBox10)
        Me.TabControlPanel18.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel18.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel18.Name = "TabControlPanel18"
        Me.TabControlPanel18.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel18.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel18.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel18.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel18.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel18.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel18.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel18.Style.GradientAngle = 90
        Me.TabControlPanel18.TabIndex = 3
        Me.TabControlPanel18.TabItem = Me.TabItem8
        '
        'TabItem8
        '
        Me.TabItem8.AttachedControl = Me.TabControlPanel18
        Me.TabItem8.Name = "TabItem8"
        Me.TabItem8.Text = "Miscellaneous"
        '
        'TabControlPanel17
        '
        Me.TabControlPanel17.Controls.Add(Me.UcLogin)
        Me.TabControlPanel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel17.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel17.Name = "TabControlPanel17"
        Me.TabControlPanel17.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel17.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel17.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel17.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel17.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel17.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel17.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel17.Style.GradientAngle = 90
        Me.TabControlPanel17.TabIndex = 2
        Me.TabControlPanel17.TabItem = Me.TabItem7
        '
        'TabItem7
        '
        Me.TabItem7.AttachedControl = Me.TabControlPanel17
        Me.TabItem7.Name = "TabItem7"
        Me.TabItem7.Text = "Database Defaults"
        '
        'tabDefaults
        '
        Me.tabDefaults.AttachedControl = Me.SuperTabControlPanel5
        Me.tabDefaults.GlobalItem = False
        Me.tabDefaults.Image = CType(resources.GetObject("tabDefaults.Image"), System.Drawing.Image)
        Me.tabDefaults.Name = "tabDefaults"
        Me.tabDefaults.Text = "User Defaults"
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.TabControl4)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(681, 530)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabHouseKeeping
        '
        'TabControl4
        '
        Me.TabControl4.BackColor = System.Drawing.Color.FromArgb(CType(CType(194, Byte), Integer), CType(CType(217, Byte), Integer), CType(CType(247, Byte), Integer))
        Me.TabControl4.CanReorderTabs = True
        Me.TabControl4.Controls.Add(Me.TabControlPanel14)
        Me.TabControl4.Controls.Add(Me.TabControlPanel15)
        Me.TabControl4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl4.ForeColor = System.Drawing.Color.Black
        Me.TabControl4.Location = New System.Drawing.Point(0, 0)
        Me.TabControl4.Name = "TabControl4"
        Me.TabControl4.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl4.SelectedTabIndex = 1
        Me.TabControl4.Size = New System.Drawing.Size(681, 530)
        Me.TabControl4.Style = DevComponents.DotNetBar.eTabStripStyle.Office2007Dock
        Me.TabControl4.TabIndex = 0
        Me.TabControl4.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl4.Tabs.Add(Me.TabItem4)
        Me.TabControl4.Tabs.Add(Me.TabItem5)
        Me.TabControl4.Text = "TabControl4"
        '
        'TabControlPanel14
        '
        Me.TabControlPanel14.Controls.Add(Me.GroupBox8)
        Me.TabControlPanel14.Controls.Add(Me.GroupBox9)
        Me.TabControlPanel14.Controls.Add(Me.cmdRegister)
        Me.TabControlPanel14.Controls.Add(Me.lsvComponents)
        Me.TabControlPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel14.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel14.Name = "TabControlPanel14"
        Me.TabControlPanel14.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel14.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel14.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel14.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel14.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel14.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel14.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel14.Style.GradientAngle = 90
        Me.TabControlPanel14.TabIndex = 1
        Me.TabControlPanel14.TabItem = Me.TabItem4
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel14
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "Components"
        '
        'TabControlPanel15
        '
        Me.TabControlPanel15.Controls.Add(Me.optFileLastModified)
        Me.TabControlPanel15.Controls.Add(Me.GroupBox4)
        Me.TabControlPanel15.Controls.Add(Me.optFileCreated)
        Me.TabControlPanel15.Controls.Add(Me.Label57)
        Me.TabControlPanel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel15.Location = New System.Drawing.Point(0, 23)
        Me.TabControlPanel15.Name = "TabControlPanel15"
        Me.TabControlPanel15.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel15.Size = New System.Drawing.Size(681, 507)
        Me.TabControlPanel15.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.TabControlPanel15.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(157, Byte), Integer), CType(CType(188, Byte), Integer), CType(CType(227, Byte), Integer))
        Me.TabControlPanel15.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel15.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(165, Byte), Integer), CType(CType(199, Byte), Integer))
        Me.TabControlPanel15.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Left Or DevComponents.DotNetBar.eBorderSide.Right) _
            Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel15.Style.GradientAngle = 90
        Me.TabControlPanel15.TabIndex = 2
        Me.TabControlPanel15.TabItem = Me.TabItem5
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel15
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "Folder Housekeeping"
        '
        'tabHouseKeeping
        '
        Me.tabHouseKeeping.AttachedControl = Me.SuperTabControlPanel4
        Me.tabHouseKeeping.GlobalItem = False
        Me.tabHouseKeeping.Image = CType(resources.GetObject("tabHouseKeeping.Image"), System.Drawing.Image)
        Me.tabHouseKeeping.Name = "tabHouseKeeping"
        Me.tabHouseKeeping.Text = "House Keeping"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdApply)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(0, 544)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(850, 31)
        Me.FlowLayoutPanel3.TabIndex = 56
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(141, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(639, 526)
        Me.SuperTabControlPanel7.TabIndex = 0
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.m_forExceptionHandling = False
        Me.UcTasks.m_showAfterType = True
        Me.UcTasks.m_showExpanded = True
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(681, 530)
        Me.UcTasks.TabIndex = 0
        '
        'ucDSN
        '
        Me.ucDSN.BackColor = System.Drawing.Color.Transparent
        Me.ucDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucDSN.ForeColor = System.Drawing.Color.Navy
        Me.ucDSN.Location = New System.Drawing.Point(8, 56)
        Me.ucDSN.m_conString = "|||"
        Me.ucDSN.m_showConnectionType = False
        Me.ucDSN.Name = "ucDSN"
        Me.ucDSN.Size = New System.Drawing.Size(472, 112)
        Me.ucDSN.TabIndex = 0
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(681, 530)
        Me.UcDest.TabIndex = 0
        '
        'UcLogin
        '
        Me.UcLogin.BackColor = System.Drawing.Color.Transparent
        Me.UcLogin.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcLogin.Location = New System.Drawing.Point(4, 7)
        Me.UcLogin.Name = "UcLogin"
        Me.UcLogin.Size = New System.Drawing.Size(416, 128)
        Me.UcLogin.TabIndex = 0
        '
        'frmOptions
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(850, 575)
        Me.Controls.Add(Me.stabOptions)
        Me.Controls.Add(Me.FlowLayoutPanel3)
        Me.Controls.Add(Me.chkUniversal)
        Me.Controls.Add(Me.chkDisableCRParsing)
        Me.Controls.Add(Me.optErrorLog)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Options"
        Me.GroupBox18.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.txtArchive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.grpThreading.ResumeLayout(False)
        Me.grpThreading.PerformLayout()
        CType(Me.txtThreadInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtThreadCount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        CType(Me.txtNumConnections, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.cmbPollIntEB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbPollInt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtDelayBy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpServices.ResumeLayout(False)
        Me.grpNTService.ResumeLayout(False)
        Me.grpSMTP.ResumeLayout(False)
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpMAPI.ResumeLayout(False)
        Me.grpGroupwise.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.grpCRDMail.ResumeLayout(False)
        Me.GroupBox11.ResumeLayout(False)
        Me.grpSMSC.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        CType(Me.txtRefresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtAutoclose, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.stabOptions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabOptions.ResumeLayout(False)
        Me.SuperTabControlPanel12.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel11.ResumeLayout(False)
        Me.SuperTabControlPanel11.PerformLayout()
        Me.SuperTabControlPanel10.ResumeLayout(False)
        Me.SuperTabControlPanel8.ResumeLayout(False)
        Me.SuperTabControlPanel8.PerformLayout()
        CType(Me.txtKeepTemps, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        CType(Me.TabControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl3.ResumeLayout(False)
        Me.TabControlPanel12.ResumeLayout(False)
        Me.TabControlPanel13.ResumeLayout(False)
        Me.TabControlPanel13.PerformLayout()
        Me.grpSMSByRedOxygen.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.grpSMSByDevice.ResumeLayout(False)
        Me.SuperTabControlPanel9.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        CType(Me.TabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl2.ResumeLayout(False)
        Me.TabControlPanel16.ResumeLayout(False)
        Me.TabControlPanel18.ResumeLayout(False)
        Me.TabControlPanel17.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        CType(Me.TabControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl4.ResumeLayout(False)
        Me.TabControlPanel14.ResumeLayout(False)
        Me.TabControlPanel15.ResumeLayout(False)
        Me.TabControlPanel15.PerformLayout()
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Sub loadDropboxAccounts()
        Try
            lsvCloud.Items.Clear()

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM dropboxattr")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim item As ListViewItem = New ListViewItem(CType(oRs("accountname").Value, String))
                    item.SubItems.Add("Dropbox")
                    item.ImageIndex = 0
                    lsvCloud.Items.Add(item)
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch : End Try
    End Sub

    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error Resume Next
        Dim I As Integer = 0
        Dim oSec As New clsMarsSecurity
        FormatForWinXP(Me)

        loadDropboxAccounts()

        setupForDragAndDrop(txtDefaultParameterName)
        setupForDragAndDrop(txtParameterDefaultValue)

        stabOptions.SelectedTab = tabGeneral

        txtParameterDefaultValue.ContextMenu = mnuInserter

        UcDest.DefaultDestinations = True

        With UcTasks
            .ScheduleID = 2777
            .oAuto = False
            .ShowAfterType = False
            .LoadTasks()
            .m_eventBased = False
            .DefaultTaskToolStripMenuItem.Visible = False
            .m_defaultTaks = True
        End With

        If gConType <> "DAT" Then
            chkCompact.Enabled = False
            GroupBox8.Enabled = False
        End If

        cmbPollInt.Text = oUI.ReadRegistry("Poll", 30)
        cmbSMTPTimeout.Text = oUI.ReadRegistry("SMTPTimeout", 30)
        chkThreading.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDThreading", 0)))
        chkCompact.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotAutoCompact", 0)))
        chkOutofProcess.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("RunOutOfProcess", 1)))
        chkDTRefresh.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DTRefresh", 0)))
        txtRefresh.Value = oUI.ReadRegistry("DTRefreshInterval", "180")

        chkAutoUpdates.Checked = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("CheckForUpdates", 1))
        chkLoadSSRSParametersAutomatically.Checked = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("LoadSSRSParametersAutomatically", 1))

        chkUNC.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotUNC", 1)))
        Me.chkDisableCRParsing.Checked = False 'Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotParseReportFields", 0)))

        Me.chkShowHiddenParameters.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShowHiddenParameters", 0)))
        chkShareUserDefaults.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShareUserDefaults", 0)))

        Dim sErrorHandle As String = oUI.ReadRegistry("ErrorHandle", "")

        If sErrorHandle.Length > 0 Then
            Select Case sErrorHandle
                Case "Log"
                    optErrorLog.Checked = True
                Case "Email"
                    optErrorMail.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case "Both"
                    optErrorMail.Checked = True
                    optErrorLog.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case "SMS"
                    optErrorSMS.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case Else
                    optErrorLog.Checked = True
            End Select

            txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")

            'oUI._DeleteRegistry(oReg, sKey, "ErrorHandle")
        Else
            optErrorLog.Checked = oUI.ReadRegistry("ErrorByLog", 1)
            optErrorMail.Checked = oUI.ReadRegistry("ErrorByMail", 0)
            optErrorSMS.Checked = oUI.ReadRegistry("ErrorBySMS", 0)

            txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
            txtAlertSMS.Text = oUI.ReadRegistry("SMSAlertWho", "")
        End If


        'NT Scheduler values

        'try the new options
        Select Case oUI.ReadRegistry("MailType", "NONE")
            Case "NONE"
                cmbMailType.Text = "NONE"
                grpSMTP.Visible = False
                grpMAPI.Visible = False
            Case "MAPI"
                cmbMailType.Text = "MAPI"
                grpMAPI.Visible = True
                grpMAPI.BringToFront()
            Case "SMTP"
                cmbMailType.Text = "SMTP"
                grpSMTP.Visible = True
                grpSMTP.BringToFront()
            Case "GROUPWISE"
                cmbMailType.Text = "GROUPWISE"
                grpGroupwise.Visible = True
                grpGroupwise.BringToFront()
            Case "SQLRDMAIL"
                cmbMailType.Text = "SQLRDMAIL"

                Dim isActivated As Boolean = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDMailActivation", "0")))

                If isActivated = True Then
                    txtCRDAddress.ReadOnly = False
                    txtCRDSender.ReadOnly = False
                Else
                    txtCRDAddress.ReadOnly = True
                    txtCRDSender.ReadOnly = True
                End If
        End Select

        chkUseAsBackupScheduler.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("UseAsBackupScheduler", 0)))

        chkEmailRetry.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("EmailRetry", 0)))

        Select Case oUI.ReadRegistry("SQL-RDService", "NONE")
            Case "NONE"
                optNoScheduling.Checked = True
                GroupBox16.Enabled = False
                GroupBox2.Enabled = False
                chkCheckService.Checked = False
            Case "WindowsApp"
                optBackgroundApp.Checked = True
                chkCheckService.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("CheckScheduler", 0)))
            Case "WindowsNT"
                optNTService.Checked = True
                chkCheckService.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("CheckScheduler", 0)))

                With oUI
                    txtWinUser.Text = .ReadRegistry("NTLoginName", "")
                    txtWinPassword.Text = .ReadRegistry("NTLoginPassword", "", True)
                    txtWinDomain.Text = .ReadRegistry("NTDomain", "")
                End With
        End Select

        Me.m_scheduleStatus = clsServiceController.itemGlobal.m_serviceStatus

        chkBlackout.Checked = Convert.ToInt32(oUI.ReadRegistry("SchedulerBlackouts", 0))

        chkUniversal.Checked = True
        cmbPriority.SelectedIndex = cmbPriority.Items.IndexOf(oUI.ReadRegistry("CPU Priority", "Normal"))
        txtThreadCount.Value = oUI.ReadRegistry("ThreadCount", 4)
        chkUseRelTime.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("Use Relative Time", 0)))
        Me.chkDelayRestart.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DelayRestart", 0)))
        Me.txtDelayBy.Value = Convert.ToInt32(oUI.ReadRegistry("DelayRestartBy", 0))
        txtThreadInterval.Value = oUI.ReadRegistry("ThreadInterval", 15)
        chkPrecheckconditions.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("PreCheckConditions", 0)))
        txtNumConnections.Value = oUI.ReadRegistry("NumOfConnections", 5)

        chkAutoclose.Checked = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AutoCloseErrors", 0)))
        txtAutoclose.Value = clsMarsUI.MainUI.ReadRegistry("AutoCloseErrorValue", 30)

        'If gConType = "DAT" Then
        '    chkUseRelTime.Checked = False
        '    chkUseRelTime.Enabled = False
        'End If

        Me.cmbPollIntEB.Value = oUI.ReadRegistry("PollEB", cmbPollInt.Value)
        Me.cmbPriorityEB.Text = oUI.ReadRegistry("CPU Priority EB", "Normal")

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.e1_BasicEventsPack) = False Then
            cmbPollIntEB.Enabled = False
            cmbPriorityEB.Enabled = False
        Else
            cmbPollIntEB.Enabled = True
            cmbPriorityEB.Enabled = True
        End If

        txtCustomerNo.Text = oUI.ReadRegistry("CustNo", "")
        txtTempFolder.Text = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

        'cmbAgedValue.Text = oUI._ReadRegistry(oReg, sKey, "AgedValue", "28")
        'cmbAgedUnit.Text = oUI._ReadRegistry(oReg, sKey, "AgedUnit", "Days")

        If chkShareUserDefaults.Checked Then
            loadUserDefaultsFromServer()
        Else
            txtDefaultEmail.Text = oUI.ReadTextFile(sAppPath & "defmsg.sqlrd")
            txtSig.Text = oUI.ReadTextFile(sAppPath & "defsig.sqlrd")
            txtDefSubject.Text = oUI.ReadRegistry("DefSubject", "")
            txtDefAttach.Text = oUI.ReadRegistry("DefAttach", "")
        End If


        cmdApply.Enabled = False
        cmdOK.Enabled = True
        cmdServiceApply.Enabled = False
        cmdServiceStart.Enabled = False

        'messaging
        txtSMTPUserID.Text = oUI.ReadRegistry("SMTPUserID", "")
        txtSMTPPassword.Text = oUI.ReadRegistry("SMTPPassword", "", True)
        txtSMTPServer.Text = oUI.ReadRegistry("SMTPServer", "")
        txtSMTPSenderAddress.Text = oUI.ReadRegistry("SMTPSenderAddress", "")
        txtSMTPSenderName.Text = oUI.ReadRegistry("SMTPSenderName", "")
        txtSig.Text = oUI.ReadTextFile(sAppPath & "defsig.sqlrd")
        cmbMAPIProfile.Text = oUI.ReadRegistry("MAPIProfile", "")
        txtMAPIPassword.Text = oUI.ReadRegistry("MAPIPassword", "", True)

        txtCRDSender.Text = oUI.ReadRegistry("SQL-RDSender", "")
        txtCRDAddress.Text = oUI.ReadRegistry("SQL-RDAddress", "")

        cmbEncoding.Text = oUI.ReadRegistry("CharSetEncoding", "US-ASCII")

        chkAutoEmbedImages.Checked = Convert.ToInt32(oUI.ReadRegistry("AutoEmbedImagesIntoHTMLEmails", 1))

        'sms options
        With oUI
            Dim smsType As String = .ReadRegistry("SMSType", "Device")

            If smsType = "Device" Then
                optSMSByDevice.Checked = True
            Else
                optSMSByRedOxygen.Checked = True
            End If

            cmbSMSDevice.Text = .ReadRegistry("SMSDevice", "")
            cmbSMSFormat.Text = .ReadRegistry("SMSDataFormat", "8,n,1")
            cmbSMSSpeed.Text = .ReadRegistry("SMSSpeed", "9600")
            txtSMSInit.Text = .ReadRegistry("SMSInit", "AT+MS=V22B")

            If .ReadRegistry("SMSDeviceType", "SMSC") = "GSM" Then
                optUseGSM.Checked = True
            Else
                optUseSMSC.Checked = True
            End If

            txtSMSNumber.Text = .ReadRegistry("SMSCNumber", "")
            cmbSMSProtocol.Text = .ReadRegistry("SMSCProtocol", "TAP")
            txtSMSPassword.Text = .ReadRegistry("SMSCPassword", "", True)
            txtSMSSender.Text = .ReadRegistry("SMSSender", "")
            chkUnicode.Checked = .ReadRegistry("UseUnicode", 0)


            '//redoxygen
            txtROAccountID.Text = .ReadRegistry("ROAccountID", "")
            txtROEmail.Text = .ReadRegistry("ROEmail", "")
            txtROPassword.Text = .ReadRegistry("ROPassword", "", True)

        End With

        chkArchive.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("Archive", 0)))
        txtArchive.Value = oUI.ReadRegistry("ArchiveAfter", 0)

        With UcLogin
            .txtUserID.Text = oUI.ReadRegistry("DefDBUser", "", True)
            .txtPassword.Text = oUI.ReadRegistry("DefPassword", "", True)
        End With


        'audit trail
        chkAudit.Checked = oUI.ReadRegistry("AuditTrail", 0)

        If chkAudit.Checked = True Then
            ucDSN.cmbDSN.Text = oUI.ReadRegistry("AuditDSN", "")
            ucDSN.txtUserID.Text = oUI.ReadRegistry("AuditUserName", "")
            ucDSN.txtPassword.Text = oUI.ReadRegistry("AuditPassword", "", True)
        End If

        'for excel options

        txtReportCache.Text = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

        Me.txtSentMessages.Text = oUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages\")

        'autocompact
        If gConType = "DAT" Then
            chkAutoCompact.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AutoCompact", 0)))
            dtAutoCompact.Value = oUI.ReadRegistry("CompactDue", Now.Date & " 18:30:00")
        Else
            chkAutoCompact.Checked = False
            dtAutoCompact.Value = Now.Date & " 18:30:00"
        End If

        ShowCompactMsg = True

        UcDest.nPackID = 2777
        UcDest.nReportID = 2777
        UcDest.nSmartID = 2777
        UcDest.cmdImport.Visible = False
        UcDest.m_CanDisable = False
        UcDest.LoadAll()

        'user defaults
        UcLogin.txtPassword.Text = oUI.ReadRegistry("DefPassword", "", True)
        txtReportLoc.Text = oUI.ReadRegistry("ReportsLoc", "http://sqlrdsamples.christiansteven.com/reportserver/reportservice.asmx")
        cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

        Dim cacheFile As String = sAppPath & "url.dat"
        Dim cache As String = IsNull(ReadTextFromFile(cacheFile))

        For Each s As String In cache.Split("|")
            If s <> "" Then
                lsvServers.Items.Add(s)
            End If
        Next

        lsvServers.Sorting = SortOrder.Ascending
        lsvServers.Sort()

        Me._LoadPaths()


        If oUI.ReadRegistry("CalculateFileAgeUsing", "DateFileCreated") = "DateFileCreated" Then
            optFileCreated.Checked = True
        Else
            optFileLastModified.Checked = True
        End If

        'system paths
        txtCachedData.Text = oUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Data\")
        txtSnapShots.Text = oUI.ReadRegistry("SnapshotsPath", sAppPath & "Snapshots\")
        Me.chkConvertToMsg.Checked = Convert.ToInt32(oUI.ReadRegistry("ConvertToMsg", 0))

        chkEmailRetry.Enabled = True
        cmdOK.Enabled = True

        getDefaultParameters()
        isLoaded = True
    End Sub

    Private Sub _LoadPaths()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        'transfer from registry into table
        Dim sPath As String
        Dim sAge As String
        Dim sUnit As String

        sPath = oUI.ReadRegistry(oReg, sKey, "TempFolder", "")
        sAge = oUI.ReadRegistry(oReg, sKey, "AgedValue", "")
        sUnit = oUI.ReadRegistry(oReg, sKey, "AgedUnit", "")

        If sUnit.Length > 0 Then
            SQL = "INSERT INTO HouseKeepingPaths (EntryID,FolderPath,KeepFor,KeepUnit) VALUES (" & _
            clsMarsData.CreateDataID("housekeepingpaths", "entryid") & "," & _
            "'" & SQLPrepare(sPath) & "'," & _
            sAge & "," & _
            "'" & sUnit & "')"

            If clsMarsData.WriteData(SQL) = True Then
                oUI.DeleteRegistry(oReg, sKey, "AgedValue")
                oUI.DeleteRegistry(oReg, sKey, "AgedUnit")
            End If
        End If

        SQL = "SELECT * FROM HouseKeepingPaths"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            lsvPaths.Items.Clear()

            Do While oRs.EOF = False
                Dim entryID As Integer = oRs("entryid").Value

                If entryID <> 27 Then
                    oItem = New ListViewItem

                    oItem.Tag = oRs("entryid").Value
                    oItem.Text = oRs("folderpath").Value
                    oItem.SubItems.Add(oRs("keepfor").Value & " " & oRs("keepunit").Value)

                    lsvPaths.Items.Add(oItem)
                Else
                    Me.txtKeepTemps.Value = oRs("keepfor").Value
                    Me.cmbKeepTempUnit.Text = oRs("keepunit").Value

                    Me.keepTemp = Me.txtKeepTemps.Value & " " & Me.cmbKeepTempUnit.Text
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub
    Private Sub cmbMailType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMailType.SelectedIndexChanged
        On Error Resume Next
        Dim oSec As clsMarsSecurity = New clsMarsSecurity
        cmdApply.Enabled = True

        oUI.ResetError(sender, ep)

        Select Case cmbMailType.Text
            Case "SMTP"
                grpSMTP.Visible = True
                grpMAPI.Visible = False
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False

                grpSMTP.BringToFront()
                txtSMTPUserID.Text = oUI.ReadRegistry("SMTPUserID", "")
                txtSMTPPassword.Text = oUI.ReadRegistry("SMTPPassword", "", True)
                txtSMTPServer.Text = oUI.ReadRegistry("SMTPServer", "")
                txtSMTPSenderAddress.Text = oUI.ReadRegistry("SMTPSenderAddress", "")
                txtSMTPSenderName.Text = oUI.ReadRegistry("SMTPSenderName", "")
                txtSMTPPort.Value = oUI.ReadRegistry("SMTPPort", 25)

                chkEmailRetry.Enabled = True
                cmdClearSettings.Enabled = True
                cmdMailTest.Enabled = True
                GroupBox11.Enabled = True
            Case "MAPI"
                grpMAPI.Visible = True
                grpSMTP.Visible = False
                grpMAPI.BringToFront()
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False
                cmbMAPIProfile.Text = oUI.ReadRegistry("MAPIProfile", "")
                txtMAPIPassword.Text = oUI.ReadRegistry("MAPIPassword", "", True)

                chkEmailRetry.Enabled = True
                cmdClearSettings.Enabled = True
                cmdMailTest.Enabled = True
                GroupBox11.Enabled = True
            Case "SQLRDMAIL"
                txtCRDSender.Text = oUI.ReadRegistry("SQL-RDSender", "")
                txtCRDAddress.Text = oUI.ReadRegistry("SQL-RDAddress", "")
                cmdMailTest.Enabled = True
                cmdApply.Enabled = False
                cmdOK.Enabled = False
                cmdActivate.Visible = True

                grpCRDMail.BringToFront()
                grpCRDMail.Visible = True

                grpMAPI.Visible = False
                grpSMTP.Visible = False
                grpGroupwise.Visible = False

                If gnEdition = MarsGlobal.gEdition.EVALUATION Then
                    txtCRDAddress.Text = "evaluation@crystalreportsdistributor.com"
                    txtCRDSender.Text = "SQL-RD Evaluation"
                    txtCRDAddress.ReadOnly = True
                    txtCRDSender.ReadOnly = True
                End If

                cmdClearSettings.Visible = True
                chkEmailRetry.Visible = True
                GroupBox11.Enabled = True
            Case "GROUPWISE"
                Me.txtgwUser.Text = oUI.ReadRegistry("GWUser", "")
                Me.txtgwPassword.Text = oUI.ReadRegistry("GWPassword", "", True)
                Me.txtgwProxy.Text = oUI.ReadRegistry("GWProxy", "")
                Me.txtgwPOIP.Text = oUI.ReadRegistry("GWPOIP", "")
                Me.txtgwPOPort.Text = oUI.ReadRegistry("GWPOPort", "")

                grpGroupwise.Visible = True
                grpGroupwise.BringToFront()

                cmdClearSettings.Visible = True
                chkEmailRetry.Visible = True
                GroupBox11.Enabled = True

                grpMAPI.Visible = False
                grpSMTP.Visible = False
                grpCRDMail.Visible = False
            Case "NONE"
                grpSMTP.Visible = False
                grpMAPI.Visible = False
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False
                chkEmailRetry.Enabled = False
                chkEmailRetry.Checked = False
                cmdClearSettings.Enabled = False
                cmdMailTest.Enabled = False
                GroupBox11.Enabled = False
        End Select
    End Sub

    Private Sub cmbPollInt_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        On Error Resume Next
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub txtCustomerNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomerNo.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkThreading_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkThreading.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False And chkThreading.Checked Then
            If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa1_MultiThreading))
            chkThreading.Checked = False
        End If

        cmdApply.Enabled = True

        txtThreadCount.Enabled = chkThreading.Checked
        txtThreadInterval.Enabled = chkThreading.Checked

        If chkThreading.Checked = False Then
            txtThreadCount.Minimum = 1
            txtThreadCount.Value = 1
        Else
            txtThreadCount.Value = 2
            txtThreadCount.Minimum = 2
        End If


    End Sub

    Private Sub optErrorLog_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorLog.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub optErrorMail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorMail.CheckedChanged
        On Error Resume Next
        cmdApply.Enabled = True

        txtErrorAlertWho.Enabled = optErrorMail.Checked
        cmdAlertWho.Enabled = optErrorMail.Checked
    End Sub

    Private Sub chkEmailRetry_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmailRetry.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtTempFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTempFolder.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub



    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        On Error Resume Next
        Dim sErrorHandle As String
        Dim nMailType As gMailType
        Dim oSec As clsMarsSecurity = New clsMarsSecurity

        If skipValidation = False Then
            'validate user entry
            If cmbPollInt.Text = "" Then
                stabOptions.SelectedTabIndex = 2
                ep.SetError(cmbPollInt, "Please enter the polling interval")
                cmbPollInt.Focus()
                Exit Sub
            ElseIf optErrorMail.Checked = True And txtErrorAlertWho.Text = "" Then
                stabOptions.SelectedTabIndex = 0
                ep.SetError(txtErrorAlertWho, "Please enter the email address for the error alerts")
                txtErrorAlertWho.Focus()
                Exit Sub
            ElseIf optErrorSMS.Checked = True And txtAlertSMS.Text.Length = 0 Then
                stabOptions.SelectedTabIndex = 0
                ep.SetError(txtAlertSMS, "Please enter the cellphone number for the error alerts")
                txtAlertSMS.Focus()
                Exit Sub
            ElseIf cmbMailType.Text = "" Then
                stabOptions.SelectedTabIndex = 1
                ep.SetError(cmbMailType, "Please select the mail type")
                cmbMailType.Focus()
                Exit Sub
            ElseIf cmbMailType.Text = "MAPI" Then
                If cmbMAPIProfile.Text = "" Then
                    stabOptions.SelectedTabIndex = 1
                    ep.SetError(cmbMAPIProfile, "Please enter the MAPI profile")
                    cmbMAPIProfile.Focus()
                    Exit Sub
                ElseIf txtMAPIPassword.Text = "" Then
                    stabOptions.SelectedTabIndex = 1
                    ep.SetError(txtMAPIPassword, "Please enter your Windows NT password here")
                    txtMAPIPassword.Focus()
                End If
            ElseIf cmbMailType.Text = "SMTP" Then
                If txtSMTPServer.Text = "" Then
                    stabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPServer, "Please enter the SMTP Server name here")
                    txtSMTPServer.Focus()
                    Exit Sub
                ElseIf txtSMTPSenderAddress.Text = "" Then
                    stabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPSenderAddress, "Please enter your sender address")
                    txtSMTPSenderAddress.Focus()
                    Exit Sub
                ElseIf txtSMTPSenderName.Text = "" Then
                    stabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPSenderName, "Please enter your sender name")
                    txtSMTPSenderName.Focus()
                    Exit Sub
                End If
            ElseIf txtTempFolder.Text = "" Then
                stabOptions.SelectedTabIndex = 7
                ep.SetError(txtTempFolder, "Please specify the temporary output folder")
                txtTempFolder.Focus()
                Exit Sub
            ElseIf optSMSByRedOxygen.Checked Then
                If txtROAccountID.Text = "" Then
                    setError(txtROAccountID, "Please enter your RedOxygen AccountID", ErrorIconAlignment.MiddleRight)
                    txtROAccountID.Focus()
                    Return
                ElseIf txtROEmail.Text = "" Then
                    setError(txtROEmail, "Please enter the email you used to sign up with RedOxygen", ErrorIconAlignment.MiddleRight)
                    txtROEmail.Focus()
                    Return
                ElseIf txtROPassword.Text = "" Then
                    setError(txtROPassword, "Please enter your RedOxygen Password", ErrorIconAlignment.MiddleRight)
                    txtROPassword.Focus()
                    Return
                End If
            ElseIf chkAudit.Checked = True Then
                If ucDSN._Validate = False Then
                    stabOptions.SelectedTabIndex = 8
                    Exit Sub
                End If
            End If

            If optErrorLog.Checked = True Then
                sErrorHandle = "Log"
            ElseIf optErrorMail.Checked = True Then
                sErrorHandle = "Email"
            ElseIf optErrorSMS.Checked = True Then
                sErrorHandle = "SMS"
            Else
                sErrorHandle = "Both"
            End If

            If cmbMailType.Text.ToLower = "none" Then
                nMailType = MarsGlobal.gMailType.NONE
                MailType = MarsGlobal.gMailType.NONE
            ElseIf cmbMailType.Text.ToLower = "mapi" Then
                nMailType = MarsGlobal.gMailType.MAPI
                MailType = MarsGlobal.gMailType.MAPI
            ElseIf cmbMailType.Text.ToLower = "smtp" Then
                nMailType = MarsGlobal.gMailType.SMTP
                MailType = MarsGlobal.gMailType.SMTP
            ElseIf cmbMailType.Text.ToLower = "sqlrdmail" Then
                nMailType = MarsGlobal.gMailType.SQLRDMAIL
                MailType = MarsGlobal.gMailType.SQLRDMAIL
            ElseIf cmbMailType.Text.ToLower = "groupwise" Then
                nMailType = gMailType.GROUPWISE
                MailType = gMailType.GROUPWISE
            End If
        End If

        Dim SMTPManager As Boolean

        If MailType = gMailType.SMTP Then
            SMTPManager = True
        Else
            SMTPManager = False
        End If

        With oUI
            'general page
            .SaveRegistry("Poll", cmbPollInt.Text)
            .SaveRegistry("CustNo", txtCustomerNo.Text)
            .SaveRegistry("SQL-RDThreading", Convert.ToInt32(chkThreading.Checked))
            .SaveRegistry("AlertWho", txtErrorAlertWho.Text)
            .SaveRegistry("DoNotAutoCompact", Convert.ToInt32(chkCompact.Checked))
            .SaveRegistry("RunOutOfProcess", Convert.ToInt32(chkOutofProcess.Checked))

            .SaveRegistry("DTRefresh", Convert.ToInt32(chkDTRefresh.Checked))
            .SaveRegistry("DTRefreshInterval", txtRefresh.Value)
            .SaveRegistry("DoNotUNC", Convert.ToInt32(chkUNC.Checked))
            .SaveRegistry("DoNotParseReportFields", Convert.ToInt32(chkDisableCRParsing.Checked))
            .SaveRegistry("ShareUserDefaults", Convert.ToInt32(chkShareUserDefaults.Checked))
            .SaveRegistry("AutoCloseErrors", Convert.ToInt32(chkAutoclose.Checked))
            .SaveRegistry("AutoCloseErrorValue", txtAutoclose.Value)
            .SaveRegistry("CheckForUpdates", Convert.ToInt32(chkAutoUpdates.Checked))
            .SaveRegistry("LoadSSRSParametersAutomatically", Convert.ToInt32(chkLoadSSRSParametersAutomatically.Checked))

            If chkShareUserDefaults.Checked Then
                saveUserDefaultsToServer()
            End If
            'hidden parameters setting
            .SaveRegistry("ShowHiddenParameters", Convert.ToInt32(Me.chkShowHiddenParameters.Checked))

            'new error handling
            .SaveRegistry("ErrorByLog", Convert.ToInt32(optErrorLog.Checked))
            .SaveRegistry("ErrorByMail", Convert.ToInt32(optErrorMail.Checked))
            .SaveRegistry("ErrorBySMS", Convert.ToInt32(optErrorSMS.Checked))
            .SaveRegistry("SMSAlertWho", txtAlertSMS.Text)

            CustID = txtCustomerNo.Text

            'messaging page
            .SaveRegistry("MailType", cmbMailType.Text)
            .SaveRegistry("MAPIProfile", cmbMAPIProfile.Text)
            .SaveRegistry("MAPIPassword", txtMAPIPassword.Text, True)

            If .ReadRegistry("MAPIType", "") = "" Then
                .SaveRegistry("MAPIType", "Single")
            End If

            .SaveRegistry("EmailRetry", Convert.ToString(Convert.ToInt32(chkEmailRetry.Checked)))
            .SaveRegistry("SMTPUserID", txtSMTPUserID.Text)
            .SaveRegistry("SMTPPassword", txtSMTPPassword.Text, True)
            .SaveRegistry("SMTPServer", txtSMTPServer.Text)
            .SaveRegistry("SMTPSenderAddress", txtSMTPSenderAddress.Text)
            .SaveRegistry("SMTPSenderName", txtSMTPSenderName.Text)
            .SaveRegistry("SMTPTimeout", cmbSMTPTimeout.Text)
            .SaveRegistry("SMTPPort", Me.txtSMTPPort.Value)

            .SaveRegistry("SQL-RDSender", txtCRDSender.Text)
            .SaveRegistry("SQL-RDAddress", txtCRDAddress.Text)

            .SaveRegistry("GWUser", Me.txtgwUser.Text)
            .SaveRegistry("GWPassword", Me.txtgwPassword.Text, True)
            .SaveRegistry("GWProxy", Me.txtgwProxy.Text)
            .SaveRegistry("GWPOIP", Me.txtgwPOIP.Text)
            .SaveRegistry("GWPOPort", Me.txtgwPOPort.Text)

            .SaveRegistry("CharSetEncoding", cmbEncoding.Text)
            .SaveRegistry("AutoEmbedImagesIntoHTMLEmails", Convert.ToInt32(chkAutoEmbedImages.Checked))

            'sms options
            If optSMSByDevice.Checked Then
                .SaveRegistry("SMSType", "Device")
            Else
                .SaveRegistry("SMSType", "RedOxygen")
            End If

            .SaveRegistry("SMSDevice", cmbSMSDevice.Text)
            .SaveRegistry("SMSDataFormat", cmbSMSFormat.Text)
            .SaveRegistry("SMSSpeed", cmbSMSSpeed.Text)
            .SaveRegistry("SMSSInit", txtSMSInit.Text)

            If optUseGSM.Checked = True Then
                .SaveRegistry("SMSDeviceType", "GSM")
            Else
                .SaveRegistry("SMSDeviceType", "SMSC")
            End If

            .SaveRegistry("SMSCNumber", txtSMSNumber.Text)
            .SaveRegistry("SMSCProtocol", cmbSMSProtocol.Text)
            .SaveRegistry("SMSCPassword", txtSMSPassword.Text, True)
            .SaveRegistry("SMSSender", txtSMSSender.Text)
            .SaveRegistry("UseUnicode", Convert.ToInt32(chkUnicode.Checked))

            '//redoxygen sms settings
            .SaveRegistry("ROAccountID", txtROAccountID.Text)
            .SaveRegistry("ROEmail", txtROEmail.Text)
            .SaveRegistry("ROPassword", txtROPassword.Text, True)

            'scheduler
            .SaveRegistry("CheckScheduler", Convert.ToInt32(chkCheckService.Checked))
            .SaveRegistry("UniversalTime", Convert.ToInt32(chkUniversal.Checked))
            .SaveRegistry("CPU Priority", cmbPriority.Text)
            .SaveRegistry("ThreadCount", txtThreadCount.Value)
            .SaveRegistry("PollEB", Me.cmbPollIntEB.Value)
            .SaveRegistry("CPU Priority EB", Me.cmbPriorityEB.Text)
            .SaveRegistry("Use Relative Time", Convert.ToInt32(chkUseRelTime.Checked))
            .SaveRegistry("DelayRestart", Convert.ToInt32(Me.chkDelayRestart.Checked))
            .SaveRegistry("DelayRestartBy", Me.txtDelayBy.Value)
            .SaveRegistry("ThreadInterval", txtThreadInterval.Value)
            .SaveRegistry("PreCheckConditions", Convert.ToInt32(chkPrecheckconditions.Checked))
            .SaveRegistry("NumOfConnections", txtNumConnections.Value)
            .SaveRegistry("SchedulerBlackouts", Convert.ToInt32(chkBlackout.Checked))
            .SaveRegistry("UseAsBackupScheduler", Convert.ToInt32(chkUseAsBackupScheduler.Checked))
            'housekeeping

            .SaveRegistry("TempFolder", _CreateUNC(txtTempFolder.Text))

            If optFileCreated.Checked Then
                .SaveRegistry("CalculateFileAgeUsing", "DateFileCreated")
            Else
                .SaveRegistry("CalculateFileAgeUsing", "DateFileLastModified")
            End If

            If UpdateAutoCompact = True Then
                If chkAutoCompact.Checked = True Then
                    .SaveRegistry("AutoCompact", 1)
                Else
                    .SaveRegistry("AutoCompact", 0)
                End If

                Dim CompactDue As String

                CompactDue = ConDateTime(Now.Date & " " & dtAutoCompact.Text)

                .SaveRegistry("CompactDue", CompactDue)
            End If

            'user defaults
            SaveTextToFile(txtDefaultEmail.Text, sAppPath & "defmsg.sqlrd", , , False)
            SaveTextToFile(txtSig.Text, sAppPath & "defsig.sqlrd", , , False)
            .SaveRegistry("DefSubject", txtDefSubject.Text)
            .SaveRegistry("DefAttach", txtDefAttach.Text)
            .SaveRegistry("Archive", Convert.ToInt32(chkArchive.Checked))
            .SaveRegistry("ArchiveAfter", txtArchive.Value)
            .SaveRegistry("DefDateTimeStamp", cmbDateTime.Text)
            .SaveRegistry("DefDBUser", Me.UcLogin.txtUserID.Text)
            .SaveRegistry("DefPassword", Me.UcLogin.txtPassword.Text)

            .SaveRegistry("ReportsLoc", txtReportLoc.Text)

            Dim urloutput As String = ""

            For Each item As ListViewItem In lsvServers.Items
                If item.Text <> "" Then
                    urloutput &= item.Text & "|"
                End If
            Next

            SaveTextToFile(urloutput, sAppPath & "url.dat", , False, False)

            'security page
            .SaveRegistry("AuditTrail", Convert.ToInt32(chkAudit.Checked))


            If chkAudit.Checked = True Then
                .SaveRegistry("AuditDSN", ucDSN.cmbDSN.Text)
                .SaveRegistry("AuditUserName", ucDSN.txtUserID.Text)
                .SaveRegistry("AuditPassword", ucDSN.txtPassword.Text, True)
            End If

            'system path
            .SaveRegistry("CachedDataPath", txtCachedData.Text)
            .SaveRegistry("SnapshotsPath", txtSnapShots.Text)
            .SaveRegistry("CachePath", Me.txtReportCache.Text)
            .SaveRegistry("SentMessagesFolder", Me.txtSentMessages.Text)
            .SaveRegistry("ConvertToMsg", Convert.ToInt32(Me.chkConvertToMsg.Checked))

            'if the keep temp items values have changed then lets save them
            If Me.txtKeepTemps.Value & " " & Me.cmbKeepTempUnit.Text <> Me.keepTemp Then
                'we are using 27 to identify the special temp folder entry in this here folder
                clsMarsData.WriteData("DELETE FROM HouseKeepingPaths WHERE EntryID = 27")

                Dim cols, vals As String

                cols = "EntryID,FolderPath,KeepFor,KeepUnit"

                vals = 27 & "," & _
                "'" & SQLPrepare(Me.txtTempFolder.Text) & "'," & _
                Me.txtKeepTemps.Value & "," & _
                "'" & Me.cmbKeepTempUnit.Text & "'"

                clsMarsData.DataItem.InsertData("HouseKeepingPaths", cols, vals, False)
            End If
        End With

        setAndShowMessageOnControl(cmdApply, "Saved!", "", "The settings have been saved successfully!")



        oUI.BusyProgress(, , True)


    End Sub


    Private Sub cmdMailTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMailTest.Click

        Dim sErr As String
        Dim vbCrLf = Environment.NewLine

        Select Case cmbMailType.Text.ToLower
            Case "mapi"
                Dim sTestAddress As String

                sTestAddress = InputBox("Please enter an email address to send to", _
               Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                oUI.SaveRegistry("TestSMTPAddress", _
                sTestAddress)

                If sTestAddress = "" Then Exit Sub

                Dim UserID As String = cmbMAPIProfile.Text
                Dim Password As String = Me.txtMAPIPassword.Text
                Dim MAPIType As String = clsMarsUI.MainUI.ReadRegistry("MAPIType", "")
                Dim sAlias As String = clsMarsUI.MainUI.ReadRegistry("MAPIAlias", "")
                Dim sxServer As String = clsMarsUI.MainUI.ReadRegistry("MAPIServer", "")
                Dim exchangeWDSUrl As String = oUI.ReadRegistry("ExchangeWDSUrl", "")
                Dim exchangeWDSUserID As String = oUI.ReadRegistry("ExchangeWDSUserID", "")
                Dim exchangeWDSPassword As String = oUI.ReadRegistry("ExchangeWDSPassword", "", True, )
                Dim exchangeWDSSender As String = oUI.ReadRegistry("ExchangeWDSSender", "")
                Dim exchangeWDSSenderAddress As String = oUI.ReadRegistry("exchangeWDSSenderAddress", "")

                Try
                    Dim cdoSession As Object = CreateObject("Redemption.RDOSession") ' MAPI.Session = New MAPI.Session

                    Select Case MAPIType
                        Case "Single", "Server"
                            If MAPIType = "Single" Then
                                cdoSession.Logon(UserID, Password, False, True)
                            Else
                                cdoSession.LogonExchangeMailbox(sAlias, sxServer)
                            End If

                            Dim Outbox As Object = cdoSession.GetDefaultFolder(4)
                            Dim cdoMessage As Object = Outbox.Items.Add

                            With cdoMessage
                                .To = sTestAddress
                                .Body = "This is a test MAPI email sent by SQL-RD"
                                .Subject = "Test MAPI Message from SQL-RD"

                                Dim Utils As Redemption.MAPIUtils = New Redemption.MAPIUtils

                                .Recipients.ResolveAll()
                                .Save()
                                .Send()

                                Utils.DeliverNow()
                                Utils.Cleanup()
                                Utils = Nothing
                            End With

                            cdoSession = Nothing
                            cdoMessage = Nothing
                        Case "ExchangeWDS"
                            Dim exchange As exchangeWDS.exchangeMail

                            If exchangeWDSUserID = "" Then
                                exchange = New exchangeWDS.exchangeMail(exchangeWDSUrl)
                            Else
                                Dim exdomain As String = ""
                                Dim exuserid As String = ""

                                If exchangeWDSUserID.Contains("\") Then
                                    exuserid = exchangeWDSUserID.Split("\")(1)
                                    exdomain = exchangeWDSUserID.Split("\")(0)
                                Else
                                    exuserid = exchangeWDSUserID
                                End If

                                exchange = New exchangeWDS.exchangeMail(exchangeWDSUrl, exuserid, exchangeWDSPassword, exdomain)
                            End If

                            exchange.sendMail(sTestAddress, "Test MAPI (Exchange WDS) Message from SQL-RD", "This is a test MAPI (Exchange WDS) email sent by SQL-RD.", exchangeWDSSenderAddress, exchangeWDSSender)
                    End Select


                    MessageBox.Show("MAPI Mail test succeeded.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cmdOK.Enabled = True
                    cmdApply.Enabled = True
                    chkEmailRetry.Enabled = True
                    'cdoSession.Logoff()
                Catch ex As Exception

                    sErr = "MAPI Mail test failed with the following error: " & ex.Message & " [" & Err.Number & "]"
                    'End If

                    MessageBox.Show(sErr, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            Case "smtp", "sqlrdmail"
                Dim sTestAddress As String
                Dim ok As Boolean

                If cmbMailType.Text.ToLower = "smtp" Then
                    MailType = gMailType.SMTP
                Else
                    MailType = gMailType.SQLRDMAIL
                End If

                skipValidation = True

                m_tempSMTPValues = New Hashtable

                'lets save the current settings in case the user cancels changing the SMTP settings
                With m_tempSMTPValues
                    .Add("server", clsMarsUI.MainUI.ReadRegistry("SMTPServer", ""))
                    .Add("user", clsMarsUI.MainUI.ReadRegistry("SMTPUserID", ""))
                    .Add("password", clsMarsUI.MainUI.ReadRegistry("SMTPPassword", ""))
                    .Add("port", clsMarsUI.MainUI.ReadRegistry("SMTPPort", 25))
                    .Add("sender", clsMarsUI.MainUI.ReadRegistry("SMTPSenderName", ""))
                    .Add("senderaddress", clsMarsUI.MainUI.ReadRegistry("SMTPSenderAddress", ""))
                    .Add("timeout", clsMarsUI.MainUI.ReadRegistry("SMTPTimeout", 30))
                    .Add("sqlrdsender", clsMarsUI.MainUI.ReadRegistry("SQL-RDSender", ""))
                    .Add("sqlrdaddress", clsMarsUI.MainUI.ReadRegistry("SQL-RDAddress", ""))
                End With

                cmdApply_Click(Nothing, Nothing)

                sTestAddress = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                skipValidation = False

                oUI.SaveRegistry("TestSMTPAddress", _
                sTestAddress)

                If sTestAddress = "" Or sTestAddress.IndexOf("@") = -1 Then Exit Sub


                Dim objSender As clsMarsMessaging = New clsMarsMessaging

                ok = objSender.SendSMTP(sTestAddress, "Test SMTP Email From SQL-RD", "This is a test SMTP email sent by SQL-RD", _
                "Single", , , , , , , , , , , , , , , , , True)

                If ok = False Then
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True, False)
                End If

                oUI.BusyProgress(100, , True)

                If ok = True Then
                    MessageBox.Show("Email sent successfully!", Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)

                    cmdApply.Enabled = True
                    cmdOK.Enabled = True
                    chkEmailRetry.Enabled = True

                End If
            Case "groupwise"
                Dim testAddress As String = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                oUI.SaveRegistry("TestSMTPAddress", testAddress)

                If testAddress = "" Or testAddress.IndexOf("@") = -1 Then Exit Sub

                If oMsg.SendGROUPWISE(testAddress, "", "", "Test GROUPWISE Mail from SQL-RD", "Test GROUPWISE email sent by SQL-RD", "", "Single") = True Then
                    MessageBox.Show("Test completed. If you have not received any errors then the email has been sent successfully", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please check the provided information and try again")
                End If
        End Select
    End Sub

    Private Sub cmdServiceApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdServiceApply.Click
        On Error Resume Next
        Dim oSec As clsMarsSecurity = New clsMarsSecurity
        Dim sCommand As String
        Dim sChar As String = Chr(34)
        Dim srv As New ServiceController("SQL-RD")
        Dim srvmon As New ServiceController("SQL-RD Monitor")
        Dim sParameter As String
        Dim oControl As New clsServiceController

        If optNTService.Checked = True Then
            If txtWinDomain.Text.Length = 0 Then
                ep.SetError(txtWinDomain, "Please enter your Windows domain name. If your pc is " & _
                "not a member of a domain then please enter your pc name")
                txtWinDomain.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            ElseIf Me.txtWinUser.Text.Length = 0 Then
                ep.SetError(txtWinUser, "Please enter your WIndows user id")
                txtWinUser.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            ElseIf Me.txtWinPassword.Text.Length = 0 Then
                ep.SetError(txtWinPassword, "Please enter your Windows password")
                txtWinPassword.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            End If

            Dim loggedinUser As String = Environment.UserDomainName & "\" & Environment.UserName

            If loggedinUser.ToLower <> (txtWinDomain.Text & "\" & txtWinUser.Text).ToLower Then
                Dim warning As String = "The specified user is not the same as the currently logged in user. It is advisable to install the NT service to run " & _
                "under the logged in user." & vbCrLf & _
                "Would you like to abort the NT service installation so that you may log out and log back in as the specified user?"

                Dim yesorno As Windows.Forms.DialogResult = MessageBox.Show(warning, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)

                If yesorno = Windows.Forms.DialogResult.Yes Then Return

            End If

            Dim checkList As frmNTCheckList = New frmNTCheckList

            If checkList.ShowCheckList() = False Then
                Return
            End If

            If oControl.InstallNTScheduler(txtWinDomain.Text, txtWinUser.Text, _
            txtWinPassword.Text, cmbMailType.Text, chkUseAsBackupScheduler.Checked) = True Then
                cmdServiceApply.Enabled = False
                cmdServiceStart.Enabled = True

                txtReportCache.Text = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

                txtCachedData.Text = oUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Date\")

                txtSnapShots.Text = oUI.ReadRegistry("SnapshotsPath", sAppPath & "Snapshots\")

                txtTempFolder.Text = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

                gServiceType = "WindowsNT"
            End If

            chkCheckService.Enabled = True

        ElseIf optBackgroundApp.Checked = True Then

            oControl.InstallBGScheduler(chkUseAsBackupScheduler.Checked)

            cmdServiceStart.Enabled = True

            oUI.BusyProgress(, , True)

            cmdServiceApply.Enabled = False

            chkCheckService.Enabled = True

            gServiceType = "WindowsApp"
        ElseIf optNoScheduling.Checked = True Then

            Dim Response As DialogResult

            Response = MessageBox.Show("Having this option selected means no reports will be sent automatically, continue?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If Response = DialogResult.Yes Then

                oUI.BusyProgress(25, "Removing services...")

                If Environment.OSVersion.Platform.ToString.ToLower = "win32nt" Then
                    oControl.RemoveNTService()
                End If

                oControl.RemoveBAS()

                oUI.BusyProgress(95, "Cleaning up...")

                oUI.BusyProgress(, , True)

                cmdServiceStart.Enabled = False

                oUI.SaveRegistry("SQL-RDService", "NONE", , , True)

                ServiceType = "NONE"

                cmdServiceStart.Enabled = False
                cmdServiceApply.Enabled = False
                chkCheckService.Enabled = False

                gServiceType = "NONE"
            Else
                optNoScheduling.Checked = False

                Select Case ServiceType
                    Case "WindowsNT"
                        optNTService.Checked = True
                    Case "WindowsApp"
                        optBackgroundApp.Checked = True
                    Case "NONE"
                        optNoScheduling.Checked = True
                End Select

                cmdServiceApply.Enabled = False
                AppStatus(False)
                Exit Sub
            End If
        End If

        cmdApply.Enabled = True
        cmdOK.Enabled = True
    End Sub

    Private Sub chkUseAsBackupScheduler_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkUseAsBackupScheduler.CheckedChanged

        If IsFeatEnabled(gEdition.CORPORATE, featureCodes.fo1_failover) = False And chkUseAsBackupScheduler.Checked Then
            If isLoaded Then _NeedUpgrade(gEdition.CORPORATE, chkUseAsBackupScheduler, "Failover Module")

            chkUseAsBackupScheduler.Checked = False
            Return
        End If

        If chkUseAsBackupScheduler.Checked Then
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM schedulerattr")

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Dim workstationID As String = IsNull(oRs("workstationid").Value)

                If workstationID = "" Then
                    MessageBox.Show("Please set up the main scheduler server before setting this one as a backup", Application.ProductName, MessageBoxButtons.OK)
                    chkUseAsBackupScheduler.Checked = False
                    Return
                ElseIf String.Compare(workstationID, Environment.MachineName, False) = 0 Then
                    MessageBox.Show("This server is the main scheduler and therefore it cannot be set as a backup scheduler", Application.ProductName, MessageBoxButtons.OK)
                    chkUseAsBackupScheduler.Checked = False
                    Return
                End If
            Else
                MessageBox.Show("Please set up the main scheduler server before setting this one as a backup", Application.ProductName, MessageBoxButtons.OK)
                chkUseAsBackupScheduler.Checked = False
                Return
            End If
        End If
    End Sub
    Private Sub cmdTempFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTempFolder.Click
        On Error Resume Next

        With ofd
            .Description = "Please select the temporary output folder..."
            .ShowNewFolderButton = True
            .ShowDialog()

            If .SelectedPath = "" Then
                Exit Sub
            Else
                txtTempFolder.Text = .SelectedPath

                If txtTempFolder.Text.EndsWith("\") = False Then
                    txtTempFolder.Text &= "\"
                End If
            End If
        End With
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)

        saveDefaultParameters()

        If Me.m_scheduleStatus = clsServiceController.enum_svcStatus.RUNNING Then
            oUI.BusyProgress(50, "Restarting scheduler...")

            Dim oSvc As clsServiceController = New clsServiceController

            oSvc.StopScheduler()

            _Delay(1.5)

            clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile, False)

            oUI.BusyProgress(90, "Cleaning up...", True)

            oSvc.StartScheduler()
        Else
            clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile, False)
        End If

        Me.Close()
    End Sub

    Private Sub txtErrorAlertWho_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtErrorAlertWho.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSMTPUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPUserID.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If
    End Sub

    Private Sub txtSMTPPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPPassword.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
        If isLoaded Then
            cmdMailTest.Pulse()
        End If

    End Sub

    Private Sub txtSMTPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPServer.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If
    End Sub

    Private Sub txtSMTPSenderAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPSenderAddress.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If

    End Sub

    Private Sub txtSMTPSenderName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPSenderName.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If

    End Sub

    Private Sub cmbSMTPTimeout_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmbMAPIProfile_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMAPIProfile.SelectedIndexChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If

    End Sub

    Private Sub txtMAPIPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMAPIPassword.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False

        If isLoaded Then
            cmdMailTest.Pulse()
        End If
    End Sub

    Private Sub optNTService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNTService.CheckedChanged
        If optNTService.Checked = True Then
            grpNTService.Enabled = True
            grpThreading.Enabled = True
        End If
        cmdServiceApply.Enabled = True

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub optBackgroundApp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optBackgroundApp.CheckedChanged
        If optBackgroundApp.Checked = True Then
            grpNTService.Enabled = False
            grpThreading.Enabled = True
        End If
        cmdServiceApply.Enabled = True

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub optNoScheduling_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNoScheduling.CheckedChanged
        cmdServiceApply.Enabled = True

        If optNoScheduling.Checked = True Then
            grpNTService.Enabled = False
            chkEmailRetry.Enabled = False
            GroupBox16.Enabled = False
            GroupBox2.Enabled = False
            grpThreading.Enabled = False
            chkUseAsBackupScheduler.Enabled = False
        Else
            If cmbMailType.Text <> "NONE" Then
                chkEmailRetry.Enabled = True
            End If

            If Me.optNTService.Checked = True Then
                grpNTService.Enabled = True
            End If
            GroupBox16.Enabled = True
            GroupBox2.Enabled = True
            grpThreading.Enabled = True
            chkUseAsBackupScheduler.Enabled = True
        End If

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub


    Private Sub cmdServiceStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdServiceStart.Click
        On Error Resume Next
        Dim I As Integer

        If optNTService.Checked = True Then
            Dim srv As New ServiceController("SQL-RD")
            Dim srvmon As New ServiceController("SQL-RD Monitor")

            If srvmon.Status <> ServiceControllerStatus.Running Then
                srvmon.Start()
            End If

            If srv.Status <> ServiceControllerStatus.Running Then
                srv.Start()
            End If

            For I = 1 To 100 Step 20
                oUI.BusyProgress(I, "Starting service...")
            Next

            Process.Start(sAppPath & "ServiceMonitor.exe")

        ElseIf optBackgroundApp.Checked = True Then
            Dim oProc As Process = New Process

            oProc.StartInfo.FileName = sAppPath & "sqlrdapp.exe"

            For I = 1 To 100 Step 20
                oUI.BusyProgress(I, "Starting service...")
            Next

            oProc.Start()

            Process.Start(sAppPath & "sqlrdappmon.exe")
        End If


        oUI.BusyProgress(, , True)

        cmdServiceStart.Enabled = False

    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oUI As New clsMarsMessaging

        oUI.OutlookAddresses(txtErrorAlertWho)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click
        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        oPicker.cmdBcc.Enabled = False
        oPicker.cmdCc.Enabled = False
        oPicker.txtBcc.Enabled = False
        oPicker.txtCc.Enabled = False

        sValues = oPicker.PickContact("to")

        If sValues Is Nothing Then Return

        If txtErrorAlertWho.Text.EndsWith(";") = False And txtErrorAlertWho.Text.Length > 0 _
        Then txtErrorAlertWho.Text &= ";"

        Try
            txtErrorAlertWho.Text &= sValues(0)
        Catch
        End Try
    End Sub

    Private Sub cmdAlertWho_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAlertWho.MouseUp
        If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False
        mnuContacts.Show(cmdAlertWho, New Point(e.X, e.Y))
    End Sub

    Private Sub cmdRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRegister.Click
        If lsvComponents.SelectedItems.Count = 0 Then Return

        Dim sPath As String = sAppPath
        Dim oProc As New Process

        sPath = sPath.ToLower.Replace("sqlrd", "common")

        If lsvComponents.SelectedItems.Count = 0 Then Return

        Try
            Select Case lsvComponents.SelectedItems(0).Text.ToLower
                Case "collaboration data objects"
                    'Dim cdoSession As MAPI.Session
                    'Dim cdoVer As String

                    'cdoSession = New MAPI.Session

                    Try
                        'cdoVer = cdoSession.Version

                        MessageBox.Show("Collaboration Data Objects Test successful." & _
                        Environment.NewLine & _
                        "Installed version is cdo " & 1.2, Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch
                        MessageBox.Show("Error initialising Collaborative Data Objects (CDO)." & Environment.NewLine & _
                        "CDO might not be installed correctly on your pc." & Environment.NewLine & _
                        "You can install CDO from your Microsoft Office as follows:" & Environment.NewLine & _
                        "1. Run your Office Setup" & Environment.NewLine & _
                        "2. Select 'Add or Remove Features'..." & Environment.NewLine & _
                        "3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & Environment.NewLine & _
                        "4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & Environment.NewLine & _
                        "5. Click the 'Update' button", Application.ProductName, _
                        MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                    End Try
                Case "compression support"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "dzactx.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "extended mapi support"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "redemption.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "mapi profiles enumerator"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "profman.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "sqlrd - crystal connector"
                    Try
                        Dim oRes As DialogResult

                        oRes = MessageBox.Show("This action will shut down SQL-RD and the scheduler so that " & _
                        "the files in use can be accessed. Proceed?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then
                            Process.Start(sAppPath & "crfr.exe")
                        End If
                    Catch ex As Exception
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                        _GetLineNumber(ex.StackTrace))
                    End Try
            End Select
        Catch : End Try
    End Sub

    Private Sub txtDefSubject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefSubject.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbMAPIProfile_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMAPIProfile.DropDown
        Try
            Dim oProfs As New ProfMan.Profiles

            cmbMAPIProfile.Items.Clear()

            For I As Integer = 1 To oProfs.Count
                cmbMAPIProfile.Items.Add(oProfs.Item(I).Name)
            Next
        Catch
        End Try

    End Sub

    Private Sub cmdSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSpell.Click
        Dim oBox(2) As TextBox

        oBox(0) = txtDefSubject
        oBox(1) = txtDefaultEmail
        oBox(2) = txtSig

        With Speller
            .Modal = True
            .ModalOwner = Me
            .TextBoxBasesToCheck = oBox
            .Check()
        End With
    End Sub

    Private Sub chkArchive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkArchive.CheckedChanged
        txtArchive.Enabled = chkArchive.Checked
        cmdApply.Enabled = True
    End Sub


    Private Sub chkMaintainColAlign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkUseFormat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkKeepImages_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub oSMTP_SMTPStatus(ByVal TotalSent As Long, ByVal TotalSize As Long) Handles oSMTP.SMTPStatus
        Dim Percent As Double

        Percent = (TotalSent / TotalSize) * 100

        oUI.BusyProgress(Percent, "Sending email...")
    End Sub

    Private Sub cmdActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActivate.Click
        Dim Msg As String

        If gnEdition = MarsGlobal.gEdition.EVALUATION Then
            Msg = "The account cannot be changed in the Evaluation edition. The preset " & vbCrLf & _
                "account is fully functional during the evaluation period. You can test " & vbCrLf & _
                "it by clicking 'Test Settings', and it can be used to email scheduled reports"

            MessageBox.Show(Msg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            clsWebProcess.Start("http://www.christiansteven.com/sql-rd/members/members.htm")

            oUI.SaveRegistry("SQL-RDMailActivation", 1)
            txtCRDAddress.ReadOnly = False
            txtCRDSender.ReadOnly = False
        End If
    End Sub

    Private Sub cmdClearSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClearSettings.Click
        Dim oRes As DialogResult

        oRes = MessageBox.Show("Clear the select mail settings? This cannot be undone", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.No Then Return

        Select Case cmbMailType.Text.ToLower
            Case "mapi"
                With oUI
                    .SaveRegistry("MAPIProfile", String.Empty)
                    .SaveRegistry("MAPIPassword", "")
                    .SaveRegistry("MAPIAlias", String.Empty)
                    .SaveRegistry("MAPIType", String.Empty)
                    .SaveRegistry("MAPIDomain", String.Empty)
                    .SaveRegistry("MAPIServer", String.Empty)
                    .SaveRegistry("MAPIUser", String.Empty)

                    Me.cmbMAPIProfile.Text = String.Empty
                    Me.txtMAPIPassword.Text = String.Empty
                End With
            Case "smtp"
                With oUI
                    .SaveRegistry("SMTPPassword", String.Empty)
                    .SaveRegistry("SMTPProfile", String.Empty)
                    .SaveRegistry("SMTPSenderAddress", String.Empty)
                    .SaveRegistry("SMTPSenderName", String.Empty)
                    .SaveRegistry("SMTPServer", String.Empty)
                    .SaveRegistry("SMTPTimeout", String.Empty)
                    .SaveRegistry("SMTPUserID", String.Empty)

                    Me.txtSMTPPassword.Text = String.Empty
                    Me.txtSMTPSenderAddress.Text = String.Empty
                    Me.txtSMTPServer.Text = String.Empty
                    Me.txtSMTPUserID.Text = String.Empty
                    Me.txtSMTPSenderName.Text = String.Empty
                    Me.cmbSMTPTimeout.Text = 0
                End With
            Case "sqlrdmail"
                With oUI
                    .SaveRegistry("SQL-RDAddress", String.Empty)
                    .SaveRegistry("SQL-RDSender", String.Empty)

                    txtCRDAddress.Text = String.Empty
                    txtCRDSender.Text = String.Empty
                End With
            Case "groupwise"
                With oUI
                    .SaveRegistry("GWUser", String.Empty)
                    .SaveRegistry("GWPassword", String.Empty, True)
                    .SaveRegistry("GWProxy", String.Empty)
                    .SaveRegistry("GWPOIP", String.Empty)
                    .SaveRegistry("GWPOPort", String.Empty)
                End With
        End Select
    End Sub



    Private Sub cmdSMTPMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSMTPMore.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, sender, getFeatDesc(featureCodes.m1_MultipleSMTPServer))
            Return
        End If

        Dim oMore As New frmSMTPServers

        oMore.IsDialog = True

        oMore.ShowDialog(Me)
    End Sub

    Private Sub cmdDBLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDBLoc.Click
        Dim ofd As New OpenFileDialog

        With ofd
            .Title = "Please select the file to set as the default attachment"
            .ShowDialog()
            .Multiselect = False

            If .FileName.Length = 0 Then Return

            txtDefAttach.Text = .FileName
        End With


    End Sub

    Private Sub chkAutoCompact_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoCompact.CheckedChanged
        dtAutoCompact.Enabled = chkAutoCompact.Checked


        If ShowCompactMsg = True And isLoaded = True Then
            UpdateAutoCompact = True

            If chkAutoCompact.Checked = True Then
                MessageBox.Show("The SQL-RD scheduler will need to shutdown the editor in order to carry out " & _
                "the auto-compact. This will be done at the selected time and will occur without warning.", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If

    End Sub


    Private Sub cmdRptLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRptLoc.Click
        With ofd
            .Description = "Please select the path to your Crystal Reports"
            .ShowNewFolderButton = False

            .ShowDialog()

            If .SelectedPath.Length > 0 Then
                txtReportLoc.Text = _CreateUNC(.SelectedPath)
            End If
        End With
    End Sub

    Private Sub optUseSMSC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optUseSMSC.CheckedChanged
        grpSMSC.Enabled = optUseSMSC.Checked
    End Sub

    Private Sub cmbSMSDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSDevice.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSDevice_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMSDevice.DropDown
        Try
            If cmbSMSDevice.Items.Count > 0 Then Return

            Dim oSMS As Object

#If Not Debug Then
            oSMS = CreateObject("ActiveXperts.SMSC") '   New ASMSCTRLLib.SMSC
#End If

            oSMS.Activate("157C2-9A5A9-57FFB")

            Dim i As Integer

            For i = 0 To oSMS.GetDeviceCount - 1
                cmbSMSDevice.Items.Add(oSMS.GetDevice(i))    ' Add Devices
            Next

            'show the COM ports
            With cmbSMSDevice.Items
                .Add("COM1")
                .Add("COM2")
                .Add("COM3")
                .Add("COM4")
            End With
        Catch
            'show the COM ports
            With cmbSMSDevice.Items
                .Add("COM1")
                .Add("COM2")
                .Add("COM3")
                .Add("COM4")
            End With
        End Try
    End Sub

    Private Sub optErrorSMS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorSMS.CheckedChanged
        On Error Resume Next
        cmdApply.Enabled = True

        txtAlertSMS.Enabled = optErrorSMS.Checked
        cmdAlertSMS.Enabled = optErrorSMS.Checked
    End Sub

    Private Sub cmdAlertWho_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlertWho.Click

    End Sub

    Private Sub cmdAlertSMS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlertSMS.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "SMS"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtAlertSMS.Text.EndsWith(";") = False And txtAlertSMS.Text.Length > 0 _
        Then txtAlertSMS.Text &= ";"

        Try
            txtAlertSMS.Text &= sValues(0)
        Catch : End Try
    End Sub

    Private Sub txtAlertSMS_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAlertSMS.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub dtAutoCompact_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtAutoCompact.ValueChanged
        If isLoaded = True Then UpdateAutoCompact = True
        cmdApply.Enabled = True
    End Sub

    Private Sub chkAudit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAudit.CheckedChanged
        If chkAudit.Checked = True And gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
            If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, sender, getFeatDesc(featureCodes.sa4_SecureUserLogon))
            chkAudit.Checked = False
            Return
        End If

        GroupBox12.Enabled = chkAudit.Checked

        cmdApply.Enabled = True
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        If ucDSN._Validate = False Then
            cmdApply.Enabled = False
            cmdOK.Enabled = False
        Else

            cmdApply.Enabled = True
            cmdOK.Enabled = True

            Dim oCon As New ADODB.Connection
            Dim SQL As String

            oUI.BusyProgress(50, "Opening connection...")

            oCon.Open(ucDSN.cmbDSN.Text, ucDSN.txtUserID.Text, ucDSN.txtPassword.Text)

            oUI.BusyProgress(85, "Creating table...")

            SQL = "CREATE TABLE CRDAuditTrail (" & _
            "AuditID        INTEGER     PRIMARY KEY," & _
            "EntryDate      DATETIME," & _
            "UserID         VARCHAR(55)," & _
            "ScheduleType   VARCHAR(55)," & _
            "ScheduleName   VARCHAR(55)," & _
            "AuditAction    VARCHAR(55))"

            Try
                oCon.Execute(SQL)
            Catch : End Try

            oUI.BusyProgress(95, "Cleaning up...")

            oCon.Close()

            oUI.BusyProgress(100, "", True)

            MessageBox.Show("Database connection established", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If lsvPaths.Items.Count >= 1 Then
            If IsFeatEnabled(gEdition.PLATINUM, modFeatCodes.sa7_FolderHousekeeping) = False Then
                If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.PLATINUM, sender, getFeatDesc(featureCodes.sa7_FolderHousekeeping))
                Return
            End If
        End If

        Dim oKeeper As New frmAddHouseKeeper

        oKeeper._AddPath()

        Me._LoadPaths()
    End Sub

    Private Sub lsvPaths_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvPaths.SelectedIndexChanged

    End Sub

    Private Sub lsvPaths_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvPaths.DoubleClick
        If lsvPaths.SelectedItems.Count = 0 Then Return

        Dim oKeeper As New frmAddHouseKeeper

        oKeeper._EditPath(lsvPaths.SelectedItems(0).Tag)

        _LoadPaths()
    End Sub



    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvPaths.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected path(s)?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each oItem As ListViewItem In lsvPaths.SelectedItems
                clsMarsData.WriteData("DELETE FROM HouseKeepingPaths WHERE EntryID =" & oItem.Tag)

                oItem.Remove()
            Next
        End If
    End Sub


    Private Sub cmdReportCache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportCache.Click
        Try
            Dim oRes As DialogResult

            With ofd
                .Description = "Please select the path to the cache folder"
                .ShowNewFolderButton = True
                oRes = .ShowDialog()
            End With

            If oRes = DialogResult.Cancel Then Return

            txtReportCache.Text = _CreateUNC(ofd.SelectedPath)

            Dim saveCursor As Cursor = Cursor.Current

            Try
                If txtReportCache.Text.EndsWith("\") = False Then txtReportCache.Text &= "\"

                If MessageBox.Show("Move the existing cached reports and update the schedules?", Application.ProductName, MessageBoxButtons.YesNo, _
                 MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Cursor.Current = Cursors.WaitCursor
                    MoveCache()
                End If
            Finally
                Cursor.Current = saveCursor
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub



    Private Sub MoveCache()
        Dim CurrentPath As String
        Dim sFile As String
        Dim nID As Integer
        Dim s As String

        Try
            CurrentPath = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

            Dim NewPath As String = txtReportCache.Text

            If IO.Directory.Exists(NewPath) = False Then
                IO.Directory.CreateDirectory(NewPath)
            End If

            'update reportattr and copy the files
            Dim oRs As ADODB.Recordset
            Dim SQL As String

            SQL = "SELECT * FROM ReportAttr"

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    CurrentPath = oRs("cachepath").Value

                    sFile = ExtractFileName(CurrentPath)

                    Dim sCache As String = NewPath & sFile

                    nID = oRs("reportid").Value

                    Try
                        IO.File.Copy(oRs("cachepath").Value, sCache, True)
                    Catch
                        Try
                            IO.File.Copy(oRs("databasepath").Value, sCache, True)
                        Catch : End Try
                    End Try

                    SQL = "UPDATE ReportAttr SET CachePath = '" & SQLPrepare(sCache) & "' WHERE ReportID =" & nID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            cmdApply.Enabled = True
            cmdOK.Enabled = True

            oUI.SaveRegistry("CachePath", NewPath)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub MoveFolder(ByVal folderPath As String, ByVal destination As String)
        Dim folderName As String = ExtractFileName(folderPath)

        If destination.EndsWith("\") = False Then destination &= "\"

        If IO.Directory.Exists(destination & folderName) = False Then
            IO.Directory.CreateDirectory(destination & folderName)
        End If

        'My.Computer.FileSystem.mo()
    End Sub

    Private Sub cmdCachedData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCachedData.Click
        Try
            ofd.Description = "Please select the folder to store cached data for event-based schedule"

            Dim temp As String = txtCachedData.Text

            If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtCachedData.Text = _CreateUNC(ofd.SelectedPath)

                If txtCachedData.Text.EndsWith("\") = False Then txtCachedData.Text &= "\"

                If MessageBox.Show("Move existing cached data to this location?", Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For Each s As String In IO.Directory.GetDirectories(temp)
                        My.Computer.FileSystem.MoveDirectory(s, txtCachedData.Text & ExtractFileName(s), True)
                        'IO.Directory.Move(s, txtCachedData.Text & ExtractFileName(s))
                    Next

                    For Each s As String In IO.Directory.GetFiles(temp)
                        IO.File.Move(s, txtCachedData.Text & ExtractFileName(s))
                    Next

                    oUI.SaveRegistry("CachedDataPath", txtCachedData.Text)

                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdSnapshots_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSnapshots.Click
        Try
            ofd.Description = "Please select the folder to store schedule snapshots"

            Dim temp As String = txtSnapShots.Text

            If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtSnapShots.Text = _CreateUNC(ofd.SelectedPath)

                If txtSnapShots.Text.EndsWith("\") = False Then txtSnapShots.Text &= "\"

                If MessageBox.Show("Move existing snapshots to this location?", Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For Each s As String In IO.Directory.GetDirectories(temp)
                        IO.Directory.Move(s, txtSnapShots.Text & ExtractFileName(s))
                    Next

                    For Each s As String In IO.Directory.GetFiles(temp)
                        Dim newFile As String = txtSnapShots.Text & ExtractFileName(s)

                        Dim SQL As String

                        SQL = "UPDATE SnapshotsAttr SET SnapPath = '" & SQLPrepare(newFile) & "' WHERE SnapPath = '" & SQLPrepare(s) & "'"

                        If clsMarsData.WriteData(SQL) = True Then
                            IO.File.Move(s, newFile)
                        Else
                            Exit For
                        End If
                    Next

                    oUI.SaveRegistry("SnapshotsPath", txtSnapShots.Text)
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub


    Private Sub chkDTRefresh_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTRefresh.CheckedChanged
        txtRefresh.Enabled = chkDTRefresh.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbPriority_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPriority.SelectedIndexChanged, cmbPriorityEB.SelectedIndexChanged
        If isLoaded = True And Me.optNoScheduling.Checked = False Then
            ep.SetError(cmbPriority, "You will need to restart the scheduler for this change to take effect")
        End If
        cmdApply.Enabled = True
    End Sub

    Private Sub txtgwUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtgwUser.TextChanged, txtgwPassword.TextChanged, txtgwPOIP.TextChanged, txtgwPOPort.TextChanged, txtgwProxy.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtRefresh_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRefresh.ValueChanged, txtRefresh.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkIgnoreSel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkCompact_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCompact.CheckedChanged
        cmdApply.Enabled = True
    End Sub


    Private Sub cmbSMSFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSFormat.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSSpeed_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSSpeed.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSProtocol_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMSProtocol.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSMSNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSNumber.TextChanged, txtSMSPassword.TextChanged, txtSMSSender.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkUnicode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUnicode.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbEncoding_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEncoding.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbPollInt_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPollInt.ValueChanged, cmbPollInt.TextChanged
        cmdApply.Enabled = True
        If isLoaded = True Then
            ep.SetError(cmbPollInt, "You must restart the scheduler for the changes to take effect")
        End If
    End Sub

    Private Sub chkCheckService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheckService.CheckedChanged
        cmdApply.Enabled = True

        If chkCheckService.Checked = True Then
            Me.chkDelayRestart.Enabled = False
            Me.chkDelayRestart.Checked = False
        Else
            Me.chkDelayRestart.Enabled = True
        End If
    End Sub

    Private Sub txtArchive_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArchive.ValueChanged
        cmdApply.Enabled = True
    End Sub


    Private Sub txtDefAttach_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefAttach.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDefaultEmail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefaultEmail.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSig_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSig.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtReportLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportLoc.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtCachedData_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCachedData.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSnapShots_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSnapShots.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtReportCache_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportCache.TextChanged, txtSentMessages.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtThreadCount_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtThreadCount.ValueChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtWinDomain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWinDomain.LostFocus
        txtWinDomain.Text = txtWinDomain.Text.ToUpper
    End Sub

    Private Sub txtWinDomain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtWinDomain.TextChanged, txtWinPassword.TextChanged, txtWinUser.TextChanged
        ep.SetError(sender, "")
        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub chkOutofProcess_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOutofProcess.CheckedChanged

    End Sub

    Private Sub txtSMTPPort_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPPort.ValueChanged
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub btnSentMessages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSentMessages.Click
        Try
            Dim oRes As DialogResult

            With ofd
                .Description = "Please select the path for the sent messages repository"
                .ShowNewFolderButton = True
                oRes = .ShowDialog()
            End With

            If oRes = DialogResult.Cancel Then Return

            Me.txtSentMessages.Text = _CreateUNC(ofd.SelectedPath)

            Dim saveCursor As Cursor = Cursor.Current

            Try
                If txtSentMessages.Text.EndsWith("\") = False Then txtSentMessages.Text &= "\"
            Finally
                Cursor.Current = saveCursor
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmbPollIntEB_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPollIntEB.ValueChanged
        cmdApply.Enabled = True

        If isLoaded = True Then
            ep.SetError(cmbPollInt, "You must restart the scheduler for the changes to take effect")
        End If
    End Sub


    Private Sub btnMAPIMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMAPIMore.Click
        Dim oMAPI As frmMAPIType = New frmMAPIType

        oMAPI.MAPISetup()
    End Sub

    Private Sub chkDelayRestart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDelayRestart.CheckedChanged
        txtDelayBy.Enabled = chkDelayRestart.Checked

        cmdApply.Enabled = True
    End Sub

    Private Sub txtDelayBy_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDelayBy.ValueChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub btnSMTPAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMTPAdvanced.Click
        Dim oSMTP As frmSMTPAdvanced = New frmSMTPAdvanced

        oSMTP.SMTPAdvanced(False)
    End Sub

    Private Sub chkConvertToMsg_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConvertToMsg.CheckedChanged
        If Me.chkConvertToMsg.Checked = True And isLoaded = True Then
            Try
                Dim ou As Object = CreateObject("Outlook.Application")

                ou = Nothing
            Catch ex As Exception
                Me.chkConvertToMsg.Checked = False

                Me.ep.SetError(Me.chkConvertToMsg, "Could not initialize MS Outlook. Please make sure that Outlook is installed on this machine.")
            End Try
        Else
            ep.SetError(Me.chkConvertToMsg, "")
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If Me.m_tempSMTPValues IsNot Nothing Then
            With clsMarsUI.MainUI
                .SaveRegistry("SMTPServer", Me.m_tempSMTPValues.Item("server"), , , True)
                .SaveRegistry("SMTPUserID", Me.m_tempSMTPValues.Item("user"), , , True)
                .SaveRegistry("SMTPPassword", Me.m_tempSMTPValues.Item("password"), , , True)
                .SaveRegistry("SMTPPort", Me.m_tempSMTPValues.Item("port"), , , True)
                .SaveRegistry("SMTPSenderName", Me.m_tempSMTPValues.Item("sender"), , , True)
                .SaveRegistry("SMTPSenderAddress", Me.m_tempSMTPValues.Item("senderaddress"), , , True)
                .SaveRegistry("SMTPTimeout", Me.m_tempSMTPValues.Item("timeout"), , , True)
                .SaveRegistry("SQL-RDSender", Me.m_tempSMTPValues.Item("sqlrdsender"), , , True)
                .SaveRegistry("SQL-RDAddress", Me.m_tempSMTPValues.Item("sqlrdaddress"), , , True)
            End With


        End If
    End Sub

    Private Sub lsvServers_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvServers.KeyDown
        If e.KeyCode = Keys.Delete Then
            For Each item As ListViewItem In lsvServers.SelectedItems
                item.Remove()
            Next
        End If
    End Sub



    Private Sub txtThreadInterval_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtThreadInterval.ValueChanged
        If Me.isLoaded = True And threadWarning = False And txtThreadInterval.Value < 15 Then
            MessageBox.Show("Reducing the Thread Interval may result in the application being quarantined or disabled by your local and network security. Please ensure that sqlrd.exe is white listed in your security software before reducing the Thread Interval.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            threadWarning = True
        End If
    End Sub

    Private Sub chkPrecheckconditions_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPrecheckconditions.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False And chkPrecheckconditions.Checked Then
            If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, getFeatDesc(featureCodes.sa1_MultiThreading))
            chkPrecheckconditions.Checked = False
        End If

        txtNumConnections.Enabled = chkPrecheckconditions.Checked
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optParameterSpecifiedValue.CheckedChanged
        If optParameterSpecifiedValue.Checked Then
            txtParameterDefaultValue.Enabled = True
            txtParameterDefaultValue.Text = ""
        End If
    End Sub

    Private Sub optParameterNull_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optParameterNull.CheckedChanged
        If optParameterNull.Checked Then
            txtParameterDefaultValue.Text = "[SQL-RDNull]"
            txtParameterDefaultValue.Enabled = False
        End If
    End Sub

    Private Sub optParameterDefault_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optParameterDefault.CheckedChanged
        If optParameterDefault.Checked Then
            txtParameterDefaultValue.Text = "[SQL-RDDefault]"
            txtParameterDefaultValue.Enabled = False
        End If
    End Sub

    Private Sub optParameterAllValues_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optParameterAllValues.CheckedChanged
        If optParameterAllValues.Checked Then
            txtParameterDefaultValue.Text = "[SelectAll]"
            txtParameterDefaultValue.Enabled = False
        End If
    End Sub

    Private Sub btnAddParameterDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddParameterDefault.Click
        If txtDefaultParameterName.Text = "" Then
            Return
        End If

        Dim oItem As ListViewItem
        Dim newItem As Boolean = True
        For Each it As ListViewItem In lsvParameterDefaults.Items
            If it.Text.ToLower = txtDefaultParameterName.Text.ToLower Then
                oItem = it
                newItem = False
                Exit For
            End If
        Next

        If oItem Is Nothing Then
            oItem = New ListViewItem(txtDefaultParameterName.Text)
            oItem.SubItems.Add("")
        End If

        oItem.SubItems(1).Text = txtParameterDefaultValue.Text

        If newItem Then
            lsvParameterDefaults.Items.Add(oItem)
        End If

        oItem.Selected = True
        oItem.EnsureVisible()
    End Sub

    Private Sub lsvParameterDefaults_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvParameterDefaults.SelectedIndexChanged
        If lsvParameterDefaults.SelectedItems.Count = 0 Then
            btnRemoveParameterDefault.Enabled = False
            Return
        Else
            btnRemoveParameterDefault.Enabled = True
        End If

        Dim item As ListViewItem = lsvParameterDefaults.SelectedItems(0)

        txtDefaultParameterName.Text = item.Text
        txtParameterDefaultValue.Text = item.SubItems(1).Text

    End Sub

    Private Sub txtParameterDefaultValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtParameterDefaultValue.TextChanged
        Select Case txtParameterDefaultValue.Text
            Case "[SQL-RDNull]"
                optParameterNull.Checked = True
                txtParameterDefaultValue.Enabled = False
            Case "[SQL-RDDefault]"
                optParameterDefault.Checked = True
                txtParameterDefaultValue.Enabled = False
            Case "[SelectAll]"
                optParameterAllValues.Checked = True
                txtParameterDefaultValue.Enabled = False
            Case Else
                optParameterSpecifiedValue.Checked = True
                txtParameterDefaultValue.Enabled = True
        End Select
    End Sub

    Private Sub btnRemoveParameterDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveParameterDefault.Click
        If lsvParameterDefaults.SelectedItems.Count = 0 Then Return

        Dim item As ListViewItem = lsvParameterDefaults.SelectedItems(0)

        clsMarsData.WriteData("DELETE FROM DefaultParameterValues WHERE parametername ='" & SQLPrepare(item.Text) & "'")

        item.Remove()
    End Sub

    Private Sub txtDefaultParameterName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefaultParameterName.TextChanged
        If txtDefaultParameterName.Text.Length = 0 Then
            btnAddParameterDefault.Enabled = False
        Else
            btnAddParameterDefault.Enabled = True
        End If
    End Sub

    Private Sub saveDefaultParameters()
        For Each it As ListViewItem In lsvParameterDefaults.Items
            clsDefaultParameter.setDefaultParameter(it.Text, it.SubItems(1).Text)
        Next

        clsMarsUI.MainUI.SaveRegistry("AllowAdminsToSetDefaultParameters", Convert.ToInt32(chkAllowAdminToChangeValues.Checked))
    End Sub

    Private Sub getDefaultParameters()
        Dim showDefaultParameters As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("SDP", 0))

        ' If showDefaultParameters Then
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM DefaultParameterValues ORDER BY parametername ASC")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim it As ListViewItem = New ListViewItem

                it.Text = oRs("parametername").Value
                it.SubItems.Add(oRs("parametervalue").Value)

                lsvParameterDefaults.Items.Add(it)

                oRs.MoveNext()
            Loop
            oRs.Close()
            oRs = Nothing
        End If

        chkAllowAdminToChangeValues.Checked = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AllowAdminsToSetDefaultParameters", 0))
        '  Else
        ' tabDefaultParameters.Visible = False
        ' End If
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txtParameterDefaultValue.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txtParameterDefaultValue.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txtParameterDefaultValue.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txtParameterDefaultValue.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txtParameterDefaultValue.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(99999)
        oInsert.m_EventBased = False
        oInsert.m_HideParameters = True
        oInsert.m_EventID = 99999

        oInsert.GetConstants(Me)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to update all your schedules' parameters to the above values? This cannot be undone.", Application.ProductName, _
                                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            For Each it As ListViewItem In lsvParameterDefaults.Items
                clsMarsData.WriteData("UPDATE reportparameter SET parvalue ='" & it.SubItems(1).Text & "' WHERE parname LIKE '" & it.Text & "'")
            Next

            MessageBox.Show("The existing report parameters have been updated successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub chkShareUserDefaults_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkShareUserDefaults.CheckedChanged
        Dim ors As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM schedulerattr")
        Dim workstationID As String = ""

        If ors IsNot Nothing AndAlso ors.EOF = False Then
            workstationID = ors(0).Value

            ors.Close()
        End If

        If isLoaded And chkShareUserDefaults.Checked Then

            If String.Compare(workstationID, Environment.MachineName, True) = 0 Then '//if its the main server then upload the settings
                Dim cols, vals As String

                cols = "defaultid,EmailSubject,EmailAttachment,EmailMessage,EmailSignature,DBUserID,DBPassword"

                vals = clsMarsData.CreateDataID("userdefaultsattr", "defaultid") & "," & _
                "'" & SQLPrepare(txtDefSubject.Text) & "'," & _
                "'" & SQLPrepare(txtDefAttach.Text) & "'," & _
                "'" & SQLPrepare(txtDefaultEmail.Text) & "'," & _
                "'" & SQLPrepare(txtSig.Text) & "'," & _
                "'" & SQLPrepare(UcLogin.txtUserID.Text) & "'," & _
                "'" & SQLPrepare(UcLogin.txtPassword.Text) & "'"

                clsMarsData.WriteData("DELETE FROM userdefaultsattr") '//there should only be one record in here

                clsMarsData.DataItem.InsertData("userdefaultsattr", cols, vals)
            Else '//this is a client machine
                Dim res As DialogResult = MessageBox.Show("Are you sure you would like to overwrite your local user defaults with the ones from the server?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If res = Windows.Forms.DialogResult.Yes Then
                    ors = clsMarsData.GetData("SELECT * FROM userdefaultsattr")

                    If ors IsNot Nothing AndAlso ors.EOF = False Then
                        txtDefSubject.Text = IsNull(ors("emailsubject").Value)
                        txtDefAttach.Text = IsNull(ors("emailattachment").Value)
                        txtDefaultEmail.Text = IsNull(ors("emailmessage").Value)
                        txtSig.Text = IsNull(ors("emailsignature").Value)
                        UcLogin.txtUserID.Text = IsNull(ors("dbuserid").Value)
                        UcLogin.txtPassword.Text = IsNull(ors("dbpassword").Value)
                    End If
                End If
            End If
        ElseIf isLoaded Then '//the user is unchecking
            If String.Compare(workstationID, Environment.MachineName, True) = 0 Then '//if its the server
                Dim res As DialogResult = MessageBox.Show("Are you sure you would like to unshare your User Defaults?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If res = Windows.Forms.DialogResult.Yes Then
                    clsMarsData.WriteData("DELETE FROM userdefaultsattr") '//delete the shared info from the server
                    cmdApply_Click(Nothing, Nothing)
                    cmdApply.Enabled = True
                End If
            End If
        End If
    End Sub

    Private Sub loadUserDefaultsFromServer()

        Dim ors As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM userdefaultsattr")

        If ors IsNot Nothing AndAlso ors.EOF = False Then
            txtDefSubject.Text = IsNull(ors("emailsubject").Value)
            txtDefAttach.Text = IsNull(ors("emailattachment").Value)
            txtDefaultEmail.Text = IsNull(ors("emailmessage").Value)
            txtSig.Text = IsNull(ors("emailsignature").Value)
            UcLogin.txtUserID.Text = IsNull(ors("dbuserid").Value)
            UcLogin.txtPassword.Text = IsNull(ors("dbpassword").Value)
        End If
    End Sub

    Private Sub saveUserDefaultsToServer()
        Dim cols, vals As String

        cols = "defaultid,EmailSubject,EmailAttachment,EmailMessage,EmailSignature,DBUserID,DBPassword"

        vals = clsMarsData.CreateDataID("userdefaultsattr", "defaultid") & "," & _
        "'" & SQLPrepare(txtDefSubject.Text) & "'," & _
        "'" & SQLPrepare(txtDefAttach.Text) & "'," & _
        "'" & SQLPrepare(txtDefaultEmail.Text) & "'," & _
        "'" & SQLPrepare(txtSig.Text) & "'," & _
        "'" & SQLPrepare(UcLogin.txtUserID.Text) & "'," & _
        "'" & SQLPrepare(UcLogin.txtPassword.Text) & "'"

        clsMarsData.WriteData("DELETE FROM userdefaultsattr") '//there should only be one record in here

        clsMarsData.DataItem.InsertData("userdefaultsattr", cols, vals)
    End Sub

    Private Sub chkAllowAdminToChangeValues_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAllowAdminToChangeValues.CheckedChanged

    End Sub

    Private Sub chkBlackout_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBlackout.CheckedChanged
        btnBlackout.Enabled = chkBlackout.Checked
    End Sub

    Private Sub btnBlackout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBlackout.Click
        Dim blackout As frmSchedulerBlackouts = New frmSchedulerBlackouts

        blackout.setSchedulerBlackouts()
    End Sub

    Private Sub optSMSByDevice_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles optSMSByDevice.CheckedChanged, optSMSByRedOxygen.CheckedChanged
        If optSMSByDevice.Checked Then
            grpSMSByDevice.Visible = True
            grpSMSByRedOxygen.Visible = False
        ElseIf optSMSByRedOxygen.Checked Then
            grpSMSByDevice.Visible = False
            grpSMSByRedOxygen.Visible = True
        End If
    End Sub

    Private Sub chkAutoclose_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkAutoclose.CheckedChanged
        txtAutoclose.Enabled = chkAutoclose.Checked
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub stabOptions_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabOptions.SelectedTabChanged
        If stabOptions.SelectedTab Is tabDefaultParameters Then
            showInserter(Me, 0)
        End If
    End Sub

    Private Sub btnAddDropbox_Click(sender As Object, e As EventArgs) Handles btnAddDropbox.Click
        Try
            Dim res As DialogResult = MessageBox.Show("Your browser will now take you to Dropbox.com where you will need to grant access to the application. Once completed, you may return here.", Application.ProductName, MessageBoxButtons.OKCancel)

            If res = Windows.Forms.DialogResult.Cancel Then Return

            Dim accountName As String = ""

            Dim accessToken As OAuthProtocol.OAuthToken = clsDropbox.GetAccessToken(accountName)

            Try
                Me.WindowState = FormWindowState.Minimized
                Me.Show()
                Me.WindowState = FormWindowState.Normal
                Me.Activate()
            Catch : End Try

            If accessToken IsNot Nothing Then
                Dim err As Exception = Nothing

                If dropboxAccount.addDropboxAccount(accountName, accessToken.Token, accessToken.Secret, err) = True Then
                    Dim item As ListViewItem = New ListViewItem(accountName)
                    item.SubItems.Add("Dropbox")
                    item.ImageIndex = 0
                    lsvCloud.Items.Add(item)
                Else
                    _ErrorHandle(err.Message, -3247100255, "Add Dropbox", 0)
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0)
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        If lsvCloud.SelectedItems.Count = 0 Then Return

        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to remove this account? All Tasks using this account will no longer function and this cannot be undone.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            If dropboxAccount.deleteDropboxAccount(lsvCloud.SelectedItems(0).Text) = True Then
                lsvCloud.SelectedItems(0).Remove()
            End If
        End If
    End Sub
End Class
