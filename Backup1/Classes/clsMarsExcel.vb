﻿Friend Class clsMarsExcel
    Public Function mergeXLSXFiles(ByVal fileArray() As String, ByVal resultFile As String, ByRef errInfo As String) As Boolean
        Try

            GemBox.Spreadsheet.SpreadsheetInfo.SetLicense("EZNJ-UAM0-VCTS-IRGH")

            Dim ef As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile

            '// Load first file (for example .xlsx)
            ef.LoadXlsxFromDirectory(unzipXLSXFile(fileArray(0)), GemBox.Spreadsheet.XlsxOptions.PreserveKeepOpen)

            '// Create and load second ExcelFile (for example .xls)
            For I As Integer = 1 To fileArray.GetUpperBound(0)
                Dim ef2 As GemBox.Spreadsheet.ExcelFile = New GemBox.Spreadsheet.ExcelFile

                ef2.LoadXlsxFromDirectory(unzipXLSXFile(fileArray(I)), GemBox.Spreadsheet.XlsxOptions.PreserveKeepOpen)

                '//merge
                For Each ws As GemBox.Spreadsheet.ExcelWorksheet In ef2.Worksheets
                    ef.Worksheets.AddCopy(IO.Path.GetFileNameWithoutExtension(fileArray(I)), ws)
                Next
            Next

            If IO.File.Exists(resultFile) = True Then
                IO.File.Delete(resultFile)
            End If

            '//save merged file
            Dim newFile As String = IO.Path.GetFileName(resultFile)
            Dim outputDir As String = IO.Path.GetDirectoryName(resultFile)

            outputDir = IO.Path.Combine(outputDir, Guid.NewGuid.ToString)

            clsMarsParser.Parser.ParseDirectory(outputDir)

            ef.SaveXlsxToDirectory(outputDir)

            Dim oTask As clsMarsTask = New clsMarsTask

            oTask.ZipFiles(resultFile, outputDir & "\*.*", True, True)

            Return True
        Catch ex As Exception
            errInfo = ex.ToString
            Return False
        End Try
    End Function

    Private Function unzipXLSXFile(file As String) As String
        Dim oTask As New clsMarsTask
        Dim unZipFolder As String = IO.Path.Combine(IO.Path.GetDirectoryName(file), Guid.NewGuid.ToString)

        oTask.UnzipFiles(file, unZipFolder, "")

        Return unZipFolder

    End Function
    Public Function MergeXLFiles(ByVal sFiles() As String, ByVal sOutputFile As String) As Boolean
        Dim logFile As String = "excelmerging_" & Process.GetCurrentProcess.Id & ".log"

        clsMarsDebug.writeToDebug(logFile, "Initiating ExcelMan instance", True)

        Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.NONE)

        clsMarsUI.MainUI.BusyProgress(50, "Merging Excel files...")

        Try
            Dim szSheetName As String
            Dim sPrevSheet As String = ""
            Dim I As Integer = 0
            Dim sTemp As String = sOutputFile

            Try
                If System.IO.File.Exists(sOutputFile) Then
                    System.IO.File.Delete(sOutputFile)
                End If
            Catch : End Try

            clsMarsDebug.writeToDebug(logFile, "Setting worksheet names to avoid duplicates", True)

            For I = 0 To sFiles.GetUpperBound(0)
                If sFiles(I).Length > 0 Then
                    For x As Integer = 1 To oXL.GetWorksheetCount(sFiles(I))
                        szSheetName = oXL.GetWorkSheetName(sFiles(I), x)

                        If szSheetName = "Sheet1" Then
                            szSheetName = ExtractFileName(sFiles(I)).Replace(".xls", "")

                            oXL.SetWorkSheetName(sFiles(I), szSheetName, x)
                        End If
                    Next
                End If
            Next

            clsMarsDebug.writeToDebug(logFile, "Sending jobs to ExcelMan for merging", True)

            Dim mergeDataOnly As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("MergeExcelDataOnly", 0))

            Dim ok As Boolean


            If mergeDataOnly = False Then

                ok = oXL.MergeExcelFiles(sOutputFile, sFiles)
            Else

                ok = oXL.MergeExcelFilesDataOnly(sOutputFile, sFiles, "")
            End If


            If ok = False Then
                Throw New Exception("Error merging Excel workbooks: " & oXL.m_errorMsg)
            End If

            clsMarsDebug.writeToDebug(logFile, "Selecting first worksheet", True)
            oXL.selectWorksheet(sOutputFile, 1)

            clsMarsDebug.writeToDebug(logFile, "Disposing off ExcelMan", True)
            oXL.Dispose()

            Return True
        Catch ex As Exception
            Try
                oXL.Dispose()
            Catch : End Try

            Throw ex
        End Try
    End Function

    Public Function setWorkBookPassword(ByVal file As String, ByVal password As String) As Boolean

        Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.NONE)
        Try
            setWorkBookPassword = oXL.SetWorkBookPassword(file, password)

            oXL.Dispose()

            Return setWorkBookPassword
        Catch
            Try
                oXL.Dispose()
            Catch : End Try

            Return False
        End Try
    End Function
End Class
