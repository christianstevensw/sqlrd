Imports System

Friend Class clsErrorInfo
    Public ErrorDesc As String
    Public ErrorNumber As Long
    Public ErrorLine As Int32
    Public ErrorSource As String
    Public ErrorSuggest As String

    Public Sub New()
        ErrorDesc = ""
        ErrorNumber = -1
        ErrorLine = -1
        ErrorSource = ""
        ErrorSuggest = ""
    End Sub

    Public Sub New(ByVal errDesc As String, _
                    ByVal errNumber As Long, _
                    ByVal errLine As Int32, _
                    ByVal errSource As String, _
                    ByVal errSuggest As String)

        ErrorDesc = errDesc
        ErrorNumber = errNumber
        ErrorLine = errLine
        ErrorSource = errSource
        ErrorSuggest = errSuggest
    End Sub

End Class