Friend Class clsOperationalHours
    Public Shared Function withinOperationHours(ByVal checkDate As Date, ByVal operationName As String) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim OperationID As Integer = 0
        Dim dtDays As DataTable
        Dim result As Boolean = False

        Try
            SQL = "SELECT * FROM OperationDays WHERE OperationID = " & _
            "(SELECT OperationID FROM OperationAttr WHERE OperationName LIKE '" & SQLPrepare(operationName) & "')"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                dtDays = New DataTable

                With dtDays.Columns
                    .Add("DayName")
                    .Add("DayIndex")
                    .Add("OpenAt")
                    .Add("CloseAt")
                End With

                If oRs.EOF = True Then Return True

                Do While oRs.EOF = False
                    Dim row As DataRow = dtDays.Rows.Add

                    row("dayname") = oRs("dayname").Value
                    row("dayindex") = oRs("dayindex").Value
                    row("openat") = oRs("openat").Value
                    row("closeat") = oRs("closeat").Value

                    oRs.MoveNext()
                Loop

                oRs.Close()
            Else
                Return True
            End If

            'check if today is part of operation hours
            Dim dayofWeek As String = checkDate.DayOfWeek.ToString

            Dim rows() As DataRow = dtDays.Select("DayName ='" & dayofWeek & "'")

            If rows.Length = 0 Then
                Return False
            End If

            'now that we have narrowed our operational hours to a day, lets see if the current time falls with range
            Dim dayRow As DataRow = rows(0)
            Dim currentTime As Date = ConDate(Now) & " " & ConTime(checkDate)
            Dim openTime As Date = ConDate(Now) & " " & ConTime(dayRow("openat"))
            Dim closeTime As Date = ConDate(Now) & " " & ConTime(dayRow("closeat"))

            If currentTime >= openTime And currentTime <= closeTime Then
                Return True
            Else
                Return False
            End If
        Catch
            Return True
        Finally
            dtDays = Nothing
            oRs = Nothing
        End Try
    End Function

    Dim m_name As String
    Dim m_id As Integer



    Sub New(ByVal id As Integer)
        m_id = id
    End Sub

    Public ReadOnly Property operationHoursID() As Integer
        Get
            Dim SQL As String = "SELECT OperationID FROM OperationAttr WHERE OperationName LIKE '" & SQLPrepare(operationHoursName) & "'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim id As Integer

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                id = oRs(0).Value
                oRs.Close()
            Else
                id = 0
            End If

            Return id
        End Get
    End Property

    Public ReadOnly Property operationHoursName() As String
        Get
            Dim SQL As String = "SELECT OperationName FROM OperationAttr WHERE OperationID = " & m_id
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim name As String

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                name = oRs(0).Value
                oRs.Close()
            Else
                name = Nothing
            End If

            Return name
        End Get
    End Property

    Public Shared Function isGroupBlackOutPeriod(ByVal checkDateTime As Date) As Boolean
        Dim SQL As String = "SELECT operationhrsid FROM groupattr g INNER JOIN groupblackoutattr b ON g.groupid = b.groupid WHERE g.groupname ='" & SQLPrepare(gRole) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                Dim opID As Integer = oRs(0).Value
                Dim opHours As clsOperationalHours = New clsOperationalHours(opID)

                If opHours.withinOperationHours(checkDateTime, opHours.operationHoursName) Then
                    oRs.Close()
                    Return True
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Return False
    End Function
End Class
