
Imports Microsoft.Win32
Imports Quiksoft.EasyMail.POP3
Imports Quiksoft.EasyMail.Parse
Imports Quiksoft.EasyMail.SSL
Imports System.IO
Imports System.Net
Imports System.Text

Public Class clsMarsMessaging

    Dim oSec As clsMarsSecurity = New clsMarsSecurity
    Friend Shared WithEvents oSMTP As Quiksoft.EasyMail.SMTP.SMTP
    'Friend Shared WithEvents cSMTP As Chilkat.MailMan
    Dim smtpStatus As String = ""
    Dim smtpProgress As Object = 0
    Dim sSMTPErr As String = ""
    Shared nBackID As Integer = 0
    Shared FaxComplete As Boolean = False
    Shared sFaxError As String = ""
    'Friend WithEvents oFax As FaxmanJr.FaxmanJr
    Friend WithEvents faxing As DataTech.FaxManJr.Faxing

#If debug Then
    shared oSMSC as Object
#Else
    Shared oSMSC As Object ' ASMSCTRLLib.SMSC
#End If

    Shared smtplogName As String = ""

    Public Shared Function sendSMSByRedOxygen(sCellNumber As String, sMsg As String, sFile As String) As Boolean

        Dim Client As WebClient = New WebClient
        Dim URL As String
        Dim accountid, email, password As String

        accountid = clsMarsUI.MainUI.ReadRegistry("ROAccountID", "")
        email = clsMarsUI.MainUI.ReadRegistry("ROEmail", "")
        password = clsMarsUI.MainUI.ReadRegistry("ROPassword", "", True)

        If sFile.Length > 0 Then
            Dim nLeft As Integer = 159 - sMsg.Length

            If nLeft > 0 Then
                sMsg &= " " & Microsoft.VisualBasic.Left(ReadTextFromFile(sFile), nLeft)
            End If

            sMsg = sMsg.Trim

        End If


        For Each recipient As String In sCellNumber.Split(";")
            If recipient <> "" Then
                URL = ("http://www.redoxygen.net/sms.dll?Action=SendSMS" _
                 + "&AccountId=" + accountid _
                 + "&Email=" + email _
                 + "&Password=" + password _
                 + "&Recipient=" + recipient _
                 + "&Message=" + System.Web.HttpUtility.UrlEncode(sMsg))

                Dim Response() As Byte = Client.DownloadData(URL)
                Dim Result As String = Encoding.ASCII.GetString(Response)
                Dim ResultCode As Integer = System.Convert.ToInt32(Result.Substring(0, 4))

                If ResultCode <> 0 Then
                    Throw New Exception("RedOxygen exception code " & ResultCode)
                End If
            End If
        Next

        Return True
    End Function

    Public Shared Function SendSMS(ByVal sCellNumber As String, ByVal sMsg As String, ByVal sFile As String) As Boolean
        Dim SMSDevice As String
        Dim SMSFormat As String
        Dim SMSSpeed As String
        Dim DeviceType As String
        Dim SMSProtocol As String
        Dim SMSPassword As String
        Dim SMSSender As String
        Dim SMSCNumber As String
        Dim SMSType As String
        Dim SMSInit As String
        Dim UseUnicode As Boolean
        Dim oReg As RegistryKey = Registry.LocalMachine
10:     Dim oParser As New clsMarsParser
        Dim smsDebug As String = "smsDebug.debug"
20:     Try
30:         clsMarsUI.MainUI.BusyProgress(50, "Activating SMS Engine...")

            SMSType = clsMarsUI.MainUI.ReadRegistry("SMSType", "Device")

            If SMSType <> "Device" Then
                Return sendSMSByRedOxygen(sCellNumber, sMsg, sFile)
            Else

#If Not Debug Then
                oSMSC = CreateObject("ActiveXperts.SMSC") ' New ASMSCTRLLib.SMSC
#End If
40:

50:             oSMSC.Activate("157C2-9A5A9-57FFB")

                clsMarsDebug.writeToDebug(smsDebug, "----------------------Begin SMS Transmission--------------", True)
60:             clsMarsUI.MainUI.BusyProgress(70, "Configuring SMS settings..")

70:             With clsMarsUI.MainUI
80:                 SMSDevice = .ReadRegistry("SMSDevice", "")
90:                 SMSFormat = .ReadRegistry("SMSDataFormat", "8,n,1")
100:                SMSSpeed = .ReadRegistry("SMSSpeed", "9600")

                    clsMarsDebug.writeToDebug(smsDebug, "SMSDevice: " & SMSDevice, True)
                    clsMarsDebug.writeToDebug(smsDebug, "SMSFormat: " & SMSFormat, True)
                    clsMarsDebug.writeToDebug(smsDebug, "SMSSpeed: " & SMSSpeed, True)

110:                If .ReadRegistry("SMSDeviceType", "SMSC") = "GSM" Then
120:                    DeviceType = "GSM"
130:                Else
140:                    DeviceType = "SMSC"
                    End If

                    clsMarsDebug.writeToDebug(smsDebug, "Device Type: " & DeviceType, True)

150:                SMSCNumber = .ReadRegistry("SMSCNumber", "")
160:                SMSProtocol = .ReadRegistry("SMSCProtocol", "TAP")
170:                SMSPassword = .ReadRegistry("SMSCPassword", "", True)
180:                SMSSender = .ReadRegistry("SMSSender", "")
190:                SMSInit = .ReadRegistry("SMSInit", "ATZ")

                    clsMarsDebug.writeToDebug(smsDebug, "SMSCNumber: " & SMSCNumber, True)
                    clsMarsDebug.writeToDebug(smsDebug, "SMSProtocol: " & SMSProtocol, True)
                    clsMarsDebug.writeToDebug(smsDebug, "SMSSender: " & SMSSender, True)
                    clsMarsDebug.writeToDebug(smsDebug, "SMSInitString: " & SMSInit, True)

                    If SMSDevice = "" Then
                        gErrorDesc = oSMSC.LastError.ToString() + " (" + oSMSC.GetErrorDescription(oSMSC.LastError) + ")"
191:                    gErrorNumber = -187
192:                    gErrorLine = 191
193:                    gErrorSource = "SendSMS"
                        _ErrorHandle("No modem device selected", gErrorNumber, gErrorSource, gErrorLine, "Please set up SMS CDevice and other settings in Options.")
194:                    GoTo en
                    End If

200:                If DeviceType = "GSM" Then
210:                    Try
220:                        UseUnicode = Convert.ToBoolean(.ReadRegistry("UseUnicode", 0))
230:                    Catch
240:                        UseUnicode = False
                        End Try
                    End If

                    clsMarsDebug.writeToDebug(smsDebug, "Use Unicode: " & UseUnicode, True)
                End With

250:            oSMSC.Device = SMSDevice

                ' Set Logfile
                clsMarsDebug.writeToDebug(smsDebug, "Internal log file set to: " & sAppPath & "sms.log", True)

260:            oSMSC.LogFile = sAppPath & "sms.log"

270:            oSMSC.DeviceSpeed = System.Int32.Parse(SMSSpeed)

                ' Retrieve DeviceSettings
280:            Select Case SMSFormat.ToLower
                    Case "default"
290:                    oSMSC.DeviceSettings = 0
300:                Case "8,n,1"
310:                    oSMSC.DeviceSettings = 1
320:                Case "7,e,1"
330:                    oSMSC.DeviceSettings = 2
                End Select
340:            oSMSC.DeviceInitString = SMSInit

                ' Retrieve Sender/Recipient properties
350:            oSMSC.Sender = SMSSender
                ' Retrieve Message properties


360:            clsMarsUI.MainUI.BusyProgress(80, "Reading report file...")

370:            If sFile.Length > 0 Then
                    Dim nLeft As Integer = 159 - sMsg.Length

380:                If nLeft > 0 Then
390:                    sMsg &= " " & Microsoft.VisualBasic.Left(ReadTextFromFile(sFile), nLeft)
                    End If

400:                sMsg = sMsg.Trim

                End If

410:            oSMSC.MessageText = sMsg

420:            Dim oConstants As New clsSMSConstants

                clsMarsDebug.writeToDebug(smsDebug, "Setting provier type...", True)

                'Retrieve Provider properties
430:            If (SMSProtocol = "UCP") Then
440:                oSMSC.ProviderType = oConstants.asPROVIDER_TYPE_UCP
450:            Else
460:                oSMSC.ProviderType = oConstants.asPROVIDER_TYPE_TAP_DEFAULT
                End If

                clsMarsDebug.writeToDebug(smsDebug, "Setting provider dial string", True)

470:            oSMSC.ProviderDialString = SMSCNumber

                clsMarsDebug.writeToDebug(smsDebug, "Native log file set to: " & sAppPath & "sms.log", True)

480:            If SMSPassword.Length > 0 Then
                    clsMarsDebug.writeToDebug(smsDebug, "Setting provider password", True)

490:                oSMSC.ProviderPassword = SMSPassword
                End If


                ' Send Message

500:            clsMarsUI.MainUI.BusyProgress(95, "Sending SMS text...")

510:            For Each s As String In sCellNumber.Split(";")

                    Dim RetryCount As Integer = 0
Retry:
                    clsMarsDebug.writeToDebug(smsDebug, "Sending SMS to " & ResolveEmailAddress(s).Replace(";", "").Trim, True)

520:                oSMSC.Recipient = ResolveEmailAddress(s).Replace(";", "").Trim

530:                If s.Length > 0 Then
                        clsMarsDebug.writeToDebug(smsDebug, "Sending SMS...", True)


540:                    If DeviceType <> "GSM" Then
550:                        oSMSC.SendMessage(oConstants.asMESSAGE_PROVIDER)
560:                    Else
570:                        oSMSC.SendMessage(oConstants.asMESSAGE_GSM)
                        End If


580:                    If (oSMSC.LastError = oConstants.asERR_SUCCESS) Then
                            clsMarsDebug.writeToDebug(smsDebug, "SMS sent with no errors", True)

590:                        Return True
600:                    Else
                            clsMarsDebug.writeToDebug(smsDebug, "SMS transmission errored with: " & oSMSC.LastError.ToString() & " (" + oSMSC.GetErrorDescription(oSMSC.LastError) & ")", True)

610:                        gErrorDesc = oSMSC.LastError.ToString() + " (" + oSMSC.GetErrorDescription(oSMSC.LastError) + ")"
620:                        gErrorNumber = -187
630:                        gErrorLine = 530
640:                        gErrorSource = "SendSMS"
                            _ErrorHandle(s & ": Attempt " & RetryCount + 1 & " of 3:  " & gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
650:                        If RetryCount < 3 Then
660:                            RetryCount = RetryCount + 1
670:                            _Delay(30)
680:                            GoTo Retry
                            End If
                        End If
                    End If
690:            Next

700:            _Delay(30)

710:            Return True
            End If
en:
720:    Catch ex As Exception
            clsMarsDebug.writeToDebug(smsDebug, "Critical SMS error: " & ex.ToString, True)

730:        gErrorDesc = ex.Message
740:        gErrorNumber = Err.Number
750:        gErrorLine = _GetLineNumber(ex.StackTrace)
760:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
770:        Return False
        End Try

    End Function

    Private Sub AddMeToFaxQueue()
        Dim cols, vals As String
        Dim SQL As String
        Dim wait As Boolean
10:     Dim trackerID As Integer = clsMarsData.CreateDataID("FaxQueue", "TrackerID")

        'Validate all the entries in the locking table that concern us...
20:     SQL = "SELECT * FROM FaxQueue"

30:     Try
            Dim oTest As ADODB.Recordset = clsMarsData.GetData(SQL)

40:         If oTest IsNot Nothing Then
50:             Do While oTest.EOF = False
                    Dim entryID As Integer = oTest("trackerid").Value
                    Dim testID As Integer = IsNull(oTest("processid").Value, 99999)

                    Dim p As Process

60:                 Try
70:                     p = Process.GetProcessById(testID)
80:                 Catch ex As Exception
90:                     clsMarsData.WriteData("DELETE FROM FaxQueue WHERE TrackerID=" & entryID, False)
                    End Try

100:                oTest.MoveNext()
110:            Loop

120:            oTest.Close()
            End If
        Catch : End Try

        'insert into the queue
130:    Try
140:        clsMarsUI.BusyProgress(40, "Waiting for free fax thread...")

150:        cols = "TrackerID,ProcessID,EntryDate"
160:        vals = trackerID & "," & _
                  Process.GetCurrentProcess.Id & "," & _
                  "'" & ConDateTime(Now) & "'"

170:        SQL = "INSERT INTO FaxQueue (" & cols & ") VALUES (" & vals & ")"

180:        clsMarsData.WriteData(SQL)

            Dim doWait As Boolean = True

            'check if its our turn
190:        While doWait = True
200:            SQL = "SELECT * FROM FaxQueue WHERE TrackerID = (SELECT MIN(TrackerID) FROM FaxQueue)"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

210:            If oRs IsNot Nothing Then
220:                If oRs.EOF = False Then
                        Dim minProcessID As Integer = oRs("processid").Value

230:                    Try
                            Dim p As Process = Process.GetProcessById(minProcessID)
240:                    Catch ex As Exception
250:                        clsMarsData.WriteData("DELETE FROM FaxQueue WHERE ProcessID =" & minProcessID)
                        End Try

260:                    If minProcessID = Process.GetCurrentProcess.Id Then
270:                        doWait = False
                        End If
                    Else
                        doWait = False
                    End If
                End If

280:            _Delay(1)
            End While

            'We can now return and send the fax

390:    Catch ex As Exception
400:        clsMarsData.WriteData("DELETE FROM FaxQueue WHERE TrackerID =" & trackerID, False)
        End Try
    End Sub

    'Public Function oldSendFax(ByVal sFaxNumber As String, ByVal sDevice As String, ByVal sType As String, _
    'ByVal sFaxTo As String, ByVal sFaxFrom As String, ByVal sFaxComments As String, ByVal sFile As String) As Boolean
    '    Dim nPort As Integer
    '    Dim nClass As String
    '    Dim oData As New clsMarsData
    '    Dim sFiles As String

    '    clsMarsDebug.writeToDebug("faxtrace.log", "Obtaining information...", False)

    '    Try
    '        nPort = sDevice.Split(":")(1)
    '        nClass = sDevice.Split(" ")(0).ToLower.Replace("class", String.Empty)
    '    Catch ex As Exception
    '        gErrorDesc = "Could not obtain a valid device port. Please check the fax setup"
    '        gErrorNumber = "-104044"
    '        Return False
    '    End Try

    '    clsMarsDebug.writeToDebug("faxtrace.log", "Resolving fax numbers", True)

    '    sFaxNumber = clsMarsParser.Parser.ParseString(ResolveEmailAddress(sFaxNumber))
    '    FaxComplete = False

    '    For Each sx As String In sFaxNumber.Split(";")

    '        If sx.Length > 0 Then
    '            clsMarsDebug.writeToDebug("faxtrace.log", "Setting up fax for " & sx, True)

    '            Try
    '                oFax = New FaxmanJr.FaxmanJr

    '                With oFax

    '                    .Port = nPort

    '                    Select Case nClass
    '                        Case 1
    '                            .Class = FaxmanJr.FAX_DEV_CLASS.FAX_1
    '                        Case 2
    '                            .Class = FaxmanJr.FAX_DEV_CLASS.FAX_2
    '                        Case 20
    '                            .Class = FaxmanJr.FAX_DEV_CLASS.FAX_20
    '                    End Select

    '                    .FaxNumber = sx.Replace(" ", String.Empty).Trim
    '                    .FaxResolution = FaxmanJr.Resolution.High
    '                    .UserCompany = sFaxFrom
    '                    .FaxCompany = sFaxTo
    '                    .FaxComments = sFaxComments


    '                    Dim sPath As String

    '                    sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "\Output\")

    '                    If sPath.EndsWith("\") = False Then
    '                        sPath &= "\Fax Jobs\"
    '                    Else
    '                        sPath &= "Fax Jobs\"
    '                    End If

    '                    If IO.Directory.Exists(sPath) = False Then
    '                        IO.Directory.CreateDirectory(sPath)
    '                    End If

    '                    Dim freeFile As Boolean = False
    '                    Dim fileCount As Integer = 0


    '                    clsMarsDebug.writeToDebug("faxtrace.log", "Obtaining free fax file", True)

    '                    Do
    '                        sPath = sPath & "Fax" & fileCount & ".fmf"

    '                        Try
    '                            If IO.File.Exists(sPath) Then
    '                                System.IO.File.Delete(sPath)
    '                            End If

    '                            freeFile = True
    '                        Catch
    '                            fileCount += 1
    '                        End Try
    '                    Loop Until freeFile = True

    '                    clsMarsDebug.writeToDebug("faxtrace.log", "Creating free fax file", True)

    '                    If sType = "Single" Then
    '                        .ImportFiles(sPath, sFile)

    '                        .FaxFiles = sPath
    '                    ElseIf sType = "Package" Then
    '                        For Each s As String In rptFileNames
    '                            If s.Length > 0 Then
    '                                'sFiles &= Chr(34) & s & Chr(34) & "+"
    '                                sFiles &= s & "+"
    '                            End If
    '                        Next

    '                        sFiles = sFiles.Substring(0, sFiles.Length - 1)

    '                        .ImportFiles(sPath, sFiles)

    '                        .FaxFiles = sPath
    '                    End If

    '                    clsMarsDebug.writeToDebug("faxtrace.log", "Adding to fax queue", True)

    '                    'lets check the fax queue to see if we can start faxing
    '                    Me.AddMeToFaxQueue()

    '                    Dim waitStart As Date = Now

    '                    clsMarsDebug.writeToDebug("faxtrace.log", "Waiting for device to be READY", True)

    '                    Do While oFax.Status <> FaxmanJr._DEVICE_STAT.DEV_READY
    '                        clsMarsUI.BusyProgress("Waiting for modem...")
    '                        Application.DoEvents()
    '                        System.Threading.Thread.Sleep(3000)

    '                        If Now.Subtract(waitStart).TotalMinutes > 5 Then
    '                            Throw New Exception("Waiting for modem to be ready time-out (5 minutes)")
    '                        End If
    '                    Loop

    '                    clsMarsDebug.writeToDebug("faxtrace.log", "Sending fax to " & sx, True)

    '                    .SendFax()
    '                End With

    '                clsMarsDebug.writeToDebug("faxtrace.log", "Waiting for fax send to complete", True)

    '                Do While FaxComplete = False
    '                    Application.DoEvents()
    '                    System.Threading.Thread.Sleep(3000)
    '                Loop

    '                clsMarsData.WriteData("DELETE FROM FaxQueue WHERE ProcessID =" & Process.GetCurrentProcess.Id)

    '                clsMarsDebug.writeToDebug("faxtrace.log", "Fax send completed to " & sx, True)

    '            Catch ex As Exception
    '                Dim sMsg As String = ""

    '                If ex.Message = "No Response to Command" Then
    '                    sMsg = "CRD was unable to connect to your Fax Modem, initikize it or " & _
    '                    "receive a positive response from it.  Check that your modem is " & _
    '                    "configured correctly and theat the modem cable is attached to the " & _
    '                    "correct port and also to a phone line."
    '                End If

    '                gErrorDesc = ex.Message
    '                gErrorNumber = Err.Number
    '                gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
    '                gErrorLine = _GetLineNumber(ex.StackTrace)
    '                gErrorSuggest = sMsg

    '                clsMarsUI.MainUI.BusyProgress(, , True)

    '                'MessageBox.Show(ex.ToString)

    '                Return False
    '            End Try
    '        End If
    '    Next

    '    If sFaxError.Length > 0 Then
    '        Dim sMsg As String = ""

    '        'MessageBox.Show(sFaxError)

    '        gErrorDesc = sFaxError
    '        gErrorNumber = -507
    '        gErrorSource = "clsMarsMessaging.SendFax"
    '        gErrorLine = "97"

    '        If sFaxError.ToLower.IndexOf("no response to command") > -1 Then
    '            sMsg = "CRD was unable to connect to your Fax Modem, initikize it or " & _
    '            "receive a positive esponse from it.  Check that your modem is " & _
    '            "configured correctly and theat the modem cable is attached to the " & _
    '            "correct port and also to a phone line."
    '        End If

    '        gErrorSuggest = sMsg

    '        clsMarsUI.MainUI.BusyProgress(, , True)
    '        Return False
    '    Else
    '        clsMarsUI.MainUI.BusyProgress(, , True)
    '        Return True
    '    End If

    'End Function
    Public Function SendFax(ByVal sFaxNumber As String, ByVal sDevice As String, ByVal sType As String, _
           ByVal sFaxTo As String, ByVal sFaxFrom As String, ByVal sFaxComments As String, ByVal sFile As String, Optional ByVal isDynamic As Boolean = False) As Boolean
        Dim nPort As Integer
        Dim nClass As String
10:     Dim oData As New clsMarsData
        Dim sFiles As String

20:     clsMarsDebug.writeToDebug("faxtrace.log", "Obtaining information...", False)

30:     Try
            nPort = sDevice.Split(":")(1)
40:         nClass = sDevice.Split(" ")(0).ToLower.Replace("class", String.Empty)
50:     Catch ex As Exception
60:         gErrorDesc = "Could not obtain a valid device port. Please check the fax setup"
70:         gErrorNumber = "-104044"
80:         Return False
        End Try

90:     clsMarsDebug.writeToDebug("faxtrace.log", "Resolving fax numbers", True)

100:    sFaxNumber = clsMarsParser.Parser.ParseString(ResolveEmailAddress(sFaxNumber))
110:    FaxComplete = False

120:    For Each sx As String In sFaxNumber.Split(";")

130:        If sx.Length > 0 Then
140:            clsMarsDebug.writeToDebug("faxtrace.log", "Setting up fax for " & sx, True)

150:            Try
160:                faxing = New DataTech.FaxManJr.Faxing

                    'set up the modem
170:                Dim modem As DataTech.FaxManJr.Modem = New DataTech.FaxManJr.Modem
                    Dim modemClass As DataTech.FaxManJr.FaxClass

180:                Select Case nClass
                        Case 1
190:                        modemClass = DataTech.FaxManJr.FaxClass.Class1
200:                    Case 2
210:                        modemClass = DataTech.FaxManJr.FaxClass.Class2
220:                    Case 20
230:                        modemClass = DataTech.FaxManJr.FaxClass.Class20
240:                    Case 21
250:                        modemClass = DataTech.FaxManJr.FaxClass.Class21
260:                    Case Else
270:                        modemClass = DataTech.FaxManJr.FaxClass.ClassNotSet
                    End Select

280:                modem.FaxClass = modemClass
290:                modem.Port = nPort

300:                faxing.Modem = modem

                    'set up the fax
310:                Dim fax As DataTech.FaxManJr.Fax = New DataTech.FaxManJr.Fax
320:                fax.FaxNumber = sx.Replace(" ", "").Trim
330:                fax.FaxResolution = DataTech.FaxManJr.FaxResolution.High
340:                fax.Company = sFaxTo
350:                fax.Comments = sFaxComments
360:                fax.SenderName = sFaxFrom

                    'set up the output path
                    Dim sPath As String = ""

370:                sPath = clsMarsReport.oReport.m_OutputFolder & "\Fax Jobs\"

                    '380:                If sPath.EndsWith("\") = False Then
                    '390:                    sPath &= "\Fax Jobs\"
                    '400:                Else
                    '410:                    sPath &= "Fax Jobs\"
                    '                    End If

420:                If IO.Directory.Exists(sPath) = False Then
430:                    IO.Directory.CreateDirectory(sPath)
                    End If

                    Dim freeFile As Boolean = False

440:                clsMarsDebug.writeToDebug("faxtrace.log", "Creating free fax file", True)

450:                If sType = "Single" Then
460:                    Do
470:                        sPath = sPath & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".fmf"

480:                        Try
490:                            If IO.File.Exists(sPath) Then
500:                                System.IO.File.Delete(sPath)
                                End If

510:                            freeFile = True
                            Catch : End Try
520:                    Loop Until freeFile = True

530:                    faxing.ImportFiles(sFile, sPath)
540:                    fax.FaxFiles.Add(sPath)
550:                ElseIf sType = "Package" Then

560:                    For Each s As String In rptFileNames
570:                        sPath = clsMarsReport.oReport.m_OutputFolder & "\Fax Jobs\"

580:                        Do
590:                            sPath = sPath & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".fmf"

600:                            Try
610:                                If IO.File.Exists(sPath) Then
620:                                    System.IO.File.Delete(sPath)
                                    End If

630:                                freeFile = True
                                Catch : End Try
640:                        Loop Until freeFile = True

650:                        faxing.ImportFiles(s, sPath)

660:                        fax.FaxFiles.Add(sPath)
670:                    Next
                    End If

680:                clsMarsDebug.writeToDebug("faxtrace.log", "Adding to fax queue", True)

                    'lets check the fax queue to see if we can start faxing
690:                Me.AddMeToFaxQueue()

                    Dim waitStart As Date = Now

700:                clsMarsDebug.writeToDebug("faxtrace.log", "Waiting for device to be READY", True)

710:                If faxing.DeviceStatus = DataTech.FaxManJr.DeviceStatuses.DeviceNotSet Then
720:                    Throw New Exception("No device has been specified for faxing")
                    End If

730:                Do While faxing.DeviceStatus <> DataTech.FaxManJr.DeviceStatuses.DeviceReady
740:                    clsMarsUI.BusyProgress("Waiting for modem...")
750:                    Application.DoEvents()
760:                    System.Threading.Thread.Sleep(1000)
770:                    If Now.Subtract(waitStart).TotalMinutes > 5 Then
780:                        Throw New Exception("Waiting for modem to be ready time-out (5 minutes)")
                        End If
790:                Loop

800:                clsMarsDebug.writeToDebug("faxtrace.log", "Sending fax to " & sx, True)
810:                faxing.Send(fax)

820:                clsMarsDebug.writeToDebug("faxtrace.log", "Waiting for fax send to complete", True)

830:                Do While FaxComplete = False
840:                    Application.DoEvents()
850:                    'System.Threading.Thread.Sleep(3000)
860:                Loop

870:                clsMarsData.WriteData("DELETE FROM FaxQueue WHERE ProcessID =" & Process.GetCurrentProcess.Id)

880:                clsMarsDebug.writeToDebug("faxtrace.log", "Fax send completed to " & sx, True)

890:            Catch ex As Exception
                    Dim sMsg As String = ""

900:                If ex.Message = "No Response to Command" Then
910:                    sMsg = "SQL-RD was unable to connect to your Fax Modem, initialize it or " & _
      "receive a positive response from it.  Check that your modem is " & _
      "configured correctly and that the modem cable is attached to the " & _
      "correct port and also to a phone line."
                    End If

                    If isDynamic = False Then
920:                    gErrorDesc = ex.Message
                    Else
                        gErrorDesc = "Key Value: " & gKeyValue & " - " & ex.Message
                    End If

930:                gErrorNumber = Err.Number
940:                gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
950:                gErrorLine = Erl()
960:                gErrorSuggest = sMsg

                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True, True)

970:                clsMarsUI.MainUI.BusyProgress(, , True)

                    'MessageBox.Show(ex.ToString)

980:                Return False
                End Try
            End If
990:    Next

1000:   If sFaxError.Length > 0 Then
            Dim sMsg As String = ""

            'MessageBox.Show(sFaxError)

            If isDynamic = False Then
1010:           gErrorDesc = sFaxError
            Else
                gErrorDesc = "Key Value: " & gKeyValue & " - " & sFaxError
            End If

1020:       gErrorNumber = -507
1030:       gErrorSource = "clsMarsMessaging.SendFax"
1040:       gErrorLine = "97"

1050:       If sFaxError.ToLower.IndexOf("no response to command") > -1 Then
1060:           sMsg = "SQL-RD was unable to connect to your Fax Modem, initialize it or " & _
            "receive a positive response from it.  Check that your modem is " & _
            "configured correctly and that the modem cable is attached to the " & _
            "correct port and also to a phone line."
            End If

1070:       gErrorSuggest = sMsg

            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True, True)

1080:       clsMarsUI.MainUI.BusyProgress(, , True)
1090:       Return False
1100:   Else
1110:       clsMarsUI.MainUI.BusyProgress(, , True)
1120:       Return True
        End If

    End Function

    Private Shared Sub LogReceiptRqst(ByVal destinationID As Integer, ByVal entryDate As Date, ByVal emailSubject As String, _
    ByVal sentTo As String, Optional ByVal sendCc As String = "", Optional ByVal sendBcc As String = "")
        Dim vals As String
        Dim cols As String

        cols = "LogID,DestinationID,EmailSubject,SentTo,EntryDate,NextCheck,RRStatus"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReadReceiptsAttr WHERE DestinationID =" & destinationID)

        If oRs Is Nothing Then Return

        If oRs.EOF = True Then Return

        Dim waitFor As Integer = oRs("waitlength").Value
        Dim nextCheck As Date = entryDate.AddMinutes(waitFor)


        If RunEditor = True Then
            entryDate = CTimeZ(entryDate, dateConvertType.WRITE)
        End If

        oRs.Close()

        For Each s As String In sentTo.Split(";")

            If s.Length > 0 Then
                vals = clsMarsData.CreateDataID("ReadReceiptsLog", "logid") & "," & _
                destinationID & "," & _
                "'" & SQLPrepare(emailSubject) & "'," & _
                "'" & SQLPrepare(s) & "'," & _
                "'" & ConDateTime(entryDate) & "'," & _
                "'" & nextCheck & "'," & _
                "'Pending'"

                clsMarsData.DataItem.InsertData("ReadReceiptsLog", cols, vals, False)
            End If
        Next

        If sendCc.Length > 0 Then
            For Each s As String In sendCc.Split(";")

                If s.Length > 0 Then
                    vals = clsMarsData.CreateDataID("ReadReceiptsLog", "logid") & "," & _
                    destinationID & "," & _
                    "'" & SQLPrepare(emailSubject) & "'," & _
                    "'" & SQLPrepare(s) & "'," & _
                    "'" & ConDateTime(entryDate) & "'," & _
                    "'" & nextCheck & "'," & _
                    "'Pending'"

                    clsMarsData.DataItem.InsertData("ReadReceiptsLog", cols, vals, False)
                End If
            Next
        End If

        If sendBcc.Length > 0 Then
            For Each s As String In sendBcc.Split(";")

                If s.Length > 0 Then
                    vals = clsMarsData.CreateDataID("ReadReceiptsLog", "logid") & "," & _
                    destinationID & "," & _
                    "'" & SQLPrepare(emailSubject) & "'," & _
                    "'" & SQLPrepare(s) & "'," & _
                    "'" & ConDateTime(entryDate) & "'," & _
                    "'" & nextCheck & "'," & _
                    "'Pending'"

                    clsMarsData.DataItem.InsertData("ReadReceiptsLog", cols, vals, False)
                End If
            Next
        End If
    End Sub

    Public Shared Function sendExchangeWDSMail(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
               ByVal SendType As String, Optional ByVal Attach As String = "", _
               Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
               Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
               Optional ByVal InBody As Boolean = False, _
               Optional ByVal sFormat As String = "", _
               Optional ByVal sName As String = "", _
               Optional ByVal LogFailure As Boolean = True, Optional ByVal sEmbedFile As String = "", _
               Optional ByVal readReceipt As Boolean = False, Optional ByVal destinationid As Integer = 0) As Boolean

        Dim oUI As clsMarsUI = New clsMarsUI

        Dim exchangeWDSUrl As String = oUI.ReadRegistry("ExchangeWDSUrl", "")
        Dim exchangeWDSUserID As String = oUI.ReadRegistry("ExchangeWDSUserID", "")
        Dim exchangeWDSPassword As String = oUI.ReadRegistry("ExchangeWDSPassword", "", True, )
        Dim exchangeWDSSender As String = oUI.ReadRegistry("ExchangeWDSSender", "")
        Dim exchangeWDSSenderAddress As String = oUI.ReadRegistry("exchangeWDSSenderAddress", "")
        Dim autoDiscover As Boolean = Convert.ToInt16(oUI.ReadRegistry("ExchangeAutoDiscover", 0))
        Dim autoDiscoverEmail As String = oUI.ReadRegistry("ExchangeAutoDiscoverEmailAddress", "")
        Dim exdomain As String = ""
        Dim exuserid As String = ""
        Dim exchange As exchangeWDS.exchangeMail

        If g_exchangeWDS Is Nothing Then
            If exchangeWDSUserID.Contains("\") Then
                exuserid = exchangeWDSUserID.Split("\")(1)
                exdomain = exchangeWDSUserID.Split("\")(0)
            Else
                exuserid = exchangeWDSUserID
                exdomain = Environment.UserDomainName
            End If

            If autoDiscover And autoDiscoverEmail <> "" Then
                Dim service As Microsoft.Exchange.WebServices.Data.ExchangeService = New Microsoft.Exchange.WebServices.Data.ExchangeService(Microsoft.Exchange.WebServices.Data.ExchangeVersion.Exchange2007_SP1)
                service.Credentials = New System.Net.NetworkCredential(exuserid, exchangeWDSPassword, exdomain)

                Try
                    service.AutodiscoverUrl(autoDiscoverEmail)
                    exchangeWDSUrl = service.Url.ToString
                Catch : End Try
            End If

            If exchangeWDSUserID = "" Then
                exchange = New exchangeWDS.exchangeMail(exchangeWDSUrl)
            Else
                exchange = New exchangeWDS.exchangeMail(exchangeWDSUrl, exuserid, exchangeWDSPassword, exdomain, autoDiscoverEmail)
            End If

            g_exchangeWDS = exchange
        Else
            exchange = g_exchangeWDS
        End If

        Dim filesArray As ArrayList = New ArrayList

        If SendType = "Single" Then
            filesArray.Add(Attach)
        Else
            If rptFileNames IsNot Nothing Then
                For Each f As String In rptFileNames
                    filesArray.Add(f)
                Next
            End If
        End If

        '//add any extra attachments
        If Extras <> "" Then
            For Each s As String In Extras.Split(";")
                If s <> "" Then
                    If s.IndexOf("*") > -1 Then
                        Dim compareMe As String = ExtractFileName(s)

                        For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
                            If ExtractFileName(x) Like compareMe Then
                                filesArray.Add(x)
                            End If
                        Next
                    Else
                        If IO.File.Exists(s) Then
                            filesArray.Add(s)
                        Else
                            _ErrorHandle("Missing custom attachment: " & s, 5, Reflection.MethodBase.GetCurrentMethod.Name, 730, "Could not locate custom attachment." & vbCrLf & vbCrLf & _
   "The email was sent without it." & vbCrLf & vbCrLf & _
   "To ensure that the email is sent with your specified attachment(s) in the future, please make sure that it exists at the location specified by the destination setup.", True)

                        End If
                    End If

                End If
            Next
        End If

        Dim bodyType As exchangeWDS.exchangeMail.EmailBodyFormatEnum ' Microsoft.Exchange.WebServices.Data.BodyType   'exchangeWDS.exchange.BodyTypeType

        If sFormat = "HTML" Then
            bodyType = exchangeWDS.exchangeMail.EmailBodyFormatEnum.HTML  ' exchangeWDS.exchange.BodyTypeType.HTML
        ElseIf sFormat = "IMAGE" Then
            bodyType = exchangeWDS.exchangeMail.EmailBodyFormatEnum.IMAGE
        Else
            bodyType = Microsoft.Exchange.WebServices.Data.BodyType.Text ' exchangeWDS.exchange.BodyTypeType.Text
        End If

        Dim files As String() = filesArray.ToArray(GetType(System.String))

        exchange.sendMailByExchangeSDK20(Addresses, Subject, Body, exchangeWDSSenderAddress, exchangeWDSSender, files, , SendCC, SendBCC, InBody, bodyType, _
                          sName, sEmbedFile, readReceipt)

        'exchange.sendMail(Addresses, Subject, Body, exchangeWDSSenderAddress, exchangeWDSSender, files, , SendCC, SendBCC, InBody, bodyType, _
        '                 sName, sEmbedFile, readReceipt)

        Return True
    End Function

    Private Shared Function embedMAPIReport(ByRef msg As Redemption.RDOMail, htmlPath As String, reportPath As String) As Boolean
        Try
            Dim PT_BOOLEAN = 11
            Dim PR_HIDE_ATTACH = msg.GetIDsFromNames("{00062008-0000-0000-C000-000000000046}", &H8514) Or PT_BOOLEAN

            msg.Fields(PR_HIDE_ATTACH) = True

            Dim reportContents As String = IO.File.ReadAllText(reportPath)

            For Each File As String In IO.Directory.GetFiles(IO.Path.Combine(htmlPath, "images"))
                Dim attachment As Object = msg.Attachments.Add(File)
                attachment.Fields(&H370E001E) = "image/jpeg" '//content type 
                attachment.Fields(&H3712001E) = IO.Path.GetFileNameWithoutExtension(File) '//cid

                reportContents = reportContents.Replace("images/" & IO.Path.GetFileName(File), "cid:" & IO.Path.GetFileNameWithoutExtension(File)) '//images/OrdersByMonth_0810c574-cefe-4231-ba85-34a38d8f795f.png
            Next

            msg.BodyFormat = 2
            msg.HTMLBody = reportContents
            msg.DownloadPictures = True

            Return True
        Catch ex As Exception
            _ErrorHandle("Error embedding report into MAPI email: " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
            Return False
        End Try
    End Function
    Public Shared Function SendMAPI(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
               ByVal SendType As String, Optional ByVal Attach As String = "", _
               Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
               Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
               Optional ByVal InBody As Boolean = False, _
               Optional ByVal sFormat As String = "", _
               Optional ByVal sName As String = "", _
               Optional ByVal LogFailure As Boolean = True, Optional ByVal sEmbedFile As String = "", _
               Optional ByVal readReceipt As Boolean = False, Optional ByVal destinationid As Integer = 0) As Boolean


        'Dim SafeItem As Redemption.SafeMailItem
        Dim rdoMessage As Object
        Dim rdoSession As Object
        Dim I As Integer
        Dim ColCount As Integer
        Dim Recs() As String
        Dim UserID As String
        Dim Password As String
        Dim Utils As Redemption.MAPIUtils
        Dim SentItemsFolderID As Object
        Dim cdoReps As Object
        Dim sRead As String
        Dim EmailRetry As Boolean
        Dim sMode As String
        Dim sAttach As String
        Dim sInBody As String
        Dim sAlias As String
        Dim sxServer As String
        Dim MAPIType As String
        Dim WinUser As String
        Dim WinPassword As String
        Dim WinDomain As String
        Dim oFile As String
        Dim sArgs() As String

        clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "---------------------------start-------------------------", False)

        'lets make sure that the addresses are solved
        clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Resolving addresses", True)

        Addresses = clsMarsMessaging.ResolveEmailAddress(Addresses)
        SendCC = clsMarsMessaging.ResolveEmailAddress(SendCC)
        SendBCC = clsMarsMessaging.ResolveEmailAddress(SendBCC)

10:     Try
20:         Const olCC = 2
30:         Const olBCC = 3

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Configuring server information", True)

40:         sMode = clsMarsUI.MainUI.ReadRegistry("SQL-RDService", "")

50:         sArgs = Environment.GetCommandLineArgs

60:         UserID = clsMarsUI.MainUI.ReadRegistry("MAPIProfile", "")

70:         Password = clsMarsUI.MainUI.ReadRegistry("MAPIPassword", "", True)

80:         MAPIType = clsMarsUI.MainUI.ReadRegistry("MAPIType", "")

90:         sAlias = clsMarsUI.MainUI.ReadRegistry("MAPIAlias", "")

100:        sxServer = clsMarsUI.MainUI.ReadRegistry("MAPIServer", "")

110:        EmailRetry = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("EmailRetry", 0)))

120:        rdoSession = CreateObject("Redemption.RDOSession") 'New MAPI.Session


            Select Case MAPIType
                Case "Single"
                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Logging into Outlook with " & UserID, True)

130:                rdoSession.Logon(UserID, Password, False, True)
                Case "Server"
                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Logging into Exchange server " & sxServer & " as " & sAlias, True)

131:                rdoSession.LogonExchangeMailbox(sAlias, sxServer)
                Case "ExchangeWDS" '//new shit - using the exchange web service
                    Return sendExchangeWDSMail(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, InBody, _
                        sFormat, sName, LogFailure, sEmbedFile, readReceipt, destinationid)
            End Select

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Getting Outbox", True)

140:        Dim Outbox As Object = rdoSession.GetDefaultFolder(4)

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Creating message in Outbox", True)

150:        rdoMessage = Outbox.Items.Add

160:        Dim cdoRep As Object

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Configuring message", True)

170:        With CType(rdoMessage, Redemption.RDOMail)

180:            If Addresses <> "" Then
                    Dim actualList As String

190:                For Each s As String In Addresses.Split(";")
200:                    If s <> "" Then
210:                        actualList &= s & ";"
                        End If
220:                Next

                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "To: " & actualList, True)

230:                .To = actualList
                End If


240:            If SendCC <> "" Then
                    Dim actualList As String

250:                For Each s As String In SendCC.Split(";")
260:                    If s <> "" Then
270:                        actualList &= s & ";"
                        End If
280:                Next

                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Cc: " & actualList, True)

290:                .CC = actualList
300:            End If

310:            If SendBCC <> "" Then
320:                Dim actualList As String

330:                For Each s As String In SendBCC.Split(";")
340:                    If s <> "" Then
350:                        actualList &= s & ";"
                        End If
360:                Next

                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Bcc: " & actualList, True)

370:                .BCC = actualList
380:            End If

                If sFormat = "HTML" Then
                    .BodyFormat = 2

                    If InBody And sEmbedFile <> "" Then
                        embedMAPIReport(rdoMessage, IO.Path.GetDirectoryName(sEmbedFile), sEmbedFile)
                    Else
                        Try
                            Dim cleanHTML As String = Body

                            Dim startp, endp As Integer

                            startp = cleanHTML.IndexOf("<HEAD>") + 6
                            endp = cleanHTML.IndexOf("</HEAD>")

                            Dim crap As String = cleanHTML.Substring(startp, endp - startp)

                            cleanHTML = cleanHTML.Remove(startp, crap.Length)

                            Body = cleanHTML
                        Catch ex As Exception
                            '//use regular expression
                            Dim val As String = Body
                            val = System.Text.RegularExpressions.Regex.Replace(val, "<HEAD>.*</HEAD>", "<HEAD></HEAD>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
                            Body = val
                        End Try

                        Dim autoEmbedImages As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AutoEmbedImagesIntoHTMLEmails", 1))

                        If autoEmbedImages Then embedHTMLIntoMAPIEmail(rdoMessage, Body)
                    End If
                ElseIf sFormat = "IMAGE" Then
                    embedPNGIntoMAPIMail(rdoMessage, sEmbedFile)
                Else
                    .BodyFormat = 1

390:                If InBody = True And sEmbedFile <> "" Then
400:                    Dim sBody As String = ReadTextFromFile(sEmbedFile)
410:                    .Body = sBody
420:                Else
430:                    If Body IsNot Nothing Then
440:                        .Body = Body
                        End If
450:                End If
                End If

                clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Subject: " & Subject, True)

460:            .Subject = Subject

470:            If SendType = "Single" Then
480:                For Each s As String In Attach.Split(";")
490:                    If s <> "" Then
                            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Adding attachment: " & s, True)
                            .Attachments.Add(s)
                        End If

500:                Next
510:            ElseIf SendType = "Package" Then
                    If rptFileNames IsNot Nothing Then
520:                    For Each oFile In rptFileNames

530:                        If oFile <> "" Then
                                clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Adding attachment: " & oFile, True)
                                .Attachments.Add(oFile)
                            End If

540:                    Next
                    End If
550:            End If

560:            If Extras <> "" Then

561:                Extras = clsMarsParser.Parser.ParseString(Extras)

                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Adding custom attachments", True)

570:                For Each s As String In Extras.Split(";")

580:                    If s.Length > 0 Then
590:                        If s.IndexOf("*") > -1 Then
                                Dim compareMe As String = ExtractFileName(s)

600:                            For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
610:                                If ExtractFileName(x) Like compareMe Then
                                        clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Attaching " & x, True)
620:                                    .Attachments.Add(x)
                                    End If
630:                            Next
640:                        Else
650:                            If IO.File.Exists(s) Then
                                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Attaching " & s, True)
660:                                .Attachments.Add(s)
670:                            Else

                                    _ErrorHandle("Missing custom attachment: " & s, 5, Reflection.MethodBase.GetCurrentMethod.Name, 730, "Could not locate custom attachment." & vbCrLf & vbCrLf & _
           "The email was sent without it." & vbCrLf & vbCrLf & _
           "To ensure that the email is sent with your specified attachment(s) in the future, please make sure that it exists at the location specified by the destination setup.", True)

                                End If
680:                        End If

690:                    End If
700:                Next
710:            End If

720:            Utils = New Redemption.MAPIUtils

730:            If readReceipt = True And destinationid > 0 Then
                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Setting to request read receipts", True)
740:                .ReadReceiptRequested = True
                    .OriginatorDeliveryReportRequested = True
                End If

                clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Resolving email addresses", True)
750:            .Recipients.ResolveAll()
                clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Saving message", True)
760:            .Save()
                clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Sending message", True)
770:            .Send()

780:            Utils.DeliverNow()
790:            Utils.Cleanup()

800:            If readReceipt = True And destinationid > 0 Then
                    clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Logging read receipts", True)
810:                LogReceiptRqst(destinationid, Now, rdoMessage.Subject, Addresses, SendCC, SendBCC)
820:            End If

830:        End With

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "cleaning up", True)
840:        rdoMessage = Nothing
850:        rdoSession = Nothing
860:        Utils = Nothing

870:        clsMarsUI.MainUI.BusyProgress(, , True)

880:        Return True

890:    Catch ex As Exception

            clsMarsDebug.writeToDebug("mapi_" & sName & ".log", "Error: " & ex.Message, True)
            Dim sError As String
            Dim sxSuggest As String = ""

900:        sError = ex.Message

910:        If sError.ToLower.IndexOf("could not resolve the message") > -1 Then
                sxSuggest = "This can be caused by:" & vbCrLf & _
           "- names not recognised in the MAPI address book" & vbCrLf & _
           "- more than one match is found in the address and would normally require the " & _
           "user to pick from the list of matches." & vbCrLf & vbCrLf & _
           "Please check the above and rectify."
            ElseIf sError.ToLower.Contains(":OpenMsgStore") Then
                sxSuggest = "Please make sure that you have full access rights to the MAPI message store."
            ElseIf sError.Contains("0x8004060C") Then
                sxSuggest = "The Outlook PST file is full or off-line.  Please resolve and try again.  You can find more information on this error by running a Google search 'Error in Send: 0x8004060C'"
            End If

920:        If LogFailure = True And EmailRetry = True Then
930:            AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
      InBody, sFormat, sError)
                gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
940:            gErrorSource = "SendMAPI"
950:            gErrorNumber = -214790210
960:            gErrorLine = Erl()
970:            gErrorSuggest = sxSuggest
980:        Else
                gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
990:            gErrorNumber = -214790210
1000:           gErrorSource = "SendMAPI"
1010:           gErrorLine = Erl()
1020:           gErrorSuggest = sxSuggest
            End If

1030:       Return False
        End Try
    End Function
    '    Public Shared Function SendMAPI(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
    '    ByVal SendType As String, Optional ByVal Attach As String = "", _
    '    Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
    '    Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
    '    Optional ByVal InBody As Boolean = False, _
    '    Optional ByVal sFormat As String = "", _
    '    Optional ByVal sName As String = "", _
    '    Optional ByVal LogFailure As Boolean = True, Optional ByVal sEmbedFile As String = "", _
    '    Optional ByVal readReceipt As Boolean = False, Optional ByVal destinationid As Integer = 0) As Boolean

    '        On Error GoTo Trap
    '        'Dim SafeItem As Redemption.SafeMailItem
    '        Dim cdoMessage As Object
    '        Dim cdoSession As Object
    '        Dim I As Integer
    '        Dim ColCount As Integer
    '        Dim Recs() As String
    '        Dim UserID As String
    '        Dim Password As String
    '        Dim Utils As Redemption.MAPIUtils
    '        Dim SentItemsFolderID As Object
    '        Dim cdoReps As Object
    '        Dim sRead As String
    '        Dim EmailRetry As Boolean
    '        Dim sMode As String
    '        Dim sAttach As String
    '        Dim sInBody As String
    '        Dim sAlias As String
    '        Dim sxServer As String
    '        Dim MAPIType As String
    '        Dim WinUser As String
    '        Dim WinPassword As String
    '        Dim WinDomain As String
    '        Dim oFile As String
    '        Dim sArgs() As String

    '        Const olCC = 2
    '        Const olBCC = 3

    '10:     sMode = clsMarsUI.MainUI.ReadRegistry("CRDService", "")

    '20:     sArgs = Environment.GetCommandLineArgs

    '30:     If RunEditor = False And sMode = "WindowsNT" Then
    '40:         AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '             InBody, sFormat, "", , , , readReceipt, destinationid)
    '50:         Return True
    '60:     End If

    '70:     UserID = clsMarsUI.MainUI.ReadRegistry("MAPIProfile", "")

    '80:     Password = clsMarsUI.MainUI.ReadRegistry("MAPIPassword", "", True)

    '90:     MAPIType = clsMarsUI.MainUI.ReadRegistry("MAPIType", "")

    '100:    sAlias = clsMarsUI.MainUI.ReadRegistry("MAPIAlias", "")

    '110:    sxServer = clsMarsUI.MainUI.ReadRegistry("MAPIServer", "")

    '120:    EmailRetry = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("EmailRetry", 0)))


    '        'to avoid mapi memory errors--------------------------------------------------
    '        'Dim sResult As String
    '        'Dim oSend As New SendMAPI.clsMAPI

    '        'sResult = oSend.SendMAPI(Addresses, Subject, Body, SendType, UserID, Password, _
    '        'rptFileNames, Attach, Numattach, Extras, SendCC, SendBCC)
    '        '-----------------------------------------------------------------------------

    '130:    cdoSession = CreateObject("Redemption.RDOSession") 'New MAPI.Session

    '140:    If MAPIType = "Single" Then
    '150:        cdoSession.Logon(UserID, Password, False, True)
    '160:    Else
    '170:        cdoSession.LogonExchangeMailbox(sAlias, sxServer)
    '180:    End If


    '181:    Dim Outbox As Object = cdoSession.GetDefaultFolder(4)

    '190:    cdoMessage = Outbox.Items.Add

    '200:    'cdoReps = cdoMessage.Recipients

    '        '210:    SafeItem = New Redemption.SafeMailItem     'Create an instance of Redemption.SafeMailItem
    '210:    Dim cdoRep As Object

    '220:    With cdoMessage
    '230:        'SentItemsFolderID = cdoSession.GetDefaultFolder(3).EntryID

    '            If Addresses <> "" Then
    '231:            .To = Addresses
    '            End If
    '330:        If SendCC <> "" Then
    '331:            .Cc = SendCC
    '400:        End If

    '410:        If SendBCC <> "" Then
    '411:            .Bcc = SendBCC
    '480:        End If

    '490:        If InBody = True And sEmbedFile <> "" Then
    '500:            Dim sBody As String = ReadTextFromFile(sEmbedFile)
    '510:            .Body = sBody
    '520:        Else
    '530:            If Body IsNot Nothing Then
    '532:                .body = Body
    '                End If
    '540:        End If

    '550:        .Subject = Subject

    '560:        If SendType = "Single" Then
    '570:            For Each s As String In Attach.Split(";")
    '580:                If s <> "" Then .Attachments.Add(s)
    '590:            Next
    '600:        ElseIf SendType = "Package" Then
    '610:            For Each oFile In rptFileNames
    '611:                If oFile IsNot Nothing Then
    '620:                    If oFile <> "" Then .Attachments.Add(oFile)
    '621:                End If
    '630:            Next
    '640:        End If

    '650:        If Extras <> "" Then
    '660:            For Each s As String In Extras.Split(";")

    '670:                If s <> "" Then
    '680:                    If s.IndexOf("*") > -1 Then
    '                            Dim compareMe As String = ExtractFileName(s)

    '690:                        For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
    '                                If ExtractFileName(x) Like compareMe Then
    '700:                                .Attachments.Add(x)
    '                                End If
    '710:                        Next
    '720:                    Else
    '                            If IO.File.Exists(s) Then
    '730:                            .Attachments.Add(s)
    '                            Else

    '                                _ErrorHandle("Missing custom attachment: " & s, 5, Reflection.MethodBase.GetCurrentMethod.Name, 730, "Could not locate custom attachment." & vbCrLf & vbCrLf & _
    '                                "The email was sent without it." & vbCrLf & vbCrLf & _
    '                                "To ensure that the email is sent with your specified attachment(s) in the future, please make sure that it exists at the location specified by the destination setup.", True)

    '                            End If
    '740:                    End If

    '1130:               End If
    '1140:           Next
    '770:        End If

    '780:        Utils = New Redemption.MAPIUtils

    '791:        If readReceipt = True And destinationid > 0 Then
    '792:            .ReadReceiptRequested = True
    '            End If

    '793:        .Recipients.ResolveAll()
    '            .Save()
    '800:        .Send()

    '810:        Utils.DeliverNow()
    '820:        Utils.Cleanup()

    '821:        If readReceipt = True And destinationid > 0 Then
    '822:            LogReceiptRqst(destinationid, Now, cdoMessage.Subject, Addresses, SendCC, SendBCC)
    '823:        End If

    '830:    End With


    '840:    'SafeItem = Nothing
    '850:    cdoMessage = Nothing
    '860:    cdoSession = Nothing
    '870:    Utils = Nothing

    '880:    clsMarsUI.MainUI.BusyProgress(, , True)

    '890:    SendMAPI = True

    '900:    Exit Function
    'Trap:
    '        Dim sError As String
    '        Dim sxSuggest As String = ""

    '        SendMAPI = False

    '        sError = Err.Description

    '        If sError.ToLower.IndexOf("could not resolve the message") > -1 Then
    '            sxSuggest = "This can be caused by:" & vbCrLf & _
    '            "- names not recognised in the MAPI address book" & vbCrLf & _
    '            "- more than one match is found in the address and would normally require the " & _
    '            "user to pick from the list of matches." & vbCrLf & vbCrLf & _
    '            "Please check the above and rectify."

    '        End If
    '        If LogFailure = True And EmailRetry = True Then
    '            AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '            InBody, sFormat, sError)
    '            gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
    '            gErrorSource = "SendMAPI"
    '            gErrorNumber = -214790210
    '            gErrorLine = Erl()
    '            gErrorSuggest = sxSuggest
    '        Else
    '            gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
    '            gErrorNumber = -214790210
    '            gErrorSource = "SendMAPI"
    '            gErrorLine = Erl()
    '            gErrorSuggest = sxSuggest
    '        End If
    '    End Function

    '    Public Shared Function oldSendMAPI(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
    '    ByVal SendType As String, Optional ByVal Attach As String = "", _
    '    Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
    '    Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
    '    Optional ByVal InBody As Boolean = False, _
    '    Optional ByVal sFormat As String = "", _
    '    Optional ByVal sName As String = "", _
    '    Optional ByVal LogFailure As Boolean = True, Optional ByVal sEmbedFile As String = "", _
    '    Optional ByVal readReceipt As Boolean = False, Optional ByVal destinationid As Integer = 0) As Boolean

    '        On Error GoTo Trap
    '        Dim SafeItem As Redemption.SafeMailItem
    '        Dim cdoMessage As Object 'MAPI.Message
    '        Dim cdoSession As Object 'MAPI.Session
    '        Dim I As Integer
    '        Dim ColCount As Integer
    '        Dim Recs() As String
    '        Dim UserID As String
    '        Dim Password As String
    '        Dim Utils As Redemption.MAPIUtils
    '        Dim SentItemsFolderID As Object
    '        Dim cdoRep As Object 'MAPI.Recipients
    '        Dim sRead As String
    '        Dim EmailRetry As Boolean
    '        Dim sMode As String
    '        Dim sAttach As String
    '        Dim sInBody As String
    '        Dim sAlias As String
    '        Dim sxServer As String
    '        Dim MAPIType As String
    '        Dim WinUser As String
    '        Dim WinPassword As String
    '        Dim WinDomain As String
    '        Dim oFile As String
    '        Dim sArgs() As String


    '10:     sMode = clsMarsUI.MainUI.ReadRegistry("CRDService", "")

    '20:     sArgs = Environment.GetCommandLineArgs

    '30:     If sArgs.Length > 1 And sMode = "WindowsNT" Then
    '40:         AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '             InBody, sFormat, "", , , , readReceipt, destinationid)
    '50:         Return True
    '60:     End If

    '70:     UserID = clsMarsUI.MainUI.ReadRegistry("MAPIProfile", "")

    '80:     Password = clsMarsUI.MainUI.ReadRegistry("MAPIPassword", "", True)

    '90:     MAPIType = clsMarsUI.MainUI.ReadRegistry("MAPIType", "")

    '100:    sAlias = clsMarsUI.MainUI.ReadRegistry("MAPIAlias", "")

    '110:    sxServer = clsMarsUI.MainUI.ReadRegistry("MAPIServer", "")

    '120:    EmailRetry = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("EmailRetry", 0)))


    '        'to avoid mapi memory errors--------------------------------------------------
    '        'Dim sResult As String
    '        'Dim oSend As New SendMAPI.clsMAPI

    '        'sResult = oSend.SendMAPI(Addresses, Subject, Body, SendType, UserID, Password, _
    '        'rptFileNames, Attach, Numattach, Extras, SendCC, SendBCC)
    '        '-----------------------------------------------------------------------------

    '130:    'cdoSession = New MAPI.Session

    '140:    If MAPIType = "Single" Then
    '150:        cdoSession.Logon(UserID, Password, False, True)
    '160:    Else
    '170:        cdoSession.Logon(, , , True, , _
    '            NoMail:=True, _
    '                ProfileInfo:=sxServer & vbLf & sAlias)
    '180:    End If

    '190:    cdoMessage = cdoSession.Outbox.Messages.Add

    '200:    cdoRep = cdoMessage.Recipients

    '210:    SafeItem = New Redemption.SafeMailItem     'Create an instance of Redemption.SafeMailItem

    '220:    With SafeItem
    '230:        SentItemsFolderID = cdoSession.GetDefaultFolder(3).ID

    '240:        '.Fields(MAPI.CdoPropTags.CdoPR_DELETE_AFTER_SUBMIT) = True

    '250:        .Item = cdoMessage 'set Item property


    '260:        ColCount = FindOccurence(Addresses, ";")

    '270:        If ColCount = 0 Then ColCount = 1

    '280:        ReDim Recs(ColCount)

    '290:        For I = 1 To ColCount
    '300:            Recs(I) = GetDelimitedWord(Addresses, I, ";")
    '310:            cdoRep.Add(Recs(I), , 1)
    '320:        Next

    '330:        If SendCC <> "" Then
    '340:            ColCount = FindOccurence(SendCC, ";")

    '350:            ReDim Recs(ColCount)

    '360:            For I = 1 To ColCount
    '370:                Recs(I) = GetDelimitedWord(SendCC, I, ";")
    '380:                cdoRep.Add(Recs(I), , 2)
    '390:            Next
    '400:        End If

    '410:        If SendBCC <> "" Then
    '420:            ColCount = FindOccurence(SendBCC, ";")
    '430:            ReDim Recs(ColCount)

    '440:            For I = 1 To ColCount
    '450:                Recs(I) = GetDelimitedWord(SendBCC, I, ";")
    '460:                cdoRep.Add(Recs(I), , 3)
    '470:            Next
    '480:        End If

    '490:        If InBody = True And sEmbedFile.Length > 0 Then
    '500:            Dim sBody As String = ReadTextFromFile(sEmbedFile)
    '510:            .Text = sBody
    '520:        Else
    '530:            If Not Body Is Nothing Then .Text = Body
    '540:        End If

    '550:        .Subject = Subject

    '560:        If SendType = "Single" Then
    '570:            For Each s As String In Attach.Split(";")
    '580:                If s.Length > 0 Then .Attachments.Add(s)
    '590:            Next
    '600:        ElseIf SendType = "Package" Then
    '610:            For Each oFile In rptFileNames
    '620:                If oFile.Length > 0 Then .Attachments.Add(oFile)
    '630:            Next
    '640:        End If

    '650:        If Extras <> "" Then
    '660:            For Each s As String In Extras.Split(";")

    '670:                If s.Length > 0 Then
    '680:                    If s.IndexOf("*") > -1 Then
    '                            Dim compareMe As String = ExtractFileName(s)

    '690:                        For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
    '                                If ExtractFileName(x) Like compareMe Then
    '700:                                .Attachments.Add(x)
    '                                End If
    '710:                        Next
    '720:                    Else
    '                            If IO.File.Exists(s) Then
    '730:                            .Attachments.Add(s)
    '                            End If
    '740:                    End If

    '1130:               End If
    '1140:           Next
    '770:        End If

    '780:        Utils = New Redemption.MAPIUtils

    '790:        Utils.HrSetOneProp(cdoMessage, _
    '            MAPI.CdoPropTags.CdoPR_SENTMAIL_ENTRYID, _
    '            Utils.HrGetOneProp(cdoSession.GetDefaultFolder(3), _
    '            MAPI.CdoPropTags.CdoPR_ENTRYID))

    '791:        If readReceipt = True And destinationid > 0 Then
    '792:            cdoMessage.ReadReceipt = True
    '            End If

    '800:        .Send()

    '810:        Utils.DeliverNow()
    '820:        Utils.Cleanup()

    '821:        If readReceipt = True And destinationid > 0 Then
    '822:            LogReceiptRqst(destinationid, Now, cdoMessage.Subject, Addresses, SendCC, SendBCC)
    '823:        End If

    '830:    End With


    '840:    SafeItem = Nothing
    '850:    cdoMessage = Nothing
    '860:    cdoSession = Nothing
    '870:    Utils = Nothing

    '880:    clsMarsUI.MainUI.BusyProgress(, , True)

    '890:    SendMAPI = True

    '900:    Exit Function
    'Trap:
    '        Dim sError As String
    '        Dim sxSuggest As String = ""

    '        SendMAPI = False

    '        sError = Err.Description

    '        If sError.ToLower.IndexOf("could not resolve the message") > -1 Then
    '            sxSuggest = "This can be caused by:" & vbCrLf & _
    '            "- names not recognised in the MAPI address book" & vbCrLf & _
    '            "- more than one match is found in the address and would normally require the " & _
    '            "user to pick from the list of matches." & vbCrLf & vbCrLf & _
    '            "Please check the above and rectify."

    '        End If
    '        If LogFailure = True And EmailRetry = True Then
    '            AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '            InBody, sFormat, sError)
    '            gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
    '            gErrorSource = "SendMAPI"
    '            gErrorNumber = -214790210
    '            gErrorLine = Erl()
    '            gErrorSuggest = sxSuggest
    '        Else
    '            gErrorDesc = SendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & Addresses & " failed with the following error: " & sError
    '            gErrorNumber = -214790210
    '            gErrorSource = "SendMAPI"
    '            gErrorLine = Erl()
    '            gErrorSuggest = sxSuggest
    '        End If
    '    End Function

    Public Shared Function IMAP4Login(ByVal sUser As String, ByVal sPassword As String, ByVal sServer As String, _
    ByVal Port As Integer, Optional ByVal useSSL As Boolean = False, Optional ByRef oIMAP As Quiksoft.EasyMail.IMAP4.IMAP4 = Nothing) As Boolean
        Dim imap4obj As Quiksoft.EasyMail.IMAP4.IMAP4 = New Quiksoft.EasyMail.IMAP4.IMAP4
        Dim SSL As SSL = New SSL

        'v2 Quiksoft.EasyMail.IMAP4.License.Key = "ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"

        Quiksoft.EasyMail.IMAP4.License.Key = QuikSoftLicense '
        Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense

        Try
            If Port = 0 Then
                imap4obj.Connect(sServer)
            Else
                If useSSL = True Then
                    imap4obj.Connect(sServer, Port, SSL.GetInterface)
                Else
                    imap4obj.Connect(sServer, Port)
                End If
            End If

            imap4obj.Login(sUser, sPassword)

            oIMAP = imap4obj

            Return True
        Catch ex As Exception
            MessageBox.Show(ex.ToString)

            Try : imap4obj.Logout() : Catch : End Try
            Return False
        End Try
    End Function
    Public Shared Function Pop3Login(ByVal sUser As String, ByVal sPassword As String, _
    ByVal sServer As String, Optional ByVal Port As Integer = 0, Optional ByVal useSSL As Boolean = False) As Boolean
        Dim pop3Obj As New POP3
        Dim SSL As SSL = New SSL

        Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
        Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Computer)/12521600F458357418750F38578A1CB6D9"

        Try
            'Connect to the POP3 mail server
            If Port = 0 Then
                pop3Obj.Connect(sServer)
            Else
                If useSSL = True Then
                    pop3Obj.Connect(sServer, Port, SSL.GetInterface)
                Else
                    pop3Obj.Connect(sServer, Port)
                End If
            End If

            'Log onto the POP3 mail server
            pop3Obj.Login(sUser, sPassword, AuthMode.Plain)

            pop3Obj.Disconnect()

            Return True
        Catch ex As Exception
            Try : pop3Obj.Disconnect() : Catch : End Try
            Return False
        End Try
    End Function

    Public Shared Function oldSendSMTP(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
           ByVal SendType As String, Optional ByVal Attach As String = "", _
           Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
           Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
           Optional ByVal sName As String = "", Optional ByVal InBody As Boolean = False, _
           Optional ByVal sFormat As String = "", _
           Optional ByVal LogFailure As Boolean = True, _
           Optional ByVal IncludeAttach As Boolean = True, _
           Optional ByVal MailFormat As String = "TEXT", _
           Optional ByVal SMTPName As String = "Default", Optional ByVal sEmbedFile As String = "", _
           Optional ByVal CustomSenderName As String = "", Optional ByVal CustomSenderAddress As String = "", Optional ByVal customMsg As Object = Nothing) As Boolean

        Dim Server As String
        Dim Sender As String
        Dim UserID As String
        Dim Password As String
        Dim sAddress As String
        Dim strAttach As String = ""
        Dim EmailRetry As Boolean
        Dim oValue As Object
        Dim oFile As String
        Dim nTimeout As Integer
        Dim sEncode As String
        Dim nPort As Integer = 25
        Dim emailPath As String = ""
        Dim popLogin As Boolean = False

10:     If sName = "" Then
20:         smtplogName = "smtp.debug"
30:     Else
40:         smtplogName = "smtp_" & sName & ".debug"
        End If

50:     clsMarsDebug.writeToDebug(smtplogName, "---------Begin SMTP Transmission for - " & sName & "-----------------", True)

60:     Quiksoft.EasyMail.SMTP.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"

70:     Try
80:         Dim n As Integer

90:         n = clsMarsUI.MainUI.ReadRegistry("EmailRetry", "1")

100:        EmailRetry = Convert.ToBoolean(n)
110:    Catch
120:        EmailRetry = True
        End Try

130:    SMTPErr = ""

140:    clsMarsDebug.writeToDebug(smtplogName, "Collecting SMTP Server Information", True)

150:    If MailType = MarsGlobal.gMailType.SMTP Then
160:        If SMTPName = "Default" Then
NoneFound:
170:            With clsMarsUI.MainUI
180:                Server = .ReadRegistry("SMTPServer", oValue)
190:                Sender = .ReadRegistry("SMTPSenderName", "")
200:                UserID = .ReadRegistry("SMTPUserID", "")
210:                Password = .ReadRegistry("SMTPPassword", "", True)
220:                sAddress = .ReadRegistry("SMTPSenderAddress", "")
230:                sEncode = .ReadRegistry("CharSetEncoding", "US-ASCII")
240:                nPort = .ReadRegistry("SMTPPort", 25)

250:                Try
260:                    nTimeout = clsMarsUI.MainUI.ReadRegistry("SMTPTimeout", "30")
270:                Catch
280:                    nTimeout = 30
                    End Try

290:                Try
300:                    popLogin = Convert.ToInt32(.ReadRegistry("RequirePOPLogin", 0))

310:                    If popLogin = True Then
                            Dim popServer, popUser, popPassword As String
                            Dim popPort As Integer
                            Dim popSSL As Boolean

320:                        popServer = .ReadRegistry("SMTPPOPServer", "")
330:                        popUser = .ReadRegistry("SMTPPOPUser", "")
340:                        popPassword = .ReadRegistry("SMTPPOPPassword", "", True)
350:                        popPort = .ReadRegistry("SMTPPOPPort", 110)
360:                        popSSL = Convert.ToInt32(.ReadRegistry("SMTPPOPSSL", 0))

370:                        If Pop3Login(popUser, popPassword, popServer, popPort, popSSL) = False Then
380:                            _ErrorHandle("Could not log into POP3 server '" & popServer & "' as '" & popUser & "'", -1989140808, Reflection.MethodBase.GetCurrentMethod.Name, _
      Erl, , True)
                            End If
                        End If
                    Catch : End Try
                End With
390:        Else
400:            Dim SQL As String
410:            Dim oRs As ADODB.Recordset
420:            Dim oData As New clsMarsData

430:            SQL = "SELECT * FROM SMTPServers WHERE SMTPName = '" & SQLPrepare(SMTPName) & "'"

440:            oRs = clsMarsData.GetData(SQL)

450:            Try
460:                If oRs.EOF = False Then
470:                    Server = oRs("smtpserver").Value
480:                    Sender = oRs("smtpsendername").Value
490:                    UserID = oRs("smtpuser").Value
500:                    Password = Decrypt(oRs("smtppassword").Value, "KalEidoscOpe")
510:                    sAddress = oRs("smtpsenderaddress").Value
520:                    nTimeout = oRs("smtptimeout").Value
530:                    nPort = IsNull(oRs("smtpport").Value, 25)

540:                    Try
550:                        popLogin = oRs("smtppoplogin").Value

560:                        If popLogin = True Then
                                Dim popServer, popUser, popPassword As String
                                Dim popPort As Integer
                                Dim popSSL As Boolean

570:                            popServer = IsNull(oRs("SMTPPOPServer").Value, "")
580:                            popUser = IsNull(oRs("SMTPPOPUser").Value)
590:                            popPassword = _DecryptDBValue(IsNull(oRs("SMTPPOPPassword").Value))
600:                            popPort = IsNull(oRs("SMTPPOPPort").Value, 110)
610:                            popSSL = IsNull(oRs("SMTPPOPSSL").Value, 0)

620:                            If Pop3Login(popUser, popPassword, popServer, popPort, popSSL) = False Then
630:                                _ErrorHandle("Could not log into POP3 server '" & popServer & "' as '" & popUser & "'", -1989140808, Reflection.MethodBase.GetCurrentMethod.Name, _
      Erl, , True)
                                End If
                            End If
                        Catch : End Try
640:                Else
650:                    oRs.Close()
660:                    GoTo NoneFound
670:                End If

680:                oRs.Close()
690:            Catch ex As Exception
700:                GoTo NoneFound
                End Try
            End If
710:    ElseIf MailType = MarsGlobal.gMailType.SQLRDMAIL Then
720:        Server = "mail.crystalreportsdistributor.com"
730:        sAddress = clsMarsUI.MainUI.ReadRegistry("SQL-RDAddress", "")

740:        If gnEdition = MarsGlobal.gEdition.EVALUATION Then
750:            Password = "evaluation123xyz"
760:            UserID = "evaluation@crystalreportsdistributor.com"
770:        Else
780:            UserID = CustID & "@crystalreportsdistributor.com"
790:            Password = CustID
800:            Sender = clsMarsUI.MainUI.ReadRegistry("SQL-RDSender", oValue)
            End If

810:        nTimeout = 120
820:        nPort = 25
        End If

830:    If CustomSenderName.Length > 0 Then
840:        Sender = CustomSenderName
850:        sAddress = CustomSenderAddress
        End If

860:    Try
870:        If MailType = MarsGlobal.gMailType.SQLRDMAIL Then
880:            If Pop3Login(UserID, Password, Server) = False Then
890:                Throw New Exception("Could not log into SQL-RDMail Server. Please make sure that you have activated your SQL-RDMail Account")
900:            End If

910:            UserID = String.Empty
920:            Password = String.Empty
            End If

930:        If Sender = "" Then Sender = sAddress

940:        oSMTP = New Quiksoft.EasyMail.SMTP.SMTP

950:        Dim oServer As New Quiksoft.EasyMail.SMTP.SMTPServer(Server)
            Dim oMsg As Quiksoft.EasyMail.SMTP.EmailMessage

960:        If customMsg IsNot Nothing Then
970:            oMsg = customMsg
980:        Else
990:            oMsg = New Quiksoft.EasyMail.SMTP.EmailMessage
            End If
1000:

1010:
1020:       oMsg.From.Email = sAddress
1030:       oMsg.From.Name = Sender
1040:       oMsg.ReplyTo = sAddress

1050:       clsMarsDebug.writeToDebug(smtplogName, "Validating message recipients", True)

1060:       If customMsg Is Nothing Then
1070:           For Each s As String In Addresses.Split(";")
1080:               If s <> "" Then
1090:                   oMsg.Recipients.Add(s, s, Quiksoft.EasyMail.SMTP.RecipientType.To)
1100:               End If
1110:           Next

1120:           If SendCC.Length > 0 Then
1130:               For Each s As String In SendCC.Split(";")
1140:                   If s <> "" Then
1150:                       oMsg.Recipients.Add(s, s, Quiksoft.EasyMail.SMTP.RecipientType.CC)
1160:                   End If
1170:               Next
                End If

1180:           If SendBCC.Length > 0 Then
1190:               For Each s As String In SendBCC.Split(";")
1200:                   If s <> "" Then
1210:                       oMsg.Recipients.Add(s, s, Quiksoft.EasyMail.SMTP.RecipientType.BCC)
1220:                   End If
1230:               Next
1240:           End If
            End If

1250:       If UserID.Length > 0 Then
1260:           oServer.AuthMode = Quiksoft.EasyMail.SMTP.SMTPAuthMode.AuthLogin
1270:           oServer.Account = UserID
1280:           oServer.Password = Password
1290:       Else
1300:           oServer.AuthMode = Quiksoft.EasyMail.SMTP.SMTPAuthMode.None
1310:       End If

1320:       If nTimeout > 0 Then
1330:           oServer.Timeout = nTimeout
1340:       Else
1350:           oServer.Timeout = 30
1360:       End If

1370:       oServer.Port = nPort

1380:       clsMarsDebug.writeToDebug(smtplogName, "Adding attachments", True)

1390:       If customMsg Is Nothing Then
1400:           If SendType = "Single" Then
1410:               If IncludeAttach = True And Attach <> "" Then
1420:                   strAttach = Attach

1430:                   For Each s As String In Attach.Split(";")
1440:                       If s.Length > 0 Then _
                                 oMsg.Attachments.Add(s)
1450:                   Next
1460:               End If
1470:           ElseIf SendType = "Package" Then
1480:               For Each oFile In rptFileNames
1490:                   oMsg.Attachments.Add(oFile)
1500:               Next
1510:           End If

1520:           If Extras.Length > 0 Then
1530:               For Each s As String In Extras.Split(";")

1540:                   If s.Length > 0 Then
1550:                       If s.IndexOf("*") > -1 Then
                                Dim compareMe As String = ExtractFileName(s)

1560:                           For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
1570:                               If ExtractFileName(x) Like compareMe Then
1580:                                   oMsg.Attachments.Add(x)
                                    End If
1590:                           Next
1600:                       Else
1610:                           If IO.File.Exists(s) Then
1620:                               oMsg.Attachments.Add(s)
1630:                           Else

                                    _ErrorHandle("Missing custom attachment: " & s, 5, Reflection.MethodBase.GetCurrentMethod.Name, 1110, "Could not locate custom attachment." & vbCrLf & vbCrLf & _
           "The email was sent without it." & vbCrLf & vbCrLf & _
           "To ensure that the email is sent with your specified attachment(s) in the future, please make sure that it exists at the location specified by the destination setup.", True)

                                End If
1640:                       End If

1650:                   End If
1660:               Next
1670:           End If

1680:           clsMarsDebug.writeToDebug(smtplogName, "Configuring SMTP Message", True)

1690:           oMsg.Subject = Subject

1700:           If MailFormat = "TEXT" Then
1710:               If InBody = False Then
1720:                   oMsg.BodyParts.Add(Body, Quiksoft.EasyMail.SMTP.BodyPartFormat.Plain)
1730:               Else
1740:                   Dim sBody As String
1750:                   Dim oBody As New Quiksoft.EasyMail.SMTP.BodyPart

1760:                   sBody = ReadTextFromFile(sEmbedFile)

1770:                   oBody.Format = Quiksoft.EasyMail.SMTP.BodyPartFormat.Plain

1780:                   oBody.Body = sBody

1790:                   oMsg.BodyParts.Add(oBody)
1800:               End If
1810:           Else
1820:               If InBody = True And sEmbedFile.Length > 0 Then

1830:                   Try
1840:                       If sEncode <> "US-ASCII" Then
1850:                           Dim sJaps As String

1860:                           sJaps = ReadTextFromFile(sEmbedFile)

1870:                           sJaps = sJaps.Replace("charset=UTF-8", "charset=shift_jis")

1880:                           SaveTextToFile(sJaps, sEmbedFile, , False)
1890:                       End If

1900:                       oMsg.ImportBody(sEmbedFile, Quiksoft.EasyMail.SMTP.BodyPartFormat.HTML, Quiksoft.EasyMail.SMTP.ImportBodyFlags.None)

1910:                   Catch
1920:                       Dim sBody As String
1930:                       Dim oBody As New Quiksoft.EasyMail.SMTP.BodyPart

1940:                       sBody = ReadTextFromFile(sEmbedFile)

1950:                       If sEncode <> "US-ASCII" Then
1960:                           sBody = sBody.Replace("charset=UTF-8", "charset=shift_jis")
                            End If

1970:                       oBody.Format = Quiksoft.EasyMail.SMTP.BodyPartFormat.HTML
1980:                       oBody.Body = sBody
1990:                       oMsg.BodyParts.Add(oBody)

                            'manually add attachments
2000:                       For Each s As String In IO.Directory.GetFiles(GetDirectory(sEmbedFile))
2010:                           If s.ToLower.EndsWith(".htm") = False Then
2020:                               oMsg.Attachments.Add(s)
2030:                           End If
2040:                       Next
2050:                   End Try
2060:               Else
2070:                   Dim oBody As New Quiksoft.EasyMail.SMTP.BodyPart

2080:                   oBody.Format = Quiksoft.EasyMail.SMTP.BodyPartFormat.HTML
2090:                   oBody.Body = Body

2100:                   oMsg.BodyParts.Add(oBody)
2110:               End If
2120:           End If
            End If

2130:       clsMarsDebug.writeToDebug(smtplogName, "Configuring SMTP Server", True)

2140:       oSMTP.SMTPServers.Add(oServer)

2150:       If sEncode <> "US-ASCII" Then
                'oMsg.CharsetEncoding = System.Text.Encoding.GetEncoding("iso-2022-jp")
2160:           oMsg.CharsetEncoding = System.Text.Encoding.GetEncoding("shift_jis")
            End If

2170:       emailPath = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages")

2180:       Try
2190:           If IO.Directory.Exists(emailPath) = False Then
2200:               IO.Directory.CreateDirectory(emailPath)
                End If

2210:           If emailPath.EndsWith("\") = False Then emailPath &= "\"

2220:           emailPath &= "email_" & Date.Now.ToString("yyyyMMddhhmmss") & ".eml"

2230:           oMsg.SaveMessage(emailPath)

2240:           clsMarsDebug.writeToDebug(smtplogName, "Saving SMTP Message to disk (" & emailPath & ")", True)

2250:           _Delay(1)
2260:       Catch
2270:           emailPath = ""
            End Try
JUSTEND:
2280:       clsMarsDebug.writeToDebug(smtplogName, "Sending message", True)

2290:       oSMTP.Send(oMsg)

2300:       clsMarsUI.MainUI.BusyProgress(100, "", True)

            clsMarsDebug.writeToDebug(smtplogName, "Message sent: Logging email", True)

2310:       EmailLog(Addresses, Subject, sName, emailPath)

2320:       clsMarsDebug.writeToDebug(smtplogName, "Disconnecting from SMTP server", True)

2330:       oSMTP.Reset()

2340:       clsMarsDebug.writeToDebug(smtplogName, "End", True)

2350:       Return True
2360:   Catch ex As Exception

2370:       Try : oSMTP.Reset() : Catch : End Try

2380:       If MailType = MarsGlobal.gMailType.SMTP Then
2390:           Dim SQL As String
2400:           Dim oData As New clsMarsData
2410:           Dim oRsx As ADODB.Recordset


2420:           SQL = "SELECT * FROM SMTPServers WHERE SMTPID >" & nBackID & " AND IsBackup = 1"

2430:           oRsx = clsMarsData.GetData(SQL)

2440:           If oRsx.EOF = False Then
2450:               nBackID = oRsx("smtpid").Value

2460:               clsMarsUI.MainUI.BusyProgress(50, "Using backup SMTP server [" & oRsx("smtpname").Value & "]")

                    Dim objSender As clsMarsMessaging = New clsMarsMessaging

2470:               If objSender.SendSMTP(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, _
                         sName, InBody, sFormat, False, IncludeAttach, MailFormat = "TEXT", _
                         oRsx("smtpname").Value) = True Then
2480:                   oRsx.Close()
2490:                   Return True
2500:               End If
2510:           End If

2520:           oRsx.Close()
2530:       End If

2540:       Dim sMsg As String = ""

2550:       If ex.Message.IndexOf("503 AUTH") > -1 Or ex.Message.ToLower.IndexOf("authentication") > -1 Then
2560:           sMsg = "Your SMTP server is not configured to use a username, password or both. Please remove the username and password and try again"
2570:       ElseIf ex.Message.IndexOf("535 authorization failed") > -1 Or _
                 ex.Message.IndexOf("not implemented") > -1 Then
2580:           sMsg = "Please check the provided userid and password. " & _
                     "Also, some SMTP servers are not configured to use a username, password or both."
2590:       ElseIf ex.Message.IndexOf("GetHostByName") > -1 Then
2600:           sMsg = "SQL-RD cannot find a connection to your server.  Please ensure that you have a connection to your server, or that the mail server name has been entered correctly."
2610:       ElseIf ex.Message.IndexOf("Administrative prohibition") > -1 Then
2620:           sMsg = "This message has been retrun by your SMTP server. " & _
                     "Please speak to you email administrator to enausre that you have the required " & _
                     "permissions.  Also ensure that your SMTP server allows relaying"
2630:       ElseIf ex.Message.ToLower.IndexOf("message size exceeds fixed maximum") > -1 Then
2640:           sMsg = "Your SMTP server has placed a liimit on the size of email that can be sent using your " & _
     "account.  Please approach your SMTP server administrator to raise this limit, " & _
     "or try zipping the files before sending."
2650:       ElseIf ex.Message.ToLower.IndexOf("data from socket") > -1 Then
2660:           sMsg = "This error comes about if the SMTP server is not available. " & _
     "Go to Options - Messaging and switch on 'Try resending failed emails'. " & _
     "This will ensure that your email is tried again and again until it is finaly " & _
     "successful."
2670:       ElseIf Err.Number = 5 Then
                sMsg = " You do not have rights to the [system output folder].  Try the following:" & vbCrLf & vbCrLf & _
           "-  Create a share to the ChristianSteven folder" & vbCrLf & _
           "- Make sure you have full rights to create, modify& delete files and subfolders" & vbCrLf & _
           "- Go to Options->System Paths and ensure that all the paths are mapped through new share."
2680:       Else
2690:           sMsg = ""
            End If


2700:       If LogFailure = True And EmailRetry = True Then
2710:           gErrorDesc = SendType & " Schedule:" & gScheduleName & "Report(s) were produced successfully but email failed with the following error: " & ex.Message
2720:           gErrorNumber = Err.Number
2730:           gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2740:           gErrorLine = Erl()
2750:           gErrorSuggest = sMsg

2760:           AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
                     InBody, sFormat, ex.Message, MailFormat, IncludeAttach, SMTPName, False, 0, sEmbedFile, emailPath)
2770:       Else
2780:           gErrorDesc = SendType & " Schedule:" & gScheduleName & ". Report(s) produced successfully but email failed with the following error: " & ex.Message
2790:           gErrorNumber = Err.Number
2800:           gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2810:           gErrorLine = Erl()
2820:           gErrorSuggest = sMsg
2830:       End If

2840:       If gErrorNumber <> 91 And gErrorNumber > 0 And gErrorLine > 0 Then _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, Erl, gErrorSuggest, False, True)

2850:       Return False
2860:   End Try
    End Function

    Private Sub embedHTMLIntoEmail(ByRef email As Chilkat.Email, ByVal embedFile As String)

        Dim htmlBody As String = IO.File.ReadAllText(embedFile)
        Dim htmlPath As String = IO.Path.GetDirectoryName(embedFile)

        For Each f As String In IO.Directory.GetFiles(htmlPath)

            If IO.Path.GetExtension(f) <> ".htm" And IO.Path.GetExtension(f) <> ".html" Then
                Dim cid As String = email.AddRelatedFile(f)

                If cid Is Nothing Then Throw New Exception(email.LastErrorText)
                htmlBody = htmlBody.ToLower.Replace(f.ToLower, "cid:" & cid)
            End If
        Next

        email.SetHtmlBody(htmlBody)

    End Sub

    Private Shared Sub embedHTMLIntoMAPIEmail(ByRef msg As Redemption.RDOMail, html As String)
        Try
            Dim fileContents As String = html

            Dim images As ArrayList = getHTMLReferencedImages(fileContents)

            msg.BodyFormat = 2

            Dim PT_BOOLEAN = 11
            Dim PR_HIDE_ATTACH = msg.GetIDsFromNames("{00062008-0000-0000-C000-000000000046}", &H8514) Or PT_BOOLEAN

            msg.Fields(PR_HIDE_ATTACH) = True

            For Each im As String In images
                Dim originalTagAttr As String = im

                If im.ToLower.StartsWith("file:///", StringComparison.CurrentCultureIgnoreCase) Then
                    ' im = im.Replace("file:///", "")
                    im = System.Text.RegularExpressions.Regex.Replace(im, "file:///", "", RegularExpressions.RegexOptions.IgnoreCase)
                    im = im.Replace("/", "\")
                End If

                If IO.File.Exists(im) Then
                    Dim Attach As Object = msg.Attachments.Add(im)

                    Attach.Fields(&H370E001E) = "image/jpeg"

                    'Attachment cid
                    Dim cid As String = Guid.NewGuid.ToString
                    Attach.Fields(&H3712001E) = cid

                    fileContents = fileContents.Replace(originalTagAttr, "cid:" & cid)
                End If
            Next

            msg.HTMLBody = fileContents
            msg.DownloadPictures = True
        Catch ex As Exception
            _ErrorHandle("Error embedding HTML images into MAPI email: " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub

    Shared Function getHTMLReferencedImages(ByVal html As String) As ArrayList
        Dim contents As String = html
        Dim imageFiles As ArrayList = New ArrayList
        Dim index As Integer = 0

        '//small letters
        Do
            '//find img tag
            index = contents.IndexOf("<img", index, StringComparison.CurrentCultureIgnoreCase)

            If index = -1 Then Exit Do

            '//find the src attr
            index = contents.IndexOf("src", index, StringComparison.CurrentCultureIgnoreCase)

            If index = -1 Then Exit Do

            index += 3

            Dim charCount As Integer = 0
            Dim result As String = ""
            Dim terminator As String = ""

            For Each s As String In contents.Substring(index, (contents.Length - index))
                If s = Chr(34) And terminator = "" Then
                    terminator = Chr(34)
                    charCount += 1
                ElseIf s = "'" And terminator = "" Then
                    terminator = "'"
                    charCount += 1
                ElseIf s = Chr(34) And terminator = Chr(34) Then
                    charCount += 1
                ElseIf s = "'" And terminator = "'" Then
                    charCount += 1
                ElseIf charCount = 0 And s = "=" Then
                    Continue For
                End If

                result += s

                If charCount = 2 Then Exit For
            Next

            result = result.Trim

            If result.StartsWith(Chr(34)) Or result.StartsWith("'") Then
                result = result.Remove(0, 1)
            End If

            If result.EndsWith(Chr(34)) Or result.EndsWith("'") Then
                result = result.Remove(result.Length - 1, 1)
            End If

            imageFiles.Add(result)

            index = contents.IndexOf("<img", index, StringComparison.CurrentCultureIgnoreCase)
        Loop Until index = -1

        'index = 0

        'Do
        '    '//find img tag
        '    index = contents.IndexOf("<IMG", index)

        '    If index = -1 Then Exit Do

        '    '//find the src attr
        '    index = contents.IndexOf("src", index) + 3

        '    If index = -1 Then Exit Do

        '    Dim charCount As Integer = 0
        '    Dim result As String = ""
        '    Dim terminator As String = ""

        '    For Each s As String In contents.Substring(index, (contents.Length - index))
        '        If s = Chr(34) And terminator = "" Then
        '            terminator = Chr(34)
        '            charCount += 1
        '        ElseIf s = "'" And terminator = "" Then
        '            terminator = "'"
        '            charCount += 1
        '        ElseIf s = Chr(34) And terminator = Chr(34) Then
        '            charCount += 1
        '        ElseIf s = "'" And terminator = "'" Then
        '            charCount += 1
        '        ElseIf charCount = 0 And s = "=" Then
        '            Continue For
        '        End If

        '        result += s

        '        If charCount = 2 Then Exit For
        '    Next

        '    result = result.Trim

        '    If result.StartsWith(Chr(34)) Or result.StartsWith("'") Then
        '        result = result.Remove(0, 1)
        '    End If

        '    If result.EndsWith(Chr(34)) Or result.EndsWith("'") Then
        '        result = result.Remove(result.Length - 1, 1)
        '    End If

        '    imageFiles.Add(result)


        '    index = contents.IndexOf("<IMG", index)
        'Loop Until index = -1

        Return imageFiles
    End Function
    Private Shared Function embedPNGIntoMAPIMail(ByRef em As Redemption.RDOMail, ByVal pngDir As String) As Boolean

        ' Dim mail As Chilkat.MailMan = New Chilkat.MailMan
10:     Try
20:         Dim lines As ArrayList = New ArrayList
30:         Dim dtSort As DataTable = New DataTable
40:         dtSort.Columns.Add("filename")
50:         dtSort.Columns.Add("datecreated", GetType(System.DateTime))

60:         For Each File As String In IO.Directory.GetFiles(pngDir)
                Dim r As DataRow = dtSort.NewRow
70:             r("filename") = File
80:             r("datecreated") = IO.File.GetCreationTime(File)

90:             dtSort.Rows.Add(r)
100:        Next

            Dim PT_BOOLEAN = 11
            Dim PR_HIDE_ATTACH = em.GetIDsFromNames("{00062008-0000-0000-C000-000000000046}", &H8514) Or PT_BOOLEAN
            em.Fields(PR_HIDE_ATTACH) = True

            Dim rows() As DataRow = dtSort.Select("", "datecreated ASC")

110:        If rows IsNot Nothing Then
120:            For Each r As DataRow In rows
                    Dim file As String = r("filename")

130:                If IO.Path.GetExtension(file).ToLower = ".png" Then
140:
                        'add attachmentB
                        Dim Attach As Object = em.Attachments.Add(file)
                        'content type
150:                    Attach.Fields(&H370E001E) = "image/jpeg"
                        'Attachment cid
160:                    Attach.Fields(&H3712001E) = IO.Path.GetFileNameWithoutExtension(file)

161:                    lines.Add("<img src='cid:" & IO.Path.GetFileNameWithoutExtension(file) & "'>")

                    End If
170:            Next
            End If

            Dim htmlbody As String = "<html><body><br>"

180:        For Each line As String In lines
190:            htmlbody &= line
200:        Next

210:        htmlbody &= "</body></html>"

220:        em.BodyFormat = 2
230:        em.HTMLBody = htmlbody

240:        Return True
250:    Catch ex As Exception
            _ErrorHandle("Error embedding PNG into MAPI email: " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Function

    Private Function embedPNGIntoMail(ByRef em As Chilkat.Email, ByVal pngDir As String) As Boolean

        ' Dim mail As Chilkat.MailMan = New Chilkat.MailMan

        Dim lines As ArrayList = New ArrayList
        Dim dtSort As DataTable = New DataTable
        dtSort.Columns.Add("filename")
        dtSort.Columns.Add("datecreated", GetType(System.DateTime))

        For Each File As String In IO.Directory.GetFiles(pngDir)
            Dim r As DataRow = dtSort.NewRow
            r("filename") = File
            r("datecreated") = IO.File.GetCreationTime(File)

            dtSort.Rows.Add(r)
        Next


        Dim rows() As DataRow = dtSort.Select("", "datecreated ASC")

        If rows IsNot Nothing Then
            For Each r As DataRow In rows
                Dim file As String = r("filename")

                If IO.Path.GetExtension(file).ToLower = ".png" Then
                    Dim cid As String = em.AddRelatedFile(file)

                    lines.Add("<img src='cid:" & cid & "'>")

                End If
            Next
        End If

        Dim htmlbody As String = "<html><body><br>"

        For Each line As String In lines
            htmlbody &= line
        Next

        htmlbody &= "</body></html>"
        ' Update the HTML to reference the image using the CID
        'Dim htmlBody As String = "<html><body>Embedded Image:<br><img src=""cid:IMAGE_CID""></body></html>"
        ' htmlBody = htmlBody.Replace("IMAGE_CID", cid)

        ' Set the basic email stuff: HTML body, subject, "from", "to"
        em.SetHtmlBody(htmlbody)

        Return True
    End Function

    Private Sub processOtherHTMLImages(ByRef em As Chilkat.Email)
        Dim fileContents As String = em.GetHtmlBody
        Dim mht As New Chilkat.Mht

        mht.UnlockComponent("CHRISTMHT_nV7Df9km3FvI")
        mht.EmbedImages = True

        Dim tmp As Chilkat.Email = New Chilkat.Email

        tmp = mht.HtmlToEmail(fileContents)

        '//copy the subject
        tmp.Subject = em.Subject

        '//copy the to
        For i As Integer = 0 To em.NumTo - 1
            tmp.AddTo(em.GetToName(i), em.GetToAddr(i))
        Next

        '//copy the cc
        For i As Integer = 0 To em.NumCC - 1
            tmp.AddCC(em.GetCcName(i), em.GetCcAddr(i))
        Next

        '//copy the bcc
        For i As Integer = 0 To em.NumBcc - 1
            tmp.AddBcc(em.GetBccName(i), em.GetBccAddr(i))
        Next

        '//copy the attachment
        For i As Integer = 0 To em.NumAttachments - 1
            tmp.AddFileAttachment(em.GetAttachmentFilename(i))
        Next

        em = tmp
    End Sub
    Public Function SendSMTP(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
           ByVal SendType As String, Optional ByVal Attach As String = "", _
           Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
           Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
           Optional ByVal sName As String = "", Optional ByVal InBody As Boolean = False, _
           Optional ByVal sFormat As String = "", _
           Optional ByVal LogFailure As Boolean = True, _
           Optional ByVal IncludeAttach As Boolean = True, _
           Optional ByVal MailFormat As String = "TEXT", _
           Optional ByVal SMTPName As String = "Default", Optional ByVal sEmbedFile As String = "", _
           Optional ByVal CustomSenderName As String = "", Optional ByVal CustomSenderAddress As String = "", _
           Optional ByVal customMsg As Chilkat.Email = Nothing, Optional ByVal ToTest As Boolean = False, _
           Optional ByVal deliveryReciept As Boolean = False, Optional ByVal destinationID As Integer = 0) As Boolean

        Dim Server As String
        Dim Sender As String
        Dim UserID As String
        Dim Password As String
        Dim sAddress As String
        Dim strAttach As String = ""
        Dim EmailRetry As Boolean
        Dim oValue As Object
        Dim oFile As String
        Dim nTimeout As Integer
        Dim sEncode As String
        Dim nPort As Integer = 25
        Dim emailPath As String = ""
        Dim popLogin As Boolean = False
        Dim SSL, StartTLS As Boolean
        Dim SMTPAuthMode As String = "LOGIN"
        Dim ok As Boolean = False
        Dim localRetry As Integer = 0
        Dim useMailObject As Boolean = False

        'lets make sure that the addresses are solved
        Addresses = clsMarsMessaging.ResolveEmailAddress(Addresses)
        SendCC = clsMarsMessaging.ResolveEmailAddress(SendCC)
        SendBCC = clsMarsMessaging.ResolveEmailAddress(SendBCC)



10:     If sName = "" Then
20:         smtplogName = "smtp_" & Date.Now.ToString("yyyyMMddHHmm") & ".debug"
30:     Else
40:         smtplogName = "smtp_" & sName & ".debug"
        End If

50:     clsMarsDebug.writeToDebug(smtplogName, "---------Begin SMTP Transmission for - " & sName & "-----------------", True)

        If Addresses = "" And SendCC = "" And SendBCC = "" Then
            clsMarsDebug.writeToDebug(smtplogName, "no email sent as there is no recipient", True)
        End If

60:     Dim cSMTP As Chilkat.MailMan = New Chilkat.MailMan

70:     cSMTP.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")

        Try
            ' If RunEditor = True Then AddHandler cSMTP.OnSendPercentDone, AddressOf cSMTP_OnSendPercentDone
        Catch : End Try

80:     Try
90:         Dim n As Integer

100:        n = clsMarsUI.MainUI.ReadRegistry("EmailRetry", "1")

110:        EmailRetry = Convert.ToBoolean(n)
120:    Catch
130:        EmailRetry = True
        End Try

140:    SMTPErr = ""

150:    clsMarsDebug.writeToDebug(smtplogName, "Collecting SMTP Server Information", True)

160:    If MailType = MarsGlobal.gMailType.SMTP Then
170:        If SMTPName = "Default" Then
NoneFound:
180:            With clsMarsUI.MainUI
190:                Server = .ReadRegistry("SMTPServer", oValue)
200:                Sender = .ReadRegistry("SMTPSenderName", "")
210:                UserID = .ReadRegistry("SMTPUserID", "")
220:                Password = .ReadRegistry("SMTPPassword", "", True)
230:                sAddress = .ReadRegistry("SMTPSenderAddress", "")
240:                sEncode = .ReadRegistry("CharSetEncoding", "US-ASCII")
250:                nPort = .ReadRegistry("SMTPPort", 25)
260:                SSL = Convert.ToInt32(.ReadRegistry("SMTPSSL", 0))
270:                StartTLS = Convert.ToInt32(.ReadRegistry("SMTPStartTLS", 0))
280:                SMTPAuthMode = .ReadRegistry("SMTPAuthMode", "LOGIN")

290:                Try
300:                    nTimeout = clsMarsUI.MainUI.ReadRegistry("SMTPTimeout", "30")
310:                Catch
320:                    nTimeout = 30
                    End Try

330:                Try
340:                    popLogin = Convert.ToInt32(.ReadRegistry("RequirePOPLogin", 0))

350:                    If popLogin = True Then
                            Dim popServer, popUser, popPassword As String
                            Dim popPort As Integer
                            Dim popSSL As Boolean

360:                        popServer = .ReadRegistry("SMTPPOPServer", "")
370:                        popUser = .ReadRegistry("SMTPPOPUser", "")
380:                        popPassword = .ReadRegistry("SMTPPOPPassword", "", True)
390:                        popPort = .ReadRegistry("SMTPPOPPort", 110)
400:                        popSSL = Convert.ToInt32(.ReadRegistry("SMTPPOPSSL", 0))

410:                        If Pop3Login(popUser, popPassword, popServer, popPort, popSSL) = False Then
420:                            _ErrorHandle("Could not log into POP3 server '" & popServer & "' as '" & popUser & "'", -1989140808, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True)
                            End If
                        End If
                    Catch : End Try
                End With
430:        Else
440:            Dim SQL As String
450:            Dim oRs As ADODB.Recordset
460:            Dim oData As New clsMarsData

470:            SQL = "SELECT * FROM SMTPServers WHERE SMTPName = '" & SQLPrepare(SMTPName) & "'"

480:            oRs = clsMarsData.GetData(SQL)

490:            Try
500:                If oRs.EOF = False Then
510:                    Server = oRs("smtpserver").Value
520:                    Sender = oRs("smtpsendername").Value
530:                    UserID = oRs("smtpuser").Value
540:                    Password = Decrypt(oRs("smtppassword").Value, "KalEidoscOpe")
550:                    sAddress = oRs("smtpsenderaddress").Value
560:                    nTimeout = oRs("smtptimeout").Value
570:                    nPort = IsNull(oRs("smtpport").Value, 25)
                        SSL = IsNull(oRs("smtpssl").Value, 0)
                        StartTLS = IsNull(oRs("smtpstarttls").Value, 0)
                        SMTPAuthMode = IsNull(oRs("smtpauthmode").Value, "LOGIN")

580:                    Try
590:                        popLogin = oRs("smtppoplogin").Value

600:                        If popLogin = True Then
                                Dim popServer, popUser, popPassword As String
                                Dim popPort As Integer
                                Dim popSSL As Boolean

610:                            popServer = IsNull(oRs("SMTPPOPServer").Value, "")
620:                            popUser = IsNull(oRs("SMTPPOPUser").Value)
630:                            popPassword = _DecryptDBValue(IsNull(oRs("SMTPPOPPassword").Value))
640:                            popPort = IsNull(oRs("SMTPPOPPort").Value, 110)
650:                            popSSL = IsNull(oRs("SMTPPOPSSL").Value, 0)

660:                            If Pop3Login(popUser, popPassword, popServer, popPort, popSSL) = False Then
670:                                _ErrorHandle("Could not log into POP3 server '" & popServer & "' as '" & popUser & "'", -1989140808, Reflection.MethodBase.GetCurrentMethod.Name, _
                                          Erl, , True)
                                End If
                            End If
                        Catch : End Try
680:                Else
690:                    oRs.Close()
700:                    GoTo NoneFound
710:                End If

720:                oRs.Close()
730:            Catch ex As Exception
740:                GoTo NoneFound
                End Try
            End If
750:    ElseIf MailType = MarsGlobal.gMailType.SQLRDMAIL Then
760:        Server = "mail.crystalreportsdistributor.com"
770:        sAddress = clsMarsUI.MainUI.ReadRegistry("SQL-RDAddress", "")

780:        If gnEdition = MarsGlobal.gEdition.EVALUATION Then
790:            Password = "evaluation123xyz"
800:            UserID = "evaluation@crystalreportsdistributor.com"
810:        Else
820:            UserID = CustID & "@crystalreportsdistributor.com"
830:            Password = CustID
840:            Sender = clsMarsUI.MainUI.ReadRegistry("SQL-RDSender", oValue)
            End If

850:        nTimeout = 120
860:        nPort = 25
        End If

870:    If CustomSenderName <> "" Then
            clsMarsDebug.writeToDebug(smtplogName, "Setting custom sender name", True)
880:        Sender = CustomSenderName
        End If

        If CustomSenderAddress <> "" Then
            clsMarsDebug.writeToDebug(smtplogName, "Setting custom sender address", True)
890:        sAddress = CustomSenderAddress
        End If

900:    Try
910:        If MailType = MarsGlobal.gMailType.SQLRDMAIL Then
920:            If Pop3Login(UserID, Password, Server) = False Then
930:                Throw New Exception("Could not log into SQL-RDMail Server. Please make sure that you have activated your SQL-RDMail Account")
940:            End If

950:            ' UserID = String.Empty
960:            'Password = String.Empty
            End If

970:        If Sender = "" Then Sender = sAddress

            Dim oMsg As Chilkat.Email

980:        If customMsg IsNot Nothing Then
                clsMarsDebug.writeToDebug(smtplogName, "Using already existing message object", True)
990:            oMsg = customMsg.Clone
                useMailObject = True
1000:       Else
                clsMarsDebug.writeToDebug(smtplogName, "Creating message object", True)

1010:           oMsg = New Chilkat.Email
            End If

1011:       clsMarsDebug.writeToDebug(smtplogName, "Configuring server...", True)

1020:       cSMTP.EnableEvents = True

1050:       cSMTP.SmtpHost = Server

1030:       If UserID <> "" Then
1040:           cSMTP.SmtpAuthMethod = SMTPAuthMode
1060:           cSMTP.SmtpUsername = UserID
1070:           cSMTP.SmtpPassword = Password
1080:       Else
1090:           cSMTP.SmtpAuthMethod = "NONE"
1100:       End If

1110:       If nTimeout > 0 Then
1120:           cSMTP.ConnectTimeout = nTimeout
1130:       Else
1140:           cSMTP.ConnectTimeout = 30
1150:       End If

1160:       cSMTP.SmtpPort = nPort
1170:       cSMTP.SmtpSsl = SSL
1180:       cSMTP.StartTLS = StartTLS

            clsMarsDebug.writeToDebug(smtplogName, "Server name:" & cSMTP.SmtpHost, True)

            clsMarsDebug.writeToDebug(smtplogName, "Verifying SMTP server connection", True)

            Do
1190:           ok = cSMTP.VerifySmtpConnection

                If ok = False Then
                    System.Threading.Thread.Sleep(1000)
                    localRetry += 1
                End If
            Loop Until ok = True Or localRetry > 3

1200:       If ok = False Then
1210:           Throw New Exception(cSMTP.LastErrorText)
            End If

            localRetry = 0

            clsMarsDebug.writeToDebug(smtplogName, "Verifying SMTP login credentials", True)

1220:       If UserID <> "" Then
                Do
                    ok = cSMTP.VerifySmtpLogin

                    If ok = False Then
                        System.Threading.Thread.Sleep(1000)
                        localRetry += 1
                    End If
                Loop Until ok = True Or localRetry > 3
            End If

1230:       If ok = False Then
1240:           Throw New Exception(cSMTP.LastErrorText)
            End If

            localRetry = 0

            clsMarsDebug.writeToDebug(smtplogName, "Opening connection", True)

            Do
1250:           ok = cSMTP.OpenSmtpConnection

                If ok = False Then
                    System.Threading.Thread.Sleep(1000)
                    localRetry += 1
                End If
            Loop Until ok = True Or localRetry > 3

1260:       If ok = False Then
1270:           Throw New Exception(cSMTP.LastErrorText)
            End If

1320:       clsMarsDebug.writeToDebug(smtplogName, "Configuring SMTP Message", True)

1321:       cSMTP.AllOrNone = False

            Dim mime As Boolean = False

            If useMailObject = False Then
                If MailFormat = "Web Archive" Then
                    oMsg.LoadEml(sEmbedFile)
                ElseIf MailFormat = "IMAGE" Then
                    embedPNGIntoMail(oMsg, sEmbedFile)
1330:           ElseIf MailFormat = "TEXT" Then
                    clsMarsDebug.writeToDebug(smtplogName, "TEXT mail format", True)

1340:               If InBody = False Then
                        If mime Then
                            oMsg.SetFromMimeText(Body)
                        Else
                            oMsg.SetTextBody(Body, "")
                        End If
1360:               Else
1370:                   Dim sBody As String
1380:                   clsMarsDebug.writeToDebug(smtplogName, "Embedding " & sEmbedFile, True)

1390:                   sBody = ReadTextFromFile(sEmbedFile)

1400:                   oMsg.Body = sBody
1410:               End If
1420:           Else
                    clsMarsDebug.writeToDebug(smtplogName, "HTML mail format", True)

1430:               If InBody = True And sEmbedFile <> "" Then
                        clsMarsDebug.writeToDebug(smtplogName, "Preparing to embed " & sEmbedFile, True)

1440:                   Dim mht As New Chilkat.Mht

1450:                   mht.UnlockComponent("CHRISTMHT_nV7Df9km3FvI")

1460:                   mht.UseCids = False

1470:                   Try
1480:                       If sEncode <> "US-ASCII" Then
1490:                           Dim sJaps As String

1500:                           sJaps = ReadTextFromFile(sEmbedFile)

1510:                           sJaps = sJaps.Replace("charset=UTF-8", "charset=shift_jis")

1520:                           SaveTextToFile(sJaps, sEmbedFile, , False)
1530:                       End If

                            Quiksoft.EasyMail.SMTP.License.Key = QuikSoftLicense

                            Dim tempMsg As Quiksoft.EasyMail.SMTP.EmailMessage = New Quiksoft.EasyMail.SMTP.EmailMessage
                            Dim tempMsgFile As String = clsMarsReport.m_OutputFolder & IO.Path.GetRandomFileName

                            tempMsg.ImportBody(sEmbedFile, Quiksoft.EasyMail.SMTP.BodyPartFormat.HTML, Quiksoft.EasyMail.SMTP.ImportBodyFlags.None)

                            tempMsg.SaveMessage(tempMsgFile)

1540:                       oMsg.LoadEml(tempMsgFile)


1550:                       If oMsg Is Nothing Then Throw New Exception(mht.LastErrorText)
1560:                   Catch ex As Exception
1570:                       Dim sBody As String

1580:                       sBody = ReadTextFromFile(sEmbedFile)

1590:                       If sEncode <> "US-ASCII" Then
1600:                           sBody = sBody.Replace("charset=UTF-8", "charset=shift_jis")
                            End If

1610:                       oMsg.SetHtmlBody(sBody)

                            'manually add attachments
                            Try
1620:                           For Each s As String In IO.Directory.GetFiles(GetDirectory(sEmbedFile))
1630:                               If s.ToLower.EndsWith(".htm") = False Then
1640:                                   oMsg.AddFileAttachment(s)
1650:                               End If
1660:                           Next
                            Catch : End Try
1670:                   End Try
1680:               Else
1681:                   clsMarsDebug.writeToDebug(smtplogName, "Setting message body to HTML: " & Body, True)

                        '//strip crap out of the HEADER
                        Try
                            Dim cleanHTML As String = Body

                            Dim startp, endp As Integer

                            startp = cleanHTML.IndexOf("<HEAD>") + 6
                            endp = cleanHTML.IndexOf("</HEAD>")

                            Dim crap As String = cleanHTML.Substring(startp, endp - startp)

                            cleanHTML = cleanHTML.Remove(startp, crap.Length)

                            Body = cleanHTML
                        Catch ex As Exception
                            '//use regular expression
                            Dim val As String = Body
                            val = System.Text.RegularExpressions.Regex.Replace(val, "<HEAD>.*</HEAD>", "<HEAD></HEAD>", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
                            Body = val
                        End Try

1690:                   oMsg.SetHtmlBody(Body)

                        Dim autoEmbedImages As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AutoEmbedImagesIntoHTMLEmails", 1))

                        If autoEmbedImages Then processOtherHTMLImages(oMsg)
1700:               End If
1710:           End If
            End If


1720:       oMsg.FromAddress = sAddress
1730:       oMsg.FromName = Sender
1740:       oMsg.ReplyTo = sAddress

1760:       If useMailObject = False Then

                clsMarsDebug.writeToDebug(smtplogName, "Setting FROM details", True)

                If Addresses.Contains(",") Then Addresses = Addresses.Replace(",", ";")
                If SendCC.Contains(",") Then SendCC = SendCC.Replace(",", ";")
                If SendBCC.Contains(",") Then SendBCC = SendBCC.Replace(",", ";")

                If Addresses <> "" And Addresses IsNot Nothing Then
                    If Addresses.Contains(";") Then
1770:                   For Each s As String In Addresses.Split(";")
1780:                       If s <> "" Then
1790:                           oMsg.AddTo(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding To recipient: " & s, True)
1800:                       End If
1810:                   Next
                    ElseIf Addresses.Contains(",") Then
1811:                   For Each s As String In Addresses.Split(",")
1812:                       If s <> "" Then
1813:                           oMsg.AddTo(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding To recipient: " & s, True)
1814:                       End If
1815:                   Next
                    Else
                        oMsg.AddTo(Addresses, Addresses)
                    End If
                End If

1820:           If SendCC <> "" And SendCC IsNot Nothing Then
                    If SendCC.Contains(";") Then
1830:                   For Each s As String In SendCC.Split(";")
1840:                       If s <> "" Then
1850:                           oMsg.AddCC(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding CC recipient: " & s, True)
1860:                       End If
1870:                   Next
                    ElseIf SendCC.Contains(",") Then
1871:                   For Each s As String In SendCC.Split(",")
1872:                       If s <> "" Then
1873:                           oMsg.AddCC(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding CC recipient: " & s, True)
1874:                       End If
1875:                   Next
                    Else
                        oMsg.AddCC(SendCC, SendCC)
                    End If
                End If

1880:           If SendBCC <> "" And SendBCC IsNot Nothing Then
                    If SendBCC.Contains(";") Then
1890:                   For Each s As String In SendBCC.Split(";")
1900:                       If s <> "" Then
1910:                           oMsg.AddBcc(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding BCC recipient: " & s, True)
1920:                       End If
1930:                   Next
                    ElseIf SendBCC.Contains(",") Then
1931:                   For Each s As String In SendBCC.Split(",")
1932:                       If s <> "" Then
1933:                           oMsg.AddBcc(s, s)
                                clsMarsDebug.writeToDebug(smtplogName, "Adding BCC recipient: " & s, True)
1934:                       End If
1935:                   Next
                    Else
                        oMsg.AddBcc(SendBCC, SendBCC)
                    End If
1940:           End If

1950:           oMsg.Subject = Subject

1960:           If SendType = "Single" Then
1970:               If IncludeAttach = True And Attach <> "" Then
1980:                   strAttach = Attach

                        clsMarsDebug.writeToDebug(smtplogName, "Files to attach " & strAttach, True)

1990:                   For Each s As String In Attach.Split(";")
2000:                       If s <> "" Then
                                If IO.File.Exists(s) = False Then clsMarsDebug.writeToDebug(smtplogName, "The file " & s & " was not attached as it doesnt exist", True)
                                clsMarsDebug.writeToDebug(smtplogName, "Attaching file " & s, True)
                                oMsg.AddFileAttachment(s)
                            End If
2010:                   Next
2020:               End If
2030:           ElseIf SendType = "Package" Then
                    If rptFileNames IsNot Nothing Then
2040:                   For Each oFile In rptFileNames
                            If IO.File.Exists(oFile) = False Then clsMarsDebug.writeToDebug(smtplogName, "The file " & oFile & " was not attached as it doesnt exist", True)
                            clsMarsDebug.writeToDebug(smtplogName, "Attaching file " & oFile, True)
2050:                       oMsg.AddFileAttachment(oFile)
2060:                   Next
                    End If
2070:           End If

                clsMarsDebug.writeToDebug(smtplogName, "Total files attached: " & oMsg.NumAttachments, True)

2080:           If Extras <> "" Then

                    Extras = clsMarsParser.Parser.ParseString(Extras)

                    clsMarsDebug.writeToDebug(smtplogName, "Extra attachments " & Extras, True)

2090:               For Each s As String In Extras.Split(";")

2100:                   If s <> "" Then
2110:                       If s.IndexOf("*") > -1 Then
                                Dim compareMe As String = ExtractFileName(s)

2120:                           For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
2130:                               If ExtractFileName(x) Like compareMe Then
                                        clsMarsDebug.writeToDebug(smtplogName, "Attaching file " & x, True)
2140:                                   oMsg.AddFileAttachment(x)
                                    End If
2150:                           Next
2160:                       Else
2170:                           If IO.File.Exists(s) Then
                                    clsMarsDebug.writeToDebug(smtplogName, "Attaching file " & s, True)
2180:                               oMsg.AddFileAttachment(s)
2190:                           Else

                                    _ErrorHandle("Missing custom attachment: " & s, 5, Reflection.MethodBase.GetCurrentMethod.Name, 1110, "Could not locate custom attachment." & vbCrLf & vbCrLf & _
           "The email was sent without it." & vbCrLf & vbCrLf & _
           "To ensure that the email is sent with your specified attachment(s) in the future, please make sure that it exists at the location specified by the destination setup.", True)

                                End If
2200:                       End If

2210:                   End If
2220:               Next
                End If
            End If

2240:       If sEncode <> "US-ASCII" Then
2250:           oMsg.Charset = "shift_jis"
            End If

2260:       emailPath = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages")

2270:       Try
2280:           If IO.Directory.Exists(emailPath) = False Then
2290:               IO.Directory.CreateDirectory(emailPath)
                End If

2300:           If emailPath.EndsWith("\") = False Then emailPath &= "\"

2310:           emailPath &= "email_" & Date.Now.ToString("yyyyMMddhhmmss") & ".eml"

2320:           oMsg.SaveEml(emailPath)

2330:           clsMarsDebug.writeToDebug(smtplogName, "Saving SMTP Message to disk (" & emailPath & ")", True)

2340:           _Delay(1)
2350:       Catch
2360:           emailPath = ""
            End Try
JUSTEND:
            clsMarsDebug.writeToDebug(smtplogName, "Validating Email Message:", True)
            clsMarsDebug.writeToDebug(smtplogName, "To Count: " & oMsg.NumTo, True)
            clsMarsDebug.writeToDebug(smtplogName, "Cc Count: " & oMsg.NumCC, True)
            clsMarsDebug.writeToDebug(smtplogName, "Bcc Count: " & oMsg.NumBcc, True)
            clsMarsDebug.writeToDebug(smtplogName, "Attachment Count: " & oMsg.NumAttachments, True)
2370:       clsMarsDebug.writeToDebug(smtplogName, "Sending message", True)


2371:       If deliveryReciept = True And destinationID > 0 Then
2372:           oMsg.AddHeaderField("Return-Receipt-To:", Chr(34) & oMsg.FromName & Chr(34) & " <" & oMsg.FromAddress & ">")
                oMsg.AddHeaderField("Generate-Delivery-Report:", Chr(34) & oMsg.FromName & Chr(34) & " <" & oMsg.FromAddress & ">")

2374:           LogReceiptRqst(destinationID, Now, oMsg.Subject, Addresses, SendCC, SendBCC)
            End If


2380:       ok = cSMTP.SendEmail(oMsg)

2390:       If ok = False Then
2400:           Throw New Exception(cSMTP.LastErrorText)
            End If

2410:       clsMarsUI.MainUI.BusyProgress(100, "", True)

            clsMarsDebug.writeToDebug(smtplogName, "Message sent: Logging email", True)

2420:       EmailLog(Addresses, Subject, sName, emailPath)

2430:       clsMarsDebug.writeToDebug(smtplogName, "Disconnecting from SMTP server", True)

2440:       cSMTP.CloseSmtpConnection()

2460:       Return True
2470:   Catch ex As Exception

2480:       Try : cSMTP.CloseSmtpConnection() : Catch : End Try

2490:       If MailType = MarsGlobal.gMailType.SMTP Then
2500:           Dim SQL As String
2510:           Dim oData As New clsMarsData
2520:           Dim oRsx As ADODB.Recordset


2530:           SQL = "SELECT * FROM SMTPServers WHERE SMTPID >" & nBackID & " AND IsBackup = 1"

2540:           oRsx = clsMarsData.GetData(SQL)

2550:           If oRsx.EOF = False Then
2560:               nBackID = oRsx("smtpid").Value

2570:               clsMarsUI.MainUI.BusyProgress(50, "Using backup SMTP server [" & oRsx("smtpname").Value & "]")

2580:               If SendSMTP(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, _
                         sName, InBody, sFormat, False, IncludeAttach, MailFormat = "TEXT", _
                         oRsx("smtpname").Value, sEmbedFile, CustomSenderName, CustomSenderAddress, customMsg, ToTest, deliveryReciept, destinationID) = True Then
2590:                   oRsx.Close()
2600:                   Return True
2610:               End If
2620:           End If

2630:           oRsx.Close()
2640:       End If

2650:       Dim sMsg As String = ""

2660:       If ex.Message.IndexOf("503 AUTH") > -1 Or ex.Message.ToLower.IndexOf("authentication") > -1 Then
2670:           sMsg = "Your SMTP server is not configured to use a username, password or both. Please remove the username and password and try again"
2680:       ElseIf ex.Message.IndexOf("535 authorization failed") > -1 Or _
                 ex.Message.IndexOf("not implemented") > -1 Then
2690:           sMsg = "Please check the provided userid and password. " & _
                     "Also, some SMTP servers are not configured to use a username, password or both."
2700:       ElseIf ex.Message.IndexOf("GetHostByName") > -1 Then
2710:           sMsg = "SQL-RD cannot find a connection to your server.  Please ensure that you have a connection to your server, or that the mail server name has been entered correctly."
2720:       ElseIf ex.Message.IndexOf("Administrative prohibition") > -1 Then
2730:           sMsg = "This message has been retrun by your SMTP server. " & _
                     "Please speak to you email administrator to enausre that you have the required " & _
                     "permissions.  Also ensure that your SMTP server allows relaying"
2740:       ElseIf ex.Message.ToLower.IndexOf("message size exceeds fixed maximum") > -1 Then
2750:           sMsg = "Your SMTP server has placed a liimit on the size of email that can be sent using your " & _
                     "account.  Please approach your SMTP server administrator to raise this limit, " & _
                     "or try zipping the files before sending."
2760:       ElseIf ex.Message.ToLower.IndexOf("data from socket") > -1 Then
2770:           sMsg = "This error comes about if the SMTP server is not available. " & _
                     "Go to Options - Messaging and switch on 'Try resending failed emails'. " & _
                     "This will ensure that your email is tried again and again until it is finaly " & _
                     "successful."
            ElseIf ex.Message.ToLower.Contains("invalid address") Then
                sMsg = "The specified email address is not valid. Please check the email address and try again"
            ElseIf ex.Message.Contains("TO:0") Then
                sMsg = "No initial recipient has been recognized. Please check the 'To' entry in the schdule and ensure that one exists and that it is a valid email address."

                '2780:       ElseIf Err.Number = 5 Then
                '                sMsg = " You do not have rights to the [system output folder].  Try the following:" & vbCrLf & vbCrLf & _
                '           "-  Create a share to the ChristianSteven folder" & vbCrLf & _
                '           "- Make sure you have full rights to create, modify& delete files and subfolders" & vbCrLf & _
                '           "- Go to Options->System Paths and ensure that all the paths are mapped through new share."
            ElseIf ex.Message.Contains("550 5.7.1 Client does not have permissions to send as this sender") Then
                sMsg = "You do not have permission to send the email as the specified user. Please see your email administrator for more information"
2790:       Else
2800:           sMsg = ""
            End If


2810:       If LogFailure = True And EmailRetry = True And ToTest = False And Subject.ToLower.Contains("sqlrd error alert") = False Then
2820:           gErrorDesc = SendType & " Schedule:" & gScheduleName & "Report(s) were produced successfully but email failed with the following error: " & ex.Message
2830:           gErrorNumber = Err.Number
2840:           gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2850:           gErrorLine = Erl()
2860:           gErrorSuggest = sMsg

2870:           AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
                     InBody, sFormat, ex.Message, MailFormat, IncludeAttach, SMTPName, False, 0, sEmbedFile, emailPath)
2880:       Else
                If ToTest = False Then
2890:               gErrorDesc = SendType & " Schedule:" & gScheduleName & ". Report(s) produced successfully but email failed with the following error: " & ex.Message
                Else
                    gErrorDesc = ex.Message
                End If
2900:           gErrorNumber = Err.Number
2910:           gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2920:           gErrorLine = Erl()
2930:           gErrorSuggest = sMsg
2940:       End If

2950:       If gErrorNumber <> 91 And gErrorNumber > 0 And gErrorLine > 0 And _
            ex.Message.ToLower.Contains("using undisclosed recipients") = False And Subject.ToLower.Contains("sqlrd error alert") = False Then
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, Erl, gErrorSuggest, False, True)
            End If

2960:       Return False
        Finally
            Try
                clsMarsDebug.writeToDebug(smtplogName, cSMTP.SmtpSessionLog, True)

                If RunEditor = True Then RemoveHandler cSMTP.OnSendPercentDone, AddressOf cSMTP_OnSendPercentDone
            Catch : End Try

            clsMarsDebug.writeToDebug(smtplogName, "---------------------End------------------------", True)
2970:   End Try
    End Function


    Public Shared Function FTPReport(ByVal sServerName As String, ByVal sUserID As String, _
    ByVal sPassword As String, ByVal sFilename As String, ByVal sPath As String, _
    ByVal sTitle As String) As Boolean
        Dim strMsg As String
        Dim objFTP As New NIBLACK.ASPFTP

        objFTP.sServerName = Replace(sServerName, "ftp://", "")

        objFTP.sUserID = sUserID
        objFTP.sPassword = sPassword



        If objFTP.bConnect Then
            objFTP.bSetCurrentDir(sPath)
            'set the properties for the put function
            objFTP.lTransferType = 1

            'now put the file

            If objFTP.bPutFile(sFilename, ExtractFileName(sFilename)) Then
                'put was successful
                FTPReport = True
            Else
                'put failed...let user know
                FTPReport = False
                gErrorDesc = "Put Failed: " & objFTP.sError
                gErrorNumber = -24051976
            End If
            'clean up...
            objFTP = Nothing
        Else
            'default return msg
            FTPReport = False
            strMsg = objFTP.sError & ": Connection failure. " & objFTP.sErrorDesc
        End If
    End Function

    Public Sub OutlookAddresses(ByVal txtBox As TextBox)
        Try
            Dim objSession As Object 'MAPI.Session
            Dim objMessage As Object 'MAPI.Message
            Dim UserID As String
            Dim Password As String

            Dim CdoPR_EMS_AB_PROXY_ADDRESSES As Object

            CdoPR_EMS_AB_PROXY_ADDRESSES = &H800F101E

            UserID = clsMarsUI.MainUI.ReadRegistry("MAPIProfile", "")
            Password = clsMarsUI.MainUI.ReadRegistry("MAPIPassword", "", True)

            objSession = CreateObject("Redemption.RDOSession")

            objSession.Logon(UserID, Password, False, True)

            Dim Outbox As Object = objSession.GetDefaultFolder(4)

            objMessage = Outbox.Items.Add

            Dim AddressBook As Object = objSession.AddressBook

            objMessage.Recipients = AddressBook.ShowAddressbook

            For Each Recip As Object In objMessage.Recipients
                txtBox.Text &= Recip.Name & ";"
            Next

            objSession = Nothing
            objMessage = Nothing
        Catch ex As Exception
            If ex.Message Like "*MAPI_E_LOGON_FAILED*" Then
                _ErrorHandle("Logon into MAPI system failed.", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            ElseIf ex.Message.ToLower.IndexOf("user_cancel") > -1 Then
                Return
            Else
                Dim sMsg As String = "Error initialising Collaboration Data Objects (CDO)." & vbCrLf & _
                         "CDO might not be installed correctly on your pc." & vbCrLf & _
                         "You can install CDO from your Microsoft Office as follows:" & vbCrLf & _
                         "1. Run your Office Setup" & vbCrLf & _
                         "2. Select 'Add or Remove Features'..." & vbCrLf & _
                         "3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & vbCrLf & _
                         "4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & vbCrLf & _
                         "5. Click the 'Update' button"

                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), sMsg)

            End If
        End Try
    End Sub

    Public Sub oldOutlookAddresses(ByVal txtBox As TextBox)
        Try
            Dim objSession As Object 'MAPI.Session
            Dim objMessage As Object 'MAPI.Message
            Dim objRecip As Object 'MAPI.Recipient
            Dim objField As Object 'MAPI.Field
            Dim I As Integer
            Dim UserID As String
            Dim Password As String
            Dim oSec As New clsMarsSecurity

            Dim CdoPR_EMS_AB_PROXY_ADDRESSES As Object

            CdoPR_EMS_AB_PROXY_ADDRESSES = &H800F101E

            UserID = clsMarsUI.MainUI.ReadRegistry("MAPIProfile", "")
            Password = clsMarsUI.MainUI.ReadRegistry("MAPIPassword", "", True)

            objSession = CreateObject("MAPI.Session")

            objSession.Logon(UserID, Password, False, True)


            objMessage = objSession.Outbox.Messages.Add

            objMessage.Recipients = objSession.AddressBook(OneAddress:=False)

            For I = 1 To objMessage.Recipients.Count
                txtBox.Text = txtBox.Text & objMessage.Recipients(I).name & ";"
            Next


            objSession = Nothing
            objMessage = Nothing
            objRecip = Nothing
            objField = Nothing
        Catch ex As Exception

            If ex.Message Like "*MAPI_E_LOGON_FAILED*" Then
                _ErrorHandle("Logon into MAPI system failed.", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            ElseIf ex.Message.ToLower.IndexOf("user_cancel") > -1 Then
                Return
            Else
                Dim sMsg As String = "Error initialising Collaborative Data Objects (CDO)." & vbCrLf & _
                         "CDO might not be installed correctly on your pc." & vbCrLf & _
                         "You can install CDO from your Microsoft Office as follows:" & vbCrLf & _
                         "1. Run your Office Setup" & vbCrLf & _
                         "2. Select 'Add or Remove Features'..." & vbCrLf & _
                         "3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & vbCrLf & _
                         "4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & vbCrLf & _
                         "5. Click the 'Update' button"

                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), sMsg)

            End If
        End Try
    End Sub

    Public Shared Sub EmailLog(ByVal SendTo As String, ByVal Subject As String, _
           Optional ByVal sName As String = "", Optional ByVal fileName As String = "")
10:     Try
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
20:         Dim oData As clsMarsData = New clsMarsData

30:         If sName = "" Then sName = gScheduleName

40:         sCols = "LogID,ScheduleName,SendTo,Subject,DateSent,MessageFile"

            Dim dateSent As Date

50:         If RunEditor = True Then
60:             dateSent = CTimeZ(Now, dateConvertType.WRITE)
70:         Else
80:             dateSent = Now
            End If

90:         sVals = clsMarsData.CreateDataID("emaillog", "logid") & "," & _
       "'" & sName.Replace("'", "''") & "'," & _
       "'" & SendTo.Replace("'", "''") & "'," & _
       "'" & Subject.Replace("'", "''") & "', '" & ConDateTime(dateSent) & "'," & _
       "'" & SQLPrepare(fileName) & "'"

100:        SQL = "INSERT INTO EmailLog (" & sCols & ") VALUES (" & sVals & ")"

110:        If clsMarsData.WriteData(SQL, False) = False Then
                Throw New Exception("Failed to log the following email:" & vbCrLf & _
           "SendTo: " & SendTo & vbCrLf & _
           "Subject: " & Subject & vbCrLf & _
           "Schedule Name: " & sName & vbCrLf & _
           "Email File: " & fileName)
            End If
120:    Catch ex As Exception
130:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
        End Try
    End Sub


    Public Shared Sub AddToMailQueue(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
    ByVal SendType As String, Optional ByVal Attach As String = "", _
    Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
    Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
    Optional ByVal sName As String = "", Optional ByVal InBody As Boolean = False, _
    Optional ByVal sFormat As String = "", _
    Optional ByVal sError As String = "", Optional ByVal MailFormat As String = "TEXT", _
    Optional ByVal IncludeAttach As Boolean = True, Optional ByVal SMTPName As String = "Default", _
    Optional ByVal ReadReceipt As Boolean = False, Optional ByVal destinationID As Integer = 0, _
    Optional ByVal sEmbedFile As String = "", Optional ByVal emailPath As String = "")
        Try
            Dim oData As clsMarsData = New clsMarsData
            Dim sCols As String
            Dim sVals As String
            Dim SQL As String
            Dim sFiles As String = ""
            Dim sFile As String = ""
            Dim sQueuePath As String
            Dim sOutputPath As String
            Dim sTemp As String = ""

            If Addresses = "" And SendCC = "" And SendBCC = "" Then Return

10:         sOutputPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

20:         If sOutputPath.EndsWith("\") = False Then sOutputPath &= "\"

30:         sQueuePath = sOutputPath & "EmailQueue\"

40:         If System.IO.Directory.Exists(sQueuePath) = False Then
50:             System.IO.Directory.CreateDirectory(sQueuePath)
60:         End If

            If Attach IsNot Nothing Then
70:             If Attach.Length > 0 Then
80:                 sQueuePath &= "{" & clsMarsData.CreateDataID("", "") & "}\"

90:                 System.IO.Directory.CreateDirectory(sQueuePath)

100:                For Each s As String In Attach.Split(";")
110:                    If s.Length > 0 Then
120:                        sFile = ExtractFileName(s)
130:                        System.IO.File.Copy(s, sQueuePath & sFile)

140:                        sTemp &= sQueuePath & sFile & ";"
150:                    End If
                    Next

160:                Attach = sTemp
                End If
            End If

170:        If Not rptFileNames Is Nothing Then
180:            sQueuePath &= "{" & clsMarsData.CreateDataID("", "") & "}\"

190:            System.IO.Directory.CreateDirectory(sQueuePath)

200:            For Each sFile In rptFileNames
201:                If sFile IsNot Nothing Then
210:                    If sFile.Length > 0 Then
220:                        sTemp = ExtractFileName(sFile)

230:                        System.IO.File.Copy(sFile, sQueuePath & sTemp)

240:                        sFile = sQueuePath & sTemp

250:                        sFiles &= sFile & "|"
260:                    End If
261:                End If
270:            Next
280:        End If

            Dim entryDate As Date

            If RunEditor = True Then
                entryDate = CTimeZ(Now, dateConvertType.WRITE)
            Else
                entryDate = Now
            End If

290:        sCols = "QueueNumber,Addresses,Subject,Body,SendType,Attach,NumAttach,Extras," & _
                "SendCC,SendBcc,sName,InBody,sFormat,FileList,LastResult,LastAttempt,MailFormat," & _
                "IncludeAttach,SMTPName,ReadReceipt,DestinationID,EmbedFile,EntryDate,MessageFile"

300:        sVals = clsMarsData.CreateDataID("sendwait", "queuenumber") & "," & _
            "'" & SQLPrepare(Addresses) & "'," & _
            "'" & SQLPrepare(Subject) & "'," & _
            "'" & SQLPrepare(Body) & "'," & _
            "'" & SendType & "'," & _
            "'" & SQLPrepare(Attach) & "'," & _
            Numattach & "," & _
            "'" & SQLPrepare(Extras) & "'," & _
            "'" & SQLPrepare(SendCC) & "'," & _
            "'" & SQLPrepare(SendBCC) & "'," & _
            "'" & SQLPrepare(sName) & "'," & _
            "'" & InBody & "'," & _
            "'" & sFormat & "'," & _
            "'" & sFiles & "'," & _
            "'" & SQLPrepare(sError) & "','" & ConDateTime(entryDate) & "'," & _
            "'" & MailFormat & "'," & _
            Convert.ToInt32(IncludeAttach) & "," & _
            "'" & SQLPrepare(SMTPName) & "'," & _
            Convert.ToInt32(ReadReceipt) & "," & _
            destinationID & "," & _
            "'" & SQLPrepare(sEmbedFile) & "'," & _
            "'" & ConDateTime(entryDate) & "'," & _
            "'" & SQLPrepare(emailPath) & "'"

310:        SQL = "INSERT INTO SendWait (" & sCols & ") VALUES(" & sVals & ")"

320:        clsMarsData.WriteData(SQL, False)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Public Sub ProcessEmailQueue()
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim oData As clsMarsData = New clsMarsData
            Dim Addresses, Subject, Body, SendType, Attach, NumAttach, Extras, _
                SendCC, SendBcc, sName, sFormat, FileList, MailFormat As String
            Dim QueueID As Integer
            Dim Success As Boolean
            Dim I As Integer = 0
            Dim InBody As Boolean
            Dim IncludeAttach As Boolean
            Dim SMTPName As String
            Dim ReadReceipt As Boolean
            Dim DestinationID As Integer = 0
            Dim sEmbed As String = ""
            Dim logFile As String = "emailQueueprocess.debug"

            SQL = "SELECT * FROM SendWait"

            oRs = clsMarsData.GetData(SQL)

            clsMarsDebug.writeToDebug(logFile, "Collecting emails to retry", False)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    I = 0
                    rptFileNames = Nothing


                    Addresses = oRs("addresses").Value
                    Subject = oRs("subject").Value
                    Body = oRs("body").Value
                    SendType = oRs("sendtype").Value
                    Attach = oRs("attach").Value
                    NumAttach = oRs("numattach").Value
                    MailFormat = IsNull(oRs("mailformat").Value)
                    sEmbed = IsNull(oRs("embedfile").Value)

                    clsMarsDebug.writeToDebug(logFile, "Email to " & Addresses & " with subject " & Subject, True)

                    If IsNumeric(NumAttach) = False Then
                        NumAttach = 0
                    End If

                    Extras = oRs("extras").Value
                    SendCC = oRs("sendcc").Value
                    SendBcc = oRs("sendbcc").Value
                    sName = oRs("sname").Value

                    SMTPName = IsNull(oRs("smtpname").Value, "Default")

                    clsMarsDebug.writeToDebug(logFile, "Using SMTP Server " & SMTPName, True)

                    Try
                        IncludeAttach = Convert.ToBoolean(oRs("includeattach").Value)
                    Catch ex As Exception
                        IncludeAttach = True
                    End Try

                    clsMarsDebug.writeToDebug(logFile, "Include attachments: " & IncludeAttach, True)

                    Try
                        InBody = Convert.ToBoolean(oRs("inbody").Value)
                    Catch
                        InBody = False
                    End Try

                    clsMarsDebug.writeToDebug(logFile, "Embed report into email body: " & InBody, True)


                    Try
                        ReadReceipt = Convert.ToBoolean(oRs("readreceipt").Value)
                        DestinationID = IsNull(oRs("destinationid").Value, 0)
                    Catch ex As Exception
                        ReadReceipt = False
                    End Try

                    sFormat = oRs("sformat").Value
                    FileList = oRs("filelist").Value

                    clsMarsDebug.writeToDebug(logFile, "Email format: " & sFormat, True)
                    clsMarsDebug.writeToDebug(logFile, "Files to attach: " & FileList, True)

                    ReDim rptFileNames(0)

                    If FileList.Length > 0 Then
                        For Each s As String In FileList.Split("|")
                            If s.Length > 0 Then
                                ReDim Preserve rptFileNames(I)

                                rptFileNames(I) = s

                                I += 1
                            End If
                        Next
                    End If

                    QueueID = oRs("queuenumber").Value

                    clsMarsDebug.writeToDebug(logFile, "Resending email", True)

                    If MailType = MarsGlobal.gMailType.MAPI Then
                        Success = SendMAPI(Addresses, Subject, Body, SendType, Attach, _
                        NumAttach, Extras, SendCC, SendBcc, Convert.ToBoolean(InBody), sFormat, sName, False, , ReadReceipt, DestinationID)
                    ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                        Success = SendSMTP(Addresses, Subject, Body, SendType, Attach, _
                        NumAttach, Extras, SendCC, SendBcc, sName, Convert.ToBoolean(InBody), _
                        sFormat, False, IncludeAttach, MailFormat, SMTPName, sEmbed)
                    End If

                    If Success = True Then
                        clsMarsDebug.writeToDebug(logFile, "Email retry succeeded", True)

                        clsMarsData.WriteData("DELETE FROM SendWait WHERE QueueNumber =" & QueueID)
                    Else
                        clsMarsDebug.writeToDebug(logFile, "Email retry failed with error: " & gErrorDesc, True)

                        clsMarsData.WriteData("UPDATE SendWait SET LastAttempt = '" & ConDateTime(Date.Now) & "', LastResult ='" & SQLPrepare(gErrorDesc) & "' " & _
                        "WHERE QueueNumber = " & QueueID, False)

                        Dim DoNotEmail As Boolean

                        Try
                            DoNotEmail = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("OneEmailPerFailure", 1)))
                        Catch ex As Exception
                            DoNotEmail = True
                        End Try

                        _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, , DoNotEmail)
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            oData.DisposeRecordset(oRs)
        Catch ex As Exception
            ''console.writeline(ex.Message)
        End Try
    End Sub


    Public Shared Function ResolveEmailAddress(ByVal sIn As String) As String
        Dim sVals() As String
        Dim sKey As String
        Dim I As Integer = 0
        Dim sProcess() As String
        Dim sOut As String
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sTemp As String

        If sIn.Length = 0 Then Return String.Empty

        sVals = sIn.Split(";")

        For Each s As String In sVals

            If s.Length > 3 Then sKey = s.Substring(0, 4).ToLower Else GoTo Here

            ReDim Preserve sProcess(I)

            Select Case sKey
                Case "<[f]"
                    s = s.Replace("<[f]", String.Empty).Replace(">", String.Empty)

                    sProcess(I) = ReadTextFromFile(s)
                Case "<[a]"
                    s = s.Replace("<[a]", String.Empty).Replace(">", String.Empty)

                    SQL = "SELECT * FROM ContactAttr c INNER JOIN " & _
                    "ContactDetail d ON c.ContactID = " & _
                    "d.ContactID WHERE c.ContactName = '" & SQLPrepare(s) & "'"

                    oRs = clsMarsData.GetData(SQL)

                    If Not oRs Is Nothing Then

                        If oRs.EOF = True Then
                            sProcess(I) = String.Empty
                        Else
                            Do While oRs.EOF = False
                                sTemp = oRs("emailaddress").Value & ";"

                                If sTemp.IndexOf("@") < 0 Then
                                    sTemp = sTemp.Replace(";", String.Empty)
                                    sTemp = ResolveEmailAddress("<[a]" & _
                                    sTemp & ">;")
                                End If

                                oRs.MoveNext()

                                sProcess(I) &= sTemp
                            Loop
                        End If
                        oRs.Close()
                    End If

                Case "<[d]"

                    Dim oItem As New frmDataItems

                    sProcess(I) = oItem._GetDataItemValue(s) & ";"
                Case "<[z]"
                    s = s.Replace("<[z]", String.Empty).Replace(">", String.Empty)

                    SQL = "SELECT * FROM ContactAttr c INNER JOIN " & _
                    "ContactDetail d ON c.ContactID = " & _
                    "d.ContactID WHERE c.ContactName = '" & SQLPrepare(s) & "'"

                    oRs = clsMarsData.GetData(SQL)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = True Then
                            sProcess(I) = String.Empty
                        Else
                            Do While oRs.EOF = False
                                sTemp = oRs("faxnumber").Value & ";"

                                'If sTemp.IndexOf("@") < 0 Then
                                '    sTemp = sTemp.Replace(";", String.Empty)
                                '    sTemp = ResolveEmailAddress("<[a]" & _
                                '    sTemp & ">;")
                                'End If

                                oRs.MoveNext()

                                sProcess(I) &= sTemp
                            Loop
                        End If
                        oRs.Close()
                    End If
                Case "<[y]"
                    s = s.Replace("<[y]", String.Empty).Replace(">", String.Empty)

                    SQL = "SELECT * FROM ContactAttr c INNER JOIN " & _
                    "ContactDetail d ON c.ContactID = " & _
                    "d.ContactID WHERE c.ContactName = '" & SQLPrepare(s) & "'"

                    oRs = clsMarsData.GetData(SQL)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = True Then
                            sProcess(I) = String.Empty
                        Else
                            Do While oRs.EOF = False
                                sTemp = oRs("phonenumber").Value & ";"

                                oRs.MoveNext()

                                sProcess(I) &= sTemp
                            Loop
                        End If
                        oRs.Close()
                    End If
                Case Else
                    sProcess(I) = s
            End Select

            Try
                If sProcess(I).EndsWith(";") = False And sProcess(I).Length > 0 Then _
                sProcess(I) &= ";"
            Catch
                sProcess(I) = ""
            End Try

            I += 1
Here:
        Next

        Try
            For Each s As String In sProcess
                If s.Length > 0 Then sOut &= s
            Next
        Catch
            sOut = sIn
        End Try

        Return sOut

    End Function

    Private Sub _GetMailValues(ByVal msgObj As Quiksoft.EasyMail.Parse.EmailMessage)
        Try
            gSubject = msgObj.Subject

            For Each ob As Quiksoft.EasyMail.Parse.BodyPart In msgObj.BodyParts
                gBody &= ob.BodyText
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.From
                gFrom &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.To
                gTo &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.CC
                gCc &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.BCC
                gBcc &= oA.EmailAddress & ";"
            Next
        Catch : End Try
    End Sub

    Public Function DownloadMessageToMemoryStream(ByVal mailserver As String _
    , ByVal username As String, ByVal password As String, ByVal Download As Boolean, _
    ByVal SearchSubject As String, ByVal SearchBody As String) As Boolean

        'create POP3 object

        Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
        Quiksoft.EasyMail.Parse.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
        Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Computer)/12521600F458357418750F38578A1CB6D9"

        Dim pop3Obj As New POP3
        Dim sReturn(1) As String
        Dim oData As New clsMarsData
        Dim msgObj As EmailMessage
        Dim MsgCount As Integer
        Dim sTempSub As String
        Dim sTempBody As String
        Dim Found As Boolean = False
        Dim sDownloadPath As String
        Dim nEmails() As Integer
        Dim sPath As String
        Dim MarkForDelete As Boolean
        Dim x As Integer = 0

        Try
            'Message will be stored in files
            Dim fs As FileStream

            sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

            sPath &= "Downloaded Emails\"

            If IO.Directory.Exists(sPath) = False Then
                IO.Directory.CreateDirectory(sPath)
            End If

            'Connect to the POP3 mail server
            pop3Obj.Connect(mailserver)

            'Log onto the POP3 mail server
            pop3Obj.Login(username, password, AuthMode.Plain)

            MsgCount = pop3Obj.GetMessageCount

            For I As Integer = 1 To MsgCount

                MarkForDelete = False

                'If Download = False Then
                '    MemoryStream = New MemoryStream
                '    pop3Obj.DownloadMessage(I, MemoryStream)
                '    MemoryStream.Position = 0
                '    msgObj = New EmailMessage(MemoryStream)
                'Else

                sDownloadPath = sPath & "{" & clsMarsData.CreateDataID("", "") & "}.eml"

                fs = New FileStream(sDownloadPath, FileMode.Create, FileAccess.ReadWrite)

                pop3Obj.DownloadMessage(I, fs)
                fs.Position = 0
                msgObj = New EmailMessage(fs)

                'End If


                sTempSub = msgObj.Subject.ToLower

                If SearchSubject.Length > 0 Then
                    Try
                        If sTempSub.IndexOf(SearchSubject.ToLower) > -1 Then
                            _GetMailValues(msgObj)

                            Found = True
                            MarkForDelete = True
                        End If
                    Catch : End Try
                End If

                sTempBody = String.Empty

                For Each msgBodyPart As BodyPart In msgObj.BodyParts

                    'Get plain text message body
                    If msgBodyPart.Format = BodyPartFormat.Text Then _
                       sTempBody &= msgBodyPart.BodyText.ToLower
                Next

                If SearchBody.Length > 0 Then
                    Try
                        If sTempBody.IndexOf(SearchBody.ToLower) > -1 Then
                            _GetMailValues(msgObj)
                            Found = True
                            MarkForDelete = True
                        End If
                    Catch : End Try
                End If


                fs.Close()

                If MarkForDelete = True Then
                    ReDim Preserve nEmails(x)
                    nEmails(x) = I

                    x += 1
                End If

                Try
                    IO.File.Delete(sDownloadPath)
                Catch : End Try
            Next

            Try
                If Download = True Then
                    For Each n As Integer In nEmails
                        pop3Obj.DeleteMessage(n)
                    Next
                End If
            Catch : End Try
Close:
            'Disconnect from POP3 server
            pop3Obj.Disconnect()

            'Close the memory stream

            Return Found

            'Catch POP3 exception errors
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function


    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    '    Public Function SendSMTP(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
    '        ByVal SendType As String, Optional ByVal Attach As String = "", _
    '        Optional ByVal Numattach As Integer = 0, Optional ByVal Extras As String = "", _
    '        Optional ByVal SendCC As String = "", Optional ByVal SendBCC As String = "", _
    '        Optional ByVal sName As String = "", Optional ByVal InBody As Boolean = False, _
    '        Optional ByVal sFormat As String = "", _
    '        Optional ByVal LogFailure As Boolean = True, _
    '        Optional ByVal IncludeAttach As Boolean = True, _
    '        Optional ByVal MailFormat As String = "TEXT") As Boolean

    '        On Error GoTo Trap
    '        Dim Server As String
    '        Dim Sender As String
    '        Dim UserID As String
    '        Dim Password As String
    '        Dim sAddress As String
    '        Dim I As Integer
    '        Dim strAttach As String = ""
    '        Dim sCRDMsg As String
    '        Dim sRead As String
    '        Dim EmailRetry As String
    '        Dim oValue As Object
    '        Dim oFile As String

    '        EmailRetry = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, sKey, _
    '        "EmailRetry", "False")

    '        SMTPErr = ""

    '        Dim tmpAtt

    '        If MailType = MarsGlobal.gMailType.SMTP Then
    '            Server = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, sKey, _
    '            "SMTPServer", oValue)

    '            Sender = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, _
    '                sKey, "SMTPSenderName", "")

    '            UserID = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, sKey, "SMTPUserID", "")
    '            Password = oSec._Decrypt(clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, sKey, _
    '            "SMTPPassword", ""))

    '            sAddress = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, _
    '                sKey, "SMTPSenderAddress", "")

    '        ElseIf MailType = MarsGlobal.gMailType.CRDMAIL Then
    '            Server = "mail.crystalreportsdistributor.com"
    '            sAddress = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, _
    '                sKey, "CRDAddress", "")

    '            If CurVersion = "Evaluation" Then
    '                Password = "evaluation123xyz"
    '                UserID = "evaluation@crystalreportsdistributor.com"
    '            Else
    '                UserID = CustID & "@crystalreportsdistributor.com"
    '                Password = CustID
    '                Sender = clsmarsui.mainui._ReadRegistry(Registry.LocalMachine, _
    '                    sKey, "CRDSender", oValue)
    '            End If

    '            'sCRDMsg = POP3Login(UserID, Password)

    '            '20:         If sCRDMsg <> "Success" Then
    '            '21:             ReportError = "SQL-RD Mail Error: " & sCRDMsg
    '            '22:             ErrorNumber = -241458
    '            '23:             GoTo Fail
    '            '            End If

    '        End If

    '        If Sender = "" Then Sender = sAddress

    '        oSMTP = New vbSendMail.clsSendMail

    '        With oSMTP
    '            .SMTPHost = Server
    '            .SMTPHostValidation = 0 'VALIDATE NON
    '            .EmailAddressValidation = 1 'VALIDATE_SYNTAX
    '            .from = sAddress
    '            .ReplyToAddress = sAddress
    '            .FromDisplayName = Sender
    '            .Delimiter = ";"

    '            If Right(Addresses, 1) = ";" Then
    '                .Recipient = Left(Addresses, Len(Addresses) - 1)
    '            Else
    '                .Recipient = Addresses
    '            End If

    '            If Right(Extras, 1) = ";" Then
    '                Extras = Left(Extras, Len(Extras) - 1)
    '            End If

    '            If SendCC <> "" Then
    '                If Right(SendCC, 1) = ";" Then
    '                    .CcRecipient = Left(SendCC, Len(SendCC) - 1)
    '                Else
    '                    .CcRecipient = SendCC
    '                End If
    '            End If

    '            If SendBCC <> "" Then
    '                If Right(SendBCC, 1) = ";" Then
    '                    .BccRecipient = Left(SendBCC, Len(SendBCC) - 1)
    '                Else
    '                    .BccRecipient = SendBCC
    '                End If
    '            End If

    '            If UserID <> "" Then
    '                .UseAuthentication = True
    '                .Username = UserID
    '                .Password = Password
    '            Else
    '                .UseAuthentication = False
    '            End If

    '            If SendType = "Single" Then
    '                If IncludeAttach = True Then
    '                    strAttach = Attach

    '                    If strAttach.EndsWith(";") = True Then _
    '                        strAttach = strAttach.Substring(0, strAttach.Length - 1)
    '                End If
    '            ElseIf SendType = "Package" Then
    '                For Each oFile In rptFileNames
    '                    If oFile.Length > 0 Then strAttach &= oFile & ";"
    '                Next
    '            End If

    '            If Extras <> "" Then
    '                If Extras.Substring(Extras.Length - 1, 1) = ";" Then Extras = _
    '                Extras.Substring(0, Extras.Length - 1)

    '                If IncludeAttach = True Then
    '                    .Attachment = strAttach & ";" & Extras
    '                Else
    '                    .Attachment = Extras
    '                End If
    '            Else
    '                If strAttach.EndsWith(";") = True Then _
    '                strAttach = strAttach.Substring(0, strAttach.Length - 1)

    '                If strAttach.Length > 0 Then .Attachment = strAttach
    '            End If



    '            .Subject = Subject

    '            If MailFormat = "TEXT" Then
    '                .AsHTML = False
    '            Else
    '                .AsHTML = True
    '            End If

    '            If InBody = True Then
    '                sRead = clsmarsui.mainui._ReadTextFile(Attach)

    '                .AsHTML = True

    '                .Message = sRead
    '            Else
    '                .Message = Body
    '            End If

    '            tmpAtt = .Attachment
    '            .Send()
    '            .SMTPHost = Server
    '        End With


    '        oSMTP.Disconnect()
    '        oSMTP = Nothing

    '        If sSMTPErr = "" Then
    '            SendSMTP = True
    '            EmailLog(Addresses, Subject, sName)
    '        Else
    'Fail:
    '            If LogFailure = "True" Then
    '                AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '                InBody, sFormat, Err.Description)
    '                SendSMTP = False
    '                gErrorDesc = "Report(s) were produced successfully but email failed with the following error: " & sSMTPErr
    '                gErrorNumber = -90210
    '            Else
    '                SendSMTP = False
    '                gErrorDesc = "Report(s) produced successfully but email failed with the following error: " & sSMTPErr
    '                gErrorNumber = -90210
    '            End If
    '        End If

    '        Exit Function
    'Trap:
    '        SendSMTP = False
    '        If LogFailure = True Then
    '            AddToMailQueue(Addresses, Subject, Body, SendType, Attach, Numattach, Extras, SendCC, SendBCC, sName, _
    '            InBody, sFormat, Err.Description)
    '        End If
    '    End Function

    Private Shared Sub oSMTP_SMTPStatus(ByVal TotalSent As Long, ByVal TotalSize As Long) ' Handles oSMTP.SMTPStatus

        Try
            Dim Percent As Double

            Percent = (TotalSent / TotalSize) * 100

            clsMarsUI.MainUI.BusyProgress(Percent, (TotalSent / 1000) & " Kbytes sent...")

            clsMarsDebug.writeToDebug(smtplogName, (TotalSent / 1000) & " Kbytes sent...", True)

        Catch : End Try
    End Sub

    Public Function TestSMTP(ByVal szServer As String, ByVal sUserID As String, ByVal sPassword As String, _
    ByVal nTimeout As Integer, ByVal szSenderName As String, ByVal szSenderAddress As String, _
    Optional ByVal nPort As Integer = 25, Optional ByVal SmtpSSL As Boolean = False, Optional ByVal SmtpStartTLS As Boolean = False, Optional ByVal AuthMode As String = "LOGIN") As Boolean
        Dim sTestAddress As String

        sTestAddress = InputBox("Please enter an email address to send to", _
        Application.ProductName, clsMarsUI.MainUI.ReadRegistry("TestSMTPAddress", ""))

        clsMarsUI.MainUI.SaveRegistry("TestSMTPAddress", _
        sTestAddress)

        If sTestAddress = "" Or sTestAddress.IndexOf("@") = -1 Then Exit Function

        Dim cSMTP As Chilkat.MailMan = New Chilkat.MailMan

        cSMTP.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")

        AddHandler cSMTP.OnSendPercentDone, AddressOf cSMTP_OnSendPercentDone

        Try
            Dim oMailMsg As New Chilkat.Email

            oMailMsg.AddTo(sTestAddress, sTestAddress)

            oMailMsg.Subject = "Test SMTP Email From SQL-RD"
            oMailMsg.Body = "This is a test SMTP email sent by SQL-RD"

            cSMTP.SmtpHost = szServer

            If sUserID.Length > 0 Then
                With cSMTP
                    .SmtpAuthMethod = AuthMode
                    .SmtpUsername = sUserID
                    .SmtpPassword = sPassword

                End With
            End If

            cSMTP.ConnectTimeout = nTimeout
            cSMTP.SmtpSsl = SmtpSSL
            cSMTP.StartTLS = SmtpStartTLS
            cSMTP.SmtpPort = nPort

            If cSMTP.VerifySmtpConnection = False Then
                Throw New Exception(cSMTP.LastErrorText)
            End If

            If sUserID <> "" Then
                If cSMTP.VerifySmtpLogin = False Then
                    Throw New Exception(cSMTP.LastErrorText)
                End If
            End If

            oMailMsg.FromAddress = szSenderAddress
            oMailMsg.FromName = szSenderName

            If cSMTP.SendEmail(oMailMsg) = False Then
                Throw New Exception(cSMTP.LastErrorText)
            End If

            MessageBox.Show("Email sent successfully!", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)

            cSMTP.CloseSmtpConnection()

            clsMarsUI.MainUI.BusyProgress(, , True)

            Return True
        Catch ex As Exception
            cSMTP.CloseSmtpConnection()

            Dim sMsg As String = ""

            If ex.Message.IndexOf("503 AUTH") > -1 Or ex.Message.ToLower.IndexOf("authentication") > -1 Then
                sMsg = "Your SMTP server is not configured to use a username, password or both. Please remove the username and password and try again"
            ElseIf ex.Message.IndexOf("535 authorization failed") > -1 Or _
            ex.Message.IndexOf("not implemented") > -1 Then
                sMsg = "Please check the provided userid and password. " & _
                "Also, some SMTP servers are not configured to use a username, password or both."
            ElseIf ex.Message.IndexOf("GetHostByName") > -1 Then
                sMsg = "SQL-RD cannot find a connection to your server.  Please ensure that you have a connection to your server, or that the mail server name has been entered correctly."
            Else
                sMsg = ""
            End If

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), sMsg)

            Return False
        Finally
            RemoveHandler cSMTP.OnSendPercentDone, AddressOf cSMTP_OnSendPercentDone
        End Try

    End Function


    'Private Sub oFax_Status(ByVal pStatObj As FaxmanJr.FaxStatusObj) Handles oFax.Status
    '    clsMarsDebug.writeToDebug("faxtrace.log", "Info from modem: " & pStatObj.CurrentStatusDesc, True)

    '    clsMarsUI.MainUI.BusyProgress(50, pStatObj.CurrentStatusDesc)
    'End Sub

    'Private Sub oFax_CompletionStatus(ByVal pStatObj As FaxmanJr.FaxStatusObj) Handles oFax.CompletionStatus
    '    FaxComplete = True

    '    If pStatObj.ErrorCode > 0 Then
    '        sFaxError = pStatObj.ErrorDesc
    '    Else
    '        sFaxError = String.Empty
    '    End If

    '    clsMarsUI.MainUI.BusyProgress(, , True)
    'End Sub

    Public Shared Function SendGROUPWISE(ByVal sendTo As String, ByVal cc As String, ByVal bcc As String, ByVal subject As String, _
    ByVal message As String, ByVal attach As String, ByVal sendType As String, Optional ByVal extras As String = "", _
    Optional ByVal includeAttach As Boolean = False, Optional ByVal inBody As Boolean = False, _
    Optional ByVal embedFile As String = "", Optional ByVal logFailure As Boolean = True, _
    Optional ByVal numAttach As Integer = 0, Optional ByVal sName As String = "", Optional ByVal sFormat As String = "") As Boolean
        Try
10:         Dim gwUser As String = clsMarsUI.MainUI.ReadRegistry("GWUser", "")
20:         Dim gwPassword As String = clsMarsUI.MainUI.ReadRegistry("GWPassword", "", True)
30:         Dim gwProxy As String = clsMarsUI.MainUI.ReadRegistry("GWProxy", "")
40:         Dim gwPOIP As String = clsMarsUI.MainUI.ReadRegistry("GWPOIP", "")
50:         Dim gwPOPort As String = clsMarsUI.MainUI.ReadRegistry("GWPOPort", "")
60:         Dim gwPath As String = sAppPath & "gwsend.exe"

70:         If IO.File.Exists(gwPath) = False Then
80:             Throw New Exception("SQL-RD could not locate a crucial Groupwise system component")
90:         End If

100:        Dim gwArguments As String
            Dim q As String = Chr(34)

101:        Dim configArguments As String

            'user
102:        If gwUser <> "" Then configArguments &= " /U=" & gwUser

            'gwpassword 
103:        configArguments &= " /Pa=" & gwPassword

            'gwproxy
104:        If gwProxy <> "" Then configArguments &= " /Prox=" & gwProxy

            'gwpoip
105:        If gwPOIP <> "" Then configArguments &= " /IPA=" & gwPOIP

            'gwpoport
106:        If gwPOPort <> "" Then configArguments &= " /IPP=" & gwPOPort

            'send to
110:        gwArguments &= " /T=" & q & clsMarsMessaging.ResolveEmailAddress(sendTo) & q

            'cc
            If cc <> "" Then
120:            gwArguments &= " /C=" & q & clsMarsMessaging.ResolveEmailAddress(cc) & q
            End If

130:        'bcc
            If bcc <> "" Then
140:            gwArguments &= " /B=" & q & clsMarsMessaging.ResolveEmailAddress(bcc) & q
            End If

            'subject
150:        gwArguments &= " /S=" & q & subject & q

            'message
160:        If inBody = True And embedFile <> "" Then
170:            Dim sBody As String = ReadTextFromFile(embedFile)
180:            gwArguments &= " /M=" & q & sBody & q
190:        Else
200:            gwArguments &= " /M=" & q & message & q
210:        End If

            'attachments
220:        Dim attachments As String = ""

230:        If includeAttach = True Then
240:            If sendType = "Single" Then
250:                attachments = attach
260:            ElseIf sendType = "Package" Then
270:                For Each oFile As String In rptFileNames
280:                    If oFile <> "" Then attachments &= oFile & ";"
290:                Next
300:            End If

310:            If attachments.EndsWith(";") = False Then attachments &= ";"
320:        End If

330:        If extras <> "" Then
340:            For Each s As String In extras.Split(";")

350:                If s <> "" Then
360:                    If s.IndexOf("*") > -1 Then
370:                        Dim compareMe As String = ExtractFileName(s)

380:                        For Each x As String In IO.Directory.GetFiles(GetDirectory(s))
400:                            If ExtractFileName(x) Like compareMe Then
410:                                attachments &= x & ";"
420:                            End If
430:                        Next
440:                    Else
450:                        If IO.File.Exists(s) Then
460:                            attachments &= s & ";"
470:                        End If
                        End If

                    End If
                Next
            End If

480:        If attachments <> "" Then
490:            gwArguments &= " /Att=" & q & attachments & q
500:        End If

            If IO.File.Exists(sAppPath & "gwsend.result") Then
504:            IO.File.Delete(sAppPath & "gwsend.result")
            End If

505:        gwArguments &= configArguments '& " -> " & Chr(34) & sAppPath & "gwsend.txt" & Chr(34)

510:        Dim gwProcess As Process = New Process

520:        With gwProcess
530:            With .StartInfo
540:                .FileName = gwPath
550:                .Arguments = gwArguments
560:                .WindowStyle = ProcessWindowStyle.Hidden
                    .UseShellExecute = True
570:            End With

590:            .Start()

600:        End With

            EmailLog(sendTo, subject, sName)
610:        Return True
        Catch ex As Exception
            Dim emailRetry As Boolean
            Dim sError As String = ex.Message

            emailRetry = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("EmailRetry", 0)))

            If logFailure = True And emailRetry = True Then
                AddToMailQueue(sendTo, subject, message, sendType, attach, numAttach, extras, cc, bcc, sName, _
                inBody, sFormat, sError)
                gErrorDesc = sendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & sendTo & " failed with the following error: " & sError
                gErrorSource = "SendGROUPWISE"
                gErrorNumber = Err.Number
                gErrorLine = Erl()
                gErrorSuggest = ""
            Else
                gErrorDesc = sendType & " Schedule:" & sName & "Report(s) were produced successfully but email to " & sendTo & " failed with the following error: " & sError
                gErrorNumber = Err.Number
                gErrorSource = "SendGROUPWISE"
                gErrorLine = Erl()
                gErrorSuggest = ""
            End If

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        End Try
    End Function

    Public Shared Sub StopEmailRetry()
        Try
            Dim SQL As String
            Dim keepValue As String = clsMarsUI.MainUI.ReadRegistry("EmailQueueDeleteAfter", "0")

            If keepValue = "0" Then Return

            Dim nKeep As Integer = 0
            Dim sUnit As String = ""

            nKeep = keepValue.Split(" ")(0)
            sUnit = keepValue.Split(" ")(1)

            If sUnit = "" Then sUnit = "Days"

            SQL = "SELECT * FROM SendWait"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim entryDate As Date = oRs("entrydate").Value
                Dim emailID As Integer = oRs("queuenumber").Value
                Dim currentDate As Date = Now.Date
                Dim actualValue As Double

                Dim timePassed As TimeSpan

                timePassed = entryDate.Subtract(currentDate)

                Select Case sUnit.ToLower
                    Case "minutes"
                        actualValue = timePassed.TotalMinutes
                    Case "hours"
                        actualValue = timePassed.TotalHours
                    Case "days"
                        actualValue = timePassed.TotalDays
                    Case "weeks"
                        actualValue = (timePassed.TotalDays / 7)
                    Case "months"
                        actualValue = (timePassed.TotalDays / 28)
                    Case Else
                        actualValue = timePassed.TotalDays
                End Select

                If actualValue > nKeep Then
                    clsMarsData.WriteData("DELETE FROM SendWait WHERE QueueNumber=" & emailID)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Const PR_ICON_INDEX = &H10800003

    Public Shared Function convertToMsg(ByVal sFile As String) As String
10:     Try
            clsMarsUI.BusyProgress(10, "Converting file...")

            Dim myOutlookApp = CreateObject("Outlook.Application")
            Dim objPost As Object
            Dim objSafePost As Object
            Dim objNS As Object
            Dim objInbox As Object

            clsMarsUI.BusyProgress(30, "Converting file...")
20:         objNS = myOutlookApp.GetNamespace("MAPI")
30:         objInbox = objNS.GetDefaultFolder(16) 'Drafts folder
40:         objPost = objInbox.Items.Add(6) 'Post Item

            clsMarsUI.BusyProgress(50, "Converting file...")
50:         objSafePost = CreateObject("Redemption.SafePostItem")
            'objPost.Save()
60:         objSafePost.Item = objPost
70:         objSafePost.Import(sFile, 1024)
80:         objSafePost.MessageClass = "IPM.Note"
            ' remove IPM.Post icon
90:         objSafePost.Fields(PR_ICON_INDEX) = ""

            clsMarsUI.BusyProgress(80, "Converting file...")
91:         Dim fileDir As String = IO.Path.GetDirectoryName(sFile)
92:         Dim msgFile As String = fileDir & "\" & IO.Path.GetFileNameWithoutExtension(sFile) & ".msg"

93:         If IO.File.Exists(msgFile) Then IO.File.Delete(msgFile)

100:        objSafePost.SaveAs(msgFile, 3) 'olMsg Type

            clsMarsUI.BusyProgress(98, "Converting file...")

110:        objSafePost = Nothing
120:        objPost = Nothing
130:        objInbox = Nothing
140:        objNS = Nothing
145:        myOutlookApp = Nothing

            Return msgFile
150:    Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Return Nothing
        Finally
            clsMarsUI.BusyProgress(, , True)
        End Try
    End Function


    Private Sub cSMTP_OnSendPercentDone(ByVal sender As Object, ByVal args As Chilkat.MailPercentDoneEventArgs) 'Handles cSMTP.OnSendPercentDone
        Try
            clsMarsUI.BusyProgress(args.PercentDone, "Sending email..." & args.PercentDone & "% ")
        Catch : End Try
    End Sub

    Private Sub faxing_FaxCompletionStatus(ByVal sender As Object, ByVal args As DataTech.FaxManJr.FaxEventArgs) Handles faxing.FaxCompletionStatus
        FaxComplete = True

        If args.Fax.FaxError <> DataTech.FaxManJr.FaxError.Ok Then
            sFaxError = args.Fax.FaxErrorString
        Else
            sFaxError = ""
        End If
    End Sub


    Private Sub faxing_FaxStatus(ByVal sender As Object, ByVal args As DataTech.FaxManJr.FaxEventArgs) Handles faxing.FaxStatus
        clsMarsUI.BusyProgress(50, args.Fax.FaxStatus.ToString)
    End Sub
End Class
