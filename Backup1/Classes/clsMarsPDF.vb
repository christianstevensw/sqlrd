Imports System.IO
Imports PDFtoolkitX
Imports PDF2ManyX
Imports System.Management

Friend Class clsMarsPDF
    Dim oData As New clsMarsData
    Const pdfOneLicense As String = "629A-3888-32A9-11A3-AB3F-9FE7-44E7-C694" ' "4B3F-5BFD-016D-B261-0526-6C3E-33D4-2E0E" '

    Public Function IsPrinterReady(PrinterName As String) As Boolean
        Try
            Dim scope As New ManagementScope("\root\cimv2")
            scope.Connect()
            Dim searcher As New ManagementObjectSearcher("select * from Win32_Printer")

            Dim strPrinter As String = String.Empty
            For Each printer As ManagementObject In searcher.[Get]()
                strPrinter = printer("Name").ToString()
                If strPrinter.ToLower() = PrinterName.ToLower() Then
                    Return printer("PrinterStatus").ToString().ToLower().Equals("3")
                End If
            Next
            Return False
        Catch
            Return True
        End Try
    End Function

    Public Sub printPDFByShell(sFile As String, nCopies As Integer, sPrinter As String)
        For i As Integer = 0 To nCopies - 1
            Dim process As New Process()


            process.StartInfo.FileName = sFile
            process.StartInfo.UseShellExecute = True
            process.StartInfo.Verb = "printto"
            process.StartInfo.Arguments = """" & sPrinter & """"
            process.Start()

            process.WaitForInputIdle()
            process.Kill()
        Next
    End Sub

    Public Function PrintPDF(ByVal sFile As String, ByVal nCopies As Integer, _
           ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        Try
            If IsPrinterReady(sPrinter) = False Then
                Throw New Exception("Printer reported as offline by Windows")
            End If

            Dim printByShell As Boolean = clsMarsUI.MainUI.ReadRegistry("PrintUsingShell", 0)


            If printByShell Then
                printPDFByShell(sFile, nCopies, sPrinter)
                Return True
            End If

10:         Dim doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)
20:         Dim pdfprinter As Gnostice.PDFOne.PDFPrinter.PDFPrinter = New Gnostice.PDFOne.PDFPrinter.PDFPrinter
            ' Dim err As Gnostice.PDFOne.PDFPrinter.PrintErrorDelegate
            Dim landscape As Boolean



            '//load the pdf document
30:         doc.Load(sFile)
40:         pdfprinter.LoadDocument(sFile)

50:         If pdfprinter.PDFLoaded = False Then
60:             Throw New Exception("Failed to load document for printing")
            End If

70:         pdfprinter.ReversePageOrder = False

80:         Dim options As Printing.PrinterSettings = New Printing.PrinterSettings

90:         options.PrinterName = sPrinter

100:        options.Copies = nCopies

110:        options.DefaultPageSettings.Margins.Left = 0
120:        options.DefaultPageSettings.Margins.Right = 0
130:        options.DefaultPageSettings.Margins.Top = 0
140:        options.DefaultPageSettings.Margins.Bottom = 0


150:        If nPageFrom = 0 And nPageTo = 0 Then
160:            options.PrintRange = Printing.PrintRange.AllPages

170:            pdfprinter.PrintOptions = options
180:            pdfprinter.PageScaleType = Gnostice.PDFOne.PDFPrinter.PrintScaleType.ReduceToPrintableArea
190:            pdfprinter.Print()


200:        Else
210:            options.PrintRange = Printing.PrintRange.SomePages

220:            If nPageFrom > doc.GetPageCount Then nPageFrom = 1
230:            If nPageTo > doc.GetPageCount Then nPageTo = doc.GetPageCount

240:            options.FromPage = nPageFrom
250:            options.ToPage = nPageTo
260:            pdfprinter.PrintOptions = options
270:            pdfprinter.PageScaleType = Gnostice.PDFOne.PDFPrinter.PrintScaleType.ReduceToPrintableArea
280:            pdfprinter.Print()
            End If


290:        pdfprinter.CloseDocument()
300:        doc.Close()
310:        pdfprinter.Dispose()
320:        doc.Dispose()
        Catch ex As Exception
            Dim message As String = "Error in PrintPDF function: " & ex.Message & vbCrLf & Erl()
            Throw New Exception(message)
        End Try
    End Function


    Public Function PrintPDF_old(ByVal sFile As String, ByVal nCopies As Integer, _
    ByVal sPrinter As String, Optional ByVal nPageFrom As Integer = 0, Optional ByVal nPageTo As Integer = 0) As Boolean
        Dim oPDF As iSED.QuickPDFClass
        Dim nOptions As Integer

        oPDF = New iSED.QuickPDFClass

        oPDF.UnlockKey("B34B74C95F359046BEFB789480980C03")

        If oPDF.Unlocked = 1 Then
            oPDF.LoadFromFile(sFile)

            nOptions = oPDF.PrintOptions(1, 1, ExtractFileName(sFile))

            If nPageFrom = 0 And nPageTo = 0 Then
                nPageFrom = 1
                nPageTo = oPDF.PageCount
            End If

            For I As Integer = 1 To nCopies
                oPDF.PrintDocument(sPrinter, nPageFrom, nPageTo, nOptions)
            Next
        End If

        Return True
    End Function

    Public Function AddBookMarks(ByVal sFile As String, ByVal nPages() As Integer, _
    ByVal sGroups() As String, ByVal sReportName As String) As String
        Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass  'iSED.QuickPDFClass = New iSED.QuickPDFClass

        Dim TargetPage As Integer = 1
        Dim TargetPages() As Integer

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim PDFDest As gtPDFDestinationX

        Dim Outline1 As gtPDFOutlineX

        'Load the Document    
        oPDF.LoadFromFile(sFile)

        'Set the parent bookmark destination settings    
        PDFDest = oPDF.GetDestination

        PDFDest.Page = 1

        PDFDest.DestinationType = TxgtPDFDestinationType.dtFit

        Outline1 = oPDF.CreateNewBookmark(sReportName, PDFDest)

        'load all the pages into the targetpages array
        ReDim TargetPages(nPages.GetUpperBound(0))

        For I As Integer = 0 To nPages.GetUpperBound(0)
            TargetPages(I) = TargetPage

            TargetPage += nPages(I)
        Next

        'create the bookmarks backwards (last one first)
        Dim n As Integer = nPages.GetUpperBound(0)

        Do
            PDFDest.Page = TargetPages(n)
            PDFDest.DestinationType = TxgtPDFDestinationType.dtFit

            Outline1.AddChild(sGroups(n), PDFDest)

            n -= 1
        Loop Until n < 0

        'save the new fle
        Dim sNewFile As String = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf"

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False
        oPDF.SaveToFile(sNewFile)

        oPDF = Nothing

        Return sNewFile

    End Function
    Public Sub setPDFExpiry(pdfFile As String, expiryDate As Date)
        Try
            Using doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)

                doc.Load(pdfFile)

                Dim js As String = My.Resources.finaldate ' "c:\users\steven\desktop\finaldate.js")

                '  js = "app.alert('This document is no longer valid.  Please contact the author'); this.closeDoc(true);"

                js = js.Replace("##/##/####", expiryDate.ToString("MM/dd/yyyy"))

                doc.AddOpenActionJavaScript(js)

                Dim newFile As String = sAppPath & "Output\{" & Guid.NewGuid.ToString & "}.pdf"

                doc.Save(newFile)
                doc.Close()

                System.Threading.Thread.Sleep(1000)

                IO.File.Delete(pdfFile)

                IO.File.Move(newFile, pdfFile)
            End Using
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
        End Try
    End Sub
    Public Function SetPDFSummary(ByVal sTitle As String, ByVal sAuthor As String, ByVal szSubject As String, ByVal sKeywords As String, _
    ByVal sCreated As Date, ByVal sProducer As String, ByVal sFile As String, doExpire As Boolean, expiryDate As Date) As Boolean
        Try

            If doExpire Then
                setPDFExpiry(sFile, expiryDate)
            End If

           
            '            Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass  'iSED.QuickPDFClass = New iSED.QuickPDFClass
            '            Dim sPath As String

            '            oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

            '21:         If sAuthor.Length = 0 And sKeywords.Length = 0 And sProducer.Length = 0 And _
            '          szSubject.Length = 0 And sTitle.Length = 0 Then
            '22:             Return True
            '23:         End If

            '50:         With oPDF
            '60:             .LoadFromFile(sFile)
            '                With .DocInfo
            '70:                 .Author = sAuthor
            '80:                 .Title = sTitle
            '90:                 .Subject = szSubject
            '100:                .Keywords = sKeywords
            '110:                .Producer = sProducer
            '120:                .Creator = "SQL-RD"
            '                    .CreationDate = sCreated
            '121:                .ModDate = sCreated
            '130:            End With
            '            End With

            Dim sPath As String = ""

140:        sPath = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf"

            Using doc As Gnostice.PDFOne.PDFDocument = New Gnostice.PDFOne.PDFDocument(pdfOneLicense)
                doc.Load(sFile)
                doc.DocInfo.Author = sAuthor
                doc.DocInfo.Title = sTitle
                doc.DocInfo.Subject = szSubject
                doc.DocInfo.Keywords = sKeywords
                doc.DocInfo.Creator = "SQL-RD"

                doc.Save(sPath)
            End Using

            '141:        oPDF.ShowSetupDialog = False
            '142:        oPDF.OpenAfterSave = False

            '150:        oPDF.SaveToFile(sPath)

            '160:        oPDF = Nothing

170:        System.IO.File.Copy(sPath, sFile, True)

180:        System.IO.File.Delete(sPath)

            SaveTextToFile(Date.Now & ": PDF Properties set successfully!", sAppPath & "pdf.log", , True)
190:        Return True

200:    Catch ex As Exception
            SaveTextToFile(Date.Now & ": " & ex.Message & " @ " & Erl(), sAppPath & "pdf.log", , True)
            Return False
        End Try

    End Function



    Public Function AddWatermark(ByVal sFile As String, ByVal sMark As String, ByVal nID As Integer) As Object

        If sMark Is Nothing Then Return Nothing

        If sMark.Length = 0 Then Return Nothing

        Dim oPDF As New PDFtoolkitX.gtPDFDocumentXClass

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM StampAttr WHERE ForeignID =" & nID)

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                oPDF.LoadFromFile(sFile)

                oPDF.MeasurementUnit = PDFtoolkitX.TxgtMeasurementUnit.muPoints

                Dim Watermark As PDFtoolkitX.gtTextWatermarkTemplateX

                Watermark = oPDF.GetTextWatermark

                With Watermark
                    .Text = sMark
                    .Angle = 45
                    .Font.Name = "Courier"
                    .Font.Size = 60
                    .Font.Bold = True
                    .TextColor = Convert.ToUInt32(RGB(127, 127, 127))
                    .RenderMode = PDFtoolkitX.TxgtRenderMode.rmxFill
                    .StrokeColor = Convert.ToUInt32(RGB(127, 127, 127))
                    .Overlay = True
                    .HorizPos = TxgtHorizontalPosition.hpCenter
                    .VertPos = TxgtVerticalPosition.vpMiddle
                    .WritingMode = TxgtWrtMode.wmHorizontal
                End With

                oPDF.InsertTextWatermark(Watermark)
            Else
                Dim stampType As String = IsNull(oRs("stamptype").Value)
                Dim overlay As String = IsNull(oRs("overlayunderlay").Value)
                Dim verticalPos As String = IsNull(oRs("verticalpos").Value)
                Dim horizontalPos As String = IsNull(oRs("horizontalpos").Value)
                Dim stampAngle As Integer = IsNull(oRs("stampangle").Value, 0)

                oPDF.LoadFromFile(sFile)

                oPDF.MeasurementUnit = TxgtMeasurementUnit.muPoints

                Select Case stampType
                    Case "TEXT"
                        Dim Watermark As PDFtoolkitX.gtTextWatermarkTemplateX
                        Watermark = oPDF.GetTextWatermark

                        Dim font As String = oRs("fontdetails").Value

                        Dim fontDtls As String = font.Split(";")(0)
                        Dim fontStyle As String = font.Split(";")(1)
                        Dim fontColor As Double = font.Split(";")(2)

                        fontDtls = fontDtls.Replace("[Font:", "").Replace("]", "").Trim

                        For Each s As String In fontDtls.Split(",")
                            If s.Split("=")(0).Trim.ToLower = "name" Then
                                Watermark.Font.Name = s.Split("=")(1)
                            ElseIf s.Split("=")(0).Trim.ToLower = "size" Then
                                Watermark.Font.Size = s.Split("=")(1)
                            End If
                        Next

                        For Each s As String In fontStyle.Split(",")
                            If s.ToLower.Trim = "bold" Then
                                Watermark.Font.Bold = True
                            ElseIf s.ToLower.Trim = "underline" Then
                                Watermark.Font.Underline = True
                            ElseIf s.ToLower.Trim = "strikeout" Then
                                Watermark.Font.Strikethrough = True
                            ElseIf s.ToLower.Trim = "italic" Then
                                Watermark.Font.Italic = True
                            End If
                        Next

                        Dim col As Color = Color.FromKnownColor(fontColor)

                        Watermark.TextColor = Convert.ToUInt32(RGB(col.R, col.G, col.B))
                        Watermark.Text = sMark
                        Watermark.Angle = stampAngle
                        Watermark.RenderMode = TxgtRenderMode.rmxFill
                        Watermark.WritingMode = TxgtWrtMode.wmHorizontal
                        Watermark.StrokeColor = Convert.ToUInt32(fontColor)

                        If overlay.ToLower = "overlay" Then
                            Watermark.Overlay = True
                        End If

                        Select Case verticalPos.ToLower
                            Case "top"
                                Watermark.VertPos = TxgtVerticalPosition.vpTop
                            Case "middle"
                                Watermark.VertPos = TxgtVerticalPosition.vpMiddle
                            Case "bottom"
                                Watermark.VertPos = TxgtVerticalPosition.vpBottom
                        End Select

                        Select Case horizontalPos.ToLower
                            Case "left"
                                Watermark.HorizPos = TxgtHorizontalPosition.hpLeft
                            Case "center"
                                Watermark.HorizPos = TxgtHorizontalPosition.hpCenter
                            Case "right"
                                Watermark.HorizPos = TxgtHorizontalPosition.hpRight
                        End Select

                        oPDF.InsertTextWatermark(Watermark)

                    Case "GRAPHIC"
                        Dim watermark As PDFtoolkitX.gtImagewatermarkTemplateX
                        Dim imageFile As String = oRs("imagefile").Value
                        Dim oparse As New clsMarsParser

                        watermark = oPDF.GetImageWatermark
                        watermark.Angle = stampAngle

                        Select Case verticalPos.ToLower
                            Case "top"
                                watermark.VertPos = TxgtVerticalPosition.vpTop
                            Case "middle"
                                watermark.VertPos = TxgtVerticalPosition.vpMiddle
                            Case "bottom"
                                watermark.VertPos = TxgtVerticalPosition.vpBottom
                        End Select

                        Select Case horizontalPos.ToLower
                            Case "left"
                                watermark.HorizPos = TxgtHorizontalPosition.hpLeft
                            Case "center"
                                watermark.HorizPos = TxgtHorizontalPosition.hpCenter
                            Case "right"
                                watermark.HorizPos = TxgtHorizontalPosition.hpRight
                        End Select

                        If overlay.ToLower = "overlay" Then
                            watermark.Overlay = True
                        End If

                        Dim imgStream As MemoryStream = New MemoryStream
                        Dim picBox As New PictureBox

                        picBox.Image = Image.FromFile(oparse.ParseString(imageFile))

                        If imageFile.ToLower.EndsWith("jpg") = True Or imageFile.ToLower.EndsWith("jpeg") = True Then
                            watermark.ImageType = TxgtImageType.itJPEG

                            picBox.Image.Save(imgStream, System.Drawing.Imaging.ImageFormat.Jpeg)
                        ElseIf imageFile.ToLower.EndsWith("bmp") = True Then
                            watermark.ImageType = TxgtImageType.itBMP

                            picBox.Image.Save(imgStream, System.Drawing.Imaging.ImageFormat.Bmp)
                        ElseIf imageFile.ToLower.EndsWith("ico") Then
                            watermark.ImageType = TxgtImageType.itIcon

                            picBox.Image.Save(imgStream, System.Drawing.Imaging.ImageFormat.Icon)
                        ElseIf imageFile.ToLower.EndsWith("mf") Then
                            watermark.ImageType = TxgtImageType.itMetafile

                            If imageFile.ToLower.EndsWith("wmf") Then
                                picBox.Image.Save(imgStream, System.Drawing.Imaging.ImageFormat.Wmf)
                            ElseIf imageFile.ToLower.EndsWith("emf") Then
                                picBox.Image.Save(imgStream, System.Drawing.Imaging.ImageFormat.Emf)
                            End If
                        End If

                        Dim imgBuffer As Byte() = imgStream.GetBuffer

                        Dim bufSize As Long = imgBuffer.Length

                        Dim bufAddr As Long = System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement(imgBuffer, 0).ToInt64

                        watermark.SetImageByMemory(bufAddr, bufSize, watermark.ImageType)

                        oPDF.InsertImageWatermark(watermark)
                End Select
            End If
        End If

        Dim sNewFile As String = GetDirectory(sFile)

        sNewFile = sNewFile & "{" & clsMarsData.CreateDataID & "}.pdf"

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False
        oPDF.SaveToFile(sNewFile)

        IO.File.Delete(sFile)

        IO.File.Move(sNewFile, sFile)

        oPDF = Nothing

        System.Threading.Thread.Sleep(2500)

        Return sFile
    End Function
    Public Function SetPDFPermissions(ByVal sFile As String, ByVal CanPrint As Integer, ByVal CanCopy As Integer, _
    ByVal CanEdit As Integer, ByVal CanNotes As Integer, ByVal CanFill As Integer, ByVal CanAccess As Integer, _
    ByVal CanAssemble As Integer, ByVal CanPrintFull As Integer, ByVal sOwnerPass As String, ByVal sUserPass As String) As Boolean
        Dim oPDF As New PDFtoolkitX.gtPDFDocumentXClass
        Dim oparse As New clsMarsParser

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        oPDF.LoadFromFile(sFile)

        oPDF.Encryption.Enabled = True
        oPDF.Encryption.Level = PDFtoolkitX.TxgtPDFEncryptionLevel.el128bit
        oPDF.Encryption.OwnerPassword = oparse.ParseString(_DecryptDBValue(sOwnerPass))
        oPDF.Encryption.UserPassword = oparse.ParseString(_DecryptDBValue(sUserPass))

        With oPDF.Encryption.UserPermission
            .AllowCopy = Convert.ToBoolean(CanCopy)
            .AllowAccessibility = Convert.ToBoolean(CanAccess)
            .AllowAnnotation = Convert.ToBoolean(CanNotes)
            .AllowDocAssembly = Convert.ToBoolean(CanAssemble)
            .AllowFormFill = Convert.ToBoolean(CanFill)
            .AllowHighResPrint = Convert.ToBoolean(CanPrintFull)
            .AllowModify = Convert.ToBoolean(CanEdit)
            .AllowPrint = Convert.ToBoolean(CanPrint)
        End With

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False

        Dim sNewFile As String = GetDirectory(sFile) & "{" & clsMarsData.CreateDataID & "}.pdf"

        oPDF.SaveToFile(sNewFile)

        IO.File.Delete(sFile)
        IO.File.Move(sNewFile, sFile)

    End Function


    Public Function MergePDFFiles(ByVal sFiles() As String, ByVal sOutput As String, Optional ByVal AddBookmarks As Boolean = False, _
    Optional ByVal sParentTree As String = "") As String
        Dim oPDF As PDFtoolkitX.gtPDFDocumentXClass = New PDFtoolkitX.gtPDFDocumentXClass

        oPDF.ActivateLicense("E91934A3-91FC-486D-A1F8-8330DA5E3BC6")

        Dim nPages() As Integer
        Dim sGroups() As String

        If AddBookmarks = True Then
            Try
                Dim I As Integer = 0
                Dim pdfDoc As iSED.QuickPDF = New iSED.QuickPDFClass

                pdfDoc.UnlockKey("B34B74C95F359046BEFB789480980C03")

                For Each s As String In sFiles
                    ReDim Preserve nPages(I)
                    ReDim Preserve sGroups(I)

                    pdfDoc.LoadFromFile(s)

                    nPages(I) = pdfDoc.PageCount
                    sGroups(I) = ExtractFileName(s)

                    If sGroups(I).ToString.ToLower.IndexOf(".") > -1 Then
                        sGroups(I) = sGroups(I).ToString.Split(".")(0)
                    End If

                    I += 1
                Next
            Catch : End Try
        End If

        oPDF.MergeDocs(sFiles)

        Dim sMergePath As String = clsMarsReport.m_OutputFolder & "{" & clsMarsData.CreateDataID & "}.pdf" 'sOutput

        oPDF.ShowSetupDialog = False
        oPDF.OpenAfterSave = False

        oPDF.SaveToFile(sMergePath)

        Dim finalPDF As String = clsMarsReport.m_OutputFolder & sOutput
        Try
            If AddBookmarks = True Then
                Dim Temp As String = Me.AddBookMarks(sMergePath, nPages, sGroups, sParentTree)

                IO.File.Copy(Temp, finalPDF, True)
            Else
                IO.File.Copy(sMergePath, finalPDF, True)
            End If
        Catch : End Try

        Return finalPDF
    End Function
    Public Function _ConvertFromPDF(ByVal ToFormat As TgtRenderTypeX, ByVal sIn As String, _
    ByVal sFilename As String, ByVal sDirectory As String, ByVal DeleteOriginal As Boolean) As String
        Dim oPDF As PDF2ManyX.gtPDF2ManyX = New PDF2ManyX.gtPDF2ManyX

        oPDF.ActivateLicense("172550B5-649A-4D82-9B5C-3ADBD014F125")

        If sDirectory.EndsWith("\") = False Then sDirectory &= "\"

        With oPDF
            .InputPDFFile = sIn
            .OutputFileName = sDirectory & sFilename
            .OpenAfterCreate = False

            If ToFormat = TgtRenderTypeX.rtXHTML Or ToFormat = TgtRenderTypeX.rtHTML Then
                .HTMLOptions.PrefAutoScroll = False
                .HTMLOptions.PrefOptimizeForIE = True
                .HTMLOptions.PrefAddTOC = False
            End If

            .RenderDocument(ToFormat)
        End With

        If DeleteOriginal = True Then IO.File.Delete(sIn)

        Dim sExt As String

        Select Case ToFormat
            Case TgtRenderTypeX.rtHTML, TgtRenderTypeX.rtXHTML
                sExt = ".htm"
            Case TgtRenderTypeX.rtTXT
                sExt = ".txt"
            Case TgtRenderTypeX.rtRTF
                sExt = ".rtf"
            Case Else
                sExt = String.Empty
        End Select

        Return oPDF.OutputFileName & sExt
    End Function


End Class
