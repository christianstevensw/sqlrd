﻿Public Class clsFileWriter

    Private m_outputBytes As Byte()
    Public Property outputBytes() As Byte()
        Get
            Return m_outputBytes
        End Get
        Set(ByVal value As Byte())
            m_outputBytes = value
        End Set
    End Property


    Private m_outPut As String
    Public Property sOutput() As String
        Get
            Return m_outPut
        End Get
        Set(ByVal value As String)
            m_outPut = value
        End Set
    End Property



    Private Function createFileFromServer(ByVal outputBytes As Byte(), ByVal sOutput As String)
        'to avoid a resource issue, we need to write the file in 1024 bit chunks
        Dim chunkSize As Long = clsMarsUI.MainUI.ReadRegistry("ChunkSizeBytes", 10485760)

10:     If outputBytes.LongLength > chunkSize Then 'if its greater than 10MB
            clsMarsDebug.writeToDebug("export.debug", "Creating file in " & chunkSize & " byte chunks", True)

20:         Try
                'get the entire byte array
                Dim fstream As IO.FileStream = System.IO.File.Create(sOutput, outputBytes.LongLength)
                Dim byteLength As Integer = outputBytes.LongLength
                'the chunk size we will be writing
                Dim n As Integer = chunkSize 'default 10mb
                Dim I As Integer = 0
                'number of times we will do the writing
                Dim lController As Integer = (byteLength / n) - 1

                'if there is any left thats less than 1024
                Dim final As Integer = byteLength - (lController * n)


                'start writing the chunks
30:             For I = 0 To lController - 1
40:                 fstream.Write(outputBytes, (I * n), n)

50:                 If RunEditor = True Then clsMarsUI.BusyProgress((I / lController) * 100, "Writing to file...", False)

                    clsMarsDebug.writeToDebug("export.debug", "Writing to file (" & I * n & ")", True)
60:             Next

                'if there is any left over then write that out
                clsMarsDebug.writeToDebug("export.debug", "Writing remaining bytes", True)
70:             If final > 0 Then fstream.Write(outputBytes, (byteLength - final), final)
71:
                'close and dispose the file stream
                Try
                    clsMarsDebug.writeToDebug("export.debug", "Closing file stream", True)
80:                 fstream.Close()
                Catch : End Try

                Try
                    clsMarsDebug.writeToDebug("export.debug", "Disposing off file stream", True)
                    fstream.Dispose()
                    System.GC.Collect()
                Catch : End Try
90:
100:        Catch ex As Exception
110:            _ErrorHandle("Error writing byte error in chunks. SQL-RD will now attempt to use write-once method. " & vbCrLf & ex.Message & vbCrLf & Erl(), Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
120:            Dim fstream As IO.FileStream = System.IO.File.Create(sOutput, outputBytes.Length)

130:            Using fstream
140:                fstream.Write(outputBytes, 0, outputBytes.LongLength)
150:                fstream.Close()
160:                fstream.Dispose()
                End Using
            End Try
170:    Else
            clsMarsDebug.writeToDebug("export.debug", "Creating file using write-once method", True)

180:        With System.IO.File.Create(sOutput, outputBytes.LongLength)
190:            .Write(outputBytes, 0, outputBytes.LongLength)
200:            .Close()
210:            .Dispose()
            End With
        End If

    End Function
End Class
