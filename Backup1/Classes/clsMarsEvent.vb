Imports System.IO
Imports Quiksoft.EasyMail.POP3
Imports Quiksoft.EasyMail.Parse
Imports Quiksoft.EasyMail.SSL
Imports Quiksoft.EasyMail.IMAP4
Imports System.Runtime.Serialization.Formatters.Binary

Friend Class clsMarsEvent
    Dim oData As New clsMarsData
    Shared nPointers() As Object
    Shared matchedMail As ArrayList
    Shared cleanerFiles As New ArrayList
    Shared cleanerFS As New ArrayList
    Shared pointer As Integer
    Public Shared m_resultsTable As Hashtable
    Public Shared currentEventID As Integer = 0
    Public Shared m_StartTime As Date = Now.Date
    Public m_HistoryID As Integer = 0
    Public Shared m_EventDataCache() As DataTable
    Public Shared m_portDataCache As DataTable

    Public Enum m_mailServerType As Integer
        POP3 = 0
        IMAP4 = 1
    End Enum
    Public Shared ReadOnly Property m_eventID() As Integer
        Get
            Return currentEventID
        End Get
    End Property

    Public Shared ReadOnly Property m_CachedDataPath() As String
        Get
            Dim value As String

            value = clsMarsUI.MainUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Data\")

            If value.EndsWith("\") = False Then value &= "\"

            Return value
        End Get
    End Property

    Public Shared Property m_Pointer() As Integer
        Get
            Return pointer
        End Get
        Set(ByVal value As Integer)
            pointer = value
        End Set
    End Property

    Public Shared ReadOnly Property m_foundFilesTable() As DataTable
        Get
            Dim dt As DataTable = New DataTable

            dt.Columns.Add("PointerID", GetType(System.Int32))
            dt.Columns.Add("FileName")
            dt.Columns.Add("FolderName")
            dt.Columns.Add("FullPath")
            dt.Columns.Add("DateLastModified")
            dt.Columns.Add("DateCreated")

            dt.PrimaryKey = New DataColumn() {dt.Columns(0)}

            Return dt
        End Get
    End Property

    Public Shared Function getOwner(ByVal eventID As Integer) As String
        Dim SQL As String = "SELECT * FROM eventAttr6 WHERE eventid = " & eventID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim owner As String = IsNull(oRs("owner").Value, "")
            oRs.Close()
            oRs = Nothing

            Return owner
        Else
            Return ""
        End If
    End Function
    Public Shared Function isEventLocked(ByVal id As Integer) As Boolean

        Dim locked As Boolean = False


        Dim SQL As String = "SELECT locked FROM eventattr6 WHERE eventid = " & id
        Dim rs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If rs IsNot Nothing AndAlso rs.EOF = False Then
            locked = IsNull(rs(0).Value, False)

            rs.Close()

            rs = Nothing
        End If

        Return locked
    End Function
    Public Shared Function areEventsLocked() As Boolean
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM eventattr6 WHERE locked = 1")

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim count As Integer = oRs(0).Value

            oRs.Close()

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function
    Public Shared Function moveintoEventPackage(ByVal packID As Integer, ByVal eventID() As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim orderID As Integer = 0

        SQL = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & packID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            orderID = 0
        ElseIf oRs.EOF = True Then
            orderID = 0
        Else
            orderID = IsNull(oRs(0).Value, 0)
        End If

        orderID += 1

        For Each n As Integer In eventID
            SQL = "UPDATE EventAttr6 SET " & _
            "Parent = 0," & _
            "PackID =" & packID & "," & _
            "PackOrderID =" & orderID & _
            " WHERE EventID = " & n

            clsMarsData.WriteData(SQL)

            orderID += 1
        Next
    End Function
    Public Shared Function doesDataMatchCriteria(data As String, searchString As String, matchingType As Integer) As Boolean
        If matchingType = 1 Then 'ALL
            Dim mismatch As Boolean
            Dim criterias As String() = searchString.Split(PD)

            '//we exit the loop if there is any mismatch in the criteria
            For Each criteria As String In criterias
                If criteria = "" Then Continue For

                Dim operat As String = criteria.Split("::")(0)
                Dim value As String = criteria.Split("::")(2)

                Select Case operat.ToLower
                    Case "starts with"
                        If data.ToLower.StartsWith(value.ToLower) = False Then
                            mismatch = True
                            Return False
                        End If
                    Case "ends with"
                        If data.ToLower.EndsWith(value.ToLower) = False Then
                            mismatch = True
                            Return False
                        End If
                    Case "contains"
                        If data.ToLower.Contains(value.ToLower) = False Then
                            mismatch = True
                            Return False
                        End If
                    Case "does not contain"
                        If data.ToLower.Contains(value.ToLower) = True Then
                            mismatch = True
                            Return False
                        End If
                End Select
            Next

            Return True
        Else
            Dim criterias As String() = searchString.Split(PD)

            '//we exit the loop if there is any mismatch in the criteria
            For Each criteria As String In criterias
                If criteria = "" Then Continue For

                Dim operat As String = criteria.Split("::")(0)
                Dim value As String = criteria.Split("::")(2)

                Select Case operat.ToLower
                    Case "starts with"
                        If data.ToLower.StartsWith(value.ToLower) = True Then
                            Return True
                        End If
                    Case "ends with"
                        If data.ToLower.EndsWith(value.ToLower) = True Then
                            Return True
                        End If
                    Case "contains"
                        If data.ToLower.Contains(value.ToLower) = True Then
                            Return True
                        End If
                    Case "does not contain"
                        If data.ToLower.Contains(value.ToLower) = False Then
                            Return True
                        End If
                End Select
            Next

            Return False
        End If
    End Function
    Public Shared Function hasDataBeenReceived(conditionID As Integer, searchString As String, serverPort As Integer, serverIP As String, matchType As Integer) As Object()
        Dim SQL As String = "SELECT * FROM portlistnerdataattr WHERE serverport =" & serverPort & " AND serverip ='" & SQLPrepare(serverIP) & "' ORDER BY entryDate ASC"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim results() As Object
        Dim exclusionList As ArrayList = New ArrayList

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                Return Nothing '//as no data is received
            Else
                Dim count As Integer

                Do While oRs.EOF = False
                    Dim dataRecvd As String = oRs("datarecvd").Value
                    Dim addToCollection As Boolean = False

                    If searchString <> "" And searchString.Length > "CONTAINS".Length Then
                        If doesDataMatchCriteria(dataRecvd, searchString, matchType) Then
                            addToCollection = True
                        Else
                            exclusionList.Add(oRs("entryid").Value)
                            addToCollection = False
                        End If
                    Else
                        addToCollection = True
                    End If

                    If addToCollection Then
                        Dim newr As DataRow = m_portDataCache.Rows.Add
                        ReDim Preserve results(count)

                        newr("entryid") = oRs("entryid").Value
                        newr("entrydate") = oRs("entrydate").Value
                        newr("datarecvd") = oRs("datarecvd").Value
                        results(count) = oRs("entryid").Value
                        count += 1
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = Nothing

                If results IsNot Nothing Then
                    For Each id As String In results
                        clsMarsData.WriteData("DELETE FROM portlistnerdataattr WHERE entryid ='" & id & "'")
                    Next
                End If

                'For Each id As String In exclusionList
                '    clsMarsData.WriteData("DELETE FROM portlistnerdataattr WHERE entryid ='" & id & "'")
                'Next

                Return results

            End If
        End If
    End Function
    Public Shared Function DoesRecordExists(ByVal name As String, ByVal query As String, ByVal connectionString As String, _
           ByVal keyColumn As String, ByVal selectType As String, ByVal cacheFolderName As String, Optional ByVal doProcess As Boolean = True) As Object()


10:     Dim dsn As String = connectionString.Split("|")(0)
20:     Dim username As String = connectionString.Split("|")(1)
30:     Dim password As String = connectionString.Split("|")(2)
40:     Dim orsOld As ADODB.Recordset
50:     Dim dataPath As String
60:     Dim results As Object() = Nothing
70:     Dim I As Integer = 0
80:     Dim x As Integer = 1
90:     Dim oCon As ADODB.Connection = New ADODB.Connection
100:    Dim oTest As ADODB.Recordset = New ADODB.Recordset

110:    Try
            query = clsMarsParser.Parser.ParseString(query)

120:        If keyColumn.IndexOf(".") > -1 Then
130:            keyColumn = keyColumn.Split(".")(1)
140:        End If

141:        clsMarsDebug.writeToDebug("doesrecordexist.log", "Condition Name: " & name, True)
            clsMarsDebug.writeToDebug("doesrecordexist.log", "Query: " & query, True)

150:        Select Case selectType
                Case "NEW"
160:                Dim newRecordFound As Boolean = False
                    Dim openRetry As Integer = 0

                    oCon.ConnectionTimeout = 0
                    oCon.CommandTimeout = 0

                    oCon.Open(dsn, username, _DecryptDBValue(password))

                    Try
openRetry1:
180:                    oTest.Open(query, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
                    Catch ex As Exception
                        If ex.Message.ToLower.Contains("deadlock") And openRetry < 4 Then
                            openRetry = +1
                            System.Threading.Thread.Sleep(1000)
                            GoTo openRetry1
                        Else
                            Throw ex
                        End If
                    End Try

190:                dataPath = m_CachedDataPath & "dbe_" & cacheFolderName 'name.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

200:                dataPath &= "\datacache.xml"

210:                If oTest IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                        Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

220:                    clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oTest)

230:                    If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
240:                        clsMarsEvent.m_EventDataCache(ubound).TableName = name
                        End If
                    End If

250:                If IO.File.Exists(dataPath) = True And keyColumn.Length > 0 Then
260:                    orsOld = clsMarsData.GetXML(dataPath)

261:                    oTest.Close()
262:                    oTest.Open()

270:                    Do While oTest.EOF = False

280:                        clsMarsUI.MainUI.BusyProgress((x / oTest.RecordCount) * 100, "Checking records...")
290:                        x += 1

300:                        orsOld.Filter = ""

310:                        Try
320:                            If oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adDate Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adDBDate Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adLongVarChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adLongVarWChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adVarChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adVarWChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adWChar Then

330:                                orsOld.Filter = keyColumn & "='" & SQLPrepare(oTest.Fields(keyColumn).Value) & "'"
340:                            Else
                                    Dim sValue As String = IsNull(oTest.Fields(keyColumn).Value, 0)
                                    Dim sFilter As String = keyColumn & "=" & sValue

350:                                orsOld.Filter = sFilter 'keyColumn & "='" & SQLPrepare(oTest.Fields(keyColumn).Value) & "'"
360:                            End If
370:                        Catch e As Exception
380:                            GoTo SkipRecord
                            End Try

390:                        If orsOld.EOF = True Then

400:                            newRecordFound = True

410:                            ReDim Preserve results(I)

420:                            results(I) = oTest(keyColumn).Value

430:                            I += 1
440:                        End If
SkipRecord:
450:                        oTest.MoveNext()
460:                    Loop

470:                End If


480:                If newRecordFound = True Or IO.File.Exists(dataPath) = False Then
490:                    clsMarsData.CreateXML(oTest, dataPath)
500:                End If

510:                oCon.Close()
520:            Case "ALL"

                    If doProcess = False Then Return Nothing

                    oCon.ConnectionTimeout = 0
                    oCon.CommandTimeout = 0
530:                oCon.Open(dsn, username, _DecryptDBValue(password))

                    Dim openRetry As Integer = 0

                    Try
openRetry2:             oTest.Open(query, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
                    Catch ex As Exception
                        If ex.Message.ToLower.Contains("deadlock") And openRetry < 4 Then
                            clsMarsDebug.writeToDebug("doesrecordexists.log", "Retrying opening recordset", True)
                            openRetry = +1
                            System.Threading.Thread.Sleep(1000)
                            GoTo openRetry2
                        Else
                            Throw ex
                        End If
                    End Try

550:                If oTest IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                        Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

560:                    clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oTest)

570:                    If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
580:                        clsMarsEvent.m_EventDataCache(ubound).TableName = name
                        End If
                    End If

581:                oTest.Close()
582:                oTest.Open()

590:                Do While oTest.EOF = False

600:                    ReDim Preserve results(I)
610:                    Dim temp As String = keyColumn

620:                    If keyColumn.IndexOf(".") > -1 Then
630:                        temp = keyColumn.Split(".")(1)
640:                    End If

650:                    results(I) = oTest(temp).Value

660:                    I += 1

670:                    oTest.MoveNext()
680:                Loop

690:                oTest.Close()

700:                oCon.Close()
            End Select

710:        Return results
720:    Catch ex As Exception
730:        _ErrorHandle(gScheduleName & " " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "")
740:        Return Nothing
        End Try
    End Function

    Private Sub _GetMailValues(ByVal msgObj As Quiksoft.EasyMail.Parse.EmailMessage)
        Try
            gSubject = msgObj.Subject

            For Each ob As Quiksoft.EasyMail.Parse.BodyPart In msgObj.BodyParts
                gBody &= ob.BodyText
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.From
                gFrom &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.To
                gTo &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.CC
                gCc &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.BCC
                gBcc &= oA.EmailAddress & ";"
            Next
        Catch : End Try
    End Sub

    Private Shared Sub ForwardMail(ByVal Messages As ArrayList, ByVal conditionID As Integer, ByVal emailData As DataTable)
        Dim nRetry As Integer = 0

RETRY:
10:     Try
            Dim msg As Chilkat.Email
            Dim SQL As String
            Dim oRs As ADODB.Recordset
20:         Dim oParse As clsMarsParser = New clsMarsParser
            Dim sPath As String
30:         Dim oMsg As New clsMarsMessaging
            Dim pointer As Integer = 0

40:         sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

50:         If sPath.EndsWith("\") = False Then sPath &= "\"

60:         sPath &= "Downloaded Emails\"

70:         SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

80:         oRs = clsMarsData.GetData(SQL)

90:         If oRs Is Nothing Then Return

100:        If oRs.EOF = True Then Return

            Dim forwardMail As Boolean = False

110:        Try
120:            forwardMail = Convert.ToBoolean(oRs("forwardmail").Value)
            Catch : End Try

130:        If forwardMail = False Then Return

140:        oRs.Close()

150:        SQL = "SELECT * FROM ForwardMailAttr WHERE ConditionID =" & conditionID

160:        oRs = clsMarsData.GetData(SQL)

170:        If oRs Is Nothing Then Return

180:        If oRs.EOF = True Then Return

            Dim sendTo, sendCC, sendBCC, mailSubject, mailBody, attachments, mailFormat, mailServer, isenderName, isenderAddress As String
            Dim attachMsg As Boolean


190:        clsMarsUI.MainUI.BusyProgress(60, "Forwading emails...")

200:        For Each msg In Messages
                Dim senderAddress As String = msg.FromAddress
                Dim senderName As String = msg.FromName
                Dim sentDate As String = msg.LocalDate.ToString("dd MMMM yyyy HH:mm")
                Dim sentTo, sentCC As String
                Dim fullBody As String = ""
                Dim fullSubject As String = ""
                Dim newMsg As Chilkat.Email = msg

                Try
                    isenderName = oParse.ParseString(IsNull(oRs("sendername").Value, ""))
                    isenderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value, ""))
                    attachMsg = IsNull(oRs("attachmsg").Value, 0)
                Catch : End Try

                Dim msgAttachFile As String = ""

                Try
                    If attachMsg = True Then
                        msgAttachFile = clsMarsReport.m_OutputFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".eml"

                        Dim ok As Boolean = msg.SaveEml(msgAttachFile)

                        If ok = False Then msgAttachFile = ""
                    End If
                Catch : End Try

210:            clsMarsEvent.m_Pointer = pointer

                'initialise the data
220:            sendTo = oParse.ParseString(IsNull(oRs("sendto").Value))
230:            sendCC = oParse.ParseString(IsNull(oRs("sendcc").Value))
240:            sendBCC = oParse.ParseString(IsNull(oRs("sendbcc").Value))
250:            mailSubject = oParse.ParseString(IsNull(oRs("mailsubject").Value))
260:            mailBody = oParse.ParseString(IsNull(oRs("mailbody").Value))

                'mailBody = mailBody.Replace("UTF-8", "ASCII")

270:            attachments = oParse.ParseString(IsNull(oRs("attachments").Value))
280:            mailFormat = IsNull(oRs("mailformat").Value, "HTML")
290:            mailServer = IsNull(oRs("mailserver").Value)

300:            sendTo = oMsg.ResolveEmailAddress(sendTo)
310:            sendCC = oMsg.ResolveEmailAddress(sendCC)
320:            sendBCC = oMsg.ResolveEmailAddress(sendBCC)

330:            fullBody = mailBody
340:            fullSubject = mailSubject

350:            sentTo = ""

360:            For n As Integer = 0 To msg.NumTo - 1
370:                sentTo &= msg.GetTo(n) & ";"
380:            Next

390:            sentCC = ""

400:            For n As Integer = 0 To msg.NumCC - 1
410:                sentCC &= msg.GetCC(n) & ";"
420:            Next

                Dim forwardSubject As String = msg.Subject
                Dim forwardBody As String = ""

                Dim header As String = ""

430:            header = "-----Original Message-----" & vbCrLf & _
                      "From: " & senderName & " [" & senderAddress & "]" & vbCrLf & _
                      "Sent: " & sentDate & vbCrLf & _
                      "To: " & sentTo & vbCrLf & _
                      "Cc: " & sentCC & vbCrLf & _
                      "Subject: " & forwardSubject & vbCrLf & vbCrLf

                If fullSubject = "" Then newMsg.Subject = "FW: " & msg.Subject Else newMsg.Subject = fullSubject

440:            fullBody &= vbCrLf & vbCrLf & header

                Dim htmlBody As String = msg.GetHtmlBody
                Dim textBody As String = msg.GetPlainTextBody

441:            If textBody Is Nothing Then textBody = newMsg.Body

450:            If (MailType = gMailType.SMTP Or MailType = gMailType.SQLRDMAIL) And mailFormat = "HTML" And htmlBody IsNot Nothing Then
460:                header = "<html><body><font face='Arial' size='2'>" & fullBody.Replace(vbCrLf, "<BR>") & "</body></html>"

470:                newMsg.SetHtmlBody(header & htmlBody)
                    ' newMsg.AddHtmlAlternativeBody(header & htmlBody)
480:            End If

490:            Dim currentBody As String = textBody
491:            Dim allBody As String

493:            allBody = fullBody & vbCrLf & currentBody

494:            newMsg.Body = allBody


500:            newMsg.ClearBcc()
510:            newMsg.ClearCC()
520:            newMsg.ClearTo()

530:            For Each s As String In sendTo.Split(";")
540:                If s <> "" Then
550:                    newMsg.AddTo(s, s)
                    End If
560:            Next

570:            For Each s As String In sendCC.Split(";")
580:                If s <> "" Then
590:                    newMsg.AddCC(s, s)
                    End If
600:            Next

610:            For Each s As String In sendBCC.Split(";")
620:                If s <> "" Then
630:                    newMsg.AddBcc(s, s)
                    End If
640:            Next

                Dim numattach As Integer = 0

650:            If attachments IsNot Nothing Then
660:                For Each s As String In attachments.Split(";")
670:                    If s <> "" Then newMsg.AddFileAttachment(s)
680:                Next

690:                numattach = attachments.Split(";").GetUpperBound(0)
                End If

                If isenderName <> "" Then newMsg.FromName = isenderName
                If isenderAddress <> "" Then newMsg.FromAddress = isenderAddress

                If msgAttachFile <> "" Then
                    newMsg.AddFileAttachment(msgAttachFile)
                End If


700:            Select Case MailType
                    Case gMailType.NONE
710:                    Throw New Exception("No messaging configuration found. Please set up your messaging in Options - Messaging")
720:                Case gMailType.SMTP, gMailType.SQLRDMAIL
                        Dim oSender As clsMarsMessaging = New clsMarsMessaging

730:                    oSender.SendSMTP(sendTo, fullSubject, fullBody, "Single", "", numattach, _
                              , sendCC, sendBCC, gScheduleName, False, mailFormat, True, True, mailFormat, mailServer, , , , newMsg)
740:                Case gMailType.MAPI
750:                    clsMarsMessaging.SendMAPI(sendTo, fullSubject, fullBody, "Single", "", numattach, , sendCC, sendBCC, _
                              False, "TEXT", gScheduleName, True)
760:                Case gMailType.GROUPWISE
770:                    clsMarsMessaging.SendGROUPWISE(sendTo, sendCC, sendBCC, fullSubject, fullBody, "", "Single", , True _
                              , , , , numattach, , mailFormat)
                End Select

780:            pointer += 1
790:        Next
800:    Catch ex As Exception
810:        If ex.Message.ToLower.IndexOf("output stream") > -1 Then
820:            If nRetry < 5 Then
830:                System.Threading.Thread.Sleep(5000)
840:                nRetry += 1
850:                GoTo RETRY
                End If
            End If

860:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub
    Private Shared Sub RedirectMail(ByVal Messages As ArrayList, ByVal ConditionID As Integer, ByVal emailData As DataTable)
10:     Try
            Dim msg As Chilkat.Email
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim sPath As String = ""
20:         Dim oMsg As New clsMarsMessaging
            Dim pointer As Integer = 0

30:         SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & ConditionID

40:         oRs = clsMarsData.GetData(SQL)

50:         If oRs Is Nothing Then Return

60:         If oRs.EOF = True Then Return

            Dim RedirectTo As String
            Dim RedirectMail As Boolean = False

70:         Try
80:             RedirectMail = Convert.ToBoolean(oRs("redirectmail").Value)
            Catch : End Try

90:         If RedirectMail = False Then Return

100:        clsMarsUI.MainUI.BusyProgress(60, "Redirecting emails...")

110:        RedirectTo = IsNull(oRs("redirectto").Value, "")

120:        If RedirectTo.Length = 0 Then Return

130:        Dim oParse As clsMarsParser = New clsMarsParser

140:        sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

150:        If sPath.EndsWith("\") = False Then sPath &= "\"

160:        sPath &= "Downloaded Emails\"

170:        For Each msg In Messages
180:            clsMarsEvent.m_Pointer = pointer

                Dim newMsg As Chilkat.Email

                newMsg = msg

                newMsg.ClearTo()
                newMsg.ClearBcc()
                newMsg.ClearCC()

400:            RedirectTo = oParse.ParseString(RedirectTo)
410:            RedirectTo = oMsg.ResolveEmailAddress(RedirectTo)

420:            For Each s As String In RedirectTo.Split(";")
430:                If s <> "" Then
440:                    newMsg.AddTo(s, s)
                    End If
450:            Next


560:            If MailType = gMailType.MAPI Or MailType = gMailType.GROUPWISE Or MailType = gMailType.NONE Then
570:                Throw New Exception("The selected SQL-RD mail type does not support mail redirection. Please use SMTP ot SQL-RDMail")
580:            Else
                    Dim objSender As clsMarsMessaging = New clsMarsMessaging
590:                objSender.SendSMTP(RedirectTo, "", "", "Single", 0, msg.NumAttachments, _
                          , , , gScheduleName, False, "", True, True, "", , , newMsg.FromName, newMsg.FromAddress, newMsg)
                End If

600:            pointer += 1
610:        Next
620:    Catch ex As Exception
630:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub

    Public Shared Function DoesEmailExist(ByVal mailserver As String _
           , ByVal username As String, ByVal password As String, ByVal Download As Boolean, _
           ByVal searchString As String, ByVal AnyAll As String, ByRef emailData As DataTable, _
           ByVal ConditionID As Integer, ByVal port As Integer, _
           ByVal useSSL As Boolean, ByVal serverType As String, ByVal imapBox As String, _
           ByVal saveAttachments As Boolean, ByVal matchExactCriteria As Boolean) As Boolean

10:     Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
20:     Quiksoft.EasyMail.Parse.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
30:     Quiksoft.EasyMail.IMAP4.License.Key = QuikSoftLicense ' "ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
40:     Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Developer)/13047341F666623200000F00262C5AAE73"

        Dim pop3 As Chilkat.MailMan
        Dim oIMAP4 As Chilkat.Imap
50:     Dim SSL As New SSL
        Dim sReturn(6) As Object
60:     Dim oData As New clsMarsData
        Dim msgCount As Integer
        Dim sPath As String
        Dim MarkForDelete As Boolean
        Dim downloadPath As String
        Dim nPointer As Integer = 0
        Dim emailFound As Boolean = False
        Dim X As Integer = 0
        Dim bodyParts As Hashtable
        Dim retryCount As Integer = 0
        Dim attachmentsPath As String = sAppPath & "Output\MailAttachments\"

70:     cleanerFiles = New ArrayList
80:     cleanerFS = New ArrayList
90:     matchedMail = New ArrayList

100:    Try


            'Message will be stored in files
            Dim fs As FileStream

110:        sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

120:        If sPath.EndsWith("\") = False Then sPath &= "\"

130:        sPath &= "Downloaded Emails\"

140:        If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

150:        If IO.Directory.Exists(sPath) = False Then
160:            IO.Directory.CreateDirectory(sPath)
            End If

170:        clsMarsUI.MainUI.BusyProgress(30, "Connecting to mail server...")

RETRY:

180:        If serverType = "POP3" Then
190:            pop3 = New Chilkat.MailMan

200:            pop3.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")
                'Connect to the POP3 mail server, if user has provided custom pop3 port then use that instead
210:            If port = 0 Then
220:                pop3.MailHost = mailserver
                    'pop3.Connect(mailserver)
230:            Else
240:                If useSSL = True Then
250:                    pop3.PopSsl = True
260:                    pop3.MailPort = port
270:                    pop3.MailHost = mailserver
                        'pop3.Connect(mailserver, port, SSL.GetInterface)
280:                Else
290:                    pop3.MailPort = port
300:                    pop3.MailHost = mailserver
                        'pop3.Connect(mailserver, port)
                    End If
                End If

310:            clsMarsUI.MainUI.BusyProgress(60, "Logging into mail server...")

320:            pop3.PopUsername = username
330:            pop3.PopPassword = password

                Dim ok As Boolean
                Dim retry As Integer = 0

340:            Do
350:                ok = pop3.VerifyPopConnection()

360:                If ok = False Then System.Threading.Thread.Sleep(3000)

370:                retry += 1
380:            Loop Until ok = True Or retry >= 3

390:            If ok = False Then
400:                Throw New Exception(pop3.LastErrorText)
                End If

410:            retry = 0

420:            Do
430:                ok = pop3.VerifyPopLogin

440:                If ok = False Then System.Threading.Thread.Sleep(3000)

450:                retry += 1
460:            Loop Until ok = True Or retry >= 3

470:            If ok = False Then
480:                Throw New Exception(pop3.LastErrorText)
                End If

490:            clsMarsUI.BusyProgress(70, "Downloading emails from server...")

                Dim bundle As Chilkat.EmailBundle = pop3.CopyMail


500:            If bundle Is Nothing Then
510:                Throw New Exception(pop3.LastErrorText)
                End If
                'pop3.Login(username, password, AuthMode.Plain)

520:            msgCount = bundle.MessageCount - 1 'pop3.GetMessageCount

                Dim msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, msgAttachments As String
                Dim msgDate As String = ""
                Dim msgDatesent As Date

530:            For I As Integer = 0 To msgCount
                    Dim email As Chilkat.Email

540:                email = bundle.GetEmail(I)
550:                msgFrom = ""
560:                msgTo = ""
570:                msgCc = ""
580:                msgBcc = ""
590:                msgSubject = ""
600:                msgBody = ""
610:                msgAttachments = ""
620:                msgDatesent = Nothing

630:                bodyParts = New Hashtable

640:                With bodyParts
650:                    .Add("TEXT", "")
660:                    .Add("HTML", "")
                    End With

670:                If msgCount > 0 Then clsMarsUI.MainUI.BusyProgress((I / msgCount) * 100, "Parsing emails..." & CType((I / msgCount) * 100, Integer) & "%")

680:                MarkForDelete = False

690:                msgFrom = email.From

700:                For n As Integer = 0 To email.NumTo - 1
710:                    msgTo &= email.GetTo(n) & ";"
720:                Next

730:                For n As Integer = 0 To email.NumCC - 1
740:                    msgCc &= email.GetCC(n) & ";"
750:                Next

760:                For n As Integer = 0 To email.NumBcc - 1
770:                    msgBcc &= email.GetBcc(n) & ";"
780:                Next

790:                msgSubject = email.Subject
                    msgDate = ConDateTime(email.LocalDate)

800:                Dim oClean As New clsMarsParser

810:                If email.HasHtmlBody = True Then
820:                    bodyParts.Item("HTML") = oClean.CleanString(email.GetHtmlBody)
830:                    msgBody &= oClean.CleanString(email.GetHtmlBody)
                    End If

840:                If email.HasPlainTextBody Then
850:                    bodyParts.Item("TEXT") = oClean.CleanString(email.GetPlainTextBody)
860:                    msgBody &= oClean.CleanString(email.GetPlainTextBody)
                    End If

870:                If searchEmail(searchString, msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, AnyAll, msgDate, matchExactCriteria) = True Then
880:                    emailFound = True

890:                    matchedMail.Add(email)

                        Dim row As DataRow

900:                    row = emailData.Rows.Add

910:                    row.Item("MsgID") = I 'msg.MessageID
920:                    row.Item("MsgFrom") = msgFrom
930:                    row.Item("MsgTo") = msgTo
940:                    row.Item("MsgCc") = msgCc
950:                    row.Item("MsgBcc") = msgBcc
960:                    row.Item("MsgSubject") = msgSubject
970:                    row.Item("msgBody") = msgBody
980:                    row.Item("msgDate") = email.LocalDate
990:                    row.Item("msgTEXT") = bodyParts.Item("TEXT")
1000:                   row.Item("msgHTML") = bodyParts.Item("HTML")
1010:
                        'If saveAttachments = True Then

1020:                   attachmentsPath = attachmentsPath & Date.Now.ToString("yyyyMMddHHmmss") & "\"

1030:                   clsMarsParser.Parser.ParseDirectory(attachmentsPath)

1040:                   If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

1050:                   email.SaveAllAttachments(attachmentsPath)

1060:                   For n As Integer = 0 To email.NumAttachments - 1
                            Dim att As String = email.GetAttachmentFilename(n)

                            Dim fullFile As String = attachmentsPath & att

1070:                       msgAttachments &= fullFile & ";"
1080:                   Next

1090:                   For n As Integer = 0 To email.NumRelatedItems - 1
                            Dim fullfile As String = attachmentsPath & email.GetRelatedFilename(I)

1100:                       email.SaveRelatedItem(I, attachmentsPath)
1110:                   Next

1120:                   If msgAttachments.EndsWith(";") Then msgAttachments = msgAttachments.Remove(msgAttachments.Length - 1, 1)
                        'End If

1130:                   row.Item("Attachments") = msgAttachments
1140:                   row.Item("AttachmentsPath") = attachmentsPath

1150:                   If Download = True Then
1160:                       ReDim Preserve nPointers(nPointer)

1170:                       nPointers(nPointer) = email.Uidl

1180:                       nPointer += 1
                        End If
                    End If
                    'cleanerFiles.Add(downloadPath)
                    'cleanerFS.Add(fs)
1190:           Next

1200:           Try
1210:               bundle.Dispose()
1220:
                Catch : End Try
1230:       Else
                Dim I As Integer = 1
                Dim ok As Boolean
                Dim retry As Integer = 0

1240:           oIMAP4 = New Chilkat.Imap

1250:           oIMAP4.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")
                'Connect to the IMAP4 mail server, if user has provided custom pop3 port then use that instead
1260:           If port > 0 Then
1290:               If useSSL = True Then
1300:                   oIMAP4.Ssl = True
1310:                   oIMAP4.Port = port
1330:               Else
1340:                   oIMAP4.Port = port
1350:               End If
                End If

                Do
1351:               ok = oIMAP4.Connect(mailserver)

1352:               If ok = False Then System.Threading.Thread.Sleep(3000)

1353:               retry += 1
1354:           Loop Until ok = True Or retry > 3

1360:           If ok = False Then
1370:               Throw New Exception(oIMAP4.LastErrorText)
                End If

1380:           clsMarsUI.MainUI.BusyProgress(60, "Logging into mail server...")

                'Log onto the IMAP4 mail server
                retry = 0

1390:           Do
1400:               ok = oIMAP4.Login(username, password)

1410:               If ok = False Then System.Threading.Thread.Sleep(3000)

1420:               retry += 1
1430:           Loop Until ok = True Or retry >= 3

1440:           If ok = False Then
1450:               Throw New Exception(oIMAP4.LastErrorText)
                End If

#If DEBUG Then
                oIMAP4.PeekMode = True
#End If
1460:           ok = oIMAP4.SelectMailbox(imapBox)

1470:           If ok = False Then
1480:               Throw New Exception(oIMAP4.LastErrorText)
                End If

1490:           clsMarsUI.BusyProgress(70, "Downloading emails from server...")

                Dim result As Chilkat.MessageSet = oIMAP4.Search("UNSEEN", True)
                Dim msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, msgAttachments As String
                Dim msgDate As String = ""
                Dim msgDatesent As Date

1500:           If result.Count > 0 Then
                    Dim envs As Chilkat.EmailBundle = oIMAP4.FetchBundle(result)

1510:               For c As Integer = 0 To envs.MessageCount - 1
                        Dim email As Chilkat.Email = envs.GetEmail(c)

1520:                   If envs.MessageCount > 0 Then clsMarsUI.MainUI.BusyProgress((I / envs.MessageCount) * 100, "Parsing emails..." & CType((I / envs.MessageCount) * 100, Integer) & "%")

1530:                   msgFrom = ""
1540:                   msgTo = ""
1550:                   msgCc = ""
1560:                   msgBcc = ""
1570:                   msgSubject = ""
1580:                   msgBody = ""
1590:                   msgAttachments = ""
1600:                   msgDatesent = Nothing

1610:                   bodyParts = New Hashtable

1620:                   With bodyParts
1630:                       .Add("TEXT", "")
1640:                       .Add("HTML", "")
                        End With

1650:                   MarkForDelete = False

1660:                   msgFrom = email.From

1670:                   For n As Integer = 0 To email.NumTo - 1
1680:                       msgTo &= email.GetTo(n) & ";"
1690:                   Next

1700:                   For n As Integer = 0 To email.NumCC - 1
1710:                       msgCc &= email.GetCC(n) & ";"
1720:                   Next

1730:                   For n As Integer = 0 To email.NumBcc - 1
1740:                       msgBcc &= email.GetBcc(n) & ";"
1750:                   Next

1760:                   msgSubject = email.Subject
                        msgDate = ConDateTime(email.LocalDate)

1770:                   Dim oClean As New clsMarsParser

1780:                   If email.HasHtmlBody = True Then
1790:                       bodyParts.Item("HTML") = oClean.ParseString(email.GetHtmlBody)
                        End If

1800:                   If email.HasPlainTextBody = True Then
1810:                       bodyParts.Item("TEXT") = oClean.ParseString(email.GetPlainTextBody)
                        End If

1820:                   msgBody = oClean.CleanString(IsNull(email.GetPlainTextBody)) & vbCrLf & oClean.CleanString(IsNull(email.GetHtmlBody))

1830:                   If searchEmail(searchString, msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, AnyAll, msgDate, matchExactCriteria) = True Then
1840:                       emailFound = True

1850:                       matchedMail.Add(email)

                            Dim row As DataRow

1860:                       row = emailData.Rows.Add

1870:                       row.Item("MsgID") = I
1880:                       row.Item("MsgFrom") = msgFrom
1890:                       row.Item("MsgTo") = msgTo
1900:                       row.Item("MsgCc") = msgCc
1910:                       row.Item("MsgBcc") = msgBcc
1920:                       row.Item("MsgSubject") = msgSubject
1930:                       row.Item("msgBody") = msgBody
1940:                       row.Item("msgDate") = ConDateTime(email.LocalDate)
1950:                       row.Item("msgTEXT") = bodyParts.Item("TEXT")
1960:                       row.Item("msgHTML") = bodyParts.Item("HTML")
1970:
                            'If saveAttachments = True Then

1980:                       attachmentsPath = attachmentsPath & Date.Now.ToString("yyyyMMddHHmmss") & "\" 'clsMarsParser.Parser.ParseString(attachmentsPath)
1990:                       clsMarsParser.Parser.ParseDirectory(attachmentsPath)

2000:                       If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

2010:                       email.SaveAllAttachments(attachmentsPath)

2020:                       For n As Integer = 0 To email.NumAttachments - 1
                                Dim att As String = email.GetAttachmentFilename(n)

                                Dim fullFile As String = attachmentsPath & att

2030:                           msgAttachments &= fullFile & ";"
2040:                       Next


2050:                       For n As Integer = 0 To email.NumRelatedItems - 1
                                Dim fName As String = email.GetRelatedFilename(n)

                                If fName = "" Then fName = IO.Path.GetRandomFileName

                                Dim fullfile As String = attachmentsPath & fName

2060:                           If email.SaveRelatedItem(n, attachmentsPath) = False Then
                                    ''console.writeline(email.LastErrorText)
                                End If
2070:                       Next


2080:                       If msgAttachments.EndsWith(";") Then msgAttachments = msgAttachments.Remove(msgAttachments.Length - 1, 1)
                            'End If

2090:                       row.Item("Attachments") = msgAttachments
2100:                       row.Item("AttachmentsPath") = attachmentsPath

2110:                       If Download = True Then
2120:                           ReDim Preserve nPointers(nPointer)

2130:                           nPointers(nPointer) = email.GetImapUid

2140:                           nPointer += 1
                            End If
2150:                   Else
2160:                       ok = oIMAP4.SetMailFlag(email, "SEEN", 0)
                        End If

2170:                   I += 1
2180:               Next
                End If

2190:           Try
2200:               oIMAP4.Logout()
2210:               oIMAP4.Disconnect()
                Catch : End Try
            End If

2220:       Return emailFound
2230:   Catch ex As Exception
2240:       If retryCount < 6 Then
2250:

2260:           retryCount += 1
2270:           System.Threading.Thread.Sleep(5000)
2280:           GoTo RETRY
            End If

            _ErrorHandle(gScheduleName & ": " & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
2290:       Return False
        End Try

    End Function


    Private Shared Sub DeleteFoundMessages(ByVal emailData As DataTable, ByVal success As Boolean)

10:     clsMarsUI.MainUI.BusyProgress(75, "Clearing matched emails from inbox...")

20:     Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
30:     Quiksoft.EasyMail.Parse.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
40:     Quiksoft.EasyMail.IMAP4.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
50:     Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Developer)/13047341F666623200000F00262C5AAE73"

        Dim pop3 As Chilkat.MailMan
        Dim oIMAP4 As Chilkat.Imap
60:     Dim SSL As SSL = New SSL
        Dim pointer As Integer = 0

70:     For Each row As DataRow In emailData.Rows
            Dim nRetry As Integer = 0
            Dim mailServer As String = row.Item("serverName")
            Dim userName As String = row.Item("userName")
            Dim password As String = row.Item("password")
            Dim Ordinals As String = row.Item("ordinalPos")
            Dim serverPort As Integer = row.Item("serverPort")
            Dim useSSL As Boolean = Convert.ToInt32(row.Item("useSSL"))
            Dim IMAPBox As String = row.Item("IMAPBox")
            Dim serverType As String = row.Item("ServerType")
            '70:         Dim nOrdinals As ArrayList = New ArrayList

            '80:         For Each s As String In Ordinals.Split(",")
            '90:             nOrdinals.Add(s)
            '100:        Next

RETRYPOINT:
80:         If serverType = "POP3" Then
90:             Try
100:                pop3 = New Chilkat.MailMan

110:                pop3.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")
                    'Connect to the POP3 mail server, if user has provided custom pop3 port then use that instead
120:                If serverPort = 0 Then
130:                    pop3.MailHost = mailServer
                        'pop3.Connect(mailserver)
140:                Else
150:                    If useSSL = True Then
160:                        pop3.PopSsl = True
170:                        pop3.MailPort = serverPort
180:                        pop3.MailHost = mailServer
                            'pop3.Connect(mailserver, port, SSL.GetInterface)
190:                    Else
200:                        pop3.MailPort = serverPort
210:                        pop3.MailHost = mailServer
                            'pop3.Connect(mailserver, port)
                        End If
                    End If

220:                clsMarsUI.MainUI.BusyProgress(60, "Deleting matched messages...")

230:                pop3.PopUsername = userName
240:                pop3.PopPassword = password

                    Dim ok As Boolean
                    Dim retry As Integer = 0

250:                Do
260:                    ok = pop3.VerifyPopConnection()

270:                    If ok = False Then System.Threading.Thread.Sleep(3000)

280:                    retry += 1
290:                Loop Until ok = True Or retry >= 3

300:                If ok = False Then
310:                    Throw New Exception(pop3.LastErrorText)
                    End If

320:                retry = 0

330:                Do
340:                    ok = pop3.VerifyPopLogin

350:                    If ok = False Then System.Threading.Thread.Sleep(3000)

360:                    retry += 1
370:                Loop Until ok = True Or retry >= 3

380:                If ok = False Then
390:                    Throw New Exception(pop3.LastErrorText)
                    End If

400:                For Each emailID As String In Ordinals.Split(",")
410:                    ok = pop3.DeleteByUidl(emailID)

420:                    If ok = False Then
430:                        ''console.writeLine(pop3.LastErrorText)
                        End If
440:                Next

450:                nPointers = Nothing
460:            Catch ex As Exception


470:                If nRetry < 5 Then
480:                    System.Threading.Thread.Sleep(30000)

490:                    GoTo RETRYPOINT
                    End If

500:                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
                End Try
510:        Else
                Dim ok As Boolean
                Dim retry As Integer = 0

520:            oIMAP4 = New Chilkat.Imap

530:            oIMAP4.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")

540:            Try
550:                If serverPort > 0 Then
560:                    If useSSL = True Then
570:                        oIMAP4.Ssl = True
580:                        oIMAP4.Port = serverPort
590:                    Else
600:                        oIMAP4.Port = serverPort
                        End If
                    End If

610:                Do
620:                    ok = oIMAP4.Connect(mailServer)

630:                    If ok = False Then System.Threading.Thread.Sleep(3000)

640:                    retry += 1
650:                Loop Until ok = True Or retry > 3

660:                If ok = False Then
670:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

680:                retry = 0
                    'Log onto the IMAP mail server
690:                Do
700:                    ok = oIMAP4.Login(userName, password)

710:                    If ok = False Then System.Threading.Thread.Sleep(3000)

720:                    retry += 1
730:                Loop Until ok = True Or retry > 3

740:                If ok = False Then
750:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

760:                ok = oIMAP4.SelectMailbox(IMAPBox)

770:                If ok = False Then
780:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

790:                For Each emailID As String In Ordinals.Split(",")

                        Dim email As Chilkat.Email = oIMAP4.FetchSingle(emailID, True)


800:                    If email IsNot Nothing Then oIMAP4.SetMailFlag(email, "Deleted", 1)
810:                Next

820:                oIMAP4.ExpungeAndClose()

830:                oIMAP4.Logout()

840:                oIMAP4.Disconnect()
850:                nPointers = Nothing
860:            Catch ex As Exception
870:                Try
880:                    oIMAP4.Logout()
890:                    oIMAP4.Disconnect()
                    Catch : End Try

900:                If nRetry < 5 Then
910:                    System.Threading.Thread.Sleep(30000)

920:                    GoTo RETRYPOINT
                    End If

930:                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
                End Try
            End If
940:    Next
    End Sub

    Private Shared Function searchEmail(ByVal Query As String, ByVal msgFrom As String, ByVal msgTo As String, ByVal msgCc As String, ByVal msgBcc As String, _
    ByVal msgSubject As String, ByVal msgBody As String, ByVal AnyAll As String, ByVal msgDate As String, ByVal matchExactCriteria As Boolean) As Boolean
        Dim conditions() As String = Query.Split("|")
        Dim results() As Boolean = Nothing
        Dim I As Integer = 0

        For Each s As String In conditions
            If s.Length = 0 Then Continue For

            Dim field As String = ""
            Dim opera As String = ""
            Dim value As String = ""

            ReDim Preserve results(I)

            results(I) = False

            Dim x As Integer = 0

            For Each v As String In s.Split("_")
                If x = 0 Then
                    field = v.Trim
                ElseIf x = 1 Then
                    opera = v.Trim
                Else
                    'if match exact is not set then lets trim the spaces for comparison
                    If matchExactCriteria = True Then
                        value &= v & "_"
                    Else
                        value &= v.Trim & "_"
                    End If
                End If

                x += 1
            Next

            value = value.Substring(0, value.Length - 1)

            'field = s.Split("_")(0).Trim
            'opera = s.Split("_")(1).Trim
            'value = s.Split("_")(2).Trim

            Dim searchField As String = ""

            Select Case field.ToLower
                Case "from"
                    searchField = msgFrom
                Case "to"
                    searchField = msgTo
                Case "cc"
                    searchField = msgCc
                Case "bcc"
                    searchField = msgBcc
                Case "subject"
                    searchField = msgSubject
                Case "message"
                    searchField = msgBody
                Case "date (yyyy-mm-dd hh:mm:ss)"
                    searchField = msgDate
            End Select

            If matchExactCriteria = False Then
                searchField = searchField.ToLower
                value = value.ToLower
            End If

            Select Case opera.ToLower
                Case "="
                    If searchField = value Then
                        results(I) = True
                    End If
                Case "<>"
                    If searchField <> value Then
                        results(I) = True
                    End If
                Case "contains"
                    If searchField Like "*" & value & "*" Then
                        results(I) = True
                    End If
                Case "does not contain"
                    ''console.writeline(searchField & " does not contain '" & value & "'")

                    If Not (searchField Like "*" & value & "*") Then
                        results(I) = True
                    End If
                Case "begins with"
                    If searchField Like value & "*" Then
                        results(I) = True
                    End If
                Case "ends with"
                    If searchField Like "*" & value Then
                        results(I) = True
                    End If
                Case ">"
                    If searchField > value Then
                        results(I) = True
                    End If
                Case "<"
                    If searchField < value Then
                        results(I) = True
                    End If
                Case "<="
                    If searchField <= value Then
                        results(I) = True
                    End If
                Case "=>"
                    If searchField >= value Then
                        results(I) = True
                    End If
            End Select

            I += 1
        Next

        If results IsNot Nothing Then
            If AnyAll = "ANY" Then
                If Array.IndexOf(results, True) > -1 Then
                    Return True
                Else
                    Return False
                End If
            ElseIf AnyAll = "ALL" Then
                If Array.IndexOf(results, False) > -1 Then
                    Return False
                Else
                    Return True
                End If
            End If
        Else
            Return False
        End If

    End Function
    Private Shared Function DoesWindowExist(ByVal windowName As String) As Process
        If windowName.IndexOf("*") > -1 Then
            For Each proc As Process In Process.GetProcesses
                If proc.MainWindowTitle.ToLower Like windowName.ToLower Then
                    Return proc
                End If
            Next
        Else
            For Each proc As Process In Process.GetProcesses
                If proc.MainWindowTitle.ToLower = windowName.ToLower Then
                    Return proc
                End If
            Next
        End If

        Return Nothing
    End Function

    Private Shared Function DoesProcessExist(ByVal processName As String) As Process
        If processName.IndexOf("*") > -1 Then
            For Each proc As Process In Process.GetProcesses
                If proc.ProcessName.ToLower Like processName.ToLower Then
                    Return proc
                End If
            Next
        Else
            For Each proc As Process In Process.GetProcesses
                If proc.ProcessName.ToLower = processName.ToLower Then
                    Return proc
                End If
            Next
        End If

        Return Nothing
    End Function

    Private Shared Sub CancelExecution(ByVal method As String)
        Application.DoEvents()
        gErrorDesc = "User Cancelled schedule execution"
        gErrorNumber = -2046082905
        gErrorSource = method
        gErrorLine = 0

        Dim ex As Exception = New Exception(gErrorDesc)

        Throw ex
    End Sub
    ''' <summary>
    ''' Returns a data table with all the matching files - PointerID, FileName, FolderName, FullPath, DateLastModified, DateCreated are the columns
    ''' </summary>
    ''' <param name="searchStr"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function findMatchingFiles(ByVal searchStr As String) As DataTable
        Dim folder As String = IO.Path.GetDirectoryName(searchStr)
        Dim mask As String = IO.Path.GetFileName(searchStr)
        Dim foundFiles As DataTable = m_foundFilesTable
        Dim I As Integer = 0

        For Each f As String In IO.Directory.GetFiles(folder, mask)
            Dim r As DataRow = foundFiles.NewRow

            r("pointerid") = I
            r("filename") = IO.Path.GetFileName(f)
            r("foldername") = IO.Path.GetDirectoryName(f)
            r("fullpath") = f
            r("datelastmodified") = File.GetLastWriteTime(f)
            r("datecreated") = File.GetCreationTime(f)

            foundFiles.Rows.Add(r)

            I += 1
        Next

        Return foundFiles
    End Function

    Public Shared Function doesFTPFileExist(conditionID As Integer, ByRef keyValues() As String) As Boolean
        Dim oFtp As Object
        Dim SQL As String = "SELECT * FROM eventconditions WHERE conditionid =" & conditionID
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)
        Dim ok As Boolean

        If oRs Is Nothing Then Return False
        If oRs.EOF = True Then Return False

        '//popserver =ftpserver
        '//popuser = ftp user
        '//poppassword = ftp password
        '//usessl = passive
        '//mailservertype = ftptype
        '//imapbox = ftp options
        Dim ftpServer, ftpUser, ftpPassword, ftpType, ftpOptions, ftpFilePath As String
        Dim port As Integer
        Dim passive As Boolean

        ftpServer = clsMarsParser.Parser.ParseString(IsNull(oRs("popserver").Value))
        ftpUser = clsMarsParser.Parser.ParseString(IsNull(oRs("popuser").Value))
        ftpPassword = clsMarsParser.Parser.ParseString(_DecryptDBValue(IsNull(oRs("poppassword").Value)))
        ftpType = clsMarsParser.Parser.ParseString(IsNull(oRs("mailservertype").Value))
        ftpOptions = clsMarsParser.Parser.ParseString(IsNull(oRs("imapbox").Value))
        ftpFilePath = clsMarsParser.Parser.ParseString(IsNull(oRs("filepath").Value))
        port = IsNull(oRs("serverport").Value, 21)
        passive = IsNull(oRs("usessl").Value, 0)

        Dim ftpCtrl As ucFTPDetails = New ucFTPDetails

        With ftpCtrl
            .cmbFTPType.Text = ftpType
            .txtFTPServer.Text = ftpServer
            .txtUserName.Text = ftpUser
            .txtPassword.Text = ftpPassword
            .txtPort.Value = port
            .chkPassive.Checked = passive
            .m_ftpOptions = ftpOptions

            oFtp = .ConnectFTP
        End With

        Dim ubound As Integer = ftpFilePath.Split("/").GetUpperBound(0)
        Dim filename As String = ftpFilePath.Split("/")(ubound)
        Dim ftpDir As String = ftpFilePath.Remove(ftpFilePath.Length - filename.Length, filename.Length)

        If TypeOf oFtp Is Chilkat.Ftp2 Then
            Dim ftp As Chilkat.Ftp2 = oFtp

            ok = ftp.ChangeRemoteDir(ftpDir)

            If ok = False Then Return False

            If filename.Contains("*") = False Then
                Try
                    Dim n As Integer = ftp.GetSizeByName(filename)

                    ReDim keyValues(4)

                    keyValues(0) = filename
                    keyValues(1) = ftpDir
                    keyValues(2) = ftpFilePath
                    keyValues(3) = ftp.GetLastModifiedTimeByName(filename)
                    keyValues(4) = ftp.GetCreateTimeByName(filename)

                    Return True
                Catch ex As Exception
                    Return False
                End Try
            Else
                Throw New Exception("Not yet implemented")
            End If
        Else
            Dim ftp As Rebex.Net.Sftp = oFtp

            ftp.ChangeDirectory(ftpDir)

            If filename.Contains("*") = False Then
                Try
                    Dim n As Integer = ftp.GetFileLength(ftpFilePath)
                    Dim it As Rebex.Net.SftpItem = ftp.GetInfo(ftpFilePath)
                    '//no error so the file exists
                    ReDim keyValues(4)

                    keyValues(0) = filename
                    keyValues(1) = ftpDir
                    keyValues(2) = ftpFilePath
                    keyValues(3) = it.Modified
                    keyValues(4) = it.Modified

                    Return True
                Catch ex As Exception
                    Return False
                End Try
            Else
                Throw New Exception("Not yet implemented")
            End If
        End If
    End Function
    Public Shared Function RunEvents6(Optional ByVal eventID As Integer = 0, Optional ByVal IsRetry As Boolean = False, _
           Optional ByRef returnResult As Boolean = False, Optional ByVal MasterHistoryID As Integer = 0) As Boolean
        Dim scheduleName As String = ""
        Dim conditionName As String = ""
        Dim packID As Integer = 0

        g_noshrinkage = True '//this variable allows the progress to stay visible between tasks

        'Dim debugFile As String = sAppPath & "Logs\ebprocessing_" & Guid.NewGuid.ToString & ".debug"

        'If eventID <> 0 Then
        '    debugFile = sAppPath & "Logs\ebprocessing_" & eventID & ".debug"
        'End If

        'If IO.File.Exists(debugFile) And eventID = 0 Then IO.File.Delete(debugFile)

10:     Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim proceed() As Boolean
            Dim I As Integer = 0
            Dim anyAll As String
            Dim numTimes() As Integer
            Dim executionFlow As String = "AFTER"
            Dim emailRemover As DataTable = Nothing
            Dim retryInterval As Integer = 0
            Dim useOpHours As Boolean = False
            Dim OpHoursName As String = ""
            Dim doprocess As Boolean = True
            Dim forceProcess As Boolean = False

20:         If eventID <> 0 Then forceProcess = True

30:         If eventID <> 0 Then
40:             gScheduleOwner = clsMarsEvent.getOwner(eventID)
            End If
            'Dim logTable As DataTable = New DataTable("eventduration")

            'With logTable.Columns
            '    .Add("eventid")
            '    .Add("eventname")
            '    .Add("starttime")
            '    .Add("finishtime")
            'End With

50:         If eventID = 0 Then
                Dim precheckConditions As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("PreCheckConditions", 0)))

60:             If precheckConditions = False Then
70:                 SQL = "SELECT * FROM EventAttr6 WHERE Status =1 AND (PackID IS NULL OR PackID =0) ORDER BY SchedulePriority"
80:             Else
90:                 SQL = "select * from eventattr6 e inner join eventconditions c on e.eventid = c.eventid where " & _
       "(c.conditiontype = 'database record exists' AND e.doprocess =1 AND c.anyall = 'ALL' and e.status =1 and packid =0 and isnull(packid,0) = 0) OR " & _
       "(c.conditiontype = 'unread email is present' AND e.doprocess =1 and e.status =1  and isnull(packid,0) = 0) OR " & _
       "(c.conditiontype NOT IN ('unread email is present','database record exists') and e.status =1 and isnull(packid,0) = 0) OR  " & _
       "(c.conditiontype = 'database record exists' AND c.anyall <> 'ALL' and e.status =1 and isnull(packid,0) = 0 ) "
                End If
100:        Else
110:            SQL = "SELECT * FROM EventAttr6 WHERE EventID =" & eventID
120:        End If

130:        oRs = clsMarsData.GetData(SQL)

140:        Dim scheduleType As String

150:        If frmMain.m_UserCancel = True Then
160:            CancelExecution("RunEvents6")
170:            Return False
            End If



180:        If oRs IsNot Nothing Then
190:            Do While oRs.EOF = False
                    'Dim logRow As DataRow = logTable.Rows.Add


200:                m_portDataCache = Nothing
210:                m_EventDataCache = Nothing
220:                Dim retryCount As Integer = 0
                    Dim nRetry As Integer = 0
RETRY:
230:                If frmMain.m_UserCancel = True Then
240:                    CancelExecution("RunEvents6")
250:                    Return False
                    End If

260:                Try
270:                    clsMarsUI.MainUI.BusyProgress(10, "Loading condition details...")

280:                    m_StartTime = Now

290:                    eventID = oRs("eventid").Value
300:                    packID = IsNull(oRs("packid").Value, 0)
310:                    anyAll = IsNull(oRs("anyall").Value, "ALL")
320:                    scheduleType = oRs("scheduletype").Value
330:                    executionFlow = IsNull(oRs("executionflow").Value, "AFTER")
340:                    scheduleName = oRs("eventname").Value
350:                    currentEventID = eventID
360:                    retryCount = IsNull(oRs("retrycount").Value, 5)
370:                    retryInterval = IsNull(oRs("retryinterval").Value, 0)
380:                    useOpHours = IsNull(oRs("useoperationhours").Value, 0)
390:                    OpHoursName = IsNull(oRs("operationname").Value, "")

                        'logRow("eventid") = eventID
                        'logRow("eventname") = scheduleName
                        'logRow("starttime") = m_StartTime

                        'SaveTextToFile("--------------------------------------------", debugFile, , True, True)
                        'SaveTextToFile(Date.Now & ": Start of " & scheduleName & " (" & eventID & ")", debugFile, , True, True)
                        '//this is a setting for db record exists (all records), if its false, the condition is not even checked
                        '#If DEBUG Then
400:                    Try
410:                        doprocess = IsNull(oRs("doprocess").Value, 1)
420:                    Catch ex As Exception
430:                        doprocess = True
                        End Try

440:                    If forceProcess Then doprocess = True

                        Dim precheckConditions As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("PreCheckConditions", 0)))

450:                    If precheckConditions = False Then doprocess = True

                        'SaveTextToFile(Date.Now & ": do process set as: " & doprocess.ToString, debugFile, , True, True)
                        '#Else
                        '                        doprocess=True
                        '#End If


460:                    If RunEditor = False Then
470:                        If packID = 0 Then
                                'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Logging entries into Task and Thread management tables", True)

480:                            clsMarsScheduler.globalItem.TaskManager("Add", , , , , , eventID)
490:                            clsMarsScheduler.globalItem.ThreadManager(eventID, "add", clsMarsScheduler.enScheduleType.EVENTBASED)
                            End If
                        End If

500:                    If packID = 0 Then
510:                        gScheduleName = scheduleName
                            clsMarsScheduler.m_progressID = "Event:" & eventID
520:                        gScheduleFolderName = clsMarsUI.getObjectParentName(eventID, clsMarsScheduler.enScheduleType.EVENTBASED)
                        End If

530:                    Dim ors1 As ADODB.Recordset

540:                    ors1 = clsMarsData.GetData("SELECT * FROM EventConditions WHERE EventID =" & eventID & " AND (Status = 1 OR Status IS NULL)")

550:                    If frmMain.m_UserCancel = True Then
560:                        CancelExecution("RunEvents6")
570:                        Return False
                        End If

580:                    If ors1.EOF = True Then
590:                        Throw New Exception("No enabled conditions were found for the event-based schedules. Enable conditions and try again.")
                        End If

600:                    m_resultsTable = New Hashtable()
610:                    I = 0
620:                    proceed = Nothing
630:                    numTimes = Nothing

                        'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Loading conditions...", True)

640:                    Do While ors1.EOF = False
650:                        If frmMain.m_UserCancel = True Then
660:                            CancelExecution("RunEvents6")
670:                            Return False
                            End If

680:                        Dim conditionType As String = ors1("conditiontype").Value
690:                        Dim returnValue As Boolean = Convert.ToBoolean(ors1("returnvalue").Value)
700:                        conditionName = ors1("conditionname").Value
710:                        Dim conditionID As Integer = ors1("conditionid").Value

720:                        ReDim Preserve proceed(I)
730:                        ReDim Preserve numTimes(I)

                            'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Processing condition: " & conditionName & " (" & conditionType & ")", True)

740:                        clsMarsUI.MainUI.BusyProgress(30, "Processing condition: " & conditionType)

750:                        proceed(I) = False
760:                        numTimes(I) = 0

770:                        Select Case conditionType.ToLower
                                Case "data received"
                                    Dim searchCriteria As String = ors1("searchcriteria").Value
                                    Dim serverPort As Integer = ors1("serverport").Value
                                    Dim serverIP As String = IsNull(ors1("popserver").Value, "")
                                    Dim matchType As Integer = IsNull(ors1("MatchExactCriteria").Value, 1)

780:                                If frmMain.m_UserCancel = True Then
790:                                    CancelExecution("RunEvents6")
800:                                    Return False
                                    End If

810:                                m_portDataCache = New DataTable("portData")
820:                                m_portDataCache.Columns.Add("entryid")
830:                                m_portDataCache.Columns.Add("entrydate")
840:                                m_portDataCache.Columns.Add("datarecvd")


                                    Dim keyValues() As Object = hasDataBeenReceived(conditionID, searchCriteria, serverPort, serverIP, matchType)

850:                                If keyValues IsNot Nothing Then
860:                                    If returnValue = True Then
870:                                        proceed(I) = True
                                        End If

880:                                    m_resultsTable.Add(conditionName, keyValues)
890:                                    numTimes(I) = keyValues.GetUpperBound(0)
900:                                Else
910:                                    If returnValue = False Then
920:                                        proceed(I) = True
                                        End If
                                    End If
930:                            Case "database record exists"
                                    Dim sAnyAll As String = IsNull(ors1("anyall").Value)

940:                                If doprocess = False And sAnyAll = "ALL" Then
950:                                    GoTo skip
960:                                ElseIf useOpHours = True And RunEditor = False And packID = 0 Then
970:                                    If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
980:                                        GoTo skip
990:                                    End If
1000:                               End If
1010:


1020:                               If frmMain.m_UserCancel = True Then
1030:                                   CancelExecution("RunEvents6")
1040:                                   Return False
                                    End If

1050:                               Dim keyValues As Object()
1060:                               Dim searchCriteria As String = ors1("searchcriteria").Value
1070:                               Dim connectionString As String = ors1("connectionstring").Value
1080:                               Dim KeyColumn As String = ors1("keycolumn").Value
1090:
                                    Dim cacheFolderName As String = IsNull(ors1("cachefoldername").Value, conditionName)
1100:                               ReDim Preserve clsMarsEvent.m_EventDataCache(I)

                                    'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Checking if record exists", True)
1110:                               keyValues = DoesRecordExists(conditionName, searchCriteria, connectionString, _
                                         KeyColumn, sAnyAll, cacheFolderName, doprocess)

                                    'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "done checking", True)

1120:                               If frmMain.m_UserCancel = True Then
1130:                                   CancelExecution("RunEvents6")
1140:                                   Return False
                                    End If

1150:                               If keyValues IsNot Nothing Then

1160:                                   If returnValue = True Then
1170:                                       proceed(I) = True
1180:                                   End If

1190:                                   m_resultsTable.Add(conditionName, keyValues)
1200:                                   numTimes(I) = keyValues.GetUpperBound(0)

1210:                                   If packID = 0 Then
1220:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventID =" & eventID)
1230:                                   Else
1240:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventPackID =" & eventID)
                                        End If
1250:                               Else
1260:                                   If returnValue = False Then
1270:                                       proceed(I) = True
1280:                                   End If
1290:                               End If

1300:                               clsMarsData.WriteData("UPDATE eventattr6 SET doprocess =0 WHERE eventid =" & eventID)
1310:                           Case "database record has changed"
1320:                               If useOpHours = True And RunEditor = False And packID = 0 Then
1330:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
1340:                                       GoTo skip
                                        End If
                                    End If

1350:                               Dim keyValues As Object()
1360:                               Dim detectInserts As Boolean
1370:                               Dim useConstraint As Boolean = Convert.ToInt32(IsNull(ors1("useconstraint").Value, 0))
1380:                               Dim constraintValue As Integer = IsNull(ors1("constraintvalue").Value, 0)
1390:                               Dim dataLastMod As String = IsNull(ors1("dataLastModified").Value, "")
                                    Dim searchCriteria As String = ors1("searchcriteria").Value
                                    Dim connString As String = ors1("connectionstring").Value
                                    Dim keyColumn As String = ors1("keycolumn").Value
                                    Dim dataModified As Boolean = False
                                    Dim lastPolled As Date = IsNull(ors1("lastpolled").Value, Date.Now)
                                    Dim runOnce As Boolean = IsNull(ors1("runonce").Value, 0)
                                    Dim ignore As Boolean = IsNull(ors1("ignorenow").Value, 0)
                                    Dim cacheFolderName As String = IsNull(ors1("cachefoldername").Value, conditionName)
1400:                               detectInserts = ors1("detectinserts").Value

1410:                               ReDim Preserve clsMarsEvent.m_EventDataCache(I)

1420:                               keyValues = HasDataChanged(conditionName, searchCriteria, connString, _
                                         keyColumn, detectInserts, returnValue, dataModified, cacheFolderName)

1430:                               Try
1440:                                   Date.Parse(dataLastMod)
1450:                               Catch ex As Exception
1460:                                   useConstraint = False
                                    End Try

1470:                               Dim doProceed As Boolean = False

1480:                               If useConstraint = False Or constraintValue = 0 Then
1490:                                   doProceed = True
1500:                               Else
1510:                                   If RunEditor = False Then
1520:                                       If Now.Subtract(lastPolled).TotalMinutes < constraintValue Then
1530:                                           GoTo skip
1540:                                       End If
1550:                                   ElseIf ignore = True And dataModified = False And RunEditor = False Then
1560:                                       GoTo 2080
                                        End If

1570:                                   Dim int As Integer = Date.Now.Subtract(dataLastMod).TotalMinutes

1580:                                   If returnValue = False Then
1590:                                       If int >= constraintValue Then
1600:                                           doProceed = True
1610:                                       End If
1620:                                   Else
1630:                                       If int <= constraintValue Then
1640:                                           doProceed = True
                                            End If
                                        End If
                                    End If

1650:                               If keyValues IsNot Nothing Then
1660:                                   If dataModified = True Then
1670:                                       clsMarsData.WriteData("UPDATE EventConditions SET dataLastModified ='" & ConDateTime(Date.Now) & "', IgnoreNow =0 WHERE ConditionID =" & conditionID)
1680:                                   ElseIf runOnce = True And doProceed = True Then
1690:                                       clsMarsData.WriteData("UPDATE EventConditions SET Ignorenow = 1 WHERE ConditionID =" & conditionID)
                                        End If
                                    End If

1700:                               If keyValues IsNot Nothing And doProceed = True Then
1710:                                   proceed(I) = True
1720:                                   numTimes(I) = keyValues.GetUpperBound(0)

1730:                                   m_resultsTable.Add(conditionName, keyValues)

1740:                                   If packID = 0 Then
1750:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventID =" & eventID)
1760:                                   Else
1770:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventPackID =" & eventID)
                                        End If

                                    End If


1780:                               SQL = "UPDATE EventConditions SET LastPolled = [cd] WHERE ConditionID =" & conditionID


1790:                               If gConType = "DAT" Then
1800:                                   SQL = SQL.Replace("[cd]", "Now()")
1810:                               Else
1820:                                   SQL = SQL.Replace("[cd]", "GetDate()")
                                    End If

1830:                               clsMarsData.WriteData(SQL)

                                    '540:                                If keyValues IsNot Nothing Then

                                    '                                        clsMarsData.WriteData("UPDATE EventConditions SET dataLastModified ='" & ConDateTime(Date.Now) & "' WHERE ConditionID =" & conditionID)
                                    '543:
                                    '552:                                    If returnValue = True Then
                                    '560:                                        proceed(I) = True
                                    '570:                                        numTimes(I) = keyValues.GetUpperBound(0)
                                    '580:                                    End If

                                    '590:                                    m_resultsTable.Add(conditionName, keyValues)
                                    '600:                                Else
                                    '610:                                    If returnValue = False Then
                                    '620:                                        proceed(I) = True
                                    '630:                                    End If
                                    '                                    End If

1840:                           Case "file exists"
1850:                               If useOpHours = True And RunEditor = False And packID = 0 Then
1860:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
1870:                                       GoTo skip
                                        End If
                                    End If

1880:                               If frmMain.m_UserCancel = True Then
1890:                                   CancelExecution("RunEvents6")
1900:                                   Return False
                                    End If

                                    Dim directoryType As String = IsNull(ors1("redirectto").Value, "LOCAL")

1910:                               If directoryType = "" Then directoryType = "LOCAL"

1920:                               Dim keyValues() As Object = Nothing
1930:                               Dim path As String = clsMarsParser.Parser.ParseString(ors1("filepath").Value)

1940:                               If directoryType = "LOCAL" Then
                                        Dim runforeachfile As Boolean = False

1950:                                   ReDim keyValues(0)

1960:                                   Try
1970:                                       runforeachfile = IsNull(ors1("runforeachfile").Value, False)
1980:                                   Catch ex As Exception
1990:                                       runforeachfile = False
                                        End Try

2000:                                   If path.Contains("*") Or path.Contains("?") Then
                                            Dim dt As DataTable = findMatchingFiles(path)

2010:                                       If runforeachfile = True Then
2020:                                           numTimes(I) = dt.Rows.Count - 1
2030:                                       Else
2040:                                           numTimes(I) = 0
                                            End If

2050:                                       If dt.Rows.Count = 0 Then '//no files found
2060:                                           If returnValue = False Then '//if the condition is file doesnt exist
2070:                                               proceed(I) = True
2080:                                           Else
2090:                                               proceed(I) = False
                                                End If

                                                '//lets add a dummy row
                                                Dim r As DataRow = dt.NewRow
2100:                                           r("pointerid") = 0
2110:                                           r("filename") = ""
2120:                                           r("foldername") = ""
2130:                                           r("fullpath") = ""
2140:                                           r("datelastmodified") = ""
2150:                                           r("datecreated") = ""

2160:                                           dt.Rows.Add(r)
2170:                                       Else
2180:                                           If returnValue = False Then
2190:                                               proceed(I) = False '//if condition was file doesnt exist 
2200:                                           Else
2210:                                               proceed(I) = True
                                                End If
                                            End If

2220:                                       keyValues(0) = dt
2230:                                       m_resultsTable.Add(conditionName, keyValues)
2240:                                   Else

2250:                                       numTimes(I) = 0

2260:                                       If IO.File.Exists(path) = returnValue Then
2270:                                           proceed(I) = True
2280:                                       End If

2290:                                       If frmMain.m_UserCancel = True Then
2300:                                           CancelExecution("RunEvents6")
2310:                                           Return False
                                            End If

2320:                                       If proceed(I) = True Then
                                                Dim dt As DataTable = m_foundFilesTable

                                                Dim r As DataRow = dt.NewRow

2330:                                           If IO.File.Exists(path) = True Then
2340:                                               r("pointerid") = 0
2350:                                               r("filename") = IO.Path.GetFileName(path)
2360:                                               r("foldername") = IO.Path.GetDirectoryName(path)
2370:                                               r("fullpath") = path
2380:                                               r("datelastmodified") = File.GetLastWriteTime(path)
2390:                                               r("datecreated") = File.GetCreationTime(path)
2400:                                           Else
2410:                                               r("pointerid") = 0
2420:                                               r("filename") = ""
2430:                                               r("foldername") = ""
2440:                                               r("fullpath") = ""
2450:                                               r("datelastmodified") = ""
2460:                                               r("datecreated") = ""
                                                End If

2470:                                           dt.Rows.Add(r)

2480:                                           keyValues(0) = dt
2490:                                           m_resultsTable.Add(conditionName, keyValues)
2500:                                       End If
                                        End If
2510:                               Else
2520:                                   proceed(I) = doesFTPFileExist(conditionID, keyValues)

2530:                                   If proceed(I) = False Then
2540:                                       ReDim keyValues(4)
2550:                                       keyValues(0) = ""
2560:                                       keyValues(1) = ""
2570:                                       keyValues(2) = ""
2580:                                       keyValues(3) = Now
2590:                                       keyValues(4) = Now
                                        End If

2600:                                   m_resultsTable.Add(conditionName, keyValues)
                                    End If
2610:                           Case "file has been modified"
2620:                               If useOpHours = True And RunEditor = False And packID = 0 Then
2630:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
2640:                                       GoTo skip
                                        End If
                                    End If

2650:                               If frmMain.m_UserCancel = True Then
2660:                                   CancelExecution("RunEvents6")
2670:                                   Return False
                                    End If

2680:                               Dim keyValues() As Object

2690:                               Dim path As String
2700:                               Dim d As Date

2710:                               path = ors1("filepath").Value

2720:                               Dim OriginalDate As Date = ConDateTime(ors1("keycolumn").Value)

2730:                               d = ConDateTime(File.GetLastWriteTime(path))

2740:                               If frmMain.m_UserCancel = True Then
2750:                                   CancelExecution("RunEvents6")
2760:                                   Return False
                                    End If

2770:                               If (d <> OriginalDate) = returnValue Then
2780:                                   proceed(I) = True
2790:                               Else
2800:                                   proceed(I) = False
2810:                               End If

2820:                               clsMarsData.WriteData("UPDATE EventConditions SET KeyColumn = " & _
     "'" & d & "' WHERE ConditionID =" & conditionID)


2830:                               If returnValue = True And proceed(I) = True Then

2840:                                   ReDim keyValues(4)

2850:                                   Dim fil As FileVersionInfo = FileVersionInfo.GetVersionInfo(path)

2860:                                   keyValues(0) = ExtractFileName(path)
2870:                                   keyValues(1) = GetDirectory(path)
2880:                                   keyValues(2) = path
2890:                                   keyValues(3) = File.GetLastWriteTime(path)
2900:                                   keyValues(4) = File.GetCreationTime(path)

2910:                                   m_resultsTable.Add(conditionName, keyValues)
2920:                               End If
2930:                           Case "process exists"
2940:                               If useOpHours = True And RunEditor = False And packID = 0 Then
2950:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
2960:                                       GoTo skip
                                        End If
                                    End If

2970:                               If frmMain.m_UserCancel = True Then
2980:                                   CancelExecution("RunEvents6")
2990:                                   Return False
                                    End If

3000:                               Dim processName As String
3010:                               Dim keyValues() As Object = Nothing
3020:                               Dim procFound As Process = Nothing

3030:                               processName = ors1("searchcriteria").Value

3040:                               procFound = DoesProcessExist(processName)

3050:                               If frmMain.m_UserCancel = True Then
3060:                                   CancelExecution("RunEvents6")
3070:                                   Return False
                                    End If

3080:                               If procFound IsNot Nothing Then
3090:                                   If returnValue = True Then
3100:                                       proceed(I) = True
3110:                                   Else
3120:                                       proceed(I) = False
3130:                                   End If
3140:                               ElseIf procFound Is Nothing Then
3150:                                   If returnValue = True Then
3160:                                       proceed(I) = False
3170:                                   Else
3180:                                       proceed(I) = True
3190:                                   End If
3200:                               End If

3210:                               If returnValue = True And proceed(I) = True Then
3220:                                   ReDim keyValues(6)

3230:                                   keyValues(0) = procFound.ProcessName
3240:                                   keyValues(1) = procFound.MainWindowTitle
3250:                                   keyValues(2) = procFound.TotalProcessorTime.TotalMinutes
3260:                                   keyValues(3) = procFound.UserProcessorTime.TotalMinutes
3270:                                   keyValues(4) = procFound.MaxWorkingSet
3280:                                   keyValues(5) = procFound.MinWorkingSet
3290:                                   keyValues(6) = procFound.Id
3300:                               End If

3310:                               m_resultsTable.Add(conditionName, keyValues)
3320:                           Case "unread email is present"

3330:                               If doprocess = False Then
3340:                                   GoTo skip
3350:                               ElseIf useOpHours = True And RunEditor = False And packID = 0 Then
3360:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
3370:                                       GoTo skip
                                        End If
                                    End If

3380:                               If frmMain.m_UserCancel = True Then
3390:                                   CancelExecution("RunEvents6")
3400:                                   Return False
                                    End If

3410:                               Dim keyValues() As Object
3420:                               Dim emailFound As Boolean = False

3430:                               Dim pollingInterval As Integer = IsNull(ors1("pollinginterval").Value, 5)
3440:                               Dim lastPolled As Date = IsNull(ors1("lastpolled").Value, Date.Now)
3450:                               Dim popServer As String = ors1("popserver").Value
3460:                               Dim popUser As String = ors1("popuser").Value
3470:                               Dim popPassword As String = _DecryptDBValue(ors1("poppassword").Value)
3480:                               Dim popRemove As Boolean = Convert.ToBoolean(ors1("popremove").Value)
3490:                               Dim searchString As String = ors1("searchcriteria").Value
3500:                               Dim emailAnyAll As String = ors1("anyall").Value
                                    Dim serverPort As Integer = IsNull(ors1("serverport").Value, 0)
                                    Dim useSSL As Boolean = Convert.ToInt32(IsNull(ors1("usessl").Value, 0))
                                    Dim serverType As String = IsNull(ors1("mailservertype").Value, "POP3")
                                    Dim IMAPBox As String = IsNull(ors1("imapbox").Value)
                                    Dim saveAttachments As Boolean = IsNull(ors1("saveattachments").Value, 0)
                                    Dim attachmentsPath As String = IsNull(ors1("attachmentspath").Value, "")
                                    Dim matchExactCriteria As Boolean = False

3510:                               Try
3520:                                   matchExactCriteria = IsNull(ors1("matchexactcriteria").Value, 0)
                                    Catch : End Try


3530:                               If serverType = "" Then serverType = "POP3"

3540:                               If RunEditor = False Then
3550:                                   If Now.Subtract(lastPolled).TotalMinutes < pollingInterval Then
#If Not Debug Then
3560:                                       GoTo skip
#End If
3570:                                   End If
3580:                               End If

3590:                               Dim emailData As New DataTable("EmailData")

3600:                               With emailData.Columns()
3610:                                   .Add("MsgID", System.Type.GetType("System.Int32"))
3620:                                   .Add("MsgFrom", System.Type.GetType("System.String"))
3630:                                   .Add("MsgTo", System.Type.GetType("System.String"))
3640:                                   .Add("MsgCc", System.Type.GetType("System.String"))
3650:                                   .Add("MsgBcc", System.Type.GetType("System.String"))
3660:                                   .Add("MsgSubject", System.Type.GetType("System.String"))
3670:                                   .Add("MsgBody", System.Type.GetType("System.String"))
3680:                                   .Add("MsgDate", System.Type.GetType("System.DateTime"))
3690:                                   .Add("MsgTEXT", System.Type.GetType("System.String"))
3700:                                   .Add("MsgHTML", System.Type.GetType("System.String"))
3710:                                   .Add("Attachments", System.Type.GetType("System.String"))
3720:                                   .Add("AttachmentsPath", System.Type.GetType("System.String"))
3730:                               End With

3740:                               emailFound = DoesEmailExist(popServer, popUser, popPassword, popRemove, _
                                         searchString, emailAnyAll, emailData, _
                                         conditionID, serverPort, useSSL, serverType, IMAPBox, saveAttachments, matchExactCriteria)

3750:                               If frmMain.m_UserCancel = True Then
3760:                                   CancelExecution("RunEvents6")
3770:                                   Return False
                                    End If

3780:                               If emailFound = True Then
3790:                                   If returnValue = True Then
3800:                                       ReDim keyValues(0)

3810:                                       keyValues(0) = emailData

3820:                                       proceed(I) = True
3830:                                       numTimes(I) = IIf(emailData.Rows.Count = 0, 0, emailData.Rows.Count - 1)

3840:                                       If packID = 0 Then
3850:                                           clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & emailData.Rows.Count & " WHERE EventID =" & eventID)
3860:                                       Else
3870:                                           clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & emailData.Rows.Count & " WHERE EventPackID =" & eventID)
                                            End If
3880:                                   End If

3890:                                   m_resultsTable.Add(conditionName, keyValues)

                                        'do save attachments, mail forwading and redirection
3900:                                   attachmentsPath = clsMarsParser.Parser.ParseString(attachmentsPath)

3910:                                   clsMarsParser.Parser.ParseDirectory(attachmentsPath)

3920:                                   RedirectMail(matchedMail, conditionID, emailData)
3930:                                   ForwardMail(matchedMail, conditionID, emailData)

                                        Dim attachmentsFound As String = ""

3940:                                   Try
3950:                                       If attachmentsPath IsNot Nothing Then
3960:                                           If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

3970:                                           For Each row As DataRow In emailData.Rows
3980:                                               attachmentsFound = IsNull(row("Attachments"))
3990:                                               If attachmentsFound <> "" Then
4000:                                                   For Each s As String In attachmentsFound.Split(";")
4010:                                                       If s <> "" Then
                                                                Dim attachment As String = IO.Path.GetFileName(s)

4020:                                                           IO.File.Copy(s, attachmentsPath & attachment, True)

                                                            End If
4030:                                                   Next
                                                    End If
4040:                                               row("AttachmentsPath") = attachmentsPath
4050:                                           Next
                                            End If
4060:                                   Catch ex As Exception
4070:                                       _ErrorHandle("Error saving attachments - " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                                        End Try

4080:

4090:                                   For Each fso As FileStream In cleanerFS
4100:                                       fso.Close()
4110:                                   Next

4120:                                   For Each s As String In cleanerFiles
4130:                                       Try
4140:                                           IO.File.Delete(s)
                                            Catch : End Try
4150:                                   Next

                                        'store the details of the server where email was found
4160:                                   If emailRemover Is Nothing Then
4170:                                       emailRemover = New DataTable("emailRemover")

4180:                                       With emailRemover.Columns
4190:                                           .Add("serverName", System.Type.GetType("System.String"))
4200:                                           .Add("userName", System.Type.GetType("System.String"))
4210:                                           .Add("password", System.Type.GetType("System.String"))
4220:                                           .Add("ordinalPos", System.Type.GetType("System.String"))
4230:                                           .Add("serverPort", System.Type.GetType("System.Int32"))
4240:                                           .Add("useSSL", System.Type.GetType("System.Int32"))
4250:                                           .Add("IMAPBox", System.Type.GetType("System.String"))
4260:                                           .Add("ServerType", System.Type.GetType("System.String"))
4270:                                       End With
4280:                                   End If

4290:                                   If nPointers IsNot Nothing Then
4300:                                       Dim ordinals As String = ""

4310:                                       For Each x As String In nPointers
4320:                                           ordinals &= x & ","
4330:                                       Next

4340:                                       ordinals = ordinals.Substring(0, ordinals.Length - 1)

4350:                                       Dim row As DataRow = emailRemover.NewRow

4360:                                       row.Item("serverName") = popServer
4370:                                       row.Item("userName") = popUser
4380:                                       row.Item("password") = popPassword
4390:                                       row.Item("ordinalPos") = ordinals
4400:                                       row.Item("serverPort") = serverPort
4410:                                       row.Item("useSSL") = Convert.ToInt32(useSSL)
4420:                                       row.Item("IMAPBox") = IMAPBox
4430:                                       row.Item("ServerType") = serverType

4440:                                       emailRemover.Rows.Add(row)
4450:                                   End If

4460:                                   cleanerFiles = Nothing
4470:                                   cleanerFS = Nothing
4480:                                   matchedMail = Nothing
4490:                               Else
4500:                                   If returnValue = False Then
4510:                                       proceed(I) = True
4520:                                   End If

4530:                                   Try
4540:                                       For Each fso As FileStream In cleanerFS
4550:                                           fso.Close()
4560:                                       Next

4570:                                       For Each s As String In cleanerFiles
4580:                                           IO.File.Delete(s)
4590:                                       Next
                                        Catch : End Try
                                    End If

4600:                               SQL = "UPDATE EventConditions SET LastPolled = [cd] WHERE ConditionID =" & conditionID

4610:                               If gConType = "DAT" Then
4620:                                   SQL = SQL.Replace("[cd]", "Now()")
4630:                               Else
4640:                                   SQL = SQL.Replace("[cd]", "GetDate()")
4650:                               End If

4660:                               clsMarsData.WriteData(SQL)

4670:                               If frmMain.m_UserCancel = True Then
4680:                                   CancelExecution("RunEvents6")
4690:                                   Return False
                                    End If

4700:                               clsMarsData.WriteData("UPDATE eventattr6 SET doprocess =0 WHERE eventid =" & eventID)
4710:                           Case "window is present"
4720:                               If useOpHours = True And RunEditor = False And packID = 0 Then
4730:                                   If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
                                            'SaveTextToFile(Date.Now & ": Skipped as its outside Operational hours", debugFile, , True, True)
4740:                                       GoTo skip
                                        End If
                                    End If

4750:                               If frmMain.m_UserCancel = True Then
4760:                                   CancelExecution("RunEvents6")
4770:                                   Return False
                                    End If

4780:                               Dim windowName As String
4790:                               Dim keyValues() As Object = Nothing
4800:                               Dim procFound As Process = Nothing

4810:                               windowName = ors1("searchcriteria").Value

4820:                               procFound = DoesWindowExist(windowName)

4830:                               If procFound IsNot Nothing Then
4840:                                   If returnValue = True Then
4850:                                       proceed(I) = True
4860:                                   Else
4870:                                       proceed(I) = False
4880:                                   End If
4890:                               ElseIf procFound Is Nothing Then
4900:                                   If returnValue = True Then
4910:                                       proceed(I) = False
4920:                                   Else
4930:                                       proceed(I) = True
4940:                                   End If
4950:                               End If

4960:                               If returnValue = True And proceed(I) = True Then
4970:                                   ReDim keyValues(6)

4980:                                   keyValues(0) = procFound.ProcessName
4990:                                   keyValues(1) = procFound.MainWindowTitle
5000:                                   keyValues(2) = procFound.TotalProcessorTime
5010:                                   keyValues(3) = procFound.UserProcessorTime
5020:                                   keyValues(4) = procFound.MaxWorkingSet
5030:                                   keyValues(5) = procFound.MinWorkingSet
5040:                                   keyValues(6) = procFound.Id
5050:                               End If

5060:                               End Select

5070:                       I += 1
5080:                       If frmMain.m_UserCancel = True Then
5090:                           CancelExecution("RunEvents6")
5100:                           Return False
                            End If

5110:                       ors1.MoveNext()
5120:                   Loop

5130:                   If frmMain.m_UserCancel = True Then
5140:                       CancelExecution("RunEvents6")
5150:                       Return False
                        End If

5160:                   ors1.Close()

                        '3941:                   clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Processing results", True)

5170:                   clsMarsUI.MainUI.BusyProgress(40, "Processing results...")

5180:                   Dim proceeder As Boolean = False

5190:                   If proceed Is Nothing Then GoTo skip

5200:                   If anyAll = "ANY" Then
5210:                       If Array.IndexOf(proceed, True) > -1 Then
5220:                           proceeder = True
5230:                       End If
5240:                   ElseIf anyAll = "ALL" Then
5250:                       If Array.IndexOf(proceed, False) > -1 Then
5260:                           proceeder = False
5270:                       Else
5280:                           proceeder = True
5290:                       End If
5300:                   End If

5310:                   If frmMain.m_UserCancel = True Then
5320:                       CancelExecution("RunEvents6")
5330:                       Return False
                        End If

5340:                   Dim n As Integer = 0


5350:                   For Each nx As Integer In numTimes
5360:                       If nx > n Then
5370:                           n = nx
5380:                       End If
5390:                   Next

                        Dim ok As Boolean

5400:                   If proceeder = True Then
5410:                       If frmMain.m_UserCancel = True Then
5420:                           CancelExecution("RunEvents6")
5430:                           Return False
                            End If

5440:                       clsMarsUI.MainUI.BusyProgress(80, "Processing schedules...")
                            'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Processing reports", True)
5450:                       Dim results As DataTable

5460:                       results = ProcessSchedules(eventID, oRs, n, executionFlow)

                            'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Reports processed", True)

                            Dim okCount As Integer = 0
                            Dim failCount As Integer = 0
                            Dim msgTally As String = ""

5470:                       For Each row As DataRow In results.Rows
5480:                           If row.Item("Result") = 1 Then
5490:                               okCount += 1
5500:                           ElseIf row.Item("Result") = 0 Then
5510:                               msgTally &= row.Item("Msg") & ";"
5520:                               failCount += 1
                                End If
5530:                       Next

5540:                       If failCount > 0 Then
5550:                           ok = False
5560:                       Else
5570:                           ok = True
                            End If

5580:                       If emailRemover IsNot Nothing Then
5590:                           DeleteFoundMessages(emailRemover, ok)
5600:                       End If

                            Dim resultMsg As String = n + 1 & " items were found. " & okCount & " succeeded and " & failCount & " failed." & msgTally

5610:                       SaveEventHistory(eventID, ok, resultMsg, MasterHistoryID)

5620:                       returnResult = ok

5630:                       If ok = False And retryInterval > 0 And RunEditor = False And IsRetry = False Then
5640:                           clsMarsReport.LogRetry(0, eventID)
                            End If

                            'clsMarsDebug.writeToDebug(IO.Path.GetFileName(debugFile), "Finished", True)
5650:                   End If

5660:                   If frmMain.m_UserCancel = True Then
5670:                       CancelExecution("RunEvents6")
5680:                       Return False
                        End If

skip:
5690:                   emailRemover = Nothing
5700:                   nPointers = Nothing

5710:                   If frmMain.m_UserCancel = True Then
5720:                       CancelExecution("RunEvents6")
5730:                       Return False
                        End If

5740:               Catch ex As Exception
5750:                   If nRetry <= retryCount Then
5760:                       nRetry += 1
5770:                       System.Threading.Thread.Sleep(1000)
5780:                       emailRemover = Nothing
5790:                       GoTo RETRY
5800:                   Else
5810:                       Throw ex
5820:                   End If
5830:               End Try

5840:               If RunEditor = False And packID = 0 Then
5850:                   clsMarsScheduler.globalItem.TaskManager("Remove", , , , , , eventID)
5860:                   clsMarsScheduler.globalItem.ThreadManager(eventID, "remove", clsMarsScheduler.enScheduleType.EVENTBASED)
                    End If

                    'logRow("finishtime") = Now

                    'SaveTextToFile(Date.Now & ": End of " & scheduleName, debugFile, , True, True)

5870:               oRs.MoveNext()
5880:           Loop


                'logTable.WriteXml(sAppPath & "eventprocess_" & Process.GetCurrentProcess.Id & ".xml", XmlWriteMode.WriteSchema)

                g_noshrinkage = False

5890:           clsMarsUI.MainUI.BusyProgress(, , True)

5900:           oRs.Close()

5910:           m_resultsTable = Nothing
5920:           currentEventID = 0
5930:       End If

5940:   Catch ex As Exception
5950:       If RunEditor = False And packID = 0 Then
5960:           clsMarsScheduler.globalItem.TaskManager("Remove", , , , , , eventID)
5970:           clsMarsScheduler.globalItem.ThreadManager(eventID, "remove", clsMarsScheduler.enScheduleType.EVENTBASED)
            End If

            _ErrorHandle(scheduleName & ": " & conditionName & " [" & ex.Message & "]", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
5980:   Finally
5990:       If eventID <> 0 Then
6000:           gScheduleOwner = ""
            End If
        End Try
    End Function




    Private Shared Function ProcessSchedules(ByVal eventID As Integer, ByVal oRs As ADODB.Recordset, ByVal loopController As Integer, _
    ByVal executionFlow As String) As DataTable
        Dim scheduleType As String
        Dim SQL As String
        Dim oRs1 As ADODB.Recordset
        Dim I As Integer = 0
        Dim ok As Boolean = True
        Dim reportTitle As String = ""
        Dim oTask As New clsMarsTask
        Dim results As DataTable = New DataTable("results")

        With results
            .Columns.Add("Pointer")
            .Columns.Add("Result")
            .Columns.Add("Msg")
        End With

        clsMarsUI.BusyProgress(50, "Processing reports")

        scheduleType = oRs("scheduletype").Value

        procs = New System.Collections.Generic.List(Of Process)

        Select Case scheduleType.ToLower
            Case "none"
                For I = 0 To loopController
                    Dim progVal As Integer

                    If RunEditor = True Then
                        Try
                            progVal = (I / loopController) * 100
                        Catch ex As Exception
                            progVal = 50
                        End Try

                        clsMarsUI.BusyProgress(progVal, "Processing result " & (I + 1) & " of " & loopController)
                    End If

                    clsMarsEvent.m_Pointer = I
                    ok = oTask.ProcessTasks(eventID, "NONE")

                    Dim row As DataRow = results.NewRow
                    Dim Msg As String = ""

                    If ok = False Then
                        Msg = gErrorDesc & " [" & gErrorNumber & "]"
                    End If

                    row.Item("Pointer") = I
                    row.Item("Result") = Convert.ToInt16(ok)
                    row.Item("Msg") = Msg

                    results.Rows.Add(row)
                Next

                Return results
            Case "new"
                For I = 0 To loopController

                    If RunEditor Then
                        Dim progVal As Integer

                        Try
                            progVal = (I / loopController) * 100
                        Catch ex As Exception
                            progVal = 50
                        End Try

                        clsMarsUI.BusyProgress(progVal, "Processing result " & I & " of " & loopController)
                    End If

                    clsMarsEvent.m_Pointer = I

                    SQL = "SELECT ReportID,ReportTitle FROM ReportAttr WHERE PackID =" & eventID

                    oRs1 = clsMarsData.GetData(SQL)

                    If oRs1 IsNot Nothing Then
                        Dim oReport As clsMarsReport = New clsMarsReport

                        If oRs1.EOF = False Then
                            Do While oRs1.EOF = False
                                reportTitle = oRs1("reporttitle").Value

                                Select Case executionFlow
                                    Case "AFTER"
                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)

                                        oTask.ProcessTasks(eventID, "NONE")

                                    Case "BEFORE"
                                        oTask.ProcessTasks(eventID, "NONE")

                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)
                                    Case "BOTH"
                                        oTask.ProcessTasks(eventID, "NONE")

                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)

                                        oTask.ProcessTasks(eventID, "NONE")
                                End Select

                                Dim row As DataRow = results.NewRow
                                Dim Msg As String = ""

                                If ok = False Then
                                    Msg = " - " & reportTitle & " [" & gErrorDesc & " (" & gErrorNumber & ")]"
                                End If

                                row.Item("Pointer") = I
                                row.Item("Result") = Convert.ToInt16(ok)
                                row.Item("Msg") = Msg

                                results.Rows.Add(row)

                                'SaveEventHistory(eventID, ok, "- " & reportTitle & " executed successfully.")


                                oRs1.MoveNext()
                            Loop
                        Else
                            ok = oTask.ProcessTasks(eventID, "NONE")

                            Dim row As DataRow = results.NewRow
                            Dim Msg As String = ""

                            If ok = False Then
                                Msg = gErrorDesc & " [" & gErrorNumber & "]"
                            End If

                            row.Item("Pointer") = I
                            row.Item("Result") = Convert.ToInt16(ok)
                            row.Item("Msg") = Msg

                            results.Rows.Add(row)
                        End If

                        oRs1.Close()
                    End If
                Next

                Return results
            Case "existing"
                gRunByEvent = True

                For I = 0 To loopController

                    Dim progVal As Integer

                    Try
                        progVal = (I / loopController) * 100
                    Catch ex As Exception
                        progVal = 50
                    End Try

                    If RunEditor = True Then clsMarsUI.BusyProgress(progVal, "Processing result " & I & " of " & loopController)

                    clsMarsEvent.m_Pointer = I

                    Select Case executionFlow
                        Case "AFTER"
                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)

                            oTask.ProcessTasks(eventID, "NONE")
                        Case "BEFORE"
                            oTask.ProcessTasks(eventID, "NONE")

                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)
                        Case "BOTH"
                            oTask.ProcessTasks(eventID, "NONE")

                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)

                            oTask.ProcessTasks(eventID, "NONE")
                    End Select

                    Dim row As DataRow = results.NewRow
                    Dim Msg As String = ""

                    If ok = False Then
                        Msg = gErrorDesc & " [" & gErrorNumber & "]"
                    End If

                    row.Item("Pointer") = I
                    row.Item("Result") = Convert.ToInt16(ok)
                    row.Item("Msg") = Msg

                    results.Rows.Add(row)
                Next

                Dim aliveCount As Integer = 0

                '//wait for the processes to exit
                If procs IsNot Nothing Then
                    Do

                        aliveCount = 0

                        For Each p As Process In procs
                            Try
                                If p.HasExited = False Then
                                    aliveCount += 1
                                Else
                                    p = Nothing
                                End If
                            Catch : End Try
                        Next

                        If aliveCount > 0 Then
                            clsMarsUI.BusyProgress(80, aliveCount & " schedule(s) remaining to complete execution", False)
                            System.Threading.Thread.Sleep(1000)
                        End If

                    Loop Until aliveCount = 0
                End If

                procs = Nothing

                gRunByEvent = False

                Return results
        End Select
    End Function
    Public Function PastePackageSchedule(ByVal nPackID As Integer, ByVal nParent As Integer) As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String = " WHERE EventPackID = " & nPackID
        Dim sNew As String
        Dim CopyReports As Boolean
        Dim NewPackID As Int64
        Dim nScheduleID As Int64
        Dim oRs As ADODB.Recordset
        Dim oUI As clsMarsUI = New clsMarsUI


retry:
        Try
            sNew = InputBox("Please enter the new Event-Based Package name", Application.ProductName)

            If sNew.Length = 0 Then Return 0

            If clsMarsData.IsDuplicate("EventPackageAttr", "PackageName", sNew, True) Then
                Dim oRes As DialogResult = MessageBox.Show _
                ("An Event-Based Package with that name already exists in the system", _
                Application.ProductName, MessageBoxButtons.RetryCancel, _
                MessageBoxIcon.Exclamation)

                If oRes = DialogResult.Retry Then
                    GoTo retry
                Else
                    Return 0
                End If
            End If

            'copy package details

            oUI.BusyProgress(20, "Copying package attributes")

            NewPackID = clsMarsData.DataItem.CopyRow("EventPackageAttr", nPackID, sWhere, "EventPackID", "PackageName", _
            sNew, , , , , , , , nParent)

            If NewPackID = 0 Then GoTo RollbackTran


            oUI.BusyProgress(40, "Copying package schedule details")

            nScheduleID = clsMarsData.DataItem.CopyRow("ScheduleAttr", 0, sWhere, "ScheduleID", , , , , , , , , , , NewPackID)

            If nScheduleID = 0 Then GoTo RollbackTran

            oUI.BusyProgress(80, "Copying custom tasks")

            oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
            "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr WHERE " & _
            "EventPackID = " & nPackID & ")")

            Do While oRs.EOF = False

                If clsMarsData.DataItem.CopyRow("Tasks", oRs(0).Value, " WHERE TaskID =" & oRs(0).Value, "TaskID", , , , _
                  , , , nScheduleID, , , , NewPackID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy event based schedules
            oRs = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID)

            oUI.BusyProgress(85, "Copying schedules...")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False

                    Me.PasteSchedule(oRs.Fields(0).Value, NewPackID, "Package", NewPackID, False)
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If


        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GoTo RollbackTran
        End Try

        oUI.BusyProgress(100, "Cleaning up")

        oUI.BusyProgress(, , True)

        Return NewPackID
RollbackTran:
        With clsMarsData.DataItem
            .WriteData("DELETE FROM EventPackageAttr WHERE EventPackID = " & NewPackID)
            .WriteData("DELETE FROM ScheduleAttr WHERE EventPackID = " & NewPackID)
            .WriteData("DELETE FROM Tasks WHERE ScheduleID = " & nScheduleID)
            .WriteData("DELETE FROM EventAttr6 WHERE PackID =" & NewPackID)
        End With

        oUI.BusyProgress(, , True)

        Return 0
    End Function
    Public Sub PasteSchedule(ByVal nEventID As Integer, ByVal nParent As Integer, ByVal parentType As String, Optional ByVal PackID As Integer = 0, Optional ByVal promptName As Boolean = True)
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Int64
        Dim oRs As ADODB.Recordset
        Dim sOld As String = ""
        Dim sName As String = ""

        oRs = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID =" & nEventID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                sOld = oRs.Fields(0).Value
                'sType = oRs(1).Value
            End If

            oRs.Close()
        End If

Retry:
        If promptName = True Then
            sName = InputBox("Please enter the new event-based schedule name", _
            Application.ProductName, "Copy of " & sOld)

            If sName.Length = 0 Then Return

            If clsMarsData.IsDuplicate("EventAttr", "EventName", sName, False) = True Then
                If MessageBox.Show("An event-based schedule with this name already exists.", Application.ProductName, _
                 MessageBoxButtons.RetryCancel) = DialogResult.Retry Then
                    GoTo Retry
                Else
                    Return
                End If
            End If
        Else
            sName = sOld
        End If

        If parentType.ToLower = "folder" Then
            nID = clsMarsData.DataItem.CopyRow("EventAttr6", nEventID, " WHERE EventID = " & nEventID, "EventID", "EventName", sName, , PackID, , nEventID, _
            , , , nParent)
        Else
            nID = clsMarsData.DataItem.CopyRow("EventAttr6", nEventID, " WHERE EventID = " & nEventID, "EventID", "EventName", sName, , nParent, , nEventID, _
            , , , 0)
        End If

        SQL = "SELECT * FROM EventConditions WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim cols, vals As String
                Dim newID As Integer = clsMarsData.CreateDataID("eventconditions", "conditionid")
                Dim conditionName As String = oRs("conditionName").Value

                Dim cacheFolderName As String = conditionName & "." & newID  'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())

                cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,ConnectionString,KeyColumn,DetectInserts,FilePath," & _
                "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll,OrderID," & _
                "PollingInterval,LastPolled,RedirectMail,RedirectTo,ForwardMail," & _
                "serverPort,UseSSL,MailServerType,IMAPBox,UseConstraint,ConstraintValue,RunOnce,IgnoreNow,DataLastModified," & _
                "SaveAttachments,AttachmentsPath,CacheFolderName,Status,RunForEachFile"

                vals = newID & "," & _
                nID & ",ConditionName,ConditionType,ReturnValue,ConnectionString,KeyColumn,DetectInserts,FilePath," & _
                "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll,OrderID," & _
                "PollingInterval,LastPolled,RedirectMail,RedirectTo,ForwardMail," & _
                "serverPort,UseSSL,MailServerType,IMAPBox,UseConstraint,ConstraintValue,RunOnce,IgnoreNow,DataLastModified," & _
                "SaveAttachments,AttachmentsPath,'" & SQLPrepare(cacheFolderName) & "',Status,RunForEachFile"


                SQL = "INSERT INTO EventConditions (" & cols & ") " & _
                "SELECT " & vals & " FROM EventConditions WHERE ConditionID =" & oRs("conditionid").Value

                If clsMarsData.WriteData(SQL) = False Then
                    Exit Do
                End If

                If oRs("conditiontype").Value = "datbase record exists" Then
                    CacheData(sName, oRs("searchcriteria").Value, oRs("connectionstring").Value, cacheFolderName)
                ElseIf oRs("conditiontype").Value = "database record has changed" Then
                    CacheData(sName, oRs("searchcriteria").Value, oRs("connectionstring").Value, oRs("keycolumn").Value, cacheFolderName)
                End If

                Dim ConditionID As Integer = oRs.Fields("conditionid").Value

                clsMarsData.DataItem.CopyRow("ForwardMailAttr", ConditionID, " WHERE ConditionID =" & ConditionID, _
                "ConditionID")

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "SELECT * FROM ReportAttr WHERE PackID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim reportID As Integer = oRs("reportid").Value

                reportID = clsMarsReport.oReport.CopyReport(reportID, 0, True)

                clsMarsData.WriteData("UPDATE ReportAttr SET PackID =" & nID & " WHERE ReportID =" & reportID)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


        SQL = "SELECT * FROM EventSchedule WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        sCols = "ID,EventID,ScheduleID,ScheduleType,Status"

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim scheduleid As Integer
                Dim scheduletype, status As String

                scheduleid = oRs("scheduleid").Value
                scheduletype = oRs("scheduletype").Value
                status = IsNull(oRs("status").Value, 1)

                sVals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                nID & "," & _
               scheduleid & "," & _
               "'" & scheduletype & "'," & _
               "'" & status & "'"

                SQL = "INSERT INTO EventSchedule " & _
                "(" & sCols & ") VALUES (" & sVals & ")"

                clsMarsData.WriteData(SQL)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE ScheduleID =" & nEventID)

        Do While oRs.EOF = False

            clsMarsData.DataItem.CopyRow(" Tasks", oRs(0).Value, " WHERE TaskID =" & oRs(0).Value, "TaskID", , , , _
            , , , nID)

            oRs.MoveNext()
        Loop

        oRs.Close()

        If promptName = True Then
            Dim oForm As New frmEventProp

            oForm.txtName.Text = sName

            oForm.cmdCancel.Visible = False

            oForm.EditSchedule(nID)
        End If
    End Sub

    Public Sub RunEvents(Optional ByVal EventID As Integer = 0)
        Dim SQL As String
        Dim sType As String
        Dim CheckValue As Boolean
        Dim sPath As String
        Dim sCon As String
        Dim d As Date
        Dim oTest As ADODB.Recordset
        Dim oCon As ADODB.Connection
        Dim s1 As String
        Dim s2 As String
        Dim Proceed As Boolean
        Dim Success As Boolean
        Dim oParse As New clsMarsParser
        Dim nRecords As Integer = 0
        Dim oUI As New clsMarsUI


        If EventID = 0 Then
            SQL = "SELECT * FROM EventAttr WHERE Status = 1"
        Else
            SQL = "SELECT * FROM EventAttr WHERE EventID =" & EventID
        End If

        oUI.BusyProgress(10, "Collecting conditions...")

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False

                Proceed = False

                sType = oRs("eventtype").Value
                EventID = oRs("eventid").Value
                gScheduleName = oRs("eventname").Value

                Try
                    CheckValue = Convert.ToBoolean(oRs("firewhen").Value)
                Catch ex As Exception
                    CheckValue = True
                End Try

                oUI.BusyProgress(20, "Obtaining event attributes...")


                Select Case sType.ToLower
                    Case "file exists"

                        oUI.BusyProgress(30, "Condition: IF File Exists =" & CheckValue.ToString)

                        sPath = oParse.ParseString(oRs("filepath").Value)

                        If File.Exists(sPath) = CheckValue Then
                            Proceed = True
                        Else
                            Proceed = False
                        End If

                    Case "file has been modified"

                        oUI.BusyProgress(30, "Condition: IF File Has Been Modified =" & CheckValue.ToString)

                        sPath = oRs("filepath").Value

                        Dim OriginalDate As Date = oRs("moddatetime").Value

                        d = File.GetLastWriteTime(sPath)

                        OriginalDate = ConDate(OriginalDate) & " " & ConTime(OriginalDate)

                        d = ConDate(d) & " " & ConTime(d)

                        If (d <> OriginalDate) = CheckValue Then
                            Proceed = True

                            'Try
                            '    ExecuteSchedule(gScheduleName, EventID)
                            'Catch : End Try

                            clsMarsData.WriteData("UPDATE EventAttr SET ModDateTime = " & _
                            "'" & d & "' WHERE EventID =" & EventID)
                        Else
                            Proceed = False
                        End If

                    Case "database record exists"

                        oUI.BusyProgress(30, "Condition: IF Database Record Exists =" & CheckValue.ToString)

                        sCon = oRs("search1").Value

                        Dim sQuery As String = oRs("search2").Value
                        Dim sDsn As String = sCon.Split("|")(0)
                        Dim sUser As String = sCon.Split("|")(1)
                        Dim sPassword As String = sCon.Split("|")(2)
                        Dim oRsOld As ADODB.Recordset
                        Dim sDataPath As String
                        Dim sKey As String

                        sKey = IsNull(oRs("filepath").Value, "")

                        oCon = New ADODB.Connection
                        oTest = New ADODB.Recordset
                        oCon.ConnectionTimeout = 0
                        oCon.CommandTimeout = 0
                        oCon.Open(sDsn, sUser, sPassword)

                        oTest.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

                        sDataPath = clsMarsEvent.m_CachedDataPath & "dbe_" & gScheduleName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        sDataPath &= "\datacache.xml"

                        If IO.File.Exists(sDataPath) = True And sKey.Length > 0 Then
                            oRsOld = clsMarsData.GetXML(sDataPath)

                            Do While oTest.EOF = False

                                oRsOld.Filter = ""

                                If oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adDate Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adDBDate Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adLongVarChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adLongVarWChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adVarChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adVarWChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adWChar Then
                                    oRsOld.Filter = sKey & "='" & SQLPrepare(oTest.Fields(sKey).Value) & "'"
                                Else
                                    oRsOld.Filter = sKey & "=" & SQLPrepare(oTest.Fields(sKey).Value)
                                End If

                                If oRsOld.EOF = CheckValue Then
                                    Proceed = True
                                    nRecords += 1
                                End If

                                oTest.MoveNext()
                            Loop
                        Else
                            If oTest.EOF = Not CheckValue Then
                                Proceed = True
                                nRecords = oTest.RecordCount
                            Else
                                Proceed = False
                            End If
                        End If

                        clsMarsData.CreateXML(oTest, sDataPath)

                    Case "database record has changed"

                        oUI.BusyProgress(30, "Condition: IF Database Record Has Been Modified =" & CheckValue.ToString)

                        sCon = oRs("search1").Value

                        Dim sQuery As String = oRs("search2").Value
                        Dim oReturn() As Object
                        Dim dataMod As Boolean
                        'oReturn = HasDataChanged(oRs("eventname").Value, sQuery, sCon, oRs("filepath").Value, _
                        'Convert.ToBoolean(oRs("popremove").Value), True, dataMod)

                        If Not oReturn Is Nothing Then
                            If oReturn(0) = CheckValue Then
                                Proceed = True
                                nRecords = oReturn(1)
                            Else
                                Proceed = False
                            End If
                        End If

                    Case "window is present"

                        oUI.BusyProgress(30, "Condition: IF Window Is Present =" & CheckValue.ToString)

                        s1 = oRs("search1").Value

                        For Each oProc As Process In Process.GetProcesses
                            If oProc.MainWindowTitle.Length > 0 Then
                                If (oProc.MainWindowTitle.ToLower. _
                                IndexOf(s1.ToLower) > -1) = CheckValue Then
                                    Proceed = True
                                    Exit For
                                Else
                                    Proceed = False
                                End If
                            End If
                        Next
                    Case "process exists"

                        oUI.BusyProgress(30, "Condition: IF Process Exists =" & CheckValue.ToString)

                        s1 = oRs("search1").Value

                        For Each oProc As Process In Process.GetProcesses
                            If oProc.ProcessName.Length > 0 Then
                                If (oProc.ProcessName.ToLower. _
                                IndexOf(s1.ToLower) > -1) = CheckValue Then
                                    Proceed = True
                                    Exit For
                                Else
                                    Proceed = False
                                End If
                            End If
                        Next
                    Case "unread email is present"

                        oUI.BusyProgress(30, "Condition: IF Unread Email Exists =" & CheckValue.ToString)

                        Dim oMsg As New clsMarsMessaging
                        Dim sServer As String
                        Dim sUser As String
                        Dim sPassword As String
                        Dim Found As Boolean
                        Dim Remove As Boolean

                        sServer = oRs("popserver").Value
                        sUser = oRs("popuserid").Value
                        sPassword = oRs("poppassword").Value
                        s1 = oRs("search1").Value
                        s2 = oRs("search2").Value

                        Try
                            Remove = Convert.ToBoolean(oRs("popremove").Value)
                        Catch ex As Exception
                            Remove = False
                        End Try

                        Found = oMsg.DownloadMessageToMemoryStream(sServer, sUser, sPassword, Remove, _
                        s1, s2)

                        If Found = CheckValue Then
                            Proceed = True
                        Else
                            Proceed = False
                        End If
                End Select

                If Proceed = True Then

                    oUI.BusyProgress(70, "Executing schedule...")

                    If nRecords = 0 Then nRecords = 1

                    For I As Integer = 1 To nRecords
                        Success = ExecuteSchedule(gScheduleName, EventID)
                    Next

                    If Success = True Or gErrorDesc.Length = 0 Or gErrorNumber = 0 Then
                        Dim oTask As New clsMarsTask

                        oTask.ProcessTasks(EventID, "NONE")

                        SaveEventHistory(EventID, Success)
                    Else
                        SaveEventHistory(EventID, Success, gErrorDesc & _
                        " [" & gErrorNumber & "]")
                    End If

                    If sType = "file has been modified" Then
                        clsMarsData.WriteData("UPDATE EventAttr SET ModDateTime = " & _
                        "'" & d & "' WHERE EventID =" & EventID)
                    End If
                End If

                oRs.MoveNext()
            Loop

            oUI.BusyProgress(100, "")

            oUI.BusyProgress(, , True)

            oRs.Close()
        Catch ex As Exception
            ''console.writeLine(ex.Message)

            oUI.BusyProgress(, , True)
        End Try

    End Sub

    Shared procs As System.Collections.Generic.List(Of Process) = New System.Collections.Generic.List(Of Process)

    Public Shared Function ExecuteSchedule(ByVal sEvent As String, ByVal nID As Integer, Optional objectID As Integer = 0) As Boolean
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sType As String
        Dim oReport As New clsMarsReport
        Dim oAuto As New clsMarsAutoSchedule
        Dim nKey As Integer
        Dim Success As Boolean
        Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Integer
        Dim scheduleName As String
        Dim async As Boolean
        Dim maxAsyncThreads As Integer = 2
        Dim pc As System.Diagnostics.ProcessPriorityClass = ProcessPriorityClass.Normal

        If objectID = 0 Then
            SQL = "SELECT * FROM EventSchedule WHERE EventID =" & nID
        Else
            SQL = "SELECT * FROM EventSchedule WHERE EventID =" & nID & " AND scheduleid =" & objectID
        End If

        oRs = clsMarsData.GetData(SQL)

        If objectID <> 0 Then '//its already in an external process
            async = False
        Else
            Dim evt As EventBased = New EventBased(nID)
            async = evt.executeExistingAsync
            maxAsyncThreads = evt.maxAsyncthreads + 1

            Try
                pc = evt.schedulePriorityClass
            Catch : End Try
        End If

        Try
            Do While oRs.EOF = False
                sType = oRs("scheduletype").Value
                nKey = oRs("scheduleid").Value

                If async = False Then
                    Select Case sType.ToLower
                        Case "report"
                            Dim type As String = ""

                            ScheduleStart = Now

                            SQL = "SELECT Dynamic, Bursting,ReportTitle,IsDataDriven FROM ReportAttr WHERE ReportID =" & nKey

                            Dim oTest As ADODB.Recordset = clsMarsData.GetData(SQL)

                            'If gSubject.Length > 0 Then
                            '    gRunByEvent = True
                            '    gEventID = nID
                            'End If

                            scheduleName = oTest.Fields("reporttitle").Value

                            If IsNull(oTest.Fields(0).Value, 0) = 1 Then
                                Success = oReport.RunDynamicSchedule(nKey, False)
                                type = " - Dynamic schedule '"
                            ElseIf IsNull(oTest.Fields(3).Value, 0) = 1 Then
                                Success = oReport.RunDataDrivenSchedule(nKey)
                                type = " - Data-Driven schedule '"
                            Else
                                Success = oReport.RunSingleSchedule(nKey, False)
                                type = " - Single schedule '"
                            End If


                            If Success = True Then
                                oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, nKey)
                                SaveEventHistory(nID, True, type & scheduleName & "' executed successfully.")
                            Else
                                oSchedule.SetScheduleHistory(False, _
                                "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                                gErrorDesc & " [" & gErrorNumber & "]", nKey)
                                SaveEventHistory(nID, False, type & scheduleName & "' : " & gErrorDesc & " [" & gErrorNumber & "]")
                            End If

                        Case "package"

                            ScheduleStart = Now
                            Dim oTest As ADODB.Recordset = clsMarsData.GetData("SELECT PackageName FROM PackageAttr WHERE PackID =" & nKey)

                            If oTest IsNot Nothing Then
                                If oTest.EOF = False Then
                                    scheduleName = oTest.Fields("packagename").Value
                                End If

                                oTest.Close()
                            End If

                            Dim type As String

                            If clsMarsData.IsScheduleDynamic(nKey, "Package") = True Then
                                Success = oReport.RunDynamicPackageSchedule(nKey)
                                type = " - Dynamic Package schedule '"
                            ElseIf clsMarsData.IsScheduleDataDriven(nKey, "Package") Then
                                Success = oReport.RunDataDrivenPackage(nKey)
                                type = " - Data-Driven Package '"
                            Else
                                Success = oReport.RunPackageSchedule(nKey)
                                type = " - Package schedule '"
                            End If

                            If Success = True Then
                                oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, _
                                , nKey)
                                SaveEventHistory(nID, True, type & scheduleName & "' executed successfully.")
                            Else
                                oSchedule.SetScheduleHistory(False, _
                                "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                                gErrorDesc & " [" & gErrorNumber & "]", , nKey)
                                SaveEventHistory(nID, False, type & scheduleName & "' : " & gErrorDesc & " [" & gErrorNumber & "]")
                            End If
                        Case "automation"
                            Success = True
                            oAuto.ExecuteAutomationSchedule(nKey)
                            nScheduleID = clsMarsScheduler.GetScheduleID(, , nKey)

                            Dim oTest As ADODB.Recordset = clsMarsData.GetData("SELECT Autoname FROM AutomationAttr WHERE AutoID =" & nKey)

                            If oTest IsNot Nothing Then
                                If oTest.EOF = False Then
                                    scheduleName = oTest.Fields("autoname").Value
                                End If

                                oTest.Close()
                            End If
                            If Success = True Then
                                oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, _
                                , , nKey)
                                SaveEventHistory(nID, True, " - Automation schedule '" & scheduleName & "' executed successfully.")
                            Else
                                oSchedule.SetScheduleHistory(False, _
                                "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                                gErrorDesc & " [" & gErrorNumber & "]", , , nKey)
                                SaveEventHistory(nID, False, "Automation schedule '" & scheduleName & "': " & gErrorDesc & " [" & gErrorNumber & "]")
                            End If
                    End Select
                Else

                    clsMarsUI.BusyProgress(80, "Running schedules async...", False)

                    Dim maxthreads As Integer = clsMarsUI.MainUI.ReadRegistry("ThreadCount", 8) + 1
                    Dim threadCount As Integer = clsMarsThreading.GetThreadCount
                    Dim asyncCount As Integer = asyncProcessCount()

                    Do While (asyncCount >= maxAsyncThreads) Or (threadCount >= maxthreads)
                        clsMarsUI.BusyProgress(50, "Waiting on free thread...")
                        Application.DoEvents()
                        threadCount = clsMarsThreading.GetThreadCount
                        asyncCount = asyncProcessCount()
                    Loop

                    Dim dt As DataTable = serializeObjectsForAsync()
                    Dim path As String = asyncPath & Guid.NewGuid.ToString & ".xml"
                    dt.WriteXml(path, XmlWriteMode.WriteSchema)

                    Dim p As Process = New Process
                    p.StartInfo.FileName = Application.ExecutablePath
                    p.StartInfo.Arguments = "eventbasedasync " & Chr(34) & "eventid=" & nID & "|eventname=" & sEvent & "|objectid=" & nKey & "|asyncdata=" & path & Chr(34)
                    p.Start()

                    Try
                        p.PriorityClass = pc
                    Catch : End Try

                    procs.Add(p)

                    System.Threading.Thread.Sleep(1000)
                    Success = True
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
           
            Return Success
        Catch ex As Exception
            gErrorNumber = Err.Number
            gErrorDesc = ex.Message
            Return False
        End Try
    End Function

    Private Shared Function asyncProcessCount() As Integer
        Dim count As Integer = 0

        For Each p As Process In procs
            Try
                If p.HasExited = False Then
                    count += 1
                End If
            Catch : End Try
        Next

        Return count
    End Function
    Public Shared ReadOnly Property asyncPath As String
        Get
            Dim s As String = sAppDataPath & "\Async\"

            If IO.Directory.Exists(s) = False Then
                IO.Directory.CreateDirectory(s)
            End If

            Return s
        End Get
    End Property

    Private Shared Function serializeObjectsForAsync() As DataTable
        'Shared nPointers() As Object
        'Shared matchedMail As ArrayList
        'Shared pointer As Integer
        'Public Shared m_resultsTable As Hashtable
        'Public Shared m_EventDataCache() As DataTable
        'Public Shared m_portDataCache As DataTable
        Dim dt As DataTable = New DataTable("currentobjects")

        With dt.Columns
            .Add("variable")
            .Add("value")
        End With

        Dim r As DataRow

        '//npointers
        r = dt.Rows.Add
        r("variable") = "npointers"
        r("value") = serializeObject(nPointers)

        '//matchedmail
        r = dt.Rows.Add
        r("variable") = "matchedmail"
        r("value") = serializeObject(matchedMail)

        '//pointer
        r = dt.Rows.Add
        r("variable") = "m_pointer"
        r("value") = m_Pointer.ToString

        '//m_resultstable
        r = dt.Rows.Add
        r("variable") = "m_resultstable"
        r("value") = serializeObject(m_resultsTable)

        '//m_eventdatacache
        r = dt.Rows.Add
        r("variable") = "m_eventdatacache"
        r("value") = serializeObject(m_EventDataCache)

        '//m_portdatacache
        r = dt.Rows.Add
        r("variable") = "m_portdatacache"
        r("value") = serializeObject(m_portDataCache)

        Return dt
    End Function

    Public Shared Function deserializeObjectsForAsync(dt As DataTable) As Boolean
        For Each r As DataRow In dt.Rows
            Dim variable, value As String

            variable = r("variable")
            value = r("value")

            Select Case variable
                Case "npointers"
                    nPointers = deserializeObject(value)
                Case "matchedmail"
                    matchedMail = deserializeObject(value)
                Case "m_pointer"
                    m_Pointer = value
                Case "m_resultstable"
                    m_resultsTable = deserializeObject(value)
                Case "m_eventdatacache"
                    m_EventDataCache = deserializeObject(value)
                Case "m_portdatacache"
                    m_portDataCache = deserializeObject(value)
            End Select
        Next
    End Function
    Public Shared Function serializeObject(ByVal item As Object) As String
        Try
            If item Is Nothing Then Return ""

            Dim cacheFile As String = asyncPath & Guid.NewGuid.ToString & ".obj"

            If IO.File.Exists(cacheFile) Then
                IO.File.Delete(cacheFile)
            End If

            Dim fs As IO.FileStream = New IO.FileStream(cacheFile, FileMode.Create, FileAccess.ReadWrite)
            Dim bf As BinaryFormatter = New BinaryFormatter

            bf.Serialize(fs, item)

            fs.Close()

            Return cacheFile
        Catch : End Try
    End Function

    Public Shared Function deserializeObject(ByVal cacheFile As String) As Object
        Try
            If cacheFile = "" Then Return Nothing

            Dim fs As FileStream = New FileStream(cacheFile, FileMode.Open, FileAccess.Read)

            Dim obj As Object
            Dim bf As BinaryFormatter = New BinaryFormatter

            obj = bf.Deserialize(fs)

            fs.Close()

            Return obj
        Catch
            Return Nothing
        End Try
    End Function
    Public Shared Sub kickOffExistingAsync(args As String) '//example "eventid=1000|eventname=steve amani|objectid=1000"
        Dim eventid As Integer
        Dim eventName As String
        Dim objectid As Integer
        Dim asyncDataPath As String

        For Each arg As String In args.Split("|")
            Dim key, value As String

            If arg.Contains("=") Then
                key = arg.Split("=")(0)
                value = arg.Split("=")(1)

                Select Case key.ToLower
                    Case "eventid"
                        eventid = value
                    Case "eventname"
                        eventName = value
                    Case "objectid"
                        objectid = value
                    Case "asyncdata"
                        asyncDataPath = value
                End Select
            End If
        Next

        Dim dt As DataTable

        If asyncDataPath <> "" AndAlso IO.File.Exists(asyncDataPath) Then
            dt = New DataTable()
            dt.ReadXml(asyncDataPath)

            deserializeObjectsForAsync(dt)
        End If

        currentEventID = eventid

        ExecuteSchedule(eventName, eventid, objectid)

        If dt IsNot Nothing Then
            Try
                For Each r As DataRow In dt.Rows
                    Try
                        IO.File.Delete(r("value"))
                    Catch : End Try
                Next

                IO.File.Delete(asyncDataPath)
            Catch : End Try
        End If
    End Sub

    Public Shared Sub SaveEventHistory(ByVal nID As Integer, ByVal Status As Boolean, _
    Optional ByVal sMsg As String = "", Optional ByVal MasterHistoryID As Integer = 0)
        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim lastFired As Date

        sCols = "HistoryID,EventID,LastFired,Status,ErrMsg,StartDate,MasterHistoryID"

        If RunEditor = True Then
            lastFired = CTimeZ(Now, dateConvertType.WRITE)
        Else
            lastFired = Now
        End If

        sVals = clsMarsData.CreateDataID("eventhistory", "historyid") & "," & _
        nID & "," & _
        "'" & ConDateTime(lastFired) & "'," & _
        Convert.ToInt32(Status) & "," & _
        "'" & SQLPrepare(sMsg) & "'," & _
        "'" & ConDateTime(m_StartTime) & "'," & _
        MasterHistoryID

        SQL = "INSERT INTO EventHistory (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL, False, False) = False Then

        End If
    End Sub

    Public Sub DrawEventHistory(ByVal oTree As TreeView, _
    Optional ByVal nID As Integer = 0, Optional ByVal SortOrder As String = "ASC", _
    Optional ByVal Filter As Boolean = False)
        Dim SQL As String
        Dim sWhere As String = String.Empty
        Dim oRs As ADODB.Recordset
        Dim oChild As ADODB.Recordset
        Dim oMain As ADODB.Recordset

        oTree.Nodes.Clear()

        If nID > 0 Then
            sWhere = " WHERE EventID =" & nID
        End If

        If nID = 0 Then
            SQL = "SELECT EventID, EventName FROM EventAttr6 WHERE (PackID IS NULL OR PackID = 0)"

            If Filter = True Then
                SQL &= " AND Status = 1"
            End If
        Else
            SQL = "SELECT EventID, EventName FROM EventAttr6 WHERE EventID =" & nID

            If Filter = True Then
                SQL &= " AND Status =1"
            End If
        End If



        oMain = clsMarsData.GetData(SQL)

        Try
            Do While oMain.EOF = False

                Dim ParentNode As TreeNode = New TreeNode

                ParentNode.Text = oMain(1).Value
                ParentNode.ImageIndex = 0
                ParentNode.SelectedImageIndex = 0

                nID = oMain(0).Value

                SQL = "SELECT * FROM EventHistory WHERE EventID =" & nID & " ORDER BY LastFired " & SortOrder

                oChild = clsMarsData.GetData(SQL)

                Try
                    Do While oChild.EOF = False
                        Dim oChildNode As TreeNode = New TreeNode

                        If oChild("status").Value = 1 Then

                            oChildNode.Text = "Last Fired: " & CTimeZ(oChild("lastfired").Value, dateConvertType.READ) & " - [Success " & IsNull(oChild("errmsg").Value) & "]"
                            oChildNode.ImageIndex = 1
                            oChildNode.SelectedImageIndex = 1
                        Else
                            oChildNode.Text = "Last Fired: " & CTimeZ(oChild("lastfired").Value, dateConvertType.READ) & " [Failed " & oChild("errmsg").Value & "]"
                            oChildNode.ImageIndex = 2
                            oChildNode.SelectedImageIndex = 2
                        End If

                        ParentNode.Nodes.Add(oChildNode)
                        oChild.MoveNext()
                    Loop
                    oChild.Close()
                Catch : End Try

                oTree.Nodes.Add(ParentNode)

                oMain.MoveNext()
            Loop

            oMain.Close()

            oTree.Nodes(0).Expand()
        Catch : End Try
    End Sub
    Public Shared Function HasDataChanged(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, _
    ByVal sKeyColumn As String, ByVal IncludeNewData As Boolean, ByVal returnValue As Boolean, _
    ByRef dataModified As Boolean, ByVal cacheFolderName As String) As Object()
        Dim sPath As String
        Dim sKeyValue As String
        Dim sText As String
        Dim nCode As Int64
        Dim nRecords As Integer = 0
        Dim oReturn() As Object
        Dim HasChanged As Boolean
        Dim I As Integer = 0
        Dim z As Integer = 1

        Try
            sPath = clsMarsEvent.m_CachedDataPath

            If System.IO.Directory.Exists(sPath) = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            sPath &= cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

            If System.IO.Directory.Exists(sPath) = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            If System.IO.File.Exists(sPath & "\data.xml") = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            Dim oXML As New ADODB.Recordset

            oXML = clsMarsData.GetXML(sPath & "\data.xml")

            Dim oRs As New ADODB.Recordset
            Dim oCon As New ADODB.Connection

            oCon.ConnectionTimeout = 0
            oCon.CommandTimeout = 0
            oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 64)

            If oRs IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

                clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oRs)

                If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
                    clsMarsEvent.m_EventDataCache(ubound).TableName = sName
                End If
            End If

            oRs.Close()
            oRs.Open()

            Do While oRs.EOF = False
                HasChanged = False

                clsMarsUI.MainUI.BusyProgress((z / oRs.RecordCount) * 100, "Checking records...")
                z += 1

                sText = String.Empty

                sKeyValue = IsNull(oRs(sKeyColumn).Value)

                For x As Integer = 0 To oRs.Fields.Count - 1
                    If oRs.Fields(x).Name <> sKeyColumn Then
                        Try
                            sText &= IsNull(oRs(x).Value) & "|"
                        Catch : End Try
                    End If
                Next

                nCode = sText.GetHashCode

                oXML.Filter = "IDColumn = '" & SQLPrepare(sKeyValue) & "'"

                If oXML.EOF = True Then
                    If IncludeNewData = True Then
                        HasChanged = True
                        dataModified = True
                    End If
                Else
                    If oXML.Fields("HashValue").Value <> nCode Then
                        HasChanged = True
                        dataModified = True
                    End If
                End If

                If HasChanged = returnValue Then
                    ReDim Preserve oReturn(I)
                    oReturn(I) = sKeyValue
                    I += 1
                End If

                oRs.MoveNext()
            Loop

            Try
                'lets detect deleted columns
                oXML.MoveFirst()
                oRs.MoveFirst()
                z = 0
                oXML.Filter = ""
                ''console.writeLine(oXML.RecordCount)

                If IncludeNewData = True Then
                    Do While oXML.EOF = False
                        HasChanged = False
                        clsMarsUI.MainUI.BusyProgress((z / oRs.RecordCount) * 100, "Checking records...")
                        z += 1
                        sText = ""

                        'get the key value
                        sKeyValue = IsNull(oXML("IDColumn").Value)
                        ''console.writeLine(sKeyValue)
                        'get the current hash code
                        Dim hashCode = oXML("hashvalue").Value

                        oRs.Filter = sKeyColumn & " = '" & SQLPrepare(sKeyValue) & "'"

                        If oRs.EOF = True Then
                            If IO.File.Exists(sPath & "deletedkeys.dat") = True Then
                                Dim prevValues As String = ReadTextFromFile(sPath & "\deletedkeys.dat")
                                Dim dup As Boolean = False

                                For Each s As String In prevValues.Split(";")
                                    If s = sKeyValue Then
                                        dup = True
                                        Exit For
                                    End If
                                Next

                                If dup = False Then
                                    HasChanged = True
                                    dataModified = True
                                End If
                            Else
                                HasChanged = True
                                dataModified = True

                                SaveTextToFile(sKeyValue & ";", sPath & "\deletedkeys.dat", , True, False)
                            End If
                        End If

                        If HasChanged = returnValue Then
                            ReDim Preserve oReturn(I)
                            oReturn(I) = sKeyValue
                            I += 1
                        End If

                        oXML.MoveNext()
                        oRs.Filter = ""
                    Loop

                End If
            Catch : End Try

            If (oReturn IsNot Nothing) Then
                CacheData(sName, sQuery, sCon, sKeyColumn, cacheFolderName)
            End If

            oRs.Close()
            oCon.Close()

            Return oReturn
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return Nothing
        End Try


    End Function

    Public Overloads Shared Function CacheData(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, _
    ByVal sKeyColumn As String, ByVal cacheFolderName As String) As Boolean
        Try
            Dim oRs As New ADODB.Recordset
            Dim oCon As New ADODB.Connection
            Dim sKeyValue As String
            Dim sText As String
            Dim nCode As Int64
            Dim oXML As New ADODB.Recordset

            sKeyColumn = sKeyColumn.Replace("[", "").Replace("]", "")
            oCon.ConnectionTimeout = 0
            oCon.CommandTimeout = 0
            oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, 64)

            oXML.Fields.Append("IDColumn", ADODB.DataTypeEnum.adVarChar, 255)
            oXML.Fields.Append("HashValue", ADODB.DataTypeEnum.adInteger)
            oXML.Open()

            Do While oRs.EOF = False
                sText = String.Empty

                sKeyValue = IsNull(oRs(sKeyColumn).Value)

                For I As Integer = 0 To oRs.Fields.Count - 1

                    If oRs.Fields(I).Name <> sKeyColumn Then
                        Try
                            sText &= IsNull(oRs(I).Value) & "|"
                        Catch : End Try
                    End If
                Next

                nCode = sText.GetHashCode

                oXML.AddNew()
                oXML.Fields(0).Value = sKeyValue
                oXML.Fields(1).Value = nCode
                oXML.Update()

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim sPath As String

            sPath = clsMarsEvent.m_CachedDataPath

            If System.IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

            sPath &= cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

            If System.IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

            clsMarsData.CreateXML(oXML, sPath & "\data.xml")

            oCon.Close()

            oCon = Nothing

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        End Try
    End Function

    Public Overloads Shared Sub CacheData(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, ByVal cacheFolderName As String)
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection
        oCon.ConnectionTimeout = 0
        oCon.CommandTimeout = 0
        oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

        oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, 64)

        Dim sPath As String = clsMarsEvent.m_CachedDataPath

        If IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

        sPath &= "dbe_" & cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

        If IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

        clsMarsData.CreateXML(oRs, sPath & "\datacache.xml")

        oCon.Close()

        oCon = Nothing
    End Sub

    Public Shared Sub DeleteEvent(ByVal eventID As Integer)
        Dim SQL As String

        SQL = "DELETE FROM EventSchedule WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        SQL = "SELECT ConditionID FROM EventConditions WHERE EventID =" & eventID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim conditionID As Integer = oRs(0).Value

                DeleteCondition(conditionID)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "DELETE FROM EventAttr6 WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM EventHistory WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID =" & eventID, False)

        oRs = Nothing
    End Sub

    Public Shared Sub DeleteEventPackage(ByVal nPackID As Integer)
        Dim SQL As String

        Dim nScheduleID As Integer
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler

        nScheduleID = clsMarsScheduler.GetScheduleID(, , , , nPackID)

        SQL = "DELETE FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ScheduleAttr WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM Tasks WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                DeleteEvent(oRs("eventid").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "DELETE FROM EventPackageAttr WHERE EventPackID =" & nPackID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ScheduleHistory WHERE EventPackID =" & nPackID

        clsMarsData.WriteData(SQL)

    End Sub
    Public Shared Sub DeleteCondition(ByVal conditionID As Integer)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim conditionType As String
        Dim conditionName As String
        Dim path As String = clsMarsEvent.m_CachedDataPath

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                conditionType = oRs("conditiontype").Value
                conditionName = oRs("conditionname").Value

                Select Case conditionType.ToLower
                    Case "database record exists"

                        path &= "dbe_" & conditionName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        If IO.Directory.Exists(path) Then
                            IO.Directory.Delete(path, True)
                        End If

                    Case "database record has been modified"
                        path &= conditionName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        If IO.Directory.Exists(path) Then
                            IO.Directory.Delete(path, True)
                        End If
                End Select
            End If
        End If

        SQL = "DELETE FROM EventConditions WHERE ConditionID =" & conditionID

        clsMarsData.WriteData(SQL)
    End Sub

    Public Shared Function IsConditionDuplicate(ByVal eventID As Integer, ByVal conditionName As String) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM EventConditions WHERE eventID =" & eventID & " AND ConditionName ='" & SQLPrepare(conditionName) & "'"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                oRs.Close()
                Return False
            Else
                oRs.Close()
                Return True
            End If
        End If
    End Function

    Public Shared Sub Convert5to6()
        Try
            Dim SQL As String = "SELECT * FROM EventAttr"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim eventID As Integer = oRs("eventid").Value
                Dim eventName As String = oRs("eventname").Value
                Dim conditionType As String = oRs("eventType").Value
                Dim fireWhen As Integer = oRs("firewhen").Value
                Dim newID As Integer = clsMarsData.CreateDataID("eventattr6", "eventid")

                'copy the main event
                Dim cols As String = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate,ScheduleType,AnyAll"
                Dim vals As String = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate,'Existing','ALL'"

                SQL = "INSERT INTO EventAttr6 (" & cols & ") " & _
                "SELECT " & vals & " FROM EventAttr WHERE EventID =" & eventID

                If clsMarsData.WriteData(SQL, False) = True Then
                    cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,OrderID,"

                    vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
                    eventID & "," & _
                    "'" & SQLPrepare(eventName) & "'," & _
                    "'" & conditionType & "'," & _
                    fireWhen & "," & _
                    0 & ","

                    Select Case conditionType.ToLower
                        Case "file exists"
                            cols &= "Filepath"

                            vals &= "FilePath"
                        Case "file has been modified"
                            cols &= "Filepath,KeyColumn"

                            vals &= "Filepath,ModDateTime"

                        Case "database record exists"
                            Dim connection As String
                            Dim dsn As String
                            Dim user As String
                            Dim password As String

                            connection = oRs("search1").Value

                            dsn = connection.Split("|")(0)
                            user = connection.Split("|")(1)
                            password = connection.Split("|")(2)

                            password = _EncryptDBValue(password)

                            connection = dsn & "|" & user & "|" & password

                            cols &= "ConnectionString,SearchCriteria,KeyColumn,DetectInserts"

                            vals &= "'" & SQLPrepare(connection) & "',search2,Filepath,0"

                        Case "window is present"

                            cols &= "SearchCriteria"

                            vals &= "search1"
                        Case "process exists"
                            cols &= "SearchCriteria"

                            vals &= "search1"
                        Case "unread email is present"
                            Dim popPassword As String = oRs("poppassword").Value

                            popPassword = _EncryptDBValue(popPassword)

                            cols &= "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll"

                            vals &= "'Message_CONTAINS_' + search1 + '|Subject_CONTAINS_' + search2,Popserver,Popuserid," & _
                            "'" & SQLPrepare(popPassword) & "',PopRemove,'ANY'"

                        Case "database record has changed"
                            Dim connection As String
                            Dim dsn As String
                            Dim user As String
                            Dim password As String

                            connection = oRs("search1").Value

                            dsn = connection.Split("|")(0)
                            user = connection.Split("|")(1)
                            password = connection.Split("|")(2)

                            password = _EncryptDBValue(password)

                            connection = dsn & "|" & user & "|" & password

                            cols &= "ConnectionString,SearchCriteria,KeyColumn,DetectInserts"

                            vals &= "'" & SQLPrepare(connection) & "',Search2,Filepath,Popremove"
                    End Select

                    SQL = "INSERT INTO EventConditions (" & cols & ") SELECT " & vals & " FROM EventAttr WHERE EventID =" & eventID

                    If clsMarsData.WriteData(SQL, False) = True Then
                        clsMarsData.WriteData("DELETE FROM EventAttr WHERE EventID =" & eventID, False)
                        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID=" & eventID, False)
                        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID NOT IN (SELECT EventID FROM EventAttr)", False)
                    End If
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Public Function RunEventPackage(ByVal nPackID As Integer) As Boolean
        Dim flowType As String = "AFTER"
        Dim packageName As String = ""
        'Dim logFile As String

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            gScheduleOwner = gScheduleOwner = clsMarsScheduler.getScheduleOwner(nPackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE)

            clsMarsUI.MainUI.BusyProgress(10, "Loading package information...")

            SQL = "SELECT * FROM EventPackageAttr WHERE EventPackID =" & nPackID

            oRs = clsMarsData.GetData(SQL)

            clsMarsScheduler.m_progressID = "Event-Package:" & nPackID

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then

                    Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(, , , , nPackID)
                    Dim ok As Boolean = False

                    packageName = oRs("packagename").Value
                    flowType = oRs("executionflow").Value

                    gScheduleName = packageName
                    gScheduleFolderName = clsMarsUI.getObjectParentName(nPackID, clsMarsScheduler.enScheduleType.EVENTPACKAGE)

                    'logFile = "eventpackagetrace_" & gScheduleName & "_" & Date.Now.ToString("HHmmss") & ".debug"

                    clsMarsUI.MainUI.BusyProgress(40, "Getting schedules...")

                    'clsMarsDebug.writeToDebug(logFile, "Starting", False)
                    'clsMarsDebug.writeToDebug(logFile, "Loading event-based schedules...", True)

                    Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID & " AND Status =1 ORDER BY PackOrderID")
                    Dim oTasks As clsMarsTask = New clsMarsTask

                    If flowType = "BEFORE" Or flowType = "BOTH" Then
                        'clsMarsDebug.writeToDebug(logFile, "Processing custom tasks (if any)...", True)

                        clsMarsUI.MainUI.BusyProgress(60, "Processing custom tasks...")

                        ok = oTasks.ProcessTasks(scheduleID, "NONE")

                        If ok = False Then
                            Return False
                        End If
                    End If

                    clsMarsUI.MainUI.BusyProgress(80, "Running schedules...")

                    'clsMarsDebug.writeToDebug(logFile, "Begin processing event-based schedules...", True)

                    If oRs1 IsNot Nothing Then
                        Do While oRs1.EOF = False
                            Dim eventID As Integer

                            eventID = oRs1("eventid").Value

                            'clsMarsDebug.writeToDebug(logFile, "Event-Based Schedule - " & GetEventBasedScheduleName(eventID) & " (ID: " & eventID & ")", True)

                            ok = RunEvents6(eventID, , , Me.m_HistoryID)

                            'clsMarsDebug.writeToDebug(logFile, "Finished Processing event-based schedule...", True)

                            oRs1.MoveNext()
                        Loop

                        oRs1.Close()
                    End If

                    If flowType = "AFTER" Or flowType = "BOTH" Then

                        'clsMarsDebug.writeToDebug(logFile, "Processing custom tasks (if any)", True)

                        clsMarsUI.MainUI.BusyProgress(90, "Processing custom tasks...")

                        ok = oTasks.ProcessTasks(scheduleID, "NONE")

                        If ok = False Then Return False
                    End If

                    'clsMarsDebug.writeToDebug(logFile, "Finished", True)

                    Return True
                Else
                    Throw New Exception("No event-based schedules were found in the package")
                End If

                'clsMarsDebug.writeToDebug(logFile, "Finished", True)

                oRs.Close()
            Else
                Throw New Exception("The query did not return a valid recordset")
            End If
        Catch ex As Exception
            gErrorDesc = "Event Package Error: " & packageName & " - " & ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            gErrorSuggest = ""

            _ErrorHandle("Event Package Error: " & packageName & " - " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
            Return False
        Finally
            gScheduleOwner = ""
            clsMarsUI.MainUI.BusyProgress(100, "", True)
        End Try
    End Function

    Public Shared Function GetEventBasedScheduleName(ByVal nID As Integer) As String
        Dim SQL As String = "SELECT EventName FROM EventAttr6 WHERE EventID =" & nID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return ""

        If oRs.EOF = True Then Return ""

        Return oRs("EventName").Value
    End Function
    Public Shared Sub RecacheAllDatasets()
        Dim SQL As String = "SELECT * FROM EventConditions WHERE (cacheFolderName = '' OR cacheFolderName IS NULL) AND " & _
        "(ConditionType ='database record exists' AND AnyAll = 'NEW')"
        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim conditionID As Integer = oRs("conditionid").Value
                Dim conditionName As String = oRs("conditionName").Value

                Dim cacheFolderName As String = conditionName & "." & conditionID 'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())

                If RefreshData(conditionID, cacheFolderName) = True Then
                    SQL = "UPDATE EventConditions SET cacheFoldername ='" & SQLPrepare(cacheFolderName) & "' WHERE ConditionID =" & conditionID

                    clsMarsData.WriteData(SQL)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


        SQL = "SELECT * FROM EventConditions WHERE (cacheFolderName = '' OR cacheFolderName IS NULL) AND ConditionType ='database record has changed'"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())
                Dim conditionName As String = oRs("conditionName").Value
                Dim conditionID As Integer = oRs("conditionid").Value
                Dim cacheFolderName As String = conditionName & "." & conditionID

                If RefreshData(conditionID, cacheFolderName) = True Then
                    SQL = "UPDATE EventConditions SET cacheFoldername ='" & SQLPrepare(cacheFolderName) & "' WHERE ConditionID =" & conditionID

                    clsMarsData.WriteData(SQL)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

    Private Shared Function RefreshData(ByVal conditionID As Integer, ByVal cacheFolderName As String) As Boolean
        Try
            Dim SQL As String = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim type As String = oRs("conditiontype").Value
                    Dim sName As String = oRs("conditionname").Value

                    Dim connString As String = oRs("connectionstring").Value
                    SQL = oRs("searchcriteria").Value

                    Dim AnyAll As String = IsNull(oRs("anyall").Value)
                    Dim keyColumn As String = oRs("keycolumn").Value

                    If type = "database record exists" Then
                        If AnyAll = "NEW" Then clsMarsEvent.CacheData(sName, SQL, connString, cacheFolderName)
                    Else
                        clsMarsEvent.CacheData(sName, SQL, connString, keyColumn, cacheFolderName)
                    End If
                End If
            End If
            Return True
        Catch
            Return False
        End Try
    End Function
End Class
