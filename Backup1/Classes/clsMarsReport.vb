Imports System.IO.File
Imports DZACTXLib
Imports Microsoft.Win32
Imports sqlrd.clsMarsMessaging
Imports System.Xml
Imports System.Threading
Imports Microsoft.Reporting.WinForms
Imports System.IO
Imports System.Security.Principal

#Region "CRXADRT"
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
Imports My.Crystal85.CRAXDRT.CRExportDestinationType
Imports My.Crystal85.CRAXDRT.CRExportFormatType
Imports My.Crystal85.CRAXDRT.CRAreaKind
Imports My.Crystal85.CRAXDRT.CRDiscreteOrRangeKind
Imports My.Crystal85.CRAXDRT.CRFieldValueType
Imports My.Crystal85.CRAXDRT.CRRangeInfo
Imports My.Crystal85.CRAXDRT.CRPaperOrientation
Imports My.Crystal85.CRAXDRT.CRPaperSize
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9
Imports My.Crystal9.CRAXDRT.CRExportDestinationType
Imports My.Crystal9.CRAXDRT.CRExportFormatType
Imports My.Crystal9.CRAXDRT.CRAreaKind
Imports My.Crystal9.CRAXDRT.CRDiscreteOrRangeKind
Imports My.Crystal9.CRAXDRT.CRFieldValueType
Imports My.Crystal9.CRAXDRT.CRRangeInfo
Imports My.Crystal9.CRAXDRT.CRPaperOrientation
Imports My.Crystal9.CRAXDRT.CRPaperSize
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10
Imports My.Crystal10.CRAXDRT.CRExportDestinationType
Imports My.Crystal10.CRAXDRT.CRExportFormatType
Imports My.Crystal10.CRAXDRT.CRAreaKind
Imports My.Crystal10.CRAXDRT.CRDiscreteOrRangeKind
Imports My.Crystal10.CRAXDRT.CRFieldValueType
Imports My.Crystal10.CRAXDRT.CRRangeInfo
Imports My.Crystal10.CRAXDRT.CRPaperOrientation
Imports My.Crystal10.CRAXDRT.CRPaperSize
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
Imports My.Crystal11.CRAXDRT.CRExportDestinationType
Imports My.Crystal11.CRAXDRT.CRExportFormatType
Imports My.Crystal11.CRAXDRT.CRAreaKind
Imports My.Crystal11.CRAXDRT.CRDiscreteOrRangeKind
Imports My.Crystal11.CRAXDRT.CRFieldValueType
Imports My.Crystal11.CRAXDRT.CRRangeInfo
Imports My.Crystal11.CRAXDRT.CRPaperOrientation
Imports My.Crystal11.CRAXDRT.CRPaperSize
#End If
#End Region

'Imports CRAXDRT.CRExportDestinationType
'Imports CRAXDRT.CRExportFormatType
'Imports CRAXDRT.CRAreaKind
'Imports CRAXDRT.CRDiscreteOrRangeKind
'Imports CRAXDRT.CRFieldValueType
'Imports CRAXDRT.CRRangeInfo
'Imports CRAXDRT.CRPaperOrientation
'Imports CRAXDRT.CRPaperSize

Friend Class clsMarsReport


    Public gReportID As Integer
    Public gPackId As Integer
    Public gShowFinish As Boolean
    Dim sTempPath As String = m_OutputFolder
    Public Shared sKeyParameter As String = ""
    Public Shared sKeyValue As String = ""
    Dim sKeyPrinter() As String
    Dim HTMLPath As String = ""
    Public sDynamicError As String = ""
    Dim EmbedPath As String = ""
    Dim sTempHTMLHolder As String
    Public Shared oReport As New clsMarsReport
    Public Shared IsDynamicSchedule As Boolean = False
    Dim DynamicCancelled As Boolean = False
    Public m_HistoryID As Integer = 0
    Public Shared m_scheduleID As Integer = 0
    Shared gCurrentTempOutputFile = ""
    Dim m_rv As Microsoft.Reporting.WinForms.ReportViewer

    Private m_ParametersTable As Hashtable
    Private m_AddThreadResultLock As New Object
    Private m_AddThreadLock As New Object
    Private m_RemoveThreadLock As New Object
    Private m_ThreadResults As New Collection
    Private m_Threads As New Collection
    Private m_ThreadMsg As String = ""
    Private m_ThreadsAborted As Boolean = False
    Private m_IsMultithreaded As Boolean = False

    Public Shared ReadOnly Property m_rdltempPath() As String
        Get
            Dim temp As String = sAppPath & "RDL\"

            If IO.Directory.Exists(temp) = False Then
                IO.Directory.CreateDirectory(temp)
            End If

            Return temp
        End Get
    End Property
    Public Shared ReadOnly Property m_serverVersion(ByVal url As String, ByVal user As String, ByVal password As String, Optional formsAuth As Boolean = False) As String
        Get
            Dim versionNumber As String = clsMarsUI.MainUI.ReadRegistry("SSRSVersion", "0")

            If versionNumber <> "" And versionNumber <> "0" Then Return versionNumber

            Dim oRv As Microsoft.Reporting.WinForms.ReportViewer = New Microsoft.Reporting.WinForms.ReportViewer
            'default to 2000
            Dim serverVersion As String = "2000"

            url = clsMarsParser.Parser.removeASMX(url)

            oRv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote
            oRv.ServerReport.ReportServerUrl = New Uri(url)

            'if we have user provided then set the credentials
RETRY:
            If user <> "" Then
                Dim userDomain As String = ""
                Dim tmpUser As String = user

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, password, userDomain)

                Try
                    If formsAuth Then oRv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, user, password, userDomain)
                Catch : End Try

                oRv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred
            Else
                oRv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials
            End If

            Try
                serverVersion = oRv.ServerReport.GetServerVersion()

                If serverVersion.Contains(".") Then
                    serverVersion = serverVersion.Split(".")(0)
                End If
            Catch ex As Exception
                'we errored so lets check if the server requires login
                If ex.Message.ToLower.Contains("login") Then
                    Dim loginInfo As frmRemoteLogin = New frmRemoteLogin

                    'if the userid is currently blank then lets get it from the user

                    loginInfo.getLoginDetails(user, password, url)

                    'if the user has set the username then lets try it again, man I hate GOTO
                    If user <> "" Then
                        GoTo RETRY
                    Else
                        serverVersion = "2005"
                    End If
                Else
                    serverVersion = "2005"
                End If
            End Try

            Return serverVersion
        End Get
    End Property
    Private ReadOnly Property m_ServerVersion2005() As Boolean
        Get

            Dim versionNumber As String = clsMarsUI.MainUI.ReadRegistry("SSRSVersion", "0")
            Dim version As String

            If versionNumber = "0" Or versionNumber = "" Then
                Try
                    version = m_rv.ServerReport.GetServerVersion
                Catch ex As Exception
                    version = "1000"
                End Try


                If version.StartsWith("20") Then '2005 and above
                    Return True
                Else
                    m_rv = Nothing
                    Return False
                End If
            Else
                If versionNumber > "1000" Then
                    Return True
                Else
                    Return False
                End If
            End If
        End Get
    End Property

    Public Shared ReadOnly Property m_CacheFolder() As String
        Get
            Dim value As String

            value = clsMarsUI.MainUI.ReadRegistry("CachePath", sAppPath & "Cache")

            If value.EndsWith("\") = False Then value &= "\"

            Return value
        End Get
    End Property

    Private Property m_DynamicSchedule() As Boolean
        Get
            Return Me.IsDynamicSchedule
        End Get
        Set(ByVal value As Boolean)
            Me.IsDynamicSchedule = value
        End Set
    End Property

    Public Shared ReadOnly Property m_CurrentTempFile() As String
        Get
            Return gCurrentTempOutputFile
        End Get

    End Property

    Public Shared Function _GetReportDescription(ByVal sFile As String) As String
        Try
            Dim oXML As New XmlDocument

            oXML.Load(sFile)

            Dim oNodes As XmlNodeList

            oNodes = oXML.GetElementsByTagName("Description")

            Return oNodes.Item(0).InnerText
        Catch
            Return ""
        End Try
    End Function
    Public Shared Function _GetParameterFields(ByVal sFile As String) As String()
        Dim sValue() As String
        Dim I As Integer = 0
        Dim x As Integer = 0
        Dim sDefValues As String
        'do we show hidden parameters
        Dim showHidden As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ShowHiddenParameters", 0)))

        Dim oXML As New Xml.XmlDocument

        oXML.Load(sFile)

        Dim oNodes As XmlNodeList

        oNodes = oXML.GetElementsByTagName("ReportParameter")

        For Each oNode As XmlNode In oNodes
            Dim hidden As Boolean = False

            Try
                hidden = oNode.Item("Hidden").InnerText
            Catch ex As Exception
                hidden = False
            End Try

            If hidden = True And showHidden = False Then Continue For

            sDefValues = String.Empty

            ReDim Preserve sValue(I)
            Dim multiValue As String = "false"

            Try
                multiValue = oNode.Item("MultiValue").InnerText
            Catch ex As Exception
                multiValue = "false"
            End Try

            Dim prompt As String = ""

            Try
                For Each n As XmlNode In oNode.ChildNodes
                    If n.Name.ToLower = "prompt" Then
                        prompt = n.InnerText
                        Exit For
                    End If
                Next
            Catch ex As Exception
                prompt = ""
            End Try

            sValue(I) = oNode.Attributes(0).Value & "|" & oNode.ChildNodes(0).InnerText & "|" & multiValue & "|" & prompt

            I += 1
        Next

        Return sValue
    End Function

    Public Shared Function _GetParameterValues(ByVal sFile As String) As String()
        Dim I As Integer = 0
        Dim x As Integer = 0
        Dim sDefValues() As String
        'do we show hidden parameters
        Dim showHidden As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ShowHiddenParameters", 0)))

        Dim oXML As New Xml.XmlDocument

        oXML.Load(sFile)

        Dim oNodes As XmlNodeList

        oNodes = oXML.GetElementsByTagName("ReportParameter")

        For Each oNode As XmlNode In oNodes
            Dim hidden As Boolean = False

            Try
                hidden = oNode.Item("Hidden").InnerText
            Catch ex As Exception
                hidden = False
            End Try

            If hidden = True And showHidden = False Then Continue For

            ReDim Preserve sDefValues(I)

            sDefValues(I) = ""

            Try
                Dim oChild As XmlNode

                oChild = oNode.Item("ValidValues").ChildNodes.Item(0) 'oNode.ChildNodes(2).ChildNodes(0)

                For Each o As XmlNode In oChild.ChildNodes
                    sDefValues(I) &= o.ChildNodes(0).InnerText & "|"
                Next
            Catch : End Try

            I += 1
        Next

        Return sDefValues
    End Function

    Public Shared Function _GetDatasources(ByVal sFile As String) As ArrayList
        Dim sValue As ArrayList = New ArrayList

        Try
            Dim oXML As New Xml.XmlDocument

            oXML.Load(sFile)

            Dim oNodes As XmlNodeList

            oNodes = oXML.GetElementsByTagName("DataSource")

            For Each oNode As XmlNode In oNodes

                sValue.Add(oNode.Attributes(0).Value)
            Next

            Return sValue
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Function

    Public Shared Function getRDLFile(ByVal serverUrl As String, ByVal reportPath As String, ByVal username As String, ByVal password As String, Optional formsAuth As Boolean = False) As String
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(serverUrl, username, password, formsAuth)
            Dim oRpt As Object

            'If srsVersion >= "2009" Then
            '    oRpt = New rsClients.rsClient2010(serverUrl)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(serverUrl)
            Else
                oRpt = New rsClients.rsClient(serverUrl)
            End If

            If serverUrl.Contains("2006.asmx") Then '//its sharepoint mode
                oRpt = New ReportServer_2006.ReportingService2006
            ElseIf serverUrl.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(serverUrl)
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & Guid.NewGuid.ToString & ".rdl"

            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is rsClients.rsClient2010 Then
                repDef = oRpt.GetItemDefinition(reportPath)
            Else
                repDef = oRpt.GetReportDefinition(reportPath)
            End If

            fsReport.Write(repDef, 0, repDef.LongLength)

            fsReport.Close()

            Return sRDLPath
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Shared Sub GetReportParameters(ByVal url As String, ByVal username As String, ByVal password As String, ByVal reportPath As String, ByVal reportName As String, ByVal addToListView As Boolean, _
           Optional ByRef lsvPars As ListView = Nothing, Optional formsAuth As Boolean = False)
10:     Try
            Dim logFile As String = "parameter_requery.log"

            clsMarsDebug.writeToDebug(logFile, "Creating report object", True)

20:         Dim rv As Microsoft.Reporting.WinForms.ReportViewer = New Microsoft.Reporting.WinForms.ReportViewer

            clsMarsDebug.writeToDebug(logFile, "Process mode = remote", True)
30:         rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

            Dim srv As Microsoft.Reporting.WinForms.ServerReport = rv.ServerReport
            Dim p As ReportParameterInfo
            Dim serverUrl As String = url
40:         url = clsMarsParser.Parser.removeASMX(url)

            clsMarsDebug.writeToDebug(logFile, "Server URI = " & url, True)

50:         srv.ReportServerUrl = New Uri(url)

            clsMarsDebug.writeToDebug(logFile, "Setting server credentials", True)

60:         If username = "" And password = "" Then
70:             srv.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultNetworkCredentials
80:         Else
90:             Dim userDomain As String = ""
                Dim tmpUser As String = username

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, password, userDomain)


100:            srv.ReportServerCredentials.NetworkCredentials = oCred

                '//forms auth
                Try
101:                If formsAuth Then srv.ReportServerCredentials.SetFormsCredentials(Nothing, username, password, Nothing)
                Catch : End Try
            End If

110:        If addToListView = True And lsvPars IsNot Nothing Then
                clsMarsDebug.writeToDebug(logFile, "Add parameters to listview", True)
120:            lsvPars.Items.Clear()
            End If

            clsMarsDebug.writeToDebug(logFile, "Report path = " & reportPath, True)
130:        rv.ServerReport.ReportPath = reportPath

            clsMarsDebug.writeToDebug(logFile, "Read report parameters ", True)
            Dim ps As ReportParameterInfoCollection = rv.ServerReport.GetParameters() 'get the parameters collection from the serverreport
140:        Dim parDefaults As Hashtable = New Hashtable

            clsMarsDebug.writeToDebug(logFile, "Total parameters found = " & ps.Count, True)

            '//get the rdl
            clsMarsDebug.writeToDebug(logFile, "Download RDL file", True)
            Dim rdlFile As String
            Try
141:            rdlFile = getRDLFile(serverUrl, reportPath, username, password, formsAuth)
            Catch
                rdlFile = ""
            End Try

            clsMarsDebug.writeToDebug(logFile, "RDL file downloaded to " & rdlFile, True)

150:        For Each p In ps
                clsMarsDebug.writeToDebug(logFile, "Parameter: " & p.Name, True)

                If rdlFile <> "" Then
                    If IsParameterHidden(rdlFile, p.Name) Then
                        clsMarsDebug.writeToDebug(logFile, "Parameter is hidden", True)
                        Continue For
                    End If
                End If

160:            Dim oItem As ListViewItem = New ListViewItem(p.Name)

170:            oItem.Tag = ""

180:            Try
190:                oItem.SubItems(1).Text = ""
200:            Catch ex As Exception
210:                oItem.SubItems.Add("")
                End Try

220:            Try
230:                oItem.SubItems(2).Text = p.MultiValue
240:            Catch ex As Exception
250:                oItem.SubItems.Add(p.MultiValue)
                End Try

                Dim sParDefaults As String = ""

260:            If p.ValidValues IsNot Nothing Then
                    Dim pValues As IList = p.ValidValues

270:                For Each val As ValidValue In pValues
280:                    sParDefaults &= val.Value & "|"
290:                Next
                End If

300:            parDefaults.Add(p.Name, sParDefaults)

                clsMarsDebug.writeToDebug(logFile, "Adding parameter to listview", True)

310:            If lsvPars IsNot Nothing Then lsvPars.Items.Add(oItem)
320:        Next

            Try
                IO.File.Delete(rdlFile)
            Catch : End Try
330:    Catch ex As Exception
340:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Public Shared Sub GetReportParameters(ByVal sUrl As String, ByVal serverUser As String, ByVal serverPassword As String, ByVal reportPath As String, _
           ByVal reportName As String, ByVal addToListView As Boolean, ByRef parDefaults As Hashtable, Optional ByVal lsvPars As ListView = Nothing, Optional formsAuth As Boolean = False)
10:     Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(sUrl, serverUser, serverPassword, formsAuth)

20:         Dim oRpt

            ' If srsVersion >= "2009" Then
            ' oRpt = New rsClients.rsClient2010(sUrl)
            If srsVersion >= "2007" Then
                oRpt = New rsClients.rsClient2008(sUrl)
            Else
                oRpt = New rsClients.rsClient(sUrl)
            End If


            If sUrl.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(sUrl)
            End If

            Dim cred As System.Net.NetworkCredential

30:         oRpt.Url = sUrl

40:         If serverUser = "" And serverPassword = "" Then
50:             oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
60:         Else

70:             Dim userDomain As String = ""
                Dim tmpUser As String = serverUser

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)

80:             oRpt.Credentials = oCred

                Try
                    oRpt.LogonUser(tmpUser, serverPassword, Nothing)
                Catch: End Try
            End If

            Dim sPath As String = reportPath

            Dim sRDLPath As String = m_rdltempPath & reportName & ".rdl"
            Dim repDef() As Byte

90:         If IO.File.Exists(sRDLPath) = True Then
100:            Try
110:                IO.File.Delete(sRDLPath)
120:            Catch
130:                sRDLPath = m_rdltempPath & clsMarsData.CreateDataID & ".rdl"
                End Try
            End If

140:        Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is ReportServer_2010.ReportingService2010 Then
                repDef = oRpt.GetItemDefinition(reportPath)
            Else
                repDef = oRpt.GetReportDefinition(reportPath)
            End If

160:        fsReport.Write(repDef, 0, repDef.Length)

170:        fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

180:        sPars = clsMarsReport._GetParameterFields(sRDLPath)
190:        sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

200:        If addToListView = True And lsvPars IsNot Nothing Then
210:            lsvPars.Items.Clear()
            End If

220:        parDefaults = New Hashtable
            Dim oItem As ListViewItem

230:        If sPars IsNot Nothing Then
240:            For Each s As String In sPars
250:                If addToListView = True And lsvPars IsNot Nothing Then
260:                    oItem = lsvPars.Items.Add(s.Split("|")(0))

270:                    oItem.Tag = s.Split("|")(1)

280:                    Try
290:                        oItem.SubItems(1).Text = ""
300:                    Catch ex As Exception
310:                        oItem.SubItems.Add("")
                        End Try


320:                    Try
330:                        oItem.SubItems(2).Text = s.Split("|")(2)
340:                    Catch ex As Exception
350:                        oItem.SubItems.Add(s.Split("|")(2))
                        End Try
                    End If

360:                parDefaults.Add(s.Split("|")(0), sParDefaults(I))

370:                I += 1
380:            Next
            End If

390:        Try
400:            IO.File.Delete(sRDLPath)
            Catch : End Try
410:    Catch ex As Exception
420:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    Public Shared Sub GetReportDatasources(ByVal sUrl As String, ByVal serverUser As String, ByVal serverPassword As String, ByVal reportPath As String, _
           ByVal reportName As String, ByVal addToListView As Boolean, Optional ByVal lsvDs As ListView = Nothing, Optional formsAuth As Boolean = False)
10:     Try

20:         clsMarsUI.MainUI.BusyProgress(10, "Setting up values...")

            Dim cred As System.Net.NetworkCredential
30:         Dim oRpt As Object 'rsClients.rsClient = New rsClients.rsClient(sUrl)
            Dim srsVersion As String = clsMarsReport.m_serverVersion(sUrl, serverUser, serverPassword, formsAuth)

40:         If srsVersion >= "2007" Then
50:             oRpt = New rsClients.rsClient2008(sUrl)
60:         Else
70:             oRpt = New ReportServer.ReportingService
            End If

            If sUrl.Contains("2010.asmx") Then
                oRpt = New rsClients.rsClient2010(sUrl)
            End If

80:         If srsVersion >= "2007" Then sUrl = clsMarsParser.Parser.fixASMXfor2008(sUrl)

90:         oRpt.Url = sUrl

100:        If serverUser = "" AndAlso serverPassword = "" Then
110:            oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
120:        Else
130:            Dim userDomain As String = ""
                Dim tmpUser As String = serverUser

                getDomainAndUserFromString(tmpUser, userDomain)

                Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, serverPassword, userDomain)

140:            oRpt.Credentials = oCred

                Try
150:                oRpt.LogonUser(serverUser, serverPassword, Nothing)

160:                Try
170:                    If (oRpt.CheckAuthorized()) Then
                            Dim type As ReportServer.ItemTypeEnum = oRpt.GetItemType("/")

180:                        Console.WriteLine(type)

                        End If

190:                Catch ex As Exception
200:                    Console.WriteLine("Exception on call to GetItemType.")
                    End Try

210:                oRpt.SessionHeaderValue = New ReportServer.SessionHeader()
                Catch : End Try
            End If

            Dim sPath As String = reportPath

            Dim sRDLPath As String = m_rdltempPath & reportName & ".rdl"
            Dim repDef() As Byte

220:        If IO.File.Exists(sRDLPath) = True Then
230:            Try
240:                IO.File.Delete(sRDLPath)
250:            Catch
260:                sRDLPath = m_rdltempPath & clsMarsData.CreateDataID & ".rdl"
                End Try
            End If

270:        clsMarsUI.MainUI.BusyProgress(30, "Getting report definition...")

280:        Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            If TypeOf oRpt Is rsClients.rsClient2010 Then
                repDef = oRpt.GetItemDefinition(reportPath)
            Else
290:            repDef = oRpt.GetReportDefinition(reportPath)
            End If


300:        fsReport.Write(repDef, 0, repDef.Length)

310:        fsReport.Close()

            'get the parameters

320:        Dim sData As ArrayList = New ArrayList

330:        clsMarsUI.MainUI.BusyProgress(60, "Reading report file...")

340:        sData = clsMarsReport._GetDatasources(sRDLPath)

350:        clsMarsUI.MainUI.BusyProgress(90, "Listing datasources...")

360:        If addToListView = True And lsvDs IsNot Nothing Then
370:            For Each s As String In sData
                    Dim found As Boolean = False

380:                For Each item As ListViewItem In lsvDs.Items
390:                    If item.Text.ToLower = s.ToLower Then
400:                        found = True
410:                        Exit For
                        End If
420:                Next

430:                If found = False Then
440:                    Dim newItem As ListViewItem = New ListViewItem(s)
450:                    lsvDs.Items.Add(newItem)

460:                    newItem.SubItems.Add("default")
470:                    newItem.ImageIndex = 0
480:                    newItem.Tag = 0
                    End If

490:                found = False
500:            Next
            End If

510:        clsMarsUI.MainUI.BusyProgress(100, , True)
520:    Catch ex As Exception
530:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl(), _
      "Please make sure that the report path specified is correct and that the report is accessible on the report server")
        End Try
    End Sub

    Private Function AddFolderToOutput(ByVal sPath As String) As String
        'randomise it
        Dim rnd As New Random
        Dim rndNum As Integer

        rndNum = rnd.Next(10000, 100000)

        Dim value As String = GetDirectory(sPath)

        If value.EndsWith("\") = False Then value &= "\"

        Dim filename As String = ExtractFileName(sPath)

        Dim folder As String = "temp" & rndNum & "\"

        Do While IO.Directory.Exists(value & folder) = True
            rndNum = rnd.Next(1000, 100000)

            folder = "temp" & rndNum & "\"
        Loop

        IO.Directory.CreateDirectory(value & folder)

        value = value & folder & filename

        Return value
    End Function
    Public Shared ReadOnly Property m_OutputFolder() As String
        Get
            Dim tryCount As Integer = 0

            Try
                Dim value As String
RETRY:
                value = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output")

                If value.EndsWith("\") = False Then value &= "\"

                If IO.Directory.Exists(value) = False Then
                    IO.Directory.CreateDirectory(value)
                End If

                Return value
            Catch ex As Exception
                If Err.Number = 57 Then
                    If tryCount < 6 Then
                        tryCount += 1
                        System.Threading.Thread.Sleep(1000)
                        GoTo RETRY
                    End If
                End If

                Dim suggest As String = ""

                If ex.Message.ToLower.Contains("network path was not found") Or ex.Message.ToLower.Contains("login failure") Then
                    suggest = "Go to Options->System Paths and ensure that" & vbCrLf & _
                    "- All the system paths there exist" & vbCrLf & _
                    "- You can connect to all of them in Explorer" & vbCrLf & _
                    "- They have not used up their diskspace quota (you may need to delete contents)" & vbCrLf & _
                    "- You have full security rights to these folders (read, write, add, delete and change)" & vbCrLf & _
                    "- If any of them are network dirves, restart the PC and see if this will reconnect correctly" & vbCrLf & _
                    "- If you have restricted user security, try going to Options -> General and check the option 'Only convert remote paths to UNC' and then reselect your system paths."
                End If

                _ErrorHandle(gScheduleName & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, suggest)
            End Try
        End Get
    End Property
    Public Function ZipFiles(ByVal sType As String, ByVal sOutput As String, _
    Optional ByVal sFile As String = "", Optional ByVal Encrypt As Boolean = False, Optional ByVal ZipCode As String = "") As String
        Dim oZip As New dzactxctrl
        Dim sList As String
        Dim I As Integer
        Dim sName As String

        sOutput = clsMarsParser.Parser.ParseString(sOutput)

        If HTMLPath.Length > 0 And sType <> "Package" Then
            For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                sName = ExtractFileName(s)

                sName = sName.Split(".")(0)

                sOutput &= sName & ".zip"
                Exit For
            Next


        Else
            If sFile.Length > 0 Then
                sName = ExtractFileName(sFile)

                sName = sName.Split(".")(0)

                sOutput &= sName & ".zip"
            End If
        End If

        If System.IO.File.Exists(sOutput) = True Then
            System.IO.File.Delete(sOutput)
        End If

        If sType = "Package" Then
            For I = 0 To rptFileNames.GetUpperBound(0)
                rptFileNames(I) = Chr(34) & rptFileNames(I) & Chr(34) & " "
                sList &= rptFileNames(I)
            Next
        Else
            If HTMLPath.Length = 0 Then
                sList = Chr(34) & sFile & Chr(34)
            Else
                For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                    sList &= Chr(34) & s & Chr(34) & " "
                Next
            End If
        End If

        With oZip
            .ItemList = sList
            .NoDirectoryEntriesFlag = True
            .NoDirectoryNamesFlag = True
            .DeleteOriginalFlag = False
            .ZIPFile = sOutput

            If Encrypt = True Then
                .EncryptFlag = True
                .EncryptCode = Decrypt(ZipCode, "preggers")
            End If

            .CompressionFactor = DZACTXLib.CompFactor.COMPFACTOR_9
            .ActionDZ = DZACTION.ZIP_ADD
        End With

        oZip = Nothing

        Return sOutput
    End Function

    Public Function prepareTextFiles(ByVal sFileName As String, Optional ByVal IsDataDriven As Boolean = False, Optional ByVal sortByDate As Boolean = True) As Boolean
10:     Try
            Dim sGo() As String
            Dim sNot() As String
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim Merged As Boolean

            '#If DEBUG Then
            '            ReDim rptFileNames(3)

            '            rptFileNames(0) = "C:\Users\steven\Downloads\Allocall\Allocall_1.txt"
            '            rptFileNames(1) = "C:\Users\steven\Downloads\Allocall\Allocall_2.txt"
            '            rptFileNames(2) = "C:\Users\steven\Downloads\Allocall\Allocall_3.txt"
            '            rptFileNames(3) = "C:\Users\steven\Downloads\Allocall\Allocall_4.txt"

            '#End If
20:         ReDim sNot(y)

            'find which files should be merged together
30:         For I As Integer = 0 To rptFileNames.GetUpperBound(0)
40:             If rptFileNames(I) IsNot Nothing Then
50:                 If rptFileNames(I).ToLower.EndsWith(".txt") = True Then
60:                     ReDim Preserve sGo(x)

70:                     sGo(x) = rptFileNames(I)

80:                     x += 1
90:                 Else
100:                    ReDim Preserve sNot(y)

110:                    sNot(y) = rptFileNames(I)

120:                    y += 1
                    End If
                End If
130:        Next

140:        If sortByDate = True Then
                Dim captcha As String() = sGo.Clone 'our insurance in case the sorting goes wrong

150:            Try
                    'lets sort by date created:
160:                Dim sorter As DataTable = New DataTable

170:                sorter.Columns.Add("File")
180:                sorter.Columns.Add("DateMod", System.Type.GetType("System.DateTime"), "")

190:                For Each s As String In sGo
                        Dim r As DataRow = sorter.Rows.Add
200:                    r("File") = s
210:                    r("DateMod") = IO.File.GetLastWriteTime(s)
220:                Next

                    Dim rs() As DataRow = sorter.Select("", "DateMod Asc")

230:                If rs IsNot Nothing Then
240:                    sGo = Nothing
250:                    x = 0

260:                    ReDim sGo(x)

270:                    For Each r As DataRow In rs
280:                        ReDim Preserve sGo(x)

290:                        ''console.writeLine(r("DateMod"))

300:                        sGo(x) = r("file")

310:                        x += 1
320:                    Next
                    End If

330:                sorter.Dispose()
340:            Catch
350:                sGo = captcha.Clone 'well, it did go wrong so lets restore captcha
                End Try
            End If


            'go and merge the files
370:        If IO.Path.GetExtension(sFileName) = "" Then sFileName &= ".txt"

            Dim tmp As String = IO.Path.GetFileName(sFileName)
            Dim sdir As String = ""

380:        Do
390:            sdir = IO.Path.GetDirectoryName(sFileName)
400:            If sdir.EndsWith("\") = False Then sdir &= "\"

410:            sdir &= IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)
420:        Loop Until IO.Directory.Exists(sdir) = False

430:        IO.Directory.CreateDirectory(sdir)

440:        If sdir.EndsWith("\") = False Then sdir &= "\"

450:        sFileName = sdir & tmp

460:        If sGo Is Nothing Then Throw New Exception("There are no Text files to merge. Please make make sure that the report is not blank.")

470:        Merged = mergeTextFiles(sGo, sFileName)

            'destroy our current package file list
480:        rptFileNames = Nothing

490:        If sNot(0) <> String.Empty Then
500:            ReDim rptFileNames(0)

                'repopulate the filelist with files that were not merged
510:            For I As Integer = 0 To sNot.GetUpperBound(0)
520:                ReDim Preserve rptFileNames(I)

530:                rptFileNames(I) = sNot(I)
540:            Next

                'now add the merged file
                Dim NewIndex As Integer = rptFileNames.GetUpperBound(0) + 1

550:            ReDim Preserve rptFileNames(NewIndex)

560:            rptFileNames(NewIndex) = sFileName
570:        Else
580:            ReDim rptFileNames(0)

590:            rptFileNames(0) = sFileName
            End If

600:        Return True
610:    Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , , IsDataDriven)
620:        gErrorDesc = ex.Message
630:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
640:        gErrorNumber = Err.Number
650:        gErrorLine = Erl()
660:        gErrorSuggest = "Please make sure that the report output is not blank"
670:        Return False
        End Try
    End Function
    Public Function PrepareXLS(ByVal sFileName As String, Optional ByVal IsDataDriven As Boolean = False, Optional ByVal sortByDate As Boolean = False) As Boolean
10:     Try
            Dim sGo() As String
            Dim sNot() As String
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim Merged As Boolean

            '_Delay(1.5)

            '10:         ReDim sGo(x)
20:         ReDim sNot(y)


            'find which files should be merged together
30:         For I As Integer = 0 To rptFileNames.GetUpperBound(0)
40:             If rptFileNames(I) IsNot Nothing Then
50:                 If rptFileNames(I).ToLower.EndsWith(".xls") = True Or rptFileNames(I).ToLower.EndsWith(".xlsx") Then
60:                     ReDim Preserve sGo(x)

70:                     sGo(x) = rptFileNames(I)

80:                     x += 1
90:                 Else
100:                    ReDim Preserve sNot(y)

110:                    sNot(y) = rptFileNames(I)

120:                    y += 1
                    End If
                End If
130:        Next

            If sGo Is Nothing Then
                Return True '//no xls to merge
            ElseIf sGo(0) Is Nothing And sGo.Length = 1 Then
                Return True
            End If

140:        If sortByDate = True Then
                Dim captcha As String() = sGo.Clone 'our insurance in case the sorting goes wrong

150:            Try
                    'lets sort by date created:
160:                Dim sorter As DataTable = New DataTable

170:                sorter.Columns.Add("File")
180:                sorter.Columns.Add("DateMod", System.Type.GetType("System.DateTime"), "")

190:                For Each s As String In sGo
                        Dim r As DataRow = sorter.Rows.Add
200:                    r("File") = s
210:                    r("DateMod") = IO.File.GetCreationTime(s)
220:                Next

                    Dim rs() As DataRow = sorter.Select("", "DateMod Asc")

230:                If rs IsNot Nothing Then
240:                    sGo = Nothing
250:                    x = 0

260:                    ReDim sGo(x)

270:                    For Each r As DataRow In rs
280:                        ReDim Preserve sGo(x)

290:                        ''console.writeLine(r("DateMod"))

300:                        sGo(x) = r("file")

310:                        x += 1
320:                    Next
                    End If

330:                sorter.Dispose()
340:            Catch
350:                sGo = captcha.Clone 'well, it did go wrong so lets restore captcha
                End Try
            End If

            Dim allXLSX As Boolean = True

            '//lets see if there are all xlsx files
            For Each s As String In sGo
                If IO.Path.GetExtension(s).ToLower <> ".xlsx" Then
                    allXLSX = False
                    Exit For
                End If
            Next

            If allXLSX = False Then
360:            Dim oXL As New clsMarsExcel 'ucXLConv

                'go and merge the files
370:            If sFileName.ToLower.EndsWith(".xls") = False Then sFileName &= ".xls"

                Dim tmp As String = IO.Path.GetFileName(sFileName)
                Dim sdir As String = ""

380:            Do
390:                sdir = IO.Path.GetDirectoryName(sFileName)
400:                If sdir.EndsWith("\") = False Then sdir &= "\"

410:                sdir &= IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)
420:            Loop Until IO.Directory.Exists(sdir) = False

430:            IO.Directory.CreateDirectory(sdir)

440:            If sdir.EndsWith("\") = False Then sdir &= "\"

450:            sFileName = sdir & tmp

460:            'If sGo Is Nothing Then Throw New Exception("There are no Excel files to merge. Please make make sure that the report is not blank.")

470:            Merged = oXL.MergeXLFiles(sGo, sFileName)
            Else
                If IO.Path.GetExtension(sFileName).ToLower <> ".xlsx" Then sFileName &= ".xlsx"

                Dim tmp As String = IO.Path.GetFileName(sFileName)
                Dim sdir As String = ""

                Do
                    sdir = IO.Path.GetDirectoryName(sFileName)
                    If sdir.EndsWith("\") = False Then sdir &= "\"

                    sdir &= IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)
                Loop Until IO.Directory.Exists(sdir) = False

                IO.Directory.CreateDirectory(sdir)

                If sdir.EndsWith("\") = False Then sdir &= "\"

                sFileName = sdir & tmp

                Dim errorMsg As String = ""

                '//try merging with Gembox
                Dim excelCls As clsMarsExcel = New clsMarsExcel
                Dim mergeIntoSingleSheet As Boolean = False

                If mergeIntoSingleSheet = False Then
                    If excelCls.mergeXLSXFiles(sGo, sFileName, errorMsg) = False Then
                        Throw New Exception(errorMsg)
                    End If
                End If
            End If
            'destroy our current package file list
480:        rptFileNames = Nothing

490:        If sNot(0) <> String.Empty Then
500:            ReDim rptFileNames(0)

                'repopulate the filelist with files that were not merged
510:            For I As Integer = 0 To sNot.GetUpperBound(0)
520:                ReDim Preserve rptFileNames(I)

530:                rptFileNames(I) = sNot(I)
540:            Next

                'now add the merged file
                Dim NewIndex As Integer = rptFileNames.GetUpperBound(0) + 1

550:            ReDim Preserve rptFileNames(NewIndex)

560:            rptFileNames(NewIndex) = sFileName
570:        Else
580:            ReDim rptFileNames(0)

590:            rptFileNames(0) = sFileName
            End If

600:        Return True
610:    Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , , IsDataDriven)
620:        gErrorDesc = ex.Message
630:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
640:        gErrorNumber = Err.Number
650:        gErrorLine = Erl()
660:        gErrorSuggest = "Please make sure that the report output is not blank"
670:        Return False
        End Try
    End Function
    Public Function PreparePDF(ByVal sFileName As String, Optional ByVal AddBookmarks As Boolean = False, Optional ByVal sortByDate As Boolean = False) As Boolean
10:     Try
            Dim sGo() As String
            Dim sNot() As String
            Dim x As Integer = 0
            Dim y As Integer = 0
            Dim sMergeFile As String

20:         ReDim sGo(x)
30:         ReDim sNot(y)

            'find which files should be merged together
40:         For I As Integer = 0 To rptFileNames.GetUpperBound(0)
50:             If rptFileNames(I).ToLower.EndsWith(".pdf") = True Then
60:                 ReDim Preserve sGo(x)

70:                 sGo(x) = rptFileNames(I)

80:                 x += 1
90:             Else
100:                ReDim Preserve sNot(y)

110:                sNot(y) = rptFileNames(I)

120:                y += 1
130:            End If
140:        Next

            If sGo Is Nothing Then
                Return True '//no pdfs to merge
            ElseIf sGo(0) Is Nothing And sGo.Length = 1 Then
                Return True
            End If

150:        Dim oPDF As New clsMarsPDF
            Dim bookMark As String = sFileName
            'go and merge the files

160:        If sFileName.ToLower.EndsWith(".pdf") = False Then sFileName &= ".pdf"

            'sMergeFile = oPDF._MergePDF(sGo, sFileName)

170:        If sortByDate = True Then
                Dim captcha As String() = sGo.Clone 'our insurance in case the sorting goes wrong

                Try
                    'lets sort by date created:
180:                Dim sorter As DataTable = New DataTable

190:                sorter.Columns.Add("File")
200:                sorter.Columns.Add("DateMod", System.Type.GetType("System.DateTime"), "")

210:                For Each s As String In sGo
                        Dim r As DataRow = sorter.Rows.Add
220:                    r("File") = s
230:                    r("DateMod") = IO.File.GetCreationTime(s)
240:                Next

                    Dim rs() As DataRow = sorter.Select("", "DateMod Asc")

250:                If rs IsNot Nothing Then
260:                    sGo = Nothing
270:                    x = 0

280:                    ReDim sGo(x)

290:                    For Each r As DataRow In rs
300:                        ReDim Preserve sGo(x)

310:                        ''console.writeLine(r("DateMod"))

320:                        sGo(x) = r("file")

330:                        x += 1
340:                    Next
                    End If

350:                sorter.Dispose()
                Catch
                    sGo = captcha.Clone 'well, it did go wrong so lets restore captcha
                End Try
            End If

360:        sMergeFile = oPDF.MergePDFFiles(sGo, sFileName, AddBookmarks, bookMark)

            'destroy our current package file list
370:        rptFileNames = Nothing

380:        If sNot(0) <> String.Empty Then
390:            ReDim rptFileNames(0)

                'repopulate the filelist with files that were not merged
400:            For I As Integer = 0 To sNot.GetUpperBound(0)
410:                ReDim Preserve rptFileNames(I)

420:                rptFileNames(I) = sNot(I)
430:            Next

                'now add the merged file
440:            Dim NewIndex As Integer = rptFileNames.GetUpperBound(0) + 1

450:            ReDim Preserve rptFileNames(NewIndex)

460:            rptFileNames(NewIndex) = sMergeFile
470:        Else
480:            ReDim rptFileNames(0)

490:            rptFileNames(0) = sMergeFile
500:        End If

510:        Return True
520:    Catch ex As Exception
530:        gErrorDesc = ex.Message
540:        gErrorNumber = Err.Number
550:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
560:        gErrorLine = Erl()
570:        gErrorSuggest = "Please check your report to make sure that group values exist"

580:        Return False
        End Try
    End Function

    Public Function RunPackageWithDefault(ByVal nPackID As Integer, ByVal nDestinationID As Integer) As Boolean
        Try
            Dim oRs As ADODB.Recordset
            Dim oRs1 As ADODB.Recordset
            Dim SQL As String
            Dim I As Integer = 0
            Dim sPackage As String
            Dim nRetry As Integer
            Dim Compress As Boolean
            Dim CheckBlank As Boolean
            ReDim rptFileNames(0)
            Dim oFile As String
            Dim sFileName As String
            Dim oSchedule As New clsMarsScheduler
            Dim oTask As New clsMarsTask
            Dim nScheduleID As Integer = oSchedule.GetScheduleID(, nPackID)
            Dim sTemp As String
            Dim Ok As Boolean
            Dim ToPrinter As Boolean = False
            Dim FailOnOne As Boolean = False
            Dim MergePDF As Boolean = False
            Dim sMergePDFName As String = ""
            Dim sPackageStamp As String = ""
            Dim oUI As clsMarsUI = New clsMarsUI

            gScheduleOwner = clsMarsScheduler.getScheduleOwner(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)
            gCurrentTempOutputFile = ""

            oUI.BusyProgress(25, "Collecting data...")

            oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)
            m_scheduleID = nScheduleID

            ScheduleStart = Now

            SQL = "SELECT * FROM PackageAttr WHERE PackID = " & nPackID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then
                gErrorDesc = "Could not obtain a valid recordset"
                gErrorNumber = -101
                gErrorSource = "clsMarsReport._RunPackageSchedule"
                gErrorLine = 110
                Return False
            End If

            If oRs.EOF = False Then
                I = 0
                sPackage = oRs("PackageName").Value
                nRetry = oRs("Retry").Value
                CheckBlank = Convert.ToBoolean(oRs("CheckBlank").Value)
                FailOnOne = Convert.ToBoolean(oRs("failonone").Value)
                gScheduleName = sPackage

                Try
                    If Convert.ToBoolean(oRs("datetimestamp").Value) = True Then
                        sPackageStamp = IsNull(oRs("stampformat").Value)
                    End If
                Catch : End Try

                Try
                    MergePDF = Convert.ToBoolean(oRs("mergepdf").Value)
                    sMergePDFName = oRs("mergepdfname").Value
                Catch
                    MergePDF = False
                End Try

            End If

            oRs.Close()

            oRs = clsMarsData.GetData("SELECT * FROM DestinationAttr WHERE " & _
            "Destinationid =" & nDestinationID)

            If oRs.EOF = False Then

                SQL = "SELECT * FROM ReportAttr r INNER JOIN PackagedReportAttr p ON " & _
                    "r.ReportID = p.ReportID WHERE " & _
                    "p.Status = 1 AND r.PackID = " & nPackID & " ORDER BY PackOrderID"

                oRs1 = clsMarsData.GetData(SQL)

                If Not oRs1 Is Nothing Then

                    If oRs1.EOF = True Then
                        oRs1.Close()
                        Return True
                    End If

                    Do While oRs1.EOF = False

                        Try
                            oUI.BusyProgress(35, "Processing report " & I + 1 & "...")

                            If oRs("destinationtype").Value = "Printer" Then
                                ToPrinter = True
                            Else
                                ToPrinter = False
                            End If

                            RunSingle(sTemp, oRs1(0).Value, nDestinationID, sPackage, nPackID, _
                            ToPrinter, sPackageStamp, , oRs("outputformat").Value)

                            If FailOnOne = True And (sTemp.Length = 0 Or sTemp = "{error}") Then Return False

                            If sTemp = "{skip}" Then
                                Ok = True
                                GoTo Skip
                            End If


                            If sTemp.Length > 0 And sTemp <> "{error}" Then
                                HTMLPath = GetDirectory(HTMLPath)

                                If HTMLPath Is Nothing Then HTMLPath = String.Empty

                                If HTMLPath.Length = 0 Then
                                    ReDim Preserve rptFileNames(I)
                                    rptFileNames(I) = sTemp
                                    gExportedFileName = sTemp
                                    I += 1
                                Else
                                    For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                        ReDim Preserve rptFileNames(I)

                                        rptFileNames(I) = s
                                        gExportedFileName = s
                                        I += 1
                                    Next
                                End If
                            Else
                                Return False
                            End If
                        Catch ex As Exception
                            gErrorDesc = ex.Message
                            gErrorNumber = Err.Number
                            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
                            gErrorLine = Erl()

                            oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", oRs1(0).Value, nPackID, , , , Me.m_HistoryID)
                        End Try
Skip:
                        oRs1.MoveNext()
                    Loop

                    Dim Proceed As Boolean = False

                    For Each s As String In rptFileNames
                        If Not s Is Nothing Then
                            Proceed = True
                        End If
                    Next

                    If Proceed = False Then Return False

                    oRs1.Close()

                    If I = 0 Then Return True

                    If MergePDF = True Then
                        PreparePDF(sMergePDFName)
                    End If

                End If

                Dim oPGP As New clsMarsPGP
                Dim oPGPResult() As Object
                Dim nFile As Integer = 0

                For Each s As String In rptFileNames
                    If s.Length > 0 Then
                        oPGPResult = oPGP.EncryptFile(s, nDestinationID)

                        If oPGPResult(0) = False Then Return False

                        rptFileNames(nFile) &= oPGPResult(1)

                        nFile += 1
                    End If
                Next

                oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)

                Compress = Convert.ToBoolean(oRs("Compress").Value)

                If Compress = True Then
                    oUI.BusyProgress(60, "Zipping reports...")
                    ZipFiles("Package", m_OutputFolder & sPackage & ".zip")

                    ReDim rptFileNames(0)

                    rptFileNames(0) = m_OutputFolder & sPackage & ".zip"
                End If

                Dim sSendTo As String
                Dim sSubject, senderName, senderAddress As String
                Dim oParse As New clsMarsParser
                Dim sCc As String
                Dim sBcc As String
                Dim sExtras As String
                Dim sMsg As String
                Dim sPath As String
                Dim SMTPServer As String = ""
                Dim readReceipt As Boolean = False

                Select Case Convert.ToString(oRs("DestinationType").Value).ToLower
                    Case "printer"
                        oUI.BusyProgress(75, "Printing reports...")

                        Ok = Me._PrintServer(rptFileNames, nDestinationID)
                    Case "email"
                        sSendTo = ResolveEmailAddress(oRs("sendto").Value)
                        sSubject = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
                        sCc = ResolveEmailAddress(oRs("cc").Value)
                        sBcc = ResolveEmailAddress(oRs("bcc").Value)
                        sExtras = oRs("extras").Value

                        sSendTo = oParse.ParseString(sSendTo, , , , , , m_ParametersTable)
                        sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
                        sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)
                        senderName = oParse.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
                        senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)
                        sMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)
                        SMTPServer = IsNull(oRs("smtpserver").Value, "Default")

                        Try
                            readReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
                        Catch ex As Exception
                            readReceipt = False
                        End Try

                        oUI.BusyProgress(75, "Emailing report...")

                        If MailType = MarsGlobal.gMailType.MAPI Then
                            Ok = SendMAPI( _
                            sSendTo, sSubject, sMsg, "Package", _
                            , I, sExtras, sCc, sBcc, False, , sPackage, , , readReceipt, nDestinationID)
                        ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then

                            Dim objSender As clsMarsMessaging = New clsMarsMessaging

                            Ok = objSender.SendSMTP( _
                            sSendTo, sSubject, sMsg, "Package", _
                            , I, sExtras, sCc, sBcc, sPackage, False, , , , IsNull(oRs("MailFormat").Value), SMTPServer, , senderName, senderAddress)
                        ElseIf MailType = gMailType.GROUPWISE Then
                            Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, "", "Package", sExtras, _
                            True, False, "", True, 0, sPackage, "")
                        Else
                            Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                        End If

                    Case "disk"
                        Dim HouseTemp As String
                        Dim oSys As New clsSystemTools

                        Dim UseDUN As Boolean
                        Dim sDUN As String
                        Dim oNet As clsNetworking
                        'if DUN is used...
                        Try
                            UseDUN = Convert.ToBoolean(oRs("usedun").Value)
                            sDUN = IsNull(oRs("dunname").Value)
                        Catch ex As Exception
                            UseDUN = False
                        End Try

                        If UseDUN = True Then
                            oNet = New clsNetworking

                            If oNet._DialConnection(sDUN, "Package Error: " & sPackage & ": ") = False Then
                                Return False
                            End If
                        End If

                        oUI.BusyProgress(75, "Copying reports...")

                        For Each oFile In rptFileNames
                            sFileName = ExtractFileName(oFile)

                            Dim paths As String = oRs("outputpath").Value

                            For Each item As String In paths.Split("|")
                                If item.Length > 0 Then

                                    sPath = oParse.ParseString(item, , , , , , m_ParametersTable)

                                    sPath = _CreateUNC(sPath)

                                    Ok = oParse.ParseDirectory(sPath)

                                    HouseTemp = sPath

                                    sPath &= sFileName

                                    System.IO.File.Copy(oFile, _
                                    sPath, True)

                                    Try
                                        oSys._HouseKeeping(nDestinationID, HouseTemp, sFileName.Split(".")(0))
                                    Catch : End Try
                                End If
                            Next
                        Next

                        Try
                            If UseDUN = True Then
                                oNet._Disconnect()
                            End If
                        Catch : End Try
                    Case "printer"
                        oUI.BusyProgress(75, "Printing reports...")
                    Case "ftp"
                        Dim sFtp As String = ""
                        Dim oFtp As New clsMarsTask
                        Dim FTPServer As String
                        Dim FTPUser As String
                        Dim FTPPassword As String
                        Dim FTPPath As String
                        Dim FTPType As String
                        Dim FTPPassive As String
                        Dim FtpOptions As String

                        FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
                        FTPUser = oParse.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
                        FTPPassword = oParse.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
                        FTPPath = oParse.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
                        FTPType = IsNull(oRs("ftptype").Value, "FTP")
                        FTPPassive = IsNull(oRs("ftppassive").Value, "")
                        FtpOptions = IsNull(oRs("ftpoptions").Value)

                        Dim ftpCount As Integer = FTPServer.Split("|").GetUpperBound(0)

                        If ftpCount > 0 Then
                            ftpCount -= 1
                        Else
                            ftpCount = 0
                        End If

                        oUI.BusyProgress(75, "Uploading reports...")

                        For z As Integer = 0 To ftpCount
                            Dim l_FTPServer As String = FTPServer.Split("|")(z)
                            Dim l_FTPUser As String = FTPUser.Split("|")(z)
                            Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(z))
                            Dim l_FTPPath As String = FTPPath.Split("|")(z)
                            Dim l_FTPType As String = FTPType.Split("|")(z)
                            Dim l_FTPPassive As Boolean = False
                            Dim l_FtpOptions As String


                            Try
                                l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(z)))
                            Catch ex As Exception
                                l_FTPPassive = False
                            End Try

                            Try
                                l_FtpOptions = FtpOptions.Split("|")(z)
                            Catch ex As Exception
                                l_FtpOptions = ""
                            End Try

                            For Each oFile In rptFileNames
                                sFtp &= oFile & "|"
                            Next

                            oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, l_FTPPassword, l_FTPPath, sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)

                            Ok = True
                        Next
                    Case "fax"
                        Dim faxer As clsMarsMessaging = New clsMarsMessaging

                        Ok = faxer.SendFax(oRs("sendto").Value, oRs("subject").Value, "Package", _
                        oRs("cc").Value, oRs("bcc").Value, oRs("message").Value, "")
                    Case "sms"
                        For Each s As String In rptFileNames
                            If s.Length > 0 Then

                                Ok = SendSMS(oRs("sendto").Value, oRs("message").Value, s)

                            End If
                        Next
                    Case "sharepoint"
                        Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint

                        Dim spServer As String
                        Dim spUser As String
                        Dim spPassword As String
                        Dim spLib As String
                        Dim spCount As Integer = 0

                        spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                        spUser = oParse.ParseString(oRs("ftpusername").Value)
                        spPassword = oParse.ParseString(oRs("ftppassword").Value)
                        spLib = oParse.ParseString(oRs("ftppath").Value)

                        oUI.BusyProgress(75, "Uploading report to SharePoint...")

                        spCount = spServer.Split("|").GetUpperBound(0)

                        If spCount > 0 Then
                            spCount -= 1
                        Else
                            spCount = 0
                        End If

                        For z As Integer = 0 To spCount
                            Dim l_spServer As String = spServer.Split("|")(z)
                            Dim l_spUser As String = spUser.Split("|")(z)
                            Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(z))
                            Dim l_spLib As String = spLib.Split("|")(z)
                            Dim errorInfo As Exception = Nothing

                            If l_spServer = "" Then Continue For

                            If l_spServer = "" Then Continue For

                            Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, rptFileNames, l_spUser, l_spPassword, errorInfo)

                            If Ok = False And errorInfo IsNot Nothing Then
                                Throw errorInfo
                            End If
                        Next

                        Ok = True
                    Case "dropbox"
                        Dim db As clsDropboxDestination = New clsDropboxDestination(nDestinationID)
                        Dim files As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)

                        For Each s As String In rptFileNames
                            files.Add(s)
                        Next

                        Dim errInfo As Exception

                        Ok = db.putFiles(errInfo, files)

                        If Ok = False Then
                            Throw errInfo
                        End If
                End Select
                oRs.MoveNext()
            End If

            oRs.Close()

            If Ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

            oUI.BusyProgress(95, "Cleaning up...")

            oUI.BusyProgress(, , True)

            oUI = Nothing
            Return Ok
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return False
        Finally
            gScheduleOwner = ""
            gScheduleName = String.Empty
        End Try
    End Function

    Public Function ProduceReportsForPackage(ByVal packID As Integer, ByVal packageName As String, ByVal dateStamp As String, ByVal mergePDF As Boolean, ByVal mergeXL As Boolean, _
            ByVal adjustStamp As Double, ByVal failonOne As Boolean, ByRef appendToFile() As Boolean, Optional ByVal reportID As Integer = 0, Optional ByVal ToPrinter As Boolean = False, _
            Optional ByVal ToFax As Boolean = False, Optional ByVal PrintDate As String = "", Optional ByVal nRetry As Integer = 0, _
            Optional ByVal rptTimeout As Integer = 60, Optional ByVal DDKeyValue As String = "", _
            Optional ByVal IsDataDriven As Boolean = False, _
            Optional ByVal IsMultithreaded As Boolean = False) As String()

10:     Try
20:         Dim SQL As String
30:         Dim oRs As ADODB.Recordset
40:         Dim reports() As String
50:         Dim I As Integer = 0
            Dim reportCount As Integer = 0
60:         Dim oUI As clsMarsUI = New clsMarsUI

            Dim threadCount As Integer = 0
            Dim maxThreadCount As Integer = 6
            Dim MultiThread As Boolean = False

70:         If reportID > 0 Then
80:             SQL = "SELECT * FROM ReportAttr WHERE ReportID =" & reportID
90:         Else
100:            SQL = "SELECT * FROM ReportAttr r LEFT OUTER JOIN PackagedReportAttr p ON r.ReportID = p.ReportID WHERE r.PackID =" & packID & " AND p.Status = 1" & _
                      " ORDER BY PackOrderID"
110:        End If

120:        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

130:        reportCount = oRs.RecordCount

            If (IsMultithreaded = True) Then

                oUI.BusyProgress(75, "Exporting reports simultaneously")
                ClearThreads()

                maxThreadCount = clsMarsUI.MainUI.ReadRegistry("MaxThreadCount", 6)
                MultiThread = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDThreading", 0)))

                'Wait for the thread count to be less than 6 (or value from the registry)
                If (MultiThread = True) Then
                    Do
                        Application.DoEvents()
                    Loop Until clsMarsThreading.GetThreadDetailCount() < maxThreadCount
                End If

                'Use Connection.Execute instead of RecordSet.Open
                clsMarsData.UseExecute = True

                'Pause any Progress bar updates
                clsMarsUI.Paused = True

                'Log all the reportIDs (ThreadManagerDetail)
                If oRs IsNot Nothing Then
                    Do While (oRs.EOF = False)
                        reportID = oRs(clsMarsData.GetColumnIndex("ReportID", oRs)).Value
                        AddThreadDetail(reportID, Process.GetCurrentProcess.Id(), 1)
                        oRs.MoveNext()
                    Loop
                    oRs.MoveFirst()
                End If

            End If

140:        If oRs IsNot Nothing Then
150:            Do While oRs.EOF = False

160:                If frmMain.m_UserCancel = True Then
170:                    Me.CancelExecution("RunPackageSchedule")
180:                    Return Nothing
                    End If

190:                oUI.BusyProgress((((I + 1) / reportCount) * 100), "Exporting report " & I + 1 & " of " & reportCount & "...")
200:                Dim exportFile As String = ""

210:                reportID = oRs(clsMarsData.GetColumnIndex("ReportID", oRs)).Value

220:                SQL = "SELECT AppendToFile FROM ReportOptions WHERE ReportID =" & reportID

                    Dim rsAppend As ADODB.Recordset = clsMarsData.GetData(SQL)
                    Dim sFormat As String = ""

230:                ReDim Preserve appendToFile(I)

240:                If rsAppend IsNot Nothing Then
250:                    If rsAppend.EOF = False Then
260:                        appendToFile(I) = IsNull(rsAppend("appendtofile").Value, 0)
270:                    End If
280:                End If

290:                rsAppend.Close()

300:                rsAppend = clsMarsData.GetData("SELECT OutputFormat FROM PackagedReportAttr WHERE ReportID =" & reportID)

310:                If rsAppend IsNot Nothing Then
320:                    If rsAppend.EOF = False Then
330:                        sFormat = rsAppend(0).Value
340:                    End If
350:                End If

360:                rsAppend.Close()

370:                Select Case sFormat
                        Case "CSV (*.csv)", "Tab Separated (*.txt)", "Text (*.txt)", "Record Style (*.rec)"
380:                    Case Else
390:                        appendToFile(I) = False
                    End Select

                    If DDKeyValue <> "" Then
                        DDKeyValue = "[" & DDKeyValue & "]: "
                    End If

                    If (IsMultithreaded = True) Then
                        SaveTextToFile(Date.Now & ", " & packID & ", Exporting reports using multiple threads", sAppPath & "package.debug", , True)

                        If (m_ThreadsAborted = True) Then Exit Do

                        threadCount += 1

                        If (MultiThread = True AndAlso clsMarsThreading.GetThreadDetailCount(Process.GetCurrentProcess.Id()) > 0) Then
                            Do
                                Application.DoEvents()
                            Loop Until (clsMarsThreading.GetThreadDetailCount(Process.GetCurrentProcess.Id()) + threadCount) <= maxThreadCount
                        End If

                        If (threadCount > maxThreadCount) Then
                            WaitForThreads(maxThreadCount - 1)
                            If (m_ThreadsAborted = True) Then Exit Do
                            threadCount = GetThreadCount() + 1
                        End If

                        Dim params As New clsRunSingleParam(reportID, 0, packageName, packID, ToPrinter, dateStamp, mergePDF, , ToFax, mergeXL, _
                        adjustStamp, PrintDate, nRetry, rptTimeout, DDKeyValue, IsDataDriven, failonOne)
                        Dim newThread As New Thread(AddressOf RunSingleThread)
                        AddThread(newThread)
                        newThread.Start(params)

                    Else

                        Dim errInfo As clsErrorInfo

                        SaveTextToFile(Date.Now & ", " & packID & ", Exporting report to " & exportFile, sAppPath & "package.debug", , True)

400:                    errInfo = RunSingle(exportFile, reportID, 0, packageName, packID, ToPrinter, dateStamp, mergePDF, , ToFax, mergeXL, _
                        adjustStamp, PrintDate, nRetry, rptTimeout, DDKeyValue, IsDataDriven)

410:                    If exportFile.Length = 0 Or exportFile = "{error}" Then
                            SaveTextToFile(Date.Now & ", " & packID & ", report export error: " & errInfo.ErrorDesc, sAppPath & "package.debug", , True)

420:                        If failonOne = True Then
430:                            _ErrorHandle(errInfo.ErrorDesc, errInfo.ErrorNumber, errInfo.ErrorSource, errInfo.ErrorLine, errInfo.ErrorSuggest)
440:                            Return Nothing
450:                        Else
460:                            _ErrorHandle(errInfo.ErrorDesc, errInfo.ErrorNumber, errInfo.ErrorSource, errInfo.ErrorLine, errInfo.ErrorSuggest)
470:                            GoTo skip
480:                        End If
                        End If

490:                    If exportFile = "{skip}" Then
500:                        GoTo skip
510:                    End If

520:                    If exportFile.Length > 0 And exportFile <> "{error}" And exportFile <> "Printer" Then
530:                        HTMLPath = GetDirectory(HTMLPath)

540:                        If HTMLPath Is Nothing Then HTMLPath = String.Empty

550:                        If HTMLPath.Length = 0 Then
560:                            ReDim Preserve reports(I)
570:                            reports(I) = exportFile

580:                            I += 1
590:                        Else
600:                            For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
610:                                ReDim Preserve reports(I)

620:                                reports(I) = s

630:                                I += 1
640:                            Next

650:                            HTMLPath = String.Empty
660:                        End If
670:                    End If
                    End If
skip:
680:                oRs.MoveNext()
690:            Loop

                If (IsMultithreaded = True) Then
                    SaveTextToFile(Date.Now & ", " & packID & ", Waiting for threads to complete", sAppPath & "package.debug", , True)

                    'Wait for threads to finish
                    WaitForThreads()

                    'If the threads were aborted then there was an error with one of the reports
                    'and the Fail On One option is set. Therefore, throw an exception (Nothing is returned)
                    If (failonOne = True And m_ThreadsAborted = True) Then
                        Dim sMsg As String = "Package completed with the following failures:" & vbCrLf & vbCrLf & m_ThreadMsg
691:                    Throw New Exception(sMsg)
                    End If

                    'Add all the thread results to the reports array if required
                    If (m_ThreadResults.Count > 0) Then
                        I = 0
692:                    ReDim Preserve reports(m_ThreadResults.Count - 1)
                        For Each s As String In m_ThreadResults
                            reports(I) = s
                            I += 1
                        Next
                    End If

                    'Was there an error and Fail On One is NOT set?
                    If (m_ThreadMsg.Length > 0 And RunEditor = True) Then
                        'Use the Custom Message form to display the error
693:                    Dim oMsg As frmCustomMsg = New frmCustomMsg
                        Dim sMsg As String = "Package completed with the following failures:" & vbCrLf & vbCrLf & m_ThreadMsg
                        oMsg.ShowMsg(sMsg, "Package Schedule Results")
                    End If

                End If

            End If

700:        oUI = Nothing

710:        Return reports
720:    Catch ex As Exception
730:        gErrorDesc = ex.Message
740:        gErrorNumber = Err.Number
750:        gErrorLine = Erl()
760:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name

770:        Return Nothing
        Finally
            clsMarsData.UseExecute = False
            clsMarsUI.Paused = False
        End Try
    End Function

    Private Function NumDestinations(ByVal nPackID As Integer, ByVal nReportID As Integer, ByRef dType As String) As Integer
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim num As Integer = 0

        SQL = "SELECT * FROM DestinationAttr "

        If nPackID > 0 Then
            SQL &= " WHERE PackID =" & nPackID
        Else
            SQL &= " WHERE ReportID =" & nReportID
        End If

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                dType = oRs("destinationtype").Value
                num += 1
                oRs.MoveNext()
            Loop

            oRs.Close()
        Else
            num = 2
        End If

        Return num
    End Function

    Public Function RunPackageSchedule(ByVal nPackID As Integer, _
           Optional ByVal ShowFinish As Boolean = False, _
           Optional ByVal CustomSend As String = "", Optional ByVal nReportID As Integer = 0, _
           Optional ByVal ForSnap As Boolean = False, Optional ByVal SnapFiles As String() = Nothing, _
           Optional ByVal IsDynamic As Boolean = False, Optional ByVal RunTasks As Boolean = True, _
           Optional ByVal PrintDate As String = "") As Boolean

        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim SQL As String
        Dim I As Integer = 0
        Dim sPackage As String
        Dim nRetry As Integer
        Dim Compress As Boolean
        Dim CheckBlank As Boolean
        Dim oFile As String
        Dim sFileName As String
10:     Dim oSchedule As New clsMarsScheduler
20:     Dim oTask As New clsMarsTask
        Dim nScheduleID As Integer = oSchedule.GetScheduleID(, nPackID)
        Dim sTemp As String
        Dim Ok As Boolean
        Dim ToPrinter As Boolean = False
        Dim FailOnOne As Boolean = False
        Dim IsMultithreaded As Boolean = False
        Dim MergePDF As Boolean = False
        Dim sMergePDFName As String = ""
        Dim MergeXL As Boolean = False
        Dim sMergeXLName As String = ""
        Dim nDestinationID As Integer
        Dim sPackageStamp As String = ""
30:     Dim oParse As New clsMarsParser
        Dim ToFax As Boolean = False
        Dim sExtras As String
        Dim TempScheduleStart As Date = ScheduleStart
        Dim IsStatic As Boolean
        Dim TempOutputs() As String
        Dim sDestName As String = ""
        Dim adjustStamp As Double
        Dim exportedReports() As String
        Dim destinationType As String = ""
40:     Dim oUI As clsMarsUI = New clsMarsUI
50:     Dim oDynamcData As New clsMarsDynamic
        Dim rptTimeout As Integer
        Dim appendToFile As Boolean()
        Dim dType As String = ""
        Dim NumDestinations As Integer = Me.NumDestinations(nPackID, 0, dType)
        Dim mergeText As Boolean = False
        Dim mergeTextName As String = ""

60:     m_scheduleID = nScheduleID

        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)
70:     gCurrentTempOutputFile = ""
        gScheduleFolderName = clsMarsUI.getObjectParentName(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)

80:     If IsDynamic = False Then ScheduleStart = Now

90:     SaveTextToFile(Date.Now & ", " & nPackID & ", package processing started...", sAppPath & "package.debug", , True)

100:    Try
110:        rptFileNames = Nothing

120:        HTMLPath = ""

130:        Me.m_HistoryID = clsMarsData.CreateDataID("ScheduleHistory", "HistoryID")

140:        ReDim rptFileNames(0)

150:        oUI.BusyProgress(25, "Collecting data...")

160:        If frmMain.m_UserCancel = True Then
170:            Me.CancelExecution("RunPackageSchedule")
180:            Return False
            End If

190:        If (IsDynamic = False) Or (IsDynamic = True And RunTasks = True) Then
200:            oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)
            End If

210:        Me.m_DynamicSchedule = IsDynamic

220:        SaveTextToFile(Date.Now & ", " & nPackID & ", report processing starting...", sAppPath & "package.debug", , True)

230:        SQL = "SELECT * FROM PackageAttr p INNER JOIN DestinationAttr d ON " & _
                  "p.PackID = d.PackID WHERE p.PackID = " & nPackID & " AND d.EnabledStatus = 1 ORDER BY d.DestOrderID"
240:
250:        oRs = clsMarsData.GetData(SQL)

260:        If oRs Is Nothing Then
270:            gErrorDesc = "Could not obtain a valid recordset"
280:            gErrorNumber = -101
290:            gErrorSource = "clsMarsReport._RunPackageSchedule"
300:            gErrorLine = 110
310:            Return False
320:        End If

330:        If frmMain.m_UserCancel = True Then
340:            Me.CancelExecution("RunPackageSchedule")
350:            Return False
            End If

360:        If oRs.EOF = False Then

370:            sPackage = oRs("PackageName").Value
380:            nRetry = IsNull(oRs("Retry").Value, 0)
390:            CheckBlank = Convert.ToBoolean(oRs("CheckBlank").Value)
400:            FailOnOne = Convert.ToBoolean(oRs("failonone").Value)
410:            gScheduleName = sPackage
420:            adjustStamp = IsNull(oRs("adjustpackagestamp").Value, 0)
430:            rptTimeout = IsNull(oRs("assumefail").Value, 60)

                Try
                    IsMultithreaded = Convert.ToBoolean(oRs("multithreaded").Value)
                Catch : End Try

                '//check to see if the user has multi-threading purchased otherwise we have to force it off
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
                    IsMultithreaded = False
                End If

440:            Try
450:                If Convert.ToBoolean(oRs("datetimestamp").Value) = True Then
460:                    sPackageStamp = IsNull(oRs("stampformat").Value)
470:                End If
                Catch : End Try

480:            Try
490:                MergePDF = Convert.ToBoolean(oRs("mergepdf").Value)
500:                sMergePDFName = oRs("mergepdfname").Value
510:            Catch
520:                MergePDF = False
                End Try

530:            Try
540:                MergeXL = Convert.ToBoolean(oRs("mergexl").Value)
550:                sMergeXLName = oRs("mergexlname").Value
560:            Catch ex As Exception
570:                MergeXL = False
                End Try

580:            If frmMain.m_UserCancel = True Then
590:                Me.CancelExecution("RunPackageSchedule")
600:                Return False
                End If


610:            If ForSnap = True And SnapFiles IsNot Nothing Then
620:                exportedReports = SnapFiles
630:            Else
                    If Not (dType = "Printer" And NumDestinations = 1) Then
640:                    exportedReports = Me.ProduceReportsForPackage(nPackID, sPackage, sPackageStamp, MergePDF, MergeXL, _
                              adjustStamp, FailOnOne, appendToFile, nReportID, , , PrintDate, nRetry, rptTimeout, , , IsMultithreaded)
                    End If
                End If

650:            If exportedReports Is Nothing And Not (dType = "Printer" And NumDestinations = 1) Then
660:                If gErrorDesc <> "" Then
670:                    Throw New Exception(Me._CreateErrorString())
680:                Else
690:                    Return True
                    End If
                End If
            End If

700:        oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)

710:        If frmMain.m_UserCancel = True Then
720:            Me.CancelExecution("RunPackageSchedule")
730:            Return False
            End If

740:        SaveTextToFile(Date.Now & ", " & nPackID & ", loading destinations...", sAppPath & "package.debug", , True)

            Dim reportCount As Integer = exportedReports.Length

750:        Do While oRs.EOF = False
760:            If frmMain.m_UserCancel = True Then
770:                Me.CancelExecution("RunPackageSchedule")
780:                Return False
                End If

790:            oUI.BusyProgress(50, "Processing destinations...")
800:            rptFileNames = Nothing
810:            I = 0

910:            sDestName = IsNull(oRs("destinationname").Value)
920:            Compress = Convert.ToBoolean(oRs("Compress").Value)
930:            nDestinationID = oRs("destinationid").Value
940:            destinationType = IsNull(oRs("destinationtype").Value)

1270:           If destinationType.ToLower = "printer" Then
1280:               rptFileNames = Me.ProduceReportsForPackage(nPackID, sPackage, "", False, False, 0, FailOnOne, appendToFile, nReportID, True, False, PrintDate, nRetry, , , , IsMultithreaded)
1290:           ElseIf destinationType.ToLower = "fax" Then
1300:               rptFileNames = Me.ProduceReportsForPackage(nPackID, sPackage, "", False, False, 0, FailOnOne, appendToFile, nReportID, False, True, PrintDate, nRetry, , , , IsMultithreaded)
                Else
820:                For Each s As String In exportedReports
830:                    If s.Length > 0 Then
840:                        ReDim Preserve rptFileNames(I)

850:                        rptFileNames(I) = s.Replace(Chr(34), "").Trim

860:                        I += 1
870:                    End If
880:                Next
                End If

890:            Try
900:


950:                Try
960:                    If Convert.ToBoolean(oRs("datetimestamp").Value) = True Then
970:                        sPackageStamp = IsNull(oRs("stampformat").Value)
980:                    End If
990:                Catch : End Try

1000:               Try
1010:                   MergePDF = Convert.ToBoolean(oRs("mergepdf").Value)
1020:                   If sPackageStamp <> "" Then
1030:                       sMergePDFName = oRs("mergepdfname").Value & Date.Now.AddDays(adjustStamp).ToString(sPackageStamp)
1040:                   Else
1050:                       sMergePDFName = oRs("mergepdfname").Value
                        End If
1060:               Catch
1070:                   MergePDF = False
1080:               End Try

1090:               Try
1100:                   MergeXL = Convert.ToBoolean(oRs("mergexl").Value)
1110:                   If sPackageStamp <> "" Then
1120:                       sMergeXLName = oRs("mergexlname").Value & Date.Now.AddDays(adjustStamp).ToString(sPackageStamp)
1130:                   Else
1140:                       sMergeXLName = oRs("mergexlname").Value
                        End If
1150:
1160:               Catch ex As Exception
1170:                   MergeXL = False
1180:               End Try

                    Try
                        mergeText = Convert.ToBoolean(oRs("mergetext").Value)

                        If sPackageStamp <> "" Then
                            mergeTextName = oRs("mergetextname").Value & Date.Now.AddDays(adjustStamp).ToString(sPackageStamp)
                        Else
                            mergeTextName = oRs("mergetextname").Value
                        End If
                    Catch ex As Exception
                        mergeText = False
                    End Try

1190:               Try
1200:                   IsDynamic = Convert.ToBoolean(oRs("dynamic").Value)
1210:               Catch ex As Exception
1220:                   IsDynamic = False
                    End Try

1230:               Try
1240:                   IsStatic = Convert.ToBoolean(oRs("staticdest").Value)
1250:               Catch ex As Exception
1260:                   IsStatic = False
                    End Try



1310:               If frmMain.m_UserCancel = True Then
1320:                   Me.CancelExecution("RunPackageSchedule")
1330:                   Return False
                    End If
1340:
1350:               ScheduleStart = TempScheduleStart
ResumeSnaps:
                    Dim Proceed As Boolean = False

1360:               For Each s As String In rptFileNames
1370:                   If Not s Is Nothing Then
1380:                       Proceed = True
1390:                   End If
1400:               Next

1410:               If Proceed = False Then Return False

                    'If ForSnap = False Then oRs1.Close()
1420:               If frmMain.m_UserCancel = True Then
1430:                   Me.CancelExecution("RunPackageSchedule")
1440:                   Return False
                    End If

1450:               If MergePDF = True And ForSnap = False Then
1460:                   appendToFile = Nothing

1470:                   PreparePDF(oParse.ParseString(sMergePDFName, , , , , , m_ParametersTable), True)

1480:                   oRs1 = clsMarsData.GetData("SELECT * FROM PackageOptions WHERE PackID = " & nPackID)

1490:                   If oRs1.EOF = False Then
1500:                       Dim oPerm As New clsMarsPDF
                            Dim sOwnerPass As String
                            Dim sUserPass As String

1510:                       For n As Integer = 0 To rptFileNames.GetUpperBound(0)
1520:                           If rptFileNames(n).IndexOf(".pdf") > -1 Then

1530:                               Try
1540:                                   Dim infoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer, sWatermark As String
1550:                                   Dim InfoCreated As Date

1560:                                   sOwnerPass = oRs1("pdfpassword").Value
1570:                                   sUserPass = oRs1("userpassword").Value

1580:                                   infoTitle = oParse.ParseString(oRs1("infotitle").Value, , , , , , m_ParametersTable)
1590:                                   InfoAuthor = oParse.ParseString(oRs1("infoauthor").Value, , , , , , m_ParametersTable)
1600:                                   InfoSubject = oParse.ParseString(oRs1("infosubject").Value, , , , , , m_ParametersTable)
1610:                                   InfoKeywords = oParse.ParseString(oRs1("infokeywords").Value, , , , , , m_ParametersTable)
1620:                                   InfoProducer = oParse.ParseString(oRs1("infoproducer").Value, , , , , , m_ParametersTable)
1630:                                   InfoCreated = oRs1("infocreated").Value
1640:                                   sWatermark = IsNull(oRs1("pdfwatermark").Value)

1650:                                   Me._PostProcessPDF(rptFileNames(n), infoTitle, InfoAuthor, InfoSubject, _
                                             InfoKeywords, InfoProducer, InfoCreated, IsDynamic, 0, Nothing, _
                                             Convert.ToBoolean(oRs1("pdfsecurity").Value), False, oRs1("canprint").Value, _
                                             oRs1("cancopy").Value, _
                                             oRs1("canedit").Value, _
                                             oRs1("cannotes").Value, oRs1("canfill").Value, oRs1("canaccess").Value, _
                                             oRs1("canassemble").Value, _
                                             oRs1("canprintfull").Value, sOwnerPass, sUserPass, sWatermark, 0, False, Date.Now)

1660:                                   oPerm.SetPDFSummary(infoTitle, InfoAuthor, _
                                             InfoSubject, InfoKeywords, InfoCreated, InfoProducer _
                                             , rptFileNames(n), False, Date.Now)
1670:                               Catch : End Try
1680:                           End If
1690:                       Next
1700:                   End If

1710:                   oRs1.Close()
1720:               End If

1730:               If frmMain.m_UserCancel = True Then
1740:                   Me.CancelExecution("RunPackageSchedule")
1750:                   Return False
                    End If

1760:               If MergeXL = True And ForSnap = False Then
1770:                   appendToFile = Nothing

1771:                   If PrepareXLS(m_OutputFolder & oParse.ParseString(sMergeXLName, , , , , , m_ParametersTable)) = False Then
                            Throw New Exception(Me._CreateErrorString)
                        End If

1790:                   Try

                            Dim oRsx As ADODB.Recordset

                            'protect it
1800:                       SQL = "SELECT ProtectExcel, ExcelPassword FROM PackageOptions WHERE PackID = " & nPackID

1810:                       oRsx = clsMarsData.GetData(SQL)

1820:                       If oRsx.EOF = False Then
1830:                           If IsNull(oRsx.Fields(0).Value, "0") = "1" Then
1840:                               Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)
1850:                               oXL.SetWorkBookPassword(rptFileNames(rptFileNames.GetUpperBound(0)), _DecryptDBValue(oRsx(1).Value))
1860:                               oXL.Dispose()
                                End If
                            End If

1870:                       oRsx.Close()
                        Catch : End Try
1880:               End If

                    '//merge text files 
                    If mergeText = True And ForSnap = False Then
                        appendToFile = Nothing

                        If prepareTextFiles(m_OutputFolder & oParse.ParseString(mergeTextName, , , , , , m_ParametersTable)) = False Then
                            Throw New Exception(Me._CreateErrorString)
                        End If
                    End If

1890:               If frmMain.m_UserCancel = True Then
1900:                   Me.CancelExecution("RunPackageSchedule")
1910:                   Return False
                    End If

                    'carry out encryption if needed
1920:               If ForSnap = False Then
1930:                   Dim oPGP As New clsMarsPGP
1940:                   Dim oPGPresult() As Object
                        Dim nFile As Integer = 0

1950:                   For Each s As String In rptFileNames
1960:                       If s.Length > 0 Then
1970:                           oPGPresult = oPGP.EncryptFile(s, nDestinationID)

1980:                           If oPGPresult(0) = False Then
1990:                               Throw New Exception("PGP Encryption Error: " & vbCrLf & Me._CreateErrorString)
                                End If

2000:                           rptFileNames(nFile) &= oPGPresult(1)

2010:                           nFile += 1
                            End If
2020:                   Next
                    End If

2030:               If frmMain.m_UserCancel = True Then
2040:                   Me.CancelExecution("RunPackageSchedule")
2050:                   Return False
                    End If

2060:               If Compress = True Then

2070:                   oUI.BusyProgress(60, "Zipping reports...")

                        Dim Encrypt As Boolean
                        Dim ZipCode As String = ""
                        Dim strNow As String
                        Dim sExt As String

2080:                   Try
2090:                       If oRs("appenddatetime").Value = 1 Then
2100:                           strNow = Date.Now.ToString(oRs("datetimeformat").Value)
2110:                       Else
2120:                           strNow = String.Empty
2130:                       End If

2140:                       If IsNull(oRs("customname").Value, "") <> "" Then
2150:                           sPackage = oRs("customname").Value
2160:                       End If

2170:                       If IsNull(oRs("customext").Value, "") <> "" Then
2180:                           sExt = oRs("customext").Value
2190:                       Else
2200:                           sExt = ".zip"
2210:                       End If

2220:                       If sExt.StartsWith(".") = False Then
2230:                           sExt = "." & sExt
2240:                       End If
2250:                   Catch : End Try

2260:                   Try
2270:                       Encrypt = oRs("encryptzip").Value
2280:                       ZipCode = IsNull(oRs("encryptzipcode").Value, "")
2290:                   Catch : Encrypt = False : End Try

                        sPackage = clsMarsParser.Parser.ParseString(sPackage)

2300:                   ZipFiles("Package", m_OutputFolder & sPackage & strNow & sExt, , Encrypt, ZipCode)

2310:                   ReDim rptFileNames(0)

2320:                   rptFileNames(0) = m_OutputFolder & sPackage & strNow & sExt

2330:                   appendToFile = Nothing
2340:               End If
2350:

2360:               Try
2370:                   gExportedFileName = ""

2380:                   For Each s As String In rptFileNames
2390:                       gExportedFileName &= ExtractFileName(s) & vbCrLf
2400:                   Next

2410:                   gExportedFileName = gExportedFileName.Substring(0, gExportedFileName.Length - 2)
2420:               Catch : End Try

2430:
                    ' If I = 0 Then Return True

2440:               Dim nDefer As Integer

2450:               Try
2460:                   nDefer = oRs("deferdelivery").Value
2470:               Catch ex As Exception
2480:                   nDefer = 0
2490:               End Try

2500:               If frmMain.m_UserCancel = True Then
2510:                   Me.CancelExecution("RunPackageSchedule")
2520:                   Return False
                    End If

2530:               If nDefer = 1 And RunEditor = False Then
                        Dim fileCollection As String = ""
2540:                   Dim nDeferBy As Double = oRs("deferby").Value

2550:                   For Each s As String In rptFileNames
2560:                       fileCollection &= s & "|"
2570:                   Next

2580:                   fileCollection = fileCollection.Substring(0, fileCollection.Length - 1)

2590:                   Ok = Me._DeferDelivery(nDestinationID, fileCollection, nDeferBy, sPackage)
2600:               Else

                        Dim sSendTo As String
                        Dim sSubject, senderName, senderAddress As String
                        Dim sCc As String
                        Dim sBcc As String
                        Dim sMsg As String
                        Dim sPath As String
                        Dim SMTPServer As String = ""
                        Dim ReadReceipt As Boolean = False

2610:                   If frmMain.m_UserCancel = True Then
2620:                       Me.CancelExecution("RunPackageSchedule")
2630:                       Return False
                        End If

2640:                   If ForSnap = False Then clsMarsSnapshots.CreateSnapshot(nPackID, rptFileNames)

2650:                   Select Case CType(oRs("DestinationType").Value, String).ToLower
                            Case "printer"
2660:                           oUI.BusyProgress(75, "Printing reports...")
2670:                           Ok = Me._PrintServer(rptFileNames, nDestinationID)
2680:                       Case "email"
2690:                           If IsDynamic = False Or IsStatic = True Then
2700:                               If CustomSend.Length = 0 Then
2710:                                   sSendTo = ResolveEmailAddress(oRs("sendto").Value)
2720:                                   sCc = ResolveEmailAddress(oRs("cc").Value)
2730:                                   sBcc = ResolveEmailAddress(oRs("bcc").Value)

2740:                                   sSendTo = oParse.ParseString(sSendTo, , , , , , m_ParametersTable)
2750:                                   sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
2760:                                   sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)

2770:                               Else
2780:                                   sSendTo = CustomSend
2790:                               End If
2800:                           Else
                                    Dim sDynamic() As String

2810:                               sDynamic = oDynamcData._GetDynamicData(nPackID, sKeyParameter, sKeyValue, "Package")

2820:                               If sDynamic Is Nothing Then
2830:                                   Return False
2840:                               End If

2850:                               For Each s As String In sDynamic
2860:                                   If s.Length > 0 Then
2870:                                       sSendTo &= s & ";"
2880:                                   End If
2890:                               Next

2900:                               If sSendTo.Length = 0 Then
2910:                                   gErrorDesc = sPackage & ": The linked column did not contain a valid email address"
2920:                                   gErrorNumber = -101
2930:                                   gErrorSource = "clsMarsReport._RunPackageSchedule"
2940:                                   Return False
2950:                               End If

2960:                               sCc = ResolveEmailAddress(oRs("cc").Value)
2970:                               sBcc = ResolveEmailAddress(oRs("bcc").Value)

2980:                               sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
2990:                               sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)
                                End If

3000:                           sSubject = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
3010:                           SMTPServer = IsNull(oRs("smtpserver").Value, "Default")
3011:                           senderName = oParse.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
3012:                           senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)

3020:                           sExtras = oRs("extras").Value

3030:                           sMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

3040:                           Try
3050:                               ReadReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
3060:                           Catch ex As Exception
3070:                               ReadReceipt = False
                                End Try

3080:                           sSubject = Me._CheckForDynamic(IsDynamic, sSubject, nPackID, sKeyParameter, sKeyValue, "Package")
3090:                           sMsg = _CheckForDynamic(IsDynamic, sMsg, nPackID, sKeyParameter, sKeyValue, "Package")

3100:                           oUI.BusyProgress(75, "Emailing report...")

3110:                           If MailType = MarsGlobal.gMailType.MAPI Then
3120:                               Ok = SendMAPI( _
                                         sSendTo, sSubject, sMsg, "Package", _
                                         , I, sExtras, sCc, sBcc, False, IsNull(oRs("MailFormat").Value), sPackage, , , ReadReceipt, nDestinationID)
3130:                           ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then

                                    Dim objSender As clsMarsMessaging = New clsMarsMessaging

3140:                               Ok = objSender.SendSMTP( _
                                         sSendTo, sSubject, sMsg, "Package", _
                                         , I, sExtras, sCc, sBcc, sPackage, False, , , , IsNull(oRs("MailFormat").Value), _
                                         SMTPServer, , senderName, senderAddress)
3150:                           ElseIf MailType = gMailType.GROUPWISE Then
3160:                               Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, "", "Package", sExtras, _
     True, False, "", True, I, sPackage, "")
3170:                           Else
3180:                               Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
3190:                           End If

3200:                       Case "disk"
                                Dim HouseTemp As String
3210:                           Dim oSys As New clsSystemTools
                                Dim UseDUN As Boolean
                                Dim sDUN As String
                                Dim oNet As clsNetworking
                                Dim append As Boolean = False

                                'if DUN is used...
3220:                           Try
3230:                               UseDUN = Convert.ToBoolean(oRs("usedun").Value)
3240:                               sDUN = IsNull(oRs("dunname").Value)
3250:                           Catch ex As Exception
3260:                               UseDUN = False
3270:                           End Try

3280:                           If UseDUN = True Then
3290:                               oNet = New clsNetworking

3300:                               If oNet._DialConnection(sDUN, "Package Error: " & sPackage & ": ") = False Then
3310:                                   Throw New Exception(Me._CreateErrorString)
3320:                               End If
3330:                           End If

3340:                           oUI.BusyProgress(75, "Copying reports...")

3350:                           If IsDynamic = False Or IsStatic = True Then
                                    Dim pointer As Integer = 0

3360:                               For Each oFile In rptFileNames
3370:                                   If appendToFile IsNot Nothing Then
3380:                                       Try
3390:                                           append = appendToFile(pointer)
3400:                                       Catch
3410:                                           append = False
                                            End Try
                                        End If


3420:                                   Dim paths As String = ""

3430:                                   paths = IsNull(oRs("outputpath").Value, "")
3440:
3450:                                   If paths.EndsWith("|") Then paths = paths.Substring(0, paths.Length - 1)

3460:                                   For Each item As String In paths.Split("|")
3470:                                       If item IsNot Nothing And item.Length > 0 Then
3480:                                           sPath = oParse.ParseString(item, , , , , , m_ParametersTable)

3490:                                           sFileName = ExtractFileName(oFile)

3500:                                           If sPath.EndsWith("\") = False Then sPath &= "\"

3510:                                           sPath = _CreateUNC(sPath)

3520:                                           Ok = oParse.ParseDirectory(sPath)

3530:                                           HouseTemp = sPath

3540:                                           sPath &= sFileName

3550:                                           If append = True And IO.File.Exists(sPath) Then
                                                    Dim sRead As String = vbCrLf & ReadTextFromFile(oFile) & vbCrLf

3560:                                               SaveTextToFile(sRead, sPath, , True, True)
3570:                                           Else
3580:                                               System.IO.File.Copy(oFile, sPath, True)
                                                End If

3590:                                           Try
3600:                                               oSys._HouseKeeping(nDestinationID, HouseTemp, sFileName.Split(".")(0))
3610:                                           Catch : End Try
                                            End If
3620:                                   Next
3630:                                   pointer += 1
3640:                               Next
3650:                           Else
                                    Dim paths As String = ""

3660:                               paths = IsNull(oRs("outputpath").Value)

3670:                               If paths.EndsWith("|") Then paths = paths.Substring(0, paths.Length - 1)


3680:                               For Each item As String In paths.Split("|")
3690:                                   sPath = oParse.ParseString(item, , , , , , m_ParametersTable)

3700:                                   sPath = Me._CheckForDynamic(IsDynamic, sPath, nPackID, sKeyParameter, sKeyValue, "Package")

3710:                                   If sPath.EndsWith("\") = False Then sPath &= "\"

3720:                                   Dim sDynamic() As String

3730:                                   sDynamic = oDynamcData._GetDynamicData(nPackID, _
                                             sKeyParameter, sKeyValue, "Package")
3740:
3750:                                   If sDynamic Is Nothing Then
3760:                                       Throw New Exception(Me._CreateErrorString)
3770:                                   End If

3780:                                   If sDynamic(0) = String.Empty Then
3790:                                       Throw New Exception("The dynamic query did not return any valid directory values")
                                        End If

3800:                                   For Each s As String In sDynamic
3810:                                       If s.Length > 0 Then
3820:                                           sTemp = sPath

3830:                                           If s.EndsWith("\") = False Then
3840:                                               If s.IndexOf(":") > -1 Then
3850:                                                   sTemp = s & "\" & sTemp
3860:                                               Else
3870:                                                   sTemp &= s & "\"
3880:                                               End If
3890:                                           Else
3900:                                               If s.IndexOf(":") > -1 Then
3910:                                                   sTemp = s & sTemp
3920:                                               Else
3930:                                                   sTemp &= s
3940:                                               End If
3950:                                           End If

3960:                                           sTemp = _CreateUNC(sTemp)

3970:                                           Ok = oParse.ParseDirectory(sTemp)

3980:                                           If sTemp.EndsWith("\") = False Then
3990:                                               sTemp &= "\"
                                                End If

                                                Dim pointer As Integer = 0

4000:                                           For Each sExport As String In rptFileNames
4010:                                               If appendToFile IsNot Nothing Then
4020:                                                   Try
4030:                                                       append = appendToFile(pointer)
4040:                                                   Catch
4050:                                                       append = False
                                                        End Try
                                                    End If

4060:                                               sFileName = ExtractFileName(sExport)

4070:                                               If append = True And IO.File.Exists(sTemp & sFileName) Then
4080:                                                   Dim sRead As String = ReadTextFromFile(sExport)

4090:                                                   SaveTextToFile(sRead, sTemp & sFileName, , True, True)
4100:                                               Else
4110:                                                   System.IO.File.Copy(sExport, sTemp & sFileName, True)
                                                    End If

4120:                                               pointer += 1
4130:                                           Next
4140:                                       End If
4150:                                   Next
4160:                               Next
                                End If
4170:

4180:                           Try
4190:                               If UseDUN = True Then
4200:                                   oNet._Disconnect()
                                    End If
                                Catch : End Try

4210:                       Case "printer"
4220:                           oUI.BusyProgress(75, "Printing reports...")
4230:                       Case "ftp"
                                Dim sFtp As String = ""
4240:                           Dim oFtp As New clsMarsTask
                                Dim FTPServer As String
                                Dim FTPUser As String
                                Dim FTPPassword As String
                                Dim FTPPath As String
                                Dim FTPType As String
                                Dim FTPPassive As String
                                Dim ftpOptions As String

4250:                           FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
4260:                           FTPUser = oParse.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
4270:                           FTPPassword = oParse.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
4280:                           FTPPath = oParse.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
4290:                           FTPType = IsNull(oRs("ftptype").Value, "FTP")
                                FTPPassive = IsNull(oRs("ftppassive").Value, "")
                                ftpOptions = IsNull(oRs("ftpoptions").Value, "")

4300:                           Dim ftpCount As Integer = FTPServer.Split("|").GetUpperBound(0)

4310:                           If ftpCount > 0 Then
4320:                               ftpCount -= 1
4330:                           Else
4340:                               ftpCount = 0
4350:                           End If

4360:                           oUI.BusyProgress(75, "Uploading reports...")

4370:                           For z As Integer = 0 To ftpCount
                                    Dim l_FTPServer As String = FTPServer.Split("|")(z)
4380:                               Dim l_FTPUser As String = FTPUser.Split("|")(z)
4390:                               Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(z))
4400:                               Dim l_FTPPath As String = FTPPath.Split("|")(z)
4410:                               Dim l_FTPType As String = FTPType.Split("|")(z)
                                    Dim l_FTPPassive As Boolean = False
                                    Dim l_FtpOptions As String

                                    Try
                                        l_FtpOptions = ftpOptions.Split("|")(z)
                                    Catch ex As Exception
                                        l_FtpOptions = ""
                                    End Try

                                    Try
4411:                                   l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(z)))
                                    Catch ex As Exception
                                        l_FTPPassive = False
                                    End Try

4420:                               If IsDynamic = False Or IsStatic = True Then

4430:                                   For Each oFile In rptFileNames
4440:                                       sFtp &= oFile & "|"
4450:                                   Next

4460:                                   Ok = oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, l_FTPPassword, _
                                        l_FTPPath, sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)
4470:                               Else
4490:                                   Dim sDynamic() As String

4500:                                   sDynamic = oDynamcData._GetDynamicData(nPackID, sKeyParameter, sKeyValue)

4510:                                   If sDynamic Is Nothing Then
4520:                                       Throw New Exception(Me._CreateErrorString)
4530:                                   End If

4540:                                   For Each s As String In sDynamic
4550:                                       l_FTPUser = Me._CheckForDynamic(IsDynamic, l_FTPUser, nPackID, sKeyParameter, sKeyValue, "Package")
4560:                                       l_FTPPassword = Me._CheckForDynamic(IsDynamic, l_FTPPassword, nPackID, sKeyParameter, sKeyValue, "Package")
4570:                                       l_FTPPath = Me._CheckForDynamic(IsDynamic, l_FTPPath, nPackID, sKeyParameter, sKeyValue, "Package")

                                            Try
                                                l_FtpOptions = ftpOptions.Split("|")(z)
                                            Catch ex As Exception
                                                l_FtpOptions = ""
                                            End Try

                                            Try
4501:                                           l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(z)))
                                            Catch ex As Exception
                                                l_FTPPassive = False
                                            End Try

4580:                                       For Each oFile In rptFileNames
4590:                                           sFtp &= oFile & "|"
4600:                                       Next

4610:                                       Ok = oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, l_FTPPassword, _
                                            l_FTPPath, sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)

4620:
4630:                                   Next
                                    End If
4640:                           Next
4650:                       Case "fax"
                                Dim faxNumber As String
                                Dim faxDevice As String
                                Dim faxTo As String
                                Dim faxFrom As String
                                Dim faxComments As String
                                Dim faxer As clsMarsMessaging = New clsMarsMessaging

4660:                           faxDevice = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
4670:                           faxTo = oParse.ParseString(oRs("cc").Value, , , , , , m_ParametersTable)
4680:                           faxFrom = oParse.ParseString(oRs("bcc").Value, , , , , , m_ParametersTable)
4690:                           faxComments = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

4700:                           If IsDynamic = False Or IsStatic = True Then
4710:                               faxNumber = oRs("sendto").Value

4720:                               Ok = faxer.SendFax(faxNumber, faxDevice, "Package", faxTo, faxFrom, faxComments, "")
4730:                           Else
                                    Dim sDynamic() As String
4740:                               Dim oDynamic As New clsMarsDynamic

4750:                               sDynamic = oDynamic._GetDynamicData(nPackID, sKeyParameter, sKeyValue, "Package")

4760:                               If sDynamic Is Nothing Then
4770:                                   Throw New Exception(Me._CreateErrorString)
4780:                               End If

4790:                               For Each s As String In sDynamic
4800:                                   If s.Length > 0 Then
4810:                                       faxNumber &= s & ";"
4820:                                   End If
4830:                               Next

4840:                               faxDevice = Me._CheckForDynamic(IsDynamic, faxDevice, nPackID, sKeyParameter, sKeyValue, "Package")

4850:                               faxTo = Me._CheckForDynamic(IsDynamic, faxTo, nPackID, sKeyParameter, sKeyValue, "Package")

4860:                               faxFrom = Me._CheckForDynamic(IsDynamic, faxFrom, nPackID, sKeyParameter, sKeyValue, "Package")


4870:                               faxComments = Me._CheckForDynamic(IsDynamic, faxTo, nPackID, sKeyParameter, sKeyValue, "Package")

4880:                               Ok = faxer.SendFax(faxNumber, faxDevice, "Package", faxTo, faxFrom, faxComments, "")
                                End If
4890:                       Case "sms"
4900:                           Dim cellNumber As String
                                Dim textMsg As String

4910:                           textMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

4920:                           If IsDynamic = False Or IsStatic = True Then
4930:                               cellNumber = oParse.ParseString(oRs("sendto").Value, , , , , , m_ParametersTable)

4940:                               For Each s As String In rptFileNames
4950:                                   Ok = SendSMS(cellNumber, textMsg, s)
4960:                               Next

4970:                           Else
                                    Dim sDynamic() As String
4980:                               Dim oDynamic As New clsMarsDynamic

4990:                               sDynamic = oDynamic._GetDynamicData(nPackID, sKeyParameter, sKeyValue, "Package")

5000:                               If sDynamic Is Nothing Then
5010:                                   Throw New Exception(Me._CreateErrorString)
                                    End If

5020:                               For Each s As String In sDynamic
5030:                                   cellNumber &= s & ";"
5040:                               Next


5050:                               textMsg = Me._CheckForDynamic(IsDynamic, textMsg, nPackID, sKeyParameter, sKeyValue, "Package")

5060:                               For Each s As String In rptFileNames
5070:                                   Ok = SendSMS(cellNumber, textMsg, s)
5080:                               Next
                                End If
                            Case "sharepoint"
                                Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint

                                Dim spServer As String
                                Dim spUser As String
                                Dim spPassword As String
                                Dim spLib As String
                                Dim spCount As Integer = 0

                                spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                                spUser = oParse.ParseString(oRs("ftpusername").Value)
                                spPassword = oParse.ParseString(oRs("ftppassword").Value)
                                spLib = oParse.ParseString(oRs("ftppath").Value)

                                oUI.BusyProgress(75, "Uploading report to SharePoint...")

                                spCount = spServer.Split("|").GetUpperBound(0)

                                If spCount > 0 Then
                                    spCount -= 1
                                Else
                                    spCount = 0
                                End If

                                For z As Integer = 0 To spCount
                                    Dim l_spServer As String = spServer.Split("|")(z)
                                    Dim l_spUser As String = spUser.Split("|")(z)
                                    Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(z))
                                    Dim l_spLib As String = spLib.Split("|")(z)
                                    Dim errorInfo As Exception = Nothing

                                    If l_spServer = "" Then Continue For

                                    If IsDynamic = False Or IsStatic = True Then

                                        If l_spServer = "" Then Continue For

                                        Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, rptFileNames, l_spUser, l_spPassword, errorInfo)

                                        If Ok = False And errorInfo IsNot Nothing Then
                                            Throw errorInfo
                                        End If

                                    Else
                                        Dim sDynamic As String() = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

                                        If sDynamic Is Nothing Then
                                            Throw New Exception(Me._CreateErrorString)
                                        End If

                                        l_spUser = Me._CheckForDynamic(IsDynamic, l_spUser, nReportID, sKeyParameter, sKeyValue)
                                        l_spPassword = Me._CheckForDynamic(IsDynamic, l_spPassword, nReportID, sKeyParameter, sKeyValue)
                                        l_spLib = Me._CheckForDynamic(IsDynamic, l_spLib, nReportID, sKeyParameter, sKeyValue)

                                        For Each s As String In sDynamic
                                            Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, rptFileNames, l_spUser, l_spPassword, errorInfo)

                                            If Ok = False And errorInfo IsNot Nothing Then
                                                Throw errorInfo
                                            End If
                                        Next
                                    End If
                                Next

                                Ok = True
                            Case "dropbox"
                                Dim db As clsDropboxDestination = New clsDropboxDestination(nDestinationID)
                                Dim files As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)

                                For Each s As String In rptFileNames
                                    files.Add(s)
                                Next

                                If IsDynamic = False Or IsStatic = True Then
                                    Dim errInfo As Exception

                                    Ok = db.putFiles(errInfo, files)

                                    If Ok = False Then
                                        Throw errInfo
                                    End If
                                Else
                                    Dim sDynamic As String() = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

                                    If sDynamic Is Nothing Then
                                        Throw New Exception(Me._CreateErrorString)
                                    End If

                                    For Each s As String In sDynamic
                                        Dim errInfo As Exception

                                        Ok = db.putFiles(errInfo, files, s)

                                        If Ok = False Then
                                            Throw errInfo
                                        End If
                                    Next

                                End If
                        End Select
                    End If
5090:           Catch ex As Exception
5100:               If IsDynamic = False Then
5110:                   Dim sFullError As String

5120:                   sFullError = "Destination Error: " & sDestName & vbCrLf & vbCrLf & "+++++++++++++++++++++++" & vbCrLf & ex.Message & vbCrLf & _
                             "+++++++++++++++++++++++"

5130:                   _ErrorHandle(sFullError, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)

5140:                   gErrorCollection &= sFullError & vbCrLf & vbCrLf

5150:                   Ok = True
                    End If
                End Try

5160:           SaveTextToFile(Date.Now & ", " & nPackID & ", loading next destination...", sAppPath & "package.debug", , True)

5170:           oRs.MoveNext()
5180:       Loop

5190:       oRs.Close()

5200:       SaveTextToFile(Date.Now & ", " & nPackID & ", package processing complete...", sAppPath & "package.debug", , True)

5210:       If Ok = False Then
5220:           _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest)
            End If

5230:       If Ok = True Then
5240:           If (IsDynamic = False) Or (IsDynamic = True And RunTasks = True) Then
5250:               oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)
                End If
            End If

5260:       oUI.BusyProgress(95, "Cleaning up...")

5270:       oUI.BusyProgress(, , True)

5280:       HTMLPath = ""

            Try
                Dim outputCount As Integer = outputTracker.calculateOutput(1, 1, reportCount)
                outputTracker.addOutput(outputCount)
            Catch : End Try

5290:       Return Ok

5300:   Catch ex As Exception
            Dim sError As String

            sError = "Package Error: " & sPackage & vbCrLf & ex.Message
5310:       gErrorDesc = sError
5320:       gErrorNumber = Err.Number
5330:       gErrorSource = "clsMarsReport._RunPackageSchedule"
5340:       gErrorLine = Erl()
5350:       oUI.BusyProgress(, , True)


5380:       Return False
5390:   Finally
5400:       oUI = Nothing
            gScheduleOwner = ""
5410:       gScheduleName = String.Empty
        End Try
    End Function

    Public Overloads Function RunSingle(ByRef sExportVal As String, _
                                        ByVal nReportID As Integer, _
                                        ByVal nDestinationID As Integer, _
                                        ByVal sPackageName As String, _
                                        ByVal nPackID As Integer, _
                                        Optional ByVal ToPrinter As Boolean = False, _
                                        Optional ByVal sPackageDateStamp As String = "", _
                                        Optional ByVal MergePDF As Boolean = False, _
                                        Optional ByVal sFormat As String = "", _
                                        Optional ByVal ToFax As Boolean = False, _
                                        Optional ByVal MergeXL As Boolean = False, _
                                        Optional ByVal adjustStamp As Double = 0, _
                                        Optional ByVal PrintDate As String = "", _
                                        Optional ByVal nRetry As Integer = 0, _
                                        Optional ByVal rptTimeout As Integer = 60, _
                                        Optional ByVal DDKeyValue As String = "", _
                                        Optional ByVal IsDataDriven As Boolean = False) As clsErrorInfo

        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sExport As String = ""
        Dim sReportPath As String
        Dim sReportName As String
        Dim sFileName As String
        Dim sDateStamp As String
        Dim sFormula As String
        Dim oParse As New clsMarsParser
        Dim sExt As String
        Dim UseSavedData As Boolean
        Dim oRpt As ReportServer.ReportingService 'rsClients.rsClient
        Dim sCluster As String
        Dim oScheduler As New clsMarsScheduler
        Dim nClose As Integer = 0
        Dim oUI As clsMarsUI = New clsMarsUI
        Dim sUrl As String
        Dim oLogins() As ReportServer.DataSourceCredentials
        Dim errInfo As New clsErrorInfo
        Dim schedStart As Date = Now

10:     sCluster = oUI.ReadRegistry("SystemEnvironment", "")

20:     SQL = "SELECT * FROM ReportAttr r INNER JOIN PackagedReportAttr p ON " & _
        "r.ReportID = p.ReportID" & _
        " WHERE p.ReportID = " & nReportID & " AND " & _
        "p.PackID =" & nPackID

30:     oRs = clsMarsData.GetData(SQL)

40:     Try
50:         If oRs Is Nothing Then
                sExportVal = String.Empty
                Return errInfo
            End If

60:         If oRs.EOF = False Then
70:             sReportPath = IsNull(oRs("CachePath").Value)
80:             sReportName = IsNull(oRs("ReportName").Value)
90:             If sFormat.Length = 0 Then sFormat = IsNull(oRs("OutputFormat").Value)

                'parse the format for dd data

151:            sFormat = clsMarsParser.Parser.ParseString(sFormat, , , , , , m_ParametersTable)

                If sFormat <> "Printer Format" And sFormat <> "Fax Format" Then
                    If Me.validateFormat(sFormat) = False Then
                        Throw New Exception("The format retrieved from the data driver ('" & sFormat & "') could not be understood by SQL-RD")
                    End If
                End If

100:            sFormula = oParse.ParseString(IsNull(oRs("selectionformula").Value), , , , , , m_ParametersTable)
101:            sUrl = oRs("databasepath").Value

                If IsNull(oRs("adjuststamp").Value, 0) <> 0 Then
                    adjustStamp = oRs("adjuststamp").Value
                End If

                'gScheduleName = CType(oRs("reporttitle").Value, String).Split(":")(1)

110:            If IsNull(oRs("CustomName").Value) <> "" Then
120:                sFileName = oParse.ParseString(oRs("CustomName").Value, , , , , , m_ParametersTable)
130:                sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                    Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
140:            Else
150:                sFileName = CType(oRs("ReportTitle").Value, String).Split(":")(1)
160:                sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                    Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
170:            End If

180:            sExt = IsNull(oRs("customext").Value)

181:            'nRetry = IsNull(oRs("retry").Value, 0)

190:            sExt = oParse.ParseString(sExt, , , , , , m_ParametersTable)


210:            oRpt = New ReportServer.ReportingService
220:            oRpt.Timeout = -1 'no timeout 3600000
                oRpt.Url = sUrl

                'set up reportviewer for 2005 servers
211:            If m_rv IsNot Nothing Then
                    m_rv = Nothing
                End If

213:            m_rv = New Microsoft.Reporting.WinForms.ReportViewer
214:            m_rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

215:            Dim serverUrl As String = ""

216:            For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
217:                serverUrl &= sUrl.Split("/")(I) & "/"
                Next

218:            m_rv.ServerReport.ReportServerUrl = New Uri(serverUrl)

                If Me.SetServerLogin(oRpt, nReportID) = False Then
                    sExportVal = "{error}"
230:                Return errInfo
240:            End If

241:            oLogins = Me.CreateDatasources(nReportID)

242:            greportItem = oRpt

470:            If sPackageDateStamp = "" Then
480:                Try
490:                    If Convert.ToBoolean(Convert.ToInt32(oRs("AppendDateTime").Value)) = True Then
500:                        sDateStamp = IsNull(oRs("DateTimeFormat").Value)
510:                        sDateStamp = (Date.Now).AddDays(adjustStamp).ToString(sDateStamp)
520:                    Else
530:                        sDateStamp = ""
540:                    End If
550:                Catch
560:                    sDateStamp = ""
570:                End Try
580:            Else
590:                sDateStamp = Now.AddDays(adjustStamp).ToString(sPackageDateStamp)
600:            End If

610:            If ToPrinter = True Then sFormat = "Acrobat Format (*.pdf)"

620:            If ToFax = True Then sFormat = "TIFF Fax (*.pdf)"

630:            oUI.BusyProgress(50, "Exporting report..")

640:            sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, sTempPath & sFileName, _
                                        sFileName, sReportPath, oLogins, sPackageName, , sExt, , MergePDF, nPackID, rptTimeout)

641:            If sExport = "{skip}" Then
                    GoTo Skippinder
                End If
1890:       End If
            'set the excel file password
1910:       If MergeXL = False Then
1920:           Try
1930:               If sFormat.ToLower.IndexOf("excel") > -1 Then
1940:                   Dim oRsExcel As ADODB.Recordset


1950:                   oRsExcel = clsMarsData.GetData("SELECT ProtectExcel,ExcelPassword FROM ReportOptions " & _
                       "WHERE ReportID =" & nReportID)

1960:                   If oRsExcel.EOF = False Then
1970:                       If IsNull(oRsExcel("protectexcel").Value, "0") = "1" Then
                                Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)
1980:                           oXL.SetWorkBookPassword(sExport, _DecryptDBValue(oRsExcel("excelpassword").Value))
                                oXL.Dispose()
1990:                       End If
2000:                   End If

2010:                   oRsExcel.Close()
2030:               End If
                Catch : End Try
2040:       End If

Skippinder:
            SaveTextToFile(Date.Now & ", " & nPackID & ", Setting history for reportid: " & nReportID, sAppPath & "package.debug", , True)

2060:       If sExport.Length > 0 And sExport <> "{error}" Then
2070:           oScheduler.SetScheduleHistoryDetail(True, DDKeyValue, Me.m_HistoryID, schedStart, nReportID)
2080:       Else
                oScheduler.SetScheduleHistoryDetail(False, DDKeyValue & gErrorDesc & " (" & gErrorNumber & ")", Me.m_HistoryID, schedStart, nReportID)
                errInfo = New clsErrorInfo(gErrorDesc, gErrorNumber, gErrorLine, gErrorSource, gErrorSuggest)
3000:       End If

3423:       Me.m_ParametersTable = Nothing

2050:       oRpt = Nothing

        Catch ex As Exception
            errInfo.ErrorDesc = "Package Name: " & sPackageName & ": " & sReportName & " - " & ex.Message
            errInfo.ErrorNumber = Err.Number
            errInfo.ErrorSource = "clsMarsReport._RunSingle"
            errInfo.ErrorLine = Erl()

            If ex.Message.ToLower.IndexOf("invalid tlv record") > -1 Then
                errInfo.ErrorSuggest = "The error is caused by one of the following: " & vbCrLf & _
                "1. The report definition is corrupt in the rpt file" & vbCrLf & _
                "2. The report was written with a Crystal version below version 8.5" & vbCrLf & _
                "3. You are trying to open a report created in a version above 8.5 using Crystal 8.5 runtimes"
            ElseIf ex.Message.ToLower.IndexOf("file not found") > -1 Then
                errInfo.ErrorSuggest = "SQL-RD cannot find the cached report file. Please refresh the schedule and try again."
            End If

            sExport = "{error}"
            oRpt = Nothing

            oScheduler.SetScheduleHistoryDetail(False, errInfo.ErrorDesc & " (" & errInfo.ErrorNumber & ")", Me.m_HistoryID, schedStart, nReportID)
        Finally
            Me.m_ParametersTable = Nothing

            oRpt = Nothing
        End Try

        sExportVal = sExport
        Return errInfo

    End Function

    'this overloaded does not accept any arguments and instead get them from the
    'gxxxx variables
    Public Function RunDynamicSchedule(ByVal nReportID As Integer, _
           Optional ByVal ShowFinish As Boolean = False) As Boolean
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sCon As String
10:     Dim oRsKey As ADODB.Recordset = New ADODB.Recordset
20:     Dim oCon As ADODB.Connection = New ADODB.Connection
        Dim Success As Boolean
        Dim I As Integer = 1
        Dim nCount As Integer
        Dim sReportName As String = ""
        Dim PersistResult As Boolean = True
        Dim dynamicTasks As Integer
        Dim runTasks As Boolean
30:     Dim oTasks As clsMarsTask = New clsMarsTask
        Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(nReportID)
40:     Dim oUI As clsMarsUI = New clsMarsUI
        Dim keyColumn As String = ""
        Dim staticDest As Boolean = False
        Dim cacheFilename As String
        Dim autoResume As Boolean = False
        Dim expireCache As Integer = 0

        g_noshrinkage = True

        ScheduleStart = Now

45:     sDynamicError = ""
        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nReportID, clsMarsScheduler.enScheduleType.REPORT)
        gScheduleFolderName = clsMarsUI.getObjectParentName(nReportID, clsMarsScheduler.enScheduleType.REPORT)

50:     If frmMain.m_UserCancel = True Then
60:         Me.CancelExecution("RunDynamicSchedule")
70:         Return False
80:     End If

90:     Try
100:        oRs = clsMarsData.GetData("SELECT ReportTitle, DynamicTasks, StaticDest FROM ReportAttr WHERE ReportID =" & nReportID)

110:        If oRs.EOF = False Then
120:            sReportName = oRs.Fields(0).Value
130:            dynamicTasks = IsNull(oRs.Fields(1).Value, 0)
140:            staticDest = IsNull(oRs("staticdest").Value, 0)
150:        End If

160:        oRs.Close()
170:    Catch : End Try

180:    If dynamicTasks = 0 Then
190:        runTasks = True
200:    Else
210:        runTasks = False
220:    End If

230:    Try
240:        SQL = "SELECT * FROM DynamicAttr WHERE ReportID =" & nReportID

250:        oRs = clsMarsData.GetData(SQL)

260:        If oRs Is Nothing Then Return False

270:        If oRs.EOF = False Then

280:            If frmMain.m_UserCancel = True Then
290:                Me.CancelExecution("RunDynamicSchedule")
300:                Return False
310:            End If

                If IO.Directory.Exists(clsMarsDynamic.m_diskCache) = False Then
                    IO.Directory.CreateDirectory(clsMarsDynamic.m_diskCache)
                End If

320:            sKeyParameter = oRs("keyparameter").Value

330:            SQL = oRs("query").Value
340:            sCon = oRs("constring").Value

                Try
                    autoResume = IsNull(oRs("autoresume").Value, 0)
                    expireCache = IsNull(oRs("expirecacheafter").Value, 0)
                Catch : End Try

350:            logDurationMaster(nReportID)

360:            If SQL = "_" Or sCon = "_" Or SQL.Length = 0 Or sCon.Length = 0 Then
370:                sKeyValue = oRs("keycolumn").Value

380:                gKeyValue = sKeyValue

390:                clsMarsData.WriteData("UPDATE ThreadManager SET DataCount = 1 WHERE ReportID =" & nReportID)

400:                clsMarsDynamic.m_DynamicDataCache = New DataTable
410:                clsMarsDynamic.m_DynamicDataCache.Columns.Add("_Column1_")

                    Dim row As DataRow = clsMarsDynamic.m_DynamicDataCache.Rows.Add

420:                row("_Column1_") = gKeyValue

430:                clsMarsDynamic.m_CurrentKeyColumn = "_Column1_"


440:                If clsMarsDynamic.oDynamic.createDynamicLinkset(nReportID, clsMarsScheduler.enScheduleType.REPORT) = False Then
450:                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, , True, True)
                    End If

460:                If runTasks = False Then

470:                    If frmMain.m_UserCancel = True Then
480:                        Me.CancelExecution("RunDynamicSchedule")
490:                        Return False
500:                    End If

510:                    oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.BEFORE)

520:                    Success = RunSingleSchedule(nReportID, False, , , , , , runTasks, True)

530:                    If Success = True Then
540:                        oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.AFTERDEL)
550:                    End If
560:                Else
570:                    If frmMain.m_UserCancel = True Then
580:                        Me.CancelExecution("RunDynamicSchedule")
590:                        Return False
600:                    End If

610:                    Success = RunSingleSchedule(nReportID, False, , , , , , runTasks, True)
620:                End If

                    If Success = False Then
                        sDynamicError &= "Key Value: [static]" & vbCrLf & _
                                         "Error Description: " & gErrorDesc & vbCrLf & _
                                         "Error Number: " & gErrorNumber & vbCrLf & _
                                         "Error Source: " & gErrorSource & vbCrLf & _
                                         "Line Number: " & gErrorLine & vbCrLf & _
                                         "+++++++++++++++++++++++++++++++++++++++++++++++++++++" & vbCrLf
                    End If
630:            Else
                    Dim sDsn, sUser, sPassword As String

640:                sDsn = sCon.Split("|")(0)
650:                sUser = sCon.Split("|")(1)
660:                sPassword = sCon.Split("|")(2)

670:                keyColumn = IsNull(oRs("keycolumn").Value)

680:                keyColumn = keyColumn.Split(",")(0)

690:                clsMarsDynamic.m_CurrentKeyColumn = keyColumn

                    cacheFilename = sReportName & ".dynr"

                    For Each s As String In IO.Path.GetInvalidFileNameChars
                        cacheFilename = cacheFilename.Replace(s, "")
                    Next

700:                If clsMarsDynamic.oDynamic.createDynamicDataset(SQL, sDsn, sUser, sPassword, cacheFilename, autoResume, expireCache) = False Then
710:                    Throw New Exception(Me._CreateErrorString)
                    End If

720:                If clsMarsDynamic.oDynamic.createDynamicLinkset(nReportID, clsMarsScheduler.enScheduleType.REPORT) = False Then
730:                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, , True, True)
                    End If

740:                If keyColumn.Contains(".") Then
750:                    keyColumn = keyColumn.Split(".")(keyColumn.Split(".").GetUpperBound(0))
                    End If

                    If keyColumn.StartsWith("[") And keyColumn.EndsWith("]") Then
                        keyColumn = keyColumn.Remove(0, 1)
                        keyColumn = keyColumn.Remove(keyColumn.Length - 1, 1)
                        keyColumn = keyColumn.Replace(" ", "_")
                    End If

760:                clsMarsDynamic.m_CurrentKeyColumn = keyColumn

770:                If frmMain.m_UserCancel = True Then
780:                    Me.CancelExecution("RunDynamicSchedule")
790:                    Return False
800:                End If

810:                nCount = clsMarsDynamic.m_DynamicDataCache.Rows.Count

820:                clsMarsData.WriteData("UPDATE ThreadManager SET DataCount = " & nCount & " WHERE ReportID =" & nReportID)

830:                SaveTextToFile(Now & ": Starting dynamic schedule '" & sReportName & "'", sAppPath & "dynamicrun.debug", , True)

                    SaveTextToFile(Now & ": Key Column: '" & keyColumn & "'", sAppPath & "dynamicrun.debug", , True)

840:                If runTasks = False Then
850:                    oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.BEFORE)
860:                End If

870:                If clsMarsDynamic.m_DynamicDataCache.Rows.Count > 0 Then
                        'clone the data cache and then save it to disk. On each iteration, delete the values from the clone

                        Dim clone As DataTable = clsMarsDynamic.m_DynamicDataCache.Copy

                        If autoResume = True Then
                            clsMarsDynamic.serializeCache(cacheFilename, clone)
                        End If

880:                    For Each row As DataRow In clsMarsDynamic.m_DynamicDataCache.Rows

890:                        If frmMain.m_UserCancel = True Then
900:                            Me.CancelExecution("RunDynamicSchedule")
910:                            Return False
920:                        End If

930:                        sKeyValue = row(keyColumn)

940:                        oUI.BusyProgress(25, "Report " & I & " of " & nCount & " (" & sKeyValue & ")")

950:                        System.Threading.Thread.Sleep(1000)

960:                        gKeyValue = sKeyValue

970:                        SaveTextToFile(Now & ": Exporting report " & I & " of " & nCount & "(" & sKeyValue & ")", sAppPath & "dynamicrun.debug", , True)

980:                        Success = RunSingleSchedule(nReportID, False, , , , , , runTasks, True)

990:                        SaveTextToFile(Now & ": Success = " & Success.ToString, sAppPath & "dynamicrun.debug", , True)

1000:                       If Success = False Then
1010:                           PersistResult = False
1020:                           sDynamicError &= "Key Value: " & sKeyValue & vbCrLf & _
                                     "Error Description: " & gErrorDesc & vbCrLf & _
                                     "Error Number: " & gErrorNumber & vbCrLf & _
                                     "Error Source: " & gErrorSource & vbCrLf & _
                                     "Line Number: " & gErrorLine & vbCrLf & _
                                     "+++++++++++++++++++++++++++++++++++++++++++++++++++++" & vbCrLf
                            Else
                                If autoResume = True Then
                                    Try
                                        Dim crow() As DataRow = clone.Select(clsMarsDynamic.m_CurrentKeyColumn & " = '" & SQLPrepare(sKeyValue) & "'")

                                        For x As Integer = 0 To crow.GetUpperBound(0)
                                            clone.Rows.Remove(crow(x))

                                            x += 1
                                        Next

                                        clsMarsDynamic.serializeCache(cacheFilename, clone)

                                    Catch : End Try
                                End If
1030:                       End If

1040:                       SaveTextToFile(Now & ": Moving to next report...", sAppPath & "dynamicrun.debug", , True)

1050:                       If Me.DynamicCancelled = True Then
1060:                           Me.DynamicCancelled = False
1070:                           Return False
                            End If

1080:                       If frmMain.m_UserCancel = True Then
1090:                           Me.CancelExecution("RunDynamicSchedule")
1100:                           Return False
1110:                       End If

1120:                       I += 1

1130:                       If I = nCount Then
1140:                           SaveTextToFile(Now & ": Last report reached...", sAppPath & "dynamicrun.debug", , True)
1150:                       End If

1160:                       Me.m_ParametersTable = Nothing
1170:                   Next

                        If autoResume = True Then
                            Try
                                If clone.Rows.Count = 0 Then
                                    IO.File.Delete(clsMarsDynamic.m_diskCache & cacheFilename)
                                End If
                            Catch : End Try
                        End If

                        clone.Dispose()
                        clone = Nothing

1180:                   If runTasks = False Then
1190:                       If Success = True Then
1200:                           oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.AFTERDEL)
1210:                       End If
1220:                   End If

1230:                   SaveTextToFile(Now & ": Completed executing dynamic schedule.", sAppPath & "dynamicrun.debug", , True)
1240:               Else
1250:                   Success = False
1260:                   gErrorDesc = sReportName & ": The query for the key parameter did not return any values"
1270:                   gErrorNumber = -101
1280:                   gErrorSource = "clsMarsReport._RunDynamicSchedule"
1290:               End If

1300:               Try
1310:                   clsMarsDynamic.m_DynamicDataCache.Dispose()
1320:                   clsMarsDynamic.m_DynamicDataCache = Nothing
1330:                   clsMarsDynamic.m_DynamicLinkCache.Dispose()
1340:                   clsMarsDynamic.m_DynamicLinkCache = Nothing
1350:                   clsMarsDynamic.m_CurrentKeyColumn = ""
1360:                   clsMarsDynamic.m_currentValueColumn = ""
                    Catch : End Try
1370:           End If
1380:       End If

1390:       SaveTextToFile(Now & ": Exiting process.", sAppPath & "dynamicrun.debug", , True)

1400:       Return PersistResult
1410:   Catch ex As Exception
1420:       gErrorDesc = ex.Message
1430:       gErrorNumber = Err.Number
1440:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
1450:       gErrorLine = Erl()

1460:       _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
1470:   Finally
            g_noshrinkage = False
            'gScheduleName = String.Empty
            gScheduleOwner = ""
1480:       sKeyParameter = ""
        End Try
    End Function
    Public Function RunDynamicPackageSchedule(ByVal nPackID As Integer, Optional ByVal ShowFinish As Boolean = False, Optional ByVal PrintDate As String = "") As Boolean
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sCon As String
        Dim Success As Boolean
        Dim I As Integer = 1
        Dim nCount As Integer
        Dim sName As String = ""
        Dim PersistResult As Boolean = True
        Dim dynamicTasks As Integer
        Dim runTasks As Boolean
        Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(, nPackID)
10:     Dim oTasks As clsMarsTask = New clsMarsTask
20:     Dim oUI As clsMarsUI = New clsMarsUI
        Dim staticDest As Boolean = False
        Dim keyColumn As String = ""
        Dim cacheFilename As String
        Dim autoResume As Boolean = False
        Dim expireCache As Integer = 0

        g_noshrinkage = True

        gScheduleFolderName = clsMarsUI.getObjectParentName(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)
        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)

30:     Try
40:         oRs = clsMarsData.GetData("SELECT PackageName,DynamicTasks,staticDest FROM PAckageAttr WHERE PackID =" & nPackID)

50:         If oRs.EOF = False Then
60:             sName = oRs.Fields(0).Value
70:             dynamicTasks = IsNull(oRs.Fields(1).Value, 0)
80:             staticDest = IsNull(oRs("staticdest").Value, 0)
            End If

90:         oRs.Close()
        Catch : End Try

        If IO.Directory.Exists(clsMarsDynamic.m_diskCache) = False Then
            IO.Directory.CreateDirectory(clsMarsDynamic.m_diskCache)
        End If

100:    If dynamicTasks = 0 Then
110:        runTasks = True
120:    Else
130:        runTasks = False
        End If

140:    If frmMain.m_UserCancel = True Then
150:        Me.CancelExecution("RunDynamicPackageSchedule")
160:        Return False
        End If

170:    Try
180:        SQL = "SELECT * FROM DynamicAttr WHERE PackID =" & nPackID

190:        oRs = clsMarsData.GetData(SQL)

200:        If oRs Is Nothing Then Return False

210:        If oRs.EOF = False Then
220:            If frmMain.m_UserCancel = True Then
230:                Me.CancelExecution("RunDynamicPackageSchedule")
240:                Return False
                End If

250:            sKeyParameter = oRs("keyparameter").Value

260:            SQL = oRs("query").Value
270:            sCon = oRs("constring").Value

                Try
                    autoResume = IsNull(oRs("autoresume").Value, 0)
                    expireCache = IsNull(oRs("expirecacheafter").Value, 0)
                Catch : End Try

280:            If SQL = "_" Or sCon = "_" Or SQL.Length = 0 Or sCon.Length = 0 Then
290:                sKeyValue = oRs("keycolumn").Value

300:                gKeyValue = sKeyValue

310:                clsMarsData.WriteData("UPDATE ThreadManager SET DataCount = 1 WHERE PackID =" & nPackID)

320:                If frmMain.m_UserCancel = True Then
330:                    Me.CancelExecution("RunDynamicPackageSchedule")
340:                    Return False
                    End If

350:                clsMarsDynamic.m_DynamicDataCache = New DataTable
360:                clsMarsDynamic.m_DynamicDataCache.Columns.Add("_Column1_")

                    Dim row As DataRow = clsMarsDynamic.m_DynamicDataCache.Rows.Add

370:                row("_Column1_") = gKeyValue

380:                clsMarsDynamic.m_CurrentKeyColumn = "_Column1_"


390:                If clsMarsDynamic.oDynamic.createDynamicLinkset(nPackID, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
400:                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, , True, True)
                    End If

410:                If runTasks = False Then
420:                    If frmMain.m_UserCancel = True Then
430:                        Me.CancelExecution("RunDynamicPackageSchedule")
440:                        Return False
                        End If

                        Try
450:                        oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.BEFORE)
                        Catch ex As Exception
                            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                        End Try

460:                    Success = RunPackageSchedule(nPackID, False, , , , , True, runTasks, PrintDate)

470:                    If Success = True Then
                            Try
480:                            oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.AFTERDEL)
                            Catch ex As Exception
                                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                            End Try
                        End If
490:                Else
500:                    If frmMain.m_UserCancel = True Then
510:                        Me.CancelExecution("RunDynamicPackageSchedule")
520:                        Return False
                        End If

530:                    Success = RunPackageSchedule(nPackID, False, , , , , True, runTasks, PrintDate)
                    End If

540:            Else
550:                If frmMain.m_UserCancel = True Then
560:                    Me.CancelExecution("RunDynamicPackageSchedule")
570:                    Return False
                    End If

                    Dim sDsn, sUser, sPassword As String

580:                sDsn = sCon.Split("|")(0)
590:                sUser = sCon.Split("|")(1)
600:                sPassword = sCon.Split("|")(2)

610:                keyColumn = IsNull(oRs("keycolumn").Value)

620:                keyColumn = keyColumn.Split(",")(0)

630:                clsMarsDynamic.m_CurrentKeyColumn = keyColumn

                    cacheFilename = sName & ".dynp"

                    For Each s As String In IO.Path.GetInvalidFileNameChars
                        cacheFilename = cacheFilename.Replace(s, "")
                    Next

640:                If clsMarsDynamic.oDynamic.createDynamicDataset(SQL, sDsn, sUser, sPassword, cacheFilename, autoResume, expireCache) = False Then
650:                    Throw New Exception(Me._CreateErrorString)
                    End If

660:                If clsMarsDynamic.oDynamic.createDynamicLinkset(nPackID, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
670:                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, , True, True)
                    End If

680:                If keyColumn.Contains(".") Then
690:                    keyColumn = keyColumn.Split(".")(1)
                    End If

                    If keyColumn.StartsWith("[") And keyColumn.EndsWith("]") Then
                        keyColumn = keyColumn.Remove(0, 1)
                        keyColumn = keyColumn.Remove(keyColumn.Length - 1, 1)
                        keyColumn = keyColumn.Replace(" ", "_")
                    End If

700:                clsMarsDynamic.m_CurrentKeyColumn = keyColumn

710:                nCount = clsMarsDynamic.m_DynamicDataCache.Rows.Count

720:                clsMarsData.WriteData("UPDATE ThreadManager SET DataCount = " & nCount & " WHERE PackID =" & nPackID)

730:                If runTasks = False Then
740:                    If frmMain.m_UserCancel = True Then
750:                        Me.CancelExecution("RunDynamicPackageSchedule")
760:                        Return False
                        End If

                        Try
770:                        oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.BEFORE)
                        Catch ex As Exception
                            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                        End Try
                    End If

780:                If clsMarsDynamic.m_DynamicDataCache.Rows.Count > 0 Then

                        Dim clone As DataTable = clsMarsDynamic.m_DynamicDataCache.Copy

                        If autoResume = True Then
                            clsMarsDynamic.serializeCache(cacheFilename, clone)
                        End If

790:                    For Each row As DataRow In clsMarsDynamic.m_DynamicDataCache.Rows

800:                        If frmMain.m_UserCancel = True Then
810:                            Me.CancelExecution("RunDynamicPackageSchedule")
820:                            Return False
                            End If

830:                        oUI.BusyProgress(25, "Package " & I & " of " & nCount)

840:                        System.Threading.Thread.Sleep(500)

850:                        sKeyValue = row(keyColumn)

860:                        gKeyValue = sKeyValue

870:                        Success = RunPackageSchedule(nPackID, False, , , , , True, runTasks, PrintDate)

880:                        If Success = False Then
890:                            PersistResult = False
                                sDynamicError &= "Key Value: " & sKeyValue & vbCrLf & _
                                "Error Description: " & gErrorDesc & vbCrLf & _
                                "Error Number: " & gErrorNumber & vbCrLf & _
                                "Error Source: " & gErrorSource & vbCrLf & _
                                "Line Number: " & gErrorLine & vbCrLf & _
                                "+++++++++++++++++++++++++++++++++++++++++++++++++++++" & vbCrLf
                            Else
                                If autoResume = True Then
                                    Try
                                        Dim crow() As DataRow = clone.Select(clsMarsDynamic.m_CurrentKeyColumn & " = '" & SQLPrepare(sKeyValue) & "'")

                                        For x As Integer = 0 To crow.GetUpperBound(0)
                                            clone.Rows.Remove(crow(x))

                                            x += 1
                                        Next

                                        clsMarsDynamic.serializeCache(cacheFilename, clone)
                                    Catch : End Try
                                End If
                            End If

900:                        If Me.DynamicCancelled = True Then
910:                            Me.DynamicCancelled = False
920:                            Return False
                            End If

930:                        If frmMain.m_UserCancel = True Then
940:                            Me.CancelExecution("RunDynamicPackageSchedule")
950:                            Return False
                            End If

960:                        I += 1
970:                    Next

980:                    If frmMain.m_UserCancel = True Then
990:                        Me.CancelExecution("RunDynamicPackageSchedule")
1000:                       Return False
                        End If

                        If autoResume = True Then
                            Try
                                If clone.Rows.Count = 0 Then
                                    IO.File.Delete(clsMarsDynamic.m_diskCache & cacheFilename)
                                End If
                            Catch : End Try
                        End If

                        clone.Dispose()
                        clone = Nothing

1010:                   If Success = True Then
                            Try
1020:                           oTasks.TaskPreProcessor(scheduleID, clsMarsTask.enRunTime.AFTERDEL)
                            Catch ex As Exception
                                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                            End Try
                        End If

1030:               Else
1040:                   Success = False
                        gErrorDesc = sName & ": The query for the key parameter did not return any values"
1050:                   gErrorNumber = -101
1060:                   gErrorSource = "clsMarsReport._RunDynamicPackageSchedule"
                    End If

1070:               Try
1080:                   clsMarsDynamic.m_DynamicDataCache.Dispose()
1090:                   clsMarsDynamic.m_DynamicDataCache = Nothing
1100:                   clsMarsDynamic.m_DynamicLinkCache.Dispose()
1110:                   clsMarsDynamic.m_DynamicLinkCache = Nothing
1120:                   clsMarsDynamic.m_CurrentKeyColumn = ""
1130:                   clsMarsDynamic.m_currentValueColumn = ""
                    Catch : End Try
                End If
            End If


1140:       Return PersistResult
1150:   Catch ex As Exception
1160:       gErrorDesc = ex.Message
1170:       gErrorNumber = Err.Number
1180:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
1190:       gErrorLine = Erl()
1200:   Finally
            g_noshrinkage = False
            gScheduleOwner = ""
1210:       gScheduleName = String.Empty
        End Try
    End Function
    Public Sub SetTableLogin(ByVal oRpt As Object, ByVal nReportID As Integer)

    End Sub
    Public Sub SetReportLogin(ByRef oRpt As Object, ByVal sUserID As String, ByVal sPassword As String, _
    ByVal sxServer As String, ByVal sDatabase As String, ByVal sDBType As String)
        Try
            For I As Integer = 1 To oRpt.Database.Tables.Count
#If CRYSTAL_VER < 9 Then
                oRpt.Database.Tables(I).SetLogOnInfo(sxServer, sDatabase, sUserID, sPassword)
#Else

                With oRpt.Database.Tables(I).ConnectionProperties

                    '.DeleteAll()

                    '.Add("Database Type", "ODBC (RDO)")
                    '.Add("DSN", sxServer)
                    '.Add("Database", sDatabase)
                    '.Add("User ID", sUserID)
                    '.Add("Password", sPassword)

                    Try
                        .Item("user id").Value = sUserID
                    Catch ex As Exception
                        If Err.Number = 9 Then .Add("User ID", sUserID)
                    End Try

                    Try
                        .Item("password").Value = sPassword
                    Catch ex As Exception
                        If Err.Number = 9 Then .Add("password", sPassword)
                    End Try

                    Select Case sDBType.ToLower
                        Case "odbc (rdo)"
                            If sxServer.Length > 0 Then
                                Try
                                    .Item("DSN").Value = sxServer
                                Catch ex As Exception
                                    If Err.Number = 9 Then .Add("DSN", sxServer)
                                End Try
                            End If

                            If sDatabase.Length > 0 Then
                                Try
                                    .Item("Database").Value = sDatabase
                                Catch ex As Exception
                                    If Err.Number = 9 Then .Add("Database", sDatabase)
                                End Try
                            End If
                        Case "ole db (ado)"
                            If sxServer.Length > 0 Then
                                Try
                                    .Item("Data Source").Value = sxServer
                                Catch ex As Exception
                                    If Err.Number = 9 Then .Add("Data Source", sxServer)
                                End Try
                            End If

                            If sDatabase.Length > 0 Then
                                Try
                                    .Item("Initial Catalog").Value = sDatabase
                                Catch ex As Exception
                                    If Err.Number = 9 Then .Add("Initial Catalog", sDatabase)
                                End Try
                            End If
                        Case Else
                            Try
                                If sxServer.Length > 0 Then .Item("data source").Value = sxServer
                                If sDatabase.Length > 0 Then .Item("initial catalog").Value = sDatabase
                            Catch : End Try
                    End Select

                End With
#End If
            Next
        Catch : End Try

    End Sub
    Public Function RunSinglewithDefault(ByVal nReportID As Integer, ByVal nDestinationID As Integer) As Boolean
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sExport As String
        Dim sReportName As String
        Dim sFormat As String
        Dim sFileName As String
        Dim sDateStamp As String
        Dim sFile As String
        Dim sFormula As String
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Int32 = oSchedule.GetScheduleID(nReportID)
        Dim IsDynamic As Boolean
        Dim oParse As New clsMarsParser
        Dim Ok As Boolean
        Dim sExt As String
        Dim HTMLSplit As Boolean
        Dim sReportPath As String
        Dim UseSavedData As Boolean
        Dim ReportTitle As String
        Dim oRpt As Object
        Dim EmbedPath As String = ""
        Dim IncludeAttach As Boolean
        Dim adjustStamp As Double
        Dim oUI As clsMarsUI = New clsMarsUI
        Dim sUrl As String = ""
        Dim oLogins() As ReportServer.DataSourceCredentials

        gCurrentTempOutputFile = ""
        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nReportID, clsMarsScheduler.enScheduleType.REPORT)
        ScheduleStart = Now
        'run custom tasks
        Try
            oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)
            m_scheduleID = nScheduleID

            oUI.BusyProgress(25, "Collecting data...")

            SQL = "SELECT * FROM ReportAttr  WHERE ReportID = " & nReportID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Err.Raise(-102, , _
            "Failed to obtain valid recordset (ReportAttr)")

            If oRs.EOF = True Then Err.Raise(-103, , _
            "The recordset did not return any records (ReportAttr)")


            'get values from recordset before looping
            If oRs.EOF = False Then
                sReportName = oRs("ReportName").Value
                sUrl = oRs("databasepath").Value
                gScheduleName = sReportName

                sReportPath = IsNull(CType(oRs.Fields("cachepath").Value, String))

                If Not oRpt Is Nothing Then
                    oRpt = Nothing
                End If

                oRpt = New ReportServer.ReportingService

                greportItem = oRpt

                If frmMain.m_UserCancel = True Then
                    Me.CancelExecution("RunSingleSchedule")
                    Return False
                End If

                oRpt.Timeout = -1 'no timeout 3600000

                oRpt.Url = sUrl

                If m_rv IsNot Nothing Then
                    m_rv = Nothing
                End If

                m_rv = New Microsoft.Reporting.WinForms.ReportViewer
                m_rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

                Dim serverUrl As String = ""

                For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
                    serverUrl &= sUrl.Split("/")(I) & "/"
                Next

                m_rv.ServerReport.ReportServerUrl = New Uri(serverUrl)

                If Me.SetServerLogin(oRpt, nReportID) = False Then
                    Return False
                End If

                oLogins = Me.CreateDatasources(nReportID)

                ReportTitle = oRs("reporttitle").Value

            End If

            oRs.Close()

            'open the destination
            oRs = clsMarsData.GetData("SELECT * FROM DestinationAttr WHERE DestinationID =" & nDestinationID)

            If oRs.EOF = False Then
                nDestinationID = oRs("destinationid").Value
                adjustStamp = IsNull(oRs("adjuststamp").Value, 0)
                'lets sort out printing right here

                sFormat = oRs("outputformat").Value

                If IsNull(oRs("CustomName").Value) <> "" Then
                    sFileName = oParse.ParseString(oRs("CustomName").Value, , , , , , m_ParametersTable)
                    sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                    Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                Else
                    sFileName = ReportTitle
                    sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                    Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                End If

                If Convert.ToBoolean(oRs("AppendDateTime").Value) = True Then
                    sDateStamp = IsNull(oRs("DateTimeFormat").Value)

                    sDateStamp = Date.Now.AddDays(adjustStamp).ToString(sDateStamp)
                Else
                    sDateStamp = ""
                End If

                HTMLSplit = Convert.ToBoolean(oRs("htmlsplit").Value)

                oUI.BusyProgress(50, "Exporting report...")

                If oRs("destinationtype").Value = "Printer" Then
                    sFormat = "Acrobat Format (*.pdf)"
                    IncludeAttach = True
                End If

                Dim sSendTo As String
                Dim sSubject, senderName, senderAddress As String
                Dim sCc As String
                Dim sBcc As String
                Dim sExtras As String
                Dim sMsg As String
                Dim sPath As String
                Dim oDynamicData As New clsMarsDynamic
                Dim Embed As Boolean
                Dim MailFormat As String
                Dim ScheduleType As String = "Single"
                Dim SMTPServer As String = ""
                Dim ReadReceipt As Boolean = False

                sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, _
                sTempPath & sFileName, sFileName, sReportPath, _
                oLogins, "", oRs("destinationid").Value, sExt, IsDynamic, False, 0)

                If sExport.ToLower = "{error}" Then Return False

                If sExport.ToLower = "{skip}" Then Return True

                Dim oSnap As New clsMarsSnapshots

                oSnap.CreateSnapshot(nReportID, sExport)

                gExportedFileName = ExtractFileName(sExport)

                sExtras = oParse.ParseString(IsNull(oRs("extras").Value), , , , , , m_ParametersTable)

                If HTMLPath.Length > 0 Then HTMLPath = GetDirectory(HTMLPath)

                Dim oPGP As New clsMarsPGP
                Dim oPGPResult() As Object

                oPGPResult = oPGP.EncryptFile(sExport, nDestinationID)

                If oPGPResult(0) = False Then
                    Return False
                End If

                sExport &= oPGPResult(1)

                oTask.TaskPreProcessor(nScheduleID, _
                    clsMarsTask.enRunTime.AFTERPROD)

                Try
                    If oRs("compress").Value = 1 Then
                        oUI.BusyProgress(60, "Zipping report...")

                        sExport = ZipFiles(ScheduleType, m_OutputFolder, sExport)

                        HTMLPath = ""
                    End If
                Catch : End Try

                Select Case CType(oRs("DestinationType").Value, String).ToLower
                    Case "printer"
                        oUI.BusyProgress(75, "Printing reports...")

                        Dim sFilePrint(0) As String
                        sFilePrint(0) = sExport

                        Ok = Me._PrintServer(sFilePrint, nDestinationID)

                        oUI.BusyProgress(75, "Printing reports...")
                    Case "email"

                        sSendTo = ResolveEmailAddress(oRs("sendto").Value)
                        sCc = ResolveEmailAddress(oRs("cc").Value)
                        sBcc = ResolveEmailAddress(oRs("bcc").Value)

                        sSendTo = oParse.ParseString(sSendTo, , , , , , m_ParametersTable)
                        sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
                        sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)

                        senderName = oParse.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
                        senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)
                        MailFormat = oRs("mailformat").Value
                        SMTPServer = IsNull(oRs("smtpserver").Value, "Default")

                        sMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)
                        sSubject = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)

                        Try
                            ReadReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
                        Catch ex As Exception
                            ReadReceipt = False
                        End Try

                        oUI.BusyProgress(75, "Emailing report...")

                        Try
                            Embed = Convert.ToBoolean(oRs("embed").Value)
                        Catch ex As Exception
                            Embed = False
                        End Try

                        Try
                            IncludeAttach = Me.DoIncludeAttach(nDestinationID)
                        Catch ex As Exception
                            IncludeAttach = True
                        End Try

                        Try
                            If Embed = True Then

                                If MailType = MarsGlobal.gMailType.MAPI Or MailType = gMailType.GROUPWISE Then
                                    MailFormat = "TEXT"
                                End If

                                If MailFormat = "HTML" Then
                                    ProcessReport("HTML (*.htm)", oRpt, "", nReportID, _
                                    m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                    "", False, False)
                                    EmbedPath = HTMLPath
                                Else
                                    EmbedPath = ProcessReport("Text (*.txt)", oRpt, "", nReportID, _
                                    m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                    , False, False)
                                End If

                            End If
                        Catch : End Try

                        If HTMLPath.Length > 0 Then
                            sExport = String.Empty

                            For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                sExport &= s & ";"
                            Next
                        End If

                        If MailType = MarsGlobal.gMailType.MAPI Then
                            Ok = SendMAPI(sSendTo, _
                            sSubject, sMsg, ScheduleType, sExport, 1, sExtras, _
                                sCc, sBcc, Embed, "", sFileName, , EmbedPath, ReadReceipt, nDestinationID)
                        ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then

                            Dim objSender As clsMarsMessaging = New clsMarsMessaging

                            Ok = objSender.SendSMTP(sSendTo, sSubject, _
                            sMsg, ScheduleType, sExport, 1, sExtras, sCc, _
                            sBcc, sFileName, Embed, "", , IncludeAttach, _
                            MailFormat, SMTPServer, EmbedPath, senderName, senderAddress)
                        ElseIf MailType = gMailType.GROUPWISE Then
                            Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, sExport, ScheduleType, _
                            sExtras, IncludeAttach, Embed, EmbedPath, True, 1, sFileName, MailFormat)
                        Else
                            Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                        End If

                    Case "disk"

                        Dim UseDUN As Boolean
                        Dim sDUN As String
                        Dim oNet As clsNetworking
                        'if DUN is used...
                        Try
                            UseDUN = Convert.ToBoolean(oRs("usedun").Value)
                            sDUN = IsNull(oRs("dunname").Value)
                        Catch ex As Exception
                            UseDUN = False
                        End Try

                        If UseDUN = True Then
                            oNet = New clsNetworking

                            If oNet._DialConnection(sDUN, "Single Schedule Error: " & sReportName & ": ") = False Then
                                Return False
                            End If
                        End If

                        oUI.BusyProgress(75, "Copying reports...")

                        sFile = ExtractFileName(sExport)

                        Dim paths As String = oRs("outputpath").Value

                        For Each item As String In paths.Split("|")
                            If item.Length > 0 Then
                                sPath = oParse.ParseString(item, , , , , , m_ParametersTable)

                                If sPath.EndsWith("\") = False And sPath.Length > 0 Then sPath &= "\"

                                sPath = _CreateUNC(sPath)

                                Ok = oParse.ParseDirectory(sPath)

                                If HTMLPath.Length = 0 Then
                                    System.IO.File.Copy(sExport, sPath & sFile, True)
                                Else
                                    For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                        sFile = ExtractFileName(s)
                                        System.IO.File.Copy(s, sPath & sFile, True)
                                    Next
                                End If

                                Dim oSys As New clsSystemTools

                                oSys._HouseKeeping(nDestinationID, sPath, sFileName)
                            End If
                        Next

                        Try
                            If UseDUN = True Then
                                oNet._Disconnect()
                            End If
                        Catch : End Try
                    Case "ftp"
                        Dim oFtp As New clsMarsTask
                        Dim sFtp As String = ""
                        Dim FTPServer As String
                        Dim FTPUser As String
                        Dim FTPPassword As String
                        Dim FTPPath As String
                        Dim FTPType As String
                        Dim ftpCount As Integer = 0
                        Dim FTPPassive As String
                        Dim FtpOptions As String

                        FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
                        FTPUser = oParse.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
                        FTPPassword = oParse.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
                        FTPPath = oParse.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
                        FTPType = IsNull(oRs("ftptype").Value, "FTP")
                        FTPPassive = IsNull(oRs("ftppassive").Value, "")
                        FtpOptions = IsNull(oRs("ftpoptions").Value)

                        oUI.BusyProgress(75, "Uploading report...")

                        ftpCount = FTPServer.Split("|").GetUpperBound(0)

                        If ftpCount > 0 Then
                            ftpCount -= 1
                        Else
                            ftpCount = 0
                        End If

                        For I As Integer = 0 To ftpCount
                            Dim l_FTPServer As String = FTPServer.Split("|")(I)
                            Dim l_FTPUser As String = FTPUser.Split("|")(I)
                            Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
                            Dim l_FTPPath As String = FTPPath.Split("|")(I)
                            Dim l_FTPType As String = FTPType.Split("|")(I)
                            Dim l_FTPPassive As Boolean = False
                            Dim l_FtpOptions As String

                            Try
                                l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(I)))
                            Catch ex As Exception
                                l_FTPPassive = False
                            End Try


                            Try
                                l_FtpOptions = FtpOptions.Split("|")(I)
                            Catch ex As Exception
                                l_FtpOptions = ""
                            End Try

                            If HTMLPath.Length = 0 Then

                                oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, _
                                 l_FTPPassword, l_FTPPath, _
                                 sExport, l_FTPType, l_FtpOptions, , , l_FTPPassive)
                            Else
                                For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                    sFtp &= s & "|"
                                Next

                                oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, _
                                 l_FTPPassword, l_FTPPath, _
                                 sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)
                            End If

                        Next
                        Ok = True
                    Case "fax"
                        Dim faxer As clsMarsMessaging = New clsMarsMessaging

                        Ok = faxer.SendFax(oRs("sendto").Value, oRs("subject").Value, "Single", _
                        oRs("cc").Value, oRs("bcc").Value, oRs("message").Value, sExport)
                    Case "sms"
                        Ok = SendSMS(oRs("sendto").Value, oRs("message").Value, sExport)
                    Case "sharepoint"
                        Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint

                        Dim spServer As String
                        Dim spUser As String
                        Dim spPassword As String
                        Dim spLib As String
                        Dim spCount As Integer = 0
                        Dim spmetaData As String

                        spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                        spUser = oParse.ParseString(oRs("ftpusername").Value)
                        spPassword = oParse.ParseString(oRs("ftppassword").Value)
                        spLib = oParse.ParseString(oRs("ftppath").Value)

                        oUI.BusyProgress(75, "Uploading report...")

                        spCount = spServer.Split("|").GetUpperBound(0)

                        If spCount > 0 Then
                            spCount -= 1
                        Else
                            spCount = 0
                        End If

                        For I As Integer = 0 To spCount
                            Dim l_spServer As String = spServer.Split("|")(I)
                            Dim l_spUser As String = spUser.Split("|")(I)
                            Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(I))
                            Dim l_spLib As String = spLib.Split("|")(I)
                            Dim l_metaData As String = ""
                            Dim errorInfo As Exception = Nothing

                            Try
                                l_metaData = spmetaData.Split("|")(I)
                            Catch : End Try

                            If l_spServer = "" Then Continue For

                            If HTMLPath.Length = 0 Then
                                Ok = sp.UploadDocument(l_spServer, l_spLib, sExport, l_spUser, l_spPassword, errorInfo, l_metaData)

                                If Ok = False And errorInfo IsNot Nothing Then
                                    Throw errorInfo
                                End If
                            Else
                                Dim tempFiles As String()
                                Dim n As Integer = 0

                                For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                    ReDim Preserve tempFiles(n)

                                    tempFiles(n) = s

                                    n += 1
                                Next

                                Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, tempFiles, l_spUser, l_spPassword, errorInfo)

                                If Ok = False And errorInfo IsNot Nothing Then
                                    Throw errorInfo
                                End If
                            End If
                        Next
                        Ok = True
                    Case "dropbox"
                        Dim db As clsDropboxDestination = New clsDropboxDestination(nDestinationID)
                        Dim files As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)

                        If HTMLPath.Length = 0 Then
                            files.Add(sExport)
                        Else
                            For Each t As String In System.IO.Directory.GetFiles(HTMLPath)
                                files.Add(t)
                            Next

                            For Each d As String In IO.Directory.GetDirectories(HTMLPath)
                                For Each f As String In IO.Directory.GetFiles(d)
                                    files.Add(f)
                                Next
                            Next
                        End If

                        Dim errInfo As Exception

                        Ok = db.putFiles(errInfo, files)

                        If Ok = False Then
                            Throw errInfo
                        End If
                End Select
            End If

            oRs.Close()

            If Ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

            oRs = Nothing

            oUI.BusyProgress(95, "Cleaning up...")

            oUI.BusyProgress(, , True)

            Return Ok
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)

            Return False
        Finally
            gScheduleOwner = ""
            gScheduleName = String.Empty
        End Try
    End Function
    Public Sub AutoRefreshSchedule(ByVal nReportID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim AutoRefresh As Boolean

        Try
            SQL = "SELECT AutoRefresh FROM ReportAttr WHERE ReportID =" & nReportID

            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                Try
                    AutoRefresh = Convert.ToBoolean(oRs.Fields(0).Value)
                Catch ex As Exception
                    AutoRefresh = False
                End Try
            End If

            oRs.Close()

            If AutoRefresh = True Then
                RefreshReport(nReportID)
            End If
        Catch : End Try

    End Sub

    Sub printerRegistryHackForsystem()
        Try
            Dim keys As ArrayList = New ArrayList

            keys.Add("Software\Microsoft\Windows NT\CurrentVersion\Devices")
            keys.Add("Software\Microsoft\Windows NT\CurrentVersion\PrinterPorts")
            keys.Add("Software\Microsoft\Windows NT\CurrentVersion\Windows")

            For Each key As String In keys

                Dim reg As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(key)
                Dim regx As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.Users.OpenSubKey(".DEFAULT\" & key, True)

                For Each v As String In reg.GetValueNames


                    Dim test = regx.GetValue(v)

                    If test Is Nothing Then
                        regx.SetValue(v, reg.GetValue(v))
                    End If
                Next
            Next
        Catch : End Try
    End Sub

    Public Function _PrintServer(ByVal sFiles() As String, ByVal nDestID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRpt As Object
        Dim retry As Integer = 0
        Dim ok As Boolean = False
        Dim printAssystem As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("PrintAsSystem", 0))
        Dim context As WindowsImpersonationContext = Nothing
        Dim spoolReset As Boolean



        If printAssystem Then
            '//turn off inmpersonation
10:         Try
20:             If Not (WindowsIdentity.GetCurrent().IsSystem) Then
30:                 context = WindowsIdentity.Impersonate(System.IntPtr.Zero)
                End If
            Catch : End Try

            printerRegistryHackForsystem()
        End If

40:     Do


50:         Try

70:             SQL = "SELECT * FROM PrinterAttr WHERE DestinationID = " & nDestID

80:             oRs = clsMarsData.GetData(SQL)

90:             Do While oRs.EOF = False

                    Dim sPDF As String
                    Dim nCopies As Integer
                    Dim PrinterName As String
                    Dim PrintPage As Integer
100:                Dim oPrint As New clsPrinters
110:                Dim oProc As New Process
120:                Dim ucPrinter As ucPDFViewer = New ucPDFViewer
                    Dim PageFrom, PageTo As Integer
130:                Dim pdf As clsMarsPDF = New clsMarsPDF

140:                nCopies = oRs("copies").Value
150:                PrinterName = clsMarsParser.Parser.ParseString(oRs("printername").Value)

160:                Try
170:                    PageFrom = oRs("pagefrom").Value
180:                    PageTo = oRs("pageto").Value
190:                Catch ex As Exception
200:                    PageFrom = 0
210:                    PageTo = 0
                    End Try

220:                For Each s As String In sFiles
230:                    sPDF = s

240:                    pdf.PrintPDF(s, nCopies, PrinterName, PageFrom, PageTo)

250:                Next

260:                oRs.MoveNext()
270:            Loop

280:            oRs.Close()

290:            oRs = Nothing

300:            ok = True
310:        Catch ex As Exception
                If ex.Message.Contains("Settings to access printer") And spoolReset = False Then
                    resetSpooler()
                    spoolReset = True
                End If

311:            System.Threading.Thread.Sleep(5000)

320:            retry += 1
330:            gErrorDesc = ex.Message
340:            gErrorNumber = Err.Number
350:            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
360:            gErrorLine = Erl()

370:            ok = False
            End Try
380:    Loop Until ok = True Or retry > 3

        If printAssystem Then
            '//turn impersonation back on
390:        Try
400:            If context IsNot Nothing Then context.Undo()
            Catch : End Try
        End If

410:    Return ok
    End Function

    Private Sub resetSpooler()
        Try
            Dim svc As ServiceProcess.ServiceController = New ServiceProcess.ServiceController("Print Spooler")

            svc.Stop()

            System.Threading.Thread.Sleep(60000)

            Dim sysFolder As String = Environment.GetFolderPath(Environment.SpecialFolder.System)
            Dim spoolFolder As String = IO.Path.Combine(sysFolder, "Spool\Printers")

            For Each f As String In IO.Directory.GetFiles(spoolFolder)
                Try
                    IO.File.Delete(f)
                Catch : End Try
            Next

            svc.Start()

            System.Threading.Thread.Sleep(3000)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, , True, True)
        End Try
    End Sub
    Private Function _CheckForDynamic(ByVal IsDynamic As Boolean, ByVal sIn As String, ByVal nID As Integer, _
    ByVal sKeyParameter As String, ByVal sKeyValue As String, Optional ByVal sType As String = "Report") As Object
        If sIn.IndexOf("<[x]") = -1 Then
            Return sIn
        Else
            Dim oDynamic As New clsMarsDynamic
            Dim sReturn As String

            sReturn = oDynamic._CreateDynamicString(sIn, nID, sKeyParameter, sKeyValue, sType)

            If sReturn Is Nothing Then
                sReturn = String.Empty
            End If

            Return sReturn
        End If
    End Function

    Private Delegate Function SendSMTP_A(ByVal Addresses As String, ByVal Subject As String, ByVal Body As String, _
       ByVal SendType As String, ByVal Attach As String, _
       ByVal Numattach As Integer, ByVal Extras As String, _
       ByVal SendCC As String, ByVal SendBCC As String, _
       ByVal sName As String, ByVal InBody As Boolean, _
       ByVal sFormat As String, _
       ByVal LogFailure As Boolean, _
       ByVal IncludeAttach As Boolean, _
       ByVal MailFormat As String, _
       ByVal SMTPName As String, ByVal sEmbedFile As String, ByVal SenderAddress As String, ByVal SenderName As String, _
    ByVal customMsg As Quiksoft.EasyMail.SMTP.EmailMessage) As Boolean

    Public Function SetServerLogin(ByVal oRpt As ReportServer.ReportingService, ByVal nReportID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim ServerUser As String = ""
        Dim ServerPassword As String = ""
        Dim formsAuth As Boolean = False

10:     Try
20:         SQL = "SELECT RptUserID, RptPassword, rptFormsAuth FROM ReportAttr WHERE ReportID = " & nReportID

30:         oRs = clsMarsData.GetData(SQL)

40:         If oRs Is Nothing Then Return False

50:         If oRs.EOF = True Then
60:             oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials

                'for 2005 larkies
70:             Dim netCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential

80:             netCredentials = System.Net.CredentialCache.DefaultCredentials

90:             If m_rv IsNot Nothing Then m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = netCredentials

100:            Try
                    '//for forms auth
110:                oRpt.LogonUser(netCredentials.UserName, netCredentials.Password, Nothing)
                Catch : End Try
120:        Else
130:            ServerUser = IsNull(oRs("rptuserid").Value)
140:            ServerPassword = _DecryptDBValue(IsNull(oRs("rptpassword").Value))

                Try
                    formsAuth = Convert.ToInt32(IsNull(oRs("rptFormsAuth").Value, 0))
                Catch : End Try

150:            If ServerUser.Length = 0 Then
160:                oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials

170:                If m_rv IsNot Nothing Then m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials
180:            Else

190:                Dim userDomain As String = ""
                    Dim tmpUser As String = ServerUser

                    getDomainAndUserFromString(tmpUser, userDomain)


                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, ServerPassword, userDomain)

200:                oRpt.Credentials = oCred

210:                If m_rv IsNot Nothing Then
                        m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred

                        '//forms auth
                        Try
                            If formsAuth Then m_rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, tmpUser, ServerPassword, Nothing)
                        Catch : End Try
                    Else
                        oRpt.LogonUser(ServerUser, ServerPassword, userDomain)
                    End If

                    Try
230:                    SetServerViewerLogin(nReportID)
                    Catch : End Try

                End If
            End If

240:        oRs.Close()

250:        Return True
260:    Catch ex As Exception
270:        gErrorDesc = ex.Message
280:        gErrorNumber = Err.Number
290:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
300:        gErrorLine = Erl()
310:        Return False
        End Try
    End Function


    Public Function SetServerViewerLogin(ByVal nReportID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim ServerUser As String = ""
        Dim ServerPassword As String = ""
        Dim formsAuth As Boolean = False

        Try
            SQL = "SELECT RptUserID, RptPassword, rptFormsAuth  FROM ReportAttr WHERE ReportID = " & nReportID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return False

            If oRs.EOF = True Then
                'for 2005 larkies
                Dim netCredentials As System.Net.NetworkCredential = New System.Net.NetworkCredential

                netCredentials = System.Net.CredentialCache.DefaultCredentials

                If m_rv IsNot Nothing Then m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = netCredentials
            Else
                ServerUser = IsNull(oRs("rptuserid").Value)
                ServerPassword = _DecryptDBValue(IsNull(oRs("rptpassword").Value))

                Try
                    formsAuth = Convert.ToInt32(IsNull(oRs("rptFormsAuth").Value, 0))
                Catch ex As Exception
                    formsAuth = False
                End Try

                If ServerUser.Length = 0 Then
                    If m_rv IsNot Nothing Then
                        m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials
                    End If
                Else

                    Dim userDomain As String = ""
                    Dim tmpUser As String = ServerUser

                    getDomainAndUserFromString(tmpUser, userDomain)


                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(tmpUser, ServerPassword, userDomain)

                    If m_rv IsNot Nothing Then
                        m_rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred

                        Try
                            If formsAuth Then m_rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, ServerUser, ServerPassword, Nothing)
                        Catch : End Try
                    End If

                End If
            End If

            oRs.Close()

            Return True
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return False
        End Try
    End Function
    Public Function CreateDatasources(ByVal nReportID As Integer) As ReportServer.DataSourceCredentials()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oLogins() As ReportServer.DataSourceCredentials
        Dim I As Integer
        Dim dsCred() As Microsoft.Reporting.WinForms.DataSourceCredentials

        SQL = "SELECT * FROM ReportDatasource WHERE ReportID =" & nReportID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim dsName As String = IsNull(oRs("datasourcename").Value, "")
                Dim userName As String = IsNull(oRs("rptuserid").Value, "default")
                Dim password As String = IsNull(oRs("rptpassword").Value, "<default>")

                If userName = "default" And password = "<default>" Then GoTo skip

                ReDim Preserve oLogins(I)
                ReDim Preserve dsCred(I)

                oLogins(I) = New ReportServer.DataSourceCredentials

                dsCred(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                With oLogins(I)
                    .DataSourceName = oRs("datasourcename").Value
                    .UserName = oRs("rptuserid").Value
                    .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                End With

                With dsCred(I)
                    .Name = oRs("datasourcename").Value
                    .UserId = oRs("rptuserid").Value
                    .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                End With
skip:
                oRs.MoveNext()
                I += 1
            Loop

            oRs.Close()
        End If

        Try
            If m_rv IsNot Nothing Then
                m_rv.ServerReport.SetDataSourceCredentials(dsCred)
            End If
        Catch : End Try

        Return oLogins
    End Function

    Public Function _CreateErrorString() As String
        Dim sError As String

        sError = "Error Description: " & gErrorDesc & vbCrLf & _
        "Error Number: " & gErrorNumber & vbCrLf & _
        "Error Source: " & gErrorSource & vbCrLf & _
        "Error Line: " & gErrorLine & vbCrLf & _
        "Suggestion: " & gErrorSuggest

        Return sError
    End Function

    Private Sub CancelExecution(ByVal method As String)
        Application.DoEvents()
        gErrorDesc = "User Cancelled schedule execution"
        gErrorNumber = -2046082905
        gErrorSource = method
        gErrorLine = 0

        If m_DynamicSchedule() = True Then
            Me.DynamicCancelled = True
        End If

        _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, 0)
    End Sub

    Private Function DoIncludeAttach(ByVal destinationID As Integer) As Boolean
        Try
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT IncludeAttach FROM DestinationAttr WHERE DestinationID =" & destinationID)

            If oRs Is Nothing Then Return True

            If oRs.EOF = True Then Return True

            Dim attach As Boolean = oRs(0).Value

            oRs.Close()

            Return attach
        Catch
            Return True
        End Try
    End Function
    Public Overloads Function RunSingleSchedule(ByVal nReportID As Integer, _
           Optional ByVal ShowFinish As Boolean = False, _
           Optional ByVal ForSnap As Boolean = False, Optional ByVal SnapPath As String = "", _
           Optional ByVal CustomSend As String = "", Optional ByVal customFax As String = "", _
           Optional ByVal customSMS As String = "", Optional ByVal RunTasks As Boolean = True, _
           Optional ByVal IsDynamic As Boolean = False, Optional ByVal PrintDate As String = "", _
           Optional ByVal customSubject As String = "") As Boolean
        Dim oRpt As ReportServer.ReportingService 'ReportServer.ReportingService
        Dim sReportName As String
        Dim sTemp As String = ""
        Dim oUI As clsMarsUI = New clsMarsUI

        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim sExport As String

            Dim sFormat As String
            Dim sFileName As String
            Dim sDateStamp As String
            Dim sFile As String
            Dim sFormula As String
10:         Dim oTask As clsMarsTask = New clsMarsTask
20:         Dim oSchedule As New clsMarsScheduler
            Dim nScheduleID As Int32 = 0
            'Dim IsDynamic As Boolean
            Dim nInclude As Integer
30:         Dim oParse As New clsMarsParser
            Dim Ok As Boolean
            Dim sDynamic() As String
            Dim sExt As String
            Dim HTMLSplit As Boolean
            Dim isDefer As Boolean
            Dim sReportPath As String
            Dim UseSavedData As Boolean
            Dim nDestinationID As Integer
            Dim sPrintMethod As String

            Dim oLogins() As ReportServer.DataSourceCredentials
            Dim sCluster As String
            Dim ExcelBurst As Boolean = False
            Dim sBurstColumn As String
            Dim oBlank As ADODB.Recordset
            Dim IsStatic As Boolean = False
            Dim TryCount As Int32 = 0

            Dim IncludeAttach As Boolean
            Dim PDFBookmark As Boolean = False
            Dim sDestName As String
            Dim adjustStamp As Double
            Dim CRFieldType As String = ""
40:
50:         Dim oDynamcData As New clsMarsDynamic
            Dim nRetry As Integer = 0
            Dim sUrl As String = ""
            Dim rptTimeout As Integer
            Dim appendToFile As Boolean = False

60:         gCurrentTempOutputFile = ""
            gScheduleFolderName = clsMarsUI.getObjectParentName(nReportID, clsMarsScheduler.enScheduleType.REPORT)
            gScheduleOwner = clsMarsScheduler.getScheduleOwner(nReportID, clsMarsScheduler.enScheduleType.REPORT)

70:         If IsDynamic = False Then ScheduleStart = Now

            m_rv = Nothing

            Try
80:             SetReportParameters(nReportID)
            Catch : End Try

90:
100:        HTMLPath = ""

110:        'AutoRefreshSchedule(nReportID)

120:        sCluster = oUI.ReadRegistry("SystemEnvironment", "")

130:        nScheduleID = oSchedule.GetScheduleID(nReportID)
140:        m_scheduleID = nScheduleID

            'run custom tasks
150:        If (IsDynamic = False) Or (IsDynamic = True And RunTasks = True) Then
160:            oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)
            End If

170:        Me.m_DynamicSchedule = IsDynamic

180:        oUI.BusyProgress(25, "Collecting data...")

190:        SQL = "SELECT * FROM ReportAttr r INNER JOIN DestinationAttr d ON " & _
                  "r.ReportID = d.ReportID " & _
                  "WHERE r.ReportID = " & nReportID & " AND d.EnabledStatus = 1 ORDER BY d.DestOrderID"

200:        oRs = clsMarsData.GetData(SQL)

210:        If oRs Is Nothing Then Err.Raise(-102, , _
                  "Failed to obtain valid recordset (ReportAttr)")

220:        If oRs.EOF = True Then Err.Raise(-103, , _
                  "The recordset did not return any records (ReportAttr)")

230:        If frmMain.m_UserCancel = True Then
240:            Me.CancelExecution("RunSingleSchedule")
250:            Return False
            End If
            'get values from recordset before looping
260:        If oRs.EOF = False Then
270:            sReportName = oRs("ReportName").Value
280:            sUrl = oRs("databasepath").Value
290:            gScheduleName = sReportName
300:            nRetry = IsNull(oRs("retry").Value, 0)
310:            rptTimeout = IsNull(oRs("assumefail").Value, 30)

320:            If rptTimeout = 0 Then rptTimeout = -1 '360

330:            sReportPath = IsNull(CType(oRs.Fields("cachepath").Value, String))

340:            If Not oRpt Is Nothing Then
350:                oRpt = Nothing
360:            End If

370:            Try
380:                IsStatic = Convert.ToBoolean(oRs("staticdest").Value)
390:            Catch
400:                IsStatic = False
                End Try

410:            oRpt = New ReportServer.ReportingService
420:            greportItem = oRpt

430:            oRpt.Timeout = -1 '3600000

440:            oRpt.Url = sUrl

                'set up reportviewer (rv) if server version is 2005
450:            If m_rv IsNot Nothing Then
460:                m_rv = Nothing
470:            End If

480:            m_rv = New Microsoft.Reporting.WinForms.ReportViewer
490:            m_rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

500:            Dim serverUrl As String = ""

510:            For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
520:                serverUrl &= sUrl.Split("/")(I) & "/"
530:            Next

540:            m_rv.ServerReport.ReportServerUrl = New Uri(serverUrl)

570:            If frmMain.m_UserCancel = True Then
580:                Me.CancelExecution("RunSingleSchedule")
590:                Return False
600:            End If

610:            If Me.SetServerLogin(oRpt, nReportID) = False Then
620:                Return False
630:            End If

640:            oLogins = Me.CreateDatasources(nReportID)
650:        End If

            'loop through the destinations
660:        If frmMain.m_UserCancel = True Then
670:            Me.CancelExecution("RunSingleSchedule")
680:            Return False
            End If

            Dim destinationCount As Integer = 0

690:        Do While oRs.EOF = False
                destinationCount += 1

700:            If frmMain.m_UserCancel = True Then
710:                Me.CancelExecution("RunSingleSchedule")
720:                Return False
                End If

730:            Try
740:                HTMLPath = String.Empty
750:                nDestinationID = oRs("destinationid").Value
760:                sExt = oParse.ParseString(IsNull(oRs("customext").Value), , , , , , m_ParametersTable)
770:                ExcelBurst = False
780:                PDFBookmark = False
790:                sDestName = IsNull(oRs("destinationname").Value, "")
800:                adjustStamp = IsNull(oRs("adjuststamp").Value, 0)
810:                sFormat = IsNull(oRs("outputformat").Value, "")

820:                If sFormat = "Fax Format" Then sFormat = "TIFF Fax (*.pdf)"

830:                If IsNull(oRs("dynamic").Value, "0") = "1" Then
840:                    IsDynamic = True
850:                Else
860:                    IsDynamic = False
870:                End If

880:                If IsNull(oRs("CustomName").Value) <> "" Then

890:                    If IsDynamic = False Then

900:                        sFileName = oParse.ParseString(oRs("CustomName").Value, , , , , , m_ParametersTable)

910:                        If sFileName.IndexOf("[p]") = -1 And sFileName.IndexOf("[~]") = -1 Then
920:                            sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                                      Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                            End If
930:                    Else

940:                        sFileName = oParse.ParseString(oRs("customname").Value, , , , , , m_ParametersTable)

950:                        If sFileName.IndexOf("<[x]") > -1 Then
960:                            sFileName = oDynamcData._CreateDynamicString(sFileName, _
                                      nReportID, sKeyParameter, sKeyValue)
970:                        End If

980:                        If sFileName.IndexOf("[p]") = -1 And sFileName.IndexOf("[~]") = -1 Then
990:                            sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                                      Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
                            End If
1000:                   End If

1010:               Else

1020:                   sFileName = oRs("ReportTitle").Value
1030:                   sFileName = sFileName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                             Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")
1040:               End If

1050:               If Convert.ToBoolean(oRs("AppendDateTime").Value) = True Then
1060:                   sDateStamp = IsNull(oRs("DateTimeFormat").Value)

1070:                   sDateStamp = (Date.Now).AddDays(adjustStamp).ToString(sDateStamp)
1080:               Else
1090:                   sDateStamp = ""
1100:               End If

1110:               oUI.BusyProgress(50, "Exporting report...")

1120:               If oRs("destinationtype").Value = "Printer" Then
1130:                   sFormat = "Acrobat Format (*.pdf)"
1140:                   IncludeAttach = True
                    End If

1150:               If frmMain.m_UserCancel = True Then
1160:                   Me.CancelExecution("RunSingleSchedule")
1170:                   Return False
                    End If

                    Dim sSendTo As String = ""
                    Dim sSubject, senderName, senderAddress As String
                    Dim sCc As String
                    Dim sBcc As String
                    Dim sExtras As String
                    Dim sMsg As String
                    Dim sName As String
                    Dim sPath As String
1180:               Dim oDynamicData As New clsMarsDynamic
                    Dim Embed As Boolean
                    Dim MailFormat As String
                    Dim ScheduleType As String = "Single"
                    Dim nDeferBy As Double
                    Dim SMTPServer As String
                    Dim ReadReceipt As Boolean = False

1190:               If ForSnap = True And SnapPath.Length > 0 Then
1200:                   sExport = SnapPath
1210:               Else

1220:                   Try
1230:                       IncludeAttach = Me.DoIncludeAttach(nDestinationID)
1240:                   Catch ex As Exception
1250:                       IncludeAttach = True
1260:                   End Try

1270:                   If frmMain.m_UserCancel = True Then
1280:                       Me.CancelExecution("RunSingleSchedule")
1290:                       Return False
                        End If

1300:                   If IncludeAttach = True Then
1310:                       sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, _
                                 sTempPath & sFileName, sFileName, sReportPath, _
                                 oLogins, "", oRs("destinationid").Value, sExt, IsDynamic, False, 0, rptTimeout)
1320:                   Else
1330:                       sExport = String.Empty
                        End If
1340:               End If

1350:               If sExport IsNot Nothing Then
1360:                   If sExport.ToLower = "{error}" Then
                            Throw New Exception("Report Processing Error: " & vbCrLf & Me._CreateErrorString)
                        End If

1370:                   If sExport.ToLower = "{skip}" Then
1380:                       Ok = True
1390:                       GoTo NextDest
                        End If
                    End If
                    'set the excel file password
1400:               Try
1410:                   If sFormat.ToLower.IndexOf("excel") > -1 Then
1420:                       Dim oRsExcel As ADODB.Recordset

1430:                       oRsExcel = clsMarsData.GetData("SELECT ProtectExcel,ExcelPassword FROM ReportOptions " & _
                                 "WHERE DestinationID =" & nDestinationID)

1440:                       If oRsExcel.EOF = False Then
1450:                           If IsNull(oRsExcel("protectexcel").Value, "0") = "1" Then
1460:                               Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)
1470:                               oXL.SetWorkBookPassword(sExport, _DecryptDBValue(oRsExcel("excelpassword").Value))
1480:                               oXL.Dispose()
1490:                           End If
1500:                       End If

1510:                       oRsExcel.Close()
1520:                   End If
                    Catch : End Try


1530:               Dim oSnap As New clsMarsSnapshots

1540:               oSnap.CreateSnapshot(nReportID, sExport, HTMLPath)
1550:
1560:               If frmMain.m_UserCancel = True Then
1570:                   Me.CancelExecution("RunSingleSchedule")
1580:                   Return False
                    End If

1590:               gExportedFileName = ExtractFileName(sExport)

1600:               sExtras = oParse.ParseString(IsNull(oRs("extras").Value), , , , , , m_ParametersTable)

1610:               Dim oPGP As New clsMarsPGP
                    Dim oPGPResult()

1620:               oPGPResult = oPGP.EncryptFile(sExport, nDestinationID)

1630:               If oPGPResult(0) = False Then
1640:                   Throw New Exception("PGP Encryption Error: " & vbCrLf & Me._CreateErrorString)
                    End If

1650:               sExport &= oPGPResult(1)

1660:               oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)

1670:               If frmMain.m_UserCancel = True Then
1680:                   Me.CancelExecution("RunSingleSchedule")
1690:                   Return False
                    End If

1700:               Try
1710:                   If oRs("compress").Value = 1 And sExport.Length > 0 Then
                            Dim Encrypt As Boolean
                            Dim ZipCode As String = ""

1720:                       oUI.BusyProgress(60, "Zipping report...")

1730:                       Try
1740:                           Encrypt = oRs("encryptzip").Value
1750:                           ZipCode = IsNull(oRs("encryptzipcode").Value, "")
                            Catch : Encrypt = False : End Try

1760:                       sExport = ZipFiles(ScheduleType, m_OutputFolder, sExport, Encrypt, ZipCode)

1770:                       HTMLPath = ""
1780:                   End If
1790:               Catch : End Try

                    'if delivery should be defered
1800:               Dim nDefer As Integer

1810:               Try
1820:                   nDefer = oRs("deferdelivery").Value
1830:               Catch ex As Exception
1840:                   nDefer = 0
1850:               End Try

1860:               If nDefer = 1 And RunEditor = False Then
1870:                   nDeferBy = oRs("deferby").Value

1880:                   Ok = _DeferDelivery(oRs("destinationid").Value, sExport, _
                             nDeferBy, sFileName)
1890:
1900:                   isDefer = True
1910:               Else
1920:                   If frmMain.m_UserCancel = True Then
1930:                       Me.CancelExecution("RunSingleSchedule")
1940:                       Return False
                        End If

1950:                   Select Case oRs("DestinationType").Value.Tolower
                            Case "printer"
1960:                           oUI.BusyProgress(75, "Printing reports...")

                                Dim sFilePrint(0) As String

1970:                           sFilePrint(0) = sExport
1980:                           Ok = Me._PrintServer(sFilePrint, nDestinationID)

1990:                           If Ok = False Then
2000:                               Throw New Exception(Me._CreateErrorString)
                                End If

                            Case "email"

2020:                           If IsDynamic = False Or IsStatic = True Then

2030:                               If CustomSend.Length = 0 Then
2040:                                   sSendTo = ResolveEmailAddress(oRs("sendto").Value)
2050:                                   sCc = ResolveEmailAddress(oRs("cc").Value)
2060:                                   sBcc = ResolveEmailAddress(oRs("bcc").Value)

2070:                                   sSendTo = oParse.ParseString(sSendTo, , , , , , m_ParametersTable)
2080:                                   sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
2090:                                   sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)
2100:                               Else
2110:                                   sSendTo = CustomSend
                                        sCc = ""
                                        sBcc = ""
2120:                               End If
2130:                           Else
2140:                               sDynamic = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

2150:                               If sDynamic Is Nothing Then
2160:                                   Return False
2170:                               End If

2180:                               For Each s As String In sDynamic
2190:                                   If s.Length > 0 Then
2200:                                       sSendTo &= s & ";"
2210:                                   End If
2220:                               Next

2230:                               If sSendTo.Length = 0 Then
2240:                                   gErrorDesc = sReportName & ": The linked column did not contain a valid email address"
2250:                                   gErrorNumber = -101
2260:                                   gErrorSource = "clsMarsReport._RunSingleSchedule"
2270:                                   gErrorLine = 1920

2280:                                   Throw New Exception(Me._CreateErrorString)

2290:                               End If

2300:                               sCc = ResolveEmailAddress(oRs("cc").Value)
2310:                               sBcc = ResolveEmailAddress(oRs("bcc").Value)

2320:                               sCc = oParse.ParseString(sCc, , , , , , m_ParametersTable)
2330:                               sBcc = oParse.ParseString(sBcc, , , , , , m_ParametersTable)
2340:                           End If

2350:                           sSubject = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)

2360:                           sSubject = Me._CheckForDynamic(IsDynamic, sSubject, nReportID, sKeyParameter, sKeyValue)

                                If customSubject <> "" Then
                                    sSubject = customSubject
                                End If

2370:                           MailFormat = oRs("mailformat").Value
2380:                           SMTPServer = IsNull(oRs("smtpserver").Value, "Default")
2381:                           senderName = oParse.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
2382:                           senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)

2390:                           sMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

2400:                           sMsg = Me._CheckForDynamic(IsDynamic, sMsg, nReportID, sKeyParameter, sKeyValue)

2410:                           Try
2420:                               ReadReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
2430:                           Catch ex As Exception
2440:                               ReadReceipt = False
                                End Try

2450:                           oUI.BusyProgress(75, "Emailing report...")

2460:                           Try
2470:                               Embed = Convert.ToBoolean(oRs("embed").Value)
2480:                           Catch ex As Exception
2490:                               Embed = False
2500:                           End Try

2510:                           If HTMLPath.Length > 0 Then
2520:                               sExport = String.Empty

2530:                               For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
2540:                                   sExport &= s & ";"
2550:                               Next
2560:                           End If

                                'now if we want to embed
2570:                           Try
2580:                               If Embed = True Then
                                        Dim preserveLinks As Boolean = False

2590:                                   If MailType = MarsGlobal.gMailType.MAPI Or MailType = gMailType.GROUPWISE Then
2600:                                       MailFormat = "TEXT"
                                        End If

2610:                                   If MailFormat = "HTML" Then
2620:                                       preserveLinks = True
2630:                                   Else
2640:                                       preserveLinks = False
                                        End If

2650:                                   If MailFormat = "HTML" Or MailFormat = "HTML (Basic)" Then
2660:                                       EmbedPath = ProcessReport("HTML (*.htm)", oRpt, "", nReportID, _
                                                 m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                                 "", False, False, , rptTimeout, preserveLinks)
                                            'EmbedPath = HTMLPath
                                        ElseIf MailFormat = "Web Archive" Then
                                            EmbedPath = ProcessReport("Web Archive (*.mhtm)", oRpt, "", nReportID, _
                                                m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                                "", False, False, , rptTimeout, preserveLinks)
                                        ElseIf MailFormat = "IMAGE" Then
                                            Dim tmp As String = ProcessReport("PNG (*.png)", oRpt, "", nReportID, _
                                                 m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                                 "", False, False, , rptTimeout, preserveLinks)
                                            EmbedPath = HTMLPath
2670:                                   Else
2680:                                       EmbedPath = ProcessReport("Text (*.txt)", oRpt, "", nReportID, _
                                                 m_OutputFolder & clsMarsData.CreateDataID, sFileName, sReportPath, oLogins, "", nDestinationID, _
                                                 , False, False, , rptTimeout)
                                        End If

2690:                                   If EmbedPath = "{skip}" Or EmbedPath.Length = 0 Then
2700:                                       Ok = True
2710:                                       GoTo NextDest
                                        End If
2720:                               End If
                                Catch : End Try

2730:                           If MailType = MarsGlobal.gMailType.MAPI Then
2740:                               Ok = SendMAPI(sSendTo, _
                                         sSubject, sMsg, ScheduleType, sExport, 1, sExtras, _
                                         sCc, sBcc, Embed, MailFormat, sFileName, , EmbedPath, ReadReceipt, nDestinationID)
2750:                           ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then

                                    Dim objSender As clsMarsMessaging = New clsMarsMessaging
2760:
2810:                               Ok = objSender.SendSMTP(sSendTo, sSubject, _
                                         sMsg, ScheduleType, sExport, 1, sExtras, sCc, _
                                         sBcc, sFileName, Embed, "", , IncludeAttach, _
                                         MailFormat, SMTPServer, EmbedPath, senderName, senderAddress)

2820:                           ElseIf MailType = gMailType.GROUPWISE Then
2830:                               Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, sExport, ScheduleType, _
                                    sExtras, IncludeAttach, Embed, EmbedPath, True, 1, sFileName, MailFormat)
2840:                           Else
2850:                               Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
2860:                           End If

2870:                           If Ok = False Then
2880:                               Throw New Exception(Me._CreateErrorString)
                                End If
                            Case "disk"

                                Dim UseDUN As Boolean
                                Dim sDUN As String
                                Dim oNet As clsNetworking
                                'if DUN is used...
2900:                           Try
2910:                               UseDUN = Convert.ToBoolean(oRs("usedun").Value)
2920:                               sDUN = IsNull(oRs("dunname").Value)
2930:                           Catch ex As Exception
2940:                               UseDUN = False
                                End Try

2950:                           If UseDUN = True Then
2960:                               oNet = New clsNetworking

2970:                               If oNet._DialConnection(sDUN, "Single Schedule Error: " & sReportName & ": ") = False Then
2980:                                   Throw New Exception(Me._CreateErrorString)
2990:                               End If
3000:                           End If

3010:                           SQL = "SELECT AppendToFile FROM ReportOptions WHERE DestinationID =" & nDestinationID

                                Dim rsAppend As ADODB.Recordset = clsMarsData.GetData(SQL)

3020:                           If rsAppend IsNot Nothing Then
3030:                               If rsAppend.EOF = False Then
3040:                                   appendToFile = IsNull(rsAppend("appendtofile").Value, 0)
                                    End If

3050:                               rsAppend.Close()
                                End If

3060:                           Select Case sFormat
                                    Case "CSV (*.csv)", "Tab Separated (*.txt)", "Text (*.txt)", "Record Style (*.rec)"
3070:                               Case Else
3080:                                   appendToFile = False
                                End Select

3090:                           oUI.BusyProgress(75, "Copying reports...")

3100:                           sFile = ExtractFileName(sExport)

3110:                           Dim paths As String = IsNull(oRs("outputpath").Value)

3120:                           If paths.EndsWith("|") Then paths = paths.Substring(0, paths.Length - 1)

3130:                           For Each item As String In paths.Split("|")

3140:                               If item IsNot Nothing Then

3150:                                   If item.Length = 0 And (IsDynamic = False Or IsStatic = True) Then Continue For

3160:                                   sPath = oParse.ParseString(item, , , , , , m_ParametersTable)

3170:                                   If sPath.EndsWith("\") = False And sPath.Length > 0 Then sPath &= "\"

3180:                                   sPath = Me._CheckForDynamic(IsDynamic, sPath, nReportID, sKeyParameter, sKeyValue)

3190:                                   If sPath Is Nothing Then
3200:                                       Throw New Exception(Me._CreateErrorString)
                                        End If

3210:                                   If IsDynamic = True And IsStatic = False Then

3220:                                       sDynamic = oDynamcData._GetDynamicData(nReportID, _
                                                 sKeyParameter, sKeyValue)
3230:
3240:                                       If sDynamic Is Nothing Then
3250:                                           Throw New Exception(Me._CreateErrorString)
3260:                                       End If

3270:                                       If sDynamic(0) = String.Empty Then
3280:                                           Throw New Exception("The dynamic query did not return any valid directory values")
                                            End If

3290:                                       For Each s As String In sDynamic
3300:                                           If s.Length > 0 Then

                                                    'If sPath.Length = 0 And s.IndexOf(":") = -1 And s.IndexOf("\\") = -1 Then
                                                    '    Continue For
                                                    'End If

3310:                                               sTemp = sPath

3320:                                               If s.EndsWith("\") = False Then
3330:                                                   If s.IndexOf(":") > -1 Then
3340:                                                       sTemp = s & "\" & sTemp
3350:                                                   Else
3360:                                                       sTemp &= s & "\"
3370:                                                   End If
3380:                                               Else
3390:                                                   If s.IndexOf(":") > -1 Then
3400:                                                       sTemp = s & sTemp
3410:                                                   Else
3420:                                                       sTemp &= s
3430:                                                   End If
3440:                                               End If

3450:                                               sTemp = _CreateUNC(sTemp)

3460:                                               Ok = oParse.ParseDirectory(sTemp)

                                                    Dim sTemp1 As String = sTemp & sFile

3470:                                               If HTMLPath.Length = 0 Then
3480:                                                   If appendToFile = True And IO.File.Exists(sTemp1) Then
                                                            Dim oRead As String = vbCrLf & ReadTextFromFile(sExport) & vbCrLf

3490:                                                       SaveTextToFile(oRead, sTemp1, , True, True)
3500:                                                   Else
3510:                                                       System.IO.File.Copy(sExport, sTemp1, True)
                                                        End If
3520:                                               Else
3530:                                                   For Each x As String In System.IO.Directory.GetFiles(HTMLPath)
3540:                                                       sFile = ExtractFileName(x)

3550:                                                       System.IO.File.Copy(x, sTemp & sFile, True)
3560:                                                   Next
3570:                                               End If
3580:                                           End If
3590:                                       Next
3600:                                   Else
3610:                                       sPath = _CreateUNC(sPath)

3620:                                       Ok = oParse.ParseDirectory(sPath)
3630:
                                            Dim sTemp1 As String = sPath & sFile

3640:                                       If HTMLPath.Length = 0 Then
3650:                                           Try
3660:                                               If appendToFile = True And IO.File.Exists(sTemp1) Then
                                                        Dim oRead As String = vbCrLf & ReadTextFromFile(sExport)

3670:                                                   SaveTextToFile(oRead, sTemp1, , True, True)
3680:                                               Else
3690:                                                   System.IO.File.Copy(sExport, sTemp1, True)
                                                    End If
3700:                                           Catch ex As Exception
3710:                                               If Err.Number = 57 Then
3720:                                                   For Each o As Process In Process.GetProcessesByName("excel")
3730:                                                       o.Kill()
3740:                                                   Next

3750:                                                   System.IO.File.Copy(sExport, sPath & sFile, True)
                                                    End If
                                                End Try
3760:                                       Else
3770:                                           For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
3780:                                               sFile = ExtractFileName(s)
3790:                                               System.IO.File.Copy(s, sPath & sFile, True)
3800:                                           Next
3810:                                       End If
3820:                                   End If

3830:                                   Dim oSys As New clsSystemTools

3840:                                   oSys._HouseKeeping(nDestinationID, sPath, sFileName)
                                    End If
3850:                           Next

3860:                           Try
3870:                               If UseDUN = True Then
3880:                                   oNet._Disconnect()
3890:                               End If
3900:                           Catch : End Try

                            Case "ftp"
3920:                           Dim oFtp As New clsMarsTask
                                Dim sFtp As String = ""
                                Dim FTPServer As String
                                Dim FTPUser As String
                                Dim FTPPassword As String
                                Dim FTPPath As String
                                Dim FTPType As String
                                Dim ftpCount As Integer = 0
                                Dim FTPPassive As String
                                Dim FtpOptions As String

3930:                           FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
3940:                           FTPUser = oParse.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
3950:                           FTPPassword = oParse.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
3960:                           FTPPath = oParse.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
3970:                           FTPType = IsNull(oRs("ftptype").Value, "FTP")
3971:                           FTPPassive = IsNull(oRs("ftppassive").Value, "")
3972:                           FtpOptions = IsNull(oRs("ftpoptions").Value, "")

3980:                           oUI.BusyProgress(75, "Uploading report...")

3990:                           ftpCount = FTPServer.Split("|").GetUpperBound(0)

4000:                           If ftpCount > 0 Then
4010:                               ftpCount -= 1
4020:                           Else
4030:                               ftpCount = 0
                                End If

4040:                           For I As Integer = 0 To ftpCount
4050:                               Dim l_FTPServer As String = FTPServer.Split("|")(I)
4060:                               Dim l_FTPUser As String = FTPUser.Split("|")(I)
4070:                               Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
4080:                               Dim l_FTPPath As String = FTPPath.Split("|")(I)
4090:                               Dim l_FTPType As String = FTPType.Split("|")(I)
                                    Dim l_FTPPassive As Boolean = False
                                    Dim l_FtpOptions As String

                                    Try
                                        l_FtpOptions = FtpOptions.Split("|")(I)
                                    Catch
                                        l_FtpOptions = ""
                                    End Try
                                    Try
3922:                                   l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(I)))
                                    Catch ex As Exception
3923:                                   l_FTPPassive = False
                                    End Try

4100:                               If IsDynamic = False Or IsStatic = True Then

                                        If l_FTPServer = "" Then Continue For

4110:                                   If HTMLPath.Length = 0 Then

4120:                                       Ok = oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, _
                                                 l_FTPPassword, l_FTPPath, _
                                                 sExport, l_FTPType, l_FtpOptions, , , l_FTPPassive)

4130:                                       If Ok = False Then
4140:                                           Throw New Exception(Me._CreateErrorString)
                                            End If
4150:                                   Else
4160:                                       For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
4170:                                           sFtp &= s & "|"
4180:                                       Next

4190:                                       Ok = oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, _
                                                 l_FTPPassword, l_FTPPath, _
                                                 sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)

4200:                                       If Ok = False Then
4210:                                           Throw New Exception(Me._CreateErrorString)
                                            End If
4220:                                   End If
4230:                               Else
4240:                                   sDynamic = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

4250:                                   If sDynamic Is Nothing Then
4260:                                       Throw New Exception(Me._CreateErrorString)
4270:                                   End If

4280:                                   l_FTPUser = Me._CheckForDynamic(IsDynamic, l_FTPUser, nReportID, sKeyParameter, sKeyValue)
4290:                                   l_FTPPassword = Me._CheckForDynamic(IsDynamic, l_FTPPassword, nReportID, sKeyParameter, sKeyValue)
4300:                                   l_FTPPath = Me._CheckForDynamic(IsDynamic, l_FTPPath, nReportID, sKeyParameter, sKeyValue)

4310:                                   For Each s As String In sDynamic

4320:                                       If HTMLPath.Length = 0 Then

4330:                                           Ok = oFtp.FTPUpload2(s, 21, l_FTPUser, _
                                                     l_FTPPassword, l_FTPPath, _
                                                     sExport, l_FTPType, l_FtpOptions, , , l_FTPPassive)

4340:                                           If Ok = False Then
4350:                                               Throw New Exception(Me._CreateErrorString)
                                                End If
4360:                                       Else
4370:                                           For Each n As String In System.IO.Directory.GetFiles(HTMLPath)
4380:                                               sFtp &= n & "|"
4390:                                           Next

4400:                                           Ok = oFtp.FTPUpload2(s, 21, l_FTPUser, _
                                                     l_FTPPassword, l_FTPPath, _
                                                     sFtp, l_FTPType, l_FtpOptions, , , l_FTPPassive)

4410:                                           If Ok = False Then
4420:                                               Throw New Exception(Me._CreateErrorString)
                                                End If
4430:                                       End If
4440:                                   Next
                                    End If
4450:                           Next
4460:                           Ok = True
4470:                       Case "fax"
                                Dim faxNumber As String
                                Dim faxDevice As String
                                Dim faxTo As String
                                Dim faxFrom As String
                                Dim faxComments As String
                                Dim faxer As clsMarsMessaging = New clsMarsMessaging

4480:                           faxDevice = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)

4490:                           faxTo = oParse.ParseString(oRs("cc").Value, , , , , , m_ParametersTable)

4500:                           faxFrom = oParse.ParseString(oRs("bcc").Value, , , , , , m_ParametersTable)
4510:                           faxComments = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

4520:                           If IsDynamic = False Or IsStatic = True Then
4530:                               If customFax.Length = 0 Then
4540:                                   faxNumber = oRs("sendto").Value
4550:                               Else
4560:                                   faxNumber = customFax
                                    End If

4570:                               Ok = faxer.SendFax(faxNumber, faxDevice, "Single", faxTo, faxFrom, faxComments, sExport)
4580:                           Else
4590:                               sDynamic = oDynamicData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

4600:                               If sDynamic Is Nothing Then
4610:                                   Throw New Exception(Me._CreateErrorString)
4620:                               ElseIf sDynamic(0) = "" Then
4630:                                   gErrorDesc = "The Key Parameter '" & sKeyValue & "' did not return a valid Fax number"
4640:                                   gErrorNumber = -29082005
4650:                                   gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
4660:                                   gErrorLine = 2964
4670:                                   gErrorSuggest = "Make sure the field contains a valid fax number"

4680:                                   Throw New Exception(Me._CreateErrorString)
4690:                               End If

4700:                               For Each s As String In sDynamic
4710:                                   If s.Length > 0 Then
4720:                                       faxNumber &= s & ";"
4730:                                   End If
4740:                               Next

4750:                               faxDevice = Me._CheckForDynamic(IsDynamic, faxDevice, nReportID, sKeyParameter, sKeyValue)
4760:                               faxTo = Me._CheckForDynamic(IsDynamic, faxTo, nReportID, sKeyParameter, sKeyValue)
4770:                               faxFrom = Me._CheckForDynamic(IsDynamic, faxFrom, nReportID, sKeyParameter, sKeyValue)
4780:                               faxComments = Me._CheckForDynamic(IsDynamic, faxComments, nReportID, sKeyParameter, sKeyValue)

4790:                               Ok = faxer.SendFax(faxNumber, faxDevice, "Single", faxTo, faxFrom, faxComments, sExport)
                                End If
4800:                       Case "sms"
4810:                           Dim cellNumber As String
                                Dim textMsg As String

4820:                           textMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

4830:                           If IsDynamic = False Or IsStatic = True Then

4840:                               If customSMS.Length = 0 Then
4850:                                   cellNumber = oParse.ParseString(oRs("sendto").Value, , , , , , m_ParametersTable)
4860:                               Else
4870:                                   cellNumber = customSMS
                                    End If

4880:                               Ok = SendSMS(cellNumber, textMsg, sExport)
4890:                           Else
4900:                               sDynamic = oDynamicData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

4910:                               If sDynamic Is Nothing Then
4920:                                   Throw New Exception(Me._CreateErrorString)
                                    End If

4930:                               For Each s As String In sDynamic
4940:                                   cellNumber &= s & ";"
4950:                               Next

4960:                               textMsg = Me._CheckForDynamic(IsDynamic, textMsg, nReportID, sKeyParameter, sKeyValue)

4970:                               Ok = SendSMS(cellNumber, textMsg, sExport)
                                End If
                            Case "sharepoint"
                                Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint

                                Dim spServer As String
                                Dim spUser As String
                                Dim spPassword As String
                                Dim spLib As String
                                Dim spCount As Integer = 0
                                Dim spmetaData As String = ""

                                spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                                spUser = oParse.ParseString(oRs("ftpusername").Value)
                                spPassword = oParse.ParseString(oRs("ftppassword").Value)
                                spLib = oParse.ParseString(oRs("ftppath").Value)
                                spmetaData = oParse.ParseString(IsNull(oRs("ftpoptions").Value))


                                oUI.BusyProgress(75, "Uploading report...")

                                spCount = spServer.Split("|").GetUpperBound(0)

                                If spCount > 0 Then
                                    spCount -= 1
                                Else
                                    spCount = 0
                                End If

                                For I As Integer = 0 To spCount
                                    Dim l_spServer As String = spServer.Split("|")(I)
                                    Dim l_spUser As String = spUser.Split("|")(I)
                                    Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(I))
                                    Dim l_spLib As String = spLib.Split("|")(I)
                                    Dim errorInfo As Exception = Nothing
                                    Dim l_metaData As String = ""
                                    If l_spServer = "" Then Continue For

                                    Try
                                        l_metaData = spmetaData.Split("|")(I)
                                    Catch : End Try

                                    If IsDynamic = False Or IsStatic = True Then

                                        If l_spServer = "" Then Continue For

                                        If HTMLPath.Length = 0 Then
                                            Ok = sp.UploadDocument(l_spServer, l_spLib, sExport, l_spUser, l_spPassword, errorInfo, l_metaData)

                                            If Ok = False And errorInfo IsNot Nothing Then
                                                Throw errorInfo
                                            End If
                                        Else
                                            Dim tempFiles As String()
                                            Dim n As Integer = 0

                                            For Each s As String In System.IO.Directory.GetFiles(HTMLPath)
                                                ReDim Preserve tempFiles(n)

                                                tempFiles(n) = s

                                                n += 1
                                            Next

                                            Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, tempFiles, l_spUser, l_spPassword, errorInfo)

                                            If Ok = False And errorInfo IsNot Nothing Then
                                                Throw errorInfo
                                            End If
                                        End If
                                    Else
                                        sDynamic = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

                                        If sDynamic Is Nothing Then
                                            Throw New Exception(Me._CreateErrorString)
                                        End If

                                        l_spUser = Me._CheckForDynamic(IsDynamic, l_spUser, nReportID, sKeyParameter, sKeyValue)
                                        l_spPassword = Me._CheckForDynamic(IsDynamic, l_spPassword, nReportID, sKeyParameter, sKeyValue)
                                        l_spLib = Me._CheckForDynamic(IsDynamic, l_spLib, nReportID, sKeyParameter, sKeyValue)

                                        For Each s As String In sDynamic
                                            If HTMLPath.Length = 0 Then

                                                Ok = sp.UploadDocument(l_spServer, l_spLib, sExport, l_spUser, l_spPassword, errorInfo, l_metaData)

                                                If Ok = False And errorInfo IsNot Nothing Then
                                                    Throw errorInfo
                                                End If
                                            Else
                                                Dim tmpFiles As String()
                                                Dim n As Integer = 0

                                                For Each t As String In System.IO.Directory.GetFiles(HTMLPath)
                                                    ReDim Preserve tmpFiles(n)

                                                    tmpFiles(n) = t

                                                    n += 1
                                                Next

                                                For Each d As String In IO.Directory.GetDirectories(HTMLPath)
                                                    For Each f As String In IO.Directory.GetFiles(d)
                                                        ReDim Preserve tmpFiles(n)

                                                        tmpFiles(n) = f

                                                        n += 1
                                                    Next
                                                Next

                                                Ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, tmpFiles, l_spUser, l_spPassword, errorInfo)

                                                If Ok = False And errorInfo IsNot Nothing Then
                                                    Throw errorInfo
                                                End If
                                            End If
                                        Next
                                    End If

                                Next
                                Ok = True
                            Case "dropbox"
                                Dim db As clsDropboxDestination = New clsDropboxDestination(nDestinationID)
                                Dim files As System.Collections.Generic.List(Of String) = New System.Collections.Generic.List(Of String)

                                If IsDynamic = False Or IsStatic = True Then
                                    If HTMLPath.Length = 0 Then
                                        files.Add(sExport)
                                    Else
                                        For Each t As String In System.IO.Directory.GetFiles(HTMLPath)
                                            files.Add(t)
                                        Next

                                        For Each d As String In IO.Directory.GetDirectories(HTMLPath)
                                            For Each f As String In IO.Directory.GetFiles(d)
                                                files.Add(f)
                                            Next
                                        Next
                                    End If

                                    Dim errInfo As Exception

                                    Ok = db.putFiles(errInfo, files)

                                    If Ok = False Then
                                        Throw errInfo
                                    End If
                                Else
                                    sDynamic = oDynamcData._GetDynamicData(nReportID, sKeyParameter, sKeyValue)

                                    If sDynamic Is Nothing Then
                                        Throw New Exception(Me._CreateErrorString)
                                    End If

                                    For Each s As String In sDynamic
                                        files = Nothing
                                        files = New System.Collections.Generic.List(Of String)

                                        If HTMLPath.Length = 0 Then
                                            files.Add(sExport)
                                        Else
                                            For Each t As String In System.IO.Directory.GetFiles(HTMLPath)
                                                files.Add(t)
                                            Next

                                            For Each d As String In IO.Directory.GetDirectories(HTMLPath)
                                                For Each f As String In IO.Directory.GetFiles(d)
                                                    files.Add(f)
                                                Next
                                            Next
                                        End If

                                        Dim errInfo As Exception

                                        Ok = db.putFiles(errInfo, files, s)

                                        If Ok = False Then
                                            Throw errInfo
                                        End If
                                    Next

                                End If
                        End Select
4980:               End If

4990:           Catch ex As Exception
5000:               If IsDynamic = False Then
5010:                   Dim sFullError As String

5020:                   sFullError = "Destination Error: " & sDestName & vbCrLf & vbCrLf & "+++++++++++++++++++++++" & vbCrLf & ex.Message & vbCrLf & _
                             "+++++++++++++++++++++++"

5030:                   _ErrorHandle(sFullError, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)

5040:                   gErrorCollection &= sFullError & vbCrLf & vbCrLf

5050:                   Ok = False
                    End If
                End Try
NextDest:
5060:           oRs.MoveNext()
5070:       Loop

5080:       oRs.Close()

5090:       If Ok = True And isDefer = False Then
5100:           If (IsDynamic = False) Or (IsDynamic = True And RunTasks = True) Then
5110:               oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)
                End If
            End If

5120:       oRs = Nothing

5130:       oUI.BusyProgress(95, "Cleaning up...")

5140:       oUI.BusyProgress(, , True)

            Dim nClose As Integer = 0

5150:       Try
                'oApp.canclose = True

5160:           System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oRpt)

5170:           If IsDynamic = True Then GC.Collect()
            Catch : End Try

            Try
                Dim reportsProduced As Integer = outputTracker.calculateOutput(1, destinationCount, 1)
                outputTracker.addOutput(reportsProduced)
            Catch : End Try

5180:       Return Ok

5190:   Catch ex As Exception
5200:       Dim sError As String
            Dim nClose As Integer = 0

5210:       Do
5220:           Application.DoEvents()

5230:           nClose += 1
5240:           If nClose >= 10 Then
5250:               Exit Do
                End If
5260:       Loop Until oApp.CanClose = True

5270:       oRpt = Nothing
5280:       sError = "Single Schedule Error: " & sReportName & vbCrLf & ex.Message
5290:       gErrorDesc = sError
5300:       gErrorNumber = Err.Number
5310:       gErrorSource = "clsMarsReport._RunSingleSchedule"

5320:       If ex.Message.ToLower.IndexOf("invalid tlv record") > -1 Then
                gErrorSuggest = "The error is caused by one of the following: " & vbCrLf & _
           "1. The report definition is corrupt in the rpt file" & vbCrLf & _
           "2. The report was written with a Crystal version below version 8.5" & vbCrLf & _
           "3. You are trying to open a report created in a version above 8.5 using Crystal 8.5 runtimes"
5330:       ElseIf Err.Number = 9 Then
                gErrorDesc = "Single Schedule Error: " & sReportName & Environment.NewLine & "Login failed. Please " & _
           "check the provided username and password for the report."

5340:           gErrorSuggest = "Please ensure that the username, password, server name and database name you have " & _
     "entered are correct.  If you are using ODBC DSN in your report to connect to the " & _
     "database, make sure that the DSN named in the report exsists on your PC and " & _
     "that it is set up as a SYSTEM DSN"

5350:           gErrorSuggest &= vbCrLf & vbCrLf & "Sometimes, the report itself may become " & _
     "corrupt and cause this error. To resolve this try the following:" & vbCrLf & vbCrLf & _
     "1.  Open the report in Crystal, log off the server, save, log on again, and save. " & _
     "Refresh the schedule and test." & vbCrLf & _
     "2.  Open the report in Crystal, reselect location and save.  " & _
     "Refresh the schedule and test." & vbCrLf & _
     "3.  Create the report completely afresh.  Write a new schedule for the new report."
5360:       ElseIf Err.Number = 5 Then
5370:           gErrorSuggest = "The generated path '" & sTemp & "' has exceeded the maximum path length " & _
     "allowed by Windows XP. Please revise your directory structure."
            End If

5380:       gErrorLine = Erl()
5390:       oUI.BusyProgress(, , True)
5400:       Return False
5410:   Finally
5420:       gScheduleName = String.Empty
            gScheduleOwner = String.Empty
5430:       oRpt = Nothing

5440:       HTMLPath = Nothing

5450:       Me.m_ParametersTable = Nothing
5460:   End Try
    End Function


    Public Function SetReportParameters(ByVal nReportID As Integer) As ReportServer.ParameterValue()
10:     Try
            Dim SQL As String
            Dim ParName As String = ""
            Dim ParValue As String = ""
            Dim parNull As Boolean = False
            Dim ParType As Integer
            Dim sQuotes As String = String.Empty
20:         Dim oParse As New clsMarsParser
30:         Dim oDynamic As New clsMarsDynamic
            Dim I As Integer = 0
            Dim oPar As ReportServer.ParameterValue()
            Dim Z As Integer
            Dim reportParameter As Microsoft.Reporting.WinForms.ReportParameter()

40:         SQL = "SELECT * FROM ReportParameter WHERE ReportID = " & nReportID & " ORDER BY ParID DESC"

            SaveTextToFile(Date.Now & ": Starting set parameters for reportid " & nReportID, sAppPath & "sqlrd_parameters.debug", , False, False)

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

50:         Me.m_ParametersTable = New Hashtable

60:         If oRs Is Nothing Then Return Nothing

70:         If oRs.EOF = True Then Return Nothing

80:         Do While oRs.EOF = False
90:             ParName = oRs("parname").Value
100:            ParValue = oRs("parvalue").Value

                SaveTextToFile(Date.Now & ": Parameter Name - " & ParName, sAppPath & "sqlrd_parameters.debug", , True, True)

110:            If ParValue.ToLower = "[sql-rddefault]" Then GoTo Skip

120:            ReDim Preserve oPar(I)
130:            ReDim Preserve reportParameter(I)

140:            oPar(I) = New ReportServer.ParameterValue
150:            reportParameter(I) = New Microsoft.Reporting.WinForms.ReportParameter

                SaveTextToFile(Date.Now & ": Key Parameter Name (if applicable) - " & sKeyParameter, sAppPath & "sqlrd_parameters.debug", , True, True)

160:            If String.Compare(ParName.Trim, sKeyParameter.Trim, True) = 0 Then
                    SaveTextToFile(Date.Now & ": Parameter matched with Key parameter, values will be replace with " & sKeyValue, sAppPath & "sqlrd_parameters.debug", , True, True)

170:                ParValue = sKeyValue
180:            Else
                    SaveTextToFile(Date.Now & ": Parameter not matched with Key parameter." & sKeyValue, sAppPath & "sqlrd_parameters.debug", , True, True)
                End If


190:            ParValue = oParse.ParseString(ParValue, , , , , , m_ParametersTable)

200:            Try
210:                If ParValue.IndexOf("<[x]") > -1 Then
220:                    ParValue = oDynamic._CreateDynamicString(ParValue, nReportID, _
      sKeyParameter, sKeyValue)
                    End If
                Catch : End Try

                'check provided command lines
230:            Try
240:                If Not gParNames Is Nothing Then
250:                    For Z = 0 To gParNames.GetUpperBound(0)
260:                        If gParNames(Z).ToLower = ParName.ToLower Then
270:                            ParValue = gParValues(Z)
                            End If
280:                    Next
                    End If
                Catch : End Try

290:            If gRunByEvent = True Then
300:                SQL = "SELECT * FROM EventSubAttr WHERE EventID =" & clsMarsEvent.currentEventID & " AND " & _
      "ReportID =" & nReportID & " AND  ParName = '" & SQLPrepare(ParName) & "'"

                    Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

310:                If Not oRs1 Is Nothing Then
320:                    If oRs1.EOF = False Then
330:                        Try
340:                            ParValue = clsMarsParser.Parser.ParseString(oRs1("parvalue").Value, , , , , , m_ParametersTable)
350:                        Catch ex As Exception
360:                            ParValue = String.Empty
                            End Try
                        End If

370:                    oRs1.Close()
                    End If
                End If

                SaveTextToFile(Date.Now & ": Parameter Value - " & ParValue, sAppPath & "sqlrd_parameters.debug", , True, True)

380:            Try
390:                parNull = Convert.ToBoolean(oRs("parnull").Value)
400:            Catch
410:                parNull = False
                End Try

420:            If ParValue IsNot Nothing Then
430:                If ParValue.ToLower = "[sql-rdnull]" Then
440:                    ParValue = Nothing
450:                ElseIf ParValue.ToLower.Contains("[selectall]") Then
                        Dim values As ArrayList = getParameterValidValues(ParName, nReportID)
460:                    ParValue = ""

470:                    For Each s As String In values
480:                        reportParameter(I).Values.Add(s)
490:                        ParValue &= s & "|"
500:                    Next
                    End If
                End If

510:            With oPar(I)
520:                .Name = ParName
530:                .Value = ParValue
                End With

540:            reportParameter(I).Name = ParName

                SaveTextToFile(Date.Now & ": Adding value to report object", sAppPath & "sqlrd_parameters.debug", , True, True)


550:            If ParValue IsNot Nothing Then
560:                For Each s As String In ParValue.Split("|")
570:                    If s.Length > 0 Then
580:                        reportParameter(I).Values.Add(s)
                        End If
590:                Next
600:            Else
610:                reportParameter(I).Values.Add(ParValue)
                End If


                '//set the value...
620:            Try
630:                '  If m_rv IsNot Nothing And reportParameter IsNot Nothing Then m_rv.ServerReport.SetParameters(reportParameter)
                Catch : End Try

640:            I += 1
Skip:
650:            Me.m_ParametersTable.Add(ParName, ParValue)

660:            oRs.MoveNext()
670:        Loop

680:        oRs.Close()

690:        If m_rv Is Nothing Then
                SaveTextToFile(Date.Now & ": Warning RV IS NOTHING! Parameters will not be set.", sAppPath & "sqlrd_parameters.debug", , True, True)
            End If

700:        Try
710:            If m_rv IsNot Nothing And reportParameter IsNot Nothing Then m_rv.ServerReport.SetParameters(reportParameter)
720:        Catch ex As Exception
                'Throw ex
                SaveTextToFile(Date.Now & ": Error setting parameters, might be SSRS 2000..." & ex.Message, sAppPath & "sqlrd_parameters.debug", , True, True)
            End Try

730:        Return oPar
740:    Catch ex As Exception
            SaveTextToFile(Date.Now & ": Error setting parameters (" & Erl() & ") : " & ex.Message, sAppPath & "sqlrd_parameters.debug", , True, True)
        End Try

    End Function

    Private Function createReportInstance(ByVal serverUrl As Uri, ByVal cred As System.Net.ICredentials, ByVal reportPath As String, Optional serveruser As String = "", Optional serverPassword As String = "", Optional formsAuth As Boolean = False) As Microsoft.Reporting.WinForms.ReportViewer
        'set up the report viewer with the report we are accessing
        Dim tmp As Microsoft.Reporting.WinForms.ReportViewer

        tmp = New Microsoft.Reporting.WinForms.ReportViewer
        tmp.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote
        tmp.ServerReport.ReportServerUrl = serverUrl
        tmp.ServerReport.ReportServerCredentials.NetworkCredentials = cred

        '//forms auth
        Try
            If formsAuth Then tmp.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, serveruser, serverPassword, Nothing)
        Catch : End Try

        tmp.ServerReport.ReportPath = reportPath

        Return tmp
    End Function

    Public Function getParameterValidValues(ByVal rv As Microsoft.Reporting.WinForms.ReportViewer, ByVal parameterName As String, ByVal parameters As Hashtable, formsAuth As Boolean)

        Dim parIndex As Integer
        Dim pValues As IList
        Dim p As ReportParameterInfo
        Dim availableValues As ArrayList = New ArrayList

        'get the parameter index
        If rv IsNot Nothing Then
            Dim tmp As Microsoft.Reporting.WinForms.ReportViewer = createReportInstance(rv.ServerReport.ReportServerUrl, rv.ServerReport.ReportServerCredentials.NetworkCredentials, rv.ServerReport.ReportPath, , , formsAuth)
            Dim ps As ReportParameterInfoCollection = tmp.ServerReport.GetParameters() 'get the parameters collection from the serverreport
            Dim reportPars() As ReportParameter = Nothing
            Dim I As Integer = 0

            For Each p In ps
                Try
                    Dim key As String = p.Name

                    If parameters.ContainsKey(key) Then
                        ReDim Preserve reportPars(I)

                        Dim parValue As String = parameters.Item(key)

                        reportPars(I) = New ReportParameter()
                        reportPars(I).Name = key
                        reportPars(I).Values.Clear()

                        If parValue.ToString.Contains("|") Then
                            For Each s As String In parValue.ToString.Split("|")
                                If s <> "" Then reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(s))
                            Next
                        Else
                            reportPars(I).Values.Add(clsMarsParser.Parser.ParseString(parValue))
                        End If

                        I += 1

                        tmp.ServerReport.SetParameters(reportPars)
                    End If

                Catch : End Try
            Next

            ps = tmp.ServerReport.GetParameters

            For Each p In ps 'loop through the parameters

                If p.Name = parameterName Then
                    pValues = p.ValidValues

                    If p.ValidValues IsNot Nothing Then
                        For Each val As ValidValue In pValues
                            availableValues.Add(val.Value)
                        Next
                    End If

                    Exit For
                End If
            Next

            tmp.Dispose()
            tmp = Nothing

        End If

        Return availableValues
    End Function
    Private Function getParameterValidValues(ByVal name As String, Optional reportID As Integer = 0)

        Dim parIndex As Integer
        Dim pValues As IList
        Dim p As ReportParameterInfo
        Dim availableValues As ArrayList = New ArrayList
        'clear the list of items

        Dim serveruser, serverpassword As String
        Dim formsAuth As Boolean = False

        serveruser = ""
        serverpassword = ""

        If reportID > 0 Then
            Dim rpt As cssreport = New cssreport(reportID)

            serveruser = rpt.reportuserid
            serverpassword = rpt.reportuserpassword
            formsAuth = rpt.usesFormsAuth
        End If

        'get the parameter index
        If m_rv IsNot Nothing Then
            Dim tmp As Microsoft.Reporting.WinForms.ReportViewer = createReportInstance(m_rv.ServerReport.ReportServerUrl, m_rv.ServerReport.ReportServerCredentials.NetworkCredentials, _
                                                                                         m_rv.ServerReport.ReportPath, serveruser, serverpassword, formsAuth)

            Dim ps As ReportParameterInfoCollection = tmp.ServerReport.GetParameters() 'get the parameters collection from the serverreport
            Dim reportPars() As ReportParameter = Nothing
            Dim I As Integer = 0

            For Each p In ps
                Try
                    Dim key As String = p.Name

                    If m_ParametersTable.ContainsKey(key) Then
                        ReDim Preserve reportPars(I)

                        Dim parValue As String = m_ParametersTable.Item(key)

                        reportPars(I) = New ReportParameter()
                        reportPars(I).Name = key
                        reportPars(I).Values.Clear()

                        If parValue.ToString.Contains("|") Then
                            For Each s As String In parValue.ToString.Split("|")
                                If s <> "" Then reportPars(I).Values.Add(s)
                            Next
                        Else
                            reportPars(I).Values.Add(parValue)
                        End If

                        I += 1

                        tmp.ServerReport.SetParameters(reportPars)
                    End If


                Catch : End Try


            Next

            ps = tmp.ServerReport.GetParameters

            For Each p In ps 'loop through the parameters

                If p.Name = name Then
                    pValues = p.ValidValues

                    If p.ValidValues IsNot Nothing Then
                        For Each val As ValidValue In pValues
                            availableValues.Add(val.Value)
                        Next
                    End If

                    Exit For
                End If
            Next

            tmp.Dispose()
            tmp = Nothing

        End If


        Return availableValues
    End Function
    Public Function _PostProcessPDF(ByVal sFullPath As String, ByVal InfoTitle As String, _
           ByVal InfoAuthor As String, ByVal InfoSubject As String, _
           ByVal InfoKeywords As String, ByVal InfoProducer As String, ByVal InfoCreated As Date, _
           ByVal IsDynamic As Boolean, ByVal nReportID As Integer, ByVal oRpt As Object, _
           ByVal PDFSecurity As Boolean, ByVal PackageMergePDF As Boolean, _
           ByVal CanPrint As Integer, ByVal CanCopy As Integer, ByVal CanEdit As Integer, _
           ByVal Cannotes As Integer, ByVal CanFill As Integer, ByVal CanAccess As Integer, _
           ByVal CanAssemble As Integer, _
           ByVal Canprintfull As Integer, ByVal PDFPassword As String, ByVal UserPassword As String, _
           ByVal sWatermark As String, ByVal DestinationID As Integer, pdfExires As Boolean, pdfExpiryDate As Date) As String
10:     Try
20:         Dim oParse As New clsMarsParser
30:         Dim oPerm As New clsMarsPDF
40:         Dim oDynamcData As New clsMarsDynamic



50:         Try
60:             If PackageMergePDF = False Then
70:                 If IsDynamic = True And InfoTitle.IndexOf("<[x]") > -1 Then
80:                     InfoTitle = oDynamcData. _
       _CreateDynamicString(InfoTitle, _
       nReportID, sKeyParameter, sKeyValue)
                    End If

90:                 If IsDynamic = True And InfoAuthor.IndexOf("<[x]") > -1 Then
100:                    InfoAuthor = oDynamcData. _
      _CreateDynamicString(InfoAuthor, _
      nReportID, sKeyParameter, sKeyValue)
                    End If

110:                If IsDynamic = True And InfoSubject.IndexOf("<[x]") > -1 Then
120:                    InfoSubject = oDynamcData. _
      _CreateDynamicString(InfoSubject, _
      nReportID, sKeyParameter, sKeyValue)
                    End If

130:                If IsDynamic = True And InfoKeywords.IndexOf("<[x]") > -1 Then
140:                    InfoKeywords = oDynamcData. _
      _CreateDynamicString(InfoKeywords, _
      nReportID, sKeyParameter, sKeyValue)
                    End If

150:                If IsDynamic = True And InfoProducer.IndexOf("<[x]") > -1 Then
160:                    InfoProducer = oDynamcData. _
      _CreateDynamicString(InfoProducer, _
      nReportID, sKeyParameter, sKeyValue)
                    End If



170:                oPerm.SetPDFSummary(InfoTitle, InfoAuthor, _
      InfoSubject, InfoKeywords, InfoCreated, InfoProducer _
      , sFullPath, pdfExires, pdfExpiryDate)


                End If

            Catch : End Try

180:        sWatermark = oParse.ParseString(sWatermark)



190:        If sWatermark.IndexOf("<[x]") > -1 Then
200:            sWatermark = oDynamcData._CreateDynamicString(sWatermark, nReportID, sKeyParameter, sKeyValue)
            End If



210:        Try
220:            If sWatermark.Length > 0 Then oPerm.AddWatermark(sFullPath, sWatermark, IIf(DestinationID > 0, DestinationID, nReportID))
            Catch : End Try

230:        If PDFSecurity = True And PackageMergePDF = False Then

                Dim nPerm As Integer
                Dim sOwnerPass As String
                Dim sUserPass As String

                'nPerm = oPerm._CreatePermissions(CanPrint, CanCopy, CanEdit, _
                'Cannotes, CanFill, CanAccess, CanAssemble, _
                'Canprintfull)

240:            sOwnerPass = oParse.ParseString(_DecryptDBValue(PDFPassword))
250:            sUserPass = oParse.ParseString(_DecryptDBValue(UserPassword))

260:            '       sOwnerPass = _EncryptDBValue(sOwnerPass)
270:            '   sUserPass = _EncryptDBValue(sUserPass)

280:            If IsDynamic = True And sOwnerPass.IndexOf("<[x]") > -1 Then
290:                sOwnerPass = oDynamcData. _
      _CreateDynamicString(sOwnerPass, _
      nReportID, sKeyParameter, sKeyValue)
                End If

300:            If IsDynamic = True And sUserPass.IndexOf("<[x]") > -1 Then
310:                sUserPass = oDynamcData. _
      _CreateDynamicString(sUserPass, _
      nReportID, sKeyParameter, sKeyValue)
                End If

                sOwnerPass = _EncryptDBValue(sOwnerPass)
                sUserPass = _EncryptDBValue(sUserPass)

                Try
320:                oPerm.SetPDFPermissions(sFullPath, CanPrint, CanCopy, CanEdit, Cannotes, CanFill, CanAccess, CanAssemble, _
          Canprintfull, sOwnerPass, sUserPass)
                Catch : End Try
                'sFullPath = oPerm._EncryptPDF(sFullPath, oParse._ParseString(sOwnerPass), _
                'oParse._ParseString(sUserPass), nPerm)
            End If
330:    Catch ex As Exception
340:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try

350:    Return sFullPath
    End Function

    Private Function createFileFromServer(ByVal outputBytes As Byte(), ByVal sOutput As String)


        'to avoid a resource issue, we need to write the file in 1024 bit chunks
        Dim chunkSize As Long = clsMarsUI.MainUI.ReadRegistry("ChunkSizeBytes", 10485760)

10:     If outputBytes.LongLength > chunkSize Then 'if its greater than 10MB
            clsMarsDebug.writeToDebug("export.debug", "Creating file in " & chunkSize & " byte chunks", True)

20:         Try
                'get the entire byte array
                Dim fstream As IO.FileStream = System.IO.File.Create(sOutput, outputBytes.LongLength)
                Dim byteLength As Integer = outputBytes.LongLength
                'the chunk size we will be writing
                Dim n As Integer = chunkSize 'default 10mb
                Dim I As Integer = 0
                'number of times we will do the writing
                Dim lController As Integer = (byteLength / n) - 1

                'if there is any left thats less than 1024
                Dim final As Integer = byteLength - (lController * n)


                'start writing the chunks
30:             For I = 0 To lController - 1
40:                 fstream.Write(outputBytes, (I * n), n)

50:                 If RunEditor = True Then clsMarsUI.BusyProgress((I / lController) * 100, "Writing to file...", False)

                    clsMarsDebug.writeToDebug("export.debug", "Writing to file (" & I * n & ")", True)
60:             Next

                'if there is any left over then write that out
                clsMarsDebug.writeToDebug("export.debug", "Writing remaining bytes", True)
70:             If final > 0 Then fstream.Write(outputBytes, (byteLength - final), final)
71:
                'close and dispose the file stream
                Try
                    clsMarsDebug.writeToDebug("export.debug", "Closing file stream", True)
80:                 fstream.Close()
                Catch : End Try

                Try
                    clsMarsDebug.writeToDebug("export.debug", "Disposing off file stream", True)
                    fstream.Dispose()
                    System.GC.Collect()
                Catch : End Try
90:
100:        Catch ex As Exception
110:            _ErrorHandle("Error writing byte error in chunks. SQL-RD will now attempt to use write-once method. " & vbCrLf & ex.Message & vbCrLf & Erl(), Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
120:            Dim fstream As IO.FileStream = System.IO.File.Create(sOutput, outputBytes.Length)

130:            Using fstream
140:                fstream.Write(outputBytes, 0, outputBytes.LongLength)
150:                fstream.Close()
160:                fstream.Dispose()
                End Using
            End Try
170:    Else
            clsMarsDebug.writeToDebug("export.debug", "Creating file using write-once method", True)

180:        With System.IO.File.Create(sOutput, outputBytes.LongLength)
190:            .Write(outputBytes, 0, outputBytes.LongLength)
200:            .Close()
210:            .Dispose()
            End With
        End If

        ' clsMarsUI.BusyProgress(100, "Writing to file...", False)
    End Function

    Private Sub renderfor20000()

    End Sub
    Private Overloads Function ReportServerRender(ByVal oRpt As ReportServer.ReportingService, ByVal sFormat As String, _
           ByVal Parameters() As ReportServer.ParameterValue, ByVal Credentials() As ReportServer.DataSourceCredentials, _
           ByVal sReportPath As String, ByVal sOutput As String, Optional ByRef oStream() As String = Nothing, _
           Optional ByVal timeout As Int64 = 3600000, Optional ByVal deviceInfo As String = "", Optional ByVal reportName As String = "") As Boolean

        Dim sWarnings As String = ""

10:     clsMarsDebug.writeToDebug("export.debug", "-----------------------------------------", True)
20:     clsMarsDebug.writeToDebug("export.debug", "Starting Export process for " & reportName, True)

        '   m_ServerVersion2005 = True

30:     If m_ServerVersion2005 = False Then
40:         clsMarsDebug.writeToDebug("export.debug", "Server version is pre 2005", True)

50:         Try
60:             Dim Warnings() As ReportServer.Warning
70:             Dim oReport() As Byte

80:             clsMarsUI.MainUI.BusyProgress(80, "Server rendering report...")

90:             oRpt.Timeout = -1 'timeout

100:            clsMarsDebug.writeToDebug("export.debug", "Server rendering report to byte stream", True)

                Dim doretry As Boolean = True

retrypoint:

                Try
110:                oReport = oRpt.Render(sReportPath, sFormat, Nothing, deviceInfo, _
                          Parameters, Credentials, Nothing, "", "", Nothing, _
                          Warnings, oStream)
                Catch e As Exception
                    If e.Message.ToLower.Contains("timeout") And doretry = True Then
                        doretry = False
                        System.Threading.Thread.Sleep(5000)
                        GoTo retrypoint
                    Else
                        Throw e
                    End If
                End Try

120:            If Not Warnings Is Nothing Then
130:                For Each Warning As ReportServer.Warning In Warnings
140:                    sWarnings = Warning.Message & " :" & Warning.Severity & vbCrLf
150:                Next
160:            End If

170:            clsMarsUI.MainUI.BusyProgress(90, "Creating report file...")


180:            clsMarsDebug.writeToDebug("export.debug", "Converting byte stream to file", True)

                createFileFromServer(oReport, sOutput)


                clsMarsDebug.writeToDebug("export.debug", "File created!", True)

                clsMarsUI.MainUI.BusyProgress(90, "Report file created...")

230:            Return True
240:        Catch ex As Exception

                clsMarsDebug.writeToDebug("export.debug", "Error: " & ex.ToString & vbCrLf & Erl(), True)

                Dim sMsg As String = ""

250:            If ex.Message.ToLower.IndexOf("client found response content type of 'text/html; charset=utf-8', but expected 'text/xml'") > -1 Then
260:                sMsg = "The web service you are attempting to access on this web server is currently unavailable. Please make sure that IIS is running."
270:            ElseIf ex.Message.ToLower.IndexOf("the request failed with http status 405") > -1 Then
280:                sMsg = "SQL-RD connects to, and consumes, the web service on the SSRS server." & _
                          "The problem is likely to be as a result of problems with ASP and IIS on the SSRS server." & _
                          "This could indicate a disconnect between IIS and asp.net, you could attempt to " & _
                          "re-associate the SSRS virtual directory with asp.net by following the steps below:" & vbCrLf & _
                          "-open a command prompt - click start->run->type in 'cmd'" & vbCrLf & _
                          "-browse to c:\winnt\microsoft.net\framework\v1.1.4322\ (winnt folder may be windows)" & vbCrLf & _
                          "-run the command 'aspnet_regiis -i'"
290:            ElseIf ex.Message.ToLower.IndexOf("timeout") > -1 Then
                    sMsg = "Please increase the server timeout in SQL Server Reports Manager (http://myReportServer/Reports/Pages/Settings.aspx)"
300:            ElseIf ex.Message.ToLower.IndexOf("default value or value provided for the report parameter") > -1 Then
310:                sMsg = "Please make sure that all the parameter fields have valid values. If you are passing 'Null' to the parameter, make sure that " & _
                          "the parameter field has the 'allow null value' set and that it DOES NOT have any queried or non-queried available values."
320:            ElseIf ex.Message.Contains("rsReportParameterValueNotSet") Then
330:                sMsg = "Please make sure that the provided value for the parameter matches the available values in the report. Also take note that the report parameters in SSRS are case-senstive"
                End If


340:            gErrorDesc = gScheduleName & " (" & reportName & ") : " & ex.Message & vbCrLf & sWarnings
350:            gErrorNumber = Err.Number
360:            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
370:            gErrorLine = Erl()
380:            gErrorSuggest = sMsg

                _ErrorHandle(gErrorDesc, Err.Number, gErrorSource, Erl, sMsg, , True)

390:            Return False

400:        End Try
410:    Else
420:        clsMarsDebug.writeToDebug("export.debug", "Server version is post 2000", True)

430:        Try
                Dim Warnings() As Microsoft.Reporting.WinForms.Warning
                Dim warning As String = ""

440:            m_rv.ServerReport.Timeout = 3600000 'timeout

450:            clsMarsDebug.writeToDebug("export.debug", "Rendering report to byte stream", True)


451:            Dim outputBytes As Byte() = m_rv.ServerReport.Render(sFormat, deviceInfo, "", "", "", oStream, Warnings)

460:            If warning IsNot Nothing Then
470:                For Each w As Microsoft.Reporting.WinForms.Warning In Warnings
                        warning &= w.Message & " :" & w.Severity & vbCrLf
480:                Next
                End If

490:            clsMarsUI.MainUI.BusyProgress(90, "Creating report file...")

500:            clsMarsDebug.writeToDebug("export.debug", "Converting byte stream to file", True)
                clsMarsDebug.writeToDebug("export.debug", "Byte stream size: " & outputBytes.LongLength, True)

510:            createFileFromServer(outputBytes, sOutput)

                clsMarsDebug.writeToDebug("export.debug", "File created!", True)
640:            Return True
650:        Catch ex As Exception

                clsMarsDebug.writeToDebug("export.debug", "Error:" & ex.Message & vbCrLf & Erl(), True)

                Dim sMsg As String = ""

660:            If ex.Message.ToLower.IndexOf("client found response content type of 'text/html; charset=utf-8', but expected 'text/xml'") > -1 Then
670:                sMsg = "The web service you are attempting to access on this web server is currently unavailable. Please make sure that IIS is running."
680:            ElseIf ex.Message.ToLower.IndexOf("the request failed with http status 405") > -1 Then
690:                sMsg = "SQL-RD connects to, and consumes, the web service on the SSRS server." & _
                          "The problem is likely to be as a result of problems with ASP and IIS on the SSRS server." & _
                          "This could indicate a disconnect between IIS and asp.net, you could attempt to " & _
                          "re-associate the SSRS virtual directory with asp.net by following the steps below:" & vbCrLf & _
                          "-open a command prompt - click start->run->type in 'cmd'" & vbCrLf & _
                          "-browse to c:\winnt\microsoft.net\framework\v1.1.4322\ (winnt folder may be windows)" & vbCrLf & _
                          "-run the command 'aspnet_regiis -i'"
700:            ElseIf ex.Message.ToLower.IndexOf("timeout") > -1 Then
                    sMsg = "Please increase the server timeout in SQL Server Reports Manager (http://myReportServer/Reports/Pages/Settings.aspx)"
710:            ElseIf ex.Message.ToLower.IndexOf("default value or value provided for the report parameter") > -1 Then
720:                sMsg = "Please make sure that all the parameter fields have valid values. If you are passing 'Null' to the parameter, make sure that " & _
                          "the parameter field has the 'allow null value' set and that it DOES NOT have any queried or non-queried available values."
730:            ElseIf ex.Message.Contains("rsReportParameterValueNotSet") Then
740:                sMsg = "Please make sure that the provided value for the parameter matches the available values in the report. Also take note that the report parameters in SSRS are case-senstive"
                ElseIf ex.Message.Contains("rsRenderingExtensionNotFound") Then
                    sMsg = "Make sure that your version of SSRS supports the export format that you have selected (" & sFormat & ")"
                End If

                Try
                    GC.Collect()
                Catch : End Try

750:            gErrorDesc = gScheduleName & " (" & reportName & ") : " & ex.Message & vbCrLf & sWarnings
760:            gErrorNumber = Err.Number
770:            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
780:            gErrorLine = Erl()
790:            gErrorSuggest = sMsg

                clsMarsDebug.writeToDebug("export.debug", "Sending error alert", True)

                _ErrorHandle(gErrorDesc, Err.Number, gErrorSource, Erl, sMsg, , True)
            End Try
        End If

    End Function

    Private Function doCheckReportIsBlank(ByVal nID As Integer, ByVal otype As clsMarsScheduler.enScheduleType) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim doCheck As Boolean = False

        If otype = clsMarsScheduler.enScheduleType.REPORT Then
            SQL = "SELECT checkblank FROM reportattr WHERE reportid =" & nID
        Else
            SQL = "SELECT checkblank FROM packageattr WHERE packid =" & nID
        End If

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            doCheck = IsNull(oRs(0).Value, False)
        Else
            doCheck = False
        End If

        Return doCheck
    End Function
    Public Function ProcessBlankReport(Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0, _
    Optional ByVal sReportName As String = "", Optional ByVal sPackageName As String = "", Optional allReportBlank As Boolean = False) As String
        Try
            Dim SendTo As String
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim oParse As New clsMarsParser
            Dim sMsg As String
            Dim szSubject As String

            Dim tasks As clsMarsTask = New clsMarsTask

            If nPackID > 0 Then
                If nReportID <> 0 Then
                    SQL = "SELECT checkBlank FROM reportattr r INNER JOIN blankReportAlert b ON r.reportid = b.reportid WHERE r.checkblank =1"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                        tasks.ProcessTasks(nReportID, "NONE", clsMarsTask.enAfterType.PRODUCTION, , True)
                    Else
                        tasks.ProcessTasks(nPackID, "NONE", clsMarsTask.enAfterType.PRODUCTION, , True)
                    End If
                Else
                    tasks.ProcessTasks(nPackID, "NONE", clsMarsTask.enAfterType.PRODUCTION, , True)
                End If

            Else
                tasks.ProcessTasks(nReportID, "NONE", clsMarsTask.enAfterType.PRODUCTION, , True)
            End If

            If nPackID > 0 Then
                gErrorDesc = sPackageName & " [" & sReportName & "] .The report contained no data"
                gErrorSource = "clsMarsReport._ProcessReport"
                gErrorNumber = "-21479462"
                gErrorLine = 340
                gErrorSuggest = ""

                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True, True)
            Else
                gErrorDesc = gScheduleName & " - The report contained no data"
                gErrorSource = "clsMarsReport._ProcessReport"
                gErrorNumber = "-21479462"
                gErrorLine = 340
                gErrorSuggest = "This error message is only raised when the schedule is executed manually. It will NOT be raised if the schedule is executed by the scheduler."
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True)
            End If
        Catch ex As Exception
            _ErrorHandle("Error processing blank report action: " & ex.Message & vbCrLf & "Schedule processing WILL proceed.", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
            Return False
        End Try
    End Function
    Public Function ProcessReport(ByVal sFormat As String, _
           ByVal oRpt As ReportServer.ReportingService, ByVal strNow As String, _
           ByVal nReportID As Integer, ByVal OutputFile As String, ByVal sReportName As String, _
           ByVal sReportPath As String, _
           ByVal oDatasources() As ReportServer.DataSourceCredentials, _
           Optional ByVal sPackName As String = "", _
           Optional ByVal nDestinationID As Integer = 0, _
           Optional ByVal sExt As String = "", _
           Optional ByVal IsDynamic As Boolean = False, _
           Optional ByVal PackageMergePDF As Boolean = False, _
           Optional ByVal nPackID As Integer = 0, Optional ByVal rptTimeout As Integer = 60, _
           Optional ByVal preserveLinks As Boolean = False, _
           Optional ByRef isReportBlank As Boolean = False) As String

        Dim I As Integer
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim iRetry As Integer
        Dim sError As String
        Dim sCSV As String
        Dim sDel As String
        Dim sChar As String
        Dim sFullPath As String
        Dim sFullFormat As String = sFormat
10:     Dim oParse As New clsMarsParser
        Dim oBlank As ADODB.Recordset
20:     Dim ozOffice As New clsOfficeFileSummary
        Dim oPars() As ReportServer.ParameterValue
        Dim oReport() As Byte
        Dim sExport As String
        Dim oWarnings() As ReportServer.Warning
        Dim maxThreadCount As Integer = 6
        Dim MultiThread As Boolean = False
        Dim checkForBlankReports As Boolean = False
        Dim errorOccured As Boolean = False

30:     Try
40:         MultiThread = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("SQL-RDThreading", 0)))
50:         maxThreadCount = clsMarsUI.MainUI.ReadRegistry("MaxThreadCount", 6)
60:     Catch ex As Exception
70:         MultiThread = False
        End Try



80:     If (m_IsMultithreaded = False) Then

90:         If (MultiThread = True) Then
100:            Do
110:                Application.DoEvents()
120:            Loop Until clsMarsThreading.GetThreadDetailCount() < maxThreadCount
            End If

130:        AddThreadDetail(nReportID, Process.GetCurrentProcess.Id())
        End If

140:    Try
150:        OutputFile = AddFolderToOutput(clsMarsReport.m_OutputFolder) & ExtractFileName(OutputFile)

160:        iRetry = 0

170:        Try
180:            rptTimeout = -1 '(rptTimeout * 60) * 1000
            Catch : End Try

190:        If m_rv IsNot Nothing Then m_rv.ServerReport.ReportPath = sReportPath

191:        setDatasourceCredentials(nReportID)

200:        oPars = SetReportParameters(nReportID)

            '//blank report
            Dim BlankType As String = ""
210:        Dim oUI As New clsMarsUI

220:        Try
230:            If nPackID > 0 Then
                    '//check if the packaged report has option to check for blank reports
240:                If doCheckReportIsBlank(nReportID, clsMarsScheduler.enScheduleType.REPORT) Then
250:                    SQL = "SELECT r.reportID, b.type FROM reportattr r INNER JOIN blankreportalert b on r.reportid = b.reportid WHERE r.reportid =" & nReportID & " AND r.checkBlank = 1"
260:                    oBlank = clsMarsData.GetData(SQL)
270:                    BlankType = IsNull(oBlank("type").Value)

280:                    If oBlank.EOF = False Then '//report has its own settings
290:                        If _BlankReport(oRpt, oPars, oDatasources, sReportPath, nReportID) = True Then

300:                            Me.ProcessBlankReport(nReportID, , sReportName)

310:                            Select Case BlankType.ToLower
                                    Case "ignore", "alert"
320:                                    Return "{skip}"
                                End Select
                            End If
                        End If

330:                    oBlank.Close()
340:                ElseIf doCheckReportIsBlank(nPackID, clsMarsScheduler.enScheduleType.PACKAGE) Then '//then check if the package has any blank report checking setup too
350:                    isReportBlank = _BlankReport(oRpt, oPars, oDatasources, sReportPath, , nPackID)

360:                    SQL = "SELECT * FROM BlankReportAlert WHERE PackID =" & nPackID & " AND (allReportsBlankOnly = 0 OR allReportsBlankOnly IS NULL)"

370:                    oBlank = clsMarsData.GetData(SQL)

380:                    If oBlank.EOF = False Then
390:                        BlankType = IsNull(oBlank("type").Value)

400:                        If isReportBlank = True Then
410:                            Me.ProcessBlankReport(, nPackID, sReportName, sPackName)

420:                            Select Case BlankType.ToLower
                                    Case "ignore", "alert"
430:                                    Return "{skip}"
                                End Select
                            End If
                        End If

440:                    oBlank.Close()
                    End If
450:            Else
460:                If doCheckReportIsBlank(nReportID, clsMarsScheduler.enScheduleType.REPORT) = True Then

470:                    oUI.BusyProgress(75, "Checking if report is blank...")

480:                    SQL = "SELECT * FROM BlankReportAlert WHERE ReportID =" & nReportID

490:                    oBlank = clsMarsData.GetData(SQL)

500:                    If oBlank.EOF = False Then
510:                        checkForBlankReports = True
520:                        BlankType = IsNull(oBlank("type").Value)

530:                        If _BlankReport(oRpt, oPars, oDatasources, sReportPath, nReportID) = True Then
540:                            Me.ProcessBlankReport(nReportID)

550:                            Select Case BlankType.ToLower
                                    Case "ignore", "alert"
560:                                    isReportBlank = True
570:                                    isReportBlank = True
580:                                    Return "{skip}"
                                End Select
                            End If
590:                    End If
                    End If
600:            End If
610:        Catch : End Try

620:        If sReportName.IndexOf("[~]") > -1 Then
630:            sReportName = sReportName.Replace("[~]", "[p]")
640:            sReportName = oParse.ParseString(sReportName, , , , , , m_ParametersTable)

650:            If OutputFile.IndexOf("[~]") > -1 Then
660:                OutputFile = GetDirectory(OutputFile.Replace("<", "").Replace(">", "")) & sReportName
670:            End If
            End If
            'TODO: check for blank report...

680:        If nDestinationID > 0 Then
690:            SQL = "SELECT * FROM ReportOptions WHERE DestinationID =" & nDestinationID
700:        ElseIf nReportID > 0 Then
710:            SQL = "SELECT * FROM ReportOptions WHERE ReportID =" & nReportID
720:        End If

730:        oRs = clsMarsData.GetData(SQL)

740:        Try
750:            If sExt.Length = 0 Then sExt = sFormat.Split("*")(1).Replace(")", String.Empty).Trim

760:            If sExt.StartsWith(".") = False Then sExt = "." & sExt

770:            sFormat = sFormat.Split("(")(0).Trim
780:        Catch ex As Exception
790:            Try
800:                sExt = sFormat.Split("*")(1).Replace(")", String.Empty).Trim
810:                sFormat = sFormat.Split("(")(0).Trim
                Catch : End Try
820:        End Try

830:        sExport = OutputFile & strNow & sExt
retry:
840:        With oRpt
850:            Select Case sFormat
                    Case "Custom" '//custom renderer extension
                        Dim sTemp As String
                        Dim renderExt As String = ""

860:                    sTemp = sExport

870:                    If oRs.EOF = False Then
880:                        renderExt = IsNull(oRs("rendername").Value, "")
                        End If

890:                    If Me.ReportServerRender(oRpt, renderExt, oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
900:                        Return "{error}"
                        End If

910:                    Return sExport
920:                Case "Text"
                        Dim sTemp As String
                        Dim textType As String = "driver"

930:                    If oRs.EOF = False Then
940:                        textType = IsNull(oRs("textengine").Value, "default")
                        End If


950:                    If textType.ToLower = "default" Then
960:                        sTemp = sExport.ToLower.Replace(sExt, ".pdf")

970:                        If Me.ReportServerRender(oRpt, "PDF", oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
980:                            Return "{error}"
                            End If

990:                        Dim ass As System.Reflection.Assembly = System.Reflection.Assembly.LoadFrom(sAppPath & "pdftotext.dll")

1000:                       Dim pdftotext As Object = ass.CreateInstance("Winnovative.PdfToTextConverter")    'Winnovative.PdfToTextConverter = New Winnovative.PdfToTextConverter
1010:                       pdftotext.LicenseKey = "SWJ7aXt8aXp8fGl7Z3lpenhneHtncHBwcA=="
1020:                       pdftotext.AddHtmlMetaTags = False
                            ' pdftotext.Layout = Winnovative.TextLayout.OriginalLayout
1030:                       pdftotext.MarkPageBreaks = True
1040:                       pdftotext.Layout = 0 'Winnovative.TextLayout.OriginalLayout

                            Dim output As String = pdftotext.ConvertToText(sTemp)

1050:                       output = output.Replace("", vbCrLf)

1060:                       IO.File.WriteAllText(sExport, output)

1070:                       Return sExport
1080:                   Else
1090:                       Dim emfFiles As ArrayList = New ArrayList
                            Dim dpi As Integer = 0
                            Dim deviceInfo As String = "<DeviceInfo><OutputFormat>emf</OutputFormat></DeviceInfo>"

1100:                       clsMarsDebug.writeToDebug("text_export.debug", "-------Starting text export for " & sReportName & "------", True)

                            '//set the dpi if the user has specified it
1110:                       If oRs.EOF = False Then
1120:                           dpi = IsNull(oRs("textdpi").Value, 0)

1130:                           If dpi <> 0 Then
1140:                               deviceInfo = "<DeviceInfo><OutputFormat>emf</OutputFormat><PrintDpiX>" & dpi & "</PrintDpiX><PrintDpiY>" & dpi & "</PrintDpiY></DeviceInfo>"
                                End If
                            End If

1150:                       clsMarsDebug.writeToDebug("text_export.debug", "Device info set as " & deviceInfo, True)

                            '//create the EMF file to print to Text later
                            Dim oStream() As String
                            Dim oRender As Byte()

1160:                       sTemp = sExport.ToLower.Replace(sExt, ".emf")

1170:                       clsMarsDebug.writeToDebug("text_export.debug", "creating EMF file", True)


1180:                       If Me.ReportServerRender(oRpt, "IMAGE", oPars, oDatasources, sReportPath, sTemp, oStream, rptTimeout, deviceInfo, sReportName) = False Then
1190:                           Return "{error}"
                            End If

1200:                       clsMarsDebug.writeToDebug("text_export.debug", "emf file created at " & sTemp, True)

1210:                       emfFiles.Add(sTemp)

                            '//render the additional pages

1220:                       Dim resourcesFolder = GetDirectory(sTemp)

1230:                       If IO.Directory.Exists(resourcesFolder) = False Then
1240:                           IO.Directory.CreateDirectory(resourcesFolder)
                            End If

1250:                       clsMarsDebug.writeToDebug("text_export.debug", "additional pages saved to " & resourcesFolder, True)

1260:                       For Each StreamID As String In oStream
1270:                           Dim oResoucePath As String = resourcesFolder & StreamID
1280:                           Dim fsResource As System.IO.FileStream = System.IO.File.Create(oResoucePath)

1290:                           If Me.m_ServerVersion2005 = False Then
1300:                               oRender = .RenderStream(sReportPath, "IMAGE", StreamID, Nothing, deviceInfo, oPars, Nothing, Nothing)
1310:                           Else
1320:                               oRender = m_rv.ServerReport.RenderStream("IMAGE", StreamID, deviceInfo, Nothing, Nothing)
                                End If

1330:                           fsResource.Write(oRender, 0, oRender.Length)

1340:                           fsResource.Close()

1350:                           clsMarsDebug.writeToDebug("text_export.debug", "Adding pages " & oResoucePath, True)

1360:                           emfFiles.Add(oResoucePath)
1370:                       Next

1380:                       Dim print As clsPrinters = New clsPrinters
                            Dim ofile As String = IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".txt"

1390:                       clsMarsDebug.writeToDebug("text_export.debug", "Print text file to Miraplacid driver", True)

1400:                       If oRs.EOF = True Then
1410:                           print.printEMFFile(emfFiles, ofile, True, 2)
1420:                       Else

1430:                           Dim textdpi As Integer = IsNull(oRs("textdpi").Value, 96)
1440:                           Dim usetextdriverdefault As Integer = IsNull(oRs("usetextdriverdefault").Value, True)
1450:                           Dim formattingstyle As String = IsNull(oRs("formattingstyle").Value, "Formatted")
1460:                           Dim characterset As Integer = IsNull(oRs("characterset").Value, 1250)
1470:                           Dim eolstyle As Integer = IsNull(oRs("eolstyle").Value, 0)
1480:                           Dim whitespacesize As Integer = IsNull(oRs("whitespacesize").Value, 100)
1490:                           Dim insertbreaks As Boolean = IsNull(oRs("insertbreaks").Value, False)
1500:                           Dim timeperpage As Integer = 1

1510:                           Try
1520:                               timeperpage = IsNull(oRs("timeperpage").Value, 1)
1530:                           Catch ex As Exception
1540:                               timeperpage = 1
                                End Try

1550:                           print.printEMFFile(emfFiles, ofile, usetextdriverdefault, timeperpage, formattingstyle, characterset, eolstyle, whitespacesize, insertbreaks)
                            End If

1560:                       clsMarsDebug.writeToDebug("text_export.debug", "Moving file to destination", True)

1570:                       Dim waitAfterPrint As Integer = clsMarsUI.MainUI.ReadRegistry("WaitAfterPrintFor", 30)
1580:                       Dim retryMoveCount As Integer = 0

1590:                       Try
MoveTextFile:                   IO.File.Move(m_OutputFolder & ofile, sExport) '//move the miraplacid print out into the tempXXXX folder
1600:                       Catch ex As Exception

1610:                           clsMarsDebug.writeToDebug("text_export.debug", "Error (" & ex.Message & ") moving file '" & m_OutputFolder & ofile & "' to " & sExport, True) '//error so write the error

1620:                           If ex.Message.ToLower.Contains("process cannot access the file") And retryMoveCount <= 3 Then '//if the error is file is locked and we havent retried 4 times then

1630:                               clsMarsDebug.writeToDebug("text_export.debug", "Waiting " & waitAfterPrint & " seconds for Miraplacid to release file", True)

1640:                               System.Threading.Thread.Sleep(waitAfterPrint * 1000) '//wait 30 seconds default

1650:                               retryMoveCount += 1 '//increment retryCount

1660:                               clsMarsDebug.writeToDebug("text_export.debug", "Retry number " & retryMoveCount & " to move file", True)

1670:                               GoTo MoveTextFile '//go and retry moving the file
1680:                           Else
1690:                               Throw ex
                                End If
                            End Try

1700:                       clsMarsDebug.writeToDebug("text_export.debug", "**Fin**", True)

1710:                       Return sExport
                        End If

                    Case "MS Word", "Rich Text Format", "MS Word - Text", "MS Word 2007"
1730:                   Dim sTemp As String
                        Dim oFormat As WordMan.WordMan.wdFormats
                        Dim renderEx As String = "WORD"
                        Dim fileEx As String = ".doc"

                        If sFormat = "MS Word 2007" Then
                            renderEx = "WORDOPENXML"
                            fileEx = ".docx"
                        End If
                        '//try the native method first

                        sTemp = sExport.ToLower.Replace(sExt, fileEx)
                        Dim conversionRequired As Boolean

                        If Me.ReportServerRender(oRpt, renderEx, oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
                            If renderEx <> "WORDOPENXML" Then
1740:                           sTemp = sExport.ToLower.Replace(sExt, ".mhtml")

1750:                           If Me.ReportServerRender(oRpt, "MHTML", oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
                                    Return "{error}"
                                End If
                            Else
                                Return "{error}"
                            End If

                            conversionRequired = True
                        End If

                        If conversionRequired Then
1770:                       Dim oW As WordMan.WordMan = New WordMan.WordMan

1790:                       If sFormat = "MS Word" Then
1800:                           oFormat = WordMan.WordMan.wdFormats.wDoc
1810:                       ElseIf sFormat = "MS Word - Text" Then

1820:                           If oRs.EOF = True Then
1830:                               oFormat = WordMan.WordMan.wdFormats.wDOSTEXT
1840:                           Else
1850:                               Try
1860:                                   oFormat = IsNull(oRs("texttype").Value, WordMan.WordMan.wdFormats.wDOSTEXT)
1870:                               Catch ex As Exception
1880:                                   oFormat = WordMan.WordMan.wdFormats.wDOSTEXT
                                    End Try
                                End If
                            ElseIf sFormat = "MS Word 2007" Then
                                oFormat = WordMan.WordMan.wdFormats.wDoc
1890:                       Else
1900:                           oFormat = WordMan.WordMan.wdFormats.wRTF
1910:                       End If

                            Dim err As Exception = Nothing

1920:                       If oW._ConvertMHTML(sTemp, oFormat, sExport, err) = False Then
1930:                           Throw New Exception("Failed to export to " & sFormat & ". " & err.Message)
1940:                       End If
                        End If

1950:                   If oFormat = WordMan.WordMan.wdFormats.wDoc Then
1960:                       If oRs.EOF = False Then
1970:                           ozOffice._GetFileInfo(sExport, oRs, IsDynamic, nReportID, IsNull(sKeyParameter), IsNull(sKeyValue), True)
1980:                       End If
1990:                   End If

2000:                   gCurrentTempOutputFile = sExport

2010:                   Return sExport
1720:               Case "MS Word", "Rich Text Format", "MS Word - Text"
                        'Dim sTemp As String
                        'Dim oFormat As WordMan.WordMan.wdFormats
                        ''//try the native method first

                        'sTemp = sExport.ToLower.Replace(sExt, ".doc")

                        'If Me.ReportServerRender(oRpt, "WORD", oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
                        '    sTemp = sExport.ToLower.Replace(sExt, ".mhtml")

                        '    If Me.ReportServerRender(oRpt, "MHTML", oPars, oDatasources, sReportPath, sTemp, , rptTimeout, , sReportName) = False Then
                        '        Return "{error}"
                        '    End If


                        '    Dim oW As WordMan.WordMan = New WordMan.WordMan



                        '    If sFormat = "MS Word" Then
                        '        oFormat = WordMan.WordMan.wdFormats.wDoc
                        '    ElseIf sFormat = "MS Word - Text" Then

                        '        If oRs.EOF = True Then
                        '            oFormat = WordMan.WordMan.wdFormats.wDOSTEXT
                        '        Else
                        '            Try
                        '                oFormat = IsNull(oRs("texttype").Value, WordMan.WordMan.wdFormats.wDOSTEXT)
                        '            Catch ex As Exception
                        '                oFormat = WordMan.WordMan.wdFormats.wDOSTEXT
                        '            End Try
                        '        End If
                        '    Else
                        '        oFormat = WordMan.WordMan.wdFormats.wRTF
                        '    End If

                        '    Dim err As Exception = Nothing

                        '    If oW._ConvertMHTML(sTemp, oFormat, sExport, err) = False Then
                        '        Throw New Exception("Failed to export to " & sFormat & ". " & err.Message)
                        '    End If
                        'End If

                        'If oFormat = WordMan.WordMan.wdFormats.wDoc Then
                        '    If oRs.EOF = False Then
                        '        ozOffice._GetFileInfo(sExport, oRs, IsDynamic, nReportID, IsNull(sKeyParameter), IsNull(sKeyValue), True)
                        '    End If
                        'End If

                        'gCurrentTempOutputFile = sExport

                        'Return sExport
2020:

2030:               Case "Rich Text Format", "Text"
2040:                   Dim oPDf As clsMarsPDF = New clsMarsPDF
                        Dim sTemp As String

2050:                   If Me.ReportServerRender(oRpt, "PDF", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
2060:                       Return "{error}"
                        End If

2070:                   If sFormat = "Rich Text Format" Then
2080:                       sTemp = oPDf._ConvertFromPDF(PDF2ManyX.TgtRenderTypeX.rtRTF, sExport, "tempfile", sTempPath, True)
2090:                   Else
2100:                       sTemp = oPDf._ConvertFromPDF(PDF2ManyX.TgtRenderTypeX.rtTXT, sExport, "tempfile", sTempPath, True)
2110:                   End If

2120:                   IO.File.Move(sTemp, sExport)

2130:                   gCurrentTempOutputFile = sExport

2140:                   Return sExport
2150:               Case "Acrobat", "PDF", "Acrobat Format (PDF)", "Printer Output", "Acrobat Format"

2160:                   If Me.ReportServerRender(oRpt, "PDF", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
2170:                       Return "{error}"
2180:                   End If

2190:                   Dim PDFsecurity As Boolean = False

2200:                   Try
2210:                       If oRs("pdfsecurity").Value = 1 Or oRs("pdfsecurity").Value = -1 Then
2220:                           PDFsecurity = True
2230:                       End If
2240:                   Catch
2250:                       PDFsecurity = False
2260:                   End Try


2270:                   If oRs.EOF = False Then
2280:                       Dim oPerm As New clsMarsPDF
                            Dim InfoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer As String
                            Dim InfoCreated As Date
                            Dim CanPrint, CanCopy, CanEdit, CanNotes, CanFill, CanAccess, CanAssemble, _
                CanPrintFull As Integer
                            Dim OwnerPassword, UserPassword, PDFWatermark As String
                            Dim pdfExpires As Boolean
                            Dim pdfExpiryDate As Date

2290:                       Try
2300:                           pdfExpires = Convert.ToInt32(IsNull(oRs("expirepdf").Value, 0))
2310:                           pdfExpiryDate = IsNull(oRs("pdfexpirydate").Value, Date.Now)
                            Catch : End Try

2320:                       InfoTitle = IsNull(oRs("infotitle").Value)
2330:                       InfoAuthor = IsNull(oRs("infoauthor").Value)
2340:                       InfoSubject = IsNull(oRs("infosubject").Value)
2350:                       InfoKeywords = IsNull(oRs("infokeywords").Value)
2360:                       InfoProducer = IsNull(oRs("infoproducer").Value)
2370:                       InfoCreated = IsNull(oRs("infocreated").Value, Now)

2380:                       CanPrint = IsNull(oRs("canprint").Value, 1)
2390:                       CanCopy = IsNull(oRs("cancopy").Value, 1)
2400:                       CanEdit = IsNull(oRs("canedit").Value, 1)
2410:                       CanNotes = IsNull(oRs("cannotes").Value, 1)
2420:                       CanFill = IsNull(oRs("canfill").Value, 1)
2430:                       CanAccess = IsNull(oRs("canaccess").Value, 1)
2440:                       CanAssemble = IsNull(oRs("canassemble").Value, 1)
2450:                       CanPrintFull = IsNull(oRs("canprintfull").Value, 1)

2460:                       OwnerPassword = IsNull(oRs("pdfpassword").Value)
2470:                       UserPassword = IsNull(oRs("userpassword").Value)
2480:                       PDFWatermark = IsNull(oRs("pdfwatermark").Value)

2490:                       sFullPath = Me._PostProcessPDF(sExport, InfoTitle, _
                                 InfoAuthor, InfoSubject, InfoKeywords, InfoProducer, _
                                 InfoCreated, IsDynamic, nReportID, oRpt, PDFsecurity, PackageMergePDF, _
                                 CanPrint, CanCopy, CanEdit, _
                                 CanNotes, CanFill, CanAccess, CanAssemble, _
                                 CanPrintFull, OwnerPassword, UserPassword, _
                                 PDFWatermark, nDestinationID, pdfExpires, pdfExpiryDate)

2500:                       gCurrentTempOutputFile = sFullPath


2510:                       Return sFullPath
2520:                   Else
2530:                       gCurrentTempOutputFile = sExport

2540:                       Return sExport
                        End If

2550:               Case "Excel", "MS Excel", "MS Excel 97-2000", "MS Excel 2007"
                        Dim renderExt As String = "EXCEL"

                        If sFormat = "MS Excel 2007" Then renderExt = "EXCELOPENXML"


2560:                   If Me.ReportServerRender(oRpt, renderExt, oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
2570:                       Return "{error}"
                        End If

2580:                   If oRs.EOF = False Then
2590:                       If IsNull(oRs("worksheetname").Value) <> "" Then
2600:                           Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)

2610:                           oXL.SetWorkSheetName(sExport, oParse.ParseString(oRs("worksheetname").Value, , , , , , m_ParametersTable))
2620:                           oXL.Dispose()

2630:                       Else
2640:                           Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)

2650:                           oXL.SetWorkSheetName(sExport, "Sheet1")
2660:                           oXL.Dispose()
                            End If

2670:                       ozOffice._GetFileInfo(sExport, oRs, IsDynamic, nReportID, IsNull(sKeyParameter), IsNull(sKeyValue))
2680:                   End If

2690:                   gCurrentTempOutputFile = sExport

2700:                   Return sExport

2710:               Case "Web Archive"
2720:                   If Me.ReportServerRender(oRpt, "MHTML", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
2730:                       Return "{error}"
                        End If

2740:                   gCurrentTempOutputFile = sExport

2750:                   Return sExport
2760:               Case "HTML", "DHTML", "XHTML"

                        Dim TempFile As String
                        Dim oPath As String
                        Dim Fol
                        Dim OutputPath As String
                        Dim Temp As String
                        Dim oStream As String()
                        Dim oRender As Byte()
                        Dim sOriginalName As String
                        Dim serverFormat As String = "HTML4.0"

2770:                   If oRs.EOF = False Then
2780:                       Try
2790:                           preserveLinks = IsNull(oRs("excelshowgrid").Value, 0)
                            Catch : End Try
                        End If

                        sOriginalName = ExtractFileName(OutputFile).Replace(".", "").Replace("\", "").Replace("/", "").Replace(":", "").Trim & strNow '& sExt

                        'create a temp folder and file name using epoch date

2800:                   TempFile = ExtractFileName(OutputFile).Replace(" ", "").Replace(".", "").Replace("\", "").Replace("/", "").Replace(":", "").Trim

2810:                   If preserveLinks = True Then
2820:                       TempFile &= ".html"
2830:                       serverFormat = "HTML4.0"
2840:                   Else
2850:                       TempFile &= ".pdf"
2860:                       serverFormat = "PDF"
                        End If

2870:                   oPath = sTempPath & clsMarsData.CreateDataID

2880:                   HTMLPath = oPath & "\" & TempFile

2890:                   If IO.Directory.Exists(oPath) = False Then
2900:                       IO.Directory.CreateDirectory(oPath)
                        End If

                        'export the HTML
2910:                   If Me.ReportServerRender(oRpt, serverFormat, oPars, oDatasources, sReportPath, HTMLPath, oStream, rptTimeout, , sReportName) = False Then
2920:                       Return "{error}"
                        End If

2930:                   If preserveLinks = True Then
2940:                       Try
2950:                           For Each StreamID As String In oStream
                                    Dim oResoucePath As String = oPath & "\" & StreamID & ".png"
                                    Dim fsResource As System.IO.FileStream = System.IO.File.Create(oResoucePath)

2960:                               If Me.m_ServerVersion2005 = False Then
2970:                                   oRender = .RenderStream(sReportPath, "HTML4.0", StreamID, Nothing, Nothing, _
                                             oPars, Nothing, Nothing)
2980:                               Else
2990:                                   oRender = m_rv.ServerReport.RenderStream("HTML4.0", StreamID, Nothing, Nothing, Nothing)
                                    End If

3000:                               fsResource.Write(oRender, 0, oRender.Length)

3010:                               fsResource.Close()
3020:                           Next

3030:                           FixHTMLImageRefs(HTMLPath, oStream)
                            Catch : End Try
3040:                   Else
3050:                       Try
3060:                           Dim oPDF As New clsMarsPDF

3070:                           HTMLPath = oPDF._ConvertFromPDF(PDF2ManyX.TgtRenderTypeX.rtHTML, _
                                     HTMLPath, sOriginalName, oPath, True)


                                'sOriginalName = GetDirectory(HTMLPath) & sOriginalName & sExt

                                'IO.File.Move(HTMLPath, sOriginalName)

                                'HTMLPath = GetDirectory(sOriginalName)
3080:                       Catch ex As Exception
3090:                           Throw New Exception(ex.Message)
                            End Try
                        End If

3100:                   sOriginalName = GetDirectory(HTMLPath) & sOriginalName & sExt

3110:                   IO.File.Move(HTMLPath, sOriginalName)

3120:                   HTMLPath = GetDirectory(sOriginalName)

3130:                   gCurrentTempOutputFile = sOriginalName

3140:                   Return sOriginalName

3150:               Case "XML"
3160:                   If Me.ReportServerRender(oRpt, "XML", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
3170:                       Return "{error}"
                        End If

3180:                   gCurrentTempOutputFile = sExport

3190:                   Return sExport
3200:               Case "CSV"
                        Dim exportEngine As String = "Standard"

3210:                   If oRs.EOF = False Then
3220:                       Try
3230:                           exportEngine = IsNull(oRs("csvengine").Value, "Standard")
3240:                       Catch ex As Exception
3250:                           exportEngine = "Standard"
                            End Try
                        End If

3260:                   If exportEngine = "Standard" Then
3270:                       If Me.ReportServerRender(oRpt, "CSV", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
3280:                           Return "{error}"
                            End If
3290:                   Else
3300:                       If Me.ReportServerRender(oRpt, "EXCEL", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
3310:                           Return "{error}"
                            End If

3320:                       Dim oxl As ExcelMan.clsExcelMan = New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id)
                            Dim tempFile As String = clsMarsReport.m_OutputFolder & IO.Path.GetRandomFileName

3330:                       If oxl.ConvertXLFile(sExport, tempFile, ExcelMan.clsExcelMan.XLFormats.CSV) = False Then
3340:                           gErrorDesc = oxl.m_errorMsg
3350:                           gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
3360:                           gErrorNumber = 3476
3370:                           gErrorLine = 6317

3380:                           oxl.Dispose()
3390:                           Return "{error}"
                            End If

3400:                       oxl.Dispose()

3410:                       IO.File.Delete(sExport)
3420:                       IO.File.Move(tempFile, sExport)
                        End If


3430:                   If oRs.EOF = False Then
                            Dim sContent As String
                            Dim sxSep As String
                            Dim sDelimiter As String
                            Dim ignoreHeaders As Boolean

3440:                       sxSep = IsNull(oRs("scharacter").Value, ",")
3450:                       sDelimiter = IsNull(oRs("sdelimiter").Value, "")

3460:                       If sxSep = "tab" Then sxSep = vbTab


3470:                       Try
3480:                           ignoreHeaders = oRs("csvIgnoreHeaders").Value
3490:                       Catch ex As Exception
3500:                           ignoreHeaders = False
                            End Try

3510:                       Dim tempFile As String = clsMarsReport.m_OutputFolder & IO.Path.GetRandomFileName
3520:                       Dim errInfo As Exception

3530:                       If Me.processCSVFile(sExport, sxSep, sDelimiter, tempFile, errInfo, ignoreHeaders) = False Then
3540:                           Throw errInfo
3550:                       Else
3560:                           IO.File.Delete(sExport)
3570:                           IO.File.Move(tempFile, sExport)
                            End If


3580:                   End If

3590:                   gCurrentTempOutputFile = sExport

3600:                   Return sExport

3610:               Case "ODBC"
3620:                   If Me.ReportServerRender(oRpt, "CSV", oPars, oDatasources, sReportPath, sExport, , rptTimeout) = False Then
3630:                       Return "{error}"
                        End If

                        Dim sxSep As String = "�"
                        Dim sxDel As String = ""

3640:                   Me.processCSVFile(sExport, sxSep, sxDel)

3650:                   Me.ODBCProcessing(sExport, nDestinationID)

3660:                   Return "{skip}"
3670:               Case "Lotus 1-2-3", "dBase IV", "dBase III", "dBase II", "Data Interchange Format"

3680:                   sExport = sExport.Replace(sExt, ".xls")

3690:                   If Me.ReportServerRender(oRpt, "EXCEL", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
3700:                       Return "{error}"
                        End If

3710:                   sFullPath = sExport

3720:                   Dim oConvert As New ucXLConv
                        Dim oResult() As Object

                        Dim sNewFile As String
                        Dim sFile As String
                        Dim sPath As String
                        Dim oDBF As Boolean
3730:                   Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)

3740:                   sFile = ExtractFileName(sFullPath)
3750:                   sPath = sFullPath.Replace(sFile, String.Empty)

3760:                   sNewFile = sPath & sFile.Replace(".xls", String.Empty) & sExt

3770:                   Select Case sFullFormat
                            Case "Lotus 1-2-3 (*.wk4)"
3780:                           oResult = oConvert.ConvertXLFile(sFullPath, sNewFile, ucXLConv.XLConvertTypes.Lotus123_4)
3790:                       Case "Lotus 1-2-3 (*.wk3)"
3800:                           oResult = oConvert.ConvertXLFile(sFullPath, sNewFile, ucXLConv.XLConvertTypes.Lotus123_3)
3810:                       Case "Lotus 1-2-3 (*.wk1)"
3820:                           oResult = oConvert.ConvertXLFile(sFullPath, sNewFile, ucXLConv.XLConvertTypes.Lotus123_1)
3830:                       Case "Lotus 1-2-3 (*.wks)"
3840:                           oResult = oConvert.ConvertXLFile(sFullPath, sNewFile, ucXLConv.XLConvertTypes.Lotus123_S)
3850:                       Case "dBase II (*.dbf)"
3860:                           oDBF = oXL.ConvertXLFile(sFullPath, sNewFile, ExcelMan.clsExcelMan.XLFormats.dBASEII)

3870:                           ReDim oResult(1)

3880:                           If oDBF = True Then
3890:                               oResult(1) = 0
3900:                               oResult(0) = ""
3910:                           Else
3920:                               oResult(1) = -10
3930:                               oResult(0) = ""
3940:                           End If
3950:                       Case "dBase III (*.dbf)"
3960:                           oDBF = oXL.ConvertXLFile(sFullPath, sNewFile, ExcelMan.clsExcelMan.XLFormats.dBASEIII)

3970:                           ReDim oResult(1)

3980:                           If oDBF = True Then
3990:                               oResult(1) = 0
4000:                               oResult(0) = ""
4010:                           Else
4020:                               oResult(1) = -10
4030:                               oResult(0) = ""
                                End If
4040:                       Case "dBase IV (*.dbf)"
4050:                           oDBF = oXL.ConvertXLFile(sFullPath, sNewFile, ExcelMan.clsExcelMan.XLFormats.dBASEIV)

4060:                           ReDim oResult(1)

4070:                           If oDBF = True Then
4080:                               oResult(1) = 0
4090:                               oResult(0) = ""
4100:                           Else
4110:                               oResult(1) = -10
4120:                               oResult(0) = ""
4130:                           End If
4140:                       Case "Data Interchange Format (*.dif)"
4150:                           oResult = oConvert.ConvertXLFile(sFullPath, sNewFile, ucXLConv.XLConvertTypes.DIF)
                        End Select
4160:                   oXL.Dispose()
4170:                   Select Case (oResult(1))
                            Case -2
4180:                           Throw New Exception("Source file does not exist." & " " & oResult(0))
4190:                       Case -3
4200:                           Throw New Exception("Unable to create Excel Application. Is it installed?" & " " & oResult(0))
4210:                       Case -4
4220:                           Throw New Exception("Unable to destroy Excel Application." & " " & oResult(0))
4230:                       Case -10
4240:                           Throw New Exception("This format is not supported by MS Office 2007. Please uninstall Office 2007 and install an earlier version of MS Office." & " " & oResult(0))
4250:                       Case -100
4260:                           Throw New Exception("File does not exist." & " " & oResult(0))
                        End Select

4270:                   sFullPath = sNewFile

4280:                   oConvert = Nothing

4290:                   gCurrentTempOutputFile = sFullPath

4300:                   Return sFullPath

4310:               Case "TIFF Fax"
                        Dim sTargetFile As String
4320:                   Dim oPrint As New clsPrinters
                        Dim sPrnName As String = "TIFF-Xchange V3"
4330:                   Dim sTemp As String = oPrint.GetDefaultPrinter.Split(",")(0)

4340:                   sPrnName = Chr(34) & sPrnName & Chr(34)

4350:                   If Me.ReportServerRender(oRpt, "PDF", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
4360:                       Return "{error}"
4370:                   End If

4380:                   sTargetFile = sExport.Substring(0, (sExport.Length - 4))

4390:                   Dim pdf2fax As clsPDF2ImageConverter = New clsPDF2ImageConverter

4400:                   pdf2fax.ConvertPDFToImage(sExport, sTargetFile & ".tif", "", "", 72, 72, 8, _
                             88888888, 1, False, True, -1, -1)

                        '4700:                   oTiff.FileName = sTargetFile
                        '4710:                   oTiff.UnLock = "YEK02CXFFIT"

                        '4750:                   oTiff.BitsPerPixel = 1

                        '4770:                   oTiff.AppPath = ""

                        '4780:                   oTiff.Apply()

                        '                        oPrint.SetDefaultPrinter("TIFF-Xchange V3")


                        '                        Dim wb As SHDocVw.InternetExplorer = New SHDocVw.InternetExplorer

                        '                        wb.Visible = True

                        '                        wb.Navigate(sExport)

                        '                        Do While wb.ReadyState <> SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE
                        '                            Application.DoEvents()
                        '                        Loop

                        '                        Dim future As Date = Now.AddSeconds(5)

                        '                        Do While Now < future
                        '                            Application.DoEvents()
                        '                        Loop

                        '                        wb.ExecWB(SHDocVw.OLECMDID.OLECMDID_PRINT, SHDocVw.OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER)

                        '                        Dim status As Long = wb.QueryStatusWB(SHDocVw.OLECMDID.OLECMDID_PRINT)

                        '                        wb.Quit()

                        '4790:                   Dim sPrinter As String = oPrint.GetPrinterDetails("TIFF-Xchange V3")

                        '4800:                   If sPrinter.Length = 0 Then
                        '4810:                       oTiff.PrintFile(sExport, sPrnName)

                        '                            'reset the default printer
                        '4820:                       oPrint.SetDefaultPrinter(sTemp)
                        '4830:                   Else
                        '4840:                       Dim ucPrinter As New ucPDFViewer

                        '                            ucPrinter._PrintFile(sExport, "TIFF-Xchange V3", 0, 0)
                        '4860:                   End If

                        'oTiff.Revoke()

4410:                   gCurrentTempOutputFile = sTargetFile & ".tif"

4420:                   Return sTargetFile & ".tif"
4430:               Case "PNG"
                        Dim deviceInfo As String = "<DeviceInfo><OutputFormat>PNG</OutputFormat></DeviceInfo>"
                        Dim oStream() As String
                        Dim oRender() As Byte
                        Dim oPath As String

4440:                   HTMLPath = ""

4450:                   If Me.ReportServerRender(oRpt, "IMAGE", oPars, oDatasources, sReportPath, sExport, oStream, rptTimeout, deviceInfo, sReportName) = False Then
4460:                       Return "{error}"
                        End If

4470:                   gCurrentTempOutputFile = sExport

4480:                   oPath = IO.Path.GetDirectoryName(sExport)

4490:                   HTMLPath = oPath

4500:                   For Each StreamID As String In oStream
                            Dim oResoucePath As String = oPath & "\" & StreamID & ".png"
                            Dim fsResource As System.IO.FileStream = System.IO.File.Create(oResoucePath)

4510:                       If Me.m_ServerVersion2005 = False Then
4520:                           oRender = .RenderStream(sReportPath, "IMAGE", StreamID, Nothing, deviceInfo, _
                oPars, Nothing, Nothing)
4530:                       Else
4540:                           oRender = m_rv.ServerReport.RenderStream("IMAGE", StreamID, deviceInfo, Nothing, Nothing)
                            End If

4550:                       fsResource.Write(oRender, 0, oRender.Length)

4560:                       fsResource.Close()
4570:                   Next
4580:               Case "TIFF"
                        Dim deviceInfo As String = "<DeviceInfo><OutputFormat>emf</OutputFormat></DeviceInfo>"

4590:                   If oRs.EOF = True Then
4600:                       If Me.ReportServerRender(oRpt, "IMAGE", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
4610:                           Return "{error}"
                            End If

4620:                       gCurrentTempOutputFile = sExport

4630:                       Return sExport

4640:                   ElseIf IsNull(oRs("tcompression").Value) = "0" Or IsNull(oRs("tcompression").Value) = "" Or IsNull(oRs("tcompression").Value) = "Native" Then

4650:                       If Me.ReportServerRender(oRpt, "IMAGE", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
4660:                           Return "{error}"
                            End If

4670:                       gCurrentTempOutputFile = sExport

4680:                       Return sExport
4690:                   Else
                            Dim sTargetFile As String
4700:                       sExport = sExport.Substring(0, (sExport.Length - 4)) & ".pdf"

4710:                       If Me.ReportServerRender(oRpt, "PDF", oPars, oDatasources, sReportPath, sExport, , rptTimeout, , sReportName) = False Then
4720:                           Return "{error}"
4730:                       End If

4740:                       sTargetFile = sExport.Substring(0, (sExport.Length - 4))
4750:                       sTargetFile = sTargetFile & ".tif"
                            'sPDF = sExport.Substring(0, (sExport.Length - 4))
                            'spdf = spdf & ".pdf"


4760:                       Dim pdf2fax As clsPDF2ImageConverter = New clsPDF2ImageConverter
                            Dim BitCount As Long
                            Dim Compression As Integer
                            Dim GreyScale As Boolean
                            Dim FromPage As Long
                            Dim ToPage As Long

4770:                       GreyScale = Convert.ToBoolean(oRs("tgrey").Value)
4780:                       FromPage = oRs("pagefrom").Value
4790:                       ToPage = oRs("pageto").Value

4800:                       If IsNull(oRs("tcolour").Value) = "Black and White" Then BitCount = 1
4810:                       If IsNull(oRs("tcolour").Value) = "256 colors" Then BitCount = 8
4820:                       If IsNull(oRs("tcolour").Value) = "24-bit color" Then BitCount = 24

4830:                       If IsNull(oRs("tcompression").Value) = "None" Then Compression = 1
4840:                       If IsNull(oRs("tcompression").Value) = "LZW" Then Compression = 5
4850:                       If IsNull(oRs("tcompression").Value) = "MAC" Then Compression = 32773
4860:                       If IsNull(oRs("tcompression").Value) = "RLE" Then Compression = 2
4870:                       If IsNull(oRs("tcompression").Value) = "FAX3" Then Compression = 3
4880:                       If IsNull(oRs("tcompression").Value) = "FAX4" Then Compression = 4
4890:                       If IsNull(oRs("tcompression").Value) = "CLASSF" Then Compression = 88888888
4900:                       If IsNull(oRs("tcompression").Value) = "CLASSF196" Then Compression = 88888889



4910:                       pdf2fax.ConvertPDFToImage(sExport, sTargetFile, "", "", 96, 96, BitCount, _
                                 Compression, 1, GreyScale, True, FromPage, ToPage)

4920:                       gCurrentTempOutputFile = sTargetFile

4930:                       Return sTargetFile
                        End If

                End Select
4940:       End With

4950:       oRs.Close()
4960:   Catch ex As Exception
            errorOccured = True

4970:       gErrorDesc = sReportName & ": " & sPackName & Environment.NewLine & ex.Message
4980:       gErrorNumber = Err.Number
4990:       gErrorLine = Erl()
5000:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name

            _ErrorHandle(gErrorDesc, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "", False, True)

5010:       sExport = String.Empty

5020:       Return "{error}"

5030:   Finally
5040:       If (m_IsMultithreaded = False) Then
5050:           RemoveThreadDetail(nReportID, Process.GetCurrentProcess.Id())
            End If
        End Try

    End Function




    Public Sub processCSVFile(ByVal sFile As String, ByVal sep As String, ByVal del As String)
        Dim temp() As String = IO.File.ReadAllLines(sFile) 'ReadTextFromFile(sFile)
        Dim result As String

        If sep.ToLower = "tab" Then sep = vbTab

        Dim resultFile As String = IO.Path.GetDirectoryName(sFile)

        If resultFile.EndsWith("\") = False Then resultFile &= "\"

        resultFile &= IO.Path.GetFileNameWithoutExtension(sFile)

        Dim owrite As IO.StreamWriter = New IO.StreamWriter(resultFile & "output.txt")

        For Each line As String In temp
            result = ""
            Dim vals() As String = line.Split(",")

            For Each s As String In vals
                If s <> vbCrLf Then
                    result &= del & s & del & sep
                End If
            Next

            If result.Length > 1 Then
                result = result.Remove(result.Length - sep.Length, sep.Length)
            End If

            owrite.WriteLine(result)
            '            SaveTextToFile(result, sFile, , False, False)
        Next

        owrite.Close()

        IO.File.Delete(sFile)

        IO.File.Move(resultFile & "output.txt", sFile)
    End Sub
    Public Function CopyPackage(ByVal nPackID As Integer, _
    ByVal nParent As Integer) As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String = " WHERE PackID = " & nPackID
        Dim sNew As String
        Dim CopyReports As Boolean
        Dim NewPackID As Int64
        Dim nScheduleID As Int64
        Dim oRs As ADODB.Recordset
        Dim oUI As clsMarsUI = New clsMarsUI

        If MessageBox.Show("Copy the reports in the package as well? ", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then
            CopyReports = True
        Else
            CopyReports = False
        End If

retry:
        Try
            sNew = InputBox("Please enter the new package name", Application.ProductName)

            If sNew.Length = 0 Then Return 0

            If clsMarsData.IsDuplicate("PackageAttr", "PackageName", sNew, True) Then
                Dim oRes As DialogResult = MessageBox.Show _
                ("A package with that name already exists in the system", _
                Application.ProductName, MessageBoxButtons.RetryCancel, _
                MessageBoxIcon.Exclamation)

                If oRes = DialogResult.Retry Then
                    GoTo retry
                Else
                    Return 0
                End If
            End If

            'copy package details

            oUI.BusyProgress(20, "Copying package attributes")

            NewPackID = clsMarsData.DataItem.CopyRow("PackageAttr", nPackID, sWhere, "PackID", "PackageName", _
            sNew, , , , , , , , nParent)

            If NewPackID = 0 Then GoTo RollbackTran


            oUI.BusyProgress(40, "Copying package schedule details")

            nScheduleID = clsMarsData.DataItem.CopyRow("ScheduleAttr", 0, sWhere, "ScheduleID", , , , NewPackID)

            If nScheduleID = 0 Then GoTo RollbackTran

            'copy destination details

            oUI.BusyProgress(60, "Copying destination attributes")

            oRs = clsMarsData.GetData("SELECT DestinationID FROM DestinationAttr " & _
            "WHERE PackID = " & nPackID)

            Do While oRs.EOF = False
                Dim nDestID As Integer = 0

                nDestID = clsMarsData.DataItem.CopyRow("DestinationAttr", oRs(0).Value, " WHERE DestinationID = " & oRs(0).Value, _
                "DestinationID", , , , NewPackID)

                If nDestID = 0 Then GoTo RollbackTran

                'printer attributes-------------------------------------------------------------------------
                Dim sColumns As String = "PrinterID,ReportID,PackID,PrinterName,PageFrom,PageTo,Copies,DestinationID"

                SQL = "SELECT DISTINCT PrinterID FROM PrinterAttr p INNER JOIN DestinationAttr d ON p.DestinationID = d.DestinationID WHERE " & _
                "d.PackID  =" & nPackID & " AND d.DestinationType ='Printer'"

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                Do While oRs1.EOF = False

                    If clsMarsData.DataItem.CopyRow("PrinterAttr", oRs1(0).Value, " WHERE PrinterID =" & oRs1(0).Value, _
                    "PrinterID", , , , NewPackID, , , , nDestID) = 0 Then GoTo RollbackTran

                    oRs1.MoveNext()
                Loop

                oRs1.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy tasks

            oUI.BusyProgress(80, "Copying custom tasks")

            oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
            "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr WHERE " & _
            "PackID = " & nPackID & ")")

            Do While oRs.EOF = False

                If clsMarsData.DataItem.CopyRow("Tasks", oRs(0).Value, " WHERE TaskID =" & oRs(0).Value, "TaskID", , , , _
                 NewPackID, , , nScheduleID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            'copy exception stuff
            clsMarsData.DataItem.CopyRow("BlankReportAlert", nPackID, " WHERE PackID =" & nPackID, "BlankID", _
            , , , NewPackID)

            oRs.Close()

            'copy dynamic stuff
            clsMarsData.DataItem.CopyRow("DynamicAttr", nPackID, " WHERE PackID =" & nPackID, "DynamicID", , , 0, NewPackID)

            clsMarsData.DataItem.CopyRow("DynamicLink", nPackID, " WHERE PackID =" & nPackID, "LinkID", , , , NewPackID)

            'copy data driven stuffings
            clsMarsData.DataItem.CopyRow("DataDrivenAttr", nPackID, " WHERE PackID =" & nPackID, "DDID", , , , NewPackID)

            If CopyReports = True Then
                Dim nReportID As Integer
                Dim NewReportID As Integer
                Dim I As Integer = 1

                oUI.BusyProgress(90, "Copying packaged reports")

                oRs = clsMarsData.GetData("SELECT ReportID FROM ReportAttr" & sWhere & " ORDER BY PackOrderID")

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        nReportID = oRs.Fields(0).Value

                        NewReportID = CopyReport(nReportID, 0, True)

                        _Delay(1)

                        If NewReportID > 0 Then

                            Dim oName As ADODB.Recordset
                            Dim sName As String

                            oName = clsMarsData.GetData("SELECT ReportTitle FROM ReportAttr WHERE ReportID =" & NewReportID)

                            sName = oName(0).Value

                            oName.Close() : oName = Nothing

                            If clsMarsData.WriteData("UPDATE ReportAttr SET " & _
                            "PackID = " & NewPackID & "," & _
                            "ReportTitle = '" & I & ":" & SQLPrepare(sName) & "' " & _
                            " WHERE ReportID = " & NewReportID) = False Then GoTo RollbackTran

                            If clsMarsData.DataItem.CopyRow("PackagedReportAttr", nReportID, _
                            " WHERE ReportID =" & nReportID, "AttrID", _
                             , , NewReportID, NewPackID) = 0 Then GoTo RollbackTran

                            I += 1
                        End If

                        oRs.MoveNext()
                    Loop
                End If

                oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GoTo RollbackTran
        End Try

        oUI.BusyProgress(100, "Cleaning up")

        oUI.BusyProgress(, , True)

        Return NewPackID
RollbackTran:
        With clsMarsData.DataItem
            .WriteData("DELETE FROM PackageAttr WHERE PackID = " & NewPackID)
            .WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & NewPackID)
            .WriteData("DELETE FROM DestinationAttr WHERE PackID = " & NewPackID)
            .WriteData("DELETE FROM ReportAttr WHERE PackID = " & NewPackID)
            .WriteData("DELETE FROM PackagedReportAttr WHERE PackID = " & NewPackID)
            .WriteData("DELETE FROM Tasks WHERE ScheduleID = " & nScheduleID)
        End With

        oUI.BusyProgress(, , True)
        Return 0
    End Function

    Public Function RefreshReport(ByVal nReportID As Integer, Optional ByVal sPath As String = "") As Boolean
        Return True
    End Function
    Public Function CopyReport(ByVal nReportID As Integer, ByVal nParent As Integer, Optional ByVal PackCopy As Boolean = False) As Integer

        Dim SQL As String
        Dim sWhere As String = " WHERE ReportID = " & nReportID
        Dim sColumns As String
        Dim sValues As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer
        Dim ReportTitle As String = String.Empty
        Dim oUI As clsMarsUI = New clsMarsUI
        Dim nScheduleID As Int64
        Dim sOriginal As String
        Dim nDestID() As Integer
        Dim I As Integer = 0

        Try

            oRs = clsMarsData.GetData("SELECT ReportTitle,DatabasePath FROM " & _
            "ReportAttr WHERE ReportID =" & nReportID)

            If oRs.EOF = False Then
                ReportTitle = oRs.Fields(0).Value
                sOriginal = oRs.Fields(1).Value
            End If

            oRs.Close()

Retry:
            If PackCopy = False Or ReportTitle.Length = 0 Then
                ReportTitle = InputBox("Please enter the name of the copied report", _
                Application.ProductName, "Copy of " & ReportTitle)
            Else
                Try
                    ReportTitle = ReportTitle.Split(":")(1)
                Catch : End Try
            End If

            If ReportTitle.Length = 0 Then Return 0


            If clsMarsData.IsDuplicate("ReportAttr", "ReportTitle", ReportTitle, True) = True And PackCopy = False Then
                Dim oRes As DialogResult

                oRes = MessageBox.Show("A report with that name already exists", _
                Application.ProductName, MessageBoxButtons.RetryCancel, _
                MessageBoxIcon.Exclamation)

                If oRes = DialogResult.Cancel Then
                    Return 0
                Else
                    GoTo Retry
                End If
            End If

            'copy the report attributes----------------------------------------------------------------
            oUI.BusyProgress(30, "Copying report attributes...")

            Dim sCache As String

            sCache = Me.CacheReport(sOriginal)

            nID = clsMarsData.DataItem.CopyRow("ReportAttr", nReportID, sWhere, "ReportID", _
            "ReportTitle", ReportTitle, , , , , , , , nParent)

            If nID = 0 Then GoTo RollbackTran

            'copy the schedule attributes--------------------------------------------------------------

            oUI.BusyProgress(55, "Copying schedule data...")


            nScheduleID = clsMarsData.DataItem.CopyRow("ScheduleAttr", 0, sWhere, "ScheduleID", , , nID)

            If nScheduleID = 0 Then GoTo RollbackTran

            'copy the schedule options-----------------------------------------------------------------

            If clsMarsData.DataItem.CopyRow("ScheduleOptions", 0, " WHERE ScheduleID = (" & _
            "SELECT ScheduleID FROM ScheduleAttr WHERE ReportID =" & nReportID & ")", _
            "OptionID", , , , , , , nScheduleID) = 0 Then
                GoTo RollbackTran
            End If

            'copy the tasks-----------------------------------------------------------------------------
            oUI.BusyProgress(60, "Copying custom tasks")

            oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
            "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr WHERE " & _
            "ReportID = " & nReportID & ")")

            Do While oRs.EOF = False

                If clsMarsData.DataItem.CopyRow("Tasks", oRs.Fields(0).Value, "WHERE TaskID =" & oRs.Fields(0).Value, _
                "TaskID", , , , , , , nScheduleID) = 0 Then GoTo RollbackTran


                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy the destination attributes------------------------------------------------------------
            oUI.BusyProgress(75, "Copying destination data...")

            oRs = clsMarsData.GetData("SELECT DestinationID FROM DestinationAttr " & sWhere)

            Do While oRs.EOF = False
                ReDim Preserve nDestID(I)

                nDestID(I) = clsMarsData.DataItem.CopyRow("DestinationAttr", oRs(0).Value, _
                " WHERE DestinationID = " & oRs(0).Value, "DestinationID", , , nID)

                If nDestID(I) = 0 Then GoTo RollbackTran

                'printer attributes-------------------------------------------------------------------------
                sColumns = "PrinterID,ReportID,PackID,PrinterName,Copies,DestinationID"

                SQL = "SELECT DISTINCT PrinterID FROM PrinterAttr p INNER JOIN DestinationAttr d ON p.DestinationID = d.DestinationID WHERE " & _
                "d.ReportID  =" & nReportID & " AND d.DestinationType ='Printer'"

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                Do While oRs1.EOF = False

                    If clsMarsData.DataItem.CopyRow("PrinterAttr", oRs1(0).Value, " WHERE PrinterID =" & oRs1(0).Value, _
                    "PrinterID", , , nID, , , , , nDestID(I)) = 0 Then GoTo RollbackTran

                    oRs1.MoveNext()
                    ''console.writeLine(SQL)
                Loop

                oRs1.Close()

                oRs.MoveNext()

                I += 1
            Loop

            oRs.Close()

            'copy the report parameters-----------------------------------------------------------------

            oUI.BusyProgress(85, "Copying report parameters...")

            sColumns = "ParID,ReportID,ParName,ParValue,ParType"

            oRs = clsMarsData.GetData("SELECT ParID FROM ReportParameter" & sWhere & " ORDER BY ParID")

            Dim nMaxParamID As Integer = 0

            Do While oRs.EOF = False
                'If clsMarsData.DataItem.CopyRow("ReportParameter", oRs(0).Value, " WHERE ParID =" & oRs(0).Value, _
                Dim nParamID As Integer = clsMarsData.DataItem.CopyRow("ReportParameter", oRs(0).Value, " WHERE ParID =" & oRs(0).Value, _
                "ParID", , , nID, , , , , , , , , , nMaxParamID)

                If nParamID = 0 Then GoTo RollbackTran

                If nParamID > nMaxParamID Then nMaxParamID = nParamID

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy report datasources
            oUI.BusyProgress(87, "Copying datasources...")

            sColumns = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"

            oRs = clsMarsData.GetData("SELECT DatasourceID FROM ReportDatasource" & sWhere)

            Do While oRs.EOF = False
                If clsMarsData.DataItem.CopyRow("ReportDatasource", oRs(0).Value, " WHERE DatasourceID =" & oRs(0).Value, "DatasourceID", _
                , , nID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            oRs.Close()
            'dynamic schedule properties----------------------------------------------------------------

            clsMarsData.DataItem.CopyRow("DynamicAttr", 0, sWhere, "DynamicID", , , nID)

            clsMarsData.DataItem.CopyRow("DynamicLink", 0, sWhere, "LinkID", , , nID)

            'copy the blank alert settings-----------------------------------------------------------

            oUI.BusyProgress(95, "Cleaning up...")

            If clsMarsData.DataItem.CopyRow("BlankReportAlert", 0, sWhere, "BlankID", , , nID) = 0 Then GoTo RollbackTran

            'copy the snapshot settings--------------------------------------------------------------

            clsMarsData.DataItem.CopyRow("ReportSnapshots", 0, sWhere, "SnapID", , , nID)

            'copy the subreport parameters----------------------------------------------------------

            sColumns = "SubParID,ReportID,SubName,ParName,ParValue,ParType"

            oRs = clsMarsData.GetData("SELECT * FROM SubReportParameters " & sWhere)

            Do While oRs.EOF = False
                If clsMarsData.DataItem.CopyRow("SubReportParameters", oRs(0).Value, " WHERE SubParID =" & oRs(0).Value, _
                "SubParID", , , nID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy the login info the subreports--------------------------------------------------------
            sColumns = "SubLoginID,ReportID,SubName,RptServer,RptDatabase,RptUserID,RptPassword"

            oRs = clsMarsData.GetData("SELECT * FROM SubReportLogin " & sWhere)

            Do While oRs.EOF = False
                If clsMarsData.DataItem.CopyRow("SubReportLogin", oRs(0).Value, " WHERE SubLoginID =" & oRs(0).Value, _
                "SubLoginID", , , nID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy custom sections

            clsMarsData.DataItem.CopyRow("CustomSections", 0, sWhere, "ReportID", , , nID)

            'copy bursting shit_______________________________________________________________________

            sColumns = "BurstID,ReportID,DestinationID,GroupValue,GroupColumn"

            oRs = clsMarsData.GetData("SELECT * FROM BurstAttr " & sWhere)

            I = 0

            Do While oRs.EOF = False
                clsMarsData.DataItem.CopyRow("BurstAttr", oRs(0).Value, " WHERE BurstID =" & oRs(0).Value, _
                "BurstID", , , nID, , , , , nDestID(I))

                oRs.MoveNext()
                I += 1
            Loop

            'copy table logins
            clsMarsData.DataItem.CopyRow("ReportTable", nReportID, sWhere, "RptTableID")
            clsMarsData.DataItem.CopyRow("SubReportTable", nReportID, sWhere, "RptTableID")

            'copy report options
            clsMarsData.DataItem.CopyRow("ReportOptions", nReportID, sWhere, "OptionID", , , nID)

            System.Threading.Thread.Sleep(1000)

            'copy data driven properties
            clsMarsData.DataItem.CopyRow("DataDrivenAttr", nReportID, sWhere, "DDID", , , nID)

            Return nID
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
            Return 0
        End Try
RollbackTran:

        With clsMarsData.DataItem
            .WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nID, False)
            .WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nID, False)
            .WriteData("DELETE FROM DestinationAttr WHERE ReportID = " & nID, False)
            .WriteData("DELETE FROM ReportParameter WHERE ReportID = " & nID, False)
            .WriteData("DELETE FROM PrinterAttr WHERE ReportID =" & nID, False)
            .WriteData("DELETE FROM BlankReportAlert WHERE ReportID =" & nID, False)
            .WriteData("DELETE FROM DynamicLink WHERE ReportID =" & nID, False)
            .WriteData("DELETE FROM DynamicAttr WHERE ReportID =" & nID, False)
            .WriteData("DELETE FROM BlankReportAlert WHERE ReportID =" & nID, False)
            .WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID, False)
            .WriteData("DELETE FROM Tasks WHERE ScheduleID =" & nScheduleID, False)
            oUI.BusyProgress(, , True)
        End With
    End Function


    Public Sub SaveReportParameters(ByVal nReportID As Integer, ByVal oList As ListView)
        Dim SQL As String
        Dim lsv As ListViewItem
        Dim I As Integer = 1

        clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID = " & nReportID)

        For Each lsv In oList.Items
            SQL = "INSERT INTO ReportParameter(ParID,ReportID,ParName,ParValue,ParType) " & _
                "VALUES (" & _
                clsMarsData.CreateDataID("ReportParameter", "ParID") & "," & _
                nReportID & "," & _
                "'" & lsv.Text.Replace("'", "''") & "'," & _
                "'" & lsv.SubItems(1).Text.Replace("'", "''") & "'," & _
                lsv.Tag & ")"

            clsMarsData.WriteData(SQL)


        Next
    End Sub

    Public Function _DeferDelivery(ByVal DestinationID As Integer, _
    ByVal sPath As String, ByVal nDefer As Double, ByVal sFileTitle As String) As Boolean
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim DeferPath As String
        Dim Stamp As String
        Dim fileCollection As String = ""

        Try

            For Each s As String In sPath.Split("|")
                Stamp = "{" & clsMarsData.CreateDataID & "}"

                DeferPath = sAppPath & "Defer\"

                If System.IO.Directory.Exists(DeferPath) = False Then
                    System.IO.Directory.CreateDirectory(DeferPath)
                End If

                Dim sFilename As String = ExtractFileName(s)
                Dim nDot As Int32 = sFilename.IndexOf(".")

                sFilename = sFilename.Insert(nDot, Stamp)

                DeferPath &= sFilename

                System.IO.File.Copy(s, DeferPath)

                fileCollection &= DeferPath & "|"
            Next

            fileCollection = fileCollection.Substring(0, fileCollection.Length - 1)

            sCols = "DeferID,DestinationID,DueDate,FilePath,FileName"

            sVals = clsMarsData.CreateDataID("DeferID", "DeferDeliveryAttr") & "," & _
            DestinationID & "," & _
            "'" & ConDateTime(Date.Now.AddHours(nDefer)) & "'," & _
            "'" & SQLPrepare(fileCollection) & "'," & _
            "'" & SQLPrepare(sFileTitle) & "'"

            SQL = "INSERT INTO DeferDeliveryAttr (" & sCols & ") VALUES " & _
            "(" & sVals & ")"

            Return clsMarsData.WriteData(SQL, False)

        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = "clsMarsReport._DeferDelivery"
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return False
        End Try


    End Function



    Public Function DeliverDefered(ByVal nID As Integer) As Boolean
        Dim SQL As String

        SQL = "SELECT * FROM DeferDeliveryAttr D INNER JOIN " & _
        "DestinationAttr x ON d.DestinationID = x.DestinationID " & _
        "WHERE d.DeferID =" & nID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim sExport As String
        Dim sType As String
        Dim sSendTo As String
        Dim sSubject, senderName, senderAddress As String
        Dim sCc As String
        Dim sBcc As String
        Dim sExtras As String
        Dim sMsg As String
        Dim sName As String
        Dim sPath As String
        Dim Embed As Boolean
        Dim IncludeAttach As Boolean
        Dim MailFormat As String
        Dim ScheduleType As String = "Single"
        Dim nDeferBy As Double
        Dim oParse As New clsMarsParser
        Dim Ok As Boolean
        Dim sFilename As String = ""
        Dim sFile As String
        Dim nReportID As Integer
        Dim sFormat As String
        Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Integer
        Dim I As Integer = 0
        Dim oUI As clsMarsUI = New clsMarsUI

        rptFileNames = Nothing

        Try
            If oRs.EOF = False Then
                sExport = oRs("filepath").Value
                sFormat = oRs("outputformat").Value
                nReportID = oRs("reportid").Value
                'nScheduleID = oSchedule.GetScheduleID(nReportID)
                sName = oRs("filename").Value

                'lets copy the file to the output folder and rename it without the stamp
                Dim nStart As Int32
                Dim nEnd As Int32
                Dim Stamp As String
                Dim sDeferFile As String

                For Each s As String In sExport.Split("|")
                    sDeferFile = ExtractFileName(s)

                    nStart = sDeferFile.IndexOf("{")
                    nEnd = sDeferFile.IndexOf("}")

                    Stamp = sDeferFile.Substring(nStart, nEnd - nStart) & "}"

                    sDeferFile = sDeferFile.Replace(Stamp, String.Empty)

                    sDeferFile = Me.m_OutputFolder & sDeferFile

                    If System.IO.File.Exists(sDeferFile) Then System.IO.File.Delete(sDeferFile)

                    System.IO.File.Copy(s, sDeferFile, True)

                    ReDim Preserve rptFileNames(I)

                    rptFileNames(I) = sDeferFile

                    I += 1
                Next

                If rptFileNames Is Nothing Then
                    Throw New Exception("No files were found to deliver")
                End If

                If rptFileNames.GetUpperBound(0) = 0 Then
                    sExport = rptFileNames(0)
                    ScheduleType = "single"
                Else
                    ScheduleType = "package"
                End If

                sType = oRs("destinationtype").Value

                Select Case sType.ToLower
                    Case "email"
                        sSendTo = ResolveEmailAddress(oRs("sendto").Value)

                        sSubject = oParse.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)

                        sCc = ResolveEmailAddress(oRs("cc").Value)
                        sBcc = ResolveEmailAddress(oRs("bcc").Value)
                        sExtras = oParse.ParseString(oRs("extras").Value, , , , , , m_ParametersTable)
                        MailFormat = oRs("mailformat").Value
                        senderName = oParse.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
                        senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)

                        sMsg = oParse.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

                        Try
                            Embed = Convert.ToBoolean(oRs("embed").Value)
                        Catch ex As Exception
                            Embed = False
                        End Try

                        If sFormat = "HTML (*.htm)" Then
                            Try
                                IncludeAttach = Convert.ToBoolean(oRs("includeattach").Value)
                            Catch ex As Exception
                                IncludeAttach = True
                            End Try
                        Else
                            IncludeAttach = True
                        End If

                        If MailType = MarsGlobal.gMailType.MAPI Then
                            If ScheduleType = "single" Then
                                Ok = SendMAPI(sSendTo, _
                                sSubject, sMsg, ScheduleType, sExport, 1, sExtras, _
                                    sCc, sBcc, False, "", sFilename)
                            Else
                                Ok = SendMAPI(sSendTo, sSubject, sMsg, "Package", , I, sExtras, sCc, sBcc, False, , sName)
                            End If
                        ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = gMailType.SQLRDMAIL Then

                            Dim objSender As clsMarsMessaging = New clsMarsMessaging

                            If ScheduleType = "single" Then
                                Ok = objSender.SendSMTP(sSendTo, sSubject, _
                                sMsg, ScheduleType, sExport, 1, sExtras, sCc, _
                                sBcc, sFilename, Embed, "", , IncludeAttach, _
                                MailFormat, , , senderName, senderAddress)
                            Else
                                Ok = objSender.SendSMTP(sSendTo, sSubject, sMsg, "Package", , I, sExtras, sCc, sBcc, sName)
                            End If
                        ElseIf MailType = gMailType.GROUPWISE Then
                            If ScheduleType = "single" Then
                                Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, sExport, ScheduleType, _
                                sExtras, IncludeAttach, Embed, EmbedPath, True, 1, sFilename, MailFormat)
                            Else
                                Ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, "", "Package", sExtras)
                            End If
                        End If

                    Case "disk"

                        sPath = oParse.ParseString(oRs("outputpath").Value, , , , , , m_ParametersTable)

                        If ScheduleType = "single" Then

                            sFile = ExtractFileName(sExport)

                            For Each s As String In sPath.Split("|")
                                If s.Length > 0 Then
                                    If s.EndsWith("\") = False Then s &= "\"

                                    Ok = oParse.ParseDirectory(s)

                                    If ScheduleType = "Single" Then
                                        System.IO.File.Copy(sExport, s & sFile, True)
                                    Else
                                        For Each t As String In System.IO.Directory.GetFiles(HTMLPath)
                                            sFile = ExtractFileName(t)
                                            System.IO.File.Copy(t, s & sFile, True)
                                        Next
                                    End If
                                End If
                            Next
                        Else
                            For Each s As String In sPath.Split("|")
                                If s.Length > 0 Then
                                    If s.EndsWith("\") = False Then s &= "\"

                                    Ok = oParse.ParseDirectory(s)

                                    For Each t As String In rptFileNames
                                        sFile = ExtractFileName(t)
                                        IO.File.Copy(t, s & sFile, True)
                                    Next
                                End If
                            Next
                        End If
                    Case "ftp"
                        Dim oFtp As New clsMarsTask
                        Dim sFtp As String = ""
                        Dim FTPServer As String
                        Dim FTPUser As String
                        Dim FTPPassword As String
                        Dim FTPPath As String
                        Dim FTPType As String
                        Dim ftpCount As Integer = 0

                        FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
                        FTPUser = oParse.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
                        FTPPassword = oParse.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
                        FTPPath = oParse.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
                        FTPType = IsNull(oRs("ftptype").Value, "FTP")

                        oUI.BusyProgress(75, "Uploading report...")

                        ftpCount = FTPServer.Split("|").GetUpperBound(0)

                        If ftpCount > 0 Then
                            ftpCount -= 1
                        Else
                            ftpCount = 0
                        End If

                        For I = 0 To ftpCount
                            Dim l_FTPServer As String = FTPServer.Split("|")(I)
                            Dim l_FTPUser As String = FTPUser.Split("|")(I)
                            Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
                            Dim l_FTPPath As String = FTPPath.Split("|")(I)
                            Dim l_FTPType As String = FTPType.Split("|")(I)


                            If ScheduleType = "Single" Then
                                oFtp.FTPUpload(l_FTPServer, 21, l_FTPUser, _
                                l_FTPPassword, l_FTPPath, _
                                sExport, l_FTPType)
                            Else
                                For Each s As String In rptFileNames
                                    oFtp.FTPUpload(l_FTPServer, 21, l_FTPUser, _
                                        l_FTPPassword, l_FTPPath, _
                                        s, l_FTPType)
                                Next
                            End If
                        Next

                        Ok = True
                End Select

            End If

            Try
                For Each s As String In oRs("filepath").Value.ToString.Split("|")
                    System.IO.File.Delete(s)
                Next
            Catch : End Try

            Try
                clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr WHERE DeferID =" & nID, False)
            Catch : End Try

            oRs.Close()

            Return True

        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return False
        End Try

    End Function
    Public Sub ClearPar(ByVal oRpt As Object)
        Try
            For I As Integer = 1 To oRpt.ParameterFields.Count
                oRpt.ParameterFields(I).ClearCurrentValueAndRange()
            Next
        Catch : End Try
    End Sub

    Public Function CacheReport(ByVal sOriginal As String) As String
        Return sOriginal
    End Function

    Public Sub ViewReport(ByVal nReportID As Integer, Optional ByVal Dialog As Boolean = False, _
    Optional ByVal UserID As String = "", Optional ByVal Password As String = "", Optional ByVal viewDialog As Boolean = False)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim UseSavedData As Boolean
        Dim oRpt As ReportServer.ReportingService
        Dim IsDynamic As Boolean = False
        Dim sFormula As String = ""
        Dim oLogins() As ReportServer.DataSourceCredentials
        Dim sReportPath As String
        Dim sReportName As String

        gScheduleOwner = clsMarsScheduler.getScheduleOwner(nReportID, clsMarsScheduler.enScheduleType.REPORT)

        oRs = clsMarsData.GetData("SELECT packid FROM reportattr WHERE reportid = " & nReportID)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim packid As Integer = IsNull(oRs(0).Value, 0)

            If packid <> 0 Then
                gScheduleOwner = clsMarsScheduler.getScheduleOwner(packid, clsMarsScheduler.enScheduleType.PACKAGE)
            End If
        End If



        SQL = "SELECT * FROM ReportAttr WHERE ReportID = " & nReportID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                clsMarsUI.MainUI.BusyProgress(10, "Obtaining server information...")

                sReportPath = oRs("cachepath").Value
                sReportName = oRs("reportname").Value

                oRpt = New ReportServer.ReportingService

                oRpt.Timeout = -1 '3600000

                oRpt.Url = oRs("databasepath").Value

                'check to see if we need to use 2005 viewer
                m_rv = New Microsoft.Reporting.WinForms.ReportViewer
                m_rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote
                Dim actualUrl As String = ""
                Dim serverUrl As String = oRs("databasepath").Value

                For I As Integer = 0 To serverUrl.Split("/").GetUpperBound(0) - 1
                    actualUrl &= serverUrl.Split("/")(I) & "/"
                Next

                m_rv.ServerReport.ReportServerUrl = New Uri(actualUrl)
                m_rv.ServerReport.ReportPath = sReportPath
                m_rv.ServerReport.Timeout = -1 'oRpt.Timeout
                clsMarsUI.MainUI.BusyProgress(20, "Logging into report server...")

                Me.SetServerLogin(oRpt, nReportID)

                clsMarsUI.MainUI.BusyProgress(30, "Loading data sources...")

                oLogins = CreateDatasources(nReportID)

                Try
                    IsDynamic = Convert.ToBoolean(oRs("dynamic").Value)
                Catch
                    IsDynamic = False
                End Try

                oRs.Close()

                If IsDynamic = True Then

                    clsMarsUI.MainUI.BusyProgress(40, "Loading dynamic data...")

                    Dim sDynaCon As String
                    Dim DynaQuery As String

                    Me.m_DynamicSchedule = True

                    SQL = "SELECT * FROM DynamicAttr WHERE ReportID =" & nReportID

                    oRs = clsMarsData.GetData(SQL)

                    If oRs.EOF = False Then
                        sDynaCon = oRs("constring").Value
                        DynaQuery = oRs("query").Value
                        sKeyParameter = oRs("keyparameter").Value

                        If DynaQuery = "_" Or DynaQuery.Length = 0 Then
                            sKeyValue = oRs("keycolumn").Value
                            gKeyValue = sKeyValue
                        Else

                            If MessageBox.Show("You are about to preview a dynamic schedule where the key parameter " & _
                            "is populated by a database query." & vbCrLf & _
                            "The preview will use the first record from the resulting recordset", _
                            Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information) = DialogResult.Cancel Then
                                Return
                            End If

                            Dim oDynaCon As New ADODB.Connection
                            Dim oDynaRs As New ADODB.Recordset
                            oDynaCon.ConnectionTimeout = 0
                            oDynaCon.CommandTimeout = 0
                            oDynaCon.Open(sDynaCon.Split("|")(0), sDynaCon.Split("|")(1), sDynaCon.Split("|")(2))

                            oDynaRs.Open(DynaQuery, oDynaCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                            If oDynaRs.EOF = True Then
                                MessageBox.Show("The query for key parameters did not return any values", Application.ProductName, _
                                MessageBoxButtons.OK, MessageBoxIcon.Information)
                                Return
                            Else
                                sKeyValue = oDynaRs.Fields(0).Value
                                gKeyValue = sKeyValue
                            End If

                            oDynaRs.Close()
                        End If
                    End If
                End If

                clsMarsUI.MainUI.BusyProgress(50, "Setting parameters...")

                Dim oPars() As ReportServer.ParameterValue

                oPars = SetReportParameters(nReportID)

                If Me.m_ServerVersion2005 = False Then
                    Dim sPath As String = clsMarsReport.m_OutputFolder & clsMarsData.CreateDataID & ".pdf"

                    If Me.ReportServerRender(oRpt, "PDF", oPars, oLogins, sReportPath, sPath, Nothing) = False Then
                        Throw New Exception(gErrorDesc)
                    End If

                    Dim oView As New frmPreview

                    clsMarsUI.MainUI.BusyProgress(, , True)

                    oView.PreviewReport(sPath, sReportName, True)

                    Try
                        IO.File.Delete(sPath)
                    Catch : End Try
                Else
                    Dim oView As frmPreview = New frmPreview

                    clsMarsUI.MainUI.BusyProgress(, , True)

                    oView.PreviewReport(m_rv, sReportName, viewDialog)
                End If
            End If

        Catch ex As Exception
            If ex.Message.ToLower.IndexOf("has thrown an exception") > -1 Or Err.Number = 2147417851 Then
                _ErrorHandle("The username or password the report is using to log into the database may be incorrect." & _
                " This error may also occur if your report does not require login, " & _
                "but you have entered a username and password in SQL-RD", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                _GetLineNumber(ex.StackTrace), "1. Remove the logon credentials and test again.  Your report may not need these." & vbCrLf & _
                "2. If your report needs to logon, then check the username and password and ensure that they are correct.")
            Else
                _ErrorHandle(ex.ToString, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End If
        Finally
            gScheduleOwner = ""
            clsMarsUI.MainUI.BusyProgress(, , True)
        End Try
    End Sub

    Public Function PrintProcessor(ByVal sFormat As String, _
    ByVal oCrystal As Object, ByVal strNow As String, _
    ByVal nReportID As Integer, ByVal OutputFile As String, ByVal sReportName As String, _
    Optional ByVal sPackName As String = "", _
    Optional ByVal nDestinationID As Integer = 0, _
    Optional ByVal sExt As String = "", Optional ByVal sFormula As String = "", _
    Optional ByVal UseSavedData As Boolean = False, Optional ByVal nPackID As Integer = 0) As Boolean

    End Function

    Public Sub DesignReport(ByVal nReportID As Integer)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sPath As String

        SQL = "SELECT DatabasePath FROM ReportAttr WHERE ReportID = " & nReportID

        oRs = clsMarsData.GetData(SQL)

        MessageBox.Show("Remember to refresh any schedules using this report so that changes are carried through", _
        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.None)

        Try
            If oRs.EOF = False Then
                Dim oProc As New Process
                sPath = oRs(0).Value

                oProc.Start(sPath)
            End If

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message & vbCrLf & sPath, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
    Private Sub ExportToCheckBlank(ByVal oRpt As ReportServer.ReportingService, parameters() As ReportServer.ParameterValue, creds() As ReportServer.DataSourceCredentials, reportPath As String)
        Dim outputFile As String = m_OutputFolder & IO.Path.GetRandomFileName

        Try
            '//export to XML
            ReportServerRender(oRpt, "XML", parameters, creds, reportPath, outputFile)
        Catch : End Try

        Try
            IO.File.Delete("")
        Catch : End Try
    End Sub

    Private Overloads Function BlankReportByFileSize(oRpt As ReportServer.ReportingService, ByVal reportid As Integer, ByVal packid As Integer, parameters() As ReportServer.ParameterValue, creds() As ReportServer.DataSourceCredentials, reportPath As String) As Boolean
        Dim SQL As String
        Dim isblank As Boolean

        If packid > 0 Then
            SQL = "SELECT * FROM BlankReportAlert WHERE PackID =" & packid
        Else
            SQL = "SELECT * FROM BlankReportAlert WHERE ReportID =" & reportid
        End If

        Dim file As String = ""

        Dim ors As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim minSize As Long = 0

        If ors IsNot Nothing AndAlso ors.EOF = False Then
            minSize = IsNull(ors("FileSizeMinimum").Value, 0)
            Dim fileInfo As IO.FileInfo

            If file <> "" Then
                fileInfo = New IO.FileInfo(file)

                If minSize > fileInfo.Length Then isblank = True
            Else
                file = m_OutputFolder & IO.Path.GetRandomFileName

                '//create the XML file for comparison
                Try
                    ReportServerRender(oRpt, "XML", parameters, creds, reportPath, file)
                Catch ex As Exception
                    Return False
                End Try

                fileInfo = New IO.FileInfo(file)

                If minSize > fileInfo.Length Then
                    isblank = True
                Else
                    isblank = False
                End If
            End If
        End If

        Return isblank
    End Function
    Public Function _BlankReport(oRpt As ReportServer.ReportingService, pars() As ReportServer.ParameterValue, creds() As ReportServer.DataSourceCredentials, _
                                 reportPath As String, Optional ByVal nReportID As Integer = 0, _
    Optional ByVal nPackID As Integer = 0) As Boolean
        Try
            Dim rptTest As Object
            Dim SendTo As String
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim oParse As New clsMarsParser
            Dim sMsg As String
            Dim szSubject As String
            Dim sQuery As String = ""
            Dim useFileSizeCheck As Boolean = False
            Dim rptBlank As Boolean

10:         If nReportID > 0 Then
20:             SQL = "SELECT * FROM BlankReportAlert WHERE ReportID =" & nReportID
30:         Else
40:             SQL = "SELECT * FROM BlankReportAlert WHERE PackID =" & nPackID
50:         End If

60:         oRs = clsMarsData.GetData(SQL)

70:         If oRs.EOF = True Then Return False

80:         If oRs.EOF = False Then
                Dim CustomQuery As String = IsNull(oRs("customquerydetails").Value)

                Try
                    useFileSizeCheck = Convert.ToBoolean(Convert.ToInt32(IsNull(oRs("UseFileSizeCheck").Value, 0)))
                Catch ex As Exception
                    useFileSizeCheck = False
                End Try

                If useFileSizeCheck = True Then
                    rptBlank = BlankReportByFileSize(oRpt, nReportID, nPackID, pars, creds, reportPath)

                    Return rptBlank
                ElseIf CustomQuery.Length > 0 And CustomQuery.Split("|").Length = 4 And useFileSizeCheck = False Then
90:                 Dim dRs As ADODB.Recordset = New ADODB.Recordset
100:                Dim dCon As ADODB.Connection = New ADODB.Connection
110:                Dim dSQL As String

                    dCon.ConnectionTimeout = 0
                    dCon.CommandTimeout = 0
112:                dCon.Open(CustomQuery.Split("|")(0), CustomQuery.Split("|")(1), _DecryptDBValue(CustomQuery.Split("|")(2)))
113:                sQuery = clsMarsParser.Parser.ParseString(CustomQuery.Split("|")(3), , True, , , , m_ParametersTable, , True)
114:

132:                dSQL = sQuery

140:                dRs.Open(dSQL, dCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

150:                If dRs.EOF = True Then
160:                    _BlankReport = True

                        '170:                    Select Case oRs("type").Value.ToLower
                        '                            Case "alert", "alertandreport"
                        '180:                            Dim oMsg As New clsMarsMessaging

                        '190:                            SendTo = oRs("to").Value

                        '200:                            SendTo = oParse.ParseString(SendTo, , , , , , m_ParametersTable)
                        '210:                            SendTo = oMsg.ResolveEmailAddress(SendTo)

                        '220:                            sMsg = oRs("msg").Value
                        '230:                            szSubject = oRs("subject").Value

                        '240:                            sMsg = oParse.ParseString(sMsg, , , , , , m_ParametersTable)
                        '250:                            szSubject = oParse.ParseString(szSubject, , , , , , m_ParametersTable)

                        '260:                            Select Case MailType
                        '                                    Case MarsGlobal.gMailType.MAPI
                        '270:                                    oMsg.SendMAPI(SendTo, szSubject, sMsg, "Single")
                        '280:                                Case MarsGlobal.gMailType.SMTP, MarsGlobal.gMailType.SQLRDMAIL
                        '                                        oMsg.SendSMTP(SendTo, szSubject, sMsg, "Single")
                        '                                    Case gMailType.GROUPWISE
                        '                                        oMsg.SendGROUPWISE(SendTo, "", "", szSubject, sMsg, "", "Single")
                        '                                End Select
                        '                        End Select
320:                Else
330:                    _BlankReport = False
                    End If

340:                dRs.Close()
350:                dCon.Close()

360:                dCon = Nothing
370:                dRs = Nothing

                    Return _BlankReport
                Else
                    Return False
380:            End If
            Else
131:            Return False
            End If
390:        oRs.Close()

400:        Return _BlankReport

410:    Catch ex As Exception
            _ErrorHandle("Error determining blank report status...:" & ex.Message, Err.Number, ex.TargetSite.GetCurrentMethod.Name, Erl)
            Return False
        End Try

    End Function

    Public Function RefreshPackage(ByVal nPackID As Integer) As Boolean
        Dim nReportID As Integer
        Dim I As Integer = 1
        Dim ok As Boolean
        Dim oUI As clsMarsUI = New clsMarsUI

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE " & _
        "PackID =" & nPackID, ADODB.CursorTypeEnum.adOpenStatic)

        Try
            Do While oRs.EOF = False

                oUI.BusyProgress((I / oRs.RecordCount) * 100, "Refreshing report " & I & " of " & oRs.RecordCount)

                nReportID = oRs.Fields(0).Value

                ok = RefreshReport(nReportID)

                If ok = False Then GoTo Ooop

                oRs.MoveNext()
                I += 1
            Loop

            oRs.Close()

        Catch
        End Try

        Return ok
Ooop:
        Return False
    End Function

    Public Sub SetReportSections(ByVal oRpt As Object, ByVal nReportID As Integer)

        Try
            Dim oRs As New ADODB.Recordset
            Dim SQL As String
            Dim nSection As Integer


            SQL = "SELECT * FROM CustomSections WHERE ReportID = " & nReportID & " ORDER BY SectionNo"

            oRs = clsMarsData.GetData(SQL)

            Do While oRs.EOF = False

                nSection = oRs("sectionno").Value

                oRpt.Sections(nSection).Suppress = Convert.ToBoolean(oRs("suppress").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing
        Catch : End Try
    End Sub
    Public Function ODBCProcessing(ByVal sFile As String, ByVal destinationID As Integer) As Boolean
        Dim SQL As String = "SELECT * FROM DestinationAttr WHERE DestinationID =" & destinationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

10:     If oRs IsNot Nothing Then
20:         If oRs.EOF = False Then
                Dim sDSN, sUserName, sPassword, sTable As String
30:             Dim cmb As ComboBox = New ComboBox
                Dim overWrite As Boolean = False

40:             sDSN = clsMarsParser.Parser.ParseString(oRs("sendto").Value, , , , , , m_ParametersTable)
50:             sUserName = clsMarsParser.Parser.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
60:             sPassword = _DecryptDBValue(oRs("cc").Value)
70:             sTable = clsMarsParser.Parser.ParseString(oRs("message").Value, , , , , , m_ParametersTable)
80:             overWrite = Convert.ToBoolean(oRs("copies").Value)

90:             clsMarsData.DataItem.GetTables(cmb, sDSN, sUserName, sPassword)

                Dim cmbItems As ComboBox.ObjectCollection = cmb.Items
100:            Dim objCon As ADODB.Connection = New ADODB.Connection
                objCon.ConnectionTimeout = 0
                objCon.CommandTimeout = 0
110:            objCon.Open(sDSN, sUserName, sPassword)

                Dim sContents As String = ReadTextFromFile(sFile)
                Dim rows() As String = sContents.Split(vbCrLf)
                Dim sCols As String = ""
                Dim justDeleted As Boolean = False
                Dim tableExists As Boolean = False

120:            For Each o As String In cmbItems

130:                If o.StartsWith("[") = False And o.EndsWith("[") = False Then o = "[" & o & "]"

140:                If o.ToLower = sTable.ToLower Then
150:                    tableExists = True
160:                    Exit For
                    End If
170:            Next

180:            If tableExists And overWrite Then
190:                SQL = "DROP TABLE " & sTable

200:                Try
210:                    objCon.Execute(SQL)

220:                    justDeleted = True
                    Catch : End Try
                End If

230:            If Not tableExists Or justDeleted Then
240:                SQL = "CREATE TABLE " & sTable & " (" & vbCrLf

250:                For Each col As String In rows(0).Split("�")
260:                    SQL &= "[" & col & "] VARCHAR(255)," & vbCrLf
270:                Next

280:                SQL = SQL.Remove(SQL.Length - 3, 3) & ")"

290:                objCon.Execute(SQL)
                End If

300:            For Each col As String In rows(0).Split("�")
310:                sCols &= "[" & col & "],"
320:            Next

330:            sCols = sCols.Remove(sCols.Length - 1, 1)

                Dim count As Integer = 0

340:            For Each row As String In rows
350:                If count = 0 Then GoTo skip

                    Dim sVals As String = ""

360:                If Asc(row.Substring(0, 1)) = 10 Then row = row.Remove(0, 1)

370:                For Each val As String In row.Split("�")
380:                    sVals &= "'" & SQLPrepare(val) & "',"
390:                Next

400:                sVals = sVals.Remove(sVals.Length - 1, 1)

410:                SQL = "INSERT INTO " & sTable & " (" & sCols & ") VALUES (" & sVals & ")"

420:                Try
430:                    objCon.Execute(SQL)
440:                Catch ex As Exception
450:                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 1)
                    End Try
skip:
460:                count += 1
470:            Next

480:            objCon.Close()

490:            objCon = Nothing
            End If
        End If
    End Function

    Private Sub ClearThreads()
        m_ThreadsAborted = False
        m_ThreadMsg = ""
        m_Threads.Clear()
        m_ThreadResults.Clear()
    End Sub

    Private Function GetThreadCount() As Integer
        Return m_Threads.Count()
    End Function

    Private Sub AddThreadResult(ByVal result As Object)
        SyncLock m_AddThreadResultLock
            m_ThreadResults.Add(result)
        End SyncLock
    End Sub

    Private Sub AddThread(ByVal thrd As Thread)
        SyncLock m_AddThreadLock
            m_Threads.Add(thrd, thrd.ManagedThreadId.ToString())
        End SyncLock
    End Sub

    Private Sub RemoveThread(ByVal threadID As Integer)
        SyncLock m_RemoveThreadLock
            m_Threads.Remove(threadID.ToString())
        End SyncLock
    End Sub

    Private Sub AbortThreads(Optional ByVal threadID As Integer = -1)
        For Each thrd As Thread In m_Threads
            If (thrd.ManagedThreadId <> threadID) Then
                thrd.Abort()
                RemoveThread(thrd.ManagedThreadId)
            End If
        Next
        m_ThreadsAborted = True
    End Sub

    Private Function WaitForThreads(Optional ByVal minCount As Integer = 0, Optional ByVal bDoEvents As Boolean = True, Optional ByVal timeOutSecs As Integer = 0) As Boolean
        Dim start As Date = DateAndTime.Now
        Dim sleep As Date = DateAndTime.Now
        Dim rc As Boolean = True

        While (m_Threads.Count() > minCount)
            If (timeOutSecs > 0) Then
                If ((DateAndTime.Now.Second - start.Second) >= timeOutSecs) Then
                    rc = False
                    Exit While
                End If
            End If

            If (bDoEvents AndAlso (DateAndTime.Now.Second - sleep.Second) >= 3) Then
                Application.DoEvents()
                sleep = DateAndTime.Now
            End If
        End While

        Return rc
    End Function

    Private Sub RunSingleThread(ByVal obj As Object)
        Dim params As clsRunSingleParam = obj
        Dim exportFile As String
        Dim rpt As New clsMarsReport
        Dim errInfo As clsErrorInfo

        rpt.m_IsMultithreaded = True
        rpt.m_HistoryID = Me.m_HistoryID

        errInfo = rpt.RunSingle(exportFile, params.m_ReportID, 0, params.m_PackageName, params.m_PackID, params.m_ToPrinter, _
                                    params.m_PackageDateStamp, params.m_MergePDF, , params.m_ToFax, params.m_MergeXL, _
                                    params.m_AdjustStamp, params.m_PrintDate, params.m_Retry, params.m_Timeout, params.m_DDKeyValue, params.m_IsDataDriven)

        If exportFile.Length = 0 Or exportFile = "{error}" Then
            If params.m_FailOnOne = True Then
                clsMarsScheduler.globalItem.SetScheduleHistory(False, params.m_DDKeyValue & errInfo.ErrorDesc, , params.m_PackID, , , , Me.m_HistoryID)
                _ErrorHandle(errInfo.ErrorDesc, errInfo.ErrorNumber, errInfo.ErrorSource, errInfo.ErrorLine, errInfo.ErrorSuggest, , True)
                AbortThreads(Thread.CurrentThread.ManagedThreadId)
            Else
                _ErrorHandle(errInfo.ErrorDesc, errInfo.ErrorNumber, errInfo.ErrorSource, errInfo.ErrorLine, errInfo.ErrorSuggest, , True)
            End If
            m_ThreadMsg += errInfo.ErrorDesc + vbCrLf + vbCrLf
            exportFile = "{error}"
        End If

        If exportFile <> "{skip}" AndAlso exportFile <> "{error}" AndAlso exportFile <> "Printer" AndAlso exportFile.Length > 0 Then
            rpt.HTMLPath = GetDirectory(rpt.HTMLPath)

            If rpt.HTMLPath Is Nothing Then rpt.HTMLPath = String.Empty

            If rpt.HTMLPath.Length = 0 Then
                AddThreadResult(exportFile)
            Else
                For Each s As String In System.IO.Directory.GetFiles(rpt.HTMLPath)
                    AddThreadResult(s)
                Next
            End If
        End If

        RemoveThread(Thread.CurrentThread.ManagedThreadId)
        RemoveThreadDetail(params.m_ReportID, Process.GetCurrentProcess.Id())

    End Sub

    Private Sub AddThreadDetail(ByVal reportID As Integer, ByVal processID As Integer, Optional ByVal isThread As Integer = 0)
        Try
            Dim sql As String = "INSERT INTO ThreadManagerDetail (ReportID,ProcessID,IsThread) VALUES (" & reportID & "," & processID & "," & isThread & ")"
            clsMarsData.WriteData(sql)
        Catch : End Try
    End Sub

    Private Sub RemoveThreadDetail(ByVal reportID As Integer, ByVal processID As Integer)
        Try
            Dim sql As String = "DELETE FROM ThreadManagerDetail WHERE ReportID=" & reportID & " AND ProcessID=" & processID
            clsMarsData.WriteData(sql)
        Catch : End Try
    End Sub

End Class

Friend Class clsRunSingleParam
    Public m_ReportID As Integer
    Public m_DestinationID As Integer
    Public m_PackageName As String
    Public m_PackID As Integer
    Public m_ToPrinter As Boolean
    Public m_PackageDateStamp As String
    Public m_MergePDF As Boolean
    Public m_Format As String
    Public m_ToFax As Boolean
    Public m_MergeXL As Boolean
    Public m_AdjustStamp As Double
    Public m_PrintDate As String
    Public m_Retry As Integer
    Public m_Timeout As Integer
    Public m_DDKeyValue As String
    Public m_IsDataDriven As Boolean
    Public m_FailOnOne As Boolean

    Public Sub New(ByVal nReportID As Integer, _
                    ByVal nDestinationID As Integer, _
                    ByVal sPackageName As String, _
                    ByVal nPackID As Integer, _
                    Optional ByVal ToPrinter As Boolean = False, _
                    Optional ByVal sPackageDateStamp As String = "", _
                    Optional ByVal MergePDF As Boolean = False, _
                    Optional ByVal sFormat As String = "", _
                    Optional ByVal ToFax As Boolean = False, _
                    Optional ByVal MergeXL As Boolean = False, _
                    Optional ByVal adjustStamp As Double = 0, _
                    Optional ByVal PrintDate As String = "", _
                    Optional ByVal nRetry As Integer = 0, _
                    Optional ByVal rptTimeout As Integer = 60, _
                    Optional ByVal DDKeyValue As String = "", _
                    Optional ByVal IsDataDriven As Boolean = False, _
                    Optional ByVal FailOnOne As Boolean = False)
        m_ReportID = nReportID
        m_DestinationID = nDestinationID
        m_PackageName = sPackageName
        m_PackID = nPackID
        m_ToPrinter = ToPrinter
        m_PackageDateStamp = sPackageDateStamp
        m_MergePDF = MergePDF
        m_Format = sFormat
        m_ToFax = ToFax
        m_MergeXL = MergeXL
        m_AdjustStamp = adjustStamp
        m_PrintDate = PrintDate
        m_Retry = nRetry
        m_DDKeyValue = DDKeyValue
        m_IsDataDriven = IsDataDriven
        m_FailOnOne = FailOnOne
        m_Timeout = rptTimeout
    End Sub
End Class

