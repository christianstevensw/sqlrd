Friend Class clsMarsTimeZones
    Public Shared ReadOnly Property m_useRelativeTimeZones() As Boolean
        Get

            If RunEditor = False Then
                Return False
            Else
                Dim val = clsMarsUI.MainUI.ReadRegistry("Use Relative Time", "0")

                Try
                    Integer.Parse(val)

                    Return Convert.ToBoolean(val)
                Catch ex As Exception
                    Return False
                End Try

            End If
        End Get
    End Property

    Public Shared ReadOnly Property m_masterZoneAdjust() As Decimal
        Get
            Try
                If sqlrd.gTimeOffSet <> 9999 Then Return gTimeOffSet

                Dim SQL As String = "SELECT TimeZone, UTCOffset FROM SystemTimeZone"
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs Is Nothing Then
                    Return TimeZone.CurrentTimeZone.GetUtcOffset(Now).TotalHours
                ElseIf oRs.EOF = True Then
                    Return TimeZone.CurrentTimeZone.GetUtcOffset(Now).TotalHours
                Else
                    Return oRs("utcoffset").Value
                End If
            Catch
                Return 0
            End Try
        End Get
    End Property
    '<DebuggerStepThrough()> _
    Public Shared Function ToMZone(ByVal theDate As Date) As Date

        If m_useRelativeTimeZones = False Then Return theDate

        Dim masterOffset As Decimal = m_masterZoneAdjust
        Dim userOffset As Decimal = TimeZone.CurrentTimeZone.GetUtcOffset(Now).TotalHours
        Dim newTime As Date
        Dim adjust As Decimal

        'first find the time difference between the two
        adjust = userOffset - masterOffset

        If adjust < 0 Then adjust = adjust * -1

        'now adjust the user user time - add if master is ahead or subtract if master is behind
        If userOffset > masterOffset Then
            newTime = theDate.AddHours(-adjust)
        ElseIf userOffset < masterOffset Then
            newTime = theDate.AddHours(adjust)
        Else
            newTime = theDate
        End If

        Return newTime
    End Function
    '<DebuggerStepThrough()> _
    Public Shared Function ToUZone(ByVal theDate As Date) As Date

        If m_useRelativeTimeZones = False Then Return theDate

        Dim masterOffset As Decimal = m_masterZoneAdjust
        Dim userOffset As Decimal = TimeZone.CurrentTimeZone.GetUtcOffset(Now).TotalHours
        Dim newTime As Date
        Dim adjust As Decimal

        'first find the time difference between the two
        adjust = userOffset - masterOffset

        If adjust < 0 Then adjust = adjust * -1

        'now adjust the user user time - add if master is ahead or subtract if master is behind
        If userOffset > masterOffset Then
            newTime = theDate.AddHours(adjust)
        ElseIf userOffset < masterOffset Then
            newTime = theDate.AddHours(-adjust)
        Else
            newTime = theDate
        End If

        Return newTime
    End Function
End Class
