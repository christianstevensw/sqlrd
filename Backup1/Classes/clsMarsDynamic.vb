Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports System.Data.Odbc
Friend Class clsMarsDynamic

    Public Shared oDynamic As New clsMarsDynamic
    Public Shared m_DynamicDataCache As DataTable
    Public Shared m_DynamicLinkCache As DataTable
    Public Shared m_CurrentKeyColumn As String
    Public Shared m_currentLinkColumn As String
    Public Shared m_currentValueColumn As String
    Public Shared m_diskCache As String = clsMarsReport.m_OutputFolder & "Dynamic Cache\"

    Function splitTableAndColumn(value As String) As String

    End Function

    Public Function _CreateDynamicString(ByVal sIn As String, ByVal nID As Integer, _
           ByVal sKey As String, ByVal sValue As String, Optional ByVal sType As String = "Report")
10:     Try
            Dim sValues() As String
            Dim I As Integer = 0
            Dim nStart As Integer
            Dim nEnd As Integer
            Dim sFunction As String
            Dim tempVal As String = sIn

20:         If Me.m_DynamicLinkCache Is Nothing Then
                Dim errorMsg As String = "Using dynamic database values is not supported when using a static destination. Please use a data item instead"

30:             Throw New Exception(errorMsg)
            End If

40:         If m_currentLinkColumn.StartsWith("[") And m_currentLinkColumn.EndsWith("]") Then
50:             m_currentLinkColumn = m_currentLinkColumn.Remove(0, 1)
60:             m_currentLinkColumn = m_currentLinkColumn.Remove(m_currentLinkColumn.Length - 1, 1)
70:             m_currentLinkColumn = m_currentLinkColumn.Replace(" ", "_")
            End If

80:         Dim rows() As DataRow = Me.m_DynamicLinkCache.Select(Me.m_currentLinkColumn & " = '" & SQLPrepare(sValue) & "'")
90:         Dim row As DataRow = rows(0)

100:        Do While sIn.IndexOf("<[x]") > -1
110:            nStart = sIn.IndexOf("<[x]")

120:            nEnd = sIn.IndexOf(">", nStart) + 1

130:            sFunction = sIn.Substring(nStart, nEnd - nStart)

140:            Dim s As String = sFunction.Replace("<[x]", String.Empty).Replace(">", String.Empty)

150:            Dim sTable As String
160:            Dim sColumn As String

170:            If s.IndexOf(".") > -1 Then
180:                sTable = s.Split(".")(0)
190:                sColumn = s.Split(".")(s.Split(".").GetUpperBound(0))
200:            Else
210:                sColumn = s
220:            End If

230:            If sColumn.StartsWith("[") And sColumn.EndsWith("]") Then
240:                sColumn = sColumn.Remove(0, 1)
250:                sColumn = sColumn.Remove(sColumn.Length - 1, 1)
260:                sColumn = sColumn.Replace(" ", "_")
                End If

270:            Try
280:                sIn = sIn.Replace(sFunction, row(sColumn))
290:            Catch
300:                If sTable.Length > 0 Then
310:                    sIn = sIn.Replace(sFunction, row(sTable & "." & sColumn))
320:                End If
330:            End Try
340:        Loop
350:

360:        Return sIn
370:    Catch ex As Exception
380:        gErrorDesc = ex.Message
390:        gErrorLine = Erl()
400:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
410:        gErrorNumber = Err.Number
420:        Return Nothing
        End Try
    End Function

    Public Function _CreateDynamicString_deprecated(ByVal sIn As String, ByVal nID As Integer, _
           ByVal sKey As String, ByVal sValue As String, Optional ByVal sType As String = "Report")
        Dim LinkSQL As String
10:     Dim LinkRs As New ADODB.Recordset
20:     Dim LinkCon As New ADODB.Connection
        Dim sCon As String
        Dim SQL As String
        Dim sValues() As String
        Dim oRs As ADODB.Recordset
        Dim I As Integer = 0
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim sFunction As String
        Dim sCn As String
        Dim sUser As String
        Dim sPassword As String
        Dim tempVal As String = sIn
        Dim retryCount1 As Integer = 0
        Dim retryCount2 As Integer = 0

30:     If sType = "Report" Then
40:         SQL = "SELECT * FROM DynamicLink WHERE ReportID = " & nID
50:     Else
60:         SQL = "SELECT * FROM DynamicLink WHERE PackID = " & nID
        End If

70:     oRs = clsMarsData.GetData(SQL)

80:     Try
90:         If Not oRs Is Nothing Then
100:            If oRs.EOF = False Then
110:                LinkSQL = oRs("linksql").Value
120:                sCon = oRs("constring").Value

130:                sCn = sCon.Split("|")(0)
140:                sUser = sCon.Split("|")(1)
150:                sPassword = sCon.Split("|")(2)

RETRY1:
160:                Try
170:                    LinkCon.Open(sCn, sUser, sPassword)
180:                Catch ex As Exception
190:                    If ex.Message.ToLower.Contains("timeout expired") Then
200:                        If retryCount1 < 5 Then
210:                            _Delay(5)

220:                            Try
230:                                LinkCon.Close()
                                Catch : End Try

240:                            retryCount1 += 1

250:                            GoTo RETRY1
                            End If
260:                    Else
270:                        Throw ex
                        End If

                    End Try

280:                sValue = clsMarsData.DataItem.ConvertToFieldType(LinkSQL, LinkCon, oRs("keycolumn").Value, sValue)

                    '50:                 Try
                    '60:                     Int32.Parse(sValue)
                    '70:                 Catch ex As Exception
                    '71:                     sValue = SQLPrepare(sValue)
                    '80:                     sValue = "'" & sValue & "'"
                    '90:                 End Try

290:                If LinkSQL.ToLower.IndexOf("where") < 0 Then
300:                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & sValue
310:                Else
320:                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & sValue
330:                End If

340:                Dim n As Integer = LinkSQL.ToLower.IndexOf("from")

350:                LinkSQL = LinkSQL.Substring(n, LinkSQL.Length - n)

360:                LinkSQL = "SELECT * " & LinkSQL

RETRY2:
370:                Try
380:                    LinkRs.Open(LinkSQL, LinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)
390:                Catch ex As Exception
400:                    If ex.Message.ToLower.Contains("timeout expired") Then
410:                        If retryCount2 < 5 Then
420:                            _Delay(5)

430:                            retryCount2 += 1

440:                            GoTo RETRY2
450:                        Else
460:                            Throw ex
                            End If
                        End If
                    End Try

470:                If LinkRs.EOF = False Then

480:                    Do While sIn.IndexOf("<[x]") > -1
490:                        nStart = sIn.IndexOf("<[x]")

500:                        nEnd = sIn.IndexOf(">", nStart) + 1

510:                        sFunction = sIn.Substring(nStart, nEnd - nStart)

520:                        Dim s As String = sFunction.Replace("<[x]", String.Empty).Replace(">", String.Empty)

530:                        Dim sTable As String
540:                        Dim sColumn As String

550:                        If s.IndexOf(".") > -1 Then
560:                            sTable = s.Split(".")(0)
570:                            sColumn = s.Split(".")(1)
580:                        Else
590:                            sColumn = s
600:                        End If

610:                        sColumn = sColumn.Replace("[", "").Replace("]", "")

620:                        Try
630:                            sIn = sIn.Replace(sFunction, LinkRs(sColumn).Value)
640:                        Catch
650:                            If sTable.Length > 0 Then
660:                                sIn = sIn.Replace(sFunction, _
                                          LinkRs(sTable & "." & sColumn).Value)
670:                            End If
680:                        End Try
690:                    Loop
700:                End If

710:                LinkRs.Close()
720:                LinkCon.Close()

730:                LinkRs = Nothing
740:                LinkCon = Nothing
750:            End If
760:        End If

            SaveTextToFile(Date.Now & ": " & tempVal & " = " & sIn, sAppPath & "dynamicrun.debug", , True, True)

770:        Return sIn
780:    Catch ex As Exception
790:        gErrorDesc = ex.Message
800:        gErrorLine = Erl()
810:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
820:        gErrorNumber = Err.Number
830:        Return Nothing
        End Try

    End Function

    Public Function createDynamicDataset(ByVal sQuery As String, ByVal sDSN As String, ByVal sUser As String, _
           ByVal sPassword As String, ByVal cacheFilename As String, ByVal autoResume As Boolean, ByVal cacheExpire As Integer) As Boolean
10:     Try
20:
30:         Dim oCon As OdbcConnection = New OdbcConnection("DSN=" & sDSN & ";Uid=" & sUser & ";Pwd=" & sPassword & ";")
            Dim da As OdbcDataAdapter

            Dim retryCount As Integer = 0

40:         oCon.Open()

50:         clsMarsUI.BusyProgress(30, "Creating data set of key values...")

60:         If autoResume = True Then
                Dim diskCache As String = clsMarsDynamic.m_diskCache & cacheFilename

                If cacheExpire > 0 Then
                    Dim dtCreated As Date = IO.File.GetCreationTime(diskCache)

                    If Now.Subtract(dtCreated).TotalMinutes > cacheExpire Then
                        IO.File.Delete(diskCache)
                    End If
                End If

70:             If IO.File.Exists(diskCache) Then

80:                 m_DynamicDataCache = Me.deserializeCache(diskCache)

                    If m_DynamicDataCache IsNot Nothing Then
100:                    If m_DynamicDataCache.Rows.Count > 0 Then
110:                        Return True
                        End If
                    End If
                End If
            End If

RETRY:
120:        Try
                sQuery = clsMarsParser.Parser.ParseString(sQuery)
130:            da = New OdbcDataAdapter(sQuery, oCon)

                m_DynamicDataCache = New DataTable

                da.Fill(m_DynamicDataCache)
140:        Catch ex As Exception

                SaveTextToFile(Date.Now & ": " & sQuery & vbCrLf & ex.ToString, sAppPath & "dynamicDatasetsql.txt", , False, False)

150:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
160:                _Delay(3)
170:                retryCount += 1
180:                GoTo RETRY
190:            Else
200:                Throw ex
                End If
            End Try

360:        oCon.Close()
            da.Dispose()
            da = Nothing
370:        oCon = Nothing

380:        Return True
390:    Catch ex As Exception
400:        gErrorDesc = ex.Message
410:        gErrorNumber = Err.Number
420:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
430:        gErrorLine = Erl()
440:        Return False
        End Try
    End Function

    Public Function createDynamicDatasetLegacy(ByVal sQuery As String, ByVal sDSN As String, ByVal sUser As String, _
           ByVal sPassword As String, ByVal cacheFilename As String, ByVal autoResume As Boolean, ByVal cacheExpire As Integer) As Boolean
10:     Try
20:         Dim oRs As ADODB.Recordset = New ADODB.Recordset
30:         Dim oCon As ADODB.Connection = New ADODB.Connection
            Dim retryCount As Integer = 0

40:         oCon.Open(sDSN, sUser, sPassword)

50:         clsMarsUI.BusyProgress(30, "Creating data set of key values...")

60:         If autoResume = True Then
                Dim diskCache As String = clsMarsDynamic.m_diskCache & cacheFilename

                If cacheExpire > 0 Then
                    Dim dtCreated As Date = IO.File.GetCreationTime(diskCache)

                    If Now.Subtract(dtCreated).TotalMinutes > cacheExpire Then
                        IO.File.Delete(diskCache)
                    End If
                End If

70:             If IO.File.Exists(diskCache) Then

80:                 m_DynamicDataCache = Me.deserializeCache(diskCache)

                    If m_DynamicDataCache IsNot Nothing Then
100:                    If m_DynamicDataCache.Rows.Count > 0 Then
110:                        Return True
                        End If
                    End If
                End If
            End If

RETRY:
120:        Try
                sQuery = clsMarsParser.Parser.ParseString(sQuery)
130:            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
140:        Catch ex As Exception
                SaveTextToFile(Date.Now & ": " & sQuery, sAppPath & "dynamicDatasetsql.txt", , False, False)

150:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
160:                _Delay(3)
170:                retryCount += 1
180:                GoTo RETRY
190:            Else
200:                Throw ex
                End If
            End Try

210:        m_DynamicDataCache = New DataTable

220:        For I As Integer = 0 To oRs.Fields.Count - 1
230:            Try
                    ''console.writeLine(oRs.Fields(I).Name)

240:                Me.m_DynamicDataCache.Columns.Add(oRs.Fields(I).Name.Replace(" ", "_"))
                Catch : End Try
250:        Next

            Dim count As Integer = 1
            Dim total As Integer = oRs.RecordCount

260:        Do While oRs.EOF = False

270:            clsMarsUI.BusyProgress((count / total) * 100, "Creating data set of key values..." & count)

                Dim row As DataRow = Me.m_DynamicDataCache.Rows.Add

280:            For I As Integer = 0 To oRs.Fields.Count - 1
                    Dim fldName As String

290:                fldName = oRs.Fields(I).Name.Replace(" ", "_")

291:                MsgBox(fldName)
292:                MsgBox(oRs.Fields(I).Value)

293:                Dim value = oRs(fldName).Value '    IsNull(oRs.Fields(I).Value)

300:                row(fldName) = value
310:            Next

320:            count += 1

330:            oRs.MoveNext()
340:        Loop

350:        oRs.Close()
360:        oCon.Close()

370:        oCon = Nothing

380:        Return True
390:    Catch ex As Exception
400:        gErrorDesc = ex.Message
410:        gErrorNumber = Err.Number
420:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
430:        gErrorLine = Erl()
440:        Return False
        End Try
    End Function

    Public Function createDynamicLinkset(ByVal nID As Integer, ByVal oType As clsMarsScheduler.enScheduleType) As Boolean

        Dim oRs As ADODB.Recordset
        Dim sLinkColumn As String
        Dim LinkKeyColumn As String
        Dim retryCount As Integer = 0
        Dim logFile As String = "dynamicLinkCache.log"


10:     Try
            m_DynamicLinkCache = New DataTable("DynamicLinkCache")

            clsMarsDebug.writeToDebug(logFile, "Records in Datacache: " & m_DynamicDataCache.Rows.Count, False)

20:         If Me.m_DynamicDataCache.Rows.Count = 0 Then Return True

            clsMarsDebug.writeToDebug(logFile, "Type is " & oType, True)

30:         If oType = clsMarsScheduler.enScheduleType.REPORT Then
40:             oRs = clsMarsData.GetData("SELECT * FROM DynamicLink WHERE ReportID = " & nID)
50:         ElseIf oType = clsMarsScheduler.enScheduleType.PACKAGE Then
60:             oRs = clsMarsData.GetData("SELECT * FROM DynamicLink WHERE PackID =" & nID)
70:         Else
80:             Return False
            End If

            Dim sDSN, sUser, sPassword, sQuery As String


90:         If oRs IsNot Nothing Then
100:            If oRs.EOF = False Then
                    Dim conn As String = oRs("constring").Value
110:                sDSN = conn.Split("|")(0)
120:                sUser = conn.Split("|")(1)
130:                sPassword = conn.Split("|")(2)
140:                sQuery = oRs("linksql").Value
150:                m_currentValueColumn = oRs("valuecolumn").Value
160:                LinkKeyColumn = oRs("keycolumn").Value
170:            Else
                    clsMarsDebug.writeToDebug(logFile, "No link SQL found. Exiting with no link cache", True)
180:                Return False
                End If

190:            oRs.Close()
200:        Else
                clsMarsDebug.writeToDebug(logFile, "Recordset is Nothing", True)
210:            Return False
            End If

            clsMarsDebug.writeToDebug(logFile, "DSN: " & sDSN, True)

220:        If sDSN = "" Then Return True

230:        clsMarsUI.BusyProgress(30, "Obtaining key column data type....")

            clsMarsDebug.writeToDebug(logFile, "Connecting to user database", True)

            Dim da As OdbcDataAdapter
            Dim oCon As OdbcConnection = New OdbcConnection("DSN=" & sDSN & ";Uid=" & sUser & ";Pwd=" & sPassword & ";")
            Dim tmpTable As DataTable = New DataTable

            Dim n As Integer = sQuery.ToLower.IndexOf("from")

260:        sQuery = sQuery.Substring(n, sQuery.Length - n)

            Dim tmpQuery As String = "SELECT TOP 1 " & LinkKeyColumn & " " & sQuery

            clsMarsDebug.writeToDebug(logFile, "Opening connection", True)

270:        oCon.Open()

            Dim OracleCheck, OtherDBCheck As Boolean
RETRY1:
            clsMarsDebug.writeToDebug(logFile, "Query: " & tmpQuery, True)

280:        Try
                tmpQuery = clsMarsParser.Parser.ParseString(tmpQuery)
290:            da = New OdbcDataAdapter(tmpQuery, oCon)

                clsMarsDebug.writeToDebug(logFile, "Populating data table", True)

                da.Fill(tmpTable)
300:        Catch ex As Exception
                clsMarsDebug.writeToDebug(logFile, "Error: " & ex.ToString, True)

310:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
320:                _Delay(3)
330:                retryCount += 1
340:                GoTo RETRY1
                ElseIf OracleCheck = False Then
                    If tmpQuery.ToLower.Contains("where") Then
                        tmpQuery = "SELECT " & LinkKeyColumn & " " & sQuery & " AND (ROWNUM < 2)"
                    Else
                        tmpQuery = "SELECT " & LinkKeyColumn & " " & sQuery & " WHERE  ROWNUM < 2"
                    End If

                    OracleCheck = True

                    GoTo RETRY1
                ElseIf OtherDBCheck = False Then
                    tmpQuery = "SELECT " & LinkKeyColumn & " " & sQuery

                    OtherDBCheck = True

                    GoTo RETRY1
350:            Else
360:                Throw ex
                End If
            End Try

370:        retryCount = 0

            Dim fld As DataColumn = tmpTable.Columns(0)
            Dim inClause As String = "("

            Dim dataKeyColumn As String

380:        dataKeyColumn = Me.m_CurrentKeyColumn

            clsMarsDebug.writeToDebug(logFile, "Data key Column: " & dataKeyColumn, True)

390:        If dataKeyColumn.Contains(".") Then
400:            dataKeyColumn = dataKeyColumn.Split(".")(dataKeyColumn.Split(".").GetUpperBound(0))
            End If

410:        If dataKeyColumn.StartsWith("[") And dataKeyColumn.EndsWith("]") Then
420:            dataKeyColumn = dataKeyColumn.Remove(0, 1)
430:            dataKeyColumn = dataKeyColumn.Remove(dataKeyColumn.Length - 1, 1)
440:            dataKeyColumn = dataKeyColumn.Replace(" ", "_")
            End If

450:        If LinkKeyColumn.Contains(".") Then
460:            Me.m_currentLinkColumn = LinkKeyColumn.Split(".")(LinkKeyColumn.Split(".").GetUpperBound(0))
470:        Else
480:            Me.m_currentLinkColumn = LinkKeyColumn
            End If

            clsMarsDebug.writeToDebug(logFile, "Link Key Column: " & m_currentLinkColumn, True)

490:        For Each row As DataRow In Me.m_DynamicDataCache.Rows
500:            Select Case fld.DataType.FullName
                    Case "System.Int32", "System.Int16", "System.Int64", "System.Decimal"

510:                    inClause &= row(dataKeyColumn) & ","
520:                Case "System.DateTime"
530:                    inClause &= "'" & ConDateTime(row(dataKeyColumn)) & "',"
540:                Case Else
550:                    inClause &= "'" & SQLPrepare(row(dataKeyColumn)) & "',"
                End Select
560:        Next

570:        inClause = inClause.Remove(inClause.Length - 1, 1) & ")"

590:        sQuery = "SELECT * " & sQuery

600:        If Me.m_DynamicDataCache.Rows.Count > 0 Then
610:            If sQuery.ToLower.Contains("where") Then
620:                sQuery &= " AND " & LinkKeyColumn & " IN " & inClause
630:            Else
640:                sQuery &= " WHERE " & LinkKeyColumn & " IN " & inClause
                End If
            End If

            clsMarsDebug.writeToDebug(logFile, "Link Query: " & sQuery, True)

650:        clsMarsUI.BusyProgress(40, "Building linking data set...")

            tmpTable.Dispose()

RETRY2:
660:        Try
                clsMarsDebug.writeToDebug(logFile, "Populating link cache with data")

                da = New OdbcDataAdapter(sQuery, oCon)
                da.Fill(m_DynamicLinkCache)
                m_DynamicLinkCache.WriteXml(sAppPath & "\Logs\dynamiclinkcache.xml", XmlWriteMode.WriteSchema)
680:        Catch ex As Exception
                clsMarsDebug.writeToDebug(logFile, "Error: " & ex.ToString)

690:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
700:                _Delay(3)
710:                retryCount += 1
720:                GoTo RETRY2
730:            Else
740:                Throw ex
                End If
            End Try

890:        oCon.Close()
900:        oCon = Nothing

910:        Return True
920:    Catch ex As Exception
            clsMarsDebug.writeToDebug(logFile, "Fatal error: " & ex.ToString)
930:        gErrorDesc = ex.Message
940:        gErrorNumber = Err.Number
950:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
960:        gErrorLine = Erl()
970:        Return False
        End Try
    End Function

    Public Function createDynamicLinksetLegacy(ByVal nID As Integer, ByVal oType As clsMarsScheduler.enScheduleType) As Boolean
        Dim oRs As ADODB.Recordset
        Dim oCon As ADODB.Connection
        Dim sLinkColumn As String
        Dim LinkKeyColumn As String
        Dim retryCount As Integer = 0


10:     Try
20:         If Me.m_DynamicDataCache.Rows.Count = 0 Then Return True

30:         If oType = clsMarsScheduler.enScheduleType.REPORT Then
40:             oRs = clsMarsData.GetData("SELECT * FROM DynamicLink WHERE ReportID = " & nID)
50:         ElseIf oType = clsMarsScheduler.enScheduleType.PACKAGE Then
60:             oRs = clsMarsData.GetData("SELECT * FROM DynamicLink WHERE PackID =" & nID)
70:         Else
80:             Return False
            End If

            Dim sDSN, sUser, sPassword, sQuery As String

90:         If oRs IsNot Nothing Then
100:            If oRs.EOF = False Then
                    Dim conn As String = oRs("constring").Value
110:                sDSN = conn.Split("|")(0)
120:                sUser = conn.Split("|")(1)
130:                sPassword = conn.Split("|")(2)
140:                sQuery = oRs("linksql").Value
150:                m_currentValueColumn = oRs("valuecolumn").Value
160:                LinkKeyColumn = oRs("keycolumn").Value
170:            Else
180:                Return False
                End If

190:            oRs.Close()
200:        Else
210:            Return False
            End If

220:        If sDSN = "" Then Return True

230:        clsMarsUI.BusyProgress(30, "Obtaining key column data type....")

240:        oRs = New ADODB.Recordset
250:        oCon = New ADODB.Connection

            Dim n As Integer = sQuery.ToLower.IndexOf("from")

260:        sQuery = sQuery.Substring(n, sQuery.Length - n)

            Dim tmpQuery As String = "SELECT TOP 1 " & LinkKeyColumn & " " & sQuery

270:        oCon.Open(sDSN, sUser, sPassword)

RETRY1:
280:        Try
                tmpQuery = clsMarsParser.Parser.ParseString(tmpQuery)
290:            oRs.Open(tmpQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
300:        Catch ex As Exception
310:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
320:                _Delay(3)
330:                retryCount += 1
340:                GoTo RETRY1
                ElseIf ex.Message.ToLower.Contains("syntax") Then
                    tmpQuery = "SELECT " & LinkKeyColumn & " " & sQuery

                    GoTo RETRY1
350:            Else
360:                Throw ex
                End If
            End Try

370:        retryCount = 0

            Dim fld As ADODB.Field = oRs.Fields(0)
            Dim inClause As String = "("

            Dim dataKeyColumn As String

380:        dataKeyColumn = Me.m_CurrentKeyColumn

390:        If dataKeyColumn.Contains(".") Then
400:            dataKeyColumn = dataKeyColumn.Split(".")(1)
            End If

410:        If dataKeyColumn.StartsWith("[") And dataKeyColumn.EndsWith("]") Then
420:            dataKeyColumn = dataKeyColumn.Remove(0, 1)
430:            dataKeyColumn = dataKeyColumn.Remove(dataKeyColumn.Length - 1, 1)
440:            dataKeyColumn = dataKeyColumn.Replace(" ", "_")
            End If

450:        If LinkKeyColumn.Contains(".") Then
460:            Me.m_currentLinkColumn = LinkKeyColumn.Split(".")(1)
470:        Else
480:            Me.m_currentLinkColumn = LinkKeyColumn
            End If

490:        For Each row As DataRow In Me.m_DynamicDataCache.Rows
500:            Select Case fld.Type
                    Case ADODB.DataTypeEnum.adBigInt, ADODB.DataTypeEnum.adDecimal, _
           ADODB.DataTypeEnum.adDouble, ADODB.DataTypeEnum.adInteger, ADODB.DataTypeEnum.adNumeric, _
           ADODB.DataTypeEnum.adSingle, ADODB.DataTypeEnum.adSmallInt, ADODB.DataTypeEnum.adTinyInt, _
           ADODB.DataTypeEnum.adVarNumeric

510:                    inClause &= row(dataKeyColumn) & ","
520:                Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBDate, ADODB.DataTypeEnum.adDBTime, ADODB.DataTypeEnum.adDBTimeStamp
530:                    inClause &= "'" & ConDateTime(row(dataKeyColumn)) & "',"
540:                Case Else
550:                    inClause &= "'" & SQLPrepare(row(dataKeyColumn)) & "',"
                End Select
560:        Next

570:        inClause = inClause.Remove(inClause.Length - 1, 1) & ")"

580:        oRs.Close()

590:        sQuery = "SELECT * " & sQuery

600:        If Me.m_DynamicDataCache.Rows.Count > 0 Then
610:            If sQuery.ToLower.Contains("where") Then
620:                sQuery &= " AND " & LinkKeyColumn & " IN " & inClause
630:            Else
640:                sQuery &= " WHERE " & LinkKeyColumn & " IN " & inClause
                End If
            End If

650:        clsMarsUI.BusyProgress(40, "Building linking data set...")

RETRY2:
660:        Try
670:            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
680:        Catch ex As Exception
                SaveTextToFile(Date.Now & ": " & sQuery, sAppPath & "\dynamiclinksetsql.txt", , False, True)

690:            If ex.Message.ToLower.Contains("timeout") And retryCount < 5 Then
700:                _Delay(3)
710:                retryCount += 1
720:                GoTo RETRY2
730:            Else
740:                Throw ex
                End If
            End Try

750:        m_DynamicLinkCache = New DataTable

760:        For I As Integer = 0 To oRs.Fields.Count - 1
770:            Try
780:                Me.m_DynamicLinkCache.Columns.Add(oRs.Fields(I).Name.Replace(" ", "_"))
                Catch : End Try
790:        Next

            Dim count As Integer = 1
            Dim total As Integer = oRs.RecordCount

800:        Do While oRs.EOF = False

                clsMarsUI.BusyProgress((count / total) * 100, "Reading records into cache: " & count)

                Dim row As DataRow = Me.m_DynamicLinkCache.Rows.Add

810:            For I As Integer = 0 To oRs.Fields.Count - 1
                    Dim fldName As String

820:                fldName = oRs.Fields(I).Name.Replace(" ", "_")

830:                row(fldName) = IsNull(oRs.Fields(I).Value)
840:            Next

850:            count += 1
860:            oRs.MoveNext()
870:        Loop

880:        oRs.Close()
890:        oCon.Close()

900:        oCon = Nothing

910:        Return True
920:    Catch ex As Exception
930:        gErrorDesc = ex.Message
940:        gErrorNumber = Err.Number
950:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
960:        gErrorLine = Erl()
970:        Return False
        End Try
    End Function


    Public Function _GetDynamicData(ByVal nID As String, ByVal sKey As String, ByVal sValue As Object, _
           Optional ByVal sType As String = "Report") As String()
10:     Try
            Dim LinkSQL As String
20:         Dim LinkRs As New ADODB.Recordset
30:         Dim LinkCon As New ADODB.Connection
            Dim sCon As String
            Dim SQL As String
            Dim sValues() As String
            Dim oRs As ADODB.Recordset
            Dim I As Integer = 0
            Dim oType As ADODB.DataTypeEnum

            If m_currentLinkColumn Is Nothing Then
                If sType = "Report" Then
                    SQL = "SELECT keyColumn FROM DynamicLink WHERE ReportID = " & nID
                Else
                    SQL = "SELECT keyColumn FROM DynamicLink WHERE PackID = " & nID
                End If

                oRs = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = False Then
                        m_currentLinkColumn = IsNull(oRs(0).Value, "")
                    End If
                End If

                If m_currentLinkColumn.Contains(".") Then
                    Me.m_currentLinkColumn = m_currentLinkColumn.Split(".")(1)
                End If
            End If

40:         If m_currentLinkColumn.StartsWith("[") And m_currentLinkColumn.EndsWith("]") Then
50:             m_currentLinkColumn = m_currentLinkColumn.Remove(0, 1)
60:             m_currentLinkColumn = m_currentLinkColumn.Remove(m_currentLinkColumn.Length - 1, 1)
70:             m_currentLinkColumn = m_currentLinkColumn.Replace(" ", "_")
            End If

80:         Dim rows() As DataRow = Me.m_DynamicLinkCache.Select(Me.m_currentLinkColumn & "= '" & SQLPrepare(gKeyValue) & "'")

90:         Dim valueCol As String = Me.m_currentValueColumn

100:        If valueCol.Contains(".") Then
110:            valueCol = valueCol.Split(".")(1)
            End If

120:        If valueCol.StartsWith("[") And valueCol.EndsWith("]") Then
130:            valueCol = valueCol.Remove(0, 1)
140:            valueCol = valueCol.Remove(valueCol.Length - 1, 1)
150:            valueCol = valueCol.Replace(" ", "_")
            End If

160:        For Each row As DataRow In rows
170:            ReDim Preserve sValues(I)

180:            sValues(I) = IsNull(row(valueCol), "")

190:            I += 1
200:        Next

210:        If sValues Is Nothing Then
220:            ReDim sValues(0)

230:            sValues(0) = String.Empty
            End If

240:        Return sValues
250:    Catch ex As Exception
260:        gErrorDesc = ex.Message
270:        gErrorLine = Erl()
280:        gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
290:        gErrorNumber = Err.Number
300:        Return Nothing
        End Try

    End Function



    Public Function _GetDynamicData_deprecated(ByVal nID As String, ByVal sKey As String, ByVal sValue As Object, _
    Optional ByVal sType As String = "Report") As String()
        Try
            Dim LinkSQL As String
            Dim LinkRs As New ADODB.Recordset
            Dim LinkCon As New ADODB.Connection
            Dim sCon As String
            Dim SQL As String
            Dim sValues() As String
            Dim oRs As ADODB.Recordset
            Dim I As Integer = 0
            Dim oType As ADODB.DataTypeEnum

            If sType = "Report" Then
                SQL = "SELECT * FROM DynamicLink WHERE ReportID = " & nID
            Else
                SQL = "SELECT * FROM DynamicLink WHERE PackID = " & nID
            End If

5:          oRs = clsMarsData.GetData(SQL)

10:         If Not oRs Is Nothing Then
20:             If oRs.EOF = False Then
30:                 LinkSQL = oRs("linksql").Value
40:                 sCon = oRs("constring").Value

50:                 LinkCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), sCon.Split("|")(2))

70:                 sValue = clsMarsData.DataItem.ConvertToFieldType(LinkSQL, LinkCon, oRs("keycolumn").Value, sValue)

                    '50:                 Try
                    '60:                     Int32.Parse(sValue)
                    '70:                 Catch ex As Exception
                    '                        sValue = SQLPrepare(sValue)
                    '80:                     sValue = "'" & sValue & "'"
                    '90:                 End Try

100:                If LinkSQL.ToLower.IndexOf("where") < 0 Then
110:                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & sValue
120:                Else
130:                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & sValue
140:                End If

                    Try
                        sqlrd.MarsCommon.SaveTextToFile(Now & " " & LinkSQL, sAppPath & "dynamic_debug.log", , True)
                    Catch : End Try


160:                LinkRs.Open(LinkSQL, LinkCon)

                    If LinkRs.EOF = True Then
161:                    sqlrd.MarsCommon.SaveTextToFile(Now & " LinkSQL Returned no data", sAppPath & "dynamic_debug.log", , True)
                    End If

170:                Do While LinkRs.EOF = False
180:                    ReDim Preserve sValues(I)

190:                    sValues(I) = IsNull(LinkRs.Fields(0).Value, "")

200:                    LinkRs.MoveNext()

210:                    I += 1
220:                Loop

230:                LinkRs.Close()
240:            End If
250:        End If

260:        If sValues Is Nothing Then
270:            ReDim sValues(0)

280:            sValues(0) = String.Empty
290:        End If

300:        Return sValues
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorLine = Erl()
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorNumber = Err.Number
            Return Nothing
        End Try

    End Function

    Public Shared Sub serializeCache(ByVal cacheFilename As String, ByVal table As DataTable)
        Try
            Dim cacheFile As String = clsMarsDynamic.m_diskCache & cacheFilename

            If IO.File.Exists(cacheFile) Then
                IO.File.Delete(cacheFile)
            End If

            Dim fs As IO.FileStream = New IO.FileStream(cacheFile, FileMode.Create, FileAccess.ReadWrite)
            Dim bf As BinaryFormatter = New BinaryFormatter

            bf.Serialize(fs, table)

            fs.Close()
        Catch : End Try
    End Sub

    Public Shared Function deserializeCache(ByVal cacheFile As String) As DataTable
        Try
            Dim fs As FileStream = New FileStream(cacheFile, FileMode.Open, FileAccess.Read)

            Dim dt As DataTable
            Dim bf As BinaryFormatter = New BinaryFormatter

            dt = CType(bf.Deserialize(fs), DataTable)

            fs.Close()

            Return dt
        Catch
            Return Nothing
        End Try
    End Function
End Class
