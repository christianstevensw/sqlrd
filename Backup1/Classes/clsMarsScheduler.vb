Imports Microsoft.Win32
Imports DZACTXLib
Public Class clsMarsScheduler

    Public Shared globalItem As New clsMarsScheduler
    Dim WithEvents m_grid As Xceed.Grid.GridControl
    Public Shared m_ScheduleID As Integer = 0
    Public Shared m_progressID As String = ""
    'Public Shared ScheduleStart As Date

    Public Enum enScheduleType
        REPORT = 0
        PACKAGE = 1
        AUTOMATION = 2
        SMARTFOLDER = 3
        EVENTBASED = 4
        EVENTPACKAGE = 5
        NONE = 6
        FOLDER = 7
        DESTINATION = 8
    End Enum
    Public Enum enCalcType As Integer
        MEDIAN = 0
        AVERAGE = 1
        MINIMUM = 2
        MAXIMUM = 3
    End Enum
    Public Shared Function getScheduleNameFromDestination(ByVal destinationID As Integer) As String
        Dim SQL As String = "SELECT reportid, packid FROM destinationattr WHERE destinationid = " & destinationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Dim value As String
        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim reportid, packid As Integer

            reportid = IsNull(oRs("reportid").Value, 0)
            packid = IsNull(oRs("packid").Value, 0)

            oRs.Close()

            If packid <> 0 Then
                SQL = "SELECT packageName FROM packageattr WHERE packid =" & packid
            Else
                SQL = "SELECT reportName FROM reportattr WHERE reportid =" & reportid
            End If

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                value = IsNull(oRs(0).Value)

                Return value
            Else
                Return ""
            End If
        Else
            Return ""
        End If
    End Function
    Public Overloads Shared Function isScheduleLocked(ByVal id As Integer) As Boolean

        Dim SQL As String = "SELECT locked FROM scheduleattr WHERE scheduleid = " & id
        Dim rs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim locked As Boolean = False

        If rs IsNot Nothing AndAlso rs.EOF = False Then
            locked = IsNull(rs(0).Value, False)

            rs.Close()

            rs = Nothing
        End If

        Return locked
    End Function
    Public Overloads Shared Function isScheduleLocked(ByVal id As Integer, ByVal scheduleType As enScheduleType) As Boolean
        Dim where As String
        Dim locked As Boolean = False

        Select Case scheduleType
            Case enScheduleType.AUTOMATION
                where = " WHERE autoid = " & id
            Case enScheduleType.REPORT
                where = " WHERE reportid = " & id
            Case enScheduleType.EVENTPACKAGE
                where = " WHERE eventpackid = " & id
            Case enScheduleType.PACKAGE
                where = " WHERE packid = " & id
            Case Else
                Return False
        End Select

        Dim SQL As String = "SELECT locked FROM scheduleattr " & where
        Dim rs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If rs IsNot Nothing AndAlso rs.EOF = False Then
            locked = IsNull(rs(0).Value, False)

            rs.Close()

            rs = Nothing
        End If

        Return locked
    End Function
    Public Shared Function areAnyScheduleLocked() As Boolean
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT COUNT(*) FROM scheduleattr WHERE locked = 1")

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim count As Integer = oRs(0).Value

            oRs.Close()

            If count = 0 Then
                Return False
            Else
                Return True
            End If
        End If
    End Function

    Public Shared Function getScheduleOwner(ByVal id As Integer, ByVal scheduleType As clsMarsScheduler.enScheduleType) As String
        Dim column As String
        Dim table As String

        Select Case scheduleType
            Case enScheduleType.AUTOMATION
                column = "autoid"
                table = "automationattr"
            Case enScheduleType.EVENTBASED
                Return clsMarsEvent.getOwner(id)
            Case enScheduleType.EVENTPACKAGE
                column = "eventpackid"
                table = "eventpackageattr"
            Case enScheduleType.PACKAGE
                column = "packid"
                table = "packageattr"
            Case enScheduleType.REPORT
                column = "reportid"
                table = "reportattr"
            Case enScheduleType.SMARTFOLDER
                column = "smartid"
                column = "smartfolderattr"
            Case Else
                Return ""
        End Select

        Dim SQL As String = "SELECT owner FROM " & table & " WHERE " & column & " = " & id
        Dim oRs As ADODB.Recordset = clsMarsData.DataItem.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim owner As String = IsNull(oRs("owner").Value, "")

            oRs.Close()
            oRs = Nothing

            Return owner
        Else
            Return ""
        End If
    End Function

    Public Function getReportDuration(ByVal reportID As Integer, ByVal dataCount As Integer) As Double
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim values As ArrayList = New ArrayList

        SQL = "SELECT * FROM ReportDurationTracker WHERE ReportID =" & reportID

        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then Return 30

            If oRs.RecordCount < 3 Then Return 30

            Do While oRs.EOF = False
                Dim startTime As Date = oRs("startdate").Value
                Dim endTime As Date = oRs("enddate").Value
                Dim duration As Double = endTime.Subtract(startTime).TotalMinutes

                values.Add(duration)

                oRs.MoveNext()
            Loop

            If values.Count = 0 Then Return 30

            Dim result As Double = clsMarsMaths.Median(values)

            result = result + (result / 2)

            Return result * dataCount
        Else
            Return 30
        End If
    End Function
    Public Function getScheduleDuration(ByVal reportID As Integer, ByVal packID As Integer, ByVal autoID As Integer, _
        ByVal eventPackID As Integer, ByVal eventID As Integer, ByVal valueType As enCalcType, _
        Optional ByVal minHistory As Integer = 0) As Decimal
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim values As ArrayList = New ArrayList

        If eventID = 0 Then
            SQL = "SELECT * FROM ScheduleHistory WHERE ReportID =" & reportID & " AND PackID =" & packID & " AND " & _
            "AutoID =" & autoID & " AND EventPackID =" & eventPackID & " AND Success = 1"

            oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

            If oRs Is Nothing Then Return 30 '* scale

            If oRs.EOF = True Then Return 30

            If oRs.RecordCount < minHistory Then Return 30

            Do While oRs.EOF = False
                Try
                    Dim entryDate As Date = oRs("entrydate").Value
                    Dim startDate As Date = oRs("startdate").Value
                    Dim duration As Double = entryDate.Subtract(startDate).TotalMinutes

                    values.Add(duration)
                Catch : End Try
                oRs.MoveNext()
            Loop

            oRs.Close()

        Else
            SQL = "SELECT * FROM EventHistory WHERE EventID =" & eventID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return 30

            If oRs.EOF = True Then Return 30

            Do While oRs.EOF = False
                Dim entryDate As Date = oRs("lastfired").Value
                Dim startDate As Date = oRs("startdate").Value
                Dim duration As Double = entryDate.Subtract(startDate).TotalMinutes

                values.Add(duration)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        If values.Count = 0 Then Return 1

        Dim result As Double

        Select Case valueType
            Case enCalcType.MEDIAN
                result = clsMarsMaths.Median(values)
            Case enCalcType.AVERAGE
                result = clsMarsMaths.Average(values)
            Case enCalcType.MINIMUM
                result = clsMarsMaths.Minimum(values)
            Case enCalcType.MAXIMUM
                result = clsMarsMaths.Maximum(values)
            Case Else
                result = clsMarsMaths.Median(values)
        End Select

        If minHistory > 0 And valueType = enCalcType.MEDIAN Then
            result = result + (result / 2)
        End If

        Return result

    End Function

    Public Shared Function IsDateAnException(ByVal checkDate As Date, ByVal calendarName As String) As Boolean
        Dim SQL As String = "SELECT * FROM CalendarAttr WHERE CalendarName ='" & SQLPrepare(calendarName) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim IsHoliday As Boolean
        Dim holidayForm As String
        Dim year As Integer

        If oRs IsNot Nothing Then
            Dim dateValue As Date

            Do While oRs.EOF = False
                IsHoliday = IsNull(oRs("isholiday").Value, 0)
                holidayForm = IsNull(oRs("holidayform").Value, "")

                year = Now.Year

                If IsHoliday = True And holidayForm.Length > 0 Then
                    Dim countryCode As String = holidayForm.Split("|")(0)
                    Dim holidayName As String = holidayForm.Split("|")(1)
                    Dim oHol As Dls.Holidays.HolidayService = New Dls.Holidays.HolidayService
                    Dim ds As DataSet
                    Dim csshol As cssholidays.holiday = New cssholidays.holiday

                    If countryCode = "AUS" Or countryCode = "NZ" Or countryCode = "CA" Then
                        ds = csshol.GetHolidaysForYear(countryCode, year)
                    Else
                        ds = oHol.GetHolidaysForYear(countryCode, year)
                    End If


                    Dim dt As DataTable = ds.Tables(0)

                    Dim rows() As DataRow = dt.Select("Name ='" & SQLPrepare(holidayName) & "'")

                    If rows.Length = 0 Then
                        dateValue = oRs("calendardate").Value
                    Else
                        Dim row As DataRow = rows(0)

                        dateValue = row("Date")
                    End If

                    year += 1

                Else
                    dateValue = oRs("calendardate").Value
                End If

                If ConDate(dateValue) = ConDate(checkDate) Then
                    oRs.Close()
                    Return True
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Return False

    End Function
    Public Shared Function GetScheduleID(Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0, _
    Optional ByVal nAutoID As Integer = 0, Optional ByVal nSmartID As Integer = 0, Optional ByVal nEventPackID As Integer = 0) As Integer
        Dim SQL As String = ""
        Dim nScheduleID As Integer

        If nReportID > 0 Then
            SQL = "SELECT ScheduleID FROM ScheduleAttr WHERE ReportID = " & nReportID
        ElseIf nPackID > 0 Then
            SQL = "SELECT ScheduleID FROM ScheduleAttr WHERE PackID = " & nPackID
        ElseIf nAutoID > 0 Then
            SQL = "SELECT ScheduleID FROM ScheduleAttr WHERE AutoID = " & nAutoID
        ElseIf nSmartID > 0 Then
            SQL = "SELECT ScheduleID FROM ScheduleAttr WHERE SmartID = " & nSmartID
        ElseIf nEventPackID > 0 Then
            SQL = "SELECT ScheduleID FROM ScheduleAttr WHERE EventPackID = " & nEventPackID
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Try
            nScheduleID = oRs("scheduleid").Value
        Catch ex As Exception
            ''console.writeLine(ex.ToString)
            nScheduleID = 0
        End Try

        clsMarsData.DataItem.DisposeRecordset(oRs)

        Return nScheduleID

    End Function
    Public Sub processExecuteNowQueue()

        Dim logFile As String = "executenow.log"
10:     Try
            Dim SQL As String = "SELECT * FROM executeNowQue ORDER BY entrydate ASC"

20:         clsMarsDebug.writeToDebug(logFile, "----------------New Run-----------------------", True)

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

30:         gErrorDesc = ""
40:         gErrorLine = 0
50:         gErrorNumber = 0
60:         gErrorSource = ""
70:         gErrorSuggest = ""

80:         If oRs IsNot Nothing Then
90:             clsMarsDebug.writeToDebug(logFile, "oRs isnot nothing", True)

100:            Do While oRs.EOF = False
                    Try
110:                    clsMarsDebug.writeToDebug(logFile, "Records have been found", True)

                        Dim scheduleType As String = oRs("scheduletype").Value
                        Dim scheduleID As Integer = oRs("scheduleid").Value
120:                    Dim oreport As clsMarsReport = New clsMarsReport
                        Dim column As String = ""

                        Dim collaboration As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("collaboration", 0)) '//check if we are using collaboration
                        Dim collaborationServerID As String = clsMarsUI.MainUI.ReadRegistry("collaborationID", "") '//get our collaboration ID
                        Dim collaborationLeader As String = clsMarsUI.MainUI.ReadRegistry("CollaborationLeader", "") '//are the collaboration leader

                        If collaborationLeader = "" Or collaborationServerID = "" Then collaboration = False

                        Dim isLeader As Boolean = False

                        If String.Compare(Environment.MachineName, collaborationLeader, True) = 0 Then isLeader = True

                        If collaboration Then
                            Dim collaboServer As String = oRs("collaborationserver").Value '//the the collaboration server assign to run this schedule
                            Dim zombies As String() = getDeadCollabServers() '//get any dead collaboration servers

                            If collaboServer <> collaborationServerID Then '//if this server is not the server assigned the schedule
                                Dim skipRecord As Boolean = True

                                For Each z As String In zombies '// see if the collaborator assigned is a zombie
                                    If z = collaborationServerID Then
                                        If isLeader Then skipRecord = False '//don't skip it if its a zombie and we are the collaboration leader
                                    End If
                                Next

                                If skipRecord Then GoTo Skip
                            End If
                        End If

130:                    Select Case scheduleType.ToLower
                            Case "report"
140:                            column = "reportid"
150:                        Case "pack"
160:                            column = "packid"
170:                        Case "auto"
180:                            column = "autoid"
190:                        Case "event-package"
200:                            column = "eventpackid"
210:                        Case "event-based"
220:                            Dim evt As clsMarsEvent = New clsMarsEvent
230:                            evt.RunEvents6(scheduleID)
                                clsMarsData.WriteData("DELETE FROM executenowque WHERE scheduleid =" & scheduleID)
240:                            Return
                        End Select

                        clsMarsDebug.writeToDebug(logFile, "Key column: " & column, True)

250:                    SQL = "SELECT * FROM scheduleattr WHERE " & column & " = " & scheduleID

                        clsMarsDebug.writeToDebug(logFile, "Schedule selection query: " & SQL, True)

                        Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

260:                    If oRs1 IsNot Nothing AndAlso oRs1.EOF = False Then
270:                        clsMarsDebug.writeToDebug(logFile, "Schedule Info found", True)

                            Dim reportid, packid, autoid, eventpackid As Integer
                            Dim ok As Boolean

280:                        packid = IsNull(oRs1("PackID").Value, 0)
290:                        reportid = IsNull(oRs1("ReportID").Value, 0)
300:                        autoid = IsNull(oRs1("autoid").Value, 0)
310:                        eventpackid = IsNull(oRs1("eventpackid").Value, 0)

                            Dim historyID As Integer = 0

320:                        historyID = oreport.m_HistoryID

330:                        ScheduleStart = Now

340:                        clsMarsDebug.writeToDebug(logFile, "Running schdule", True)

350:                        Try
360:                            Select Case scheduleType.ToLower
                                    Case "pack"
370:                                    packid = scheduleID

                                        m_progressID = "Package:" & packid

380:                                    If clsMarsData.IsScheduleDynamic(packid, "Package") = True Then
390:                                        ok = oreport.RunDynamicPackageSchedule(packid)
400:                                    ElseIf clsMarsData.IsScheduleDataDriven(packid, "Package") = True Then
410:                                        ok = oreport.RunDataDrivenPackage(packid)
420:                                    Else
430:                                        ok = oreport.RunPackageSchedule(packid)
                                        End If
440:                                Case "report"
450:                                    reportid = scheduleID

                                        m_progressID = "Report:" & reportid

460:                                    SQL = "SELECT Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportID =" & reportid

                                        Dim oDynamic As ADODB.Recordset = clsMarsData.GetData(SQL)
                                        Dim sReportName As String = Me.GetScheduleName(reportid, enScheduleType.REPORT)

470:                                    Try
480:                                        If IsNull(oDynamic("dynamic").Value, 0) = "1" Then
490:                                            ok = oreport.RunDynamicSchedule(reportid)
500:                                        ElseIf IsNull(oDynamic("isdatadriven").Value, 0) = "1" Then
530:                                            ok = oreport.RunDataDrivenSchedule(reportid)
540:                                        Else
550:                                            ok = oreport.RunSingleSchedule(reportid)
                                            End If
560:                                    Catch ex As Exception
570:                                        ok = oreport.RunSingleSchedule(reportid)
                                        End Try

580:                                    oDynamic.Close()
590:                                Case "auto"

600:                                    Dim oTask As New clsMarsAutoSchedule

610:                                    oTask.ExecuteAutomationSchedule(autoid)

620:                                    ok = True
630:                                Case "event-package"
640:                                    Dim evt As clsMarsEvent = New clsMarsEvent

650:                                    ok = evt.RunEventPackage(eventpackid)
                                End Select
660:                        Catch ex As Exception
670:                            ok = False
                                clsMarsDebug.writeToDebug(logFile, "Error: " & ex.ToString, True)
                                clsMarsDebug.writeToDebug("scheduler.debug", "Schedule errored: " & ex.Message & " [" & Erl() & "]", True)
680:                            gErrorDesc = ex.Message & "; " & gErrorDesc
690:                            gErrorNumber = Err.Number
700:                            gErrorLine = Erl()
710:                            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
                            End Try

720:                        clsMarsDebug.writeToDebug(logFile, "Setting history for schedule ", True)

730:                        If ok = True Then
740:                            SetScheduleHistory(True, "", reportid, packid, autoid, 0, 0, historyID)
750:                        Else
760:                            SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", reportid, packid, autoid, 0, 0, historyID)
                            End If

770:                        clsMarsDebug.writeToDebug(logFile, "Removing from queue", True)

780:                        clsMarsData.WriteData("DELETE FROM executenowque WHERE scheduleid =" & scheduleID)
                        End If

790:                    oRs1.Close()

800:                    oRs1 = Nothing

                    Catch ex As Exception
                        clsMarsDebug.writeToDebug(logFile, "Error: " & ex.ToString & vbCrLf & "Line number: " & Erl(), True)
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
                    End Try
skip:
810:                oRs.MoveNext()
820:            Loop

830:            oRs.Close()

840:            oRs = Nothing
            End If
850:    Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            clsMarsDebug.writeToDebug(logFile, "Error: " & ex.ToString & vbCrLf & "Line number: " & Erl(), True)
        End Try
    End Sub
    Public Sub ScheduleThread(ByVal nID As Integer, ByVal oType As enScheduleType)
        Dim RunSuccess As Boolean
        Dim nScheduleID As Integer
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sFrequency As String
        Dim StartDate As Date
        Dim StartTime As Date
        Dim NextRun As Date
        Dim nRepeat As Double
        Dim sUntil As String
        Dim sCal As String = ""
        Dim IsCluster As Boolean = False
        Dim UseException As Boolean = False
        Dim ExceptionCal As String = ""
        Dim column As String = ""
        Dim oEvent As New clsMarsEvent
10:     Dim oReport As clsMarsReport = New clsMarsReport

20:     Try
30:         If clsMarsUI.MainUI.ReadRegistry("SystemEnvironment", "") = "SQL-RD Cluster" Then
40:             IsCluster = True
            End If

50:         If IsCluster = True Then
60:             clsMarsData.DataItem.CloseMainDataConnection()

                Dim ClusterTYpe As String = clsMarsUI.MainUI.ReadRegistry("ClusterType", "Slave")

                Dim ClusterMaster As String

70:             If ClusterTYpe = "Slave" Then
80:                 ClusterMaster = clsMarsUI.MainUI.ReadRegistry("ClusterMaster", "")
90:             Else
100:                ClusterMaster = sAppPath
                End If

110:            If ClusterMaster.EndsWith("\") = False Then ClusterMaster &= "\"

120:            gConfigFile = ClusterMaster & "sqlrdlive.config"

130:            sAppPath = ClusterMaster

                Dim ConType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT")

140:            gConType = ConType

150:            If ConType = "DAT" Then
160:                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ClusterMaster & "sqlrdlive.dat;Persist Security Info=False"
170:            Else
180:                sCon = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)
                End If

190:            clsMarsData.DataItem.OpenMainDataConnection()
            End If

200:        ScheduleStart = Now

201:

210:        Select Case oType
                Case enScheduleType.REPORT
                    m_progressID = "Report:" & nID

220:                nScheduleID = GetScheduleID(nID)

230:                If clsMarsData.IsDuplicate("ThreadManager", "ReportID", nID, False) = True Then Return

240:                TaskManager("Add", nID)
250:            Case enScheduleType.PACKAGE

                    m_progressID = "Package:" & nID

260:                nScheduleID = GetScheduleID(, nID)

270:                If clsMarsData.IsDuplicate("ThreadManager", "PackID", nID, False) = True Then Return

280:                TaskManager("Add", , nID)
290:            Case enScheduleType.AUTOMATION
                    m_progressID = "Automation:" & nID

300:                nScheduleID = GetScheduleID(, , nID)

310:                If clsMarsData.IsDuplicate("ThreadManager", "AutoID", nID, False) = True Then Return

320:                TaskManager("Add", , , nID)
330:            Case enScheduleType.SMARTFOLDER
340:                nScheduleID = GetScheduleID(, , , nID)

350:                If clsMarsData.IsDuplicate("ThreadManager", "SmartID", nID, False) = True Then Return

360:            Case enScheduleType.EVENTBASED
370:                nScheduleID = 0

380:                If clsMarsData.IsDuplicate("ThreadManager", "EventID", nID, False) = True Then Return

390:                column = "EventID"
400:            Case enScheduleType.EVENTPACKAGE

                    nScheduleID = GetScheduleID(, , , , nID)

                    m_progressID = "Event-Package:" & nID

410:                If clsMarsData.IsDuplicate("ThreadManager", "EventPackID", nID, False) = True Then Return

                    TaskManager("Add", , , , , nID)
            End Select

420:        If oType <> enScheduleType.EVENTBASED Then Me.ThreadManager(nID, "add", oType)

421:        m_ScheduleID = nScheduleID

430:        SQL = "SELECT * FROM ScheduleAttr WHERE ScheduleID =" & nScheduleID

440:        oRs = clsMarsData.GetData(SQL)

450:        If oRs Is Nothing Then Return

460:        If oType <> enScheduleType.EVENTBASED Then
470:            If oRs.EOF = False Then
480:                sFrequency = oRs("Frequency").Value
490:                StartDate = oRs("StartDate").Value
500:                StartTime = oRs("StartTime").Value
510:                NextRun = oRs("NextRun").Value
520:                sCal = IsNull(oRs("calendarname").Value)

530:                Try
540:                    UseException = Convert.ToBoolean(oRs("useexception").Value)
550:                    ExceptionCal = IsNull(oRs("exceptioncalendar").Value)
560:                Catch ex As Exception
570:                    UseException = False
                    End Try

580:                Try
590:                    nRepeat = IsNonEnglishRegionRead(oRs("repeatinterval").Value)
600:                Catch
610:                    nRepeat = 0
                    End Try

620:                Try
630:                    sUntil = ConTime(oRs("RepeatUntil").Value)
640:                Catch
650:                    sUntil = oRs("RepeatUntil").Value
                    End Try
                End If

660:            If Me.IsDateAnException(NextRun, ExceptionCal) = True And UseException = True Then
670:                RunSuccess = False
680:                gErrorDesc = "Schedule not executed as execution date was included in exception calendar"
690:                gErrorNumber = 0
700:                GoTo ExceptionCalendar
                End If

710:            Select Case oType
                    Case enScheduleType.AUTOMATION

720:                    Dim oTask As New clsMarsTask

730:                    RunSuccess = oTask.ProcessTasks(nScheduleID, "NONE")

740:                    TaskManager("Remove", , , nID)
750:                Case enScheduleType.PACKAGE

760:                    If clsMarsData.IsScheduleDynamic(nID, "Package") = True Then
770:                        RunSuccess = oReport.RunDynamicPackageSchedule(nID)
                        ElseIf clsMarsData.IsScheduleDataDriven(nID, "Package") = True Then
                            RunSuccess = oReport.RunDataDrivenPackage(nID)
780:                    Else
790:                        RunSuccess = oReport.RunPackageSchedule(nID)
                        End If

800:                    TaskManager("Remove", , nID)
810:                Case enScheduleType.REPORT

820:                    SQL = "SELECT Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportID =" & nID

                        Dim oDynamic As ADODB.Recordset = clsMarsData.GetData(SQL)

830:                    Try
840:                        If IsNull(oDynamic("dynamic").Value, 0) = 1 Then
850:                            RunSuccess = oReport.RunDynamicSchedule(nID)
860:                        ElseIf IsNull(oDynamic("isdatadriven").Value, 0) = "1" Then
890:                            RunSuccess = oReport.RunDataDrivenSchedule(nID)
900:                        Else

910:                            RunSuccess = oReport.RunSingleSchedule(nID)

920:                            If RunSuccess = False And gErrorDesc.Length = 0 Then
930:                                RunSuccess = True
                                End If

                            End If
940:                    Catch ex As Exception
950:                        RunSuccess = oReport.RunSingleSchedule(nID)
                        End Try

960:                    oDynamic.Close()

970:                    TaskManager("Remove", nID)
980:                Case enScheduleType.EVENTPACKAGE

1000:                   RunSuccess = oEvent.RunEventPackage(nID)

1010:                   TaskManager("Remove", , , , , nID)
1020:               Case enScheduleType.SMARTFOLDER
1030:                   Dim oSmart As New frmSmartReport

1040:                   RunSuccess = oSmart._GenerateReport(nID)
                End Select
1050:       Else
1070:           oEvent.RunEvents6(nID)
            End If

ExceptionCalendar:
1080:       If oType <> enScheduleType.EVENTBASED Then
1090:           NextRun = GetNextRun(nScheduleID, sFrequency, StartDate, StartTime, _
     Convert.ToBoolean(oRs("Repeat").Value), nRepeat, sUntil, sCal, , , , IsNull(oRs("repeatunit").Value, "hours"))

1100:           SQL = "UPDATE ScheduleAttr SET NextRun = '" & _
     ConDateTime(NextRun) & "' WHERE ScheduleID =" & nScheduleID

1110:           clsMarsData.WriteData(SQL, True, True)
            End If

1120:       Select Case oType
                Case enScheduleType.REPORT
1130:               If RunSuccess = True Then
1140:                   SetScheduleHistory(True, "", nID)
1150:               Else
1160:                   SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)

1170:                   If gErrorDesc.Contains("Report(s) were produced successfully but email failed") = False Then
                            Dim retryInt As Integer = clsMarsReport.getScheduleRetryInterval(nScheduleID, enScheduleType.REPORT)

1180:                       If retryInt > 0 Then
1190:                           clsMarsReport.LogRetry(nScheduleID, 0)
                            End If
                        End If
                    End If
1200:           Case enScheduleType.PACKAGE
1210:               If RunSuccess = True Then
1220:                   SetScheduleHistory(True, "", , nID, , , , oReport.m_HistoryID)
1230:               Else
1240:                   SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , nID, , , , oReport.m_HistoryID)

1250:                   If gErrorDesc.Contains("Report(s) were produced successfully but email failed") = False Then
                            Dim retryInt As Integer = clsMarsReport.getScheduleRetryInterval(nScheduleID, enScheduleType.PACKAGE)

1260:                       If retryInt > 0 Then
1270:                           clsMarsReport.LogRetry(nScheduleID, 0)
                            End If
                        End If
                    End If
1280:           Case enScheduleType.AUTOMATION
1290:               If RunSuccess = True Then
1300:                   SetScheduleHistory(True, "", , , nID)
1310:               Else
1320:                   SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , nID)
                    End If

1330:               clsMarsData.WriteData("DELETE FROM ThreadManager WHERE AutoID =" & nID, , True)
1340:           Case enScheduleType.SMARTFOLDER
1350:               If RunSuccess = True Then
1360:                   SetScheduleHistory(True, , , , , nID)
1370:               Else
1380:                   SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , , nID)
                    End If
1390:           Case enScheduleType.EVENTPACKAGE
1400:               If RunSuccess = True Then
1410:                   SetScheduleHistory(True, , , , , , nID, oEvent.m_HistoryID)
1420:               Else
1430:                   SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , , , nID, oEvent.m_HistoryID)
                    End If

1440:           Case enScheduleType.EVENTBASED

1460:               clsMarsData.WriteData("DELETE FROM ThreadManager WHERE EventID =" & nID)
            End Select

1470:       If oType <> enScheduleType.EVENTBASED Then Me.ThreadManager(nID, "remove", oType)
1480:   Catch ex As Exception
1490:       Me.ThreadManager(nID, "remove", oType)

1500:       _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try

1510:   oReport = Nothing
1520:   gScheduleName = String.Empty
    End Sub



    Public Sub OtherScheduleProcesses(Optional ByVal WithEvent As Boolean = True)

        Dim RunIt As Boolean
10:     Dim oUI As New clsMarsUI
20:     Dim oEvent As New clsMarsEvent
30:     Dim oMsg As clsMarsMessaging = New clsMarsMessaging
        Dim tracerFile As String = "systemmaintenance.debug"

        clsMarsDebug.writeToDebug(tracerFile, "Starting system maintenance", False)

        Me.m_progressID = "Backup:99999"

40:     Try
50:         TaskManager("Add", , , 99999)
            ThreadManager(99999, "Add", enScheduleType.AUTOMATION)
            '60:         clsMarsUI.MainUI.BusyProgress(10, "Processing schedule retries...")

            '70:         Me.scheduleRetries()

            Dim sType As String

80:         sType = oUI.ReadRegistry("SQL-RDService", "NONE")

90:         clsMarsUI.MainUI.BusyProgress(20, "Processing email queue...")

            '100:        If MailType <> MarsGlobal.gMailType.MAPI Then
            '110:            oMsg.ProcessEmailQueue()
            '120:        Else
            '130:            If sType <> "WindowsNT" Then
            '140:                oMsg.ProcessEmailQueue()
            '                End If
            '            End If

            clsMarsDebug.writeToDebug(tracerFile, "Email Queue", True)

            oMsg.ProcessEmailQueue()

150:        Try
160:            RunIt = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AutoBackUp", 0, , , True)))
170:        Catch
180:            RunIt = False
            End Try

190:        If RunIt = True Then
                clsMarsDebug.writeToDebug(tracerFile, "System backup", True)

200:            RunSystemBackup()
            End If

210:        Try
220:            RunIt = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AutoRefresh", 0)))
230:        Catch
240:            RunIt = False
            End Try

250:        If RunIt = True Then
260:            clsMarsUI.MainUI.BusyProgress(40, "Running system backup...")
                clsMarsDebug.writeToDebug(tracerFile, "Schedule refresh", True)
270:            RefreshSchedules()
            End If


280:        If MailType = gMailType.MAPI Then
290:            clsMarsUI.MainUI.BusyProgress(60, "Processing read receipts...")
                clsMarsDebug.writeToDebug(tracerFile, "Read/Delivery reciepts", True)
300:            clsReadReceipts.ProcessReceipts()
            End If

310:        clsMarsUI.MainUI.BusyProgress(80, "Processing deffered schedules...")

            clsMarsDebug.writeToDebug(tracerFile, "Deferred delivery", True)

320:        ScheduleDeferred()

330:        Dim oSys As New clsSystemTools

340:        clsMarsUI.MainUI.BusyProgress(90, "Processing House-keeping...")

            clsMarsDebug.writeToDebug(tracerFile, "Housekeeping", True)

350:        oSys._HouseKeeping()

            clsMarsDebug.writeToDebug(tracerFile, "Email Retry", True)

360:        clsMarsMessaging.StopEmailRetry()

            clsMarsDebug.writeToDebug(tracerFile, "Clear message queue", True)
361:        sqlrd.ChristianStevenMessaging.messageQ.cleanOldMessagesAsync()
            'clsMarsData.CleanThreadManager()
370:    Catch ex As Exception
380:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
390:    Finally
400:        TaskManager("Remove", , , 99999)
            ThreadManager(99999, "Remove", enScheduleType.AUTOMATION)
        End Try
    End Sub


    Public Sub DisableSchedule(ByVal nScheduleID As Integer)
        Try
            Dim SQL As String

            SQL = "UPDATE ScheduleAttr SET Status = 0 WHERE ScheduleID = " & nScheduleID

            clsMarsData.WriteData(SQL)
        Catch : End Try
    End Sub
    Public Sub EnableSchedule(ByVal nScheduleID As Integer)
        Try
            Dim SQL As String

            SQL = "UPDATE ScheduleAttr SET Status = 1 WHERE ScheduleID = " & nScheduleID

            clsMarsData.WriteData(SQL)
        Catch : End Try
    End Sub

    Public Overloads Shared Function GetScheduleName(ByVal ID As String, ByVal type As enScheduleType) As String
        Dim SQL As String
        Dim sTable As String
        Dim sCol As String
        Dim sKey As String

        Try
            Select Case type
                Case enScheduleType.AUTOMATION
                    sTable = "automationattr"
                    sCol = "autoname"
                    sKey = "autoid"
                Case enScheduleType.EVENTBASED
                    sTable = "eventattr6"
                    sCol = "eventname"
                    sKey = "eventid"
                Case enScheduleType.EVENTPACKAGE
                    sTable = "eventpackageattr"
                    sCol = "packagename"
                    sKey = "eventpackid"
                Case enScheduleType.PACKAGE
                    sTable = "packageattr"
                    sCol = "packagename"
                    sKey = "packid"
                Case enScheduleType.REPORT
                    sTable = "reportattr"
                    sCol = "reporttitle"
                    sKey = "reportid"
            End Select

            SQL = "SELECT " & sCol & " FROM " & sTable & " WHERE " & sKey & " = " & ID

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return ID

            If oRs.EOF = True Then Return ID

            Dim sName As String = oRs(sCol).Value

            oRs.Close()

            Return sName

        Catch
            Return ID
        End Try

    End Function
    Public Overloads Function GetScheduleName(ByVal nScheduleID As Integer) As String
        Try
            Dim SQL As String
            Dim nReportID, nPackID, nAutoID, nEventPackID As Integer
            Dim oRs As ADODB.Recordset
            Dim sName As String

            SQL = "SELECT ReportID,PackID,AutoID,EventPackID FROM ScheduleAttr WHERE ScheduleID =" & nScheduleID

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    nReportID = IsNull(oRs(0).Value, 0)
                    nPackID = IsNull(oRs(1).Value, 0)
                    nAutoID = IsNull(oRs(2).Value, 0)
                    nEventPackID = IsNull(oRs(3).Value, 0)
                End If

                oRs.Close()
            End If

            If nReportID > 0 Then
                SQL = "SELECT ReportTitle FROM ReportAttr WHERE ReportID =" & nReportID
            ElseIf nPackID > 0 Then
                SQL = "SELECT PackageName FROM PackageAttr WHERE PackID =" & nPackID
            ElseIf nAutoID > 0 Then
                SQL = "SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nAutoID
            ElseIf nEventPackID > 0 Then
                SQL = "SELECT PackageName FROM EventPackageAttr WHERE EventPackID =" & nEventPackID
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    sName = oRs(0).Value
                Else
                    sName = String.Empty
                End If

                oRs.Close()

                Return sName
            Else
                Return String.Empty
            End If
        Catch
            Return String.Empty
        End Try
    End Function
    Private Function getDeadCollabServers() As String()
        Try
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT collaboid FROM collaboratorsAttr")
            Dim zombies() As String = Nothing
            Dim I As Integer = 0

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim serverName, username, password, domain As String
                    Dim serverID As Integer = oRs("collaboid").Value

                    Dim queryProc As String
                    Dim schedulerType As String
                    Dim obj As clsCollaboration = New clsCollaboration(serverID)

                    If obj.m_schedulerType = "NONE" Then
                        ReDim Preserve zombies(I)
                        zombies(I) = serverID
                        I += 1
                    Else
                        If obj.m_schedulerType = "WindowsApp" Then
                            queryProc = "sqlrdapp.exe"
                        ElseIf obj.m_schedulerType = "WindowsNT" Then
                            queryProc = "sqlrdsvc.exe"
                        End If

                        Dim errInfo As Exception
                        Dim remoteProc As remoteProcessController.controller = New remoteProcessController.controller
                        Dim dt As DataTable = remoteProc.ConnectToRemoteMachine(obj.m_domain, obj.m_serverName, obj.m_username, obj.m_password, True, errInfo)

                        If dt IsNot Nothing Then
                            Dim num As Integer = dt.Select("processName LIKE '%" & SQLPrepare(queryProc) & "%'").Length

                            If num = 0 Then
                                ReDim Preserve zombies(I)
                                zombies(I) = serverID
                                I += 1
                            End If
                        Else
                            ReDim Preserve zombies(I)
                            zombies(I) = serverID
                            I += 1
                        End If
                    End If

                    obj.Dispose()

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Return zombies
        Catch
            Return Nothing
        End Try

    End Function

    Private Function isBlackoutTime() As Boolean
        Try
            Dim useBlackout As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("SchedulerBlackouts", 0))

            If useBlackout = False Then Return False

            '//lets see if this group has black out times
            Dim SQL As String = "SELECT operationhrsid FROM groupblackoutattr WHERE groupid = 2911"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                Do While oRs.EOF = False
                    Dim opID As Integer = oRs(0).Value
                    Dim opHours As clsOperationalHours = New clsOperationalHours(opID)
                    Dim checkDate As Date = Date.Now

                    If opHours.withinOperationHours(checkDate, opHours.operationHoursName) = True Then
                        oRs.Close()
                        Return True
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Return False
        Catch
            Return False
        End Try
    End Function



    Public Sub Schedule(Optional ByVal InCluster As Boolean = False)
#If Not Debug Then
        If isBlackoutTime() Then Return
#End If

        Dim nPackId As Integer
        Dim nReportID As Integer
10:     Dim oReport As clsMarsReport = New clsMarsReport
11:     Dim oEvent As clsMarsEvent = New clsMarsEvent

20:     Try
            Dim SQL As String = ""
            Dim oRs As ADODB.Recordset
            Dim RunSuccess As Boolean
            Dim sWhere As String
            Dim sFrequency As String
            Dim StartDate As Date
            Dim StartTime As Date
            Dim nRepeat As Double
            Dim sUntil As String
            Dim nAutoID As Integer
            Dim nEventPackID As Integer
            Dim nScheduleID As Integer
30:         Dim oUI As New clsMarsUI
            Dim sCal As String = ""
40:         Dim oThread As New Process
            Dim MultiThread As Boolean = False
            Dim nSmartID As Integer
            Dim OriginalNextRun As Date
            Dim UseUniversal As Boolean = False
            Dim I As Integer = 0
            Dim UseException As Boolean = False
            Dim ExceptionCal As String = ""
            Dim cpuPriority As String = "Normal"
            Dim ThreadCount As Integer = 4
            Dim threadInterval As Integer = 15
            Dim RepeatUnit As String = "hours"
            Dim scheduleType As clsMarsScheduler.enScheduleType = enScheduleType.NONE

            SaveTextToFile(ConDateTime(Now) & ": Loading schedules' data", sAppPath & "scheduler.debug", , True)

            '//collaboration settings
            Dim collabo As Boolean

            Try
                collabo = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("collaboration", "0"))
            Catch : End Try

            Dim collaborationLeader As String = clsMarsUI.MainUI.ReadRegistry("collaborationLeader", "")
            Dim collabServers As String = ""
            Dim iamLeader As Boolean = False

            If collaborationLeader = "" Then collabo = False

            If String.Compare(Environment.MachineName, collaborationLeader, True) = 0 And collabo = True Then
                iamLeader = True

                collabServers = "0"

                Dim zombies As String() = getDeadCollabServers()

                If zombies IsNot Nothing Then
                    For Each server As String In zombies
                        If server <> "" Then
                            collabServers &= ", " & server
                        End If
                    Next
                End If
            ElseIf collabo = True Then
                iamLeader = False

                collabServers = clsMarsUI.MainUI.ReadRegistry("collaborationID", "")

                If collabServers = "" Or collabServers = 0 Then collabo = False
            End If

            If collabo = True And IsFeatEnabled(MarsGlobal.gEdition.CORPORATE, featureCodes.sa1_MultiThreading) = False Then
                collabo = False
            End If

            If collabo = False Then
                If Environment.MachineName.ToLower <> clsServiceController.GetRegisteredServiceName.ToLower And clsServiceController.GetRegisteredServiceName <> "" Then
                    Dim msg As String = Application.ProductName & " cannot execute schedules as another machine (" & clsServiceController.GetRegisteredServiceName & ") is the registered scheduler."
                    _ErrorHandle(msg, -104710130, "Schedule", 0, "Please set up the scheduler again (Options > Scheduling) in order to register this machine as the scheduler.", True, True)
                    Return
                End If
            End If

50:         Try
60:             MultiThread = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDThreading", 0)))
70:         Catch ex As Exception
80:             MultiThread = False
90:         End Try

100:        UseUniversal = True

110:        cpuPriority = oUI.ReadRegistry("CPU Priority", "Normal")
            Dim priority As System.Diagnostics.ProcessPriorityClass

120:        Select Case cpuPriority
                Case "Real Time"
130:                priority = ProcessPriorityClass.RealTime
140:            Case "High"
150:                priority = ProcessPriorityClass.High
160:            Case "Above Normal"
170:                priority = ProcessPriorityClass.AboveNormal
180:            Case "Normal"
190:                priority = ProcessPriorityClass.Normal
200:            Case "Below Normal"
210:                priority = ProcessPriorityClass.BelowNormal
220:            Case "Low"
230:                priority = ProcessPriorityClass.Idle
            End Select

240:        ThreadCount = oUI.ReadRegistry("ThreadCount", 4)
241:        threadInterval = oUI.ReadRegistry("ThreadInterval", 15)

250:        If InCluster = False Then
                If collabo = False Then
270:                If gConType = "DAT" Then
                        SQL = "SELECT * FROM ScheduleAttr WHERE Status = 1 AND " & _
                               "Frequency <> 'None' AND DATEDIFF ('d', EndDate,Now()) <= 0 AND DATEDIFF('h',Now(),NextRun) <= 1  ORDER BY NextRun" '//schedules due in the next hour
290:                Else
                        SQL = "SELECT * FROM ScheduleAttr WHERE Status = 1 AND " & _
                               "Frequency <> 'None' AND DATEDIFF (d, EndDate,GetDate()) <= 0 AND DATEDIFF(minute,GetDate(),NextRun) <= 0  ORDER BY NextRun" '//schedules past due
                    End If
                Else
                    If gConType = "DAT" Then
291:                    SQL = "SELECT * FROM ScheduleAttr WHERE Status = 1 AND " & _
                               "Frequency <> 'None' AND DATEDIFF ('d', EndDate,Now()) <= 0 AND DATEDIFF('h',Now(),NextRun) <= 1  " & _
                               "AND collaborationServer in (" & collabServers & ") ORDER BY NextRun" '//schedules due in the next hour
                    Else
                        SQL = "SELECT * FROM ScheduleAttr WHERE Status = 1 AND " & _
                               "Frequency <> 'None' AND DATEDIFF (d, EndDate,GetDate()) <= 0 AND DATEDIFF(minute,GetDate(),NextRun) <= 0  " & _
                               "AND collaborationServer in (" & collabServers & ") ORDER BY NextRun" '//schedules past due
                    End If
                End If

310:            oRs = clsMarsData.GetData(SQL)

320:            If oRs Is Nothing Then Return

                SaveTextToFile(ConDateTime(Now) & ": Initializing schedule itenary", sAppPath & "scheduler.debug", , True)

                'Remove any detail rows that do not have a valid process ID
                clsMarsThreading.CleanThreadManagerDetail()

330:            Do While oRs.EOF = False
                    Dim NextRun As Date = oRs("nextrun").Value
                    Dim CurrentTime As Date = Now
340:                nScheduleID = oRs("scheduleid").Value




350:                Try
360:                    UseException = Convert.ToBoolean(oRs("useexception").Value)
370:                    ExceptionCal = IsNull(oRs("exceptioncalendar").Value)
380:                Catch ex As Exception
390:                    UseException = False
                    End Try

400:                If UseUniversal = True Then
410:                    NextRun = NextRun.ToUniversalTime
420:                    CurrentTime = Now.ToUniversalTime
                    End If

430:                If ConDateTime(NextRun) <= ConDateTime(CurrentTime) Then

431:                    m_ScheduleID = nScheduleID

                        SaveTextToFile(ConDateTime(Now) & ": Processing schedule ID - " & nScheduleID & " (" & NextRun & ")", sAppPath & "scheduler.debug", , True)

440:                    If MultiThread = True Then
450:                        Do
460:                            Application.DoEvents()
470:                        Loop Until clsMarsThreading.GetThreadCount <= ThreadCount
480:                    End If

490:                    SQL = "select nextrun from scheduleattr where scheduleid = " & oRs("scheduleid").Value
                        Dim ValidateRS As ADODB.Recordset = clsMarsData.GetData(SQL)
500:                    NextRun = ValidateRS(0).Value
510:                    NextRun = NextRun.ToUniversalTime
520:                    If ConDateTime(NextRun) > ConDateTime(CurrentTime) Then
530:                        GoTo skipper
                        End If


540:                    gErrorDesc = String.Empty
550:                    OriginalNextRun = oRs("nextrun").Value
560:                    nPackId = IsNull(oRs("PackID").Value, 0)
570:                    nReportID = IsNull(oRs("ReportID").Value, 0)
580:                    nAutoID = IsNull(oRs("autoid").Value, 0)
590:                    sFrequency = oRs("Frequency").Value
600:                    StartDate = oRs("StartDate").Value
610:                    StartTime = oRs("StartTime").Value
620:                    sCal = IsNull(oRs("calendarname").Value)
630:                    nSmartID = IsNull(oRs("smartid").Value, 0)
640:                    nEventPackID = IsNull(oRs("eventpackid").Value, 0)

650:                    Try
660:                        nRepeat = IsNonEnglishRegionRead(oRs("repeatinterval").Value)
670:                    Catch
680:                        nRepeat = 0
690:                    End Try

700:                    Try
710:                        sUntil = ConTime(oRs("RepeatUntil").Value)
720:                    Catch
730:                        sUntil = IsNull(oRs("RepeatUntil").Value, "")
740:                    End Try

750:                    sWhere = " WHERE PackID = " & nPackId & " AND ReportID = " & nReportID

760:                    ScheduleStart = Now

                        SaveTextToFile(ConDateTime(Now) & ": Checking exception calendar rules", sAppPath & "scheduler.debug", , True)

770:                    If Me.IsDateAnException(NextRun, ExceptionCal) = True And UseException = True Then
780:                        RunSuccess = False
790:                        gErrorDesc = "Schedule not executed as execution date was included in exception calendar"
800:                        gErrorNumber = 0
810:                        GoTo ExceptionCalendar
                        End If

                        SaveTextToFile(ConDateTime(Now) & ": Processing reports", sAppPath & "scheduler.debug", , True)

820:                    Try
830:                        If nPackId > 0 Then
                                m_progressID = "Package:" & nPackId

840:                            scheduleType = enScheduleType.PACKAGE

850:                            If MultiThread = False Then
860:                                TaskManager("Add", , nPackId)

870:                                ThreadManager(nPackId, "add", enScheduleType.PACKAGE)

880:                                If clsMarsData.IsScheduleDynamic(nPackId, "Package") = True Then
890:                                    RunSuccess = oReport.RunDynamicPackageSchedule(nPackId)
                                    ElseIf clsMarsData.IsScheduleDataDriven(nPackId, "Package") = True Then
                                        RunSuccess = oReport.RunDataDrivenPackage(nPackId)
900:                                Else
910:                                    RunSuccess = oReport.RunPackageSchedule(nPackId)
                                    End If

920:                                TaskManager("Remove", , nPackId)

930:                                ThreadManager(nPackId, "remove", enScheduleType.PACKAGE)

940:                            Else
950:                                If clsMarsData.IsDuplicate("ThreadManager", "PackID", nPackId, False) = False Then
960:                                    With oThread
970:                                        .StartInfo.Arguments = "!xp " & nPackId
980:                                        .StartInfo.FileName = sAppPath & assemblyName
                                            .StartInfo.UseShellExecute = True
990:                                        .Start()
1000:                                       .PriorityClass = priority
1010:                                   End With

1020:                                   _Delay(threadInterval)
1030:                               End If
1040:                           End If
1050:                       ElseIf nReportID > 0 Then
                                m_progressID = "Report:" & nReportID

                                ''clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Processing: " & nReportID, False)

1060:                           scheduleType = enScheduleType.REPORT

1070:                           If MultiThread = True Then
                                    'clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Multi-Threading is : " & MultiThread.ToString, True)
                                    'clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Checking ThreadManager table for duplicate", True)

1080:                               If clsMarsData.IsDuplicate("ThreadManager", "ReportID", nReportID, False) = False Then
                                        'clsMarsDebug.writeToDebug("dynamicThreading_" & Process.GetCurrentProcess.Id & ".log", "Found no duplicates, starting thread for " & nReportID, True)
1090:                                   With oThread
1100:                                       .StartInfo.Arguments = "!xs " & nReportID
1110:                                       .StartInfo.FileName = sAppPath & assemblyName
                                            .StartInfo.UseShellExecute = True
1120:                                       .Start()
1130:                                       Try
1140:                                           .PriorityClass = priority
                                            Catch : End Try
1150:                                   End With
1160:                                   _Delay(threadInterval)
1170:                               End If
1180:                           Else
1190:                               TaskManager("Add", nReportID)

1200:                               ThreadManager(nReportID, "add", enScheduleType.REPORT)

1210:                               SQL = "SELECT Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportID =" & nReportID

1220:                               Dim oDynamic As ADODB.Recordset = clsMarsData.GetData(SQL)

1230:                               Try
1240:                                   If IsNull(oDynamic("dynamic").Value, 0) = "1" Then
1250:                                       RunSuccess = oReport.RunDynamicSchedule(nReportID)
1260:                                   ElseIf IsNull(oDynamic("isdatadriven").Value, 0) = "1" Then
1290:                                       RunSuccess = oReport.RunDataDrivenSchedule(nReportID)
1300:                                   Else
1310:                                       RunSuccess = oReport.RunSingleSchedule(nReportID)
1320:                                   End If
1330:                               Catch ex As Exception
1340:                                   ''console.writeLine(ex.ToString)
1350:                                   RunSuccess = oReport.RunSingleSchedule(nReportID)
1360:                               End Try

1370:                               oDynamic.Close()
1380:
1390:                               TaskManager("Remove", nReportID)

1400:                               ThreadManager(nReportID, "remove", enScheduleType.REPORT)

1410:                           End If
1420:                       ElseIf nAutoID > 0 Then

                                m_progressID = "Automation:" & nAutoID
                                gScheduleName = GetScheduleName(nAutoID, enScheduleType.AUTOMATION)

1430:                           If MultiThread = True Then
1440:                               If clsMarsData.IsDuplicate("ThreadManager", "AutoID", nAutoID, False) = False Then
1450:                                   With oThread
1460:                                       .StartInfo.Arguments = "!xa " & nAutoID
1470:                                       .StartInfo.FileName = sAppPath & assemblyName
                                            .StartInfo.UseShellExecute = True
1480:                                       .Start()
1490:                                       Try
1500:                                           .PriorityClass = priority
                                            Catch : End Try
1510:                                   End With
1520:                                   _Delay(threadInterval)
1530:                               End If
1540:                           Else
1550:                               Dim oTask As New clsMarsTask

                                    TaskManager("Add", , , nAutoID)

1560:                               ThreadManager(nAutoID, "add", enScheduleType.AUTOMATION)

1570:                               RunSuccess = oTask.ProcessTasks(nScheduleID, "NONE")

1580:                               ThreadManager(nAutoID, "remove", enScheduleType.AUTOMATION)

                                    TaskManager("Remove", , , nAutoID)
1590:                           End If
1600:                       ElseIf nEventPackID > 0 Then
                                m_progressID = "Event-Package:" & nEventPackID

1610:                           If MultiThread = False Then

                                    TaskManager("Add", , , , , nEventPackID)
1630:                               ThreadManager(nEventPackID, "add", enScheduleType.EVENTPACKAGE)

1640:                               RunSuccess = oEvent.RunEventPackage(nEventPackID)

1650:                               ThreadManager(nEventPackID, "remove", enScheduleType.EVENTPACKAGE)
                                    TaskManager("Remove", , , , , nEventPackID)
1660:                           Else
1670:                               If clsMarsData.IsDuplicate("ThreadManager", "EventPackID", nEventPackID, False) = False Then
1680:                                   With oThread
1690:                                       .StartInfo.Arguments = "!xz " & nEventPackID
1700:                                       .StartInfo.FileName = sAppPath & assemblyName
                                            .StartInfo.UseShellExecute = True
1710:                                       .Start()
1720:                                       Try
1730:                                           .PriorityClass = priority
1740:                                       Catch : End Try
                                        End With
1750:                                   _Delay(threadInterval)
1760:                               End If
                                End If
1770:                       ElseIf nSmartID > 0 Then
                                m_progressID = nSmartID

1780:                           If MultiThread = False Then
1790:                               Dim oSmart As New frmSmartReport

1800:                               ThreadManager(nSmartID, "add", enScheduleType.SMARTFOLDER)
1810:                               RunSuccess = oSmart._GenerateReport(nSmartID)
1820:                               ThreadManager(nSmartID, "remove", enScheduleType.SMARTFOLDER)
1830:                           Else
1840:                               If clsMarsData.IsDuplicate("ThreadManager", "SmartID", nSmartID, False) = False Then
1850:                                   With oThread
1860:                                       .StartInfo.Arguments = "!xf " & nSmartID
1870:                                       .StartInfo.FileName = sAppPath & assemblyName
                                            .StartInfo.UseShellExecute = True
1880:                                       .Start()
1890:                                       Try
1900:                                           .PriorityClass = priority
1910:                                       Catch : End Try
1920:                                   End With
1930:                                   _Delay(threadInterval)
                                    End If
                                End If
1940:                       End If
1950:                   Catch ex As Exception
                            SaveTextToFile(nScheduleID & " :" & Now & ": schedule errored. " & ex.Message & " [" & Erl() & "]", sAppPath & "scheduler.debug", , True)
1960:                       gErrorDesc = ex.Message & "; " & gErrorDesc
1970:                       gErrorNumber = Err.Number
1980:                       gErrorLine = Erl()
1990:                       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2000:                   End Try

ExceptionCalendar:
2010:                   If MultiThread = False Or UseException = True Then

                            SaveTextToFile(ConDateTime(Now) & ": Updating schedule data", sAppPath & "scheduler.debug", , True)

                            Dim Repeat As Boolean = False

2020:                       Try
2030:                           Repeat = Convert.ToBoolean(oRs("repeat").Value)
2040:                       Catch
2050:                           Repeat = False
                            End Try

2060:                       Try
2070:                           RepeatUnit = IsNull(oRs("repeatunit").Value, "hours")
2080:                       Catch ex As Exception
2090:                           RepeatUnit = "hours"
                            End Try

2100:                       NextRun = GetNextRun(nScheduleID, sFrequency, StartDate, StartTime, _
                                                  Repeat, nRepeat, sUntil, sCal, , , , RepeatUnit)

2110:                       If NextRun = OriginalNextRun Then
2120:                           _ErrorHandle(nScheduleID & ": Catastrophic scheduler processing error." & _
                                                          "Next run not incremented for schedule '" & Me.GetScheduleName(nScheduleID) & "'. The schedule will be disabled to avoid repeatition.", _
                                                          -2417187, "_Schedule", 1350, "Please set the next run date manually from the schedule's properties.")

2130:                           Me.DisableSchedule(nScheduleID)
                            End If

2140:                       SQL = "UPDATE ScheduleAttr SET NextRun = '" & _
                                                  ConDateTime(NextRun) & "' WHERE ScheduleID =" & nScheduleID

2150:                       If clsMarsData.WriteData(SQL, True, True) = False Then
2160:                           _ErrorHandle("Fatal Scheduler Error: Could not update schedule's next runtime for  Schedule: " & nScheduleID, _
                                                          -214768951, "clsMarsScheduler._Schedule", 1371)
                            End If

2170:                       _Delay(4)

                            Dim historyID As Integer = 0

                            If nEventPackID > 0 Then
2171:                           historyID = oEvent.m_HistoryID
                            Else
2172:                           historyID = oReport.m_HistoryID
                            End If

2180:                       If RunSuccess = True Then
2190:                           SetScheduleHistory(True, "", nReportID, nPackId, nAutoID, nSmartID, nEventPackID, historyID)
2200:                       Else
2210:                           SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nReportID, nPackId, nAutoID, nSmartID, nEventPackID, historyID)

2220:                           If gErrorDesc.Contains("Report(s) were produced successfully but email failed") = False And scheduleType <> enScheduleType.NONE Then
2230:                               Dim retryInt As Integer = clsMarsReport.getScheduleRetryInterval(nScheduleID, scheduleType)

2240:                               If retryInt > 0 Then
2250:                                   clsMarsReport.LogRetry(nScheduleID, 0)
2260:                               End If
                                End If
                            End If

                            '+++++++++++++++++++++++++++++++++validate the nextrun value from database++++++++++++++++++++
                            Dim TestDate As Date

2270:                       Do
2280:                           Try
2290:                               Dim oTest As ADODB.Recordset

2300:                               oTest = New ADODB.Recordset

2310:                               oTest.Open("SELECT NextRun FROM ScheduleAttr WHERE ScheduleID =" & nScheduleID, _
                              sCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

2320:                               If oTest.EOF = False Then
2330:                                   TestDate = oTest(0).Value

2340:                                   If ConDateTime(TestDate) <> ConDateTime(NextRun) Then
2350:                                       clsMarsData.WriteData(SQL, False)

2360:                                       _ErrorHandle("Data Persist Error - NextRun not updated for '" & Me.GetScheduleName(nScheduleID) & "'", -2147689187, "clsMarsSchedule._Schedule", 1418)
2370:                                   Else
                                            SaveTextToFile(ConDateTime(Now) & ": Nextrun verified as " & TestDate, sAppPath & "scheduler.debug", , True)
2380:                                   End If
2390:                               Else
2400:                                   Exit Do
2410:                               End If

2420:                               oTest.Close()

2430:                               oTest = Nothing

2440:                           Catch ex As Exception
2450:                               _ErrorHandle("Data Persist Error - " & ex.Message, Err.Number, _
                              Reflection.MethodBase.GetCurrentMethod.Name, Erl)

2460:                               Exit Do
                                End Try
2470:                       Loop Until TestDate = NextRun

                            '++++++++++++++++++++++++++++++++++++++end validate++++++++++++++++++++++++++++++++++++++++
                        End If

                        SaveTextToFile(ConDateTime(Now) & ": processing of schedule (" & nScheduleID & ") completed.", sAppPath & "scheduler.debug", , True)
2480:                   SaveTextToFile("+++++++++++++++++++++++++++++++", sAppPath & "scheduler.debug", , True)

                    End If

                    Threading.Thread.Sleep(3000)
skipper:

2490:               oRs.MoveNext()
2500:           Loop
2510:       Else
2520:           ScheduleForCluster()
            End If

2530:       Try
                processExecuteNowQueue()
            Catch : End Try
2540:   Catch ex As Exception

2550:       Try
2560:           If nPackId > 0 Then
2570:               TaskManager("Remove", , nPackId)
2580:           ElseIf nReportID > 0 Then
2590:               TaskManager("Remove", nReportID)
                End If
            Catch : End Try

2600:       gErrorDesc = ex.Message
2610:       gErrorNumber = Err.Number
2620:       gErrorLine = Erl()

2630:       _ErrorHandle(gErrorDesc, gErrorNumber, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
2640:   Finally
2650:       gScheduleName = String.Empty
        End Try

2660:       oReport = Nothing
    End Sub


    Public Sub ScheduleForCluster()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim RunSuccess As Boolean
        Dim nReportID As Integer
        Dim sWhere As String
        Dim sFrequency As String
        Dim StartDate As Date
        Dim StartTime As Date
        Dim nRepeat As Double
        Dim sUntil As String
        Dim nPackId As Integer
        Dim nScheduleID As Integer
        Dim oUI As New clsMarsUI
        Dim oRs2 As ADODB.Recordset
        Dim oReport As clsMarsReport = New clsMarsReport

        SQL = "SELECT * FROM TaskManager WHERE PCName = '" & SQLPrepare(Environment.MachineName) & "'"

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                nReportID = oRs("reportid").Value
                nPackId = oRs("packid").Value

                SQL = "SELECT * FROM ScheduleAttr WHERE ReportID =" & nReportID & " AND PackID =" & nPackId

                oRs2 = clsMarsData.GetData(SQL)

                If oRs2.EOF = False Then
                    nScheduleID = oRs2("scheduleid").Value
                    sFrequency = oRs2("Frequency").Value
                    StartDate = oRs2("StartDate").Value
                    StartTime = oRs2("StartTime").Value

                    Try
                        nRepeat = oRs2("repeatinterval").Value
                    Catch
                        nRepeat = 0
                    End Try

                    sUntil = oRs2("RepeatUntil").Value

                    sWhere = " WHERE PackID = " & nPackId & " AND ReportID = " & nReportID

                    ScheduleStart = Now

                    If nPackId > 0 Then
                        clsMarsData.WriteData("UPDATE TaskManager SET Status = 'Processing....' WHERE PackID =" & nPackId, False)

                        If clsMarsData.IsScheduleDynamic(nPackId, "Package") = False Then
                            RunSuccess = oReport.RunPackageSchedule(nPackId)
                        Else
                            RunSuccess = oReport.RunDynamicPackageSchedule(nPackId)
                        End If

                        TaskManager("Remove", , nPackId)
                    ElseIf nReportID > 0 Then
                        clsMarsData.WriteData("UPDATE TaskManager SET Status = 'Processing...' WHERE ReportID =" & nReportID, False)

                        Dim oTest As ADODB.Recordset

                        oTest = clsMarsData.GetData("SELECT Dynamic,Bursting FROM ReportAttr WHERE ReportID =" & nReportID)

                        If Not oTest Is Nothing Then
                            If oTest.EOF = False Then
                                If IsNull(oTest(0).Value, 0) = "1" Then
                                    RunSuccess = oReport.RunDynamicSchedule(nReportID)

                                Else
                                    RunSuccess = oReport.RunSingleSchedule(nReportID)
                                End If
                            End If

                            oTest.Close()
                        End If

                        TaskManager("Remove", nReportID)
                    End If

                    Dim NextRun As Date

                    NextRun = GetNextRun(nScheduleID, sFrequency, StartDate, StartTime, _
                    Convert.ToBoolean(oRs2("Repeat").Value), nRepeat, sUntil, , , , , IsNull(oRs2("repeatunit").Value, "hours"))

                    SQL = "UPDATE ScheduleAttr SET NextRun = '" & _
                     ConDateTime(NextRun) & "' " & sWhere

                    clsMarsData.WriteData(SQL)

                    If RunSuccess = True Then
                        SetScheduleHistory(True, "", nReportID, nPackId, , , , oReport.m_HistoryID)
                    Else
                        SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nReportID, nPackId, , , , oReport.m_HistoryID)
                    End If
                End If

                oRs2.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        oReport = Nothing
    End Sub

    Public Sub ScheduleDeferred()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
10:     Dim oReport As New clsMarsReport
        Dim Ok As Boolean
        Dim nReportID As Integer = 0
        Dim nPackID As Integer = 0

20:     SQL = "SELECT d.DeferID, x.ReportID, x.PackID FROM DeferDeliveryAttr d INNER JOIN DestinationAttr x ON " & _
       "d.DestinationID = x.DestinationID WHERE DATEDIFF([n],[CurrentDate],DueDate) <= 0"

30:     If gConType = "DAT" Then
40:         SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[n]", "'n'")
50:     Else
60:         SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[n]", "n")
        End If
70:     oRs = clsMarsData.GetData(SQL)

80:     Try
90:         Do While oRs.EOF = False
100:            Ok = oReport.DeliverDefered(oRs("deferid").Value)

110:            If Ok = True Then
120:                Dim oTask As New clsMarsTask
                    Dim nScheduleID As Integer

130:                nReportID = oRs("reportid").Value
140:                nPackID = oRs("packid").Value

150:                If nPackID > 0 Then
160:                    nReportID = 0
                    End If

170:                nScheduleID = GetScheduleID(nReportID, nPackID)

180:                oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

190:                SetScheduleHistory(True, "Deferred delivery peformed successfully", nReportID, nPackID)
200:            Else
210:                SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nReportID, nPackID)
                End If

220:            oRs.MoveNext()
230:        Loop
240:    Catch ex As Exception
250:        '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub


    Public Overloads Function ForceGetNextRun(ByVal nScheduleID As Integer, ByVal sFrequency As String, _
    ByVal StartDate As Date, ByVal StartTime As Date, Optional ByVal CalendarName As String = "") As Date
        Dim NextRun As Date         'the schedules next run date
        Dim NextRunTime As String   'the schedules next run time
        Dim OnSameDate As Boolean = False
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        Select Case UCase(sFrequency)
            Case "DAILY"
                SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Daily'"

                oRs = clsMarsData.GetData(SQL)

                If oRs Is Nothing Then
                    NextRun = Date.Now.Date.AddDays(1)
                ElseIf oRs.EOF = True Then
                    NextRun = Date.Now.Date.AddDays(1)
                Else
                    Try
                        NextRun = Date.Now.AddDays(oRs("ncount").Value)
                    Catch
                        NextRun = Date.Now.Date.AddDays(1)
                    End Try
                End If

            Case "WEEKLY"
                SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Weekly'"

                oRs = clsMarsData.GetData(SQL)

                If oRs Is Nothing Then
                    NextRun = StartDate.AddDays(7)

                    While NextRun <= Date.Now.Date
                        NextRun = NextRun.AddDays(7)
                    End While
                ElseIf oRs.EOF = True Then
                    NextRun = StartDate.AddDays(7)

                    While NextRun <= Date.Now.Date
                        NextRun = NextRun.AddDays(7)
                    End While
                Else
                    NextRun = GetNextRunForWeekly(StartDate, oRs("ncount").Value, oRs("monthsin").Value)
                End If

            Case "MONTHLY"

                SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Monthly'"

                oRs = clsMarsData.GetData(SQL)

                If oRs Is Nothing Then
                    NextRun = StartDate.AddMonths(1)

                    While NextRun <= Date.Now.Date
                        NextRun = NextRun.AddMonths(1)
                    End While
                ElseIf oRs.EOF = True Then
                    NextRun = StartDate.AddMonths(1)

                    While NextRun <= Date.Now.Date
                        NextRun = NextRun.AddMonths(1)
                    End While
                    oRs.Close()
                Else
                    Dim NewMonth As Date

                    NewMonth = GetNextMonth(oRs("monthsin").Value)

                    NextRun = GetNthDayOfMonth(NewMonth, _
                        oRs("weekno").Value, oRs("dayno").Value)
                    oRs.Close()
                End If

            Case "WEEK-DAILY", "WEEKDAYS"
                'Dim sTry As Date = ConDate(Now.Date) & " " & NextRunTime
                'If sTry > Now And ForceTomorrow = False Then
                'N() 'extRun = Now.Date
                'Else

                NextRun = Date.Now.Date.AddDays(1)

                Do While NextRun.DayOfWeek = DayOfWeek.Saturday Or _
                NextRun.DayOfWeek = DayOfWeek.Sunday
                    NextRun = NextRun.AddDays(1)
                Loop
                'End If

            Case "WEEKENDS"

                NextRun = Date.Now.AddDays(1)

                Do While NextRun.DayOfWeek <> DayOfWeek.Saturday And _
                    NextRun.DayOfWeek <> DayOfWeek.Sunday
                    NextRun = NextRun.AddDays(1)
                Loop

            Case "YEARLY"

                NextRun = StartDate.AddYears(1)

                While NextRun <= Date.Now.Date
                    NextRun = NextRun.AddYears(1)
                End While

            Case "CUSTOM"
                NextRun = GetCustomNextRunDate(CalendarName)

                If Convert.ToDateTime(ConDate(NextRun) & " " & NextRunTime) < Now Then
                    NextRun = Now.AddDays(1)
                End If
            Case Else

                NextRun = Date.Now.AddDays(1)
        End Select

        Return Convert.ToDateTime(ConDate(NextRun) & " " & NextRunTime)
    End Function
    Public Shared Sub migrateNoOptionSchedules()
        Dim SQL As String = "SELECT * FROM scheduleattr WHERE scheduleid NOT IN (SELECT scheduleid FROM scheduleoptions) AND Frequency = 'Weekly'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                Dim scheduleID As Integer = oRs("scheduleid").Value
                Dim nextrun As Date = oRs("nextrun").Value
                Dim nextrunDay As DayOfWeek = nextrun.DayOfWeek
                Dim dayValue As Integer = nextrunDay

                Dim cols, vals As String

                cols = "OptionID,MonthsIn,nCount,ScheduleID,ScheduleType"

                vals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                    "'" & dayValue & "|'," & _
                    1 & "," & _
                    scheduleID & "," & _
                    "'Weekly'"


                clsMarsData.DataItem.InsertData("scheduleoptions", cols, vals, False)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Public Overloads Function GetNextRun(ByVal nScheduleID As Integer, _
     ByVal sFrequency As String, ByVal StartDate As Date, _
     ByVal StartTime As Date, ByVal IsRepeat As Boolean, _
     ByVal Repeat As Double, _
     ByVal sUntil As String, _
     Optional ByVal CalendarName As String = "", Optional ByVal ForceTomorrow As Boolean = False, _
     Optional ByVal ForceNext As Boolean = False, Optional ByVal DueTime As String = "", _
    Optional ByVal RepeatUnit As String = "hours", Optional ByVal hackTomoz As Boolean = False) As Date

        Dim NextRun As Date         'the schedules next run date
        Dim NextRunTime As String   'the schedules next run time
        Dim OnSameDate As Boolean = Nothing
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        '---------------------notes--------------------------------
        'sFrequency - the schedule's frequency
        'StartDate - the schedule's original start date
        'StartTime - the schedule's execution time
        'Repeat - the schedule's repeat interval
        'nUntil - the time the schedule is supposed to repeat until
        '----------------------------------------------------------

        'set initial values
        NextRunTime = ConTime(StartTime)
        'next calculate the next run time

        SaveTextToFile(ConDateTime(Now) & ": Schedule Repeat = " & IsRepeat.ToString, _
        sAppPath & "scheduler.debug", , True)

        If IsRepeat = True Then
            NextRun = Date.Now.Date

            If ForceTomorrow = True Then
                OnSameDate = False

                SaveTextToFile(ConDateTime(Now) & ": Moving next run to tomorrow", _
                sAppPath & "scheduler.debug", , True)
            Else
                Dim stillToday As Boolean = False

                SaveTextToFile(ConDateTime(Now) & ": Next run date is today", _
                sAppPath & "scheduler.debug", , True)

                Select Case RepeatUnit
                    Case "hours"
                        If Date.Now.AddMinutes(Convert.ToDouble(Repeat) * 60) <= Convert.ToDateTime(Date.Now.Date & " " & ConTime(sUntil)) Then
                            stillToday = True
                        Else
                            stillToday = False
                        End If
                    Case "minutes"
                        If Date.Now.AddMinutes(Convert.ToDouble(Repeat)) <= Convert.ToDateTime(Date.Now.Date & " " & ConTime(sUntil)) Then
                            stillToday = True
                        Else
                            stillToday = False
                        End If
                End Select

                SaveTextToFile(ConDateTime(Now) & ": stillToday =" & stillToday.ToString, _
                sAppPath & "scheduler.debug", , True)

                If stillToday = True Then
                    If ForceNext = False Then
                        DueTime = Date.Now
                    End If

                    Do
                        If ConDateTime(Date.Now.Date & " " & NextRunTime) > ConDateTime(DueTime) Then
                            'NextRunTime = ConTime(Convert.ToDateTime(Date.Now.Date & " " & NextRunTime).AddMinutes(Convert.ToDouble(Repeat * 60)))
                            Exit Do
                        Else
                            Dim workingValue As Date = Convert.ToDateTime(Date.Now.Date & " " & NextRunTime)

                            SaveTextToFile(ConDateTime(Now) & ": Working value = " & workingValue, _
                            sAppPath & "scheduler.debug", , True)



                            Select Case RepeatUnit
                                Case "hours"
                                    workingValue = workingValue.AddMinutes(Convert.ToDouble(Repeat * 60))

                                    SaveTextToFile(ConDateTime(Now) & ": Working value after conversion = " & ConTime(workingValue), _
                                    sAppPath & "scheduler.debug", , True)

                                    NextRunTime = ConTime(workingValue)

                                    SaveTextToFile(ConDateTime(Now) & ": Repeat unit = " & RepeatUnit, _
                                    sAppPath & "scheduler.debug", , True)

                                Case "minutes"
                                    workingValue = workingValue.AddMinutes(Convert.ToDouble(Repeat))

                                    SaveTextToFile(ConDateTime(Now) & ": Working value after conversion = " & ConTime(workingValue), _
                                    sAppPath & "scheduler.debug", , True)

                                    NextRunTime = ConTime(workingValue)

                                    SaveTextToFile(ConDateTime(Now) & ": Repeat unit = " & RepeatUnit, _
                                    sAppPath & "scheduler.debug", , True)
                            End Select

                        End If

                        SaveTextToFile(ConDateTime(Now) & ": **In the loop to get next repeat time - Nextrun incremented by loop to " & NextRunTime & "**", _
                        sAppPath & "scheduler.debug", , True)
                    Loop

                    OnSameDate = True
                Else
                    OnSameDate = False
                End If
            End If
        Else
            If (ConTime(Now) < NextRunTime) And sFrequency.ToLower = "daily" Then
                OnSameDate = True
                NextRun = Now
            Else
                OnSameDate = False
            End If
        End If

        Dim IncrementDate As Boolean = Not OnSameDate

        SaveTextToFile(ConDateTime(Now) & ": Increment Date = " & IncrementDate.ToString, _
        sAppPath & "scheduler.debug", , True)

        If OnSameDate = False Or hackTomoz = True Then
            Select Case UCase(sFrequency)
                Case "WORKING DAY"
                    SQL = "SELECT * FROM scheduleoptions WHERE scheduleid = " & nScheduleID & " AND scheduletype = 'Working Day'"
                    oRs = clsMarsData.GetData(SQL)

                    If oRs IsNot Nothing AndAlso oRs.EOF = False Then
                        NextRun = getnthWorkingDay(oRs("monthsin").Value, Date.Now.AddMonths(1).Month, Date.Now.AddMonths(1).Year)
                    Else
                        NextRun = Date.Now.AddDays(1)
                    End If
                Case "DAILY"
                    SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                    "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Daily'"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        NextRun = Date.Now.Date.AddDays(1)
                    ElseIf oRs.EOF = True Then
                        NextRun = Date.Now.Date.AddDays(1)
                    Else
                        Try
                            NextRun = Date.Now.AddDays(oRs("ncount").Value)
                        Catch
                            NextRun = Date.Now.Date.AddDays(1)
                        End Try
                    End If

                Case "WEEKLY"
                    SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                    "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Weekly'"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        NextRun = StartDate.AddDays(7)

                        If (ConDate(NextRun) <= ConDate(Now.Date)) = False Then
                            SaveTextToFile(ConDateTime(Now) & ": Nextrun not incremented = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        End If

                        Do While ConDate(NextRun) <= ConDate(Now.Date)
                            NextRun = NextRun.AddDays(7)

                            SaveTextToFile(ConDateTime(Now) & ": Nextrun incremented to = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        Loop
                    ElseIf oRs.EOF = True Then
                        NextRun = StartDate.AddDays(7)

                        If (ConDate(NextRun) <= ConDate(Now.Date)) = False Then
                            SaveTextToFile(ConDateTime(Now) & ": Nextrun not incremented = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        End If

                        Do While ConDate(NextRun) <= ConDate(Now.Date)
                            NextRun = NextRun.AddDays(7)

                            SaveTextToFile(ConDateTime(Now) & ": Nextrun incremented to = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        Loop
                    Else
                        NextRun = GetNextRunForWeekly(StartDate, oRs("ncount").Value, oRs("monthsin").Value)
                    End If

                Case "MONTHLY"

                    SQL = "SELECT * FROM ScheduleOptions WHERE " & _
                    "ScheduleID = " & nScheduleID & " AND ScheduleType = 'Monthly'"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        NextRun = StartDate.AddMonths(1)

                        Do While ConDate(NextRun) <= ConDate(Now.Date)
                            NextRun = NextRun.AddMonths(1)

                            SaveTextToFile(ConDateTime(Now) & ": Nextrun incremented to = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        Loop
                    ElseIf oRs.EOF = True Then
                        NextRun = StartDate.AddMonths(1)

                        Do While ConDate(NextRun) <= ConDate(Now.Date)
                            NextRun = NextRun.AddMonths(1)

                            SaveTextToFile(ConDateTime(Now) & ": Nextrun incremented to = " & NextRun, _
                            sAppPath & "scheduler.debug", , True)
                        Loop
                        oRs.Close()
                    Else
                        Dim NewMonth As Date

                        NewMonth = GetNextMonth(oRs("monthsin").Value)

                        NextRun = GetNthDayOfMonth(NewMonth, _
                            oRs("weekno").Value, oRs("dayno").Value)
                        oRs.Close()
                    End If

                Case "WEEK-DAILY", "WEEKDAYS"
                    'Dim sTry As Date = ConDate(Now.Date) & " " & NextRunTime
                    'If sTry > Now And ForceTomorrow = False Then
                    'N() 'extRun = Now.Date
                    'Else

                    NextRun = Date.Now.Date.AddDays(1)

                    Do While NextRun.DayOfWeek = DayOfWeek.Saturday Or _
                    NextRun.DayOfWeek = DayOfWeek.Sunday
                        NextRun = NextRun.AddDays(1)

                    Loop
                    'End If

                Case "WEEKENDS"

                    NextRun = Date.Now.AddDays(1)

                    Do While NextRun.DayOfWeek <> DayOfWeek.Saturday And _
                        NextRun.DayOfWeek <> DayOfWeek.Sunday
                        NextRun = NextRun.AddDays(1)

                    Loop

                Case "YEARLY"

                    NextRun = StartDate.AddYears(1)

                    Do While ConDate(NextRun) <= ConDate(Now.Date)
                        NextRun = NextRun.AddYears(1)

                    Loop

                Case "CUSTOM"

                    NextRun = GetCustomNextRunDate(CalendarName, , nScheduleID)

                    If Convert.ToDateTime(ConDate(NextRun) & " " & NextRunTime) < Now Then
                        NextRun = Now.AddDays(1)
                    End If
                Case "OTHER"
                    SQL = "SELECT * FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID & " AND " & _
                    "ScheduleType ='Other'"

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then Throw New Exception("could not obtain details of 'other' frequency")
                    If oRs.EOF = True Then Throw New Exception("could not obtain details of 'other' frequency")

                    Dim otherFrequency As Decimal = oRs("otherfrequency").Value
                    Dim otherUnit As String = oRs("otherunit").Value

                    Select Case otherUnit
                        Case "Seconds"
                            Dim temp As Date = ConDate(Now) & " " & NextRunTime

                            NextRun = temp '.AddSeconds(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddSeconds(otherFrequency)
                            Loop

                            NextRunTime = ConTime(NextRun)
                        Case "Minutes"
                            Dim temp As Date = ConDate(Now) & " " & NextRunTime   '//example 2012-05-15 8:30:00

                            NextRun = temp '.AddMinutes(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddMinutes(otherFrequency)
                            Loop

                            NextRunTime = ConTime(NextRun)
                        Case "Hours"
                            Dim temp As Date = ConDate(Now) & " " & NextRunTime

                            NextRun = temp '.AddHours(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddHours(otherFrequency)
                            Loop

                            NextRunTime = ConTime(NextRun)
                        Case "Days"
                            Dim temp As Date = ConDate(StartDate) & " " & NextRunTime

                            NextRun = temp '.AddDays(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddDays(otherFrequency)
                            Loop
                        Case "Weeks"
                            Dim temp As Date = ConDate(StartDate) & " " & NextRunTime

                            NextRun = temp '.AddDays(otherFrequency * 7)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddDays(otherFrequency * 7)
                            Loop
                        Case "Months"
                            Dim temp As Date = ConDate(StartDate) & " " & NextRunTime

                            NextRun = temp '.AddMonths(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddMonths(otherFrequency)
                            Loop
                        Case "Years"
                            Dim temp As Date = ConDate(StartDate) & " " & NextRunTime

                            NextRun = temp '.AddYears(otherFrequency)

                            Do While ConDateTime(NextRun) <= ConDateTime(Now)
                                NextRun = NextRun.AddYears(otherFrequency)
                            Loop
                    End Select
                Case Else

                    NextRun = Date.Now.AddDays(1)
            End Select
        End If

        SaveTextToFile(ConDateTime(Now) & ": GetNextRun = " & Convert.ToDateTime(ConDate(NextRun) & " " & NextRunTime), sAppPath & "scheduler.debug", , True)

        Return Convert.ToDateTime(ConDate(NextRun) & " " & NextRunTime)
    End Function
    Public Sub SetScheduleHistory(ByVal Success As Boolean, Optional ByVal sErrMsg As String = "", _
    Optional ByVal nReportID As Integer = 0, _
    Optional ByVal nPackID As Integer = 0, _
    Optional ByVal nAutoID As Integer = 0, Optional ByVal nSmartID As Integer = 0, _
    Optional ByVal nEventPackID As Integer = 0, Optional ByVal HistoryID As Integer = 0)
        Try
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim started, entryDate As Date

            sCols = "HistoryID,ReportID,PackID,AutoID,EntryDate," & _
            "Success,ErrMsg,StartDate,SmartID,EventPackID,serverName"

            If HistoryID = 0 Then
                HistoryID = clsMarsData.CreateDataID("ScheduleHistory", "HistoryID")
            End If

            If RunEditor = True Then
                entryDate = CTimeZ(Now, dateConvertType.WRITE)
                started = CTimeZ(ScheduleStart, dateConvertType.WRITE)
            Else
                entryDate = Now
                started = ScheduleStart
            End If

            sVals = HistoryID & "," & _
                nReportID & "," & _
                nPackID & "," & _
                nAutoID & _
                ", '" & ConDateTime(entryDate) & "'," & _
                Convert.ToInt32(Success) & "," & _
                "'" & SQLPrepare(sErrMsg) & "'," & _
                "'" & ConDateTime(started) & "'," & _
                nSmartID & "," & _
                nEventPackID & ", " & _
                "'" & SQLPrepare(Environment.MachineName) & "'"

            SQL = "INSERT INTO ScheduleHistory (" & sCols & ") VALUES (" & _
            sVals & ")"

            Dim ok As Boolean
            Dim errMsg As Exception

            ok = clsMarsData.WriteData(SQL, False, , errMsg)

            If ok = False And errMsg IsNot Nothing Then
                Throw errMsg
            End If
        Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 1)
        End Try
    End Sub

    Public Sub ThreadManager(ByVal nID As Integer, ByVal AddorRemove As String, ByVal type As clsMarsScheduler.enScheduleType)
        Dim SQL As String = ""
        Dim cols As String
        Dim vals As String
        Dim keyCol As String

        If AddorRemove.ToLower = "add" Then
            Select Case type
                Case enScheduleType.AUTOMATION
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EntryDate,ProcessID"
                    vals = "0,0," & nID & ",0,0,[date]," & Process.GetCurrentProcess.Id
                Case enScheduleType.EVENTBASED
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EntryDate,ProcessID"
                    vals = "0,0,0," & nID & ",0,[date]," & Process.GetCurrentProcess.Id
                Case enScheduleType.PACKAGE
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EntryDate,ProcessID"
                    vals = "0," & nID & ",0,0,0,[date]," & Process.GetCurrentProcess.Id
                Case enScheduleType.REPORT
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EntryDate,ProcessID"
                    vals = nID & ",0,0,0,0,[date]," & Process.GetCurrentProcess.Id
                Case enScheduleType.EVENTPACKAGE
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EventPackID,EntryDate,ProcessID"
                    vals = "0,0,0,0,0," & nID & ",[date]," & Process.GetCurrentProcess.Id
                Case enScheduleType.SMARTFOLDER
                    cols = "ReportID,PackID,AutoID,EventID,SmartID,EntryDate,ProcessID"
                    vals = "0,0,0,0," & nID & ",[date]," & Process.GetCurrentProcess.Id
            End Select

            Dim dt As String = ConDateTime(Now)

            If gConType = "DAT" Then
                vals = vals.Replace("[date]", "'" & dt & "'")
            Else
                vals = vals.Replace("[date]", "'" & dt & "'")
            End If

            clsMarsData.DataItem.InsertData("ThreadManager", cols, vals, False)
        Else
            Select Case type
                Case enScheduleType.AUTOMATION
                    keyCol = "autoid"
                Case enScheduleType.EVENTBASED
                    keyCol = "eventid"
                Case enScheduleType.EVENTPACKAGE
                    keyCol = "eventpackid"
                Case enScheduleType.PACKAGE
                    keyCol = "packid"
                Case enScheduleType.REPORT
                    keyCol = "reportid"
                Case enScheduleType.SMARTFOLDER
                    keyCol = "smartid"
            End Select

            SQL = "DELETE FROM ThreadManager WHERE " & keyCol & " = " & nID

            clsMarsData.WriteData(SQL, False)
        End If

    End Sub
    Public Sub TaskManager(ByVal AddOrRemove As String, Optional ByVal nReportID As Integer = 0, _
    Optional ByVal nPackID As Integer = 0, _
    Optional ByVal nAutoID As Integer = 0, Optional ByVal Log As Boolean = False, _
    Optional ByVal nEventPackID As Integer = 0, Optional ByVal nEventID As Integer = 0)
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        Select Case AddOrRemove
            Case "Add"
                Dim ors As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM TaskManager WHERE " & _
                "ReportID =" & nReportID & " AND PackID =" & nPackID & " AND AutoID = " & _
                nAutoID & " AND EventPackID =" & nEventPackID & " AND EventID = " & nEventID)

                Try
                    If ors.EOF = False Then
                        ors.Close()
                        Return
                    End If
                    ors.Close()
                Catch : End Try

                sCols = "MonitorID,ReportID,PackID,AutoID,EventPackID,EventID,EntryDate,PCName,Status,ProcessID"

                sVals = clsMarsData.CreateDataID("TaskManager", "MonitorID") & "," & _
                nReportID & "," & _
                nPackID & "," & _
                nAutoID & "," & _
                nEventPackID & "," & _
                nEventID & "," & _
                "'" & ConDateTime(Date.Now) & "'," & _
                "'" & Environment.MachineName & "'," & _
                "'Executing...'" & "," & _
                Process.GetCurrentProcess.Id

                SQL = "INSERT INTO TaskManager (" & sCols & ") VALUES (" & sVals & ")"

                clsMarsData.WriteData(SQL, False)

                clsMarsData.IamAlive()

            Case "Remove"
                SQL = "DELETE FROM TaskManager WHERE ReportID = " & nReportID & " AND " & _
                        "PackID = " & nPackID & " AND AutoID =" & _
                        nAutoID & " AND EventPackID =" & nEventPackID & " AND EventID = " & nEventID

                clsMarsData.WriteData(SQL, False)

                Try
                    IO.File.Delete(sAppPath & "processAlive\" & Process.GetCurrentProcess.Id)
                Catch : End Try
        End Select

        If Log = True Then
            Me.SetScheduleHistory(False, "User cancelled schedule execution", nReportID, nPackID)
        End If
    End Sub

    Public Sub DrawScheduleHistory(ByVal oTree As TreeView, ByVal sType As enScheduleType, _
    Optional ByVal nID As Integer = 0, Optional ByVal SortOrder As String = " ASC ", _
    Optional ByVal Filter As Boolean = False)
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim sWhere As String
            Dim sCol As String
            Dim sTable As String
            Dim sAnd As String
            Dim oRs1 As ADODB.Recordset
            Dim oRs2 As ADODB.Recordset
            Dim oFreq, oReport, oHistory, oRec As TreeNode
            Dim nObjectID As Integer
            Dim nStatus As Integer

            Select Case sType
                Case enScheduleType.REPORT
                    sCol = "ReportID"
                    sTable = "ReportAttr"
                    sAnd = " AND R.ReportID = " & nID
                Case enScheduleType.PACKAGE
                    sCol = "PackID"
                    sTable = "PackageAttr"
                    sAnd = " AND R.PackID = " & nID
                Case enScheduleType.AUTOMATION
                    sCol = "AutoID"
                    sTable = "AutomationAttr"
                    sAnd = " AND R.AutoID = " & nID
                Case enScheduleType.SMARTFOLDER
                    sCol = "SmartID"
                    sTable = "SmartFolders"
                    sAnd = "AND R.SmartID = " & nID
                Case enScheduleType.EVENTPACKAGE
                    sCol = "EventPackID"
                    sTable = "EventPackageAttr"
                    sAnd = "AND R.EventPackID = " & nID
            End Select

            If nID = 0 Then
                sWhere = " WHERE " & sCol & " <> 0"

                If Filter = False Then
                    sAnd = String.Empty
                Else
                    sAnd = " AND Status = 1"
                End If

            Else
                sWhere = " WHERE " & sCol & " = " & nID

                If Filter = True Then
                    sAnd &= " AND Status = 1"
                End If
            End If

            SQL = "SELECT DISTINCT(Frequency) FROM ScheduleAttr " & sWhere

            If Filter = True Then
                SQL &= " AND Status =1"
            End If

            oRs = clsMarsData.GetData(SQL)

            oTree.BeginUpdate()

            oTree.Nodes.Clear()


            If Not oRs Is Nothing And oRs.EOF = False Then
                Do While oRs.EOF = False
                    oFreq = oTree.Nodes.Add(oRs("frequency").Value)

                    oFreq.ImageIndex = 1
                    oFreq.SelectedImageIndex = 1
                    oFreq.Tag = String.Empty

                    Select Case sType
                        Case enScheduleType.REPORT
                            SQL = "SELECT ReportTitle, R.ReportID, NextRun, Status FROM " & _
                                "ReportAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.ReportID = S.ReportID " & _
                                "WHERE R.PackID = 0 AND Frequency = '" & oFreq.Text & "'" & sAnd
                        Case enScheduleType.PACKAGE
                            SQL = "SELECT PackageName, R.PackID, NextRun, Status FROM " & _
                                "PackageAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.PackID = S.PackID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case enScheduleType.AUTOMATION
                            SQL = "SELECT AutoName, R.AutoID, NextRun, Status FROM " & _
                                "AutomationAttr R " & _
                                "INNER JOIN " & _
                                "ScheduleAttr S " & _
                                "ON R.AutoID = S.AutoID " & _
                                "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case enScheduleType.SMARTFOLDER
                            SQL = "SELECT SmartName, R.SmartID, NextRun, Status FROM " & _
                            "SmartFolders R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.SmartID = S.SmartID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                        Case enScheduleType.EVENTPACKAGE
                            SQL = "SELECT PackageName, R.EventPackID, NextRun,Status FROM " & _
                            "EventPackageAttr R " & _
                            "INNER JOIN ScheduleAttr S " & _
                            "ON R.EventPackID = S.EventPackID " & _
                            "WHERE Frequency = '" & oFreq.Text & "'" & sAnd
                    End Select

                    oRs1 = clsMarsData.GetData(SQL)

                    Do While oRs1.EOF = False
                        oReport = oFreq.Nodes.Add(oRs1(0).Value)
                        nObjectID = oRs1(1).Value

                        Try
                            nStatus = oRs1("status").Value
                        Catch ex As Exception
                            nStatus = 1
                        End Try

                        oReport.ImageIndex = 0
                        oReport.SelectedImageIndex = 0
                        oReport.Tag = nObjectID

                        If oFreq.Text.ToLower = "none" Or nStatus = 0 Then
                            oRec = oReport.Nodes.Add("Disabled")
                        Else
                            oRec = oReport.Nodes.Add("Recurring " & CTimeZ(oRs1(2).Value, dateConvertType.READ))
                        End If

                        oRec.ImageIndex = 4
                        oRec.SelectedImageIndex = 4

                        SQL = "SELECT * FROM ScheduleHistory " & _
                        "WHERE " & sCol & "=" & nObjectID & _
                        " ORDER BY EntryDate " & SortOrder

                        oRs2 = clsMarsData.GetData(SQL)

                        Do While oRs2.EOF = False

                            Dim historyID As Integer = oRs2(0).Value
                            Dim fSuccess As Integer = oRs2("success").Value

                            If IsPartialSuccess(historyID) = True Then
                                oHistory = oReport.Nodes.Add(CTimeZ(IsNull(oRs2("entrydate").Value), dateConvertType.READ) & " - Partial Success: " & IsNull(oRs2("errmsg").Value))
                                oHistory.ImageIndex = 6
                                oHistory.SelectedImageIndex = 6
                            ElseIf oRs2("success").Value = 1 Then
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - Success: " & IsNull(oRs2("errmsg").Value))
                                oHistory.ImageIndex = 2
                                oHistory.SelectedImageIndex = 2
                            Else
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - " & oRs2("errmsg").Value)
                                oHistory.ImageIndex = 3
                                oHistory.SelectedImageIndex = 3
                            End If

                            Dim oStarted As TreeNode = oHistory.Nodes.Add("Schedule Initiated @ " & _
                            IsNull(CTimeZ(oRs2("startdate").Value, dateConvertType.READ)))

                            If sType = enScheduleType.EVENTPACKAGE Then
                                SQL = "SELECT h.LastFired AS EntryDate,h.Status AS Success,h.ErrMsg,h.StartDate, r.EventName AS ReportTitle FROM EventHistory h INNER JOIN EventAttr6 r ON h.EventID = r.EventID WHERE MasterHistoryID = " & historyID
                            Else
                                SQL = "SELECT h.EntryDate,h.Success,h.ErrMsg,h.StartDate, r.ReportTitle FROM HistoryDetailAttr h INNER JOIN ReportAttr r ON h.ReportID = r.ReportID WHERE HistoryID = " & historyID
                            End If

                            Dim oRs3 As ADODB.Recordset = clsMarsData.GetData(SQL)
                            Dim bAtLeastOneSuccess As Boolean = False
                            Dim bAtLeastOneFailure As Boolean = False

                            Do While oRs3.EOF = False

                                Dim detailNode As TreeNode

                                If oRs3("success").Value = 1 Then
                                    bAtLeastOneSuccess = True

                                    If (fSuccess = 1 AndAlso bAtLeastOneFailure) Then
                                        oHistory.Text = CTimeZ(IsNull(oRs2("entrydate").Value), dateConvertType.READ) & " - Partial Success"
                                        oHistory.ImageIndex = 6
                                        oHistory.SelectedImageIndex = 6
                                    End If

                                    detailNode = oHistory.Nodes.Add(ConTime(oRs3("entrydate").Value) & Space(10) & oRs3("reporttitle").Value & " - Success: " & IsNull(oRs3("errmsg").Value))

                                    detailNode.ImageIndex = 2
                                    detailNode.SelectedImageIndex = 2
                                Else
                                    bAtLeastOneFailure = True

                                    If (fSuccess = 1 AndAlso bAtLeastOneSuccess) Then
                                        oHistory.Text = CTimeZ(IsNull(oRs2("entrydate").Value), dateConvertType.READ) & " - Partial Success"
                                        oHistory.ImageIndex = 6
                                        oHistory.SelectedImageIndex = 6
                                    ElseIf (fSuccess = 0) Then
                                        oHistory.Text = CTimeZ(IsNull(oRs2("entrydate").Value), dateConvertType.READ) & " - Execution cancelled due to at least one failed item"
                                        oHistory.ImageIndex = 3
                                        oHistory.SelectedImageIndex = 3
                                    End If

                                    detailNode = oHistory.Nodes.Add(ConTime(oRs3("entrydate").Value) & Space(10) & oRs3("reporttitle").Value & " - " & IsNull(oRs3("errmsg").Value))

                                    detailNode.ImageIndex = 3
                                    detailNode.SelectedImageIndex = 3
                                End If

                                Dim subDetail As TreeNode

                                Try
                                    subDetail = detailNode.Nodes.Add("Initiated @ " & IsNull(ConTime(oRs3("startdate").Value)))
                                Catch
                                    subDetail = detailNode.Nodes.Add("Initiated @ <Invalid starttime ignored by system>")
                                End Try

                                subDetail.ImageIndex = 5
                                subDetail.SelectedImageIndex = 5

                                oRs3.MoveNext()
                            Loop

                            oRs3.Close()
                            oStarted.ImageIndex = 5
                            oStarted.SelectedImageIndex = 5

                            oHistory.Tag = "History"

                            oRs2.MoveNext()
                        Loop

                        oRs2.Close()

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()


                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Dim oNode As TreeNode

            For Each oNode In oTree.Nodes
                If oNode.Tag <> "History" Then oNode.Expand()
            Next

            oTree.EndUpdate()

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    'Public Function GetCustomNextRunDate(ByVal CalendarName As String, Optional ByVal StartDate As String = "") As String
    '    Dim oRs As ADODB.Recordset
    '    Dim SQL As String

    '    If StartDate.Length = 0 Then StartDate = ConDate(Now.Date)

    '    SQL = "SELECT TOP 1 CalendarDate FROM CalendarAttr WHERE " & _
    '    "CalendarName = '" & SQLPrepare(CalendarName) & "' " & _
    '    "AND CalendarDate > '" & ConDate(StartDate) & "' ORDER BY CalendarDate"

    '    oRs = clsMarsData.GetData(SQL)

    '    If Not oRs Is Nothing Then
    '        With oRs
    '            If .EOF = False Then
    '                Dim temp As String = oRs(0).Value

    '                Return temp
    '            Else
    '                Return GetCustomEndDate(CalendarName)
    '            End If
    '            .Close()
    '        End With
    '    Else
    '        Return String.Empty
    '    End If
    'End Function

    Public Function GetCustomNextRunDate(ByVal CalendarName As String, _
    Optional ByVal StartDate As String = "", Optional ByVal nScheduleID As Integer = 0) As String
        Dim SQL As String = "SELECT * FROM CalendarAttr WHERE " & _
        "CalendarName = '" & SQLPrepare(CalendarName) & "'"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim allDates As ArrayList = New ArrayList
        Dim isHoliday As Boolean = False
        Dim holidayForm As String = ""
        Dim dateValue As Date
        Dim year As Integer

        If StartDate = "" Then
            StartDate = ConDate(Now)
        End If

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 0 WHERE ScheduleID =" & nScheduleID)
                Throw New Exception("The schedule has been disabled because the calendar '" & CalendarName & "' does not exist.  Edit the schedule and select a calendar from the list.")
            End If

            Do While oRs.EOF = False
                isHoliday = IsNull(oRs("isholiday").Value, 0)
                holidayForm = IsNull(oRs("holidayform").Value, "")

                If isHoliday = True And holidayForm.Length > 0 Then
                    Dim countryCode As String = holidayForm.Split("|")(0)
                    Dim holidayName As String = holidayForm.Split("|")(1)
                    Dim oHol As Dls.Holidays.HolidayService = New Dls.Holidays.HolidayService
                    Dim csshol As cssholidays.holiday = New cssholidays.holiday

                    year = Now.Year

                    Do
                        Dim ds As DataSet

                        If countryCode = "AUS" Or countryCode = "NZ" Then
                            ds = csshol.GetHolidaysForYear(countryCode, year)
                        Else
                            ds = oHol.GetHolidaysForYear(countryCode, year)
                        End If

                        Dim dt As DataTable = ds.Tables(0)

                        Dim rows() As DataRow = dt.Select("Name ='" & SQLPrepare(holidayName) & "'")

                        If rows.Length = 0 Then
                            dateValue = oRs("calendardate").Value
                        Else
                            Dim row As DataRow = rows(0)

                            dateValue = row("Date")
                        End If

                        year += 1
                    Loop Until ConDate(dateValue) > ConDate(StartDate)
                Else
                    dateValue = oRs("calendardate").Value
                End If

                allDates.Add(dateValue)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        allDates.Sort()


        Dim nextRun As String = ""

        For Each s As String In allDates
            If ConDate(s) > ConDate(StartDate) Then
                nextRun = s
                Exit For
            End If
        Next

        If nextRun = "" Then
            Return GetCustomEndDate(CalendarName)
        Else
            Return nextRun
        End If
    End Function

    Public Sub RunSystemBackup()
10:     Dim oRs As New ADODB.Recordset
        Dim sPath As String = sAppPath & "backup.xml"
20:     Dim oParse As New clsMarsParser
30:     Dim oSys As New clsSystemTools
40:     Dim oZip As New dzactxctrl

        'check to see if it is advanced or simple: read schedule attr
50:     Dim oRs1 As New ADODB.Recordset
60:     oRs1 = clsMarsData.GetData("select * from scheduleattr where scheduleid = 11")
        Dim aStartDate As Date
        Dim aEnddate As Date
        Dim aFrequency As String
        Dim aNextRun As DateTime
        Dim aStartTime As String
        Dim aRepeatInterval As String
        Dim aRepeat As Int32
        Dim aRepeatUntil As String
        Dim aStatus As Int32
        Dim aCalendarName As String
        Dim aUseException As Int32
        Dim aExceptionCalendar As String
        Dim aRepeatUnit As String
        Dim bZipName As String
        Dim BPath As String = ""
        Dim IsZip As Boolean
        Dim ZipPath As String
        Dim sFrequency As String
        Dim nInterval As Integer
        Dim sBackupPath As String
        Dim NextRun As Date
        Dim KeepFor As Integer
        Dim runAdvancedBackup As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("AdvancedBackupSchedule", 0)))

70:     aStatus = 1

80:     Try
90:         If Not oRs1 Is Nothing Then
100:            If oRs1.EOF = False Then
110:                aStartDate = oRs1("StartDate").Value
120:                aEnddate = oRs1("EndDate").Value
130:                aFrequency = IsNull(oRs1("Frequency").Value)
140:                aNextRun = oRs1("NextRun").Value
150:                aStartTime = IsNull(oRs1("StartTime").Value)
160:                aRepeatInterval = IsNull(oRs1("RepeatInterval").Value)
170:                aRepeat = IsNull(oRs1("Repeat").Value, 0)
180:                aRepeatUntil = IsNull(oRs1("RepeatUntil").Value)
190:                aStatus = IsNull(oRs1("Status").Value, 5)
200:                aCalendarName = IsNull(oRs1("CalendarName").Value)
210:                aUseException = IsNull(oRs1("UseException").Value, 0)
220:                aExceptionCalendar = IsNull(oRs1("ExceptionCalendar").Value)
230:                aRepeatUnit = IsNull(oRs1("RepeatUnit").Value)
240:                bZipName = clsMarsParser.Parser.ParseString(IsNull(oRs1("bZipName").Value))
250:                BPath = clsMarsParser.Parser.ParseString((oRs1("bPath").Value))

260:                If BPath.EndsWith("\") = False Then BPath = BPath & "\"
270:                ZipPath = BPath & bZipName & ".zip"
                Else
                    If runAdvancedBackup = True Then Return
                End If
280:            oRs1.Close()
            End If

290:        If System.IO.File.Exists(sPath) Then

300:            oRs = clsMarsData.GetXML(sPath)

310:            If oRs Is Nothing Then Return

320:            sFrequency = oRs("frequency").Value
330:            nInterval = oRs("repeatevery").Value
340:            sBackupPath = clsMarsParser.Parser.ParseString(IsNull(oRs("backuppath").Value, ""))
350:            NextRun = oRs("nextrun").Value

360:            If sBackupPath = "" Then Return

370:            Try
380:                KeepFor = oRs("keepfor").Value
390:            Catch
400:                KeepFor = 5
                End Try
            Else
                If runAdvancedBackup = False Then Return
            End If

            'for simple

410:        If aStatus = 0 Or aStatus = 1 Or aStatus = 5 Then
420:            sBackupPath = _CreateUNC(sBackupPath)

430:            If NextRun < Now Then

440:                oSys._BackupSystem(sBackupPath, False)
450:                Try
460:                    For Each s As String In IO.Directory.GetDirectories(sBackupPath)
                            Dim d As Date
470:                        d = IO.Directory.GetCreationTime(s)

480:                        Select Case aFrequency.ToLower
                                Case "daily"
490:                                If DateDiff(DateInterval.Day, d, Now.Date) > KeepFor Then
500:                                    IO.Directory.Delete(s, True)
                                    End If
510:                            Case "weekly"
520:                                If DateDiff(DateInterval.Day, d, Now.Date) > (KeepFor * 7) Then
530:                                    IO.Directory.Delete(s, True)
                                    End If
540:                            Case "monthly"
550:                                If DateDiff(DateInterval.Month, d, Now.Date) > KeepFor Then
560:                                    IO.Directory.Delete(s, True)
                                    End If
570:                            Case "yearly"
580:                                If DateDiff(DateInterval.Year, d, Now.Date) > KeepFor Then
590:                                    IO.Directory.Delete(s, True)
                                    End If
                            End Select
600:                    Next
                    Catch : End Try

610:                Select Case sFrequency.ToLower
                        Case "daily"
620:                        NextRun = NextRun.AddDays(nInterval)
630:                    Case "weekly"
640:                        NextRun = NextRun.AddDays(nInterval * 7)
650:                    Case "monthly"
660:                        NextRun = NextRun.AddMonths(nInterval)
670:                    Case "yearly"
680:                        NextRun = NextRun.AddYears(nInterval)
                    End Select

690:                oRs("nextrun").Value = NextRun

700:                oRs.Update()

710:                clsMarsData.CreateXML(oRs, sPath)
                End If
            End If

            ' use advanced
720:        If aStatus = 2 Or aStatus = 3 Then
730:            If aStatus = 3 Then
740:                IsZip = True
750:            Else
760:                IsZip = False
                End If

770:            If IsZip = False Then
780:                If ConDateTime(aNextRun) < ConDateTime(Now) And ConDate(aEnddate) > ConDate(Now) Then

790:                    BPath = _CreateUNC(BPath)

800:                    oSys._BackupSystem(BPath, False, False)
                    End If
                End If

810:            If IsZip = True Then
820:                If ConDateTime(aNextRun) < ConDateTime(Now) And ConDate(aEnddate) > ConDate(Now) Then

830:                    Dim TempPath As String = sAppPath & "Output\11"

840:                    oParse.ParseDirectory(TempPath)

850:                    _Delay(1)
860:                    oSys._BackupSystem(TempPath, False, False)
870:                    _Delay(1)
880:                    BPath = _CreateUNC(BPath)
890:                    If System.IO.File.Exists(ZipPath) = True Then
900:                        System.IO.File.Delete(ZipPath)
                        End If

910:                    With oZip
920:                        .QuietFlag = True
930:                        .ZipSubOptions = 1
940:                        .ItemList = Chr(34) & TempPath & "*.*" & Chr(34)
950:                        .NoDirectoryEntriesFlag = True
960:                        .NoDirectoryNamesFlag = False
970:                        .RecurseFlag = True
980:                        .DeleteOriginalFlag = False
990:                        .ZIPFile = ZipPath
1000:                       .CompressionFactor = DZACTXLib.CompFactor.COMPFACTOR_9
1010:                       .ActionDZ = DZACTION.ZIP_ADD
                        End With
1020:                   oZip = Nothing
1030:                   DoDeleteFiles(TempPath & "\*.*", True)

                    End If
                End If

                'clear old files
1040:           Try
1050:               For Each s As String In IO.Directory.GetDirectories(BPath)
                        Dim d As Date
1060:                   d = IO.Directory.GetCreationTime(s)
1070:                   If DateDiff(DateInterval.Day, d, Now.Date) > KeepFor Then
1080:                       IO.Directory.Delete(s, True)
                        End If
1090:               Next
                Catch : End Try
                ' calculate next run and update
1100:           aNextRun = GetNextRun(11, aFrequency, aStartDate, aStartTime, aRepeat, aRepeatInterval, aRepeatUntil, aCalendarName, , , aStartTime, aRepeatUnit)
1110:           clsMarsData.WriteData("Update ScheduleAttr set nextrun = '" & ConDateTime(aNextRun) & "' where scheduleid = 11")
            End If
1120:   Catch ex As Exception
1130:       _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Public Function GetCustomEndDate(ByVal CalendarName As String) As String
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim dates As ArrayList = New ArrayList
        Dim isHoliday As Boolean = False
        Dim holidayForm As String
        Dim year As Integer
        Dim dateValue As Date

        SQL = "SELECT * FROM CalendarAttr WHERE " & _
        "CalendarName = '" & SQLPrepare(CalendarName) & "'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then

            If oRs.EOF = True Then Return ConDate(Now.Date.AddDays(1))

            Do While oRs.EOF = False

                isHoliday = IsNull(oRs("isholiday").Value, 0)
                holidayForm = IsNull(oRs("holidayform").Value, "")

                If isHoliday = True And holidayForm.Length > 0 Then
                    Dim countryCode As String = holidayForm.Split("|")(0)
                    Dim holidayName As String = holidayForm.Split("|")(1)
                    Dim oHol As Dls.Holidays.HolidayService = New Dls.Holidays.HolidayService
                    Dim csshol As cssholidays.holiday = New cssholidays.holiday
                    year = Now.Year

                    Do
                        Dim ds As DataSet

                        If countryCode = "AUS" Or countryCode = "NZ" Then
                            ds = csshol.GetHolidaysForYear(countryCode, year)
                        Else
                            ds = oHol.GetHolidaysForYear(countryCode, year)
                        End If

                        Dim dt As DataTable = ds.Tables(0)

                        Dim rows() As DataRow = dt.Select("Name ='" & SQLPrepare(holidayName) & "'")

                        If rows.Length = 0 Then
                            dateValue = oRs("calendardate").Value
                        Else
                            Dim row As DataRow = rows(0)

                            dateValue = row("Date")
                        End If

                        year += 100
                    Loop Until ConDate(dateValue) > ConDate(Now.Date)
                Else
                    dateValue = oRs("calendardate").Value
                End If

                dates.Add(dateValue)

                oRs.MoveNext()
            Loop

            oRs.Close()

        End If

        dates.Sort()

        Return dates(dates.Count - 1)
    End Function
    Public Function GetNextMonth(ByVal sMonths As String, Optional ByVal nMonth As Integer = 0) As Date
        Dim sList() As String
        Dim I As Integer
        Dim NewMonth As Integer
        Dim NewYear As Integer
        Dim nTemp As Integer

        sList = sMonths.Split("|")

        If nMonth = 0 Then
            nMonth = Now.Month
        End If

        For I = 0 To sList.GetUpperBound(0)
            Try
                nTemp = sList(I)
            Catch
                nTemp = 0
            End Try

            If nTemp > nMonth Then
                NewMonth = sList(I)
                NewYear = Now.Year
                Exit For
            End If
        Next

        If NewMonth = 0 Then
            NewMonth = sList(0)
            NewYear = Now.Year + 1
        End If

        Return NewYear & "-" & NewMonth & "-01"

    End Function

    Public Function GetNthDayOfMonth(ByVal StartDate As Date, ByVal n As Integer, ByVal nDay As Integer) As Date
        Dim DayCounter As Date
        Dim I As Integer

        DayCounter = StartDate

        If nDay = 7 Then
            Select Case n
                Case 5
                    DayCounter = DayCounter.AddMonths(1)

                    DayCounter = DayCounter.AddDays(-DayCounter.Day)
                Case Else
                    DayCounter = New Date(DayCounter.Year, DayCounter.Month, n)
            End Select

            Return DayCounter
        End If

        'check the number of occurences for that date
        Do While DayCounter.Month = StartDate.Month
            If DayCounter.DayOfWeek = nDay Then I += 1

            DayCounter = DayCounter.AddDays(1)
        Loop

        If n > I Then n = I

        DayCounter = StartDate

        I = 0

        Do While DayCounter.Month = StartDate.Month
            If DayCounter.DayOfWeek = nDay Then
                I = I + 1

                If I = n Then
                    Exit Do
                End If
            End If

            DayCounter = DayCounter.AddDays(1)
        Loop

        Return DayCounter
    End Function
    Public Function GetNextRunForWeekly(ByVal StartDate As Date, ByVal nCount As Integer, ByVal sDays As String) As Date
        Dim DayCounter As Date
        Dim n As Integer
        Dim AddWeeks As Boolean

        'get last run day
        Dim LastRunDay As Integer = Now.DayOfWeek
        Dim NextRunDay As Integer = 0

        If sDays.EndsWith("|") = True Then
            sDays = sDays.Substring(0, sDays.Length - 1)
        End If

        'get next run day
        Dim sDaysAr() As String = sDays.Split("|")

        For Each s As String In sDaysAr
            Try
                n = s
            Catch ex As Exception
                n = 0
            End Try

            If n > LastRunDay Then
                NextRunDay = n
                Exit For
            End If
        Next

        If NextRunDay = 0 Then NextRunDay = sDaysAr(0) : AddWeeks = True

        If AddWeeks = True Then
            DayCounter = Now.AddDays(nCount * 7)

            Do While DayCounter.DayOfWeek <> DayOfWeek.Sunday
                DayCounter = DayCounter.AddDays(-1)
            Loop

            Do While DayCounter.DayOfWeek <> NextRunDay
                DayCounter = DayCounter.AddDays(1)
            Loop
        Else
            DayCounter = Now.Date

            Do While DayCounter.DayOfWeek <> NextRunDay
                DayCounter = DayCounter.AddDays(1)
            Loop
        End If

        Return DayCounter

    End Function

    Public Function GetNextWeek(ByVal sDays As String) As Date
        Dim DayCounter As Date
        Dim I As Integer
        Dim sList() As String
        Dim nDay As Integer = 0
        Dim nTemp As Integer

        DayCounter = Now.Date

        'first lets get the next available day
        If sDays.EndsWith("|") = True Then
            sDays = sDays.Substring(0, sDays.Length - 1)
        End If

        sList = sDays.Split("|")

        For I = 0 To sList.GetUpperBound(0)
            Try
                nTemp = sList(I)
            Catch ex As Exception
                nTemp = 0
                Exit For
            End Try

            If nTemp > DayCounter.DayOfWeek Then
                nDay = nTemp
                Exit For
            End If
        Next

        If nDay = 0 Then nDay = sList(0)

        'now lets move forward to the said date

        If DayCounter.DayOfWeek <> nDay Then
            Do While DayCounter.DayOfWeek <> nDay
                DayCounter = DayCounter.AddDays(1)
            Loop
        End If

        Return DayCounter

    End Function

    Public Sub DeleteAllHistory(ByVal oType As enScheduleType)
        Dim SQL As String

        Select Case oType
            Case enScheduleType.AUTOMATION
                SQL = "DELETE FROM ScheduleHistory WHERE AutoID > 0"
            Case enScheduleType.PACKAGE
                SQL = "DELETE FROM ScheduleHistory WHERE PackID > 0"
            Case enScheduleType.REPORT
                SQL = "DELETE FROM ScheduleHistory WHERE ReportID > 0"
        End Select

        clsMarsData.WriteData(SQL, False)

        clsMarsData.WriteData("DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory)")

        If oType = enScheduleType.REPORT Or oType = enScheduleType.PACKAGE Then
            Dim res As DialogResult = MessageBox.Show("Would you like to remove all report duration information?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If res = Windows.Forms.DialogResult.Yes Then
                clsMarsData.WriteData("DELETE FROM ReportDuration")
                clsMarsData.WriteData("DELETE FROM ReportDurationTracker")
            End If
        End If

        MessageBox.Show("History cleared successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        _Delay(1)
    End Sub

    Public Sub RefreshSchedules()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sType As String
        Dim nID As Integer
        Dim RefreshID As Integer
        Dim sFreq As String
        Dim nRepeat As Integer
        Dim oReport As New clsMarsReport
        Dim NextRun As Date
        Dim StartTime As String
        Dim sName As String

        Try
            If gConType = "DAT" Then
                SQL = "SELECT * FROM RefresherAttr WHERE NextRun <= Now()"
            Else
                SQL = "SELECT * FROM RefresherAttr WHERE NextRun <= GetDate()"
            End If

            oRs = clsMarsData.GetData(SQL)

            Do While oRs.EOF = False
                sType = oRs("objecttype").Value
                nID = oRs("objectid").Value
                RefreshID = oRs("refreshid").Value
                sFreq = oRs("frequency").Value
                nRepeat = oRs("repeatevery").Value
                StartTime = oRs("starttime").Value
                sName = oRs("objectname").Value

                Select Case sType.ToLower
                    Case "report"
                        oReport.RefreshReport(nID)
                    Case "package"
                        oReport.RefreshPackage(nID)
                    Case "folder", "desktop"
                        Dim oChild As ADODB.Recordset

                        If sName.ToLower = "desktop" Then
                            SQL = "SELECT * FROM ReportAttr WHERE PackID =0"
                        Else
                            SQL = "SELECT ReportID FROM ReportAttr WHERE Parent = " & nID
                        End If

                        oChild = clsMarsData.GetData(SQL)

                        Do While oChild.EOF = False
                            oReport.RefreshReport(oChild.Fields(0).Value)

                            oChild.MoveNext()
                        Loop

                        oChild.Close()

                        If sName.ToLower = "desktop" Then
                            SQL = "SELECT * FROM PackageAttr"
                        Else
                            SQL = "SELECT PackID FROM PackageAttr WHERE Parent =" & nID
                        End If

                        oChild = clsMarsData.GetData(SQL)

                        Do While oChild.EOF = False
                            oReport.RefreshPackage(oChild.Fields(0).Value)

                            oChild.MoveNext()
                        Loop

                        oChild.Close()
                End Select

                Select Case sFreq
                    Case "Daily"
                        NextRun = Now.Date.AddDays(nRepeat)
                    Case "Weekly"
                        NextRun = Now.Date.AddDays(nRepeat * 7)
                    Case "Monthly"
                        NextRun = Now.Date.AddMonths(nRepeat)
                    Case "Yearly"
                        NextRun = Now.Date.AddYears(nRepeat)
                End Select

                NextRun = ConDate(NextRun) & " " & StartTime

                SQL = "UPDATE RefresherAttr SET LastRun = '" & ConDateTime(Date.Now) & "', NextRun ='" & NextRun & "' WHERE RefreshID = " & RefreshID

                clsMarsData.WriteData(SQL, False)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Private Sub grid_QueryGroupKeys(ByVal sender As Object, ByVal e As Xceed.Grid.QueryGroupKeysEventArgs) Handles m_grid.QueryGroupKeys
        Dim i As Integer

        Try
            For i = 0 To e.GroupKeys.Count - 1
                If TypeOf e.GroupKeys(i) Is String Then
                    e.GroupKeys(i) = (CType(e.GroupKeys(i), String)).ToUpper()
                End If
            Next
        Catch exception As Exception
            MessageBox.Show(exception.ToString())
        End Try
    End Sub
    Public Sub DrawScheduleHistory6(ByVal grid As Xceed.Grid.GridControl, ByVal sType As enScheduleType, Optional ByVal nID As Integer = 0, Optional ByVal filter As Boolean = False)
        Dim SQL As String = ""
        Dim oRs As ADODB.Recordset

        Select Case sType
            Case enScheduleType.REPORT
                SQL = "SELECT " & _
                "ReportName,Frequency,NextRun,Status,EntryDate,Success,ErrMsg,ScheduleHistory.StartDate, " & _
                "Description,Keyword,CalendarName,ExceptionCalendar,Repeat,RepeatInterval,RepeatUntil,RepeatUnit," & _
                "DisabledDate,StartTime,EndDate " & _
                "FROM (ReportAttr INNER JOIN ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID) INNER JOIN " & _
                "ScheduleHistory ON ScheduleAttr.ReportID = ScheduleHistory.ReportID"

                If nID > 0 Then
                    SQL &= " WHERE ReportAttr.ReportID =" & nID
                End If

                SQL &= " ORDER BY EntryDate DESC"
            Case enScheduleType.PACKAGE
                SQL = "SELECT " & _
                "PackageName,Frequency,NextRun,Status,EntryDate,Success,ErrMsg,ScheduleHistory.StartDate, " & _
                "Description,Keyword,CalendarName,ExceptionCalendar,Repeat,RepeatInterval,RepeatUntil,RepeatUnit," & _
                "DisabledDate,StartTime,EndDate " & _
                "FROM (PackageAttr INNER JOIN ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID) INNER JOIN " & _
                "ScheduleHistory ON ScheduleAttr.PackID = ScheduleHistory.PackID"

                If nID > 0 Then
                    SQL &= " WHERE PackageAttr.PackID =" & nID
                End If

                SQL &= " ORDER BY EntryDate DESC"
            Case enScheduleType.AUTOMATION
                SQL = "SELECT " & _
                "AutoName,Frequency,NextRun,Status,EntryDate,Success,ErrMsg,ScheduleHistory.StartDate, " & _
                "Description,Keyword,CalendarName,ExceptionCalendar,Repeat,RepeatInterval,RepeatUntil,RepeatUnit," & _
                "DisabledDate,StartTime,EndDate " & _
                "FROM (AutomationAttr INNER JOIN ScheduleAttr ON AutomationAttr.AutoID = ScheduleAttr.AutoID) " & _
                "INNER JOIN ScheduleHistory ON ScheduleAttr.AutoID = ScheduleHistory.AutoID"

                If nID > 0 Then
                    SQL &= " WHERE AutomationAttr.AutoID =" & nID
                End If

                SQL &= " ORDER BY EntryDate DESC"
            Case enScheduleType.EVENTBASED
                SQL = "SELECT " & _
                "EventName,'Event-Based' AS Frequency,'Event-Based' AS NextRun,EventAttr6.Status,LastFired,EventHistory.Status,ErrMsg,EventHistory.StartDate, " & _
                "Description,Keyword,'' AS CalendarName, '' AS ExceptionCalendar,0 AS Repeat, 'N/A' AS RepeatInterval, 'N/A' AS RepeatUntil, '' AS RepeatUnit," & _
                "DisabledDate,'Event-Based' AS StartTime, 'Event-Based AS EndDate " & _
                "FROM (EventAttr6 LEFT OUTER JOIN EventHistory ON EventAttr6.EventID = EventHistory.EventID)"

                If nID > 0 Then
                    SQL &= " WHERE EventAttr6.EventID =" & nID
                End If

                SQL &= " ORDER BY LastFired DESC"
            Case enScheduleType.EVENTPACKAGE
                SQL = "SELECT " & _
                "PackageName,Frequency,NextRun,Status,EntryDate,Success,ErrMsg,ScheduleHistory.StartDate, " & _
                "Description,Keyword,CalendarName,ExceptionCalendar,Repeat,RepeatInterval,RepeatUntil,RepeatUnit," & _
                "DisabledDate,StartTime,EndDate " & _
                "FROM (EventPackageAttr INNER JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) " & _
                "INNER JOIN ScheduleHistory ON ScheduleAttr.EventPackID = ScheduleHistory.EventPackID"

                If nID > 0 Then
                    SQL &= " WHERE EventPackageAttr.EventPackID =" & nID
                End If

                SQL &= " ORDER BY EntryDate DESC"
            Case enScheduleType.SMARTFOLDER
                SQL = "SELECT " & _
                "SmartName,Frequency,NextRun,Status,EntryDate,Success,ErrMsg,ScheduleHistory.StartDate, " & _
                "Description,Keyword,CalendarName,ExceptionCalendar,Repeat,RepeatInterval,RepeatUntil,RepeatUntil," & _
                "DisabledDate,StartTime,EndDate " & _
                "FROM (SmartFolders INNER JOIN ScheduleAttr ON SmartFolders.SmartID = ScheduleAttr.SmartID) " & _
                "INNER JOIN ScheduleHistory ON ScheduleAttr.SmartID = ScheduleHistory.SmartID"

                If nID > 0 Then
                    SQL &= " WHERE SmartFolders.SmartID =" & nID
                End If

                SQL &= " ORDER BY EntryDate DESC"
        End Select

        Me.m_grid = grid

        grid.Clear()

        grid.FixedHeaderRows.Add(New Xceed.Grid.GroupByRow)
        grid.FixedHeaderRows.Add(New Xceed.Grid.ColumnManagerRow)

        Dim xls As Xceed.Grid.StyleSheet = New Xceed.Grid.StyleSheet

        grid.ApplyStyleSheet(xls.Document)

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Dim dt As DataTable = New DataTable("History")

            With dt.Columns
                .Add("Name")
                .Add("Frequency")
                .Add("Enabled", Type.GetType("System.Boolean"))
                .Add("Recurring")
                .Add("Last Run")
                .Add("Duration")
                .Add("Success")
                .Add("Result")
                .Add("Description")
                .Add("Keywords")
                .Add("Calendar Name")
                .Add("Exception Calendar")
                .Add("Repeat", Type.GetType("System.Boolean"))
                .Add("Repeat Interval")
                .Add("Repeat Until")
                .Add("Disabled Date")
                .Add("Execution Time")
                .Add("End Date")
            End With

            Do While oRs.EOF = False
                Dim row As DataRow = dt.Rows.Add

                row("Name") = oRs(0).Value
                row("Frequency") = oRs("frequency").Value
                row("Recurring") = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)

                If IsNull(oRs(3).Value, "0") = "0" Then
                    row("Enabled") = False
                    Try
                        row("disabled date") = CTimeZ(IsNull(oRs("disableddate").Value), dateConvertType.READ)
                    Catch
                        row("disabled date") = ConDate(Date.Now)
                    End Try
                ElseIf IsNull(oRs(3).Value, "0") = "1" Then
                    row("Enabled") = True
                    row("disabled date") = ""
                End If

                row("Last Run") = CTimeZ(oRs(4).Value, dateConvertType.READ)

                If IsNull(oRs(4).Value, "") <> "" Then
                    Dim duration As Double = 0.0
                    Dim lastrun As Date = IsNull(oRs(4).Value, "")
                    Dim startrun As Date = IsNull(oRs(7).Value, "")

                    duration = lastrun.Subtract(startrun).TotalMinutes

                    row("Duration") = Math.Round(duration, 2) & " seconds"
                Else
                    row("Duration") = ""
                End If

                If IsNull(oRs(5).Value, "") = "0" Then
                    row("Success") = False
                ElseIf IsNull(oRs(5).Value, "") = "1" Then
                    row("Success") = True
                Else
                    row("Success") = False
                End If

                row("result") = IsNull(oRs("errmsg").Value, "")

                '--------------hidden columns
                row("description") = IsNull(oRs("description").Value)
                row("keywords") = IsNull(oRs("keyword").Value)
                row("calendar name") = IsNull(oRs("calendarname").Value)
                row("exception calendar") = IsNull(oRs("exceptioncalendar").Value)

                If IsNull(oRs("repeat").Value, "0") = "0" Then
                    row("repeat") = False
                    row("repeat interval") = ""
                    row("repeat until") = ""
                Else
                    row("repeat") = True
                    row("repeat interval") = IsNull(oRs("repeatinterval").Value) & " " & IsNull(oRs("repeatunit").Value)
                    row("repeat until") = CTimeZ(IsNull(oRs("repeatuntil").Value), dateConvertType.READ)
                End If

                row("execution time") = CTimeZ(IsNull(oRs("starttime").Value), dateConvertType.READ)
                row("end date") = IsNull(oRs("enddate").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim ds As DataSet = New DataSet("History_Set")

            ds.Tables.Add(dt)

            grid.BeginInit()
            grid.DataSource = ds
            grid.DataMember = "History"

            grid.EndInit()

            'add groups
            Dim grp As Xceed.Grid.Group = New Xceed.Grid.Group
            grp.GroupBy = "Frequency"
            grp.HeaderRows.Add(New Xceed.Grid.GroupManagerRow)

            AddHandler grid.QueryGroupKeys, AddressOf grid_QueryGroupKeys
            grid.GroupTemplates.Add(grp)


            grp = New Xceed.Grid.Group
            grp.GroupBy = "Name"
            grp.HeaderRows.Add(New Xceed.Grid.GroupManagerRow)
            grid.GroupTemplates.Add(grp)


            grp = New Xceed.Grid.Group
            grp.GroupBy = "Recurring"
            grp.HeaderRows.Add(New Xceed.Grid.GroupManagerRow)
            grid.GroupTemplates.Add(grp)

            grid.UpdateGrouping()


            'format the columns

            grid.Columns("Enabled").Width = grid.Columns("Enabled").GetFittedWidth()
            grid.Columns("Success").Width = grid.Columns("Success").GetFittedWidth()
            grid.Columns("Last Run").Width = grid.Columns("Last Run").GetFittedWidth()
            grid.Columns("Result").Width = 250

            'hide all other columns
            grid.Columns("Frequency").Visible = False
            grid.Columns("Name").Visible = False
            grid.Columns("Recurring").Visible = False
            grid.Columns("Description").Visible = False
            grid.Columns("Keywords").Visible = False
            grid.Columns("Calendar Name").Visible = False
            grid.Columns("Exception Calendar").Visible = False
            grid.Columns("Repeat").Visible = False
            grid.Columns("Repeat Interval").Visible = False
            grid.Columns("Repeat Until").Visible = False
            grid.Columns("Disabled Date").Visible = False
            grid.Columns("Execution Time").Visible = False
            grid.Columns("End Date").Visible = False

            'highlight errored entries
            For Each r As Xceed.Grid.DataRow In grid.DataRows
                If r.Cells(7).Value = False Then
                    'r.Font = New System.Drawing.Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point, 0)
                    r.ForeColor = Color.Red
                    r.Height = 40
                End If
            Next


        End If
    End Sub

    Public Sub scheduleRetries()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim scheduleID, eventID As Integer
        Dim result As Boolean
        Dim retryID As Integer
        Dim entryDate As Date
        Dim maxRetry As Integer
        Dim retryCount As Integer
        Dim retryInterval As Integer
        Dim maxRetries As Integer
        Dim rsRetry As ADODB.Recordset
        Dim oReport As clsMarsReport = New clsMarsReport

        Me.m_progressID = "Retry:99998"

        Try
            TaskManager("Add", , , 9998)
            ThreadManager(9998, "Add", enScheduleType.AUTOMATION)

10:         SQL = "SELECT * FROM RetryTracker"

20:         oRs = clsMarsData.GetData(SQL)

30:         If oRs IsNot Nothing Then
40:             Do While oRs.EOF = False
50:                 scheduleID = IsNull(oRs("scheduleid").Value, 0)
60:                 retryID = oRs("retryid").Value
70:                 entryDate = oRs("entrydate").Value
80:                 retryCount = oRs("retrynumber").Value
90:                 eventID = IsNull(oRs("eventid").Value, 0)

100:                SQL = "SELECT * FROM ScheduleAttr WHERE ScheduleID =" & scheduleID

                    Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

110:                If oRs1 IsNot Nothing Then
120:                    If oRs1.EOF = False Then
                            Dim reportID As Integer
                            Dim packID As Integer
                            Dim tableName As String

130:                        reportID = IsNull(oRs1("reportid").Value, 0)
140:                        packID = IsNull(oRs1("packid").Value, 0)

150:                        If packID > 0 Then
160:                            SQL = "SELECT Retry, RetryInterval FROM PackageAttr WHERE PackID =" & packID
170:                        ElseIf reportID > 0 Then
180:                            SQL = "SELECT Retry, RetryInterval FROM ReportAttr WHERE ReportID =" & reportID
190:                        Else
200:                            GoTo skipRetry
                            End If

210:                        rsRetry = clsMarsData.GetData(SQL)

220:                        If rsRetry Is Nothing Then
230:                            clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
240:                            GoTo skipRetry
250:                        ElseIf rsRetry.EOF = True Then
260:                            clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
270:                            GoTo skipRetry
280:                        Else
290:                            maxRetry = IsNull(rsRetry("retry").Value, 0)
300:                            retryInterval = IsNull(rsRetry("retryinterval").Value, 0)
                            End If

310:                        If maxRetry = 0 Or retryInterval = 0 Then
320:                            clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
330:                            GoTo skipRetry
                            End If

340:                        If Date.Now.Subtract(entryDate).TotalMinutes < retryInterval Then GoTo skipRetry

350:                        If packID > 0 Then
360:                            If clsMarsData.IsScheduleDynamic(packID, "package") = True Then
370:                                result = oReport.RunDynamicPackageSchedule(packID)
380:                            Else
390:                                result = oReport.RunPackageSchedule(packID)
                                End If
400:                        ElseIf reportID > 0 Then
410:                            If clsMarsData.IsScheduleDataDriven(reportID) = True Then
440:                                result = oReport.RunDataDrivenSchedule(reportID)
450:                            ElseIf clsMarsData.IsScheduleDynamic(reportID, "report") = True Then
460:                                result = oReport.RunDynamicSchedule(reportID)
470:                            Else
480:                                result = oReport.RunSingleSchedule(reportID)
                                End If
                            End If

490:                        If result = True Then
500:                            SetScheduleHistory(True, , reportID, packID, , , , oReport.m_HistoryID)
510:                            clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
520:                        Else
                                'log failure in history
530:                            SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", reportID, packID, oReport.m_HistoryID)

                                'increment retry count, if retrycount in db = max retry count then delete it from the table
540:                            retryCount += 1

                                'update the entry date in the table to reflect the last time it was retried or delete it if it has reached max
550:                            If retryCount >= maxRetry Then
560:                                clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
570:                            Else
580:                                SQL = "UPDATE RetryTracker SET " & _
          "RetryNumber = " & retryCount & "," & _
          "EntryDate = '" & ConDateTime(Now) & "' " & _
          "WHERE RetryID =" & retryID

590:                                clsMarsData.WriteData(SQL)
                                End If
                            End If
600:                    Else
610:                        oRs1.Close()

620:                        SQL = "SELECT * FROM EventAttr6 WHERE EventID =" & eventID

630:                        oRs1 = clsMarsData.GetData(SQL)

640:                        If oRs1.EOF = False Then
650:                            maxRetry = IsNull(oRs1("retrycount").Value, 0)
660:                            retryInterval = IsNull(oRs1("retryinterval").Value, 0)

670:                            If maxRetry = 0 Or retryInterval = 0 Then
680:                                clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
690:                                GoTo skipRetry
                                End If

700:                            If Date.Now.Subtract(entryDate).TotalMinutes < retryInterval Then GoTo skipRetry

710:                            Dim oEvent As clsMarsEvent = New clsMarsEvent

720:                            oEvent.RunEvents6(eventID, True, result)

730:                            If result = True Then
740:                                clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
750:                            Else
760:                                retryCount += 1

770:                                If retryCount >= maxRetry Then
780:                                    clsMarsData.WriteData("DELETE FROM RetryTracker WHERE RetryID =" & retryID)
790:                                Else
800:                                    SQL = "UPDATE RetryTracker SET " & _
          "RetryNumber = " & retryCount & "," & _
          "EntryDate = '" & ConDateTime(Now) & "' " & _
          "WHERE RetryID =" & retryID

810:                                    clsMarsData.WriteData(SQL)
                                    End If
                                End If

                            End If
                        End If
                    End If
skipRetry:
820:                oRs.MoveNext()
830:            Loop

840:            oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            TaskManager("Remove", , , 9998)
            ThreadManager(9998, "Remove", enScheduleType.AUTOMATION)
        End Try
    End Sub


    Public Function GetOther(ByVal frequency As Integer, ByVal unit As String) As Date
        Dim startDate As Date = Now

        Select Case unit
            Case "Seconds"
                Return startDate.AddSeconds(frequency)
            Case "Minutes"
                Return startDate.AddMinutes(frequency)
            Case "Hours"
                Return startDate.AddHours(frequency)
            Case "Days"
                Return startDate.AddDays(frequency)
            Case "Weeks"
                Return startDate.AddDays(frequency * 7)
            Case "Months"
                Return startDate.AddMonths(frequency)
            Case "Years"
                Return startDate.AddYears(frequency)
            Case Else
                Return startDate
        End Select
    End Function

    Public Sub SetScheduleHistoryDetail(ByVal Success As Boolean, ByVal sErrMsg As String, _
    ByVal HistoryID As Integer, ByVal initTime As Date, ByVal ReportID As Integer)
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        sCols = "DetailID,HistoryID,ReportID,EntryDate,Success,ErrMsg,StartDate"

        sVals = clsMarsData.CreateDataID("HistoryDetailAttr", "DetailID") & "," & _
        HistoryID & "," & _
        ReportID & "," & _
        "'" & ConDateTime(Date.Now) & "'," & _
        Convert.ToInt32(Success) & "," & _
        "'" & SQLPrepare(sErrMsg) & "'," & _
        "'" & ConDateTime(initTime) & "'"

        clsMarsData.DataItem.InsertData("HistoryDetailAttr", sCols, sVals, False)

    End Sub

    Private Function IsPartialSuccess(ByVal historyID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim results As ArrayList = New ArrayList

        SQL = "SELECT * FROM historydetailattr WHERE historyid =" & historyID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                results.Add(IsNull(oRs("success").Value, 1))
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        'check to see we have mixed results
        If results.Contains("0") And results.Contains("1") Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function getnthWorkingDay(sn As String, month As Integer, year As Integer) As Date
        Dim startDate As Date = New Date(year, month, 1)

        Dim weekdays As ArrayList = New ArrayList
        weekdays.Add(DayOfWeek.Monday)
        weekdays.Add(DayOfWeek.Tuesday)
        weekdays.Add(DayOfWeek.Wednesday)
        weekdays.Add(DayOfWeek.Thursday)
        weekdays.Add(DayOfWeek.Friday)

        Dim done As Boolean
        Dim I As Integer

        If sn.ToLower = "last" Then
            ''//get todays date
            'Dim temp As Date = Date.Now

            ''//get month after next
            'temp = Date.Now.AddMonths(2)

            ''//get the first of that month and take away 1 to get the end of next month
            'Dim temp2 As Date = New Date(temp.Year, temp.Month, 1).AddDays(-1)
            '//add a month to the specified date
            Dim temp As Date = startDate.AddMonths(1)

            '//take away one day to get the end of the specified month
            temp = temp.AddDays(-1)

            Do
                If weekdays.Contains(temp.DayOfWeek) Then
                    startDate = temp
                    Exit Do
                Else
                    temp = temp.AddDays(-1) '//take away a day
                End If
            Loop
        Else
            Dim n As Integer = CInt(sn)

            If n < 1 Then n = 1

            Do
                Dim currentDate As DayOfWeek = startDate.DayOfWeek

                If weekdays.Contains(currentDate) Then
                    I += 1

                    If n = I Then Exit Do

                    startDate = startDate.AddDays(1)
                Else
                    startDate = startDate.AddDays(1)
                End If
            Loop
        End If



        Return startDate
    End Function
End Class
