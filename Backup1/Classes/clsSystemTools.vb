Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Security.Cryptography
Friend Class clsSystemTools
    Dim oData As New clsMarsData
    Dim ofd As New FolderBrowserDialog

    Public Shared Sub performDailyBackup()
        Try
            Dim backupPath As String = IO.Path.Combine(sAppDataPath, "Backups\")

            If IO.Directory.Exists(backupPath) = False Then IO.Directory.CreateDirectory(backupPath)

            For Each ff As String In IO.Directory.GetFiles(backupPath)
                Dim fi As IO.FileInfo = New FileInfo(ff)
                Dim datecreated As Date = fi.CreationTime

                If Date.Now.Subtract(datecreated).TotalDays > 10 Then
                    IO.File.Delete(ff) '//delete files older than 10 days 
                End If
            Next

            Dim supportFiles As frmSupportFiles = New frmSupportFiles

            Dim nCustID As String

            nCustID = clsMarsUI.MainUI.ReadRegistry("CustNo", 0)

            Dim backupFile As String = "SQL-RD_" & nCustID & "_" & Date.Now.ToString("yyyyMMdd") & ".zip"

            supportFiles._CreateSupportFiles(backupPath)
        Catch : End Try
    End Sub
    Public Shared Sub deleteFilesInfolder(ByVal path As String, ByVal int As Integer, ByVal unit As String, ByVal deleteParent As Boolean)
        Try
            Dim logFile As String = "housekeeping.debug"

            clsMarsDebug.writeToDebug(logFile, "Running house-keeping on " + path, False)

            For Each fil As String In IO.Directory.GetFiles(path)
                Dim filInfo As IO.FileInfo = New IO.FileInfo(fil)
                Dim dateCreated As Date = filInfo.CreationTime
                Dim doDelete As Boolean = False

                Select Case unit
                    Case "Seconds"
                        If Date.Now.Subtract(dateCreated).TotalSeconds > int Then doDelete = True
                    Case "Minutes"
                        If Date.Now.Subtract(dateCreated).TotalMinutes > int Then doDelete = True
                    Case "Hours"
                        If Date.Now.Subtract(dateCreated).TotalHours > int Then doDelete = True
                    Case "Days"
                        If Date.Now.Subtract(dateCreated).TotalDays > int Then doDelete = True
                    Case "Weeks"
                        If Date.Now.Subtract(dateCreated).TotalDays > (int * 7) Then doDelete = True
                    Case "Months"
                        If Date.Now.Subtract(dateCreated).TotalDays > (int * 31) Then doDelete = True
                End Select

                Try
                    If doDelete = True Then
                        clsMarsDebug.writeToDebug(logFile, "Deleting file " + fil, True)
                        IO.File.Delete(fil)
                    End If

                Catch : End Try
            Next

            For Each sdir As String In IO.Directory.GetDirectories(path)
                If deleteParent = False Then
                    deleteFilesInfolder(sdir, int, unit, deleteParent)
                Else
                    Dim dirInfo As IO.DirectoryInfo = New DirectoryInfo(sdir)
                    Dim dateLastMod As Date = dirInfo.LastWriteTime
                    Dim doDelete As Boolean = False

                    Select Case unit
                        Case "Minutes"
                            If Date.Now.Subtract(dateLastMod).TotalMinutes > int Then doDelete = True
                        Case "Hours"
                            If Date.Now.Subtract(dateLastMod).TotalHours > int Then doDelete = True
                        Case "Days"
                            If Date.Now.Subtract(dateLastMod).TotalDays > int Then doDelete = True
                        Case "Weeks"
                            If Date.Now.Subtract(dateLastMod).TotalDays > (int * 7) Then doDelete = True
                        Case "Months"
                            If Date.Now.Subtract(dateLastMod).TotalDays > (int * 31) Then doDelete = True
                    End Select

                    Try
                        If doDelete = True Then
                            clsMarsDebug.writeToDebug(logFile, "Deleting folder " + sdir, True)
                            dirInfo.Delete(True)
                        End If

                    Catch : End Try
                End If
            Next
        Catch : End Try
    End Sub
    Public Sub RunDatabaseDiagnostic()
        If RunEditor = False Then Return

        Dim response As DialogResult
        'check that the service exists
        clsMarsUI.BusyProgress(10, "Running diagnostics...")
        Dim svcs As ServiceProcess.ServiceController() = ServiceProcess.ServiceController.GetServices()
        Dim svcExists As Boolean = False

        For Each svc As ServiceProcess.ServiceController In svcs
            If String.Compare(svc.DisplayName, clsMigration.m_DatabaseService, True) = 0 Then
                svcExists = True
                Exit For
            End If
        Next

        clsMarsUI.BusyProgress(40, "Running diagnostics...")
        '//if the service doesnt exist then install it and migrate
        If svcExists = False Then
            response = MessageBox.Show("The SQL-RD database is missing. Would you like SQL-RD to install the service now?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If response = DialogResult.Yes Then
                Dim mig As clsMigration = New clsMigration

                mig.MigrateSystemDBtoLocal()

            Else
                End
            End If
        Else
            clsMarsUI.BusyProgress(80, "Running diagnostics...")
            '//the service exists then we should just restart it
            Dim svc As ServiceProcess.ServiceController = New ServiceProcess.ServiceController(clsMigration.m_DatabaseService)

            Try
                If svc.Status <> ServiceProcess.ServiceControllerStatus.Running Then
                    svc.Start()
                End If

                System.Threading.Thread.Sleep(10000)

            Catch exc As Exception
                MessageBox.Show("SQL-RD could not start the database service due to the following error: " & ControlChars.CrLf & exc.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End
            End Try
        End If

        clsMarsUI.BusyProgress(0, "Running diagnostics...", True)
    End Sub

    Public Shared Function CompareFiles(ByVal FileFullPath1 As String, _
    ByVal FileFullPath2 As String) As Boolean

        'returns true if two files passed to is are identical, false
        'otherwise

        'does byte comparison; works for both text and binary files

        'Throws exception on errors; you can change to just return 
        'false if you prefer


        Dim objMD5 As New MD5CryptoServiceProvider()
        Dim objEncoding As New System.Text.ASCIIEncoding()

        Dim aFile1() As Byte, aFile2() As Byte
        Dim strContents1, strContents2 As String
        Dim objReader As StreamReader
        Dim objFS As FileStream
        Dim bAns As Boolean
        If Not File.Exists(FileFullPath1) Then _
            Throw New Exception(FileFullPath1 & " doesn't exist")
        If Not File.Exists(FileFullPath2) Then _
         Throw New Exception(FileFullPath2 & " doesn't exist")

        Try

            objFS = New FileStream(FileFullPath1, FileMode.Open)
            objReader = New StreamReader(objFS)
            aFile1 = objEncoding.GetBytes(objReader.ReadToEnd)
            strContents1 = _
              objEncoding.GetString(objMD5.ComputeHash(aFile1))
            objReader.Close()
            objFS.Close()


            objFS = New FileStream(FileFullPath2, FileMode.Open)
            objReader = New StreamReader(objFS)
            aFile2 = objEncoding.GetBytes(objReader.ReadToEnd)
            strContents2 = _
             objEncoding.GetString(objMD5.ComputeHash(aFile2))

            bAns = strContents1 = strContents2
            objReader.Close()
            objFS.Close()
            aFile1 = Nothing
            aFile2 = Nothing

        Catch ex As Exception
            Return True
        End Try

        Return bAns
    End Function
    Public Enum configSaveTime As Integer
        onOpen = 0
        onClose = 1
    End Enum
    Public Shared Sub BackConfigFile(ByVal saveTime As configSaveTime)
        Try
            Dim backupDir As String = sAppPath & "configRestore\"

            If IO.Directory.Exists(backupDir) = False Then
                IO.Directory.CreateDirectory(backupDir)
            End If

            Dim backupName As String

            If saveTime = configSaveTime.onOpen Then
                backupName = "config_onopen.bak"
            Else
                backupName = "config_onclose.bak"
            End If

            Try : IO.File.Delete(backupDir & backupName) : Catch : End Try

            IO.File.Copy(sAppPath & "sqlrdlive.config", backupDir & backupName, True)
        Catch : End Try
    End Sub

    Public Shared Sub RestoreConfigFile(ByVal saveTime As configSaveTime)
        Try
            Dim backupDir As String = sAppPath & "configRestore\"

            If IO.Directory.Exists(backupDir) = False Then Return

            Dim backupName As String

            If saveTime = configSaveTime.onOpen Then
                backupName = "config_onopen.bak"
            Else
                backupName = "config_onclose.bak"
            End If

            If IO.File.Exists(backupDir & backupName) = False Then Return

            IO.File.Delete(sAppPath & "sqlrdlive.config")

            IO.File.Copy(backupDir & backupName, sAppPath & "sqlrdlive.config", True)

        Catch : End Try
    End Sub
    Public Sub _RefreshAll()
        Dim oReport As New clsMarsReport
        Dim SQL As String

        SQL = "SELECT ReportID FROM ReportAttr"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            oReport.RefreshReport(oRs.Fields(0).Value)
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub
    Public Sub _DeactivateSystem(ByVal ShowPrompt As Boolean)
        Dim oRes As DialogResult
        Dim oSvc As New clsServiceController

        If ShowPrompt = True Then
            Dim sMsg As String

            sMsg = "Deactivation means you will no longer be able to use SQL-RD on this machine. Are you sure you want to Deactivate SQL-RD?"
            oRes = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        Else
            oRes = DialogResult.Yes
        End If

        If oRes = DialogResult.Yes Then
            'stop the scheduler
            oSvc.StopScheduler()

            'start the wizard
            Dim oProc As New Process

            If IO.File.Exists(sAppPath & "regwizx.exe") = False Then
                With oProc
                    With .StartInfo
                        .FileName = sAppPath & "link.exe"
                        .Arguments = "unregwiz"
                    End With

                    .Start()
                End With
            Else
                With oProc
                    With .StartInfo
                        .FileName = sAppPath & "regwizx.exe"
                        .Arguments = "deactivate"
                    End With

                    .Start()
                End With
            End If

            For Each o As Process In Process.GetProcessesByName("sqlrd")
                o.Kill()
            Next

            End
        Else
            Return
        End If

    End Sub
    Public Sub _ActivateSystem(ByVal ShowPrompt As Boolean, ByVal ExitAfter As Boolean)
        Dim oRes As DialogResult
        Dim oSvc As New clsServiceController

        
        If ShowPrompt = True Then
            oRes = MessageBox.Show("SQL-RD will now close all sessions and the scheduling services in order to complete this process.", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        Else
            oRes = DialogResult.OK
        End If

        If oRes = DialogResult.OK Then
            'stop the scheduler
            oSvc.StopScheduler()

            'start the wizard
            Dim oProc As New Process

            If IO.File.Exists(sAppPath & "regwizx.exe") = False Then
                With oProc
                    With .StartInfo
                        .FileName = sAppPath & "link.exe"
                        .Arguments = "regwiz"
                    End With

                    .Start()
                End With
            Else
                With oProc
                    With .StartInfo
                        .FileName = sAppPath & "regwizx.exe"
                    End With

                    .Start()
                End With
            End If

            If ExitAfter = True Then
                For Each o As Process In Process.GetProcessesByName("sqlrd")
                    o.Kill()
                Next
            End If

            End
        Else
            Return
        End If

    End Sub
    Public Sub _BackupSystem(Optional ByVal sLoc As String = "", Optional ByVal ShowMsg As Boolean = True, Optional ByVal UseDefDirName As Boolean = True)
10:     Try
20:         If clsMarsSecurity._HasGroupAccess("Backup & Restore") = False Then
30:             Return
            End If

40:         Dim oParse As New clsMarsParser
41:         Dim oUI As New clsMarsUI

80:         If sLoc = "" And RunEditor = True Then
90:             With ofd
100:                .Description = "Please select the folder to backup to"
110:                .ShowNewFolderButton = True
120:                .ShowDialog()
                End With

130:            sLoc = ofd.SelectedPath
            End If

140:        If sLoc = "" Or sLoc Is Nothing Then Exit Sub

150:        If sLoc.Substring(sLoc.Length - 1, 1) <> "\" Then
160:            sLoc += "\"
            End If

            clsMarsUI.BusyProgress(10, "Creating backup directory...")

170:        sLoc = _CreateUNC(sLoc)

180:        oParse.ParseDirectory(sLoc)

190:        If sLoc.EndsWith("\") = False Then sLoc &= "\"
200:        If UseDefDirName = True Then

210:            sLoc &= "Backup" & Now.ToString("yyyyMMddHHmm") & "\"

            End If

            clsMarsUI.BusyProgress(30, "Backing up configuration...")

220:        System.IO.Directory.CreateDirectory(sLoc)
230:        System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sLoc & "sqlrdlive.dat", True)

240:        If IO.File.Exists(sAppPath & "sqlrdlive.config") Then
250:            System.IO.File.Copy(sAppPath & "sqlrdlive.config", sLoc & "sqlrdlive.config", True)
            End If

            clsMarsUI.BusyProgress(40, "Backing up error log...")

            '271:        If IO.File.Exists(sAppPath & "eventlog.dat") Then
            '272:            IO.File.Copy(sAppPath & "eventlog.dat", sLoc & "eventlog.dat", True)
            '273:        End If

            clsMarsUI.BusyProgress(60, "Backing up data files...")

            If gConType = "LOCAL" Then
                If IO.Directory.Exists(sAppPath & "DATA") Then
                    'create the data directory if it doesnt exist
                    Dim dm As New DataMigrator(DataMigrator.dbType.LOCALSQL)
                    Dim errInfo As Exception
274:                Dim ok As Boolean = dm.Export("", sAppPath & "sqlrdlive.def", errInfo)

                    If ok = False Then
                        Throw New Exception("Could not create def file during backup")
                        Return
                    End If

275:                System.IO.File.Copy(sAppPath & "sqlrdlive.def", sLoc & "sqlrdlive.def", True)
                End If
276:        ElseIf gConType = "ODBC" Then

                Dim srcDSN As String
                Dim conString As String
                Dim dsn As String
                Dim user As String
                Dim pwd As String

277:            conString = oUI.ReadRegistry("ConString", "", True)
                If conString.Length > 0 Then
                    dsn = conString.Split(";")(4).Split("=")(1)
                    user = conString.Split(";")(3).Split("=")(1)
                    pwd = conString.Split(";")(1).Split("=")(1)
                    srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
                End If

                Dim dm As New DataMigrator(DataMigrator.dbType.ODBC)
                Dim errInfo As Exception

278:            Dim ok As Boolean = dm.Export(srcDSN, sAppPath & "sqlrdlive.def", errInfo)

                If ok = False Then
                    Throw New Exception("Could not create def file during backup")
                End If

279:            System.IO.File.Copy(sAppPath & "sqlrdlive.def", sLoc & "sqlrdlive.def", True)
            End If

            'copy all the files to the new directory
            Dim sFile As String

280:        clsMarsUI.BusyProgress(90, "Backing up Cached Data...")


310:        If IO.Directory.Exists(clsMarsEvent.m_CachedDataPath) And clsMarsEvent.m_CachedDataPath.Length > 1 Then

320:            If System.IO.Directory.Exists(sLoc & "Cached Data") = False Then
330:                IO.Directory.CreateDirectory(sLoc & "Cached Data")
                End If

                'copy all the folder and subitems
340:            For Each sFolder As String In IO.Directory.GetDirectories(clsMarsEvent.m_CachedDataPath)
                    Dim sFolderName As String = sFolder.Split("\")(sFolder.Split("\").GetUpperBound(0))
                    Dim sNewFolder As String = sLoc & "Cached Data\" & sFolderName

350:                If IO.Directory.Exists(sNewFolder) = False Then
360:                    IO.Directory.CreateDirectory(sNewFolder)
                    End If

370:                For Each sFile In IO.Directory.GetFiles(sFolder)
380:                    IO.File.Copy(sFile, sNewFolder & "\" & ExtractFileName(sFile), True)
390:                Next
400:            Next
            End If

            '//copy the config restore files
            If IO.Directory.Exists(sAppPath & "configRestore") Then
                If IO.Directory.Exists(sLoc & "configRestore") = False Then
                    IO.Directory.CreateDirectory(sLoc & "configRestore")
                End If

                For Each s As String In IO.Directory.GetFiles(sAppPath & "configRestore")
                    IO.File.Copy(s, sLoc & "configRestore\" & IO.Path.GetFileName(s), True)
                Next
            End If

420:        oUI.ExportRegistry(sKey, sLoc & "sqlrd.reg")

430:        If ShowMsg = True Then MessageBox.Show("System backed up successfully", Application.ProductName, _
       MessageBoxButtons.OK, MessageBoxIcon.Information)
440:    Catch ex As Exception
450:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            clsMarsUI.BusyProgress(100, , True)
        End Try
    End Sub


    Public Sub _RestoreSystem()
        Dim sPath As String
        Dim sFile As String
        Dim Fol
        Dim X As Integer
        Dim I As Integer

        If clsMarsSecurity._HasGroupAccess("Backup & Restore") = False Then
            Return
        End If

        With ofd
            .Description = "Please select the folder containing the backup files"
            .ShowNewFolderButton = False
            .ShowDialog()
        End With

        sPath = ofd.SelectedPath

        If sPath.Length = 0 Then Exit Sub

        If sPath.EndsWith("\") = False Then sPath &= "\"

        If gConType = "DAT" Then
            If System.IO.File.Exists(sPath & "\sqlrdlive.dat") = False Then
                MessageBox.Show("Cannot find sqlrdlive.dat.This back up is invalid or corrupt.  Please try restoring from another backup", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Exclamation)
                Exit Sub
            End If
        ElseIf gConType = "LOCAL" Then
            If System.IO.File.Exists(sPath & "\sqlrdlive.def") = False And System.IO.File.Exists(sPath & "\sqlrdlive.dat") = False Then
                MessageBox.Show("Cannot find sqlrdlive.dat or sqlrdlive.def in the selected location. This back up is invalid or corrupt.  Please try restoring from another backup", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Exclamation)
                Exit Sub
            ElseIf System.IO.File.Exists(sPath & "\sqlrdlive.def") = False And System.IO.File.Exists(sPath & "\sqlrdlive.dat") = True Then '//restoring dat file to SQL
                Dim defFile As String = sPath & "\sqlrdlive.def"
                Dim exc As Exception = Nothing

                If DataMigrator.convertDatToDef(sPath & "\sqlrdlive.dat", sPath & "\sqlrdlive.def", exc) = False Then
                    MessageBox.Show("Failed to restore the backup due to a conversion error: " & vbCrLf & exc.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Exit Sub
                End If
            End If
        End If

        Try
            If MessageBox.Show("This will overwrite the existing system files and cached report files. Continue to restore?", _
            Application.ProductName, MessageBoxButtons.YesNo, _
             MessageBoxIcon.Question) = DialogResult.Yes Then
                AppStatus(True)

                clsMarsUI.BusyProgress(10, "Closing connection...")

                Dim oData As New clsMarsData

                oData.CloseMainDataConnection()

                clsMarsUI.BusyProgress(30, "Restoring data files...")

                If gConType = "DAT" Then
                    System.IO.File.Copy(sPath & "\sqlrdlive.dat", sAppPath & "\sqlrdlive.dat", True)
                ElseIf gConType = "LOCAL" Then
                    Dim dm As DataMigrator = New DataMigrator(DataMigrator.dbType.LOCALSQL)

                    Dim ok As Boolean = dm.Import(clsMigration.m_localConStringdotNET, sPath & "\sqlrdlive.def")

                    If ok = False Then
                        Throw New Exception("Error restoring the data from backup. System restore failed.")
                        Return
                    End If
                End If

                clsMarsUI.BusyProgress(50, "Restoring system configuration...")

                If IO.File.Exists(sPath & "\sqlrdlive.config") Then
                    System.IO.File.Copy(sPath & "sqlrdlive.config", sAppPath & "sqlrdlive.config", True)

                    '//resetting the constring
                    clsMarsUI.MainUI.SaveRegistry("ConString", "", False, , True)
                End If

                clsMarsUI.BusyProgress(90, "Restoring data caches...")

                If IO.Directory.Exists(sPath & "\Cached Data") Then
                    Dim sDatacache As String = clsMarsEvent.m_CachedDataPath

                    If IO.Directory.Exists(sDatacache) = True Then
                        IO.Directory.Delete(sDatacache, True)
                    End If

                    IO.Directory.CreateDirectory(sDatacache)

                    For Each s As String In IO.Directory.GetDirectories(sPath & "\Cached Data")
                        Dim sFolder As String = s.Split("\")(s.Split("\").GetUpperBound(0))

                        IO.Directory.CreateDirectory(sDatacache & sFolder)

                        For Each f As String In IO.Directory.GetFiles(s)
                            IO.File.Copy(f, sDatacache & sFolder & "\" & ExtractFileName(f))
                        Next
                    Next
                End If

                '//restore config backups
                If IO.Directory.Exists(sPath & "\configRestore") Then
                    If IO.Directory.Exists(sAppPath & "configRestore") = False Then
                        IO.Directory.CreateDirectory(sAppPath & "configRestore")
                    End If

                    For Each s As String In IO.Directory.GetFiles(sPath & "\configRestore")
                        IO.File.Copy(s, sAppPath & "configRestore\" & IO.Path.GetFileName(s), True)
                    Next
                End If

            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please make sure that the scheduler has been stopped")
            Return
        Finally
            clsMarsUI.BusyProgress(100, "", True)
        End Try

        'If IO.File.Exists(sPath & "\sqlrd6.reg") = True Then
        '    Process.Start(sPath & "\sqlrd6.reg")
        'End If

        'copy to main database
        If gConType = "ODBC" Then
            MessageBox.Show("SQL-RD does not transfer from the dat file to your ODBC/SQL database. You should restore your database server backup instead.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            'Dim oSQL As New frmSQLAdmin
            'Dim sConn As String
            'Dim oUI As New clsMarsUI

            'sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Application.StartupPath & "\sqlrdlive.dat" & ";Persist Security Info=False"

            'oData.OpenMainDataConnection()

            'sConn = oUI.ReadRegistry("ConString", "", True)

            'If clsMigration.MigrateSystem(clsMigration.m_datConString, "", "") = True Then
            '    MessageBox.Show("Restore completed. SQL-RD will now exit so that the changes can be applied", _
            '                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Process.Start(Application.ExecutablePath)

            End
            'End If
        Else

            MessageBox.Show("SQL-RD will now exit so that the restore will complete.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Process.Start(Application.ExecutablePath)

            End
        End If

    End Sub

    Public Sub _CompactSystem()

        If clsMarsSecurity._HasGroupAccess("Compact System") = False Then
            Return
        End If

        Dim oRes As DialogResult

        oRes = MessageBox.Show("To obtain optimum results, SQL-RD will shutdown the background scheduling applications. Proceed?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

        If oRes = DialogResult.No Then Exit Sub

        'get the service type
        Dim sType As String
        Dim oUI As New clsMarsUI
        Dim sProvider As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="

        sType = oUI.ReadRegistry("SQL-RDService", "NONE")

        If sType = "WindowsApp" Then
            For Each o As Process In Process.GetProcesses
                If o.ProcessName = "sqlrdapp" Then
                    o.Kill()
                    Exit For
                End If
            Next
        ElseIf sType = "WindowsNT" Then
            Try
                Dim oSrv As New ServiceController("SQL-RD")

                If oSrv.Status = ServiceControllerStatus.Running Then oSrv.Stop()
            Catch : End Try
        End If

        Try
            If gConType <> "DAT" Then
                'Dim oSQL As New frmSQLAdmin

                'If oSQL._ReturntoDat(False) = False Then
                '    Return
                'End If

                Return
            End If

            Dim oDB As New JRO.JetEngine
            Dim oData As New clsMarsData

            oUI.BusyProgress(10, "Closing data connection...")
            oData.CloseMainDataConnection()

            AppStatus(True)

            Dim nTime As Date
            nTime = Now.AddSeconds(60)

            Do
                Application.DoEvents()
            Loop Until IO.File.Exists(sAppPath & "\sqlrdlive.ldb") = False Or nTime < Now

            oUI.BusyProgress(25, "Validating file system...")

            If System.IO.File.Exists(sAppPath & "sqlrdlive.bak") Then
                System.IO.File.Delete(sAppPath & "sqlrdlive.bak")
            End If

            If System.IO.File.Exists(sAppPath & "temp.tmp") Then
                System.IO.File.Delete(sAppPath & "temp.tmp")
            End If

            If System.IO.File.Exists(sAppPath & "temp1.tmp") Then
                System.IO.File.Delete(sAppPath & "temp1.tmp")
            End If

            oUI.BusyProgress(50, "Creating backup of system...")
            'first create a copy of the current database
            System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sAppPath & "sqlrdlive.bak")

            'create a copy of marslive.mdb for compacting
            oUI.BusyProgress(60, "Creating working file...")
            System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sAppPath & "temp.tmp")

            'compact the file to temp1.mdb
            oUI.BusyProgress(80, "Compacting system database...")

            oDB.CompactDatabase(sProvider & sAppPath & "temp.tmp", sProvider & sAppPath & "temp1.tmp")

            'delete the older file
            oUI.BusyProgress(90, "Cleaning up...")
            System.IO.File.Delete(sAppPath & "temp.tmp")

            'delete the old marslive
            System.IO.File.Delete(sAppPath & "sqlrdlive.dat")

            'create a new marslive.mdb
            System.IO.File.Move(sAppPath & "temp1.tmp", sAppPath & "sqlrdlive.dat")

            oUI.BusyProgress(95, "Restoring data connection...")

            'oData._OpenMainDataConnection()

            oUI.BusyProgress(, , True)

            MessageBox.Show("System compacted and repaired successfully! SQL-RD will now exit so that the update can be applied", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            'restart services
            If sType = "WindowsApp" Then
                Try
                    Process.Start(sAppPath & "sqlrdapp.exe")
                Catch : End Try
            ElseIf sType = "WindowsNT" Then
                Try
                    Dim oSrv As New ServiceController("SQL-RD")

                    If oSrv.Status = ServiceControllerStatus.Stopped Then oSrv.Start()
                Catch : End Try
            End If

            Process.Start(Application.ExecutablePath)

            End
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please make sure that the background scheduler or the " & _
            "NT Service have been stopped. If this does not solve the problem then please restart your pc.")

            oData.OpenMainDataConnection()
        End Try
    End Sub
    Public Sub _CheckSysSize()
        Dim oUI As New clsMarsUI

        If gConType <> "DAT" Then Return

        If Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotAutoCompact", 0))) = True Then
            Return
        End If

        Dim oFil As New IO.FileInfo(sAppPath & "sqlrdlive.dat")

        Dim nSize As Double = oFil.Length()

        nSize /= 1000000

        If nSize > 50 Then
            If MessageBox.Show("The system database is now more than 50MB in size, would like to compact it now?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                gRole = "administrator"
                _CompactSystem()
            End If
        End If
    End Sub

    Public Sub _CheckSysIntegrity()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim SysValid As Boolean
        Dim Value

        If gConType <> "DAT" Then Return

        SQL = "SELECT TOP 1 Status FROM ScheduleAttr "

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                Value = oRs.Fields(0).Value
            End If

            oRs.Close()
            SysValid = True
        Catch ex As Exception
            SysValid = False
        End Try

        oRs = Nothing

        If SysValid = False Then
            If MessageBox.Show("The SQL-RD system files have become fragmented and this might be affecting system stability." & vbCrLf & _
            "Would you like to compact and defragment the system file now?", Application.ProductName, _
            MessageBoxButtons.YesNo) = DialogResult.Yes Then
                _CompactSystem()
            End If
        End If
    End Sub
    Public Sub _HouseKeeping(Optional ByVal DestinationID As Integer = 0, _
    Optional ByVal sPath As String = "", Optional ByVal sTitle As String = "")

        Dim sValue As Integer
        Dim sUnit As String
        Dim Interval As Long
        Dim intervalSpan As TimeSpan
        Dim I As Integer
        Dim oUI As New clsMarsUI
        Dim sFile As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim LastMod As Date
        Dim logFile As String = "housekeeping.debug"
        Dim calculateFileAgeUsing As String = clsMarsUI.MainUI.ReadRegistry("CalculateFileAgeUsing", "DateFileCreated")

        If Process.GetProcessesByName("csshousekeepinghelper").Length > 0 Then Return

        Dim dt As DataTable = New DataTable("housekeepingpaths")

        With dt.Columns
            .Add("path")
            .Add("unit")
            .Add("value")
            .Add("calculateageusing")
        End With

        Try
            If DestinationID = 0 Then
                clsMarsDebug.writeToDebug(logFile, "---------Begin general housekeeping----------", True)

                clsMarsDebug.writeToDebug(logFile, "Calculating file age using: " & calculateFileAgeUsing, True)

                SQL = "SELECT * FROM HouseKeepingPaths"

                oRs = clsMarsData.GetData(SQL)

                Do While oRs.EOF = False
                    sValue = oRs("keepfor").Value
                    sUnit = oRs("keepunit").Value
                    sPath = oRs("folderpath").Value

                    Dim r As DataRow = dt.Rows.Add

                    r("path") = sPath
                    r("unit") = sUnit
                    r("value") = sValue
                    r("calculateageusing") = calculateFileAgeUsing

                    oRs.MoveNext()
                Loop

                oRs.Close()

                '//add the system temp folder too

                clsMarsDebug.writeToDebug(logFile, "---------End general housekeeping----------", True)

            Else
                SQL = "SELECT * FROM HouseKeepingAttr WHERE DestinationID =" & DestinationID

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    sValue = oRs("agevalue").Value
                    sUnit = oRs("ageunit").Value

                    Dim r As DataRow = dt.Rows.Add

                    r("path") = sPath
                    r("unit") = sUnit
                    r("value") = sValue
                    r("calculateageusing") = calculateFileAgeUsing
                Else
                    oRs.Close()
                    Return
                End If
            End If

            '//manually add the process alive path
            Dim rp As DataRow = dt.Rows.Add
            rp("path") = Application.StartupPath & "\processAlive\"
            rp("unit") = "Days"
            rp("value") = 2
            rp("calculateageusing") = "DateFileCreated"

            '//manually add the system paths


            IO.File.Delete(Application.StartupPath & "\hkPath.xml")

            dt.WriteXml(Application.StartupPath & "\hkPath.xml", XmlWriteMode.WriteSchema)

            Try
                Dim proc As Process = New Process

                proc.StartInfo.FileName = Application.StartupPath & "\csshousekeepinghelper.exe"
                proc.Start()
                proc.PriorityClass = ProcessPriorityClass.BelowNormal
            Catch : End Try

            clsMarsDebug.writeToDebug(logFile, "Processing tracker files", True)

            'for process tracker files
            Try
                Dim alivePath As String = Application.StartupPath & "\processAlive\"

                If IO.Directory.Exists(alivePath) = True Then
                    For Each s As String In IO.Directory.GetFiles(alivePath)
                        Dim procID As String = IO.Path.GetFileName(s)

                        Try
                            Dim p As Process = Process.GetProcessById(procID)
                        Catch ex As Exception
                            Try
                                IO.File.Delete(s)
                            Catch : End Try
                        End Try
                    Next
                End If
            Catch : End Try

        Catch ex As Exception
            clsMarsDebug.writeToDebug(logFile, "Error in housekeeping: " & ex.Message, True)
        End Try

    End Sub
    Public Function _HouseKeeping_old(Optional ByVal DestinationID As Integer = 0, _
    Optional ByVal sPath As String = "", Optional ByVal sTitle As String = "")

        Dim sValue As Integer
        Dim sUnit As String
        Dim Interval As Long
        Dim intervalSpan As TimeSpan
        Dim I As Integer
        Dim oUI As New clsMarsUI
        Dim sFile As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim LastMod As Date
        Dim logFile As String = "housekeeping.debug"
        Dim calculateFileAgeUsing As String = clsMarsUI.MainUI.ReadRegistry("CalculateFileAgeUsing", "DateFileCreated")

        Try
            If DestinationID = 0 Then
                clsMarsDebug.writeToDebug(logFile, "---------Begin general housekeeping----------", True)

                clsMarsDebug.writeToDebug(logFile, "Calculating file age using: " & calculateFileAgeUsing, True)

                SQL = "SELECT * FROM HouseKeepingPaths"

                oRs = clsMarsData.GetData(SQL)

                Do While oRs.EOF = False
                    sValue = oRs("keepfor").Value
                    sUnit = oRs("keepunit").Value
                    sPath = oRs("folderpath").Value

                    Dim oParse As New clsMarsParser

                    sPath = oParse.ParseString(sPath)

                    clsMarsDebug.writeToDebug(logFile, "Processing path: " & sPath, True)
                    clsMarsDebug.writeToDebug(logFile, "Delete files and folders older than " & sValue & " " & sUnit, True)

                    For Each s As String In System.IO.Directory.GetFiles(sPath)
                        clsMarsDebug.writeToDebug(logFile, "File " & s, True)

                        If calculateFileAgeUsing = "DateFileCreated" Then
                            LastMod = System.IO.File.GetCreationTime(s)
                        Else
                            LastMod = System.IO.File.GetLastWriteTime(s)
                        End If

                        clsMarsDebug.writeToDebug(logFile, "Last modified/created " & LastMod, True)

                        intervalSpan = Date.Now.Subtract(LastMod)

                        Select Case sUnit
                            Case "Minutes"
                                Interval = intervalSpan.TotalMinutes
                            Case "Hours"
                                Interval = intervalSpan.TotalHours
                            Case "Days"
                                Interval = intervalSpan.TotalDays
                            Case "Weeks"
                                Interval = (intervalSpan.TotalDays) / 7
                            Case "Months"
                                Interval = (intervalSpan.TotalDays) / 28
                        End Select

                        clsMarsDebug.writeToDebug(logFile, "Interval span " & Interval, True)

                        If Interval > sValue Then
                            clsMarsDebug.writeToDebug(logFile, "Deleting file", True)

                            Try
                                System.IO.File.Delete(s)
                            Catch ex As Exception
                                clsMarsDebug.writeToDebug(logFile, "Error Deleting file: " & ex.Message, True)
                            End Try
                        End If
                    Next

                    clsMarsDebug.writeToDebug(logFile, "Processing folders", True)

                    For Each s As String In System.IO.Directory.GetDirectories(sPath)
                        clsMarsDebug.writeToDebug(logFile, "Folder " & s, True)

                        If calculateFileAgeUsing = "DateFileCreated" Then
                            LastMod = System.IO.File.GetCreationTime(s)
                        Else
                            LastMod = System.IO.File.GetLastWriteTime(s)
                        End If

                        clsMarsDebug.writeToDebug(logFile, "Last modified/created : " & LastMod, True)

                        intervalSpan = Date.Now.Subtract(LastMod)

                        Select Case sUnit
                            Case "Minutes"
                                Interval = intervalSpan.TotalMinutes
                            Case "Hours"
                                Interval = intervalSpan.TotalHours
                            Case "Days"
                                Interval = intervalSpan.TotalDays
                            Case "Weeks"
                                Interval = (intervalSpan.TotalDays) / 7
                            Case "Months"
                                Interval = (intervalSpan.TotalDays) / 28
                        End Select

                        clsMarsDebug.writeToDebug(logFile, "Interval span " & Interval, True)

                        If Interval > sValue Then
                            clsMarsDebug.writeToDebug(logFile, "Deleting folder " & s, True)

                            Try
                                System.IO.Directory.Delete(s, True)
                            Catch ex As Exception
                                clsMarsDebug.writeToDebug(logFile, "Error Deleting folder: " & ex.Message, True)
                            End Try
                        End If
                    Next

                    oRs.MoveNext()
                Loop

                oRs.Close()

            Else
                SQL = "SELECT * FROM HouseKeepingAttr WHERE DestinationID =" & DestinationID

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    sValue = oRs("agevalue").Value
                    sUnit = oRs("ageunit").Value
                Else
                    oRs.Close()
                    Return Nothing
                End If
            End If

            For Each s As String In System.IO.Directory.GetFiles(sPath)

                If calculateFileAgeUsing = "DateFileCreated" Then
                    LastMod = System.IO.File.GetCreationTime(s)
                Else
                    LastMod = System.IO.File.GetLastWriteTime(s)
                End If

                intervalSpan = Date.Now.Subtract(LastMod)

                Select Case sUnit
                    Case "Minutes"
                        Interval = intervalSpan.TotalMinutes
                    Case "Hours"
                        Interval = intervalSpan.TotalHours
                    Case "Days"
                        Interval = intervalSpan.TotalDays
                    Case "Weeks"
                        Interval = (intervalSpan.TotalDays) / 7
                    Case "Months"
                        Interval = (intervalSpan.TotalDays) / 28
                End Select

                If Interval > sValue Then
                    clsMarsDebug.writeToDebug(logFile, "Deleting file " & s, True)

                    Try
                        System.IO.File.Delete(s)
                    Catch ex As Exception
                        clsMarsDebug.writeToDebug(logFile, "Error Deleting file: " & ex.Message, True)
                    End Try
                End If
            Next

            For Each s As String In System.IO.Directory.GetDirectories(sPath)

                If calculateFileAgeUsing = "DateFileCreated" Then
                    LastMod = System.IO.File.GetCreationTime(s)
                Else
                    LastMod = System.IO.File.GetLastWriteTime(s)
                End If

                intervalSpan = Date.Now.Subtract(LastMod)

                Select Case sUnit
                    Case "Minutes"
                        Interval = intervalSpan.TotalMinutes
                    Case "Hours"
                        Interval = intervalSpan.TotalHours
                    Case "Days"
                        Interval = intervalSpan.TotalDays
                    Case "Weeks"
                        Interval = (intervalSpan.TotalDays) / 7
                    Case "Months"
                        Interval = (intervalSpan.TotalDays) / 28
                End Select

                If Interval > sValue Then
                    clsMarsDebug.writeToDebug(logFile, "Deleting folder " & s, True)

                    Try
                        System.IO.Directory.Delete(s, True)
                    Catch ex As Exception
                        clsMarsDebug.writeToDebug(logFile, "Error Deleting folder: " & s, True)
                    End Try
                End If
            Next

            'for process tracker files
            Try
                Dim alivePath As String = Application.StartupPath & "\processAlive\"

                If IO.Directory.Exists(alivePath) = True Then
                    For Each s As String In IO.Directory.GetFiles(alivePath)
                        Dim procID As String = IO.Path.GetFileName(s)

                        Try
                            Dim p As Process = Process.GetProcessById(procID)
                        Catch ex As Exception
                            Try
                                IO.File.Delete(s)
                            Catch : End Try
                        End Try
                    Next
                End If
            Catch : End Try

        Catch ex As Exception
            clsMarsDebug.writeToDebug(logFile, "Error in housekeeping: " & ex.Message, True)
        End Try

    End Function
    Public Function _CreateSysInfoFile(ByVal sPath As String) As Boolean
        Dim sOut As String
        Dim oFile As FileVersionInfo

        Try
            oFile = FileVersionInfo.GetVersionInfo(sAppPath & assemblyName)

            sOut = "Product Name: " & Application.ProductName & vbCrLf & _
            "Version: " & oFile.FileMajorPart & "." & oFile.FileMinorPart & "." & _
            oFile.FileBuildPart & "." & oFile.FilePrivatePart & vbCrLf & _
            "Build: " & oFile.Comments & vbCrLf & _
            "File Type: " & gConType

            SaveTextToFile(sOut, sPath, , False)

            Return True
        Catch
            Return False
        End Try
    End Function
    Public Function deleteContents(ByVal folderName As String) As Boolean
        For Each s As String In IO.Directory.GetFiles(folderName)
            IO.File.Delete(s)
        Next


        For Each s As String In IO.Directory.GetDirectories(folderName)
            deleteContents(s)


            IO.Directory.Delete(s)
        Next


    End Function

    Public Function IsConnectionAvailable() As Boolean
        
        Dim objUrl As New System.Uri("http://www.google.com/")
        ' Setup WebRequest
        Dim objWebReq As System.Net.WebRequest
        objWebReq = System.Net.WebRequest.Create(objUrl)
        Dim objResp As System.Net.WebResponse
        Try
            ' Attempt to get response and return True
            objResp = objWebReq.GetResponse
            objResp.Close()
            objWebReq = Nothing
            Return True
        Catch ex As Exception
            ' Error, exit and return False
            objResp.Close()
            objWebReq = Nothing
            Return False
        End Try
    End Function

    Public Sub checkforUpdates(ByVal checkUrl As String, Optional ByVal ManualCheck As Boolean = False)
        Try

            Dim check As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("CheckForUpdates", 1))

            If check = False And ManualCheck = False Then Return

            If IsConnectionAvailable() = False Then Return

            Dim wb As WebBrowser = New WebBrowser

            wb.Navigate(checkUrl)

            Dim timeNow As Date = Now

            Do
                Application.DoEvents()
            Loop Until Now.Subtract(timeNow).TotalSeconds > 5

            Dim build

            Try
                build = wb.Document.Body.InnerHtml.Trim
            Catch : End Try

            If build Is Nothing Then build = "19770227"

            For Each s As String In build
                If IsNumeric(s) = False Then
                    build = build.Replace(s, "")
                End If
            Next

            Dim current As String = GetCurrentBuild()
            Dim skipBuild As String = clsMarsUI.MainUI.ReadRegistry("SkipUpdateForBuild", "19770227")

            If build = skipBuild And ManualCheck = False Then Return

            If (ToDate(current) < ToDate(build)) Then

                Dim Msg As frmUpdateAvail = New frmUpdateAvail

                Dim oRes As DialogResult = Msg.DoshowDialog(build, current)

                If oRes = DialogResult.Yes Then

                    Dim proc As Process = New Process
                    Dim dlink As String = "http://download.christiansteven.com/sql-rd/SQL-RDSetup.exe"

                    If IO.File.Exists(sAppPath & "regwizx.exe") Then
                        dlink = "http://download.christiansteven.com/sql-rd/SQL-RDSetupX.exe"
                    End If

                    With proc
                        .StartInfo.FileName = "iexplore"
                        .StartInfo.Arguments = dlink
                        .Start()
                    End With
                ElseIf oRes = DialogResult.Ignore Then
                    clsMarsUI.MainUI.SaveRegistry("SkipUpdateForBuild", build, , , True)
                End If
            Else
                If ManualCheck Then
                    MessageBox.Show("No updates are available at this time.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            If ManualCheck Then _ErrorHandle("There was an error checking for new updates. The error was: " & ex.Message, Err.Number, _
            Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please make sure you have an active internet connection", True)
        End Try
    End Sub

    Private Function GetCurrentBuild() As String
        Dim sBuild As String
        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        sBuild = oFile.Comments

        sBuild = sBuild.Split("/")(0).ToLower.Replace("build", String.Empty).Trim

        Return sBuild
    End Function
    Private Function ToDate(ByVal sIn As String) As Date
        Dim nYear As Integer
        Dim nMonth As Integer
        Dim nDay As Integer

        nYear = sIn.Substring(0, 4)
        nMonth = sIn.Substring(4, 2)
        nDay = sIn.Substring(6, 2)

        Dim nDate As Date = New Date(nYear, nMonth, nDay)

        Return nDate
    End Function
End Class
