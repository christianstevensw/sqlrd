﻿Imports System.Net

Namespace rsClients

    Friend Class rsClient
        Inherits ReportServer.ReportingService

        Public m_needLogon As Boolean = False
        Private m_authCookieName As String
        Private m_authCookie As System.Net.Cookie

        Sub New(ByVal url As String)
            ' Set the server URL
            MyBase.Url = url
            '// Set default credentials to integrated.
            Credentials = System.Net.CredentialCache.DefaultCredentials
        End Sub

        '/// <summary>
        '/// Gets the type of the item on the report server. Use the 
        '/// new modifier to hide the base implementation.
        '/// </summary>
        Public Shadows Function GetItemType(ByVal item As String) As ReportServer.ItemTypeEnum

            Dim type As ReportServer.ItemTypeEnum = ReportServer.ItemTypeEnum.Unknown

            Try
                type = MyBase.GetItemType(item)
            Catch sex As System.Exception

                Return ReportServer.ItemTypeEnum.Unknown
            End Try

            Return type
        End Function

        '/// <summary>
        '/// Get whether the given credentials can connect to the report server.
        '/// Returns false if not authorized. Other errors throw an exception.
        '/// </summary>
        Public Function CheckAuthorized() As Boolean

            Try

                MyBase.GetItemType("/")

            Catch e As System.Net.WebException

                If Not (TypeOf e.Response Is System.Net.HttpWebResponse) OrElse CType(e.Response, System.Net.HttpWebResponse).StatusCode <> Net.HttpStatusCode.Unauthorized Then
                    Throw e
                End If

                Return False

            Catch e As System.InvalidOperationException

                ' // This condition could be caused by a redirect to a forms logon page
                Console.WriteLine("InvalidOperationException")

                If m_needLogon Then

                    Dim creds As System.Net.NetworkCredential = Me.Credentials


                    If (creds IsNot Nothing AndAlso creds.UserName IsNot Nothing) Then

                        Try
                            MyBase.CookieContainer = New Net.CookieContainer()
                            MyBase.LogonUser(creds.UserName, creds.Password, Nothing)
                            Return True
                        Catch ex As Exception
                            Return False
                        End Try
                    Else
                        Throw New Exception("this is crap")
                    End If
                End If
            End Try
            Return True
        End Function
        '/// <summary>
        '/// Enables users to enter credentials from the command prompt.
        '/// </summary>
        Public Function GetCredentials(ByVal username As String, ByVal password As String) As System.Net.NetworkCredential
            Return New Net.NetworkCredential(username, password)
        End Function

        Protected Overrides Function GetWebRequest(ByVal Uri As Uri) As Net.WebRequest
            Dim request As HttpWebRequest

            request = CType(HttpWebRequest.Create(Uri), HttpWebRequest)
            request.Credentials = Me.Credentials
            request.CookieContainer = New CookieContainer()

            If (m_authCookie IsNot Nothing) Then
                request.CookieContainer.Add(m_authCookie)
            End If

            Return request
        End Function

        Protected Overrides Function GetWebResponse(ByVal request As WebRequest) As WebResponse

            Dim response As WebResponse = MyBase.GetWebResponse(request)
            Dim cookieName As String = response.Headers("RSAuthenticationHeader")

            If (cookieName IsNot Nothing) Then

                m_authCookieName = cookieName
                Dim webResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Dim authCookie As Cookie = webResponse.Cookies(cookieName)

                '// save it away 
                m_authCookie = authCookie
            End If

            ' // need to call logon
            If (response.Headers("RSNotAuthenticated") IsNot Nothing) Then

                m_needLogon = True
            End If

            Return response
        End Function
    End Class

    Friend Class rsClient2008
        Inherits ReportServer_2005.ReportingService2005

        Public m_needLogon As Boolean = False
        Private m_authCookieName As String
        Private m_authCookie As System.Net.Cookie

        Sub New(ByVal url As String)
            ' Set the server URL
            MyBase.Url = url
            '// Set default credentials to integrated.
            Credentials = System.Net.CredentialCache.DefaultCredentials
        End Sub

        '/// <summary>
        '/// Gets the type of the item on the report server. Use the 
        '/// new modifier to hide the base implementation.
        '/// </summary>
        Public Shadows Function GetItemType(ByVal item As String) As ReportServer.ItemTypeEnum

            Dim type As ReportServer.ItemTypeEnum = ReportServer.ItemTypeEnum.Unknown

            Try
                type = MyBase.GetItemType(item)
            Catch sex As System.Exception

                Return ReportServer.ItemTypeEnum.Unknown
            End Try

            Return type
        End Function

        '/// <summary>
        '/// Get whether the given credentials can connect to the report server.
        '/// Returns false if not authorized. Other errors throw an exception.
        '/// </summary>
        Public Function CheckAuthorized() As Boolean

            Try

                MyBase.GetItemType("/")

            Catch e As System.Net.WebException

                If Not (TypeOf e.Response Is System.Net.HttpWebResponse) OrElse CType(e.Response, System.Net.HttpWebResponse).StatusCode <> Net.HttpStatusCode.Unauthorized Then
                    Throw e
                End If

                Return False

            Catch e As System.InvalidOperationException

                ' // This condition could be caused by a redirect to a forms logon page
                Console.WriteLine("InvalidOperationException")

                If m_needLogon Then

                    Dim creds As System.Net.NetworkCredential = Me.Credentials


                    If (creds IsNot Nothing AndAlso creds.UserName IsNot Nothing) Then

                        Try
                            MyBase.CookieContainer = New Net.CookieContainer()
                            MyBase.LogonUser(creds.UserName, creds.Password, Nothing)
                            Return True
                        Catch ex As Exception
                            Return False
                        End Try
                    Else
                        Throw New Exception("this is crap")
                    End If
                End If
            End Try
            Return True
        End Function
        '/// <summary>
        '/// Enables users to enter credentials from the command prompt.
        '/// </summary>
        Public Function GetCredentials(ByVal username As String, ByVal password As String) As System.Net.NetworkCredential
            Return New Net.NetworkCredential(username, password)
        End Function

        Protected Overrides Function GetWebRequest(ByVal Uri As Uri) As Net.WebRequest
            Dim request As HttpWebRequest

            request = CType(HttpWebRequest.Create(Uri), HttpWebRequest)
            request.Credentials = Me.Credentials
            request.CookieContainer = New CookieContainer()

            If (m_authCookie IsNot Nothing) Then
                request.CookieContainer.Add(m_authCookie)
            End If

            Return request
        End Function

        Protected Overrides Function GetWebResponse(ByVal request As WebRequest) As WebResponse

            Dim response As WebResponse = MyBase.GetWebResponse(request)
            Dim cookieName As String = response.Headers("RSAuthenticationHeader")

            If (cookieName IsNot Nothing) Then

                m_authCookieName = cookieName
                Dim webResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Dim authCookie As Cookie = webResponse.Cookies(cookieName)

                '// save it away 
                m_authCookie = authCookie
            End If

            ' // need to call logon
            If (response.Headers("RSNotAuthenticated") IsNot Nothing) Then

                m_needLogon = True
            End If

            Return response
        End Function
    End Class

    Friend Class rsClient2010
        Inherits ReportServer_2010.ReportingService2010

        Public m_needLogon As Boolean = False
        Private m_authCookieName As String
        Private m_authCookie As System.Net.Cookie

        Sub New(ByVal url As String)
            ' Set the server URL
            MyBase.Url = url
            '// Set default credentials to integrated.
            Credentials = System.Net.CredentialCache.DefaultCredentials
        End Sub

        '/// <summary>
        '/// Gets the type of the item on the report server. Use the 
        '/// new modifier to hide the base implementation.
        '/// </summary>
        Public Shadows Function GetItemType(ByVal item As String) As ReportServer.ItemTypeEnum

            Dim type As ReportServer.ItemTypeEnum = ReportServer.ItemTypeEnum.Unknown

            Try
                type = MyBase.GetItemType(item)
            Catch sex As System.Exception

                Return ReportServer.ItemTypeEnum.Unknown
            End Try

            Return type
        End Function

        '/// <summary>
        '/// Get whether the given credentials can connect to the report server.
        '/// Returns false if not authorized. Other errors throw an exception.
        '/// </summary>
        Public Function CheckAuthorized() As Boolean

            Try

                MyBase.GetItemType("/")

            Catch e As System.Net.WebException

                If Not (TypeOf e.Response Is System.Net.HttpWebResponse) OrElse CType(e.Response, System.Net.HttpWebResponse).StatusCode <> Net.HttpStatusCode.Unauthorized Then
                    Throw e
                End If

                Return False

            Catch e As System.InvalidOperationException

                ' // This condition could be caused by a redirect to a forms logon page
                Console.WriteLine("InvalidOperationException")

                If m_needLogon Then

                    Dim creds As System.Net.NetworkCredential = Me.Credentials


                    If (creds IsNot Nothing AndAlso creds.UserName IsNot Nothing) Then

                        Try
                            MyBase.CookieContainer = New Net.CookieContainer()
                            MyBase.LogonUser(creds.UserName, creds.Password, Nothing)
                            Return True
                        Catch ex As Exception
                            Return False
                        End Try
                    Else
                        Throw New Exception("this is crap")
                    End If
                End If
            End Try
            Return True
        End Function
        '/// <summary>
        '/// Enables users to enter credentials from the command prompt.
        '/// </summary>
        Public Function GetCredentials(ByVal username As String, ByVal password As String) As System.Net.NetworkCredential
            Return New Net.NetworkCredential(username, password)
        End Function

        Protected Overrides Function GetWebRequest(ByVal Uri As Uri) As Net.WebRequest
            Dim request As HttpWebRequest

            request = CType(HttpWebRequest.Create(Uri), HttpWebRequest)
            request.Credentials = Me.Credentials
            request.CookieContainer = New CookieContainer()

            If (m_authCookie IsNot Nothing) Then
                request.CookieContainer.Add(m_authCookie)
            End If

            Return request
        End Function

        Protected Overrides Function GetWebResponse(ByVal request As WebRequest) As WebResponse

            Dim response As WebResponse = MyBase.GetWebResponse(request)
            Dim cookieName As String = response.Headers("RSAuthenticationHeader")

            If (cookieName IsNot Nothing) Then

                m_authCookieName = cookieName
                Dim webResponse As HttpWebResponse = CType(response, HttpWebResponse)
                Dim authCookie As Cookie = webResponse.Cookies(cookieName)

                '// save it away 
                m_authCookie = authCookie
            End If

            ' // need to call logon
            If (response.Headers("RSNotAuthenticated") IsNot Nothing) Then

                m_needLogon = True
            End If

            Return response
        End Function
    End Class
End Namespace
