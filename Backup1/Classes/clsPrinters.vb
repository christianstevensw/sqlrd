Imports Microsoft.Win32

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
imports My.crystal85.CRAXDRT.CRPaperOrientation
imports My.Crystal85.CRAXDRT.CRPaperSize 
imports My.Crystal85.CRAXDRT.CRPaperSource 
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9
imports My.crystal9.CRAXDRT.CRPaperOrientation
imports My.Crystal9.CRAXDRT.CRPaperSize 
imports My.Crystal9.CRAXDRT.CRPaperSource 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
imports My.crystal10.CRAXDRT.CRPaperOrientation
imports My.Crystal10.CRAXDRT.CRPaperSize
imports My.Crystal10.CRAXDRT.CRPaperSource 
#ElseIf CRYSTAL_VER = 11 Or CRYSTAL_VER = 11.5 Or CRYSTAL_VER = 12 Then
Imports My.Crystal11
Imports My.crystal11.CRAXDRT.CRPaperOrientation
Imports My.Crystal11.CRAXDRT.CRPaperSize
Imports My.Crystal11.CRAXDRT.CRPaperSource
#End If

'Imports CRAXDRT.CRPaperOrientation
'Imports CRAXDRT.CRPaperSize

Friend Class clsPrinters
    Dim oUI As clsMarsUI = New clsMarsUI

    Private Structure DOCINFO
        Dim pDocName As String
        Dim pOutputFile As String
        Dim pDatatype As String
    End Structure

    Private Declare Function ClosePrinter Lib "winspool.drv" (ByVal _
            hPrinter As Long) As Long
    Private Declare Function EndDocPrinter Lib "winspool.drv" (ByVal _
            hPrinter As Long) As Long
    Private Declare Function EndPagePrinter Lib "winspool.drv" (ByVal _
            hPrinter As Long) As Long
    Private Declare Function OpenPrinter Lib "winspool.drv" Alias _
            "OpenPrinterA" (ByVal pPrinterName As String, ByVal phPrinter As Long, _
            ByVal pDefault As Long) As Long
    Private Declare Function StartDocPrinter Lib "winspool.drv" Alias _
            "StartDocPrinterA" (ByVal hPrinter As Long, ByVal Level As Long, _
    ByVal pDocInfo As DOCINFO) As Long
    Private Declare Function StartPagePrinter Lib "winspool.drv" (ByVal _
            hPrinter As Long) As Long
    Private Declare Function WritePrinter Lib "winspool.drv" (ByVal _
            hPrinter As Long, ByVal pBuf As Object, ByVal cdBuf As Long, _
    ByVal pcWritten As Long) As Long

    Public Sub SetDefaultPrinter(ByVal sPrinterName As String)
        Dim moReturn As Management.ManagementObjectCollection
        Dim moSearch As Management.ManagementObjectSearcher
        Dim mo As Management.ManagementObject

        sPrinterName = sPrinterName.Replace("\", "\\")

        moSearch = New Management.ManagementObjectSearcher("Select * from Win32_Printer WHERE Name = '" & sPrinterName & "'")

        moReturn = moSearch.Get

        For Each mo In moReturn
            Dim sName As String = mo("Name")
            Dim sDriver As String = mo("DriverName")
            Dim sPort As String = mo("PortName")

            Dim sFinal As String = sName & "," & sDriver & "," & sPort

            oUI.SaveRegistry(Registry.CurrentUser, "Software\Microsoft\Windows NT\CurrentVersion\Windows", "Device", sFinal)
        Next

        _Delay(5)
    End Sub

    Public Function GetDefaultPrinter() As String
        Dim sPrinter As String

        sPrinter = oUI.ReadRegistry(Registry.CurrentUser, "Software\Microsoft\Windows NT\CurrentVersion\Windows", "Device", "")

        Return sPrinter
    End Function

    Public Function GetPrinterDetails(ByVal sPrinterName As String) As String
        Dim moReturn As Management.ManagementObjectCollection
        Dim moSearch As Management.ManagementObjectSearcher
        Dim mo As Management.ManagementObject
        Dim sFinal As String
        Dim sPrinterKey As String = "SYSTEM\ControlSet001\Control\Print\Printers\" & sPrinterName
        Dim sName As String = ""
        Dim sDriver As String
        Dim sPort As String

        sPrinterName = sPrinterName.Replace("\", "\\")

        Try
            moSearch = New Management.ManagementObjectSearcher("Select * from Win32_Printer WHERE Name = '" & sPrinterName & "'")

            moReturn = moSearch.Get

            For Each mo In moReturn
                sName = mo("Name")
                sDriver = mo("DriverName")
                sPort = mo("PortName")
            Next

            If sName.Length = 0 Then
                sName = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Name", "")
                sDriver = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Printer Driver", "")
                sPort = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Port", "")

                If sName = "" Then sName = sPrinterName
            End If

        Catch
            sName = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Name", "")
            sDriver = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Printer Driver", "")
            sPort = oUI.ReadRegistry(Registry.LocalMachine, sPrinterKey, "Port", "")

            If sName = "" Then sName = sPrinterName
        End Try

        sFinal = sName & "," & sDriver & "," & sPort

        Return sFinal
    End Function




    Public Function _PrintReportFile(ByVal sFileName As String, _
    ByVal nPrinterID As Integer) As Boolean
        Dim Temp As String
        Dim I As Integer
        Dim WinHandle As Long
        Dim intClose As Integer
        Dim PrintPage As Long
        Dim pWait As Long
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim nCopies As Integer
        Dim oProc As Process = New Process
        Dim PrinterName As String

        Try

            'get the current user default printer
            Temp = GetDefaultPrinter()

            SQL = "SELECT * FROM PrinterAttr WHERE PrinterID = " & nPrinterID

            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                nCopies = oRs("copies").Value
                PrinterName = oRs("printername").Value
                PrintPage = oRs("pagesperreport").Value
            End If

            'set the set printer to default
            SetDefaultPrinter(PrinterName)

            _Delay(5)

            If InitPrinter(GetDelimitedWord(PrinterName, 1, ",")) = False Then
                Err.Raise(-2147072600, , "Could initialise the selected printer (" & PrinterName & ")")
                Exit Function
            End If

            For I = 1 To nCopies
                oProc.StartInfo.FileName = sFileName
                oProc.StartInfo.Verb = "print"
                oProc.StartInfo.CreateNoWindow = True
                oProc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                oProc.Start()
            Next

            pWait = (10 * PrintPage) / 12

            If pWait < 10 Then pWait = 10

            _Delay((pWait * nCopies) + 10)

            'reset the printer
            SetDefaultPrinter(Temp.Split(",")(0))

            For Each oProc In Process.GetProcesses
                If oProc.ProcessName.ToLower = "acrord32" Then
                    oProc.CloseMainWindow()
                End If
            Next

            For Each oProc In Process.GetProcesses
                If oProc.ProcessName.ToLower = "acrord32" Then
                    oProc.Kill()
                End If
            Next

            Return True
            Exit Function
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return False
        End Try
    End Function

    Public Function InitPrinter(ByVal sPrinterName As String) As Boolean
        On Error GoTo Trap
        Dim lhPrinter As Long
        Dim lReturn As Long
        Dim lpcWritten As Long
        Dim lDoc As Long
        Dim sWrittenData As String
        Dim MyDocInfo As DOCINFO

        lReturn = OpenPrinter(sPrinterName, lhPrinter, 0)

        If lReturn = 0 Then
            InitPrinter = False
            Exit Function
        End If

        MyDocInfo.pDocName = "Printer Spooler..."
        MyDocInfo.pOutputFile = vbNullString
        MyDocInfo.pDatatype = vbNullString

        'MyDocInfo.pDatatype = vbInteger

        lDoc = StartDocPrinter(lhPrinter, 1, MyDocInfo)
        '   Call StartPagePrinter(lhPrinter)
        'sWrittenData = "How's that for Magic !!!!" & vbFormFeed
        sWrittenData = " "
        'lReturn = WritePrinter(lhPrinter, ByVal sWrittenData, _
        'Len(sWrittenData), lpcWritten)
        lReturn = WritePrinter(lhPrinter, sWrittenData, Len(sWrittenData), 0)
        'lReturn = EndPagePrinter(lhPrinter)
        'lReturn = EndDocPrinter(lhPrinter)
        lReturn = ClosePrinter(lhPrinter)
        'Text1.Text = ""
        'Debug.Print lReturn & "Es"
        InitPrinter = True

        Exit Function
Trap:
        InitPrinter = False
    End Function

    '//EMF Printing

    Dim emfImages As ArrayList
    Dim printingPage As Integer = 0

    Public Function printEMFFile(ByVal files As ArrayList, ByVal outputFilename As String, _
                                ByVal useDriverDefaults As Boolean, _
                                ByVal timeperpage As Integer, _
                                Optional ByVal Format As String = "Formatted", _
                                Optional ByVal characterSet As Integer = 0, _
                                Optional ByVal eolStyle As Integer = 0, _
                                Optional ByVal whitespacesize As Integer = 100, _
                                Optional ByVal insertBreaks As Boolean = False)

        '//set up the miraplacid driver
        Dim p As Object = CreateObject("Miraplacid.TextDriver")

        clsMarsDebug.writeToDebug("text_export.debug", "Miraplacid ActiveX Object created successfully", True)


        p.AutoSave = True
        p.TransportParam("File", "path") = clsMarsReport.m_OutputFolder
        p.TransportParam("File", "filename") = outputFilename
        p.TransportParam("File", "append") = 0

        clsMarsDebug.writeToDebug("text_export.debug", "Print file set to " & outputFilename, True)

        If useDriverDefaults = False Then
            clsMarsDebug.writeToDebug("text_export.debug", "Setting Miraplacid driver settings", True)

            p.Format = Format
            p.Charset = characterSet
            p.UnixEOL = eolStyle
            p.PageBreak = Convert.ToInt32(insertBreaks)

            If Format = "Formatted" Then
                p.FormatParam("Formatted", "space") = whitespacesize
            End If
        End If


        '//load the emf files
        emfImages = New ArrayList

        For Each f As String In files
            Dim emf As System.Drawing.Imaging.Metafile = System.Drawing.Imaging.Metafile.FromFile(f)
            emfImages.Add(emf)
        Next

        '//the effing emf files to text
        Print(clsMarsReport.m_OutputFolder & outputFilename, timeperpage)

        '//reset the printer
        p.AutoSave = False

        p = Nothing
    End Function

    Private Sub Print(ByVal output As String, ByVal timeperpage As Integer)
        If emfImages Is Nothing Or emfImages.Count <= 0 Then
            Throw New ArgumentException("An image is required to print.")
        End If

        Dim printer As String = "Miraplacid Text Driver"

        If (String.IsNullOrEmpty(printer)) Then
            Throw New ArgumentException("A printer is required.")
        End If

        clsMarsDebug.writeToDebug("text_export.debug", "Begin printing...", True)

        Dim printingPage As Integer = 0
        Dim pd As System.Drawing.Printing.PrintDocument = New System.Drawing.Printing.PrintDocument

        pd.PrinterSettings.PrinterName = printer
        AddHandler pd.PrintPage, AddressOf pd_PrintPage

        pd.Print()

        Dim timeout As Integer = 10
        Dim time As Integer = 0

        If emfImages.Count < 10 Then
            timeout = 10 * timeperpage
        Else
            timeout = emfImages.Count * timeperpage
        End If

        clsMarsDebug.writeToDebug("text_export.debug", "Waiting " & timeout & " seconds for printing to complete", True)

        Do
            System.Threading.Thread.Sleep(1000)

            time += 1
        Loop Until IO.File.Exists(output) = True Or time >= timeout

        clsMarsDebug.writeToDebug("text_export.debug", "Finished waiting...", True)

        If IO.File.Exists(output) = False Then Throw New Exception("Miraplacid failed to print the EMF file(s). Please check the Miraplacid logs")

    End Sub

    Private Sub pd_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)

        clsMarsDebug.writeToDebug("text_export.debug", "Printing page " & printingPage, True)

        Dim page As System.Drawing.Imaging.Metafile = emfImages(printingPage)
        Dim h, w As Integer

        h = page.Height
        w = page.Width

        If h > w Then e.PageSettings.Landscape = True

        e.Graphics.DrawImage(page, 0, 0, w, h)

        printingPage += 1

        If printingPage < emfImages.Count Then
            e.HasMorePages = True
        Else
            e.HasMorePages = False
        End If
    End Sub
End Class
