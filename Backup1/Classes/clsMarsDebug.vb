Friend Class clsMarsDebug
    Shared ReadOnly Property m_logsDir()
        Get
            If IO.Directory.Exists(sAppPath & "Logs\") = False Then
                IO.Directory.CreateDirectory(sAppPath & "Logs\")
            End If

            Return sAppPath & "Logs\"
        End Get
    End Property


    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fileName">the name of the file excluding directory</param>
    ''' <param name="message">the message to put into the debug - date will be appended to this message</param>
    ''' <param name="append">start creating a new debug file or append?</param>
    ''' <remarks></remarks>
    ''' 
    <DebuggerStepThrough()> _
    Public Shared Sub writeToDebug(ByVal fileName As String, ByVal message As String, Optional ByVal append As Boolean = True)


        If fileName = "" Then Return

        For Each s As String In IO.Path.GetInvalidFileNameChars
            fileName = fileName.Replace(s, "")
        Next

        Dim path As String = m_logsDir & fileName

        Try
            If IO.Path.GetExtension(path) = "" Then
                path = path & ".debug"
            End If
        Catch : End Try

        Dim fullMsg As String = Now & ": " & message

        SaveTextToFile(fullMsg, path, , append, True)

        manageLogsDirect()
    End Sub

    Dim m_filename As String
    Dim m_message As String
    Dim m_append As Boolean

    Private Sub writeToDebugAsync()
        If m_filename = "" Then Return

        For Each s As String In IO.Path.GetInvalidFileNameChars
            m_filename = m_filename.Replace(s, "")
        Next

        Dim path As String = m_logsDir & m_filename

        Try
            If IO.Path.GetExtension(path) = "" Then
                path = path & ".debug"
            End If
        Catch : End Try

        Dim fullMsg As String = Now & ": " & m_message

        SaveTextToFile(fullMsg, path, , m_append, True)
    End Sub

    Public Shared Sub manageLogs()
        Dim oT As Threading.Thread = New Threading.Thread(AddressOf manageLogsDirect)

        oT.Start()
    End Sub

    <DebuggerStepThrough()> _
    Public Shared Sub manageLogsDirect()
        Try
            For Each file As String In IO.Directory.GetFiles(m_logsDir)
                Dim fileInfo As IO.FileInfo = New IO.FileInfo(file)
                Dim dateCreated As Date = fileInfo.CreationTime
                Dim dateModified As Date = fileInfo.LastWriteTime

                If fileInfo.Length > 5000000 Then
                    Try
                        fileInfo.Delete()
                    Catch : End Try
                ElseIf Date.Now.Subtract(dateCreated).TotalDays >= 3 Or Date.Now.Subtract(dateModified).TotalDays >= 3 Then
                    Try
                        fileInfo.Delete()
                    Catch : End Try
                End If
            Next
        Catch : End Try
    End Sub
End Class
