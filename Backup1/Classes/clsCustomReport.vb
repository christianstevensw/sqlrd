Imports System.Data
Imports System.Data.Odbc

Friend Class clsCustomReport
    Public Function buildWorkbook(ByVal taskID As Integer) As Boolean
        Dim SQL As String = "SELECT * FROM tasks WHERE taskID = " & taskID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim parser As clsMarsParser = New clsMarsParser

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Dim outputFile As String = parser.ParseString(oRs("filelist").Value)
            Dim replaceFile As Boolean = False
            Dim dir As String = IO.Path.GetDirectoryName(outputFile)
            Dim file As String = IO.Path.GetFileName(outputFile)

            For Each s As String In IO.Path.GetInvalidPathChars
                dir = dir.Replace(s, "")
            Next

            parser.ParseDirectory(dir)

            For Each s As String In IO.Path.GetInvalidFileNameChars
                file = file.Replace(s, "")
            Next

            outputFile = dir & "\" & file

            Try
                replaceFile = IsNull(oRs("replacefiles").Value, 0)
            Catch : End Try

            If IO.File.Exists(outputFile) And replaceFile = True Then
                IO.File.Delete(outputFile)
            End If

            oRs.Close()



            oRs = clsMarsData.GetData("SELECT * FROM datasetattr WHERE taskid = " & taskID & " ORDER BY ordernumber ASC")

            If oRs IsNot Nothing Then
                Dim ds As DataSet = New DataSet
                Dim errInfo As Exception

                Do While oRs.EOF = False
                    '//datasetid,taskid,datasetname,constring,sqltext,ordernumber
                    Dim workSheetName As String = parser.ParseString(oRs("datasetname").Value)
                    Dim con As String = parser.ParseString(oRs("constring").Value)
                    Dim cmdText As String = parser.ParseString(oRs("sqltext").Value)

                    Dim dsn As String = con.Split("|")(0)
                    Dim userid As String = con.Split("|")(1)
                    Dim password As String = _DecryptDBValue(con.Split("|")(2))

                    Dim conString As String = "Dsn=" & dsn & ";uid=" & userid & ";pwd=" & password

                    Dim odbcCon As OdbcConnection = New OdbcConnection(conString)
                    Dim dt As DataTable = New DataTable(workSheetName)

                    Using odbcCon
                        odbcCon.Open()

                        Dim da As OdbcDataAdapter = New OdbcDataAdapter(cmdText, odbcCon)

                        da.Fill(dt)
                        ds.Tables.Add(dt)
                        odbcCon.Close()
                    End Using

                    oRs.MoveNext()
                Loop

                oRs.Close()

                If ExcelMan.clsExcelMan.createWorkBookFromDatasetGemBox(ds, outputFile, errInfo) = False Then
                    Throw New Exception("Error processing data set: " & errInfo.Message)
                End If
            End If
        End If

        Return True
    End Function
    Public Function databaseExport(ByVal taskID As Integer) As Boolean

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oParse As clsMarsParser = New clsMarsParser
        Dim taskName As String = ""
        Dim ok As Boolean
        'Connection String -> ProgramParameters
        'Final SQL -> Msg


        SQL = "SELECT * FROM tasks WHERE taskid =" & taskID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            '//create our datatable

            Dim con As String = IsNull(oRs("programparameters").Value, "")
            Dim strSQL As String = IsNull(oRs("msg").Value)
            Dim dsn, userid, password As String
            Dim dt As DataTable = New DataTable("CRDExport")

            taskName = oRs("taskname").Value
            dsn = con.Split("|")(0)
            userid = con.Split("|")(1)
            password = _DecryptDBValue(con.Split("|")(2))

            Dim conString As String = "Dsn=" & dsn & ";uid=" & userid & ";pwd=" & password

            Dim odbcCon As OdbcConnection = New OdbcConnection(conString)

            Using odbcCon
                odbcCon.ConnectionTimeout = 0
                odbcCon.Open()

                Dim da As OdbcDataAdapter = New OdbcDataAdapter(strSQL, odbcCon)

                da.Fill(dt)

                odbcCon.Close()
            End Using

            oRs.Close()

            SQL = "SELECT * FROM destinationattr WHERE reportid =" & taskID

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim destinationID As String = oRs("destinationid").Value
                    Dim sExt As String = oParse.ParseString(IsNull(oRs("customext").Value))
                    Dim adjustStamp As String = IsNull(oRs("adjuststamp").Value, 0)
                    Dim sFormat As String = IsNull(oRs("outputformat").Value, "")
                    Dim sDateStamp As String = ""
                    Dim sSendTo As String = ""
                    Dim sSubject, senderName, senderAddress As String
                    Dim sCc As String
                    Dim sBcc As String
                    Dim sExtras As String
                    Dim sMsg As String
                    Dim sName As String
                    Dim sPath As String
                    Dim oDynamicData As New clsMarsDynamic
                    Dim Embed As Boolean
                    Dim MailFormat As String
                    Dim ScheduleType As String = "Single"
                    Dim nDeferBy As Double
                    Dim SMTPServer As String
                    Dim ReadReceipt As Boolean = False
                    Dim oUi As clsMarsUI = New clsMarsUI
                    Dim oMsg As clsMarsMessaging = New clsMarsMessaging

                    If Convert.ToBoolean(oRs("AppendDateTime").Value) = True Then
                        sDateStamp = IsNull(oRs("DateTimeFormat").Value)

                        sDateStamp = (Date.Now).AddDays(adjustStamp).ToString(sDateStamp)

                        For Each s As String In IO.Path.GetInvalidFileNameChars
                            sDateStamp = sDateStamp.Replace(s, "")
                        Next
                    End If

                    '//if odbc then export to csv"
                    If sFormat.ToLower = "odbc (*.odbc)" Then
                        sFormat = "csv (*.csv)"
                    End If

                    '//produce the report
                    Dim err As Exception = Nothing

                    Dim export As String = processQuery(sFormat, dt, destinationID, err)

                    dt.Dispose()

                    If export Is Nothing Then Throw err

                    Dim appendDate As Boolean

                    Try
                        appendDate = Convert.ToBoolean(oRs("AppendDateTime").Value)
                    Catch : End Try

                    '//customize the name or set it to the taskname
                    If IsNull(oRs("CustomName").Value) <> "" Then
                        Dim customName As String = oParse.ParseString(oRs("CustomName").Value)

                        customName = customName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                        Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")

                        Dim newFile As String = ""

                        If appendDate Then
                            newFile = IO.Path.GetDirectoryName(export) & "\" & customName & sDateStamp & IO.Path.GetExtension(export)
                        Else
                            newFile = IO.Path.GetDirectoryName(export) & "\" & customName & IO.Path.GetExtension(export)
                        End If

                        If IO.File.Exists(newFile) Then IO.File.Delete(newFile)

                        IO.File.Move(export, newFile)

                        export = newFile
                    Else
                        Dim customName As String = taskName

                        customName = customName.Replace(":", "").Replace("\", "").Replace("/", "").Replace("|", "").Replace("?", ""). _
                        Replace("""", "").Replace("<", "").Replace(">", "").Replace("*", "")

                        Dim newFile As String = ""

                        If appendDate Then
                            newFile = IO.Path.GetDirectoryName(export) & "\" & customName & sDateStamp & IO.Path.GetExtension(export)
                        Else
                            newFile = IO.Path.GetDirectoryName(export) & "\" & customName & IO.Path.GetExtension(export)
                        End If

                        If IO.File.Exists(newFile) Then IO.File.Delete(newFile)

                        IO.File.Move(export, newFile)

                        export = newFile
                    End If

                    If sExt <> "" Then
                        If sExt.StartsWith(".") = False Then sExt = "." & sExt

                        Dim newFile As String = IO.Path.GetFileNameWithoutExtension(export) & sExt

                        If IO.File.Exists(newFile) Then IO.File.Delete(newFile)
                        IO.File.Move(export, newFile)
                        export = newFile
                    End If

                    '//add datestamp is needed

                    '//PGP
                    'Dim oPGP As New clsMarsPGP
                    'Dim oPGPResult()

                    'oPGPResult = oPGP.EncryptFile(export, destinationID)

                    'If oPGPResult(0) = False Then
                    '    Throw New Exception("PGP Encryption Error!")
                    'End If

                    'export = oPGPResult(1)

                    Try
                        If oRs("compress").Value = 1 And export.Length > 0 Then
                            Dim Encrypt As Boolean
                            Dim ZipCode As String = ""

                            clsMarsUI.BusyProgress(60, "Zipping report...")

                            Try
                                Encrypt = oRs("encryptzip").Value
                                ZipCode = IsNull(oRs("encryptzipcode").Value, "")
                            Catch : Encrypt = False : End Try

                            export = clsMarsReport.oReport.ZipFiles(ScheduleType, clsMarsReport.m_OutputFolder, export, Encrypt, ZipCode)
                        End If
                    Catch : End Try

                    Select Case CType(oRs("DestinationType").Value, String).ToLower
                        Case "email"

                            sSendTo = oMsg.ResolveEmailAddress(oRs("sendto").Value)
                            sCc = oMsg.ResolveEmailAddress(oRs("cc").Value)
                            sBcc = oMsg.ResolveEmailAddress(oRs("bcc").Value)

                            sSendTo = oParse.ParseString(sSendTo)
                            sCc = oParse.ParseString(sCc)
                            sBcc = oParse.ParseString(sBcc)

                            MailFormat = oRs("mailformat").Value
                            SMTPServer = IsNull(oRs("smtpserver").Value, "Default")
                            senderName = oParse.ParseString(IsNull(oRs("sendername").Value))
                            senderAddress = oParse.ParseString(IsNull(oRs("senderaddress").Value))

                            sMsg = oParse.ParseString(oRs("message").Value)
                            sSubject = oParse.ParseString(oRs("subject").Value)

                            Try
                                ReadReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
                            Catch ex As Exception
                                ReadReceipt = False
                            End Try

                            oUi.BusyProgress(75, "Emailing report...")

                            Try
                                Embed = Convert.ToBoolean(oRs("embed").Value)
                            Catch ex As Exception
                                Embed = False
                            End Try

                            If MailType = MarsGlobal.gMailType.MAPI Then
                                ok = oMsg.SendMAPI(sSendTo, sSubject, sMsg, ScheduleType, export, 1, sExtras, _
                                sCc, sBcc, Embed, "", taskName, , , , destinationID)
                            ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                                ok = oMsg.SendSMTP(sSendTo, sSubject, sMsg, ScheduleType, export, 1, sExtras, sCc, _
                                sBcc, taskName, Embed, "", , , MailFormat, SMTPServer, , senderName, senderAddress)
                            ElseIf MailType = gMailType.GROUPWISE Then
                                ok = oMsg.SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, export, ScheduleType, sExtras, , Embed, , True, 1, taskName, MailFormat)
                            Else
                                Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                            End If

                        Case "disk"

                            Dim UseDUN As Boolean
                            Dim sDUN As String
                            Dim oNet As clsNetworking

                            'if DUN is used...
                            Try
                                UseDUN = Convert.ToBoolean(oRs("usedun").Value)
                                sDUN = IsNull(oRs("dunname").Value)
                            Catch ex As Exception
                                UseDUN = False
                            End Try

                            If UseDUN = True Then
                                oNet = New clsNetworking

                                If oNet._DialConnection(sDUN, "Custom Report Schedule Error: " & taskName & ": ") = False Then
                                    Return False
                                End If
                            End If

                            oUi.BusyProgress(75, "Copying reports...")

                            Dim file As String = ExtractFileName(export)

                            Dim paths As String = oRs("outputpath").Value

                            For Each item As String In paths.Split("|")
                                If item.Length > 0 Then
                                    sPath = oParse.ParseString(item)

                                    If sPath.EndsWith("\") = False And sPath.Length > 0 Then sPath &= "\"

                                    sPath = _CreateUNC(sPath)

                                    ok = oParse.ParseDirectory(sPath)


                                    System.IO.File.Copy(export, sPath & file, True)

                                    Dim oSys As New clsSystemTools

                                    oSys._HouseKeeping(destinationID, sPath, taskName)
                                End If
                            Next

                            Try
                                If UseDUN = True Then
                                    oNet._Disconnect()
                                End If
                            Catch : End Try
                        Case "ftp"
                            Dim oFtp As New clsMarsTask
                            Dim sFtp As String = ""
                            Dim FTPServer As String
                            Dim FTPUser As String
                            Dim FTPPassword As String
                            Dim FTPPath As String
                            Dim FTPType As String
                            Dim ftpCount As Integer = 0
                            Dim FTPPassive As String
                            Dim FtpOptions As String

                            FTPServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                            FTPUser = oParse.ParseString(oRs("ftpusername").Value)
                            FTPPassword = oParse.ParseString(oRs("ftppassword").Value)
                            FTPPath = oParse.ParseString(oRs("ftppath").Value)
                            FTPType = IsNull(oRs("ftptype").Value, "FTP")
                            FTPPassive = IsNull(oRs("ftppassive").Value, "")
                            FtpOptions = IsNull(oRs("ftpoptions").Value)

                            oUi.BusyProgress(75, "Uploading report...")

                            ftpCount = FTPServer.Split("|").GetUpperBound(0)

                            If ftpCount > 0 Then
                                ftpCount -= 1
                            Else
                                ftpCount = 0
                            End If

                            For I As Integer = 0 To ftpCount
                                Dim l_FTPServer As String = FTPServer.Split("|")(I)
                                Dim l_FTPUser As String = FTPUser.Split("|")(I)
                                Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
                                Dim l_FTPPath As String = FTPPath.Split("|")(I)
                                Dim l_FTPType As String = FTPType.Split("|")(I)
                                Dim l_FTPPort As Integer
                                Dim l_FTPPassive As Boolean = False
                                Dim l_FtpOptions As String

                                Try
                                    l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(I)))
                                Catch ex As Exception
                                    l_FTPPassive = False
                                End Try

                                Try
                                    l_FtpOptions = FtpOptions.Split("|")(I)
                                Catch ex As Exception
                                    l_FtpOptions = ""
                                End Try

                                If l_FTPServer = "" Then Continue For

                                If l_FTPServer.IndexOf(":") > -1 Then
                                    Try
                                        l_FTPPort = l_FTPServer.Split(":")(l_FTPServer.Split(":").GetUpperBound(0))
                                        l_FTPServer = l_FTPServer.Substring(0, l_FTPServer.Length - (CType(l_FTPPort, String).Length + 1))
                                    Catch
                                        l_FTPPort = 21
                                    End Try
                                Else
                                    l_FTPPort = 21
                                End If


                                oFtp.FTPUpload2(l_FTPServer, l_FTPPort, l_FTPUser, l_FTPPassword, l_FTPPath, _
                                export, l_FTPType, l_FtpOptions, , , l_FTPPassive)

                            Next
                            ok = True
                        Case "sharepoint"
                            'Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint
                            'Dim spServer As String
                            'Dim spUser As String
                            'Dim spPassword As String
                            'Dim spLib As String
                            'Dim spCount As Integer = 0

                            'spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                            'spUser = oParse.ParseString(oRs("ftpusername").Value)
                            'spPassword = oParse.ParseString(oRs("ftppassword").Value)
                            'spLib = oParse.ParseString(oRs("ftppath").Value)

                            'oUi.BusyProgress(75, "Uploading report...")

                            'spCount = spServer.Split("|").GetUpperBound(0)

                            'If spCount > 0 Then
                            '    spCount -= 1
                            'Else
                            '    spCount = 0
                            'End If

                            'For I As Integer = 0 To spCount
                            '    Dim l_spServer As String = spServer.Split("|")(I)
                            '    Dim l_spUser As String = spUser.Split("|")(I)
                            '    Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(I))
                            '    Dim l_spLib As String = spLib.Split("|")(I)
                            '    Dim errorInfo As Exception = Nothing

                            '    If l_spServer = "" Then Continue For

                            '    ok = sp.UploadDocument(l_spServer, l_spLib, export, l_spUser, l_spPassword, errorInfo)

                            '    If ok = False And errorInfo IsNot Nothing Then
                            '        Throw errorInfo
                            '    End If
                            'Next

                            'ok = True
                        Case "odbc"
                            Dim sxSep As String = "�"
                            Dim sxDel As String = ""

                            clsMarsReport.oReport.processCSVFile(export, sxSep, sxDel)

                            ok = clsMarsReport.oReport.ODBCProcessing(export, destinationID)
                        Case Else
                            Throw New Exception("Unexpected destination was encountered!")
                    End Select
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        End If

        Return ok
    End Function
    Private Function convertDataTableToExcel(ByVal dt As DataTable, ByVal outputFile As String, ByRef err As Exception) As Boolean
        Try
            Dim grid As System.Web.UI.WebControls.DataGrid = New System.Web.UI.WebControls.DataGrid

            grid.HeaderStyle.Font.Bold = True
            grid.DataSource = dt
            grid.DataMember = dt.TableName

            grid.DataBind()

            Using sw As IO.StreamWriter = New IO.StreamWriter(outputFile)
                Using hw As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(sw)
                    grid.RenderControl(hw)
                End Using
            End Using

            Return True
        Catch ex As Exception
            err = ex
            Return False
        End Try

    End Function
    Private Function processQuery(ByVal format As String, ByVal dt As DataTable, ByVal destID As Integer, ByRef errorInfo As Exception) As String
        Dim SQL As String = "SELECT * FROM reportoptions WHERE destinationid = " & destID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim outputFile As String = ""

        Try
            Select Case format.ToLower
                Case "ms excel 97-2000 (*.xls)"

                    Dim err As Exception

                    outputFile = clsMarsReport.m_OutputFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".xls"

                    If oRs.EOF = True Then
                        Dim ok As Boolean = ExcelMan.clsExcelMan.createWorkBookFromDataTableGemBox(dt, outputFile, "Sheet1", err) 'oxl.createSheetfromDataTable(dt, outputFile, err)

                        If ok = False And err IsNot Nothing Then Throw err
                    Else
                        Dim protectXL As Boolean = IsNull(oRs("protectexcel").Value, 0)
                        Dim password As String = _DecryptDBValue(IsNull(oRs("excelpassword").Value))
                        Dim sheetname As String = IsNull(oRs("worksheetname").Value)

                        If protectXL = False Then password = ""

                        Dim ok As Boolean = ExcelMan.clsExcelMan.createWorkBookFromDataTableGemBox(dt, outputFile, sheetname, err) 'oxl.createSheetfromDataTable(dt, outputFile, err, password, sheetname)

                        If ok = False And err IsNot Nothing Then Throw err

                        If sheetname <> "" Or password <> "" Then
                            Dim oxl As ExcelMan.clsExcelMan = New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel12)

                            oxl.SetWorkSheetName(outputFile, sheetname)

                            System.Threading.Thread.Sleep(2000)

                            oxl.SetWorkBookPassword(outputFile, password)

                            oxl.Dispose()
                        End If
                    End If
                Case "xml (*.xml)"

                    outputFile = clsMarsReport.m_OutputFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".xml"

                    If oRs.EOF = True Then
                        dt.WriteXml(outputFile, XmlWriteMode.WriteSchema)
                    Else
                        Dim xmlwritehierachy As Boolean = False
                        Dim xmlmode As Integer = 0
                        Dim xmltablename As String = ""

                        xmlwritehierachy = IsNull(oRs("xmlwritehierachy").Value, 0)
                        xmlmode = IsNull(oRs("xmlmode").Value, 0)
                        xmltablename = IsNull(oRs("xmltablename").Value, "")

                        If xmltablename <> "" Then dt.TableName = xmltablename
                        dt.WriteXml(outputFile, xmlmode, xmlwritehierachy)
                    End If
                Case "csv (*.csv)"

                    Dim headers As String
                    Dim records As String
                    Dim sep As String = ","
                    Dim del As String = Chr(34)

                    outputFile = clsMarsReport.m_OutputFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & ".csv"

                    If oRs.EOF = False Then
                        sep = IsNull(oRs("scharacter").Value, "")
                        del = IsNull(oRs("sdelimiter").Value, "")

                        If sep = "tab" Then
                            sep = ControlChars.Tab
                        End If
                    End If

                    '//create the columns
                    For Each col As DataColumn In dt.Columns
                        headers &= del & col.ColumnName & del & sep
                    Next

                    headers = headers.Remove(headers.Length - 1, 1)
                    '//get the data
                    For Each row As DataRow In dt.Rows
                        For Each col As DataColumn In dt.Columns
                            records &= del & row(col.ColumnName) & del & sep
                        Next

                        records = records.Remove(records.Length - 1, 1)

                        records &= ControlChars.CrLf
                    Next

                    Dim data As String = headers & ControlChars.CrLf & records

                    SaveTextToFile(data, outputFile, , False, False)
            End Select

            Return outputFile
        Catch ex As Exception
            errorInfo = ex
            Return Nothing
        End Try
    End Function

End Class
