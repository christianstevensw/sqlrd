#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Or crystal_ver = 11.5 Or CRYSTAL_VER >= 11.6 Then
Imports My.Crystal11
#End If

Imports DotRas

Public Class frmDestinationSelect
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Dim oField As Object
    Dim sField As String = "to"
    Dim Dynamic As Boolean
    Dim Package As Boolean
    Dim Bursting As Boolean
    Dim DataDriven As Boolean
    Dim IsSmart As Boolean
    Dim sTitle As String = ""
    Dim xDestinationID As Integer = 99999
    Dim xReportID As Integer = 99999
    Dim xEmbedID As Integer = 99999
    Dim IsQuery As Boolean
    Dim m_inserter As frmInserter
    Public IsStatic As Boolean = False
    Dim oSec As New clsMarsSecurity
    Dim sMode As String = "Add"
    Dim formLoaded As Boolean = False
    Dim ep As New ErrorProvider
    Dim spServer, spUsername, spPassword, spLibrary As String
    Friend WithEvents grpODBC As System.Windows.Forms.Panel
    Friend WithEvents txtTableName As DevComponents.DotNetBar.Controls.TextBoxX   'DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label24 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnODBC As DevComponents.DotNetBar.ButtonX
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents pnReadReciepts As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnMailServer As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkReadReceipt As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnReadReceipts As DevComponents.DotNetBar.ButtonX  'System.Windows.Forms.Button 
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As DevComponents.DotNetBar.LabelX
    Dim oNet As New clsNetworking
    Dim eventBased As Boolean = False
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Friend WithEvents lsvPaths As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents btnAddPath As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnRemovePath As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnRemoveFTP As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAddFTP As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvFTP As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkOverwrite As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents superTip As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents chkKeyParameter As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents UcFTP As sqlrd.ucFTPDetails
    Public eventID As Integer = 99999
    Friend WithEvents grpSender As System.Windows.Forms.GroupBox
    Friend WithEvents txtSenderAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSenderName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label18 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label17 As DevComponents.DotNetBar.LabelX
    Public disableEmbed As Boolean
    Friend WithEvents btnEmbedOptions As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpSharePoint As System.Windows.Forms.GroupBox
    Friend WithEvents btnAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvSharePointLibs As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents btnRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuEditSharepoint As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SimpleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdvancedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkDeliveryReciept As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnDeliveryReciept As DevComponents.DotNetBar.ButtonX
    Friend WithEvents stabDestination As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents stabSetupPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabSetup As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabFormatPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabFormat As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabNamingPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabNaming As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabMiscPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabCompression As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents stabPGPPanel As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents stabPGP As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ucRptOptions As sqlrd.frmRptOptions
    Friend WithEvents btnPicker As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkStatus As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents grpHouseKeeping As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtMsg As sqlrd.ucEditorX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents grpNothing As System.Windows.Forms.Panel
    Friend WithEvents UcMessageLabel1 As sqlrd.ucMessageLabel
    Friend WithEvents chkInstadrop As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpCloud As System.Windows.Forms.Panel
    Friend WithEvents btnBrowseCloud As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtCloudFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbCloundAccount As System.Windows.Forms.ComboBox
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel


    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Property m_IsQuery() As Boolean
        Get
            Return Me.IsQuery
        End Get
        Set(ByVal value As Boolean)
            IsQuery = value
        End Set
    End Property

    Public Property outputType() As String
        Get
            Return cmbDestination.Text
        End Get
        Set(ByVal value As String)
            cmbDestination.Text = value
        End Set
    End Property
    Private Sub vetOutputTypes(ByRef s() As String)
        Dim grp As usergroup = New usergroup(gRole)

        If grp.destinationAllowance <> usergroup.e_destinationAllowance.TypesOnly Then Return

        Dim allowedDestinations As ArrayList = grp.getUserAllowedDestinationTypes

        Dim newList As ArrayList = New ArrayList

        For Each val As String In s
            If allowedDestinations.Contains(val) Then
                newList.Add(val)
            End If
        Next

        s = newList.ToArray(GetType(String))
    End Sub
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpEmail As System.Windows.Forms.Panel
    Friend WithEvents txtTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCC As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdBcc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtCC As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtBCC As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtSubject As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtAttach As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdAttach As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpPrinter As System.Windows.Forms.Panel
    Friend WithEvents lsvPrinters As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents cmdAddPrinter As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbPrinters As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRemovePrinter As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpFile As System.Windows.Forms.Panel
    Friend WithEvents txtDirectory As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpFTP As System.Windows.Forms.Panel
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbDestination As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents ofg As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents chkIncludeAttach As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkEmbed As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkCustomExt As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtCustomName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents optNaming As System.Windows.Forms.RadioButton
    Friend WithEvents optCustomName As System.Windows.Forms.RadioButton
    Friend WithEvents txtCustomExt As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkAppendDateTime As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkZip As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdInsert As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdInsert2 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents cmbMailFormat As System.Windows.Forms.ComboBox
    Friend WithEvents chkSplit As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSignature As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents txtDefault As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkDefer As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtDefer As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkHouseKeeping As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtHouseNum As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents chkburst As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbServer As System.Windows.Forms.ComboBox
    Friend WithEvents grpFax As System.Windows.Forms.Panel
    Friend WithEvents cmdFaxTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFaxNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbFaxDevice As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFaxTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtFaxFrom As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFaxComments As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuAttachment As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents cmdProps As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkZipSecurity As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmbEncryptionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtZipCode As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtConfirmCode As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpZip As System.Windows.Forms.GroupBox
    Friend WithEvents chkPGP As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents UcPGP As sqlrd.ucPGP
    Friend WithEvents chkDUN As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbDUN As System.Windows.Forms.ComboBox
    Friend WithEvents cmdConnect As DevComponents.DotNetBar.ButtonX
    Friend WithEvents grpSMS As System.Windows.Forms.Panel
    Friend WithEvents txtSMSMsg As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label23 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMSNumber As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdSMSTo As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDestinationSelect))
        Me.grpEmail = New System.Windows.Forms.Panel()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.btnEmbedOptions = New DevComponents.DotNetBar.ButtonX()
        Me.grpSender = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtSenderAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label18 = New DevComponents.DotNetBar.LabelX()
        Me.txtSenderName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label17 = New DevComponents.DotNetBar.LabelX()
        Me.pnMailServer = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.cmbServer = New System.Windows.Forms.ComboBox()
        Me.chkDeliveryReciept = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnDeliveryReciept = New DevComponents.DotNetBar.ButtonX()
        Me.cmbMailFormat = New System.Windows.Forms.ComboBox()
        Me.chkEmbed = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdAttach = New DevComponents.DotNetBar.ButtonX()
        Me.cmdTo = New DevComponents.DotNetBar.ButtonX()
        Me.txtTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdBcc = New DevComponents.DotNetBar.ButtonX()
        Me.txtAttach = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem()
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem()
        Me.mnuSignature = New System.Windows.Forms.MenuItem()
        Me.mnuAttachment = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.mnuSpell = New System.Windows.Forms.MenuItem()
        Me.cmdCC = New DevComponents.DotNetBar.ButtonX()
        Me.txtSubject = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCC = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.txtBCC = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.pnReadReciepts = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkReadReceipt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnReadReceipts = New DevComponents.DotNetBar.ButtonX()
        Me.cmdInsert = New DevComponents.DotNetBar.ButtonX()
        Me.chkburst = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpPrinter = New System.Windows.Forms.Panel()
        Me.lsvPrinters = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdAddPrinter = New DevComponents.DotNetBar.ButtonX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.cmbPrinters = New System.Windows.Forms.ComboBox()
        Me.cmdRemovePrinter = New DevComponents.DotNetBar.ButtonX()
        Me.grpFile = New System.Windows.Forms.Panel()
        Me.chkInstadrop = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.lsvPaths = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grpHouseKeeping = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtHouseNum = New System.Windows.Forms.NumericUpDown()
        Me.cmbUnit = New System.Windows.Forms.ComboBox()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddPath = New DevComponents.DotNetBar.ButtonX()
        Me.chkHouseKeeping = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnRemovePath = New DevComponents.DotNetBar.ButtonX()
        Me.chkDUN = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtDirectory = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.cmbDUN = New System.Windows.Forms.ComboBox()
        Me.Label21 = New DevComponents.DotNetBar.LabelX()
        Me.cmdConnect = New DevComponents.DotNetBar.ButtonX()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.cmdInsert2 = New DevComponents.DotNetBar.ButtonX()
        Me.grpFTP = New System.Windows.Forms.Panel()
        Me.btnRemoveFTP = New DevComponents.DotNetBar.ButtonX()
        Me.btnAddFTP = New DevComponents.DotNetBar.ButtonX()
        Me.lsvFTP = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDestination = New System.Windows.Forms.ComboBox()
        Me.stabDestination = New DevComponents.DotNetBar.SuperTabControl()
        Me.stabSetupPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.grpNothing = New System.Windows.Forms.Panel()
        Me.grpCloud = New System.Windows.Forms.Panel()
        Me.btnBrowseCloud = New DevComponents.DotNetBar.ButtonX()
        Me.txtCloudFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbCloundAccount = New System.Windows.Forms.ComboBox()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX1 = New DevComponents.DotNetBar.LabelX()
        Me.grpSharePoint = New System.Windows.Forms.GroupBox()
        Me.btnEdit = New DevComponents.DotNetBar.ButtonX()
        Me.btnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.btnAdd = New DevComponents.DotNetBar.ButtonX()
        Me.lsvSharePointLibs = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader11 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.grpSMS = New System.Windows.Forms.Panel()
        Me.Label23 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMSMsg = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtSMSNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdSMSTo = New DevComponents.DotNetBar.ButtonX()
        Me.grpODBC = New System.Windows.Forms.Panel()
        Me.chkOverwrite = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtTableName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label24 = New DevComponents.DotNetBar.LabelX()
        Me.btnODBC = New DevComponents.DotNetBar.ButtonX()
        Me.grpFax = New System.Windows.Forms.Panel()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.txtFaxTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtFaxFrom = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtFaxComments = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmbFaxDevice = New System.Windows.Forms.ComboBox()
        Me.txtFaxNumber = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdFaxTo = New DevComponents.DotNetBar.ButtonX()
        Me.stabSetup = New DevComponents.DotNetBar.SuperTabItem()
        Me.stabFormatPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.cmdProps = New DevComponents.DotNetBar.ButtonX()
        Me.cmbFormat = New System.Windows.Forms.ComboBox()
        Me.Label22 = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.chkIncludeAttach = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.stabFormat = New DevComponents.DotNetBar.SuperTabItem()
        Me.stabMiscPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.txtDefer = New System.Windows.Forms.NumericUpDown()
        Me.grpZip = New System.Windows.Forms.GroupBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtZipCode = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.cmbEncryptionLevel = New System.Windows.Forms.ComboBox()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.txtConfirmCode = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkZipSecurity = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.chkZip = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkDefer = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.stabCompression = New DevComponents.DotNetBar.SuperTabItem()
        Me.stabNamingPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkKeyParameter = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.chkCustomExt = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtCustomName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.optNaming = New System.Windows.Forms.RadioButton()
        Me.optCustomName = New System.Windows.Forms.RadioButton()
        Me.txtCustomExt = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkAppendDateTime = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtDefault = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.stabNaming = New DevComponents.DotNetBar.SuperTabItem()
        Me.stabPGPPanel = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.chkPGP = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.stabPGP = New DevComponents.DotNetBar.SuperTabItem()
        Me.chkSplit = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.mnuDB = New System.Windows.Forms.MenuItem()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuConstants = New System.Windows.Forms.MenuItem()
        Me.ofg = New System.Windows.Forms.OpenFileDialog()
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu()
        Me.MenuItem15 = New System.Windows.Forms.MenuItem()
        Me.MenuItem16 = New System.Windows.Forms.MenuItem()
        Me.MenuItem17 = New System.Windows.Forms.MenuItem()
        Me.MenuItem18 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.superTip = New DevComponents.DotNetBar.SuperTooltip()
        Me.mnuEditSharepoint = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SimpleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdvancedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnPicker = New DevComponents.DotNetBar.ButtonX()
        Me.chkStatus = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.UcMessageLabel1 = New sqlrd.ucMessageLabel()
        Me.txtMsg = New sqlrd.ucEditorX()
        Me.UcFTP = New sqlrd.ucFTPDetails()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.ucRptOptions = New sqlrd.frmRptOptions()
        Me.UcPGP = New sqlrd.ucPGP()
        Me.grpEmail.SuspendLayout()
        Me.grpSender.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.pnMailServer.SuspendLayout()
        Me.pnReadReciepts.SuspendLayout()
        Me.grpPrinter.SuspendLayout()
        Me.grpFile.SuspendLayout()
        Me.grpHouseKeeping.SuspendLayout()
        CType(Me.txtHouseNum, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.grpFTP.SuspendLayout()
        CType(Me.stabDestination, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabDestination.SuspendLayout()
        Me.stabSetupPanel.SuspendLayout()
        Me.grpNothing.SuspendLayout()
        Me.grpCloud.SuspendLayout()
        Me.grpSharePoint.SuspendLayout()
        Me.grpSMS.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.grpFax.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.stabFormatPanel.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.stabMiscPanel.SuspendLayout()
        CType(Me.txtDefer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpZip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.stabNamingPanel.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabPGPPanel.SuspendLayout()
        Me.mnuEditSharepoint.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpEmail
        '
        Me.grpEmail.BackColor = System.Drawing.Color.Transparent
        Me.grpEmail.Controls.Add(Me.txtMsg)
        Me.grpEmail.Controls.Add(Me.Label2)
        Me.grpEmail.Controls.Add(Me.btnEmbedOptions)
        Me.grpEmail.Controls.Add(Me.grpSender)
        Me.grpEmail.Controls.Add(Me.pnMailServer)
        Me.grpEmail.Controls.Add(Me.cmbMailFormat)
        Me.grpEmail.Controls.Add(Me.chkEmbed)
        Me.grpEmail.Controls.Add(Me.cmdAttach)
        Me.grpEmail.Controls.Add(Me.cmdTo)
        Me.grpEmail.Controls.Add(Me.txtTo)
        Me.grpEmail.Controls.Add(Me.cmdBcc)
        Me.grpEmail.Controls.Add(Me.txtAttach)
        Me.grpEmail.Controls.Add(Me.cmdCC)
        Me.grpEmail.Controls.Add(Me.txtSubject)
        Me.grpEmail.Controls.Add(Me.txtCC)
        Me.grpEmail.Controls.Add(Me.Label14)
        Me.grpEmail.Controls.Add(Me.txtBCC)
        Me.grpEmail.Controls.Add(Me.pnReadReciepts)
        Me.grpEmail.Location = New System.Drawing.Point(4, 3)
        Me.grpEmail.Name = "grpEmail"
        Me.grpEmail.Size = New System.Drawing.Size(649, 554)
        Me.grpEmail.TabIndex = 8
        Me.grpEmail.Visible = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(19, 392)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 16)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "Format"
        '
        'btnEmbedOptions
        '
        Me.btnEmbedOptions.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEmbedOptions.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnEmbedOptions.Enabled = False
        Me.btnEmbedOptions.Location = New System.Drawing.Point(195, 363)
        Me.btnEmbedOptions.Name = "btnEmbedOptions"
        Me.btnEmbedOptions.Size = New System.Drawing.Size(31, 24)
        Me.btnEmbedOptions.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnEmbedOptions.TabIndex = 28
        Me.btnEmbedOptions.Text = "..."
        '
        'grpSender
        '
        Me.grpSender.Controls.Add(Me.TableLayoutPanel1)
        Me.grpSender.Location = New System.Drawing.Point(6, 460)
        Me.grpSender.Name = "grpSender"
        Me.grpSender.Size = New System.Drawing.Size(619, 86)
        Me.grpSender.TabIndex = 15
        Me.grpSender.TabStop = False
        Me.grpSender.Text = "Customize sender details (optional)"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.txtSenderAddress, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label18, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtSenderName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label17, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(10, 18)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(603, 61)
        Me.TableLayoutPanel1.TabIndex = 19
        '
        'txtSenderAddress
        '
        '
        '
        '
        Me.txtSenderAddress.Border.Class = "TextBoxBorder"
        Me.txtSenderAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderAddress.Location = New System.Drawing.Point(54, 33)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(549, 21)
        Me.txtSenderAddress.TabIndex = 17
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        '
        '
        '
        Me.Label18.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label18.Location = New System.Drawing.Point(3, 33)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(45, 16)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Address:"
        '
        'txtSenderName
        '
        '
        '
        '
        Me.txtSenderName.Border.Class = "TextBoxBorder"
        Me.txtSenderName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderName.Location = New System.Drawing.Point(54, 3)
        Me.txtSenderName.Name = "txtSenderName"
        Me.txtSenderName.Size = New System.Drawing.Size(549, 21)
        Me.txtSenderName.TabIndex = 16
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        '
        '
        '
        Me.Label17.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label17.Location = New System.Drawing.Point(3, 3)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(35, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name:"
        '
        'pnMailServer
        '
        Me.pnMailServer.Controls.Add(Me.Label5)
        Me.pnMailServer.Controls.Add(Me.cmbServer)
        Me.pnMailServer.Controls.Add(Me.chkDeliveryReciept)
        Me.pnMailServer.Controls.Add(Me.btnDeliveryReciept)
        Me.pnMailServer.Location = New System.Drawing.Point(16, 423)
        Me.pnMailServer.Name = "pnMailServer"
        Me.pnMailServer.Size = New System.Drawing.Size(407, 27)
        Me.pnMailServer.TabIndex = 27
        Me.pnMailServer.Visible = False
        '
        'Label5
        '
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(3, 3)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 24)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Mail Server"
        '
        'cmbServer
        '
        Me.cmbServer.ItemHeight = 13
        Me.cmbServer.Items.AddRange(New Object() {"Default"})
        Me.cmbServer.Location = New System.Drawing.Point(73, 3)
        Me.cmbServer.Name = "cmbServer"
        Me.cmbServer.Size = New System.Drawing.Size(136, 21)
        Me.cmbServer.TabIndex = 26
        '
        'chkDeliveryReciept
        '
        Me.chkDeliveryReciept.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.chkDeliveryReciept.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDeliveryReciept.ForeColor = System.Drawing.Color.Navy
        Me.chkDeliveryReciept.Location = New System.Drawing.Point(215, 3)
        Me.chkDeliveryReciept.Name = "chkDeliveryReciept"
        Me.chkDeliveryReciept.Size = New System.Drawing.Size(127, 24)
        Me.chkDeliveryReciept.TabIndex = 29
        Me.chkDeliveryReciept.Text = "Delivery Reciept"
        '
        'btnDeliveryReciept
        '
        Me.btnDeliveryReciept.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDeliveryReciept.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDeliveryReciept.Enabled = False
        Me.btnDeliveryReciept.Location = New System.Drawing.Point(348, 3)
        Me.btnDeliveryReciept.Name = "btnDeliveryReciept"
        Me.btnDeliveryReciept.Size = New System.Drawing.Size(31, 24)
        Me.btnDeliveryReciept.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnDeliveryReciept.TabIndex = 28
        Me.btnDeliveryReciept.Text = "..."
        '
        'cmbMailFormat
        '
        Me.cmbMailFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailFormat.ItemHeight = 13
        Me.cmbMailFormat.Items.AddRange(New Object() {"TEXT", "HTML", "HTML (Basic)"})
        Me.cmbMailFormat.Location = New System.Drawing.Point(91, 390)
        Me.cmbMailFormat.Name = "cmbMailFormat"
        Me.cmbMailFormat.Size = New System.Drawing.Size(135, 21)
        Me.cmbMailFormat.TabIndex = 10
        '
        'chkEmbed
        '
        Me.chkEmbed.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkEmbed.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkEmbed.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkEmbed.ForeColor = System.Drawing.Color.Navy
        Me.chkEmbed.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmbed.Location = New System.Drawing.Point(19, 363)
        Me.chkEmbed.Name = "chkEmbed"
        Me.chkEmbed.Size = New System.Drawing.Size(129, 21)
        Me.chkEmbed.TabIndex = 11
        Me.chkEmbed.Text = "Embed report"
        '
        'cmdAttach
        '
        Me.cmdAttach.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAttach.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAttach.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAttach.Image = CType(resources.GetObject("cmdAttach.Image"), System.Drawing.Image)
        Me.cmdAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAttach.Location = New System.Drawing.Point(11, 128)
        Me.cmdAttach.Name = "cmdAttach"
        Me.cmdAttach.Size = New System.Drawing.Size(64, 21)
        Me.cmdAttach.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdAttach.TabIndex = 7
        Me.cmdAttach.Text = "Attach"
        '
        'cmdTo
        '
        Me.cmdTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTo.Image = CType(resources.GetObject("cmdTo.Image"), System.Drawing.Image)
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(11, 19)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdTo.TabIndex = 0
        Me.cmdTo.Text = "To..."
        '
        'txtTo
        '
        '
        '
        '
        Me.txtTo.Border.Class = "TextBoxBorder"
        Me.txtTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTo.ForeColor = System.Drawing.Color.Blue
        Me.txtTo.Location = New System.Drawing.Point(91, 19)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(535, 21)
        Me.txtTo.TabIndex = 1
        Me.txtTo.Tag = "memo"
        '
        'cmdBcc
        '
        Me.cmdBcc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBcc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBcc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBcc.Image = CType(resources.GetObject("cmdBcc.Image"), System.Drawing.Image)
        Me.cmdBcc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBcc.Location = New System.Drawing.Point(11, 73)
        Me.cmdBcc.Name = "cmdBcc"
        Me.cmdBcc.Size = New System.Drawing.Size(64, 21)
        Me.cmdBcc.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdBcc.TabIndex = 4
        Me.cmdBcc.Text = "BCC..."
        '
        'txtAttach
        '
        Me.txtAttach.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtAttach.Border.Class = "TextBoxBorder"
        Me.txtAttach.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAttach.ContextMenu = Me.mnuInserter
        Me.txtAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtAttach.HideSelection = False
        Me.txtAttach.Location = New System.Drawing.Point(91, 128)
        Me.txtAttach.Name = "txtAttach"
        Me.txtAttach.Size = New System.Drawing.Size(535, 21)
        Me.txtAttach.TabIndex = 8
        Me.txtAttach.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem13, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuDefMsg, Me.mnuSignature, Me.mnuAttachment})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 5
        Me.mnuDefMsg.Text = "Default Message"
        '
        'mnuSignature
        '
        Me.mnuSignature.Index = 6
        Me.mnuSignature.Text = "Default Signature"
        '
        'mnuAttachment
        '
        Me.mnuAttachment.Index = 7
        Me.mnuAttachment.Text = "Default Attachment"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 10
        Me.MenuItem13.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'cmdCC
        '
        Me.cmdCC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCC.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCC.Image = CType(resources.GetObject("cmdCC.Image"), System.Drawing.Image)
        Me.cmdCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCC.Location = New System.Drawing.Point(11, 46)
        Me.cmdCC.Name = "cmdCC"
        Me.cmdCC.Size = New System.Drawing.Size(64, 21)
        Me.cmdCC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdCC.TabIndex = 2
        Me.cmdCC.Text = "CC..."
        '
        'txtSubject
        '
        '
        '
        '
        Me.txtSubject.Border.Class = "TextBoxBorder"
        Me.txtSubject.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSubject.ContextMenu = Me.mnuInserter
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(91, 100)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(535, 21)
        Me.txtSubject.TabIndex = 6
        Me.txtSubject.Tag = "memo"
        '
        'txtCC
        '
        '
        '
        '
        Me.txtCC.Border.Class = "TextBoxBorder"
        Me.txtCC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCC.ForeColor = System.Drawing.Color.Blue
        Me.txtCC.Location = New System.Drawing.Point(91, 46)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(535, 21)
        Me.txtCC.TabIndex = 3
        Me.txtCC.Tag = "memo"
        '
        'Label14
        '
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.Enabled = False
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(11, 105)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 16)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "Subject"
        '
        'txtBCC
        '
        '
        '
        '
        Me.txtBCC.Border.Class = "TextBoxBorder"
        Me.txtBCC.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBCC.ForeColor = System.Drawing.Color.Blue
        Me.txtBCC.Location = New System.Drawing.Point(91, 73)
        Me.txtBCC.Name = "txtBCC"
        Me.txtBCC.Size = New System.Drawing.Size(535, 21)
        Me.txtBCC.TabIndex = 5
        Me.txtBCC.Tag = "memo"
        '
        'pnReadReciepts
        '
        Me.pnReadReciepts.Controls.Add(Me.chkReadReceipt)
        Me.pnReadReciepts.Controls.Add(Me.btnReadReceipts)
        Me.pnReadReciepts.Location = New System.Drawing.Point(16, 422)
        Me.pnReadReciepts.Name = "pnReadReciepts"
        Me.pnReadReciepts.Size = New System.Drawing.Size(405, 28)
        Me.pnReadReciepts.TabIndex = 12
        Me.pnReadReciepts.Visible = False
        '
        'chkReadReceipt
        '
        '
        '
        '
        Me.chkReadReceipt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkReadReceipt.Location = New System.Drawing.Point(3, 3)
        Me.chkReadReceipt.Name = "chkReadReceipt"
        Me.chkReadReceipt.Size = New System.Drawing.Size(222, 24)
        Me.chkReadReceipt.TabIndex = 13
        Me.chkReadReceipt.Text = "Request a read receipt for this message"
        '
        'btnReadReceipts
        '
        Me.btnReadReceipts.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnReadReceipts.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnReadReceipts.Enabled = False
        Me.btnReadReceipts.Location = New System.Drawing.Point(231, 3)
        Me.btnReadReceipts.Name = "btnReadReceipts"
        Me.btnReadReceipts.Size = New System.Drawing.Size(31, 24)
        Me.btnReadReceipts.TabIndex = 14
        Me.btnReadReceipts.Text = "..."
        '
        'cmdInsert
        '
        Me.cmdInsert.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdInsert.Image = CType(resources.GetObject("cmdInsert.Image"), System.Drawing.Image)
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(232, 765)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(24, 23)
        Me.cmdInsert.TabIndex = 24
        Me.cmdInsert.Visible = False
        '
        'chkburst
        '
        '
        '
        '
        Me.chkburst.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkburst.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkburst.Location = New System.Drawing.Point(20, 764)
        Me.chkburst.Name = "chkburst"
        Me.chkburst.Size = New System.Drawing.Size(192, 24)
        Me.chkburst.TabIndex = 26
        Me.chkburst.Text = "Use report bursting for emails"
        Me.chkburst.Visible = False
        '
        'grpPrinter
        '
        Me.grpPrinter.BackColor = System.Drawing.Color.Transparent
        Me.grpPrinter.Controls.Add(Me.lsvPrinters)
        Me.grpPrinter.Controls.Add(Me.cmdAddPrinter)
        Me.grpPrinter.Controls.Add(Me.Label16)
        Me.grpPrinter.Controls.Add(Me.cmbPrinters)
        Me.grpPrinter.Controls.Add(Me.cmdRemovePrinter)
        Me.grpPrinter.Location = New System.Drawing.Point(4, 3)
        Me.grpPrinter.Name = "grpPrinter"
        Me.grpPrinter.Size = New System.Drawing.Size(649, 554)
        Me.grpPrinter.TabIndex = 10
        Me.grpPrinter.Visible = False
        '
        'lsvPrinters
        '
        '
        '
        '
        Me.lsvPrinters.Border.Class = "ListViewBorder"
        Me.lsvPrinters.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvPrinters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvPrinters.FullRowSelect = True
        Me.lsvPrinters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvPrinters.Location = New System.Drawing.Point(8, 96)
        Me.lsvPrinters.Name = "lsvPrinters"
        Me.lsvPrinters.Size = New System.Drawing.Size(628, 450)
        Me.lsvPrinters.TabIndex = 3
        Me.lsvPrinters.UseCompatibleStateImageBehavior = False
        Me.lsvPrinters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Printer Name"
        Me.ColumnHeader3.Width = 325
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Copies"
        '
        'cmdAddPrinter
        '
        Me.cmdAddPrinter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddPrinter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddPrinter.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddPrinter.Image = CType(resources.GetObject("cmdAddPrinter.Image"), System.Drawing.Image)
        Me.cmdAddPrinter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddPrinter.Location = New System.Drawing.Point(601, 64)
        Me.cmdAddPrinter.Name = "cmdAddPrinter"
        Me.cmdAddPrinter.Size = New System.Drawing.Size(35, 24)
        Me.cmdAddPrinter.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdAddPrinter.TabIndex = 1
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.Enabled = False
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 24)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(160, 16)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Select Printers"
        '
        'cmbPrinters
        '
        Me.cmbPrinters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinters.ForeColor = System.Drawing.Color.Blue
        Me.cmbPrinters.ItemHeight = 13
        Me.cmbPrinters.Location = New System.Drawing.Point(8, 40)
        Me.cmbPrinters.Name = "cmbPrinters"
        Me.cmbPrinters.Size = New System.Drawing.Size(628, 21)
        Me.cmbPrinters.TabIndex = 0
        '
        'cmdRemovePrinter
        '
        Me.cmdRemovePrinter.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemovePrinter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemovePrinter.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemovePrinter.Image = CType(resources.GetObject("cmdRemovePrinter.Image"), System.Drawing.Image)
        Me.cmdRemovePrinter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemovePrinter.Location = New System.Drawing.Point(8, 64)
        Me.cmdRemovePrinter.Name = "cmdRemovePrinter"
        Me.cmdRemovePrinter.Size = New System.Drawing.Size(35, 24)
        Me.cmdRemovePrinter.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdRemovePrinter.TabIndex = 2
        '
        'grpFile
        '
        Me.grpFile.BackColor = System.Drawing.Color.Transparent
        Me.grpFile.Controls.Add(Me.chkInstadrop)
        Me.grpFile.Controls.Add(Me.lsvPaths)
        Me.grpFile.Controls.Add(Me.grpHouseKeeping)
        Me.grpFile.Controls.Add(Me.cmdBrowse)
        Me.grpFile.Controls.Add(Me.btnAddPath)
        Me.grpFile.Controls.Add(Me.chkHouseKeeping)
        Me.grpFile.Controls.Add(Me.btnRemovePath)
        Me.grpFile.Controls.Add(Me.chkDUN)
        Me.grpFile.Controls.Add(Me.txtDirectory)
        Me.grpFile.Controls.Add(Me.GroupBox7)
        Me.grpFile.Controls.Add(Me.Label15)
        Me.grpFile.Location = New System.Drawing.Point(4, 3)
        Me.grpFile.Name = "grpFile"
        Me.grpFile.Size = New System.Drawing.Size(649, 554)
        Me.grpFile.TabIndex = 1
        Me.grpFile.Visible = False
        '
        'chkInstadrop
        '
        '
        '
        '
        Me.chkInstadrop.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkInstadrop.Location = New System.Drawing.Point(11, 92)
        Me.chkInstadrop.Name = "chkInstadrop"
        Me.chkInstadrop.Size = New System.Drawing.Size(427, 23)
        Me.chkInstadrop.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkInstadrop.TabIndex = 30
        Me.chkInstadrop.Text = "Deliver the report immediately after production"
        Me.chkInstadrop.Visible = False
        '
        'lsvPaths
        '
        '
        '
        '
        Me.lsvPaths.Border.Class = "ListViewBorder"
        Me.lsvPaths.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvPaths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvPaths.FullRowSelect = True
        Me.lsvPaths.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvPaths.Location = New System.Drawing.Point(11, 244)
        Me.lsvPaths.Name = "lsvPaths"
        Me.lsvPaths.Size = New System.Drawing.Size(625, 302)
        Me.lsvPaths.TabIndex = 25
        Me.lsvPaths.UseCompatibleStateImageBehavior = False
        Me.lsvPaths.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Path"
        Me.ColumnHeader1.Width = 379
        '
        'grpHouseKeeping
        '
        Me.grpHouseKeeping.Controls.Add(Me.Label4)
        Me.grpHouseKeeping.Controls.Add(Me.txtHouseNum)
        Me.grpHouseKeeping.Controls.Add(Me.cmbUnit)
        Me.grpHouseKeeping.Enabled = False
        Me.grpHouseKeeping.Location = New System.Drawing.Point(14, 139)
        Me.grpHouseKeeping.Name = "grpHouseKeeping"
        Me.grpHouseKeeping.Size = New System.Drawing.Size(427, 28)
        Me.grpHouseKeeping.TabIndex = 29
        '
        'Label4
        '
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 3)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 21)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Delete reports older than"
        '
        'txtHouseNum
        '
        Me.txtHouseNum.Location = New System.Drawing.Point(141, 3)
        Me.txtHouseNum.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtHouseNum.Name = "txtHouseNum"
        Me.txtHouseNum.Size = New System.Drawing.Size(64, 21)
        Me.txtHouseNum.TabIndex = 1
        Me.txtHouseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHouseNum.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.ItemHeight = 13
        Me.cmbUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months"})
        Me.cmbUnit.Location = New System.Drawing.Point(211, 3)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(176, 21)
        Me.cmbUnit.TabIndex = 2
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.Location = New System.Drawing.Point(596, 188)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 23)
        Me.cmdBrowse.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdBrowse.TabIndex = 26
        Me.cmdBrowse.Text = "..."
        '
        'btnAddPath
        '
        Me.btnAddPath.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddPath.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddPath.Image = CType(resources.GetObject("btnAddPath.Image"), System.Drawing.Image)
        Me.btnAddPath.Location = New System.Drawing.Point(596, 217)
        Me.btnAddPath.Name = "btnAddPath"
        Me.btnAddPath.Size = New System.Drawing.Size(40, 23)
        Me.btnAddPath.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAddPath.TabIndex = 26
        '
        'chkHouseKeeping
        '
        '
        '
        '
        Me.chkHouseKeeping.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkHouseKeeping.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkHouseKeeping.Location = New System.Drawing.Point(11, 115)
        Me.chkHouseKeeping.Name = "chkHouseKeeping"
        Me.chkHouseKeeping.Size = New System.Drawing.Size(336, 24)
        Me.chkHouseKeeping.TabIndex = 26
        Me.chkHouseKeeping.Text = "Enable housekeeping for this destination"
        '
        'btnRemovePath
        '
        Me.btnRemovePath.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemovePath.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemovePath.Image = CType(resources.GetObject("btnRemovePath.Image"), System.Drawing.Image)
        Me.btnRemovePath.Location = New System.Drawing.Point(11, 217)
        Me.btnRemovePath.Name = "btnRemovePath"
        Me.btnRemovePath.Size = New System.Drawing.Size(40, 23)
        Me.btnRemovePath.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnRemovePath.TabIndex = 26
        '
        'chkDUN
        '
        '
        '
        '
        Me.chkDUN.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDUN.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDUN.Location = New System.Drawing.Point(10, 7)
        Me.chkDUN.Name = "chkDUN"
        Me.chkDUN.Size = New System.Drawing.Size(200, 24)
        Me.chkDUN.TabIndex = 27
        Me.chkDUN.Text = "Connect using dial-up networking"
        '
        'txtDirectory
        '
        Me.txtDirectory.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDirectory.Border.Class = "TextBoxBorder"
        Me.txtDirectory.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDirectory.ContextMenu = Me.mnuInserter
        Me.txtDirectory.ForeColor = System.Drawing.Color.Blue
        Me.txtDirectory.Location = New System.Drawing.Point(11, 190)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(579, 21)
        Me.txtDirectory.TabIndex = 0
        Me.txtDirectory.Tag = "memo"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.cmbDUN)
        Me.GroupBox7.Controls.Add(Me.Label21)
        Me.GroupBox7.Controls.Add(Me.cmdConnect)
        Me.GroupBox7.Enabled = False
        Me.GroupBox7.Location = New System.Drawing.Point(14, 35)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(427, 50)
        Me.GroupBox7.TabIndex = 28
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Dial-Up Networking Setup"
        '
        'cmbDUN
        '
        Me.cmbDUN.ItemHeight = 13
        Me.cmbDUN.Location = New System.Drawing.Point(110, 22)
        Me.cmbDUN.Name = "cmbDUN"
        Me.cmbDUN.Size = New System.Drawing.Size(178, 21)
        Me.cmbDUN.TabIndex = 1
        '
        'Label21
        '
        '
        '
        '
        Me.Label21.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 24)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(96, 16)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Connection Name"
        '
        'cmdConnect
        '
        Me.cmdConnect.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdConnect.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdConnect.Image = CType(resources.GetObject("cmdConnect.Image"), System.Drawing.Image)
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(312, 22)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 21)
        Me.cmdConnect.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdConnect.TabIndex = 1
        Me.cmdConnect.Text = "Connect"
        '
        'Label15
        '
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.Enabled = False
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(11, 171)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(57, 16)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "File Path"
        '
        'cmdInsert2
        '
        Me.cmdInsert2.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdInsert2.Image = CType(resources.GetObject("cmdInsert2.Image"), System.Drawing.Image)
        Me.cmdInsert2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert2.Location = New System.Drawing.Point(96, 794)
        Me.cmdInsert2.Name = "cmdInsert2"
        Me.cmdInsert2.Size = New System.Drawing.Size(24, 21)
        Me.cmdInsert2.TabIndex = 24
        Me.cmdInsert2.Visible = False
        '
        'grpFTP
        '
        Me.grpFTP.BackColor = System.Drawing.Color.Transparent
        Me.grpFTP.Controls.Add(Me.UcFTP)
        Me.grpFTP.Controls.Add(Me.btnRemoveFTP)
        Me.grpFTP.Controls.Add(Me.btnAddFTP)
        Me.grpFTP.Controls.Add(Me.lsvFTP)
        Me.grpFTP.Location = New System.Drawing.Point(4, 3)
        Me.grpFTP.Name = "grpFTP"
        Me.grpFTP.Size = New System.Drawing.Size(649, 554)
        Me.grpFTP.TabIndex = 11
        Me.grpFTP.Visible = False
        '
        'btnRemoveFTP
        '
        Me.btnRemoveFTP.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemoveFTP.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemoveFTP.Image = CType(resources.GetObject("btnRemoveFTP.Image"), System.Drawing.Image)
        Me.btnRemoveFTP.Location = New System.Drawing.Point(10, 218)
        Me.btnRemoveFTP.Name = "btnRemoveFTP"
        Me.btnRemoveFTP.Size = New System.Drawing.Size(40, 23)
        Me.btnRemoveFTP.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnRemoveFTP.TabIndex = 28
        '
        'btnAddFTP
        '
        Me.btnAddFTP.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAddFTP.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAddFTP.Image = CType(resources.GetObject("btnAddFTP.Image"), System.Drawing.Image)
        Me.btnAddFTP.Location = New System.Drawing.Point(585, 218)
        Me.btnAddFTP.Name = "btnAddFTP"
        Me.btnAddFTP.Size = New System.Drawing.Size(40, 23)
        Me.btnAddFTP.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAddFTP.TabIndex = 29
        '
        'lsvFTP
        '
        '
        '
        '
        Me.lsvFTP.Border.Class = "ListViewBorder"
        Me.lsvFTP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFTP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9})
        Me.lsvFTP.FullRowSelect = True
        Me.lsvFTP.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvFTP.Location = New System.Drawing.Point(10, 244)
        Me.lsvFTP.Name = "lsvFTP"
        Me.lsvFTP.Size = New System.Drawing.Size(616, 302)
        Me.lsvFTP.TabIndex = 27
        Me.lsvFTP.UseCompatibleStateImageBehavior = False
        Me.lsvFTP.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Server"
        Me.ColumnHeader2.Width = 100
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "User Name"
        Me.ColumnHeader5.Width = 78
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Password"
        Me.ColumnHeader6.Width = 100
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Directory"
        Me.ColumnHeader7.Width = 197
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Type"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Passive"
        '
        'Label9
        '
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Enabled = False
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(290, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(102, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Type"
        '
        'cmbDestination
        '
        Me.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestination.ForeColor = System.Drawing.Color.Blue
        Me.cmbDestination.ItemHeight = 13
        Me.cmbDestination.Location = New System.Drawing.Point(323, 10)
        Me.cmbDestination.Name = "cmbDestination"
        Me.cmbDestination.Size = New System.Drawing.Size(160, 21)
        Me.cmbDestination.TabIndex = 0
        '
        'stabDestination
        '
        Me.stabDestination.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        '
        '
        '
        Me.stabDestination.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabDestination.ControlBox.MenuBox.Name = ""
        Me.stabDestination.ControlBox.Name = ""
        Me.stabDestination.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabDestination.ControlBox.MenuBox, Me.stabDestination.ControlBox.CloseBox})
        Me.stabDestination.Controls.Add(Me.stabSetupPanel)
        Me.stabDestination.Controls.Add(Me.stabFormatPanel)
        Me.stabDestination.Controls.Add(Me.stabMiscPanel)
        Me.stabDestination.Controls.Add(Me.stabNamingPanel)
        Me.stabDestination.Controls.Add(Me.stabPGPPanel)
        Me.stabDestination.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabDestination.Location = New System.Drawing.Point(0, 43)
        Me.stabDestination.Name = "stabDestination"
        Me.stabDestination.ReorderTabsEnabled = True
        Me.stabDestination.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabDestination.SelectedTabIndex = 0
        Me.stabDestination.ShowFocusRectangle = True
        Me.stabDestination.Size = New System.Drawing.Size(660, 594)
        Me.stabDestination.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabDestination.TabIndex = 28
        Me.stabDestination.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabSetup, Me.stabFormat, Me.stabNaming, Me.stabCompression, Me.stabPGP})
        Me.stabDestination.Text = "SuperTabControl1"
        '
        'stabSetupPanel
        '
        Me.stabSetupPanel.CanvasColor = System.Drawing.Color.Transparent
        Me.stabSetupPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.stabSetupPanel.Controls.Add(Me.grpCloud)
        Me.stabSetupPanel.Controls.Add(Me.grpNothing)
        Me.stabSetupPanel.Controls.Add(Me.grpFile)
        Me.stabSetupPanel.Controls.Add(Me.grpEmail)
        Me.stabSetupPanel.Controls.Add(Me.grpFTP)
        Me.stabSetupPanel.Controls.Add(Me.grpSharePoint)
        Me.stabSetupPanel.Controls.Add(Me.grpSMS)
        Me.stabSetupPanel.Controls.Add(Me.grpPrinter)
        Me.stabSetupPanel.Controls.Add(Me.grpODBC)
        Me.stabSetupPanel.Controls.Add(Me.grpFax)
        Me.stabSetupPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabSetupPanel.Location = New System.Drawing.Point(0, 26)
        Me.stabSetupPanel.Name = "stabSetupPanel"
        Me.stabSetupPanel.Size = New System.Drawing.Size(660, 568)
        Me.stabSetupPanel.TabIndex = 1
        Me.stabSetupPanel.TabItem = Me.stabSetup
        '
        'grpNothing
        '
        Me.grpNothing.Controls.Add(Me.UcMessageLabel1)
        Me.grpNothing.Location = New System.Drawing.Point(0, 0)
        Me.grpNothing.Name = "grpNothing"
        Me.grpNothing.Size = New System.Drawing.Size(640, 562)
        Me.grpNothing.TabIndex = 29
        '
        'grpCloud
        '
        Me.grpCloud.Controls.Add(Me.btnBrowseCloud)
        Me.grpCloud.Controls.Add(Me.txtCloudFolder)
        Me.grpCloud.Controls.Add(Me.cmbCloundAccount)
        Me.grpCloud.Controls.Add(Me.LabelX2)
        Me.grpCloud.Controls.Add(Me.LabelX1)
        Me.grpCloud.Location = New System.Drawing.Point(0, 0)
        Me.grpCloud.Name = "grpCloud"
        Me.grpCloud.Size = New System.Drawing.Size(626, 540)
        Me.grpCloud.TabIndex = 3
        Me.grpCloud.Visible = False
        '
        'btnBrowseCloud
        '
        Me.btnBrowseCloud.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBrowseCloud.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBrowseCloud.Location = New System.Drawing.Point(487, 52)
        Me.btnBrowseCloud.Name = "btnBrowseCloud"
        Me.btnBrowseCloud.Size = New System.Drawing.Size(39, 20)
        Me.btnBrowseCloud.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnBrowseCloud.TabIndex = 6
        Me.btnBrowseCloud.Text = "..."
        '
        'txtCloudFolder
        '
        '
        '
        '
        Me.txtCloudFolder.Border.Class = "TextBoxBorder"
        Me.txtCloudFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCloudFolder.Location = New System.Drawing.Point(96, 52)
        Me.txtCloudFolder.Name = "txtCloudFolder"
        Me.txtCloudFolder.Size = New System.Drawing.Size(377, 21)
        Me.txtCloudFolder.TabIndex = 5
        '
        'cmbCloundAccount
        '
        Me.cmbCloundAccount.FormattingEnabled = True
        Me.cmbCloundAccount.Location = New System.Drawing.Point(97, 21)
        Me.cmbCloundAccount.Name = "cmbCloundAccount"
        Me.cmbCloundAccount.Size = New System.Drawing.Size(376, 21)
        Me.cmbCloundAccount.TabIndex = 4
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(16, 50)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(75, 23)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Folder"
        '
        'LabelX1
        '
        '
        '
        '
        Me.LabelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX1.Location = New System.Drawing.Point(16, 21)
        Me.LabelX1.Name = "LabelX1"
        Me.LabelX1.Size = New System.Drawing.Size(75, 23)
        Me.LabelX1.TabIndex = 0
        Me.LabelX1.Text = "Account Name"
        '
        'grpSharePoint
        '
        Me.grpSharePoint.BackColor = System.Drawing.Color.Transparent
        Me.grpSharePoint.Controls.Add(Me.btnEdit)
        Me.grpSharePoint.Controls.Add(Me.btnRemove)
        Me.grpSharePoint.Controls.Add(Me.btnAdd)
        Me.grpSharePoint.Controls.Add(Me.lsvSharePointLibs)
        Me.grpSharePoint.Location = New System.Drawing.Point(4, 3)
        Me.grpSharePoint.Name = "grpSharePoint"
        Me.grpSharePoint.Size = New System.Drawing.Size(649, 554)
        Me.grpSharePoint.TabIndex = 28
        Me.grpSharePoint.TabStop = False
        Me.grpSharePoint.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnEdit.Location = New System.Drawing.Point(487, 43)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(84, 23)
        Me.btnEdit.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "Edit"
        '
        'btnRemove
        '
        Me.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemove.Location = New System.Drawing.Point(487, 73)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(84, 23)
        Me.btnRemove.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Remove"
        '
        'btnAdd
        '
        Me.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAdd.Location = New System.Drawing.Point(487, 13)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(84, 23)
        Me.btnAdd.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        '
        'lsvSharePointLibs
        '
        '
        '
        '
        Me.lsvSharePointLibs.Border.Class = "ListViewBorder"
        Me.lsvSharePointLibs.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSharePointLibs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12})
        Me.lsvSharePointLibs.FullRowSelect = True
        Me.lsvSharePointLibs.Location = New System.Drawing.Point(6, 13)
        Me.lsvSharePointLibs.Name = "lsvSharePointLibs"
        Me.lsvSharePointLibs.Size = New System.Drawing.Size(475, 535)
        Me.lsvSharePointLibs.TabIndex = 3
        Me.lsvSharePointLibs.UseCompatibleStateImageBehavior = False
        Me.lsvSharePointLibs.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Server"
        Me.ColumnHeader10.Width = 288
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Library"
        Me.ColumnHeader11.Width = 90
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "User Name"
        Me.ColumnHeader12.Width = 74
        '
        'grpSMS
        '
        Me.grpSMS.BackColor = System.Drawing.Color.Transparent
        Me.grpSMS.Controls.Add(Me.Label23)
        Me.grpSMS.Controls.Add(Me.txtSMSMsg)
        Me.grpSMS.Controls.Add(Me.txtSMSNumber)
        Me.grpSMS.Controls.Add(Me.cmdSMSTo)
        Me.grpSMS.Location = New System.Drawing.Point(4, 3)
        Me.grpSMS.Name = "grpSMS"
        Me.grpSMS.Size = New System.Drawing.Size(649, 554)
        Me.grpSMS.TabIndex = 27
        Me.grpSMS.Visible = False
        '
        'Label23
        '
        '
        '
        '
        Me.Label23.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(5, 472)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(168, 16)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Characters Left: "
        '
        'txtSMSMsg
        '
        '
        '
        '
        Me.txtSMSMsg.Border.Class = "TextBoxBorder"
        Me.txtSMSMsg.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSMsg.ContextMenu = Me.mnuInserter
        Me.txtSMSMsg.Location = New System.Drawing.Point(8, 56)
        Me.txtSMSMsg.MaxLength = 160
        Me.txtSMSMsg.Multiline = True
        Me.txtSMSMsg.Name = "txtSMSMsg"
        Me.txtSMSMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSMSMsg.Size = New System.Drawing.Size(439, 374)
        Me.txtSMSMsg.TabIndex = 4
        Me.txtSMSMsg.Tag = "memo"
        '
        'txtSMSNumber
        '
        '
        '
        '
        Me.txtSMSNumber.Border.Class = "TextBoxBorder"
        Me.txtSMSNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMSNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtSMSNumber.Location = New System.Drawing.Point(88, 24)
        Me.txtSMSNumber.Name = "txtSMSNumber"
        Me.txtSMSNumber.Size = New System.Drawing.Size(362, 21)
        Me.txtSMSNumber.TabIndex = 3
        Me.txtSMSNumber.Tag = "memo"
        '
        'cmdSMSTo
        '
        Me.cmdSMSTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSMSTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSMSTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSMSTo.Image = CType(resources.GetObject("cmdSMSTo.Image"), System.Drawing.Image)
        Me.cmdSMSTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSMSTo.Location = New System.Drawing.Point(8, 24)
        Me.cmdSMSTo.Name = "cmdSMSTo"
        Me.cmdSMSTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdSMSTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdSMSTo.TabIndex = 2
        Me.cmdSMSTo.Text = "To..."
        '
        'grpODBC
        '
        Me.grpODBC.BackColor = System.Drawing.Color.Transparent
        Me.grpODBC.Controls.Add(Me.chkOverwrite)
        Me.grpODBC.Controls.Add(Me.txtTableName)
        Me.grpODBC.Controls.Add(Me.Label24)
        Me.grpODBC.Controls.Add(Me.btnODBC)
        Me.grpODBC.Controls.Add(Me.UcDSN)
        Me.grpODBC.Location = New System.Drawing.Point(4, 3)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(649, 554)
        Me.grpODBC.TabIndex = 27
        Me.grpODBC.Visible = False
        '
        'chkOverwrite
        '
        Me.chkOverwrite.AutoSize = True
        '
        '
        '
        Me.chkOverwrite.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOverwrite.Location = New System.Drawing.Point(79, 203)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(137, 16)
        Me.chkOverwrite.TabIndex = 3
        Me.chkOverwrite.Text = "Overwrite if table exists"
        '
        'txtTableName
        '
        '
        '
        '
        Me.txtTableName.Border.Class = "TextBoxBorder"
        Me.txtTableName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTableName.Enabled = False
        Me.txtTableName.Location = New System.Drawing.Point(80, 176)
        Me.txtTableName.Name = "txtTableName"
        Me.txtTableName.Size = New System.Drawing.Size(399, 21)
        Me.txtTableName.TabIndex = 0
        '
        'Label24
        '
        '
        '
        '
        Me.Label24.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(8, 178)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(64, 16)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Table Name"
        '
        'btnODBC
        '
        Me.btnODBC.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnODBC.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnODBC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnODBC.Location = New System.Drawing.Point(247, 131)
        Me.btnODBC.Name = "btnODBC"
        Me.btnODBC.Size = New System.Drawing.Size(75, 23)
        Me.btnODBC.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnODBC.TabIndex = 1
        Me.btnODBC.Text = "Connect..."
        '
        'grpFax
        '
        Me.grpFax.BackColor = System.Drawing.Color.Transparent
        Me.grpFax.Controls.Add(Me.GroupBox6)
        Me.grpFax.Controls.Add(Me.Label6)
        Me.grpFax.Controls.Add(Me.cmbFaxDevice)
        Me.grpFax.Controls.Add(Me.txtFaxNumber)
        Me.grpFax.Controls.Add(Me.cmdFaxTo)
        Me.grpFax.Location = New System.Drawing.Point(4, 3)
        Me.grpFax.Name = "grpFax"
        Me.grpFax.Size = New System.Drawing.Size(649, 554)
        Me.grpFax.TabIndex = 13
        Me.grpFax.Visible = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtFaxTo)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.txtFaxFrom)
        Me.GroupBox6.Controls.Add(Me.Label8)
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.txtFaxComments)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 88)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(457, 458)
        Me.GroupBox6.TabIndex = 4
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Cover Page"
        '
        'txtFaxTo
        '
        '
        '
        '
        Me.txtFaxTo.Border.Class = "TextBoxBorder"
        Me.txtFaxTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFaxTo.ContextMenu = Me.mnuInserter
        Me.txtFaxTo.Location = New System.Drawing.Point(88, 24)
        Me.txtFaxTo.Name = "txtFaxTo"
        Me.txtFaxTo.Size = New System.Drawing.Size(360, 21)
        Me.txtFaxTo.TabIndex = 1
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "To"
        '
        'txtFaxFrom
        '
        '
        '
        '
        Me.txtFaxFrom.Border.Class = "TextBoxBorder"
        Me.txtFaxFrom.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFaxFrom.ContextMenu = Me.mnuInserter
        Me.txtFaxFrom.Location = New System.Drawing.Point(88, 56)
        Me.txtFaxFrom.Name = "txtFaxFrom"
        Me.txtFaxFrom.Size = New System.Drawing.Size(360, 21)
        Me.txtFaxFrom.TabIndex = 1
        '
        'Label8
        '
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 58)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "From"
        '
        'Label10
        '
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Comments"
        '
        'txtFaxComments
        '
        '
        '
        '
        Me.txtFaxComments.Border.Class = "TextBoxBorder"
        Me.txtFaxComments.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFaxComments.ContextMenu = Me.mnuInserter
        Me.txtFaxComments.Location = New System.Drawing.Point(8, 104)
        Me.txtFaxComments.Multiline = True
        Me.txtFaxComments.Name = "txtFaxComments"
        Me.txtFaxComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFaxComments.Size = New System.Drawing.Size(440, 347)
        Me.txtFaxComments.TabIndex = 1
        Me.txtFaxComments.Tag = "Memo"
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Fax Device"
        '
        'cmbFaxDevice
        '
        Me.cmbFaxDevice.ItemHeight = 13
        Me.cmbFaxDevice.Location = New System.Drawing.Point(96, 56)
        Me.cmbFaxDevice.Name = "cmbFaxDevice"
        Me.cmbFaxDevice.Size = New System.Drawing.Size(360, 21)
        Me.cmbFaxDevice.TabIndex = 2
        '
        'txtFaxNumber
        '
        '
        '
        '
        Me.txtFaxNumber.Border.Class = "TextBoxBorder"
        Me.txtFaxNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFaxNumber.Location = New System.Drawing.Point(96, 24)
        Me.txtFaxNumber.Name = "txtFaxNumber"
        Me.txtFaxNumber.Size = New System.Drawing.Size(360, 21)
        Me.txtFaxNumber.TabIndex = 1
        '
        'cmdFaxTo
        '
        Me.cmdFaxTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFaxTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFaxTo.Image = CType(resources.GetObject("cmdFaxTo.Image"), System.Drawing.Image)
        Me.cmdFaxTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFaxTo.Location = New System.Drawing.Point(8, 24)
        Me.cmdFaxTo.Name = "cmdFaxTo"
        Me.cmdFaxTo.Size = New System.Drawing.Size(75, 23)
        Me.cmdFaxTo.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdFaxTo.TabIndex = 0
        Me.cmdFaxTo.Text = "Fax No."
        '
        'stabSetup
        '
        Me.stabSetup.AttachedControl = Me.stabSetupPanel
        Me.stabSetup.GlobalItem = False
        Me.stabSetup.Name = "stabSetup"
        Me.stabSetup.Text = "Setup"
        '
        'stabFormatPanel
        '
        Me.stabFormatPanel.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Windows7
        Me.stabFormatPanel.Controls.Add(Me.ucRptOptions)
        Me.stabFormatPanel.Controls.Add(Me.cmdProps)
        Me.stabFormatPanel.Controls.Add(Me.cmbFormat)
        Me.stabFormatPanel.Controls.Add(Me.Label22)
        Me.stabFormatPanel.Controls.Add(Me.FlowLayoutPanel1)
        Me.stabFormatPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabFormatPanel.Location = New System.Drawing.Point(0, 0)
        Me.stabFormatPanel.Name = "stabFormatPanel"
        Me.stabFormatPanel.Size = New System.Drawing.Size(665, 594)
        Me.stabFormatPanel.TabIndex = 0
        Me.stabFormatPanel.TabItem = Me.stabFormat
        '
        'cmdProps
        '
        Me.cmdProps.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdProps.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdProps.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdProps.Location = New System.Drawing.Point(303, 472)
        Me.cmdProps.Name = "cmdProps"
        Me.cmdProps.Size = New System.Drawing.Size(32, 21)
        Me.cmdProps.TabIndex = 14
        Me.cmdProps.Text = "..."
        Me.cmdProps.Visible = False
        '
        'cmbFormat
        '
        Me.cmbFormat.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmbFormat.ForeColor = System.Drawing.Color.Blue
        Me.cmbFormat.ItemHeight = 13
        Me.cmbFormat.Items.AddRange(New Object() {"Acrobat Format (*.pdf)", "Crystal Reports (*.rpt)", "CSV (*.csv)", "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", "MS Excel 97-2000 (*.xls)", "MS Word (*.doc)", "ODBC (*.odbc)", "Rich Text Format (*.rtf)", "Tab Separated (*.txt)", "Text (*.txt)", "TIFF (*.tif)", "XML (*.xml)"})
        Me.cmbFormat.Location = New System.Drawing.Point(95, 472)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.Size = New System.Drawing.Size(184, 21)
        Me.cmbFormat.Sorted = True
        Me.cmbFormat.TabIndex = 9
        Me.cmbFormat.Visible = False
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label22.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label22.ForeColor = System.Drawing.Color.Navy
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(47, 474)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 16)
        Me.Label22.TabIndex = 8
        Me.Label22.Text = "Format"
        Me.Label22.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.chkIncludeAttach)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 569)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(665, 25)
        Me.FlowLayoutPanel1.TabIndex = 16
        '
        'chkIncludeAttach
        '
        Me.chkIncludeAttach.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkIncludeAttach.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkIncludeAttach.Checked = True
        Me.chkIncludeAttach.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeAttach.CheckValue = "Y"
        Me.chkIncludeAttach.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkIncludeAttach.ForeColor = System.Drawing.Color.Navy
        Me.chkIncludeAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkIncludeAttach.Location = New System.Drawing.Point(3, 3)
        Me.chkIncludeAttach.Name = "chkIncludeAttach"
        Me.chkIncludeAttach.Size = New System.Drawing.Size(152, 16)
        Me.chkIncludeAttach.TabIndex = 11
        Me.chkIncludeAttach.Text = "Include attachment"
        Me.chkIncludeAttach.Visible = False
        '
        'stabFormat
        '
        Me.stabFormat.AttachedControl = Me.stabFormatPanel
        Me.stabFormat.GlobalItem = False
        Me.stabFormat.Name = "stabFormat"
        Me.stabFormat.Text = "Format"
        '
        'stabMiscPanel
        '
        Me.stabMiscPanel.Controls.Add(Me.txtDefer)
        Me.stabMiscPanel.Controls.Add(Me.grpZip)
        Me.stabMiscPanel.Controls.Add(Me.Label3)
        Me.stabMiscPanel.Controls.Add(Me.chkZip)
        Me.stabMiscPanel.Controls.Add(Me.chkDefer)
        Me.stabMiscPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabMiscPanel.Location = New System.Drawing.Point(0, 0)
        Me.stabMiscPanel.Name = "stabMiscPanel"
        Me.stabMiscPanel.Size = New System.Drawing.Size(665, 594)
        Me.stabMiscPanel.TabIndex = 0
        Me.stabMiscPanel.TabItem = Me.stabCompression
        '
        'txtDefer
        '
        Me.txtDefer.DecimalPlaces = 1
        Me.txtDefer.Enabled = False
        Me.txtDefer.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtDefer.Location = New System.Drawing.Point(138, 205)
        Me.txtDefer.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtDefer.Name = "txtDefer"
        Me.txtDefer.Size = New System.Drawing.Size(64, 21)
        Me.txtDefer.TabIndex = 1
        Me.txtDefer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpZip
        '
        Me.grpZip.BackColor = System.Drawing.Color.Transparent
        Me.grpZip.Controls.Add(Me.Panel1)
        Me.grpZip.Controls.Add(Me.chkZipSecurity)
        Me.grpZip.Enabled = False
        Me.grpZip.Location = New System.Drawing.Point(18, 36)
        Me.grpZip.Name = "grpZip"
        Me.grpZip.Size = New System.Drawing.Size(360, 160)
        Me.grpZip.TabIndex = 15
        Me.grpZip.TabStop = False
        Me.grpZip.Text = "Zip File Encryption"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtZipCode)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.cmbEncryptionLevel)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtConfirmCode)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(8, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(344, 104)
        Me.Panel1.TabIndex = 2
        '
        'txtZipCode
        '
        '
        '
        '
        Me.txtZipCode.Border.Class = "TextBoxBorder"
        Me.txtZipCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtZipCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtZipCode.Location = New System.Drawing.Point(112, 40)
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtZipCode.Size = New System.Drawing.Size(160, 21)
        Me.txtZipCode.TabIndex = 4
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Password"
        '
        'cmbEncryptionLevel
        '
        Me.cmbEncryptionLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEncryptionLevel.ItemHeight = 13
        Me.cmbEncryptionLevel.Items.AddRange(New Object() {"128", "192", "256"})
        Me.cmbEncryptionLevel.Location = New System.Drawing.Point(112, 8)
        Me.cmbEncryptionLevel.Name = "cmbEncryptionLevel"
        Me.cmbEncryptionLevel.Size = New System.Drawing.Size(160, 21)
        Me.cmbEncryptionLevel.TabIndex = 2
        '
        'Label11
        '
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Encryption Level"
        '
        'Label13
        '
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 74)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Confirm Password"
        '
        'txtConfirmCode
        '
        '
        '
        '
        Me.txtConfirmCode.Border.Class = "TextBoxBorder"
        Me.txtConfirmCode.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtConfirmCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtConfirmCode.Location = New System.Drawing.Point(112, 72)
        Me.txtConfirmCode.Name = "txtConfirmCode"
        Me.txtConfirmCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtConfirmCode.Size = New System.Drawing.Size(160, 21)
        Me.txtConfirmCode.TabIndex = 4
        '
        'chkZipSecurity
        '
        '
        '
        '
        Me.chkZipSecurity.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkZipSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkZipSecurity.Location = New System.Drawing.Point(8, 24)
        Me.chkZipSecurity.Name = "chkZipSecurity"
        Me.chkZipSecurity.Size = New System.Drawing.Size(320, 24)
        Me.chkZipSecurity.TabIndex = 0
        Me.chkZipSecurity.Text = "Enable zip encryption"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(208, 210)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Hours"
        '
        'chkZip
        '
        Me.chkZip.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkZip.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkZip.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkZip.ForeColor = System.Drawing.Color.Navy
        Me.chkZip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkZip.Location = New System.Drawing.Point(18, 14)
        Me.chkZip.Name = "chkZip"
        Me.chkZip.Size = New System.Drawing.Size(152, 16)
        Me.chkZip.TabIndex = 13
        Me.chkZip.Text = "Compress (ZIP) output"
        '
        'chkDefer
        '
        Me.chkDefer.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkDefer.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDefer.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDefer.Location = New System.Drawing.Point(18, 202)
        Me.chkDefer.Name = "chkDefer"
        Me.chkDefer.Size = New System.Drawing.Size(160, 24)
        Me.chkDefer.TabIndex = 0
        Me.chkDefer.Text = "Defer delivery by"
        '
        'stabCompression
        '
        Me.stabCompression.AttachedControl = Me.stabMiscPanel
        Me.stabCompression.GlobalItem = False
        Me.stabCompression.Name = "stabCompression"
        Me.stabCompression.Text = "Misc"
        '
        'stabNamingPanel
        '
        Me.stabNamingPanel.Controls.Add(Me.GroupBox3)
        Me.stabNamingPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabNamingPanel.Location = New System.Drawing.Point(0, 0)
        Me.stabNamingPanel.Name = "stabNamingPanel"
        Me.stabNamingPanel.Size = New System.Drawing.Size(665, 594)
        Me.stabNamingPanel.TabIndex = 0
        Me.stabNamingPanel.TabItem = Me.stabNaming
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.chkKeyParameter)
        Me.GroupBox3.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.cmbDateTime)
        Me.GroupBox3.Controls.Add(Me.chkCustomExt)
        Me.GroupBox3.Controls.Add(Me.txtCustomName)
        Me.GroupBox3.Controls.Add(Me.optNaming)
        Me.GroupBox3.Controls.Add(Me.optCustomName)
        Me.GroupBox3.Controls.Add(Me.txtCustomExt)
        Me.GroupBox3.Controls.Add(Me.chkAppendDateTime)
        Me.GroupBox3.Controls.Add(Me.txtDefault)
        Me.GroupBox3.Location = New System.Drawing.Point(9, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(646, 554)
        Me.GroupBox3.TabIndex = 17
        Me.GroupBox3.TabStop = False
        '
        'chkKeyParameter
        '
        Me.chkKeyParameter.AutoSize = True
        '
        '
        '
        Me.chkKeyParameter.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkKeyParameter.Enabled = False
        Me.chkKeyParameter.Location = New System.Drawing.Point(200, 82)
        Me.chkKeyParameter.Name = "chkKeyParameter"
        Me.chkKeyParameter.Size = New System.Drawing.Size(126, 16)
        Me.chkKeyParameter.TabIndex = 19
        Me.chkKeyParameter.Text = "Insert Key Parameter"
        Me.chkKeyParameter.Visible = False
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(200, 181)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 18
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        '
        '
        '
        Me.Label26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label26.Location = New System.Drawing.Point(5, 185)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(165, 16)
        Me.Label26.TabIndex = 17
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM", "yyyy-MM-dd", "hh:mm:ss"})
        Me.cmbDateTime.Location = New System.Drawing.Point(200, 147)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(366, 21)
        Me.cmbDateTime.TabIndex = 16
        '
        'chkCustomExt
        '
        '
        '
        '
        Me.chkCustomExt.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkCustomExt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCustomExt.Location = New System.Drawing.Point(8, 105)
        Me.chkCustomExt.Name = "chkCustomExt"
        Me.chkCustomExt.Size = New System.Drawing.Size(184, 24)
        Me.chkCustomExt.TabIndex = 15
        Me.chkCustomExt.Text = "Customize output extension"
        '
        'txtCustomName
        '
        '
        '
        '
        Me.txtCustomName.Border.Class = "TextBoxBorder"
        Me.txtCustomName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomName.ContextMenu = Me.mnuInserter
        Me.txtCustomName.Enabled = False
        Me.txtCustomName.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomName.Location = New System.Drawing.Point(200, 56)
        Me.txtCustomName.Name = "txtCustomName"
        Me.txtCustomName.Size = New System.Drawing.Size(366, 21)
        Me.txtCustomName.TabIndex = 13
        Me.txtCustomName.Tag = "memo"
        '
        'optNaming
        '
        Me.optNaming.Checked = True
        Me.optNaming.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNaming.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNaming.Location = New System.Drawing.Point(8, 16)
        Me.optNaming.Name = "optNaming"
        Me.optNaming.Size = New System.Drawing.Size(192, 24)
        Me.optNaming.TabIndex = 10
        Me.optNaming.TabStop = True
        Me.optNaming.Text = "Use the default naming convention"
        '
        'optCustomName
        '
        Me.optCustomName.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optCustomName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustomName.Location = New System.Drawing.Point(8, 56)
        Me.optCustomName.Name = "optCustomName"
        Me.optCustomName.Size = New System.Drawing.Size(184, 24)
        Me.optCustomName.TabIndex = 11
        Me.optCustomName.Text = "Customize the output file name"
        '
        'txtCustomExt
        '
        '
        '
        '
        Me.txtCustomExt.Border.Class = "TextBoxBorder"
        Me.txtCustomExt.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomExt.ContextMenu = Me.mnuInserter
        Me.txtCustomExt.Enabled = False
        Me.txtCustomExt.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomExt.Location = New System.Drawing.Point(200, 105)
        Me.txtCustomExt.Name = "txtCustomExt"
        Me.txtCustomExt.Size = New System.Drawing.Size(366, 21)
        Me.txtCustomExt.TabIndex = 12
        '
        'chkAppendDateTime
        '
        '
        '
        '
        Me.chkAppendDateTime.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAppendDateTime.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAppendDateTime.Location = New System.Drawing.Point(8, 145)
        Me.chkAppendDateTime.Name = "chkAppendDateTime"
        Me.chkAppendDateTime.Size = New System.Drawing.Size(192, 24)
        Me.chkAppendDateTime.TabIndex = 14
        Me.chkAppendDateTime.Text = "Append date/time to report output"
        '
        'txtDefault
        '
        '
        '
        '
        Me.txtDefault.Border.Class = "TextBoxBorder"
        Me.txtDefault.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDefault.ContextMenu = Me.mnuInserter
        Me.txtDefault.ForeColor = System.Drawing.Color.Blue
        Me.txtDefault.Location = New System.Drawing.Point(200, 18)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.ReadOnly = True
        Me.txtDefault.Size = New System.Drawing.Size(366, 21)
        Me.txtDefault.TabIndex = 13
        '
        'stabNaming
        '
        Me.stabNaming.AttachedControl = Me.stabNamingPanel
        Me.stabNaming.GlobalItem = False
        Me.stabNaming.Name = "stabNaming"
        Me.stabNaming.Text = "Naming"
        '
        'stabPGPPanel
        '
        Me.stabPGPPanel.Controls.Add(Me.UcPGP)
        Me.stabPGPPanel.Controls.Add(Me.chkPGP)
        Me.stabPGPPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabPGPPanel.Location = New System.Drawing.Point(0, 0)
        Me.stabPGPPanel.Name = "stabPGPPanel"
        Me.stabPGPPanel.Size = New System.Drawing.Size(665, 594)
        Me.stabPGPPanel.TabIndex = 0
        Me.stabPGPPanel.TabItem = Me.stabPGP
        '
        'chkPGP
        '
        Me.chkPGP.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.chkPGP.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPGP.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPGP.Location = New System.Drawing.Point(18, 14)
        Me.chkPGP.Name = "chkPGP"
        Me.chkPGP.Size = New System.Drawing.Size(344, 24)
        Me.chkPGP.TabIndex = 0
        Me.chkPGP.Text = "Enable PGP Encryption"
        '
        'stabPGP
        '
        Me.stabPGP.AttachedControl = Me.stabPGPPanel
        Me.stabPGP.GlobalItem = False
        Me.stabPGP.Name = "stabPGP"
        Me.stabPGP.Text = "PGP"
        '
        'chkSplit
        '
        Me.chkSplit.BackColor = System.Drawing.SystemColors.Control
        '
        '
        '
        Me.chkSplit.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSplit.Enabled = False
        Me.chkSplit.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkSplit.ForeColor = System.Drawing.Color.Navy
        Me.chkSplit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSplit.Location = New System.Drawing.Point(406, 794)
        Me.chkSplit.Name = "chkSplit"
        Me.chkSplit.Size = New System.Drawing.Size(16, 16)
        Me.chkSplit.TabIndex = 10
        Me.chkSplit.Text = "Split into seperate HTML files"
        Me.chkSplit.Visible = False
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(500, 5)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(580, 5)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(121, 9)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(160, 21)
        Me.txtName.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(10, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Destination Name"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem8, Me.MenuItem11, Me.MenuItem9, Me.MenuItem10, Me.MenuItem12})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.Text = "Default Subject"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 5
        Me.MenuItem9.Text = "Default Message"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "Default Signature"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 7
        Me.MenuItem12.Text = "Default Attachment"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'superTip
        '
        Me.superTip.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.superTip.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.superTip.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'mnuEditSharepoint
        '
        Me.mnuEditSharepoint.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SimpleToolStripMenuItem, Me.AdvancedToolStripMenuItem})
        Me.mnuEditSharepoint.Name = "mnuEditSharepoint"
        Me.mnuEditSharepoint.Size = New System.Drawing.Size(128, 48)
        '
        'SimpleToolStripMenuItem
        '
        Me.SimpleToolStripMenuItem.Name = "SimpleToolStripMenuItem"
        Me.SimpleToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.SimpleToolStripMenuItem.Text = "Simple"
        '
        'AdvancedToolStripMenuItem
        '
        Me.AdvancedToolStripMenuItem.Name = "AdvancedToolStripMenuItem"
        Me.AdvancedToolStripMenuItem.Size = New System.Drawing.Size(127, 22)
        Me.AdvancedToolStripMenuItem.Text = "Advanced"
        '
        'btnPicker
        '
        Me.btnPicker.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPicker.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPicker.Location = New System.Drawing.Point(4, 5)
        Me.btnPicker.Name = "btnPicker"
        Me.btnPicker.Size = New System.Drawing.Size(75, 23)
        Me.btnPicker.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPicker.TabIndex = 28
        Me.btnPicker.Text = "@@"
        Me.btnPicker.Visible = False
        '
        'chkStatus
        '
        '
        '
        '
        Me.chkStatus.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.CheckValue = "Y"
        Me.chkStatus.Location = New System.Drawing.Point(12, 513)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(100, 23)
        Me.chkStatus.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.chkStatus.TabIndex = 29
        Me.chkStatus.Text = "Enabled"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Controls.Add(Me.txtName)
        Me.Panel2.Controls.Add(Me.cmbDestination)
        Me.Panel2.Controls.Add(Me.Label9)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(660, 43)
        Me.Panel2.TabIndex = 30
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.btnPicker)
        Me.Panel3.Controls.Add(Me.cmdOK)
        Me.Panel3.Controls.Add(Me.cmdCancel)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(0, 637)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(660, 33)
        Me.Panel3.TabIndex = 31
        '
        'UcMessageLabel1
        '
        Me.UcMessageLabel1.BackColor = System.Drawing.Color.Transparent
        Me.UcMessageLabel1.Location = New System.Drawing.Point(35, 134)
        Me.UcMessageLabel1.MessageToShow = resources.GetString("UcMessageLabel1.MessageToShow")
        Me.UcMessageLabel1.Name = "UcMessageLabel1"
        Me.UcMessageLabel1.Size = New System.Drawing.Size(425, 150)
        Me.UcMessageLabel1.TabIndex = 1
        '
        'txtMsg
        '
        Me.txtMsg.bodyFormat = sqlrd.ucEditorX.format.TEXT
        Me.txtMsg.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMsg.Location = New System.Drawing.Point(11, 159)
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.Size = New System.Drawing.Size(615, 198)
        Me.txtMsg.TabIndex = 30
        '
        'UcFTP
        '
        Me.UcFTP.BackColor = System.Drawing.Color.Transparent
        Me.UcFTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcFTP.Location = New System.Drawing.Point(3, 19)
        Me.UcFTP.m_browseFTPForFile = False
        Me.UcFTP.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
    "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;AsciiMode=0;RetryUpload=0;"
        Me.UcFTP.m_ftpSuccess = False
        Me.UcFTP.m_ShowBrowse = True
        Me.UcFTP.Name = "UcFTP"
        Me.UcFTP.Size = New System.Drawing.Size(623, 193)
        Me.UcFTP.TabIndex = 30
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(11, 21)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(483, 112)
        Me.UcDSN.TabIndex = 0
        '
        'ucRptOptions
        '
        Me.ucRptOptions.BackColor = System.Drawing.Color.Transparent
        Me.ucRptOptions.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucRptOptions.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucRptOptions.Location = New System.Drawing.Point(0, 0)
        Me.ucRptOptions.m_destinationType = Nothing
        Me.ucRptOptions.m_eventBased = False
        Me.ucRptOptions.m_isQuery = False
        Me.ucRptOptions.Name = "ucRptOptions"
        Me.ucRptOptions.selectedFormat = ""
        Me.ucRptOptions.Size = New System.Drawing.Size(665, 569)
        Me.ucRptOptions.TabIndex = 15
        '
        'UcPGP
        '
        Me.UcPGP.BackColor = System.Drawing.Color.Transparent
        Me.UcPGP.Enabled = False
        Me.UcPGP.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcPGP.Location = New System.Drawing.Point(8, 35)
        Me.UcPGP.Name = "UcPGP"
        Me.UcPGP.Size = New System.Drawing.Size(424, 359)
        Me.UcPGP.TabIndex = 1
        '
        'frmDestinationSelect
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(660, 670)
        Me.ControlBox = False
        Me.Controls.Add(Me.stabDestination)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.chkStatus)
        Me.Controls.Add(Me.cmdInsert2)
        Me.Controls.Add(Me.chkSplit)
        Me.Controls.Add(Me.chkburst)
        Me.Controls.Add(Me.cmdInsert)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmDestinationSelect"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Destination"
        Me.grpEmail.ResumeLayout(False)
        Me.grpEmail.PerformLayout()
        Me.grpSender.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.pnMailServer.ResumeLayout(False)
        Me.pnReadReciepts.ResumeLayout(False)
        Me.grpPrinter.ResumeLayout(False)
        Me.grpFile.ResumeLayout(False)
        Me.grpHouseKeeping.ResumeLayout(False)
        CType(Me.txtHouseNum, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.grpFTP.ResumeLayout(False)
        CType(Me.stabDestination, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabDestination.ResumeLayout(False)
        Me.stabSetupPanel.ResumeLayout(False)
        Me.grpNothing.ResumeLayout(False)
        Me.grpCloud.ResumeLayout(False)
        Me.grpSharePoint.ResumeLayout(False)
        Me.grpSMS.ResumeLayout(False)
        Me.grpODBC.ResumeLayout(False)
        Me.grpODBC.PerformLayout()
        Me.grpFax.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.stabFormatPanel.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.stabMiscPanel.ResumeLayout(False)
        CType(Me.txtDefer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpZip.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.stabNamingPanel.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabPGPPanel.ResumeLayout(False)
        Me.mnuEditSharepoint.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub setEmailAutocompleteSource(txt As DevComponents.DotNetBar.Controls.TextBoxX)
        Try
            Dim ac As AutoCompleteStringCollection = New AutoCompleteStringCollection
            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")

            If IO.File.Exists(emailCache) = False Then Return

            Dim dt As DataTable = New DataTable

            dt.ReadXml(emailCache)

            For Each row As DataRow In dt.Rows
                ac.Add(row("emailaddress"))
            Next

            txt.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            txt.AutoCompleteSource = AutoCompleteSource.CustomSource
            txt.AutoCompleteCustomSource = ac
        Catch : End Try
    End Sub

    Private Sub txtTo_LostFocus(sender As Object, e As System.EventArgs) Handles txtTo.LostFocus, txtBCC.LostFocus, txtCC.LostFocus
        Try
            If CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text = "" Then Return

            Dim emailCache As String = IO.Path.Combine(sAppDataPath, "emailcache.dat")
            Dim dtCache As DataTable = New DataTable("emailcache")

            If IO.File.Exists(emailCache) Then
                dtCache.ReadXml(emailCache)
            Else
                With dtCache.Columns
                    .Add("emailAddress")
                End With
            End If

            Dim row As DataRow
            Dim rows() As DataRow = dtCache.Select("emailaddress = '" & CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text & "'")

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                row = rows(0)
            Else
                row = dtCache.Rows.Add
                row("emailaddress") = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX).Text
            End If

            dtCache.WriteXml(emailCache, XmlWriteMode.WriteSchema)
        Catch : End Try
    End Sub

    Public Sub EditDestinaton(ByVal nID As Integer, _
           Optional ByVal IsDynamic As Boolean = False, _
           Optional ByVal IsPackage As Boolean = False, _
           Optional ByVal sReportTitle As String = "", Optional ByVal ExcelBurst As Boolean = False, _
           Optional ByVal StaticDest As Boolean = False, Optional ByVal SmartID As Integer = 99999, _
           Optional ByVal IsBursting As Boolean = False, Optional ByVal IsDataDriven As Boolean = False)
        Try
10:         sMode = "Edit"

            Dim SQL As String
            Dim sWhere As String = " WHERE DestinationID = " & nID
            Dim NewID As Integer
            Dim nReportID As Integer
            Dim nPackID As Integer
            Dim s() As String

20:         xDestinationID = nID

30:         sTitle = sReportTitle

40:         Dynamic = IsDynamic
50:         Package = IsPackage
60:         IsStatic = StaticDest
70:         Bursting = IsBursting
80:         DataDriven = IsDataDriven

90:         UcPGP.Dynamic = Dynamic
100:        UcPGP.IsStatic = StaticDest
110:        UcPGP.eventID = eventID
120:        UcPGP.m_eventBased = m_eventBased

130:        m_inserter = New frmInserter(eventID)

            ucRptOptions.m_isQuery = m_IsQuery

140:        If IsDynamic = True Then
150:            s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "SharePoint", "Dropbox"}

160:            If StaticDest = False Then
170:                cmdTo.Visible = False
180:                txtTo.Visible = False
190:                UcFTP.btnVerify.Visible = False
200:                UcFTP.txtFTPServer.Enabled = False
210:                chkHouseKeeping.Enabled = False
                End If

220:            Me.chkKeyParameter.Visible = True
230:        ElseIf ExcelBurst = True Then
240:            s = New String() {"Email", "Disk", "FTP"}
250:            cmdTo.Visible = True
260:            txtTo.Visible = True

270:            cmbFormat.Items.Clear()

280:            Dim s1() As String = New String() {"MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", _
          "MS Excel 97-2000 (*.xls)"}

290:            cmbFormat.Items.AddRange(s1)
300:        ElseIf SmartID <> 99999 And SmartID <> 2777 And SmartID <> 0 Then
310:            s = New String() {"Email", "Disk", "FTP"}

320:            cmbFormat.Items.Clear()

330:            Dim s1 As String() = New String() {"MS Excel  (*.xls)", "XML (*.xml)"}

340:            cmbFormat.Items.AddRange(s1)

350:            IsSmart = True
360:        Else
#If CRYSTAL_VER < 11.6 Then
370:            s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "ODBC", "SharePoint", "Dropbox"}
#Else
380:            s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "SharePoint","Dropbox"}
#End If
390:            cmdTo.Visible = True
400:            txtTo.Visible = True
            End If

410:        If Me.DataDriven = True Then
                'GroupBox4.Enabled = False
                chkInstadrop.Visible = True
            End If

            vetOutputTypes(s)

420:        cmbDestination.Items.AddRange(s)

430:        cmbDestination.Sorted = True

440:        SQL = "SELECT * FROM DestinationAttr " & sWhere

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

450:        If oRs Is Nothing Then Return

460:        If oRs.EOF = False Then
470:            nReportID = IsNull(oRs("reportid").Value, 0)
480:            nPackID = IsNull(oRs("packid").Value, 0)

490:            xReportID = nReportID

500:            cmbDestination.Text = oRs("destinationtype").Value
510:            txtName.Text = oRs("destinationname").Value

                'lets add the new destination items
520:            If Package = True Then
530:                cmbFormat.Items.Add("Package")
540:                cmbFormat.Text = "Package"
                    stabFormatPanel.Enabled = False
                    stabFormat.Visible = False
                    stabNaming.Visible = False
550:            Else
560:                ucRptOptions.selectedFormat = oRs("outputformat").Value
                End If

570:            If IsNull(oRs("customext").Value).Length > 0 Then
580:                chkCustomExt.Checked = True
590:                txtCustomExt.Text = oRs("customext").Value
                End If

600:            chkAppendDateTime.Checked = Convert.ToBoolean(oRs("appenddatetime").Value)
610:            cmbDateTime.Text = IsNull(oRs("datetimeformat").Value)

620:            Try
630:                chkSplit.Checked = Convert.ToBoolean(IsNull(oRs("htmlsplit").Value))
640:            Catch
650:                chkSplit.Checked = False
                End Try

660:            If IsNull(oRs("customname").Value).Length > 0 Then
670:                optCustomName.Checked = True
680:                txtCustomName.Text = oRs("customname").Value
690:            Else
700:                optNaming.Checked = True

710:                If sTitle.Length > 0 And cmbFormat.Text.IndexOf("*") > -1 Then
720:                    txtDefault.Text = sTitle & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
                    End If
                End If

                Try
730:                chkZip.Checked = Convert.ToBoolean(oRs("compress").Value)
                Catch : End Try

                Try
740:                chkEmbed.Checked = Convert.ToBoolean(oRs("embed").Value)
                Catch : End Try

                Try
750:                chkIncludeAttach.Checked = Convert.ToBoolean(oRs("includeattach").Value)
                Catch : End Try

760:            Try
770:                If IsNull(oRs("deferdelivery").Value, 0) = 1 Then
780:                    chkDefer.Checked = True
790:                    txtDefer.Value = oRs("deferby").Value
                    End If
                Catch : End Try

800:            If MailType = gMailType.MAPI Then
810:                Try
820:                    chkReadReceipt.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(oRs("readreceipts").Value, 0)))
830:                Catch ex As Exception
840:                    chkReadReceipt.Checked = False
                    End Try
850:            Else
860:                Try
870:                    chkDeliveryReciept.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(oRs("readreceipts").Value, 0)))
880:                Catch ex As Exception
890:                    chkDeliveryReciept.Checked = False
                    End Try
                End If

900:            txtAdjustStamp.Value = IsNull(oRs("adjuststamp").Value, 0)

910:            Try
920:                chkStatus.Checked = Convert.ToBoolean(oRs("enabledstatus").Value)
930:            Catch ex As Exception
940:                chkStatus.Checked = True
                End Try

950:            Try
960:                chkZipSecurity.Checked = Convert.ToBoolean(oRs("encryptzip").Value)
970:                cmbEncryptionLevel.Text = Convert.ToString(oRs("encryptziplevel").Value)
980:                txtZipCode.Text = Decrypt(oRs("encryptzipcode").Value, "preggers")
990:                txtConfirmCode.Text = Decrypt(oRs("encryptzipcode").Value, "preggers")
                Catch : End Try

                Try
                    chkInstadrop.Checked = Convert.ToInt32(IsNull(oRs("instantDelivery").Value, 0))
                Catch : End Try

1000:           Select Case cmbDestination.Text.ToLower
                    Case "email"
                        Dim msg As String = IsNull(oRs("message").Value)

1010:                   cmbMailFormat.Text = IsNull(oRs("mailformat").Value)

1040:                   txtTo.Text = IsNull(oRs("sendto").Value)
1050:                   txtCC.Text = IsNull(oRs("cc").Value)
1060:                   txtBCC.Text = IsNull(oRs("bcc").Value)
1070:                   txtSubject.Text = IsNull(oRs("subject").Value)
1080:                   txtMsg.Text = msg
1090:                   txtAttach.Text = IsNull(oRs("extras").Value)


1100:                   If cmbMailFormat.Text.Length = 0 Then cmbMailFormat.Text = "TEXT"

1110:                   If MailType = MarsGlobal.gMailType.SMTP Then
1120:                       cmbServer.Text = IsNull(oRs("smtpserver").Value, "Default")
                        End If

1130:                   Try
1140:                       Me.txtSenderAddress.Text = IsNull(oRs("senderaddress").Value)
1150:                       Me.txtSenderName.Text = IsNull(oRs("sendername").Value)
                        Catch : End Try
1160:               Case "disk"
                        Dim paths As String = IsNull(oRs("outputpath").Value)

1170:                   For Each si As String In paths.Split("|")
1180:                       If si.Length > 0 Then
1190:                           lsvPaths.Items.Add(si)
                            End If
1200:                   Next

                        'txtDirectory.Text = IsNull(oRs("outputpath").Value)

1210:                   Try
1220:                       chkDUN.Checked = Convert.ToBoolean(IsNull(oRs("UseDUN").Value, 0))
1230:                       cmbDUN.Text = oRs("dunname").Value
                        Catch : End Try
1240:               Case "ftp"
                        Dim ftpServers As String = IsNull(oRs("ftpserver").Value)
                        Dim ftpUsers As String = IsNull(oRs("ftpusername").Value)
                        Dim ftpPasswords As String = IsNull(oRs("ftppassword").Value)
                        Dim ftpPaths As String = IsNull(oRs("ftppath").Value)
                        Dim ftpTypes As String = IsNull(oRs("ftptype").Value, "FTP")
                        Dim ftpPassives As String = IsNull(oRs("ftppassive").Value, "")
                        Dim ftpOptions As String = IsNull(oRs("ftpoptions").Value, "")

1250:                   For I As Integer = 0 To ftpUsers.Split("|").GetUpperBound(0)
1260:                       Dim item As ListViewItem = New ListViewItem

1270:                       item.Text = ftpServers.Split("|")(I)

1280:                       Try


1290:                           With item.SubItems
1300:                               .Add(ftpUsers.Split("|")(I))
1310:                               .Add(ftpPasswords.Split("|")(I))
1320:                               .Add(ftpPaths.Split("|")(I))
1330:                               .Add(ftpTypes.Split("|")(I))

1340:                               Try
1350:                                   .Add(ftpPassives.Split("|")(I))
1360:                               Catch ex As Exception
1370:                                   .Add("0")
                                    End Try

1380:                               Try
1390:                                   .Add(ftpOptions.Split("|")(I))
1400:                               Catch ex As Exception
1410:                                   .Add("")
                                    End Try
                                End With

1420:                           lsvFTP.Items.Add(item)

                            Catch : End Try
1430:                   Next

1440:               Case "printer"
                        Dim oP As ADODB.Recordset

1450:                   oP = clsMarsData.GetData("SELECT * FROM PrinterAttr WHERE " & _
         "DestinationID = " & nID)

1460:                   If oP Is Nothing Then Exit Select

1470:                   Do While oP.EOF = False
1480:                       Dim oItem As New ListViewItem

1490:                       oItem.Text = oP("printername").Value
1500:                       oItem.SubItems.Add(oP("copies").Value)
1510:                       oItem.Tag = oP("printerid").Value

1520:                       lsvPrinters.Items.Add(oItem)

1530:                       If oItem.Text = "<[x]Dynamic Printer>" Then
1540:                           Me.cmbPrinters.Enabled = False
1550:                           Me.cmdAddPrinter.Enabled = False
1560:                           Me.cmdRemovePrinter.Enabled = False
                            End If

1570:                       oP.MoveNext()
1580:                   Loop

1590:                   oP.Close()
1600:               Case "fax"
                        'sendto - fax number
                        'subject - fax device
                        'cc - to name
                        'bcc - from name
                        'message - comments
1610:                   txtFaxNumber.Text = oRs("sendto").Value
1620:                   cmbFaxDevice.Text = oRs("subject").Value
1630:                   txtFaxTo.Text = oRs("cc").Value
1640:                   txtFaxFrom.Text = oRs("bcc").Value
1650:                   txtFaxComments.Text = oRs("message").Value
1660:               Case "sms"
1670:                   txtSMSNumber.Text = oRs("sendto").Value
1680:                   txtSMSMsg.Text = IsNull(oRs("message").Value)
1690:               Case "odbc"
                        'sendto - dsn name
                        'subject - dsn username
                        'cc - dsn password
                        'message - table name
                        'copies - overwrite

1700:                   With UcDSN
1710:                       .cmbDSN.Text = oRs("sendto").Value
1720:                       .txtUserID.Text = oRs("subject").Value
1730:                       .txtPassword.Text = _DecryptDBValue(oRs("cc").Value)
                        End With

1740:                   txtTableName.Text = CType(oRs("message").Value, String).Replace("[", "").Replace("]", "")

1750:                   Try
1760:                       chkOverwrite.Checked = Convert.ToBoolean(oRs("copies").Value)
1770:                   Catch ex As Exception
1780:                       chkOverwrite.Checked = False
                        End Try
1790:               Case "sharepoint"
                        Dim spServers As String = IsNull(oRs("ftpserver").Value)
                        Dim spUsers As String = IsNull(oRs("ftpusername").Value)
                        Dim spPasswords As String = IsNull(oRs("ftppassword").Value)
                        Dim spLibs As String = IsNull(oRs("ftppath").Value)
                        Dim spMetaData As String = IsNull(oRs("ftpoptions").Value)

1800:                   For I As Integer = 0 To spServers.Split("|").GetUpperBound(0)
1810:                       Dim item As ListViewItem = New ListViewItem

1820:                       If spServers.Split("|")(I) = "" Then Continue For

1830:                       item.Text = spServers.Split("|")(I)

1840:                       Try
1850:                           With item.SubItems
1860:                               .Add(spLibs.Split("|")(I))
1870:                               .Add(spUsers.Split("|")(I))
1880:                               Try
1890:                                   .Add(spPasswords.Split("|")(I))
1900:                               Catch
1910:                                   .Add("")
                                    End Try


1920:                               Try
1930:                                   .Add(spMetaData.Split("|")(I))
1940:                               Catch ex As Exception
1950:                                   .Add("")
                                    End Try
                                End With

1960:                           Me.lsvSharePointLibs.Items.Add(item)
                            Catch : End Try
1970:                   Next
                    Case "dropbox"
                        txtCloudFolder.Text = oRs("ftppath").Value
                        cmbCloundAccount.Text = oRs("ftpserver").Value
                End Select

            End If

1980:       oRs.Close()

1990:       Try
2000:           oRs = clsMarsData.GetData("SELECT * FROM HouseKeepingAttr WHERE DestinationID =" & nID)

2010:           If oRs.EOF = False Then
2020:               chkHouseKeeping.Checked = True
2030:               txtHouseNum.Value = oRs("agevalue").Value
2040:               cmbUnit.Text = oRs("ageunit").Value
                End If

2050:           oRs.Close()
            Catch : End Try

2060:       cmbDestination.Enabled = False

2070:       oRs = clsMarsData.GetData("SELECT * FROM PGPAttr WHERE DestinationID = " & nID)

2080:       If oRs.EOF = False Then
2090:           chkPGP.Checked = True

                Dim sKeys As String

2100:           With UcPGP
2110:               .txtPGPUserID.Text = oRs("pgpid").Value

2120:               sKeys = IsNull(oRs("keylocation").Value)

2130:               If sKeys.IndexOf("|") > -1 Then
2140:                   .txtKeyLocation.Text = sKeys.Split("|")(0)
2150:                   .txtSecring.Text = sKeys.Split("|")(1)
2160:               Else
2170:                   .txtKeyLocation.Text = sKeys
                    End If

2180:               Try
2190:                   .chkPGPExt.Checked = Convert.ToBoolean(oRs("pgpext").Value)
2200:               Catch
2210:                   .chkPGPExt.Checked = False
                    End Try
                End With
            End If

2220:       oRs = clsMarsData.GetData("SELECT * FROM EmbedAttr WHERE DestinationID =" & nID)

2230:       If oRs IsNot Nothing Then
2240:           If oRs.EOF = False Then
2250:               Me.xEmbedID = oRs("embedid").Value
                End If

2260:           oRs.Close()
            End If

2270:       If IsPackage Then
2280:           ucRptOptions.setFormats(New String() {"Package (*.*)"})
2290:           ucRptOptions.selectedFormat = "Package (*.*)"
2300:           stabFormatPanel.Enabled = False
                chkEmbed.Enabled = False
            End If


2310:       ucRptOptions.loadReportOptions(IsDynamic, "Single", nID, nReportID, IsBursting)


2320:       Me.formLoaded = True

            txtName.Focus()

2330:       Me.ShowDialog()

2340:       If UserCancel = True Then Return

            Dim sVals As String
            Dim sCols As String

2350:       Select Case cmbDestination.Text.ToLower
                Case "email"
                    Dim msg As String = txtMsg.Text

                    If msg Is Nothing Then msg = ""

2380:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
         "SendTo = '" & SQLPrepare(txtTo.Text) & "'," & _
         "Cc = '" & SQLPrepare(txtCC.Text) & "'," & _
         "Bcc = '" & SQLPrepare(txtBCC.Text) & "'," & _
         "Subject = '" & SQLPrepare(txtSubject.Text) & "'," & _
         "Message = '" & SQLPrepare(msg) & "'," & _
         "Extras = '" & SQLPrepare(txtAttach.Text) & "'," & _
         "MailFormat = '" & cmbMailFormat.Text & "'," & _
         "SMTPServer = '" & SQLPrepare(cmbServer.Text) & "'," & _
         "SenderName = '" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
         "SenderAddress = '" & SQLPrepare(Me.txtSenderAddress.Text) & "'"
2390:           Case "disk"
                    Dim paths As String = ""

2400:               For Each item As ListViewItem In lsvPaths.Items
2410:                   paths &= item.Text & "|"
2420:               Next

2430:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "OutputPath = '" & SQLPrepare(paths) & "'," & _
     "UseDUN = " & Convert.ToInt32(chkDUN.Checked) & "," & _
     "DUNName = '" & SQLPrepare(cmbDUN.Text) & "'"
2440:           Case "ftp"
                    Dim ftpServers As String = ""
                    Dim ftpUsers As String = ""
                    Dim ftpPasswords As String = ""
                    Dim ftpPaths As String = ""
                    Dim ftpTypes As String = ""
                    Dim ftpPassives As String = ""
                    Dim ftpOptions As String = ""

2450:               For Each item As ListViewItem In lsvFTP.Items
2460:                   ftpServers &= item.Text & "|"
2470:                   ftpUsers &= item.SubItems(1).Text & "|"
2480:                   ftpPasswords &= item.SubItems(2).Text & "|"
2490:                   ftpPaths &= item.SubItems(3).Text & "|"
2500:                   ftpTypes &= item.SubItems(4).Text & "|"
2510:                   Try
2520:                       ftpPassives &= item.SubItems(5).Text & "|"
2530:                   Catch
2540:                       ftpPassives &= "0" & "|"
                        End Try

2550:                   Try
2560:                       ftpOptions &= item.SubItems(6).Text & "|"
2570:                   Catch ex As Exception
2580:                       ftpOptions &= "" & "|"
                        End Try
2590:               Next

2600:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "FTPServer = '" & SQLPrepare(ftpServers) & "'," & _
     "FTPUserName = '" & SQLPrepare(ftpUsers) & "'," & _
     "FTPPassword = '" & SQLPrepare(ftpPasswords) & "'," & _
     "FTPPath = '" & SQLPrepare(ftpPaths) & "'," & _
     "FTPType = '" & SQLPrepare(ftpTypes) & "'," & _
     "FTPPassive = '" & ftpPassives & "'," & _
     "FTPOptions = '" & SQLPrepare(ftpOptions) & "'"
2610:           Case "fax"
2620:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "SendTo = '" & SQLPrepare(txtFaxNumber.Text) & "'," & _
     "Subject = '" & SQLPrepare(cmbFaxDevice.Text) & "'," & _
     "Cc = '" & SQLPrepare(txtFaxTo.Text) & "'," & _
     "Bcc = '" & SQLPrepare(txtFaxFrom.Text) & "'," & _
     "Message ='" & SQLPrepare(txtFaxComments.Text) & "'"
2630:           Case "sms"
2640:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "SendTo = '" & SQLPrepare(txtSMSNumber.Text) & "'," & _
     "Message = '" & SQLPrepare(txtSMSMsg.Text) & "'"
2650:           Case "odbc"

2660:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "SendTo = '" & SQLPrepare(UcDSN.cmbDSN.Text) & "'," & _
     "Subject = '" & SQLPrepare(UcDSN.txtUserID.Text) & "'," & _
     "Cc = '" & SQLPrepare(_EncryptDBValue(UcDSN.txtPassword.Text)) & "'," & _
     "Message = '[" & SQLPrepare(txtTableName.Text) & "]'," & _
     "Copies =" & Convert.ToInt32(chkOverwrite.Checked)
2670:           Case "sharepoint"
                    Dim spServers As String = ""
                    Dim spUsers As String = ""
                    Dim spPasswords As String = ""
                    Dim spLibs As String = ""
                    Dim spTypes As String = ""
                    Dim spPassives As String = ""
                    Dim spMetaData As String = ""

2680:               For Each item As ListViewItem In Me.lsvSharePointLibs.Items

2690:                   If item.Text = "" Then Continue For

2700:                   spServers &= item.Text & "|"
2710:                   spLibs &= item.SubItems(1).Text & "|"
2720:                   spUsers &= item.SubItems(2).Text & "|"
2730:                   spPasswords &= item.SubItems(3).Text & "|"
2740:                   spMetaData &= item.SubItems(4).Text & "|"
2750:               Next

2760:               sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
     "FTPServer = '" & SQLPrepare(spServers) & "'," & _
     "FTPUserName = '" & SQLPrepare(spUsers) & "'," & _
     "FTPPassword = '" & SQLPrepare(spPasswords) & "'," & _
     "FTPPath = '" & SQLPrepare(spLibs) & "'," & _
     "FTPOptions = '" & SQLPrepare(spMetaData) & "'"
                Case "dropbox"
                    sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                        "FTPServer ='" & SQLPrepare(cmbCloundAccount.Text) & "'," & _
                        "FTPPath = '" & SQLPrepare(txtCloudFolder.Text) & "'"
            End Select

            Dim nEncrypt As Integer
2770:       Try
2780:           nEncrypt = cmbEncryptionLevel.Text
2790:       Catch ex As Exception
2800:           nEncrypt = 0
            End Try


            Dim requestReciept As Boolean

2810:       If MailType = gMailType.MAPI Then
2820:           requestReciept = chkReadReceipt.Checked
2830:       Else
2840:           requestReciept = chkDeliveryReciept.Checked
            End If

2850:       sVals &= ",OutputFormat ='" & ucRptOptions.selectedFormat & "'," & _
     "CustomExt ='" & SQLPrepare(txtCustomExt.Text) & "'," & _
     "AppendDateTime =" & Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
     "DateTimeFormat ='" & cmbDateTime.Text & "'," & _
     "CustomName ='" & SQLPrepare(txtCustomName.Text) & "'," & _
     "Includeattach =" & Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
     "Compress =" & Convert.ToInt32(chkZip.Checked) & "," & _
     "Embed =" & Convert.ToInt32(chkEmbed.Checked) & "," & _
     "HTMLSplit =" & Convert.ToInt32(chkSplit.Checked) & "," & _
     "DeferDelivery =" & Convert.ToInt32(chkDefer.Checked) & "," & _
     "DeferBy ='" & txtDefer.Value & "'," & _
     "EncryptZip =" & Convert.ToInt32(chkZipSecurity.Checked) & "," & _
     "EncryptZipLevel =" & nEncrypt & "," & _
     "EncryptZipCode ='" & Encrypt(txtZipCode.Text, "preggers") & "'," & _
     "ReadReceipts = " & Convert.ToInt32(requestReciept) & "," & _
     "AdjustStamp = " & txtAdjustStamp.Value & "," & _
     "EnabledStatus =" & Convert.ToInt32(chkStatus.Checked) & "," & _
     "InstantDelivery =" & Convert.ToInt32(chkInstadrop.Checked)

2860:       If cmbDestination.Text.ToLower = "printer" Then

2870:           clsMarsData.WriteData("UPDATE DestinationAttr SET " & _
     "DestinationName = '" & SQLPrepare(txtName.Text) & "' WHERE " & _
     "DestinationID =" & nID)

                'save the printers
                Dim I As Integer = 1

2880:           clsMarsData.WriteData("UPDATE PrinterAttr SET DestinationID =" & nID & " WHERE DestinationID= 99999")
2890:       Else
2900:           SQL = "UPDATE DestinationAttr SET " & sVals & sWhere

2910:           clsMarsData.WriteData(SQL)
            End If

2920:       ucRptOptions.SaveReportOptions("Single", nID, nReportID)

2930:       clsMarsData.WriteData("DELETE FROM HouseKeepingAttr WHERE DestinationID = " & nID)

2940:       If chkHouseKeeping.Checked = True Then
2950:           sCols = "HouseID,DestinationID,AgeValue,AgeUnit"

2960:           sVals = clsMarsData.CreateDataID("housekeepingattr", "houseid") & "," & _
     nID & "," & _
     txtHouseNum.Value & "," & _
     "'" & cmbUnit.Text & "'"

2970:           SQL = "INSERT INTO HouseKeepingAttr (" & sCols & ") VALUES(" & sVals & ")"

2980:           clsMarsData.WriteData(SQL)
            End If

2990:       clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID =" & nID)

3000:       If chkPGP.Checked = True Then
3010:           With UcPGP
3020:               ._AddKey(nID)
                End With
3030:       Else
3040:           clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID = " & nID)
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub

    Public Function AddDestination(ByVal DestOrderID As Integer, Optional ByVal nReportID As Integer = 99999, _
    Optional ByVal nPackID As Integer = 99999, _
    Optional ByVal IsDynamic As Boolean = False, _
    Optional ByVal IsPackage As Boolean = False, _
    Optional ByVal PreDestination As String = "", _
    Optional ByVal sReportTitle As String = "", _
    Optional ByVal ExcelBurst As Boolean = False, Optional ByVal StaticDest As Boolean = False, _
    Optional ByVal nSmartID As Integer = 99999, _
    Optional ByVal IsBursting As Boolean = False, Optional ByVal IsDataDriven As Boolean = False, Optional ByVal destinationType As String = "") As Integer

        Try

            Dim DestinationID As Integer = clsMarsData.CreateDataID("DestinationAttr", "DestinationID")

            xReportID = nReportID

            sMode = "Add"

            Dim s() As String

            sTitle = sReportTitle

            txtDefault.Text = sTitle

            Dynamic = IsDynamic
            Package = IsPackage
            IsStatic = StaticDest
            Bursting = IsBursting
            DataDriven = IsDataDriven

            UcPGP.Dynamic = Dynamic
            UcPGP.IsStatic = StaticDest
            UcPGP.eventID = eventID
            UcPGP.m_eventBased = m_eventBased

            chkStatus.Checked = True

            If IsDynamic = True Then
                s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "SharePoint", "Dropbox"}

                If StaticDest = False Then
                    cmdTo.Visible = False
                    txtTo.Visible = False
                    UcFTP.txtFTPServer.Enabled = False
                    UcFTP.btnVerify.Visible = False
                    cmdFaxTo.Visible = False
                    txtFaxNumber.Visible = False
                    txtSMSNumber.Visible = False
                    cmdSMSTo.Visible = False
                    chkHouseKeeping.Enabled = False
                End If

                Me.chkKeyParameter.Visible = True
            ElseIf ExcelBurst = True Then
                s = New String() {"Email", "Disk", "FTP"}
                cmdTo.Visible = True
                txtTo.Visible = True

                Dim s1() As String = New String() {"MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", _
                "MS Excel 97-2000 (*.xls)"}

                ucRptOptions.setFormats(s1)
            ElseIf nSmartID <> 99999 And nSmartID <> 2777 And nSmartID <> 0 Then
                s = New String() {"Email", "Disk", "FTP"}

                Dim s1 As String() = New String() {"MS Excel  (*.xls)", "XML (*.xml)"}

                ucRptOptions.setFormats(s1)

                IsSmart = True
            Else
                s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "ODBC", "SharePoint", "Dropbox"}
                cmdTo.Visible = True
                txtTo.Visible = True
            End If

            vetOutputTypes(s)

            cmbDestination.Items.AddRange(s)

            cmbDestination.Sorted = True

            Try
                If PreDestination.Length > 0 Then
                    cmbDestination.Text = PreDestination
                    cmbDestination.Enabled = False

                    If PreDestination.ToLower = "printer" And IsStatic = False Then
                        Me.cmbPrinters.Items.Add("<[x]Dynamic Printer>")
                        Me.cmbPrinters.Text = "<[x]Dynamic Printer>"
                        Me.cmbPrinters.Enabled = False
                        Me.cmdAddPrinter_Click(Nothing, Nothing)
                        Me.cmdAddPrinter.Enabled = False
                        Me.cmdRemovePrinter.Enabled = False
                    End If
                End If
            Catch : End Try

            cmbMailFormat.Text = "TEXT"
            cmbServer.Text = "Default"

            If Me.DataDriven = True Then
                chkInstadrop.Visible = True
            End If

            Me.formLoaded = True

            If destinationType <> "" Then cmbDestination.Text = destinationType

            If IsDynamic And cmbDestination.Text <> "" Then cmbDestination.Enabled = False

            'set up report options
            If nPackID <> 0 And nPackID <> 99999 And nPackID <> 2777 Then
                ucRptOptions.m_ReportType = "Package"
                ucRptOptions.m_ReportID = nReportID
                ucRptOptions.m_DestinationID = DestinationID
                '  ucRptOptions.m_Format = cmbFormat.Text
                ucRptOptions.m_eventBased = eventBased
                ucRptOptions.Dynamic = IsDynamic
                ucRptOptions.Enabled = False
            Else
                ucRptOptions.m_ReportType = "Single"
                ucRptOptions.m_ReportID = nReportID
                ucRptOptions.m_DestinationID = DestinationID
                '  ucRptOptions.m_Format = cmbFormat.Text
                ucRptOptions.m_eventBased = eventBased
                ucRptOptions.Dynamic = IsDynamic
                ucRptOptions.Enabled = True
            End If

            If IsPackage Then
                ucRptOptions.setFormats(New String() {"Package (*.*)"})
                ucRptOptions.selectedFormat = "Package (*.*)"
                stabNaming.Visible = False
                stabFormatPanel.Enabled = False
                stabFormat.Visible = False
                chkEmbed.Enabled = False
            End If

            m_inserter = New frmInserter(eventID)

            txtName.Focus()

            Me.ShowDialog()

            If UserCancel = True Then Return 0

            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim I As Integer = 1
            Dim NewID As Integer
            Dim paths As String = ""

            'DestinationID = milliCount & DestinationID

            sCols = "destinationid,destinationtype,destinationname,"

            Select Case cmbDestination.Text.ToLower
                Case "email"
                    Dim msg As String = txtMsg.Text

                    sCols &= "SendTo,CC,Bcc,Subject,Message,Extras,MailFormat,SMTPServer,SenderName,SenderAddress,"

                    sVals = DestinationID & "," & _
                    "'Email'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtTo.Text) & "'," & _
                    "'" & SQLPrepare(txtCC.Text) & "'," & _
                    "'" & SQLPrepare(txtBCC.Text) & "'," & _
                    "'" & SQLPrepare(txtSubject.Text) & "'," & _
                    "'" & SQLPrepare(msg) & "'," & _
                    "'" & SQLPrepare(txtAttach.Text) & "'," & _
                    "'" & cmbMailFormat.Text & "'," & _
                    "'" & SQLPrepare(cmbServer.Text) & "'," & _
                    "'" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
                    "'" & SQLPrepare(Me.txtSenderAddress.Text) & "'"

                Case "disk"
                    For Each item As ListViewItem In lsvPaths.Items
                        paths &= item.Text & "|"
                    Next

                    sCols &= "OutputPath,"

                    sVals = DestinationID & "," & _
                    "'Disk'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(paths) & "'"
                Case "printer"
                    sCols &= "Printername,Copies,"
                Case "ftp"
                    Dim ftpServers As String = ""
                    Dim ftpUsers As String = ""
                    Dim ftpPasswords As String = ""
                    Dim ftpPaths As String = ""
                    Dim ftpTypes As String = ""
                    Dim ftpPassives As String = ""
                    Dim ftpOptions As String = ""

                    For Each item As ListViewItem In lsvFTP.Items
                        ftpServers &= item.Text & "|"
                        ftpUsers &= item.SubItems(1).Text & "|"
                        ftpPasswords &= item.SubItems(2).Text & "|"
                        ftpPaths &= item.SubItems(3).Text & "|"
                        ftpTypes &= item.SubItems(4).Text & "|"
                        ftpPassives &= item.SubItems(5).Text & "|"
                        ftpOptions &= item.SubItems(6).Text & "|"
                    Next

                    sCols &= "FTPServer,FTPUserName,FTPPassword,FTPPath,FTPType,FTPPassive,FTPOptions,"

                    sVals = DestinationID & "," & _
                    "'FTP'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(ftpServers) & "'," & _
                    "'" & SQLPrepare(ftpUsers) & "'," & _
                    "'" & SQLPrepare(ftpPasswords) & "'," & _
                    "'" & SQLPrepare(ftpPaths) & "'," & _
                    "'" & SQLPrepare(ftpTypes) & "'," & _
                    "'" & ftpPassives & "'," & _
                    "'" & SQLPrepare(ftpOptions) & "'"
                Case "fax"
                    'sendto - fax number
                    'subject - fax device
                    'cc - to name
                    'bcc - from name
                    'message - comments
                    sCols &= "SendTo,Subject,Cc,Bcc,Message,"

                    sVals = DestinationID & "," & _
                    "'Fax'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxNumber.Text) & "'," & _
                    "'" & SQLPrepare(cmbFaxDevice.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxTo.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxFrom.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxComments.Text) & "'"
                Case "sms"
                    sCols &= "SendTo,Message,"

                    sVals = DestinationID & "," & _
                    "'SMS'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtSMSNumber.Text) & "'," & _
                    "'" & SQLPrepare(txtSMSMsg.Text) & "'"
                Case "odbc"
                    'sendto - dsn name
                    'subject - dsn username
                    'cc - dsn password
                    'message - table name
                    'copies - overwrite table

                    sCols &= "SendTo,Subject,Cc,Message,Copies,"

                    sVals = DestinationID & "," & _
                    "'ODBC'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(UcDSN.cmbDSN.Text) & "'," & _
                    "'" & SQLPrepare(UcDSN.txtUserID.Text) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(UcDSN.txtPassword.Text)) & "'," & _
                    "'[" & SQLPrepare(txtTableName.Text) & "]'," & _
                    Convert.ToInt32(chkOverwrite.Checked)
                Case "sharepoint"
                    'will use FTP fields for sharepoint info storage
                    Dim spServers As String = ""
                    Dim spUsers As String = ""
                    Dim spPasswords As String = ""
                    Dim spLibs As String = ""
                    Dim spMetadata As String = ""

                    For Each item As ListViewItem In Me.lsvSharePointLibs.Items
                        spServers &= item.Text & "|"
                        spLibs &= item.SubItems(1).Text & "|"
                        spUsers &= item.SubItems(2).Text & "|"
                        spPasswords &= item.SubItems(3).Text & "|"
                        spMetadata &= item.SubItems(4).Text & "|"
                    Next

                    sCols &= "FTPServer,FTPPath,FTPUserName,FTPPassword,FTPOptions,"

                    sVals = DestinationID & "," & _
                    "'SharePoint'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(spServers) & "'," & _
                    "'" & SQLPrepare(spLibs) & "'," & _
                    "'" & SQLPrepare(spUsers) & "'," & _
                    "'" & SQLPrepare(spPasswords) & "'," & _
                    "'" & SQLPrepare(spMetadata) & "'"
                Case "dropbox"
                    '//ftpserver = accountname
                    '//ftppath = dropbox path
                    sCols &= "FTPServer, FTPPath,"

                    sVals = DestinationID & "," & _
                    "'Dropbox'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(cmbCloundAccount.Text) & "'," & _
                    "'" & SQLPrepare(txtCloudFolder.Text) & "'"
            End Select

            sCols &= "ReportID,PackID,OutputFormat,Customext,AppendDateTime," & _
            "DateTimeFormat,CustomName,IncludeAttach,Compress,Embed,HTMLSplit," & _
            "DeferDelivery,DeferBy,EncryptZip,EncryptZipLevel,EncryptZipCode,UseDUN,DUNName,SmartID," & _
            "ReadReceipts,AdjustStamp,EnabledStatus,InstantDelivery,DestOrderID"

            Dim enLevel As Integer

            Try
                enLevel = cmbEncryptionLevel.Text
            Catch ex As Exception
                enLevel = 0
            End Try

            Dim requestReciept As Boolean

            If MailType = gMailType.MAPI Then
                requestReciept = chkReadReceipt.Checked
            Else
                requestReciept = chkDeliveryReciept.Checked
            End If

            If cmbDestination.Text.ToLower = "printer" Then

                sVals = DestinationID & "," & _
                "'Printer'," & _
                "'" & SQLPrepare(txtName.Text) & "'," & _
                "'Printer'," & _
                "1," & _
                nReportID & "," & _
                nPackID & "," & _
                "'" & ucRptOptions.selectedFormat & "'," & _
                "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
                Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
                "'" & cmbDateTime.Text & "'," & _
                "'" & SQLPrepare(txtCustomName.Text) & "'," & _
                Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
                Convert.ToInt32(chkZip.Checked) & "," & _
                Convert.ToInt32(chkEmbed.Checked) & "," & _
                Convert.ToInt32(chkSplit.Checked) & "," & _
                Convert.ToInt32(chkDefer.Checked) & "," & _
                "'" & txtDefer.Value & "'," & _
                Convert.ToInt32(chkZipSecurity.Checked) & "," & _
                enLevel & "," & _
                "'" & SQLPrepare(Encrypt(txtZipCode.Text, "preggers")) & "',0,''," & nSmartID & "," & _
                Convert.ToInt32(requestReciept) & "," & _
                txtAdjustStamp.Value & "," & _
                Convert.ToInt32(chkStatus.Checked) & "," & _
                Convert.ToInt32(chkInstadrop.Checked) & "," & _
                DestOrderID

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") VALUES (" & sVals & ")"

                clsMarsData.WriteData(SQL)

                sCols = "printerid,reportid,packid,printername,copies,destinationid"

                SQL = "UPDATE PrinterAttr SET DestinationID = " & DestinationID & " WHERE DestinationID = 99999"

                clsMarsData.WriteData(SQL)
            Else

                sVals &= "," & nReportID & "," & _
                nPackID & "," & _
                "'" & ucRptOptions.selectedFormat & "'," & _
                "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
                Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
                "'" & cmbDateTime.Text & "'," & _
                "'" & SQLPrepare(txtCustomName.Text) & "'," & _
                Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
                Convert.ToInt32(chkZip.Checked) & "," & _
                Convert.ToInt32(chkEmbed.Checked) & "," & _
                Convert.ToInt32(chkSplit.Checked) & "," & _
                Convert.ToInt32(chkDefer.Checked) & "," & _
                "'" & txtDefer.Value & "'," & _
                Convert.ToInt32(chkZipSecurity.Checked) & "," & _
                Convert.ToInt32(enLevel) & "," & _
                "'" & SQLPrepare(Encrypt(txtZipCode.Text, "preggers")) & "'," & _
                Convert.ToInt32(chkDUN.Checked) & "," & _
                "'" & SQLPrepare(cmbDUN.Text) & "'," & _
                nSmartID & "," & _
                Convert.ToInt32(requestReciept) & "," & _
                txtAdjustStamp.Value & "," & _
                Convert.ToInt32(chkStatus.Checked) & "," & _
                Convert.ToInt32(chkInstadrop.Checked) & "," & _
                DestOrderID

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
                "VALUES(" & sVals & ")"

                clsMarsData.WriteData(SQL)

                ucRptOptions.SaveReportOptions("Single", DestinationID, 0)

                'clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID = " & DestinationID & ", ReportID = 0 WHERE " & _
                '"DestinationID = 99999 OR ReportID = 99999")

                clsMarsData.WriteData("UPDATE ReadReceiptsAttr SET DestinationID = " & DestinationID & _
                " WHERE DestinationID = 99999")

                clsMarsData.WriteData("UPDATE StampAttr SET ForeignID =" & DestinationID & " WHERE ForeignID = 99999")

                If chkHouseKeeping.Checked = True And cmbDestination.Text.ToLower = "disk" Then
                    sCols = "HouseID,DestinationID,AgeValue,AgeUnit"

                    sVals = clsMarsData.CreateDataID("housekeepingattr", "houseid") & "," & _
                    DestinationID & "," & _
                    txtHouseNum.Value & "," & _
                    "'" & cmbUnit.Text & "'"

                    SQL = "INSERT INTO HouseKeepingAttr (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                End If

                'clsMarsData.WriteData("UPDATE EmbedAttr SET DestinationID =" & DestinationID & " WHERE DestinationID = 99999")
            End If

            If chkPGP.Checked = True Then
                UcPGP._AddKey(DestinationID)
            End If

            Return DestinationID
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return 0
        End Try
    End Function

    Private Sub SetContextMenu()
        setupForDragAndDrop(txtTableName)
        setupForDragAndDrop(txtFaxFrom)
        setupForDragAndDrop(txtFaxNumber)
        setupForDragAndDrop(txtFaxTo)
        setupForDragAndDrop(txtFaxComments)
        setupForDragAndDrop(cmbFaxDevice)

        setupForDragAndDrop(txtSMSNumber)
        setupForDragAndDrop(txtSMSMsg)

        setupForDragAndDrop(txtTableName)
        setupForDragAndDrop(UcDSN.cmbDSN)
        setupForDragAndDrop(UcDSN.txtUserID)

        setupForDragAndDrop(txtSenderAddress)
        setupForDragAndDrop(txtSenderName)

        setupForDragAndDrop(txtDirectory)
        setupForDragAndDrop(txtTo)
        setupForDragAndDrop(txtCC)
        setupForDragAndDrop(txtBCC)
        setupForDragAndDrop(txtSubject)
        setupForDragAndDrop(txtAttach)
        setupForDragAndDrop(txtMsg)
        setupForDragAndDrop(txtCustomName)
        setupForDragAndDrop(txtCustomExt)

        'Me.txtFaxFrom.ContextMenu = Me.mnuInserter
        'Me.txtFaxNumber.ContextMenu = Me.mnuInserter
        'Me.txtFaxTo.ContextMenu = Me.mnuInserter
        'Me.txtFaxComments.ContextMenu = Me.mnuInserter
        'Me.cmbFaxDevice.ContextMenu = Me.mnuInserter

        'Me.txtSMSNumber.ContextMenu = Me.mnuInserter
        'Me.txtSMSMsg.ContextMenu = Me.mnuInserter

        'Me.txtTableName.ContextMenu = Me.mnuInserter
        'Me.UcDSN.cmbDSN.ContextMenu = Me.mnuInserter
        'Me.UcDSN.txtUserID.ContextMenu = Me.mnuInserter
        'Me.UcDSN.txtPassword.ContextMenu = Me.mnuInserter

        'Me.txtSenderName.ContextMenu = Me.mnuInserter
        'Me.txtSenderAddress.ContextMenu = Me.mnuInserter

        If Me.DataDriven = True Then
            ' Me.cmbFormat.ContextMenu = Me.mnuInserter
            ucRptOptions.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
            setupForDragAndDrop(ucRptOptions.cmbFormat)
        End If

        'UcPGP.txtKeyLocation.ContextMenu = mnuInserter
        'UcPGP.txtPGPUserID.ContextMenu = mnuInserter
        'UcPGP.txtSecring.ContextMenu = mnuInserter

        'AddHandler UcPGP.txtKeyLocation.GotFocus, AddressOf pgpTxt_GotFocus
        'AddHandler UcPGP.txtPGPUserID.GotFocus, AddressOf pgpTxt_GotFocus
        'AddHandler UcPGP.txtSecring.GotFocus, AddressOf pgpTxt_GotFocus
    End Sub

    Private Sub frmDestinationSelect_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SetContextMenu()

            Dim oPrint As New System.Drawing.Printing.PrinterSettings
            Dim strItem As String
            Dim strPrinters As String
            Dim I As Int32 = 0
            Dim ilimo As Boolean

            FormatForWinXP(Me)

            If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False

            If MailType = MarsGlobal.gMailType.MAPI Then
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
                Me.pnMailServer.Visible = False
                Me.pnReadReciepts.Visible = True
            End If

            If MailType = gMailType.GROUPWISE Then
                cmbMailFormat.Text = "TEXT"
                cmbMailFormat.Enabled = False
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
                Me.pnMailServer.Visible = False
                Me.pnReadReciepts.Visible = False
            End If

            If MailType <> MarsGlobal.gMailType.SMTP Then
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
            Else
                cmbServer.Enabled = True
                Me.pnMailServer.Visible = True
                Me.pnReadReciepts.Visible = False
            End If

            If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
                Me.grpSender.Enabled = True
            Else
                Me.grpSender.Enabled = False
            End If

            For Each strItem In oPrint.InstalledPrinters
                For Each o As ListViewItem In lsvPrinters.Items
                    If strItem.ToLower = o.Text.ToLower Then
                        ilimo = True
                        Exit For
                    Else
                        ilimo = False
                    End If
                Next

                If ilimo = False Then cmbPrinters.Items.Add(strItem)

            Next

            If Package = True Then
                If chkZip.Checked = False Then
                    stabFormat.Enabled = False
                Else
                    stabFormat.Enabled = True
                End If

                'tabDestination.TabPages(3).Enabled = False
                cmbFormat.Items.Add("Package")
                cmbFormat.Text = "Package"
                cmbFormat.Enabled = False
                chkEmbed.Enabled = False
                chkZip.Enabled = True
            ElseIf Me.disableEmbed = True Then
                chkEmbed.Enabled = False
                chkEmbed.Checked = False
            End If
        Catch
        End Try

        txtTo.ContextMenu = Me.mnuInserter
        txtCC.ContextMenu = Me.mnuInserter
        txtBCC.ContextMenu = Me.mnuInserter

        m_inserter = New frmInserter(eventID)
        m_inserter.m_EventBased = m_eventBased

        m_inserter.GetConstants(Me)

        ucRptOptions.m_isQuery = m_IsQuery

        AddHandler ucRptOptions.cmbFormat.SelectedValueChanged, AddressOf changeNamingName

        setEmailAutocompleteSource(txtTo)
        setEmailAutocompleteSource(txtCC)
        setEmailAutocompleteSource(txtBCC)
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sFileDir As String

        fbg.ShowNewFolderButton = True
        fbg.Description = "Please select the destination directory"

        fbg.ShowDialog()

        sFileDir = _CreateUNC(fbg.SelectedPath)

        If sFileDir <> "" Then
            If sFileDir.Substring(sFileDir.Length - 1, 1) <> "\" Then
                txtDirectory.Text = sFileDir & "\"
            Else
                txtDirectory.Text = sFileDir
            End If

            Me.btnAddPath_Click(sender, e)
        End If
    End Sub

    Private Sub cmdAddPrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddPrinter.Click
        Try
            Dim I As Int32 = 0
            Dim lsv As ListViewItem

            If lsvPrinters.Items.Count >= 1 Then
                If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.p1_BulkPrinting) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, cmdAddPrinter, "Bulk Printing")
                    Return
                End If
            End If

            If cmbPrinters.Text = "" Then Exit Sub

            For I = 0 To lsvPrinters.Items.Count - 1
                If cmbPrinters.Text = lsvPrinters.Items(I).Text Then
                    Exit Sub
                End If
            Next

            Dim oPrinter As New frmPrinterAttr
            Dim sReturn() As Object

            sReturn = oPrinter._AddPrinter(cmbPrinters.Text, xDestinationID)

            If Not sReturn Is Nothing Then
                lsv = lsvPrinters.Items.Add(sReturn(2))

                lsv.SubItems.Add(sReturn(1))

                lsv.Tag = sReturn(0)

                If sReturn(2) = cmbPrinters.Text Then cmbPrinters.Items.Remove(cmbPrinters.Text)
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdRemovePrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemovePrinter.Click
        Dim I As Int32
        Dim oLsv As ListViewItem
        Dim SQL As String

        If lsvPrinters.Items.Count = 0 Or lsvPrinters.SelectedItems.Count = 0 Then _
            Exit Sub

        For Each oLsv In lsvPrinters.SelectedItems
            If oLsv.Text.Contains("<[") = False Then cmbPrinters.Items.Add(oLsv.Text)
        Next

        For Each oLsv In lsvPrinters.SelectedItems
            SQL = "DELETE FROM PrinterAttr WHERE PrinterID =" & oLsv.Tag

            If clsMarsData.WriteData(SQL) = True Then
                oLsv.Remove()
            End If
        Next
    End Sub

    Sub something()
        Dim txt As String = txtMsg.Text

        MsgBox(txt)
    End Sub

    Private Sub cmbDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDestination.SelectedIndexChanged
        Try
            SetError(sender, "")

            stabSetup.Text = cmbDestination.Text

            If cmbDestination.Text.Length = 0 Then
                stabDestination.Visible = False
            Else
                stabDestination.Visible = True
            End If

            Me.ucRptOptions.m_destinationType = cmbDestination.Text

            Select Case cmbDestination.Text.ToLower
                Case "dropbox"

                    grpCloud.Visible = True
                    grpCloud.BringToFront()
                    grpCloud.Dock = DockStyle.Fill

                    cmbFormat.Items.Remove("Printer Format")
                    ucRptOptions.removeFormat("Printer Format")

                    If cmbFormat.Text = "Printer Format" Then cmbFormat.Text = ""

                    If Package = False Then
                        stabSetupPanel.Enabled = True
                        stabNamingPanel.Enabled = True
                        stabNaming.Visible = True
                        stabFormatPanel.Enabled = True
                        stabFormat.Visible = True
                        stabCompression.Visible = True
                        stabPGP.Visible = True
                        ucRptOptions.setFormats("", IsSmart, m_IsQuery)
                    End If

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False
                Case "email"

                    If MailType = MarsGlobal.gMailType.NONE Then
                        MessageBox.Show("Please set up your messaging options before creating an email destination", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        cmbDestination.Text = "Disk"
                        Return
                    End If



                    txtMsg.m_eventID = Me.eventID
                    txtMsg.m_eventBased = Me.eventBased

                    grpEmail.Visible = True
                    grpEmail.BringToFront()
                    grpEmail.Dock = DockStyle.Fill


                    Try
                        ucRptOptions.removeFormat("Printer Format")
                        ucRptOptions.removeFormat("Fax Format")
                        ucRptOptions.removeFormat("ODBC (*.odbc)")
                        If ucRptOptions.selectedFormat = "Printer Format" Or ucRptOptions.selectedFormat = "Fax Format" Or ucRptOptions.selectedFormat = "ODBC (*.odbc)" Then ucRptOptions.selectedFormat = ""
                    Catch : End Try

                    If Package = False Then
                        stabSetupPanel.Enabled = True
                        stabFormatPanel.Enabled = True
                        stabNamingPanel.Enabled = True
                        stabFormat.Visible = True
                        stabNaming.Visible = True
                        stabCompression.Visible = True
                        stabPGP.Visible = True
                        '_SetFormats("")
                        ucRptOptions.setFormats("", IsSmart, m_IsQuery)
                    End If

                    If chkEmbed.Checked = True Then
                        chkIncludeAttach.Visible = True
                    End If

                Case "disk"
                    grpFile.Visible = True
                    grpFile.Dock = DockStyle.Fill
                    grpFile.BringToFront()

                    Try
                        ucRptOptions.removeFormat("Printer Format")
                        ucRptOptions.removeFormat("Fax Format")
                        ucRptOptions.removeFormat("ODBC (*.odbc)")
                        If ucRptOptions.selectedFormat = "Printer Format" Or ucRptOptions.selectedFormat = "Fax Format" Or ucRptOptions.selectedFormat = "ODBC (*.odbc)" Then ucRptOptions.selectedFormat = ""
                    Catch : End Try

                    If Package = False Then
                        stabSetupPanel.Enabled = True
                        stabNamingPanel.Enabled = True
                        stabNaming.Visible = True
                        stabFormatPanel.Enabled = True
                        stabFormat.Visible = True
                        stabCompression.Visible = True
                        stabPGP.Visible = True
                        ucRptOptions.setFormats("", IsSmart, m_IsQuery)
                    End If

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False

                    If lsvPaths.Items.Count = 0 And sMode <> "Edit" Then
                        cmdBrowse_Click(Nothing, Nothing)
                    End If
                Case "printer"

                    grpPrinter.Visible = True
                    grpPrinter.BringToFront()
                    grpPrinter.Dock = Dock.Fill

                    cmbFormat.Items.Add("Printer Format")

                    ucRptOptions.addFormat("Printer Format", True)

                    cmbFormat.Text = "Printer Format"

                    stabSetupPanel.Enabled = True

                    stabNaming.Visible = False
                    stabFormat.Visible = False
                    stabCompression.Visible = False
                    stabPGP.Visible = False


                    '   stabNamingPanel.Enabled = False
                    '  stabFormatPanel.Enabled = False
                    '  stabMiscPanel.Enabled = False

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False

                Case "ftp"

                    If IsFeatEnabled(gEdition.PLATINUM, modFeatCodes.f1_FTPDestination) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.PLATINUM, cmbDestination, "FTP Destination")
                        cmbDestination.Text = "Email"
                        Return
                    End If

                    grpFTP.Visible = True
                    grpFTP.BringToFront()
                    grpFTP.Dock = Dock.Fill

                    Try
                        ucRptOptions.removeFormat("Printer Format")
                        ucRptOptions.removeFormat("Fax Format")
                        ucRptOptions.removeFormat("ODBC (*.odbc)")
                        If ucRptOptions.selectedFormat = "Printer Format" Or ucRptOptions.selectedFormat = "Fax Format" Or ucRptOptions.selectedFormat = "ODBC (*.odbc)" Then ucRptOptions.selectedFormat = ""
                    Catch : End Try

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False

                    If Package = False Then
                        stabSetupPanel.Enabled = True
                        stabNamingPanel.Enabled = True
                        stabNaming.Visible = True
                        stabFormatPanel.Enabled = True
                        stabFormat.Visible = True
                        stabCompression.Visible = True
                        stabPGP.Visible = True
                        ucRptOptions.setFormats("", IsSmart, m_IsQuery)
                    End If

                    UcFTP.cmbFTPType.SelectedIndex = 0
                Case "fax"
                    If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.x1_AutomatedFaxing) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, cmbDestination, "Automated Faxing")
                        cmbDestination.Text = "Disk"
                        Return
                    End If

                    grpFax.Visible = True
                    grpFax.BringToFront()
                    grpFax.Dock = Dock.Fill

                    ucRptOptions.removeFormat("Printer Format")
                    ucRptOptions.addFormat("Fax Format", True)

                    stabSetupPanel.Enabled = True
                    '  stabNamingPanel.Enabled = False
                    '  stabFormatPanel.Enabled = False
                    ' stabMiscPanel.Enabled = False

                    stabNaming.Visible = False
                    stabFormat.Visible = False
                    stabCompression.Visible = False
                    stabPGP.Visible = False

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False
                Case "sms"

                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.o1_SMS) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, cmbDestination, "SMS Destination")
                        cmbDestination.Text = "Disk"
                        Return
                    End If

                    Try
                        ucRptOptions.removeFormat("Printer Format")
                        ucRptOptions.removeFormat("Fax Format")
                        ucRptOptions.removeFormat("ODBC (*.odbc)")
                        If ucRptOptions.selectedFormat = "Printer Format" Or ucRptOptions.selectedFormat = "Fax Format" Or ucRptOptions.selectedFormat = "ODBC (*.odbc)" Then ucRptOptions.selectedFormat = ""
                    Catch : End Try

                    grpSMS.Visible = True
                    grpSMS.BringToFront()
                    grpSMS.Dock = Dock.Fill

                    stabNamingPanel.Enabled = False
                    stabNaming.Visible = False
                    stabFormatPanel.Enabled = True
                    stabFormat.Visible = True
                    stabMiscPanel.Enabled = False
                    stabCompression.Visible = False
                    stabMiscPanel.Visible = False
                    stabCompression.Visible = True
                    stabPGP.Visible = True

                    If Package = False Then
                        stabSetupPanel.Enabled = True
                        ucRptOptions.setFormats("SMS", IsSmart, m_IsQuery)
                    End If

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False

                    chkZip.Enabled = False
                    chkZip.Checked = False
                Case "odbc"

                    '#If CRYSTAL_VER >= 11.6 Then
                    '                    MessageBox.Show("The ODBC destination is not yet supported with Crystal 2008. This might change in the near future.", Application.ProductName, MessageBoxButtons.OK, _
                    '                    MessageBoxIcon.Exclamation)
                    '                    cmbDestination.Text = "Disk"
                    '                    Return
                    '#End If
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.o3_ODBC) = False Then
                        _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, cmbDestination, "ODBC Destination")
                        cmbDestination.Text = "Disk"
                        Return
                    End If

                    ucRptOptions.removeFormat("Printer Format")
                    ucRptOptions.addFormat("ODBC (*.odbc)", True)

                    stabSetupPanel.Enabled = True

                    stabMiscPanel.Enabled = False
                    stabCompression.Visible = False
                    stabFormatPanel.Enabled = False
                    stabFormat.Visible = False
                    stabNamingPanel.Enabled = False
                    stabNaming.Visible = False
                    stabPGPPanel.Enabled = False
                    stabPGP.Visible = False
                    stabDestination.SelectedTab = stabSetup

                    grpODBC.Visible = True
                    grpODBC.BringToFront()
                    grpODBC.Dock = DockStyle.Fill

                    'AddHandler UcDSN.txtPassword.GotFocus, AddressOf SetField
                    AddHandler UcDSN.cmbDSN.GotFocus, AddressOf SetField
                    AddHandler UcDSN.txtUserID.GotFocus, AddressOf SetField
                Case "sharepoint"
                    If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.o4_SharePointDestination) = False Then
                        _NeedUpgrade(gEdition.CORPORATE, cmbDestination, "SharePoint Destination")
                        cmbDestination.Text = "Disk"
                        Return
                    End If

                    Me.grpSharePoint.Visible = True
                    grpSharePoint.Dock = DockStyle.Fill
                    grpSharePoint.BringToFront()

                    '//clean up the formats
                    Try
                        ucRptOptions.removeFormat("Printer Format")
                        ucRptOptions.removeFormat("Fax Format")
                        ucRptOptions.removeFormat("ODBC (*.odbc)")
                        If ucRptOptions.selectedFormat = "Printer Format" Or ucRptOptions.selectedFormat = "Fax Format" Or ucRptOptions.selectedFormat = "ODBC (*.odbc)" Then ucRptOptions.selectedFormat = ""
                    Catch : End Try

                    If Package = False Then
                        stabSetupPanel.Enabled = True

                        stabNamingPanel.Enabled = True
                        stabNaming.Visible = True
                        stabFormatPanel.Enabled = True
                        stabFormat.Visible = True
                        stabCompression.Visible = True
                        stabPGP.Visible = True
                        ucRptOptions.setFormats("", IsSmart, m_IsQuery)
                    End If

                    chkIncludeAttach.Checked = True
                    chkIncludeAttach.Visible = False
            End Select

            If txtName.Text.Length = 0 Then txtName.Text = Me.cmbDestination.Text
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), ex.ToString)
        End Try
    End Sub
    Public Sub certRcvd(ByVal sender As Object, ByVal e As Xceed.Ftp.CertificateReceivedEventArgs)
        ' MsgBox(e.ServerCertificate.EffectiveDate)
    End Sub


    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If cmbDestination.Text = "" Then
            SetError(cmbDestination, "Please select the schedule destination")
            cmbDestination.Focus()
            Exit Sub
        ElseIf txtName.Text.Length = 0 Then
            SetError(txtName, "Please provide a name for this destination")
            txtName.Focus()
            Return
        ElseIf chkIncludeAttach.Checked And cmbDestination.Text <> "ODBC" Then
            If ucRptOptions.isDataValid = False Then
                stabDestination.SelectedTab = stabFormat
                Return
            End If
            'ElseIf cmbFormat.Text.Length = 0 And chkIncludeAttach.Checked = True Then
            '    stabDestination.SelectedTab = stabFormat
            '    SetError(cmbFormat, "Please select the output format")
            '    cmbFormat.Focus()
            '    Return
        ElseIf optCustomName.Checked = True And txtCustomName.Text.Length = 0 Then
            stabDestination.SelectedTab = stabNaming
            SetError(txtCustomName, "Please enter the custom output name")
            txtCustomName.Focus()
            Return
        ElseIf chkCustomExt.Checked = True And txtCustomExt.Text.Length = 0 Then
            stabDestination.SelectedTab = stabNaming
            SetError(txtCustomExt, "Please enter the custom extension")
            txtCustomExt.Focus()
            Return
        ElseIf chkAppendDateTime.Checked = True And cmbDateTime.Text.Length = 0 Then
            stabDestination.SelectedTab = stabNaming
            SetError(cmbDateTime, "Please select the date/time format")
            cmbDateTime.Focus()
            Return
        End If

        If chkHouseKeeping.Checked = True Then
            If cmbUnit.Text.Length = 0 Then
                stabDestination.SelectedTab = stabSetup
                setError(cmbUnit, "Please specify the correct interval")
                cmbUnit.Focus()
                Return
            End If
        End If

        If chkZipSecurity.Checked = True Then
            If cmbEncryptionLevel.Text.Length = 0 Then
                stabDestination.SelectedTab = stabCompression
                setError(cmbEncryptionLevel, "Please select the encryption level")
                cmbEncryptionLevel.Focus()
                Return
            ElseIf txtZipCode.Text.Length = 0 Or txtConfirmCode.Text.Length = 0 Then
                stabDestination.SelectedTab = stabCompression
                setError(txtZipCode, "Please provide the password for encryption")
                txtZipCode.Focus()
                Return
            ElseIf txtZipCode.Text <> txtConfirmCode.Text Then
                stabDestination.SelectedTab = stabCompression
                setError(txtZipCode, "The confirm password and the password do not match")
                txtZipCode.Focus()
                Return
            End If
        End If

        If chkDUN.Checked = True And cmbDUN.Text.Length = 0 Then
            stabDestination.SelectedTab = stabSetup
            setError(cmbDUN, "Please select the connection from the drop-down list")
            cmbDUN.Focus()
            Return
        End If

        If chkPGP.Checked = True Then
            With UcPGP
                If .txtPGPUserID.Text.Length = 0 Then
                    stabDestination.SelectedTab = stabPGP
                    setError(.txtPGPUserID, "Please provide the Key User ID")
                    .txtPGPUserID.Focus()
                    Return
                ElseIf .txtKeyLocation.Text.Length = 0 Then
                    stabDestination.SelectedTab = stabPGP
                    setError(.txtKeyLocation, "Please provide the path to the public keyring file")
                    .txtKeyLocation.Focus()
                    Return
                ElseIf .txtSecring.Text.Length = 0 Then
                    stabDestination.SelectedTab = stabPGP
                    setError(.txtSecring, "Please provide the path to the private keyring file")
                    .txtSecring.Focus()
                    Return
                End If
            End With
        End If

        Select Case cmbDestination.Text.ToLower
            Case "email"
                If txtTo.Text = "" And (Dynamic = False Or IsStatic = True) Then
                    SetError(txtTo, "Please provide the email recipient")
                    stabDestination.SelectedTab = stabSetup
                    txtTo.Focus()
                    Exit Sub
                    'Else
                    'For Each s As String In txtAttach.Text.Split(";")
                    'If (s.IndexOf("*") = -1 And s.Length > 0) Or (s.Contains("<") = False And s.Length > 0) Then
                    'Try
                    '    If IO.File.Exists(s) = False Then
                    '        SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure attachment paths are valid")
                    '        txtAttach.Focus()
                    '        tabDestination.SelectedTab = tbDestination
                    '        Return
                    '    End If
                    '    Catch
                    '        SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure attachment paths are valid")
                    '        txtAttach.Focus()
                    '        tabDestination.SelectedTab = tbDestination
                    '        Return
                    '    End Try
                    'End If
                    '    Next
                End If
            Case "disk"
                If lsvPaths.Items.Count = 0 And (Dynamic = False Or IsStatic = True) Then
                    SetError(txtDirectory, "Please select the output directory for the schedule")
                    stabDestination.SelectedTab = stabSetup
                    txtDirectory.Focus()
                    Exit Sub
                End If
            Case "ftp"
                If lsvFTP.Items.Count = 0 Then
                    stabDestination.SelectedTab = stabSetup
                    setError(UcFTP.txtFTPServer, "Please add an ftp destination")
                    UcFTP.txtFTPServer.Focus()
                    Return
                End If
            Case "printer"
                If lsvPrinters.Items.Count <= 0 And (Dynamic = False Or IsStatic = True) Then
                    SetError(cmbPrinters, "Please select at least one printer for the schedule")
                    stabDestination.SelectedTab = stabSetup
                    cmbPrinters.Focus()
                    Exit Sub
                End If
            Case "fax"
                If txtFaxNumber.Text.Length = 0 And (Dynamic = False Or IsStatic = True) Then
                    SetError(txtFaxNumber, "Please provide the fax number")
                    stabDestination.SelectedTab = stabSetup
                    txtFaxNumber.Focus()
                    Return
                ElseIf cmbFaxDevice.Text.Length = 0 Then
                    SetError(cmbFaxDevice, "Please select the fax device")
                    stabDestination.SelectedTab = stabSetup
                    cmbFaxDevice.Focus()
                    Return
                End If
            Case "sms"
                If txtSMSNumber.Text.IndexOf("<[") = -1 And (Dynamic = False Or IsStatic = True) Then
                    Dim test As String = txtSMSNumber.Text.Replace(" ", "")

                    Try
                        Int64.Parse(test)
                    Catch ex As Exception
                        stabDestination.SelectedTab = stabSetup
                        setError(txtSMSNumber, "Please enter a valid cellphone number")
                        txtSMSNumber.Focus()
                        Return
                    End Try
                End If
            Case "odbc"
                With UcDSN
                    If .cmbDSN.Text.Length = 0 Then
                        stabDestination.SelectedTab = stabSetup
                        setError(.cmbDSN, "Please select the datasource name")
                        .cmbDSN.Focus()
                        Return
                    ElseIf txtTableName.Text.Length = 0 Then
                        stabDestination.SelectedTab = stabSetup
                        setError(txtTableName, "Please provide a table name")
                        txtTableName.Focus()
                        Return
                    Else
                        Try
                            Dim oRs As New ADODB.Recordset
                            Dim oCon As New ADODB.Connection

                            oCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)

                            oRs.Open("SELECT TOP 1 * FROM " & txtTableName.Text)

                            oRs.Close()

                            setError(txtTableName, "A table with this name already exists in the selected datasource")
                            txtTableName.Focus()
                            Return
                        Catch : End Try
                    End If
                End With
        End Select

        '  ucRptOptions.SaveReportOptions( "Single",)

        Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.GotFocus, txtCC.GotFocus, txtBCC.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub txtDirectory_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDirectory.TextChanged
        SetError(sender, "")

        If txtDirectory.Text.Length = 0 Then
            Me.btnAddPath.Enabled = False
            Return
        End If

        Dim itemFound As Boolean = False

        For Each item As ListViewItem In lsvPaths.Items
            If item.Text = Me.txtDirectory.Text Then
                itemFound = True
                Exit For
            End If
        Next

        Me.btnAddPath.Enabled = Not (itemFound)

    End Sub

    Private Sub txtFTPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SetError(sender, "")
    End Sub

    Private Sub txtFTPUsername_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SetError(sender, "")
    End Sub

    Private Sub txtFTPPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        SetError(sender, "")
    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdTo.MouseUp

        If e.Button <> MouseButtons.Left Then Return

        oField = txtTo
        sField = "to"
        mnuContacts.Show(cmdTo, New Point(e.X, e.Y))

    End Sub

    Private Sub cmdCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtCC
        sField = "cc"
        mnuContacts.Show(cmdCC, New Point(e.X, e.Y))
    End Sub



    Private Sub cmdBcc_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdBcc.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtBCC
        sField = "bcc"
        mnuContacts.Show(cmdBcc, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oUI As New clsMarsMessaging

        oUI.OutlookAddresses(oField)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click

        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact(sField)

        If sValues Is Nothing Then Return

        If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
        Then txtTo.Text &= ";"

        Try
            txtTo.Text &= sValues(0)
        Catch
        End Try

        If txtCC.Text.EndsWith(";") = False And txtCC.Text.Length > 0 _
        Then txtCC.Text &= ";"

        Try
            txtCC.Text &= sValues(1)
        Catch
        End Try

        If txtBCC.Text.EndsWith(";") = False And txtBCC.Text.Length > 0 _
        Then txtBCC.Text &= ";"

        Try
            txtBCC.Text &= sValues(2)
        Catch
        End Try

    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, mnuFile, "Mailing Lists")
            Return
        End If

        ofg.Title = "Please select the file to read email addresses from"

        ofg.ShowDialog()

        If ofg.FileName.Length > 0 Then
            If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
            oField.Text &= ";"

            oField.Text &= "<[f]" & ofg.FileName & ">;"
        End If
    End Sub

    Private Sub cmdAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAttach.Click
        ofg.Multiselect = True
        ofg.Title = "Select files to attach"
        ofg.ShowDialog()

        If ofg.FileNames.GetLength(0) = 0 Then Return

        If txtAttach.Text.EndsWith(";") = False And txtAttach.Text.Length > 0 Then _
        txtAttach.Text &= ";"

        For Each s As String In ofg.FileNames
            If s.Length > 0 Then
                s = _CreateUNC(s)
                txtAttach.SelectedText &= s & ";"
            End If
        Next
    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click

        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, mnuDB, "Mailing Lists")
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(Me.eventID)

        If sTemp.Length > 0 Then
            If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"

            oField.Text &= sTemp & ";"
        End If
    End Sub
    Private Sub pgpTxt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        oField = sender
    End Sub

    Private Sub txtSubject_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSubject.GotFocus
        oField = txtSubject
    End Sub

    Private Sub txtMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs)
        oField = txtMsg
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            If TypeOf oField Is TextBox Then
                oField.Undo()
            ElseIf TypeOf oField Is ComboBox Then
                SendKeys.SendWait("^Z")
            End If
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        If TypeOf oField Is TextBox Then
            oField.Cut()
        ElseIf TypeOf oField Is ComboBox Then
            SendKeys.SendWait("^X")
        End If

    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        If TypeOf oField Is TextBox Then
            oField.Copy()
        ElseIf TypeOf oField Is ComboBox Then
            SendKeys.SendWait("^C")
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        If TypeOf oField Is TextBox Then
            oField.Paste()
        ElseIf TypeOf oField Is ComboBox Then
            SendKeys.SendWait("^V")
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        'oField.SelectedText = oInsert.GetConstants(Me)

        oInsert.GetConstants(Me, sender.Container)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        If Dynamic = False Or IsStatic = True Then
            Dim oItem As New frmDataItems

            oField.SelectedText = oItem._GetDataItem(Me.eventID)
        Else
            Dim oIntruder As New frmInserter(0)

            oIntruder.GetDatabaseField(gTables, gsCon, Me)
        End If
    End Sub

    Private Sub txtDirectory_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDirectory.GotFocus
        oField = txtDirectory
    End Sub

    Private Sub txtDirectory_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtDirectory.MouseUp
        oField = txtDirectory
    End Sub

    Private Sub cmbFormat_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFormat.GotFocus
        If Me.DataDriven = True Then
            Me.oField = cmbFormat
        End If
    End Sub

    Private Sub changeNamingName(sender As System.Object, e As System.EventArgs)
        If optNaming.Checked = True And sTitle.Length > 0 And ucRptOptions.cmbFormat.Text.IndexOf("*") > -1 Then
            txtDefault.Text = sTitle & ucRptOptions.cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
        End If
    End Sub

    Private Sub cmbFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) ' Handles cmdProps.Click

        Return

        If cmbFormat.Text = "HTML (*.htm)" Then chkSplit.Enabled = True Else chkSplit.Enabled = False

        setError(sender, String.Empty)

        If optNaming.Checked = True And sTitle.Length > 0 And cmbFormat.Text.IndexOf("*") > -1 Then
            txtDefault.Text = sTitle & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
        End If

        If Me.Visible = True Then
            Dim opt As New frmRptOptions

            opt.m_eventBased = Me.m_eventBased
            opt.eventID = Me.eventID

            Select Case cmbFormat.Text
                Case "Acrobat Format (*.pdf)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic, , Bursting)
                Case "CSV (*.csv)"
                    If Me.m_IsQuery = False Then
                        opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                    Else
                        opt.taskOptions(cmbFormat.Text.Split("(")(0).Trim, xDestinationID)
                    End If
                Case "HTML (*.htm)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "MS Excel - Data Only (*.xls)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic, sTitle, Bursting)
                Case "MS Excel 97-2000 (*.xls)"
                    If Me.m_IsQuery = False Then
                        opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic, sTitle, Bursting)
                    Else
                        opt.taskOptions(cmbFormat.Text.Split("(")(0).Trim, xDestinationID)
                    End If
                Case "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic, sTitle, Bursting)
                Case "MS Word (*.doc)", "MS Word - Editable (*.rtf)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "ODBC (*.odbc)"
                    'opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", oRpt, , xDestinationID, Dynamic)
                    ' MessageBox.Show("ODBC as a format is no longer supported in CRD. Please set up a destination to ODBC instead", _
                    'Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    'cmbFormat.Text = ""
                Case "Rich Text Format (*.rtf)", "TIFF (*.tif)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "Text (*.txt)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "Record Style (*.rec)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "TIFF (*.tif)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Single", , xDestinationID, Dynamic)
                Case "XML (*.xml)"
                    If Me.m_IsQuery = True Then
                        opt.taskOptions(cmbFormat.Text.Split("(")(0).Trim, xDestinationID)
                    End If
            End Select
        End If
    End Sub

    Private Sub chkEmbed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmbed.CheckedChanged
        txtMsg.Enabled = Not chkEmbed.Checked
        chkIncludeAttach.Visible = chkEmbed.Checked

        Dim temp As String = cmbMailFormat.Text

        If chkEmbed.Checked = False Then
            chkIncludeAttach.Checked = True
            chkIncludeAttach.Visible = False
            cmbMailFormat.Items.Clear()

            Dim formats As String() = New String() {"TEXT", "HTML", "HTML (Basic}"}
            cmbMailFormat.Items.AddRange(formats)
        Else
            chkIncludeAttach.Visible = True
            chkIncludeAttach.Checked = False
            cmbMailFormat.Items.Clear()

            Dim formats As String()= New String() {"TEXT", "HTML", "HTML (Basic}", "IMAGE"}
            cmbMailFormat.Items.AddRange(formats)

            If MailType = gMailType.SMTP Then
                cmbMailFormat.Items.Add("Web Archive")
            End If
        End If

        cmbMailFormat.Text = temp

        Me.btnEmbedOptions.Enabled = (chkEmbed.Checked And cmbMailFormat.Text.ToLower = "text")
    End Sub

    Private Sub optCustomName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustomName.CheckedChanged

        If optCustomName.Checked = True Then
            txtCustomName.Enabled = True
            txtDefault.Text = String.Empty
            Me.chkKeyParameter.Enabled = True
        Else
            txtCustomName.Enabled = False
            txtCustomName.Text = ""
            Me.chkKeyParameter.Enabled = False
        End If

        SetError(txtCustomName, String.Empty)
    End Sub

    Private Sub chkCustomExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomExt.CheckedChanged
        If chkCustomExt.Checked = True Then
            txtCustomExt.Enabled = True
        Else
            txtCustomExt.Enabled = False
            txtCustomExt.Text = ""
        End If

        SetError(txtCustomExt, String.Empty)
    End Sub

    Private Sub chkAppendDateTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAppendDateTime.CheckedChanged
        If chkAppendDateTime.Checked = True Then
            cmbDateTime.Enabled = True

            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

            txtAdjustStamp.Enabled = True
        Else
            cmbDateTime.Enabled = False
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Enabled = False
            txtAdjustStamp.Value = 0
        End If

        SetError(cmbDateTime, String.Empty)
    End Sub

    Private Sub txtCustomName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomName.TextChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub txtCustomExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomExt.TextChanged
        SetError(sender, String.Empty)
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        SetError(sender, String.Empty)


    End Sub

    Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click

    End Sub

    Private Sub cmdInsert_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert.MouseUp
        Try
            If oField Is Nothing Then Return
            mnuSubInsert.Show(cmdInsert, New Point(e.X, e.Y))
        Catch
        End Try
    End Sub

    Private Sub cmdInsert2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert2.MouseUp
        oField = txtDirectory
        mnuSubInsert.Show(cmdInsert2, New Point(e.X, e.Y))
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub txtAttach_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttach.GotFocus
        oField = txtAttach
    End Sub

    Private Sub txtCustomName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomName.GotFocus
        oField = txtCustomName
    End Sub

    Private Sub txtCustomExt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomExt.GotFocus
        oField = txtCustomExt
    End Sub

    Private Sub chkZip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZip.CheckedChanged
        
        grpZip.Enabled = chkZip.Checked

        If Package And chkZip.Checked Then
            stabNaming.Visible = True
            stabDestination.Tabs.Remove(stabNaming)
            stabDestination.Tabs.Insert(3, stabNaming)
        Else
            stabNaming.Visible = False
        End If

    End Sub

    Private Sub chkMerge_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSplit.CheckedChanged
        If chkSplit.Checked = True Then
            chkEmbed.Checked = False
        End If
    End Sub

    Private Sub mnuDefMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub mnuSignature_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSignature.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub





    Private Sub mnuDefSubject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefSubject.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub optNaming_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNaming.CheckedChanged
        Try
            If optNaming.Checked = True Then
                If optNaming.Checked = True And sTitle.Length > 0 Then
                    txtDefault.Text = sTitle & ucRptOptions.cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
                End If
            End If
        Catch : End Try
    End Sub

    Private Sub chkDefer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDefer.CheckedChanged
        If chkDefer.Checked = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.fr2_DeferredDelivery) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.PLATINUM, chkDefer, "Deferred Delivery")
                chkDefer.Checked = False
                Return
            End If
        End If

        txtDefer.Enabled = chkDefer.Checked
    End Sub

    Private Sub lsvPrinters_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvPrinters.DoubleClick
        If lsvPrinters.SelectedItems.Count = 0 Then Return

        Dim printer As ListViewItem = lsvPrinters.SelectedItems(0)
        Dim oPrint As New frmPrinterAttr

        Dim hs As Hashtable = oPrint._EditPrinter(lsvPrinters.SelectedItems(0).Tag)

        If hs IsNot Nothing Then
            printer.SubItems(0).Text = hs("Name")
            printer.SubItems(1).Text = hs("Copies")
        End If
    End Sub

    Private Sub chkHouseKeeping_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHouseKeeping.CheckedChanged
        If IsFeatEnabled(gEdition.PLATINUM, modFeatCodes.sa7_FolderHousekeeping) = False And chkHouseKeeping.Checked = True Then
            _NeedUpgrade(MarsGlobal.gEdition.PLATINUM, chkHouseKeeping, "House Keeping")
            chkHouseKeeping.Checked = False
            Return
        End If

        grpHouseKeeping.Enabled = chkHouseKeeping.Checked
    End Sub


    Private Sub cmbServer_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbServer.DropDown
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        SQL = "SELECT * FROM SMTPServers"

        cmbServer.Items.Clear()
        cmbServer.Items.Add("Default")

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            cmbServer.Items.Add(oRs("smtpname").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()


    End Sub


    Private Sub cmbFaxDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFaxDevice.SelectedIndexChanged

    End Sub

    Private Sub cmbFaxDevice_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFaxDevice.DropDown
        If cmbFaxDevice.Items.Count > 0 Then Return

        Try
            Dim modems As DataTech.FaxManJr.Modems = New DataTech.FaxManJr.Modems
            Dim sText As String

            clsMarsUI.BusyProgress(90, "Enumerating modems...")

            modems.AutoDetect()

            clsMarsUI.BusyProgress(100, "Enumerating modems...", True)

            For Each modem As DataTech.FaxManJr.Modem In modems
                sText = ""

                Select Case modem.FaxClass
                    Case DataTech.FaxManJr.FaxClass.Class1
                        sText = "Class1"
                    Case DataTech.FaxManJr.FaxClass.Class2
                        sText = "Class2"
                    Case DataTech.FaxManJr.FaxClass.Class20
                        sText = "Class20"
                    Case DataTech.FaxManJr.FaxClass.Class21
                        sText = "Class21"
                End Select

                sText &= " Device on Port:" & modem.Port

                Me.cmbFaxDevice.Items.Add(sText)
            Next


            'Static i As Integer
            'Dim desc As FaxmanJr.DeviceDesc
            'Dim sText As String

            'If AxFaxFinder1.DeviceCount = 0 Then
            '    SetError(cmbFaxDevice, "No Fax Devices Detected.")
            '    Exit Sub
            'End If

            'If i > (AxFaxFinder1.DeviceCount - 1) Then
            '    i = 0
            'End If

            'For i = 0 To AxFaxFinder1.DeviceCount - 1
            '    desc = AxFaxFinder1.Item(i)

            '    sText = ""

            '    'set the active com port
            '    AxFaxmanJr1.Port = desc.Port

            '    If desc.bClass1 Then
            '        sText = "Class1"
            '    End If

            '    If desc.bClass2 Then
            '        sText = "Class2"
            '    End If

            '    If desc.bClass20 Then
            '        sText = "Class20"
            '    End If

            '    sText &= " Device on Port:" & desc.Port

            '    cmbFaxDevice.Items.Add(sText)
            'Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub mnuAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAttachment.Click, MenuItem12.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefAttach", "")
    End Sub


    Private Sub cmdFaxTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFaxTo.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "Fax"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtFaxNumber.Text.EndsWith(";") = False And txtFaxNumber.Text.Length > 0 _
        Then txtFaxNumber.Text &= ";"

        Try
            txtFaxNumber.Text &= sValues(0)
        Catch
        End Try

    End Sub

    Private Sub txtFaxTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFaxTo.GotFocus, txtFaxFrom.GotFocus, txtFaxComments.GotFocus, txtFaxNumber.GotFocus

        oField = CType(sender, TextBox)
    End Sub

    Private Sub txtFaxTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFaxTo.TextChanged
        Dim I As Integer = 0

        Try
            For Each s As String In txtFaxTo.Text.Split(";")
                If s.Length > 0 Then I += 1
            Next

            If I > 1 Then
                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa9_MultipleDestinations) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, txtFaxTo, "Multiple Destinations")
                    txtFaxTo.Text = txtFaxTo.Text.Split(";")(0)
                End If
            End If
        Catch : End Try
    End Sub



    Private Sub cmbMailFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMailFormat.SelectedIndexChanged, btnEmbedOptions.Click

        If Me.chkEmbed.Checked And Me.chkIncludeAttach.Checked = False Then
            ucRptOptions.cmbFormat.Text = Me.cmbMailFormat.Text & " Embeded"
        End If

        If cmbMailFormat.Text.ToLower = "text" And chkEmbed.Checked = True Then
            Me.btnEmbedOptions.Enabled = True
        Else
            Me.btnEmbedOptions.Enabled = False
        End If

        If cmbMailFormat.Text.ToLower = "text" Then
            txtMsg.bodyFormat = ucEditorX.format.TEXT
        Else
            txtMsg.bodyFormat = ucEditorX.format.HTML
        End If

    End Sub

    Private Sub chkZipSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZipSecurity.CheckedChanged
        'If chkZipSecurity.Checked = True Then
        '    If gnEdition < MarsGlobal.gEdition.ENTERPRISEPROPLUS Then
        '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
        '        chkZipSecurity.Checked = False
        '        Return
        '    End If
        'End If

        Panel1.Enabled = chkZipSecurity.Checked
    End Sub

    Private Sub chkIncludeAttach_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeAttach.CheckedChanged
        ucRptOptions.Enabled = chkIncludeAttach.Checked

        If chkIncludeAttach.Checked = False And Me.Visible = True Then
            ucRptOptions.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
            ucRptOptions.cmbFormat.Text = cmbMailFormat.Text & " Embeded"
            stabMiscPanel.Enabled = False
        ElseIf chkIncludeAttach.Checked = True And Me.Visible = True Then
            ucRptOptions.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
            ucRptOptions.cmbFormat.Text = ""
            stabMiscPanel.Visible = True
        End If
    End Sub

    Private Sub chkPGP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPGP.CheckedChanged
        'If gnEdition < MarsGlobal.gEdition.ENTERPRISEPROPLUS And chkPGP.Checked Then
        '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
        '    chkPGP.Checked = False
        '    Return
        'End If

        UcPGP.Enabled = chkPGP.Checked
    End Sub

    Private Sub cmbDUN_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDUN.DropDown
        Try
            Dim pbk As RasPhoneBook = New RasPhoneBook

            pbk.Open(RasPhoneBook.GetPhoneBookPath(RasPhoneBookType.AllUsers))

            cmbDUN.Items.Clear()

            For Each entry As RasEntry In pbk.Entries
                cmbDUN.Items.Add(entry.Name)
            Next
        Catch : End Try

    End Sub

    Private Sub chkDUN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDUN.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.o2_FileTransfer) = False And chkDUN.Checked = True Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, chkDUN, "Remote File Transfer")
            chkDUN.Checked = False
        End If

        GroupBox7.Enabled = chkDUN.Checked
    End Sub

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If cmbDUN.Text.Length = 0 Then Return

        If cmdConnect.Text = "Connect" Then
            cmdConnect.Enabled = False
            If oNet._DialConnection(cmbDUN.Text) = True Then
                MessageBox.Show("Connected successfully")
                cmdConnect.Text = "Disconnect"
                cmdConnect.Enabled = True
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                cmdConnect.Enabled = True
            End If

        Else
            oNet._Disconnect()
            cmdConnect.Text = "Connect"
        End If
    End Sub

    Private Sub txtSMSMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSMsg.GotFocus
        oField = txtSMSMsg
    End Sub

    Private Sub _SetFormats(ByVal sType As String)
        Dim s() As String

        If sType = "SMS" Then
            s = New String() {"CSV (*.csv)", "Tab Seperated (*.txt)", "Text (*.txt)", "Record Style (*.rec)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf IsSmart = True Then
            s = New String() {"MS Excel (*.xls)", "XML (*.xml)", "Acrobat Format (*.pdf)", "HTML (*.htm)", "JPEG (*.jpg)", "TIFF (*.tif)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf Me.m_IsQuery = True Then
            s = New String() {"MS Excel 97-2000 (*.xls)", "XML (*.xml)", "CSV (*.csv)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        Else
            'if you ever add a format here then also add it to clsmarsreport2.validateformat

            s = New String() {"Acrobat Format (*.pdf)", "Crystal Reports (*.rpt)", "CSV (*.csv)", _
            "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
            "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
            "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel - Data Only (*.xls)", _
            "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", "MS Excel 97-2000 (*.xls)", _
            "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Tab Separated (*.txt)", _
            "Text (*.txt)", "TIFF (*.tif)", "XML (*.xml)", "Record Style (*.rec)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""

#If CRYSTAL_VER >= 12 Then
            cmbFormat.Items.Add("MS Word - Editable (*.rtf)")
#End If
        End If
    End Sub

    Private Sub txtSMSMsg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMSMsg.TextChanged
        Try
            Label23.Text = "Characters left: " & 160 - (txtSMSMsg.Text.Length)
        Catch : End Try
    End Sub

    Private Sub cmdSMSTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSMSTo.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "SMS"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtSMSNumber.Text.EndsWith(";") = False And txtSMSNumber.Text.Length > 0 _
        Then txtSMSNumber.Text &= ";"

        Try
            txtSMSNumber.Text &= sValues(0)
        Catch : End Try
    End Sub

    Private Sub txtDefault_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDefault.GotFocus
        oField = txtDefault
    End Sub



    Private Sub btnODBC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnODBC.Click
        If UcDSN._Validate = False Then
            SetError(UcDSN.cmbDSN, "Invalid database details. Please check and try again")
            UcDSN.cmbDSN.Focus()
            txtTableName.Text = ""
            txtTableName.Enabled = False
        Else
            txtTableName.Enabled = True
        End If
    End Sub

    Private Sub chkReadReceipt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReadReceipt.CheckedChanged

        If chkReadReceipt.Checked Then
            If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.m3_ReadReceipts) = False Then
                _NeedUpgrade(gEdition.CORPORATE, chkReadReceipt, "Read Receipts")
                chkReadReceipt.Checked = False
                Return
            End If

            If formLoaded = True Then
                Dim Read As frmReadReciepts = New frmReadReciepts
                Read.AddReceipt(xDestinationID)
            End If
        End If

        btnReadReceipts.Enabled = chkReadReceipt.Checked

    End Sub

    Private Sub btnReadReceipts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReadReceipts.Click
        Dim Read As frmReadReciepts = New frmReadReciepts
        Read.AddReceipt(frmReadReciepts.recieptType.READ, xDestinationID)
    End Sub



    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        oInsert.GetConstants(Me)
    End Sub


    Private Sub btnAddPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPath.Click

        If Me.txtDirectory.Text = "" Then Return

        Dim itemFound As Boolean = False

        If lsvPaths.Items.Count >= 1 Then
            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa9_MultipleDestinations) = False Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO, btnAddPath, "Multiple Destinations")
                Return
            End If
        End If

        For Each item As ListViewItem In lsvPaths.Items
            If item.Text = Me.txtDirectory.Text Then
                itemFound = True
                Exit For
            End If
        Next

        If itemFound = False Then
            If txtDirectory.Text.EndsWith("\") = False Then txtDirectory.Text &= "\"
            lsvPaths.Items.Add(txtDirectory.Text)
            txtDirectory.Text = String.Empty
        End If
    End Sub

    Private Sub btnRemovePath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemovePath.Click
        If lsvPaths.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In lsvPaths.SelectedItems
            item.Remove()
            Me.txtDirectory.Text = item.Text
        Next
    End Sub

    Private Sub btnAddFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFTP.Click
        Dim ftpItem As ListViewItem
        Dim match As Boolean = False

        If UcFTP.txtFTPServer.Text = "" And (Dynamic = False Or IsStatic = True) Then
            SetError(UcFTP.txtFTPServer, "Please specify the FTP Server address")
            stabDestination.SelectedTab = stabSetup
            UcFTP.txtFTPServer.Focus()
            Return
        ElseIf UcFTP.txtUserName.Text = "" Then
            SetError(UcFTP.txtUserName, "Please specify the FTP username")
            stabDestination.SelectedTab = stabSetup
            UcFTP.txtUserName.Focus()
            Return
        ElseIf UcFTP.txtPassword.Text = "" Then
            SetError(UcFTP.txtPassword, "Please specify the FTP password")
            stabDestination.SelectedTab = stabSetup
            UcFTP.txtPassword.Focus()
            Return
        End If

        Dim FtpServer As String = UcFTP.txtFTPServer.Text & ":" & UcFTP.txtPort.Value

        ftpItem = New ListViewItem(FtpServer)

        With ftpItem.SubItems
            .Add(UcFTP.txtUserName.Text)
            .Add(_EncryptDBValue(UcFTP.txtPassword.Text))
            .Add(UcFTP.txtDirectory.Text)
            .Add(UcFTP.cmbFTPType.Text)
            .Add(Convert.ToInt32(UcFTP.chkPassive.Checked))
            .Add(UcFTP.m_ftpOptions)
        End With

        For Each item As ListViewItem In lsvFTP.Items
            If item.Equals(ftpItem) Then
                match = True
                Exit For
            End If
        Next

        If match = False Then
            lsvFTP.Items.Add(ftpItem)

            UcFTP.ResetAll()

            For Each it As ListViewItem In lsvFTP.Items
                If it.SubItems(1).Text = "" Then it.Remove()
            Next
        End If

        For Each it As ListViewItem In lsvFTP.Items
            If it.SubItems(1).Text = "" Then it.Remove()
        Next
    End Sub

    Private Sub btnRemoveFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveFTP.Click
        For Each item As ListViewItem In lsvFTP.SelectedItems
            Dim port As Integer = 0

            If item.Text.Contains(":") Then
                UcFTP.txtFTPServer.Text = item.Text.Split(":")(0)
                port = item.Text.Split(":")(1)
            Else
                UcFTP.txtFTPServer.Text = item.Text
            End If

            UcFTP.txtUserName.Text = item.SubItems(1).Text
            UcFTP.txtPassword.Text = _DecryptDBValue(item.SubItems(2).Text)
            UcFTP.txtDirectory.Text = item.SubItems(3).Text
            UcFTP.cmbFTPType.Text = item.SubItems(4).Text

            Try
                UcFTP.chkPassive.Checked = Convert.ToBoolean(Convert.ToInt32(item.SubItems(5).Text))
            Catch : End Try

            Try
                UcFTP.m_ftpOptions = item.SubItems(6).Text
            Catch : End Try

            If port <> 0 Then UcFTP.txtPort.Value = port

            item.Remove()

            For Each it As ListViewItem In lsvFTP.Items
                If it.SubItems(1).Text = "" Then it.Remove()
            Next
        Next
    End Sub

    Private Sub chkKeyParameter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkKeyParameter.CheckedChanged
        If Me.chkKeyParameter.Checked = True Then
            Try
                Me.txtCustomName.SelectedText = "<[m]Key Parameter Value>"
            Catch
                Me.txtCustomName.Text = "<[m]Key Parameter Value>"
            End Try
        Else
            Try
                Dim nstart As Integer = Me.txtCustomName.Text.IndexOf("<[m]Key Parameter Value>")
                Dim nend As Integer = nstart + 24

                If nstart > -1 Then
                    Me.txtCustomName.Text = Me.txtCustomName.Text.Remove(nstart, nend - nstart)
                End If
            Catch
                Me.txtCustomName.Text = ""
            End Try
        End If
    End Sub

    Private Sub cmbDateTime_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbDateTime.Validating
        For Each s As String In IO.Path.GetInvalidFileNameChars
            cmbDateTime.Text = cmbDateTime.Text.Replace(s, "")
        Next
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSenderName.GotFocus, txtSenderAddress.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub

    Private Sub txtSenderName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSenderName.TextChanged, txtSenderAddress.TextChanged
        Dim txt As DevComponents.DotNetBar.Controls.TextBoxX = CType(sender, DevComponents.DotNetBar.Controls.TextBoxX)

        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False Then
            If formLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, sender, "Multiple SMTP Servers")
            txtSenderName.Text = ""
            txtSenderAddress.Text = ""
        ElseIf clsMarsSecurity._HasGroupAccess("Customize Sender Information (SMTP)") = False And txt.Text <> "" Then
            txt.Text = ""
            txt.Enabled = False
            Return
        End If

    End Sub

    Private Sub SetField(ByVal sender As Object, ByVal e As System.EventArgs)
        If TypeOf sender Is TextBox Then
            oField = CType(sender, TextBox)
        ElseIf TypeOf sender Is ComboBox Then
            oField = CType(sender, ComboBox)
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa9_MultipleDestinations) = False And lsvSharePointLibs.Items.Count <> 0 Then
            If formLoaded = True Then _NeedUpgrade(gEdition.ENTERPRISEPRO, btnAdd, "Multiple Destinations")

            Return
        End If

        Dim sh As SharePointer.frmSharePointBrowser = New SharePointer.frmSharePointBrowser
        Dim spVal As Boolean
        Dim metadata As String = ""

        spVal = sh.GetSharePointLib(spServer, spUsername, spPassword, spLibrary, metadata)

        If spVal = True Then
            Dim it As ListViewItem = New ListViewItem
            it.Text = spServer
            it.SubItems.Add(spLibrary)
            it.SubItems.Add(spUsername)
            it.SubItems.Add(_EncryptDBValue(spPassword))
            it.SubItems.Add(metadata)

            Me.lsvSharePointLibs.Items.Add(it)
        End If
    End Sub



    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        For Each it As ListViewItem In Me.lsvSharePointLibs.SelectedItems
            it.Remove()
        Next
    End Sub

    Private Sub SimpleToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SimpleToolStripMenuItem.Click
        If lsvSharePointLibs.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = Me.lsvSharePointLibs.SelectedItems(0)

        If it IsNot Nothing Then
            Dim sh As SharePointer.frmSharePointBrowser = New SharePointer.frmSharePointBrowser
            Me.spServer = it.Text
            Me.spLibrary = it.SubItems(1).Text
            Me.spUsername = it.SubItems(2).Text
            Me.spPassword = _DecryptDBValue(it.SubItems(3).Text)

            Dim metadata As String = ""

            Try
                metadata = it.SubItems(4).Text
            Catch : End Try

            Dim ok As Boolean = sh.GetSharePointLib(spServer, spUsername, spPassword, spLibrary, metadata)

            If ok = True Then
                it.Text = spServer
                it.SubItems(1).Text = spLibrary
                it.SubItems(2).Text = spUsername
                it.SubItems(3).Text = _EncryptDBValue(spPassword)
                it.SubItems(4).Text = metadata
            End If
        End If
    End Sub

    Private Sub btnEdit_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnEdit.MouseClick
        mnuEditSharepoint.Show(btnEdit, New Point(e.X, e.Y))
    End Sub

    Private Sub AdvancedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AdvancedToolStripMenuItem.Click
        If lsvSharePointLibs.SelectedItems.Count = 0 Then Return

        Dim spEditor As frmEditSharePointFolder = New frmEditSharePointFolder
        Dim it As ListViewItem = lsvSharePointLibs.SelectedItems(0)

        Dim newLib As String = spEditor.editSharepointLib(it.SubItems(1).Text, it.SubItems(4).Text)

        If newLib IsNot Nothing Then
            it.SubItems(1).Text = newLib
        End If
    End Sub

    Private Sub btnDeliveryReciept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeliveryReciept.Click
        Dim Read As frmReadReciepts = New frmReadReciepts
        Read.AddReceipt(frmReadReciepts.recieptType.DELIVERY, xDestinationID)
    End Sub

    Private Sub chkDeliveryReciept_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeliveryReciept.CheckedChanged
        btnDeliveryReciept.Enabled = chkDeliveryReciept.Checked
    End Sub

    Private Sub btnPicker_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPicker.Click
        If m_inserter.IsDisposed Then
            m_inserter = New frmInserter(eventID)
        End If

        m_inserter.GetConstants(Me)
    End Sub


    Private Sub cmbDUN_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbDUN.SelectedIndexChanged

    End Sub

    Private Sub cmbCloundAccount_DropDown(sender As Object, e As EventArgs) Handles cmbCloundAccount.DropDown
        If cmbCloundAccount.Items.Count = 0 Then
            For Each s As String In clsDropbox.getAllStoredAccounts
                cmbCloundAccount.Items.Add(s)
            Next
        End If
    End Sub

    Private Sub btnBrowseCloud_Click(sender As Object, e As EventArgs) Handles btnBrowseCloud.Click
        Try
            If cmbCloundAccount.Text = "" Then
                ep.SetError(cmbCloundAccount, "You must select a Dropbox account first. You can set up Dropbox accounts in the Options screen.")
                Return
            End If

            Dim fbd As frmCloudBrowser = New frmCloudBrowser
            Dim db As dropboxAccount = New dropboxAccount(clsMarsParser.Parser.ParseString(cmbCloundAccount.Text))

            With fbd
                If .browseDropBoxFolders(db.token, db.secret) <> DialogResult.Cancel Then
                    txtCloudFolder.Text = .SelectedPath
                End If
            End With
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "Make sure that you have added it in Options > Cloud Storage.")
        End Try
    End Sub
End Class
