
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Or crystal_ver = 11.5 Or CRYSTAL_VER >= 11.6 Then
Imports My.Crystal11
#End If

Public Class ucDestination
    Inherits System.Windows.Forms.UserControl
    Public nReportID As Integer = 99999
    Public nPackID As Integer = 99999
    Public nSmartID As Integer = 99999
    Public eventID As Integer = 99999
    Public isDynamic As Boolean = False
    Public isPackage As Boolean = False
    Public isDataDriven As Boolean = False
    Public sTables() As String
    Public sCon As String
    Public DynamicDestination As String
    Public sReportTitle As String = ""
    Public DefaultDestinations As Boolean = False
#If CRYSTAL_VER < 11.6 Then
    Public oRpt As Object
#Else
    Public oRpt As object
#End If
    Public m_parameterList As ArrayList
    Public ExcelBurst As Boolean = False
    Dim delayDelete As Boolean = False
    Friend WithEvents btnUp As DevComponents.DotNetBar.ButtonX  'System.Windows.Forms.Button
    Friend WithEvents btnDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnEmail As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnDisk As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnFax As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnFTP As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnODBC As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnPrinter As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSharepoint As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents btnSMS As DevComponents.DotNetBar.ButtonItem
    Friend WithEvents ImageList2 As System.Windows.Forms.ImageList
    Friend WithEvents lsvDestination As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ColumnHeader1 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents ColumnHeader2 As DevComponents.AdvTree.ColumnHeader
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnAssign As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnDuplicate As DevComponents.DotNetBar.ButtonX
    Public StaticDest As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer


    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdImport As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucDestination))
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.btnEmail = New DevComponents.DotNetBar.ButtonItem()
        Me.btnDisk = New DevComponents.DotNetBar.ButtonItem()
        Me.btnFax = New DevComponents.DotNetBar.ButtonItem()
        Me.btnFTP = New DevComponents.DotNetBar.ButtonItem()
        Me.btnODBC = New DevComponents.DotNetBar.ButtonItem()
        Me.btnPrinter = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSharepoint = New DevComponents.DotNetBar.ButtonItem()
        Me.btnSMS = New DevComponents.DotNetBar.ButtonItem()
        Me.cmdImport = New DevComponents.DotNetBar.ButtonX()
        Me.btnDown = New DevComponents.DotNetBar.ButtonX()
        Me.btnUp = New DevComponents.DotNetBar.ButtonX()
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.lsvDestination = New DevComponents.AdvTree.AdvTree()
        Me.ColumnHeader1 = New DevComponents.AdvTree.ColumnHeader()
        Me.ColumnHeader2 = New DevComponents.AdvTree.ColumnHeader()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnAssign = New DevComponents.DotNetBar.ButtonX()
        Me.btnDuplicate = New DevComponents.DotNetBar.ButtonX()
        Me.btnDropbox = New DevComponents.DotNetBar.ButtonItem()
        CType(Me.lsvDestination, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(8, 61)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 13
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(8, 32)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 10
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(8, 3)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.btnEmail, Me.btnDisk, Me.btnFax, Me.btnFTP, Me.btnODBC, Me.btnPrinter, Me.btnSharepoint, Me.btnSMS, Me.btnDropbox})
        Me.cmdAdd.TabIndex = 9
        Me.cmdAdd.Text = "&Add"
        '
        'btnEmail
        '
        Me.btnEmail.GlobalItem = False
        Me.btnEmail.Image = CType(resources.GetObject("btnEmail.Image"), System.Drawing.Image)
        Me.btnEmail.Name = "btnEmail"
        Me.btnEmail.Text = "Email"
        '
        'btnDisk
        '
        Me.btnDisk.GlobalItem = False
        Me.btnDisk.Image = CType(resources.GetObject("btnDisk.Image"), System.Drawing.Image)
        Me.btnDisk.Name = "btnDisk"
        Me.btnDisk.Text = "Disk"
        '
        'btnFax
        '
        Me.btnFax.GlobalItem = False
        Me.btnFax.Image = CType(resources.GetObject("btnFax.Image"), System.Drawing.Image)
        Me.btnFax.Name = "btnFax"
        Me.btnFax.Text = "Fax"
        '
        'btnFTP
        '
        Me.btnFTP.GlobalItem = False
        Me.btnFTP.Image = CType(resources.GetObject("btnFTP.Image"), System.Drawing.Image)
        Me.btnFTP.Name = "btnFTP"
        Me.btnFTP.Text = "FTP"
        '
        'btnODBC
        '
        Me.btnODBC.GlobalItem = False
        Me.btnODBC.Image = CType(resources.GetObject("btnODBC.Image"), System.Drawing.Image)
        Me.btnODBC.Name = "btnODBC"
        Me.btnODBC.Text = "ODBC"
        '
        'btnPrinter
        '
        Me.btnPrinter.GlobalItem = False
        Me.btnPrinter.Image = CType(resources.GetObject("btnPrinter.Image"), System.Drawing.Image)
        Me.btnPrinter.Name = "btnPrinter"
        Me.btnPrinter.Text = "Printer"
        '
        'btnSharepoint
        '
        Me.btnSharepoint.GlobalItem = False
        Me.btnSharepoint.Image = CType(resources.GetObject("btnSharepoint.Image"), System.Drawing.Image)
        Me.btnSharepoint.Name = "btnSharepoint"
        Me.btnSharepoint.Text = "Sharepoint"
        '
        'btnSMS
        '
        Me.btnSMS.GlobalItem = False
        Me.btnSMS.Image = CType(resources.GetObject("btnSMS.Image"), System.Drawing.Image)
        Me.btnSMS.Name = "btnSMS"
        Me.btnSMS.Text = "SMS"
        '
        'cmdImport
        '
        Me.cmdImport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdImport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdImport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdImport.Location = New System.Drawing.Point(8, 90)
        Me.cmdImport.Name = "cmdImport"
        Me.cmdImport.Size = New System.Drawing.Size(75, 23)
        Me.cmdImport.TabIndex = 14
        Me.cmdImport.Text = "Import"
        '
        'btnDown
        '
        Me.btnDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDown.Location = New System.Drawing.Point(8, 148)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(75, 23)
        Me.btnDown.TabIndex = 14
        '
        'btnUp
        '
        Me.btnUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnUp.Location = New System.Drawing.Point(8, 119)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(75, 23)
        Me.btnUp.TabIndex = 14
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(32, 32)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'lsvDestination
        '
        Me.lsvDestination.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.lsvDestination.AllowDrop = True
        Me.lsvDestination.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvDestination.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.lsvDestination.BackgroundStyle.Class = "TreeBorderKey"
        Me.lsvDestination.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvDestination.Columns.Add(Me.ColumnHeader1)
        Me.lsvDestination.Columns.Add(Me.ColumnHeader2)
        Me.lsvDestination.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.lsvDestination.Location = New System.Drawing.Point(0, 0)
        Me.lsvDestination.Name = "lsvDestination"
        Me.lsvDestination.NodesConnector = Me.NodeConnector1
        Me.lsvDestination.NodeStyle = Me.ElementStyle1
        Me.lsvDestination.PathSeparator = ";"
        Me.lsvDestination.Size = New System.Drawing.Size(366, 293)
        Me.lsvDestination.Styles.Add(Me.ElementStyle1)
        Me.lsvDestination.TabIndex = 15
        Me.lsvDestination.Text = "AdvTree1"
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Name = "ColumnHeader1"
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width.Absolute = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Name = "ColumnHeader2"
        Me.ColumnHeader2.Text = "Format"
        Me.ColumnHeader2.Width.Absolute = 150
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.cmdAdd, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdEdit, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnDown, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btnUp, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdDelete, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.cmdImport, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAssign, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.btnDuplicate, 0, 7)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(372, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(86, 293)
        Me.TableLayoutPanel1.TabIndex = 16
        '
        'btnAssign
        '
        Me.btnAssign.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAssign.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAssign.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAssign.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnAssign.Location = New System.Drawing.Point(8, 177)
        Me.btnAssign.Name = "btnAssign"
        Me.btnAssign.Size = New System.Drawing.Size(75, 23)
        Me.btnAssign.TabIndex = 10
        Me.btnAssign.Text = "&Share"
        '
        'btnDuplicate
        '
        Me.btnDuplicate.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDuplicate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDuplicate.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDuplicate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDuplicate.Location = New System.Drawing.Point(8, 206)
        Me.btnDuplicate.Name = "btnDuplicate"
        Me.btnDuplicate.Size = New System.Drawing.Size(75, 23)
        Me.btnDuplicate.TabIndex = 10
        Me.btnDuplicate.Text = "&Duplicate"
        '
        'btnDropbox
        '
        Me.btnDropbox.GlobalItem = False
        Me.btnDropbox.Image = Global.sqlrd.My.Resources.Resources.dropbox
        Me.btnDropbox.Name = "btnDropbox"
        Me.btnDropbox.Text = "Dropbox"
        '
        'ucDestination
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.lsvDestination)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucDestination"
        Me.Size = New System.Drawing.Size(458, 293)
        CType(Me.lsvDestination, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim oData As New clsMarsData
    Dim eventBased As Boolean = False
    Dim m_tobeDeleted As ArrayList = New ArrayList
    Dim IsQuery As Boolean
    Public disableEmbed As Boolean
    Public burstingMode As Object
    Public bursting As Boolean = False

    Public ReadOnly Property destinationCount() As Integer
        Get
            Return Me.lsvDestination.Nodes.Count
        End Get
    End Property

    Private m_destinationsPicker As Boolean
    Public Property destinationsPicker() As Boolean
        Get
            Return m_destinationsPicker
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                TableLayoutPanel1.Visible = False
                lsvDestination.Dock = DockStyle.Fill
            Else
                TableLayoutPanel1.Visible = True
                btnAssign.Visible = False
            End If

            m_destinationsPicker = value
        End Set
    End Property

    Public Property m_IsQuery() As Boolean
        Get
            Return Me.IsQuery
        End Get
        Set(ByVal value As Boolean)
            Me.IsQuery = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Public Property m_IsDynamic() As Boolean
        Get
            Return isDynamic
        End Get
        Set(ByVal value As Boolean)
            isDynamic = value
        End Set
    End Property

    Public Property m_isPackage() As Boolean
        Get
            Return isPackage
        End Get
        Set(ByVal value As Boolean)
            isPackage = value
        End Set
    End Property

    Public Property m_ExcelBurst() As Boolean
        Get
            Return ExcelBurst
        End Get
        Set(ByVal value As Boolean)
            ExcelBurst = value
        End Set
    End Property

    Public Property m_StaticDest() As Boolean
        Get
            Return StaticDest
        End Get
        Set(ByVal value As Boolean)
            StaticDest = value
        End Set
    End Property

    Dim canDisable As Boolean = True

    Public Property m_CanDisable() As Boolean
        Get
            Return canDisable
        End Get
        Set(ByVal value As Boolean)
            canDisable = value
        End Set
    End Property

    Public Property m_DelayDelete() As Boolean
        Get
            Return delayDelete
        End Get
        Set(ByVal value As Boolean)
            delayDelete = value
        End Set
    End Property

    Public ReadOnly Property m_checkedNodes As Integer
        Get
            Dim I As Integer = 0

            For Each n As DevComponents.AdvTree.Node In lsvDestination.Nodes
                If n.Checked Then
                    I += 1
                End If
            Next

            Return I
        End Get
    End Property
    Public Sub LoadAll()
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        lsvDestination.Nodes.Clear()

        If gRole.ToLower = "administrator" And DefaultDestinations = True Then
            btnAssign.Visible = True
        Else
            btnAssign.Visible = False

            Dim grp As usergroup = New usergroup(gRole)

            If grp.destinationAllowance = usergroup.e_destinationAllowance.DefaultOnly Then
                cmdAdd.Enabled = False
                cmdEdit.Enabled = False

                If DefaultDestinations Then cmdDelete.Enabled = False
            End If

            grp.dispose()
        End If


        SQL = "SELECT * FROM DestinationAttr WHERE ReportID =" & nReportID & " AND " & _
        "PackID = " & nPackID & " AND SmartID = " & nSmartID


        '//check the default destinations allowed for this group
        If gRole.ToLower <> "administrator" And DefaultDestinations = True Then
            Dim grp As usergroup = New usergroup(gRole)

            If grp.destinationAllowance = usergroup.e_destinationAllowance.DefaultOnly Then
                Dim allowedDefaults As ArrayList = grp.getUserAllowedDestinations
                Dim allowedStr As String = "null,"

                For Each dest As String In allowedDefaults
                    allowedStr &= dest & ","
                Next

                If allowedStr <> "" Then
                    allowedStr = allowedStr.Remove(allowedStr.Length - 1, 1)

                    SQL &= " AND DestinationID IN (" & allowedStr & ")"
                End If
            End If

            grp.dispose()
        End If

        SQL &= " ORDER BY DestOrderID"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Exit Sub

        Dim bAtLeastOneChecked As Boolean = False

        Dim emailImage, diskImage, printerImage, ftpImage, faxImage, smsImage, odbcImage, sharepointImage, dropBoxImage As Image
        Dim imageSize As Size = New Size(20, 20)

        emailImage = resizeImage(Image.FromFile(getAssetLocation("mail.png")), imageSize)
        diskImage = resizeImage(Image.FromFile(getAssetLocation("disk.png")), imageSize)
        printerImage = resizeImage(Image.FromFile(getAssetLocation("printer.png")), imageSize)
        ftpImage = resizeImage(Image.FromFile(getAssetLocation("ftp.png")), imageSize)
        faxImage = resizeImage(Image.FromFile(getAssetLocation("fax.png")), imageSize)
        smsImage = resizeImage(Image.FromFile(getAssetLocation("sms.png")), imageSize)
        odbcImage = resizeImage(Image.FromFile(getAssetLocation("odbc.png")), imageSize)
        sharepointImage = resizeImage(Image.FromFile(getAssetLocation("sharepoint.png")), imageSize)
        dropBoxImage = resizeImage(My.Resources.dropbox_large, imageSize)

        Do While oRs.EOF = False
            Dim nID As Integer = oRs("destinationid").Value

            If Me.m_tobeDeleted.Contains(nID) = True Then GoTo skip

            Dim oItem As DevComponents.AdvTree.Node = New DevComponents.AdvTree.Node

            oItem.Text = oRs("destinationname").Value
            oItem.Tag = oRs("destinationid").Value

            oItem.CheckBoxVisible = True ' Not m_destinationsPicker

            Select Case CType(oRs("destinationtype").Value.ToLower, String)
                Case "email"
                    oItem.Image = emailImage
                Case "disk"
                    oItem.Image = diskImage
                Case "printer"
                    oItem.Image = printerImage
                Case "ftp"
                    oItem.Image = ftpImage
                Case "fax"
                    oItem.Image = faxImage
                Case "sms"
                    oItem.Image = smsImage
                Case "odbc"
                    oItem.Image = odbcImage
                Case "sharepoint"
                    oItem.Image = sharepointImage
                Case "dropbox"
                    oItem.Image = dropBoxImage
            End Select

            oItem.Cells.Add(New DevComponents.AdvTree.Cell(oRs("outputformat").Value))

            If destinationsPicker = False Then
                If IsNull(oRs("enabledstatus").Value, "1") = "1" Then
                    oItem.Checked = True
                    bAtLeastOneChecked = True
                Else
                    oItem.Checked = False
                End If
            Else
                oItem.Checked = False
            End If

            lsvDestination.Nodes.Add(oItem)
skip:
            oRs.MoveNext()
        Loop

        'Always make sure at least one destination is enabled
        If destinationsPicker = False Then
            If (lsvDestination.Nodes.Count > 0 And bAtLeastOneChecked = False) Then
                lsvDestination.Nodes(0).Checked = True
            End If
        End If

        oRs.Close()
    End Sub

    Public Sub cmdAdd_Click(sender As Object, e As EventArgs) Handles btnDisk.Click, btnEmail.Click, btnFTP.Click, btnFax.Click, btnODBC.Click, btnPrinter.Click, btnSharepoint.Click, btnSMS.Click, btnDropbox.Click

        If ExcelBurst = True And lsvDestination.Nodes.Count > 0 Then
            MessageBox.Show("A schedule set to burst groups to excel can only have one destination", _
                    Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Exclamation)
            Return
        ElseIf lsvDestination.Nodes.Count >= 1 Then
            If DefaultDestinations = False Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.sa9_MultipleDestinations) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, cmdAdd, "Multiple Destinations")
                    Return
                End If
            End If
        End If

        If isDynamic = True And StaticDest = False And lsvDestination.Nodes.Count > 0 Then
            StaticDest = True
            DynamicDestination = ""
        ElseIf StaticDest = True Then
            DynamicDestination = ""
        End If

        setError(lsvDestination, String.Empty)

        Dim oAdd As New frmDestinationSelect
        oAdd.eventID = Me.eventID
        oAdd.disableEmbed = Me.disableEmbed
        oAdd.m_eventBased = Me.m_eventBased
        oAdd.m_IsQuery = Me.IsQuery
        Dim destinationType As String

        If TypeOf sender Is DevComponents.DotNetBar.ButtonItem Then
            Dim btn As DevComponents.DotNetBar.ButtonItem = CType(sender, DevComponents.DotNetBar.ButtonItem)


            Select Case btn.Name
                Case "btnDisk"
                    destinationType = "Disk"
                Case "btnEmail"
                    destinationType = "Email"
                Case "btnFax"
                    destinationType = "Fax"
                Case "btnFTP"
                    destinationType = "FTP"
                Case "btnODBC"
                    destinationType = "ODBC"
                Case "btnPrinter"
                    destinationType = "Printer"
                Case "btnSharepoint"
                    destinationType = "SharePoint"
                Case "btnSMS"
                    destinationType = "SMS"
                Case "btnDropbox"
                    destinationType = "Dropbox"
            End Select
        End If

        oAdd.AddDestination(lsvDestination.Nodes.Count, nReportID, nPackID, _
        isDynamic, isPackage, DynamicDestination, sReportTitle, ExcelBurst, StaticDest, nSmartID, Me.bursting, Me.isDataDriven, destinationType)

        LoadAll()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click

        If lsvDestination.SelectedNodes.Count = 0 Then Return

        Dim oEdit As New frmDestinationSelect
        Dim nID As Integer

        nID = lsvDestination.SelectedNode.Tag
        oEdit.m_eventBased = Me.m_eventBased
        oEdit.disableEmbed = Me.disableEmbed
        oEdit.eventID = Me.eventID
        oEdit.m_IsQuery = Me.IsQuery
        oEdit.EditDestinaton(nID, isDynamic, isPackage, sReportTitle, ExcelBurst, StaticDest, nSmartID, Me.bursting, Me.isDataDriven)

        LoadAll()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvDestination.SelectedNodes.Count = 0 Then Return

        If Me.isDynamic = True Then
            Dim lsv As DevComponents.AdvTree.Node = Me.lsvDestination.SelectedNode

            If lsv.Index = 0 And StaticDest = False Then
                MessageBox.Show("You cannot delete the key destination from the Dynamic schedule unless you have set your linking to a static destination", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                Return
            End If
        End If

        Dim nID As Integer

        nID = lsvDestination.SelectedNode.Tag

        If Me.m_DelayDelete = False Then
            If DeleteDestination(nID) = True Then
                lsvDestination.SelectedNode.Remove()
            End If
        Else
            Me.m_tobeDeleted.Add(nID)
            lsvDestination.SelectedNode.Remove()
        End If
    End Sub

    Public Sub doDeletions()
        Try
            For Each nID As Integer In Me.m_tobeDeleted
                DeleteDestination(nID)
            Next
        Catch : End Try
    End Sub
    Private Function DeleteDestination(ByVal nID As Integer) As Boolean
        Dim SQL As String

        SQL = "DELETE FROM DestinationAttr WHERE DestinationID = " & nID

        If clsMarsData.WriteData(SQL) = True Then

            SQL = "DELETE FROM PrinterAttr WHERE DestinationID = " & nID

            clsMarsData.WriteData(SQL, False)

            Return clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID = " & nID)
        Else
            Return False
        End If
    End Function



    Private Sub lsvDestination_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs)



    End Sub

    Private Sub lsvDestination_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim lsv As ListView = CType(sender, ListView)
        Dim Item As ListViewItem

        If lsv IsNot Nothing Then

            Item = lsv.SelectedItems(0)

            If Item IsNot Nothing Then
                If Item.Checked Then
                    Item.Checked = False
                Else
                    Item.Checked = True
                End If
            End If
        End If

        cmdEdit_Click(sender, e)
    End Sub

    Private Sub cmdImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImport.Click

        'If isDynamic = True And lsvDestination.Items.Count > 0 And StaticDest = False Then
        '    MessageBox.Show("A dynamic schedule can only have one destination", _
        '            Application.ProductName, MessageBoxButtons.OK, _
        '            MessageBoxIcon.Exclamation)
        '    Return
        'Else
        If lsvDestination.Nodes.Count >= 1 Then

            If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa9_MultipleDestinations) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, cmdAdd, "Multiple Destinations")
                Return
            End If

        End If

        Dim oDefault As New frmTestDestination
        Dim nID As Integer


        nID = oDefault._ImportDestination

        If nID = 0 Then Return

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim nDestID As Integer = clsMarsData.CreateDataID("destinationattr", "destinationid")
        Dim sOutput As String = ""

        sCols = "DestinationID,DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        "PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate],ReportID,PackID,FTPServer,FTPUserName," & _
        "FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        "Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy,SmartID,SMTPServer,UseBursting,UseDUN," & _
        "DUNName,ReadReceipts,FTPType,AdjustStamp,EnabledStatus,DestOrderID,SenderName,SenderAddress"

        sVals = nDestID & "," & _
        "DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        "PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate]," & nReportID & _
        "," & nPackID & ",FTPServer,FTPUserName," & _
        "FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        "Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy," & nSmartID & ",SMTPServer,UseBursting,UseDUN," & _
        "DUNName,ReadReceipts,FTPType,AdjustStamp,1," & lsvDestination.Nodes.Count & ",SenderName,SenderAddress"


        SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM DestinationAttr WHERE DestinationID = " & nID

        clsMarsData.WriteData(SQL)

        sCols = "KeyUserID,KeyPassword,KeyLocation,KeySize,KeyType,DestinationID,PGPID"

        sVals = "'" & clsMarsData.CreateDataID("pgpattr", "keyuserid") & "',KeyPassword,KeyLocation,KeySize,KeyType," & _
        nDestID & ",PGPID"

        SQL = "INSERT INTO PGPAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM PGPAttr WHERE DestinationID = " & nID

        clsMarsData.WriteData(SQL)

        sCols = "[Collate],Copies,DestinationID,Orientation,PageFrom,PagesPerReport,PageTo,PaperSize,PrinterDriver,PrinterID," & _
        "PrinterName,PrinterPort,PrintMethod"

        sVals = "[Collate],Copies," & nDestID & ",Orientation,PageFrom,PagesPerReport,PageTo,PaperSize,PrinterDriver," & clsMarsData.CreateDataID("printerattr", "printerid") & "," & _
        "PrinterName,PrinterPort,PrintMethod"

        SQL = "INSERT INTO PrinterAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM PrinterAttr WHERE DestinationID = " & nID

        clsMarsData.WriteData(SQL)

        If isPackage = True Then
            clsMarsData.WriteData("UPDATE DestinationAttr SET OutputFormat = 'Package' WHERE DestinationID =" & nDestID)
        End If

        LoadAll()
    End Sub



    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If lsvDestination.SelectedNodes.Count = 0 Then Return

        Dim oItem As DevComponents.AdvTree.Node = lsvDestination.SelectedNode

        If oItem.Index = 0 Then Return

        Dim oMove As DevComponents.AdvTree.Node = lsvDestination.Nodes(oItem.Index - 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & newIndex & " WHERE DestinationID =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & oldIndex & " WHERE DestinationID =" & moveID

        clsMarsData.WriteData(SQL)

        Me.LoadAll()

        For Each item As DevComponents.AdvTree.Node In lsvDestination.Nodes
            If item.Tag = itemID Then
                lsvDestination.SelectedNode = item
                item.EnsureVisible()
                Exit For
            End If
        Next

    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If lsvDestination.SelectedNodes.Count = 0 Then Return

        Dim oItem As DevComponents.AdvTree.Node = lsvDestination.SelectedNode

        If oItem.Index = lsvDestination.Nodes.Count - 1 Then Return

        Dim oMove As DevComponents.AdvTree.Node = lsvDestination.Nodes(oItem.Index + 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & newIndex & " WHERE DestinationID =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & oldIndex & " WHERE DestinationID =" & moveID

        clsMarsData.WriteData(SQL)

        Me.LoadAll()

        For Each item As DevComponents.AdvTree.Node In lsvDestination.Nodes
            If item.Tag = itemID Then
                lsvDestination.SelectedNode = item
                item.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub lsvDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEmail.Click

    End Sub

    Private Sub ucDestination_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If gRole.ToLower = "administrator" And DefaultDestinations = True Then
                btnAssign.Visible = True
                btnDuplicate.Visible = True
            Else
                btnAssign.Visible = False
                btnDuplicate.Visible = False

                Dim grp As usergroup = New usergroup(gRole)

                If grp.destinationAllowance = usergroup.e_destinationAllowance.DefaultOnly Then
                    cmdAdd.Enabled = False
                    cmdEdit.Enabled = False

                    If DefaultDestinations Then cmdDelete.Enabled = False
                End If

                grp.dispose()
            End If
        Catch : End Try
        ep = New ErrorProvider
    End Sub
    Private Sub btnDuplicate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDuplicate.Click
        If lsvDestination.SelectedNodes.Count = 0 Then Return

        Dim destinationID As Integer = lsvDestination.SelectedNodes(0).Tag
        Dim newID As Integer = clsMarsData.DataItem.CopyRow("destinationattr", destinationID, " WHERE destinationid = " & destinationID, "destinationid", _
                                       , , 2777, 2777, , , , destinationID, , , , 2777)

        clsMarsData.DataItem.CopyRow("reportoptions", destinationID, " WHERE destinationid = " & destinationID, "optionid", , , 2777, 2777, , , , newID)

        clsMarsData.DataItem.CopyRow("printerattr", destinationID, " WHERE destinationid = " & destinationID, "printerid", , , , , , , , newID)

        Me.LoadAll()

        For Each it As DevComponents.AdvTree.Node In lsvDestination.Nodes
            If it.Tag = newID Then
                lsvDestination.SelectedNode = it
                it.EnsureVisible()

                Exit For
            End If
        Next

        cmdEdit_Click(sender, e)
    End Sub

    Private Sub lsvDestination_AfterCheck(sender As Object, e As DevComponents.AdvTree.AdvTreeCellEventArgs) Handles lsvDestination.AfterCheck
        Try
            If destinationsPicker Then Return

            Dim oItem As DevComponents.AdvTree.Node = lsvDestination.SelectedNode

            Dim destinationID As Integer = oItem.Tag
            Dim SQL As String = ""

            If oItem.Checked Then '//reverse as the the event is thrown BEFORE The change is made
                SQL = "UPDATE DestinationAttr SET EnabledStatus = 1 WHERE DestinationID =" & destinationID
            Else
                SQL = "UPDATE DestinationAttr SET EnabledStatus = 0 WHERE DestinationID =" & destinationID
            End If

            clsMarsData.WriteData(SQL)
        Catch : End Try
    End Sub



    Private Sub cmdAdd_Click_1(sender As System.Object, e As System.EventArgs) Handles cmdAdd.Click

        If ExcelBurst = True And lsvDestination.Nodes.Count > 0 Then
            MessageBox.Show("A schedule set to burst groups to excel can only have one destination", _
                    Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Exclamation)
            Return
        ElseIf lsvDestination.Nodes.Count >= 1 Then
            If DefaultDestinations = False Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.sa9_MultipleDestinations) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO, lsvDestination, "Multiple Destinations")
                    Return
                End If
            End If
        End If

        If isDynamic = True And StaticDest = False And lsvDestination.Nodes.Count > 0 Then
            StaticDest = True
            DynamicDestination = ""
        ElseIf StaticDest = True Then
            DynamicDestination = ""
        End If

        setError(lsvDestination, String.Empty)

        Dim oAdd As New frmDestinationSelect
        oAdd.eventID = Me.eventID
        oAdd.disableEmbed = Me.disableEmbed
        oAdd.m_eventBased = Me.m_eventBased
        oAdd.m_IsQuery = Me.IsQuery




        oAdd.AddDestination(lsvDestination.Nodes.Count, nReportID, nPackID, _
        isDynamic, isPackage, DynamicDestination, sReportTitle, ExcelBurst, StaticDest, nSmartID, Me.bursting, Me.isDataDriven)

        LoadAll()
    End Sub



    Private Sub lsvDestination_DoubleClick1(sender As Object, e As System.EventArgs) Handles lsvDestination.DoubleClick
        If destinationsPicker Then Return

        If cmdEdit.Enabled Then cmdEdit_Click(sender, e)
    End Sub


    Private Sub btnAssign_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssign.Click
        If lsvDestination.SelectedNodes.Count = 0 Then
            Dim assigner As frmDefaultDestinationAssigner = New frmDefaultDestinationAssigner

            assigner.assignDestinationToGroup()
        Else
            Dim assignDest As frmAssignDestToGroup = New frmAssignDestToGroup

            assignDest.assignDestinationToGroup(lsvDestination.SelectedNodes(0).Tag)
        End If
    End Sub

 
End Class
