Friend Class ucXLConv
    Inherits System.Windows.Forms.UserControl
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents XLConv As AxXLSConverterX.AxXLSConverterXCtrl


    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ucXLConv))
        Me.XLConv = New AxXLSConverterX.AxXLSConverterXCtrl
        CType(Me.XLConv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'XLConv
        '
        Me.XLConv.Enabled = True
        Me.XLConv.Location = New System.Drawing.Point(56, 80)
        Me.XLConv.Name = "XLConv"
        Me.XLConv.OcxState = CType(resources.GetObject("XLConv.OcxState"), System.Windows.Forms.AxHost.State)
        Me.XLConv.Size = New System.Drawing.Size(32, 32)
        Me.XLConv.TabIndex = 0
        '
        'ucXLConv
        '
        Me.Controls.Add(Me.XLConv)
        Me.Name = "ucXLConv"
        CType(Me.XLConv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Enum XLConvertTypes
        Lotus123_4 = 38
        Lotus123_3 = 15
        Lotus123_1 = 5
        Lotus123_S = 4
        dBaseIV = 11
        dBaseIII = 8
        dBaseII = 7
        DIF = 9
    End Enum
    Public Function csvChangeCharacter(ByVal sChar As String, ByVal input As String, ByVal output As String) As Integer
        Return XLConv.csvChangeDelimitationCharacter(input, output, sChar)
    End Function

    Public Function csvAddDelimiter(ByVal sChar As String, ByVal sDelimiter As String, ByVal input As String, ByVal output As String) As Integer
        Try
            Dim final As String = ""

            Dim fields() As String

            fields = ReadTextFromFile(input).Split(sChar)

            For Each field As String In fields

                If field.EndsWith(vbCrLf) = False Then
                    field = sDelimiter & field & sDelimiter
                Else
                    field = field.Substring(0, field.Length - 2)

                    field = sDelimiter & field & sDelimiter & vbCrLf
                End If

                final &= field & sChar

            Next

            final = final.Substring(0, final.Length - 1)

            SaveTextToFile(final, output, , False, False)


            Return 0
        Catch
            Return -1
        End Try
    End Function
    Public Function ConvertXLFile(ByVal sFile As String, ByVal sOutput As String, ByVal sFormat As XLConvertTypes) As Object()
        Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)
        Dim n As Integer
        Dim oResult(1) As Object

        XLConv.Key = "00000373-C352-0000-0012-019E"

        n = XLConv.OpenXLSSaveAs(sFile, 1, sOutput, Convert.ToInt32(sFormat), False)
        oResult(0) = XLConv.ErrorString()
        oResult(1) = n

        XLConv.Dispose()

        oXL.Dispose()
        Return oResult
    End Function
    Public Function MergeXLFiles(ByVal sFiles() As String, ByVal sOutputFile As String) As Boolean

        Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)


        Try
            Dim szSheetName As String
            Dim sPrevSheet As String = ""
            Dim I As Integer = 0
            Dim sTemp As String = sOutputFile

            Try
                If System.IO.File.Exists(sOutputFile) Then
                    System.IO.File.Delete(sOutputFile)
                End If
            Catch : End Try



            For I = 0 To sFiles.GetUpperBound(0)
                If sFiles(I).Length > 0 Then
                    For x As Integer = 1 To oXL.GetWorksheetCount(sFiles(I))
                        szSheetName = oXL.GetWorkSheetName(sFiles(I), x)


                        If szSheetName = "Sheet1" Then
                            szSheetName = ExtractFileName(sFiles(I)).Replace(".xls", "")

                            oXL.SetWorkSheetName(sFiles(I), szSheetName, x)
                        End If


                    Next
                End If
            Next


            Dim ok As Boolean = oXL.MergeExcelFiles(sOutputFile, sFiles)

            If ok = False Then Throw New Exception("Error merging Excel workbooks: " & oXL.m_errorMsg)

            oXL.selectWorksheet(sOutputFile, 1)

            oXL.Dispose()

            Return True
        Catch ex As Exception
            Try
                oXL.Dispose()
            Catch : End Try

            Throw ex
        End Try
    End Function
    Public Function XLConvMergeXLFiles(ByVal sFiles() As String, ByVal sOutputFile As String) As Boolean
        Dim oXL As New ExcelMan.clsExcelMan(sAppPath, Process.GetCurrentProcess.Id, ExcelMan.clsExcelMan.XLFormats.Excel8)
        Dim szSheetName As String
        Dim sPrevSheet As String = ""
        Dim I As Integer = 0
        Dim sTemp As String = sOutputFile

        Try
            If System.IO.File.Exists(sOutputFile) Then
                System.IO.File.Delete(sOutputFile)
            End If
        Catch : End Try

        sOutputFile = sFiles(0)

        For I = 0 To sFiles.GetUpperBound(0)
            If sFiles(I).Length > 0 Then
                szSheetName = oXL.GetWorkSheetName(sFiles(I), 1)

                If szSheetName = "Sheet1" Then
                    szSheetName = ExtractFileName(sFiles(I)).Replace(".xls", "")
                End If

                _CopySheet(sFiles(I), szSheetName, sOutputFile)

            End If
        Next

        System.IO.File.Copy(sFiles(0), sTemp, True)

        oXL.selectWorksheet(sTemp, 1)

        oXL.Dispose()

        Return True

    End Function

    Private Sub _CopySheet(ByVal sFile As String, ByVal sSheetName As String, ByVal sOuputFile As String)
        Dim lReturn As Long

        XLConv.Key = "00000373-C352-0000-0012-019E"

        lReturn = XLConv.CopySheet(sFile, sSheetName, sOuputFile, sSheetName, "", "lastlast", 0)

    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub ucXLConv_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        XLConv.Key = "00000373-C352-0000-0012-019E"
    End Sub
End Class
