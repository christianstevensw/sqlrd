Imports Microsoft.Win32


Friend Class ucLoginDetails
    Inherits System.Windows.Forms.UserControl
    Public ep As New ErrorProvider
    Public cmdApply As Button
    Dim oReg As RegistryKey = Registry.LocalMachine
    'Public oRpt As object

    '#Const CRYSTAL_VER = 9
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtUserID)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtPassword)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(312, 64)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(8, 32)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(136, 21)
        Me.txtUserID.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "User ID"
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(168, 32)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(120, 21)
        Me.txtPassword.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(168, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Password"
        '
        'ucLoginDetails
        '
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucLoginDetails"
        Me.Size = New System.Drawing.Size(312, 64)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub chkLogin_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'GroupBox2.Enabled = chkLogin.Checked

        'If chkLogin.Checked = True Then
        '    Dim sUser As String
        '    Dim sPassword As String
        '    Dim sServer As String
        '    Dim sDatabase As String
        '    Dim oUse As Boolean
        '    Dim oUI As New clsMarsUI

        '    Try
        '        oUse = CType(oUI._ReadRegistry(oReg, sKey, "DefUse", 0), Boolean)
        '    Catch
        '        oUse = False
        '    End Try

        '    If oUse = True Then
        '        Dim oSec As New clsMarsSecurity
        '        If txtUserID.Text.Length = 0 Then txtUserID.Text = oUI._ReadRegistry(oReg, sKey, "DefUsername", "")

        '        Try
        '            If txtPassword.Text.Length = 0 Then txtPassword.Text = oSec._Decrypt(oUI._ReadRegistry(oReg, sKey, "DefPassword", ""))
        '        Catch : End Try

        '        If txtServer.Text.Length = 0 Then txtServer.Text = oUI._ReadRegistry(oReg, sKey, "DefServer", "")
        '        If txtDatabase.Text.Length = 0 Then txtDatabase.Text = oUI._ReadRegistry(oReg, sKey, "DefDatabase", "")
        '    End If
        '    '    Try
        '    '        '#If CRYSTAL_VER > 8.5 Then
        '    '        'txtUserID.Text = oRpt.Database.Tables(1).ConnectionProperties.Item("User ID").Value
        '    '        '#End If
        '    '    Catch : End Try
        '    'End If
        'End If

        'If chkLogin.Checked = False Then
        '    ep.SetError(txtUserID, String.Empty)
        '    ep.SetError(txtPassword, String.Empty)
        '    ep.SetError(txtServer, String.Empty)
        '    ep.SetError(txtDatabase, String.Empty)
        'End If
    End Sub

    Private Sub txtUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserID.TextChanged
        ep.SetError(sender, String.Empty)

        If Not cmdApply Is Nothing Then
            cmdApply.Enabled = True
        End If
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        ep.SetError(sender, String.Empty)

        If Not cmdApply Is Nothing Then
            cmdApply.Enabled = True
        End If
    End Sub

    Private Sub txtServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, String.Empty)

        If Not cmdApply Is Nothing Then
            cmdApply.Enabled = True
        End If
    End Sub

    Private Sub txtDatabase_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, String.Empty)

        If Not cmdApply Is Nothing Then
            cmdApply.Enabled = True
        End If
    End Sub
End Class
