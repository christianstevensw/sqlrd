Public Class frmDataItems
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Dim gItemList As ArrayList
    Dim ep As New ErrorProvider
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSearch As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Public m_EventID As Integer = 99999

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lsvItems As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDelete As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdEdit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAddWhere As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtValue As DevComponents.DotNetBar.Controls.TextBoxX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataItems))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtSearch = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdAddWhere = New DevComponents.DotNetBar.ButtonX()
        Me.txtValue = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDelete = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAdd = New DevComponents.DotNetBar.ButtonX()
        Me.lsvItems = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdEdit = New DevComponents.DotNetBar.ButtonX()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtSearch)
        Me.Panel2.Controls.Add(Me.cmdAddWhere)
        Me.Panel2.Controls.Add(Me.txtValue)
        Me.Panel2.Controls.Add(Me.cmdDelete)
        Me.Panel2.Controls.Add(Me.cmdAdd)
        Me.Panel2.Controls.Add(Me.lsvItems)
        Me.Panel2.Controls.Add(Me.cmdEdit)
        Me.Panel2.Location = New System.Drawing.Point(2, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(392, 336)
        Me.Panel2.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(8, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Search"
        '
        'txtSearch
        '
        '
        '
        '
        Me.txtSearch.Border.Class = "TextBoxBorder"
        Me.txtSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSearch.Location = New System.Drawing.Point(54, 13)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(250, 21)
        Me.txtSearch.TabIndex = 7
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddWhere.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(264, 272)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 6
        '
        'txtValue
        '
        '
        '
        '
        Me.txtValue.Border.Class = "TextBoxBorder"
        Me.txtValue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtValue.Location = New System.Drawing.Point(8, 304)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(296, 21)
        Me.txtValue.TabIndex = 3
        '
        'cmdDelete
        '
        Me.cmdDelete.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDelete.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(310, 104)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(72, 23)
        Me.cmdDelete.TabIndex = 2
        Me.cmdDelete.Text = "Remove"
        '
        'cmdAdd
        '
        Me.cmdAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(310, 40)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(72, 23)
        Me.cmdAdd.TabIndex = 1
        Me.cmdAdd.Text = "Add"
        '
        'lsvItems
        '
        '
        '
        '
        Me.lsvItems.Border.Class = "ListViewBorder"
        Me.lsvItems.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvItems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvItems.Location = New System.Drawing.Point(8, 40)
        Me.lsvItems.Name = "lsvItems"
        Me.lsvItems.Size = New System.Drawing.Size(296, 224)
        Me.lsvItems.TabIndex = 0
        Me.lsvItems.UseCompatibleStateImageBehavior = False
        Me.lsvItems.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Item Name"
        Me.ColumnHeader1.Width = 287
        '
        'cmdEdit
        '
        Me.cmdEdit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEdit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(310, 72)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(72, 23)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(244, 345)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(324, 345)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(416, 43)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 273)
        Me.ReflectionImage1.TabIndex = 3
        '
        'frmDataItems
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(544, 371)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmDataItems"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Items"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Sub ViewDataItems()
        Me.cmdAddWhere.Enabled = False
        Me.txtValue.Enabled = False

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.ShowInTaskbar = True
        Me.ControlBox = True
        Me.MaximizeBox = False

        Me.LoadAll()

        Me.Show()
    End Sub
    Public Function _GetDataItemValue(ByVal sName As String, Optional ByVal inParameter As Boolean = False, Optional parametersTable As Hashtable = Nothing) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sReturn As String

10:     sName = sName.ToLower.Replace("<[d]", String.Empty).Replace(">", String.Empty)

20:     SQL = "SELECT * FROM DataItems WHERE ItemName ='" & SQLPrepare(sName) & "'"

30:     oRs = clsMarsData.GetData(SQL)

        MarsCommon.SaveTextToFile("------------", sAppPath & "dataitem.debug", , True, True)
        MarsCommon.SaveTextToFile(Date.Now & ": Parsing " & sName, sAppPath & "dataitem.debug", , True, True)

40:     Try
50:         If oRs.EOF = False Then
                Dim sCon As String
                Dim sQuery As String
                Dim sDSN As String
                Dim sUser As String
                Dim sPassword As String
60:             Dim oCon As ADODB.Connection = New ADODB.Connection
                Dim AllowMultiple As Boolean = False
                Dim sxSep As String
70:             Dim oparse As New clsMarsParser
                Dim ReplaceNull, ReplaceEOF As Boolean
                Dim ReplaceNullValue, ReplaceEOFValue As String
                Dim del As String = ""
                Dim forParameter As Boolean = False


90:             sCon = oRs("constring").Value

100:            sDSN = sCon.Split("|")(0)
110:            sUser = sCon.Split("|")(1)
120:            sPassword = sCon.Split("|")(2)


                If sPassword.StartsWith("$$") And sPassword.EndsWith("$$") Then
                    sPassword = _DecryptDBValue(sPassword)
                End If

                Try
                    ReplaceNull = IsNull(oRs("replacenull").Value, 0)
                    ReplaceNullValue = IsNull(oRs("replacenullvalue").Value, "")

                    ReplaceEOF = IsNull(oRs("replaceeof").Value, 0)
                    ReplaceEOFValue = IsNull(oRs("replaceeofvalue").Value, "")
                Catch : End Try

                MarsCommon.SaveTextToFile(Date.Now & ": DSN = " & sDSN, sAppPath & "dataitem.debug", , True, True)
                MarsCommon.SaveTextToFile(Date.Now & ": UserID  = " & sUser, sAppPath & "dataitem.debug", , True, True)
                MarsCommon.SaveTextToFile(Date.Now & ": Replace Null =" & ReplaceNull, sAppPath & "dataitem.debug", , True, True)
                MarsCommon.SaveTextToFile(Date.Now & ": Replace Null Value =" & ReplaceNullValue, sAppPath & "dataitem.debug", , True, True)
                MarsCommon.SaveTextToFile(Date.Now & ": Replace if EOF =" & ReplaceEOF, sAppPath & "dataitem.debug", , True, True)
                MarsCommon.SaveTextToFile(Date.Now & ": Replace EOF with =" & ReplaceEOFValue, sAppPath & "dataitem.debug", , True, True)

130:            Try
140:                AllowMultiple = Convert.ToBoolean(oRs("allowmultiple").Value)
150:                sxSep = oRs("multiplesep").Value

160:                sxSep = sxSep.ToLower.Replace("<sqlrdcrlf>", vbCrLf)
170:            Catch ex As Exception
180:                AllowMultiple = False
                End Try

                Try
                    del = IsNull(oRs("FieldDelimiter").Value, "")
                Catch : End Try

                Try
                    forParameter = Convert.ToBoolean(Convert.ToInt32(IsNull(oRs("UseForParameters").Value, 0)))
                Catch : End Try

                sQuery = oparse.ParseString(oRs("sqlquery").Value, , Not (AllowMultiple), , , , parametersTable, False)

                MarsCommon.SaveTextToFile(Date.Now & ": Query = " & sQuery, sAppPath & "dataitem.debug", , True, True)

190:            oRs.Close()

200:            oCon.Open(sDSN, sUser, sPassword)

210:            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

220:            If oRs.EOF = False Then
                    MarsCommon.SaveTextToFile(Date.Now & ": Records found = True", sAppPath & "dataitem.debug", , True, True)

221:                If ReplaceNull = False Then ReplaceNullValue = ""

230:                If AllowMultiple = False Then
240:                    sReturn = IsNull(oRs.Fields(0).Value, ReplaceNullValue)
250:                Else
                        If inParameter = False Then
260:                        Do While oRs.EOF = False
270:                            sReturn &= del & IsNull(oRs(0).Value, ReplaceNullValue) & del & sxSep
280:                            oRs.MoveNext()
290:                        Loop
                        ElseIf inParameter = True And forParameter = True Then
                            Do While oRs.EOF = False
                                sReturn &= IsNull(oRs(0).Value, ReplaceNullValue) & PD
                                oRs.MoveNext()
                            Loop
                        End If
                    End If
300:            Else
                    MarsCommon.SaveTextToFile(Date.Now & ": Record Found = False", sAppPath & "dataitem.debug", , True, True)

301:                If ReplaceEOF = False Then
310:                    Dim emptyItem As Exception = New Exception("The data item '" & sName & "' did not return any value")

320:                    Throw emptyItem
                    Else
                        sReturn = ReplaceEOFValue
                    End If
                End If

                Try
330:                If AllowMultiple = True Then
                        If sReturn IsNot Nothing Then
                            If sReturn <> "" Then
340:                            sReturn = sReturn.Substring(0, sReturn.Length - sxSep.Length)
                            End If
                        End If
                    End If
                Catch : End Try

350:            oRs.Close()
360:            oCon.Close()

370:            oRs = Nothing
380:            oCon = Nothing

                MarsCommon.SaveTextToFile(Date.Now & ": Return Value = " & sReturn, sAppPath & "dataitem.debug", , True, True)

390:            Return sReturn
            End If
400:    Catch ex As Exception

            MarsCommon.SaveTextToFile(Date.Now & ": Error = " & ex.Message & "(Line " & Erl() & ")", sAppPath & "dataitem.debug", , True, True)

            If clsMarsReport.IsDynamicSchedule = False Then
410:            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 0)
            Else
                _ErrorHandle(clsMarsReport.sKeyParameter & ": " & clsMarsReport.sKeyValue & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 0)
            End If

420:        Return Nothing
        End Try
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim sItem As String
        Dim oItem As New frmEditDataItem

        oItem.m_eventID = Me.m_EventID

        sItem = oItem.AddDataItem

        If Not sItem Is Nothing Then
            LoadAll()
            Me.txtSearch.Text = ""
        End If
    End Sub

    Private Sub LoadAll()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM DataItems ORDER BY ItemName"

        oRs = clsMarsData.GetData(SQL)

        Dim imgL As ImageList = createImageList(New String() {"cube.png"})

        lsvItems.LargeImageList = imgL
        lsvItems.SmallImageList = imgL

        lsvItems.Items.Clear()

        gItemList = New ArrayList

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.Text = oRs("itemname").Value
                oItem.Tag = oRs("itemid").Value
                oItem.ImageIndex = 0

                lsvItems.Items.Add(oItem)

                gItemList.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim nID As Integer
        Dim oItem As New frmEditDataItem
        Dim sItem As String

        Try
            oItem.m_eventID = Me.m_EventID

            nID = lsvItems.SelectedItems(0).Tag

            sItem = oItem.EditDataItems(nID)

            If sItem Is Nothing Then Return

            LoadAll()

            Me.txtSearch.Text = ""
        Catch : End Try
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim nID As Integer

        Try
            nID = lsvItems.SelectedItems(0).Tag

            Dim SQL As String = "DELETE FROM DataItems WHERE ItemID =" & nID

            clsMarsData.WriteData(SQL)

            lsvItems.SelectedItems(0).Remove()
        Catch
        End Try
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Try
            txtValue.Text &= "<[d]" & lsvItems.SelectedItems(0).Text & ">"
        Catch
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtValue.Enabled = True Then
            If txtValue.Text.Length = 0 Then
                SetError(txtValue, "Please select at least one data item")
                txtValue.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Public Function _GetDataItem(ByVal eventID As Integer) As String

        Me.m_EventID = eventID
        LoadAll()
        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Return txtValue.Text
    End Function

  

    Private Sub frmDataItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE, Me, "Inserts")
            Close()
            Return
        End If

        FormatForWinXP(Me)
    End Sub

    Private Sub lsvItems_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvItems.DoubleClick
        If cmdAddWhere.Enabled = True Then
            Me.cmdAddWhere_Click(sender, e)
        Else
            Me.cmdEdit_Click(sender, e)
        End If
    End Sub

    Private Sub lsvItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvItems.SelectedIndexChanged

    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim searchTerm As String = Me.txtSearch.Text.ToLower

            If txtSearch.Text.Length > 0 Then
                For Each it As ListViewItem In Me.lsvItems.Items
                    Dim itemText As String = it.Text.ToLower

                    If itemText.Contains(searchTerm) = False Then
                        it.Remove()
                    End If
                Next
            Else
                If gItemList Is Nothing Then
                    LoadAll()
                Else
                    Me.lsvItems.Items.Clear()
                    Me.lsvItems.BeginUpdate()
                    For Each it As ListViewItem In gItemList
                        lsvItems.Items.Add(it)
                    Next
                    Me.lsvItems.EndUpdate()
                End If
            End If
        Catch : End Try
    End Sub
End Class
