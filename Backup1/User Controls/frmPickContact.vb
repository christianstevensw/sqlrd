Public Class frmPickContact
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim UserCancel As Boolean
    Public sMode As String = "Email"
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Dim oField As TextBox
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdBcc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvContacts As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents txtTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtCc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtBcc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdNew As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuNew As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuContact As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuGroup As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lsvContacts = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdTo = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCc = New DevComponents.DotNetBar.ButtonX()
        Me.cmdBcc = New DevComponents.DotNetBar.ButtonX()
        Me.txtTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtCc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtBcc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdOK = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNew = New DevComponents.DotNetBar.ButtonX()
        Me.mnuNew = New System.Windows.Forms.ContextMenu()
        Me.mnuContact = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.mnuGroup = New System.Windows.Forms.MenuItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvContacts
        '
        '
        '
        '
        Me.lsvContacts.Border.Class = "ListViewBorder"
        Me.lsvContacts.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvContacts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvContacts.FullRowSelect = True
        Me.lsvContacts.HideSelection = False
        Me.lsvContacts.Location = New System.Drawing.Point(8, 56)
        Me.lsvContacts.Name = "lsvContacts"
        Me.lsvContacts.Size = New System.Drawing.Size(392, 224)
        Me.lsvContacts.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lsvContacts.TabIndex = 0
        Me.lsvContacts.UseCompatibleStateImageBehavior = False
        Me.lsvContacts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Display Name"
        Me.ColumnHeader1.Width = 192
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Addresses"
        Me.ColumnHeader2.Width = 187
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Type in or select name from the list"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(8, 24)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(192, 21)
        Me.txtName.TabIndex = 2
        '
        'cmdTo
        '
        Me.cmdTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(8, 315)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(48, 21)
        Me.cmdTo.TabIndex = 3
        Me.cmdTo.Text = "To"
        '
        'cmdCc
        '
        Me.cmdCc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCc.Location = New System.Drawing.Point(8, 347)
        Me.cmdCc.Name = "cmdCc"
        Me.cmdCc.Size = New System.Drawing.Size(48, 21)
        Me.cmdCc.TabIndex = 3
        Me.cmdCc.Text = "Cc"
        '
        'cmdBcc
        '
        Me.cmdBcc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBcc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBcc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBcc.Location = New System.Drawing.Point(8, 379)
        Me.cmdBcc.Name = "cmdBcc"
        Me.cmdBcc.Size = New System.Drawing.Size(48, 21)
        Me.cmdBcc.TabIndex = 3
        Me.cmdBcc.Text = "Bcc"
        '
        'txtTo
        '
        '
        '
        '
        Me.txtTo.Border.Class = "TextBoxBorder"
        Me.txtTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTo.Location = New System.Drawing.Point(64, 315)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(336, 21)
        Me.txtTo.TabIndex = 2
        '
        'txtCc
        '
        '
        '
        '
        Me.txtCc.Border.Class = "TextBoxBorder"
        Me.txtCc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCc.Location = New System.Drawing.Point(64, 347)
        Me.txtCc.Name = "txtCc"
        Me.txtCc.Size = New System.Drawing.Size(336, 21)
        Me.txtCc.TabIndex = 2
        '
        'txtBcc
        '
        '
        '
        '
        Me.txtBcc.Border.Class = "TextBoxBorder"
        Me.txtBcc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtBcc.Location = New System.Drawing.Point(64, 379)
        Me.txtBcc.Name = "txtBcc"
        Me.txtBcc.Size = New System.Drawing.Size(336, 21)
        Me.txtBcc.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(96, 296)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(304, 8)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 288)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Message Recipients"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(8, 405)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 8)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "3dline"
        '
        'cmdOK
        '
        Me.cmdOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(252, 3)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 23)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(330, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdNew
        '
        Me.cmdNew.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNew.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNew.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNew.Location = New System.Drawing.Point(216, 24)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.Size = New System.Drawing.Size(75, 21)
        Me.cmdNew.TabIndex = 7
        Me.cmdNew.Text = "New..."
        '
        'mnuNew
        '
        Me.mnuNew.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuContact, Me.MenuItem2, Me.mnuGroup})
        '
        'mnuContact
        '
        Me.mnuContact.Index = 0
        Me.mnuContact.Text = "&Contact"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnuGroup
        '
        Me.mnuGroup.Index = 2
        Me.mnuGroup.Text = "&Group"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdOK)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 419)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(405, 30)
        Me.FlowLayoutPanel1.TabIndex = 8
        '
        'frmPickContact
        '
        Me.AcceptButton = Me.cmdTo
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(405, 449)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdTo)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lsvContacts)
        Me.Controls.Add(Me.cmdCc)
        Me.Controls.Add(Me.cmdBcc)
        Me.Controls.Add(Me.txtTo)
        Me.Controls.Add(Me.txtCc)
        Me.Controls.Add(Me.txtBcc)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmPickContact"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Contacts"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub frmPickContact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Function PickContact(ByVal sField As String) As String()
        LoadContacts()

        Select Case sField.ToLower
            Case "to"
                oField = txtTo
            Case "cc"
                oField = txtCc
            Case "bcc"
                oField = txtBcc
        End Select

        If sMode = "Fax" Or sMode = "SMS" Then
            txtCc.Enabled = False
            txtBcc.Enabled = False
            cmdBcc.Enabled = False
            cmdCc.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        Dim sReturn(2) As String

        sReturn(0) = txtTo.Text
        sReturn(1) = txtCc.Text
        sReturn(2) = txtBcc.Text

        Return sReturn
    End Function
    Private Sub LoadContacts()
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        'add groups

        Dim imgL As ImageList = createImageList(New String() {"group.png", "user.png"})

        lsvContacts.Items.Clear()

        lsvContacts.LargeImageList = imgL
        lsvContacts.SmallImageList = imgL
        SQL = "SELECT * FROM ContactAttr WHERE ContactType = 'group'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oItem As New ListViewItem
                Dim members As String = ""

                oItem.Text = oRs("contactname").Value
                oItem.ImageIndex = 0

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT emailaddress FROM ContactDetail WHERE ContactID =" & oRs("contactid").Value)

                If oRs1 IsNot Nothing Then
                    Do While oRs1.EOF = False
                        members &= oRs1("emailaddress").Value & ", "
                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                If members.EndsWith(", ") Then members = members.Remove(members.Length - 2, 2)

                oItem.SubItems.Add(members)
                lsvContacts.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        'add contacts

        SQL = "SELECT * FROM ContactAttr c INNER JOIN ContactDetail d ON " & _
        "c.ContactID = d.ContactID WHERE c.ContactType = 'contact'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oItem As New ListViewItem

                oitem.Text = oRs("contactname").Value

                If sMode = "Email" Then
                    oitem.SubItems.Add(oRs("emailaddress").Value)
                Else
                    oitem.SubItems.Add(oRs("faxnumber").Value)
                End If

                oitem.ImageIndex = 1

                lsvContacts.Items.Add(oitem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        Dim s As String = txtName.Text
        Dim n As Integer = s.Length

        Try
            If lsvContacts.Items.Count = 0 Then Return

            For Each oItem As ListViewItem In lsvContacts.Items
                oitem.Selected = False
            Next

            If s.Length = 0 Then Return

            For Each oItem As ListViewItem In lsvContacts.Items

                If oItem.Text.Substring(0, n).ToLower = s.ToLower Then
                    oItem.Selected = True
                    oItem.EnsureVisible()
                    Exit For
                End If
            Next
        Catch : End Try
    End Sub


    Private Sub lsvContacts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvContacts.DoubleClick
        If lsvContacts.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvContacts.SelectedItems(0)

        If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
        oField.Text &= ";"

        If sMode = "Email" Then
            oField.Text &= "<[a]" & oItem.Text & ">;"
        ElseIf sMode = "Fax" Then
            oField.Text &= "<[z]" & oItem.Text & ">;"
        ElseIf sMode = "SMS" Then
            oField.Text &= "<[y]" & oItem.Text & ">;"
        End If
    End Sub

    Private Sub cmdTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTo.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
            Then txtTo.Text &= ";"

            If sMode = "Email" Then
                txtTo.Text &= "<[a]" & oItem.Text & ">;"
            ElseIf sMode = "Fax" Then
                txtTo.Text &= "<[z]" & oItem.Text & ">;"
            ElseIf sMode = "SMS" Then
                txtTo.Text &= "<[y]" & oItem.Text & ">;"
            End If
        Next
    End Sub

    Private Sub cmdCc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCc.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtCc.Text.EndsWith(";") = False And txtCc.Text.Length > 0 _
            Then txtCc.Text &= ";"

            txtCc.Text &= "<[a]" & oItem.Text & ">;"
        Next
    End Sub

    Private Sub cmdBcc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBcc.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtBcc.Text.EndsWith(";") = False And txtBcc.Text.Length > 0 _
            Then txtBcc.Text &= ";"

            txtBcc.Text &= "<[a]" & oItem.Text & ">;"
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub




    Private Sub mnuContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuContact.Click
        Dim oContact As New frmAddContact
        Dim s As String = oContact.AddContact()

        If s.Length = 0 Then Return

        LoadContacts()

        For Each oitem As ListViewItem In lsvContacts.Items
            If oitem.Text.ToLower = s Then
                oitem.Selected = True
                oitem.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub mnuGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGroup.Click
        Dim oContact As New frmAddContact
        Dim s As String = oContact.AddGroup()

        If s.Length = 0 Then Return

        LoadContacts()

        For Each oitem As ListViewItem In lsvContacts.Items
            If oitem.Text.ToLower = s Then
                oitem.Selected = True
                oitem.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub cmdNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNew.Click

    End Sub

    Private Sub cmdNew_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdNew.MouseUp
        mnuNew.Show(cmdNew, New Point(e.X, e.Y))
    End Sub

    Private Sub lsvContacts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvContacts.SelectedIndexChanged

    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged

    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.GotFocus
        oField = txtTo
    End Sub

    Private Sub txtCc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCc.TextChanged

    End Sub

    Private Sub txtCc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCc.GotFocus
        oField = txtCc
    End Sub

    Private Sub txtBcc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBcc.TextChanged

    End Sub

    Private Sub txtBcc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBcc.GotFocus
        oField = txtBcc
    End Sub
End Class
