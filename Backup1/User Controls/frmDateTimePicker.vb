﻿Public Class frmDateTimePicker
    Dim hasCancelled As Boolean

    Public Property selectedDate As String
        Get
            Return ConDate(mcDate.SelectionStart)
        End Get
        Set(value As String)
            Try
                mcDate.SelectionStart = value
            Catch : End Try
        End Set
    End Property

    Public Property selectedDateTime As String
        Get
            Dim dt As New Date(mcDate.SelectionStart.Year, mcDate.SelectionStart.Month, mcDate.SelectionStart.Day, dtTime.Value.Hour, dtTime.Value.Minute, dtTime.Value.Second)

            Return ConDateTime(dt)
        End Get
        Set(value As String)
            Try
                Dim dt As Date = value

                mcDate.SelectionStart = dt

                dtTime.Value = value
            Catch : End Try
        End Set
    End Property

    Public Property includeTime As Boolean
        Get
            Return dtTime.Visible
        End Get
        Set(value As Boolean)
            dtTime.Visible = value
        End Set
    End Property

    Public Function pickDate(Optional selectedDate As String = "") As DialogResult

        Try
            Date.Parse(selectedDate)

            Dim dt As Date = selectedDate

            mcDate.SelectionStart = dt

            dtTime.Value = dt
        Catch ex As Exception
            dtTime.Value = Date.Now
        End Try

        Me.ShowDialog()

        If hasCancelled Then
            Return Windows.Forms.DialogResult.Cancel
        Else
            Return Windows.Forms.DialogResult.OK
        End If

    End Function

    Private Sub ButtonX2_Click(sender As System.Object, e As System.EventArgs) Handles btnCancel.Click
        hasCancelled = True
        Close()
    End Sub

    Private Sub btnOK_Click(sender As System.Object, e As System.EventArgs) Handles btnOK.Click
        Close()
    End Sub
End Class