<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUpdateAvail
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUpdateAvail))
        Me.Button1 = New DevComponents.DotNetBar.ButtonX()
        Me.OK_Button = New DevComponents.DotNetBar.ButtonX()
        Me.Cancel_Button = New DevComponents.DotNetBar.ButtonX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.chkOff = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtCurrent = New DevComponents.DotNetBar.LabelX()
        Me.lblNew = New DevComponents.DotNetBar.LabelX()
        Me.lblCurrent = New DevComponents.DotNetBar.LabelX()
        Me.txtNew = New DevComponents.DotNetBar.LabelX()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.wbUpdate = New System.Windows.Forms.WebBrowser()
        Me.GroupPanel1 = New DevComponents.DotNetBar.Controls.GroupPanel()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.GroupPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Button1.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.Button1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Button1.Location = New System.Drawing.Point(189, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Skip this Build"
        '
        'OK_Button
        '
        Me.OK_Button.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.OK_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.OK_Button.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.OK_Button.Location = New System.Drawing.Point(3, 3)
        Me.OK_Button.Name = "OK_Button"
        Me.OK_Button.Size = New System.Drawing.Size(87, 23)
        Me.OK_Button.TabIndex = 0
        Me.OK_Button.Text = "Yes"
        '
        'Cancel_Button
        '
        Me.Cancel_Button.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.Cancel_Button.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Cancel_Button.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel_Button.Location = New System.Drawing.Point(96, 3)
        Me.Cancel_Button.Name = "Cancel_Button"
        Me.Cancel_Button.Size = New System.Drawing.Size(87, 23)
        Me.Cancel_Button.TabIndex = 1
        Me.Cancel_Button.Text = "Not Now"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(12, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(363, 15)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "A new version of SQL-RD is available. Would you like to download it now?"
        '
        'chkOff
        '
        Me.chkOff.AutoSize = True
        '
        '
        '
        Me.chkOff.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkOff.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.chkOff.Location = New System.Drawing.Point(0, 423)
        Me.chkOff.Name = "chkOff"
        Me.chkOff.Size = New System.Drawing.Size(316, 15)
        Me.chkOff.TabIndex = 3
        Me.chkOff.Text = "Automatically check for product updates on program start-up"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.txtCurrent, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.lblNew, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.lblCurrent, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtNew, 1, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(15, 21)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(408, 53)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'txtCurrent
        '
        Me.txtCurrent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCurrent.AutoSize = True
        '
        '
        '
        Me.txtCurrent.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCurrent.Location = New System.Drawing.Point(207, 3)
        Me.txtCurrent.Name = "txtCurrent"
        Me.txtCurrent.Size = New System.Drawing.Size(63, 15)
        Me.txtCurrent.TabIndex = 5
        Me.txtCurrent.Text = "Latest Build:"
        '
        'lblNew
        '
        Me.lblNew.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblNew.AutoSize = True
        '
        '
        '
        Me.lblNew.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblNew.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNew.Location = New System.Drawing.Point(3, 29)
        Me.lblNew.Name = "lblNew"
        Me.lblNew.Size = New System.Drawing.Size(72, 16)
        Me.lblNew.TabIndex = 0
        Me.lblNew.Tag = "noformat"
        Me.lblNew.Text = "Latest Build:"
        '
        'lblCurrent
        '
        Me.lblCurrent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCurrent.AutoSize = True
        '
        '
        '
        Me.lblCurrent.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lblCurrent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurrent.Location = New System.Drawing.Point(3, 3)
        Me.lblCurrent.Name = "lblCurrent"
        Me.lblCurrent.Size = New System.Drawing.Size(109, 16)
        Me.lblCurrent.TabIndex = 1
        Me.lblCurrent.Tag = "noformat"
        Me.lblCurrent.Text = "Your Current Build:"
        '
        'txtNew
        '
        Me.txtNew.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNew.AutoSize = True
        '
        '
        '
        Me.txtNew.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtNew.Location = New System.Drawing.Point(207, 29)
        Me.txtNew.Name = "txtNew"
        Me.txtNew.Size = New System.Drawing.Size(63, 15)
        Me.txtNew.TabIndex = 5
        Me.txtNew.Text = "Latest Build:"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.OK_Button)
        Me.FlowLayoutPanel1.Controls.Add(Me.Cancel_Button)
        Me.FlowLayoutPanel1.Controls.Add(Me.Button1)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 396)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(567, 27)
        Me.FlowLayoutPanel1.TabIndex = 6
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(429, 114)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 238)
        Me.ReflectionImage1.TabIndex = 7
        '
        'wbUpdate
        '
        Me.wbUpdate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wbUpdate.Location = New System.Drawing.Point(0, 0)
        Me.wbUpdate.MinimumSize = New System.Drawing.Size(20, 20)
        Me.wbUpdate.Name = "wbUpdate"
        Me.wbUpdate.Size = New System.Drawing.Size(402, 304)
        Me.wbUpdate.TabIndex = 8
        '
        'GroupPanel1
        '
        Me.GroupPanel1.CanvasColor = System.Drawing.SystemColors.Control
        Me.GroupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007
        Me.GroupPanel1.Controls.Add(Me.wbUpdate)
        Me.GroupPanel1.Location = New System.Drawing.Point(15, 80)
        Me.GroupPanel1.Name = "GroupPanel1"
        Me.GroupPanel1.Size = New System.Drawing.Size(408, 310)
        '
        '
        '
        Me.GroupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2
        Me.GroupPanel1.Style.BackColorGradientAngle = 90
        Me.GroupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground
        Me.GroupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderBottomWidth = 1
        Me.GroupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.GroupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderLeftWidth = 1
        Me.GroupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderRightWidth = 1
        Me.GroupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.GroupPanel1.Style.BorderTopWidth = 1
        Me.GroupPanel1.Style.CornerDiameter = 4
        Me.GroupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded
        Me.GroupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.GroupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText
        Me.GroupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near
        '
        '
        '
        Me.GroupPanel1.StyleMouseDown.CornerType = DevComponents.DotNetBar.eCornerType.Square
        '
        '
        '
        Me.GroupPanel1.StyleMouseOver.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.GroupPanel1.TabIndex = 9
        '
        'frmUpdateAvail
        '
        Me.AcceptButton = Me.OK_Button
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.Cancel_Button
        Me.ClientSize = New System.Drawing.Size(567, 438)
        Me.Controls.Add(Me.GroupPanel1)
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.chkOff)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.EnableGlass = False
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmUpdateAvail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SQL-RD"
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.GroupPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents OK_Button As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Cancel_Button As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Button1 As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkOff As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCurrent As DevComponents.DotNetBar.LabelX
    Friend WithEvents lblNew As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCurrent As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtNew As DevComponents.DotNetBar.LabelX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents wbUpdate As System.Windows.Forms.WebBrowser
    Friend WithEvents GroupPanel1 As DevComponents.DotNetBar.Controls.GroupPanel

End Class
