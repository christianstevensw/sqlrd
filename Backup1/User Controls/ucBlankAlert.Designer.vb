<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucBlankAlert
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    Private components As System.ComponentModel.IContainer

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpBlank As System.Windows.Forms.GroupBox
    Friend WithEvents grpBlankAlert As System.Windows.Forms.GroupBox
    Friend WithEvents txtAlertTo As System.Windows.Forms.TextBox
    Friend WithEvents cmdAlertTo As System.Windows.Forms.Button
    Friend WithEvents txtBlankMsg As System.Windows.Forms.TextBox
    Friend WithEvents optAlert As System.Windows.Forms.RadioButton
    Friend WithEvents optAlertandReport As System.Windows.Forms.RadioButton
    Friend WithEvents optIgnore As System.Windows.Forms.RadioButton
    Friend WithEvents chkBlankReport As System.Windows.Forms.CheckBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents cmdInsert As System.Windows.Forms.Button
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.grpBlank = New System.Windows.Forms.GroupBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grpBlankAlert = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdInsert = New System.Windows.Forms.Button
        Me.txtAlertTo = New System.Windows.Forms.TextBox
        Me.cmdAlertTo = New System.Windows.Forms.Button
        Me.txtBlankMsg = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkCustomQuery = New System.Windows.Forms.CheckBox
        Me.optAlert = New System.Windows.Forms.RadioButton
        Me.optAlertandReport = New System.Windows.Forms.RadioButton
        Me.optIgnore = New System.Windows.Forms.RadioButton
        Me.chkBlankReport = New System.Windows.Forms.CheckBox
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.mnuDB = New System.Windows.Forms.MenuItem
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuConstants = New System.Windows.Forms.MenuItem
        Me.grpBlank.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.grpBlankAlert.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpBlank
        '
        Me.grpBlank.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpBlank.Controls.Add(Me.TabControl1)
        Me.grpBlank.Controls.Add(Me.optAlert)
        Me.grpBlank.Controls.Add(Me.optAlertandReport)
        Me.grpBlank.Controls.Add(Me.optIgnore)
        Me.grpBlank.Enabled = False
        Me.grpBlank.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.grpBlank.Location = New System.Drawing.Point(0, 24)
        Me.grpBlank.Name = "grpBlank"
        Me.grpBlank.Size = New System.Drawing.Size(415, 397)
        Me.grpBlank.TabIndex = 1
        Me.grpBlank.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(8, 38)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(406, 353)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grpBlankAlert)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(398, 327)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Alert"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'grpBlankAlert
        '
        Me.grpBlankAlert.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpBlankAlert.Controls.Add(Me.Label1)
        Me.grpBlankAlert.Controls.Add(Me.cmdInsert)
        Me.grpBlankAlert.Controls.Add(Me.txtAlertTo)
        Me.grpBlankAlert.Controls.Add(Me.cmdAlertTo)
        Me.grpBlankAlert.Controls.Add(Me.txtBlankMsg)
        Me.grpBlankAlert.Controls.Add(Me.txtSubject)
        Me.grpBlankAlert.Location = New System.Drawing.Point(6, 6)
        Me.grpBlankAlert.Name = "grpBlankAlert"
        Me.grpBlankAlert.Size = New System.Drawing.Size(386, 316)
        Me.grpBlankAlert.TabIndex = 0
        Me.grpBlankAlert.TabStop = False
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Subject"
        '
        'cmdInsert
        '
        Me.cmdInsert.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(338, 287)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(24, 23)
        Me.cmdInsert.TabIndex = 26
        Me.cmdInsert.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdInsert.Visible = False
        '
        'txtAlertTo
        '
        Me.txtAlertTo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAlertTo.ForeColor = System.Drawing.Color.Blue
        Me.txtAlertTo.Location = New System.Drawing.Point(64, 17)
        Me.txtAlertTo.Name = "txtAlertTo"
        Me.txtAlertTo.Size = New System.Drawing.Size(298, 21)
        Me.txtAlertTo.TabIndex = 1
        '
        'cmdAlertTo
        '
        Me.cmdAlertTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAlertTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAlertTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAlertTo.Location = New System.Drawing.Point(8, 16)
        Me.cmdAlertTo.Name = "cmdAlertTo"
        Me.cmdAlertTo.Size = New System.Drawing.Size(48, 21)
        Me.cmdAlertTo.TabIndex = 0
        Me.cmdAlertTo.Text = "To..."
        Me.cmdAlertTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBlankMsg
        '
        Me.txtBlankMsg.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtBlankMsg.ContextMenu = Me.mnuInserter
        Me.txtBlankMsg.ForeColor = System.Drawing.Color.Blue
        Me.txtBlankMsg.Location = New System.Drawing.Point(8, 67)
        Me.txtBlankMsg.Multiline = True
        Me.txtBlankMsg.Name = "txtBlankMsg"
        Me.txtBlankMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBlankMsg.Size = New System.Drawing.Size(354, 241)
        Me.txtBlankMsg.TabIndex = 3
        Me.txtBlankMsg.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem10, Me.MenuItem11})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefMsg})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 4
        Me.mnuDefMsg.Text = "Default Message"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 10
        Me.MenuItem10.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 11
        Me.MenuItem11.Text = "Spell Check"
        '
        'txtSubject
        '
        Me.txtSubject.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSubject.ContextMenu = Me.mnuInserter
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(64, 43)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(298, 21)
        Me.txtSubject.TabIndex = 2
        Me.txtSubject.Tag = "memo"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Panel1)
        Me.TabPage2.Controls.Add(Me.chkCustomQuery)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(398, 327)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Query"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnBuild)
        Me.Panel1.Controls.Add(Me.txtQuery)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(392, 304)
        Me.Panel1.TabIndex = 1
        '
        'btnBuild
        '
        Me.btnBuild.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuild.Location = New System.Drawing.Point(294, 278)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'txtQuery
        '
        Me.txtQuery.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtQuery.Location = New System.Drawing.Point(6, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(363, 253)
        Me.txtQuery.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 3)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "SQL Query"
        '
        'chkCustomQuery
        '
        Me.chkCustomQuery.AutoSize = True
        Me.chkCustomQuery.Checked = True
        Me.chkCustomQuery.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCustomQuery.Dock = System.Windows.Forms.DockStyle.Top
        Me.chkCustomQuery.Location = New System.Drawing.Point(3, 3)
        Me.chkCustomQuery.Name = "chkCustomQuery"
        Me.chkCustomQuery.Size = New System.Drawing.Size(392, 17)
        Me.chkCustomQuery.TabIndex = 0
        Me.chkCustomQuery.Text = "Use custom query to check if report is blank"
        Me.chkCustomQuery.UseVisualStyleBackColor = True
        Me.chkCustomQuery.Visible = False
        '
        'optAlert
        '
        Me.optAlert.BackColor = System.Drawing.Color.Transparent
        Me.optAlert.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optAlert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAlert.Location = New System.Drawing.Point(8, 8)
        Me.optAlert.Name = "optAlert"
        Me.optAlert.Size = New System.Drawing.Size(88, 24)
        Me.optAlert.TabIndex = 1
        Me.optAlert.TabStop = True
        Me.optAlert.Text = "Send an alert"
        Me.optAlert.UseVisualStyleBackColor = False
        '
        'optAlertandReport
        '
        Me.optAlertandReport.BackColor = System.Drawing.Color.Transparent
        Me.optAlertandReport.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optAlertandReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAlertandReport.Location = New System.Drawing.Point(104, 8)
        Me.optAlertandReport.Name = "optAlertandReport"
        Me.optAlertandReport.Size = New System.Drawing.Size(160, 24)
        Me.optAlertandReport.TabIndex = 2
        Me.optAlertandReport.Text = "Send an alert and the report"
        Me.optAlertandReport.UseVisualStyleBackColor = False
        '
        'optIgnore
        '
        Me.optIgnore.BackColor = System.Drawing.Color.Transparent
        Me.optIgnore.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optIgnore.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optIgnore.Location = New System.Drawing.Point(272, 8)
        Me.optIgnore.Name = "optIgnore"
        Me.optIgnore.Size = New System.Drawing.Size(112, 24)
        Me.optIgnore.TabIndex = 3
        Me.optIgnore.Text = "Ignore the report"
        Me.optIgnore.UseVisualStyleBackColor = False
        '
        'chkBlankReport
        '
        Me.chkBlankReport.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkBlankReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkBlankReport.Location = New System.Drawing.Point(0, 0)
        Me.chkBlankReport.Name = "chkBlankReport"
        Me.chkBlankReport.Size = New System.Drawing.Size(232, 24)
        Me.chkBlankReport.TabIndex = 0
        Me.chkBlankReport.Text = "Perform checks for blank report"
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem8, Me.MenuItem9})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 4
        Me.MenuItem9.Text = "Default Message"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = False
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = Nothing
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'ucBlankAlert
        '
        Me.Controls.Add(Me.chkBlankReport)
        Me.Controls.Add(Me.grpBlank)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucBlankAlert"
        Me.Size = New System.Drawing.Size(418, 421)
        Me.grpBlank.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.grpBlankAlert.ResumeLayout(False)
        Me.grpBlankAlert.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkCustomQuery As System.Windows.Forms.CheckBox

End Class
