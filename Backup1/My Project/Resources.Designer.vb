﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.34003
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),  _
     Global.Microsoft.VisualBasic.HideModuleNameAttribute()>  _
    Friend Module Resources
        
        Private resourceMan As Global.System.Resources.ResourceManager
        
        Private resourceCulture As Global.System.Globalization.CultureInfo
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("sqlrd.Resources", GetType(Resources).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Friend Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property about() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("about", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property arrow_left_green() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("arrow_left_green", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property arrow_right_green() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("arrow_right_green", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property atom1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("atom1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box_closed() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box_closed", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box_closed_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box_closed_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box_new() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box_new", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box_new_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box_new_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property box2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("box2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property calendar() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("calendar", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property camera2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("camera2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property check2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("check2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property component_yellow() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("component_yellow", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property components() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("components", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property crd6_abouta() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("crd6_abouta", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property crd6_aboutb() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("crd6_aboutb", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property crd6a() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("crd6a", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property crd6b() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("crd6b", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property cube_molecule() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("cube_molecule", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property cube_molecule_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("cube_molecule_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property cube_molecule2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("cube_molecule2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property data_driven_box() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("data_driven_box", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property data_driven_box_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("data_driven_box_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property data_driven_package1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("data_driven_package1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property date_time_preferences() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("date_time_preferences", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property delete() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property delete2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property delete21() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete21", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property delete22() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("delete22", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_atom1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_atom1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_atoms() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_atoms", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_attachment() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_attachment", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_attachment2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_attachment2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_chart() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_chart", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_chart2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_chart2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_datadriven_schedule1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_datadriven_schedule1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_flash() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_flash", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_flash_bw() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_flash_bw", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_gear() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_gear", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_gear2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_gear2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_out() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_out", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_pulse() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_pulse", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property document_pulse2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("document_pulse2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property dropbox() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("dropbox", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property dropbox_large() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("dropbox_large", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property dynamic_package1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("dynamic_package1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to 
        '''var curDate = new Date();
        '''
        '''//app.alert(curDate);
        '''
        '''var finalDate = new Date(&quot;##/##/####&quot;);
        '''
        '''//app.alert(finalDate);
        '''
        '''
        '''if(finalDate.getTime() &lt; curDate.getTime())
        '''
        '''{
        '''
        '''app.alert(&apos;This document is no longer valid.  Please contact the author&apos;);
        '''
        '''this.closeDoc(true);
        '''
        '''}.
        '''</summary>
        Friend ReadOnly Property finaldate() As String
            Get
                Return ResourceManager.GetString("finaldate", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property Folder_256x256_forbidden() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("Folder_256x256_forbidden", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property Folder_256x2561() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("Folder_256x2561", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_closed() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_closed", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_closed1() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_closed1", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property folder_cubes() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("folder_cubes", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property Folder_cubes_256x256() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("Folder_cubes_256x256", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property Folder_info_256x256() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("Folder_info_256x256", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property gear_refresh() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("gear_refresh", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property gray_strip() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("gray-strip", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property home() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("home", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property lightbulb_on() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("lightbulb_on", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property logo_helen_sqlrd5() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("logo_helen_sqlrd5", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property logo_sarah_sqlrd5() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("logo_sarah_sqlrd5", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property mail() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("mail", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property name() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("name", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property server_document() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("server_document", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property sqlrd_abouta() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("sqlrd_abouta", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property sqlrd_aboutb() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("sqlrd_aboutb", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property strip() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("strip", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property transform2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("transform2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property users_family() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("users_family", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property view() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("view", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property warning() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("warning", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized resource of type System.Drawing.Bitmap.
        '''</summary>
        Friend ReadOnly Property warning2() As System.Drawing.Bitmap
            Get
                Dim obj As Object = ResourceManager.GetObject("warning2", resourceCulture)
                Return CType(obj,System.Drawing.Bitmap)
            End Get
        End Property
    End Module
End Namespace
