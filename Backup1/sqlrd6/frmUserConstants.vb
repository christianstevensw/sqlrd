Public Class frmUserConstants
    Dim gItemList As ArrayList

    Private Sub frmUserConstants_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadAll()
    End Sub

    Private Sub LoadAll(Optional ByVal search As String = "")
        Dim SQL As String

        If search = "" Then
            SQL = "SELECT * FROM FormulaAttr ORDER BY FormulaName"
        Else
            SQL = "SELECT * FROM FormulaAttr WHERE FormulaName LIKE '%" & SQLPrepare(search) & "%' OR FormulaDef LIKE '%" & SQLPrepare(search) & "%' ORDER BY FormulaName"
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim il As ImageList = crdxCommon.createImageList(New String() {"userconstant.png"})

        lsvConstants.LargeImageList = il
        lsvConstants.SmallImageList = il

        Me.lsvConstants.Items.Clear()

        gItemList = New ArrayList

        'pick up all the user constants
        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            Do While oRs.EOF = False
                Dim item As ListViewItem = New ListViewItem

                item.Text = oRs("formulaname").Value
                item.Tag = oRs("formulaid").Value
                item.ImageIndex = 0

                gItemList.Add(item)

                lsvConstants.Items.Add(item)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
   
    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If Me.chkSearchContents.Checked = False Then
            Try
                Dim searchTerm As String = Me.txtSearch.Text.ToLower

                If txtSearch.Text.Length > 0 Then
                    For Each it As ListViewItem In Me.lsvConstants.Items
                        Dim itemText As String = it.Text.ToLower

                        If itemText.Contains(searchTerm) = False Then
                            it.Remove()
                        End If
                    Next
                Else
                    If gItemList Is Nothing Then
                        LoadAll()
                    Else
                        Me.lsvConstants.Items.Clear()
                        Me.lsvConstants.BeginUpdate()
                        For Each it As ListViewItem In gItemList
                            Me.lsvConstants.Items.Add(it)
                        Next
                        Me.lsvConstants.EndUpdate()
                    End If
                End If
            Catch : End Try
        Else
            LoadAll(Me.txtSearch.Text)
        End If
    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Dim formEditor As frmFormulaEditor = New frmFormulaEditor

        formEditor.AddConstant()

        LoadAll()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If Me.lsvConstants.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = Me.lsvConstants.SelectedItems(0)
        Dim nID As Integer = it.Tag

        Dim formEditor As frmFormulaEditor = New frmFormulaEditor
        Dim name As String = formEditor.EditConstant(nID)

        If name IsNot Nothing Then
            it.Text = name
        End If
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Close()
    End Sub

    Private Sub BtnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnRemove.Click
        If Me.lsvConstants.SelectedItems.Count = 0 Then Return
        Dim reply As DialogResult

        'if only 1 selected constant then prompt user with constant name

        If Me.lsvConstants.SelectedItems.Count = 1 Then
            reply = MessageBox.Show("Delete the User Constant '" & Me.lsvConstants.SelectedItems(0).Text & "'? This cannot be undone.", _
                        Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        Else
            reply = MessageBox.Show("Delete the selected User Constants? This cannot be undone.", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        End If

        If reply = Windows.Forms.DialogResult.OK Then
            For Each it As ListViewItem In Me.lsvConstants.SelectedItems
                Dim nID As Integer = Me.lsvConstants.SelectedItems(0).Tag

                If clsMarsData.WriteData("DELETE FROM FormulaAttr WHERE FormulaID =" & nID) = True Then
                    Me.lsvConstants.SelectedItems(0).Remove()
                End If
            Next
        End If
    End Sub
End Class
