<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUnhandledException
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUnhandledException))
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnCopy = New DevComponents.DotNetBar.ButtonX()
        Me.txtStackTrace = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.btnContinue = New DevComponents.DotNetBar.ButtonX()
        Me.btnExit = New DevComponents.DotNetBar.ButtonX()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(6, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(549, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "The following error has occured. This error is not fatal and you may continue to " & _
            "use the application"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnCopy)
        Me.GroupBox1.Controls.Add(Me.txtStackTrace)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(10, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(561, 337)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'btnCopy
        '
        Me.btnCopy.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCopy.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCopy.Location = New System.Drawing.Point(9, 303)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(97, 25)
        Me.btnCopy.TabIndex = 1
        Me.btnCopy.Text = "Copy Error"
        '
        'txtStackTrace
        '
        Me.txtStackTrace.BackColor = System.Drawing.SystemColors.Info
        '
        '
        '
        Me.txtStackTrace.Border.Class = "TextBoxBorder"
        Me.txtStackTrace.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtStackTrace.Location = New System.Drawing.Point(9, 42)
        Me.txtStackTrace.Multiline = True
        Me.txtStackTrace.Name = "txtStackTrace"
        Me.txtStackTrace.ReadOnly = True
        Me.txtStackTrace.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtStackTrace.Size = New System.Drawing.Size(546, 255)
        Me.txtStackTrace.TabIndex = 0
        '
        'btnContinue
        '
        Me.btnContinue.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnContinue.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnContinue.Location = New System.Drawing.Point(478, 355)
        Me.btnContinue.Name = "btnContinue"
        Me.btnContinue.Size = New System.Drawing.Size(93, 25)
        Me.btnContinue.TabIndex = 1
        Me.btnContinue.Text = "Continue"
        Me.ToolTip1.SetToolTip(Me.btnContinue, "Log the error, and continue using the application")
        '
        'btnExit
        '
        Me.btnExit.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnExit.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnExit.Location = New System.Drawing.Point(382, 355)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(93, 25)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Exit"
        Me.ToolTip1.SetToolTip(Me.btnExit, "Exit the application")
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.Class = ""
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(577, 54)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(128, 263)
        Me.ReflectionImage1.TabIndex = 3
        '
        'frmUnhandledException
        '
        Me.AcceptButton = Me.btnContinue
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(717, 384)
        Me.ControlBox = False
        Me.Controls.Add(Me.ReflectionImage1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnContinue)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmUnhandledException"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Application Domain Error"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtStackTrace As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnCopy As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnContinue As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnExit As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
End Class
