Public Class frmOperationsExplorer
    Dim imgList As ImageList

    Private Sub frmOperationsExplorer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        imgList = New ImageList

        With imgList
            .ImageSize = New Size(16, 16)
            .ColorDepth = ColorDepth.Depth32Bit
            .Images.Add(Image.FromFile(getAssetLocation("clock.png")))
            .Images.Add(Image.FromFile(getAssetLocation("greencircle.png")))
            .Images.Add(Image.FromFile(getAssetLocation("calendar.png")))
        End With

        tvOperations.ImageList = imgList
        lsvDetails.SmallImageList = imgList
        lsvDetails.LargeImageList = imgList

        LoadOperations()
    End Sub

    Private Sub LoadOperations()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim topNode As TreeNode = Me.tvOperations.Nodes(0)

        topNode.Nodes.Clear()
        topNode.ImageIndex = 0
        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim node As TreeNode = New TreeNode

                node.Text = oRs("operationname").Value
                node.Tag = oRs("operationid").Value
                node.ImageIndex = 1
                node.SelectedImageIndex = 1
                topNode.Nodes.Add(node)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        topNode.Expand()
    End Sub

    Private Sub tvOperations_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvOperations.AfterSelect
        Dim topNode As TreeNode = Me.tvOperations.Nodes(0)

        lsvDetails.Items.Clear()

        If Me.tvOperations.SelectedNode Is Nothing Then Return

        If Me.tvOperations.SelectedNode Is topNode Then Return

        Dim selNode As TreeNode = Me.tvOperations.SelectedNode
        Dim opID As Integer = selNode.Tag
        Dim SQL As String = "SELECT * FROM OperationDays WHERE OperationID =" & opID & " ORDER BY DayIndex ASC"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim it As ListViewItem = New ListViewItem
                it.Text = oRs("dayname").Value
                it.Tag = oRs("dayid").Value
                it.ImageIndex = 2

                it.SubItems.Add(oRs("openat").Value)
                it.SubItems.Add(oRs("closeat").Value)

                Me.lsvDetails.Items.Add(it)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim oAdd As frmOperationalHours = New frmOperationalHours
        Dim newName As String = oAdd.AddOperationalHours

        If newName <> "" Then

            Me.LoadOperations()

            Dim topNode As TreeNode = Me.tvOperations.Nodes(0)

            For Each node As TreeNode In topNode.Nodes
                If node.Text = newName Then
                    Me.tvOperations.SelectedNode = node
                    node.EnsureVisible()
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If Me.tvOperations.SelectedNode Is Nothing Then Return

        If Me.tvOperations.SelectedNode Is Me.tvOperations.Nodes(0) Then Return

        Dim opId As Integer = Me.tvOperations.SelectedNode.Tag

        Dim oEdit As frmOperationalHours = New frmOperationalHours

        If oEdit.EditOperationalHours(opId) = True Then
            Me.LoadOperations()

            Dim topNode As TreeNode = Me.tvOperations.Nodes(0)

            For Each node As TreeNode In topNode.Nodes
                If node.Tag = opId Then
                    Me.tvOperations.SelectedNode = node
                    node.EnsureVisible()
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If Me.tvOperations.SelectedNode Is Nothing Then Return

        If Me.tvOperations.SelectedNode Is Me.tvOperations.Nodes(0) Then Return

        Dim SQL As String = "SELECT * FROM EventAttr6 WHERE UseOperationHours = 1 AND OperationName ='" & SQLPrepare(tvOperations.SelectedNode.Text) & "'"
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                MessageBox.Show("The selected configuration cannot be deleted as it is currently in use by schedules", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                oRs.Close()
                Return
            End If

            oRs.Close()
        End If

        Dim opID As Integer = Me.tvOperations.SelectedNode.Tag
        Dim res As DialogResult = MessageBox.Show("Delete the selected Operational Hours configuration?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        Dim index As Integer = Me.tvOperations.SelectedNode.Index

        If res = Windows.Forms.DialogResult.Yes Then

            If clsMarsData.WriteData("DELETE FROM OperationAttr WHERE OperationID =" & opID) = True Then
                clsMarsData.WriteData("DELETE FROM OperationDays WHERE OperationID =" & opID)

                Me.tvOperations.SelectedNode.Remove()

                Try
                    Me.tvOperations.SelectedNode = Me.tvOperations.Nodes(0).Nodes(index - 1)
                Catch ex As Exception
                    Me.tvOperations.SelectedNode = Me.tvOperations.Nodes(0)
                    Me.lsvDetails.Items.Clear()
                End Try
            End If
        End If
    End Sub
End Class