

Public Class frmSMTPAdvanced
    Dim err As New ErrorProvider
    Dim m_userCancel As Boolean = True

    Private Sub chkRequiresPop_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRequiresPop.CheckedChanged
        Me.grpPop.Enabled = Me.chkRequiresPop.Checked

        Me.btnOK.Enabled = Not Me.chkRequiresPop.Checked
    End Sub

    Private Sub frmSMTPAdvanced_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        If Me.txtPOPServer.Text = "" Then
            setError(Me.txtPOPServer, "Please enter the POP3 server address")
            Me.txtPOPServer.Focus()
            Return
        ElseIf Me.txtPopUser.Text = "" Then
            setError(Me.txtPopUser, "Please enter the POP3 UserID")
            Me.txtPopUser.Focus()
            Return
        ElseIf Me.txtPopPassword.Text = "" Then
            setError(Me.txtPopPassword, "Please provide the POP3 server password")
            Me.txtPopPassword.Focus()
            Return
        ElseIf Me.txtPopPort.Text = "" Then
            setError(Me.txtPopPort, "Please enter the POP3 server port")
            Me.txtPopPort.Focus()
            Return
        End If

        If clsMarsMessaging.Pop3Login(Me.txtPopUser.Text, Me.txtPopPassword.Text, Me.txtPOPServer.Text, _
        Me.txtPopPort.Text, Me.chkPOPSSL.Checked) = False Then
            setError(Me.txtPOPServer, "Could not log into the POP3 server. Please check the address and credentials provided.")
            Me.txtPOPServer.Focus()
        Else
            Me.btnOK.Enabled = True
            setError(Me.txtPopPassword, "")
            setError(Me.txtPopPort, "")
            setError(Me.txtPOPServer, "")
            setError(Me.txtPopPort, "")
        End If

    End Sub

    Public Function SMTPAdvanced(ByVal backupServer As Boolean, Optional ByRef tblValues As DataTable = Nothing) As Boolean

        If backupServer = False Then
            With clsMarsUI.MainUI
                Me.chkRequiresPop.Checked = Convert.ToInt32(.ReadRegistry("RequirePOPLogin", 0))
                Me.txtPOPServer.Text = .ReadRegistry("SMTPPOPServer", "")
                Me.txtPopUser.Text = .ReadRegistry("SMTPPOPUser", "")
                Me.txtPopPassword.Text = .ReadRegistry("SMTPPOPPassword", "", True)
                Me.txtPopPort.Text = .ReadRegistry("SMTPPOPPort", 110)
                Me.chkPOPSSL.Checked = Convert.ToInt32(.ReadRegistry("SMTPPOPSSL", 0))
                Me.chkSmtpSSL.Checked = Convert.ToInt32(.ReadRegistry("SMTPSSL", 0))
                Me.chkSmtpStartTLS.Checked = Convert.ToInt32(.ReadRegistry("SMTPStartTLS", 0))
                Me.cmbSmtpAuthMode.Text = .ReadRegistry("SMTPAuthMode", "LOGIN")
            End With
        Else
            If tblValues IsNot Nothing Then
                Dim row As DataRow = tblValues.Rows(0)

                Me.chkRequiresPop.Checked = row("smtppoplogin")
                Me.txtPOPServer.Text = row("smtppopserver")
                Me.txtPopUser.Text = row("smtppopuser")
                Me.txtPopPassword.Text = row("smtppoppassword")
                Me.txtPopPort.Text = row("smtppopport")
                Me.chkPOPSSL.Checked = row("smtppopssl")
                Me.chkSmtpSSL.Checked = row("smtpssl")
                Me.chkSmtpStartTLS.Checked = row("smtpstarttls")
                Me.cmbSmtpAuthMode.Text = row("smtpauthmode")
            End If
        End If

        '//IMAP settings
        With clsMarsUI.MainUI
            txtIMAPServer.Text = .ReadRegistry("IMAPServer", "")
            txtIMAPUser.Text = .ReadRegistry("IMAPUser", "")
            txtIMAPPassword.Text = .ReadRegistry("IMAPPassword", "", True)
            txtIMAPPort.Value = .ReadRegistry("IMAPPort", 143)
            chkIMAPSSL.Checked = Convert.ToBoolean(Convert.ToInt32(.ReadRegistry("IMAPSSL", 0)))
        End With

        Me.ShowDialog()

        If Me.m_userCancel = True Then
            Return False
        End If

        If backupServer = False Then
            With clsMarsUI.MainUI
                .SaveRegistry("RequirePOPLogin", Convert.ToInt32(Me.chkRequiresPop.Checked))
                .SaveRegistry("SMTPPOPServer", Me.txtPOPServer.Text)
                .SaveRegistry("SMTPPOPUser", Me.txtPopUser.Text)
                .SaveRegistry("SMTPPOPPassword", Me.txtPopPassword.Text, True)
                .SaveRegistry("SMTPPOPPort", Me.txtPopPort.Text)
                .SaveRegistry("SMTPPOPSSL", Convert.ToInt32(Me.chkPOPSSL.Checked))
                .SaveRegistry("SMTPSSL", Convert.ToInt32(Me.chkSmtpSSL.Checked))
                .SaveRegistry("SMTPStartTLS", Convert.ToInt32(Me.chkSmtpStartTLS.Checked))
                .SaveRegistry("SMTPAuthMode", Me.cmbSmtpAuthMode.Text)
            End With

            Return True
        Else
            tblValues = New DataTable

            With tblValues.Columns
                .Add("smtppoplogin")
                .Add("smtppopserver")
                .Add("smtppopuser")
                .Add("smtppoppassword")
                .Add("smtppopport")
                .Add("smtppopssl")
                .Add("smtpssl")
                .Add("smtpstarttls")
                .Add("smtpauthmode")
            End With

            Dim irow As DataRow = tblValues.Rows.Add

            irow("smtppoplogin") = Convert.ToInt32(Me.chkRequiresPop.Checked)
            irow("smtppopserver") = Me.txtPOPServer.Text
            irow("smtppopuser") = Me.txtPopUser.Text
            irow("smtppoppassword") = Me.txtPopPassword.Text
            irow("smtppopport") = Me.txtPopPort.Text
            irow("smtppopssl") = Convert.ToInt32(Me.chkPOPSSL.Checked)
            irow("smtpssl") = Convert.ToInt32(Me.chkSmtpSSL.Checked)
            irow("smtpstarttls") = Convert.ToInt32(Me.chkSmtpStartTLS.Checked)
            irow("smtpauthmode") = Me.cmbSmtpAuthMode.Text
            Return True
        End If

        '//IMAP settings
        With clsMarsUI.MainUI
            .SaveRegistry("IMAPServer", txtIMAPServer.Text)
            .SaveRegistry("IMAPUser", txtIMAPUser.Text)
            .SaveRegistry("IMAPPassword", txtIMAPPassword.Text, True)
            .SaveRegistry("IMAPPort", txtIMAPPort.Value)
            .SaveRegistry("IMAPSSL", Convert.ToInt32(chkIMAPSSL.Checked))
        End With
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If txtIMAPServer.Text <> "" Or txtIMAPUser.Text <> "" Or txtIMAPPassword.Text <> "" Then
            If TestIMAP() = False Then
                TabControl1.SelectedTab = tbIMAP
                err.SetIconAlignment(btnTestIMAP, ErrorIconAlignment.MiddleLeft)
                setError(btnTestIMAP, "Please verify your IMAP settings as they appear to be wrong")
                btnTestIMAP.Focus()
                Return
            End If
        End If


        Me.m_userCancel = False

        Close()
    End Sub

    Private Sub txtPOPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPOPServer.TextChanged, _
     txtPopPassword.TextChanged, txtPopPort.TextChanged, txtPopUser.TextChanged
        setError(sender, "")

    End Sub

    Private Sub chkPOPSSL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPOPSSL.CheckedChanged
        Me.btnOK.Enabled = False
    End Sub


    Private Sub cmbSmtpAuthMode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbSmtpAuthMode.Validating
        If Me.cmbSmtpAuthMode.Text <> "NONE" And Me.cmbSmtpAuthMode.Text <> "LOGIN" And Me.cmbSmtpAuthMode.Text <> "PLAIN" And _
        Me.cmbSmtpAuthMode.Text <> "CRAM-MD5" And Me.cmbSmtpAuthMode.Text <> "NTLM" Then
            Me.cmbSmtpAuthMode.Text = "LOGIN"
        End If
    End Sub

    Private Sub btnTestIMAP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTestIMAP.Click
        If TestIMAP() = True Then
            MessageBox.Show("IMAP test succeded!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            setError(btnTestIMAP, "")
        End If

    End Sub

    Private Function TestIMAP() As Boolean
        Dim imap As Chilkat.Imap = New Chilkat.Imap
        Dim ok As Boolean

        Try

            imap.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")

            If txtIMAPPort.Value > 0 Then
                If chkIMAPSSL.Checked Then
                    imap.Ssl = True
                    imap.Port = txtIMAPPort.Value
                Else
                    imap.Port = txtIMAPPort.Value
                End If
            End If

            If imap.Connect(txtIMAPServer.Text) = False Then
                Throw New Exception(imap.LastErrorText)
            End If

            If imap.Login(txtIMAPUser.Text, txtIMAPPassword.Text) = False Then
                Throw New Exception(imap.LastErrorText)
            End If

            imap.Logout()
            imap.Disconnect()

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, -10475621, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Return False
        End Try
    End Function
End Class