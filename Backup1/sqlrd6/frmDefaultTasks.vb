Public Class frmDefaultTasks
    Dim userCancel As Boolean = True
    Dim ep As New ErrorProvider

    Public Function GetDefaultTask() As DevComponents.AdvTree.NodeCollection
        Me.UcTasks.m_ShowImportExport = False

        Me.UcTasks.ScheduleID = 2777
        Me.UcTasks.LoadTasks()

        Me.ShowDialog()

        If userCancel = True Then
            Return Nothing
        End If

        Return UcTasks.m_SelItems

    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        userCancel = False

        If UcTasks.m_SelItems.Count = 0 Then
            setError(UcTasks.tvTasks, "Please select a custom task")
            UcTasks.tvTasks.Focus()
            Return
        End If

        Close()
    End Sub
End Class