Imports C1.Win.C1Chart

Public Class frmScheduleView
    Public Enum enCalcType As Integer
        MEDIAN = 0
        AVERAGE = 1
        MINIMUM = 2
        MAXIMUM = 3
    End Enum
    Dim userCancel As Boolean = True
    Dim useCustomRange As Boolean = False
    Dim ep As ErrorProvider = New ErrorProvider
    Private m_startTime As DateTime
    Private m_endTime As DateTime

    Public Sub setChartRange(ByVal chart As C1.Win.C1Chart.C1Chart, ByVal viewDate As DateTime, _
    ByVal startTime As DateTime, ByVal endTime As DateTime, ByVal calcType As enCalcType, _
    Optional ByVal excludeTypes As String() = Nothing, Optional ByVal filter As String = "")

        Me.useCustomRange = True

        Me.m_startTime = startTime
        Me.m_endTime = endTime

        drawChart(chart, viewDate, calcType, excludeTypes, filter)
    End Sub
    Private Sub frmScheduleView_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub drawChart(ByVal chart As C1.Win.C1Chart.C1Chart, ByVal viewDate As Date, _
    ByVal calcType As enCalcType, Optional ByVal excludeTypes As String() = Nothing, Optional ByVal filter As String = "")
        Dim SQL As String = "SELECT * FROM ScheduleAttr WHERE Status = 1 AND Frequency <> 'None' AND DATEDIFF([x],NextRun,'[y]') = 0"
        Dim oRs As ADODB.Recordset
        Dim I As Integer = 1

        chart.BeginUpdate()

        If gConType = "DAT" Then
            SQL = SQL.Replace("[x]", "'d'").Replace("[y]", ConDate(viewDate))
        Else
            SQL = SQL.Replace("[x]", "d").Replace("[y]", ConDate(viewDate))
        End If

        If excludeTypes IsNot Nothing Then
            For Each s As String In excludeTypes
                Select Case s
                    Case "Report"
                        SQL &= " AND (ReportID  = 0 OR ReportID IN (SELECT ReportID FROM ReportAttr WHERE (Dynamic = 1 OR Bursting = 1 OR IsDataDriven = 1)))"
                    Case "Dynamic"
                        SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE Dynamic =1))"
                    Case "Bursting"
                        SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE Bursting =1))"
                    Case "Data-Driven"
                        SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE IsDataDriven =1))"
                    Case "Package"
                        SQL &= " AND (PackID = 0 OR PackID IN (SELECT PackID FROM PackageAttr WHERE Dynamic = 1))"
                    Case "Dynamic Package"
                        SQL &= " AND (PackID = 0 OR PackID NOT IN (SELECT PackID FROM PackageAttr WHERE Dynamic = 1))"
                    Case "Automation"
                        SQL &= " AND AutoID = 0"
                    Case "EventPackage"
                        SQL &= " AND EventPackID = 0"
                End Select
            Next
        End If

        SQL &= " ORDER BY NextRun"

        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

        formatGantt(chart, viewDate)

        '   Specify the chart type as Gantt in the ChartGroup
        Dim cg As ChartGroup = chart.ChartGroups.Group0
        cg.ChartType = Chart2DTypeEnum.Gantt
        cg.Gantt.Width = 40

        '   Clear the existing data and add new Series data.
        Dim cdsc As ChartDataSeriesCollection = cg.ChartData.SeriesList
        cdsc.Clear()
        cdsc.RemoveAll()

        'return
        clsMarsUI.MainUI.BusyProgress(50, "Drawing chart....")

        Do While oRs.EOF = False

            Dim scheduleName As String = clsMarsScheduler.globalItem.GetScheduleName(oRs("scheduleid").Value)
            Dim reportID, packID, autoID, eventPackID As Integer

            If filter.Length > 0 Then
                Dim doSkip As Boolean = True

                For Each s As String In filter.Split(";")
                    If scheduleName.ToLower Like s.ToLower Then
                        doSkip = False
                    End If
                Next

                If doSkip = True Then GoTo Skip
            End If

            reportID = IsNull(oRs("reportid").Value, 0)
            packID = IsNull(oRs("packid").Value, 0)
            autoID = IsNull(oRs("autoid").Value, 0)
            eventPackID = IsNull(oRs("eventpackid").Value, 0)

            Dim scheduleDuration As Double = clsMarsScheduler.globalItem.getScheduleDuration(reportID, packID, _
            autoID, eventPackID, 0, calcType)

            Dim startTime As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)

            AddGanttSeriesData(cdsc, scheduleName, New DateTime() {startTime}, New DateTime() {startTime.AddMinutes(scheduleDuration)})

            'AddGanttTaskLabels(chart, cdsc)
skip:

            oRs.MoveNext()

            I += 1
        Loop

        clsMarsUI.MainUI.BusyProgress(, , True)

        oRs.Close()

        chart.EndUpdate()
    End Sub

    Private Sub formatGantt(ByVal chart As C1.Win.C1Chart.C1Chart, ByVal viewDate As Date)
        'Dim chart As C1.Win.C1Chart.C1Chart = Me.gantt
        chart.Style.BackColor = Color.MintCream
        chart.Style.BackColor2 = Color.White
        chart.Style.GradientStyle = GradientStyleEnum.Horizontal

        Dim area As Area = chart.ChartArea
        area.Style.BackColor = Color.Transparent
        area.Style.GradientStyle = GradientStyleEnum.None
        area.Inverted = True ' X axis is vertical
        '   Plot Area style
        area.PlotArea.BackColor = Color.MintCream
        area.PlotArea.Boxed = True

        '   Set up the style and format of the Horizontal (Y) axis.
        Dim ax As Axis = area.AxisY
        ax.AnnoFormat = FormatEnum.DateShortTime
        ax.AnnoFormatString = "HH:mm" '"MMM-dd"
        ax.AnnotationRotation = -30

        If Me.useCustomRange = False Then
            Dim startDate As DateTime = viewDate 'iCal.SelectionStart
            Dim endDate As DateTime = viewDate.AddDays(1) 'iCal.SelectionStart.AddDays(1)

            ax.Min = New DateTime(startDate.Year, startDate.Month, startDate.Day, 0, 0, 0).ToOADate  'New DateTime(2004, 1, 1).ToOADate()
            ax.Max = New DateTime(startDate.Year, startDate.Month, startDate.Day, 23, 59, 0).ToOADate  'New DateTime(2004, 4, 30).ToOADate(
        Else
            ax.Min = New DateTime(viewDate.Year, viewDate.Month, viewDate.Day, Me.m_startTime.Hour, Me.m_startTime.Minute, 0).ToOADate
            ax.Max = New DateTime(viewDate.Year, viewDate.Month, viewDate.Day, Me.m_endTime.Hour, Me.m_endTime.Minute, 0).ToOADate
        End If

        ax.Origin = ax.Min
        ax.Font = New Font("Tahoma", 8, FontStyle.Regular)
        ax.UnitMajor = (1 / 24) '0.041666666666667
        ax.TickMajor = TickMarksEnum.Outside
        ax.GridMajor.Pattern = LinePatternEnum.Dash
        ax.GridMajor.Color = Color.Black
        ax.GridMajor.Visible = True
        ax.Text = ""
        'ax.ForeColor = Color.Navy

        '   Set up the style and format of the Vertical (X) axis.

        ax = area.AxisX
        ax.TickMinor = TickMarksEnum.None
        ax.Reversed = True ' top to bottom
        ax.UnitMajor = 1
        ax.AnnotationRotation = 0
        ax.GridMinor.Pattern = LinePatternEnum.Dash
        ax.GridMinor.Color = Color.Black
        ax.GridMinor.Visible = True
        ax.GridMajor.Pattern = LinePatternEnum.Solid
        ax.GridMajor.Color = area.PlotArea.BackColor
        ax.GridMajor.Visible = True
        ax.Font = New Font("Tahoma", 8, FontStyle.Regular)
        ax.Text = ""
        'ax.ForeColor = Color.Navy
    End Sub

    Private Sub AddGanttSeriesData(ByVal cdsc As ChartDataSeriesCollection, ByVal taskName As String, ByVal startTimes() As DateTime, ByVal endTimes() As DateTime)
        Dim cds As ChartDataSeries = cdsc.AddNewSeries()
        cds.Label = taskName
        cds.Y.CopyDataIn(startTimes)
        cds.Y1.CopyDataIn(endTimes)
    End Sub 'AddGanttSeriesData


    ' Add Chart Labels with beginning and ending dates for each data point
    ' in the Gantt chart.  Labels are placed inside on the western edge.
    Private Sub AddGanttTaskLabels(ByVal chart As C1Chart, ByVal cdsc As ChartDataSeriesCollection)
        Dim cl As ChartLabels = chart.ChartLabels
        cl.DefaultLabelStyle.BackColor = Color.Transparent
        cl.DefaultLabelStyle.GradientStyle = GradientStyleEnum.None
        cl.DefaultLabelStyle.ForeColor = Color.Azure
        cl.DefaultLabelStyle.HorizontalAlignment = AlignHorzEnum.Far

        Dim clc As C1.Win.C1Chart.LabelsCollection = cl.LabelsCollection
        clc.Clear()

        Dim slen As Integer = cdsc.Count
        Dim s As Integer
        For s = 0 To cdsc.Count - 1
            Dim cds As ChartDataSeries = cdsc(s)
            Dim p As Integer
            For p = 0 To cds.Length - 1
                Dim lab As C1.Win.C1Chart.Label = clc.AddNewLabel()
                Dim start As DateTime = CType(cds.Y(p), DateTime)
                Dim [end] As DateTime = CType(cds.Y1(p), DateTime)
                lab.Text = start.ToString("HH:mm") + "-" + [end].ToString("HH:mm")
                lab.AttachMethod = AttachMethodEnum.DataIndex
                lab.AttachMethodData.GroupIndex = 0
                lab.AttachMethodData.SeriesIndex = s
                lab.AttachMethodData.PointIndex = p
                lab.Compass = LabelCompassEnum.West
                lab.Offset = 0
                lab.Visible = True
            Next p
        Next s
    End Sub 'AddGanttTaskLabels


    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        If Me.dtEnd.Value <= Me.dtStart.Value Then
            SetError(Me.dtEnd, "Please select a valid range")
            Return
        End If

        userCancel = False

        Close()
    End Sub

    Private Sub dtStart_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtStart.ValueChanged, dtEnd.ValueChanged
        SetError(dtEnd, "")
    End Sub
End Class