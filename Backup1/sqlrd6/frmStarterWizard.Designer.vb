<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStarterWizard
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStarterWizard))
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.cmdSaveUser = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtConfirm = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.txtLastName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtFirstName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Step2 = New System.Windows.Forms.Panel()
        Me.grpGroupwise = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label28 = New DevComponents.DotNetBar.LabelX()
        Me.txtgwUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label29 = New DevComponents.DotNetBar.LabelX()
        Me.Label41 = New DevComponents.DotNetBar.LabelX()
        Me.Label45 = New DevComponents.DotNetBar.LabelX()
        Me.Label46 = New DevComponents.DotNetBar.LabelX()
        Me.txtgwPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwProxy = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwPOIP = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtgwPOPort = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.grpMAPI = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.txtMAPIPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmbMAPIProfile = New System.Windows.Forms.ComboBox()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.grpSMTP = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSMTPTimeout = New System.Windows.Forms.NumericUpDown()
        Me.txtSMTPUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label19 = New DevComponents.DotNetBar.LabelX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPSenderName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPSenderAddress = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtSMTPPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdMailTest = New DevComponents.DotNetBar.ButtonX()
        Me.cmbMailType = New System.Windows.Forms.ComboBox()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.lnkProcess = New System.Windows.Forms.LinkLabel()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label15 = New DevComponents.DotNetBar.LabelX()
        Me.txtCustomerNo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Step4 = New System.Windows.Forms.Panel()
        Me.chkAdvanced = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label16 = New DevComponents.DotNetBar.LabelX()
        Me.toolTip = New DevComponents.DotNetBar.SuperTooltip()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.LabelX2 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX3 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX4 = New DevComponents.DotNetBar.LabelX()
        Me.LabelX5 = New DevComponents.DotNetBar.LabelX()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.tabGettingStarted = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabAdmin = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabMessaging = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabSupport = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabFinish = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Step1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.grpGroupwise.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.grpMAPI.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.grpSMTP.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step3.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.Step4.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.BackColor = System.Drawing.Color.Transparent
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(650, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(73, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.BackColor = System.Drawing.Color.Transparent
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(571, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(73, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(492, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.cmdSaveUser)
        Me.Step1.Controls.Add(Me.TableLayoutPanel1)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Location = New System.Drawing.Point(57, 46)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(350, 317)
        Me.Step1.TabIndex = 5
        '
        'cmdSaveUser
        '
        Me.cmdSaveUser.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdSaveUser.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdSaveUser.Location = New System.Drawing.Point(243, 188)
        Me.cmdSaveUser.Name = "cmdSaveUser"
        Me.cmdSaveUser.Size = New System.Drawing.Size(75, 23)
        Me.cmdSaveUser.TabIndex = 1
        Me.cmdSaveUser.Text = "Save"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtConfirm, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPassword, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUserID, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLastName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFirstName, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(8, 46)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(337, 135)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(113, 19)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "First Name"
        '
        'txtConfirm
        '
        '
        '
        '
        Me.txtConfirm.Border.Class = "TextBoxBorder"
        Me.txtConfirm.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtConfirm.Location = New System.Drawing.Point(127, 108)
        Me.txtConfirm.Name = "txtConfirm"
        Me.txtConfirm.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtConfirm.Size = New System.Drawing.Size(183, 21)
        Me.txtConfirm.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 33)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 19)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Last Name"
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(127, 83)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(183, 21)
        Me.txtPassword.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(113, 19)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "UserID"
        '
        'txtUserID
        '
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.Location = New System.Drawing.Point(127, 58)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(183, 21)
        Me.txtUserID.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 108)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(113, 19)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Confirm Password"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 83)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(113, 19)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Password"
        '
        'txtLastName
        '
        '
        '
        '
        Me.txtLastName.Border.Class = "TextBoxBorder"
        Me.txtLastName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtLastName.Location = New System.Drawing.Point(127, 33)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(183, 21)
        Me.txtLastName.TabIndex = 1
        '
        'txtFirstName
        '
        '
        '
        '
        Me.txtFirstName.Border.Class = "TextBoxBorder"
        Me.txtFirstName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFirstName.Location = New System.Drawing.Point(127, 8)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(183, 21)
        Me.txtFirstName.TabIndex = 0
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(337, 36)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please set up an administrator user so that you may log into the system"
        Me.Label1.WordWrap = True
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.grpGroupwise)
        Me.Step2.Controls.Add(Me.grpMAPI)
        Me.Step2.Controls.Add(Me.grpSMTP)
        Me.Step2.Controls.Add(Me.cmdMailTest)
        Me.Step2.Controls.Add(Me.cmbMailType)
        Me.Step2.Controls.Add(Me.Label7)
        Me.Step2.Location = New System.Drawing.Point(57, 46)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(350, 317)
        Me.Step2.TabIndex = 6
        '
        'grpGroupwise
        '
        Me.grpGroupwise.Controls.Add(Me.TableLayoutPanel2)
        Me.grpGroupwise.Location = New System.Drawing.Point(6, 48)
        Me.grpGroupwise.Name = "grpGroupwise"
        Me.grpGroupwise.Size = New System.Drawing.Size(297, 216)
        Me.grpGroupwise.TabIndex = 12
        Me.grpGroupwise.TabStop = False
        Me.grpGroupwise.Text = "Groupwise"
        Me.grpGroupwise.Visible = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel2.Controls.Add(Me.Label28, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwUser, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label29, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label41, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label45, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label46, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPassword, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwProxy, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPOIP, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.txtgwPOPort, 1, 4)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(8, 28)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(278, 135)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        '
        '
        '
        Me.Label28.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label28.Location = New System.Drawing.Point(3, 3)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(39, 16)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "User ID"
        '
        'txtgwUser
        '
        '
        '
        '
        Me.txtgwUser.Border.Class = "TextBoxBorder"
        Me.txtgwUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwUser.Location = New System.Drawing.Point(84, 3)
        Me.txtgwUser.Name = "txtgwUser"
        Me.txtgwUser.Size = New System.Drawing.Size(164, 21)
        Me.txtgwUser.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        '
        '
        '
        Me.Label29.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label29.Location = New System.Drawing.Point(3, 30)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 16)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Password"
        '
        'Label41
        '
        Me.Label41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        '
        '
        '
        Me.Label41.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label41.Location = New System.Drawing.Point(3, 57)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(44, 16)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Proxy ID"
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        '
        '
        '
        Me.Label45.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label45.Location = New System.Drawing.Point(3, 84)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(75, 16)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Server Address"
        '
        'Label46
        '
        Me.Label46.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label46.AutoSize = True
        '
        '
        '
        Me.Label46.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label46.Location = New System.Drawing.Point(3, 111)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(57, 16)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Server Port"
        '
        'txtgwPassword
        '
        '
        '
        '
        Me.txtgwPassword.Border.Class = "TextBoxBorder"
        Me.txtgwPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPassword.Location = New System.Drawing.Point(84, 30)
        Me.txtgwPassword.Name = "txtgwPassword"
        Me.txtgwPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtgwPassword.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPassword.TabIndex = 1
        '
        'txtgwProxy
        '
        '
        '
        '
        Me.txtgwProxy.Border.Class = "TextBoxBorder"
        Me.txtgwProxy.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwProxy.Location = New System.Drawing.Point(84, 57)
        Me.txtgwProxy.Name = "txtgwProxy"
        Me.txtgwProxy.Size = New System.Drawing.Size(164, 21)
        Me.txtgwProxy.TabIndex = 1
        '
        'txtgwPOIP
        '
        '
        '
        '
        Me.txtgwPOIP.Border.Class = "TextBoxBorder"
        Me.txtgwPOIP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPOIP.Location = New System.Drawing.Point(84, 84)
        Me.txtgwPOIP.Name = "txtgwPOIP"
        Me.txtgwPOIP.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPOIP.TabIndex = 1
        '
        'txtgwPOPort
        '
        '
        '
        '
        Me.txtgwPOPort.Border.Class = "TextBoxBorder"
        Me.txtgwPOPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtgwPOPort.Location = New System.Drawing.Point(84, 111)
        Me.txtgwPOPort.Name = "txtgwPOPort"
        Me.txtgwPOPort.Size = New System.Drawing.Size(164, 21)
        Me.txtgwPOPort.TabIndex = 1
        '
        'grpMAPI
        '
        Me.grpMAPI.Controls.Add(Me.TableLayoutPanel4)
        Me.grpMAPI.Location = New System.Drawing.Point(6, 48)
        Me.grpMAPI.Name = "grpMAPI"
        Me.grpMAPI.Size = New System.Drawing.Size(297, 216)
        Me.grpMAPI.TabIndex = 14
        Me.grpMAPI.TabStop = False
        Me.grpMAPI.Text = "MAPI Mail"
        Me.grpMAPI.Visible = False
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel4.Controls.Add(Me.Label13, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.txtMAPIPassword, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.cmbMAPIProfile, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label14, 0, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(11, 24)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(258, 59)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 8)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(88, 18)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "MAPI Profile"
        '
        'txtMAPIPassword
        '
        '
        '
        '
        Me.txtMAPIPassword.Border.Class = "TextBoxBorder"
        Me.txtMAPIPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMAPIPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtMAPIPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtMAPIPassword.Location = New System.Drawing.Point(102, 32)
        Me.txtMAPIPassword.Name = "txtMAPIPassword"
        Me.txtMAPIPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtMAPIPassword.Size = New System.Drawing.Size(149, 21)
        Me.txtMAPIPassword.TabIndex = 1
        '
        'cmbMAPIProfile
        '
        Me.cmbMAPIProfile.ForeColor = System.Drawing.Color.Blue
        Me.cmbMAPIProfile.ItemHeight = 13
        Me.cmbMAPIProfile.Location = New System.Drawing.Point(102, 8)
        Me.cmbMAPIProfile.Name = "cmbMAPIProfile"
        Me.cmbMAPIProfile.Size = New System.Drawing.Size(149, 21)
        Me.cmbMAPIProfile.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 32)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(88, 19)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Password"
        '
        'grpSMTP
        '
        Me.grpSMTP.Controls.Add(Me.TableLayoutPanel3)
        Me.grpSMTP.Location = New System.Drawing.Point(6, 48)
        Me.grpSMTP.Name = "grpSMTP"
        Me.grpSMTP.Size = New System.Drawing.Size(297, 216)
        Me.grpSMTP.TabIndex = 1
        Me.grpSMTP.TabStop = False
        Me.grpSMTP.Text = "SMTP Mail"
        Me.grpSMTP.Visible = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel3.Controls.Add(Me.Label8, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cmbSMTPTimeout, 1, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPUserID, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label19, 0, 5)
        Me.TableLayoutPanel3.Controls.Add(Me.Label9, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPSenderName, 1, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.Label12, 0, 4)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPSenderAddress, 1, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.Label11, 0, 3)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPServer, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label10, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.txtSMTPPassword, 1, 1)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(10, 28)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.Padding = New System.Windows.Forms.Padding(5)
        Me.TableLayoutPanel3.RowCount = 6
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(276, 171)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 8)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(88, 20)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "User ID"
        '
        'cmbSMTPTimeout
        '
        Me.cmbSMTPTimeout.Location = New System.Drawing.Point(102, 138)
        Me.cmbSMTPTimeout.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.cmbSMTPTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbSMTPTimeout.Name = "cmbSMTPTimeout"
        Me.cmbSMTPTimeout.Size = New System.Drawing.Size(57, 21)
        Me.cmbSMTPTimeout.TabIndex = 5
        Me.cmbSMTPTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbSMTPTimeout.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'txtSMTPUserID
        '
        '
        '
        '
        Me.txtSMTPUserID.Border.Class = "TextBoxBorder"
        Me.txtSMTPUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPUserID.Location = New System.Drawing.Point(102, 8)
        Me.txtSMTPUserID.Name = "txtSMTPUserID"
        Me.txtSMTPUserID.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPUserID.TabIndex = 0
        '
        'Label19
        '
        Me.Label19.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label19.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 138)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(88, 25)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "SMTP Timeout"
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 34)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(88, 20)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Password"
        '
        'txtSMTPSenderName
        '
        '
        '
        '
        Me.txtSMTPSenderName.Border.Class = "TextBoxBorder"
        Me.txtSMTPSenderName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderName.Location = New System.Drawing.Point(102, 112)
        Me.txtSMTPSenderName.Name = "txtSMTPSenderName"
        Me.txtSMTPSenderName.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPSenderName.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 112)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 20)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Sender Name"
        '
        'txtSMTPSenderAddress
        '
        '
        '
        '
        Me.txtSMTPSenderAddress.Border.Class = "TextBoxBorder"
        Me.txtSMTPSenderAddress.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderAddress.Location = New System.Drawing.Point(102, 86)
        Me.txtSMTPSenderAddress.Name = "txtSMTPSenderAddress"
        Me.txtSMTPSenderAddress.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPSenderAddress.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 86)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(88, 20)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Sender Address"
        '
        'txtSMTPServer
        '
        '
        '
        '
        Me.txtSMTPServer.Border.Class = "TextBoxBorder"
        Me.txtSMTPServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPServer.Location = New System.Drawing.Point(102, 60)
        Me.txtSMTPServer.Name = "txtSMTPServer"
        Me.txtSMTPServer.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPServer.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 60)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(88, 20)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Server Name"
        '
        'txtSMTPPassword
        '
        '
        '
        '
        Me.txtSMTPPassword.Border.Class = "TextBoxBorder"
        Me.txtSMTPPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtSMTPPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMTPPassword.Location = New System.Drawing.Point(102, 34)
        Me.txtSMTPPassword.Name = "txtSMTPPassword"
        Me.txtSMTPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMTPPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtSMTPPassword.TabIndex = 1
        '
        'cmdMailTest
        '
        Me.cmdMailTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdMailTest.BackColor = System.Drawing.Color.Transparent
        Me.cmdMailTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdMailTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMailTest.Location = New System.Drawing.Point(199, 268)
        Me.cmdMailTest.Name = "cmdMailTest"
        Me.cmdMailTest.Size = New System.Drawing.Size(104, 24)
        Me.cmdMailTest.TabIndex = 2
        Me.cmdMailTest.Text = "Test Settings"
        Me.cmdMailTest.Visible = False
        '
        'cmbMailType
        '
        Me.cmbMailType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailType.ForeColor = System.Drawing.Color.Blue
        Me.cmbMailType.ItemHeight = 13
        Me.cmbMailType.Items.AddRange(New Object() {"MAPI", "SMTP", "GROUPWISE", "NONE"})
        Me.cmbMailType.Location = New System.Drawing.Point(71, 8)
        Me.cmbMailType.Name = "cmbMailType"
        Me.cmbMailType.Size = New System.Drawing.Size(199, 21)
        Me.cmbMailType.TabIndex = 0
        Me.cmbMailType.Tag = "unsorted"
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 11)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Mail Type"
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.lnkProcess)
        Me.Step3.Controls.Add(Me.TableLayoutPanel5)
        Me.Step3.Location = New System.Drawing.Point(57, 46)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(350, 317)
        Me.Step3.TabIndex = 0
        '
        'lnkProcess
        '
        Me.lnkProcess.Location = New System.Drawing.Point(9, 51)
        Me.lnkProcess.Name = "lnkProcess"
        Me.lnkProcess.Size = New System.Drawing.Size(336, 30)
        Me.lnkProcess.TabIndex = 1
        Me.lnkProcess.TabStop = True
        Me.lnkProcess.Text = "Click here to register for support and get your Customer number for free"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel5.Controls.Add(Me.Label15, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txtCustomerNo, 1, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(12, 11)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(285, 29)
        Me.TableLayoutPanel5.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label15.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(3, 3)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(83, 23)
        Me.Label15.TabIndex = 2
        Me.Label15.Text = "Customer #"
        '
        'txtCustomerNo
        '
        '
        '
        '
        Me.txtCustomerNo.Border.Class = "TextBoxBorder"
        Me.txtCustomerNo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtCustomerNo.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomerNo.Location = New System.Drawing.Point(92, 3)
        Me.txtCustomerNo.Name = "txtCustomerNo"
        Me.txtCustomerNo.Size = New System.Drawing.Size(182, 21)
        Me.txtCustomerNo.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.chkAdvanced)
        Me.Step4.Controls.Add(Me.Label16)
        Me.Step4.Location = New System.Drawing.Point(57, 46)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(350, 317)
        Me.Step4.TabIndex = 0
        '
        'chkAdvanced
        '
        Me.chkAdvanced.AutoSize = True
        '
        '
        '
        Me.chkAdvanced.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkAdvanced.Location = New System.Drawing.Point(12, 163)
        Me.chkAdvanced.Name = "chkAdvanced"
        Me.chkAdvanced.Size = New System.Drawing.Size(326, 16)
        Me.chkAdvanced.TabIndex = 1
        Me.chkAdvanced.Text = "Go to configure advanced  system settings when I click 'Finish'"
        '
        'Label16
        '
        '
        '
        '
        Me.Label16.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(9, 22)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(336, 111)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Congratulations! You have successfully finished setting up your system. Click 'Fi" & _
    "nish' to exit the Wizard."
        Me.Label16.WordWrap = True
        '
        'toolTip
        '
        Me.toolTip.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.toolTip.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.toolTip.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'stabMain
        '
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Top
        Me.stabMain.Location = New System.Drawing.Point(0, 0)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 2
        Me.stabMain.Size = New System.Drawing.Size(726, 427)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabMain.TabIndex = 7
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGettingStarted, Me.tabAdmin, Me.tabMessaging, Me.tabSupport, Me.tabFinish})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabMain.Text = "stabMain"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.TableLayoutPanel6)
        Me.SuperTabControlPanel5.Controls.Add(Me.ReflectionImage1)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(192, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(534, 427)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabGettingStarted
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.LabelX2, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.LabelX3, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.LabelX4, 0, 2)
        Me.TableLayoutPanel6.Controls.Add(Me.LabelX5, 0, 3)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(57, 46)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 5
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(355, 163)
        Me.TableLayoutPanel6.TabIndex = 3
        '
        'LabelX2
        '
        '
        '
        '
        Me.LabelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX2.Location = New System.Drawing.Point(3, 3)
        Me.LabelX2.Name = "LabelX2"
        Me.LabelX2.Size = New System.Drawing.Size(349, 26)
        Me.LabelX2.TabIndex = 0
        Me.LabelX2.Text = "Welcome to <b>SQL-RD</b>. This will wizard will guide you through:"
        '
        'LabelX3
        '
        '
        '
        '
        Me.LabelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX3.Location = New System.Drawing.Point(3, 35)
        Me.LabelX3.Name = "LabelX3"
        Me.LabelX3.Size = New System.Drawing.Size(349, 26)
        Me.LabelX3.TabIndex = 0
        Me.LabelX3.Text = "- Creating a SQL-RD Admin user"
        '
        'LabelX4
        '
        '
        '
        '
        Me.LabelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX4.Location = New System.Drawing.Point(3, 67)
        Me.LabelX4.Name = "LabelX4"
        Me.LabelX4.Size = New System.Drawing.Size(349, 26)
        Me.LabelX4.TabIndex = 0
        Me.LabelX4.Text = "- Configuring your messaging settings for Email"
        '
        'LabelX5
        '
        '
        '
        '
        Me.LabelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.LabelX5.Location = New System.Drawing.Point(3, 99)
        Me.LabelX5.Name = "LabelX5"
        Me.LabelX5.Size = New System.Drawing.Size(349, 26)
        Me.LabelX5.TabIndex = 0
        Me.LabelX5.Text = "- Registering for our excellent free support"
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(163, 320)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.ReflectionEnabled = False
        Me.ReflectionImage1.Size = New System.Drawing.Size(275, 102)
        Me.ReflectionImage1.TabIndex = 4
        '
        'tabGettingStarted
        '
        Me.tabGettingStarted.AttachedControl = Me.SuperTabControlPanel5
        Me.tabGettingStarted.GlobalItem = False
        Me.tabGettingStarted.Name = "tabGettingStarted"
        Me.tabGettingStarted.Text = "Getting Started with SQL-RD"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(192, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(534, 427)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabAdmin
        '
        'tabAdmin
        '
        Me.tabAdmin.AttachedControl = Me.SuperTabControlPanel1
        Me.tabAdmin.GlobalItem = False
        Me.tabAdmin.Name = "tabAdmin"
        Me.tabAdmin.Text = "Create Admin User"
        Me.tabAdmin.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.Step2)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(192, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(534, 427)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabMessaging
        '
        'tabMessaging
        '
        Me.tabMessaging.AttachedControl = Me.SuperTabControlPanel2
        Me.tabMessaging.GlobalItem = False
        Me.tabMessaging.Name = "tabMessaging"
        Me.tabMessaging.Text = "Configure messaging (optional)"
        Me.tabMessaging.Visible = False
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(192, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(534, 427)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabSupport
        '
        'tabSupport
        '
        Me.tabSupport.AttachedControl = Me.SuperTabControlPanel3
        Me.tabSupport.GlobalItem = False
        Me.tabSupport.Name = "tabSupport"
        Me.tabSupport.Text = "Register for free support (optional)"
        Me.tabSupport.Visible = False
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.Step4)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(192, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(534, 427)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabFinish
        '
        'tabFinish
        '
        Me.tabFinish.AttachedControl = Me.SuperTabControlPanel4
        Me.tabFinish.GlobalItem = False
        Me.tabFinish.Name = "tabFinish"
        Me.tabFinish.Text = "Finish"
        Me.tabFinish.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 470)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(726, 34)
        Me.FlowLayoutPanel1.TabIndex = 8
        '
        'frmStarterWizard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(726, 504)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.stabMain)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmStarterWizard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welcome to SQL-RD"
        Me.Step1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.grpGroupwise.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.grpMAPI.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.grpSMTP.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step3.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.Step4.PerformLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtConfirm As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtFirstName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtLastName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents cmbMailType As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents grpGroupwise As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label28 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtgwUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label29 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label41 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label45 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label46 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtgwPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwProxy As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwPOIP As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtgwPOPort As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpSMTP As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbSMTPTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label19 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPSenderAddress As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtSMTPSenderName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents grpMAPI As System.Windows.Forms.GroupBox
    Friend WithEvents txtMAPIPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbMAPIProfile As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cmdMailTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Label15 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtCustomerNo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents lnkProcess As System.Windows.Forms.LinkLabel
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents chkAdvanced As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label16 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents toolTip As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmdSaveUser As DevComponents.DotNetBar.ButtonX
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGettingStarted As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabFinish As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabAdmin As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSupport As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabMessaging As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LabelX2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents LabelX5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
End Class
