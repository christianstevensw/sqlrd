<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDock
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDock))
        Me.BubbleBar = New DevComponents.DotNetBar.BubbleBar
        Me.BubbleBarTab2 = New DevComponents.DotNetBar.BubbleBarTab(Me.components)
        Me.btnOptions = New DevComponents.DotNetBar.BubbleButton
        Me.btnSystemMonitor = New DevComponents.DotNetBar.BubbleButton
        Me.btnProperties = New DevComponents.DotNetBar.BubbleButton
        Me.btnDesign = New DevComponents.DotNetBar.BubbleButton
        Me.btnCalendar = New DevComponents.DotNetBar.BubbleButton
        Me.btnAddressBook = New DevComponents.DotNetBar.BubbleButton
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton
        Me.btnSingle = New DevComponents.DotNetBar.BubbleButton
        Me.btnPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamic = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamicPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnAutomation = New DevComponents.DotNetBar.BubbleButton
        Me.btnEvent = New DevComponents.DotNetBar.BubbleButton
        Me.btnEventPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnBursting = New DevComponents.DotNetBar.BubbleButton
        Me.btnDDSchedule = New DevComponents.DotNetBar.BubbleButton
        Me.btnDDPackage = New DevComponents.DotNetBar.BubbleButton
        Me.BubbleButton2 = New DevComponents.DotNetBar.BubbleButton
        Me.btnHelp = New DevComponents.DotNetBar.BubbleButton
        Me.btnForums = New DevComponents.DotNetBar.BubbleButton
        Me.btnKbase = New DevComponents.DotNetBar.BubbleButton
        Me.btnHowTo = New DevComponents.DotNetBar.BubbleButton
        CType(Me.BubbleBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BubbleBar
        '
        Me.BubbleBar.Alignment = DevComponents.DotNetBar.eBubbleButtonAlignment.Bottom
        Me.BubbleBar.AntiAlias = True
        Me.BubbleBar.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.BubbleBar.ButtonBackAreaStyle.BackColorGradientAngle = 90
        Me.BubbleBar.ButtonBackAreaStyle.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar.ButtonBackAreaStyle.BorderLeftWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid
        Me.BubbleBar.ButtonBackAreaStyle.BorderRightWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.BorderTopWidth = 1
        Me.BubbleBar.ButtonBackAreaStyle.PaddingBottom = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingLeft = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingRight = 3
        Me.BubbleBar.ButtonBackAreaStyle.PaddingTop = 3
        Me.BubbleBar.ButtonSpacing = 1
        Me.BubbleBar.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BubbleBar.ImageSizeNormal = New System.Drawing.Size(24, 24)
        Me.BubbleBar.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.BubbleBar.Location = New System.Drawing.Point(0, 0)
        Me.BubbleBar.MouseOverTabColors.BorderColor = System.Drawing.SystemColors.Highlight
        Me.BubbleBar.Name = "BubbleBar"
        Me.BubbleBar.SelectedTab = Me.BubbleBarTab2
        Me.BubbleBar.SelectedTabColors.BorderColor = System.Drawing.Color.Black
        Me.BubbleBar.Size = New System.Drawing.Size(566, 118)
        Me.BubbleBar.TabIndex = 26
        Me.BubbleBar.Tabs.Add(Me.BubbleBarTab2)
        Me.BubbleBar.Text = "Create"
        '
        'BubbleBarTab2
        '
        Me.BubbleBarTab2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(244, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.BubbleBarTab2.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.Buttons.AddRange(New DevComponents.DotNetBar.BubbleButton() {Me.btnOptions, Me.btnSystemMonitor, Me.btnProperties, Me.btnDesign, Me.btnCalendar, Me.btnAddressBook, Me.BubbleButton1, Me.btnSingle, Me.btnPackage, Me.btnDynamic, Me.btnDynamicPackage, Me.btnAutomation, Me.btnEvent, Me.btnEventPackage, Me.btnBursting, Me.btnDDSchedule, Me.btnDDPackage, Me.BubbleButton2, Me.btnHelp, Me.btnForums, Me.btnKbase, Me.btnHowTo})
        Me.BubbleBarTab2.DarkBorderColor = System.Drawing.Color.FromArgb(CType(CType(190, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer), CType(CType(105, Byte), Integer))
        Me.BubbleBarTab2.LightBorderColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.BubbleBarTab2.Name = "BubbleBarTab2"
        Me.BubbleBarTab2.PredefinedColor = DevComponents.DotNetBar.eTabItemColor.Yellow
        Me.BubbleBarTab2.Text = "Task Dock"
        Me.BubbleBarTab2.TextColor = System.Drawing.Color.Black
        '
        'btnOptions
        '
        Me.btnOptions.Image = CType(resources.GetObject("btnOptions.Image"), System.Drawing.Image)
        Me.btnOptions.ImageLarge = CType(resources.GetObject("btnOptions.ImageLarge"), System.Drawing.Image)
        Me.btnOptions.Name = "btnOptions"
        Me.btnOptions.TooltipText = "System Options"
        '
        'btnSystemMonitor
        '
        Me.btnSystemMonitor.Image = CType(resources.GetObject("btnSystemMonitor.Image"), System.Drawing.Image)
        Me.btnSystemMonitor.ImageLarge = CType(resources.GetObject("btnSystemMonitor.ImageLarge"), System.Drawing.Image)
        Me.btnSystemMonitor.Name = "btnSystemMonitor"
        Me.btnSystemMonitor.TooltipText = "System Monitor"
        '
        'btnProperties
        '
        Me.btnProperties.Enabled = False
        Me.btnProperties.Image = CType(resources.GetObject("btnProperties.Image"), System.Drawing.Image)
        Me.btnProperties.ImageLarge = CType(resources.GetObject("btnProperties.ImageLarge"), System.Drawing.Image)
        Me.btnProperties.Name = "btnProperties"
        Me.btnProperties.TooltipText = "Properties"
        '
        'btnDesign
        '
        Me.btnDesign.Enabled = False
        Me.btnDesign.Image = CType(resources.GetObject("btnDesign.Image"), System.Drawing.Image)
        Me.btnDesign.ImageLarge = CType(resources.GetObject("btnDesign.ImageLarge"), System.Drawing.Image)
        Me.btnDesign.Name = "btnDesign"
        Me.btnDesign.TooltipText = "Report Design"
        Me.btnDesign.Visible = False
        '
        'btnCalendar
        '
        Me.btnCalendar.Image = CType(resources.GetObject("btnCalendar.Image"), System.Drawing.Image)
        Me.btnCalendar.ImageLarge = CType(resources.GetObject("btnCalendar.ImageLarge"), System.Drawing.Image)
        Me.btnCalendar.Name = "btnCalendar"
        Me.btnCalendar.TooltipText = "Custom Calendar"
        '
        'btnAddressBook
        '
        Me.btnAddressBook.Image = CType(resources.GetObject("btnAddressBook.Image"), System.Drawing.Image)
        Me.btnAddressBook.ImageLarge = CType(resources.GetObject("btnAddressBook.ImageLarge"), System.Drawing.Image)
        Me.btnAddressBook.Name = "btnAddressBook"
        Me.btnAddressBook.TooltipText = "Address Book"
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Enabled = False
        Me.BubbleButton1.Image = CType(resources.GetObject("BubbleButton1.Image"), System.Drawing.Image)
        Me.BubbleButton1.ImageLarge = CType(resources.GetObject("BubbleButton1.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton1.Name = "BubbleButton1"
        '
        'btnSingle
        '
        Me.btnSingle.Image = CType(resources.GetObject("btnSingle.Image"), System.Drawing.Image)
        Me.btnSingle.ImageLarge = CType(resources.GetObject("btnSingle.ImageLarge"), System.Drawing.Image)
        Me.btnSingle.Name = "btnSingle"
        Me.btnSingle.TooltipText = "Single Schedule"
        '
        'btnPackage
        '
        Me.btnPackage.Image = CType(resources.GetObject("btnPackage.Image"), System.Drawing.Image)
        Me.btnPackage.ImageLarge = CType(resources.GetObject("btnPackage.ImageLarge"), System.Drawing.Image)
        Me.btnPackage.Name = "btnPackage"
        Me.btnPackage.TooltipText = "Packaged Reports Schedule"
        '
        'btnDynamic
        '
        Me.btnDynamic.Image = CType(resources.GetObject("btnDynamic.Image"), System.Drawing.Image)
        Me.btnDynamic.ImageLarge = CType(resources.GetObject("btnDynamic.ImageLarge"), System.Drawing.Image)
        Me.btnDynamic.Name = "btnDynamic"
        Me.btnDynamic.TooltipText = "Dynamic Schedule"
        '
        'btnDynamicPackage
        '
        Me.btnDynamicPackage.Image = CType(resources.GetObject("btnDynamicPackage.Image"), System.Drawing.Image)
        Me.btnDynamicPackage.ImageLarge = CType(resources.GetObject("btnDynamicPackage.ImageLarge"), System.Drawing.Image)
        Me.btnDynamicPackage.Name = "btnDynamicPackage"
        Me.btnDynamicPackage.TooltipText = "Dynamic Package Schedule"
        '
        'btnAutomation
        '
        Me.btnAutomation.Image = CType(resources.GetObject("btnAutomation.Image"), System.Drawing.Image)
        Me.btnAutomation.ImageLarge = CType(resources.GetObject("btnAutomation.ImageLarge"), System.Drawing.Image)
        Me.btnAutomation.Name = "btnAutomation"
        Me.btnAutomation.TooltipText = "Automation Schedule"
        '
        'btnEvent
        '
        Me.btnEvent.Image = CType(resources.GetObject("btnEvent.Image"), System.Drawing.Image)
        Me.btnEvent.ImageLarge = CType(resources.GetObject("btnEvent.ImageLarge"), System.Drawing.Image)
        Me.btnEvent.Name = "btnEvent"
        Me.btnEvent.TooltipText = "Event-Based Schedule"
        '
        'btnEventPackage
        '
        Me.btnEventPackage.Image = CType(resources.GetObject("btnEventPackage.Image"), System.Drawing.Image)
        Me.btnEventPackage.ImageLarge = CType(resources.GetObject("btnEventPackage.ImageLarge"), System.Drawing.Image)
        Me.btnEventPackage.Name = "btnEventPackage"
        Me.btnEventPackage.TooltipText = "Event-Based Package"
        '
        'btnBursting
        '
        Me.btnBursting.Image = CType(resources.GetObject("btnBursting.Image"), System.Drawing.Image)
        Me.btnBursting.ImageLarge = CType(resources.GetObject("btnBursting.ImageLarge"), System.Drawing.Image)
        Me.btnBursting.Name = "btnBursting"
        Me.btnBursting.TooltipText = "Bursting Schedule"
        Me.btnBursting.Visible = False
        '
        'btnDDSchedule
        '
        Me.btnDDSchedule.Image = CType(resources.GetObject("btnDDSchedule.Image"), System.Drawing.Image)
        Me.btnDDSchedule.ImageLarge = CType(resources.GetObject("btnDDSchedule.ImageLarge"), System.Drawing.Image)
        Me.btnDDSchedule.Name = "btnDDSchedule"
        Me.btnDDSchedule.TooltipText = "Data-Driven Schedule"
        '
        'btnDDPackage
        '
        Me.btnDDPackage.Image = Global.sqlrd.My.Resources.Resources.data_driven_box
        Me.btnDDPackage.ImageLarge = Global.sqlrd.My.Resources.Resources.data_driven_box
        Me.btnDDPackage.Name = "btnDDPackage"
        '
        'BubbleButton2
        '
        Me.BubbleButton2.Enabled = False
        Me.BubbleButton2.Image = CType(resources.GetObject("BubbleButton2.Image"), System.Drawing.Image)
        Me.BubbleButton2.ImageLarge = CType(resources.GetObject("BubbleButton2.ImageLarge"), System.Drawing.Image)
        Me.BubbleButton2.Name = "BubbleButton2"
        '
        'btnHelp
        '
        Me.btnHelp.Image = CType(resources.GetObject("btnHelp.Image"), System.Drawing.Image)
        Me.btnHelp.ImageLarge = CType(resources.GetObject("btnHelp.ImageLarge"), System.Drawing.Image)
        Me.btnHelp.Name = "btnHelp"
        Me.btnHelp.TooltipText = "Help"
        '
        'btnForums
        '
        Me.btnForums.Image = CType(resources.GetObject("btnForums.Image"), System.Drawing.Image)
        Me.btnForums.ImageLarge = CType(resources.GetObject("btnForums.ImageLarge"), System.Drawing.Image)
        Me.btnForums.Name = "btnForums"
        Me.btnForums.TooltipText = "User Forums"
        '
        'btnKbase
        '
        Me.btnKbase.Image = CType(resources.GetObject("btnKbase.Image"), System.Drawing.Image)
        Me.btnKbase.ImageLarge = CType(resources.GetObject("btnKbase.ImageLarge"), System.Drawing.Image)
        Me.btnKbase.Name = "btnKbase"
        Me.btnKbase.TooltipText = "Knowledge Base"
        '
        'btnHowTo
        '
        Me.btnHowTo.Image = CType(resources.GetObject("btnHowTo.Image"), System.Drawing.Image)
        Me.btnHowTo.ImageLarge = CType(resources.GetObject("btnHowTo.ImageLarge"), System.Drawing.Image)
        Me.btnHowTo.Name = "btnHowTo"
        Me.btnHowTo.TooltipText = "How To...Demos"
        '
        'frmDock
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(566, 118)
        Me.Controls.Add(Me.BubbleBar)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmDock"
        Me.Opacity = 0.6
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.TransparencyKey = System.Drawing.SystemColors.Control
        CType(Me.BubbleBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BubbleBar As DevComponents.DotNetBar.BubbleBar
    Friend WithEvents BubbleBarTab2 As DevComponents.DotNetBar.BubbleBarTab
    Friend WithEvents btnOptions As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnSystemMonitor As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnProperties As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDesign As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnCalendar As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAddressBook As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnSingle As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamic As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamicPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAutomation As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEvent As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnBursting As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton2 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnHelp As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnForums As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnKbase As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnHowTo As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEventPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDDSchedule As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDDPackage As DevComponents.DotNetBar.BubbleButton
End Class
