<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucExistingReports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucExistingReports))
        Me.pnExisting = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lsvSchedules = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector2 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle2 = New DevComponents.DotNetBar.ElementStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnDown = New DevComponents.DotNetBar.ButtonX()
        Me.btnUp = New DevComponents.DotNetBar.ButtonX()
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.btnAdd = New DevComponents.DotNetBar.ButtonX()
        Me.tvSchedules = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuParameters = New System.Windows.Forms.ContextMenu()
        Me.mnuParameter = New System.Windows.Forms.MenuItem()
        Me.pnExisting.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.lsvSchedules, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.tvSchedules, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnExisting
        '
        Me.pnExisting.BackColor = System.Drawing.Color.Transparent
        Me.pnExisting.Controls.Add(Me.Panel2)
        Me.pnExisting.Controls.Add(Me.ExpandableSplitter1)
        Me.pnExisting.Controls.Add(Me.Panel1)
        Me.pnExisting.Controls.Add(Me.tvSchedules)
        Me.pnExisting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnExisting.Location = New System.Drawing.Point(0, 0)
        Me.pnExisting.Name = "pnExisting"
        Me.pnExisting.Size = New System.Drawing.Size(570, 368)
        Me.pnExisting.TabIndex = 8
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lsvSchedules)
        Me.Panel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(249, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(321, 368)
        Me.Panel2.TabIndex = 8
        '
        'lsvSchedules
        '
        Me.lsvSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.lsvSchedules.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.lsvSchedules.BackgroundStyle.Class = "TreeBorderKey"
        Me.lsvSchedules.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSchedules.DragDropEnabled = False
        Me.lsvSchedules.DragDropNodeCopyEnabled = False
        Me.lsvSchedules.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.lsvSchedules.Location = New System.Drawing.Point(0, 0)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.NodesConnector = Me.NodeConnector2
        Me.lsvSchedules.NodeStyle = Me.ElementStyle2
        Me.lsvSchedules.PathSeparator = ";"
        Me.lsvSchedules.Size = New System.Drawing.Size(321, 339)
        Me.lsvSchedules.Styles.Add(Me.ElementStyle2)
        Me.lsvSchedules.TabIndex = 6
        Me.lsvSchedules.Text = "AdvTree2"
        '
        'NodeConnector2
        '
        Me.NodeConnector2.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle2
        '
        Me.ElementStyle2.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle2.Name = "ElementStyle2"
        Me.ElementStyle2.TextColor = System.Drawing.SystemColors.ControlText
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.btnDown)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnUp)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 339)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(321, 29)
        Me.FlowLayoutPanel1.TabIndex = 7
        '
        'btnDown
        '
        Me.btnDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Location = New System.Drawing.Point(283, 3)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(35, 23)
        Me.btnDown.TabIndex = 1
        '
        'btnUp
        '
        Me.btnUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Location = New System.Drawing.Point(242, 3)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(35, 23)
        Me.btnUp.TabIndex = 2
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.ForeColor = System.Drawing.Color.Black
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(213, Byte), Integer), CType(CType(140, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer), CType(CType(246, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(246, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 368)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 5
        Me.ExpandableSplitter1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnRemove)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(198, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(48, 368)
        Me.Panel1.TabIndex = 3
        '
        'btnRemove
        '
        Me.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(6, 194)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(35, 23)
        Me.btnRemove.TabIndex = 0
        '
        'btnAdd
        '
        Me.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(6, 153)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnAdd.TabIndex = 0
        '
        'tvSchedules
        '
        Me.tvSchedules.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvSchedules.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvSchedules.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvSchedules.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvSchedules.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvSchedules.DragDropEnabled = False
        Me.tvSchedules.DragDropNodeCopyEnabled = False
        Me.tvSchedules.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvSchedules.Location = New System.Drawing.Point(0, 0)
        Me.tvSchedules.Name = "tvSchedules"
        Me.tvSchedules.NodesConnector = Me.NodeConnector1
        Me.tvSchedules.NodeStyle = Me.ElementStyle1
        Me.tvSchedules.PathSeparator = ";"
        Me.tvSchedules.Size = New System.Drawing.Size(198, 368)
        Me.tvSchedules.Styles.Add(Me.ElementStyle1)
        Me.tvSchedules.TabIndex = 4
        Me.tvSchedules.Text = "AdvTree1"
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        Me.imgFolders.Images.SetKeyName(12, "")
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'ucExistingReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnExisting)
        Me.Name = "ucExistingReports"
        Me.Size = New System.Drawing.Size(570, 368)
        Me.pnExisting.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.lsvSchedules, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.tvSchedules, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnExisting As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    Friend WithEvents lsvSchedules As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector2 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle2 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvSchedules As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnUp As DevComponents.DotNetBar.ButtonX

End Class
