Public Class ucConditions
    Dim nEventID As Integer = 99999
    Dim nOrderID As Integer = 0

    Public Property m_EventID() As Integer
        Get
            Return nEventID
        End Get
        Set(ByVal value As Integer)
            nEventID = value
        End Set
    End Property

    Public Property m_OrderID() As Integer
        Get
            Return nOrderID
        End Get
        Set(ByVal value As Integer)
            nOrderID = value
        End Set
    End Property

    Public ReadOnly Property numberofEnabledConditions() As Integer
        Get
            Dim count As Integer = 0

            For Each it As ListViewItem In lsvConditions.Items
                If it.Checked = True Then count += 1
            Next

            Return count
        End Get
    End Property
    Public Sub LoadConditions(Optional ByVal EventID As Integer = 99999)
        Me.m_EventID = EventID

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM EventConditions WHERE EventID = " & EventID & " ORDER BY OrderID"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then

            Me.lsvConditions.Items.Clear()

            Do While oRs.EOF = False
                Dim item As ListViewItem

                item = Me.lsvConditions.Items.Add(oRs("conditionname").Value)

                item.SubItems.Add(CType(oRs("conditiontype").Value, String).ToUpper)

                If oRs("returnvalue").Value = 1 Then
                    item.SubItems.Add("TRUE")
                Else
                    item.SubItems.Add("FALSE")
                End If

                item.Tag = oRs("conditionid").Value

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub


    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click, lsvConditions.DoubleClick
        If Me.lsvConditions.SelectedItems.Count = 0 Then Return

        Dim item As ListViewItem = lsvConditions.SelectedItems(0)
        Dim conditionID As Integer
        Dim condition As String

        condition = item.SubItems(1).Text
        conditionID = item.Tag

        Select Case condition.ToLower
            Case "file exists"
                Dim oForm As New frmEventFileExists

                oForm.m_Type = frmEventFileExists.TYPES.FILE_EXISTS

                oForm.EditCondition(conditionID)
            Case "database record exists"
                Dim oform As New frmEventDatabaseExists

                oform.m_Type = frmEventDatabaseExists.TYPES.DB_EXISTS

                oform.EditCondition(conditionID)
            Case "file has been modified"
                Dim oform As New frmEventFileExists

                oform.m_Type = frmEventFileExists.TYPES.FILE_MODIFIED

                oform.EditCondition(conditionID)
            Case "window is present"
                Dim oform As New frmEventProcessExists

                oform.m_Type = frmEventProcessExists.TYPES.WINDOW_EXISTS

                oform.EditCondition(conditionID)
            Case "process exists"
                Dim oform As New frmEventProcessExists

                oform.m_Type = frmEventProcessExists.TYPES.PROCESS_EXISTS

                oform.EditCondition(conditionID)
            Case "unread email is present"
                Dim oform As New frmEventEmailExists

                oform.EditCondition(conditionID)

            Case "database record has changed"
                Dim oform As New frmEventDatabaseExists

                oform.m_Type = frmEventDatabaseExists.TYPES.DB_MODIFIED

                oform.EditCondition(conditionID)
            Case "data received"
                Dim frm As New frmEventDataRecvd

                frm.EditCondition(conditionID)
        End Select

        Me.LoadConditions(Me.m_EventID)
    End Sub

    Private Sub btnAdd_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnAdd.MouseClick
        Me.mnuType.Show(btnAdd, New Point(e.X, e.Y))
    End Sub

    Private Sub DatabaseRecordExistsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles DatabaseRecordExistsToolStripMenuItem.Click, DatabaseRecordHasBeenModifiedToolStripMenuItem.Click, _
    FileExistsToolStripMenuItem.Click, FileHasBeenModifiedToolStripMenuItem.Click, ProcessExistsToolStripMenuItem.Click, _
    UnreadMailIsPresentToolStripMenuItem.Click, _
    WindowIsPresentToolStripMenuItem.Click, DataIsReceivedOnAPortToolStripMenuItem.Click

        Dim mnu As ToolStripMenuItem = CType(sender, ToolStripMenuItem)

        If lsvConditions.Items.Count > 0 And IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
            _NeedUpgrade(gEdition.CORPORATE, lsvConditions, "Advanced Events Pack")
            Return
        End If

        nOrderID = lsvConditions.Items.Count + 1

        Select Case mnu.Text.ToLower
            Case "database record exists"

                Dim oForm As frmEventDatabaseExists = New frmEventDatabaseExists

                oForm.m_Type = frmEventDatabaseExists.TYPES.DB_EXISTS

                oForm.AddCondition(nEventID, nOrderID)
            Case "database record has been modified"
                Dim oForm As frmEventDatabaseExists = New frmEventDatabaseExists

                oForm.m_Type = frmEventDatabaseExists.TYPES.DB_MODIFIED

                oForm.AddCondition(nEventID, nOrderID)

            Case "file exists"

                Dim oForm As frmEventFileExists = New frmEventFileExists

                oForm.m_Type = frmEventFileExists.TYPES.FILE_EXISTS

                oForm.AddCondition(nEventID, nOrderID)

            Case "file has been modified"
                Dim oForm As frmEventFileExists = New frmEventFileExists

                oForm.m_Type = frmEventFileExists.TYPES.FILE_MODIFIED

                oForm.AddCondition(nEventID, nOrderID)

            Case "process exists"
                Dim oForm As frmEventProcessExists = New frmEventProcessExists

                oForm.m_Type = frmEventProcessExists.TYPES.PROCESS_EXISTS

                oForm.AddCondition(nEventID, nOrderID)

            Case "unread mail is present"

                Dim oForm As frmEventEmailExists = New frmEventEmailExists

                oForm.AddCondition(nEventID, nOrderID)

            Case "window is present"
                Dim oForm As frmEventProcessExists = New frmEventProcessExists

                oForm.m_Type = frmEventProcessExists.TYPES.WINDOW_EXISTS

                oForm.AddCondition(nEventID, nOrderID)
            Case "data is received on a port"
                If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
                    _NeedUpgrade(gEdition.CORPORATE, DatabaseRecordHasBeenModifiedToolStripMenuItem, "Advanced Events Pack")
                    Return
                End If

                Dim frm As frmEventDataRecvd = New frmEventDataRecvd

                frm.AddCondition(nEventID, nOrderID)
        End Select

        Me.LoadConditions(Me.m_EventID)
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If lsvConditions.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected event definition?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) _
        = DialogResult.Yes Then
            Dim ConditionID As Integer = lsvConditions.SelectedItems(0).Tag

            clsMarsEvent.DeleteCondition(ConditionID)

            lsvConditions.SelectedItems(0).Remove()
        End If
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click

        If lsvConditions.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvConditions.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvConditions.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT OrderID FROM EventConditions WHERE ConditionID = " & olsv.Tag)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                OldID = oRs(0).Value
            End If

            oRs.Close()
        End If

        oRs = clsMarsData.GetData("SELECT OrderID FROM EventConditions WHERE ConditionID = " & olsv2.Tag)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                NewID = oRs(0).Value
            End If

            oRs.Close()
        End If

        Dim SQL As String

        'update the item being moved up

        SQL = "UPDATE EventConditions SET OrderID = " & NewID & " WHERE ConditionID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE EventConditions SET OrderID = " & OldID & " WHERE ConditionID = " & olsv2.Tag

        clsMarsData.WriteData(SQL)

        Me.LoadConditions(Me.m_EventID)

        For Each olsv In lsvConditions.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub

    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If lsvConditions.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvConditions.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvConditions.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvConditions.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT OrderID FROM EventConditions WHERE ConditionID = " & olsv.Tag)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                OldID = oRs(0).Value
            End If

            oRs.Close()
        End If

        oRs = clsMarsData.GetData("SELECT OrderID FROM EventConditions WHERE ConditionID = " & olsv2.Tag)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                NewID = oRs(0).Value
            End If

            oRs.Close()
        End If

        Dim SQL As String

        'update the item being moved down

        SQL = "UPDATE EventConditions SET OrderID = " & NewID & " WHERE ConditionID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE EventConditions SET OrderID = " & OldID & " WHERE ConditionID = " & olsv2.Tag

        clsMarsData.WriteData(SQL)

        Me.LoadConditions(Me.m_EventID)

        For Each olsv In lsvConditions.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub


    Private Sub RefreshData()
        If lsvConditions.SelectedItems.Count = 0 Then Return

        Dim conditionID As Integer = lsvConditions.SelectedItems(0).Tag

        Dim SQL As String = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                Dim type As String = oRs("conditiontype").Value
                Dim cacheFolderName As String = IsNull(oRs("cachefoldername").Value)
                Dim sName As String = oRs("conditionname").Value
                Dim NewFolderName As String = sName & "." & conditionID

                If cacheFolderName = "" Then cacheFolderName = NewFolderName

                If type.ToLower.IndexOf("database") = -1 Then
                    oRs.Close()
                    Return
                End If

                Dim connString As String = oRs("connectionstring").Value
                SQL = oRs("searchcriteria").Value

                Dim AnyAll As String = IsNull(oRs("anyall").Value)
                Dim keyColumn As String = oRs("keycolumn").Value

                If type = "database record exists" Then
                    If AnyAll = "NEW" Then clsMarsEvent.CacheData(lsvConditions.SelectedItems(0).Text, SQL, connString, NewFolderName)
                Else
                    clsMarsEvent.CacheData(lsvConditions.SelectedItems(0).Text, SQL, connString, keyColumn, NewFolderName)
                End If

                If cacheFolderName <> NewFolderName Then
                    clsMarsData.WriteData("UPDATE EventConditions SET cachefoldername ='" & SQLPrepare(NewFolderName) & "' WHERE ConditionID=" & conditionID)

                    IO.File.Delete(cacheFolderName)
                End If
            End If
        End If


    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If lsvConditions.SelectedItems.Count = 0 Then Return

        Dim oRs As ADODB.Recordset
        Dim conditionType As String = ""
        Dim file As String
        Dim nID As Integer = lsvConditions.SelectedItems(0).Tag

        oRs = clsMarsData.GetData("SELECT ConditionType, FilePath FROM EventConditions WHERE ConditionID =" & nID)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                conditionType = oRs(0).Value
                file = IsNull(oRs("filepath").Value, "")
            End If
        End If

        If conditionType.ToLower.IndexOf("database") > -1 Then
            RefreshData()
            clsMarsData.WriteData("UPDATE EventConditions SET " & _
            "IgnoreNow = 0," & _
            "DataLastModified ='" & ConDateTime(Date.Now) & "' WHERE ConditionID =" & nID)
        ElseIf conditionType = "file has been modified" Then
            Dim fileInfo As IO.FileInfo = New IO.FileInfo(file)

            Dim dateMod As Date = ConDateTime(fileInfo.LastWriteTime)

            clsMarsData.WriteData("UPDATE EventConditions SET KeyColumn ='" & dateMod & "' WHERE ConditionID = " & nID)
        End If

        MessageBox.Show("The cached data has been refreshed successfully", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub lsvConditions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvConditions.SelectedIndexChanged
        If lsvConditions.SelectedItems.Count = 0 Then Return

        Dim type As String = lsvConditions.SelectedItems(0).SubItems(1).Text.ToLower

        Select Case type
            Case "file has been modified"
                btnRefresh.Enabled = True
            Case "database record exists"
                btnRefresh.Enabled = True
            Case "database record has changed"
                btnRefresh.Enabled = True
            Case Else
                btnRefresh.Enabled = False
        End Select
    End Sub



    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

    End Sub

    Private Sub DataIsReceivedOnAPortToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles DataIsReceivedOnAPortToolStripMenuItem.Click

    End Sub
End Class
