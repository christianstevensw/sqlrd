Imports Rebex.Net
Public Class ucFTPDetails
    Dim ep As ErrorProvider = New ErrorProvider
    Dim ftpSuccess As Boolean
    Dim oField As TextBox
    Dim showBrowse As Boolean = True

    Public m_eventID As Integer = 99999

    Public Property m_ShowBrowse() As Boolean
        Get
            Return showBrowse
        End Get
        Set(ByVal value As Boolean)
            showBrowse = value

            Me.txtDirectory.Visible = value
            Me.btnBrowseFTP.Visible = value
            Label6.Visible = value
        End Set
    End Property

    Public Property m_browseFTPForFile As Boolean

    Public Property m_ftpSuccess() As Boolean
        Get
            Return ftpSuccess
        End Get
        Set(ByVal value As Boolean)
            ftpSuccess = value
        End Set
    End Property

    Public Property m_ftpOptions() As String
        Get
            'use the following template
            'UsePFX;UsePVK;CertFile;KeyFile;CertName;CertPassword;UseProxy;ProxyServer;ProxyUser;ProxyPassword;ProxyPort
            Dim value As String

            value &= "UsePFX=" & Convert.ToInt32(Me.optPFX.Checked) & ";"

            value &= "UsePVK=" & Convert.ToInt32(Me.optPVK.Checked) & ";"

            value &= "CertFile=" & Me.txtCertFile.Text & ";"

            value &= "KeyFile=" & Me.txtPrivateKeyFile.Text & ";"

            value &= "CertName=" & Me.txtCertName.Text & ";"

            value &= "CertPassword=" & _EncryptDBValue(Me.txtCertPassword.Text) & ";"

            value &= "UseProxy=" & Convert.ToInt32(Me.chkProxy.Checked) & ";"

            value &= "ProxyServer=" & Me.txtProxyServer.Text & ";"

            value &= "ProxyUser=" & Me.txtProxyUserName.Text & ";"

            value &= "ProxyPassword=" & _EncryptDBValue(Me.txtProxyPassword.Text) & ";"

            value &= "ProxyPort=" & Me.txtProxyPort.Value & ";"

            value &= "AsciiMode=" & Convert.ToInt32(Me.chkAscii.Checked) & ";"

            value &= "RetryUpload=" & Convert.ToInt32(Me.chkRetryUpload.Checked) & ";"

            value &= "ClearChannel=" & Convert.ToInt32(chkClearChannel.Checked) & ";"

            Return value
        End Get

        Set(ByVal value As String)
            Dim tmp As String() = value.Split(";")

            For Each entry As String In tmp
                If entry = "" Or entry Is Nothing Then Continue For

                Dim propName As String = entry.Split("=")(0)
                Dim propValue As String = ""

                For I As Integer = 1 To entry.Split("=").GetUpperBound(0)
                    propValue = propValue & entry.Split("=")(I) & "="
                Next

                If propValue <> "" Then
                    propValue = propValue.Remove(propValue.Length - 1, 1)
                End If

                Select Case propName.ToLower
                    Case "usepfx"
                        Me.optPFX.Checked = Convert.ToInt32(propValue)
                    Case "usepvk"
                        Me.optPVK.Checked = Convert.ToInt32(propValue)
                    Case "certfile"
                        Me.txtCertFile.Text = propValue
                    Case "keyfile"
                        Me.txtPrivateKeyFile.Text = propValue
                    Case "certname"
                        Me.txtCertName.Text = propValue
                    Case "certpassword"
                        Me.txtCertPassword.Text = _DecryptDBValue(propValue)
                    Case "useproxy"
                        Me.chkProxy.Checked = Convert.ToInt32(propValue)
                    Case "proxyserver"
                        Me.txtProxyServer.Text = propValue
                    Case "proxyuser"
                        Me.txtProxyUserName.Text = propValue
                    Case "proxypassword"
                        Me.txtProxyPassword.Text = _DecryptDBValue(propValue)
                    Case "proxyport"
                        Me.txtProxyPort.Value = propValue
                    Case "asciimode"
                        Me.chkAscii.Checked = Convert.ToInt32(propValue)
                    Case "retryupload"
                        Me.chkRetryUpload.Checked = Convert.ToInt32(propValue)
                    Case "clearchannel"
                        chkClearChannel.Checked = Convert.ToInt32(propValue)
                End Select
            Next
        End Set

    End Property

    Public Function validateFtpInfo(Optional ByVal IsDynamic As Boolean = False) As Boolean
        If Me.txtFTPServer.Text = "" And IsDynamic = False Then
            SetError(Me.txtFTPServer, "Missing server information...")
            Me.txtFTPServer.Focus()
            Return False
        ElseIf Me.txtUserName.Text = "" Then
            SetError(Me.txtUserName, "Please enter the username")
            txtUserName.Focus()
            Return False
        ElseIf Me.txtPassword.Text = "" And optPVK.Checked = False Then
            setError(Me.txtPassword, "Please enter the password")
            Me.txtPassword.Focus()
            Return False
        ElseIf Me.chkProxy.Checked And Me.txtProxyServer.Text = "" Then
            SetError(Me.txtProxyServer, "Please enter the Proxy server")
            Me.txtProxyServer.Focus()
            Return False
        End If

        Return True
    End Function
    Public Sub ResetAll()
        Try
            txtCertFile.Text = ""
            txtCertPassword.Text = ""
            txtDirectory.Text = ""
            txtFTPServer.Text = ""
            txtPassword.Text = ""
            txtPort.Text = 25
            txtPrivateKeyFile.Text = ""
            txtProxyServer.Text = ""
            txtProxyUserName.Text = ""
            txtUserName.Text = ""

            chkAscii.Checked = False
            chkPassive.Checked = False
            chkProxy.Checked = False

            cmbFTPType.SelectedIndex = 0
        Catch : End Try
    End Sub

    Public Function ConnectSFTP() As Rebex.Net.Sftp
        Try
            Dim ftp As Rebex.Net.Sftp = New Rebex.Net.Sftp
            Dim log As Rebex.Net.ProxySocket = New Rebex.Net.ProxySocket

            AddHandler ftp.ResponseRead, AddressOf ResponseRead
            AddHandler ftp.CommandSent, AddressOf CommandSent

            clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Connecting SFTP to " & clsMarsParser.Parser.ParseString(txtFTPServer.Text) & " on " & txtPort.Value, True)

            ftp.Connect(clsMarsParser.Parser.ParseString(txtFTPServer.Text), txtPort.Value)

            If chkProxy.Checked Then
                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Configuring proxy settings", True)
                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential
                cred.UserName = clsMarsParser.Parser.ParseString(Me.txtProxyUserName.Text)
                cred.Password = clsMarsParser.Parser.ParseString(Me.txtProxyPassword.Text)

                ftp.Proxy = New Rebex.Net.Proxy(Rebex.Net.ProxyType.HttpConnect, Rebex.Net.ProxyAuthentication.Basic, Me.txtProxyServer.Text, _
                Me.txtProxyPort.Value, cred)
            End If

            clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Logging into FTP server as " & txtUserName.Text, True)

            If optPVK.Checked And txtPrivateKeyFile.Text <> "" Then
                '// create client and connect 

                '// verify the server's fingerprint (client.Fingerprint) 
                '...
                '// load the private key
                Dim privateKey As Rebex.Net.SshPrivateKey

                If txtCertPassword.Text = "" Then
                    privateKey = New Rebex.Net.SshPrivateKey(txtPrivateKeyFile.Text, Nothing)
                Else
                    privateKey = New Rebex.Net.SshPrivateKey(txtPrivateKeyFile.Text, clsMarsParser.Parser.ParseString(txtCertPassword.Text))
                End If

                ftp.Login(clsMarsParser.Parser.ParseString(txtUserName.Text), privateKey)
            Else
                ftp.Login(clsMarsParser.Parser.ParseString(txtUserName.Text), clsMarsParser.Parser.ParseString(txtPassword.Text))
            End If

            clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "SFTP connection established successfully", True)

            If chkAscii.Checked Then ftp.TransferType = SftpTransferType.Ascii

            Return ftp
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "Please check the provided credentials and server address")

            clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "SFTP connection failed due to error: " & ex.Message, True)

            Return Nothing
        End Try
    End Function
    Public Function ConnectFTP() As Object 'Chilkat.Ftp2
        Try
            Dim ftp As Chilkat.Ftp2 = New Chilkat.Ftp2
            Dim logFile As String = "ftp_" & gScheduleName

            ' ftp.SendBufferSize = 0

            Dim ok As Boolean = ftp.UnlockComponent("CHRISTFTP_QZc6MEfg5UnY")

            clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Preparing to connect using FTP type: " & cmbFTPType.Text, True)

            Select Case cmbFTPType.Text
                Case "FTP - SSL 3.1 (TLS)"
                    ftp.AuthTls = True
                    ftp.Ssl = False
                Case "FTP - SSL 3.0"
                    ftp.Ssl = True
                    ftp.AuthTls = False
                Case "FTP - SSL 3.0 (Implicit)"
                    ftp.Ssl = True
                    ftp.AuthTls = False
                Case "FTP - SSH (FTPS)"
                    Return ConnectSFTP()
            End Select

            ftp.Hostname = clsMarsParser.Parser.ParseString(txtFTPServer.Text)
            ftp.Username = clsMarsParser.Parser.ParseString(txtUserName.Text)
            ftp.Password = clsMarsParser.Parser.ParseString(txtPassword.Text)

            ftp.Passive = Me.chkPassive.Checked
            ftp.Port = Me.txtPort.Value

            If chkAscii.Checked Then ftp.SetTypeAscii()

            ftp.KeepSessionLog = True

            If Me.optPVK.Checked And Me.txtCertFile.Text <> "" Then

                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Configuring PVK and PFX certificates", True)

                Dim certStore As New Chilkat.CertStore()
                Dim password As String = clsMarsParser.Parser.ParseString(Me.txtCertPassword.Text)

                '  Load the certs from a PFX into an in-memory certificate store:
                Dim success As Boolean = certStore.LoadPfxFile(clsMarsParser.Parser.ParseString(Me.txtCertFile.Text), password)

                If (success <> True) Then
                    Throw New Exception(certStore.LastErrorText)
                End If

                '  Find the exact cert we'll use:
                Dim cert As Chilkat.Cert
                cert = certStore.FindCertBySubject(Me.txtCertName.Text)

                If (cert Is Nothing) Then
                    Throw New Exception("Certificate '" & txtCertName.Text & "' not found!")

                End If

                '  Use this certificate for our secure (SSL/TLS) connection:
                ftp.SetSslClientCert(cert)
            ElseIf Me.optPFX.Checked And Me.txtCertFile.Text <> "" Then
                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Configuring PEM and DER files", True)

                Dim cert As New Chilkat.Cert()

                '  LoadFromFile will load either PEM and DER formatted files.
                '  It automatically recognizes the file format based on the
                '  file contents.

                clsMarsDebug.writeToDebug(logFile, "Loading Cert file")

                Dim success As Boolean = cert.LoadFromFile(clsMarsParser.Parser.ParseString(Me.txtCertFile.Text)) ' cert.LoadPfxFile(clsMarsParser.Parser.ParseString(Me.txtCertFile.Text), txtCertPassword.Text) '

                If (success <> True) Then
                    clsMarsDebug.writeToDebug(logFile, "Certificate failed to load with error " & cert.LastErrorText)
                    Throw New Exception(cert.LastErrorText)
                End If

                Dim password As String = Me.txtCertPassword.Text

                If txtPrivateKeyFile.Text <> "" Then
                    clsMarsDebug.writeToDebug(logFile, "Loading Private Key File")

                    Dim pvk As New Chilkat.PrivateKey()

                    success = pvk.LoadPvkFile(clsMarsParser.Parser.ParseString(txtPrivateKeyFile.Text), password)

                    If (success <> True) Then
                        clsMarsDebug.writeToDebug(logFile, "Error loading private key file " & pvk.LastErrorText)
                        Throw New Exception(pvk.LastErrorText)
                    End If

                    ''  Import the private key to a Windows key container and link
                    ''  it to the certificate.  (It's OK if the key is already
                    ''  imported and present in the key container...)
                    'Dim bForSigning As Boolean
                    'Dim bForKeyExchange As Boolean
                    'Dim bMachineKeyset As Boolean
                    'Dim bNeedPrivateKeyAccess As Boolean
                    'Dim keyContainerName As String

                    ''  Choose anything for the key container name.
                    'keyContainerName = "MyCertForFtp"

                    ''  We'll import the key to our logged-on user keyset rather
                    ''  than the machine keyset:
                    'bMachineKeyset = False

                    'bNeedPrivateKeyAccess = True

                    'clsMarsDebug.writeToDebug(logFile, "Opening Key Container if one exists")
                    ''  Create a key container and import the private key.
                    'Dim keyContainer As New Chilkat.KeyContainer()
                    'success = keyContainer.OpenContainer(keyContainerName, bNeedPrivateKeyAccess, bMachineKeyset)

                    'If (success <> True) Then
                    '    clsMarsDebug.writeToDebug(logFile, "Container not found. Creating one")
                    '    success = keyContainer.CreateContainer(keyContainerName, bMachineKeyset)
                    'End If

                    'If (success <> True) Then
                    '    clsMarsDebug.writeToDebug(logFile, "Error creating container " & keyContainer.LastErrorText)
                    '    Throw New Exception(keyContainer.LastErrorText)
                    'End If

                    ''  Import the private key into the key container.
                    ''  We're using the key for key exchange, not signing:
                    'bForKeyExchange = True

                    'clsMarsDebug.writeToDebug(logFile, "Importing private key file")

                    'success = keyContainer.ImportPrivateKey(pvk, bForKeyExchange)

                    'If (success <> True) Then
                    '    clsMarsDebug.writeToDebug(logFile, "Error importing key file " & keyContainer.LastErrorText)
                    '    Throw New Exception(keyContainer.LastErrorText)
                    'End If

                    ''  Link the cert with the private key in the key container.
                    'bForSigning = False

                    'clsMarsDebug.writeToDebug(logFile, "Linking private key file to certificate")

                    'success = cert.LinkPrivateKey(keyContainerName, bMachineKeyset, bForSigning)

                    'If (success <> True) Then
                    '    clsMarsDebug.writeToDebug(logFile, "Error linking PVK to PFX " & cert.LastErrorText)

                    '    Throw New Exception(cert.LastErrorText)
                    'End If

                    '  The cert now has access to a private key and is ready to be
                    '  used...

                    '  Use this certificate for our secure (SSL/TLS) connection:

                    clsMarsDebug.writeToDebug(logFile, "Adding the private key to the certificate")
                    success = cert.SetPrivateKey(pvk)

                End If

                clsMarsDebug.writeToDebug(logFile, "Setting the FTP certificate for the connection")


                success = ftp.SetSslClientCert(cert)

                If Not success Then
                    Throw New Exception(ftp.LastErrorText)
                End If
            End If

            If Me.chkProxy.Checked = True And Me.txtProxyServer.Text <> "" Then
                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "Configuring Proxy server", True)

                ftp.ProxyHostname = clsMarsParser.Parser.ParseString(txtProxyServer.Text)
                ftp.ProxyPort = clsMarsParser.Parser.ParseString(txtProxyPort.Value)

                '  Note: Your FTP Proxy server may or may not require authentication.
                If clsMarsParser.Parser.ParseString(Me.txtProxyUserName.Text) <> "" Then
                    ftp.ProxyUsername = clsMarsParser.Parser.ParseString(txtProxyUserName.Text)
                    ftp.ProxyPassword = clsMarsParser.Parser.ParseString(txtProxyPassword.Text)
                End If
                '  The ProxyMethod should be an integer value between 1 and 5.
                '  If you know your FTP proxy server's authentication scheme,
                '  you may set it directly.  To determine the ProxyMethod,
                '  call DetermineProxyMethod.  A return value of -1 indicates a failure.
                '  A return value of 0 indicates that nothing worked.
                '  A return value of 1-5 indicates the ProxyMethod that was
                '  successful, and this should be the value used for the ProxyMethod
                '  property.

                Dim pMethod As Long
                pMethod = ftp.DetermineProxyMethod()

                If pMethod = -1 Then
                    setError(btnVerify, "Could not determine Proxy method. Will continue to connect...")
                End If
            End If

            Dim retry As Integer = 0

            Do
                ok = ftp.Connect

                If ok = False Then
                    clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "FTP connection failed with " & ftp.LastErrorText & ". Will retry.", True)
                    System.Threading.Thread.Sleep(2000)
                End If

                retry += 1
            Loop Until ok = True Or retry >= 3


            'If ftp.AuthTls = True Then ftp.ClearControlChannel()

            If ok = False Then
                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "FTP connection failure persisted - will no longer retry. Error: " & ftp.LastErrorText, True)
                Throw New Exception(ftp.LastErrorText)
            Else
                clsMarsDebug.writeToDebug("ftp_" & gScheduleName, "FTP connection initiated succeeded", True)
                Me.m_ftpSuccess = True
            End If

            If chkClearChannel.Checked Then
                clsMarsDebug.writeToDebug(logFile, "Clearing command channel")

                ok = ftp.ClearControlChannel

                If ok = False Then
                    clsMarsDebug.writeToDebug(logFile, "Error clearing command channel " & ftp.LastErrorText)
                End If
            End If

            Return ftp

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Return Nothing
        End Try
    End Function
    Private Sub btnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            SetError(btnVerify, "")

            If cmbFTPType.Text = "FTP - SSH (FTPS)" Then
                Dim ftp As Rebex.Net.Sftp = Me.ConnectFTP

                If Me.txtDirectory.Text = "" Then Me.txtDirectory.Text = ftp.GetCurrentDirectory

                ftp.Disconnect()
            Else
                Dim ftp As Chilkat.Ftp2 = Me.ConnectFTP()

                If Me.txtDirectory.Text = "" Then Me.txtDirectory.Text = ftp.GetCurrentRemoteDir

                ftp.Disconnect()
            End If

            MessageBox.Show("Connection tested successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnBrowseFTP.Enabled = True
            logUrlHistory(txtFTPServer.Text)
        Catch ex As Exception
            SetError(btnVerify, ex.Message)
        End Try
    End Sub

    Private Sub cmbFTPType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFTPType.SelectedIndexChanged
        If cmbFTPType.Text = "FTP" Then
            Me.grpCerts.Enabled = False
            Me.optPFX.Enabled = False
            Me.optPVK.Enabled = False
            Me.txtPort.Value = 21
        ElseIf cmbFTPType.Text = "FTP - SSL 3.0 (Implicit)" Then
            Me.txtPort.Value = 990
        ElseIf cmbFTPType.Text = "FTP - SSH (FTPS)" Then
            Me.txtPort.Value = 22
            Me.grpCerts.Enabled = True
            Me.optPFX.Enabled = True
            Me.optPVK.Enabled = True
        Else
            Me.grpCerts.Enabled = True
            Me.optPFX.Enabled = True
            Me.optPVK.Enabled = True
        End If

        txtDirectory.Text = ""
    End Sub

    Private Sub optPFX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPFX.CheckedChanged, optPVK.CheckedChanged
        If optPFX.Checked = True Then
            ' Me.txtPrivateKeyFile.Enabled = False
            'Me.btnBrowse2.Enabled = False
            Me.grpCerts.Enabled = True
        ElseIf Me.optPVK.Checked Then
            Me.txtPrivateKeyFile.Enabled = True
            Me.btnBrowse2.Enabled = True
            Me.grpCerts.Enabled = True
        End If


    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the Certificate File..."

        If Me.optPFX.Checked Then
            ofd.Filter = "Certificate|*.crt|PEM File|*.pem|Certificate|*.pfx|All|*.*"
        Else
            ofd.Filter = "Certificate|*.crt|PEM File|*.pem|All|*.*"
        End If

        ofd.Multiselect = False

        If ofd.ShowDialog <> DialogResult.Cancel Then
            Me.txtCertFile.Text = ofd.FileName
        End If
    End Sub

    Private Sub btnBrowse2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse2.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the Certificate File..."

        ofd.Filter = "Certificate|*.pvk|Private Certificate|*.ppk|All Files|*.*"

        ofd.Multiselect = False

        If ofd.ShowDialog <> DialogResult.Cancel Then
            Me.txtPrivateKeyFile.Text = ofd.FileName
        End If
    End Sub

    Private Sub chkProxy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkProxy.CheckedChanged
        Me.grpProxy.Enabled = Me.chkProxy.Checked
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch : End Try
    End Sub

    Private Sub txtFTPServer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFTPServer.GotFocus, txtCertFile.GotFocus, _
     txtCertName.GotFocus, txtCertPassword.GotFocus, txtDirectory.GotFocus, txtFTPServer.GotFocus, txtPassword.GotFocus, _
      txtPrivateKeyFile.GotFocus, txtProxyPassword.GotFocus, txtProxyServer.GotFocus, txtProxyUserName.GotFocus, _
     txtUserName.GotFocus

        Try
            oField = CType(sender, TextBox)

            oField.ContextMenu = Me.mnuInserter
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            oField.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Try
            Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

            oField.SelectedText = oInsert.GetConstants()
        Catch : End Try
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        'Dim box As TextBox

        'box = CType(Me.ActiveControl, TextBox)

        oField.SelectedText = oData._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub txtFTPServer_LostFocus(sender As Object, e As System.EventArgs) Handles txtFTPServer.LostFocus
        If txtFTPServer.Text <> "" Then
            prepopulateFTPInfo()
        End If
    End Sub

    Private Sub txtFTPServer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFTPServer.TextChanged, txtPassword.TextChanged, _
    txtUserName.TextChanged, txtProxyServer.TextChanged
        SetError(sender, "")
    End Sub

    Private Sub btnBrowseFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseFTP.Click
        Dim oFtp As frmFTPBrowser2 = New frmFTPBrowser2

        Dim lastItem As String = txtDirectory.Text.Split("/")(txtDirectory.Text.Split("/").GetUpperBound(0))
        Dim sReturn As String()

        If IO.Path.GetExtension(lastitem) = "" Then
            sReturn = oFtp.GetFtpInfo(Me.ConnectFTP, txtDirectory.Text)
        Else
            Dim tmp As String = ""

            For I As Integer = 0 To txtDirectory.Text.Split("/").GetUpperBound(0) - 1
                tmp &= txtDirectory.Text.Split("/")(I) & "/"
            Next

            sReturn = oFtp.GetFtpInfo(Me.ConnectFTP, tmp)
        End If

        If sReturn IsNot Nothing Then
            If m_browseFTPForFile Then
                txtDirectory.Text = sReturn(0) & "/" & sReturn(1)
            Else
                Me.txtDirectory.Text = sReturn(0)
            End If
        End If
    End Sub

    Private Sub ResponseRead(ByVal sender As Object, ByVal e As SftpResponseReadEventArgs)
        clsMarsDebug.writeToDebug("ftp_" & gScheduleName, " -> " & e.Response, True)
    End Sub 'ResponseRead

    Private Sub CommandSent(ByVal sender As Object, ByVal e As SftpCommandSentEventArgs)
        clsMarsDebug.writeToDebug("ftp_" & gScheduleName, " <- " & e.Command, True)
    End Sub 'ResponseRead

    Private Sub ucFTPDetails_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        setupForDragAndDrop(txtFTPServer)
        setupForDragAndDrop(txtUserName)
        setupForDragAndDrop(txtPassword)

        With Me.txtFTPServer
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With
    End Sub
    Public Sub prepopulateFTPInfo()
        Try
            Dim col As AutoCompleteStringCollection = New AutoCompleteStringCollection
            Dim ftpurlcache As String = IO.Path.Combine(sAppDataPath, "ftpurlcache.dat")

            If IO.File.Exists(ftpurlcache) = False Then Return

            Dim dtcache As DataTable = New DataTable("FtpUrlCache")

            dtcache.ReadXml(ftpurlcache)

            Dim rows() As DataRow = dtcache.Select("url ='" & txtFTPServer.Text & "'")
            Dim row As DataRow

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                row = rows(0)
            Else
                row = dtcache.Rows.Add
            End If

            txtUserName.Text = row("username")
            txtPassword.Text = _DecryptDBValue(row("password"))
            cmbFTPType.Text = row("ftptype")
            Me.m_ftpOptions = row("options")
            chkPassive.Checked = row("passive")
            txtPort.Value = row("port")
        Catch : End Try
    End Sub

    Public Function getUrlHistory() As AutoCompleteStringCollection
        Dim col As AutoCompleteStringCollection = New AutoCompleteStringCollection
        Dim ftpurlcache As String = IO.Path.Combine(sAppDataPath, "ftpurlcache.dat")

        If IO.File.Exists(ftpurlcache) = False Then Return col

        Dim dtcache As DataTable = New DataTable("FtpUrlCache")

        dtcache.ReadXml(ftpurlcache)

        For Each row As DataRow In dtcache.Rows
            col.Add(row("url"))
        Next

        Return col
    End Function
    Public Sub logUrlHistory(ByVal url As String)
        Try
            Dim dtCache As DataTable = New DataTable("FtpUrlCache")
            Dim ftpurlcache As String = IO.Path.Combine(sAppDataPath, "ftpurlcache.dat")

            If IO.File.Exists(ftpurlcache) Then
                dtCache.ReadXml(ftpurlcache)
            Else
                With dtCache.Columns
                    .Add("url")
                    .Add("username")
                    .Add("password")
                    .Add("port")
                    .Add("ftptype")
                    .Add("options")
                    .Add("passive")
                End With
            End If

            Dim rows() As DataRow = dtCache.Select("url ='" & txtFTPServer.Text & "'")
            Dim row As DataRow

            If rows IsNot Nothing AndAlso rows.Length > 0 Then
                row = rows(0)
            Else
                row = dtCache.Rows.Add
            End If

            row("url") = txtFTPServer.Text
            row("username") = txtUserName.Text
            row("password") = _EncryptDBValue(txtPassword.Text)
            row("port") = txtPort.Value
            row("ftptype") = cmbFTPType.Text
            row("options") = Me.m_ftpOptions
            row("passive") = chkPassive.Checked

            dtCache.WriteXml(ftpurlcache, XmlWriteMode.WriteSchema)
        Catch : End Try
    End Sub
End Class
