<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucReports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucReports))
        Me.cmdUp = New DevComponents.DotNetBar.ButtonX()
        Me.cmdDown = New DevComponents.DotNetBar.ButtonX()
        Me.cmdAddReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdRemoveReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEditReport = New DevComponents.DotNetBar.ButtonX()
        Me.tvReports = New DevComponents.AdvTree.AdvTree()
        Me.NodeConnector1 = New DevComponents.AdvTree.NodeConnector()
        Me.ElementStyle1 = New DevComponents.DotNetBar.ElementStyle()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.tvReports, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdUp
        '
        Me.cmdUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdUp.BackColor = System.Drawing.Color.Transparent
        Me.cmdUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(3, 93)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(80, 24)
        Me.cmdUp.TabIndex = 15
        '
        'cmdDown
        '
        Me.cmdDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDown.BackColor = System.Drawing.Color.Transparent
        Me.cmdDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(3, 123)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(80, 24)
        Me.cmdDown.TabIndex = 14
        '
        'cmdAddReport
        '
        Me.cmdAddReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAddReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(3, 3)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdAddReport.TabIndex = 13
        Me.cmdAddReport.Text = "&Add"
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdRemoveReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(3, 63)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdRemoveReport.TabIndex = 12
        Me.cmdRemoveReport.Text = "&Delete"
        '
        'cmdEditReport
        '
        Me.cmdEditReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEditReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdEditReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdEditReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEditReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(3, 33)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdEditReport.TabIndex = 11
        Me.cmdEditReport.Text = "&Edit"
        '
        'tvReports
        '
        Me.tvReports.AccessibleRole = System.Windows.Forms.AccessibleRole.Outline
        Me.tvReports.AllowDrop = True
        Me.tvReports.BackColor = System.Drawing.SystemColors.Window
        '
        '
        '
        Me.tvReports.BackgroundStyle.Class = "TreeBorderKey"
        Me.tvReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.tvReports.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvReports.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.tvReports.Location = New System.Drawing.Point(0, 0)
        Me.tvReports.Name = "tvReports"
        Me.tvReports.NodesConnector = Me.NodeConnector1
        Me.tvReports.NodeStyle = Me.ElementStyle1
        Me.tvReports.PathSeparator = ";"
        Me.tvReports.Size = New System.Drawing.Size(381, 298)
        Me.tvReports.Styles.Add(Me.ElementStyle1)
        Me.tvReports.TabIndex = 18
        '
        'NodeConnector1
        '
        Me.NodeConnector1.LineColor = System.Drawing.SystemColors.ControlText
        '
        'ElementStyle1
        '
        Me.ElementStyle1.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ElementStyle1.Name = "ElementStyle1"
        Me.ElementStyle1.TextColor = System.Drawing.SystemColors.ControlText
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdAddReport)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdEditReport)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdRemoveReport)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdUp)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdDown)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(381, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(87, 298)
        Me.FlowLayoutPanel1.TabIndex = 19
        '
        'ucReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tvReports)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Name = "ucReports"
        Me.Size = New System.Drawing.Size(468, 298)
        CType(Me.tvReports, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDown As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdAddReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdEditReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents tvReports As DevComponents.AdvTree.AdvTree
    Friend WithEvents NodeConnector1 As DevComponents.AdvTree.NodeConnector
    Friend WithEvents ElementStyle1 As DevComponents.DotNetBar.ElementStyle
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel

End Class
