﻿Public Class frmLocalSQLCon
    Dim ep As ErrorProvider = New ErrorProvider
    Dim cancel As Boolean = False

    Private ReadOnly Property m_conString() As String
        Get
            Dim con As String = ""

            Dim instanceName As String = txtinstancename.Text

            If instanceName <> "" Then instanceName = "\" & instanceName

            con = "Provider=SQLOLEDB.1;Password=" & txtsystemadmin.Text & ";Persist Security Info=True;User ID=sa;Initial Catalog=" & txtdatabasename.Text & ";Data Source=" & Environment.MachineName & instanceName

            Return con
        End Get
    End Property
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        cancel = True
        Me.Close()

    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Dim errInfo As Exception

        If txtservername.Text = "" Then
            SetError(txtservername, "Please enter your Servername")
            txtservername.Focus()
            Return
        ElseIf txtdatabasename.Text = "" Then
            SetError(txtdatabasename, "Please enter your Database name")
            txtdatabasename.Focus()
            Return
        ElseIf txtsystemadmin.Text = "" Then
            SetError(txtsystemadmin, " Please enter your System Admin (sa) password")
            txtsystemadmin.Focus()
            Return
        ElseIf validateConnection(errInfo) = False Then
            Dim msg As String = "Failed to connect to the selected SQL Server database"
            If errInfo IsNot Nothing Then
                msg = errInfo.Message
            End If
            SetError(txtservername, msg)
            txtservername.Focus()
            Return
        End If

        Close()
    End Sub

    Private Function validateConnection(ByRef errInfo As Exception) As Boolean

        Dim con As ADODB.Connection = New ADODB.Connection

        Try
            con.Open(m_conString)

            Try
                Dim rs As ADODB.Recordset = con.Execute("SELECT TOP 1 * FROM ScheduleAttr")
            Catch e As Exception
                _ErrorHandle(e.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Connection was successful but failed to validate any system tables", True)
            End Try

            con.Close()
            Return True
        Catch ex As Exception
            errInfo = ex
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please make sure that the SQL Server service is running", True)
            Return False
        Finally
            con = Nothing
        End Try
    End Function
    Private Sub frmLocalSQLCon_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub

    Public Function editSQLInfo() As Boolean
        Dim conString As String = clsMarsUI.MainUI.ReadRegistry("ConString", "", True)

        If conString = "" Then conString = clsMigration.m_localConString

        Dim servername, userid, password, dbname As String

        '"Provider=SQLOLEDB.1;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=CRD;Data Source=" & Environment.MachineName & "\CRD"

        For Each s As String In conString.Split(";")
            Dim parameter, value As String

            If s <> "" Then
                If s.Contains("=") Then
                    parameter = s.Split("=")(0)
                    value = s.Split("=")(1)

                    Select Case parameter.ToLower.Trim
                        Case "password"
                            password = value
                        Case "user id"
                            userid = value
                        Case "initial catalog"
                            dbname = value
                        Case "data source"
                            servername = value
                    End Select
                End If
            End If
        Next

        txtdatabasename.Text = dbname
        txtservername.Text = Environment.MachineName
        txtsystemadmin.Text = password

        If servername.Contains("\") Then
            txtinstancename.Text = servername.Split("\")(1)
        End If

        Me.ShowDialog()

        If cancel = True Then Return False

        clsMarsUI.MainUI.SaveRegistry("ConString", m_conString, True, , True)

        Return True
    End Function
End Class
