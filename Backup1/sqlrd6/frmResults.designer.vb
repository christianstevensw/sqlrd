﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmResults
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdResults = New System.Windows.Forms.DataGridView()
        Me.lsvFeatures = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnSave = New DevComponents.DotNetBar.ButtonX()
        CType(Me.grdResults, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'grdResults
        '
        Me.grdResults.AllowUserToAddRows = False
        Me.grdResults.AllowUserToDeleteRows = False
        Me.grdResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdResults.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdResults.Location = New System.Drawing.Point(310, 0)
        Me.grdResults.Name = "grdResults"
        Me.grdResults.Size = New System.Drawing.Size(852, 408)
        Me.grdResults.TabIndex = 0
        '
        'lsvFeatures
        '
        '
        '
        '
        Me.lsvFeatures.Border.Class = "ListViewBorder"
        Me.lsvFeatures.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvFeatures.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFeatures.Dock = System.Windows.Forms.DockStyle.Left
        Me.lsvFeatures.FullRowSelect = True
        Me.lsvFeatures.HideSelection = False
        Me.lsvFeatures.Location = New System.Drawing.Point(0, 0)
        Me.lsvFeatures.MultiSelect = False
        Me.lsvFeatures.Name = "lsvFeatures"
        Me.lsvFeatures.Size = New System.Drawing.Size(310, 408)
        Me.lsvFeatures.TabIndex = 1
        Me.lsvFeatures.UseCompatibleStateImageBehavior = False
        Me.lsvFeatures.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Feature Name"
        Me.ColumnHeader1.Width = 299
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 408)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1162, 48)
        Me.Panel1.TabIndex = 2
        '
        'btnSave
        '
        Me.btnSave.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSave.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSave.Location = New System.Drawing.Point(1075, 13)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save Data"
        '
        'frmResults
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1162, 456)
        Me.Controls.Add(Me.grdResults)
        Me.Controls.Add(Me.lsvFeatures)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmResults"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Missing functionality"
        CType(Me.grdResults, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents grdResults As System.Windows.Forms.DataGridView
    Friend WithEvents lsvFeatures As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnSave As DevComponents.DotNetBar.ButtonX
End Class
