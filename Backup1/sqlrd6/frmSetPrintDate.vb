Friend Class frmSetPrintDate
    Dim UserCancel As Boolean = True

    Private Sub frmSetPrintDate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOther.CheckedChanged
        pnDateTime.Enabled = optOther.Checked
    End Sub

    'Public Function GetPrintDateTime() As String
    '    Try
    '        Dim prompt As Boolean = True

    '        prompt = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("PromptPrintDate", 1)))

    '        If prompt = False Then Return ""

    '        Me.ShowDialog()

    '        If UserCancel = True Then Return ""

    '        If optToday.Checked Then
    '            Return ""
    '        Else
    '            Return dtDate.Value.ToString("yyyy-MM-dd") & " " & dtTime.Value.ToString("HH:mm:ss")
    '        End If
    '    Catch ex As Exception
    '        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
    '    End Try
    'End Function


    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        UserCancel = False

        Close()
    End Sub
End Class