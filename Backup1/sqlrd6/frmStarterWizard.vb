Public Class frmStarterWizard
    Dim nStep As Integer = 1
    Dim ep As ErrorProvider = New ErrorProvider
    Dim tipShown As Boolean = False

    Private Sub lnkProcess_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        clsWebProcess.Start("http://www.christiansteven.com/sqlrd/purchase/preregister.htm")
    End Sub

    Private Function NumAdminExist() As Integer
        Dim ntlogin As Boolean = Convert.ToInt16(clsMarsUI.MainUI.ReadRegistry("UnifiedLogin", 0))

        If Not ntlogin Then
            Dim SQL As String = "SELECT COUNT(*) FROM CRDUsers WHERE UserRole = 'Administrator' AND UserID <> 'SQLRDAdmin'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            Dim nReturn As Integer = oRs(0).Value

            oRs.Close()


            Return nReturn
        Else
            Dim SQL As String = "SELECT COUNT(*) FROM domainattr WHERE UserRole = 'Administrator'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            Dim nReturn As Integer = oRs(0).Value

            oRs.Close()

            Return nReturn
        End If
    End Function
    'Private Sub cmdNext_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim oUser As New clsMarsUsers

    '    Select Case nStep
    '        Case 1
    '            'enforce entry if no admin exists

    '            If NumAdminExist() = 0 Then
    '                MessageBox.Show("Please add an Admin user before proceeding to the next steps", "CRD", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '                Me.txtFirstName.Focus()
    '                Return
    '            End If

    '            cmbMailType.Text = clsMarsUI.MainUI.ReadRegistry("MailType", "NONE", , , True)

    '            Step1.Visible = False
    '            Step2.Visible = True
    '            Step2.BringToFront()

    '            Me.toolTip.SetSuperTooltip(cmdSaveUser, _
    '            New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Click here to save user", Nothing, _
    '            Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))

    '            cmdBack.Enabled = True
    '        Case 2
    '            Me.txtCustomerNo.Text = clsMarsUI.MainUI.ReadRegistry("CustNo", "", , , True)
    '            Step2.Visible = False
    '            Step3.Visible = True
    '            Step3.BringToFront()
    '        Case 3
    '            Step3.Visible = False
    '            Step4.Visible = True
    '            Step4.BringToFront()

    '            cmdNext.Visible = False
    '            cmdFinish.Visible = True
    '    End Select

    '    Dim parentNode As TreeNode = tvSteps.Nodes(0)

    '    For Each node As TreeNode In parentNode.Nodes
    '        If node.Index = nStep Then
    '            node.ForeColor = Color.Black
    '        Else
    '            node.ForeColor = Color.Silver
    '        End If
    '    Next

    '    nStep += 1

    '    tipShown = False

    '    toolTip.SetSuperTooltip(cmdNext, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Click to go to the next step", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))
    'End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If Me.NumAdminExist = 0 Then
            End
        Else
            Close()
        End If
    End Sub

    Private Sub cmbMailType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMailType.SelectedIndexChanged
        On Error Resume Next
        Dim oSec As clsMarsSecurity = New clsMarsSecurity
        Dim oUI As New clsMarsUI


        Select Case cmbMailType.Text
            Case "SMTP"
                grpSMTP.Visible = True
                grpMAPI.Visible = False
                grpGroupwise.Visible = False

                grpSMTP.BringToFront()
                txtSMTPUserID.Text = oUI.ReadRegistry("SMTPUserID", "", , , True)
                txtSMTPPassword.Text = oUI.ReadRegistry("SMTPPassword", "", True, , True)
                txtSMTPServer.Text = oUI.ReadRegistry("SMTPServer", "", , , True)
                txtSMTPSenderAddress.Text = oUI.ReadRegistry("SMTPSenderAddress", "", , , True)
                txtSMTPSenderName.Text = oUI.ReadRegistry("SMTPSenderName", "", , , True)
                Me.cmbSMTPTimeout.Value = oUI.ReadRegistry("SMTPTimeout", 30, , , True)

                cmdMailTest.Enabled = True
                cmdMailTest.Visible = True

            Case "MAPI"
                grpMAPI.Visible = True
                grpSMTP.Visible = False
                grpMAPI.BringToFront()
                grpGroupwise.Visible = False
                cmbMAPIProfile.Text = oUI.ReadRegistry("MAPIProfile", "", , , True)
                txtMAPIPassword.Text = oUI.ReadRegistry("MAPIPassword", "", True, , True)

                cmdMailTest.Enabled = True
                cmdMailTest.Visible = True
            Case "GROUPWISE"
                Me.txtgwUser.Text = oUI.ReadRegistry("GWUser", "", , , True)
                Me.txtgwPassword.Text = oUI.ReadRegistry("GWPassword", "", True, , True)
                Me.txtgwProxy.Text = oUI.ReadRegistry("GWProxy", "", , , True)
                Me.txtgwPOIP.Text = oUI.ReadRegistry("GWPOIP", "", , , True)
                Me.txtgwPOPort.Text = oUI.ReadRegistry("GWPOPort", "", , , True)

                grpGroupwise.Visible = True
                grpGroupwise.BringToFront()

                grpMAPI.Visible = False
                grpSMTP.Visible = False
            Case "NONE"
                grpSMTP.Visible = False
                grpMAPI.Visible = False
                grpGroupwise.Visible = False
                cmdMailTest.Enabled = False
                cmdMailTest.Visible = False
            Case Else
                cmdMailTest.Visible = False
        End Select
    End Sub

    Private Sub txtSMTPUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmbMAPIProfile.TextChanged

        If Me.Step2.Visible = False Then Return

        Dim tip As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo

        tip.HeaderText = "For your information:"
        tip.HeaderVisible = True
        tip.FooterVisible = False
        tip.Color = DevComponents.DotNetBar.eTooltipColor.Lemon
        tip.BodyText = "Please note that the 'Next' button has been disabled. It will become " & _
        "enabled again once you have successfully tested the mail settings (by clicking the 'Test Settings' button) or " & _
        "you remove values from all the fields."
        tip.CustomSize = New Drawing.Size(New Drawing.Point(300, 100))

        toolTip.SetSuperTooltip(Me.cmdNext, tip)

        Select Case cmbMailType.Text
            Case "MAPI"
                If cmbMAPIProfile.Text = "" And txtMAPIPassword.Text = "" Then
                    cmdNext.Enabled = True
                Else
                    cmdNext.Enabled = False

                    If tipShown = False Then
                        toolTip.ShowTooltip(Me.cmdNext)
                        tipShown = True
                    End If
                End If
            Case "SMTP"
                If txtSMTPUserID.Text = "" And txtSMTPPassword.Text = "" And txtSMTPSenderAddress.Text = "" And _
                txtSMTPSenderName.Text = "" And txtSMTPServer.Text = "" Then
                    cmdNext.Enabled = True
                Else
                    cmdNext.Enabled = False

                    If tipShown = False Then
                        toolTip.ShowTooltip(Me.cmdNext)
                        tipShown = True
                    End If
                End If
        End Select

    End Sub

    Private Sub frmStarterWizard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        stabMain.SelectedTab = tabGettingStarted

    End Sub


    Private Sub txtFirstName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
    txtFirstName.TextChanged, txtLastName.TextChanged, txtUserID.TextChanged, txtPassword.TextChanged, _
    txtConfirm.TextChanged
        SetError(sender, "")
        Me.toolTip.SetSuperTooltip(cmdSaveUser, _
         New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Click here to save user", Nothing, _
        Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))

    End Sub

    'Private Sub tvSteps_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs)
    '    If tvSteps.SelectedNode Is Nothing Then Return

    '    If tvSteps.SelectedNode.Level = 0 Then Return

    '    Dim selIndex As Integer = tvSteps.SelectedNode.Index
    '    Me.toolTip.SetSuperTooltip(cmdSaveUser, _
    '    New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Click here to save user", Nothing, _
    '    Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))


    '    Dim panels(3) As Panel

    '    panels(0) = Me.Step1
    '    panels(1) = Me.Step2
    '    panels(2) = Me.Step3
    '    panels(3) = Me.Step4

    '    If selIndex <> 0 And NumAdminExist() = 0 Then
    '        MessageBox.Show("Please create an Admin user before moving to the other steps", "CRD", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    '        Me.tvSteps.SelectedNode = Me.tvSteps.Nodes(0).Nodes(0)
    '        Me.txtFirstName.Focus()
    '        Return
    '    End If

    '    For I As Integer = 0 To 3
    '        If I = selIndex Then
    '            panels(I).Visible = True
    '            panels(I).BringToFront()
    '        Else
    '            panels(I).Visible = False
    '        End If
    '    Next

    '    nStep = selIndex + 1

    '    If selIndex = 0 Then
    '        cmdBack.Enabled = False
    '        cmdNext.Visible = True
    '        cmdFinish.Visible = False
    '    ElseIf selIndex = 1 Then
    '        Me.cmbMailType.Text = clsMarsUI.MainUI.ReadRegistry("MailType", "NONE", , , True)
    '        cmdFinish.Visible = False
    '        cmdNext.Visible = True
    '        cmdBack.Enabled = True
    '    ElseIf selIndex = 2 Then
    '        Me.txtCustomerNo.Text = clsMarsUI.MainUI.ReadRegistry("CustNo", "", , , True)
    '        cmdFinish.Visible = False
    '        cmdNext.Visible = True
    '        cmdBack.Enabled = True
    '    ElseIf selIndex = 3 Then
    '        cmdFinish.Visible = True
    '        cmdNext.Visible = False
    '        cmdBack.Enabled = True
    '    Else
    '        cmdFinish.Visible = False
    '        cmdNext.Visible = True
    '        cmdBack.Enabled = True
    '    End If

    '    For Each node As TreeNode In tvSteps.Nodes(0).Nodes
    '        If node.Index = selIndex Then
    '            node.ForeColor = Color.Black
    '        Else
    '            node.ForeColor = Color.Silver
    '        End If
    '    Next
    'End Sub
    Dim WithEvents oSMTP As New Quiksoft.EasyMail.SMTP.SMTP

    Private Sub cmdMailTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMailTest.Click
        Cursor.Current = Cursors.WaitCursor

        Dim sErr As String
        Dim vbCrLf = Environment.NewLine
        Dim oMsg As clsMarsMessaging = New clsMarsMessaging
        Dim oUI As New clsMarsUI

        Select Case cmbMailType.Text.ToLower
            Case "mapi"
                Try
                    Dim cdoSession As Object = CreateObject("Redemption.RDOSession") ' MAPI.Session = New MAPI.Session

                    cdoSession.Logon(cmbMAPIProfile.Text, txtMAPIPassword.Text, False, True)
                    MessageBox.Show("MAPI Mail test succeeded.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    cmdNext.Enabled = True

                    oUI.SaveRegistry("MAPIProfile", Me.cmbMAPIProfile.Text, , , True)
                    oUI.SaveRegistry("MAPIPassword", Me.txtMAPIPassword.Text, True, , True)
                    oUI.SaveRegistry("MailType", Me.cmbMailType.Text, , , True)

                Catch ex As Exception
                    If Err.Number = 429 Or Err.Number = 91 Then
                        sErr = ex.Message & vbCrLf & "Error initialising Collaborative Data Objects (CDO)." & vbCrLf & _
                            "CDO might not be installed correctly on your pc." & vbCrLf & _
                            "You can install CDO from your Microsoft Office as follows:" & vbCrLf & _
                            "1. Run your Office Setup" & vbCrLf & _
                            "2. Select 'Add or Remove Features'..." & vbCrLf & _
                            "3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & vbCrLf & _
                            "4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & vbCrLf & _
                            "5. Click the 'Update' button"
                    ElseIf Err.Number = -2147221231 Then
                        sErr = "Logging on to MAPI system failed. Please check the provided credentials"
                    ElseIf Err.Number = 5 Then
                        sErr = "SQL-RD cannot access the internet." & vbCrLf & _
                        "Please ensure that this PC is connected to the internet." & vbCrLf & _
                        "Please check your firewall and ensure that you have given full access to SQL-RD."
                    Else
                        sErr = "MAPI Mail test failed with the following error: " & ex.Message & " [" & Err.Number & "]"
                    End If

                    MessageBox.Show(sErr, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    cmdNext.Enabled = False
                End Try
            Case "smtp", "crdmail"
                Dim sTestAddress As String

                sTestAddress = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                If sTestAddress <> "" Then oUI.SaveRegistry("TestSMTPAddress", sTestAddress)

                If sTestAddress = "" Or sTestAddress.IndexOf("@") = -1 Then Exit Sub

                Try
                    Dim oMailMsg As New Quiksoft.EasyMail.SMTP.EmailMessage
                    Dim oCon As Quiksoft.EasyMail.SMTP.SMTPServer

                    Quiksoft.EasyMail.SMTP.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"

                    oMailMsg.Recipients.Add(sTestAddress, "", Quiksoft.EasyMail.SMTP.RecipientType.To)

                    Select Case cmbMailType.Text.ToLower
                        Case "smtp"
                            oMailMsg.Subject = "Test SMTP Email From SQL-RD"
                            oMailMsg.BodyParts.Add("This is a test SMTP email sent by SQL-RD", Quiksoft.EasyMail.SMTP.BodyPartFormat.Plain)
                            oCon = New Quiksoft.EasyMail.SMTP.SMTPServer(txtSMTPServer.Text)

                            If txtSMTPUserID.Text.Length > 0 Then
                                With oCon
                                    .AuthMode = Quiksoft.EasyMail.SMTP.SMTPAuthMode.AuthLogin
                                    .Account = txtSMTPUserID.Text
                                    .Password = txtSMTPPassword.Text
                                    .Timeout = cmbSMTPTimeout.Text
                                End With
                            End If
                            oMailMsg.From.Email = txtSMTPSenderAddress.Text
                            oMailMsg.From.Name = txtSMTPSenderName.Text
                    End Select

                    oSMTP.SMTPServers.Add(oCon)

                    oSMTP.Send(oMailMsg)

                    oUI.BusyProgress(100, , True)

                    MessageBox.Show("Email sent successfully!", Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)

                    cmdNext.Enabled = True

                    oSMTP.Reset()

                    clsMarsUI.MainUI.SaveRegistry("SMTPServer", Me.txtSMTPServer.Text, , , True)
                    clsMarsUI.MainUI.SaveRegistry("SMTPSenderName", Me.txtSMTPSenderName.Text, , , True)
                    clsMarsUI.MainUI.SaveRegistry("SMTPUserID", Me.txtSMTPUserID.Text, , , True)
                    clsMarsUI.MainUI.SaveRegistry("SMTPPassword", Me.txtSMTPPassword.Text, True, , True)
                    clsMarsUI.MainUI.SaveRegistry("SMTPSenderAddress", Me.txtSMTPSenderAddress.Text, , , True)
                    clsMarsUI.MainUI.SaveRegistry("SMTPTimeout", Me.cmbSMTPTimeout.Value, , , True)
                    clsMarsUI.MainUI.SaveRegistry("CharSetEncoding", "US-ASCII", , , True)
                    oUI.SaveRegistry("MailType", Me.cmbMailType.Text, , , True)

                Catch ex As Exception
                    oSMTP.Reset()

                    Dim sMsg As String = ""

                    If ex.Message.ToLower.IndexOf("auth") > -1 Then
                        sMsg = "Your SMTP server is not configured to use a username, password or both. Please remove the username and password and try again"
                    ElseIf ex.Message.IndexOf("5") > -1 Or _
                    ex.Message.IndexOf("not implemented") > -1 Then
                        sMsg = "Please check the provided userid and password. " & _
                        "Also, some SMTP servers are not configured to use a username, password or both."
                    ElseIf ex.Message.IndexOf("GetHostByName") > -1 Then
                        sMsg = "SQL-RD cannot find a connection to your server.  Please ensure that you have a connection to your server, or that the mail server name has been entered correctly."
                    Else
                        sMsg = ""
                    End If

                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), sMsg)

                    cmdNext.Enabled = False

                End Try
            Case "groupwise"
                Dim testAddress As String = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                oUI.SaveRegistry("TestSMTPAddress", testAddress, , , True)

                If testAddress = "" Or testAddress.IndexOf("@") = -1 Then Exit Sub

                If clsMarsMessaging.SendGROUPWISE(testAddress, "", "", "Test GROUPWISE Mail from SQL-RD", "Test GROUPWISE email sent by SQL-RD", "", "Single") = True Then
                    MessageBox.Show("Test completed. If you have not received any errors then the email has been sent successfully", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please check the provided information and try again")
                End If
        End Select

        toolTip.SetSuperTooltip(cmdNext, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Click to go to the next step", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))
        Cursor.Current = Cursors.Default
        cmdNext.Focus()
    End Sub


    Private Sub cmdSaveUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveUser.Click
        If txtFirstName.Text.Length = 0 Then
            SetError(txtFirstName, "Please enter the first name")
            txtFirstName.Focus()
            Return
        ElseIf txtLastName.Text.Length = 0 Then
            SetError(txtLastName, "Please enter the last name")
            txtLastName.Focus()
            Return
        ElseIf txtUserID.Text.Length = 0 Then
            SetError(txtUserID, "Please enter the userid")
            txtUserID.Focus()
            Return
        ElseIf txtPassword.Text.Length = 0 Then
            SetError(txtPassword, "Please enter a password")
            txtPassword.Focus()
            Return
        ElseIf txtPassword.Text <> txtConfirm.Text Then
            SetError(txtConfirm, "The password does not match the confirm password")
            txtPassword.Focus()
            Return
        ElseIf txtUserID.Text.ToLower = "crdadmin" Then
            setError(txtUserID, "The provided user id is used by the SQL-RD system. Please choose a different user id")
            txtUserID.Focus()
            Return
        End If

        gUser = "System"

        Dim oUser As New clsMarsUsers

        If oUser.AddUser(txtUserID.Text, txtPassword.Text, txtFirstName.Text, _
        txtLastName.Text, "Administrator", False, True) = True Then

            Dim oUI As New clsMarsUI

            oUI.SaveRegistry("LastUserID", txtUserID.Text, , , True)

            Me.toolTip.SetSuperTooltip(cmdSaveUser, _
            New DevComponents.DotNetBar.SuperTooltipInfo("", "", "The user has been saved successfully", Nothing, _
            Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, Nothing))

            Me.toolTip.ShowTooltip(cmdSaveUser)

        Else
            Me.toolTip.SetSuperTooltip(cmdSaveUser, _
            New DevComponents.DotNetBar.SuperTooltipInfo("", "", "There was an error saving the user. Make sure the specified username is not already in use", Nothing, _
            Nothing, DevComponents.DotNetBar.eTooltipColor.Orange, False, False, Nothing))

            Me.toolTip.ShowTooltip(cmdSaveUser)

            Return
        End If
        cmdNext.Focus()
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        If chkAdvanced.Checked = True Then
            Close()
            Dim oConfig As frmOptions = New frmOptions

            oConfig.ShowDialog(Me)

        Else
            Close()
        End If
    End Sub


    Private Sub cmdNext_Click_1(sender As System.Object, e As System.EventArgs) Handles cmdNext.Click
        Select Case stabMain.SelectedTab.Name
            Case tabGettingStarted.Name
                tabAdmin.Visible = True
                stabMain.SelectedTab = tabAdmin
            Case tabAdmin.Name

                If NumAdminExist() = 0 Then
                    MessageBox.Show("Please add an Admin user before proceeding to the next steps", "SQL-RD", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Me.txtFirstName.Focus()
                    Return
                End If

                cmbMailType.Text = clsMarsUI.MainUI.ReadRegistry("MailType", "NONE", , , True)

                tabMessaging.Visible = True
                stabMain.SelectedTab = tabMessaging
            Case tabMessaging.Name
                clsMarsUI.MainUI.SaveRegistry("MailType", Me.cmbMailType.Text, , , True)

                Me.txtCustomerNo.Text = clsMarsUI.MainUI.ReadRegistry("CustNo", "", , , True)

                tabSupport.Visible = True
                stabMain.SelectedTab = tabSupport
            Case tabSupport.Name
                tabFinish.Visible = True
                stabMain.SelectedTab = tabFinish
                cmdFinish.Enabled = True
                cmdNext.Enabled = False

        End Select
    End Sub

    Private Sub cmbMailType_SelectedIndexChanged_1(sender As System.Object, e As System.EventArgs)

    End Sub
End Class