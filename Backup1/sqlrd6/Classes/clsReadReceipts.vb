Imports MAPI
Imports sqlrd.clsMarsUI

Friend Class clsReadReceipts

    Public Shared Sub ProcessReceipts()
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            SQL = "SELECT * FROM ReadReceiptsAttr"

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim destinationID As Integer
                Dim waitFor As Double
                Dim repeat As Boolean = False
                Dim repeatEvery As Double
                Dim receiptID As Integer
                Dim readOnes() As Integer
                Dim I As Integer = 0
                Dim logID As Integer
                Dim useIMAP As Boolean
                Dim imapPort As Integer = 443
                Dim imapSSL As Boolean

                destinationID = oRs("destinationid").Value
                receiptID = oRs("receiptid").Value
                waitFor = oRs("waitlength").Value

                Try
                    repeat = Convert.ToBoolean(oRs("repeat").Value)
                Catch : End Try

                repeatEvery = oRs("repeatevery").Value

                Try
                    useIMAP = IsNull(oRs("useimap").Value, False)
                    imapPort = IsNull(oRs("imapport").Value, 443)
                    imapSSL = IsNull(oRs("imapssl").Value, False)
                Catch : End Try
                'check if read receipt exists
                Dim oRs1 As ADODB.Recordset

                SQL = "SELECT * FROM ReadReceiptsLog WHERE DestinationID =" & destinationID & " AND RRStatus = 'Pending'"

                oRs1 = clsMarsData.GetData(SQL)

                If oRs1 Is Nothing Then Continue Do

                I = 0

                ReDim readOnes(I)

                Do While oRs1.EOF = False
                    Dim emailSubject As String
                    Dim entryDate As Date
                    Dim sentTo As String
                    Dim resultTable As Hashtable = New Hashtable

                    logID = oRs1("logid").Value
                    emailSubject = oRs1("emailsubject").Value
                    entryDate = oRs1("nextcheck").Value
                    sentTo = oRs1("sentto").Value

                    Dim recieptFound As Boolean

                    clsMarsDebug.writeToDebug("readreciepts", "starting rr processing", False)

                    clsMarsDebug.writeToDebug("readreciepts", "Use IMAP interface =" & useIMAP, True)

                    If useIMAP = True Then
                        recieptFound = clsReadReceipts.ReceiptExists(emailSubject, sentTo, imapPort, imapSSL)
                    Else
                        recieptFound = clsReadReceipts.ReceiptExists(emailSubject, sentTo)
                    End If

                    If recieptFound Then
                        ReDim Preserve readOnes(I)

                        readOnes(I) = logID

                        I += 1
                    Else
                        Dim waitedFor As Double

                        waitedFor = Now.Subtract(entryDate).TotalMinutes

                        If waitedFor > waitFor Then
                            Dim oTask As clsMarsTask = New clsMarsTask

                            oTask.ProcessTasks(receiptID, "NONE")
                        End If

                        clsMarsData.WriteData("UPDATE ReadReceiptsLog SET LastCheck = '" & ConDateTime(Now) & "', NextCheck ='" & ConDateTime(Now.AddMinutes(repeatEvery)) & "' WHERE LogID =" & logID)
                    End If

                    oRs1.MoveNext()
                Loop

                For Each n As Integer In readOnes
                    If n > 0 Then clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus = 'Received', " & _
                    "LastCheck ='" & ConDateTime(Now) & "' WHERE LogID=" & n)
                Next

                oRs1.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Overloads Shared Function ReceiptExists(ByVal subject As String, ByVal sentTo As String) As Boolean
        Try
            Dim cdoSession As Object 'Session
            Const PR_SENDER_ADDRTYPE = &HC1E001E
            Const PR_EMAIL = &H39FE001E

            'create a mail session object
            cdoSession = CreateObject("Redemption.RDOSession") 'New Session
            Dim cdoFolders As Object 'Folders
            'Dim cdoFolder As Folder
            Dim cdoMessage As Object 'Message

            Dim sMode As String = MainUI.ReadRegistry("SQL-RDService", "")

            Dim sArgs() As String = Environment.GetCommandLineArgs

            Dim userid As String = MainUI.ReadRegistry("MAPIProfile", "")

            Dim password As String = MainUI.ReadRegistry("MAPIPassword", "", True)

            Dim mapiType As String = MainUI.ReadRegistry("MAPIType", "")

            Dim sAlias As String = MainUI.ReadRegistry("MAPIAlias", "")

            Dim sxServer As String = MainUI.ReadRegistry("MAPIServer", "")

            Dim EmailRetry As String = Convert.ToBoolean(Convert.ToInt32(MainUI.ReadRegistry("EmailRetry", 0)))

            If mapiType = "Single" Then
                cdoSession.Logon(userid, password, False, True)
            Else
                cdoSession.LogonExchangeMailbox(sAlias, sxServer) ', , , True, , _
                'NoMail:=True, _
                '   ProfileInfo:=sxServer & vbLf & sAlias)
            End If

            'create a folder object in the session's inbox
            cdoFolders = cdoSession.GetDefaultFolder(6).Folders 'cdoSession.Inbox.Folders
            'loop through all the folders
            'For I As Integer = 1 To cdoFolders.Count
            'cdoFolder = cdoFolders.Item(I)

            Dim inbox As Object = cdoSession.GetDefaultFolder(6)

            For Each cdoMessage In inbox.Items 'cdoSession.Inbox.messages
                Dim oSafe As Redemption.SafeMailItem = New Redemption.SafeMailItem

                Dim mailSubject As String
                Dim mailSender, mailSenderAddress As String
                Dim addressEntry As Object 'Redemption.AddressEntry

                oSafe.Item = cdoMessage

                mailSubject = oSafe.subject
                addressEntry = oSafe.Sender
                mailSender = addressEntry.Name

                Dim strType As String = oSafe.Fields(PR_SENDER_ADDRTYPE)

                If strType = "SMTP" Then
                    mailSenderAddress = addressEntry.Address
                ElseIf strType = "EX" Then
                    mailSenderAddress = oSafe.Fields(PR_EMAIL)
                End If

                If mailSenderAddress Is Nothing Then
                    Try
                        mailSenderAddress = addressEntry.SMTPAddress
                    Catch
                        mailSenderAddress = ""
                    End Try
                End If

                If LCase(mailSubject) = "read: " & LCase(subject) Then

                    If String.Compare(mailSender, sentTo, True) = 0 Then
                        Return True
                    ElseIf String.Compare(mailSenderAddress, sentTo, True) = 0 Then
                        Return True
                    End If
                End If

                Try
                    If LCase(mailSubject) = "read: " & LCase(subject) And (LCase(mailSender) = LCase(sentTo) Or LCase(mailSenderAddress) = LCase(sentTo)) Then
                        Return True
                    End If
                Catch : End Try

                oSafe = Nothing
            Next

            ' Next

            cdoFolders = Nothing
            cdoSession = Nothing

            Return False
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function

    Private Overloads Shared Function ReceiptExists(ByVal subject As String, ByVal sentTo As String, ByVal imapPort As Integer, ByVal imapSSL As Boolean) As Boolean
        Try
            Dim sMode As String = MainUI.ReadRegistry("SQL-RDService", "")
            Dim userid As String = Environment.UserDomainName & "\" & Environment.UserName
            Dim password As String = MainUI.ReadRegistry("MAPIPassword", "", True)
            Dim mapiType As String = MainUI.ReadRegistry("MAPIType", "")
            Dim sAlias As String = MainUI.ReadRegistry("MAPIAlias", "")
            Dim sxServer As String = MainUI.ReadRegistry("MAPIServer", "")
            Dim EmailRetry As String = Convert.ToBoolean(Convert.ToInt32(MainUI.ReadRegistry("EmailRetry", 0)))
            Dim imap As Chilkat.Imap = New Chilkat.Imap
            Dim ok As Boolean
            Dim recieptFound As Boolean = False

            imap.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")

            If mapiType = "Single" Then
                Throw New Exception("Processing read reciepts using IMAP requires MAPI with Exchange Server.")
            Else
                imap.Port = imapPort
                imap.Ssl = imapSSL

                ok = imap.Connect(sxServer)

                If ok = False Then
                    Throw New Exception(imap.LastErrorText)
                End If


                ok = imap.Login(userid, password)

                If ok = False Then
                    Throw New Exception(imap.LastErrorText)
                End If



                ok = imap.SelectMailbox("INBOX")

                imap.PeekMode = True

                If ok = False Then
                    Throw New Exception(imap.LastErrorText)
                End If

                Dim result As Chilkat.MessageSet = imap.Search("UNSEEN", True)
                Dim msgFromName, msgFromAddress, msgTo, msgCc, msgBcc, msgSubject, msgBody, msgAttachments As String
                Dim msgDate As String = ""
                Dim msgDatesent As Date

                If result.Count > 0 Then
                    Dim envs As Chilkat.EmailBundle = imap.FetchBundle(result)

                    For c As Integer = 0 To envs.MessageCount - 1
                        Dim email As Chilkat.Email = envs.GetEmail(c)

                        msgFromName = ""
                        msgFromAddress = ""
                        msgTo = ""
                        msgCc = ""
                        msgBcc = ""
                        msgSubject = ""
                        msgBody = ""
                        msgAttachments = ""
                        msgDatesent = Nothing

                        msgFromName = email.FromName
                        msgFromAddress = email.FromAddress

                        For n As Integer = 0 To email.NumTo - 1
                            msgTo &= email.GetTo(n) & ";"
                        Next

                        For n As Integer = 0 To email.NumCC - 1
                            msgCc &= email.GetCC(n) & ";"
                        Next

                        For n As Integer = 0 To email.NumBcc - 1
                            msgBcc &= email.GetBcc(n) & ";"
                        Next

                        msgSubject = email.Subject
                        msgDate = ConDateTime(email.LocalDate)


                        If LCase(msgSubject) = "read: " & LCase(subject) Then

                            If String.Compare(msgFromName, sentTo, True) = 0 Then
                                recieptFound = True
                                imap.SetMailFlag(email, "SEEN", 1) '//mark as read
                                Exit For
                            ElseIf String.Compare(msgFromAddress, sentTo, True) = 0 Then
                                recieptFound = True
                                imap.SetMailFlag(email, "SEEN", 1)
                                Exit For
                            Else
                                imap.SetMailFlag(email, "SEEN", 0) '//mark as unread
                            End If
                        Else
                            imap.SetMailFlag(email, "SEEN", 0)
                        End If
                    Next

                    Try
                        imap.Logout()
                        imap.Disconnect()
                    Catch : End Try
                End If

            End If

            Return recieptFound
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function
End Class
