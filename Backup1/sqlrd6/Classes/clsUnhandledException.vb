Friend Class clsUnhandledException

    Public Sub onUnhandledException(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        Try
            If RunEditor = True Then
                Dim ex As Exception = e.ExceptionObject

                Dim frmU As frmUnhandledException = New frmUnhandledException

                If ex.Message.ToLower.Contains("object reference not set to an instance of an object") Then
                    _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
                    Return
                Else
                    frmU.showError(ex)
                End If
            Else
                Dim ex As Exception = e.ExceptionObject

                _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
            End If
        Catch : End Try
    End Sub
    Public Sub onThreadException(ByVal sender As Object, ByVal e As Threading.ThreadExceptionEventArgs)
        Try
            If RunEditor = True Then
                Dim ex As Exception = e.Exception

                Dim frmU As frmUnhandledException = New frmUnhandledException

                If ex.Message.ToLower.Contains("object reference not set to an instance of an object") Then
                    _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
                    Return
                Else
                    frmU.showError(ex)
                End If
            Else
                Dim ex As Exception = e.Exception

                _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
            End If
        Catch : End Try
    End Sub
End Class
