﻿Friend Class clsReportDelivery
    Public m_sqlrdPath As String = ""


    <STAThread()> _
    Public Function deliverReport(ByVal sExport As String, ByVal templateName As String, ByRef errInfo As Exception) As Boolean
        Try

            If m_sqlrdPath = "" Then Throw New Exception("Sorry but you did not tell me where SQL-RD is located...")

            _SetGlobalVars(m_sqlrdPath)

            clsMarsDebug.writeToDebug("webservice.debug", "opening connection to " & sCon, False)

            clsMarsData.DataItem.OpenMainDataConnection()

            Dim report As clsMarsReport = New clsMarsReport
            Dim ok As Boolean

            RunEditor = False

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM destinationAttr WHERE destinationName ='" & SQLPrepare(templateName) & "' AND reportid = 2777 and packid = 2777")

            If oRs Is Nothing Then Throw New Exception("Could not obtain destination details")

            If oRs.EOF = True Then Throw New Exception("Could not find destination called " & templateName)

            Dim nDestinationID As Integer = oRs("destinationid").Value

            Try
                If oRs("compress").Value = 1 And sExport.Length > 0 Then
                    Dim Encrypt As Boolean
                    Dim ZipCode As String = ""


                    Try
                        Encrypt = oRs("encryptzip").Value
                        ZipCode = IsNull(oRs("encryptzipcode").Value, "")
                    Catch : Encrypt = False : End Try

                    sExport = report.ZipFiles("Single", report.m_OutputFolder, sExport, Encrypt, ZipCode)

                End If
            Catch : End Try

            'if delivery should be defered
            Dim nDefer As Integer
            Dim nDeferBy As Integer
            Dim isDefer As Boolean
            Dim oparse As clsMarsParser = New clsMarsParser
            Dim sExtras As String = IsNull(oRs("extras").Value)
            Dim sfilename As String = IO.Path.GetFileNameWithoutExtension(sExport)
            Try
                nDefer = oRs("deferdelivery").Value
            Catch ex As Exception
                nDefer = 0
            End Try

            If nDefer = 1 And RunEditor = False Then
                nDeferBy = oRs("deferby").Value

                ok = report._DeferDelivery(oRs("destinationid").Value, sExport, _
                     nDeferBy, IO.Path.GetFileNameWithoutExtension(sExport))

                isDefer = True
            Else

                Select Case oRs("DestinationType").Value.Tolower
                    Case "printer"
                        Dim sFilePrint(0) As String

                        sFilePrint(0) = sExport

                        ok = report._PrintServer(sFilePrint, nDestinationID)

                        If ok = False Then
                            Throw New Exception(report._CreateErrorString)
                        End If

                    Case "email"
                        Dim sendTo, cc, bcc, subject, mailformat, smtpserver, sendername, senderaddress, msgbody, embedpath As String
                        Dim readreceipt, embed As Boolean

                        sendTo = clsMarsMessaging.ResolveEmailAddress(oRs("sendto").Value)
                        cc = clsMarsMessaging.ResolveEmailAddress(oRs("cc").Value)
                        bcc = clsMarsMessaging.ResolveEmailAddress(oRs("bcc").Value)

                        sendTo = oparse.ParseString(sendTo, , , , , , Nothing)
                        cc = oparse.ParseString(cc, , , , , , Nothing)
                        bcc = oparse.ParseString(bcc, , , , , , Nothing)

                        subject = oparse.ParseString(oRs("subject").Value, , , , , , Nothing)

                        mailformat = oRs("mailformat").Value
                        smtpserver = IsNull(oRs("smtpserver").Value, "Default")
                        sendername = oparse.ParseString(IsNull(oRs("sendername").Value), , , , , , Nothing)
                        senderaddress = oparse.ParseString(IsNull(oRs("senderaddress").Value), , , , , , Nothing)

                        msgbody = oparse.ParseString(oRs("message").Value, , , , , , Nothing)

                        Try
                            readreceipt = Convert.ToBoolean(oRs("readreceipts").Value)
                        Catch ex As Exception
                            readreceipt = False
                        End Try

                        Try
                            embed = Convert.ToBoolean(oRs("embed").Value)
                        Catch ex As Exception
                            embed = False
                        End Try

                        'now if we want to embed
                        Dim messagingEngine As clsMarsMessaging = New clsMarsMessaging

                        If MailType = MarsGlobal.gMailType.MAPI Then
                            ok = messagingEngine.SendMAPI(sendTo, _
                                 subject, msgbody, "Single", sExport, 1, sExtras, _
                                 cc, bcc, embed, "", sfilename, , embedpath, readreceipt, nDestinationID)
                        ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then

                            Dim objSender As clsMarsMessaging = New clsMarsMessaging

                            ok = objSender.SendSMTP(sendTo, subject, _
                                 msgbody, "Single", sExport, 1, sExtras, cc, _
                                 bcc, sfilename, embed, "", , True, _
                                 mailformat, smtpserver, embedpath, sendername, senderaddress)

                        ElseIf MailType = gMailType.GROUPWISE Then
                            ok = messagingEngine.SendGROUPWISE(sendTo, cc, bcc, subject, msgbody, sExport, "Single", _
                            sExtras, True, embed, embedpath, True, 1, sfilename, mailformat)
                        Else
                            Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                        End If

                        If ok = False Then
                            Throw New Exception(report._CreateErrorString)
                        End If
                    Case "disk"

                        Dim UseDUN As Boolean
                        Dim sDUN As String
                        Dim oNet As clsNetworking
                        Dim appendToFile As Boolean

                        'if DUN is used...
                        Try
                            UseDUN = Convert.ToBoolean(oRs("usedun").Value)
                            sDUN = IsNull(oRs("dunname").Value)
                        Catch ex As Exception
                            UseDUN = False
                        End Try

                        If UseDUN = True Then
                            oNet = New clsNetworking

                            If oNet._DialConnection(sDUN, "Single Schedule Error: " & sfilename & ": ") = False Then
                                Throw New Exception(report._CreateErrorString)
                            End If
                        End If

                        Dim sFile As String = ExtractFileName(sExport)

                        Dim paths As String = IsNull(oRs("outputpath").Value)
                        Dim spath As String

                        If paths.EndsWith("|") Then paths = paths.Substring(0, paths.Length - 1)

                        For Each item As String In paths.Split("|")

                            If item IsNot Nothing Then

                                If item = "" Then Continue For

                                spath = oparse.ParseString(item, , , , , , Nothing)

                                If spath.EndsWith("\") = False And spath.Length > 0 Then spath &= "\"

                                If spath Is Nothing Then
                                    Throw New Exception(report._CreateErrorString)
                                End If

                                spath = _CreateUNC(spath)

                                ok = oparse.ParseDirectory(spath)

                                Dim sTemp1 As String = spath & sFile


                                Try
                                    If appendToFile = True And IO.File.Exists(sTemp1) Then
                                        Dim oRead As String = ReadTextFromFile(sExport)

                                        SaveTextToFile(oRead, sTemp1, , True, True)
                                    Else
                                        System.IO.File.Copy(sExport, sTemp1, True)
                                    End If
                                Catch ex As Exception
                                    If Err.Number = 57 Then
                                        For Each o As Process In Process.GetProcessesByName("excel")
                                            o.Kill()
                                        Next

                                        System.IO.File.Copy(sExport, spath & sFile, True)
                                    End If
                                End Try

                            End If

                            Dim oSys As New clsSystemTools

                            oSys._HouseKeeping(nDestinationID, spath, sfilename)

                        Next

                        Try
                            If UseDUN = True Then
                                oNet._Disconnect()
                            End If
                        Catch : End Try

                    Case "ftp"
                        Dim oFtp As New clsMarsTask
                        Dim sFtp As String = ""
                        Dim FTPServer As String
                        Dim FTPUser As String
                        Dim FTPPassword As String
                        Dim FTPPath As String
                        Dim FTPType As String
                        Dim ftpCount As Integer = 0
                        Dim FTPPassive As String
                        Dim FtpOptions As String

                        FTPServer = oparse.ParseString(IsNull(oRs("ftpserver").Value), , , , , , Nothing)
                        FTPUser = oparse.ParseString(oRs("ftpusername").Value, , , , , , Nothing)
                        FTPPassword = oparse.ParseString(oRs("ftppassword").Value, , , , , , Nothing)
                        FTPPath = oparse.ParseString(oRs("ftppath").Value, , , , , , Nothing)
                        FTPType = IsNull(oRs("ftptype").Value, "FTP")
                        FTPPassive = IsNull(oRs("ftppassive").Value, "")
                        FtpOptions = IsNull(oRs("ftpoptions").Value, "")

                        ftpCount = FTPServer.Split("|").GetUpperBound(0)

                        If ftpCount > 0 Then
                            ftpCount -= 1
                        Else
                            ftpCount = 0
                        End If

                        For I As Integer = 0 To ftpCount
                            Dim l_FTPServer As String = FTPServer.Split("|")(I)
                            Dim l_FTPUser As String = FTPUser.Split("|")(I)
                            Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(I))
                            Dim l_FTPPath As String = FTPPath.Split("|")(I)
                            Dim l_FTPType As String = FTPType.Split("|")(I)
                            Dim l_FTPPassive As Boolean = False
                            Dim l_FtpOptions As String

                            Try
                                l_FtpOptions = FtpOptions.Split("|")(I)
                            Catch
                                l_FtpOptions = ""
                            End Try
                            Try
                                l_FTPPassive = Convert.ToBoolean(Convert.ToInt32(FTPPassive.Split("|")(I)))
                            Catch ex As Exception
                                l_FTPPassive = False
                            End Try

                            If l_FTPServer = "" Then Continue For

                            ok = oFtp.FTPUpload2(l_FTPServer, 21, l_FTPUser, _
                                 l_FTPPassword, l_FTPPath, _
                                 sExport, l_FTPType, l_FtpOptions, , , l_FTPPassive)

                            If ok = False Then
                                Throw New Exception(report._CreateErrorString)
                            End If
                        Next

                        ok = True
                    Case "fax"
                        Dim faxNumber As String
                        Dim faxDevice As String
                        Dim faxTo As String
                        Dim faxFrom As String
                        Dim faxComments As String
                        Dim faxer As clsMarsMessaging = New clsMarsMessaging

                        faxDevice = oparse.ParseString(oRs("subject").Value, , , , , , Nothing)

                        faxTo = oparse.ParseString(oRs("cc").Value, , , , , , Nothing)

                        faxFrom = oparse.ParseString(oRs("bcc").Value, , , , , , Nothing)
                        faxComments = oparse.ParseString(oRs("message").Value, , , , , , Nothing)

                        faxNumber = oRs("sendto").Value

                        ok = faxer.SendFax(faxNumber, faxDevice, "Single", faxTo, faxFrom, faxComments, sExport)

                    Case "sms"
                        Dim cellNumber As String
                        Dim textMsg As String
                        Dim messagingEngine As clsMarsMessaging = New clsMarsMessaging

                        textMsg = oparse.ParseString(oRs("message").Value, , , , , , Nothing)

                        ok = messagingEngine.SendSMS(cellNumber, textMsg, sExport)

                    Case "sharepoint"
                        Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint

                        Dim spServer As String
                        Dim spUser As String
                        Dim spPassword As String
                        Dim spLib As String
                        Dim spCount As Integer = 0
                        Dim spmetaData As String = ""

                        spServer = oparse.ParseString(IsNull(oRs("ftpserver").Value))
                        spUser = oparse.ParseString(oRs("ftpusername").Value)
                        spPassword = oparse.ParseString(oRs("ftppassword").Value)
                        spLib = oparse.ParseString(oRs("ftppath").Value)
                        spmetaData = oparse.ParseString(IsNull(oRs("ftpoptions").Value))



                        spCount = spServer.Split("|").GetUpperBound(0)

                        If spCount > 0 Then
                            spCount -= 1
                        Else
                            spCount = 0
                        End If

                        For I As Integer = 0 To spCount
                            Dim l_spServer As String = spServer.Split("|")(I)
                            Dim l_spUser As String = spUser.Split("|")(I)
                            Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(I))
                            Dim l_spLib As String = spLib.Split("|")(I)
                            Dim errorInfo As Exception = Nothing
                            Dim l_metaData As String = ""
                            If l_spServer = "" Then Continue For

                            Try
                                l_metaData = spmetaData.Split("|")(I)
                            Catch : End Try

                            If l_spServer = "" Then Continue For

                            ok = sp.UploadDocument(l_spServer, l_spLib, sExport, l_spUser, l_spPassword, errorInfo, l_metaData)

                            If ok = False And errorInfo IsNot Nothing Then
                                Throw errorInfo
                            End If

                        Next

                        ok = True
                End Select
            End If

            Return True
        Catch ex As Exception
            errInfo = ex
            Return False
        Finally
            clsMarsData.DataItem.CloseMainDataConnection()
        End Try
    End Function

    'Public Sub setGlobalVars(Optional ByVal assemblyPath As String = "")
    '    Try
    '        Dim sPath As String
    '        Dim oUI As clsMarsUI = New clsMarsUI
    '        Dim sMail As String

    '        sKey = "Software\ChristianSteven\" & Application.ProductName

    '        If assemblyPath = "" Then
    '            sAppPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "")
    '        Else
    '            sAppPath = assemblyPath
    '        End If

    '        If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

    '        If Environment.GetCommandLineArgs.Length = 1 Then
    '            sPath = sAppPath & "sqlrdlive.dat"
    '        Else
    '            If Environment.GetCommandLineArgs(1).IndexOf("syspath") = -1 Then
    '                sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
    '            Else
    '                Dim sTemp As String

    '                For I As Integer = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
    '                    sTemp &= Environment.GetCommandLineArgs(I) & " "
    '                Next

    '                sPath = sTemp.Split("=")(1).Trim

    '                If sPath.ToLower.IndexOf("sqlrdlive.dat") = -1 Then
    '                    If sPath.EndsWith("\") = False Then sPath &= "\"

    '                    sPath &= "sqlrdlive.dat"
    '                End If

    '                Try
    '                    If System.IO.File.Exists(sPath) = False Then
    '                        MessageBox.Show("SQL-RD could not connect to the specified system file. SQL-RD will use the default system file", _
    '                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '                        sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
    '                    End If
    '                Catch
    '                    MessageBox.Show("SQL-RD could not connect to the specified system file. SQL-RD will use the default system file", _
    '                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

    '                    sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
    '                End Try
    '            End If
    '        End If

    '        gPath = sAppPath & "Samples"

    '        gConfigFile = sAppPath & "sqlrdlive.config"

    '        clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

    '        sMail = oUI.ReadRegistry("MailType", "NONE")

    '        Select Case sMail
    '            Case "MAPI"
    '                MailType = gMailType.MAPI
    '            Case "SMTP"
    '                MailType = gMailType.SMTP
    '            Case "NONE"
    '                MailType = gMailType.NONE
    '            Case "SQLRDMAIL"
    '                MailType = gMailType.SQLRDMAIL
    '            Case "GROUPWISE"
    '                MailType = gMailType.GROUPWISE
    '        End Select


    '        Dim ConType As String

    '        ConType = oUI.ReadRegistry("ConType", "DAT")

    '        If ConType = "DAT" Then
    '            gConType = "DAT"
    '            sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
    '        Else
    '            'don't just decrypt it as it may not be encrypted...
    '            Dim test As String = oUI.ReadRegistry("ConString", "")

    '            If test.ToLower.Contains("provider") Then
    '                oUI.SaveRegistry("ConString", test, True, , True)
    '            End If

    '            gConType = ConType '"ODBC"
    '            sCon = oUI.ReadRegistry("ConString", "", True)
    '        End If

    '        If sCon.Length = 0 Then
    '            sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
    '        End If

    '        Try
    '            CustID = oUI.ReadRegistry("CustNo", "0")
    '        Catch ex As Exception
    '            CustID = 0
    '        End Try

    '        gHelpPath = sAppPath & "SQL-RD.chm"

    '        gServiceType = oUI.ReadRegistry("SQL-RDService", "NONE")

    '        'rename sqlrderror.log
    '        Try
    '            If IO.File.Exists(sAppPath & "sqlrderror.legacy") = False And IO.File.Exists(sAppPath & "sqlrderror.log") Then
    '                IO.File.Move(sAppPath & "sqlrderror.log", sAppPath & "sqlrderror.legacy")

    '                Dim msg As String = "The error log has been moved from a new system file. To view its contents, open SQL-RD and " & _
    '                "go to System Monitor -> Error Log." & vbCrLf & _
    '                "The contents of the old error log have been copied into sqlrderror.legacy."

    '                SaveTextToFile(msg, sAppPath & "sqlrderror.log", , False, False)
    '            End If
    '        Catch : End Try
    '    Catch : End Try
    'End Sub
End Class
