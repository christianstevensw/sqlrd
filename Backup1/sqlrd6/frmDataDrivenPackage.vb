Imports Microsoft.Win32
Friend Class frmDataDrivenPackage
    Inherits DevComponents.DotNetBar.Office2007Form
    Dim nStep As Int32
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oUI As New clsMarsUI
    Dim m_PackID As Integer = clsMarsData.CreateDataID("PackageAttr", "PackID")

    Const S1 As String = "Step 1: Package Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Data-Driver Setup"
    Const S4 As String = "Step 4: Destination Setup"
    Const S5 As String = "Step 5: Add Reports"
    Const S6 As String = "Step 6: Exception Handling"
    Friend WithEvents chkSnapshot As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As DevComponents.DotNetBar.LabelX
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents btnBuild As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtQuery As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkGroupReports As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents txtMergeAllXL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMergeAllPDF As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkMergeAllXL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMergeAllPDF As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents stabMain As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabGeneral As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel7 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabTasks As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel6 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabException As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel5 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabReports As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel4 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDestinations As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel3 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabDataDriver As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents tabSchedule As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ucSet As sqlrd.ucSchedule
    Friend WithEvents ReflectionImage1 As DevComponents.DotNetBar.Controls.ReflectionImage
    Friend WithEvents UcBlankReportX1 As sqlrd.ucBlankReportX
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnXLSMerge As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnPDFMerge As DevComponents.DotNetBar.ButtonX
    Const S7 As String = "Step 7: Custom Tasks"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdFinish As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdLoc As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtFolder As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents lsvReports As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdRemoveReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents txtDesc As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtKeyWord As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents cmdEditReport As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkMergePDF As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkMergeXL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtMergePDF As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtMergeXL As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents cmdUp As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdDown As DevComponents.DotNetBar.ButtonX
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataDrivenPackage))
        Me.cmdFinish = New DevComponents.DotNetBar.ButtonX()
        Me.cmdCancel = New DevComponents.DotNetBar.ButtonX()
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdLoc = New DevComponents.DotNetBar.ButtonX()
        Me.txtFolder = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Step1 = New System.Windows.Forms.Panel()
        Me.ReflectionImage1 = New DevComponents.DotNetBar.Controls.ReflectionImage()
        Me.txtDesc = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.txtKeyWord = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown()
        Me.Label26 = New DevComponents.DotNetBar.LabelX()
        Me.cmbDateTime = New System.Windows.Forms.ComboBox()
        Me.chkDTStamp = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Step5 = New System.Windows.Forms.Panel()
        Me.btnXLSMerge = New DevComponents.DotNetBar.ButtonX()
        Me.btnPDFMerge = New DevComponents.DotNetBar.ButtonX()
        Me.DividerLabel2 = New sqlrd.DividerLabel()
        Me.chkSnapshot = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown()
        Me.chkMergeXL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.txtMergeXL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdUp = New DevComponents.DotNetBar.ButtonX()
        Me.txtMergePDF = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdDown = New DevComponents.DotNetBar.ButtonX()
        Me.chkMergePDF = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmdAddReport = New DevComponents.DotNetBar.ButtonX()
        Me.lsvReports = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ReportName = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Format = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.cmdRemoveReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdEditReport = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.optOnce = New System.Windows.Forms.RadioButton()
        Me.optAll = New System.Windows.Forms.RadioButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkMergeAllXL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkMergeAllPDF = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.Step3 = New System.Windows.Forms.Panel()
        Me.txtMergeAllXL = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtMergeAllPDF = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkGroupReports = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpQuery = New System.Windows.Forms.GroupBox()
        Me.cmbDDKey = New System.Windows.Forms.ComboBox()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.btnBuild = New DevComponents.DotNetBar.ButtonX()
        Me.txtQuery = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.UcDSN = New sqlrd.ucDSN()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.stabMain = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel5 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabReports = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabGeneral = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel6 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcBlankReportX1 = New sqlrd.ucBlankReportX()
        Me.UcError = New sqlrd.ucErrorHandler()
        Me.tabException = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel7 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.oTask = New sqlrd.ucTasks()
        Me.tabTasks = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel4 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.UcDest = New sqlrd.ucDestination()
        Me.tabDestinations = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel3 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.tabDataDriver = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ucSet = New sqlrd.ucSchedule()
        Me.tabSchedule = New DevComponents.DotNetBar.SuperTabItem()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step5.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.grpQuery.SuspendLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabMain.SuspendLayout()
        Me.SuperTabControlPanel5.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuperTabControlPanel6.SuspendLayout()
        Me.SuperTabControlPanel7.SuspendLayout()
        Me.SuperTabControlPanel4.SuspendLayout()
        Me.SuperTabControlPanel3.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdFinish
        '
        Me.cmdFinish.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdFinish.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdFinish.Enabled = False
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(595, 3)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(73, 25)
        Me.cmdFinish.TabIndex = 0
        Me.cmdFinish.Text = "&Finish"
        '
        'cmdCancel
        '
        Me.cmdCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(437, 3)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Cancel"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "MS Access Database|*.mdb|All Files|*.*"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'cmdLoc
        '
        Me.cmdLoc.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(328, 80)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 3
        Me.cmdLoc.Text = "..."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtFolder.Border.Class = "TextBoxBorder"
        Me.txtFolder.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 80)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 2
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        '
        '
        '
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Package Name"
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.Color.Transparent
        Me.Step1.Controls.Add(Me.ReflectionImage1)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Step1.ForeColor = System.Drawing.Color.Navy
        Me.Step1.Location = New System.Drawing.Point(0, 0)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(535, 413)
        Me.Step1.TabIndex = 3
        '
        'ReflectionImage1
        '
        '
        '
        '
        Me.ReflectionImage1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.ReflectionImage1.BackgroundStyle.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center
        Me.ReflectionImage1.Image = CType(resources.GetObject("ReflectionImage1.Image"), System.Drawing.Image)
        Me.ReflectionImage1.Location = New System.Drawing.Point(390, 128)
        Me.ReflectionImage1.Name = "ReflectionImage1"
        Me.ReflectionImage1.Size = New System.Drawing.Size(152, 231)
        Me.ReflectionImage1.TabIndex = 20
        '
        'txtDesc
        '
        Me.txtDesc.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtDesc.Border.Class = "TextBoxBorder"
        Me.txtDesc.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtDesc.ForeColor = System.Drawing.Color.Black
        Me.txtDesc.Location = New System.Drawing.Point(8, 128)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(376, 171)
        Me.txtDesc.TabIndex = 19
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 305)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtKeyWord.Border.Class = "TextBoxBorder"
        Me.txtKeyWord.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 321)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 16
        Me.txtKeyWord.Tag = "memo"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox5.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.cmbDateTime)
        Me.GroupBox5.Controls.Add(Me.chkDTStamp)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 325)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(527, 78)
        Me.GroupBox5.TabIndex = 9
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Date/Time Stamp"
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(232, 48)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 22
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        '
        '
        '
        Me.Label26.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label26.Location = New System.Drawing.Point(15, 52)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(165, 16)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(232, 16)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(152, 21)
        Me.cmbDateTime.TabIndex = 18
        '
        'chkDTStamp
        '
        '
        '
        '
        Me.chkDTStamp.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(16, 16)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 0
        Me.chkDTStamp.Text = "Append date/time stamp"
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.Color.Transparent
        Me.Step5.Controls.Add(Me.btnXLSMerge)
        Me.Step5.Controls.Add(Me.btnPDFMerge)
        Me.Step5.Controls.Add(Me.DividerLabel2)
        Me.Step5.Controls.Add(Me.chkSnapshot)
        Me.Step5.Controls.Add(Me.txtSnapshots)
        Me.Step5.Controls.Add(Me.chkMergeXL)
        Me.Step5.Controls.Add(Me.txtMergeXL)
        Me.Step5.Controls.Add(Me.cmdUp)
        Me.Step5.Controls.Add(Me.txtMergePDF)
        Me.Step5.Controls.Add(Me.cmdDown)
        Me.Step5.Controls.Add(Me.chkMergePDF)
        Me.Step5.Controls.Add(Me.cmdAddReport)
        Me.Step5.Controls.Add(Me.lsvReports)
        Me.Step5.Controls.Add(Me.cmdRemoveReport)
        Me.Step5.Controls.Add(Me.cmdEditReport)
        Me.Step5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step5.Location = New System.Drawing.Point(0, 0)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(535, 413)
        Me.Step5.TabIndex = 16
        '
        'btnXLSMerge
        '
        Me.btnXLSMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnXLSMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnXLSMerge.Enabled = False
        Me.btnXLSMerge.Location = New System.Drawing.Point(455, 343)
        Me.btnXLSMerge.Name = "btnXLSMerge"
        Me.btnXLSMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnXLSMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnXLSMerge.TabIndex = 16
        Me.btnXLSMerge.Text = ".xls"
        '
        'btnPDFMerge
        '
        Me.btnPDFMerge.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnPDFMerge.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnPDFMerge.Enabled = False
        Me.btnPDFMerge.Location = New System.Drawing.Point(455, 373)
        Me.btnPDFMerge.Name = "btnPDFMerge"
        Me.btnPDFMerge.Size = New System.Drawing.Size(75, 21)
        Me.btnPDFMerge.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled
        Me.btnPDFMerge.TabIndex = 17
        Me.btnPDFMerge.Text = ".pdf"
        '
        'DividerLabel2
        '
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(5, 327)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(559, 13)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 12
        Me.DividerLabel2.Text = "Merging"
        '
        'chkSnapshot
        '
        Me.chkSnapshot.AutoSize = True
        '
        '
        '
        Me.chkSnapshot.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSnapshot.Location = New System.Drawing.Point(5, 449)
        Me.chkSnapshot.Name = "chkSnapshot"
        Me.chkSnapshot.Size = New System.Drawing.Size(206, 16)
        Me.chkSnapshot.TabIndex = 11
        Me.chkSnapshot.Text = "Enable snapshots and keep for (days)"
        Me.chkSnapshot.Visible = False
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.txtSnapshots.Location = New System.Drawing.Point(237, 447)
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.txtSnapshots.Size = New System.Drawing.Size(32, 21)
        Me.txtSnapshots.TabIndex = 10
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Visible = False
        '
        'chkMergeXL
        '
        '
        '
        '
        Me.chkMergeXL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(5, 337)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.chkMergeXL.Size = New System.Drawing.Size(264, 32)
        Me.chkMergeXL.TabIndex = 1
        Me.chkMergeXL.Text = "Merge all Excel outputs into a single workbook"
        Me.ToolTip1.SetToolTip(Me.chkMergeXL, "All reports in Excel format will be merged into a single Excel workbook")
        '
        'txtMergeXL
        '
        Me.txtMergeXL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeXL.Border.Class = "TextBoxBorder"
        Me.txtMergeXL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeXL.Enabled = False
        Me.txtMergeXL.ForeColor = System.Drawing.Color.Black
        Me.txtMergeXL.Location = New System.Drawing.Point(281, 343)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.txtMergeXL.Size = New System.Drawing.Size(162, 21)
        Me.txtMergeXL.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.txtMergeXL, "Enter the resulting PDF file name")
        '
        'cmdUp
        '
        Me.cmdUp.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdUp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUp.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(455, 125)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 9
        '
        'txtMergePDF
        '
        Me.txtMergePDF.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergePDF.Border.Class = "TextBoxBorder"
        Me.txtMergePDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergePDF.Enabled = False
        Me.txtMergePDF.ForeColor = System.Drawing.Color.Black
        Me.txtMergePDF.Location = New System.Drawing.Point(281, 373)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.txtMergePDF.Size = New System.Drawing.Size(162, 21)
        Me.txtMergePDF.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.txtMergePDF, "Enter the resulting PDF file name")
        '
        'cmdDown
        '
        Me.cmdDown.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdDown.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDown.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(455, 155)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 8
        '
        'chkMergePDF
        '
        '
        '
        '
        Me.chkMergePDF.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(5, 367)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.chkMergePDF.Size = New System.Drawing.Size(264, 32)
        Me.chkMergePDF.TabIndex = 0
        Me.chkMergePDF.Text = "Merge all PDF outputs into a single PDF file"
        Me.ToolTip1.SetToolTip(Me.chkMergePDF, "All reports in PDF format will be merged into a single PDF file")
        '
        'cmdAddReport
        '
        Me.cmdAddReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdAddReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(455, 8)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 6
        Me.cmdAddReport.Text = "&Add"
        '
        'lsvReports
        '
        Me.lsvReports.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.lsvReports.Border.Class = "ListViewBorder"
        Me.lsvReports.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(8, 8)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.lsvReports.Size = New System.Drawing.Size(435, 313)
        Me.lsvReports.TabIndex = 5
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 150
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdRemoveReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(455, 72)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 6
        Me.cmdRemoveReport.Text = "&Remove"
        '
        'cmdEditReport
        '
        Me.cmdEditReport.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdEditReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEditReport.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(455, 40)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdEditReport.TabIndex = 6
        Me.cmdEditReport.Text = "&Edit"
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(516, 3)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(73, 25)
        Me.cmdNext.TabIndex = 19
        Me.cmdNext.Text = "&Next"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.optOnce)
        Me.GroupBox1.Controls.Add(Me.optAll)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 329)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(529, 74)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 1
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 0
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'chkMergeAllXL
        '
        Me.chkMergeAllXL.AutoSize = True
        '
        '
        '
        Me.chkMergeAllXL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergeAllXL.Location = New System.Drawing.Point(23, 377)
        Me.chkMergeAllXL.Name = "chkMergeAllXL"
        Me.chkMergeAllXL.Size = New System.Drawing.Size(250, 16)
        Me.chkMergeAllXL.TabIndex = 23
        Me.chkMergeAllXL.Text = "Merge the group into a single Excel Workbook:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllXL, "Selecting this option will over-ride the Excel merging settings for the individua" & _
        "l reports (per record merging) .")
        '
        'chkMergeAllPDF
        '
        Me.chkMergeAllPDF.AutoSize = True
        '
        '
        '
        Me.chkMergeAllPDF.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMergeAllPDF.Location = New System.Drawing.Point(23, 357)
        Me.chkMergeAllPDF.Name = "chkMergeAllPDF"
        Me.chkMergeAllPDF.Size = New System.Drawing.Size(210, 16)
        Me.chkMergeAllPDF.TabIndex = 24
        Me.chkMergeAllPDF.Text = "Merge the group into a single PDF file:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllPDF, "Selecting this option will over-ride the PDF merging settings for the individual " & _
        "reports (per record merging) .")
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.txtMergeAllXL)
        Me.Step3.Controls.Add(Me.txtMergeAllPDF)
        Me.Step3.Controls.Add(Me.chkMergeAllXL)
        Me.Step3.Controls.Add(Me.chkMergeAllPDF)
        Me.Step3.Controls.Add(Me.chkGroupReports)
        Me.Step3.Controls.Add(Me.grpQuery)
        Me.Step3.Controls.Add(Me.UcDSN)
        Me.Step3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step3.Location = New System.Drawing.Point(0, 0)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(535, 413)
        Me.Step3.TabIndex = 26
        '
        'txtMergeAllXL
        '
        Me.txtMergeAllXL.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeAllXL.Border.Class = "TextBoxBorder"
        Me.txtMergeAllXL.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeAllXL.Enabled = False
        Me.txtMergeAllXL.ForeColor = System.Drawing.Color.Black
        Me.txtMergeAllXL.Location = New System.Drawing.Point(318, 377)
        Me.txtMergeAllXL.Name = "txtMergeAllXL"
        Me.txtMergeAllXL.Size = New System.Drawing.Size(195, 21)
        Me.txtMergeAllXL.TabIndex = 25
        '
        'txtMergeAllPDF
        '
        Me.txtMergeAllPDF.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtMergeAllPDF.Border.Class = "TextBoxBorder"
        Me.txtMergeAllPDF.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtMergeAllPDF.Enabled = False
        Me.txtMergeAllPDF.ForeColor = System.Drawing.Color.Black
        Me.txtMergeAllPDF.Location = New System.Drawing.Point(318, 354)
        Me.txtMergeAllPDF.Name = "txtMergeAllPDF"
        Me.txtMergeAllPDF.Size = New System.Drawing.Size(195, 21)
        Me.txtMergeAllPDF.TabIndex = 26
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        '
        '
        '
        Me.chkGroupReports.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkGroupReports.Checked = True
        Me.chkGroupReports.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkGroupReports.CheckValue = "Y"
        Me.chkGroupReports.Location = New System.Drawing.Point(8, 334)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(407, 16)
        Me.chkGroupReports.TabIndex = 20
        Me.chkGroupReports.Text = "Group reports together by email address/directory (email/disk destination only)"
        '
        'grpQuery
        '
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label8)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(8, 3)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(521, 325)
        Me.grpQuery.TabIndex = 19
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(78, 298)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(171, 21)
        Me.cmbDDKey.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        '
        '
        '
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(6, 302)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(60, 16)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Key Column"
        '
        'btnBuild
        '
        Me.btnBuild.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnBuild.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnBuild.Location = New System.Drawing.Point(430, 269)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        '
        '
        '
        Me.txtQuery.Border.Class = "TextBoxBorder"
        Me.txtQuery.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtQuery.ForeColor = System.Drawing.Color.Black
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(496, 244)
        Me.txtQuery.TabIndex = 0
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(297, 81)
        Me.UcDSN.m_conString = "|||"
        Me.UcDSN.m_showConnectionType = False
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(39, 39)
        Me.UcDSN.TabIndex = 17
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'stabMain
        '
        Me.stabMain.BackColor = System.Drawing.Color.White
        '
        '
        '
        '
        '
        '
        Me.stabMain.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabMain.ControlBox.MenuBox.Name = ""
        Me.stabMain.ControlBox.Name = ""
        Me.stabMain.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabMain.ControlBox.MenuBox, Me.stabMain.ControlBox.CloseBox})
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel5)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel6)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel7)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel4)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel3)
        Me.stabMain.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stabMain.ForeColor = System.Drawing.Color.Black
        Me.stabMain.Location = New System.Drawing.Point(0, 0)
        Me.stabMain.Name = "stabMain"
        Me.stabMain.ReorderTabsEnabled = True
        Me.stabMain.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabMain.SelectedTabIndex = 0
        Me.stabMain.Size = New System.Drawing.Size(671, 413)
        Me.stabMain.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.stabMain.TabFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stabMain.TabIndex = 27
        Me.stabMain.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.tabGeneral, Me.tabSchedule, Me.tabDataDriver, Me.tabDestinations, Me.tabReports, Me.tabException, Me.tabTasks})
        Me.stabMain.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        Me.stabMain.Text = "SuperTabControl1"
        '
        'SuperTabControlPanel5
        '
        Me.SuperTabControlPanel5.Controls.Add(Me.Step5)
        Me.SuperTabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel5.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel5.Name = "SuperTabControlPanel5"
        Me.SuperTabControlPanel5.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel5.TabIndex = 0
        Me.SuperTabControlPanel5.TabItem = Me.tabReports
        '
        'tabReports
        '
        Me.tabReports.AttachedControl = Me.SuperTabControlPanel5
        Me.tabReports.GlobalItem = False
        Me.tabReports.Image = CType(resources.GetObject("tabReports.Image"), System.Drawing.Image)
        Me.tabReports.Name = "tabReports"
        Me.tabReports.Text = "Reports"
        Me.tabReports.Visible = False
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.Step1)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.tabGeneral
        '
        'tabGeneral
        '
        Me.tabGeneral.AttachedControl = Me.SuperTabControlPanel1
        Me.tabGeneral.GlobalItem = False
        Me.tabGeneral.Image = CType(resources.GetObject("tabGeneral.Image"), System.Drawing.Image)
        Me.tabGeneral.Name = "tabGeneral"
        Me.tabGeneral.Text = "General"
        '
        'SuperTabControlPanel6
        '
        Me.SuperTabControlPanel6.Controls.Add(Me.UcBlankReportX1)
        Me.SuperTabControlPanel6.Controls.Add(Me.UcError)
        Me.SuperTabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel6.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel6.Name = "SuperTabControlPanel6"
        Me.SuperTabControlPanel6.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel6.TabIndex = 0
        Me.SuperTabControlPanel6.TabItem = Me.tabException
        '
        'UcBlankReportX1
        '
        Me.UcBlankReportX1.BackColor = System.Drawing.Color.Transparent
        Me.UcBlankReportX1.blankID = 0
        Me.UcBlankReportX1.blankType = "AlertandReport"
        Me.UcBlankReportX1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcBlankReportX1.Location = New System.Drawing.Point(0, 93)
        Me.UcBlankReportX1.m_customDSN = ""
        Me.UcBlankReportX1.m_customPassword = ""
        Me.UcBlankReportX1.m_customUserID = ""
        Me.UcBlankReportX1.m_showAllReportsBlankForTasks = False
        Me.UcBlankReportX1.Name = "UcBlankReportX1"
        Me.UcBlankReportX1.Size = New System.Drawing.Size(535, 320)
        Me.UcBlankReportX1.TabIndex = 9
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcError.Location = New System.Drawing.Point(0, 0)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(535, 93)
        Me.UcError.TabIndex = 8
        '
        'tabException
        '
        Me.tabException.AttachedControl = Me.SuperTabControlPanel6
        Me.tabException.GlobalItem = False
        Me.tabException.Image = CType(resources.GetObject("tabException.Image"), System.Drawing.Image)
        Me.tabException.Name = "tabException"
        Me.tabException.Text = "Exception Handling"
        Me.tabException.Visible = False
        '
        'SuperTabControlPanel7
        '
        Me.SuperTabControlPanel7.Controls.Add(Me.oTask)
        Me.SuperTabControlPanel7.Controls.Add(Me.GroupBox1)
        Me.SuperTabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel7.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel7.Name = "SuperTabControlPanel7"
        Me.SuperTabControlPanel7.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel7.TabIndex = 0
        Me.SuperTabControlPanel7.TabItem = Me.tabTasks
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.Transparent
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Top
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.oTask.Location = New System.Drawing.Point(0, 0)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.m_forExceptionHandling = False
        Me.oTask.m_showAfterType = True
        Me.oTask.m_showExpanded = True
        Me.oTask.Name = "oTask"
        Me.oTask.Size = New System.Drawing.Size(535, 323)
        Me.oTask.TabIndex = 7
        '
        'tabTasks
        '
        Me.tabTasks.AttachedControl = Me.SuperTabControlPanel7
        Me.tabTasks.GlobalItem = False
        Me.tabTasks.Image = CType(resources.GetObject("tabTasks.Image"), System.Drawing.Image)
        Me.tabTasks.Name = "tabTasks"
        Me.tabTasks.Text = "Custom Tasks"
        Me.tabTasks.Visible = False
        '
        'SuperTabControlPanel4
        '
        Me.SuperTabControlPanel4.Controls.Add(Me.UcDest)
        Me.SuperTabControlPanel4.Controls.Add(Me.GroupBox5)
        Me.SuperTabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel4.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel4.Name = "SuperTabControlPanel4"
        Me.SuperTabControlPanel4.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel4.TabIndex = 0
        Me.SuperTabControlPanel4.TabItem = Me.tabDestinations
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.destinationsPicker = False
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(0, 0)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_IsQuery = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(535, 319)
        Me.UcDest.TabIndex = 0
        '
        'tabDestinations
        '
        Me.tabDestinations.AttachedControl = Me.SuperTabControlPanel4
        Me.tabDestinations.GlobalItem = False
        Me.tabDestinations.Image = CType(resources.GetObject("tabDestinations.Image"), System.Drawing.Image)
        Me.tabDestinations.Name = "tabDestinations"
        Me.tabDestinations.Text = "Destinations"
        Me.tabDestinations.Visible = False
        '
        'SuperTabControlPanel3
        '
        Me.SuperTabControlPanel3.Controls.Add(Me.Step3)
        Me.SuperTabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel3.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel3.Name = "SuperTabControlPanel3"
        Me.SuperTabControlPanel3.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel3.TabIndex = 0
        Me.SuperTabControlPanel3.TabItem = Me.tabDataDriver
        '
        'tabDataDriver
        '
        Me.tabDataDriver.AttachedControl = Me.SuperTabControlPanel3
        Me.tabDataDriver.GlobalItem = False
        Me.tabDataDriver.Image = CType(resources.GetObject("tabDataDriver.Image"), System.Drawing.Image)
        Me.tabDataDriver.Name = "tabDataDriver"
        Me.tabDataDriver.Text = "Data-Driver"
        Me.tabDataDriver.Visible = False
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.ucSet)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(136, 0)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(535, 413)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.tabSchedule
        '
        'ucSet
        '
        Me.ucSet.BackColor = System.Drawing.Color.Transparent
        Me.ucSet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucSet.Location = New System.Drawing.Point(0, 0)
        Me.ucSet.m_collaborationServerID = 0
        Me.ucSet.m_nextRun = "2013-11-26 11:09:41"
        Me.ucSet.m_RepeatUnit = ""
        Me.ucSet.mode = sqlrd.ucSchedule.modeEnum.SETUP
        Me.ucSet.Name = "ucSet"
        Me.ucSet.scheduleID = 0
        Me.ucSet.scheduleStatus = True
        Me.ucSet.sFrequency = "Daily"
        Me.ucSet.Size = New System.Drawing.Size(535, 413)
        Me.ucSet.TabIndex = 0
        '
        'tabSchedule
        '
        Me.tabSchedule.AttachedControl = Me.SuperTabControlPanel2
        Me.tabSchedule.GlobalItem = False
        Me.tabSchedule.Image = CType(resources.GetObject("tabSchedule.Image"), System.Drawing.Image)
        Me.tabSchedule.Name = "tabSchedule"
        Me.tabSchedule.Text = "Schedule"
        Me.tabSchedule.Visible = False
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdFinish)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdCancel)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 413)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(671, 34)
        Me.FlowLayoutPanel1.TabIndex = 28
        '
        'frmDataDrivenPackage
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(671, 447)
        Me.ControlBox = False
        Me.Controls.Add(Me.stabMain)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmDataDrivenPackage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Data-Driven Package Wizard"
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step5.ResumeLayout(False)
        Me.Step5.PerformLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        CType(Me.stabMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabMain.ResumeLayout(False)
        Me.SuperTabControlPanel5.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel6.ResumeLayout(False)
        Me.SuperTabControlPanel7.ResumeLayout(False)
        Me.SuperTabControlPanel4.ResumeLayout(False)
        Me.SuperTabControlPanel3.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub



    Private Sub frmPackWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s5_DataDrivenSched) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, Me, getFeatDesc(featureCodes.s5_DataDrivenSched))
            Close()
            Return
        End If

        Me.Text = "Data-Driven Package Wizard"

        If _CheckScheduleCount() = False Then Return

        Dim I As Int32 = 0



        UcDest.isPackage = True
        UcDest.nPackID = Me.m_PackID
        UcDest.isDataDriven = True

        FormatForWinXP(Me)

        ucSet.dtEndDate.Value = Now.AddYears(100)

        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        End If

        If gParentID > 0 And gParent.Length > 0 Then
            Dim fld As folder = New folder(gParentID)
            txtFolder.Text = fld.getFolderPath
            txtFolder.Tag = gParentID
        End If

        Dim oData As New clsMarsData

        oData.CleanDB()

        setupForDragAndDrop(Me.txtMergePDF)
        setupForDragAndDrop(Me.txtMergeXL)
        setupForDragAndDrop(Me.txtMergeAllPDF)
        setupForDragAndDrop(Me.txtMergePDF)
    End Sub


    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub




    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case stabMain.SelectedTab.Text
            Case "General"
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE, , , True) = False Then
                    ErrProv.SetError(txtName, "A Data-Driven package schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                tabSchedule.Visible = True
                stabMain.SelectedTab = tabSchedule
            Case "Schedule"
                If ucSet.isAllDataValid = False Then
                    Return
                End If

                tabDataDriver.Visible = True
                stabMain.SelectedTab = tabDataDriver

                Me.btnBuild_Click(sender, e)
                grpQuery.Enabled = True

            Case "Data-Driver"
                If txtQuery.Text.Length = 0 Then
                    ErrProv.SetError(txtQuery, "Please create your data selection query")
                    txtQuery.Focus()
                    Return
                ElseIf Me.cmbDDKey.Text = "" Then
                    ErrProv.SetError(Me.cmbDDKey, "Please select the key column")
                    Me.cmbDDKey.Focus()
                    Return
                ElseIf Me.chkMergeAllPDF.Checked And Me.txtMergeAllPDF.Text = "" Then
                    ErrProv.SetError(Me.txtMergeAllPDF, "Please provide a name of the merged PDF file")
                    Me.txtMergeAllPDF.Focus()
                    Return
                ElseIf Me.chkMergeAllXL.Checked And Me.txtMergeAllXL.Text = "" Then
                    ErrProv.SetError(Me.txtMergeAllXL, "Please provide a name of the merged Excel file")
                    Me.txtMergeAllXL.Focus()
                    Return
                End If

                clsMarsReport.populateDataDrivenCache(txtQuery.Text, Me.UcDSN.m_conString, True)

                'check if the selected key column contains unique values only. Going to do this by reading from the data cache
                If clsMarsReport.m_dataDrivenCache IsNot Nothing Then
                    For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                        Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                        Dim rows() As DataRow

                        'use select to get all the rows where the key column matches the dupKey
                        rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                        If rows IsNot Nothing Then
                            If rows.Length > 1 Then
                                ErrProv.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                                cmbDDKey.Focus()
                                Return
                            End If
                        End If
                    Next
                End If

                tabDestinations.Visible = True
                stabMain.SelectedTab = tabDestinations
            Case "Destinations"
                If UcDest.lsvDestination.Nodes.Count = 0 Then
                    ErrProv.SetError(UcDest.lsvDestination, "Please add a destination")
                    Return
                ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
                    ErrProv.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
                    Me.cmbDateTime.Focus()
                    Return
                End If

                tabReports.Visible = True
                stabMain.SelectedTab = tabReports
            Case "Reports"
                If chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
                    ErrProv.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
                    txtMergePDF.Focus()
                    Return
                ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
                    ErrProv.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
                    txtMergeXL.Focus()
                    Return
                ElseIf chkDTStamp.Checked = True And cmbDateTime.Text.Length = 0 Then
                    ErrProv.SetError(cmbDateTime, "Please select the datetime stamp")
                    cmbDateTime.Focus()
                    Return
                ElseIf lsvReports.Items.Count = 0 Then
                    setError(lsvReports, "Please add a report to a package", ErrorIconAlignment.MiddleRight)
                    cmdAddReport.Focus()
                    Return
                End If

                tabException.Visible = True
                stabMain.SelectedTab = tabException
            Case "Exception Handling"
                tabTasks.Visible = True
                stabMain.SelectedTab = tabTasks


                cmdNext.Enabled = False
                cmdFinish.Enabled = True
        End Select
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        removeError(ErrProv, lsvReports)

        Dim sReturn(2) As String
        Dim oNewReport As New frmPackedReport

        oNewReport = New frmPackedReport
        oNewReport.IsDataDriven = True
        sReturn = oNewReport.AddReport(lsvReports.Items.Count + 1, m_PackID)

        If sReturn(0) = "" Then Exit Sub

        Dim lsvItem As ListViewItem = New ListViewItem
        Dim lsvSub As ListViewItem.ListViewSubItem = New ListViewItem.ListViewSubItem

        lsvItem.Text = sReturn(0)
        lsvItem.Tag = sReturn(1)
        lsvSub.Text = sReturn(2)

        lsvItem.SubItems.Add(lsvSub)

        lsvReports.Items.Add(lsvItem)
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nPackID As Integer
        Dim WriteSuccess As Boolean
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim I As Integer = 1
        Dim dynamicTasks As Integer

        'save the package

        If optAll.Checked Then
            dynamicTasks = 1
        Else
            dynamicTasks = 0
        End If

        oErr.BusyProgress(10, "Saving package data...")

        cmdFinish.Enabled = False

        nPackID = Me.m_PackID 'clsMarsData.CreateDataID("PackageAttr", "PackID")

        SQL = "INSERT INTO PackageAttr(PackID,PackageName,Parent,Retry,AssumeFail," & _
            "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName,DateTimeStamp," & _
            "StampFormat,Dynamic,AdjustPackageStamp,AutoCalc,RetryInterval,IsDataDriven,DynamicTasks) VALUES(" & _
            nPackID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter(True) & "," & _
            Convert.ToInt32(UcBlankReportX1.chkCheckBlankReport.Checked) & "," & _
            "'" & gUser & "'," & _
            Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
            Convert.ToInt32(chkMergePDF.Checked) & "," & _
            Convert.ToInt32(chkMergeXL.Checked) & "," & _
            "'" & SQLPrepare(txtMergePDF.Text) & "'," & _
            "'" & SQLPrepare(txtMergeXL.Text) & "'," & _
            Convert.ToInt32(chkDTStamp.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "',0," & _
            txtAdjustStamp.Value & "," & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value & ",1," & _
            dynamicTasks & ")"

        If clsMarsData.WriteData(SQL) = False Then Exit Sub

        oErr.BusyProgress(30, "Saving schedule data...")

        'save the schedule
        Dim ScheduleID As Int64 = ucSet.saveSchedule(m_PackID, clsMarsScheduler.enScheduleType.PACKAGE, txtDesc.Text, txtKeyWord.Text)


        'save the destination

        oErr.BusyProgress(50, "Saving destination data...")

        SQL = "UPDATE DestinationAttr SET ReportID =0, SmartID = 0 WHERE PackID = " & nPackID

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE PackageOptions SET PackID =" & nPackID & " WHERE PackID =99999")

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        'save the data driven information
        Dim conString As String = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _EncryptDBValue(UcDSN.txtPassword.Text)
        SQL = "INSERT INTO DataDrivenAttr (DDID,ReportID,ConString,SQLQuery,FailOnOne,GroupReports,KeyColumn,PackID,MergeAllPDF,MergeAllPDFName,MergeAllXL,MergeAllXLName) " & _
        "VALUES (" & _
        clsMarsData.CreateDataID("datadrivenattr", "ddid") & "," & _
        0 & "," & _
        "'" & SQLPrepare(conString) & "'," & _
        "'" & SQLPrepare(txtQuery.Text) & "'," & _
        Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
        Convert.ToInt32(Me.chkGroupReports.Checked) & "," & _
        "'" & SQLPrepare(Me.cmbDDKey.Text) & "'," & _
        Me.m_PackID & "," & _
        Convert.ToInt32(Me.chkMergeAllPDF.Checked) & "," & _
        "'" & SQLPrepare(Me.txtMergeAllPDF.Text) & "'," & _
        Convert.ToInt32(Me.chkMergeAllXL.Checked) & "," & _
        "'" & SQLPrepare(Me.txtMergeAllXL.Text) & "')"

        clsMarsData.WriteData(SQL)

        oErr.BusyProgress(50, "Saving printers...")

        'save the tasks

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        'save the blank report alert information
        UcBlankReportX1.saveInfo(m_PackID, clsMarsScheduler.enScheduleType.PACKAGE)

        oErr.BusyProgress(90, "Saving reports data...")

        'update the report for this package
        SQL = "UPDATE ReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"
        If clsMarsData.WriteData(SQL) = False Then GoTo RollBackTransaction

        SQL = "UPDATE PackagedReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID =0 WHERE DestinationID =99999")

        'sort out snapshots for the package
        If chkSnapshot.Checked = True Then
            Dim sCols, sVals As String

            sCols = "SnapID,PackID,KeepSnap"

            sVals = clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
            nPackID & "," & _
            txtSnapshots.Value

            SQL = "INSERT INTO ReportSnapshots (" & sCols & ") VALUES " & _
            "(" & sVals & ")"

            clsMarsData.WriteData(SQL)
        End If
        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nPackID, gUser, clsMarsUsers.enViewType.ViewPackage)
        End If

        oErr.BusyProgress(, , True)

        On Error Resume Next

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.CREATE)

        frmRDScheduleWizard.m_DataFields = Nothing
        clsMarsReport.m_dataDrivenCache = Nothing

        Me.Close()
        Exit Sub
RollBackTransaction:
        oErr.BusyProgress(, , True)
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE PackID =" & nPackID)
        cmdFinish.Enabled = True
    End Sub

    Dim closeFlag As Boolean = False

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If closeFlag = False Then
            Dim sp As DevComponents.DotNetBar.SuperTooltip = New DevComponents.DotNetBar.SuperTooltip
            Dim spi As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo("Really Cancel?", "",
                                                                                                               "Are you sure you would like to close the wizard? All your progress will be lost. Click Cancel again to confirm.",
                                                                                                           Nothing,
                                                                                                               Nothing,
                                                                                                               DevComponents.DotNetBar.eTooltipColor.Office2003)
            sp.SetSuperTooltip(sender, spi)
            sp.ShowTooltip(sender)
            closeFlag = True
        Else
            Dim oData As clsMarsData = New clsMarsData

            oData.CleanDB(, Me.m_PackID)

            Dim SQL As String
            Try
                For Each oitem As ListViewItem In lsvReports.Items
                    SQL = "DELETE FROM ReportParameter WHERE ReportID =" & oitem.Tag

                    clsMarsData.WriteData(SQL)
                Next
            Catch
            End Try

            Me.Close()
        End If
    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As ListViewItem = lsvReports.SelectedItems(0)

        nReportID = lsv.Tag

        Dim SQL As String = "DELETE FROM ReportAttr WHERE PackID = " & Me.m_PackID & " AND " & _
                "ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ReportParameter WHERE ReportID=" & nReportID

        clsMarsData.WriteData(SQL)

        lsv.Remove()
    End Sub

    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmPackedReport
        Dim sVals() As String

        oEdit.IsDataDriven = True

        sVals = oEdit.EditReport(lsvReports.SelectedItems(0).Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)

            oItem.SubItems(1).Text = sVals(1)

            lsvReports.Refresh()
        End If


    End Sub

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        cmdEditReport_Click(sender, e)
    End Sub

    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced PDF Pack")
            chkMergePDF.Checked = False
            Return
        End If

        txtMergePDF.Enabled = chkMergePDF.Checked

        If chkMergePDF.Checked = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("PDF", Me.m_PackID)
        End If

        btnPDFMerge.Enabled = chkMergePDF.Checked
    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced Excel Pack")
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True Then
            Dim oOptions As New frmReportOptionsView

            oOptions.packageOptions("XLS", Me.m_PackID)
        End If

        btnXLSMerge.Enabled = chkMergeXL.Checked
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergePDF.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeXL.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked
        txtAdjustStamp.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Value = 0
        Else
            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

        End If
    End Sub


    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up
        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = " & Me.m_PackID & " ORDER BY PackOrderID"


        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = " & Me.m_PackID & " ORDER BY PackOrderID"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub

    Private Sub chkSnapshot_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshot.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.sa8_Snapshots) = False And chkSnapshot.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPRO, sender, "Snapshots")
            chkSnapshot.Checked = False
            Return
        End If

        txtSnapshots.Enabled = chkSnapshot.Checked
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged

    End Sub

    Private Sub cmbDateTime_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbDateTime.Validating
        For Each s As String In IO.Path.GetInvalidFileNameChars
            cmbDateTime.Text = cmbDateTime.Text.Replace(s, "")
        Next
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'If UcDSN._Validate = True Then
        '    Me.grpQuery.Enabled = True

        '    If Me.txtQuery.Text = "" Then
        '        Me.btnBuild_Click(sender, e)
        '    End If
        'End If
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""

        If UcDSN.cmbDSN.Text.Length > 0 Then
            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSN.cmbDSN.Text = sCon.Split("|")(0)
        UcDSN.txtUserID.Text = sCon.Split("|")(1)
        UcDSN.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        oRs.Open(clsMarsParser.Parser.ParseString(txtQuery.Text), oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Me.cmbDDKey.Items.Clear()

        Dim I As Integer = 0

        frmRDScheduleWizard.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            frmRDScheduleWizard.m_DataFields.Add(I, fld.Name)

            Me.cmbDDKey.Items.Add(fld.Name)

            I += 1
        Next

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing
    End Sub

    Private Sub cmbDDKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDDKey.SelectedIndexChanged
        Me.ErrProv.SetError(Me.cmbDDKey, "")
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Undo()
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim getData As frmDataItems = New frmDataItems

        Dim ctrl As TextBox = CType(Me.ActiveControl, TextBox)

        ctrl.Text = getData._GetDataItem(99999)
    End Sub

    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.chkMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.chkMergeAllXL.Enabled = chkGroupReports.Checked

        Me.txtMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.txtMergeAllXL.Enabled = Me.chkGroupReports.Checked
    End Sub

    Private Sub chkMergeAllPDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllPDF.CheckedChanged
        Me.txtMergeAllPDF.Enabled = Me.chkMergeAllPDF.Checked
        Me.chkMergePDF.Enabled = Not chkMergeAllPDF.Checked

        If chkMergeAllPDF.Checked Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced PDF Pack")
                chkMergeAllPDF.Checked = False
                Return
            End If

            Me.chkMergePDF.Checked = False


            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("PDF", Me.m_PackID)
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub

    Private Sub chkMergeAllXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllXL.CheckedChanged
        Me.txtMergeAllXL.Enabled = Me.chkMergeAllXL.Checked
        Me.chkMergeXL.Enabled = Not chkMergeAllXL.Checked

        If chkMergeAllXL.Checked = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, sender, "Advanced Excel Pack")
                chkMergeAllXL.Checked = False
                Return
            End If

            Me.chkMergeXL.Checked = False

            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("XLS", Me.m_PackID)
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub



    Private Sub txtMergeAllPDF_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.Validated, txtMergeAllXL.Validated
        Dim txt As TextBox = CType(sender, TextBox)

        If txt.Text.Contains("<[r]") Then
            MessageBox.Show("Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txt.Focus()
            ErrProv.SetError(txt, "Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver")
        End If
    End Sub

    Private Sub txtMergeAllPDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.TextChanged, txtMergeAllXL.TextChanged
        ErrProv.SetError(CType(sender, TextBox), "")
    End Sub

    Dim inserter As frmInserter

    Private Sub stabMain_SelectedTabChanged(sender As Object, e As DevComponents.DotNetBar.SuperTabStripSelectedTabChangedEventArgs) Handles stabMain.SelectedTabChanged
        If e.NewValue.Text = "Reports" Then
            inserter = showInserter(Me, 99999)
        Else
            If inserter IsNot Nothing AndAlso inserter.IsDisposed = False Then
                inserter.Hide()
            End If
        End If
    End Sub

    Private Sub btnPDFMerge_Click(sender As Object, e As EventArgs) Handles btnPDFMerge.Click
        chkMergePDF_CheckedChanged(Nothing, Nothing)
    End Sub

    Private Sub btnXLSMerge_Click(sender As Object, e As EventArgs) Handles btnXLSMerge.Click
        chkMergeXL_CheckedChanged(Nothing, Nothing)
    End Sub
End Class
