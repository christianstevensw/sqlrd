﻿Public Class frmResults
    Dim resultsTable As DataTable
    Dim imgList As ImageList = New ImageList

    Private Sub frmResults_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub viewResults(ByVal dt As DataTable)
        imgList.Images.Add(Image.FromFile(getAssetLocation("warning.png")))
        imgList.ImageSize = New Size(16, 16)
        imgList.ColorDepth = ColorDepth.Depth32Bit
        lsvFeatures.SmallImageList = imgList
        lsvFeatures.LargeImageList = imgList


        resultsTable = dt

        Dim dv As DataView = New DataView(dt)
        Dim distinctValues As DataTable = dv.ToTable(True, New String() {"featurename", "key"})

        For Each row As DataRow In distinctValues.Rows
            Dim fName As String = row("featurename")
            Dim it As ListViewItem = lsvFeatures.Items.Add(fName)
            it.ImageIndex = 0
            it.Tag = row("key")
        Next

        Me.ShowDialog()

    End Sub

    Private Sub lsvFeatures_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvFeatures.SelectedIndexChanged
        If lsvFeatures.SelectedItems.Count = 0 Then Return

        Dim it As ListViewItem = lsvFeatures.SelectedItems(0)

        Dim dt As DataTable = New DataTable

        dt.Columns.Add("Name")
        dt.Columns.Add("Location")

        For Each r As DataRow In resultsTable.Select("key ='" & it.Tag & "'")
            Dim newr As DataRow = dt.Rows.Add
            newr(0) = r("schedulename")
            newr(1) = r("schedulelocation")
        Next

        grdResults.DataSource = dt
        grdResults.Refresh()

        grdResults.Columns(0).Width = grdResults.Width / 2
        grdResults.Columns(1).Width = grdResults.Width / 2
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim sfd As SaveFileDialog = New SaveFileDialog

        sfd.Title = "Save data to file"
        sfd.DefaultExt = ".xml"

        If sfd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Dim file As String = sfd.FileName

            resultsTable.WriteXml(file)
        End If
    End Sub
End Class