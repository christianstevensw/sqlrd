<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSMTPAdvanced
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkRequiresPop = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpPop = New System.Windows.Forms.GroupBox()
        Me.btnTest = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtPOPServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPopUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.txtPopPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPopPort = New System.Windows.Forms.MaskedTextBox()
        Me.chkPOPSSL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.cmbSmtpAuthMode = New System.Windows.Forms.ComboBox()
        Me.chkSmtpStartTLS = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkSmtpSSL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.tbIMAP = New System.Windows.Forms.TabPage()
        Me.btnTestIMAP = New DevComponents.DotNetBar.ButtonX()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.txtIMAPServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIMAPUser = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIMAPPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtIMAPPort = New System.Windows.Forms.NumericUpDown()
        Me.chkIMAPSSL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.grpPop.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.tbIMAP.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.txtIMAPPort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkRequiresPop
        '
        Me.chkRequiresPop.AutoSize = True
        '
        '
        '
        Me.chkRequiresPop.BackgroundStyle.Class = ""
        Me.chkRequiresPop.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRequiresPop.Location = New System.Drawing.Point(6, 6)
        Me.chkRequiresPop.Name = "chkRequiresPop"
        Me.chkRequiresPop.Size = New System.Drawing.Size(222, 16)
        Me.chkRequiresPop.TabIndex = 0
        Me.chkRequiresPop.Text = "SMTP Server requires POP aunthetication"
        '
        'grpPop
        '
        Me.grpPop.Controls.Add(Me.btnTest)
        Me.grpPop.Controls.Add(Me.TableLayoutPanel1)
        Me.grpPop.Enabled = False
        Me.grpPop.Location = New System.Drawing.Point(6, 29)
        Me.grpPop.Name = "grpPop"
        Me.grpPop.Size = New System.Drawing.Size(363, 180)
        Me.grpPop.TabIndex = 1
        Me.grpPop.TabStop = False
        Me.grpPop.Text = "POP Details"
        '
        'btnTest
        '
        Me.btnTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnTest.Location = New System.Drawing.Point(150, 149)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 0
        Me.btnTest.Text = "Verify"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.19277!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.80723!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtPOPServer, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopUser, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopPassword, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopPort, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.chkPOPSSL, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(25, 19)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(332, 124)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'txtPOPServer
        '
        '
        '
        '
        Me.txtPOPServer.Border.Class = "TextBoxBorder"
        Me.txtPOPServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPOPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtPOPServer.Location = New System.Drawing.Point(79, 3)
        Me.txtPOPServer.Name = "txtPOPServer"
        Me.txtPOPServer.Size = New System.Drawing.Size(226, 21)
        Me.txtPOPServer.TabIndex = 0
        '
        'txtPopUser
        '
        '
        '
        '
        Me.txtPopUser.Border.Class = "TextBoxBorder"
        Me.txtPopUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPopUser.ForeColor = System.Drawing.Color.Blue
        Me.txtPopUser.Location = New System.Drawing.Point(79, 27)
        Me.txtPopUser.Name = "txtPopUser"
        Me.txtPopUser.Size = New System.Drawing.Size(226, 21)
        Me.txtPopUser.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "POP Server"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(3, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "User Name"
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(3, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Password"
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(3, 75)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(22, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Port"
        '
        'txtPopPassword
        '
        '
        '
        '
        Me.txtPopPassword.Border.Class = "TextBoxBorder"
        Me.txtPopPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPopPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtPopPassword.Location = New System.Drawing.Point(79, 51)
        Me.txtPopPassword.Name = "txtPopPassword"
        Me.txtPopPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPopPassword.Size = New System.Drawing.Size(226, 21)
        Me.txtPopPassword.TabIndex = 2
        '
        'txtPopPort
        '
        Me.txtPopPort.ForeColor = System.Drawing.Color.Blue
        Me.txtPopPort.Location = New System.Drawing.Point(79, 75)
        Me.txtPopPort.Mask = "00000"
        Me.txtPopPort.Name = "txtPopPort"
        Me.txtPopPort.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txtPopPort.Size = New System.Drawing.Size(44, 21)
        Me.txtPopPort.TabIndex = 3
        Me.txtPopPort.Text = "110"
        Me.txtPopPort.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals
        Me.txtPopPort.ValidatingType = GetType(Integer)
        '
        'chkPOPSSL
        '
        Me.chkPOPSSL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPOPSSL.AutoSize = True
        '
        '
        '
        Me.chkPOPSSL.BackgroundStyle.Class = ""
        Me.chkPOPSSL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkPOPSSL.Location = New System.Drawing.Point(79, 99)
        Me.chkPOPSSL.Name = "chkPOPSSL"
        Me.chkPOPSSL.Size = New System.Drawing.Size(62, 16)
        Me.chkPOPSSL.TabIndex = 4
        Me.chkPOPSSL.Text = "Use SSL"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.Location = New System.Drawing.Point(311, 251)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Location = New System.Drawing.Point(230, 251)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "&OK"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.tbIMAP)
        Me.TabControl1.Location = New System.Drawing.Point(2, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(384, 241)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkRequiresPop)
        Me.TabPage1.Controls.Add(Me.grpPop)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(376, 215)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "POP Aunthentication"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.cmbSmtpAuthMode)
        Me.TabPage2.Controls.Add(Me.chkSmtpStartTLS)
        Me.TabPage2.Controls.Add(Me.chkSmtpSSL)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(376, 215)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "SMTP Security"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(6, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Aunthentication Method"
        '
        'cmbSmtpAuthMode
        '
        Me.cmbSmtpAuthMode.FormattingEnabled = True
        Me.cmbSmtpAuthMode.Items.AddRange(New Object() {"NONE", "LOGIN", "PLAIN", "CRAM-MD5", "NTLM"})
        Me.cmbSmtpAuthMode.Location = New System.Drawing.Point(132, 68)
        Me.cmbSmtpAuthMode.Name = "cmbSmtpAuthMode"
        Me.cmbSmtpAuthMode.Size = New System.Drawing.Size(121, 21)
        Me.cmbSmtpAuthMode.TabIndex = 2
        Me.cmbSmtpAuthMode.Text = "LOGIN"
        '
        'chkSmtpStartTLS
        '
        Me.chkSmtpStartTLS.AutoSize = True
        '
        '
        '
        Me.chkSmtpStartTLS.BackgroundStyle.Class = ""
        Me.chkSmtpStartTLS.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSmtpStartTLS.Location = New System.Drawing.Point(6, 40)
        Me.chkSmtpStartTLS.Name = "chkSmtpStartTLS"
        Me.chkSmtpStartTLS.Size = New System.Drawing.Size(97, 16)
        Me.chkSmtpStartTLS.TabIndex = 1
        Me.chkSmtpStartTLS.Text = "Use ""Start TLS"""
        '
        'chkSmtpSSL
        '
        Me.chkSmtpSSL.AutoSize = True
        '
        '
        '
        Me.chkSmtpSSL.BackgroundStyle.Class = ""
        Me.chkSmtpSSL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSmtpSSL.Location = New System.Drawing.Point(6, 17)
        Me.chkSmtpSSL.Name = "chkSmtpSSL"
        Me.chkSmtpSSL.Size = New System.Drawing.Size(119, 16)
        Me.chkSmtpSSL.TabIndex = 0
        Me.chkSmtpSSL.Text = "Use SSL Connection"
        '
        'tbIMAP
        '
        Me.tbIMAP.Controls.Add(Me.btnTestIMAP)
        Me.tbIMAP.Controls.Add(Me.TableLayoutPanel2)
        Me.tbIMAP.Location = New System.Drawing.Point(4, 22)
        Me.tbIMAP.Name = "tbIMAP"
        Me.tbIMAP.Padding = New System.Windows.Forms.Padding(3)
        Me.tbIMAP.Size = New System.Drawing.Size(376, 215)
        Me.tbIMAP.TabIndex = 2
        Me.tbIMAP.Text = "IMAP (Delivery Receipts)"
        Me.tbIMAP.UseVisualStyleBackColor = True
        '
        'btnTestIMAP
        '
        Me.btnTestIMAP.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnTestIMAP.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnTestIMAP.Location = New System.Drawing.Point(295, 164)
        Me.btnTestIMAP.Name = "btnTestIMAP"
        Me.btnTestIMAP.Size = New System.Drawing.Size(75, 23)
        Me.btnTestIMAP.TabIndex = 0
        Me.btnTestIMAP.Text = "Test"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.49575!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.50425!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label9, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label10, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.txtIMAPServer, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtIMAPUser, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.txtIMAPPassword, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.txtIMAPPort, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.chkIMAPSSL, 1, 4)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(6, 6)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 5
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(364, 152)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.Location = New System.Drawing.Point(3, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(86, 24)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "IMAP Server"
        '
        'Label7
        '
        Me.Label7.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.Location = New System.Drawing.Point(3, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 24)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "User Name"
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label8.BackgroundStyle.Class = ""
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 63)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(86, 24)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Password"
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label9.BackgroundStyle.Class = ""
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(3, 93)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 24)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Port"
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label10.BackgroundStyle.Class = ""
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Location = New System.Drawing.Point(3, 123)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 26)
        Me.Label10.TabIndex = 0
        '
        'txtIMAPServer
        '
        Me.txtIMAPServer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtIMAPServer.Border.Class = "TextBoxBorder"
        Me.txtIMAPServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIMAPServer.Location = New System.Drawing.Point(95, 3)
        Me.txtIMAPServer.Name = "txtIMAPServer"
        Me.txtIMAPServer.Size = New System.Drawing.Size(266, 21)
        Me.txtIMAPServer.TabIndex = 0
        '
        'txtIMAPUser
        '
        Me.txtIMAPUser.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtIMAPUser.Border.Class = "TextBoxBorder"
        Me.txtIMAPUser.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIMAPUser.Location = New System.Drawing.Point(95, 33)
        Me.txtIMAPUser.Name = "txtIMAPUser"
        Me.txtIMAPUser.Size = New System.Drawing.Size(266, 21)
        Me.txtIMAPUser.TabIndex = 1
        '
        'txtIMAPPassword
        '
        Me.txtIMAPPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.txtIMAPPassword.Border.Class = "TextBoxBorder"
        Me.txtIMAPPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIMAPPassword.Location = New System.Drawing.Point(95, 63)
        Me.txtIMAPPassword.Name = "txtIMAPPassword"
        Me.txtIMAPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtIMAPPassword.Size = New System.Drawing.Size(266, 21)
        Me.txtIMAPPassword.TabIndex = 2
        '
        'txtIMAPPort
        '
        Me.txtIMAPPort.Location = New System.Drawing.Point(95, 93)
        Me.txtIMAPPort.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtIMAPPort.Name = "txtIMAPPort"
        Me.txtIMAPPort.Size = New System.Drawing.Size(56, 21)
        Me.txtIMAPPort.TabIndex = 3
        Me.txtIMAPPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkIMAPSSL
        '
        Me.chkIMAPSSL.AutoSize = True
        '
        '
        '
        Me.chkIMAPSSL.BackgroundStyle.Class = ""
        Me.chkIMAPSSL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkIMAPSSL.Location = New System.Drawing.Point(95, 123)
        Me.chkIMAPSSL.Name = "chkIMAPSSL"
        Me.chkIMAPSSL.Size = New System.Drawing.Size(62, 16)
        Me.chkIMAPSSL.TabIndex = 4
        Me.chkIMAPSSL.Text = "Use SSL"
        '
        'frmSMTPAdvanced
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(391, 278)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.Name = "frmSMTPAdvanced"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Advanced..."
        Me.grpPop.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.tbIMAP.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        CType(Me.txtIMAPPort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkRequiresPop As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents grpPop As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPOPServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPopUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtPopPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents btnTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtPopPort As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents chkPOPSSL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbSmtpAuthMode As System.Windows.Forms.ComboBox
    Friend WithEvents chkSmtpStartTLS As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkSmtpSSL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents tbIMAP As System.Windows.Forms.TabPage
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtIMAPServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIMAPUser As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIMAPPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtIMAPPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkIMAPSSL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents btnTestIMAP As DevComponents.DotNetBar.ButtonX
End Class
