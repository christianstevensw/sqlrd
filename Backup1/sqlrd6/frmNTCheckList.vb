Public Class frmNTCheckList
    Dim ep As New ErrorProvider
    Dim userCancel As Boolean = True

    Private Sub btnProceed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProceed.Click
        For Each ctrl As Control In Panel1.Controls
            If TypeOf ctrl Is CheckBox Then
                Dim check As CheckBox = CType(ctrl, CheckBox)

                If check.Checked = False Then
                    SetError(check, "Please make sure you have checked all the options before proceeding")
                    Return
                End If
            End If
        Next

        userCancel = False

        Close()
    End Sub

    Public Function ShowCheckList() As Boolean
        Me.ShowDialog()

        Return Not (userCancel)
    End Function

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles CheckBox1.CheckedChanged, CheckBox2.CheckedChanged, CheckBox3.CheckedChanged, CheckBox4.CheckedChanged, CheckBox5.CheckedChanged
        SetError(sender, "")
    End Sub

    Private Sub frmNTCheckList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        userCancel = True
        Close()
    End Sub
End Class