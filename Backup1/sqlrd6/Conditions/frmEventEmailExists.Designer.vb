<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventEmailExists
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventEmailExists))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Page1 = New System.Windows.Forms.GroupBox()
        Me.Label13 = New DevComponents.DotNetBar.LabelX()
        Me.cmbServerType = New System.Windows.Forms.ComboBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtPoll = New System.Windows.Forms.NumericUpDown()
        Me.Label11 = New DevComponents.DotNetBar.LabelX()
        Me.cmbValue1 = New System.Windows.Forms.ComboBox()
        Me.Label5 = New DevComponents.DotNetBar.LabelX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkSSL = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label12 = New DevComponents.DotNetBar.LabelX()
        Me.Label1 = New DevComponents.DotNetBar.LabelX()
        Me.txtUserID = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtPort = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.txtServer = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label6 = New DevComponents.DotNetBar.LabelX()
        Me.cmdTest = New DevComponents.DotNetBar.ButtonX()
        Me.txtPassword = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label7 = New DevComponents.DotNetBar.LabelX()
        Me.Page2 = New System.Windows.Forms.GroupBox()
        Me.chkMatchExact = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.pnAttach = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnSaveAttach = New DevComponents.DotNetBar.ButtonX()
        Me.txtAttach = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkSaveAttach = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.btnForward = New DevComponents.DotNetBar.ButtonX()
        Me.pnRedirect = New System.Windows.Forms.FlowLayoutPanel()
        Me.cmdTo = New DevComponents.DotNetBar.ButtonX()
        Me.txtTo = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.chkRedirect = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.chkForward = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.cmbValue = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label8 = New DevComponents.DotNetBar.LabelX()
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox()
        Me.Label9 = New DevComponents.DotNetBar.LabelX()
        Me.btnRemove = New DevComponents.DotNetBar.ButtonX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.btnAdd = New DevComponents.DotNetBar.ButtonX()
        Me.cmbField = New System.Windows.Forms.ComboBox()
        Me.chkRemove = New DevComponents.DotNetBar.Controls.CheckBoxX()
        Me.Label2 = New DevComponents.DotNetBar.LabelX()
        Me.lsvSearch = New DevComponents.DotNetBar.Controls.ListViewEx()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Page1a = New System.Windows.Forms.GroupBox()
        Me.tvIMAP = New System.Windows.Forms.TreeView()
        Me.Label14 = New DevComponents.DotNetBar.LabelX()
        Me.txtIMAPPath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Page3 = New System.Windows.Forms.GroupBox()
        Me.Label10 = New DevComponents.DotNetBar.LabelX()
        Me.cmdBack = New DevComponents.DotNetBar.ButtonX()
        Me.cmdNext = New DevComponents.DotNetBar.ButtonX()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.mnuContacts = New System.Windows.Forms.ContextMenu()
        Me.mnuMAPI = New System.Windows.Forms.MenuItem()
        Me.mnuMARS = New System.Windows.Forms.MenuItem()
        Me.mnuDB = New System.Windows.Forms.MenuItem()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuConstants = New System.Windows.Forms.MenuItem()
        Me.ofd = New System.Windows.Forms.OpenFileDialog()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.mnuDatabase = New System.Windows.Forms.MenuItem()
        Me.tippy = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.Page1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtPoll, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.Page2.SuspendLayout()
        Me.pnAttach.SuspendLayout()
        Me.pnRedirect.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Page1a.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page1a)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 415)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Condition Details"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.Label13)
        Me.Page1.Controls.Add(Me.cmbServerType)
        Me.Page1.Controls.Add(Me.GroupBox3)
        Me.Page1.Controls.Add(Me.cmbValue1)
        Me.Page1.Controls.Add(Me.Label5)
        Me.Page1.Controls.Add(Me.txtName)
        Me.Page1.Controls.Add(Me.Label4)
        Me.Page1.Controls.Add(Me.GroupBox2)
        Me.Page1.Location = New System.Drawing.Point(6, 20)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(427, 364)
        Me.Page1.TabIndex = 0
        Me.Page1.TabStop = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        '
        '
        '
        Me.Label13.BackgroundStyle.Class = ""
        Me.Label13.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label13.Location = New System.Drawing.Point(3, 83)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(61, 16)
        Me.Label13.TabIndex = 41
        Me.Label13.Text = "Server Type"
        '
        'cmbServerType
        '
        Me.cmbServerType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbServerType.FormattingEnabled = True
        Me.cmbServerType.Items.AddRange(New Object() {"POP3", "IMAP4"})
        Me.cmbServerType.Location = New System.Drawing.Point(145, 79)
        Me.cmbServerType.Name = "cmbServerType"
        Me.cmbServerType.Size = New System.Drawing.Size(195, 21)
        Me.cmbServerType.TabIndex = 40
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtPoll)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Location = New System.Drawing.Point(7, 257)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(414, 54)
        Me.GroupBox3.TabIndex = 39
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Mailbox Polling"
        '
        'txtPoll
        '
        Me.txtPoll.Location = New System.Drawing.Point(271, 25)
        Me.txtPoll.Maximum = New Decimal(New Integer() {8640, 0, 0, 0})
        Me.txtPoll.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtPoll.Name = "txtPoll"
        Me.txtPoll.Size = New System.Drawing.Size(62, 21)
        Me.txtPoll.TabIndex = 39
        Me.txtPoll.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPoll.Value = New Decimal(New Integer() {5, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        '
        '
        '
        Me.Label11.BackgroundStyle.Class = ""
        Me.Label11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label11.Location = New System.Drawing.Point(6, 29)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(141, 16)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Poll mailbox every (minutes)"
        '
        'cmbValue1
        '
        Me.cmbValue1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValue1.FormattingEnabled = True
        Me.cmbValue1.Items.AddRange(New Object() {"TRUE", "FALSE"})
        Me.cmbValue1.Location = New System.Drawing.Point(145, 50)
        Me.cmbValue1.Name = "cmbValue1"
        Me.cmbValue1.Size = New System.Drawing.Size(195, 21)
        Me.cmbValue1.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        '
        '
        '
        Me.Label5.BackgroundStyle.Class = ""
        Me.Label5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label5.Location = New System.Drawing.Point(3, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(107, 16)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "If unread email exists"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(145, 20)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(195, 21)
        Me.txtName.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.Class = ""
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(3, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(31, 16)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Name"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkSSL)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtUserID)
        Me.GroupBox2.Controls.Add(Me.txtPort)
        Me.GroupBox2.Controls.Add(Me.txtServer)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.cmdTest)
        Me.GroupBox2.Controls.Add(Me.txtPassword)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 109)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(415, 146)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Server details"
        '
        'chkSSL
        '
        Me.chkSSL.AutoSize = True
        '
        '
        '
        Me.chkSSL.BackgroundStyle.Class = ""
        Me.chkSSL.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSSL.Location = New System.Drawing.Point(94, 90)
        Me.chkSSL.Name = "chkSSL"
        Me.chkSSL.Size = New System.Drawing.Size(271, 16)
        Me.chkSSL.TabIndex = 4
        Me.chkSSL.Text = "This server requires an encrypted connection (SSL)"
        '
        'Label12
        '
        '
        '
        '
        Me.Label12.BackgroundStyle.Class = ""
        Me.Label12.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(251, 18)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 16)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Port"
        '
        'Label1
        '
        '
        '
        '
        Me.Label1.BackgroundStyle.Class = ""
        Me.Label1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Server Address"
        '
        'txtUserID
        '
        '
        '
        '
        Me.txtUserID.Border.Class = "TextBoxBorder"
        Me.txtUserID.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtUserID.Location = New System.Drawing.Point(94, 39)
        Me.txtUserID.MaxLength = 100
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(240, 21)
        Me.txtUserID.TabIndex = 2
        Me.txtUserID.Tag = "memo"
        '
        'txtPort
        '
        '
        '
        '
        Me.txtPort.Border.Class = "TextBoxBorder"
        Me.txtPort.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPort.Location = New System.Drawing.Point(292, 14)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(42, 21)
        Me.txtPort.TabIndex = 1
        '
        'txtServer
        '
        '
        '
        '
        Me.txtServer.Border.Class = "TextBoxBorder"
        Me.txtServer.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtServer.Location = New System.Drawing.Point(94, 15)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(147, 21)
        Me.txtServer.TabIndex = 0
        '
        'Label6
        '
        '
        '
        '
        Me.Label6.BackgroundStyle.Class = ""
        Me.Label6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(6, 41)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "User ID"
        '
        'cmdTest
        '
        Me.cmdTest.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTest.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(272, 115)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(62, 21)
        Me.cmdTest.TabIndex = 5
        Me.cmdTest.Text = "Test"
        '
        'txtPassword
        '
        '
        '
        '
        Me.txtPassword.Border.Class = "TextBoxBorder"
        Me.txtPassword.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(94, 63)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(240, 21)
        Me.txtPassword.TabIndex = 3
        '
        'Label7
        '
        '
        '
        '
        Me.Label7.BackgroundStyle.Class = ""
        Me.Label7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(6, 65)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Password"
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.chkMatchExact)
        Me.Page2.Controls.Add(Me.pnAttach)
        Me.Page2.Controls.Add(Me.chkSaveAttach)
        Me.Page2.Controls.Add(Me.btnForward)
        Me.Page2.Controls.Add(Me.pnRedirect)
        Me.Page2.Controls.Add(Me.chkRedirect)
        Me.Page2.Controls.Add(Me.chkForward)
        Me.Page2.Controls.Add(Me.cmbValue)
        Me.Page2.Controls.Add(Me.TableLayoutPanel1)
        Me.Page2.Controls.Add(Me.btnRemove)
        Me.Page2.Controls.Add(Me.Label3)
        Me.Page2.Controls.Add(Me.cmbOperator)
        Me.Page2.Controls.Add(Me.btnAdd)
        Me.Page2.Controls.Add(Me.cmbField)
        Me.Page2.Controls.Add(Me.chkRemove)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.lsvSearch)
        Me.Page2.Location = New System.Drawing.Point(6, 20)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(427, 364)
        Me.Page2.TabIndex = 1
        Me.Page2.TabStop = False
        Me.Page2.Visible = False
        '
        'chkMatchExact
        '
        Me.chkMatchExact.AutoSize = True
        '
        '
        '
        Me.chkMatchExact.BackgroundStyle.Class = ""
        Me.chkMatchExact.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkMatchExact.Location = New System.Drawing.Point(9, 55)
        Me.chkMatchExact.Name = "chkMatchExact"
        Me.chkMatchExact.Size = New System.Drawing.Size(117, 16)
        Me.chkMatchExact.TabIndex = 15
        Me.chkMatchExact.Text = "Match exact phrase"
        Me.tippy.SetToolTip(Me.chkMatchExact, "(CRD will match CASE and include all trailing and proceeding spaces in the search" & _
        " criteria)")
        '
        'pnAttach
        '
        Me.pnAttach.Controls.Add(Me.btnSaveAttach)
        Me.pnAttach.Controls.Add(Me.txtAttach)
        Me.pnAttach.Enabled = False
        Me.pnAttach.Location = New System.Drawing.Point(128, 306)
        Me.pnAttach.Name = "pnAttach"
        Me.pnAttach.Size = New System.Drawing.Size(287, 28)
        Me.pnAttach.TabIndex = 14
        '
        'btnSaveAttach
        '
        Me.btnSaveAttach.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnSaveAttach.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnSaveAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSaveAttach.Location = New System.Drawing.Point(3, 3)
        Me.btnSaveAttach.Name = "btnSaveAttach"
        Me.btnSaveAttach.Size = New System.Drawing.Size(48, 21)
        Me.btnSaveAttach.TabIndex = 13
        Me.btnSaveAttach.Text = "To"
        '
        'txtAttach
        '
        '
        '
        '
        Me.txtAttach.Border.Class = "TextBoxBorder"
        Me.txtAttach.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtAttach.Location = New System.Drawing.Point(57, 3)
        Me.txtAttach.Name = "txtAttach"
        Me.txtAttach.Size = New System.Drawing.Size(221, 21)
        Me.txtAttach.TabIndex = 12
        Me.txtAttach.Tag = "memo"
        '
        'chkSaveAttach
        '
        Me.chkSaveAttach.AutoSize = True
        '
        '
        '
        Me.chkSaveAttach.BackgroundStyle.Class = ""
        Me.chkSaveAttach.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkSaveAttach.Location = New System.Drawing.Point(7, 310)
        Me.chkSaveAttach.Name = "chkSaveAttach"
        Me.chkSaveAttach.Size = New System.Drawing.Size(110, 16)
        Me.chkSaveAttach.TabIndex = 13
        Me.chkSaveAttach.Text = "Save Attachments"
        '
        'btnForward
        '
        Me.btnForward.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnForward.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnForward.Enabled = False
        Me.btnForward.Location = New System.Drawing.Point(131, 239)
        Me.btnForward.Name = "btnForward"
        Me.btnForward.Size = New System.Drawing.Size(48, 21)
        Me.btnForward.TabIndex = 12
        Me.btnForward.Text = "..."
        '
        'pnRedirect
        '
        Me.pnRedirect.Controls.Add(Me.cmdTo)
        Me.pnRedirect.Controls.Add(Me.txtTo)
        Me.pnRedirect.Enabled = False
        Me.pnRedirect.Location = New System.Drawing.Point(128, 269)
        Me.pnRedirect.Name = "pnRedirect"
        Me.pnRedirect.Size = New System.Drawing.Size(287, 28)
        Me.pnRedirect.TabIndex = 2
        '
        'cmdTo
        '
        Me.cmdTo.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdTo.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(3, 3)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(48, 21)
        Me.cmdTo.TabIndex = 13
        Me.cmdTo.Text = "To"
        '
        'txtTo
        '
        '
        '
        '
        Me.txtTo.Border.Class = "TextBoxBorder"
        Me.txtTo.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtTo.Location = New System.Drawing.Point(57, 3)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(221, 21)
        Me.txtTo.TabIndex = 12
        Me.txtTo.Tag = "memo"
        '
        'chkRedirect
        '
        Me.chkRedirect.AutoSize = True
        '
        '
        '
        Me.chkRedirect.BackgroundStyle.Class = ""
        Me.chkRedirect.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRedirect.Location = New System.Drawing.Point(6, 275)
        Me.chkRedirect.Name = "chkRedirect"
        Me.chkRedirect.Size = New System.Drawing.Size(111, 16)
        Me.chkRedirect.TabIndex = 11
        Me.chkRedirect.Text = "Redirect the email"
        '
        'chkForward
        '
        Me.chkForward.AutoSize = True
        '
        '
        '
        Me.chkForward.BackgroundStyle.Class = ""
        Me.chkForward.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkForward.Location = New System.Drawing.Point(7, 241)
        Me.chkForward.Name = "chkForward"
        Me.chkForward.Size = New System.Drawing.Size(110, 16)
        Me.chkForward.TabIndex = 10
        Me.chkForward.Text = "Forward the email"
        '
        'cmbValue
        '
        '
        '
        '
        Me.cmbValue.Border.Class = "TextBoxBorder"
        Me.cmbValue.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.cmbValue.Location = New System.Drawing.Point(284, 95)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(131, 21)
        Me.cmbValue.TabIndex = 9
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.23622!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.11927!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.78899!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbAnyAll, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 16)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(421, 26)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label8.BackgroundStyle.Class = ""
        Me.Label8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label8.Location = New System.Drawing.Point(3, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(37, 20)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Match"
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.FormattingEnabled = True
        Me.cmbAnyAll.Items.AddRange(New Object() {"ANY", "ALL"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(46, 3)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        '
        '
        '
        Me.Label9.BackgroundStyle.Class = ""
        Me.Label9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label9.Location = New System.Drawing.Point(122, 3)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(195, 20)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "of the following criteria"
        '
        'btnRemove
        '
        Me.btnRemove.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnRemove.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(100, 120)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(40, 23)
        Me.btnRemove.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.Class = ""
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(281, 81)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Search value"
        '
        'cmbOperator
        '
        Me.cmbOperator.FormattingEnabled = True
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", "CONTAINS", "DOES NOT CONTAIN", "BEGINS WITH", "ENDS WITH", ">", "<", "=>", "<="})
        Me.cmbOperator.Location = New System.Drawing.Point(162, 95)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(104, 21)
        Me.cmbOperator.TabIndex = 1
        '
        'btnAdd
        '
        Me.btnAdd.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnAdd.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(284, 120)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(40, 23)
        Me.btnAdd.TabIndex = 5
        '
        'cmbField
        '
        Me.cmbField.FormattingEnabled = True
        Me.cmbField.Items.AddRange(New Object() {"From", "To", "Cc", "Bcc", "Subject", "Message", "Date (yyyy-MM-dd HH:mm:ss)"})
        Me.cmbField.Location = New System.Drawing.Point(9, 95)
        Me.cmbField.Name = "cmbField"
        Me.cmbField.Size = New System.Drawing.Size(127, 21)
        Me.cmbField.TabIndex = 0
        '
        'chkRemove
        '
        '
        '
        '
        Me.chkRemove.BackgroundStyle.Class = ""
        Me.chkRemove.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.chkRemove.Checked = True
        Me.chkRemove.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkRemove.CheckValue = "Y"
        Me.chkRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRemove.Location = New System.Drawing.Point(6, 337)
        Me.chkRemove.Name = "chkRemove"
        Me.chkRemove.Size = New System.Drawing.Size(334, 24)
        Me.chkRemove.TabIndex = 7
        Me.chkRemove.Text = "Remove matched mail from server (Recommended)"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        '
        '
        '
        Me.Label2.BackgroundStyle.Class = ""
        Me.Label2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label2.Location = New System.Drawing.Point(6, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Email Field"
        '
        'lsvSearch
        '
        '
        '
        '
        Me.lsvSearch.Border.Class = "ListViewBorder"
        Me.lsvSearch.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.lsvSearch.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSearch.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvSearch.HideSelection = False
        Me.lsvSearch.Location = New System.Drawing.Point(9, 146)
        Me.lsvSearch.Name = "lsvSearch"
        Me.lsvSearch.Size = New System.Drawing.Size(407, 89)
        Me.lsvSearch.TabIndex = 6
        Me.lsvSearch.UseCompatibleStateImageBehavior = False
        Me.lsvSearch.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Search value"
        Me.ColumnHeader1.Width = 397
        '
        'Page1a
        '
        Me.Page1a.Controls.Add(Me.tvIMAP)
        Me.Page1a.Controls.Add(Me.Label14)
        Me.Page1a.Controls.Add(Me.txtIMAPPath)
        Me.Page1a.Location = New System.Drawing.Point(6, 20)
        Me.Page1a.Name = "Page1a"
        Me.Page1a.Size = New System.Drawing.Size(427, 364)
        Me.Page1a.TabIndex = 2
        Me.Page1a.TabStop = False
        Me.Page1a.Visible = False
        '
        'tvIMAP
        '
        Me.tvIMAP.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvIMAP.Location = New System.Drawing.Point(3, 16)
        Me.tvIMAP.Name = "tvIMAP"
        Me.tvIMAP.PathSeparator = "/"
        Me.tvIMAP.Size = New System.Drawing.Size(421, 324)
        Me.tvIMAP.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        '
        '
        '
        Me.Label14.BackgroundStyle.Class = ""
        Me.Label14.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label14.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label14.Location = New System.Drawing.Point(3, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(169, 16)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Please select the folder to monitor"
        '
        'txtIMAPPath
        '
        '
        '
        '
        Me.txtIMAPPath.Border.Class = "TextBoxBorder"
        Me.txtIMAPPath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtIMAPPath.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.txtIMAPPath.Location = New System.Drawing.Point(3, 340)
        Me.txtIMAPPath.Name = "txtIMAPPath"
        Me.txtIMAPPath.ReadOnly = True
        Me.txtIMAPPath.Size = New System.Drawing.Size(421, 21)
        Me.txtIMAPPath.TabIndex = 1
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.Label10)
        Me.Page3.Location = New System.Drawing.Point(6, 20)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(427, 364)
        Me.Page3.TabIndex = 2
        Me.Page3.TabStop = False
        Me.Page3.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        '
        '
        '
        Me.Label10.BackgroundStyle.Class = ""
        Me.Label10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Navy
        Me.Label10.Location = New System.Drawing.Point(9, 23)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(291, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Setting up of condition completed. Click OK to finish"
        '
        'cmdBack
        '
        Me.cmdBack.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBack.BackColor = System.Drawing.Color.Transparent
        Me.cmdBack.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBack.Enabled = False
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(15, 388)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 22)
        Me.cmdBack.TabIndex = 1
        Me.cmdBack.Text = " "
        '
        'cmdNext
        '
        Me.cmdNext.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdNext.BackColor = System.Drawing.Color.Transparent
        Me.cmdNext.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdNext.Enabled = False
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(366, 388)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 22)
        Me.cmdNext.TabIndex = 2
        Me.cmdNext.Text = " "
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.BackColor = System.Drawing.Color.Transparent
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.Enabled = False
        Me.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnOK.Location = New System.Drawing.Point(454, 12)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 0
        Me.btnOK.Text = "&OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.BackColor = System.Drawing.Color.Transparent
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCancel.Location = New System.Drawing.Point(454, 42)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "&Cancel"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "CRD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuDelete, Me.mnuCopy, Me.mnuPaste, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 3
        Me.mnuDelete.Text = "Delete"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 4
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 5
        Me.mnuPaste.Text = "Paste"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'frmEventEmailExists
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(532, 427)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmEventEmailExists"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Condition - Unread Email  Exists"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page1.ResumeLayout(False)
        Me.Page1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.txtPoll, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Page2.ResumeLayout(False)
        Me.Page2.PerformLayout()
        Me.pnAttach.ResumeLayout(False)
        Me.pnRedirect.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Page1a.ResumeLayout(False)
        Me.Page1a.PerformLayout()
        Me.Page3.ResumeLayout(False)
        Me.Page3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmdTest As DevComponents.DotNetBar.ButtonX
    Friend WithEvents Label7 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPassword As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label6 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtServer As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents txtUserID As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents chkRemove As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbField As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents lsvSearch As DevComponents.DotNetBar.Controls.ListViewEx
    Friend WithEvents btnRemove As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnAdd As DevComponents.DotNetBar.ButtonX
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdBack As DevComponents.DotNetBar.ButtonX
    Friend WithEvents cmdNext As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbValue1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As DevComponents.DotNetBar.LabelX
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label8 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbValue As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPoll As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label11 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkRedirect As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents chkForward As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents txtTo As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdTo As DevComponents.DotNetBar.ButtonX
    Friend WithEvents pnRedirect As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnForward As DevComponents.DotNetBar.ButtonX
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents chkSSL As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents Label12 As DevComponents.DotNetBar.LabelX
    Friend WithEvents txtPort As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmbServerType As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As DevComponents.DotNetBar.LabelX
    Friend WithEvents Page1a As System.Windows.Forms.GroupBox
    Friend WithEvents txtIMAPPath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents tvIMAP As System.Windows.Forms.TreeView
    Friend WithEvents Label14 As DevComponents.DotNetBar.LabelX
    Friend WithEvents chkSaveAttach As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents pnAttach As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnSaveAttach As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtAttach As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents chkMatchExact As DevComponents.DotNetBar.Controls.CheckBoxX
    Friend WithEvents tippy As System.Windows.Forms.ToolTip
End Class
