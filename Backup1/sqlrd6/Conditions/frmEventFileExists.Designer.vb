<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEventFileExists
    Inherits DevComponents.DotNetBar.Office2007Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.stabFiles = New DevComponents.DotNetBar.SuperTabControl()
        Me.SuperTabControlPanel2 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.ucFTP = New sqlrd.ucFTPDetails()
        Me.SuperTabItem2 = New DevComponents.DotNetBar.SuperTabItem()
        Me.SuperTabControlPanel1 = New DevComponents.DotNetBar.SuperTabControlPanel()
        Me.txtFilePath = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.cmdBrowse = New DevComponents.DotNetBar.ButtonX()
        Me.SuperTabItem1 = New DevComponents.DotNetBar.SuperTabItem()
        Me.btnOK = New DevComponents.DotNetBar.ButtonX()
        Me.btnCancel = New DevComponents.DotNetBar.ButtonX()
        Me.txtName = New DevComponents.DotNetBar.Controls.TextBoxX()
        Me.Label3 = New DevComponents.DotNetBar.LabelX()
        Me.cmbValue = New System.Windows.Forms.ComboBox()
        Me.Label4 = New DevComponents.DotNetBar.LabelX()
        Me.mnuInserter = New System.Windows.Forms.ContextMenu()
        Me.mnuUndo = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.mnuCut = New System.Windows.Forms.MenuItem()
        Me.mnuCopy = New System.Windows.Forms.MenuItem()
        Me.mnuPaste = New System.Windows.Forms.MenuItem()
        Me.mnuDelete = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.optRunForEachFile = New System.Windows.Forms.RadioButton()
        Me.optRunOnce = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        CType(Me.stabFiles, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stabFiles.SuspendLayout()
        Me.SuperTabControlPanel2.SuspendLayout()
        Me.SuperTabControlPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.stabFiles)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 60)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(576, 254)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "File Details"
        '
        'stabFiles
        '
        '
        '
        '
        '
        '
        '
        Me.stabFiles.ControlBox.CloseBox.Name = ""
        '
        '
        '
        Me.stabFiles.ControlBox.MenuBox.Name = ""
        Me.stabFiles.ControlBox.Name = ""
        Me.stabFiles.ControlBox.SubItems.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.stabFiles.ControlBox.MenuBox, Me.stabFiles.ControlBox.CloseBox})
        Me.stabFiles.Controls.Add(Me.SuperTabControlPanel1)
        Me.stabFiles.Controls.Add(Me.SuperTabControlPanel2)
        Me.stabFiles.Location = New System.Drawing.Point(6, 20)
        Me.stabFiles.Name = "stabFiles"
        Me.stabFiles.ReorderTabsEnabled = True
        Me.stabFiles.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.stabFiles.SelectedTabIndex = 1
        Me.stabFiles.Size = New System.Drawing.Size(564, 228)
        Me.stabFiles.TabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stabFiles.TabIndex = 2
        Me.stabFiles.Tabs.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SuperTabItem1, Me.SuperTabItem2})
        Me.stabFiles.TabStyle = DevComponents.DotNetBar.eSuperTabStyle.Office2010BackstageBlue
        '
        'SuperTabControlPanel2
        '
        Me.SuperTabControlPanel2.Controls.Add(Me.ucFTP)
        Me.SuperTabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel2.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel2.Name = "SuperTabControlPanel2"
        Me.SuperTabControlPanel2.Size = New System.Drawing.Size(564, 204)
        Me.SuperTabControlPanel2.TabIndex = 0
        Me.SuperTabControlPanel2.TabItem = Me.SuperTabItem2
        '
        'ucFTP
        '
        Me.ucFTP.BackColor = System.Drawing.SystemColors.Control
        Me.ucFTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFTP.Location = New System.Drawing.Point(3, 3)
        Me.ucFTP.m_browseFTPForFile = False
        Me.ucFTP.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
    "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;AsciiMode=0;"
        Me.ucFTP.m_ftpSuccess = False
        Me.ucFTP.m_ShowBrowse = True
        Me.ucFTP.Name = "ucFTP"
        Me.ucFTP.Size = New System.Drawing.Size(553, 197)
        Me.ucFTP.TabIndex = 1
        '
        'SuperTabItem2
        '
        Me.SuperTabItem2.AttachedControl = Me.SuperTabControlPanel2
        Me.SuperTabItem2.GlobalItem = False
        Me.SuperTabItem2.Name = "SuperTabItem2"
        Me.SuperTabItem2.Text = "Remote (FTP) File"
        '
        'SuperTabControlPanel1
        '
        Me.SuperTabControlPanel1.Controls.Add(Me.optRunForEachFile)
        Me.SuperTabControlPanel1.Controls.Add(Me.optRunOnce)
        Me.SuperTabControlPanel1.Controls.Add(Me.txtFilePath)
        Me.SuperTabControlPanel1.Controls.Add(Me.cmdBrowse)
        Me.SuperTabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SuperTabControlPanel1.Location = New System.Drawing.Point(0, 24)
        Me.SuperTabControlPanel1.Name = "SuperTabControlPanel1"
        Me.SuperTabControlPanel1.Size = New System.Drawing.Size(564, 204)
        Me.SuperTabControlPanel1.TabIndex = 1
        Me.SuperTabControlPanel1.TabItem = Me.SuperTabItem1
        '
        'txtFilePath
        '
        '
        '
        '
        Me.txtFilePath.Border.Class = "TextBoxBorder"
        Me.txtFilePath.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtFilePath.Location = New System.Drawing.Point(3, 13)
        Me.txtFilePath.Name = "txtFilePath"
        Me.txtFilePath.Size = New System.Drawing.Size(341, 21)
        Me.txtFilePath.TabIndex = 0
        '
        'cmdBrowse
        '
        Me.cmdBrowse.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.cmdBrowse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBrowse.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(362, 12)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(56, 21)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "..."
        '
        'SuperTabItem1
        '
        Me.SuperTabItem1.AttachedControl = Me.SuperTabControlPanel1
        Me.SuperTabItem1.GlobalItem = False
        Me.SuperTabItem1.Name = "SuperTabItem1"
        Me.SuperTabItem1.Text = "Local File"
        '
        'btnOK
        '
        Me.btnOK.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnOK.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnOK.Location = New System.Drawing.Point(590, 60)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 3
        Me.btnOK.Text = "&OK"
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton
        Me.btnCancel.ColorTable = DevComponents.DotNetBar.eButtonColor.Office2007WithBackground
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCancel.Location = New System.Drawing.Point(590, 91)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 4
        Me.btnCancel.Text = "&Cancel"
        '
        'txtName
        '
        '
        '
        '
        Me.txtName.Border.Class = "TextBoxBorder"
        Me.txtName.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.txtName.Location = New System.Drawing.Point(147, 6)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(208, 21)
        Me.txtName.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        '
        '
        '
        Me.Label3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label3.Location = New System.Drawing.Point(5, 9)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 16)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Name"
        '
        'cmbValue
        '
        Me.cmbValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValue.FormattingEnabled = True
        Me.cmbValue.Items.AddRange(New Object() {"TRUE", "FALSE"})
        Me.cmbValue.Location = New System.Drawing.Point(147, 34)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(208, 21)
        Me.cmbValue.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        '
        '
        '
        Me.Label4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square
        Me.Label4.Location = New System.Drawing.Point(5, 38)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 16)
        Me.Label4.TabIndex = 33
        Me.Label4.Text = "If file exists"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'optRunForEachFile
        '
        Me.optRunForEachFile.AutoSize = True
        Me.optRunForEachFile.BackColor = System.Drawing.Color.Transparent
        Me.optRunForEachFile.Location = New System.Drawing.Point(3, 63)
        Me.optRunForEachFile.Name = "optRunForEachFile"
        Me.optRunForEachFile.Size = New System.Drawing.Size(135, 17)
        Me.optRunForEachFile.TabIndex = 3
        Me.optRunForEachFile.Text = "Run for each file found"
        Me.optRunForEachFile.UseVisualStyleBackColor = False
        '
        'optRunOnce
        '
        Me.optRunOnce.AutoSize = True
        Me.optRunOnce.BackColor = System.Drawing.Color.Transparent
        Me.optRunOnce.Checked = True
        Me.optRunOnce.Location = New System.Drawing.Point(3, 40)
        Me.optRunOnce.Name = "optRunOnce"
        Me.optRunOnce.Size = New System.Drawing.Size(153, 17)
        Me.optRunOnce.TabIndex = 4
        Me.optRunOnce.TabStop = True
        Me.optRunOnce.Text = "Run once for all files found"
        Me.optRunOnce.UseVisualStyleBackColor = False
        '
        'frmEventFileExists
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.ClientSize = New System.Drawing.Size(667, 318)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmbValue)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.btnOK)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmEventFileExists"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Condition - File Exists"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.stabFiles, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stabFiles.ResumeLayout(False)
        Me.SuperTabControlPanel2.ResumeLayout(False)
        Me.SuperTabControlPanel1.ResumeLayout(False)
        Me.SuperTabControlPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFilePath As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents cmdBrowse As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnOK As DevComponents.DotNetBar.ButtonX
    Friend WithEvents btnCancel As DevComponents.DotNetBar.ButtonX
    Friend WithEvents txtName As DevComponents.DotNetBar.Controls.TextBoxX
    Friend WithEvents Label3 As DevComponents.DotNetBar.LabelX
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As DevComponents.DotNetBar.LabelX
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents stabFiles As DevComponents.DotNetBar.SuperTabControl
    Friend WithEvents SuperTabControlPanel2 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem2 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents SuperTabControlPanel1 As DevComponents.DotNetBar.SuperTabControlPanel
    Friend WithEvents SuperTabItem1 As DevComponents.DotNetBar.SuperTabItem
    Friend WithEvents ucFTP As sqlrd.ucFTPDetails
    Friend WithEvents optRunForEachFile As System.Windows.Forms.RadioButton
    Friend WithEvents optRunOnce As System.Windows.Forms.RadioButton
End Class
