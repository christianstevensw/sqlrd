Imports System.Windows.Forms


Public Class frmUpdateAvail

    Dim result As DialogResult

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        result = Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        result = System.Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub frmUpdateAvail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Function DoshowDialog(ByVal newBuild As String, ByVal currentBuild As String) As DialogResult
        Me.chkOff.Checked = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("CheckForUpdates", 1))
        Me.txtCurrent.Text = currentBuild
        Me.txtNew.Text = newBuild
        Me.Text = Application.ProductName
        Me.ShowDialog()

        Return result
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.result = Windows.Forms.DialogResult.Ignore
        Close()
    End Sub

    Private Sub chkOff_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOff.CheckedChanged
        clsMarsUI.MainUI.SaveRegistry("CheckForUpdates", Convert.ToInt16(Me.chkOff.Checked), , , True)
    End Sub
End Class
