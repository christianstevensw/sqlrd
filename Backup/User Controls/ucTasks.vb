Public Class ucTasks
    Inherits System.Windows.Forms.UserControl
    Dim oTask As clsMarsTask = New clsMarsTask
    Public ScheduleID As Integer = 99999
    Public ShowAfterType As Boolean = True
    Public oAuto As Boolean = False
    Dim eventBased As Boolean = False
    Friend WithEvents mnuType As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DefaultTaskToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ImportTaskListToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim eventID As Integer = 99999
    Dim defaultTasks As Boolean = False
    Dim loaded As Boolean = False
    Dim DeletedItems As ArrayList

    Public ReadOnly Property m_DeletedItems() As ArrayList
        Get
            Return Me.DeletedItems
        End Get
    End Property

    Public Property m_defaultTaks() As Boolean
        Get
            Return defaultTasks
        End Get
        Set(ByVal value As Boolean)
            defaultTasks = value
        End Set
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    ''' <summary>
    ''' hide or show import and export buttons
    ''' </summary>
    ''' <value></value>
    ''' <remarks></remarks>
    Public WriteOnly Property m_ShowImportExport() As Boolean
        Set(ByVal value As Boolean)
            cmdLoad.Visible = value
            cmdSave.Visible = value
        End Set
    End Property

    ''' <summary>
    ''' returns number of selected items
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_SelTasks() As Integer
        Get
            Return Me.lsvTasks.SelectedItems.Count
        End Get
    End Property
    ''' <summary>
    ''' return the selected items collection
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property m_SelItems() As ListView.SelectedListViewItemCollection
        Get
            Return lsvTasks.SelectedItems
        End Get
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdDown As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdUp As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvTasks As System.Windows.Forms.ListView
    Friend WithEvents imgTasks As System.Windows.Forms.ImageList
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdLoad As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucTasks))
        Me.lsvTasks = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.imgTasks = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdLoad = New System.Windows.Forms.Button
        Me.mnuType = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DefaultTaskToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.ImportTaskListToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdDown = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdUp = New System.Windows.Forms.Button
        Me.mnuType.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvTasks
        '
        Me.lsvTasks.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvTasks.CheckBoxes = True
        Me.lsvTasks.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvTasks.FullRowSelect = True
        Me.lsvTasks.HideSelection = False
        Me.lsvTasks.Location = New System.Drawing.Point(8, 8)
        Me.lsvTasks.Name = "lsvTasks"
        Me.lsvTasks.Size = New System.Drawing.Size(408, 288)
        Me.lsvTasks.SmallImageList = Me.imgTasks
        Me.lsvTasks.TabIndex = 3
        Me.lsvTasks.UseCompatibleStateImageBehavior = False
        Me.lsvTasks.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Task List"
        Me.ColumnHeader1.Width = 193
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Run"
        Me.ColumnHeader2.Width = 85
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "After Type"
        Me.ColumnHeader3.Width = 89
        '
        'imgTasks
        '
        Me.imgTasks.ImageStream = CType(resources.GetObject("imgTasks.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTasks.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTasks.Images.SetKeyName(0, "")
        Me.imgTasks.Images.SetKeyName(1, "")
        Me.imgTasks.Images.SetKeyName(2, "")
        Me.imgTasks.Images.SetKeyName(3, "")
        Me.imgTasks.Images.SetKeyName(4, "")
        Me.imgTasks.Images.SetKeyName(5, "")
        Me.imgTasks.Images.SetKeyName(6, "")
        Me.imgTasks.Images.SetKeyName(7, "")
        Me.imgTasks.Images.SetKeyName(8, "")
        Me.imgTasks.Images.SetKeyName(9, "")
        Me.imgTasks.Images.SetKeyName(10, "")
        Me.imgTasks.Images.SetKeyName(11, "")
        Me.imgTasks.Images.SetKeyName(12, "")
        Me.imgTasks.Images.SetKeyName(13, "")
        Me.imgTasks.Images.SetKeyName(14, "")
        Me.imgTasks.Images.SetKeyName(15, "")
        Me.imgTasks.Images.SetKeyName(16, "")
        Me.imgTasks.Images.SetKeyName(17, "")
        Me.imgTasks.Images.SetKeyName(18, "")
        Me.imgTasks.Images.SetKeyName(19, "")
        Me.imgTasks.Images.SetKeyName(20, "")
        Me.imgTasks.Images.SetKeyName(21, "")
        Me.imgTasks.Images.SetKeyName(22, "")
        Me.imgTasks.Images.SetKeyName(23, "")
        Me.imgTasks.Images.SetKeyName(24, "")
        Me.imgTasks.Images.SetKeyName(25, "")
        Me.imgTasks.Images.SetKeyName(26, "")
        Me.imgTasks.Images.SetKeyName(27, "")
        Me.imgTasks.Images.SetKeyName(28, "")
        Me.imgTasks.Images.SetKeyName(29, "")
        Me.imgTasks.Images.SetKeyName(30, "")
        Me.imgTasks.Images.SetKeyName(31, "")
        Me.imgTasks.Images.SetKeyName(32, "")
        Me.imgTasks.Images.SetKeyName(33, "")
        Me.imgTasks.Images.SetKeyName(34, "")
        Me.imgTasks.Images.SetKeyName(35, "")
        Me.imgTasks.Images.SetKeyName(36, "")
        Me.imgTasks.Images.SetKeyName(37, "")
        Me.imgTasks.Images.SetKeyName(38, "")
        Me.imgTasks.Images.SetKeyName(39, "")
        Me.imgTasks.Images.SetKeyName(40, "")
        Me.imgTasks.Images.SetKeyName(41, "")
        Me.imgTasks.Images.SetKeyName(42, "")
        Me.imgTasks.Images.SetKeyName(43, "")
        Me.imgTasks.Images.SetKeyName(44, "mda.png")
        '
        'cmdSave
        '
        Me.cmdSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSave.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSave.Location = New System.Drawing.Point(392, 304)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(24, 23)
        Me.cmdSave.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cmdSave, "Export task list")
        '
        'cmdLoad
        '
        Me.cmdLoad.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdLoad.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoad.Image = CType(resources.GetObject("cmdLoad.Image"), System.Drawing.Image)
        Me.cmdLoad.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoad.Location = New System.Drawing.Point(360, 304)
        Me.cmdLoad.Name = "cmdLoad"
        Me.cmdLoad.Size = New System.Drawing.Size(24, 23)
        Me.cmdLoad.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cmdLoad, "Import task list")
        '
        'mnuType
        '
        Me.mnuType.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DefaultTaskToolStripMenuItem, Me.ToolStripMenuItem1, Me.ImportTaskListToolStripMenuItem})
        Me.mnuType.Name = "mnuType"
        Me.mnuType.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuType.Size = New System.Drawing.Size(159, 54)
        '
        'DefaultTaskToolStripMenuItem
        '
        Me.DefaultTaskToolStripMenuItem.Name = "DefaultTaskToolStripMenuItem"
        Me.DefaultTaskToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.DefaultTaskToolStripMenuItem.Text = "Default Task"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(155, 6)
        '
        'ImportTaskListToolStripMenuItem
        '
        Me.ImportTaskListToolStripMenuItem.Name = "ImportTaskListToolStripMenuItem"
        Me.ImportTaskListToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ImportTaskListToolStripMenuItem.Text = "Import Task List"
        '
        'cmdDelete
        '
        Me.cmdDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(224, 304)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(64, 23)
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdDown
        '
        Me.cmdDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(12, 304)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(24, 23)
        Me.cmdDown.TabIndex = 6
        '
        'cmdEdit
        '
        Me.cmdEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(152, 304)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(64, 23)
        Me.cmdEdit.TabIndex = 5
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdAdd
        '
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(80, 304)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(64, 23)
        Me.cmdAdd.TabIndex = 4
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdUp
        '
        Me.cmdUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(44, 304)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(24, 23)
        Me.cmdUp.TabIndex = 6
        '
        'ucTasks
        '
        Me.BackColor = System.Drawing.Color.White
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdDown)
        Me.Controls.Add(Me.cmdEdit)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdUp)
        Me.Controls.Add(Me.lsvTasks)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.cmdLoad)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Name = "ucTasks"
        Me.Size = New System.Drawing.Size(424, 336)
        Me.mnuType.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
       

        Dim oTasks As frmTaskSelection = New frmTaskSelection

        oTasks.ShowAfter = ShowAfterType
        oTasks.ScheduleID = ScheduleID
        oTasks.oAuto = oAuto
        oTasks.m_eventBased = Me.m_eventBased
        oTasks.m_eventID = Me.m_eventID

        oTasks.ShowDialog()

        LoadTasks()
    End Sub

    Public Sub AddTasks()
        Dim oTasks As frmTaskSelection = New frmTaskSelection

        oTasks.ScheduleID = ScheduleID
        oTasks.oAuto = oAuto
        oTasks.ShowDialog()

        LoadTasks()
    End Sub
    Public Sub LoadTasks()
        Try
10:         Me.loaded = False

            Dim SQL As String
20:         Dim oData As clsMarsData = New clsMarsData
            Dim sWhere As String = " WHERE ScheduleID = " & ScheduleID
            Dim sOrder As String = " ORDER BY OrderID"
            Dim lsv As ListViewItem
            Dim sTaskType As String
            Dim nTaskID As Integer
            Dim selected As ListViewItem

            If lsvTasks.SelectedItems.Count > 0 Then selected = lsvTasks.SelectedItems(0)

30:         If lsvTasks.Items.Count > 0 Then lsvTasks.Items.Clear()

40:         SQL = "SELECT * FROM Tasks " & sWhere & sOrder

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

50:         If Not oRs Is Nothing Then
60:             Do While oRs.EOF = False
70:                 lsv = New ListViewItem

80:                 sTaskType = IsNull(oRs("tasktype").Value)

90:                 nTaskID = oRs("taskid").Value

                    If Me.DeletedItems IsNot Nothing Then
                        If Me.DeletedItems.Contains(nTaskID) Then GoTo skip
                    End If

                    lsv.Tag = sTaskType & ":" & nTaskID

100:                lsv.Text = oRs("taskname").Value

110:                Try
120:                    lsv.Checked = IsNull(oRs("enabledstatus").Value, 1)
130:                Catch ex As Exception
140:                    lsv.Checked = True
                    End Try

150:                Select Case sTaskType.ToLower
                        Case "copyfile"
160:                        lsv.ImageIndex = 5
170:                    Case "deletefile"
180:                        lsv.ImageIndex = 7
190:                    Case "sendmail"
200:                        lsv.ImageIndex = 13
210:                    Case "createfolder"
220:                        lsv.ImageIndex = 9
230:                    Case "movefolder"
240:                        lsv.ImageIndex = 11
250:                    Case "pause"
260:                        lsv.ImageIndex = 14
270:                    Case "print"
280:                        lsv.ImageIndex = 2
290:                    Case "program"
300:                        lsv.ImageIndex = 1
310:                    Case "renamefile"
320:                        lsv.ImageIndex = 6
330:                    Case "webbrowse"
340:                        lsv.ImageIndex = 15
350:                    Case "writefile"
360:                        lsv.ImageIndex = 8
370:                    Case "sqlscript"
380:                        lsv.ImageIndex = 22
390:                    Case "sqlproc"
400:                        lsv.ImageIndex = 21
410:                    Case "dbupdate"
420:                        lsv.ImageIndex = 35
430:                    Case "dbinsert"
440:                        lsv.ImageIndex = 33
450:                    Case "dbdelete"
460:                        lsv.ImageIndex = 34
470:                    Case "dbcreatetable"
480:                        lsv.ImageIndex = 23
490:                    Case "dbdeletetable"
500:                        lsv.ImageIndex = 24
510:                    Case "dbmodifytable_add"
520:                        lsv.ImageIndex = 25
530:                    Case "dbmodifytable_drop"
540:                        lsv.ImageIndex = 26
550:                    Case "ftpupload"
560:                        lsv.ImageIndex = 29
570:                    Case "ftpdownload"
580:                        lsv.ImageIndex = 30
590:                    Case "ftpdeletefile", "ftpdeletedirectory"
600:                        lsv.ImageIndex = 31
610:                    Case "ftpcreatedirectory"
620:                        lsv.ImageIndex = 32
630:                    Case "registrycreatekey"
640:                        lsv.ImageIndex = 39
650:                    Case "registrydeletekey"
660:                        lsv.ImageIndex = 38
670:                    Case "registrysetkey"
680:                        lsv.ImageIndex = 37
690:                    Case "runschedule"
700:                        lsv.ImageIndex = 40
710:                    Case "zipfiles"
720:                        lsv.ImageIndex = 41
730:                    Case "unzipfile"
740:                        lsv.ImageIndex = 42
750:                    Case "mergepdf"
760:                        lsv.ImageIndex = 43
                        Case "sendsms"
                            lsv.ImageIndex = 44
                        Case "dbexport"
                            lsv.ImageIndex = 30
                    End Select

770:                With lsv.SubItems
780:                    .Add(IsNull(oRs("runwhen").Value))
790:                    .Add(IsNull(oRs("aftertype").Value))
                    End With

800:                lsvTasks.Items.Add(lsv)
skip:
810:                oRs.MoveNext()
820:            Loop

830:            oRs.Close()

                If selected IsNot Nothing Then
                    For Each it As ListViewItem In Me.lsvTasks.Items
                        If it.Tag = selected.Tag Then
                            it.Selected = True
                            Exit For
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
840:        Me.loaded = True
        End Try
    End Sub


    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdEdit.Click, lsvTasks.DoubleClick
        If lsvTasks.SelectedItems.Count = 0 Then Exit Sub

        Dim sTaskType As String
        Dim nTaskID As Integer
        Dim lsv As ListViewItem

        lsv = lsvTasks.SelectedItems(0)

        sTaskType = GetDelimitedWord(lsv.Tag, 1, ":")

        nTaskID = GetDelimitedWord(lsv.Tag, 2, ":")

        Select Case sTaskType.ToLower
            Case "copyfile"
                Dim oForm As New frmTaskCopyFile

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "deletefile"
                Dim oForm As New frmTaskDeleteFile

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sendmail"
                Dim oForm As New frmTaskEmail

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "createfolder"
                Dim oForm As New frmTaskFolderCreate

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "movefolder"
                Dim oForm As New frmTaskFolderMove

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "pause"
                Dim oForm As New frmTaskPause

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "print"
                Dim oForm As New frmTaskPrint

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "program"
                Dim oForm As New frmTaskProgram

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "renamefile"
                Dim oForm As New frmTaskRenameFile

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "webbrowse"
                Dim oForm As New frmTaskWebBrowse

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "writefile"
                Dim oForm As New frmTaskWriteFile

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sqlscript"
                Dim oForm As New frmTaskSQLScript

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sqlproc"
                Dim oForm As New frmTaskSQLProc

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbupdate"
                Dim oForm As New frmTaskDBUpdate

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbinsert"
                Dim oForm As New frmTaskDBInsert

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbdelete"
                Dim oForm As New frmTaskDBDelete

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbcreatetable"
                Dim oForm As New frmTaskTableCreate

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbdeletetable"
                Dim oForm As New frmTaskTableDelete

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbmodifytable_add", "dbmodifytable_drop"
                Dim oForm As New frmTaskTableModify

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpupload"
                Dim oForm As New frmTaskFTPUpload

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdownload"
                Dim oForm As New frmTaskFTPDownload

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdeletefile"
                Dim oForm As New frmTaskFTPDeleteFile

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpcreatedirectory"
                Dim oForm As New frmTaskFTPCreateDirectory

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "ftpdeletedirectory"
                Dim oForm As New frmTaskFTPDeleteDirectory

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrycreatekey"
                Dim oForm As New frmTaskAddKey

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrydeletekey"
                Dim oForm As New frmTaskDeleteKey

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "registrysetkey"
                Dim oForm As New frmTaskSetKey

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "runschedule"
                Dim oForm As New frmTaskRunSchedule

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "zipfiles"
                Dim oForm As New frmTaskZip

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "unzipfile"
                Dim oForm As New frmTaskUnzip

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "mergepdf"
                Dim oForm As New frmTaskPDFMerge

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "sendsms"
                Dim oForm As New frmTaskSMS

                oForm.EditTask(nTaskID, ShowAfterType)
            Case "dbexport"
                Dim task As frmTaskSQLExport = New frmTaskSQLExport

                task.EditTask(nTaskID, ShowAfterType)
        End Select

        LoadTasks()
    End Sub

    Private Sub ucTasks_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub CommitDeletions()
        If Me.DeletedItems IsNot Nothing Then
            For Each n As Integer In Me.DeletedItems
                oTask.DeleteTask(n)
            Next
        End If
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If Me.DeletedItems Is Nothing Then
            Me.DeletedItems = New ArrayList
        End If

        Dim nTaskID As Integer
        Dim lsv As ListViewItem

        If lsvTasks.SelectedItems.Count = 0 Then Exit Sub

        If MessageBox.Show("Delete the selected task(s)?", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then

            For Each lsv In lsvTasks.SelectedItems

                nTaskID = GetDelimitedWord(lsv.Tag, 2, ":")

                Me.DeletedItems.Add(nTaskID)

                lsv.Remove()
            Next
        End If
    End Sub

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvTasks.SelectedItems.Count = 0 Then Exit Sub

        Dim nTaskID As Integer
        Dim lsv As ListViewItem

        lsv = lsvTasks.SelectedItems(0)

        Dim selTag As String = lsv.Tag

        nTaskID = GetDelimitedWord(lsv.Tag, 2, ":")

        oTask.MoveTask(ScheduleID, nTaskID, clsMarsTask.TaskDirection.Up)

        LoadTasks()

        For Each item As ListViewItem In lsvTasks.Items
            If item.Tag = selTag Then
                item.Selected = True
            End If
        Next
    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvTasks.SelectedItems.Count = 0 Then Exit Sub

        Dim nTaskID As Integer
        Dim lsv As ListViewItem

        lsv = lsvTasks.SelectedItems(0)

        Dim selTag As String = lsv.Tag

        nTaskID = GetDelimitedWord(lsv.Tag, 2, ":")

        oTask.MoveTask(ScheduleID, nTaskID, clsMarsTask.TaskDirection.Down)

        LoadTasks()

        For Each item As ListViewItem In lsvTasks.Items
            If item.Tag = selTag Then
                item.Selected = True
            End If
        Next
    End Sub

    Private Sub lsvTasks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvTasks.SelectedIndexChanged

    End Sub

    Private Sub lsvTasks_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvTasks.DoubleClick
        Dim lsv As ListView = CType(sender, ListView)
        Dim Item As ListViewItem

        If lsv IsNot Nothing Then

            Item = lsv.SelectedItems(0)

            If Item IsNot Nothing Then
                If Item.Checked Then
                    Item.Checked = False
                Else
                    Item.Checked = True
                End If
            End If
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM Tasks WHERE " & _
        "ScheduleID = " & ScheduleID & " ORDER BY OrderID"

        Dim oData As New clsMarsData

        oRs = clsmarsdata.GetData(SQL)

        Try
            If oRs.EOF = False Then

                Dim sPath As String = sAppPath & "\Tasks\"

                If System.IO.Directory.Exists(sPath) = False Then
                    System.IO.Directory.CreateDirectory(sPath)
                End If

                Dim sName As String = InputBox("Please enter the name of the task list", Application.ProductName)

                If sName.Length = 0 Then Return

                sPath &= sName

                If sPath.EndsWith(".xml") = False Then
                    sPath &= ".xml"
                End If

                If System.IO.File.Exists(sPath) = True Then
                    If MessageBox.Show("A task list with that name already " & _
                    "exists. Ok to overwrite?", Application.ProductName, _
                    MessageBoxButtons.YesNo, _
                    MessageBoxIcon.Question) = DialogResult.No Then
                        Return
                    End If
                End If

                oData.CreateXML(oRs, sPath)

                MessageBox.Show("Task list saved successfully")
            End If
        Catch ex As Exception
            _ErrorHandle("Failed to export the task list (" & ex.Message & ")", _
            Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub cmdLoad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoad.Click
        
    End Sub

    Private Sub ImportTaskListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportTaskListToolStripMenuItem.Click
        Me.CommitDeletions()

        Dim oForm As New frmTaskManager
        Dim sReturn As String

        sReturn = oForm._GetTaskListPath

        If sReturn.Length = 0 Then Return

        Dim oData As New clsMarsData

        Dim oRs As ADODB.Recordset = oData.GetXML(sReturn)

        If oRs Is Nothing Then Return

        Dim sCols As String
        Dim sVals As String
        Dim I As Integer
        Dim oTask As New clsMarsTask
        Dim SQL As String
        Dim nPort As Integer = 0
        Dim nReplace As Integer = 0
        Dim nPause As Integer = 0
        Dim runWhen As String = "AFTER"
        Dim afterType As String = "Delivery"
        Dim status As Integer = 1

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath," & _
        "ProgramParameters,WindowStyle,PrinterName,FileList,PauseInt," & _
        "ReplaceFiles,Msg,SendTo,Cc,Bcc,Subject,OrderID,FTPServer,FtpPort," & _
        "FTPUser,FTPPassword,FTPDirectory,RunWhen,AfterType,EnabledStatus"

        Do While oRs.EOF = False
            Try
                nPort = oRs("ftpport").Value
            Catch ex As Exception
                nPort = 0
            End Try

            Try
                nReplace = oRs("replacefiles").Value
            Catch ex As Exception
                nReplace = 0
            End Try

            Try
                nPause = oRs("pauseint").Value
            Catch ex As Exception
                nPause = 0
            End Try

            Try
                status = oRs("enabledstatus").Value
            Catch ex As Exception
                status = 1
            End Try

            Try
                afterType = oRs("aftertype").Value
            Catch :End Try

            Try
                runWhen = oRs("runwhen").Value
            Catch : End Try

            sVals = clsMarsData.CreateDataID("tasks", "taskid") & "," & _
            ScheduleID & "," & _
            "'" & SQLPrepare(oRs("tasktype").Value) & "'," & _
            "'" & SQLPrepare(oRs("taskname").Value) & "'," & _
            "'" & SQLPrepare(oRs("programpath").Value) & "'," & _
            "'" & SQLPrepare(oRs("programparameters").Value) & "'," & _
            "'" & SQLPrepare(oRs("windowstyle").Value) & "'," & _
            "'" & SQLPrepare(oRs("printername").Value) & "'," & _
            "'" & SQLPrepare(oRs("filelist").Value) & "'," & _
            nPause & "," & _
            nReplace & "," & _
            "'" & SQLPrepare(oRs("msg").Value) & "'," & _
            "'" & SQLPrepare(oRs("sendto").Value) & "'," & _
            "'" & SQLPrepare(oRs("cc").Value) & "'," & _
            "'" & SQLPrepare(oRs("bcc").Value) & "'," & _
            "'" & SQLPrepare(oRs("subject").Value) & "'," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            "'" & SQLPrepare(oRs("ftpserver").Value) & "'," & _
            nPort & "," & _
            "'" & SQLPrepare(oRs("ftpuser").Value) & "'," & _
            "'" & SQLPrepare(oRs("ftppassword").Value) & "'," & _
            "'" & SQLPrepare(oRs("ftpdirectory").Value) & "'," & _
            "'" & runWhen & "'," & _
            "'" & afterType & "'," & _
            status

            SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

            'console.writeLine(SQL)

            If clsMarsData.WriteData(SQL) = False Then
                Exit Sub
            End If

            oRs.MoveNext()
        Loop

        oRs.Close()

        LoadTasks()
    End Sub

    Private Sub cmdLoad_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdLoad.MouseUp
        mnuType.Show(cmdLoad, New Point(e.X, e.Y))
    End Sub

    Private Sub DefaultTaskToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DefaultTaskToolStripMenuItem.Click
        Me.CommitDeletions()

        Dim odefault As New frmDefaultTasks

        Dim items As ListView.SelectedListViewItemCollection

        items = odefault.GetDefaultTask

        If items IsNot Nothing Then
            For Each item As ListViewItem In items
                Dim taskID As Integer = item.Tag.split(":")(1)

                clsMarsData.DataItem.CopyRow("Tasks", taskID, " WHERE TaskID =" & taskID, "TaskID", _
                , , , , , , ScheduleID)

            Next

            LoadTasks()
        End If
    End Sub

    Private Sub lsvTasks_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvTasks.KeyDown
        If e.KeyCode = Keys.Delete Then
            Me.cmdDelete_Click(sender, e)
        End If
    End Sub

    Private Sub lsvTasks_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvTasks.ItemChecked
        '        If loaded = False Then Return

        Dim it As ListViewItem = e.Item

        Dim taskID As Integer = it.Tag.ToString.Split(":")(1)

        Dim SQL As String = "UPDATE Tasks SET EnabledStatus =" & Convert.ToInt32(it.Checked) & _
        " WHERE TaskID =" & taskID

        clsMarsData.WriteData(SQL)

    End Sub
End Class
