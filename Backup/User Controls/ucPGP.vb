Public Class ucPGP
    Inherits System.Windows.Forms.UserControl
    Dim oData As New clsMarsData

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDbLoc As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtKeyLocation As System.Windows.Forms.TextBox
    Friend WithEvents tip As System.Windows.Forms.ToolTip
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPGPUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdSec As System.Windows.Forms.Button
    Friend WithEvents txtSecring As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkPGPExt As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ucPGP))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdDbLoc = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtKeyLocation = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtPGPUserID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtSecring = New System.Windows.Forms.TextBox
        Me.cmdSec = New System.Windows.Forms.Button
        Me.tip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.chkPGPExt = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkPGPExt)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmdDbLoc)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtKeyLocation)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtPGPUserID)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtSecring)
        Me.GroupBox1.Controls.Add(Me.cmdSec)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 256)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(3, 237)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(402, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Tag = "red"
        Me.Label3.Text = "*PGP functionality powered by PGP (www.pgp.com)"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDbLoc.Image = CType(resources.GetObject("cmdDbLoc.Image"), System.Drawing.Image)
        Me.cmdDbLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(360, 88)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.cmdDbLoc.Size = New System.Drawing.Size(40, 21)
        Me.cmdDbLoc.TabIndex = 2
        Me.cmdDbLoc.Text = "..."
        Me.cmdDbLoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 72)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Public Keyring file"
        '
        'txtKeyLocation
        '
        Me.txtKeyLocation.BackColor = System.Drawing.Color.White
        Me.txtKeyLocation.Location = New System.Drawing.Point(8, 88)
        Me.txtKeyLocation.Name = "txtKeyLocation"
        Me.txtKeyLocation.ReadOnly = True
        Me.txtKeyLocation.Size = New System.Drawing.Size(328, 21)
        Me.txtKeyLocation.TabIndex = 1
        Me.txtKeyLocation.Tag = "memo"
        Me.txtKeyLocation.Text = ""
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "PGP UserID"
        '
        'txtPGPUserID
        '
        Me.txtPGPUserID.BackColor = System.Drawing.Color.White
        Me.txtPGPUserID.Location = New System.Drawing.Point(8, 40)
        Me.txtPGPUserID.Name = "txtPGPUserID"
        Me.txtPGPUserID.Size = New System.Drawing.Size(184, 21)
        Me.txtPGPUserID.TabIndex = 0
        Me.txtPGPUserID.Tag = "memo"
        Me.txtPGPUserID.Text = ""
        Me.tip.SetToolTip(Me.txtPGPUserID, "Your PGP Key UserID")
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 120)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Private Keyring file"
        '
        'txtSecring
        '
        Me.txtSecring.BackColor = System.Drawing.Color.White
        Me.txtSecring.Location = New System.Drawing.Point(8, 136)
        Me.txtSecring.Name = "txtSecring"
        Me.txtSecring.ReadOnly = True
        Me.txtSecring.Size = New System.Drawing.Size(328, 21)
        Me.txtSecring.TabIndex = 3
        Me.txtSecring.Tag = "memo"
        Me.txtSecring.Text = ""
        '
        'cmdSec
        '
        Me.cmdSec.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSec.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSec.Image = CType(resources.GetObject("cmdSec.Image"), System.Drawing.Image)
        Me.cmdSec.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSec.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSec.Location = New System.Drawing.Point(360, 136)
        Me.cmdSec.Name = "cmdSec"
        Me.cmdSec.Size = New System.Drawing.Size(40, 21)
        Me.cmdSec.TabIndex = 4
        Me.cmdSec.Text = "..."
        Me.cmdSec.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'chkPGPExt
        '
        Me.chkPGPExt.Location = New System.Drawing.Point(8, 176)
        Me.chkPGPExt.Name = "chkPGPExt"
        Me.chkPGPExt.Size = New System.Drawing.Size(328, 24)
        Me.chkPGPExt.TabIndex = 6
        Me.chkPGPExt.Text = "Append '.pgp' extension to resulting filename"
        '
        'ucPGP
        '
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucPGP"
        Me.Size = New System.Drawing.Size(424, 272)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Function _AddKey(Optional ByVal nDestinationID As Integer = 99999) As Boolean

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim sResult As String

        sCols = "KeyUserID,KeyPassword,KeyLocation,KeySize,KeyType,DestinationID,PGPID,PGPExt"

        sVals = "'" & clsMarsData.CreateDataID("pgpattr", "keyuserid") & "'," & _
        "''," & _
        "'" & SQLPrepare(txtKeyLocation.Text & "|" & txtSecring.Text) & "'," & _
        "''," & _
        "''," & _
        nDestinationID & "," & _
        "'" & SQLPrepare(txtPGPUserID.Text) & "'," & _
        Convert.ToInt32(chkPGPExt.Checked)

        SQL = "INSERT INTO PGPAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Return True
        End If

    End Function

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        Dim oRes As DialogResult

        With ofd
            .Title = "Select the public keyring file..."
            .Filter = "Public keyring files (*.pkr)|*.pkr|All files (*.*)|*.*"

            oRes = .ShowDialog()

            If oRes = DialogResult.Cancel Then Return

            txtKeyLocation.Text = .FileName
        End With
    End Sub


    Private Sub txtKeyLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeyLocation.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub cmdSec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSec.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        Dim oRes As DialogResult

        With ofd
            .Title = "Select the private keyring file..."
            .Filter = "Private keyring files (*.skr)|*.skr|All files (*.*)|*.*"

            oRes = .ShowDialog()

            If oRes = DialogResult.Cancel Then Return

            txtSecring.Text = .FileName
        End With
    End Sub
End Class
