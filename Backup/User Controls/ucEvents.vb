Imports Quiksoft.EasyMail.POP3
Public Class ucEvents
    Inherits System.Windows.Forms.UserControl
    Public SQLValid As Boolean = False
    Public cmdApply As Button
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpMain As System.Windows.Forms.GroupBox
    Friend WithEvents grpEmail As System.Windows.Forms.GroupBox
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtServer As System.Windows.Forms.TextBox
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtMessage As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents grpDatabase As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBuild As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents cmdParse As System.Windows.Forms.Button
    Friend WithEvents cmbcondition As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents grpProcess As System.Windows.Forms.GroupBox
    Friend WithEvents cmbProcess As System.Windows.Forms.ComboBox
    Friend WithEvents grpWindow As System.Windows.Forms.GroupBox
    Friend WithEvents cmbWindowName As System.Windows.Forms.ComboBox
    Friend WithEvents grpFileMod As System.Windows.Forms.GroupBox
    Friend WithEvents cmdPathMod As System.Windows.Forms.Button
    Friend WithEvents txtModPath As System.Windows.Forms.TextBox
    Friend WithEvents grpFile As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents sfg As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents Query As System.Windows.Forms.Label
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents chkRemove As System.Windows.Forms.CheckBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMars As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents grpDBChange As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN1 As sqlrd.ucDSN
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmbTableName As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents chkTreat As System.Windows.Forms.CheckBox
    Friend WithEvents grpTable As System.Windows.Forms.GroupBox
    Friend WithEvents optRecordset As System.Windows.Forms.RadioButton
    Friend WithEvents optTable As System.Windows.Forms.RadioButton
    Friend WithEvents grpRecordset As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtRsQuery As System.Windows.Forms.TextBox
    Friend WithEvents cmbRsColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmdRsBuild As System.Windows.Forms.Button
    Friend WithEvents cmdRsParse As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucEvents))
        Me.grpMain = New System.Windows.Forms.GroupBox
        Me.grpEmail = New System.Windows.Forms.GroupBox
        Me.chkRemove = New System.Windows.Forms.CheckBox
        Me.cmdTest = New System.Windows.Forms.Button
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtServer = New System.Windows.Forms.TextBox
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtMessage = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.grpFile = New System.Windows.Forms.GroupBox
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuMars = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.grpDBChange = New System.Windows.Forms.GroupBox
        Me.grpRecordset = New System.Windows.Forms.GroupBox
        Me.cmbRsColumn = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdRsBuild = New System.Windows.Forms.Button
        Me.txtRsQuery = New System.Windows.Forms.TextBox
        Me.cmdRsParse = New System.Windows.Forms.Button
        Me.grpTable = New System.Windows.Forms.GroupBox
        Me.cmbTableName = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.optTable = New System.Windows.Forms.RadioButton
        Me.optRecordset = New System.Windows.Forms.RadioButton
        Me.chkTreat = New System.Windows.Forms.CheckBox
        Me.UcDSN1 = New sqlrd.ucDSN
        Me.grpDatabase = New System.Windows.Forms.GroupBox
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Query = New System.Windows.Forms.Label
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.cmdParse = New System.Windows.Forms.Button
        Me.grpFileMod = New System.Windows.Forms.GroupBox
        Me.cmdPathMod = New System.Windows.Forms.Button
        Me.txtModPath = New System.Windows.Forms.TextBox
        Me.grpProcess = New System.Windows.Forms.GroupBox
        Me.cmbProcess = New System.Windows.Forms.ComboBox
        Me.grpWindow = New System.Windows.Forms.GroupBox
        Me.cmbWindowName = New System.Windows.Forms.ComboBox
        Me.cmbcondition = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.sfg = New System.Windows.Forms.SaveFileDialog
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.grpMain.SuspendLayout()
        Me.grpEmail.SuspendLayout()
        Me.grpFile.SuspendLayout()
        Me.grpDBChange.SuspendLayout()
        Me.grpRecordset.SuspendLayout()
        Me.grpTable.SuspendLayout()
        Me.grpDatabase.SuspendLayout()
        Me.grpFileMod.SuspendLayout()
        Me.grpProcess.SuspendLayout()
        Me.grpWindow.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpMain
        '
        Me.grpMain.Controls.Add(Me.grpDatabase)
        Me.grpMain.Controls.Add(Me.grpEmail)
        Me.grpMain.Controls.Add(Me.grpFile)
        Me.grpMain.Controls.Add(Me.grpDBChange)
        Me.grpMain.Controls.Add(Me.grpFileMod)
        Me.grpMain.Controls.Add(Me.grpProcess)
        Me.grpMain.Controls.Add(Me.grpWindow)
        Me.grpMain.Controls.Add(Me.cmbcondition)
        Me.grpMain.Controls.Add(Me.Label3)
        Me.grpMain.Controls.Add(Me.Label5)
        Me.grpMain.Controls.Add(Me.cmbValue)
        Me.grpMain.Location = New System.Drawing.Point(0, 0)
        Me.grpMain.Name = "grpMain"
        Me.grpMain.Size = New System.Drawing.Size(464, 392)
        Me.grpMain.TabIndex = 0
        Me.grpMain.TabStop = False
        Me.grpMain.Text = "Condition"
        '
        'grpEmail
        '
        Me.grpEmail.Controls.Add(Me.chkRemove)
        Me.grpEmail.Controls.Add(Me.cmdTest)
        Me.grpEmail.Controls.Add(Me.txtSubject)
        Me.grpEmail.Controls.Add(Me.Label1)
        Me.grpEmail.Controls.Add(Me.txtServer)
        Me.grpEmail.Controls.Add(Me.txtUserID)
        Me.grpEmail.Controls.Add(Me.Label6)
        Me.grpEmail.Controls.Add(Me.txtPassword)
        Me.grpEmail.Controls.Add(Me.Label7)
        Me.grpEmail.Controls.Add(Me.Label8)
        Me.grpEmail.Controls.Add(Me.txtMessage)
        Me.grpEmail.Controls.Add(Me.Label9)
        Me.grpEmail.Location = New System.Drawing.Point(8, 56)
        Me.grpEmail.Name = "grpEmail"
        Me.grpEmail.Size = New System.Drawing.Size(448, 328)
        Me.grpEmail.TabIndex = 6
        Me.grpEmail.TabStop = False
        Me.grpEmail.Text = "Email Details"
        Me.grpEmail.Visible = False
        '
        'chkRemove
        '
        Me.chkRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRemove.Location = New System.Drawing.Point(8, 243)
        Me.chkRemove.Name = "chkRemove"
        Me.chkRemove.Size = New System.Drawing.Size(232, 24)
        Me.chkRemove.TabIndex = 6
        Me.chkRemove.Text = "Remove found mail from server"
        '
        'cmdTest
        '
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(344, 72)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(40, 21)
        Me.cmdTest.TabIndex = 3
        Me.cmdTest.Text = "Test"
        '
        'txtSubject
        '
        Me.txtSubject.Location = New System.Drawing.Point(8, 112)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(376, 21)
        Me.txtSubject.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "POP Server"
        '
        'txtServer
        '
        Me.txtServer.Location = New System.Drawing.Point(96, 24)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(240, 21)
        Me.txtServer.TabIndex = 0
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(96, 48)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(240, 21)
        Me.txtUserID.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 50)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(88, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "User ID"
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(96, 72)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(240, 21)
        Me.txtPassword.TabIndex = 2
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(88, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Password"
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 98)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(248, 16)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Subject contains"
        '
        'txtMessage
        '
        Me.txtMessage.Location = New System.Drawing.Point(8, 160)
        Me.txtMessage.Multiline = True
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMessage.Size = New System.Drawing.Size(376, 80)
        Me.txtMessage.TabIndex = 5
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 144)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(272, 16)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Message contains"
        '
        'grpFile
        '
        Me.grpFile.Controls.Add(Me.cmdBrowse)
        Me.grpFile.Controls.Add(Me.txtPath)
        Me.grpFile.Location = New System.Drawing.Point(8, 56)
        Me.grpFile.Name = "grpFile"
        Me.grpFile.Size = New System.Drawing.Size(448, 80)
        Me.grpFile.TabIndex = 2
        Me.grpFile.TabStop = False
        Me.grpFile.Text = "File Details"
        Me.grpFile.Visible = False
        '
        'cmdBrowse
        '
        Me.cmdBrowse.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(376, 32)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(56, 21)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "...."
        Me.cmdBrowse.UseVisualStyleBackColor = False
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        Me.txtPath.ContextMenu = Me.mnuInserter
        Me.txtPath.Location = New System.Drawing.Point(24, 32)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(320, 21)
        Me.txtPath.TabIndex = 0
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMars, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'mnuMars
        '
        Me.mnuMars.Index = 0
        Me.mnuMars.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'grpDBChange
        '
        Me.grpDBChange.Controls.Add(Me.grpRecordset)
        Me.grpDBChange.Controls.Add(Me.grpTable)
        Me.grpDBChange.Controls.Add(Me.optTable)
        Me.grpDBChange.Controls.Add(Me.optRecordset)
        Me.grpDBChange.Controls.Add(Me.chkTreat)
        Me.grpDBChange.Controls.Add(Me.UcDSN1)
        Me.grpDBChange.Location = New System.Drawing.Point(8, 56)
        Me.grpDBChange.Name = "grpDBChange"
        Me.grpDBChange.Size = New System.Drawing.Size(448, 328)
        Me.grpDBChange.TabIndex = 7
        Me.grpDBChange.TabStop = False
        Me.grpDBChange.Text = "Data has changed"
        Me.grpDBChange.Visible = False
        '
        'grpRecordset
        '
        Me.grpRecordset.Controls.Add(Me.cmbRsColumn)
        Me.grpRecordset.Controls.Add(Me.Label11)
        Me.grpRecordset.Controls.Add(Me.Label2)
        Me.grpRecordset.Controls.Add(Me.cmdRsBuild)
        Me.grpRecordset.Controls.Add(Me.txtRsQuery)
        Me.grpRecordset.Controls.Add(Me.cmdRsParse)
        Me.grpRecordset.Location = New System.Drawing.Point(8, 152)
        Me.grpRecordset.Name = "grpRecordset"
        Me.grpRecordset.Size = New System.Drawing.Size(424, 144)
        Me.grpRecordset.TabIndex = 2
        Me.grpRecordset.TabStop = False
        Me.grpRecordset.Visible = False
        '
        'cmbRsColumn
        '
        Me.cmbRsColumn.ItemHeight = 13
        Me.cmbRsColumn.Location = New System.Drawing.Point(216, 103)
        Me.cmbRsColumn.Name = "cmbRsColumn"
        Me.cmbRsColumn.Size = New System.Drawing.Size(120, 21)
        Me.cmbRsColumn.TabIndex = 11
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 98)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(200, 30)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Please select the column that uniquely identifies the data in each row"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Query"
        '
        'cmdRsBuild
        '
        Me.cmdRsBuild.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRsBuild.Image = CType(resources.GetObject("cmdRsBuild.Image"), System.Drawing.Image)
        Me.cmdRsBuild.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRsBuild.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRsBuild.Location = New System.Drawing.Point(344, 32)
        Me.cmdRsBuild.Name = "cmdRsBuild"
        Me.cmdRsBuild.Size = New System.Drawing.Size(75, 23)
        Me.cmdRsBuild.TabIndex = 5
        Me.cmdRsBuild.Text = "Build..."
        Me.cmdRsBuild.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRsQuery
        '
        Me.txtRsQuery.Location = New System.Drawing.Point(8, 32)
        Me.txtRsQuery.Multiline = True
        Me.txtRsQuery.Name = "txtRsQuery"
        Me.txtRsQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtRsQuery.Size = New System.Drawing.Size(328, 56)
        Me.txtRsQuery.TabIndex = 4
        Me.txtRsQuery.Tag = "memo"
        '
        'cmdRsParse
        '
        Me.cmdRsParse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRsParse.Image = CType(resources.GetObject("cmdRsParse.Image"), System.Drawing.Image)
        Me.cmdRsParse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRsParse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRsParse.Location = New System.Drawing.Point(344, 64)
        Me.cmdRsParse.Name = "cmdRsParse"
        Me.cmdRsParse.Size = New System.Drawing.Size(75, 23)
        Me.cmdRsParse.TabIndex = 6
        Me.cmdRsParse.Text = "Parse..."
        Me.cmdRsParse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpTable
        '
        Me.grpTable.Controls.Add(Me.cmbTableName)
        Me.grpTable.Controls.Add(Me.Label4)
        Me.grpTable.Controls.Add(Me.Label10)
        Me.grpTable.Controls.Add(Me.cmbColumn)
        Me.grpTable.Location = New System.Drawing.Point(8, 152)
        Me.grpTable.Name = "grpTable"
        Me.grpTable.Size = New System.Drawing.Size(424, 144)
        Me.grpTable.TabIndex = 1
        Me.grpTable.TabStop = False
        '
        'cmbTableName
        '
        Me.cmbTableName.ItemHeight = 13
        Me.cmbTableName.Location = New System.Drawing.Point(264, 22)
        Me.cmbTableName.Name = "cmbTableName"
        Me.cmbTableName.Size = New System.Drawing.Size(128, 21)
        Me.cmbTableName.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(176, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Please select the table to monitor"
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 64)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(248, 24)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Please select the column that uniquely identifies the data in each row"
        '
        'cmbColumn
        '
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(264, 66)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(128, 21)
        Me.cmbColumn.TabIndex = 9
        '
        'optTable
        '
        Me.optTable.Checked = True
        Me.optTable.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optTable.Location = New System.Drawing.Point(8, 128)
        Me.optTable.Name = "optTable"
        Me.optTable.Size = New System.Drawing.Size(232, 24)
        Me.optTable.TabIndex = 12
        Me.optTable.TabStop = True
        Me.optTable.Text = "Data in a table"
        '
        'optRecordset
        '
        Me.optRecordset.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optRecordset.Location = New System.Drawing.Point(240, 128)
        Me.optRecordset.Name = "optRecordset"
        Me.optRecordset.Size = New System.Drawing.Size(200, 24)
        Me.optRecordset.TabIndex = 11
        Me.optRecordset.Text = "Data in a recordset (advanced)"
        '
        'chkTreat
        '
        Me.chkTreat.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkTreat.Location = New System.Drawing.Point(8, 296)
        Me.chkTreat.Name = "chkTreat"
        Me.chkTreat.Size = New System.Drawing.Size(424, 24)
        Me.chkTreat.TabIndex = 10
        Me.chkTreat.Text = "Also detect data inserts and deletes"
        '
        'UcDSN1
        '
        Me.UcDSN1.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN1.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN1.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN1.Name = "UcDSN1"
        Me.UcDSN1.Size = New System.Drawing.Size(432, 112)
        Me.UcDSN1.TabIndex = 0
        '
        'grpDatabase
        '
        Me.grpDatabase.Controls.Add(Me.cmbKey)
        Me.grpDatabase.Controls.Add(Me.Label12)
        Me.grpDatabase.Controls.Add(Me.Query)
        Me.grpDatabase.Controls.Add(Me.UcDSN)
        Me.grpDatabase.Controls.Add(Me.cmdBuild)
        Me.grpDatabase.Controls.Add(Me.txtQuery)
        Me.grpDatabase.Controls.Add(Me.cmdParse)
        Me.grpDatabase.Location = New System.Drawing.Point(8, 56)
        Me.grpDatabase.Name = "grpDatabase"
        Me.grpDatabase.Size = New System.Drawing.Size(448, 328)
        Me.grpDatabase.TabIndex = 3
        Me.grpDatabase.TabStop = False
        Me.grpDatabase.Text = "Database Query"
        Me.grpDatabase.Visible = False
        '
        'cmbKey
        '
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(128, 256)
        Me.cmbKey.Name = "cmbKey"
        Me.cmbKey.Size = New System.Drawing.Size(121, 21)
        Me.cmbKey.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(24, 258)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Key Column"
        '
        'Query
        '
        Me.Query.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Query.Location = New System.Drawing.Point(24, 136)
        Me.Query.Name = "Query"
        Me.Query.Size = New System.Drawing.Size(100, 16)
        Me.Query.TabIndex = 3
        Me.Query.Text = "Query"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(24, 16)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(400, 128)
        Me.UcDSN.TabIndex = 0
        '
        'cmdBuild
        '
        Me.cmdBuild.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBuild.Image = CType(resources.GetObject("cmdBuild.Image"), System.Drawing.Image)
        Me.cmdBuild.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBuild.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBuild.Location = New System.Drawing.Point(24, 296)
        Me.cmdBuild.Name = "cmdBuild"
        Me.cmdBuild.Size = New System.Drawing.Size(75, 23)
        Me.cmdBuild.TabIndex = 2
        Me.cmdBuild.Text = "Build..."
        Me.cmdBuild.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtQuery
        '
        Me.txtQuery.Location = New System.Drawing.Point(24, 152)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(392, 96)
        Me.txtQuery.TabIndex = 2
        Me.txtQuery.Tag = "memo"
        '
        'cmdParse
        '
        Me.cmdParse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdParse.Image = CType(resources.GetObject("cmdParse.Image"), System.Drawing.Image)
        Me.cmdParse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdParse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParse.Location = New System.Drawing.Point(344, 296)
        Me.cmdParse.Name = "cmdParse"
        Me.cmdParse.Size = New System.Drawing.Size(75, 23)
        Me.cmdParse.TabIndex = 3
        Me.cmdParse.Text = "Parse..."
        Me.cmdParse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpFileMod
        '
        Me.grpFileMod.Controls.Add(Me.cmdPathMod)
        Me.grpFileMod.Controls.Add(Me.txtModPath)
        Me.grpFileMod.Location = New System.Drawing.Point(8, 56)
        Me.grpFileMod.Name = "grpFileMod"
        Me.grpFileMod.Size = New System.Drawing.Size(448, 88)
        Me.grpFileMod.TabIndex = 3
        Me.grpFileMod.TabStop = False
        Me.grpFileMod.Text = "File Details"
        Me.grpFileMod.Visible = False
        '
        'cmdPathMod
        '
        Me.cmdPathMod.BackColor = System.Drawing.SystemColors.Control
        Me.cmdPathMod.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdPathMod.Image = CType(resources.GetObject("cmdPathMod.Image"), System.Drawing.Image)
        Me.cmdPathMod.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPathMod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPathMod.Location = New System.Drawing.Point(376, 32)
        Me.cmdPathMod.Name = "cmdPathMod"
        Me.cmdPathMod.Size = New System.Drawing.Size(56, 21)
        Me.cmdPathMod.TabIndex = 1
        Me.cmdPathMod.Text = "...."
        Me.cmdPathMod.UseVisualStyleBackColor = False
        '
        'txtModPath
        '
        Me.txtModPath.BackColor = System.Drawing.Color.White
        Me.txtModPath.Location = New System.Drawing.Point(24, 32)
        Me.txtModPath.Name = "txtModPath"
        Me.txtModPath.ReadOnly = True
        Me.txtModPath.Size = New System.Drawing.Size(320, 21)
        Me.txtModPath.TabIndex = 0
        '
        'grpProcess
        '
        Me.grpProcess.Controls.Add(Me.cmbProcess)
        Me.grpProcess.Location = New System.Drawing.Point(8, 56)
        Me.grpProcess.Name = "grpProcess"
        Me.grpProcess.Size = New System.Drawing.Size(448, 88)
        Me.grpProcess.TabIndex = 5
        Me.grpProcess.TabStop = False
        Me.grpProcess.Text = "Process Name"
        Me.grpProcess.Visible = False
        '
        'cmbProcess
        '
        Me.cmbProcess.ItemHeight = 13
        Me.cmbProcess.Location = New System.Drawing.Point(16, 40)
        Me.cmbProcess.Name = "cmbProcess"
        Me.cmbProcess.Size = New System.Drawing.Size(256, 21)
        Me.cmbProcess.TabIndex = 0
        '
        'grpWindow
        '
        Me.grpWindow.Controls.Add(Me.cmbWindowName)
        Me.grpWindow.Location = New System.Drawing.Point(8, 56)
        Me.grpWindow.Name = "grpWindow"
        Me.grpWindow.Size = New System.Drawing.Size(448, 88)
        Me.grpWindow.TabIndex = 4
        Me.grpWindow.TabStop = False
        Me.grpWindow.Text = "Window Name"
        Me.grpWindow.Visible = False
        '
        'cmbWindowName
        '
        Me.cmbWindowName.ItemHeight = 13
        Me.cmbWindowName.Location = New System.Drawing.Point(16, 40)
        Me.cmbWindowName.Name = "cmbWindowName"
        Me.cmbWindowName.Size = New System.Drawing.Size(256, 21)
        Me.cmbWindowName.TabIndex = 0
        '
        'cmbcondition
        '
        Me.cmbcondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbcondition.ItemHeight = 13
        Me.cmbcondition.Items.AddRange(New Object() {"File exists", "Database record exists", "File has been modified", "Window is present", "Process exists", "Unread email is present", "Database record has changed"})
        Me.cmbcondition.Location = New System.Drawing.Point(32, 24)
        Me.cmbcondition.Name = "cmbcondition"
        Me.cmbcondition.Size = New System.Drawing.Size(216, 21)
        Me.cmbcondition.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "IF"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(256, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "="
        '
        'cmbValue
        '
        Me.cmbValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Items.AddRange(New Object() {"TRUE", "FALSE"})
        Me.cmbValue.Location = New System.Drawing.Point(280, 24)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(64, 21)
        Me.cmbValue.TabIndex = 1
        '
        'sfg
        '
        Me.sfg.OverwritePrompt = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'ucEvents
        '
        Me.Controls.Add(Me.grpMain)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucEvents"
        Me.Size = New System.Drawing.Size(464, 392)
        Me.grpMain.ResumeLayout(False)
        Me.grpEmail.ResumeLayout(False)
        Me.grpEmail.PerformLayout()
        Me.grpFile.ResumeLayout(False)
        Me.grpFile.PerformLayout()
        Me.grpDBChange.ResumeLayout(False)
        Me.grpRecordset.ResumeLayout(False)
        Me.grpRecordset.PerformLayout()
        Me.grpTable.ResumeLayout(False)
        Me.grpDatabase.ResumeLayout(False)
        Me.grpDatabase.PerformLayout()
        Me.grpFileMod.ResumeLayout(False)
        Me.grpFileMod.PerformLayout()
        Me.grpProcess.ResumeLayout(False)
        Me.grpWindow.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmbcondition_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbcondition.SelectedIndexChanged
        Dim sGrpName As String

        Select Case cmbcondition.Text.ToLower
            Case "file exists"
                sGrpName = "grpfile"
            Case "database record exists"
                sGrpName = "grpdatabase"
            Case "file has been modified"
                sGrpName = "grpfilemod"
            Case "window is present"
                sGrpName = "grpwindow"
            Case "process exists"
                sGrpName = "grpprocess"
            Case "unread email is present"
                sGrpName = "grpemail"
            Case "database record has changed"
                sGrpName = "grpdbchange"
        End Select

        Try
            For Each grp As GroupBox In grpMain.Controls
                If grp.Name.ToLower = sGrpName Then
                    grp.Visible = True
                Else
                    grp.Visible = False
                End If
            Next
        Catch
        End Try

        ep.SetError(sender, String.Empty)

        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        sfg.Title = "Please enter the name of the file"

        sfg.ShowDialog()

        If sfg.FileName.Length = 0 Then Return

        txtPath.Text = sfg.FileName
    End Sub

    Private Sub cmdPathMod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPathMod.Click
        ofd.Title = "Please select the file to monitor"

        ofd.ShowDialog()

        If ofd.FileName.Length > 0 Then txtModPath.Text = ofd.FileName
    End Sub


    Private Sub cmbProcess_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbProcess.DropDown
        cmbProcess.Items.Clear()
        AppStatus(True)
        For Each o As Process In Process.GetProcesses
            cmbProcess.Items.Add(o.ProcessName)
        Next
        AppStatus(False)
    End Sub

    Private Sub cmbWindowName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbWindowName.SelectedIndexChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub cmbWindowName_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWindowName.DropDown
        AppStatus(True)
        cmbWindowName.Items.Clear()

        For Each o As Process In Process.GetProcesses
            If o.MainWindowTitle.Length > 0 Then _
            cmbWindowName.Items.Add(o.MainWindowTitle)
        Next
        AppStatus(False)
    End Sub

    Private Sub cmdBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuild.Click
        Dim oForm As New frmDynamicParameter
        Dim sVals(1) As String
        Dim sCon As String = ""

        If UcDSN.cmbDSN.Text.Length > 0 Then
            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|"
        End If

        sVals = oForm.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSN.cmbDSN.Text = sCon.Split("|")(0)
        UcDSN.txtUserID.Text = sCon.Split("|")(1)
        UcDSN.txtPassword.Text = sCon.Split("|")(2)
    End Sub

    Private Sub cmdParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParse.Click
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        Try
            oCon.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            oRs.Close()

            MessageBox.Show("SQL query is valid", Application.ProductName, _
             MessageBoxButtons.OK, MessageBoxIcon.Information)

            SQLValid = True

            ep.SetError(txtQuery, String.Empty)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            SQLValid = False
        End Try
        ep.SetError(UcDSN, String.Empty)
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtModPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtModPath.TextChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtQuery_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuery.TextChanged
        ep.SetError(sender, String.Empty)
        SQLValid = False
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub cmbProcess_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbProcess.SelectedIndexChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServer.TextChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserID.TextChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub


    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        ep.SetError(sender, String.Empty)
        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtSubject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSubject.TextChanged

        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub txtMessage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMessage.TextChanged

        If Not cmdApply Is Nothing Then cmdApply.Enabled = True
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click

        AppStatus(True)

        Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense ' "ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"

        Dim pop3Obj As New POP3

        Try
            'Connect to the POP3 mail server
            pop3Obj.Connect(txtServer.Text)

            'Log onto the POP3 mail server
            pop3Obj.Login(txtUserID.Text, txtPassword.Text, AuthMode.Plain)

            pop3Obj.Disconnect()
            MessageBox.Show("POP3 Aunthentication succeeded!", _
            Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check the pop3 server details and try again")
        End Try

        AppStatus(False)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        txtPath.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        txtPath.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        txtPath.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        txtPath.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        txtPath.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        txtPath.SelectAll()
    End Sub

    Private Sub mnuMars_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMars.Click
        Dim oInsert As New frmInserter(0)

        oInsert.cmbType.Text = "@@SQL-RD Constants"

        oInsert.GetConstants(Me.ParentForm)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(0)

        If sTemp.Length > 0 Then
            txtPath.SelectedText = sTemp
        End If
    End Sub

    Private Sub cmbTableName_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbTableName.DropDown
        cmbTableName.Items.Clear()

        Dim oData As New clsMarsData

        oData.GetTables(cmbTableName, UcDSN1.cmbDSN.Text, UcDSN1.txtUserID.Text, UcDSN1.txtPassword.Text)
    End Sub

    Private Sub cmbColumn_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbColumn.DropDown
        cmbColumn.Items.Clear()

        Dim oData As New clsMarsData

        oData.GetColumns(cmbColumn, UcDSN1.cmbDSN.Text, cmbTableName.Text, UcDSN1.txtUserID.Text, _
        UcDSN1.txtPassword.Text)

    End Sub

    Private Sub optTable_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTable.CheckedChanged
        grpTable.Visible = optTable.Checked

        grpRecordset.Visible = Not optTable.Checked
    End Sub

    Private Sub optRecordset_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optRecordset.CheckedChanged
        grpRecordset.Visible = optRecordset.Checked

        grpTable.Visible = Not optRecordset.Checked
    End Sub

    Private Sub cmdRsBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRsBuild.Click
        Dim oForm As New frmDynamicParameter
        Dim sVals(1) As String
        Dim sCon As String = ""

        If UcDSN1.cmbDSN.Text.Length > 0 Then
            sCon = UcDSN1.cmbDSN.Text & "|" & UcDSN1.txtUserID.Text & "|" & UcDSN1.txtPassword.Text & "|"
        End If

        sVals = oForm.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtRsQuery.Text = sVals(1)

        UcDSN1.cmbDSN.Text = sCon.Split("|")(0)
        UcDSN1.txtUserID.Text = sCon.Split("|")(1)
        UcDSN1.txtPassword.Text = sCon.Split("|")(2)
    End Sub

    Private Sub cmdRsParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRsParse.Click
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        If txtRsQuery.Text.Length = 0 Then Return
        Try
            oCon.Open(UcDSN1.cmbDSN.Text, UcDSN1.txtUserID.Text, UcDSN1.txtPassword.Text)

            oRs.Open(txtRsQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            oRs.Close()

            MessageBox.Show("SQL query is valid", Application.ProductName, _
             MessageBoxButtons.OK, MessageBoxIcon.Information)

            SQLValid = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            SQLValid = False
        End Try
        ep.SetError(UcDSN1, String.Empty)
    End Sub

    Private Sub cmbRsColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRsColumn.SelectedIndexChanged

    End Sub

    Private Sub cmbRsColumn_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbRsColumn.DropDown
        If txtRsQuery.Text.Length = 0 Then Return

        cmbRsColumn.Items.Clear()

        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        With UcDSN1
            oCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
        End With

        oRs.Open(txtRsQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

        For I As Integer = 0 To oRs.Fields.Count - 1
            cmbRsColumn.Items.Add(oRs.Fields(I).Name)
        Next

        oRs.Close()
    End Sub


    Private Sub cmbKey_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbKey.DropDown
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        cmbKey.Items.Clear()

        Try
            With UcDSN
                oCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
            End With

            oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            For I As Integer = 0 To oRs.Fields.Count - 1
                If oRs.Fields(I).Type <> ADODB.DataTypeEnum.adLongVarWChar Then
                    cmbKey.Items.Add(oRs.Fields(I).Name)
                End If
            Next

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), "Please check the provided credentials")
        End Try
    End Sub

    Private Sub cmbKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbKey.SelectedIndexChanged

    End Sub
End Class
