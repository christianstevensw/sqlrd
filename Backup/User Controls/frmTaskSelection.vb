Public Class frmTaskSelection
    Inherits System.Windows.Forms.Form
    Public ScheduleID As Integer = 99999
    Public ShowAfter As Boolean = True
    Public oAuto As Boolean = False
    Dim eventBased As Boolean = False
    Dim eventID As Integer = 99999

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tvTaskTypes As System.Windows.Forms.TreeView
    Friend WithEvents imgTasks As System.Windows.Forms.ImageList
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdSelect As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim TreeNode1 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Run Program/Open Document", 1, 1)
        Dim TreeNode2 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Print Document", 2, 2)
        Dim TreeNode3 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Wait/Pause", 14, 14)
        Dim TreeNode4 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Execute Schedule", 40, 40)
        Dim TreeNode5 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Send SMS", 44, 44)
        Dim TreeNode6 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("General", New System.Windows.Forms.TreeNode() {TreeNode1, TreeNode2, TreeNode3, TreeNode4, TreeNode5})
        Dim TreeNode7 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Copy File", 5, 5)
        Dim TreeNode8 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Rename/Move File", 6, 6)
        Dim TreeNode9 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete File", 7, 7)
        Dim TreeNode10 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Write Text File", 8, 8)
        Dim TreeNode11 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Create Folder", 9, 9)
        Dim TreeNode12 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Rename/Move Folder", 11, 11)
        Dim TreeNode13 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Zip Files", 41, 41)
        Dim TreeNode14 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Unzip File", 42, 42)
        Dim TreeNode15 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Merge PDF Files", 43, 43)
        Dim TreeNode16 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Files and Folders", 4, 4, New System.Windows.Forms.TreeNode() {TreeNode7, TreeNode8, TreeNode9, TreeNode10, TreeNode11, TreeNode12, TreeNode13, TreeNode14, TreeNode15})
        Dim TreeNode17 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Send Email", 13, 13)
        Dim TreeNode18 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Open Web Address", 15, 15)
        Dim TreeNode19 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Upload File(s)", 29, 29)
        Dim TreeNode20 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Download File", 30, 30)
        Dim TreeNode21 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete FTP File", 31, 31)
        Dim TreeNode22 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Create Directory", 32, 32)
        Dim TreeNode23 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete Directory", 31, 31)
        Dim TreeNode24 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("FTP", 28, 28, New System.Windows.Forms.TreeNode() {TreeNode19, TreeNode20, TreeNode21, TreeNode22, TreeNode23})
        Dim TreeNode25 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Internet", 12, 12, New System.Windows.Forms.TreeNode() {TreeNode17, TreeNode18, TreeNode24})
        Dim TreeNode26 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Execute SQL Script (from file)", 22, 22)
        Dim TreeNode27 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Update a database record", 35, 35)
        Dim TreeNode28 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Insert a database record", 33, 33)
        Dim TreeNode29 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete a database record", 34, 34)
        Dim TreeNode30 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Run a stored procedure", 21, 21)
        Dim TreeNode31 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Export Data to Report", 30, 30)
        Dim TreeNode32 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Create a table", 23, 23)
        Dim TreeNode33 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete a table", 24, 24)
        Dim TreeNode34 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Add column to table", 25, 25)
        Dim TreeNode35 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete column from table", 26, 26)
        Dim TreeNode36 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Table", 27, 27, New System.Windows.Forms.TreeNode() {TreeNode32, TreeNode33, TreeNode34, TreeNode35})
        Dim TreeNode37 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Database", 16, 16, New System.Windows.Forms.TreeNode() {TreeNode26, TreeNode27, TreeNode28, TreeNode29, TreeNode30, TreeNode31, TreeNode36})
        Dim TreeNode38 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Set Value", 37, 37)
        Dim TreeNode39 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Delete Key", 38, 38)
        Dim TreeNode40 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Create Key", 39, 39)
        Dim TreeNode41 As System.Windows.Forms.TreeNode = New System.Windows.Forms.TreeNode("Registry", 36, 36, New System.Windows.Forms.TreeNode() {TreeNode38, TreeNode39, TreeNode40})
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskSelection))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tvTaskTypes = New System.Windows.Forms.TreeView
        Me.imgTasks = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdSelect = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tvTaskTypes)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 360)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'tvTaskTypes
        '
        Me.tvTaskTypes.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tvTaskTypes.ForeColor = System.Drawing.Color.Navy
        Me.tvTaskTypes.ImageIndex = 0
        Me.tvTaskTypes.ImageList = Me.imgTasks
        Me.tvTaskTypes.Indent = 19
        Me.tvTaskTypes.ItemHeight = 20
        Me.tvTaskTypes.Location = New System.Drawing.Point(8, 16)
        Me.tvTaskTypes.Name = "tvTaskTypes"
        TreeNode1.ImageIndex = 1
        TreeNode1.Name = ""
        TreeNode1.SelectedImageIndex = 1
        TreeNode1.Text = "Run Program/Open Document"
        TreeNode2.ImageIndex = 2
        TreeNode2.Name = ""
        TreeNode2.SelectedImageIndex = 2
        TreeNode2.Text = "Print Document"
        TreeNode3.ImageIndex = 14
        TreeNode3.Name = ""
        TreeNode3.SelectedImageIndex = 14
        TreeNode3.Text = "Wait/Pause"
        TreeNode4.ImageIndex = 40
        TreeNode4.Name = ""
        TreeNode4.SelectedImageIndex = 40
        TreeNode4.Text = "Execute Schedule"
        TreeNode5.ImageIndex = 44
        TreeNode5.Name = "Node0"
        TreeNode5.SelectedImageIndex = 44
        TreeNode5.Text = "Send SMS"
        TreeNode6.Name = ""
        TreeNode6.Text = "General"
        TreeNode7.ImageIndex = 5
        TreeNode7.Name = ""
        TreeNode7.SelectedImageIndex = 5
        TreeNode7.Text = "Copy File"
        TreeNode8.ImageIndex = 6
        TreeNode8.Name = ""
        TreeNode8.SelectedImageIndex = 6
        TreeNode8.Text = "Rename/Move File"
        TreeNode9.ImageIndex = 7
        TreeNode9.Name = ""
        TreeNode9.SelectedImageIndex = 7
        TreeNode9.Text = "Delete File"
        TreeNode10.ImageIndex = 8
        TreeNode10.Name = ""
        TreeNode10.SelectedImageIndex = 8
        TreeNode10.Text = "Write Text File"
        TreeNode11.ImageIndex = 9
        TreeNode11.Name = ""
        TreeNode11.SelectedImageIndex = 9
        TreeNode11.Text = "Create Folder"
        TreeNode12.ImageIndex = 11
        TreeNode12.Name = ""
        TreeNode12.SelectedImageIndex = 11
        TreeNode12.Text = "Rename/Move Folder"
        TreeNode13.ImageIndex = 41
        TreeNode13.Name = ""
        TreeNode13.SelectedImageIndex = 41
        TreeNode13.Text = "Zip Files"
        TreeNode14.ImageIndex = 42
        TreeNode14.Name = ""
        TreeNode14.SelectedImageIndex = 42
        TreeNode14.Text = "Unzip File"
        TreeNode15.ImageIndex = 43
        TreeNode15.Name = ""
        TreeNode15.SelectedImageIndex = 43
        TreeNode15.Text = "Merge PDF Files"
        TreeNode16.ImageIndex = 4
        TreeNode16.Name = ""
        TreeNode16.SelectedImageIndex = 4
        TreeNode16.Text = "Files and Folders"
        TreeNode17.ImageIndex = 13
        TreeNode17.Name = ""
        TreeNode17.SelectedImageIndex = 13
        TreeNode17.Text = "Send Email"
        TreeNode18.ImageIndex = 15
        TreeNode18.Name = ""
        TreeNode18.SelectedImageIndex = 15
        TreeNode18.Text = "Open Web Address"
        TreeNode19.ImageIndex = 29
        TreeNode19.Name = ""
        TreeNode19.SelectedImageIndex = 29
        TreeNode19.Text = "Upload File(s)"
        TreeNode20.ImageIndex = 30
        TreeNode20.Name = ""
        TreeNode20.SelectedImageIndex = 30
        TreeNode20.Text = "Download File"
        TreeNode21.ImageIndex = 31
        TreeNode21.Name = ""
        TreeNode21.SelectedImageIndex = 31
        TreeNode21.Text = "Delete FTP File"
        TreeNode22.ImageIndex = 32
        TreeNode22.Name = ""
        TreeNode22.SelectedImageIndex = 32
        TreeNode22.Text = "Create Directory"
        TreeNode23.ImageIndex = 31
        TreeNode23.Name = ""
        TreeNode23.SelectedImageIndex = 31
        TreeNode23.Text = "Delete Directory"
        TreeNode24.ImageIndex = 28
        TreeNode24.Name = ""
        TreeNode24.SelectedImageIndex = 28
        TreeNode24.Text = "FTP"
        TreeNode25.ImageIndex = 12
        TreeNode25.Name = ""
        TreeNode25.SelectedImageIndex = 12
        TreeNode25.Text = "Internet"
        TreeNode26.ImageIndex = 22
        TreeNode26.Name = ""
        TreeNode26.SelectedImageIndex = 22
        TreeNode26.Text = "Execute SQL Script (from file)"
        TreeNode27.ImageIndex = 35
        TreeNode27.Name = ""
        TreeNode27.SelectedImageIndex = 35
        TreeNode27.Text = "Update a database record"
        TreeNode28.ImageIndex = 33
        TreeNode28.Name = ""
        TreeNode28.SelectedImageIndex = 33
        TreeNode28.Text = "Insert a database record"
        TreeNode29.ImageIndex = 34
        TreeNode29.Name = ""
        TreeNode29.SelectedImageIndex = 34
        TreeNode29.Text = "Delete a database record"
        TreeNode30.ImageIndex = 21
        TreeNode30.Name = ""
        TreeNode30.SelectedImageIndex = 21
        TreeNode30.Text = "Run a stored procedure"
        TreeNode31.ImageIndex = 30
        TreeNode31.Name = "Node0"
        TreeNode31.SelectedImageIndex = 30
        TreeNode31.Text = "Export Data to Report"
        TreeNode32.ImageIndex = 23
        TreeNode32.Name = ""
        TreeNode32.SelectedImageIndex = 23
        TreeNode32.Text = "Create a table"
        TreeNode33.ImageIndex = 24
        TreeNode33.Name = ""
        TreeNode33.SelectedImageIndex = 24
        TreeNode33.Text = "Delete a table"
        TreeNode34.ImageIndex = 25
        TreeNode34.Name = ""
        TreeNode34.SelectedImageIndex = 25
        TreeNode34.Text = "Add column to table"
        TreeNode35.ImageIndex = 26
        TreeNode35.Name = ""
        TreeNode35.SelectedImageIndex = 26
        TreeNode35.Text = "Delete column from table"
        TreeNode36.ImageIndex = 27
        TreeNode36.Name = ""
        TreeNode36.SelectedImageIndex = 27
        TreeNode36.Text = "Table"
        TreeNode37.ImageIndex = 16
        TreeNode37.Name = ""
        TreeNode37.SelectedImageIndex = 16
        TreeNode37.Text = "Database"
        TreeNode38.ImageIndex = 37
        TreeNode38.Name = ""
        TreeNode38.SelectedImageIndex = 37
        TreeNode38.Text = "Set Value"
        TreeNode39.ImageIndex = 38
        TreeNode39.Name = ""
        TreeNode39.SelectedImageIndex = 38
        TreeNode39.Text = "Delete Key"
        TreeNode40.ImageIndex = 39
        TreeNode40.Name = ""
        TreeNode40.SelectedImageIndex = 39
        TreeNode40.Text = "Create Key"
        TreeNode41.ImageIndex = 36
        TreeNode41.Name = ""
        TreeNode41.SelectedImageIndex = 36
        TreeNode41.Text = "Registry"
        Me.tvTaskTypes.Nodes.AddRange(New System.Windows.Forms.TreeNode() {TreeNode6, TreeNode16, TreeNode25, TreeNode37, TreeNode41})
        Me.tvTaskTypes.SelectedImageIndex = 0
        Me.tvTaskTypes.Size = New System.Drawing.Size(344, 336)
        Me.tvTaskTypes.TabIndex = 0
        '
        'imgTasks
        '
        Me.imgTasks.ImageStream = CType(resources.GetObject("imgTasks.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTasks.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTasks.Images.SetKeyName(0, "")
        Me.imgTasks.Images.SetKeyName(1, "")
        Me.imgTasks.Images.SetKeyName(2, "")
        Me.imgTasks.Images.SetKeyName(3, "")
        Me.imgTasks.Images.SetKeyName(4, "")
        Me.imgTasks.Images.SetKeyName(5, "")
        Me.imgTasks.Images.SetKeyName(6, "")
        Me.imgTasks.Images.SetKeyName(7, "")
        Me.imgTasks.Images.SetKeyName(8, "")
        Me.imgTasks.Images.SetKeyName(9, "")
        Me.imgTasks.Images.SetKeyName(10, "")
        Me.imgTasks.Images.SetKeyName(11, "")
        Me.imgTasks.Images.SetKeyName(12, "")
        Me.imgTasks.Images.SetKeyName(13, "")
        Me.imgTasks.Images.SetKeyName(14, "")
        Me.imgTasks.Images.SetKeyName(15, "")
        Me.imgTasks.Images.SetKeyName(16, "")
        Me.imgTasks.Images.SetKeyName(17, "")
        Me.imgTasks.Images.SetKeyName(18, "")
        Me.imgTasks.Images.SetKeyName(19, "")
        Me.imgTasks.Images.SetKeyName(20, "")
        Me.imgTasks.Images.SetKeyName(21, "")
        Me.imgTasks.Images.SetKeyName(22, "")
        Me.imgTasks.Images.SetKeyName(23, "")
        Me.imgTasks.Images.SetKeyName(24, "")
        Me.imgTasks.Images.SetKeyName(25, "")
        Me.imgTasks.Images.SetKeyName(26, "")
        Me.imgTasks.Images.SetKeyName(27, "")
        Me.imgTasks.Images.SetKeyName(28, "")
        Me.imgTasks.Images.SetKeyName(29, "")
        Me.imgTasks.Images.SetKeyName(30, "")
        Me.imgTasks.Images.SetKeyName(31, "")
        Me.imgTasks.Images.SetKeyName(32, "")
        Me.imgTasks.Images.SetKeyName(33, "")
        Me.imgTasks.Images.SetKeyName(34, "")
        Me.imgTasks.Images.SetKeyName(35, "")
        Me.imgTasks.Images.SetKeyName(36, "")
        Me.imgTasks.Images.SetKeyName(37, "")
        Me.imgTasks.Images.SetKeyName(38, "")
        Me.imgTasks.Images.SetKeyName(39, "")
        Me.imgTasks.Images.SetKeyName(40, "")
        Me.imgTasks.Images.SetKeyName(41, "")
        Me.imgTasks.Images.SetKeyName(42, "")
        Me.imgTasks.Images.SetKeyName(43, "")
        Me.imgTasks.Images.SetKeyName(44, "mda.png")
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(293, 376)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Done"
        '
        'cmdSelect
        '
        Me.cmdSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSelect.Image = CType(resources.GetObject("cmdSelect.Image"), System.Drawing.Image)
        Me.cmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSelect.Location = New System.Drawing.Point(208, 376)
        Me.cmdSelect.Name = "cmdSelect"
        Me.cmdSelect.Size = New System.Drawing.Size(75, 23)
        Me.cmdSelect.TabIndex = 2
        Me.cmdSelect.Text = "&Select"
        '
        'frmTaskSelection
        '
        Me.AcceptButton = Me.cmdSelect
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(378, 408)
        Me.Controls.Add(Me.cmdSelect)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmTaskSelection"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Select Task"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub tvTaskTypes_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvTaskTypes.DoubleClick

        If tvTaskTypes.SelectedNode.GetNodeCount(False) = 0 Then
            Select Case tvTaskTypes.SelectedNode.Text.ToLower
                Case "run program/open document"
                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '        Return
                    '    End If
                    'End If

                    Dim oForm As frmTaskProgram = New frmTaskProgram
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID
                    oForm.AddTask(ScheduleID, ShowAfter)

                Case "print document"

                    'If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '    Return
                    'End If

                    Dim oForm As frmTaskPrint = New frmTaskPrint

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "wait/pause"

                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '        Return
                    '    End If
                    'End If

                    Dim oForm As frmTaskPause = New frmTaskPause
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "copy file"
                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '        Return
                    '    End If

                    'End If
                    Dim oForm As frmTaskCopyFile = New frmTaskCopyFile
                    oForm.m_eventID = Me.eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "rename/move file"
                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '        Return
                    '    End If

                    'End If
                    Dim oForm As frmTaskRenameFile = New frmTaskRenameFile
                    oForm.m_eventID = Me.eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "delete file"
                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISEPRO Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                    '        Return
                    '    End If

                    'End If
                    Dim oForm As frmTaskDeleteFile = New frmTaskDeleteFile

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "write text file"

                    'If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '    Return
                    'End If

                    Dim oForm As frmTaskWriteFile = New frmTaskWriteFile
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "create folder"

                    'If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '    Return
                    'End If

                    Dim oForm As frmTaskFolderCreate = New frmTaskFolderCreate
                    oForm.m_EventID = Me.eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "send email"
                    'If oAuto = True Then
                    '    If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '        Return
                    '    End If
                    'Else
                    '    If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '        Return
                    '    End If

                    'End If
                    Dim oForm As frmTaskEmail = New frmTaskEmail
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "open web address"

                    'If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '    Return
                    'End If

                    Dim oForm As frmTaskWebBrowse = New frmTaskWebBrowse
                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "execute sql script (from file)"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If

                    End If
                    Dim oForm As frmTaskSQLScript = New frmTaskSQLScript

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "run a stored procedure"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If

                    End If
                    Dim oForm As frmTaskSQLProc = New frmTaskSQLProc
                    oForm.m_eventID = Me.eventID
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "update a database record"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskDBUpdate = New frmTaskDBUpdate
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "insert a database record"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If
                    Dim oForm As frmTaskDBInsert = New frmTaskDBInsert
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID
                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "delete a database record"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskDBDelete = New frmTaskDBDelete
                    oForm.m_eventBased = Me.m_eventBased
                    oForm.m_eventID = Me.m_eventID
                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "create a table"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskTableCreate = New frmTaskTableCreate

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "delete a table"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskTableDelete = New frmTaskTableDelete

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "add column to table"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskTableModify = New frmTaskTableModify

                    oform.cmbModifyType.Text = "ADD COLUMN"

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "delete column from table"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    End If

                    Dim oForm As frmTaskTableModify = New frmTaskTableModify

                    oform.cmbModifyType.Text = "DROP COLUMN"

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "upload file(s)"
                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskFTPUpload

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "download file"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskFTPDownload

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "delete ftp file"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskFTPDeleteFile

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "create directory"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskFTPCreateDirectory

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "delete directory"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca4_FTPCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskFTPDeleteDirectory

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "create key"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskAddKey

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "delete key"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskDeleteKey

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "set value"

                    If oAuto = False Then
                        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                            Return
                        End If
                    Else
                        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.ca5_RegistryCustomAction) = False Then
                            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                            Return
                        End If
                    End If

                    Dim oForm As New frmTaskSetKey

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "execute schedule"

                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.ca2_SchedulesCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                        Return
                    End If

                    Dim oForm As New frmTaskRunSchedule

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "rename/move folder"
                    'If gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    '    Return
                    'End If

                    Dim oForm As New frmTaskFolderMove

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "zip files"
                    'If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '    Return
                    'End If

                    Dim oForm As New frmTaskZip

                    oform.AddTask(ScheduleID, ShowAfter)
                Case "unzip file"
                    'If gnEdition < MarsGlobal.gEdition.GOLD Then
                    '    _NeedUpgrade(MarsGlobal.gEdition.GOLD)
                    '    Return
                    'End If

                    Dim oForm As New frmTaskUnzip
                    
                    oform.AddTask(ScheduleID, ShowAfter)
                Case "merge pdf files"
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca6_PDFCustomAction) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                        Return
                    End If

                    Dim oForm As New frmTaskPDFMerge

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "send sms"
                    If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o1_SMS) Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                        Return
                    End If

                    Dim oForm As frmTaskSMS = New frmTaskSMS

                    oForm.AddTask(ScheduleID, ShowAfter)
                Case "export data to report"
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.ca3_DatabaseCustomActions) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS, "Database Custom Actions")
                        Return
                    End If

                    Dim task As frmTaskSQLExport = New frmTaskSQLExport

                    task.AddTask(ScheduleID, ShowAfter)
            End Select
        End If
    End Sub

    Private Sub cmdSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelect.Click
        tvTaskTypes_DoubleClick(sender, e)
    End Sub

    Private Sub frmTaskSelection_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub


    Private Sub tvTaskTypes_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvTaskTypes.AfterSelect

    End Sub
End Class
