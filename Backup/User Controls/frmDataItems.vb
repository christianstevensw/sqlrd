Public Class frmDataItems
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Dim gItemList As ArrayList
    Friend WithEvents Footer1 As WizardFooter.Footer
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Dim ep As New ErrorProvider
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Public m_EventID As Integer = 99999
    Public m_ParameterList As ArrayList
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lsvItems As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents imgItems As System.Windows.Forms.ImageList
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdAddWhere As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtValue As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDataItems))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.cmdAddWhere = New System.Windows.Forms.Button
        Me.txtValue = New System.Windows.Forms.TextBox
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.lsvItems = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgItems = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Footer1 = New WizardFooter.Footer
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(394, 72)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(296, 23)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Data Items"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(336, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.txtSearch)
        Me.Panel2.Controls.Add(Me.cmdAddWhere)
        Me.Panel2.Controls.Add(Me.txtValue)
        Me.Panel2.Controls.Add(Me.cmdDelete)
        Me.Panel2.Controls.Add(Me.cmdAdd)
        Me.Panel2.Controls.Add(Me.lsvItems)
        Me.Panel2.Controls.Add(Me.cmdEdit)
        Me.Panel2.Location = New System.Drawing.Point(0, 88)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(392, 336)
        Me.Panel2.TabIndex = 1
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(264, 272)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 6
        Me.cmdAddWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtValue
        '
        Me.txtValue.Location = New System.Drawing.Point(8, 304)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(296, 21)
        Me.txtValue.TabIndex = 3
        '
        'cmdDelete
        '
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(310, 104)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(72, 23)
        Me.cmdDelete.TabIndex = 2
        Me.cmdDelete.Text = "Remove"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdAdd
        '
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(310, 40)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(72, 23)
        Me.cmdAdd.TabIndex = 1
        Me.cmdAdd.Text = "Add"
        '
        'lsvItems
        '
        Me.lsvItems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvItems.LargeImageList = Me.imgItems
        Me.lsvItems.Location = New System.Drawing.Point(8, 40)
        Me.lsvItems.Name = "lsvItems"
        Me.lsvItems.Size = New System.Drawing.Size(296, 224)
        Me.lsvItems.SmallImageList = Me.imgItems
        Me.lsvItems.TabIndex = 0
        Me.lsvItems.UseCompatibleStateImageBehavior = False
        Me.lsvItems.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Item Name"
        Me.ColumnHeader1.Width = 287
        '
        'imgItems
        '
        Me.imgItems.ImageStream = CType(resources.GetObject("imgItems.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgItems.TransparentColor = System.Drawing.Color.Transparent
        Me.imgItems.Images.SetKeyName(0, "")
        '
        'cmdEdit
        '
        Me.cmdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(310, 72)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(72, 23)
        Me.cmdEdit.TabIndex = 1
        Me.cmdEdit.Text = "Edit"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(232, 440)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(312, 440)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 423)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(416, 15)
        Me.Footer1.TabIndex = 4
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 72)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(394, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 5
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(54, 13)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(250, 21)
        Me.txtSearch.TabIndex = 7
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Search"
        '
        'frmDataItems
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(394, 472)
        Me.ControlBox = False
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmDataItems"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Items"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Public Sub ViewDataItems()
        Me.cmdAddWhere.Enabled = False
        Me.txtValue.Enabled = False

        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedSingle
        Me.ShowInTaskbar = False
        Me.ControlBox = True
        Me.MaximizeBox = False
        Me.MdiParent = oMain
        Me.LoadAll()

        Me.Show()
    End Sub
    Public Function _GetDataItemValue(ByVal sName As String) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sReturn As String


10:     sName = sName.ToLower.Replace("<[d]", String.Empty).Replace(">", String.Empty)

20:     SQL = "SELECT * FROM DataItems WHERE ItemName ='" & SQLPrepare(sName) & "'"

30:     oRs = clsMarsData.GetData(SQL)

        sqlrd.MarsCommon.SaveTextToFile("------------", sAppPath & "dataitem.debug", , True, True)
        sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Parsing " & sName, sAppPath & "dataitem.debug", , True, True)

40:     Try
50:         If oRs.EOF = False Then
                Dim sCon As String
                Dim sQuery As String
                Dim sDSN As String
                Dim sUser As String
                Dim sPassword As String
60:             Dim oCon As ADODB.Connection = New ADODB.Connection
                Dim AllowMultiple As Boolean = False
                Dim sxSep As String
70:             Dim oparse As New clsMarsParser
                Dim ReplaceNull, ReplaceEOF As Boolean
                Dim ReplaceNullValue, ReplaceEOFValue As String

80:             sQuery = oparse.ParseString(oRs("sqlquery").Value, , True)
90:             sCon = oRs("constring").Value

100:            sDSN = sCon.Split("|")(0)
110:            sUser = sCon.Split("|")(1)
120:            sPassword = sCon.Split("|")(2)

                Try
                    ReplaceNull = IsNull(oRs("replacenull").Value, 0)
                    ReplaceNullValue = IsNull(oRs("replacenullvalue").Value, "")

                    ReplaceEOF = IsNull(oRs("replaceeof").Value, 0)
                    ReplaceEOFValue = IsNull(oRs("replaceeofvalue").Value, "")
                Catch : End Try

                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": DSN = " & sDSN, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": UserID  = " & sUser, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Query = " & sQuery, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Replace Null =" & ReplaceNull, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Replace Null Value =" & ReplaceNullValue, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Replace if EOF =" & ReplaceEOF, sAppPath & "dataitem.debug", , True, True)
                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Replace EOF with =" & ReplaceEOFValue, sAppPath & "dataitem.debug", , True, True)

130:            Try
140:                AllowMultiple = Convert.ToBoolean(oRs("allowmultiple").Value)
150:                sxSep = oRs("multiplesep").Value

160:                sxSep = sxSep.ToLower.Replace("<sqlrdcrlf>", vbCrLf)
170:            Catch ex As Exception
180:                AllowMultiple = False
                End Try

190:            oRs.Close()

200:            oCon.Open(sDSN, sUser, sPassword)

210:            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, _
      ADODB.LockTypeEnum.adLockReadOnly)

220:            If oRs.EOF = False Then
                    sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Records found = True", sAppPath & "dataitem.debug", , True, True)

221:                If ReplaceNull = False Then ReplaceNullValue = ""


230:                If AllowMultiple = False Then
240:                    sReturn = IsNull(oRs.Fields(0).Value, ReplaceNullValue)
250:                Else
260:                    Do While oRs.EOF = False
270:                        sReturn &= IsNull(oRs(0).Value, ReplaceNullValue) & sxSep
280:                        oRs.MoveNext()
290:                    Loop
                    End If
300:            Else

                    sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Record Found = False", sAppPath & "dataitem.debug", , True, True)

301:                If ReplaceEOF = False Then
310:                    Dim emptyItem As Exception = New Exception("The data item did not return any value")

320:                    Throw emptyItem
                    Else
                        sReturn = ReplaceEOFValue
                    End If
                End If

330:            If AllowMultiple = True Then
340:                sReturn = sReturn.Substring(0, sReturn.Length - sxSep.Length)
                End If

350:            oRs.Close()
360:            oCon.Close()

370:            oRs = Nothing
380:            oCon = Nothing

                sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Return Value = " & sReturn, sAppPath & "dataitem.debug", , True, True)

390:            Return sReturn
            End If
400:    Catch ex As Exception
            sqlrd.MarsCommon.SaveTextToFile(Date.Now & ": Error = " & ex.Message & "(Line " & Erl() & ")", sAppPath & "dataitem.debug", , True, True)

410:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 0)
420:        Return Nothing
        End Try
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim sItem As String
        Dim oItem As New frmEditDataItem

        oItem.m_eventID = Me.m_EventID
        oItem.m_ParameterList = Me.m_ParameterList

        sItem = oItem.AddDataItem

        If Not sItem Is Nothing Then
            LoadAll()
            Me.txtSearch.Text = ""
        End If
    End Sub

    Private Sub LoadAll()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM DataItems ORDER BY ItemName"

        oRs = clsMarsData.GetData(SQL)

        lsvItems.Items.Clear()

        gItemList = New ArrayList

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.Text = oRs("itemname").Value
                oItem.Tag = oRs("itemid").Value
                oItem.ImageIndex = 0

                lsvItems.Items.Add(oItem)

                gItemList.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim nID As Integer
        Dim oItem As New frmEditDataItem
        Dim sItem As String

        Try
            oItem.m_eventID = Me.m_EventID

            nID = lsvItems.SelectedItems(0).Tag

            sItem = oItem.EditDataItems(nID)

            If sItem Is Nothing Then Return

            LoadAll()

            Me.txtSearch.Text = ""
        Catch : End Try
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim nID As Integer

        Try
            nID = lsvItems.SelectedItems(0).Tag

            Dim SQL As String = "DELETE FROM DataItems WHERE ItemID =" & nID

            clsMarsData.WriteData(SQL)

            lsvItems.SelectedItems(0).Remove()
        Catch
        End Try
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Try
            txtValue.Text &= "<[d]" & lsvItems.SelectedItems(0).Text & ">"
        Catch
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtValue.Enabled = True Then
            If txtValue.Text.Length = 0 Then
                ep.SetError(txtValue, "Please select at least one data item")
                txtValue.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Public Function _GetDataItem(ByVal eventID As Integer) As String

        Me.m_EventID = eventID
        LoadAll()
        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Return txtValue.Text
    End Function

  

    Private Sub frmDataItems_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Close()
            Return
        End If

        FormatForWinXP(Me)
    End Sub

    Private Sub lsvItems_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvItems.DoubleClick
        If cmdAddWhere.Enabled = True Then
            Me.cmdAddWhere_Click(sender, e)
        Else
            Me.cmdEdit_Click(sender, e)
        End If
    End Sub

    Private Sub lsvItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvItems.SelectedIndexChanged

    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Try
            Dim searchTerm As String = Me.txtSearch.Text.ToLower

            If txtSearch.Text.Length > 0 Then
                For Each it As ListViewItem In Me.lsvItems.Items
                    Dim itemText As String = it.Text.ToLower

                    If itemText.Contains(searchTerm) = False Then
                        it.Remove()
                    End If
                Next
            Else
                If gItemList Is Nothing Then
                    LoadAll()
                Else
                    Me.lsvItems.Items.Clear()
                    Me.lsvItems.BeginUpdate()
                    For Each it As ListViewItem In gItemList
                        lsvItems.Items.Add(it)
                    Next
                    Me.lsvItems.EndUpdate()
                End If
            End If
        Catch : End Try
    End Sub
End Class
