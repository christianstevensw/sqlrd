Public Class ucScheduleSet
    Inherits System.Windows.Forms.UserControl
    Public sFrequency As String = "Daily"
    Public nStatus As Integer = 1
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbException As System.Windows.Forms.ComboBox
    Friend WithEvents chkException As System.Windows.Forms.CheckBox
    Public ScheduleID As Integer = 99999
    Friend WithEvents pnlRepeat As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents optOther As System.Windows.Forms.RadioButton
    Friend WithEvents cmdOther As System.Windows.Forms.Button
    Dim ep As New ErrorProvider
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkNoEnd As System.Windows.Forms.CheckBox
    Friend WithEvents cmbRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents RepeatUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkRepeat As System.Windows.Forms.CheckBox
    Friend WithEvents StartDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents RunAt As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optYearly As System.Windows.Forms.RadioButton
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents optCustom As System.Windows.Forms.RadioButton
    Friend WithEvents cmbCustom As System.Windows.Forms.ComboBox
    Friend WithEvents cmdMonthly As System.Windows.Forms.Button
    Friend WithEvents cmdDaily As System.Windows.Forms.Button
    Friend WithEvents cmdWeekly As System.Windows.Forms.Button
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.pnlRepeat = New System.Windows.Forms.FlowLayoutPanel
        Me.cmbRpt = New System.Windows.Forms.NumericUpDown
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.RepeatUntil = New System.Windows.Forms.DateTimePicker
        Me.chkNoEnd = New System.Windows.Forms.CheckBox
        Me.chkRepeat = New System.Windows.Forms.CheckBox
        Me.StartDate = New System.Windows.Forms.DateTimePicker
        Me.EndDate = New System.Windows.Forms.DateTimePicker
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.RunAt = New System.Windows.Forms.DateTimePicker
        Me.Label12 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdOther = New System.Windows.Forms.Button
        Me.optOther = New System.Windows.Forms.RadioButton
        Me.cmdMonthly = New System.Windows.Forms.Button
        Me.cmbCustom = New System.Windows.Forms.ComboBox
        Me.optDaily = New System.Windows.Forms.RadioButton
        Me.optWeekDaily = New System.Windows.Forms.RadioButton
        Me.optWeekly = New System.Windows.Forms.RadioButton
        Me.optMonthly = New System.Windows.Forms.RadioButton
        Me.optCustom = New System.Windows.Forms.RadioButton
        Me.optYearly = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.cmdDaily = New System.Windows.Forms.Button
        Me.cmdWeekly = New System.Windows.Forms.Button
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmbException = New System.Windows.Forms.ComboBox
        Me.chkException = New System.Windows.Forms.CheckBox
        Me.GroupBox3.SuspendLayout()
        Me.pnlRepeat.SuspendLayout()
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.pnlRepeat)
        Me.GroupBox3.Controls.Add(Me.chkNoEnd)
        Me.GroupBox3.Controls.Add(Me.chkRepeat)
        Me.GroupBox3.Controls.Add(Me.StartDate)
        Me.GroupBox3.Controls.Add(Me.EndDate)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.RunAt)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Location = New System.Drawing.Point(0, 185)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(416, 120)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        '
        'pnlRepeat
        '
        Me.pnlRepeat.Controls.Add(Me.cmbRpt)
        Me.pnlRepeat.Controls.Add(Me.cmbUnit)
        Me.pnlRepeat.Controls.Add(Me.Label13)
        Me.pnlRepeat.Controls.Add(Me.RepeatUntil)
        Me.pnlRepeat.Enabled = False
        Me.pnlRepeat.Location = New System.Drawing.Point(110, 87)
        Me.pnlRepeat.Name = "pnlRepeat"
        Me.pnlRepeat.Size = New System.Drawing.Size(248, 28)
        Me.pnlRepeat.TabIndex = 4
        '
        'cmbRpt
        '
        Me.cmbRpt.DecimalPlaces = 2
        Me.cmbRpt.ForeColor = System.Drawing.Color.Navy
        Me.cmbRpt.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Location = New System.Drawing.Point(3, 3)
        Me.cmbRpt.Minimum = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Name = "cmbRpt"
        Me.cmbRpt.Size = New System.Drawing.Size(48, 21)
        Me.cmbRpt.TabIndex = 0
        Me.cmbRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbRpt.Value = New Decimal(New Integer() {25, 0, 0, 131072})
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.Items.AddRange(New Object() {"hours", "minutes"})
        Me.cmbUnit.Location = New System.Drawing.Point(57, 3)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(74, 21)
        Me.cmbUnit.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.Enabled = False
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(137, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(30, 27)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "until"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'RepeatUntil
        '
        Me.RepeatUntil.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RepeatUntil.Location = New System.Drawing.Point(173, 3)
        Me.RepeatUntil.Name = "RepeatUntil"
        Me.RepeatUntil.ShowUpDown = True
        Me.RepeatUntil.Size = New System.Drawing.Size(72, 21)
        Me.RepeatUntil.TabIndex = 2
        '
        'chkNoEnd
        '
        Me.chkNoEnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNoEnd.Location = New System.Drawing.Point(216, 32)
        Me.chkNoEnd.Name = "chkNoEnd"
        Me.chkNoEnd.Size = New System.Drawing.Size(104, 24)
        Me.chkNoEnd.TabIndex = 2
        Me.chkNoEnd.Text = "No End Date"
        '
        'chkRepeat
        '
        Me.chkRepeat.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRepeat.Location = New System.Drawing.Point(8, 89)
        Me.chkRepeat.Name = "chkRepeat"
        Me.chkRepeat.Size = New System.Drawing.Size(96, 21)
        Me.chkRepeat.TabIndex = 7
        Me.chkRepeat.Text = "Repeat every"
        '
        'StartDate
        '
        Me.StartDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.StartDate.Location = New System.Drawing.Point(8, 32)
        Me.StartDate.Name = "StartDate"
        Me.StartDate.Size = New System.Drawing.Size(88, 21)
        Me.StartDate.TabIndex = 0
        '
        'EndDate
        '
        Me.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EndDate.Location = New System.Drawing.Point(112, 32)
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Size = New System.Drawing.Size(88, 21)
        Me.EndDate.TabIndex = 1
        '
        'Label10
        '
        Me.Label10.Enabled = False
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 16)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "Start Date"
        '
        'Label11
        '
        Me.Label11.Enabled = False
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(112, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(112, 16)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "End Date"
        '
        'RunAt
        '
        Me.RunAt.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RunAt.Location = New System.Drawing.Point(112, 60)
        Me.RunAt.Name = "RunAt"
        Me.RunAt.ShowUpDown = True
        Me.RunAt.Size = New System.Drawing.Size(88, 21)
        Me.RunAt.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.Enabled = False
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 64)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(112, 16)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Run At"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdOther)
        Me.GroupBox2.Controls.Add(Me.optOther)
        Me.GroupBox2.Controls.Add(Me.cmdMonthly)
        Me.GroupBox2.Controls.Add(Me.cmbCustom)
        Me.GroupBox2.Controls.Add(Me.optDaily)
        Me.GroupBox2.Controls.Add(Me.optWeekDaily)
        Me.GroupBox2.Controls.Add(Me.optWeekly)
        Me.GroupBox2.Controls.Add(Me.optMonthly)
        Me.GroupBox2.Controls.Add(Me.optCustom)
        Me.GroupBox2.Controls.Add(Me.optYearly)
        Me.GroupBox2.Controls.Add(Me.optNone)
        Me.GroupBox2.Controls.Add(Me.cmdDaily)
        Me.GroupBox2.Controls.Add(Me.cmdWeekly)
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 134)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'cmdOther
        '
        Me.cmdOther.Enabled = False
        Me.cmdOther.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOther.Location = New System.Drawing.Point(283, 76)
        Me.cmdOther.Name = "cmdOther"
        Me.cmdOther.Size = New System.Drawing.Size(40, 16)
        Me.cmdOther.TabIndex = 11
        Me.cmdOther.Text = "..."
        '
        'optOther
        '
        Me.optOther.AutoSize = True
        Me.optOther.Location = New System.Drawing.Point(208, 76)
        Me.optOther.Name = "optOther"
        Me.optOther.Size = New System.Drawing.Size(53, 17)
        Me.optOther.TabIndex = 10
        Me.optOther.TabStop = True
        Me.optOther.Text = "Other"
        Me.optOther.UseVisualStyleBackColor = True
        '
        'cmdMonthly
        '
        Me.cmdMonthly.Enabled = False
        Me.cmdMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMonthly.Location = New System.Drawing.Point(142, 106)
        Me.cmdMonthly.Name = "cmdMonthly"
        Me.cmdMonthly.Size = New System.Drawing.Size(40, 16)
        Me.cmdMonthly.TabIndex = 6
        Me.cmdMonthly.Text = "..."
        '
        'cmbCustom
        '
        Me.cmbCustom.Enabled = False
        Me.cmbCustom.ItemHeight = 13
        Me.cmbCustom.Location = New System.Drawing.Point(283, 45)
        Me.cmbCustom.Name = "cmbCustom"
        Me.cmbCustom.Size = New System.Drawing.Size(127, 21)
        Me.cmbCustom.TabIndex = 9
        '
        'optDaily
        '
        Me.optDaily.Checked = True
        Me.optDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDaily.Location = New System.Drawing.Point(8, 16)
        Me.optDaily.Name = "optDaily"
        Me.optDaily.Size = New System.Drawing.Size(104, 24)
        Me.optDaily.TabIndex = 0
        Me.optDaily.TabStop = True
        Me.optDaily.Text = "Every Day"
        '
        'optWeekDaily
        '
        Me.optWeekDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekDaily.Location = New System.Drawing.Point(8, 42)
        Me.optWeekDaily.Name = "optWeekDaily"
        Me.optWeekDaily.Size = New System.Drawing.Size(112, 24)
        Me.optWeekDaily.TabIndex = 2
        Me.optWeekDaily.Text = "Every Week Day"
        '
        'optWeekly
        '
        Me.optWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekly.Location = New System.Drawing.Point(8, 72)
        Me.optWeekly.Name = "optWeekly"
        Me.optWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optWeekly.TabIndex = 3
        Me.optWeekly.Text = "Every Week"
        '
        'optMonthly
        '
        Me.optMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optMonthly.Location = New System.Drawing.Point(8, 102)
        Me.optMonthly.Name = "optMonthly"
        Me.optMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optMonthly.TabIndex = 5
        Me.optMonthly.Text = "Every Month"
        '
        'optCustom
        '
        Me.optCustom.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustom.Location = New System.Drawing.Point(208, 42)
        Me.optCustom.Name = "optCustom"
        Me.optCustom.Size = New System.Drawing.Size(69, 24)
        Me.optCustom.TabIndex = 8
        Me.optCustom.Text = "Calendar"
        '
        'optYearly
        '
        Me.optYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optYearly.Location = New System.Drawing.Point(208, 16)
        Me.optYearly.Name = "optYearly"
        Me.optYearly.Size = New System.Drawing.Size(104, 24)
        Me.optYearly.TabIndex = 7
        Me.optYearly.Text = "Every Year"
        '
        'optNone
        '
        Me.optNone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNone.Location = New System.Drawing.Point(208, 102)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(104, 24)
        Me.optNone.TabIndex = 12
        Me.optNone.Text = "None"
        '
        'cmdDaily
        '
        Me.cmdDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDaily.Location = New System.Drawing.Point(142, 20)
        Me.cmdDaily.Name = "cmdDaily"
        Me.cmdDaily.Size = New System.Drawing.Size(40, 16)
        Me.cmdDaily.TabIndex = 1
        Me.cmdDaily.Text = "..."
        '
        'cmdWeekly
        '
        Me.cmdWeekly.Enabled = False
        Me.cmdWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdWeekly.Location = New System.Drawing.Point(142, 76)
        Me.cmdWeekly.Name = "cmdWeekly"
        Me.cmdWeekly.Size = New System.Drawing.Size(40, 16)
        Me.cmdWeekly.TabIndex = 4
        Me.cmdWeekly.Text = "..."
        '
        'chkStatus
        '
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStatus.Location = New System.Drawing.Point(8, 311)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(104, 24)
        Me.chkStatus.TabIndex = 3
        Me.chkStatus.Text = "Enabled"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbException)
        Me.GroupBox1.Controls.Add(Me.chkException)
        Me.GroupBox1.Location = New System.Drawing.Point(0, 135)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(416, 52)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'cmbException
        '
        Me.cmbException.Enabled = False
        Me.cmbException.FormattingEnabled = True
        Me.cmbException.Location = New System.Drawing.Point(142, 18)
        Me.cmbException.Name = "cmbException"
        Me.cmbException.Size = New System.Drawing.Size(148, 21)
        Me.cmbException.TabIndex = 1
        '
        'chkException
        '
        Me.chkException.AutoSize = True
        Me.chkException.Location = New System.Drawing.Point(6, 20)
        Me.chkException.Name = "chkException"
        Me.chkException.Size = New System.Drawing.Size(138, 17)
        Me.chkException.TabIndex = 0
        Me.chkException.Text = "Use exception calendar"
        Me.chkException.UseVisualStyleBackColor = True
        '
        'ucScheduleSet
        '
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkStatus)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucScheduleSet"
        Me.Size = New System.Drawing.Size(421, 338)
        Me.GroupBox3.ResumeLayout(False)
        Me.pnlRepeat.ResumeLayout(False)
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public ReadOnly Property m_EnableRepeat(ByVal scheduleID As Integer) As Boolean
        Get
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            SQL = "SELECT OtherUnit FROM ScheduleOptions WHERE ScheduleID =" & scheduleID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then
                Return True
            ElseIf oRs.EOF = True Then
                Return True
            Else
                Dim unit As String = IsNull(oRs("otherunit").Value, "")

                Select Case unit
                    Case "Seconds", "Minutes", "Hours"
                        Return False
                    Case Else
                        Return True
                End Select
            End If
        End Get
    End Property
    Public ReadOnly Property m_startDate() As String
        Get
            Return ConDateTime(CTimeZ(StartDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_endDate() As String
        Get
            ConDateTime(CTimeZ(EndDate.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_nextRun() As String
        Get
            Return ConDate(CTimeZ(StartDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(RunAt.Value, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_startTime() As String
        Get
            Return ConTime(CTimeZ(RunAt.Value.ToShortTimeString, dateConvertType.WRITE))
        End Get
    End Property

    Public ReadOnly Property m_RepeatUntil() As String
        Get
            CTimeZ(RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE)
        End Get
    End Property


    Public Property m_RepeatUnit() As String
        Get
            Return cmbUnit.Text
        End Get
        Set(ByVal value As String)
            cmbUnit.SelectedIndex = cmbUnit.Items.IndexOf(value)
        End Set
    End Property
    Private Sub optDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDaily.CheckedChanged
        If optDaily.Checked = True Then
            sFrequency = "Daily"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdDaily.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
        Else
            cmdDaily.Enabled = False
        End If
    End Sub

    Private Sub optWeekDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optWeekDaily.CheckedChanged
        If optWeekDaily.Checked = True Then
            sFrequency = "Week-Daily"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            chkStatus.Checked = True
            chkStatus.Enabled = True
        End If
    End Sub

    Private Sub optWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optWeekly.CheckedChanged
        If optWeekly.Checked = True Then
            sFrequency = "Weekly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdWeekly.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
        Else
            cmdWeekly.Enabled = False
        End If
    End Sub

    Private Sub optMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMonthly.CheckedChanged
        If optMonthly.Checked = True Then
            sFrequency = "Monthly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdMonthly.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
        Else
            cmdMonthly.Enabled = False
        End If
    End Sub

    Private Sub optYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optYearly.CheckedChanged
        If optYearly.Checked = True Then
            sFrequency = "Yearly"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            chkStatus.Checked = True
            chkStatus.Enabled = True
        End If
    End Sub

    Private Sub optNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNone.CheckedChanged
        If optNone.Checked = True Then
            sFrequency = "None"
            nStatus = 0
            GroupBox3.Visible = False
            cmbCustom.Enabled = False
            chkStatus.Checked = False
            chkStatus.Enabled = False
        End If
    End Sub

    Private Sub chkNoEnd_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoEnd.CheckedChanged
        If chkNoEnd.Checked = True Then
            EndDate.Enabled = False
        Else
            EndDate.Enabled = True
        End If
    End Sub

    Private Sub chkRepeat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeat.CheckedChanged
        Dim I As Double

        If chkRepeat.CheckState = CheckState.Checked Then

            pnlRepeat.Enabled = True
        Else
            pnlRepeat.Enabled = False
        End If
    End Sub

    Private Sub ucScheduleSet_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        StartDate.Value = Now
        EndDate.Value = Now.AddYears(50)
    End Sub

    Private Sub optCustom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustom.CheckedChanged
        If optCustom.Checked = True Then
          

            sFrequency = "Custom"
            nStatus = 1
            GroupBox3.Visible = True
            cmbCustom.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
        Else
            cmbCustom.Text = ""
        End If
    End Sub

    Private Sub cmbCustom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCustom.SelectedIndexChanged
        If cmbCustom.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar

                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbCustom_DropDown(sender, e)
                    cmbCustom.SelectedIndex = cmbCustom.Items.IndexOf(temp)
                End If

            End If

        Else
            Dim oS As New clsMarsScheduler

            StartDate.Value = oS.GetCustomNextRunDate(cmbCustom.Text, Now.Date.AddDays(-1))
            EndDate.Value = oS.GetCustomEndDate(cmbCustom.Text)
        End If
    End Sub

    Private Sub cmbCustom_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustom.DropDown
        cmbCustom.Items.Clear()
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbCustom.Items.Add("[New...]")

       
        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbCustom.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


    End Sub

    Private Sub cmdMonthly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMonthly.Click


        Dim oForm As New frmScheduleOptions

        oForm.MonthlyOptions(ScheduleID, StartDate.Value)
    End Sub

    Private Sub cmdDaily_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDaily.Click



        Dim oForm As New frmScheduleOptions

        oForm.DailyOptions(ScheduleID)
    End Sub

    Private Sub cmdWeekly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdWeekly.Click

        Dim oForm As New frmScheduleOptions

        oForm.WeeklyOptions(ScheduleID, StartDate.Value)
    End Sub

    Private Sub chkException_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkException.CheckedChanged
        If chkException.Checked And gnEdition < gEdition.GOLD Then
            _NeedUpgrade(gEdition.GOLD)
            chkException.Checked = False
            Return
        End If

        cmbException.Enabled = chkException.Checked

        If chkException.Checked = False Then cmbException.Text = ""
    End Sub

    Private Sub cmbException_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbException.DropDown
        Me.cmbException.Items.Clear()

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbException.Items.Add("[New...]")


        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbException.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub cmbException_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbException.SelectedIndexChanged
        If cmbException.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar
                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbException_DropDown(sender, e)
                    cmbException.SelectedIndex = cmbException.Items.IndexOf(temp)
                End If
            End If
        End If
    End Sub

    Public Function ValidateSchedule() As Boolean
        If StartDate.Value > EndDate.Value Then
            ep.SetError(EndDate, "Please select a valid date range")
            StartDate.Focus()
            Return False
        ElseIf chkRepeat.Checked = True Then
            If cmbRpt.Text = 0 Then
                ep.SetError(cmbRpt, "Please select the repeat interval")
                cmbRpt.Focus()
                Return False
            ElseIf RunAt.Value > RepeatUntil.Value Then
                ep.SetError(RunAt, "The execution time cannot be after the repeat until time")
                RunAt.Focus()
                Return False
            End If
        ElseIf optCustom.Checked = True And _
            (cmbCustom.Text.Length = 0 Or _
            cmbCustom.Text = "[New]") Then
            ep.SetError(cmbCustom, "Please select a valid calendar")
            cmbCustom.Focus()
            Return False
        ElseIf chkException.Checked And (cmbException.Text.Length = 0 Or cmbException.Text = "[New...]") Then
            ep.SetError(cmbException, "Please select an exception calendar")
            cmbException.Focus()
            Return False
        ElseIf cmbException.Text = cmbCustom.Text And cmbCustom.Text.Length > 0 And chkException.Checked = True And optCustom.Checked = True Then
            ep.SetError(cmbException, "You cannot use the same calendar for exception calendar and exception calendar")
            cmbException.Focus()
            Return False
        ElseIf clsMarsData.IsDuplicate("ScheduleOptions", "ScheduleID", ScheduleID, False) = False And optOther.Checked Then
            ep.SetError(cmdOther, "Please set up your schedule's frequency properties")
            cmdOther.Focus()
            Return False
        End If

        Return True
    End Function


    Private Sub optOther_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optOther.CheckedChanged
        If optOther.Checked = True Then
            sFrequency = "Other"
            GroupBox3.Visible = True
            nStatus = 1
            cmbCustom.Enabled = False
            cmdOther.Enabled = True
            chkStatus.Checked = True
            chkStatus.Enabled = True
            Dim selectedFreq As String = ""

            Dim others As New frmScheduleOptions

            others.OtherOptions(ScheduleID, selectedFreq, StartDate.Value)

            If selectedFreq = "Seconds" Or selectedFreq = "Minutes" Or selectedFreq = "Hours" Then
                Me.chkRepeat.Checked = False
                Me.chkRepeat.Enabled = False
            Else
                Me.chkRepeat.Enabled = True
            End If
        Else
            cmdOther.Enabled = False
            Me.chkRepeat.Enabled = True
        End If
    End Sub

    Private Sub cmdOther_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOther.Click

        Dim others As New frmScheduleOptions
        Dim selectedFreq As String = ""

        others.OtherOptions(ScheduleID, selectedFreq, StartDate.Value)

        If selectedFreq = "Seconds" Or selectedFreq = "Minutes" Or selectedFreq = "Hours" Then
            Me.chkRepeat.Checked = False
            Me.chkRepeat.Enabled = False
        Else
            Me.chkRepeat.Enabled = True
        End If
    End Sub

End Class
