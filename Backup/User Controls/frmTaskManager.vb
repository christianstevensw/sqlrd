Public Class frmTaskManager
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lsvTasks As System.Windows.Forms.ListView
    Friend WithEvents imgTasks As System.Windows.Forms.ImageList
    Friend WithEvents cmdSelect As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents mnuTasks As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuRename As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskManager))
        Me.lsvTasks = New System.Windows.Forms.ListView
        Me.imgTasks = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdSelect = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.mnuTasks = New System.Windows.Forms.ContextMenu
        Me.mnuRename = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SuspendLayout()
        '
        'lsvTasks
        '
        Me.lsvTasks.LargeImageList = Me.imgTasks
        Me.lsvTasks.Location = New System.Drawing.Point(8, 8)
        Me.lsvTasks.Name = "lsvTasks"
        Me.lsvTasks.Size = New System.Drawing.Size(336, 352)
        Me.lsvTasks.SmallImageList = Me.imgTasks
        Me.lsvTasks.TabIndex = 0
        '
        'imgTasks
        '
        Me.imgTasks.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgTasks.ImageSize = New System.Drawing.Size(32, 32)
        Me.imgTasks.ImageStream = CType(resources.GetObject("imgTasks.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTasks.TransparentColor = System.Drawing.Color.Transparent
        '
        'cmdSelect
        '
        Me.cmdSelect.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSelect.Image = CType(resources.GetObject("cmdSelect.Image"), System.Drawing.Image)
        Me.cmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSelect.Location = New System.Drawing.Point(352, 8)
        Me.cmdSelect.Name = "cmdSelect"
        Me.cmdSelect.TabIndex = 4
        Me.cmdSelect.Text = "&Select"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(352, 40)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'mnuTasks
        '
        Me.mnuTasks.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuRename, Me.MenuItem3, Me.mnuDelete})
        Me.mnuTasks.RightToLeft = System.Windows.Forms.RightToLeft.No
        '
        'mnuRename
        '
        Me.mnuRename.Index = 0
        Me.mnuRename.Text = "Rename"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 2
        Me.mnuDelete.Text = "Delete"
        '
        'frmTaskManager
        '
        Me.AcceptButton = Me.cmdSelect
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(434, 368)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdSelect)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.lsvTasks)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.MaximizeBox = False
        Me.Name = "frmTaskManager"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Task List Manager"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If System.IO.Directory.Exists(sAppPath & "Tasks") = False Then
            System.IO.Directory.CreateDirectory(sAppPath & "Tasks")
        End If

        For Each sFile As String In System.IO.Directory.GetFiles(sAppPath & "Tasks")
            Dim oItem As New ListViewItem

            oItem.Text = ExtractFileName(sFile).Replace(".xml", String.Empty)

            oItem.ImageIndex = 0
            oItem.Tag = sFile
            lsvTasks.Items.Add(oItem)
        Next
    End Sub

    Private Sub lsvTasks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvTasks.SelectedIndexChanged
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sTip As String

        If lsvTasks.SelectedItems.Count = 0 Then Return

        oRs = oData.GetXML(lsvTasks.SelectedItems(0).Tag)

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            sTip &= "- " & oRs("tasktype").Value & ": " & _
            oRs("taskname").Value & Environment.NewLine

            oRs.MoveNext()
        Loop

        ToolTip1.SetToolTip(lsvTasks, sTip)

        oRs.Close()
    End Sub

    Private Sub lsvTasks_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lsvTasks.MouseUp

        If lsvTasks.SelectedItems.Count = 0 Then Return

        If e.Button = MouseButtons.Right Then
            mnuTasks.Show(lsvTasks, New Point(e.X, e.Y))
        End If
    End Sub

    Private Sub mnuRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRename.Click
        If lsvTasks.SelectedItems.Count = 0 Then Return

        lsvTasks.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub lsvTasks_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.LabelEditEventArgs) Handles lsvTasks.AfterLabelEdit

        If e.Label.Length = 0 Then e.CancelEdit = True : Return

        Dim oItem As ListViewItem = lsvTasks.SelectedItems(0)

        System.IO.File.Move(oItem.Tag, sAppPath & "Tasks\" & e.Label & ".xml")

    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Dim oRes As DialogResult

        If lsvTasks.SelectedItems.Count = 0 Then Return

        oRes = MessageBox.Show("Delete the selected task list?", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            Dim oItem As ListViewItem = lsvTasks.SelectedItems(0)

            System.IO.File.Delete(oItem.Tag)

            oItem.Remove()
        End If
    End Sub

    Public Function _GetTaskListPath() As String
        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Return lsvTasks.SelectedItems(0).Tag

    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelect.Click
        If lsvTasks.SelectedItems.Count = 0 Then
            MessageBox.Show("Please select the task list", _
            Application.ProductName, MessageBoxButtons.OK, _
             MessageBoxIcon.Exclamation)
            Return
        End If

        Close()
    End Sub

    Private Sub lsvTasks_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvTasks.DoubleClick
        If lsvTasks.SelectedItems.Count = 0 Then Return

        cmdSelect_Click(sender, e)
    End Sub
End Class
