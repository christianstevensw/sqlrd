Public Class ucBlankAlert
    Inherits System.Windows.Forms.UserControl
    Public ep As New ErrorProvider
    Dim oField As TextBox = txtBlankMsg
    Dim customDSN, customUserID, customPassword As String
    Dim parametersList As ArrayList

    Public Property m_customDSN() As String
        Get
            If customDSN Is Nothing Then customDSN = ""

            Return customDSN
        End Get
        Set(ByVal value As String)
            customDSN = value
        End Set
    End Property

    Public Property m_customUserID() As String
        Get
            If customUserID Is Nothing Then customUserID = ""

            Return customUserID
        End Get
        Set(ByVal value As String)
            customUserID = value
        End Set
    End Property
    
    Public Property m_customPassword() As String
        Get
            If customPassword Is Nothing Then customPassword = ""

            Return customPassword
        End Get
        Set(ByVal value As String)
            customPassword = value
        End Set
    End Property

    Public ReadOnly Property m_CustomBlankQuery() As String
        Get
            If Me.chkCustomQuery.Checked Then
                Dim query As String

                query = Me.m_customDSN & "|" & Me.m_customUserID & "|" & _EncryptDBValue(Me.m_customPassword) & "|" & txtQuery.Text

                Return query
            Else
                Return ""
            End If
        End Get
    End Property

    Public Property m_ParametersList() As ArrayList
        Get
            Return Me.parametersList
        End Get
        Set(ByVal value As ArrayList)
            Me.parametersList = value
        End Set
    End Property
    Private Sub ucBlankAlert_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        Else
            mnuMAPI.Enabled = True
        End If

        Me.txtAlertTo.ContextMenu = Me.mnuInserter
        Me.txtQuery.ContextMenu = Me.mnuInserter
    End Sub

    Private Sub cmdAlertTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlertTo.Click

    End Sub

    Private Sub cmdAlertTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAlertTo.MouseUp
        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        End If

        mnuContacts.Show(cmdAlertTo, New Point(e.X, e.Y))
    End Sub


    Private Sub chkBlankReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBlankReport.CheckedChanged

        If chkBlankReport.Checked = True Then

            grpBlank.Enabled = True
            optAlert.Checked = True
        Else
            grpBlank.Enabled = False
            optAlert.Checked = False
            optAlertandReport.Checked = False
            optIgnore.Checked = False
            txtAlertTo.Text = String.Empty
            txtBlankMsg.Text = String.Empty
            chkCustomQuery.Checked = False
        End If

    End Sub

    Private Sub txtAlertTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAlertTo.GotFocus
        Me.oField = Me.txtAlertTo
    End Sub

    Private Sub txtAlertTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAlertTo.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        oField.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        oField.SelectAll()
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
        With Speller
            .Modal = True
            .ModalOwner = Me
            .TextBoxBaseToCheck = txtBlankMsg
            .Check()
        End With
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_ParameterList = Me.m_ParametersList

        oInsert.GetConstants(Me.ParentForm, Me)

    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        oField.SelectedText = oItem._GetDataItem(0)
    End Sub

    Private Sub mnuDefMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.mars")
        Catch : End Try
    End Sub

    Private Sub cmdInsert_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert.MouseUp
        mnuSubInsert.Show(cmdInsert, New Point(e.X, e.Y))
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        mnuDefMsg_Click(sender, e)
    End Sub


    Private Sub txtBlankMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBlankMsg.GotFocus
        oField = txtBlankMsg
    End Sub

    Private Sub txtSubject_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSubject.GotFocus
        oField = txtSubject
    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oUI As New clsMarsMessaging

        oUI.OutlookAddresses(Me.txtAlertTo)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click
        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact("To")

        If sValues Is Nothing Then Return

        If txtAlertTo.Text.EndsWith(";") = False And txtAlertTo.Text.Length > 0 _
        Then txtAlertTo.Text &= ";"

        Try
            txtAlertTo.Text &= sValues(0)
        Catch
        End Try

    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click
        If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(0)

        If sTemp.Length > 0 Then
            If Me.txtAlertTo.Text.Length > 0 And txtAlertTo.Text.EndsWith(";") = False Then txtAlertTo.Text &= ";"

            txtAlertTo.Text &= sTemp & ";"
        End If
    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.m2_MailingLists) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        Dim ofg As New OpenFileDialog


        ofg.Title = "Please select the file to read email addresses from"

        ofg.ShowDialog()

        If ofg.FileName.Length > 0 Then
            If Me.txtAlertTo.Text.EndsWith(";") = False And Me.txtAlertTo.Text.Length > 0 Then _
            txtAlertTo.Text &= ";"

            txtAlertTo.Text &= "<[f]" & ofg.FileName & ">;"
        End If
    End Sub

    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.GetConstants(Me.Container)
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim getSQL As frmDynamicParameter = New frmDynamicParameter
        Dim sCon As String

        sCon = Me.m_customDSN & "|" & Me.m_customUserID & "|" & Me.m_customPassword

        getSQL.m_ParameterList = Me.m_ParametersList

        Dim query As String() = getSQL.ReturnSQLQuery(sCon, Me.txtQuery.Text)

        If query IsNot Nothing Then
            sCon = query(0)
            txtQuery.Text = query(1)

            Me.m_customDSN = sCon.Split("|")(0)
            Me.m_customUserID = sCon.Split("|")(1)
            Me.m_customPassword = sCon.Split("|")(2)
        End If

    End Sub

    Private Sub chkCustomQuery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomQuery.CheckedChanged
        Panel1.Enabled = chkCustomQuery.Checked
    End Sub

    Public Function ValidateEntries() As Boolean
        If Me.chkBlankReport.Checked = True Then
            If (Me.optAlert.Checked = True Or Me.optAlertandReport.Checked = True) And _
            Me.txtAlertTo.Text.Length = 0 Then
                Me.ep.SetError(Me.txtAlertTo, "Please enter the alert recipient")
                Me.txtAlertTo.Focus()
                Me.TabControl1.SelectedTab = Me.TabControl1.TabPages(0)
                Return False
            ElseIf Me.m_CustomBlankQuery = "" Or Me.m_CustomBlankQuery.Split("|").Length < 4 Or Me.m_CustomBlankQuery = "|||" Then
                Me.ep.SetError(txtQuery, "Please provide a valid SQL query and connection information so that SQL-RD can determine the report's blank status")
                Me.btnBuild.Focus()
                Me.TabControl1.SelectedTab = Me.TabControl1.TabPages(1)
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If

        Return True
    End Function


    Private Sub txtQuery_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuery.TextChanged
        oField = Me.txtQuery
    End Sub
End Class
