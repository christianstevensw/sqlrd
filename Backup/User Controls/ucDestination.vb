Public Class ucDestination
    Public nReportID As Integer = 99999
    Public nPackID As Integer = 99999
    Public nSmartID As Integer = 99999
    Public eventID As Integer = 99999
    Public isDynamic As Boolean = False
    Public isPackage As Boolean = False
    Public sTables() As String
    Public sCon As String
    Public DynamicDestination As String
    Public sReportTitle As String = ""
    Public DefaultDestinations As Boolean = False
    Public ExcelBurst As Boolean = False
    Public StaticDest As Boolean = False
    Public m_ParameterList As ArrayList = Nothing
    Dim oData As New clsMarsData
    Dim eventBased As Boolean = False
    Dim DeletedItems As ArrayList
    Public isDataDriven As Boolean = False
    Dim delayDelete As Boolean = False
    Public disableEmbed As Boolean
    Dim IsQuery As Boolean

    Public ReadOnly Property destinationCount() As Integer
        Get
            Return Me.lsvDestination.Items.Count
        End Get
    End Property
    Public Property m_IsQuery() As Boolean
        Get
            Return Me.IsQuery
        End Get
        Set(ByVal value As Boolean)
            Me.IsQuery = value
        End Set
    End Property

    Public Property m_DelayDelete() As Boolean
        Get
            Return delayDelete
        End Get
        Set(ByVal value As Boolean)
            delayDelete = value
        End Set
    End Property

    Public ReadOnly Property m_DeletedItems() As ArrayList
        Get
            Return Me.DeletedItems
        End Get
    End Property
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
    Public Property m_IsDynamic() As Boolean
        Get
            Return isDynamic
        End Get
        Set(ByVal value As Boolean)
            isDynamic = value
        End Set
    End Property

    Public Property m_isPackage() As Boolean
        Get
            Return isPackage
        End Get
        Set(ByVal value As Boolean)
            isPackage = value
        End Set
    End Property

    Public Property m_ExcelBurst() As Boolean
        Get
            Return ExcelBurst
        End Get
        Set(ByVal value As Boolean)
            ExcelBurst = value
        End Set
    End Property

    Public Property m_StaticDest() As Boolean
        Get
            Return StaticDest
        End Get
        Set(ByVal value As Boolean)
            StaticDest = value
        End Set
    End Property

    Public Property m_CanDisable() As Boolean
        Get
            Return lsvDestination.CheckBoxes
        End Get
        Set(ByVal value As Boolean)
            lsvDestination.CheckBoxes = value
        End Set
    End Property

    Public Sub LoadAll()
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        lsvDestination.Items.Clear()

        Dim exception As String = ""

        If Me.m_DeletedItems IsNot Nothing Then
            For Each s As String In Me.m_DeletedItems
                exception &= s & ","
            Next
            exception = "(" & exception.Substring(0, exception.Length - 1) & ")"
        End If



        SQL = "SELECT * FROM DestinationAttr WHERE ReportID =" & nReportID & " AND " & _
        "PackID = " & nPackID & " AND SmartID = " & nSmartID

        If Me.m_DeletedItems IsNot Nothing Then
            SQL &= " AND DestinationID NOT IN " & exception
        End If

        SQL &= " ORDER BY DestOrderID"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Exit Sub

        Dim bAtLeastOneChecked As Boolean = False

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = oRs("destinationname").Value
            oItem.Tag = oRs("destinationid").Value

            Select Case CType(oRs("destinationtype").Value.ToLower, String)
                Case "email"
                    oItem.ImageIndex = 0
                Case "disk"
                    oItem.ImageIndex = 1
                Case "printer"
                    oItem.ImageIndex = 2
                Case "ftp"
                    oItem.ImageIndex = 3
                Case "fax"
                    oItem.ImageIndex = 4
                Case "sms"
                    oItem.ImageIndex = 5
                Case "odbc"
                    oItem.ImageIndex = 6
                Case "sharepoint"
                    oItem.ImageIndex = 7
            End Select

            oItem.SubItems.Add(oRs("outputformat").Value)

            If IsNull(oRs("enabledstatus").Value, "1") = "1" Then
                oItem.Checked = True
                bAtLeastOneChecked = True
            Else
                oItem.Checked = False
            End If

            lsvDestination.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        'Always make sure at least one destination is enabled
        If (lsvDestination.Items.Count > 0 And bAtLeastOneChecked = False) Then
            lsvDestination.Items(0).Checked = True
        End If

        oRs.Close()
    End Sub

    Public Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click

        If ExcelBurst = True And lsvDestination.Items.Count > 0 Then
            MessageBox.Show("A schedule set to burst groups to excel can only have one destination", _
                    Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Exclamation)
            Return
        ElseIf lsvDestination.Items.Count >= 1 Then
            If DefaultDestinations = False Then
                If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa9_MultipleDestinations) Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                    Return
                End If
            End If

        End If

        ep.SetError(lsvDestination, String.Empty)

        Dim oAdd As New frmDestinationSelect

        oAdd.eventID = Me.eventID
        oAdd.m_eventBased = Me.m_eventBased
        oAdd.m_ParamterList = Me.m_ParameterList
        oAdd.disableEmbed = Me.disableEmbed
        oAdd.m_IsQuery = Me.m_IsQuery
        oAdd.AddDestination(lsvDestination.Items.Count, nReportID, nPackID, _
        isDynamic, isPackage, DynamicDestination, sReportTitle, ExcelBurst, StaticDest, nSmartID, , Me.isDataDriven)

        LoadAll()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click

        If lsvDestination.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmDestinationSelect
        Dim nID As Integer

        nID = lsvDestination.SelectedItems(0).Tag
        oEdit.m_eventBased = Me.m_eventBased
        oEdit.eventID = Me.eventID
        oEdit.m_ParamterList = Me.m_ParameterList
        oEdit.disableEmbed = Me.disableEmbed
        oEdit.m_IsQuery = Me.m_IsQuery
        oEdit.EditDestinaton(nID, isDynamic, isPackage, sReportTitle, ExcelBurst, StaticDest, nSmartID, , Me.isDataDriven)

        LoadAll()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvDestination.SelectedItems.Count = 0 Then Return

        If Me.DeletedItems Is Nothing Then
            DeletedItems = New ArrayList
        End If

        Dim nID As Integer

        nID = lsvDestination.SelectedItems(0).Tag

        DeletedItems.Add(nID)

        lsvDestination.SelectedItems(0).Remove()

    End Sub

    Private Function DeleteDestination(ByVal nID As Integer) As Boolean
        Dim SQL As String

        SQL = "DELETE FROM DestinationAttr WHERE DestinationID = " & nID

        If clsmarsdata.WriteData(SQL) = True Then

            SQL = "DELETE FROM PrinterAttr WHERE DestinationID = " & nID

            clsMarsData.WriteData(SQL, False)

            Return clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID = " & nID)
        Else
            Return False
        End If
    End Function

    Public Sub CommitDeletions()
        If Me.DeletedItems IsNot Nothing Then
            For Each n As Integer In Me.DeletedItems
                DeleteDestination(n)
            Next
        End If
    End Sub


    Private Sub lsvDestination_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lsvDestination.ItemCheck
        Try
            Dim oItem As ListViewItem = lsvDestination.Items(e.Index)

            Dim destinationID As Integer = oItem.Tag
            Dim SQL As String = ""

            If e.NewValue = CheckState.Checked Then
                SQL = "UPDATE DestinationAttr SET EnabledStatus = 1 WHERE DestinationID =" & destinationID
            Else
                SQL = "UPDATE DestinationAttr SET EnabledStatus = 0 WHERE DestinationID =" & destinationID
            End If

            clsMarsData.WriteData(SQL)
        Catch : End Try
    End Sub

    Private Sub lsvDestination_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDestination.DoubleClick
        Try
            Dim lsv As ListView = CType(sender, ListView)
            Dim Item As ListViewItem

            If lsv IsNot Nothing Then

                Item = lsv.SelectedItems(0)

                If Item IsNot Nothing Then
                    If Item.Checked Then
                        Item.Checked = False
                    Else
                        Item.Checked = True
                    End If
                End If
            End If

            cmdEdit_Click(sender, e)
        Catch : End Try
    End Sub

    Private Sub cmdImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdImport.Click
        Me.CommitDeletions()

        If isDynamic = True And lsvDestination.Items.Count > 0 And StaticDest = False Then
            MessageBox.Show("A dynamic schedule can only have one destination", _
                    Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Exclamation)
            Return
        ElseIf lsvDestination.Items.Count >= 1 Then

            If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa9_MultipleDestinations) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                Return
            End If

        End If

        Dim oDefault As New frmTestDestination
        Dim nID As Integer


        nID = oDefault._ImportDestination

        If nID = 0 Then Return

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim nDestID As Integer = clsMarsData.DataItem.CopyRow("DestinationAttr", nID, " WHERE DestinationID =" & nID, "DestinationID", , , _
        nReportID, nPackID, , , , , , , , nSmartID)

        'sCols = "DestinationID,DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        '"PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate],ReportID,PackID,FTPServer,FTPUserName," & _
        '"FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        '"Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy,SmartID,SMTPServer,UseBursting,UseDUN," & _
        '"DUNName,ReadReceipts,FTPType,AdjustStamp,EnabledStatus,DestOrderID"

        'sVals = nDestID & "," & _
        '"DestinationType,SendTo,CC,BCC,Subject,Message,Extras,OutputPath,PrinterName," & _
        '"PrinterDriver,PrinterPort,Orientation,PageFrom,PageTo,Copies,[Collate]," & nReportID & _
        '"," & nPackID & ",FTPServer,FTPUserName," & _
        '"FTPPassword,FTPPath,DestinationName,OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,IncludeAttach," & _
        '"Compress,Embed,MailFormat,HTMLSplit,DeferDelivery,DeferBy," & nSmartID & ",SMTPServer,UseBursting,UseDUN," & _
        '"DUNName,ReadReceipts,FTPType,AdjustStamp,1," & lsvDestination.Items.Count


        'SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
        '"SELECT " & sVals & " FROM DestinationAttr WHERE DestinationID = " & nID

        'clsMarsData.WriteData(SQL)

        sCols = "KeyUserID,KeyPassword,KeyLocation,KeySize,KeyType,DestinationID,PGPID"

        sVals = "'" & clsMarsData.CreateDataID("pgpattr", "keyuserid") & "',KeyPassword,KeyLocation,KeySize,KeyType," & _
        nDestID & ",PGPID"

        SQL = "INSERT INTO PGPAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM PGPAttr WHERE DestinationID = " & nID

        clsMarsData.WriteData(SQL)

        clsMarsData.DataItem.CopyRow("PrinterAttr", nID, " WHERE DestinationID =" & nID, "PrInterID", , , , , , , , nDestID)

        'sCols = "[Collate],Copies,DestinationID,Orientation,PageFrom,PagesPerReport,PageTo,PaperSize,PrinterDriver,PrinterID," & _
        '"PrinterName,PrinterPort,PrintMethod"

        'sVals = "[Collate],Copies," & nDestID & ",Orientation,PageFrom,PagesPerReport,PageTo,PaperSize,PrinterDriver," & clsMarsData.CreateDataID & "," & _
        '"PrinterName,PrinterPort,PrintMethod"

        'SQL = "INSERT INTO PrinterAttr (" & sCols & ") " & _
        '"SELECT " & sVals & " FROM PrinterAttr WHERE DestinationID = " & nID

        'clsMarsData.WriteData(SQL)

        clsMarsData.DataItem.CopyRow("ReportOptions", nID, " WHERE DestinationID =" & nID, "OptionID", , , nReportID, nPackID, , , , nDestID)

        clsMarsData.DataItem.CopyRow("StampAttr", nID, " WHERE ForeignID =" & nID, "StampID", , , , , , , , nDestID)
        LoadAll()
    End Sub



    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        If lsvDestination.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvDestination.SelectedItems(0)

        If oItem.Index = 0 Then Return

        Dim oMove As ListViewItem = lsvDestination.Items(oItem.Index - 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & newIndex & " WHERE DestinationID =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & oldIndex & " WHERE DestinationID =" & moveID

        clsMarsData.WriteData(SQL)

        Me.LoadAll()

        For Each item As ListViewItem In lsvDestination.Items
            If item.Tag = itemID Then
                item.Selected = True
                item.EnsureVisible()
                Exit For
            End If
        Next

    End Sub

    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        If lsvDestination.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvDestination.SelectedItems(0)

        If oItem.Index = lsvDestination.Items.Count - 1 Then Return

        Dim oMove As ListViewItem = lsvDestination.Items(oItem.Index + 1)

        Dim newIndex As Integer = oMove.Index
        Dim oldIndex As Integer = oItem.Index

        Dim itemID As Integer = oItem.Tag
        Dim moveID As Integer = oMove.Tag

        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & newIndex & " WHERE DestinationID =" & itemID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DestinationAttr SET DestOrderID =" & oldIndex & " WHERE DestinationID =" & moveID

        clsMarsData.WriteData(SQL)

        Me.LoadAll()

        For Each item As ListViewItem In lsvDestination.Items
            If item.Tag = itemID Then
                item.Selected = True
                item.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub lsvDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvDestination.SelectedIndexChanged

    End Sub
End Class
