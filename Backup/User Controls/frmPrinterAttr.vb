Public Class frmPrinterAttr
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtPageFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents optAllPages As System.Windows.Forms.RadioButton
    Friend WithEvents optPageRange As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPageTo As System.Windows.Forms.NumericUpDown
    Dim ep As New ErrorProvider
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()



        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbPrintMethod As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCopies As System.Windows.Forms.NumericUpDown
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtPrinter As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPrinterAttr))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbPrintMethod = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtCopies = New System.Windows.Forms.NumericUpDown
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtPrinter = New System.Windows.Forms.TextBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtPageFrom = New System.Windows.Forms.NumericUpDown
        Me.Label7 = New System.Windows.Forms.Label
        Me.optAllPages = New System.Windows.Forms.RadioButton
        Me.optPageRange = New System.Windows.Forms.RadioButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtPageTo = New System.Windows.Forms.NumericUpDown
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtCopies, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtPageFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtPageTo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(306, 59)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(144, 30)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Printer Setup"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(232, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 47)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Print Method"
        '
        'cmbPrintMethod
        '
        Me.cmbPrintMethod.Enabled = False
        Me.cmbPrintMethod.ItemHeight = 13
        Me.cmbPrintMethod.Items.AddRange(New Object() {"Default"})
        Me.cmbPrintMethod.Location = New System.Drawing.Point(8, 30)
        Me.cmbPrintMethod.Name = "cmbPrintMethod"
        Me.cmbPrintMethod.Size = New System.Drawing.Size(248, 21)
        Me.cmbPrintMethod.TabIndex = 0
        Me.cmbPrintMethod.Text = "Default"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "# Copies"
        '
        'txtCopies
        '
        Me.txtCopies.Location = New System.Drawing.Point(8, 74)
        Me.txtCopies.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtCopies.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtCopies.Name = "txtCopies"
        Me.txtCopies.Size = New System.Drawing.Size(104, 21)
        Me.txtCopies.TabIndex = 1
        Me.txtCopies.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtCopies.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(0, 336)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(352, 8)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Tag = "3dline"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(137, 350)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 22)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(217, 350)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 22)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 67)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 15)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Printer Details"
        '
        'txtPrinter
        '
        Me.txtPrinter.Location = New System.Drawing.Point(8, 82)
        Me.txtPrinter.Name = "txtPrinter"
        Me.txtPrinter.Size = New System.Drawing.Size(288, 21)
        Me.txtPrinter.TabIndex = 12
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox2)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.cmbPrintMethod)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtCopies)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 111)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(288, 219)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtPageFrom)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.optAllPages)
        Me.GroupBox2.Controls.Add(Me.optPageRange)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtPageTo)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 101)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(278, 112)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Page Range"
        '
        'txtPageFrom
        '
        Me.txtPageFrom.Enabled = False
        Me.txtPageFrom.Location = New System.Drawing.Point(56, 74)
        Me.txtPageFrom.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtPageFrom.Name = "txtPageFrom"
        Me.txtPageFrom.Size = New System.Drawing.Size(48, 21)
        Me.txtPageFrom.TabIndex = 2
        Me.txtPageFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 74)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 15)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "From"
        '
        'optAllPages
        '
        Me.optAllPages.Checked = True
        Me.optAllPages.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAllPages.Location = New System.Drawing.Point(8, 22)
        Me.optAllPages.Name = "optAllPages"
        Me.optAllPages.Size = New System.Drawing.Size(216, 23)
        Me.optAllPages.TabIndex = 0
        Me.optAllPages.TabStop = True
        Me.optAllPages.Text = "All Pages"
        '
        'optPageRange
        '
        Me.optPageRange.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPageRange.Location = New System.Drawing.Point(8, 45)
        Me.optPageRange.Name = "optPageRange"
        Me.optPageRange.Size = New System.Drawing.Size(216, 22)
        Me.optPageRange.TabIndex = 1
        Me.optPageRange.Text = "Page Range"
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(120, 74)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(24, 15)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "To"
        '
        'txtPageTo
        '
        Me.txtPageTo.Enabled = False
        Me.txtPageTo.Location = New System.Drawing.Point(168, 74)
        Me.txtPageTo.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtPageTo.Name = "txtPageTo"
        Me.txtPageTo.Size = New System.Drawing.Size(48, 21)
        Me.txtPageTo.TabIndex = 3
        Me.txtPageTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuCut
        '
        Me.mnuCut.Index = 0
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 2
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 3
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 5
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 6
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 7
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'frmPrinterAttr
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(306, 381)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.txtPrinter)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label9)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmPrinterAttr"
        Me.Text = "Printer Setup"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtCopies, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtPageFrom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtPageTo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmPrinterAttr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        txtPrinter.ContextMenu = Me.mnuInserter
    End Sub

    

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If optPageRange.Checked = True Then
            If txtPageFrom.Value = 0 Or txtPageTo.Value = 0 Then
                ep.SetError(txtPageTo, "Please enter a valid page range")
                txtPageFrom.Focus()
                Return
            ElseIf txtPageTo.Value < txtPageFrom.Value Then
                ep.SetError(txtPageTo, "Please enter a valid page range")
                txtPageFrom.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Public Function _AddPrinter(ByVal sPrinter As String, Optional ByVal nDestinationID As Integer = 99999) As _
    Object()
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim oPrint As New clsPrinters
        Dim nPrinterID As Integer = clsMarsData.CreateDataID("printerattr", "printerid")
        Dim sReturn(2) As Object
        Dim sDriver As String
        Dim sPort As String

        If sPrinter.Contains("<[") = False Then
            txtPrinter.Text = oPrint.GetPrinterDetails(sPrinter)

            Try
                sDriver = txtPrinter.Text.Split(",")(1)
            Catch ex As Exception
                sDriver = String.Empty
            End Try

            Try
                sPort = txtPrinter.Text.Split(",")(2)
            Catch ex As Exception
                sPort = String.Empty
            End Try
        Else
            txtPrinter.Text = sPrinter
            sDriver = ""
            sPort = ""
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        Dim printerValue As String

        If txtPrinter.Text.Contains("<[") = True Then
            printerValue = txtPrinter.Text
            sDriver = ""
            sPort = ""
        Else
            printerValue = sPrinter
        End If

        sCols = "PrinterID,DestinationID,PrinterName,Orientation,PrinterDriver," & _
        "PrinterPort,PageFrom,PageTo,Copies,[Collate],PagesPerReport,PrintMethod," & _
        "PaperSize,PaperSource"

        sVals = nPrinterID & "," & _
        nDestinationID & "," & _
        "'" & SQLPrepare(printerValue) & "'," & _
        "''," & _
        "'" & SQLPrepare(sDriver) & "'," & _
        "'" & SQLPrepare(sPort) & "'," & _
        Me.txtPageFrom.Value & "," & _
        Me.txtPageTo.Value & "," & _
        txtCopies.Value & "," & _
        0 & "," & _
        0 & "," & _
        "'" & cmbPrintMethod.Text & "'," & _
        "''," & _
        "''"

        SQL = "INSERT INTO PrinterAttr (" & sCols & ") VALUES (" & sVals & ")"

        sReturn(0) = nPrinterID
        sReturn(1) = txtCopies.Value
        sReturn(2) = printerValue

        If clsMarsData.WriteData(SQL) = True Then Return sReturn Else Return Nothing
    End Function

    Public Function _EditPrinter(ByVal nPrinterID As Integer) As Hashtable
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sVals As String

        SQL = "SELECT * FROM PrinterAttr WHERE PrinterID =" & nPrinterID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                txtPrinter.Text = oRs("printername").Value & "," & oRs("printerdriver").Value & "," & oRs("printerport").Value
                txtCopies.Value = oRs("copies").Value
                cmbPrintMethod.Text = oRs("printmethod").Value
                txtPageFrom.Value = oRs("pagefrom").Value
                txtPageTo.Value = oRs("pageto").Value

                If txtPageFrom.Value > 0 Then
                    optPageRange.Checked = True
                End If
            End If

            oRs.Close()

            Me.ShowDialog()

            If UserCancel = True Then Return Nothing

            Dim printerName, printerDriver, printerPort As String

            printerName = txtPrinter.Text.Split(",")(0)

            Try
                printerDriver = txtPrinter.Text.Split(",")(1)
            Catch
                printerDriver = ""
            End Try

            Try
                printerPort = txtPrinter.Text.Split(",")(2)
            Catch
                printerPort = ""
            End Try

            sVals = "Orientation = ''," & _
            "PageFrom =" & Me.txtPageFrom.Value & "," & _
            "PageTo =" & Me.txtPageTo.Value & "," & _
            "Copies =" & txtCopies.Value & "," & _
            "[Collate] =" & 0 & "," & _
            "PagesPerReport =" & 0 & "," & _
            "PrintMethod =''," & _
            "PaperSize =''," & _
            "PaperSource = ''," & _
            "printername ='" & SQLPrepare(printerName) & "'," & _
            "printerdriver = '" & SQLPrepare(printerDriver) & "'," & _
            "printerport = '" & SQLPrepare(printerPort) & "'"


            SQL = "UPDATE PrinterAttr SET " & sVals & " WHERE PrinterID = " & nPrinterID

            clsMarsData.WriteData(SQL)

            Dim hs As New Hashtable
            hs.Add("Name", printerName)
            hs.Add("Copies", Me.txtCopies.Value)

            Return hs
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return Nothing
        End Try
    End Function

    Private Sub txtCopies_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCopies.ValueChanged
        If txtCopies.Value > 1 Then
            If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.p1_BulkPrinting) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                txtCopies.Value = 1
                Return
            End If
        End If
    End Sub



    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            Clipboard.SetText(cmb.SelectedText)

            cmb.SelectedText = ""
        End If
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            Clipboard.SetText(cmb.SelectedText)
        End If
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            cmb.SelectedText = Clipboard.GetText
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            cmb.SelectedText = ""
        End If
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            cmb.SelectAll()
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next

        Dim insert As frmInserter = New frmInserter(0)

        insert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        Dim dataItem As frmDataItems = New frmDataItems

        On Error Resume Next

        Dim ctrl As Control

        ctrl = Me.ActiveControl

        If TypeOf ctrl Is ComboBox Then
            Dim cmb As ComboBox = CType(ctrl, ComboBox)

            cmb.SelectedText = dataItem._GetDataItem(0)
        End If
    End Sub

    Private Sub optPageRange_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPageRange.CheckedChanged
        If optPageRange.Checked = True Then
            txtPageFrom.Enabled = True
            txtPageTo.Enabled = True
            If txtPageFrom.Value = 0 Then txtPageFrom.Value = 1
            If txtPageTo.Value = 0 Then txtPageTo.Value = 1
        Else
            txtPageFrom.Enabled = False
            txtPageTo.Enabled = False
            txtPageFrom.Value = 0
            txtPageTo.Value = 0
        End If
    End Sub

    Private Sub optAllPages_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAllPages.CheckedChanged

    End Sub
End Class
