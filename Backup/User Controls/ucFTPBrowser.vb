Imports Xceed.Ftp
Imports Xceed.FileSystem
Public Class ucFTPBrowser
    Inherits System.Windows.Forms.UserControl
    Public WithEvents oFTP As Xceed.Ftp.FtpClient
    Public sPath As String
    Public sFTPItem As String
    Public sFTPFolder As String
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbFTPType As System.Windows.Forms.ComboBox
    Friend WithEvents chkPassive As System.Windows.Forms.CheckBox
    Dim oUI As New clsMarsUI

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'Xceed.Ftp.Licenser.LicenseKey = "FTN10-Y4T6U-4BUKN-MAJA"

        oFTP = New Xceed.Ftp.FtpClient
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtServerAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents imgFTP As System.Windows.Forms.ImageList
    Friend WithEvents lsvFTP As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblPath As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucFTPBrowser))
        Me.txtServerAddress = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.lsvFTP = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.imgFTP = New System.Windows.Forms.ImageList(Me.components)
        Me.lblPath = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbFTPType = New System.Windows.Forms.ComboBox
        Me.chkPassive = New System.Windows.Forms.CheckBox
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtServerAddress
        '
        Me.txtServerAddress.Location = New System.Drawing.Point(112, 8)
        Me.txtServerAddress.Name = "txtServerAddress"
        Me.txtServerAddress.Size = New System.Drawing.Size(264, 21)
        Me.txtServerAddress.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 16)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "FTP Server Address"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 34)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "User Name"
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(112, 32)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(144, 21)
        Me.txtUserName.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 16)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Password"
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(112, 56)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(144, 21)
        Me.txtPassword.TabIndex = 2
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(344, 32)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(32, 21)
        Me.txtPort.TabIndex = 3
        Me.txtPort.Text = "21"
        Me.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(264, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Port Number"
        '
        'cmdConnect
        '
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(267, 86)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(80, 21)
        Me.cmdConnect.TabIndex = 4
        Me.cmdConnect.Text = "Connect..."
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'lsvFTP
        '
        Me.lsvFTP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvFTP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader4})
        Me.lsvFTP.HideSelection = False
        Me.lsvFTP.Location = New System.Drawing.Point(8, 134)
        Me.lsvFTP.Name = "lsvFTP"
        Me.lsvFTP.Size = New System.Drawing.Size(368, 167)
        Me.lsvFTP.SmallImageList = Me.imgFTP
        Me.lsvFTP.TabIndex = 5
        Me.lsvFTP.UseCompatibleStateImageBehavior = False
        Me.lsvFTP.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 132
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Date"
        Me.ColumnHeader2.Width = 122
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Size"
        '
        'imgFTP
        '
        Me.imgFTP.ImageStream = CType(resources.GetObject("imgFTP.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFTP.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFTP.Images.SetKeyName(0, "")
        Me.imgFTP.Images.SetKeyName(1, "")
        Me.imgFTP.Images.SetKeyName(2, "")
        Me.imgFTP.Images.SetKeyName(3, "")
        '
        'lblPath
        '
        Me.lblPath.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lblPath.Location = New System.Drawing.Point(0, 307)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.ReadOnly = True
        Me.lblPath.Size = New System.Drawing.Size(384, 21)
        Me.lblPath.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 86)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "FTP Type"
        '
        'cmbFTPType
        '
        Me.cmbFTPType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFTPType.FormattingEnabled = True
        Me.cmbFTPType.Items.AddRange(New Object() {"FTP", "FTP - SSL 3.1 (TLS)", "FTP - SSL 3.0"})
        Me.cmbFTPType.Location = New System.Drawing.Point(112, 84)
        Me.cmbFTPType.Name = "cmbFTPType"
        Me.cmbFTPType.Size = New System.Drawing.Size(144, 21)
        Me.cmbFTPType.TabIndex = 10
        '
        'chkPassive
        '
        Me.chkPassive.AutoSize = True
        Me.chkPassive.Location = New System.Drawing.Point(112, 111)
        Me.chkPassive.Name = "chkPassive"
        Me.chkPassive.Size = New System.Drawing.Size(112, 17)
        Me.chkPassive.TabIndex = 31
        Me.chkPassive.Text = "Use Passive Mode"
        Me.chkPassive.UseVisualStyleBackColor = True
        '
        'ucFTPBrowser
        '
        Me.Controls.Add(Me.chkPassive)
        Me.Controls.Add(Me.cmbFTPType)
        Me.Controls.Add(Me.lblPath)
        Me.Controls.Add(Me.lsvFTP)
        Me.Controls.Add(Me.cmdConnect)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtUserName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtPort)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtServerAddress)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucFTPBrowser"
        Me.Size = New System.Drawing.Size(384, 328)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If cmdConnect.Text = "Connect..." Then

            If txtServerAddress.Text.Length = 0 Then
                ep.SetError(txtServerAddress, "Please enter the FTP Server address")
                txtServerAddress.Focus()
                Return
            ElseIf txtUserName.Text.Length = 0 Then
                ep.SetError(txtUserName, "Please provide the username")
                txtUserName.Focus()
                Return
            ElseIf txtPassword.Text.Length = 0 Then
                ep.SetError(txtPassword, "Please enter the password")
                txtPassword.Focus()
                Return
            End If


            If Connect() = True Then
                lblPath.Text = sPath
                cmdConnect.Text = "Disconnect..."
            End If

        Else
            Disconnect()
            lsvFTP.Items.Clear()

            sPath = ""
            lblPath.Text = ""
            cmdConnect.Text = "Connect..."
        End If

    End Sub

    Public Function Connect() As Boolean
        Try
            Dim oUI As New clsMarsUI
            Dim nPort As Int32 = 0

            
            If oFTP.Connected = True Then
                oFTP.Disconnect()
            End If

            oUI.BusyProgress(25, "Connecting to FTP Server...")

            Try
                nPort = Int32.Parse(txtPort.Text)
            Catch ex As Exception
                nPort = 21
            End Try

            'FTP
            'FTP - SSL 3.1 (TLS)
            'FTP - SSL 3.0

            Select Case cmbFTPType.Text
                Case "FTP"
                    oFTP.Connect(txtServerAddress.Text, nPort)
                Case "FTP - SSL 3.1 (TLS)"
                    oFTP.Connect(txtServerAddress.Text, AuthenticationMethod.Tls, VerificationFlags.None, Nothing)
                Case "FTP - SSL 3.0"
                    oFTP.Connect(txtServerAddress.Text, AuthenticationMethod.Ssl, VerificationFlags.None, Nothing)
            End Select

            oUI.BusyProgress(50, "Connected, logging in...")

            oFTP.PassiveTransfer = Me.chkPassive.Checked

            If oFTP.Connected = True Then
                oFTP.Login(txtUserName.Text, txtPassword.Text)
            End If

            oUI.BusyProgress(75, "Retrieving folder contents...")

            GetDirContent()

            oUI.BusyProgress(90, "Caching folder listing...")

            oUI.BusyProgress(100, , True)

            cmdConnect.Text = "Disconnect..."

            Return True
        Catch ex As Exception
            _ErrorHandle("Could not connect to the specified FTP Server.", _
            Err.Number, "ucFTPBrowser.Connect", _GetLineNumber(ex.StackTrace), _
            "Please check the provided port number and provided credentials")
            Return False
        End Try
    End Function


    Public Sub Disconnect()
        Try

            oFTP.Disconnect()

            cmdConnect.Text = "Connect..."
        Catch:End Try
    End Sub

    Public Sub GetDirContent()

        Dim nImage As Int32 = 0
        Dim sTag As String

        lsvFTP.Items.Clear()

       
        Dim oItems As FtpItemInfoList
        Dim oItem As FtpItemInfo

        oItems = oFTP.GetFolderContents()

        lsvFTP.Items.Add("...", 0)

        lsvFTP.Items(0).Tag = "Up"

        For Each oItem In oItems
            If oItem.Type = FtpItemType.Folder Or oItem.Type = FtpItemType.Link Then
                nImage = 1
                sTag = "Folder"
            Else
                nImage = 3
                sTag = "File"
            End If

            Dim oListItem As ListViewItem = New ListViewItem

            With oListItem
                .Text = oItem.Name
                .Tag = sTag
                .ImageIndex = nImage

                With .SubItems
                    .Add(oItem.DateTime)
                    .Add(oItem.Size)
                End With
            End With

            lsvFTP.Items.Add(oListItem)
        Next

    End Sub

    Private Sub txtServerAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServerAddress.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub lsvFTP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvFTP.SelectedIndexChanged
        If lsvFTP.SelectedItems.Count = 0 Then Return

        Dim lsvItem As ListViewItem = lsvFTP.SelectedItems(0)

        If lsvItem.Tag = "File" Then
            sFTPItem = lsvItem.Text
            sFTPFolder = ""
        ElseIf lsvItem.Tag = "Folder" Then
            sFTPFolder = lsvItem.Text
            sFTPItem = ""
        Else
            sFTPItem = ""
            sFTPFolder = ""
        End If
    End Sub

    Private Sub lsvFTP_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvFTP.DoubleClick
        If lsvFTP.SelectedItems.Count = 0 Then Return

        If oFTP.Connected = False Then
            MessageBox.Show("CRD is not connected to the FTP Server", _
            Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Warning)
            Return
        End If

        Dim oListItem As ListViewItem = lsvFTP.SelectedItems(0)

        If oListItem.Tag = "Folder" Then
            oFTP.ChangeCurrentFolder(oListItem.Text)

            sPath &= oListItem.Text & "/"

            lblPath.Text = sPath

            GetDirContent()
        ElseIf oListItem.Tag = "Up" Then
            Try
                oFTP.ChangeToParentFolder()

                Dim sFolders As String() = sPath.Split("/")

                sPath = ""

                For I As Int32 = 0 To sFolders.GetUpperBound(0) - 2
                    sPath &= sFolders(I) & "/"
                Next

                lblPath.Text = sPath

                GetDirContent()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try
        End If

    End Sub

    Private Sub ucFTPBrowser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub oFTP_FileTransferStatus(ByVal sender As Object, ByVal e As Xceed.Ftp.FileTransferStatusEventArgs) Handles oFTP.FileTransferStatus
        oUI.BusyProgress(e.AllBytesPercent, "Transfering files...")
    End Sub

    
    'Private Sub cmbFTPType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFTPType.SelectedIndexChanged
    '    If cmbFTPType.Text <> "FTP" Then
    '        If gnEdition < gEdition.ENTERPRISEPROPLUS Then
    '            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
    '            cmbFTPType.SelectedIndex = 0
    '            Return
    '        End If
    '    End If
    'End Sub
End Class
