Public Class frmEditDataItem
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Dim sMode As String
    Friend WithEvents chkDefault As System.Windows.Forms.CheckBox
    Friend WithEvents txtDefault As System.Windows.Forms.TextBox
    Friend WithEvents chkNoRecords As System.Windows.Forms.CheckBox
    Friend WithEvents txtNoRecords As System.Windows.Forms.TextBox
    Friend WithEvents tb As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Public m_eventID As Integer = 99999
    Public m_ParameterList As ArrayList
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSep As System.Windows.Forms.TextBox
    Friend WithEvents cmdBuild As System.Windows.Forms.Button
    Friend WithEvents chkMulti As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdParse As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditDataItem))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.chkMulti = New System.Windows.Forms.CheckBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.UcDSN = New sqlrd.ucDSN
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdBuild = New System.Windows.Forms.Button
        Me.cmdParse = New System.Windows.Forms.Button
        Me.txtSep = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkNoRecords = New System.Windows.Forms.CheckBox
        Me.chkDefault = New System.Windows.Forms.CheckBox
        Me.txtNoRecords = New System.Windows.Forms.TextBox
        Me.txtDefault = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tb = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tb.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Data Item Name"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(8, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(344, 21)
        Me.txtName.TabIndex = 0
        '
        'txtQuery
        '
        Me.txtQuery.Location = New System.Drawing.Point(8, 24)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(328, 104)
        Me.txtQuery.TabIndex = 0
        Me.txtQuery.Tag = "memo"
        '
        'cmdConnect
        '
        Me.cmdConnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(144, 184)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 23)
        Me.cmdConnect.TabIndex = 5
        Me.cmdConnect.Text = "Connect"
        '
        'chkMulti
        '
        Me.chkMulti.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMulti.Location = New System.Drawing.Point(6, 7)
        Me.chkMulti.Name = "chkMulti"
        Me.chkMulti.Size = New System.Drawing.Size(256, 40)
        Me.chkMulti.TabIndex = 6
        Me.chkMulti.Text = "Allow multiple values to be returned by query. Values will be separated using "
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(168, 144)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 21)
        Me.TextBox3.TabIndex = 5
        Me.TextBox3.Text = "TextBox3"
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 64)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(360, 112)
        Me.UcDSN.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtQuery)
        Me.GroupBox1.Controls.Add(Me.cmdBuild)
        Me.GroupBox1.Controls.Add(Me.cmdParse)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 168)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Query"
        '
        'cmdBuild
        '
        Me.cmdBuild.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBuild.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBuild.Location = New System.Drawing.Point(8, 136)
        Me.cmdBuild.Name = "cmdBuild"
        Me.cmdBuild.Size = New System.Drawing.Size(75, 23)
        Me.cmdBuild.TabIndex = 1
        Me.cmdBuild.Text = "Build"
        '
        'cmdParse
        '
        Me.cmdParse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdParse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParse.Location = New System.Drawing.Point(88, 136)
        Me.cmdParse.Name = "cmdParse"
        Me.cmdParse.Size = New System.Drawing.Size(75, 23)
        Me.cmdParse.TabIndex = 2
        Me.cmdParse.Text = "Parse"
        '
        'txtSep
        '
        Me.txtSep.Enabled = False
        Me.txtSep.Location = New System.Drawing.Point(290, 15)
        Me.txtSep.Name = "txtSep"
        Me.txtSep.Size = New System.Drawing.Size(94, 21)
        Me.txtSep.TabIndex = 7
        Me.txtSep.Text = ";"
        Me.ToolTip1.SetToolTip(Me.txtSep, "user <sqlrdcrlf> for carriage return")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdConnect)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.UcDSN)
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.TextBox3)
        Me.GroupBox2.Controls.Add(Me.txtName)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(366, 382)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'chkNoRecords
        '
        Me.chkNoRecords.AutoSize = True
        Me.chkNoRecords.Location = New System.Drawing.Point(6, 85)
        Me.chkNoRecords.Name = "chkNoRecords"
        Me.chkNoRecords.Size = New System.Drawing.Size(278, 17)
        Me.chkNoRecords.TabIndex = 8
        Me.chkNoRecords.Text = "If no records found then return the default value of "
        Me.chkNoRecords.UseVisualStyleBackColor = True
        '
        'chkDefault
        '
        Me.chkDefault.AutoSize = True
        Me.chkDefault.Location = New System.Drawing.Point(6, 51)
        Me.chkDefault.Name = "chkDefault"
        Me.chkDefault.Size = New System.Drawing.Size(249, 17)
        Me.chkDefault.TabIndex = 8
        Me.chkDefault.Text = "Replace NULL values with the default value of "
        Me.chkDefault.UseVisualStyleBackColor = True
        '
        'txtNoRecords
        '
        Me.txtNoRecords.Enabled = False
        Me.txtNoRecords.Location = New System.Drawing.Point(290, 81)
        Me.txtNoRecords.Name = "txtNoRecords"
        Me.txtNoRecords.Size = New System.Drawing.Size(94, 21)
        Me.txtNoRecords.TabIndex = 7
        '
        'txtDefault
        '
        Me.txtDefault.Enabled = False
        Me.txtDefault.Location = New System.Drawing.Point(290, 49)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.Size = New System.Drawing.Size(94, 21)
        Me.txtDefault.TabIndex = 7
        '
        'cmdOK
        '
        Me.cmdOK.Enabled = False
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(261, 417)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(72, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(341, 417)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(72, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'tb
        '
        Me.tb.Controls.Add(Me.TabPage1)
        Me.tb.Controls.Add(Me.TabPage2)
        Me.tb.Location = New System.Drawing.Point(3, 2)
        Me.tb.Name = "tb"
        Me.tb.SelectedIndex = 0
        Me.tb.Size = New System.Drawing.Size(410, 413)
        Me.tb.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(402, 387)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Definition"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(402, 387)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Options"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkNoRecords)
        Me.GroupBox3.Controls.Add(Me.chkMulti)
        Me.GroupBox3.Controls.Add(Me.chkDefault)
        Me.GroupBox3.Controls.Add(Me.txtSep)
        Me.GroupBox3.Controls.Add(Me.txtDefault)
        Me.GroupBox3.Controls.Add(Me.txtNoRecords)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 3)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(390, 378)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'frmEditDataItem
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(415, 444)
        Me.ControlBox = False
        Me.Controls.Add(Me.tb)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.Name = "frmEditDataItem"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data Item"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tb.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If UcDSN._Validate = True Then
            GroupBox1.Enabled = True

            If txtQuery.Text.Length = 0 Then
                Me.cmdBuild_Click(Nothing, Nothing)
            End If
        Else
            GroupBox1.Enabled = False
        End If
    End Sub

    Private Sub chkMulti_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMulti.CheckedChanged
        txtSep.Enabled = chkMulti.Checked
    End Sub

    Private Sub frmEditDataItem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBuild.Click
        Dim sReturn(1) As String
        Dim oReturn As New frmDynamicParameter

        oReturn.m_eventID = Me.m_eventID

        oReturn.m_ParameterList = Me.m_ParameterList

        With UcDSN
            sReturn = oReturn.ReturnSQLQuery(.cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text & "|", txtQuery.Text)

            If Not sReturn Is Nothing Then
                .cmbDSN.Text = sReturn(0).Split("|")(0)
                .txtUserID.Text = sReturn(0).Split("|")(1)
                .txtPassword.Text = sReturn(0).Split("|")(2)

                txtQuery.Text = sReturn(1)

                cmdOK.Enabled = True
            End If
        End With
        
    End Sub

    Private Sub cmdParse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParse.Click
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        If txtQuery.Text.Contains("<[d") Or txtQuery.Text.Contains("<[p") Or txtQuery.Text.Contains("<[e") Or _
        txtQuery.Text.Contains("<[r") Or txtQuery.Text.Contains("<[c") Then
            MessageBox.Show("SQL parsed successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = True
            Return
        End If

        Try
            oCon.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oRes As DialogResult = MessageBox.Show("SQL parsed successfully! View Results?", _
            Application.ProductName, MessageBoxButtons.YesNo, _
            MessageBoxIcon.Question)

            If oRes = Windows.Forms.DialogResult.Yes Then
                Dim oResult As New frmDBResults

                oResult._ShowResults(oRs)
            End If

            cmdOK.Enabled = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            cmdOK.Enabled = False
        End Try
    End Sub

    Private Sub txtQuery_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtQuery.TextChanged
        cmdOK.Enabled = False
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter the name of the data item")
            txtName.Focus()
            Me.tb.SelectTab(0)
            Return
        ElseIf clsMarsData.IsDuplicate("DataItems", "ItemName", txtName.Text, False) And sMode = "Add" Then
            ep.SetError(txtName, "A data item with this name already exists in the system")
            txtName.Focus()
            Me.tb.SelectTab(0)
            Return
        ElseIf txtQuery.Text.Length = 0 Then
            ep.SetError(txtQuery, "Please provide the SQL query for the data item")
            txtQuery.Focus()
            Me.tb.SelectTab(0)
            Return
        ElseIf UcDSN.cmbDSN.Text.Length = 0 Then
            ep.SetError(UcDSN.cmbDSN, "Please select the DSN name")
            UcDSN.cmbDSN.Focus()
            Me.tb.SelectTab(0)
            Return
        ElseIf chkMulti.Checked And txtSep.Text.Length = 0 Then
            ep.SetError(txtSep, "Please provide the value seperator for multiple records returned")
            txtSep.Focus()
            Me.tb.SelectTab(1)
            Return
        End If

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Public Function AddDataItem() As String

        sMode = "Add"

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim sCon As String

        With UcDSN
            sCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text & "|"
        End With


        '        ReplaceNull         INTEGER;
        'ALTER TABLE DataItems   ADD     ReplaceNullValue    VARCHAR(255);
        'ALTER TABLE DataItems   ADD     ReplaceEOF          INTEGER;
        'ALTER TABLE DataItems   ADD     REplaceEOFValue     VARCHAR(255);

        sCols = "ItemID,ItemName,ConString,SQLQuery,AllowMultiple,MultipleSep," & _
        "ReplaceNull,ReplaceNullValue,ReplaceEOF,ReplaceEOFValue"

        sVals = clsMarsData.CreateDataID("dataitems", "itemid") & "," & _
        "'" & SQLPrepare(txtName.Text) & "', " & _
        "'" & SQLPrepare(sCon) & "'," & _
        "'" & SQLPrepare(txtQuery.Text) & "'," & _
        Convert.ToInt32(chkMulti.Checked) & "," & _
        "'" & SQLPrepare(txtSep.Text) & "'," & _
        Convert.ToInt32(Me.chkDefault.Checked) & "," & _
        "'" & SQLPrepare(Me.txtDefault.Text) & "'," & _
        Convert.ToInt32(Me.chkNoRecords.Checked) & "," & _
        "'" & SQLPrepare(Me.txtNoRecords.Text) & "'"

        SQL = "INSERT INTO DataItems (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Return txtName.Text
        Else
            Return Nothing
        End If

    End Function

    Public Function EditDataItems(ByVal nID As Integer) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sCon As String

        SQL = "SELECT * FROM DataItems WHERE ItemID = " & nID

        oRs = clsmarsdata.GetData(SQL)

        If oRs Is Nothing Then Return Nothing

        If oRs.EOF = True Then
            Return Nothing
        Else
            txtName.Text = oRs("itemname").Value
            sCon = oRs("constring").Value
            txtQuery.Text = oRs("sqlquery").Value

            Try
                chkMulti.Checked = Convert.ToBoolean(oRs("allowmultiple").Value)
                txtSep.Text = IsNull(oRs("multiplesep").Value, ";")
            Catch
                chkMulti.Checked = False
            End Try

            With UcDSN
                .cmbDSN.Text = sCon.Split("|")(0)
                .txtUserID.Text = sCon.Split("|")(1)
                .txtPassword.Text = sCon.Split("|")(2)
            End With


            Me.chkDefault.Checked = IsNull(oRs("replacenull").Value, 0)
            Me.txtDefault.Text = IsNull(oRs("replacenullvalue").Value, "")

            Me.chkNoRecords.Checked = IsNull(oRs("replaceeof").Value, 0)
            Me.txtNoRecords.Text = IsNull(oRs("replaceeofvalue").Value, "")
        End If

        cmdOK.Enabled = True

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        With UcDSN
            sCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text & "|"
        End With

        SQL = "ItemName = '" & SQLPrepare(txtName.Text) & "'," & _
        "ConString = '" & SQLPrepare(sCon) & "'," & _
        "SQLQuery= '" & SQLPrepare(txtQuery.Text) & "'," & _
        "AllowMultiple = " & Convert.ToInt32(chkMulti.Checked) & "," & _
        "MultipleSep = '" & SQLPrepare(txtSep.Text) & "'," & _
        "ReplaceNull =" & Convert.ToInt32(Me.chkDefault.Checked) & "," & _
        "ReplaceNullValue = '" & SQLPrepare(Me.txtDefault.Text) & "'," & _
        "ReplaceEOF =" & Convert.ToInt32(Me.chkNoRecords.Checked) & "," & _
        "ReplaceEOFValue ='" & SQLPrepare(Me.txtNoRecords.Text) & "'"

        SQL = "UPDATE DataItems SET " & SQL & " WHERE ItemID = " & nID

        If clsMarsData.WriteData(SQL) = True Then
            Return txtName.Text
        Else
            Return Nothing
        End If

    End Function

    Private Sub chkDefault_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDefault.CheckedChanged
        Me.txtDefault.Enabled = Me.chkDefault.Checked
    End Sub

    Private Sub chkNoRecords_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNoRecords.CheckedChanged
        Me.txtNoRecords.Enabled = Me.chkNoRecords.Checked
    End Sub

End Class
