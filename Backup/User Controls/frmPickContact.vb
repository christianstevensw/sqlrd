Public Class frmPickContact
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
    Public sMode As String = "Email"
    Dim oField As TextBox
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdTo As System.Windows.Forms.Button
    Friend WithEvents cmdCc As System.Windows.Forms.Button
    Friend WithEvents cmdBcc As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents lsvContacts As System.Windows.Forms.ListView
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents txtCc As System.Windows.Forms.TextBox
    Friend WithEvents txtBcc As System.Windows.Forms.TextBox
    Friend WithEvents cmdNew As System.Windows.Forms.Button
    Friend WithEvents mnuNew As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuContact As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuGroup As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPickContact))
        Me.lsvContacts = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmdTo = New System.Windows.Forms.Button
        Me.cmdCc = New System.Windows.Forms.Button
        Me.cmdBcc = New System.Windows.Forms.Button
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.txtCc = New System.Windows.Forms.TextBox
        Me.txtBcc = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdNew = New System.Windows.Forms.Button
        Me.mnuNew = New System.Windows.Forms.ContextMenu
        Me.mnuContact = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.mnuGroup = New System.Windows.Forms.MenuItem
        Me.SuspendLayout()
        '
        'lsvContacts
        '
        Me.lsvContacts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvContacts.FullRowSelect = True
        Me.lsvContacts.HideSelection = False
        Me.lsvContacts.LargeImageList = Me.ImageList1
        Me.lsvContacts.Location = New System.Drawing.Point(8, 56)
        Me.lsvContacts.Name = "lsvContacts"
        Me.lsvContacts.Size = New System.Drawing.Size(392, 224)
        Me.lsvContacts.SmallImageList = Me.ImageList1
        Me.lsvContacts.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lsvContacts.TabIndex = 0
        Me.lsvContacts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Display Name"
        Me.ColumnHeader1.Width = 192
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Addresses"
        Me.ColumnHeader2.Width = 187
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Type in or select name from the list"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(8, 24)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(192, 21)
        Me.txtName.TabIndex = 2
        Me.txtName.Text = ""
        '
        'cmdTo
        '
        Me.cmdTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTo.Image = CType(resources.GetObject("cmdTo.Image"), System.Drawing.Image)
        Me.cmdTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(8, 315)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(48, 21)
        Me.cmdTo.TabIndex = 3
        Me.cmdTo.Text = "To"
        Me.cmdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdCc
        '
        Me.cmdCc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCc.Image = CType(resources.GetObject("cmdCc.Image"), System.Drawing.Image)
        Me.cmdCc.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCc.Location = New System.Drawing.Point(8, 347)
        Me.cmdCc.Name = "cmdCc"
        Me.cmdCc.Size = New System.Drawing.Size(48, 21)
        Me.cmdCc.TabIndex = 3
        Me.cmdCc.Text = "Cc"
        Me.cmdCc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdBcc
        '
        Me.cmdBcc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBcc.Image = CType(resources.GetObject("cmdBcc.Image"), System.Drawing.Image)
        Me.cmdBcc.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdBcc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBcc.Location = New System.Drawing.Point(8, 379)
        Me.cmdBcc.Name = "cmdBcc"
        Me.cmdBcc.Size = New System.Drawing.Size(48, 21)
        Me.cmdBcc.TabIndex = 3
        Me.cmdBcc.Text = "Bcc"
        Me.cmdBcc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTo
        '
        Me.txtTo.Location = New System.Drawing.Point(64, 315)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(336, 21)
        Me.txtTo.TabIndex = 2
        Me.txtTo.Text = ""
        '
        'txtCc
        '
        Me.txtCc.Location = New System.Drawing.Point(64, 347)
        Me.txtCc.Name = "txtCc"
        Me.txtCc.Size = New System.Drawing.Size(336, 21)
        Me.txtCc.TabIndex = 2
        Me.txtCc.Text = ""
        '
        'txtBcc
        '
        Me.txtBcc.Location = New System.Drawing.Point(64, 379)
        Me.txtBcc.Name = "txtBcc"
        Me.txtBcc.Size = New System.Drawing.Size(336, 21)
        Me.txtBcc.TabIndex = 2
        Me.txtBcc.Text = ""
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(96, 296)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(304, 8)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 288)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(104, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Message Recipients"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(8, 405)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 8)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "3dline"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(240, 424)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(328, 424)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdNew
        '
        Me.cmdNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNew.Image = CType(resources.GetObject("cmdNew.Image"), System.Drawing.Image)
        Me.cmdNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdNew.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNew.Location = New System.Drawing.Point(216, 24)
        Me.cmdNew.Name = "cmdNew"
        Me.cmdNew.Size = New System.Drawing.Size(75, 21)
        Me.cmdNew.TabIndex = 7
        Me.cmdNew.Text = "New..."
        '
        'mnuNew
        '
        Me.mnuNew.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuContact, Me.MenuItem2, Me.mnuGroup})
        Me.mnuNew.RightToLeft = System.Windows.Forms.RightToLeft.No
        '
        'mnuContact
        '
        Me.mnuContact.Index = 0
        Me.mnuContact.Text = "&Contact"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnuGroup
        '
        Me.mnuGroup.Index = 2
        Me.mnuGroup.Text = "&Group"
        '
        'frmPickContact
        '
        Me.AcceptButton = Me.cmdTo
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(410, 456)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdNew)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdTo)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lsvContacts)
        Me.Controls.Add(Me.cmdCc)
        Me.Controls.Add(Me.cmdBcc)
        Me.Controls.Add(Me.txtTo)
        Me.Controls.Add(Me.txtCc)
        Me.Controls.Add(Me.txtBcc)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.MaximizeBox = False
        Me.Name = "frmPickContact"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Contacts"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmPickContact_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Function PickContact(ByVal sField As String) As String()
        LoadContacts()

        Select Case sField.ToLower
            Case "to"
                oField = txtTo
            Case "cc"
                oField = txtCc
            Case "bcc"
                oField = txtBcc
        End Select

        If sMode = "Fax" Or sMode = "SMS" Then
            txtCc.Enabled = False
            txtBcc.Enabled = False
            cmdBcc.Enabled = False
            cmdCc.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        Dim sReturn(2) As String

        sReturn(0) = txtTo.Text
        sReturn(1) = txtCc.Text
        sReturn(2) = txtBcc.Text

        Return sReturn
    End Function
    Private Sub LoadContacts()
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        'add groups

        lsvContacts.Items.Clear()

        SQL = "SELECT * FROM ContactAttr WHERE ContactType = 'group'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oItem As New ListViewItem

                oItem.Text = oRs("contactname").Value
                oItem.ImageIndex = 2

                lsvContacts.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        'add contacts

        SQL = "SELECT * FROM ContactAttr c INNER JOIN ContactDetail d ON " & _
        "c.ContactID = d.ContactID WHERE c.ContactType = 'contact'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oItem As New ListViewItem

                oitem.Text = oRs("contactname").Value

                If sMode = "Email" Then
                    oitem.SubItems.Add(oRs("emailaddress").Value)
                Else
                    oitem.SubItems.Add(oRs("faxnumber").Value)
                End If

                oitem.ImageIndex = 1

                lsvContacts.Items.Add(oitem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        Dim s As String = txtName.Text
        Dim n As Integer = s.Length

        Try
            If lsvContacts.Items.Count = 0 Then Return

            For Each oItem As ListViewItem In lsvContacts.Items
                oitem.Selected = False
            Next

            If s.Length = 0 Then Return

            For Each oItem As ListViewItem In lsvContacts.Items

                If oItem.Text.Substring(0, n).ToLower = s.ToLower Then
                    oItem.Selected = True
                    oItem.EnsureVisible()
                    Exit For
                End If
            Next
        Catch : End Try
    End Sub


    Private Sub lsvContacts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvContacts.DoubleClick
        If lsvContacts.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvContacts.SelectedItems(0)

        If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
        oField.Text &= ";"

        If sMode = "Email" Then
            oField.Text &= "<[a]" & oItem.Text & ">;"
        ElseIf sMode = "Fax" Then
            oField.Text &= "<[z]" & oItem.Text & ">;"
        ElseIf sMode = "SMS" Then
            oField.Text &= "<[y]" & oItem.Text & ">;"
        End If
    End Sub

    Private Sub cmdTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTo.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
            Then txtTo.Text &= ";"

            If sMode = "Email" Then
                txtTo.Text &= "<[a]" & oItem.Text & ">;"
            ElseIf sMode = "Fax" Then
                txtTo.Text &= "<[z]" & oItem.Text & ">;"
            ElseIf sMode = "SMS" Then
                txtTo.Text &= "<[y]" & oItem.Text & ">;"
            End If
        Next
    End Sub

    Private Sub cmdCc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCc.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtCc.Text.EndsWith(";") = False And txtCc.Text.Length > 0 _
            Then txtCc.Text &= ";"

            txtCc.Text &= "<[a]" & oItem.Text & ">;"
        Next
    End Sub

    Private Sub cmdBcc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBcc.Click
        If lsvContacts.SelectedItems.Count = 0 Then Return

        For Each oItem As ListViewItem In lsvContacts.SelectedItems

            If txtBcc.Text.EndsWith(";") = False And txtBcc.Text.Length > 0 _
            Then txtBcc.Text &= ";"

            txtBcc.Text &= "<[a]" & oItem.Text & ">;"
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub




    Private Sub mnuContact_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuContact.Click
        Dim oContact As New frmAddContact
        Dim s As String = oContact.AddContact()

        If s.Length = 0 Then Return

        LoadContacts()

        For Each oitem As ListViewItem In lsvContacts.Items
            If oitem.Text.ToLower = s Then
                oitem.Selected = True
                oitem.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub mnuGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuGroup.Click
        Dim oContact As New frmAddContact
        Dim s As String = oContact.AddGroup()

        If s.Length = 0 Then Return

        LoadContacts()

        For Each oitem As ListViewItem In lsvContacts.Items
            If oitem.Text.ToLower = s Then
                oitem.Selected = True
                oitem.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub cmdNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNew.Click

    End Sub

    Private Sub cmdNew_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdNew.MouseUp
        mnuNew.Show(cmdNew, New Point(e.X, e.Y))
    End Sub

    Private Sub lsvContacts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvContacts.SelectedIndexChanged

    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged

    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.GotFocus
        oField = txtTo
    End Sub

    Private Sub txtCc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCc.TextChanged

    End Sub

    Private Sub txtCc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCc.GotFocus
        oField = txtCc
    End Sub

    Private Sub txtBcc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBcc.TextChanged

    End Sub

    Private Sub txtBcc_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtBcc.GotFocus
        oField = txtBcc
    End Sub
End Class
