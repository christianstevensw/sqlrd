#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmDestinationSelect
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean = True
    Dim oField As TextBox
    Dim sField As String = "to"
    Dim Dynamic As Boolean
    Dim Package As Boolean
    Dim DataDriven As Boolean
    Dim Bursting As Boolean
    Dim IsSmart As Boolean
    Dim sTitle As String = ""
    Dim xDestinationID As Integer = 99999
    Dim xReportID As Integer = 99999
    Public oRpt As Object
    Public IsStatic As Boolean = False
    Dim oSec As New clsMarsSecurity
    Dim sMode As String = "Add"
    Dim formLoaded As Boolean = False
    Dim ep As New ErrorProvider
    Dim IsQuery As Boolean
    Dim spServer, spUsername, spPassword, spLibrary As String
#Region "Hide"
    Friend WithEvents grpODBC As System.Windows.Forms.GroupBox
    Friend WithEvents txtTableName As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents btnODBC As System.Windows.Forms.Button
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents pnReadReciepts As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents pnMailServer As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkReadReceipt As System.Windows.Forms.CheckBox
    Friend WithEvents btnReadReceipts As System.Windows.Forms.Button
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Dim oNet As New clsNetworking
    Dim eventBased As Boolean = False
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvPaths As System.Windows.Forms.ListView
    Friend WithEvents btnAddPath As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnRemovePath As System.Windows.Forms.Button
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents btnRemoveFTP As System.Windows.Forms.Button
    Friend WithEvents btnAddFTP As System.Windows.Forms.Button
    Friend WithEvents lsvFTP As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkOverwrite As System.Windows.Forms.CheckBox
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    Public eventID As Integer = 99999
    Friend WithEvents chkUseKeyPar As System.Windows.Forms.CheckBox
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents UcFTP As sqlrd.ucFTPDetails
    Friend WithEvents grpSender As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtSenderName As System.Windows.Forms.TextBox
    Friend WithEvents txtSenderAddress As System.Windows.Forms.TextBox
    Public m_ParamterList As ArrayList = Nothing
    Friend WithEvents grpSharePoint As System.Windows.Forms.GroupBox
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents lsvSharePointLibs As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
#End Region
    Public disableEmbed As Boolean

    Property m_IsQuery() As Boolean
        Get
            Return Me.IsQuery
        End Get
        Set(ByVal value As Boolean)
            IsQuery = value
        End Set
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpEmail As System.Windows.Forms.GroupBox
    Friend WithEvents txtMsg As System.Windows.Forms.TextBox
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents cmdTo As System.Windows.Forms.Button
    Friend WithEvents cmdCC As System.Windows.Forms.Button
    Friend WithEvents cmdBcc As System.Windows.Forms.Button
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents txtBCC As System.Windows.Forms.TextBox
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtAttach As System.Windows.Forms.TextBox
    Friend WithEvents cmdAttach As System.Windows.Forms.Button
    Friend WithEvents grpPrinter As System.Windows.Forms.GroupBox
    Friend WithEvents lsvPrinters As System.Windows.Forms.ListView
    Friend WithEvents cmdAddPrinter As System.Windows.Forms.Button
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbPrinters As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRemovePrinter As System.Windows.Forms.Button
    Friend WithEvents grpFile As System.Windows.Forms.GroupBox
    Friend WithEvents txtDirectory As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents grpFTP As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbDestination As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents ofg As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents tbDestination As System.Windows.Forms.TabPage
    Friend WithEvents tbOutput As System.Windows.Forms.TabPage
    Friend WithEvents tbNaming As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkIncludeAttach As System.Windows.Forms.CheckBox
    Friend WithEvents cmbFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents chkEmbed As System.Windows.Forms.CheckBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkCustomExt As System.Windows.Forms.CheckBox
    Friend WithEvents txtCustomName As System.Windows.Forms.TextBox
    Friend WithEvents optNaming As System.Windows.Forms.RadioButton
    Friend WithEvents optCustomName As System.Windows.Forms.RadioButton
    Friend WithEvents txtCustomExt As System.Windows.Forms.TextBox
    Friend WithEvents chkAppendDateTime As System.Windows.Forms.CheckBox
    Friend WithEvents tabDestination As System.Windows.Forms.TabControl
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkZip As System.Windows.Forms.CheckBox
    Friend WithEvents cmdInsert As System.Windows.Forms.Button
    Friend WithEvents cmdInsert2 As System.Windows.Forms.Button
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbMailFormat As System.Windows.Forms.ComboBox
    Friend WithEvents chkSplit As System.Windows.Forms.CheckBox
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSignature As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents txtDefault As System.Windows.Forms.TextBox
    Friend WithEvents tbDelivery As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkDefer As System.Windows.Forms.CheckBox
    Friend WithEvents txtDefer As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grpHouseKeeping As System.Windows.Forms.GroupBox
    Friend WithEvents chkHouseKeeping As System.Windows.Forms.CheckBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtHouseNum As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents chkburst As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbServer As System.Windows.Forms.ComboBox
    Friend WithEvents grpFax As System.Windows.Forms.GroupBox
    Friend WithEvents cmdFaxTo As System.Windows.Forms.Button
    Friend WithEvents txtFaxNumber As System.Windows.Forms.TextBox
    Friend WithEvents cmbFaxDevice As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtFaxTo As System.Windows.Forms.TextBox
    Friend WithEvents txtFaxFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtFaxComments As System.Windows.Forms.TextBox
    Friend WithEvents mnuAttachment As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents cmdProps As System.Windows.Forms.Button
    Friend WithEvents chkZipSecurity As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmbEncryptionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtZipCode As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtConfirmCode As System.Windows.Forms.TextBox
    Friend WithEvents grpZip As System.Windows.Forms.GroupBox
    Friend WithEvents tbPGP As System.Windows.Forms.TabPage
    Friend WithEvents chkPGP As System.Windows.Forms.CheckBox
    Friend WithEvents UcPGP As sqlrd.ucPGP
    Friend WithEvents chkDUN As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbDUN As System.Windows.Forms.ComboBox
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents grpSMS As System.Windows.Forms.GroupBox
    Friend WithEvents txtSMSMsg As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents txtSMSNumber As System.Windows.Forms.TextBox
    Friend WithEvents cmdSMSTo As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDestinationSelect))
        Me.grpEmail = New System.Windows.Forms.GroupBox
        Me.pnMailServer = New System.Windows.Forms.FlowLayoutPanel
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbServer = New System.Windows.Forms.ComboBox
        Me.grpSender = New System.Windows.Forms.GroupBox
        Me.txtSenderAddress = New System.Windows.Forms.TextBox
        Me.txtSenderName = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.pnReadReciepts = New System.Windows.Forms.FlowLayoutPanel
        Me.chkReadReceipt = New System.Windows.Forms.CheckBox
        Me.btnReadReceipts = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbMailFormat = New System.Windows.Forms.ComboBox
        Me.chkEmbed = New System.Windows.Forms.CheckBox
        Me.cmdAttach = New System.Windows.Forms.Button
        Me.cmdTo = New System.Windows.Forms.Button
        Me.txtMsg = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem
        Me.mnuSignature = New System.Windows.Forms.MenuItem
        Me.mnuAttachment = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.mnuSpell = New System.Windows.Forms.MenuItem
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.cmdBcc = New System.Windows.Forms.Button
        Me.txtAttach = New System.Windows.Forms.TextBox
        Me.cmdCC = New System.Windows.Forms.Button
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.txtCC = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtBCC = New System.Windows.Forms.TextBox
        Me.cmdInsert = New System.Windows.Forms.Button
        Me.chkburst = New System.Windows.Forms.CheckBox
        Me.grpPrinter = New System.Windows.Forms.GroupBox
        Me.lsvPrinters = New System.Windows.Forms.ListView
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddPrinter = New System.Windows.Forms.Button
        Me.Label16 = New System.Windows.Forms.Label
        Me.cmbPrinters = New System.Windows.Forms.ComboBox
        Me.cmdRemovePrinter = New System.Windows.Forms.Button
        Me.grpFile = New System.Windows.Forms.GroupBox
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.btnRemovePath = New System.Windows.Forms.Button
        Me.btnAddPath = New System.Windows.Forms.Button
        Me.lsvPaths = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.txtDirectory = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.grpHouseKeeping = New System.Windows.Forms.GroupBox
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.txtHouseNum = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkHouseKeeping = New System.Windows.Forms.CheckBox
        Me.chkDUN = New System.Windows.Forms.CheckBox
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.cmbDUN = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.cmdInsert2 = New System.Windows.Forms.Button
        Me.grpFTP = New System.Windows.Forms.GroupBox
        Me.btnRemoveFTP = New System.Windows.Forms.Button
        Me.btnAddFTP = New System.Windows.Forms.Button
        Me.lsvFTP = New System.Windows.Forms.ListView
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader9 = New System.Windows.Forms.ColumnHeader
        Me.Label9 = New System.Windows.Forms.Label
        Me.cmbDestination = New System.Windows.Forms.ComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tabDestination = New System.Windows.Forms.TabControl
        Me.tbDestination = New System.Windows.Forms.TabPage
        Me.grpODBC = New System.Windows.Forms.GroupBox
        Me.chkOverwrite = New System.Windows.Forms.CheckBox
        Me.txtTableName = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.btnODBC = New System.Windows.Forms.Button
        Me.grpFax = New System.Windows.Forms.GroupBox
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.txtFaxTo = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtFaxFrom = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtFaxComments = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmbFaxDevice = New System.Windows.Forms.ComboBox
        Me.txtFaxNumber = New System.Windows.Forms.TextBox
        Me.cmdFaxTo = New System.Windows.Forms.Button
        Me.grpSMS = New System.Windows.Forms.GroupBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtSMSMsg = New System.Windows.Forms.TextBox
        Me.txtSMSNumber = New System.Windows.Forms.TextBox
        Me.cmdSMSTo = New System.Windows.Forms.Button
        Me.tbOutput = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.grpZip = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.txtZipCode = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.cmbEncryptionLevel = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.txtConfirmCode = New System.Windows.Forms.TextBox
        Me.chkZipSecurity = New System.Windows.Forms.CheckBox
        Me.cmdProps = New System.Windows.Forms.Button
        Me.chkZip = New System.Windows.Forms.CheckBox
        Me.cmbFormat = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.chkSplit = New System.Windows.Forms.CheckBox
        Me.chkIncludeAttach = New System.Windows.Forms.CheckBox
        Me.tbNaming = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkUseKeyPar = New System.Windows.Forms.CheckBox
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown
        Me.Label26 = New System.Windows.Forms.Label
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.chkCustomExt = New System.Windows.Forms.CheckBox
        Me.txtCustomName = New System.Windows.Forms.TextBox
        Me.optNaming = New System.Windows.Forms.RadioButton
        Me.optCustomName = New System.Windows.Forms.RadioButton
        Me.txtCustomExt = New System.Windows.Forms.TextBox
        Me.chkAppendDateTime = New System.Windows.Forms.CheckBox
        Me.txtDefault = New System.Windows.Forms.TextBox
        Me.tbDelivery = New System.Windows.Forms.TabPage
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtDefer = New System.Windows.Forms.NumericUpDown
        Me.chkDefer = New System.Windows.Forms.CheckBox
        Me.tbPGP = New System.Windows.Forms.TabPage
        Me.chkPGP = New System.Windows.Forms.CheckBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.mnuDB = New System.Windows.Forms.MenuItem
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuConstants = New System.Windows.Forms.MenuItem
        Me.ofg = New System.Windows.Forms.OpenFileDialog
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.grpSharePoint = New System.Windows.Forms.GroupBox
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.lsvSharePointLibs = New System.Windows.Forms.ListView
        Me.ColumnHeader10 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader11 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader12 = New System.Windows.Forms.ColumnHeader
        Me.UcDSN = New sqlrd.ucDSN
        Me.UcFTP = New sqlrd.ucFTPDetails
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.UcPGP = New sqlrd.ucPGP
        Me.grpEmail.SuspendLayout()
        Me.pnMailServer.SuspendLayout()
        Me.grpSender.SuspendLayout()
        Me.pnReadReciepts.SuspendLayout()
        Me.grpPrinter.SuspendLayout()
        Me.grpFile.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.grpHouseKeeping.SuspendLayout()
        CType(Me.txtHouseNum, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.grpFTP.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tabDestination.SuspendLayout()
        Me.tbDestination.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.grpFax.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.grpSMS.SuspendLayout()
        Me.tbOutput.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.grpZip.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.tbNaming.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbDelivery.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtDefer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbPGP.SuspendLayout()
        Me.grpSharePoint.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpEmail
        '
        Me.grpEmail.Controls.Add(Me.pnMailServer)
        Me.grpEmail.Controls.Add(Me.grpSender)
        Me.grpEmail.Controls.Add(Me.pnReadReciepts)
        Me.grpEmail.Controls.Add(Me.Label2)
        Me.grpEmail.Controls.Add(Me.cmbMailFormat)
        Me.grpEmail.Controls.Add(Me.chkEmbed)
        Me.grpEmail.Controls.Add(Me.cmdAttach)
        Me.grpEmail.Controls.Add(Me.cmdTo)
        Me.grpEmail.Controls.Add(Me.txtMsg)
        Me.grpEmail.Controls.Add(Me.txtTo)
        Me.grpEmail.Controls.Add(Me.cmdBcc)
        Me.grpEmail.Controls.Add(Me.txtAttach)
        Me.grpEmail.Controls.Add(Me.cmdCC)
        Me.grpEmail.Controls.Add(Me.txtSubject)
        Me.grpEmail.Controls.Add(Me.txtCC)
        Me.grpEmail.Controls.Add(Me.Label14)
        Me.grpEmail.Controls.Add(Me.txtBCC)
        Me.HelpProvider1.SetHelpKeyword(Me.grpEmail, "Email_Setup.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpEmail, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpEmail.Location = New System.Drawing.Point(8, 8)
        Me.grpEmail.Name = "grpEmail"
        Me.HelpProvider1.SetShowHelp(Me.grpEmail, True)
        Me.grpEmail.Size = New System.Drawing.Size(424, 378)
        Me.grpEmail.TabIndex = 8
        Me.grpEmail.TabStop = False
        Me.grpEmail.Text = "Email"
        Me.grpEmail.Visible = False
        '
        'pnMailServer
        '
        Me.pnMailServer.Controls.Add(Me.Label5)
        Me.pnMailServer.Controls.Add(Me.cmbServer)
        Me.pnMailServer.Location = New System.Drawing.Point(8, 273)
        Me.pnMailServer.Name = "pnMailServer"
        Me.pnMailServer.Size = New System.Drawing.Size(213, 27)
        Me.pnMailServer.TabIndex = 27
        Me.pnMailServer.Visible = False
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 24)
        Me.Label5.TabIndex = 25
        Me.Label5.Text = "Mail Server"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbServer
        '
        Me.cmbServer.ItemHeight = 13
        Me.cmbServer.Items.AddRange(New Object() {"Default"})
        Me.cmbServer.Location = New System.Drawing.Point(73, 3)
        Me.cmbServer.Name = "cmbServer"
        Me.cmbServer.Size = New System.Drawing.Size(136, 21)
        Me.cmbServer.TabIndex = 26
        '
        'grpSender
        '
        Me.grpSender.Controls.Add(Me.txtSenderAddress)
        Me.grpSender.Controls.Add(Me.txtSenderName)
        Me.grpSender.Controls.Add(Me.Label18)
        Me.grpSender.Controls.Add(Me.Label17)
        Me.grpSender.Location = New System.Drawing.Point(8, 306)
        Me.grpSender.Name = "grpSender"
        Me.grpSender.Size = New System.Drawing.Size(404, 66)
        Me.grpSender.TabIndex = 29
        Me.grpSender.TabStop = False
        Me.grpSender.Text = "Customize sender details (optional)"
        '
        'txtSenderAddress
        '
        Me.txtSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderAddress.Location = New System.Drawing.Point(83, 41)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(297, 21)
        Me.txtSenderAddress.TabIndex = 15
        '
        'txtSenderName
        '
        Me.txtSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderName.Location = New System.Drawing.Point(83, 14)
        Me.txtSenderName.Name = "txtSenderName"
        Me.txtSenderName.Size = New System.Drawing.Size(297, 21)
        Me.txtSenderName.TabIndex = 14
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(4, 43)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 13)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Address:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 17)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(38, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name:"
        '
        'pnReadReciepts
        '
        Me.pnReadReciepts.Controls.Add(Me.chkReadReceipt)
        Me.pnReadReciepts.Controls.Add(Me.btnReadReceipts)
        Me.pnReadReciepts.Location = New System.Drawing.Point(8, 273)
        Me.pnReadReciepts.Name = "pnReadReciepts"
        Me.pnReadReciepts.Size = New System.Drawing.Size(380, 28)
        Me.pnReadReciepts.TabIndex = 28
        Me.pnReadReciepts.Visible = False
        '
        'chkReadReceipt
        '
        Me.chkReadReceipt.Location = New System.Drawing.Point(3, 3)
        Me.chkReadReceipt.Name = "chkReadReceipt"
        Me.chkReadReceipt.Size = New System.Drawing.Size(222, 24)
        Me.chkReadReceipt.TabIndex = 12
        Me.chkReadReceipt.Text = "Request a read receipt for this message"
        Me.chkReadReceipt.UseVisualStyleBackColor = True
        '
        'btnReadReceipts
        '
        Me.btnReadReceipts.Enabled = False
        Me.btnReadReceipts.Location = New System.Drawing.Point(231, 3)
        Me.btnReadReceipts.Name = "btnReadReceipts"
        Me.btnReadReceipts.Size = New System.Drawing.Size(31, 24)
        Me.btnReadReceipts.TabIndex = 13
        Me.btnReadReceipts.Text = "..."
        Me.btnReadReceipts.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(9, 249)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 21)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "Mail Format"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbMailFormat
        '
        Me.cmbMailFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailFormat.ItemHeight = 13
        Me.cmbMailFormat.Items.AddRange(New Object() {"TEXT", "HTML", "HTML (Basic)"})
        Me.cmbMailFormat.Location = New System.Drawing.Point(81, 249)
        Me.cmbMailFormat.Name = "cmbMailFormat"
        Me.cmbMailFormat.Size = New System.Drawing.Size(136, 21)
        Me.cmbMailFormat.TabIndex = 10
        '
        'chkEmbed
        '
        Me.chkEmbed.BackColor = System.Drawing.SystemColors.Control
        Me.chkEmbed.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkEmbed.ForeColor = System.Drawing.Color.Navy
        Me.chkEmbed.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmbed.Location = New System.Drawing.Point(223, 250)
        Me.chkEmbed.Name = "chkEmbed"
        Me.chkEmbed.Size = New System.Drawing.Size(176, 21)
        Me.chkEmbed.TabIndex = 11
        Me.chkEmbed.Text = "Embed report into email body"
        Me.chkEmbed.UseVisualStyleBackColor = False
        '
        'cmdAttach
        '
        Me.cmdAttach.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAttach.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAttach.Image = CType(resources.GetObject("cmdAttach.Image"), System.Drawing.Image)
        Me.cmdAttach.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAttach.Location = New System.Drawing.Point(11, 128)
        Me.cmdAttach.Name = "cmdAttach"
        Me.cmdAttach.Size = New System.Drawing.Size(64, 21)
        Me.cmdAttach.TabIndex = 7
        Me.cmdAttach.Text = "Attach"
        Me.cmdAttach.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAttach.UseVisualStyleBackColor = False
        '
        'cmdTo
        '
        Me.cmdTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTo.Image = CType(resources.GetObject("cmdTo.Image"), System.Drawing.Image)
        Me.cmdTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(11, 19)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdTo.TabIndex = 0
        Me.cmdTo.Text = "To..."
        Me.cmdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdTo.UseVisualStyleBackColor = False
        '
        'txtMsg
        '
        Me.txtMsg.ContextMenu = Me.mnuInserter
        Me.txtMsg.ForeColor = System.Drawing.Color.Blue
        Me.txtMsg.Location = New System.Drawing.Point(11, 155)
        Me.txtMsg.Multiline = True
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMsg.Size = New System.Drawing.Size(376, 89)
        Me.txtMsg.TabIndex = 9
        Me.txtMsg.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem13, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuDefMsg, Me.mnuSignature, Me.mnuAttachment})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 5
        Me.mnuDefMsg.Text = "Default Message"
        '
        'mnuSignature
        '
        Me.mnuSignature.Index = 6
        Me.mnuSignature.Text = "Default Signature"
        '
        'mnuAttachment
        '
        Me.mnuAttachment.Index = 7
        Me.mnuAttachment.Text = "Default Attachment"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 10
        Me.MenuItem13.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'txtTo
        '
        Me.txtTo.ForeColor = System.Drawing.Color.Blue
        Me.txtTo.Location = New System.Drawing.Point(91, 19)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(296, 21)
        Me.txtTo.TabIndex = 1
        Me.txtTo.Tag = "memo"
        '
        'cmdBcc
        '
        Me.cmdBcc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdBcc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBcc.Image = CType(resources.GetObject("cmdBcc.Image"), System.Drawing.Image)
        Me.cmdBcc.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdBcc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBcc.Location = New System.Drawing.Point(11, 73)
        Me.cmdBcc.Name = "cmdBcc"
        Me.cmdBcc.Size = New System.Drawing.Size(64, 21)
        Me.cmdBcc.TabIndex = 4
        Me.cmdBcc.Text = "BCC..."
        Me.cmdBcc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBcc.UseVisualStyleBackColor = False
        '
        'txtAttach
        '
        Me.txtAttach.BackColor = System.Drawing.Color.White
        Me.txtAttach.ContextMenu = Me.mnuInserter
        Me.txtAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtAttach.HideSelection = False
        Me.txtAttach.Location = New System.Drawing.Point(91, 128)
        Me.txtAttach.Name = "txtAttach"
        Me.txtAttach.Size = New System.Drawing.Size(296, 21)
        Me.txtAttach.TabIndex = 8
        Me.txtAttach.Tag = "memo"
        '
        'cmdCC
        '
        Me.cmdCC.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCC.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCC.Image = CType(resources.GetObject("cmdCC.Image"), System.Drawing.Image)
        Me.cmdCC.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCC.Location = New System.Drawing.Point(11, 46)
        Me.cmdCC.Name = "cmdCC"
        Me.cmdCC.Size = New System.Drawing.Size(64, 21)
        Me.cmdCC.TabIndex = 2
        Me.cmdCC.Text = "CC..."
        Me.cmdCC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCC.UseVisualStyleBackColor = False
        '
        'txtSubject
        '
        Me.txtSubject.ContextMenu = Me.mnuInserter
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(91, 100)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(296, 21)
        Me.txtSubject.TabIndex = 6
        Me.txtSubject.Tag = "memo"
        '
        'txtCC
        '
        Me.txtCC.ForeColor = System.Drawing.Color.Blue
        Me.txtCC.Location = New System.Drawing.Point(91, 46)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(296, 21)
        Me.txtCC.TabIndex = 3
        Me.txtCC.Tag = "memo"
        '
        'Label14
        '
        Me.Label14.Enabled = False
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(11, 105)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 16)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "Subject"
        '
        'txtBCC
        '
        Me.txtBCC.ForeColor = System.Drawing.Color.Blue
        Me.txtBCC.Location = New System.Drawing.Point(91, 73)
        Me.txtBCC.Name = "txtBCC"
        Me.txtBCC.Size = New System.Drawing.Size(296, 21)
        Me.txtBCC.TabIndex = 5
        Me.txtBCC.Tag = "memo"
        '
        'cmdInsert
        '
        Me.cmdInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdInsert.Image = CType(resources.GetObject("cmdInsert.Image"), System.Drawing.Image)
        Me.cmdInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(228, 623)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(24, 23)
        Me.cmdInsert.TabIndex = 24
        Me.cmdInsert.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdInsert.Visible = False
        '
        'chkburst
        '
        Me.chkburst.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkburst.Location = New System.Drawing.Point(16, 622)
        Me.chkburst.Name = "chkburst"
        Me.chkburst.Size = New System.Drawing.Size(192, 24)
        Me.chkburst.TabIndex = 26
        Me.chkburst.Text = "Use report bursting for emails"
        Me.chkburst.Visible = False
        '
        'grpPrinter
        '
        Me.grpPrinter.Controls.Add(Me.lsvPrinters)
        Me.grpPrinter.Controls.Add(Me.cmdAddPrinter)
        Me.grpPrinter.Controls.Add(Me.Label16)
        Me.grpPrinter.Controls.Add(Me.cmbPrinters)
        Me.grpPrinter.Controls.Add(Me.cmdRemovePrinter)
        Me.HelpProvider1.SetHelpKeyword(Me.grpPrinter, "Printer_Setup.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpPrinter, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpPrinter.Location = New System.Drawing.Point(8, 8)
        Me.grpPrinter.Name = "grpPrinter"
        Me.HelpProvider1.SetShowHelp(Me.grpPrinter, True)
        Me.grpPrinter.Size = New System.Drawing.Size(424, 378)
        Me.grpPrinter.TabIndex = 10
        Me.grpPrinter.TabStop = False
        Me.grpPrinter.Text = "Printer"
        Me.grpPrinter.Visible = False
        '
        'lsvPrinters
        '
        Me.lsvPrinters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvPrinters.FullRowSelect = True
        Me.lsvPrinters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvPrinters.Location = New System.Drawing.Point(8, 96)
        Me.lsvPrinters.Name = "lsvPrinters"
        Me.lsvPrinters.Size = New System.Drawing.Size(392, 275)
        Me.lsvPrinters.TabIndex = 2
        Me.lsvPrinters.UseCompatibleStateImageBehavior = False
        Me.lsvPrinters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Printer Name"
        Me.ColumnHeader3.Width = 325
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Copies"
        '
        'cmdAddPrinter
        '
        Me.cmdAddPrinter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddPrinter.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddPrinter.Image = CType(resources.GetObject("cmdAddPrinter.Image"), System.Drawing.Image)
        Me.cmdAddPrinter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddPrinter.Location = New System.Drawing.Point(365, 64)
        Me.cmdAddPrinter.Name = "cmdAddPrinter"
        Me.cmdAddPrinter.Size = New System.Drawing.Size(35, 24)
        Me.cmdAddPrinter.TabIndex = 1
        Me.cmdAddPrinter.UseVisualStyleBackColor = False
        '
        'Label16
        '
        Me.Label16.Enabled = False
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 24)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(160, 16)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "Select Printers"
        '
        'cmbPrinters
        '
        Me.cmbPrinters.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPrinters.ForeColor = System.Drawing.Color.Blue
        Me.cmbPrinters.ItemHeight = 13
        Me.cmbPrinters.Location = New System.Drawing.Point(8, 40)
        Me.cmbPrinters.Name = "cmbPrinters"
        Me.cmbPrinters.Size = New System.Drawing.Size(392, 21)
        Me.cmbPrinters.TabIndex = 0
        '
        'cmdRemovePrinter
        '
        Me.cmdRemovePrinter.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemovePrinter.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemovePrinter.Image = CType(resources.GetObject("cmdRemovePrinter.Image"), System.Drawing.Image)
        Me.cmdRemovePrinter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemovePrinter.Location = New System.Drawing.Point(8, 64)
        Me.cmdRemovePrinter.Name = "cmdRemovePrinter"
        Me.cmdRemovePrinter.Size = New System.Drawing.Size(35, 24)
        Me.cmdRemovePrinter.TabIndex = 3
        Me.cmdRemovePrinter.UseVisualStyleBackColor = False
        '
        'grpFile
        '
        Me.grpFile.Controls.Add(Me.GroupBox9)
        Me.grpFile.Controls.Add(Me.GroupBox5)
        Me.HelpProvider1.SetHelpKeyword(Me.grpFile, "Disk_Setup.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpFile, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpFile.Location = New System.Drawing.Point(8, 8)
        Me.grpFile.Name = "grpFile"
        Me.HelpProvider1.SetShowHelp(Me.grpFile, True)
        Me.grpFile.Size = New System.Drawing.Size(424, 378)
        Me.grpFile.TabIndex = 0
        Me.grpFile.TabStop = False
        Me.grpFile.Text = "File"
        Me.grpFile.Visible = False
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.btnRemovePath)
        Me.GroupBox9.Controls.Add(Me.btnAddPath)
        Me.GroupBox9.Controls.Add(Me.lsvPaths)
        Me.GroupBox9.Controls.Add(Me.cmdBrowse)
        Me.GroupBox9.Controls.Add(Me.txtDirectory)
        Me.GroupBox9.Controls.Add(Me.Label15)
        Me.GroupBox9.Location = New System.Drawing.Point(6, 203)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(412, 168)
        Me.GroupBox9.TabIndex = 1
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "File Paths"
        '
        'btnRemovePath
        '
        Me.btnRemovePath.Image = CType(resources.GetObject("btnRemovePath.Image"), System.Drawing.Image)
        Me.btnRemovePath.Location = New System.Drawing.Point(6, 47)
        Me.btnRemovePath.Name = "btnRemovePath"
        Me.btnRemovePath.Size = New System.Drawing.Size(40, 23)
        Me.btnRemovePath.TabIndex = 4
        Me.btnRemovePath.UseVisualStyleBackColor = True
        '
        'btnAddPath
        '
        Me.btnAddPath.Image = CType(resources.GetObject("btnAddPath.Image"), System.Drawing.Image)
        Me.btnAddPath.Location = New System.Drawing.Point(354, 46)
        Me.btnAddPath.Name = "btnAddPath"
        Me.btnAddPath.Size = New System.Drawing.Size(40, 23)
        Me.btnAddPath.TabIndex = 2
        Me.btnAddPath.UseVisualStyleBackColor = True
        '
        'lsvPaths
        '
        Me.lsvPaths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvPaths.FullRowSelect = True
        Me.lsvPaths.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvPaths.HideSelection = False
        Me.lsvPaths.Location = New System.Drawing.Point(6, 76)
        Me.lsvPaths.Name = "lsvPaths"
        Me.lsvPaths.Size = New System.Drawing.Size(388, 83)
        Me.lsvPaths.TabIndex = 3
        Me.lsvPaths.UseCompatibleStateImageBehavior = False
        Me.lsvPaths.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Path"
        Me.ColumnHeader1.Width = 379
        '
        'cmdBrowse
        '
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(354, 20)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 23)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtDirectory
        '
        Me.txtDirectory.BackColor = System.Drawing.Color.White
        Me.txtDirectory.ContextMenu = Me.mnuInserter
        Me.txtDirectory.ForeColor = System.Drawing.Color.Blue
        Me.txtDirectory.Location = New System.Drawing.Point(58, 20)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(285, 21)
        Me.txtDirectory.TabIndex = 0
        Me.txtDirectory.Tag = "memo"
        '
        'Label15
        '
        Me.Label15.Enabled = False
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(3, 22)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(57, 16)
        Me.Label15.TabIndex = 6
        Me.Label15.Text = "File Path"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.grpHouseKeeping)
        Me.GroupBox5.Controls.Add(Me.chkHouseKeeping)
        Me.GroupBox5.Controls.Add(Me.chkDUN)
        Me.GroupBox5.Controls.Add(Me.GroupBox7)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 15)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(411, 182)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "File Options"
        '
        'grpHouseKeeping
        '
        Me.grpHouseKeeping.Controls.Add(Me.cmbUnit)
        Me.grpHouseKeeping.Controls.Add(Me.txtHouseNum)
        Me.grpHouseKeeping.Controls.Add(Me.Label4)
        Me.grpHouseKeeping.Enabled = False
        Me.grpHouseKeeping.Location = New System.Drawing.Point(6, 126)
        Me.grpHouseKeeping.Name = "grpHouseKeeping"
        Me.grpHouseKeeping.Size = New System.Drawing.Size(399, 48)
        Me.grpHouseKeeping.TabIndex = 3
        Me.grpHouseKeeping.TabStop = False
        Me.grpHouseKeeping.Text = "House Keeping"
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.ItemHeight = 13
        Me.cmbUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months"})
        Me.cmbUnit.Location = New System.Drawing.Point(224, 20)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(163, 21)
        Me.cmbUnit.TabIndex = 1
        '
        'txtHouseNum
        '
        Me.txtHouseNum.Location = New System.Drawing.Point(152, 20)
        Me.txtHouseNum.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtHouseNum.Name = "txtHouseNum"
        Me.txtHouseNum.Size = New System.Drawing.Size(64, 21)
        Me.txtHouseNum.TabIndex = 0
        Me.txtHouseNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtHouseNum.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 22)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(144, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Delete reports older than"
        '
        'chkHouseKeeping
        '
        Me.chkHouseKeeping.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkHouseKeeping.Location = New System.Drawing.Point(6, 102)
        Me.chkHouseKeeping.Name = "chkHouseKeeping"
        Me.chkHouseKeeping.Size = New System.Drawing.Size(336, 24)
        Me.chkHouseKeeping.TabIndex = 2
        Me.chkHouseKeeping.Text = "Enable housekeeping for this destination"
        '
        'chkDUN
        '
        Me.chkDUN.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDUN.Location = New System.Drawing.Point(6, 16)
        Me.chkDUN.Name = "chkDUN"
        Me.chkDUN.Size = New System.Drawing.Size(200, 24)
        Me.chkDUN.TabIndex = 0
        Me.chkDUN.Text = "Connect using dial-up networking"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.cmbDUN)
        Me.GroupBox7.Controls.Add(Me.Label21)
        Me.GroupBox7.Controls.Add(Me.cmdConnect)
        Me.GroupBox7.Enabled = False
        Me.GroupBox7.Location = New System.Drawing.Point(6, 46)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(399, 50)
        Me.GroupBox7.TabIndex = 1
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Dial-Up Networking Setup"
        '
        'cmbDUN
        '
        Me.cmbDUN.ItemHeight = 13
        Me.cmbDUN.Location = New System.Drawing.Point(110, 22)
        Me.cmbDUN.Name = "cmbDUN"
        Me.cmbDUN.Size = New System.Drawing.Size(178, 21)
        Me.cmbDUN.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 24)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(96, 16)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "Connection Name"
        '
        'cmdConnect
        '
        Me.cmdConnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdConnect.Image = CType(resources.GetObject("cmdConnect.Image"), System.Drawing.Image)
        Me.cmdConnect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(312, 22)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 21)
        Me.cmdConnect.TabIndex = 1
        Me.cmdConnect.Text = "Connect"
        Me.cmdConnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdInsert2
        '
        Me.cmdInsert2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdInsert2.Image = CType(resources.GetObject("cmdInsert2.Image"), System.Drawing.Image)
        Me.cmdInsert2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdInsert2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert2.Location = New System.Drawing.Point(92, 652)
        Me.cmdInsert2.Name = "cmdInsert2"
        Me.cmdInsert2.Size = New System.Drawing.Size(24, 21)
        Me.cmdInsert2.TabIndex = 24
        Me.cmdInsert2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdInsert2.Visible = False
        '
        'grpFTP
        '
        Me.grpFTP.Controls.Add(Me.UcFTP)
        Me.grpFTP.Controls.Add(Me.btnRemoveFTP)
        Me.grpFTP.Controls.Add(Me.btnAddFTP)
        Me.grpFTP.Controls.Add(Me.lsvFTP)
        Me.grpFTP.Controls.Add(Me.DividerLabel1)
        Me.HelpProvider1.SetHelpKeyword(Me.grpFTP, "FTP_Setup.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpFTP, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpFTP.Location = New System.Drawing.Point(8, 8)
        Me.grpFTP.Name = "grpFTP"
        Me.HelpProvider1.SetShowHelp(Me.grpFTP, True)
        Me.grpFTP.Size = New System.Drawing.Size(424, 378)
        Me.grpFTP.TabIndex = 0
        Me.grpFTP.TabStop = False
        Me.grpFTP.Text = "File Transfer Protocol"
        Me.grpFTP.Visible = False
        '
        'btnRemoveFTP
        '
        Me.btnRemoveFTP.Image = CType(resources.GetObject("btnRemoveFTP.Image"), System.Drawing.Image)
        Me.btnRemoveFTP.Location = New System.Drawing.Point(10, 224)
        Me.btnRemoveFTP.Name = "btnRemoveFTP"
        Me.btnRemoveFTP.Size = New System.Drawing.Size(40, 23)
        Me.btnRemoveFTP.TabIndex = 8
        Me.btnRemoveFTP.UseVisualStyleBackColor = True
        '
        'btnAddFTP
        '
        Me.btnAddFTP.Image = CType(resources.GetObject("btnAddFTP.Image"), System.Drawing.Image)
        Me.btnAddFTP.Location = New System.Drawing.Point(372, 221)
        Me.btnAddFTP.Name = "btnAddFTP"
        Me.btnAddFTP.Size = New System.Drawing.Size(40, 23)
        Me.btnAddFTP.TabIndex = 6
        Me.btnAddFTP.UseVisualStyleBackColor = True
        '
        'lsvFTP
        '
        Me.lsvFTP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9})
        Me.lsvFTP.FullRowSelect = True
        Me.lsvFTP.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvFTP.HideSelection = False
        Me.lsvFTP.Location = New System.Drawing.Point(10, 248)
        Me.lsvFTP.Name = "lsvFTP"
        Me.lsvFTP.Size = New System.Drawing.Size(404, 140)
        Me.lsvFTP.TabIndex = 7
        Me.lsvFTP.UseCompatibleStateImageBehavior = False
        Me.lsvFTP.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Server"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "User Name"
        Me.ColumnHeader5.Width = 100
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Password"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Directory"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "Type"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Passive"
        '
        'Label9
        '
        Me.Label9.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.Label9, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label9, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.HelpProvider1.SetShowHelp(Me.Label9, True)
        Me.Label9.Size = New System.Drawing.Size(102, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Destination Type"
        '
        'cmbDestination
        '
        Me.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestination.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.cmbDestination, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbDestination, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbDestination.ItemHeight = 13
        Me.cmbDestination.Location = New System.Drawing.Point(111, 13)
        Me.cmbDestination.Name = "cmbDestination"
        Me.HelpProvider1.SetShowHelp(Me.cmbDestination, True)
        Me.cmbDestination.Size = New System.Drawing.Size(160, 21)
        Me.cmbDestination.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbDestination)
        Me.GroupBox1.Controls.Add(Me.tabDestination)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 467)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'tabDestination
        '
        Me.tabDestination.Controls.Add(Me.tbDestination)
        Me.tabDestination.Controls.Add(Me.tbOutput)
        Me.tabDestination.Controls.Add(Me.tbNaming)
        Me.tabDestination.Controls.Add(Me.tbDelivery)
        Me.tabDestination.Controls.Add(Me.tbPGP)
        Me.tabDestination.ItemSize = New System.Drawing.Size(97, 18)
        Me.tabDestination.Location = New System.Drawing.Point(10, 40)
        Me.tabDestination.Name = "tabDestination"
        Me.tabDestination.SelectedIndex = 0
        Me.tabDestination.Size = New System.Drawing.Size(445, 420)
        Me.tabDestination.TabIndex = 12
        Me.tabDestination.Visible = False
        '
        'tbDestination
        '
        Me.tbDestination.Controls.Add(Me.grpSharePoint)
        Me.tbDestination.Controls.Add(Me.grpEmail)
        Me.tbDestination.Controls.Add(Me.grpODBC)
        Me.tbDestination.Controls.Add(Me.grpFax)
        Me.tbDestination.Controls.Add(Me.grpFTP)
        Me.tbDestination.Controls.Add(Me.grpFile)
        Me.tbDestination.Controls.Add(Me.grpSMS)
        Me.tbDestination.Controls.Add(Me.grpPrinter)
        Me.tbDestination.Location = New System.Drawing.Point(4, 22)
        Me.tbDestination.Name = "tbDestination"
        Me.tbDestination.Size = New System.Drawing.Size(437, 394)
        Me.tbDestination.TabIndex = 0
        Me.tbDestination.Text = "Destination Setup"
        '
        'grpODBC
        '
        Me.grpODBC.Controls.Add(Me.chkOverwrite)
        Me.grpODBC.Controls.Add(Me.txtTableName)
        Me.grpODBC.Controls.Add(Me.Label24)
        Me.grpODBC.Controls.Add(Me.btnODBC)
        Me.grpODBC.Controls.Add(Me.UcDSN)
        Me.grpODBC.Location = New System.Drawing.Point(8, 8)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(424, 378)
        Me.grpODBC.TabIndex = 0
        Me.grpODBC.TabStop = False
        Me.grpODBC.Text = "ODBC Options"
        Me.grpODBC.Visible = False
        '
        'chkOverwrite
        '
        Me.chkOverwrite.AutoSize = True
        Me.chkOverwrite.Location = New System.Drawing.Point(80, 203)
        Me.chkOverwrite.Name = "chkOverwrite"
        Me.chkOverwrite.Size = New System.Drawing.Size(141, 17)
        Me.chkOverwrite.TabIndex = 3
        Me.chkOverwrite.Text = "Overwrite if table exists"
        Me.chkOverwrite.UseVisualStyleBackColor = True
        '
        'txtTableName
        '
        Me.txtTableName.Enabled = False
        Me.txtTableName.Location = New System.Drawing.Point(80, 176)
        Me.txtTableName.Name = "txtTableName"
        Me.txtTableName.Size = New System.Drawing.Size(300, 21)
        Me.txtTableName.TabIndex = 2
        '
        'Label24
        '
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(8, 178)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(64, 16)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Table Name"
        '
        'btnODBC
        '
        Me.btnODBC.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnODBC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnODBC.Location = New System.Drawing.Point(193, 144)
        Me.btnODBC.Name = "btnODBC"
        Me.btnODBC.Size = New System.Drawing.Size(75, 23)
        Me.btnODBC.TabIndex = 1
        Me.btnODBC.Text = "Connect..."
        '
        'grpFax
        '
        Me.grpFax.Controls.Add(Me.GroupBox6)
        Me.grpFax.Controls.Add(Me.Label6)
        Me.grpFax.Controls.Add(Me.cmbFaxDevice)
        Me.grpFax.Controls.Add(Me.txtFaxNumber)
        Me.grpFax.Controls.Add(Me.cmdFaxTo)
        Me.HelpProvider1.SetHelpKeyword(Me.grpFax, "Export_to_FAX_(Local_Fax_Modem).htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpFax, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpFax.Location = New System.Drawing.Point(8, 8)
        Me.grpFax.Name = "grpFax"
        Me.HelpProvider1.SetShowHelp(Me.grpFax, True)
        Me.grpFax.Size = New System.Drawing.Size(424, 378)
        Me.grpFax.TabIndex = 0
        Me.grpFax.TabStop = False
        Me.grpFax.Text = "Fax"
        Me.grpFax.Visible = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.txtFaxTo)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.txtFaxFrom)
        Me.GroupBox6.Controls.Add(Me.Label8)
        Me.GroupBox6.Controls.Add(Me.Label10)
        Me.GroupBox6.Controls.Add(Me.txtFaxComments)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 88)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(392, 224)
        Me.GroupBox6.TabIndex = 1
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Other Information"
        '
        'txtFaxTo
        '
        Me.txtFaxTo.ContextMenu = Me.mnuInserter
        Me.txtFaxTo.Location = New System.Drawing.Point(88, 24)
        Me.txtFaxTo.Name = "txtFaxTo"
        Me.txtFaxTo.Size = New System.Drawing.Size(288, 21)
        Me.txtFaxTo.TabIndex = 0
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 26)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(48, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "To"
        '
        'txtFaxFrom
        '
        Me.txtFaxFrom.ContextMenu = Me.mnuInserter
        Me.txtFaxFrom.Location = New System.Drawing.Point(88, 56)
        Me.txtFaxFrom.Name = "txtFaxFrom"
        Me.txtFaxFrom.Size = New System.Drawing.Size(288, 21)
        Me.txtFaxFrom.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 58)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "From"
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Comments"
        '
        'txtFaxComments
        '
        Me.txtFaxComments.ContextMenu = Me.mnuInserter
        Me.txtFaxComments.Location = New System.Drawing.Point(8, 104)
        Me.txtFaxComments.Multiline = True
        Me.txtFaxComments.Name = "txtFaxComments"
        Me.txtFaxComments.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFaxComments.Size = New System.Drawing.Size(368, 112)
        Me.txtFaxComments.TabIndex = 2
        Me.txtFaxComments.Tag = "Memo"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 58)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Fax Device"
        '
        'cmbFaxDevice
        '
        Me.cmbFaxDevice.ItemHeight = 13
        Me.cmbFaxDevice.Location = New System.Drawing.Point(96, 56)
        Me.cmbFaxDevice.Name = "cmbFaxDevice"
        Me.cmbFaxDevice.Size = New System.Drawing.Size(304, 21)
        Me.cmbFaxDevice.TabIndex = 2
        '
        'txtFaxNumber
        '
        Me.txtFaxNumber.Location = New System.Drawing.Point(96, 24)
        Me.txtFaxNumber.Name = "txtFaxNumber"
        Me.txtFaxNumber.Size = New System.Drawing.Size(304, 21)
        Me.txtFaxNumber.TabIndex = 1
        '
        'cmdFaxTo
        '
        Me.cmdFaxTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFaxTo.Image = CType(resources.GetObject("cmdFaxTo.Image"), System.Drawing.Image)
        Me.cmdFaxTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdFaxTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFaxTo.Location = New System.Drawing.Point(8, 24)
        Me.cmdFaxTo.Name = "cmdFaxTo"
        Me.cmdFaxTo.Size = New System.Drawing.Size(75, 23)
        Me.cmdFaxTo.TabIndex = 0
        Me.cmdFaxTo.Text = "Fax No."
        Me.cmdFaxTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpSMS
        '
        Me.grpSMS.Controls.Add(Me.Label23)
        Me.grpSMS.Controls.Add(Me.txtSMSMsg)
        Me.grpSMS.Controls.Add(Me.txtSMSNumber)
        Me.grpSMS.Controls.Add(Me.cmdSMSTo)
        Me.HelpProvider1.SetHelpKeyword(Me.grpSMS, "Export_to_SMS_(Cellphone_Text).htm")
        Me.HelpProvider1.SetHelpNavigator(Me.grpSMS, System.Windows.Forms.HelpNavigator.Topic)
        Me.grpSMS.Location = New System.Drawing.Point(8, 8)
        Me.grpSMS.Name = "grpSMS"
        Me.HelpProvider1.SetShowHelp(Me.grpSMS, True)
        Me.grpSMS.Size = New System.Drawing.Size(424, 320)
        Me.grpSMS.TabIndex = 27
        Me.grpSMS.TabStop = False
        Me.grpSMS.Text = "SMS"
        Me.grpSMS.Visible = False
        '
        'Label23
        '
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(8, 296)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(168, 16)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Characters Left: "
        '
        'txtSMSMsg
        '
        Me.txtSMSMsg.ContextMenu = Me.mnuInserter
        Me.txtSMSMsg.Location = New System.Drawing.Point(8, 56)
        Me.txtSMSMsg.MaxLength = 160
        Me.txtSMSMsg.Multiline = True
        Me.txtSMSMsg.Name = "txtSMSMsg"
        Me.txtSMSMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSMSMsg.Size = New System.Drawing.Size(376, 232)
        Me.txtSMSMsg.TabIndex = 2
        Me.txtSMSMsg.Tag = "memo"
        '
        'txtSMSNumber
        '
        Me.txtSMSNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtSMSNumber.Location = New System.Drawing.Point(88, 24)
        Me.txtSMSNumber.Name = "txtSMSNumber"
        Me.txtSMSNumber.Size = New System.Drawing.Size(296, 21)
        Me.txtSMSNumber.TabIndex = 1
        Me.txtSMSNumber.Tag = "memo"
        '
        'cmdSMSTo
        '
        Me.cmdSMSTo.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSMSTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSMSTo.Image = CType(resources.GetObject("cmdSMSTo.Image"), System.Drawing.Image)
        Me.cmdSMSTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdSMSTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSMSTo.Location = New System.Drawing.Point(8, 24)
        Me.cmdSMSTo.Name = "cmdSMSTo"
        Me.cmdSMSTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdSMSTo.TabIndex = 0
        Me.cmdSMSTo.Text = "To..."
        Me.cmdSMSTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSMSTo.UseVisualStyleBackColor = False
        '
        'tbOutput
        '
        Me.tbOutput.Controls.Add(Me.GroupBox2)
        Me.tbOutput.Controls.Add(Me.chkIncludeAttach)
        Me.tbOutput.Location = New System.Drawing.Point(4, 22)
        Me.tbOutput.Name = "tbOutput"
        Me.tbOutput.Size = New System.Drawing.Size(437, 394)
        Me.tbOutput.TabIndex = 1
        Me.tbOutput.Text = "Output Format"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.grpZip)
        Me.GroupBox2.Controls.Add(Me.cmdProps)
        Me.GroupBox2.Controls.Add(Me.chkZip)
        Me.GroupBox2.Controls.Add(Me.cmbFormat)
        Me.GroupBox2.Controls.Add(Me.Label22)
        Me.GroupBox2.Controls.Add(Me.chkSplit)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox2, "Output_Formats.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox2, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 24)
        Me.GroupBox2.Name = "GroupBox2"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox2, True)
        Me.GroupBox2.Size = New System.Drawing.Size(424, 367)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'grpZip
        '
        Me.grpZip.Controls.Add(Me.Panel1)
        Me.grpZip.Controls.Add(Me.chkZipSecurity)
        Me.grpZip.Location = New System.Drawing.Point(56, 80)
        Me.grpZip.Name = "grpZip"
        Me.grpZip.Size = New System.Drawing.Size(360, 160)
        Me.grpZip.TabIndex = 2
        Me.grpZip.TabStop = False
        Me.grpZip.Text = "Zip File Encryption"
        Me.grpZip.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.txtZipCode)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.cmbEncryptionLevel)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtConfirmCode)
        Me.Panel1.Enabled = False
        Me.Panel1.Location = New System.Drawing.Point(8, 48)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(344, 104)
        Me.Panel1.TabIndex = 2
        '
        'txtZipCode
        '
        Me.txtZipCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtZipCode.Location = New System.Drawing.Point(112, 40)
        Me.txtZipCode.Name = "txtZipCode"
        Me.txtZipCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtZipCode.Size = New System.Drawing.Size(160, 21)
        Me.txtZipCode.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 42)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "Password"
        '
        'cmbEncryptionLevel
        '
        Me.cmbEncryptionLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEncryptionLevel.ItemHeight = 13
        Me.cmbEncryptionLevel.Items.AddRange(New Object() {"128", "192", "256"})
        Me.cmbEncryptionLevel.Location = New System.Drawing.Point(112, 8)
        Me.cmbEncryptionLevel.Name = "cmbEncryptionLevel"
        Me.cmbEncryptionLevel.Size = New System.Drawing.Size(160, 21)
        Me.cmbEncryptionLevel.TabIndex = 1
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 1
        Me.Label11.Text = "Encryption Level"
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 74)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Confirm Password"
        '
        'txtConfirmCode
        '
        Me.txtConfirmCode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtConfirmCode.Location = New System.Drawing.Point(112, 72)
        Me.txtConfirmCode.Name = "txtConfirmCode"
        Me.txtConfirmCode.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtConfirmCode.Size = New System.Drawing.Size(160, 21)
        Me.txtConfirmCode.TabIndex = 3
        '
        'chkZipSecurity
        '
        Me.chkZipSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkZipSecurity.Location = New System.Drawing.Point(8, 24)
        Me.chkZipSecurity.Name = "chkZipSecurity"
        Me.chkZipSecurity.Size = New System.Drawing.Size(320, 24)
        Me.chkZipSecurity.TabIndex = 0
        Me.chkZipSecurity.Text = "Enable zip encryption"
        '
        'cmdProps
        '
        Me.cmdProps.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdProps.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdProps.Location = New System.Drawing.Point(264, 24)
        Me.cmdProps.Name = "cmdProps"
        Me.cmdProps.Size = New System.Drawing.Size(32, 21)
        Me.cmdProps.TabIndex = 1
        Me.cmdProps.Text = "..."
        '
        'chkZip
        '
        Me.chkZip.BackColor = System.Drawing.SystemColors.Control
        Me.chkZip.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkZip.ForeColor = System.Drawing.Color.Navy
        Me.chkZip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkZip.Location = New System.Drawing.Point(56, 56)
        Me.chkZip.Name = "chkZip"
        Me.chkZip.Size = New System.Drawing.Size(152, 16)
        Me.chkZip.TabIndex = 2
        Me.chkZip.Text = "Compress (ZIP) output"
        Me.chkZip.UseVisualStyleBackColor = False
        '
        'cmbFormat
        '
        Me.cmbFormat.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmbFormat.ForeColor = System.Drawing.Color.Blue
        Me.cmbFormat.ItemHeight = 13
        Me.cmbFormat.Items.AddRange(New Object() {"Acrobat Format (*.pdf)", "Crystal Reports (*.rpt)", "CSV (*.csv)", "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", "MS Excel 97-2000 (*.xls)", "MS Word (*.doc)", "ODBC (*.odbc)", "Rich Text Format (*.rtf)", "Tab Separated (*.txt)", "Text (*.txt)", "TIFF (*.tif)", "XML (*.xml)"})
        Me.cmbFormat.Location = New System.Drawing.Point(56, 24)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.Size = New System.Drawing.Size(184, 21)
        Me.cmbFormat.Sorted = True
        Me.cmbFormat.TabIndex = 0
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.SystemColors.Control
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label22.ForeColor = System.Drawing.Color.Navy
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(8, 26)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 16)
        Me.Label22.TabIndex = 8
        Me.Label22.Text = "Format"
        '
        'chkSplit
        '
        Me.chkSplit.BackColor = System.Drawing.SystemColors.Control
        Me.chkSplit.Enabled = False
        Me.chkSplit.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkSplit.ForeColor = System.Drawing.Color.Navy
        Me.chkSplit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSplit.Location = New System.Drawing.Point(344, 24)
        Me.chkSplit.Name = "chkSplit"
        Me.chkSplit.Size = New System.Drawing.Size(16, 16)
        Me.chkSplit.TabIndex = 10
        Me.chkSplit.Text = "Split into seperate HTML files"
        Me.chkSplit.UseVisualStyleBackColor = False
        Me.chkSplit.Visible = False
        '
        'chkIncludeAttach
        '
        Me.chkIncludeAttach.BackColor = System.Drawing.SystemColors.Control
        Me.chkIncludeAttach.Checked = True
        Me.chkIncludeAttach.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkIncludeAttach.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkIncludeAttach.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.chkIncludeAttach, "Output_Formats.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkIncludeAttach, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkIncludeAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkIncludeAttach.Location = New System.Drawing.Point(8, 8)
        Me.chkIncludeAttach.Name = "chkIncludeAttach"
        Me.HelpProvider1.SetShowHelp(Me.chkIncludeAttach, True)
        Me.chkIncludeAttach.Size = New System.Drawing.Size(152, 16)
        Me.chkIncludeAttach.TabIndex = 0
        Me.chkIncludeAttach.Text = "Include attachment"
        Me.chkIncludeAttach.UseVisualStyleBackColor = False
        Me.chkIncludeAttach.Visible = False
        '
        'tbNaming
        '
        Me.tbNaming.Controls.Add(Me.GroupBox3)
        Me.tbNaming.Location = New System.Drawing.Point(4, 22)
        Me.tbNaming.Name = "tbNaming"
        Me.tbNaming.Size = New System.Drawing.Size(437, 394)
        Me.tbNaming.TabIndex = 2
        Me.tbNaming.Text = "Naming"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkUseKeyPar)
        Me.GroupBox3.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox3.Controls.Add(Me.Label26)
        Me.GroupBox3.Controls.Add(Me.cmbDateTime)
        Me.GroupBox3.Controls.Add(Me.chkCustomExt)
        Me.GroupBox3.Controls.Add(Me.txtCustomName)
        Me.GroupBox3.Controls.Add(Me.optNaming)
        Me.GroupBox3.Controls.Add(Me.optCustomName)
        Me.GroupBox3.Controls.Add(Me.txtCustomExt)
        Me.GroupBox3.Controls.Add(Me.chkAppendDateTime)
        Me.GroupBox3.Controls.Add(Me.txtDefault)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox3, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox3, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox3, True)
        Me.GroupBox3.Size = New System.Drawing.Size(424, 383)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        '
        'chkUseKeyPar
        '
        Me.chkUseKeyPar.AutoSize = True
        Me.chkUseKeyPar.Enabled = False
        Me.chkUseKeyPar.Location = New System.Drawing.Point(200, 79)
        Me.chkUseKeyPar.Name = "chkUseKeyPar"
        Me.chkUseKeyPar.Size = New System.Drawing.Size(118, 17)
        Me.chkUseKeyPar.TabIndex = 4
        Me.chkUseKeyPar.Text = "Use Key Parameter"
        Me.chkUseKeyPar.UseVisualStyleBackColor = True
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(200, 172)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 9
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(5, 176)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 17
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM", "yyyy-MM-dd", "hh:mm:ss"})
        Me.cmbDateTime.Location = New System.Drawing.Point(200, 138)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(200, 21)
        Me.cmbDateTime.TabIndex = 8
        '
        'chkCustomExt
        '
        Me.chkCustomExt.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkCustomExt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCustomExt.Location = New System.Drawing.Point(8, 103)
        Me.chkCustomExt.Name = "chkCustomExt"
        Me.chkCustomExt.Size = New System.Drawing.Size(184, 24)
        Me.chkCustomExt.TabIndex = 5
        Me.chkCustomExt.Text = "Customize output extension"
        '
        'txtCustomName
        '
        Me.txtCustomName.ContextMenu = Me.mnuInserter
        Me.txtCustomName.Enabled = False
        Me.txtCustomName.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomName.Location = New System.Drawing.Point(200, 56)
        Me.txtCustomName.Name = "txtCustomName"
        Me.txtCustomName.Size = New System.Drawing.Size(200, 21)
        Me.txtCustomName.TabIndex = 3
        Me.txtCustomName.Tag = "memo"
        '
        'optNaming
        '
        Me.optNaming.Checked = True
        Me.optNaming.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNaming.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNaming.Location = New System.Drawing.Point(8, 16)
        Me.optNaming.Name = "optNaming"
        Me.optNaming.Size = New System.Drawing.Size(192, 24)
        Me.optNaming.TabIndex = 1
        Me.optNaming.TabStop = True
        Me.optNaming.Text = "Use the default naming convention"
        '
        'optCustomName
        '
        Me.optCustomName.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optCustomName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustomName.Location = New System.Drawing.Point(8, 56)
        Me.optCustomName.Name = "optCustomName"
        Me.optCustomName.Size = New System.Drawing.Size(184, 24)
        Me.optCustomName.TabIndex = 2
        Me.optCustomName.Text = "Customize the output file name"
        '
        'txtCustomExt
        '
        Me.txtCustomExt.ContextMenu = Me.mnuInserter
        Me.txtCustomExt.Enabled = False
        Me.txtCustomExt.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomExt.Location = New System.Drawing.Point(200, 103)
        Me.txtCustomExt.Name = "txtCustomExt"
        Me.txtCustomExt.Size = New System.Drawing.Size(200, 21)
        Me.txtCustomExt.TabIndex = 6
        '
        'chkAppendDateTime
        '
        Me.chkAppendDateTime.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkAppendDateTime.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAppendDateTime.Location = New System.Drawing.Point(8, 136)
        Me.chkAppendDateTime.Name = "chkAppendDateTime"
        Me.chkAppendDateTime.Size = New System.Drawing.Size(192, 24)
        Me.chkAppendDateTime.TabIndex = 7
        Me.chkAppendDateTime.Text = "Append date/time to report output"
        '
        'txtDefault
        '
        Me.txtDefault.ContextMenu = Me.mnuInserter
        Me.txtDefault.ForeColor = System.Drawing.Color.Blue
        Me.txtDefault.Location = New System.Drawing.Point(200, 18)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.ReadOnly = True
        Me.txtDefault.Size = New System.Drawing.Size(200, 21)
        Me.txtDefault.TabIndex = 13
        Me.txtDefault.TabStop = False
        '
        'tbDelivery
        '
        Me.tbDelivery.Controls.Add(Me.GroupBox4)
        Me.tbDelivery.Location = New System.Drawing.Point(4, 22)
        Me.tbDelivery.Name = "tbDelivery"
        Me.tbDelivery.Size = New System.Drawing.Size(437, 394)
        Me.tbDelivery.TabIndex = 3
        Me.tbDelivery.Text = "Deferred Delivery"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.txtDefer)
        Me.GroupBox4.Controls.Add(Me.chkDefer)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox4, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox4, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox4.Name = "GroupBox4"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox4, True)
        Me.GroupBox4.Size = New System.Drawing.Size(424, 383)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(240, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Hours"
        '
        'txtDefer
        '
        Me.txtDefer.DecimalPlaces = 1
        Me.txtDefer.Enabled = False
        Me.txtDefer.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.txtDefer.Location = New System.Drawing.Point(168, 24)
        Me.txtDefer.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtDefer.Name = "txtDefer"
        Me.txtDefer.Size = New System.Drawing.Size(64, 21)
        Me.txtDefer.TabIndex = 1
        Me.txtDefer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkDefer
        '
        Me.chkDefer.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDefer.Location = New System.Drawing.Point(8, 22)
        Me.chkDefer.Name = "chkDefer"
        Me.chkDefer.Size = New System.Drawing.Size(160, 24)
        Me.chkDefer.TabIndex = 0
        Me.chkDefer.Text = "Defer delivery by"
        '
        'tbPGP
        '
        Me.tbPGP.Controls.Add(Me.UcPGP)
        Me.tbPGP.Controls.Add(Me.chkPGP)
        Me.tbPGP.Location = New System.Drawing.Point(4, 22)
        Me.tbPGP.Name = "tbPGP"
        Me.tbPGP.Size = New System.Drawing.Size(437, 394)
        Me.tbPGP.TabIndex = 4
        Me.tbPGP.Text = "PGP Encryption"
        '
        'chkPGP
        '
        Me.chkPGP.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPGP.Location = New System.Drawing.Point(8, 8)
        Me.chkPGP.Name = "chkPGP"
        Me.chkPGP.Size = New System.Drawing.Size(344, 24)
        Me.chkPGP.TabIndex = 0
        Me.chkPGP.Text = "Enable PGP Encryption"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdOK, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdOK, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(312, 518)
        Me.cmdOK.Name = "cmdOK"
        Me.HelpProvider1.SetShowHelp(Me.cmdOK, True)
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdCancel, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdCancel, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(397, 518)
        Me.cmdCancel.Name = "cmdCancel"
        Me.HelpProvider1.SetShowHelp(Me.cmdCancel, True)
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'txtName
        '
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(119, 5)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(323, 21)
        Me.txtName.TabIndex = 0
        '
        'Label1
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label1, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Destination Name"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem8, Me.MenuItem11, Me.MenuItem9, Me.MenuItem10, Me.MenuItem12})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.Text = "Default Subject"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 5
        Me.MenuItem9.Text = "Default Message"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "Default Signature"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 7
        Me.MenuItem12.Text = "Default Attachment"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.chkStatus, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkStatus, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkStatus.Location = New System.Drawing.Point(119, 32)
        Me.chkStatus.Name = "chkStatus"
        Me.HelpProvider1.SetShowHelp(Me.chkStatus, True)
        Me.chkStatus.Size = New System.Drawing.Size(64, 17)
        Me.chkStatus.TabIndex = 1
        Me.chkStatus.Text = "Enabled"
        Me.chkStatus.UseVisualStyleBackColor = True
        '
        'grpSharePoint
        '
        Me.grpSharePoint.Controls.Add(Me.btnEdit)
        Me.grpSharePoint.Controls.Add(Me.btnRemove)
        Me.grpSharePoint.Controls.Add(Me.btnAdd)
        Me.grpSharePoint.Controls.Add(Me.lsvSharePointLibs)
        Me.grpSharePoint.Location = New System.Drawing.Point(8, 8)
        Me.grpSharePoint.Name = "grpSharePoint"
        Me.grpSharePoint.Size = New System.Drawing.Size(424, 378)
        Me.grpSharePoint.TabIndex = 51
        Me.grpSharePoint.TabStop = False
        Me.grpSharePoint.Text = "SharePoint Output"
        Me.grpSharePoint.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(334, 46)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(84, 25)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRemove.Location = New System.Drawing.Point(334, 76)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(84, 25)
        Me.btnRemove.TabIndex = 4
        Me.btnRemove.Text = "Remove"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(334, 16)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(84, 25)
        Me.btnAdd.TabIndex = 4
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'lsvSharePointLibs
        '
        Me.lsvSharePointLibs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader10, Me.ColumnHeader11, Me.ColumnHeader12})
        Me.lsvSharePointLibs.FullRowSelect = True
        Me.lsvSharePointLibs.HideSelection = False
        Me.lsvSharePointLibs.Location = New System.Drawing.Point(6, 16)
        Me.lsvSharePointLibs.Name = "lsvSharePointLibs"
        Me.lsvSharePointLibs.Size = New System.Drawing.Size(322, 356)
        Me.lsvSharePointLibs.TabIndex = 3
        Me.lsvSharePointLibs.UseCompatibleStateImageBehavior = False
        Me.lsvSharePointLibs.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Server"
        Me.ColumnHeader10.Width = 135
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Library"
        Me.ColumnHeader11.Width = 91
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "User Name"
        Me.ColumnHeader12.Width = 74
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(11, 21)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(376, 112)
        Me.UcDSN.TabIndex = 0
        '
        'UcFTP
        '
        Me.UcFTP.BackColor = System.Drawing.SystemColors.Control
        Me.UcFTP.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcFTP.Location = New System.Drawing.Point(6, 22)
        Me.UcFTP.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
            "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;"
        Me.UcFTP.m_ftpSuccess = False
        Me.UcFTP.m_ShowBrowse = True
        Me.UcFTP.Name = "UcFTP"
        Me.UcFTP.Size = New System.Drawing.Size(411, 190)
        Me.UcFTP.TabIndex = 9
        '
        'DividerLabel1
        '
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(7, 217)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(405, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 6
        '
        'UcPGP
        '
        Me.UcPGP.Enabled = False
        Me.UcPGP.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcPGP, "Destinations.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcPGP, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcPGP.Location = New System.Drawing.Point(8, 32)
        Me.UcPGP.Name = "UcPGP"
        Me.HelpProvider1.SetShowHelp(Me.UcPGP, True)
        Me.UcPGP.Size = New System.Drawing.Size(424, 359)
        Me.UcPGP.TabIndex = 1
        '
        'frmDestinationSelect
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(478, 546)
        Me.Controls.Add(Me.chkStatus)
        Me.Controls.Add(Me.cmdInsert2)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkburst)
        Me.Controls.Add(Me.cmdInsert)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDestinationSelect"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Destination"
        Me.grpEmail.ResumeLayout(False)
        Me.grpEmail.PerformLayout()
        Me.pnMailServer.ResumeLayout(False)
        Me.grpSender.ResumeLayout(False)
        Me.grpSender.PerformLayout()
        Me.pnReadReciepts.ResumeLayout(False)
        Me.grpPrinter.ResumeLayout(False)
        Me.grpFile.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.grpHouseKeeping.ResumeLayout(False)
        CType(Me.txtHouseNum, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.grpFTP.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.tabDestination.ResumeLayout(False)
        Me.tbDestination.ResumeLayout(False)
        Me.grpODBC.ResumeLayout(False)
        Me.grpODBC.PerformLayout()
        Me.grpFax.ResumeLayout(False)
        Me.grpFax.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.grpSMS.ResumeLayout(False)
        Me.grpSMS.PerformLayout()
        Me.tbOutput.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.grpZip.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.tbNaming.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbDelivery.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtDefer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbPGP.ResumeLayout(False)
        Me.grpSharePoint.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Public Sub EditDestinaton(ByVal nID As Integer, _
    Optional ByVal IsDynamic As Boolean = False, _
    Optional ByVal IsPackage As Boolean = False, _
    Optional ByVal sReportTitle As String = "", Optional ByVal ExcelBurst As Boolean = False, _
    Optional ByVal StaticDest As Boolean = False, Optional ByVal SmartID As Integer = 99999, _
    Optional ByVal IsBursting As Boolean = False, Optional ByVal isDataDriven As Boolean = False)

        sMode = "Edit"

        Dim SQL As String
        Dim sWhere As String = " WHERE DestinationID = " & nID
        Dim NewID As Integer
        Dim nReportID As Integer
        Dim nPackID As Integer
        Dim s() As String

        xDestinationID = nID

        sTitle = sReportTitle

        Dynamic = IsDynamic
        Package = IsPackage
        IsStatic = StaticDest
        Bursting = IsBursting
        DataDriven = isDataDriven

        If IsDynamic = True Then
            s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS"}

            If StaticDest = False Then
                cmdTo.Visible = False
                txtTo.Visible = False
                UcFTP.btnBrowse2.Visible = False
                UcFTP.txtFTPServer.Visible = False
                chkHouseKeeping.Enabled = False
            End If
        ElseIf ExcelBurst = True Then
            s = New String() {"Email", "Disk", "FTP"}
            cmdTo.Visible = True
            txtTo.Visible = True

            cmbFormat.Items.Clear()

            Dim s1() As String = New String() {"MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", _
            "MS Excel 97-2000 (*.xls)"}

            cmbFormat.Items.AddRange(s1)
        ElseIf SmartID <> 99999 And SmartID <> 2777 And SmartID <> 0 Then
            s = New String() {"Email", "Disk", "FTP"}

            cmbFormat.Items.Clear()

            Dim s1 As String() = New String() {"MS Excel  (*.xls)", "XML (*.xml)"}

            cmbFormat.Items.AddRange(s1)

            IsSmart = True
        ElseIf Me.m_IsQuery = True Then
            s = New String() {"Email", "Disk", "FTP"}
            tbDelivery.Enabled = False
            tbPGP.Enabled = False
        Else
            If Package = False Then
                s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "ODBC", "SharePoint"}
            Else
                s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "SharePoint"}
            End If

            cmdTo.Visible = True
            txtTo.Visible = True
        End If

        cmbDestination.Items.AddRange(s)

        cmbDestination.Sorted = True

        SQL = "SELECT * FROM DestinationAttr " & sWhere

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        If oRs.EOF = False Then

            cmbDestination.Text = oRs("destinationtype").Value
            txtName.Text = oRs("destinationname").Value

            'lets add the new destination items
            If Package = True Then
                cmbFormat.Items.Add("Package")
                cmbFormat.Text = "Package"
            Else
                cmbFormat.Text = oRs("outputformat").Value
            End If

            If oRs("customext").Value.Length > 0 Then
                chkCustomExt.Checked = True
                txtCustomExt.Text = oRs("customext").Value
            End If

            chkAppendDateTime.Checked = Convert.ToBoolean(oRs("appenddatetime").Value)
            cmbDateTime.Text = IsNull(oRs("datetimeformat").Value)

            Try
                chkSplit.Checked = Convert.ToBoolean(oRs("htmlsplit").Value)
            Catch
                chkSplit.Checked = False
            End Try

            If oRs("customname").Value.Length > 0 Then
                optCustomName.Checked = True
                txtCustomName.Text = oRs("customname").Value
            Else
                optNaming.Checked = True

                If sTitle.Length > 0 And cmbFormat.Text.IndexOf("*") > -1 Then
                    txtDefault.Text = sTitle & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
                End If
            End If

            chkZip.Checked = Convert.ToBoolean(oRs("compress").Value)

            chkEmbed.Checked = Convert.ToBoolean(oRs("embed").Value)

            chkIncludeAttach.Checked = Convert.ToBoolean(oRs("includeattach").Value)

            Try
                If IsNull(oRs("deferdelivery").Value) = 1 Then
                    chkDefer.Checked = True
                    txtDefer.Value = oRs("deferby").Value
                End If
            Catch : End Try

            Try
                chkReadReceipt.Checked = Convert.ToBoolean(oRs("readreceipts").Value)
            Catch ex As Exception
                chkReadReceipt.Checked = False
            End Try

            txtAdjustStamp.Value = IsNull(oRs("adjuststamp").Value, 0)

            Try
                chkStatus.Checked = Convert.ToBoolean(oRs("enabledstatus").Value)
            Catch ex As Exception
                chkStatus.Checked = True
            End Try

            Try
                chkZipSecurity.Checked = Convert.ToBoolean(oRs("encryptzip").Value)
                cmbEncryptionLevel.Text = Convert.ToString(oRs("encryptziplevel").Value)
                txtZipCode.Text = Decrypt(oRs("encryptzipcode").Value, "preggers")
                txtConfirmCode.Text = Decrypt(oRs("encryptzipcode").Value, "preggers")
            Catch : End Try


            Select Case cmbDestination.Text.ToLower
                Case "email"
                    txtTo.Text = IsNull(oRs("sendto").Value)
                    txtCC.Text = IsNull(oRs("cc").Value)
                    txtBCC.Text = IsNull(oRs("bcc").Value)
                    txtSubject.Text = IsNull(oRs("subject").Value)
                    txtMsg.Text = IsNull(oRs("message").Value)
                    txtAttach.Text = IsNull(oRs("extras").Value)
                    cmbMailFormat.Text = IsNull(oRs("mailformat").Value)

                    If cmbMailFormat.Text.Length = 0 Then cmbMailFormat.Text = "TEXT"

                    If MailType = MarsGlobal.gMailType.SMTP Then
                        cmbServer.Text = IsNull(oRs("smtpserver").Value, "Default")
                    End If

                    Try
                        Me.txtSenderAddress.Text = IsNull(oRs("senderaddress").Value)
                        Me.txtSenderName.Text = IsNull(oRs("sendername").Value)
                    Catch : End Try
                Case "disk"
                    Dim paths As String = IsNull(oRs("outputpath").Value)

                    For Each si As String In paths.Split("|")
                        If si.Length > 0 Then
                            lsvPaths.Items.Add(si)
                        End If
                    Next

                    'txtDirectory.Text = IsNull(oRs("outputpath").Value)

                    Try
                        chkDUN.Checked = Convert.ToBoolean(oRs("UseDUN").Value)
                        cmbDUN.Text = oRs("dunname").Value
                    Catch : End Try
                Case "ftp"
                    Dim ftpServers As String = IsNull(oRs("ftpserver").Value)
                    Dim ftpUsers As String = IsNull(oRs("ftpusername").Value)
                    Dim ftpPasswords As String = IsNull(oRs("ftppassword").Value)
                    Dim ftpPaths As String = IsNull(oRs("ftppath").Value)
                    Dim ftpTypes As String = IsNull(oRs("ftptype").Value, "FTP")
                    Dim ftpPassives As String = IsNull(oRs("ftppassive").Value, "")
                    Dim ftpOptions As String = IsNull(oRs("ftpoptions").Value, "")

                    For I As Integer = 0 To ftpUsers.Split("|").GetUpperBound(0)
                        Dim item As ListViewItem = New ListViewItem

                        item.Text = ftpServers.Split("|")(I)

                        Try


                            With item.SubItems
                                .Add(ftpUsers.Split("|")(I))
                                .Add(ftpPasswords.Split("|")(I))
                                .Add(ftpPaths.Split("|")(I))
                                .Add(ftpTypes.Split("|")(I))

                                Try
                                    .Add(ftpPassives.Split("|")(I))
                                Catch ex As Exception
                                    .Add("0")
                                End Try

                                Try
                                    .Add(ftpOptions.Split("|")(I))
                                Catch ex As Exception
                                    .Add("")
                                End Try
                            End With

                            lsvFTP.Items.Add(item)

                        Catch : End Try
                    Next

                Case "printer"
                    Dim oP As ADODB.Recordset

                    oP = clsMarsData.GetData("SELECT * FROM PrinterAttr WHERE " & _
                    "DestinationID = " & nID)

                    If oP Is Nothing Then Exit Select

                    Do While oP.EOF = False
                        Dim oItem As New ListViewItem

                        oItem.Text = oP("printername").Value
                        oItem.SubItems.Add(oP("copies").Value)
                        oItem.Tag = oP("printerid").Value

                        lsvPrinters.Items.Add(oItem)

                        oP.MoveNext()
                    Loop

                    oP.Close()
                Case "fax"
                    'sendto - fax number
                    'subject - fax device
                    'cc - to name
                    'bcc - from name
                    'message - comments
                    txtFaxNumber.Text = oRs("sendto").Value
                    cmbFaxDevice.Text = oRs("subject").Value
                    txtFaxTo.Text = oRs("cc").Value
                    txtFaxFrom.Text = oRs("bcc").Value
                    txtFaxComments.Text = oRs("message").Value
                Case "sms"
                    txtSMSNumber.Text = oRs("sendto").Value
                    txtSMSMsg.Text = IsNull(oRs("message").Value)
                Case "odbc"
                    'sendto - dsn name
                    'subject - dsn username
                    'cc - dsn password
                    'message - table name
                    'copies - overwrite

                    With UcDSN
                        .cmbDSN.Text = oRs("sendto").Value
                        .txtUserID.Text = oRs("subject").Value
                        .txtPassword.Text = _DecryptDBValue(oRs("cc").Value)
                    End With

                    txtTableName.Text = CType(oRs("message").Value, String).Replace("[", "").Replace("]", "")

                    Try
                        chkOverwrite.Checked = Convert.ToBoolean(oRs("copies").Value)
                    Catch ex As Exception
                        chkOverwrite.Checked = False
                    End Try
                Case "sharepoint"
                    Dim spServers As String = IsNull(oRs("ftpserver").Value)
                    Dim spUsers As String = IsNull(oRs("ftpusername").Value)
                    Dim spPasswords As String = IsNull(oRs("ftppassword").Value)
                    Dim spLibs As String = IsNull(oRs("ftppath").Value)

                    For I As Integer = 0 To spServers.Split("|").GetUpperBound(0)
                        Dim item As ListViewItem = New ListViewItem

                        If spServers.Split("|")(I) = "" Then Continue For

                        item.Text = spServers.Split("|")(I)

                        Try
                            With item.SubItems
                                .Add(spLibs.Split("|")(I))
                                .Add(spUsers.Split("|")(I))
                                Try
                                    .Add(spPasswords.Split("|")(I))
                                Catch
                                    .Add("")
                                End Try
                            End With

                            Me.lsvSharePointLibs.Items.Add(item)
                        Catch : End Try
                    Next
            End Select

        End If

        oRs.Close()

        Try
            oRs = clsMarsData.GetData("SELECT * FROM HouseKeepingAttr WHERE DestinationID =" & nID)

            If oRs.EOF = False Then
                chkHouseKeeping.Checked = True
                txtHouseNum.Value = oRs("agevalue").Value
                cmbUnit.Text = oRs("ageunit").Value
            End If

            oRs.Close()
        Catch : End Try

        cmbDestination.Enabled = False

        oRs = clsMarsData.GetData("SELECT * FROM PGPAttr WHERE DestinationID = " & nID)

        If oRs.EOF = False Then
            chkPGP.Checked = True

            Dim sKeys As String

            With UcPGP
                .txtPGPUserID.Text = oRs("pgpid").Value

                sKeys = IsNull(oRs("keylocation").Value)

                If sKeys.IndexOf("|") > -1 Then
                    .txtKeyLocation.Text = sKeys.Split("|")(0)
                    .txtSecring.Text = sKeys.Split("|")(1)
                Else
                    .txtKeyLocation.Text = sKeys
                End If

                Try
                    .chkPGPExt.Checked = Convert.ToBoolean(oRs("pgpext").Value)
                Catch
                    .chkPGPExt.Checked = False
                End Try
            End With
        End If

        If Package = True Or Me.disableEmbed = True Then
            Me.chkEmbed.Checked = False
            Me.chkEmbed.Enabled = False
        End If

        Me.formLoaded = True

        If MailType = gMailType.MAPI Then
            Me.grpSender.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim sVals As String
        Dim sCols As String

        Select Case cmbDestination.Text.ToLower
            Case "email"
                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "SendTo = '" & SQLPrepare(txtTo.Text) & "'," & _
                "Cc = '" & SQLPrepare(txtCC.Text) & "'," & _
                "Bcc = '" & SQLPrepare(txtBCC.Text) & "'," & _
                "Subject = '" & SQLPrepare(txtSubject.Text) & "'," & _
                "Message = '" & SQLPrepare(txtMsg.Text) & "'," & _
                "Extras = '" & SQLPrepare(txtAttach.Text) & "'," & _
                "MailFormat = '" & cmbMailFormat.Text & "'," & _
                "SMTPServer = '" & SQLPrepare(cmbServer.Text) & "'," & _
                "SenderName = '" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
                "SenderAddress = '" & SQLPrepare(Me.txtSenderAddress.Text) & "'"
            Case "disk"
                Dim paths As String = ""

                For Each item As ListViewItem In lsvPaths.Items
                    paths &= item.Text & "|"
                Next

                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "OutputPath = '" & SQLPrepare(paths) & "'," & _
                "UseDUN = " & Convert.ToInt32(chkDUN.Checked) & "," & _
                "DUNName = '" & SQLPrepare(cmbDUN.Text) & "'"
            Case "ftp"
                Dim ftpServers As String = ""
                Dim ftpUsers As String = ""
                Dim ftpPasswords As String = ""
                Dim ftpPaths As String = ""
                Dim ftpTypes As String = ""
                Dim ftpPassives As String = ""
                Dim ftpOptions As String = ""

                For Each item As ListViewItem In lsvFTP.Items
                    ftpServers &= item.Text & "|"
                    ftpUsers &= item.SubItems(1).Text & "|"
                    ftpPasswords &= item.SubItems(2).Text & "|"
                    ftpPaths &= item.SubItems(3).Text & "|"
                    ftpTypes &= item.SubItems(4).Text & "|"
                    Try
                        ftpPassives &= item.SubItems(5).Text & "|"
                    Catch
                        ftpPassives &= "0" & "|"
                    End Try

                    Try
                        ftpOptions &= item.SubItems(6).Text & "|"
                    Catch ex As Exception
                        ftpOptions &= "" & "|"
                    End Try
                Next

                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "FTPServer = '" & SQLPrepare(ftpServers) & "'," & _
                "FTPUserName = '" & SQLPrepare(ftpUsers) & "'," & _
                "FTPPassword = '" & SQLPrepare(ftpPasswords) & "'," & _
                "FTPPath = '" & SQLPrepare(ftpPaths) & "'," & _
                "FTPType = '" & SQLPrepare(ftpTypes) & "'," & _
                "FTPPassive = '" & ftpPassives & "'," & _
                "FTPOptions = '" & SQLPrepare(ftpOptions) & "'"
            Case "fax"
                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "SendTo = '" & SQLPrepare(txtFaxNumber.Text) & "'," & _
                "Subject = '" & SQLPrepare(cmbFaxDevice.Text) & "'," & _
                "Cc = '" & SQLPrepare(txtFaxTo.Text) & "'," & _
                "Bcc = '" & SQLPrepare(txtFaxFrom.Text) & "'," & _
                "Message ='" & SQLPrepare(txtFaxComments.Text) & "'"
            Case "sms"
                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "SendTo = '" & SQLPrepare(txtSMSNumber.Text) & "'," & _
                "Message = '" & SQLPrepare(txtSMSMsg.Text) & "'"
            Case "odbc"
                'sendto - dsn name
                'subject - dsn username
                'cc - dsn password
                'message - table name
                'copies - overwrite
                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "SendTo = '" & SQLPrepare(UcDSN.cmbDSN.Text) & "'," & _
                "Subject = '" & SQLPrepare(UcDSN.txtUserID.Text) & "'," & _
                "Cc = '" & SQLPrepare(_EncryptDBValue(UcDSN.txtPassword.Text)) & "'," & _
                "Message = '[" & SQLPrepare(txtTableName.Text) & "]'," & _
                "Copies =" & Convert.ToInt32(chkOverwrite.Checked)
            Case "sharepoint"
                Dim spServers As String = ""
                Dim spUsers As String = ""
                Dim spPasswords As String = ""
                Dim spLibs As String = ""
                Dim spTypes As String = ""
                Dim spPassives As String = ""
                Dim spOptions As String = ""

                For Each item As ListViewItem In Me.lsvSharePointLibs.Items

                    If item.Text = "" Then Continue For

                    spServers &= item.Text & "|"
                    spLibs &= item.SubItems(1).Text & "|"
                    spUsers &= item.SubItems(2).Text & "|"
                    spPasswords &= item.SubItems(3).Text & "|"
                Next

                sVals = "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
                "FTPServer = '" & SQLPrepare(spServers) & "'," & _
                "FTPUserName = '" & SQLPrepare(spUsers) & "'," & _
                "FTPPassword = '" & SQLPrepare(spPasswords) & "'," & _
                "FTPPath = '" & SQLPrepare(spLibs) & "'"
        End Select

        Dim nEncrypt As Integer
        Try
            nEncrypt = cmbEncryptionLevel.Text
        Catch ex As Exception
            nEncrypt = 0
        End Try

        sVals &= ",OutputFormat ='" & cmbFormat.Text & "'," & _
        "CustomExt ='" & SQLPrepare(txtCustomExt.Text) & "'," & _
        "AppendDateTime =" & Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
        "DateTimeFormat ='" & cmbDateTime.Text & "'," & _
        "CustomName ='" & SQLPrepare(txtCustomName.Text) & "'," & _
        "Includeattach =" & Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
        "Compress =" & Convert.ToInt32(chkZip.Checked) & "," & _
        "Embed =" & Convert.ToInt32(chkEmbed.Checked) & "," & _
        "HTMLSplit =" & Convert.ToInt32(chkSplit.Checked) & "," & _
        "DeferDelivery =" & Convert.ToInt32(chkDefer.Checked) & "," & _
        "DeferBy ='" & txtDefer.Value & "'," & _
        "EncryptZip =" & Convert.ToInt32(chkZipSecurity.Checked) & "," & _
        "EncryptZipLevel =" & nEncrypt & "," & _
        "EncryptZipCode ='" & Encrypt(txtZipCode.Text, "preggers") & "'," & _
        "ReadReceipts = " & Convert.ToInt32(Me.chkReadReceipt.Checked) & "," & _
        "AdjustStamp = " & txtAdjustStamp.Value & "," & _
        "EnabledStatus =" & Convert.ToInt32(chkStatus.Checked)

        If cmbDestination.Text.ToLower = "printer" Then

            clsMarsData.WriteData("UPDATE DestinationAttr SET " & _
            "DestinationName = '" & SQLPrepare(txtName.Text) & "'," & _
            "EnabledStatus = " & Convert.ToInt32(Me.chkStatus.Checked) & _
            " WHERE DestinationID =" & nID)

            'save the printers
            Dim I As Integer = 1

            clsMarsData.WriteData("UPDATE PrinterAttr SET DestinationID =" & nID & " WHERE DestinationID= 99999")
        Else
            SQL = "UPDATE DestinationAttr SET " & sVals & sWhere

            clsMarsData.WriteData(SQL)
        End If

        clsMarsData.WriteData("DELETE FROM HouseKeepingAttr WHERE DestinationID = " & nID)

        If chkHouseKeeping.Checked = True Then
            sCols = "HouseID,DestinationID,AgeValue,AgeUnit"

            sVals = clsMarsData.CreateDataID("housekeepingattr", "houseid") & "," & _
            nID & "," & _
            txtHouseNum.Value & "," & _
            "'" & cmbUnit.Text & "'"

            SQL = "INSERT INTO HouseKeepingAttr (" & sCols & ") VALUES(" & sVals & ")"

            clsMarsData.WriteData(SQL)
        End If

        clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID =" & nID)

        If chkPGP.Checked = True Then
            With UcPGP
                ._AddKey(nID)
            End With
        Else
            clsMarsData.WriteData("DELETE FROM PGPAttr WHERE DestinationID = " & nID)
        End If
    End Sub
    Public Function AddDestination(ByVal DestOrderID As Integer, Optional ByVal nReportID As Integer = 99999, _
    Optional ByVal nPackID As Integer = 99999, _
    Optional ByVal IsDynamic As Boolean = False, _
    Optional ByVal IsPackage As Boolean = False, _
    Optional ByVal PreDestination As String = "", _
    Optional ByVal sReportTitle As String = "", _
    Optional ByVal ExcelBurst As Boolean = False, Optional ByVal StaticDest As Boolean = False, _
    Optional ByVal nSmartID As Integer = 99999, _
    Optional ByVal IsBursting As Boolean = False, Optional ByVal isDataDriven As Boolean = False) As Integer

        Try
            Dim DestinationID As Int64 = clsMarsData.CreateDataID("DestinationAttr", "DestinationID")

            sMode = "Add"

            Dim s() As String

            sTitle = sReportTitle

            txtDefault.Text = sTitle

            Dynamic = IsDynamic
            Package = IsPackage
            IsStatic = StaticDest
            Bursting = IsBursting
            DataDriven = isDataDriven

            chkStatus.Checked = True

            If IsDynamic = True Then
                s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS"}

                If StaticDest = False Then
                    cmdTo.Visible = False
                    txtTo.Visible = False
                    UcFTP.txtFTPServer.Visible = False
                    UcFTP.btnVerify.Visible = False
                    cmdFaxTo.Visible = False
                    txtFaxNumber.Visible = False
                    txtSMSNumber.Visible = False
                    cmdSMSTo.Visible = False
                    chkHouseKeeping.Enabled = False
                End If
            ElseIf ExcelBurst = True Then
                s = New String() {"Email", "Disk", "FTP"}
                cmdTo.Visible = True
                txtTo.Visible = True

                cmbFormat.Items.Clear()

                Dim s1() As String = New String() {"MS Excel - Data Only (*.xls)", "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)", _
                "MS Excel 97-2000 (*.xls)"}

                cmbFormat.Items.AddRange(s1)
            ElseIf nSmartID <> 99999 And nSmartID <> 2777 And nSmartID <> 0 Then
                s = New String() {"Email", "Disk", "FTP"}

                cmbFormat.Items.Clear()

                Dim s1 As String() = New String() {"MS Excel  (*.xls)", "XML (*.xml)"}

                cmbFormat.Items.AddRange(s1)

                IsSmart = True
            ElseIf Me.m_IsQuery = True Then
                s = New String() {"Email", "Disk", "FTP"}
                tbDelivery.Enabled = False
                tbPGP.Enabled = False
            Else
                If Package = False Then
                    s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "ODBC", "SharePoint"}
                Else
                    s = New String() {"Email", "Disk", "Printer", "FTP", "Fax", "SMS", "SharePoint"}
                End If

                cmdTo.Visible = True
                txtTo.Visible = True
            End If

            cmbDestination.Items.AddRange(s)

            cmbDestination.Sorted = True

            Try
                If PreDestination.Length > 0 Then
                    cmbDestination.Text = PreDestination
                    cmbDestination.Enabled = False
                End If
            Catch
            End Try

            cmbMailFormat.Text = "TEXT"
            cmbServer.Text = "Default"

            If Package = True Or Me.disableEmbed = True Then
                Me.chkEmbed.Checked = False
                Me.chkEmbed.Enabled = False
            End If

            Me.formLoaded = True

            If MailType = gMailType.MAPI Then
                Me.grpSender.Enabled = False
            End If

            Me.ShowDialog()

            If UserCancel = True Then Return 0

            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim I As Integer = 1
            Dim NewID As Integer
            Dim paths As String = ""

            sCols = "destinationid,destinationtype,destinationname,"

            Select Case cmbDestination.Text.ToLower
                Case "email"
                    sCols &= "SendTo,CC,Bcc,Subject,Message,Extras,MailFormat,SMTPServer,SenderName,SenderAddress,"

                    sVals = DestinationID & "," & _
                    "'Email'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtTo.Text) & "'," & _
                    "'" & SQLPrepare(txtCC.Text) & "'," & _
                    "'" & SQLPrepare(txtBCC.Text) & "'," & _
                    "'" & SQLPrepare(txtSubject.Text) & "'," & _
                    "'" & SQLPrepare(txtMsg.Text) & "'," & _
                    "'" & SQLPrepare(txtAttach.Text) & "'," & _
                    "'" & cmbMailFormat.Text & "'," & _
                    "'" & SQLPrepare(cmbServer.Text) & "'," & _
                    "'" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
                    "'" & SQLPrepare(Me.txtSenderAddress.Text) & "'"

                Case "disk"
                    For Each item As ListViewItem In lsvPaths.Items
                        paths &= item.Text & "|"
                    Next

                    sCols &= "OutputPath,"

                    sVals = DestinationID & "," & _
                    "'Disk'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(paths) & "'"
                Case "printer"
                    sCols &= "Printername,Copies,"
                Case "ftp"
                    Dim ftpServers As String = ""
                    Dim ftpUsers As String = ""
                    Dim ftpPasswords As String = ""
                    Dim ftpPaths As String = ""
                    Dim ftpTypes As String = ""
                    Dim ftpPassives As String = ""
                    Dim ftpOptions As String = ""

                    For Each item As ListViewItem In lsvFTP.Items
                        ftpServers &= item.Text & "|"
                        ftpUsers &= item.SubItems(1).Text & "|"
                        ftpPasswords &= item.SubItems(2).Text & "|"
                        ftpPaths &= item.SubItems(3).Text & "|"
                        ftpTypes &= item.SubItems(4).Text & "|"
                        ftpPassives &= item.SubItems(5).Text & "|"
                        ftpOptions &= item.SubItems(6).Text & "|"
                    Next

                    sCols &= "FTPServer,FTPUserName,FTPPassword,FTPPath,FTPType,FTPPassive,FTPOptions,"

                    sVals = DestinationID & "," & _
                    "'FTP'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(ftpServers) & "'," & _
                    "'" & SQLPrepare(ftpUsers) & "'," & _
                    "'" & SQLPrepare(ftpPasswords) & "'," & _
                    "'" & SQLPrepare(ftpPaths) & "'," & _
                    "'" & SQLPrepare(ftpTypes) & "'," & _
                    "'" & ftpPassives & "'," & _
                    "'" & SQLPrepare(ftpOptions) & "'"
                Case "fax"
                    'sendto - fax number
                    'subject - fax device
                    'cc - to name
                    'bcc - from name
                    'message - comments
                    sCols &= "SendTo,Subject,Cc,Bcc,Message,"

                    sVals = DestinationID & "," & _
                    "'Fax'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxNumber.Text) & "'," & _
                    "'" & SQLPrepare(cmbFaxDevice.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxTo.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxFrom.Text) & "'," & _
                    "'" & SQLPrepare(txtFaxComments.Text) & "'"
                Case "sms"
                    sCols &= "SendTo,Message,"

                    sVals = DestinationID & "," & _
                    "'SMS'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(txtSMSNumber.Text) & "'," & _
                    "'" & SQLPrepare(txtSMSMsg.Text) & "'"
                Case "odbc"
                    'sendto - dsn name
                    'subject - dsn username
                    'cc - dsn password
                    'message - table name
                    'copies - overwrite table

                    sCols &= "SendTo,Subject,Cc,Message,Copies,"

                    sVals = DestinationID & "," & _
                    "'ODBC'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(UcDSN.cmbDSN.Text) & "'," & _
                    "'" & SQLPrepare(UcDSN.txtUserID.Text) & "'," & _
                    "'" & SQLPrepare(_EncryptDBValue(UcDSN.txtPassword.Text)) & "'," & _
                    "'[" & SQLPrepare(txtTableName.Text) & "]'," & _
                    Convert.ToInt32(chkOverwrite.Checked)
                Case "sharepoint"
                    'will use FTP fields for sharepoint info storage
                    Dim spServers As String = ""
                    Dim spUsers As String = ""
                    Dim spPasswords As String = ""
                    Dim spLibs As String = ""

                    For Each item As ListViewItem In Me.lsvSharePointLibs.Items
                        spServers &= item.Text & "|"
                        spLibs &= item.SubItems(1).Text & "|"
                        spUsers &= item.SubItems(2).Text & "|"
                        spPasswords &= item.SubItems(3).Text & "|"
                    Next

                    sCols &= "FTPServer,FTPPath,FTPUserName,FTPPassword,"

                    sVals = DestinationID & "," & _
                    "'SharePoint'," & _
                    "'" & SQLPrepare(txtName.Text) & "'," & _
                    "'" & SQLPrepare(spServers) & "'," & _
                    "'" & SQLPrepare(spLibs) & "'," & _
                    "'" & SQLPrepare(spUsers) & "'," & _
                    "'" & SQLPrepare(spPasswords) & "'"
            End Select

            sCols &= "ReportID,PackID,OutputFormat,Customext,AppendDateTime," & _
            "DateTimeFormat,CustomName,IncludeAttach,Compress,Embed,HTMLSplit," & _
            "DeferDelivery,DeferBy,EncryptZip,EncryptZipLevel,EncryptZipCode,UseDUN,DUNName,SmartID," & _
            "ReadReceipts,AdjustStamp,EnabledStatus,DestOrderID"

            Dim enLevel As Integer

            Try
                enLevel = cmbEncryptionLevel.Text
            Catch ex As Exception
                enLevel = 0
            End Try

            If cmbDestination.Text.ToLower = "printer" Then

                sVals = DestinationID & "," & _
                "'Printer'," & _
                "'" & SQLPrepare(txtName.Text) & "'," & _
                "'Printer'," & _
                "1," & _
                nReportID & "," & _
                nPackID & "," & _
                "'" & cmbFormat.Text & "'," & _
                "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
                Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
                "'" & cmbDateTime.Text & "'," & _
                "'" & SQLPrepare(txtCustomName.Text) & "'," & _
                Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
                Convert.ToInt32(chkZip.Checked) & "," & _
                Convert.ToInt32(chkEmbed.Checked) & "," & _
                Convert.ToInt32(chkSplit.Checked) & "," & _
                Convert.ToInt32(chkDefer.Checked) & "," & _
                "'" & txtDefer.Value & "'," & _
                Convert.ToInt32(chkZipSecurity.Checked) & "," & _
                enLevel & "," & _
                "'" & SQLPrepare(Encrypt(txtZipCode.Text, "preggers")) & "',0,''," & nSmartID & "," & _
                Convert.ToInt32(chkReadReceipt.Checked) & "," & _
                txtAdjustStamp.Value & "," & _
                Convert.ToInt32(chkStatus.Checked) & "," & _
                DestOrderID

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") VALUES (" & sVals & ")"

                clsMarsData.WriteData(SQL)

                sCols = "printerid,reportid,packid,printername,copies,destinationid"

                SQL = "UPDATE PrinterAttr SET DestinationID = " & DestinationID & " WHERE DestinationID = 99999"

                clsMarsData.WriteData(SQL)
            Else

                sVals &= "," & nReportID & "," & _
                nPackID & "," & _
                "'" & cmbFormat.Text & "'," & _
                "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
                Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
                "'" & cmbDateTime.Text & "'," & _
                "'" & SQLPrepare(txtCustomName.Text) & "'," & _
                Convert.ToInt32(chkIncludeAttach.Checked) & "," & _
                Convert.ToInt32(chkZip.Checked) & "," & _
                Convert.ToInt32(chkEmbed.Checked) & "," & _
                Convert.ToInt32(chkSplit.Checked) & "," & _
                Convert.ToInt32(chkDefer.Checked) & "," & _
                "'" & txtDefer.Value & "'," & _
                Convert.ToInt32(chkZipSecurity.Checked) & "," & _
                Convert.ToInt32(enLevel) & "," & _
                "'" & SQLPrepare(Encrypt(txtZipCode.Text, "preggers")) & "'," & _
                Convert.ToInt32(chkDUN.Checked) & "," & _
                "'" & SQLPrepare(cmbDUN.Text) & "'," & _
                nSmartID & "," & _
                Convert.ToInt32(chkReadReceipt.Checked) & "," & _
                txtAdjustStamp.Value & "," & _
                Convert.ToInt32(chkStatus.Checked) & "," & _
                DestOrderID

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
                "VALUES(" & sVals & ")"

                clsMarsData.WriteData(SQL)

                clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID = " & DestinationID & ", ReportID = 0 WHERE " & _
                "DestinationID = 99999 OR ReportID = 99999")

                clsMarsData.WriteData("UPDATE ReadReceiptsAttr SET DestinationID = " & DestinationID & _
                " WHERE DestinationID = 99999")

                clsMarsData.WriteData("UPDATE StampAttr SET ForeignID =" & DestinationID & " WHERE ForeignID = 99999")

                If chkHouseKeeping.Checked = True And cmbDestination.Text.ToLower = "disk" Then
                    sCols = "HouseID,DestinationID,AgeValue,AgeUnit"

                    sVals = clsMarsData.CreateDataID("housekeepingattr", "houseid") & "," & _
                    DestinationID & "," & _
                    txtHouseNum.Value & "," & _
                    "'" & cmbUnit.Text & "'"

                    SQL = "INSERT INTO HouseKeepingAttr (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                End If

            End If

            If chkPGP.Checked = True Then
                UcPGP._AddKey(DestinationID)
            End If

            Return DestinationID
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return 0
        End Try
    End Function

    Private Sub SetContextMenu()
        Me.txtFaxFrom.ContextMenu = Me.mnuInserter
        Me.txtFaxNumber.ContextMenu = Me.mnuInserter
        Me.txtFaxTo.ContextMenu = Me.mnuInserter
        Me.txtFaxComments.ContextMenu = Me.mnuInserter
        Me.cmbFaxDevice.ContextMenu = Me.mnuInserter

        Me.txtSMSNumber.ContextMenu = Me.mnuInserter
        Me.txtSMSMsg.ContextMenu = Me.mnuInserter

        Me.txtTableName.ContextMenu = Me.mnuInserter
        Me.UcDSN.cmbDSN.ContextMenu = Me.mnuInserter
        Me.UcDSN.txtUserID.ContextMenu = Me.mnuInserter

        Me.txtSenderName.ContextMenu = Me.mnuInserter
        Me.txtSenderAddress.ContextMenu = Me.mnuInserter

        If Me.DataDriven = True Then
            Me.cmbFormat.ContextMenu = Me.mnuInserter
        End If
    End Sub



    Private Sub frmDestinationSelect_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            SetContextMenu()

            Dim oPrint As New System.Drawing.Printing.PrinterSettings
            Dim strItem As String
            Dim strPrinters As String
            Dim I As Int32 = 0
            Dim ilimo As Boolean

            FormatForWinXP(Me)

            If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False

            If MailType = MarsGlobal.gMailType.MAPI Then
                cmbMailFormat.Text = "TEXT"
                cmbMailFormat.Enabled = False
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
                Me.pnMailServer.Visible = False
                Me.pnReadReciepts.Visible = True
            End If

            If MailType = gMailType.GROUPWISE Then
                cmbMailFormat.Text = "TEXT"
                cmbMailFormat.Enabled = False
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
                Me.pnMailServer.Visible = False
                Me.pnReadReciepts.Visible = False
                Me.grpSender.Enabled = False
            End If

            If MailType <> MarsGlobal.gMailType.SMTP Then
                cmbServer.Text = "Default"
                cmbServer.Enabled = False
            Else
                cmbServer.Enabled = True
                Me.pnMailServer.Visible = True
                Me.pnReadReciepts.Visible = False
            End If

            If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
                Me.grpSender.Enabled = True
            Else
                Me.grpSender.Enabled = False
            End If

            For Each strItem In oPrint.InstalledPrinters
                For Each o As ListViewItem In lsvPrinters.Items
                    If strItem.ToLower = o.Text.ToLower Then
                        ilimo = True
                        Exit For
                    Else
                        ilimo = False
                    End If
                Next

                If ilimo = False Then cmbPrinters.Items.Add(strItem)

            Next

            If Package = True Then
                If chkZip.Checked = False Then
                    tabDestination.TabPages(2).Enabled = False
                Else
                    tabDestination.TabPages(2).Enabled = True
                End If

                'tabDestination.TabPages(3).Enabled = False
                cmbFormat.Items.Add("Package")
                cmbFormat.Text = "Package"
                cmbFormat.Enabled = False
                chkEmbed.Enabled = False
                chkZip.Enabled = True

            End If
        Catch : End Try

        txtTo.ContextMenu = Me.mnuInserter
        txtCC.ContextMenu = Me.mnuInserter
        txtBCC.ContextMenu = Me.mnuInserter
        HelpProvider1.HelpNamespace = gHelpPath
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sFileDir As String

        fbg.ShowNewFolderButton = True
        fbg.Description = "Please select the destination directory"

        fbg.ShowDialog()

        sFileDir = _CreateUNC(fbg.SelectedPath)

        If sFileDir <> "" Then
            If sFileDir.Substring(sFileDir.Length - 1, 1) <> "\" Then
                txtDirectory.Text = sFileDir & "\"
            Else
                txtDirectory.Text = sFileDir
            End If

            Me.btnAddPath_Click(sender, e)
        End If
    End Sub

    Private Sub cmdAddPrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddPrinter.Click
        Try
            Dim I As Int32 = 0
            Dim lsv As ListViewItem

            If lsvPrinters.Items.Count >= 1 Then
                If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.p1_BulkPrinting) Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    Return
                End If
            End If

            If cmbPrinters.Text = "" Then Exit Sub

            For I = 0 To lsvPrinters.Items.Count - 1
                If cmbPrinters.Text = lsvPrinters.Items(I).Text Then
                    Exit Sub
                End If
            Next

            Dim oPrinter As New frmPrinterAttr
            Dim sReturn() As Object

            sReturn = oPrinter._AddPrinter(cmbPrinters.Text, xDestinationID)

            If Not sReturn Is Nothing Then
                lsv = lsvPrinters.Items.Add(sReturn(2))

                lsv.SubItems.Add(sReturn(1))

                lsv.Tag = sReturn(0)

                If sReturn(2) = cmbPrinters.Text Then cmbPrinters.Items.Remove(cmbPrinters.Text)
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdRemovePrinter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemovePrinter.Click
        Dim I As Int32
        Dim oLsv As ListViewItem
        Dim SQL As String

        If lsvPrinters.Items.Count = 0 Or lsvPrinters.SelectedItems.Count = 0 Then _
            Exit Sub

        For Each oLsv In lsvPrinters.SelectedItems
            If oLsv.Text.Contains("<[") = False Then cmbPrinters.Items.Add(oLsv.Text)
        Next

        For Each oLsv In lsvPrinters.SelectedItems
            SQL = "DELETE FROM PrinterAttr WHERE PrinterID =" & oLsv.Tag

            If clsMarsData.WriteData(SQL) = True Then
                oLsv.Remove()
            End If
        Next
    End Sub




    Private Sub cmbDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDestination.SelectedIndexChanged
        ep.SetError(sender, "")

        If cmbDestination.Text.Length = 0 Then
            tabDestination.Visible = False
        Else
            tabDestination.Visible = True
        End If

        Select Case cmbDestination.Text.ToLower
            Case "email"
                If MailType = MarsGlobal.gMailType.NONE Then
                    MessageBox.Show("Please set up your messaging options before creating an email destination", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    cmbDestination.Text = "Disk"
                    Return
                End If

                grpEmail.Visible = True
                grpFax.Visible = False
                grpFile.Visible = False
                grpFTP.Visible = False
                grpODBC.Visible = False
                grpPrinter.Visible = False
                grpSMS.Visible = False

                grpEmail.BringToFront()
                cmdTo.Focus()

                cmbFormat.Items.Remove("Printer Format")

                If cmbFormat.Text = "Printer Format" Then cmbFormat.Text = ""



                If Package = False Then
                    tabDestination.TabPages(1).Enabled = True
                    tabDestination.TabPages(2).Enabled = True
                    tabDestination.TabPages(3).Enabled = True
                    _SetFormats("")
                End If

                If chkEmbed.Checked = True Then
                    chkIncludeAttach.Visible = True
                End If

            Case "disk"
                grpFile.Visible = True
                grpEmail.Visible = False
                grpFax.Visible = False
                grpFTP.Visible = False
                grpODBC.Visible = False
                grpPrinter.Visible = False
                grpSMS.Visible = False

                grpFile.BringToFront()

                cmbFormat.Items.Remove("Printer Format")

                If cmbFormat.Text = "Printer Format" Then cmbFormat.Text = ""



                If Package = False Then
                    tabDestination.TabPages(1).Enabled = True
                    tabDestination.TabPages(2).Enabled = True
                    tabDestination.TabPages(3).Enabled = True
                    _SetFormats("")
                End If


                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False
            Case "printer"

                grpPrinter.Visible = True
                grpFile.Visible = False
                grpEmail.Visible = False
                grpFax.Visible = False
                grpFTP.Visible = False
                grpODBC.Visible = False
                grpSMS.Visible = False

                grpPrinter.BringToFront()

                cmbFormat.Items.Add("Printer Format")

                cmbFormat.Text = "Printer Format"

                tabDestination.TabPages(1).Enabled = False
                tabDestination.TabPages(2).Enabled = False
                tabDestination.TabPages(3).Enabled = False
                tabDestination.TabPages(4).Enabled = False

                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False

            Case "ftp"

                If Not IsFeatEnabled(gEdition.PLATINUM, featureCodes.f1_FTPDestination) Then
                    _NeedUpgrade(MarsGlobal.gEdition.PLATINUM)
                    cmbDestination.Text = "Email"
                    Return
                End If

                grpFTP.Visible = True
                grpPrinter.Visible = False
                grpFile.Visible = False
                grpEmail.Visible = False
                grpFax.Visible = False
                grpODBC.Visible = False
                grpSMS.Visible = False

                grpFTP.BringToFront()

                cmbFormat.Items.Remove("Printer Format")

                If cmbFormat.Text = "Printer Format" Then cmbFormat.Text = ""

                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False

                If Package = False Then
                    tabDestination.TabPages(1).Enabled = True
                    tabDestination.TabPages(2).Enabled = True
                    tabDestination.TabPages(3).Enabled = True
                    _SetFormats("")
                End If

                UcFTP.cmbFTPType.SelectedIndex = 0
            Case "fax"
                If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.x1_AutomatedFaxing) Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
                    cmbDestination.Text = "Disk"
                    Return
                End If

                grpFax.Visible = True
                grpPrinter.Visible = False
                grpFile.Visible = False
                grpEmail.Visible = False
                grpFTP.Visible = False
                grpODBC.Visible = False
                grpSMS.Visible = False
                grpFax.BringToFront()

                cmbFormat.Items.Remove("Printer Format")
                cmbFormat.Items.Add("Fax Format")
                cmbFormat.Text = "Fax Format"
                tabDestination.TabPages(1).Enabled = False
                tabDestination.TabPages(2).Enabled = False
                tabDestination.TabPages(3).Enabled = False
                tabDestination.TabPages(4).Enabled = False

                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False
            Case "sms"

                If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o1_SMS) Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                    cmbDestination.Text = "Disk"
                    Return
                End If

                grpSMS.Visible = True
                grpPrinter.Visible = False
                grpFile.Visible = False
                grpEmail.Visible = False
                grpFax.Visible = False
                grpFTP.Visible = False
                grpODBC.Visible = False

                grpSMS.BringToFront()

                tabDestination.TabPages(2).Enabled = False
                tabDestination.TabPages(3).Enabled = False
                tabDestination.TabPages(4).Enabled = False

                If Package = False Then
                    tabDestination.TabPages(1).Enabled = True
                    _SetFormats("SMS")
                End If

                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False

                chkZip.Enabled = False
                chkZip.Checked = False
            Case "odbc"

                If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o3_ODBC) Then
                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
                    cmbDestination.Text = "Disk"
                    Return
                End If

                cmbFormat.Text = "ODBC (*.odbc)"

                tabDestination.TabPages(0).Enabled = True
                tabDestination.TabPages(2).Enabled = False
                tabDestination.TabPages(3).Enabled = False
                tabDestination.TabPages(4).Enabled = False
                tabDestination.TabPages(1).Enabled = False
                tabDestination.SelectedTab = tabDestination.TabPages(0)

                grpODBC.Visible = True
                grpPrinter.Visible = False
                grpFile.Visible = False
                grpEmail.Visible = False
                grpFax.Visible = False
                grpFTP.Visible = False
                grpSMS.Visible = False

                grpODBC.BringToFront()
            Case "sharepoint"
                If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.o4_SharePointDestination) = False Then
                    _NeedUpgrade(gEdition.CORPORATE, "SharePoint Destination")
                    cmbDestination.Text = "Disk"
                    Return
                End If

                Me.grpSharePoint.Visible = True
                grpSharePoint.BringToFront()

                cmbFormat.Items.Remove("Printer Format")

                If cmbFormat.Text = "Printer Format" Then cmbFormat.Text = ""

                If Package = False Then
                    tabDestination.TabPages(1).Enabled = True
                    tabDestination.TabPages(2).Enabled = True
                    tabDestination.TabPages(3).Enabled = True
                    _SetFormats("")
                End If

                chkIncludeAttach.Checked = True
                chkIncludeAttach.Visible = False
        End Select

        If txtName.Text.Length = 0 Then txtName.Text = Me.cmbDestination.Text
    End Sub



    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If cmbDestination.Text = "" Then
            ep.SetError(cmbDestination, "Please select the schedule destination")
            cmbDestination.Focus()
            Exit Sub
        ElseIf txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please provide a name for this destination")
            txtName.Focus()
            Return
        ElseIf cmbFormat.Text.Length = 0 And chkIncludeAttach.Checked = True Then
            tabDestination.SelectedTab = tbOutput
            ep.SetError(cmbFormat, "Please select the output format")
            cmbFormat.Focus()
            Return
        ElseIf optCustomName.Checked = True And txtCustomName.Text.Length = 0 Then
            tabDestination.SelectedTab = tbNaming
            ep.SetError(txtCustomName, "Please enter the custom output name")
            txtCustomName.Focus()
            Return
        ElseIf chkCustomExt.Checked = True And txtCustomExt.Text.Length = 0 Then
            tabDestination.SelectedTab = tbNaming
            ep.SetError(txtCustomExt, "Please enter the custom extension")
            txtCustomExt.Focus()
            Return
        ElseIf chkAppendDateTime.Checked = True And cmbDateTime.Text.Length = 0 Then
            tabDestination.SelectedTab = tbNaming
            ep.SetError(cmbDateTime, "Please select the date/time format")
            cmbDateTime.Focus()
            Return
        End If

        If chkHouseKeeping.Checked = True Then
            If cmbUnit.Text.Length = 0 Then
                ep.SetError(cmbUnit, "Please specify the correct interval")
                cmbUnit.Focus()
                Return
            End If
        End If

        If chkZipSecurity.Checked = True Then
            If cmbEncryptionLevel.Text.Length = 0 Then
                ep.SetError(cmbEncryptionLevel, "Please select the encryption level")
                cmbEncryptionLevel.Focus()
                Return
            ElseIf txtZipCode.Text.Length = 0 Or txtConfirmCode.Text.Length = 0 Then
                ep.SetError(txtZipCode, "Please provide the password for encryption")
                txtZipCode.Focus()
                Return
            ElseIf txtZipCode.Text <> txtConfirmCode.Text Then
                ep.SetError(txtZipCode, "The confirm password and the password do not match")
                txtZipCode.Focus()
                Return
            End If
        End If

        If chkDUN.Checked = True And cmbDUN.Text.Length = 0 Then
            ep.SetError(cmbDUN, "Please select the connection from the drop-down list")
            cmbDUN.Focus()
            Return
        End If

        If chkPGP.Checked = True Then
            With UcPGP
                If .txtPGPUserID.Text.Length = 0 Then
                    .ep.SetError(.txtPGPUserID, "Please provide the Key User ID")
                    .txtPGPUserID.Focus()
                    Return
                ElseIf .txtKeyLocation.Text.Length = 0 Then
                    ep.SetError(.txtKeyLocation, "Please provide the path to the public keyring file")
                    .txtKeyLocation.Focus()
                    Return
                ElseIf .txtSecring.Text.Length = 0 Then
                    ep.SetError(.txtSecring, "Please provide the path to the private keyring file")
                    .txtSecring.Focus()
                    Return
                End If
            End With
        End If

        Select Case cmbDestination.Text.ToLower
            Case "email"
                If txtTo.Text = "" And (Dynamic = False Or IsStatic = True) Then
                    ep.SetError(txtTo, "Please provide the email recipient")
                    tabDestination.SelectedTab = tbDestination
                    txtTo.Focus()
                    Exit Sub
                    'Else
                    '    For Each s As String In txtAttach.Text.Split(";")
                    '        If (s.IndexOf("*") = -1 And s.Length > 0) Or (s.StartsWith("<") = False And s.Length > 0) Then
                    '            Try
                    '                If IO.File.Exists(s) = False Then
                    '                    ep.SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure attachment paths are valid")
                    '                    txtAttach.Focus()
                    '                    tabDestination.SelectedTab = tbDestination
                    '                    Return
                    '                End If
                    '            Catch
                    '                ep.SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure attachment paths are valid")
                    '                txtAttach.Focus()
                    '                tabDestination.SelectedTab = tbDestination
                    '                Return
                    '            End Try
                    '        End If
                    '    Next
                End If
            Case "disk"
                If lsvPaths.Items.Count = 0 And (Dynamic = False Or IsStatic = True) Then
                    ep.SetError(txtDirectory, "Please select the output directory for the schedule")
                    tabDestination.SelectedTab = tbDestination
                    txtDirectory.Focus()
                    Exit Sub
                End If
            Case "ftp"
                If lsvFTP.Items.Count = 0 Then
                    ep.SetError(UcFTP.txtFTPServer, "Please add an ftp destination")
                    UcFTP.txtFTPServer.Focus()
                    Return
                End If
            Case "printer"
                If lsvPrinters.Items.Count <= 0 And (Dynamic = False Or IsStatic = True) Then
                    ep.SetError(cmbPrinters, "Please select at least one printer for the schedule")
                    tabDestination.SelectedTab = tbDestination
                    cmbPrinters.Focus()
                    Exit Sub
                End If
            Case "fax"
                If txtFaxNumber.Text.Length = 0 And (Dynamic = False Or IsStatic = True) Then
                    ep.SetError(txtFaxNumber, "Please provide the fax number")
                    tabDestination.SelectedTab = tbDestination
                    txtFaxNumber.Focus()
                    Return
                ElseIf cmbFaxDevice.Text.Length = 0 Then
                    ep.SetError(cmbFaxDevice, "Please select the fax device")
                    tabDestination.SelectedTab = tbDestination
                    cmbFaxDevice.Focus()
                    Return
                End If
            Case "sms"
                If txtSMSNumber.Text.IndexOf("<[") = -1 Then
                    Dim test As String = txtSMSNumber.Text.Replace(" ", "")

                    Try
                        Int64.Parse(test)
                    Catch ex As Exception
                        ep.SetError(txtSMSNumber, "Please enter a valid cellphone number")
                        txtSMSNumber.Focus()
                        Return
                    End Try
                End If
            Case "odbc"
                With UcDSN
                    If .cmbDSN.Text.Length = 0 Then
                        .ep.SetError(.cmbDSN, "Please select the datasource name")
                        .cmbDSN.Focus()
                        Return
                    ElseIf txtTableName.Text.Length = 0 Then
                        ep.SetError(txtTableName, "Please provide a table name")
                        txtTableName.Focus()
                        Return
                    Else
                        Try
                            Dim oRs As New ADODB.Recordset
                            Dim oCon As New ADODB.Connection

                            oCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)

                            oRs.Open("SELECT TOP 1 * FROM " & txtTableName.Text)

                            oRs.Close()

                            ep.SetError(txtTableName, "A table with this name already exists in the selected datasource")
                            txtTableName.Focus()
                            Return
                        Catch : End Try
                    End If
                End With
        End Select
        UserCancel = False
        Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTo.GotFocus, txtCC.GotFocus, txtBCC.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtDirectory_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDirectory.TextChanged
        ep.SetError(sender, "")

        If txtDirectory.Text.Length = 0 Then
            Me.btnAddPath.Enabled = False
            Return
        End If

        Dim itemFound As Boolean = False

        For Each item As ListViewItem In lsvPaths.Items
            If item.Text = Me.txtDirectory.Text Then
                itemFound = True
                Exit For
            End If
        Next

        Me.btnAddPath.Enabled = Not (itemFound)

    End Sub

    Private Sub txtFTPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, "")
    End Sub

    Private Sub txtFTPUsername_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, "")
    End Sub

    Private Sub txtFTPPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ep.SetError(sender, "")
    End Sub



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdTo.MouseUp

        If e.Button <> MouseButtons.Left Then Return

        oField = txtTo
        sField = "to"
        mnuContacts.Show(cmdTo, New Point(e.X, e.Y))

    End Sub

    Private Sub cmdCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtCC
        sField = "cc"
        mnuContacts.Show(cmdCC, New Point(e.X, e.Y))
    End Sub



    Private Sub cmdBcc_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdBcc.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtBCC
        sField = "bcc"
        mnuContacts.Show(cmdBcc, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oUI As New clsMarsMessaging

        oUI.OutlookAddresses(oField)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click

        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact(sField)

        If sValues Is Nothing Then Return

        If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
        Then txtTo.Text &= ";"

        Try
            txtTo.Text &= sValues(0)
        Catch
        End Try

        If txtCC.Text.EndsWith(";") = False And txtCC.Text.Length > 0 _
        Then txtCC.Text &= ";"

        Try
            txtCC.Text &= sValues(1)
        Catch
        End Try

        If txtBCC.Text.EndsWith(";") = False And txtBCC.Text.Length > 0 _
        Then txtBCC.Text &= ";"

        Try
            txtBCC.Text &= sValues(2)
        Catch
        End Try

    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.m2_MailingLists) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        ofg.Title = "Please select the file to read email addresses from"

        ofg.ShowDialog()

        If ofg.FileName.Length > 0 Then
            If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
            oField.Text &= ";"

            oField.Text &= "<[f]" & ofg.FileName & ">;"
        End If
    End Sub

    Private Sub cmdAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAttach.Click
        ofg.Multiselect = True
        ofg.Title = "Select files to attach"
        ofg.ShowDialog()

        If ofg.FileNames.GetLength(0) = 0 Then Return

        If txtAttach.Text.EndsWith(";") = False And txtAttach.Text.Length > 0 Then _
        txtAttach.Text &= ";"

        For Each s As String In ofg.FileNames
            If s.Length > 0 Then
                s = _CreateUNC(s)
                txtAttach.SelectedText &= s & ";"
            End If
        Next
    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click

        If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(Me.eventID)

        If sTemp.Length > 0 Then
            If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"

            oField.Text &= sTemp & ";"
        End If
    End Sub


    Private Sub txtSubject_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSubject.GotFocus
        oField = txtSubject
    End Sub

    Private Sub txtMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMsg.GotFocus
        oField = txtMsg
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        oInsert.m_ParameterList = Me.m_ParamterList
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        If Dynamic = False Or IsStatic = True Then
            Dim oItem As New frmDataItems

            oItem.m_ParameterList = Me.m_ParamterList
            oField.SelectedText = oItem._GetDataItem(Me.eventID)
        Else
            Dim oIntruder As New frmInserter(0)

            oIntruder.GetDatabaseField(gTables, gsCon, Me)
        End If
    End Sub

    Private Sub txtDirectory_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDirectory.GotFocus
        oField = txtDirectory
    End Sub

    Private Sub txtDirectory_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtDirectory.MouseUp
        oField = txtDirectory
    End Sub

    Private Sub cmbFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFormat.SelectedIndexChanged, cmdProps.Click
        ep.SetError(sender, String.Empty)

        If optNaming.Checked = True And sTitle.Length > 0 And cmbFormat.Text.IndexOf("*") > -1 Then
            txtDefault.Text = sTitle & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
        End If

        If Me.Visible = True Then
            Dim opt As New frmRptOptions

            opt.m_eventBased = Me.m_eventBased
            opt.eventID = Me.eventID

            Dim format As String = ""

            If cmbFormat.Text.Contains("(") Then
                format = cmbFormat.Text.Split("(")(0).Trim()
            Else
                format = cmbFormat.Text
            End If


            Select Case cmbFormat.Text
                Case "Acrobat Format (*.pdf)"
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic, )
                Case "MS Excel 97-2000 (*.xls)"
                    If Me.m_IsQuery = False Then
                        opt.ReportOptions(format, "Single", , xDestinationID, Dynamic, sTitle)
                    Else
                        opt.taskOptions(format, xDestinationID)
                    End If
                Case "MS Excel 7 (*.xls)", "MS Excel 8 (*.xls)"
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic, sTitle)
                Case "MS Word (*.doc)"
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic)
                Case "CSV (*.csv)"
                    If Me.m_IsQuery = False Then
                        opt.ReportOptions(format, "Single", , xDestinationID, Dynamic)
                    Else
                        opt.taskOptions(format, xDestinationID)
                    End If
                Case "HTML (*.htm)"
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic)
                Case "TIFF (*.tif)"
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic)
                Case "XML (*.xml)"
                    If Me.m_IsQuery = True Then opt.taskOptions(format, xDestinationID)
                Case "Custom (*.*)"
                    Me.chkCustomExt.Checked = True
                    opt.ReportOptions(format, "Single", , xDestinationID, Dynamic)
            End Select
        End If
    End Sub

    Private Sub chkEmbed_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmbed.CheckedChanged
        txtMsg.Enabled = Not chkEmbed.Checked
        chkIncludeAttach.Visible = chkEmbed.Checked

        If chkEmbed.Checked = False Then
            chkIncludeAttach.Checked = True
            chkIncludeAttach.Visible = False
        Else
            chkIncludeAttach.Visible = True
            chkIncludeAttach.Checked = False
        End If
    End Sub

    Private Sub optCustomName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustomName.CheckedChanged
        If optCustomName.Checked = True Then
            txtCustomName.Enabled = True
            txtDefault.Text = String.Empty
            If Me.Dynamic = True Then Me.chkUseKeyPar.Enabled = True
        Else
            txtCustomName.Enabled = False
            txtCustomName.Text = ""
            Me.chkUseKeyPar.Enabled = False
            Me.chkUseKeyPar.Checked = False
        End If

        ep.SetError(txtCustomName, String.Empty)
    End Sub

    Private Sub chkCustomExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomExt.CheckedChanged
        If chkCustomExt.Checked = True Then
            txtCustomExt.Enabled = True
        Else
            txtCustomExt.Enabled = False
            txtCustomExt.Text = ""
        End If

        ep.SetError(txtCustomExt, String.Empty)
    End Sub

    Private Sub chkAppendDateTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAppendDateTime.CheckedChanged
        If chkAppendDateTime.Checked = True Then
            cmbDateTime.Enabled = True

            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

            txtAdjustStamp.Enabled = True
        Else
            cmbDateTime.Enabled = False
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Enabled = False
            txtAdjustStamp.Value = 0
        End If

        ep.SetError(cmbDateTime, String.Empty)
    End Sub

    Private Sub txtCustomName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomName.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtCustomExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomExt.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        ep.SetError(sender, String.Empty)

        'If txtDefault.Text.Length > 0 Then
        '    Dim sTemp As String
        '    Dim sExt As String

        '    sTemp = txtDefault.Text.Substring(0, txtDefault.Text.Length - 4)

        '    sTemp &= Date.Now.ToString(cmbDateTime.Text)

        '    sExt = txtDefault.Text.Split(".")(txtDefault.Text.Split(".").GetUpperBound(0))




        '    txtDefault.Text = sTemp & "." & sExt
        'End If
    End Sub

    Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click

    End Sub

    Private Sub cmdInsert_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert.MouseUp
        Try
            If oField Is Nothing Then Return
            mnuSubInsert.Show(cmdInsert, New Point(e.X, e.Y))
        Catch
        End Try
    End Sub

    Private Sub cmdInsert2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert2.MouseUp
        oField = txtDirectory
        mnuSubInsert.Show(cmdInsert2, New Point(e.X, e.Y))
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub txtAttach_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttach.GotFocus
        oField = txtAttach
    End Sub

    Private Sub txtCustomName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomName.GotFocus
        oField = txtCustomName
    End Sub

    Private Sub txtCustomExt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomExt.GotFocus
        oField = txtCustomExt
    End Sub

    Private Sub chkZip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZip.CheckedChanged


        grpZip.Visible = chkZip.Checked

        If Package = True Then
            tabDestination.TabPages(2).Enabled = chkZip.Checked
        End If
    End Sub

    Private Sub chkMerge_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSplit.CheckedChanged
        If chkSplit.Checked = True Then
            chkEmbed.Checked = False
        End If
    End Sub

    Private Sub mnuDefMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub mnuSignature_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSignature.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub



    Private Sub mnuDefSubject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefSubject.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub optNaming_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNaming.CheckedChanged
        Try
            If optNaming.Checked = True Then
                If optNaming.Checked = True And sTitle.Length > 0 Then
                    txtDefault.Text = sTitle & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
                End If
            End If
        Catch : End Try
    End Sub

    Private Sub chkDefer_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDefer.CheckedChanged
        If chkDefer.Checked = True Then
            If Not IsFeatEnabled(gEdition.PLATINUM, featureCodes.fr2_DeferredDelivery) Then
                _NeedUpgrade(MarsGlobal.gEdition.PLATINUM)
                chkDefer.Checked = False
                Return
            End If
        End If

        txtDefer.Enabled = chkDefer.Checked
    End Sub

    Private Sub lsvPrinters_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvPrinters.DoubleClick
        If lsvPrinters.SelectedItems.Count = 0 Then Return

        Dim oPrint As New frmPrinterAttr

        oPrint._EditPrinter(lsvPrinters.SelectedItems(0).Tag)
    End Sub

    Private Sub chkHouseKeeping_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHouseKeeping.CheckedChanged
        If sqlrd.MarsGlobal.gnEdition < MarsGlobal.gEdition.PLATINUM And chkHouseKeeping.Checked = True Then
            _NeedUpgrade(MarsGlobal.gEdition.PLATINUM)
            chkHouseKeeping.Checked = False
            Return
        End If

        grpHouseKeeping.Enabled = chkHouseKeeping.Checked
    End Sub

    Private Sub cmbPrinters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPrinters.SelectedIndexChanged

    End Sub

    'Private Sub Chkburst_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkburst.CheckedChanged
    '    If chkburst.Checked = True Then
    '        GroupBox5.Enabled = False

    '        Dim oBurst As New frmGroupBursting

    '        oBurst.oRpt = oRpt
    '        oBurst.ConfigureBursting()
    '    Else
    '        GroupBox5.Enabled = True
    '    End If
    'End Sub

    Private Sub cmbServer_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbServer.DropDown
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        SQL = "SELECT * FROM SMTPServers"

        cmbServer.Items.Clear()
        cmbServer.Items.Add("Default")

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            cmbServer.Items.Add(oRs("smtpname").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()


    End Sub


    Private Sub cmbFaxDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFaxDevice.SelectedIndexChanged

    End Sub

    Private Sub cmbFaxDevice_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFaxDevice.DropDown

        If cmbFaxDevice.Items.Count > 0 Then Return

        Try
            Dim modems As DataTech.FaxManJr.Modems = New DataTech.FaxManJr.Modems
            Dim sText As String

            clsMarsUI.BusyProgress(90, "Enumerating modems...")

            modems.AutoDetect()

            clsMarsUI.BusyProgress(100, "Enumerating modems...", True)

            For Each modem As DataTech.FaxManJr.Modem In modems
                sText = ""

                Select Case modem.FaxClass
                    Case DataTech.FaxManJr.FaxClass.Class1
                        sText = "Class1"
                    Case DataTech.FaxManJr.FaxClass.Class2
                        sText = "Class2"
                    Case DataTech.FaxManJr.FaxClass.Class20
                        sText = "Class20"
                    Case DataTech.FaxManJr.FaxClass.Class21
                        sText = "Class21"
                End Select

                sText &= " Device on Port:" & modem.Port

                Me.cmbFaxDevice.Items.Add(sText)
            Next

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub mnuAttachment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAttachment.Click, MenuItem12.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefAttach", "")
    End Sub


    Private Sub cmdFaxTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFaxTo.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "Fax"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtFaxNumber.Text.EndsWith(";") = False And txtFaxNumber.Text.Length > 0 _
        Then txtFaxNumber.Text &= ";"

        Try
            txtFaxNumber.Text &= sValues(0)
        Catch
        End Try

    End Sub

    Private Sub txtFaxTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFaxTo.GotFocus, txtFaxFrom.GotFocus, txtFaxComments.GotFocus, txtFaxNumber.GotFocus

        oField = CType(sender, TextBox)
    End Sub

    Private Sub txtFaxTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFaxTo.TextChanged
        Dim I As Integer = 0

        Try
            For Each s As String In txtFaxTo.Text.Split(";")
                If s.Length > 0 Then I += 1
            Next

            If I > 1 Then
                If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.x1_AutomatedFaxing) Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                    txtFaxTo.Text = txtFaxTo.Text.Split(";")(0)
                End If
            End If
        Catch : End Try
    End Sub



    Private Sub cmbMailFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMailFormat.SelectedIndexChanged
        If Me.chkEmbed.Checked And Me.chkIncludeAttach.Checked = False Then
            Me.cmbFormat.Text = Me.cmbMailFormat.Text & " Embeded"
        End If
    End Sub

    Private Sub chkZipSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZipSecurity.CheckedChanged
        'If chkZipSecurity.Checked = True Then
        '    If gnEdition < MarsGlobal.gEdition.ENTERPRISEPROPLUS Then
        '        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
        '        chkZipSecurity.Checked = False
        '        Return
        '    End If
        'End If

        Panel1.Enabled = chkZipSecurity.Checked
    End Sub

    Private Sub chkIncludeAttach_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIncludeAttach.CheckedChanged
        GroupBox2.Enabled = chkIncludeAttach.Checked

        If chkIncludeAttach.Checked = False And Me.Visible = True Then
            cmbFormat.Text = cmbMailFormat.Text & " Embeded"
            tabDestination.TabPages(4).Visible = False
        ElseIf chkIncludeAttach.Checked = True And Me.Visible = True Then
            cmbFormat.Text = String.Empty
            tabDestination.TabPages(4).Visible = True
        End If
    End Sub

    Private Sub chkPGP_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPGP.CheckedChanged
        'If gnEdition < MarsGlobal.gEdition.ENTERPRISEPROPLUS And chkPGP.Checked Then
        '    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
        '    chkPGP.Checked = False
        '    Return
        'End If

        UcPGP.Enabled = chkPGP.Checked
    End Sub

    Private Sub cmbDUN_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDUN.DropDown
        Try
            Dim connectionName As DialUpComponent.DialUpConnectionName
            Dim session As DialUpComponent.DialUpSession = New DialUpComponent.DialUpSession

            cmbDUN.Items.Clear()

            For Each connectionName In session.Connections
                cmbDUN.Items.Add(connectionName.Name)
            Next
        Catch : End Try

    End Sub

    Private Sub chkDUN_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDUN.CheckedChanged
        If Not (IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.o2_FileTransfer)) And chkDUN.Checked = True Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkDUN.Checked = False
        End If

        GroupBox7.Enabled = chkDUN.Checked
    End Sub

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If cmbDUN.Text.Length = 0 Then Return

        If cmdConnect.Text = "Connect" Then
            cmdConnect.Enabled = False
            If oNet._DialConnection(cmbDUN.Text) = True Then
                MessageBox.Show("Connected successfully")
                cmdConnect.Text = "Disconnect"
                cmdConnect.Enabled = True
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                cmdConnect.Enabled = True
            End If

        Else
            oNet._Disconnect()
            cmdConnect.Text = "Connect"
        End If
    End Sub

    Private Sub txtSMSMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSMsg.GotFocus
        oField = txtSMSMsg
    End Sub


    Private Sub _SetFormats(ByVal sType As String)
        Dim s() As String

        If sType = "SMS" Then
            s = New String() {"CSV (*.csv)", "Text (*.txt)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf IsSmart = True Then
            s = New String() {"MS Excel  (*.xls)", "XML (*.xml)", "Acrobat Format (*.pdf)", "HTML (*.html)", "JPEG (*.jpg)", "TIFF (*.tif)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        ElseIf Me.m_IsQuery = True Then
            s = New String() {"MS Excel 97-2000 (*.xls)", "XML (*.xml)", "CSV (*.csv)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        Else
            s = New String() {"Acrobat Format (*.pdf)", "CSV (*.csv)", _
            "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
            "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
            "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", _
            "MS Excel 97-2000 (*.xls)", _
            "Web Archive (*.mhtml)", _
            "TIFF (*.tif)", "XML (*.xml)", "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Text (*.txt)", "Custom (*.*)"}

            cmbFormat.Items.Clear()
            cmbFormat.Items.AddRange(s)
            cmbFormat.Text = ""
        End If
    End Sub
    Private Sub txtSMSMsg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMSMsg.TextChanged
        Try
            Label23.Text = "Characters left: " & 160 - (txtSMSMsg.Text.Length)
        Catch : End Try
    End Sub

    Private Sub cmdSMSTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSMSTo.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "SMS"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtSMSNumber.Text.EndsWith(";") = False And txtSMSNumber.Text.Length > 0 _
        Then txtSMSNumber.Text &= ";"

        Try
            txtSMSNumber.Text &= sValues(0)
        Catch : End Try
    End Sub

    Private Sub txtDefault_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDefault.GotFocus
        oField = txtDefault
    End Sub

    Private Sub btnODBC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnODBC.Click
        If UcDSN.cmbDSN.Text.Contains("<[") Then
            txtTableName.Enabled = True
        ElseIf UcDSN._Validate = False Then
            ep.SetError(UcDSN.cmbDSN, "Invalid database details. Please check and try again")
            UcDSN.cmbDSN.Focus()
            txtTableName.Text = ""
            txtTableName.Enabled = False
        Else
            txtTableName.Enabled = True
        End If
    End Sub

    Private Sub chkReadReceipt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReadReceipt.CheckedChanged

        If chkReadReceipt.Checked Then
            If IsFeatEnabled(gEdition.CORPORATE, featureCodes.m3_ReadReceipts) = False Then
                _NeedUpgrade(gEdition.CORPORATE)
                Me.chkReadReceipt.Checked = False
                Return
            End If

            If formLoaded = True Then
                Dim Read As frmReadReciepts = New frmReadReciepts
                Read.AddReceipt(xDestinationID)
            End If
        End If

        btnReadReceipts.Enabled = chkReadReceipt.Checked

    End Sub

    Private Sub btnReadReceipts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReadReceipts.Click
        Dim Read As frmReadReciepts = New frmReadReciepts
        Read.AddReceipt(xDestinationID)
    End Sub

    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        oInsert.GetConstants(Me)
    End Sub


    Private Sub btnAddPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPath.Click

        Dim itemFound As Boolean = False

        If lsvPaths.Items.Count >= 1 Then
            If Not IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa9_MultipleDestinations) Then
                _NeedUpgrade(gEdition.ENTERPRISEPRO)
                Return
            End If
        End If

        For Each item As ListViewItem In lsvPaths.Items
            If item.Text = Me.txtDirectory.Text Then
                itemFound = True
                Exit For
            End If
        Next

        If itemFound = False Then
            lsvPaths.Items.Add(txtDirectory.Text)
            txtDirectory.Text = String.Empty
        End If
    End Sub

    Private Sub btnRemovePath_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemovePath.Click
        If lsvPaths.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In lsvPaths.SelectedItems
            item.Remove()
            Me.txtDirectory.Text = item.Text
        Next
    End Sub

    Private Sub btnAddFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFTP.Click
        Dim ftpItem As ListViewItem
        Dim match As Boolean = False

        If UcFTP.validateFtpInfo(Me.Dynamic) = False Then
            Return
        End If

        Dim FtpServer As String = UcFTP.txtFTPServer.Text & ":" & UcFTP.txtPort.Value

        ftpItem = New ListViewItem(FtpServer)

        With ftpItem.SubItems
            .Add(UcFTP.txtUserName.Text)
            .Add(_EncryptDBValue(UcFTP.txtPassword.Text))
            .Add(UcFTP.txtDirectory.Text)
            .Add(UcFTP.cmbFTPType.Text)
            .Add(Convert.ToInt32(UcFTP.chkPassive.Checked))
            .Add(UcFTP.m_ftpOptions)
        End With

        For Each item As ListViewItem In lsvFTP.Items
            If item.Equals(ftpItem) Then
                match = True
                Exit For
            End If
        Next

        If match = False Then
            lsvFTP.Items.Add(ftpItem)

            UcFTP.ResetAll()

            For Each it As ListViewItem In lsvFTP.Items
                If it.SubItems(1).Text = "" Then it.Remove()
            Next
        End If

        For Each it As ListViewItem In lsvFTP.Items
            If it.SubItems(1).Text = "" Then it.Remove()
        Next
    End Sub

    Private Sub btnRemoveFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveFTP.Click
        For Each item As ListViewItem In lsvFTP.SelectedItems

            If item.Text.Contains(":") Then
                UcFTP.txtFTPServer.Text = item.Text.Split(":")(0)
                UcFTP.txtPort.Value = item.Text.Split(":")(1)
            Else
                UcFTP.txtFTPServer.Text = item.Text
            End If

            UcFTP.txtUserName.Text = item.SubItems(1).Text
            UcFTP.txtPassword.Text = _DecryptDBValue(item.SubItems(2).Text)
            UcFTP.txtDirectory.Text = item.SubItems(3).Text
            UcFTP.cmbFTPType.Text = item.SubItems(4).Text

            Try
                UcFTP.chkPassive.Checked = Convert.ToBoolean(Convert.ToInt32(item.SubItems(5).Text))
            Catch : End Try

            Try
                UcFTP.m_ftpOptions = item.SubItems(6).Text
            Catch : End Try

            item.Remove()

            For Each it As ListViewItem In lsvFTP.Items
                If it.SubItems(1).Text = "" Then it.Remove()
            Next
        Next
    End Sub


    Private Sub chkUseKeyPar_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUseKeyPar.CheckedChanged
        If Me.chkUseKeyPar.Checked = True Then
            txtCustomName.Text = "<[m]Key Parameter Value>"
        Else
            Me.txtCustomName.Text = ""
        End If
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSenderName.GotFocus, txtSenderAddress.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub


    Private Sub txtSenderName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSenderName.TextChanged, txtSenderAddress.TextChanged
        Dim txt As TextBox = CType(sender, TextBox)

        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False And txt.Text <> "" Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
            txt.Text = ""
            Return
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa9_MultipleDestinations) = False Then
            If formLoaded = True Then _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, "Multiple Destinations")

            Return
        End If

        Dim sh As SharePointer.frmSharePointBrowser = New SharePointer.frmSharePointBrowser
        Dim spVal As Boolean

        spVal = sh.GetSharePointLib(spServer, spUsername, spPassword, spLibrary)

        If spVal = True Then
            Dim it As ListViewItem = New ListViewItem
            it.Text = spServer
            it.SubItems.Add(spLibrary)
            it.SubItems.Add(spUsername)
            it.SubItems.Add(_EncryptDBValue(spPassword))

            Me.lsvSharePointLibs.Items.Add(it)
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Dim it As ListViewItem = Me.lsvSharePointLibs.SelectedItems(0)

        If it IsNot Nothing Then
            Dim sh As SharePointer.frmSharePointBrowser = New SharePointer.frmSharePointBrowser
            Me.spServer = it.Text
            Me.spLibrary = it.SubItems(1).Text
            Me.spUsername = it.SubItems(2).Text
            Me.spPassword = _DecryptDBValue(it.SubItems(3).Text)

            Dim ok As Boolean = sh.GetSharePointLib(spServer, spUsername, spPassword, spLibrary)

            If ok = True Then
                it.Text = spServer
                it.SubItems(1).Text = spLibrary
                it.SubItems(2).Text = spUsername
                it.SubItems(3).Text = _EncryptDBValue(spPassword)
            End If
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        For Each it As ListViewItem In Me.lsvSharePointLibs.SelectedItems
            it.Remove()
        Next
    End Sub
End Class
