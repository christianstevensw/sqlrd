Imports Xceed.Ftp
Imports Xceed.FileSystem
Imports System.Xml

Public Class ucFTPBrowser2
    Public oFTP As Object 'Chilkat.Ftp2
    Public sPath As String
    Public sFTPItem As String
    Public sFTPFolder As String
    Dim oUI As New clsMarsUI
    Public m_eventID As Integer = 99999

    Public ReadOnly Property m_selectedItem() As String()
        Get
            Dim values(1) As String

            values(0) = Me.lsvFTP.SelectedItems(0).Text
            values(1) = lblPath.Text

            Return values
        End Get
    End Property

    Public Overloads Function GetFtpFolderOrFile(ByVal ftp As Rebex.Net.Sftp)
        oFTP = ftp

        Me.GetDirContent()
    End Function



    Public Overloads Function GetFtpFolderOrFile(ByVal ftp As Chilkat.Ftp2)
        oFTP = ftp

        Me.GetDirContent()
    End Function
    Public Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If cmdConnect.Text = "Connect..." Then

            'If txtServerAddress.Text.Length = 0 Then
            '    ep.SetError(txtServerAddress, "Please enter the FTP Server address")
            '    txtServerAddress.Focus()
            '    Return
            'ElseIf txtUserName.Text.Length = 0 Then
            '    ep.SetError(txtUserName, "Please provide the username")
            '    txtUserName.Focus()
            '    Return
            'ElseIf txtPassword.Text.Length = 0 Then
            '    ep.SetError(txtPassword, "Please enter the password")
            '    txtPassword.Focus()
            '    Return
            'End If



            'If Connect() = True Then
            '    lblPath.Text = sPath
            '    cmdConnect.Text = "Disconnect..."
            'End If

        Else
            Disconnect()
            lsvFTP.Items.Clear()

            sPath = ""
            lblPath.Text = ""
            cmdConnect.Text = "Connect..."
        End If

    End Sub

    'Public Function Connect() As Boolean
    '    Try
    '        Dim oUI As New clsMarsUI
    '        Dim nPort As Int32 = 0
    '        If txtServerAddress.Text.Contains("<") = True Or _
    '        txtUserName.Text.Contains("<") = True Or _
    '        txtPassword.Text.Contains("<") = True Then
    '            Return False
    '        End If

    '        If oFTP.Connected = True Then
    '            oFTP.Disconnect()
    '        End If

    '        oUI.BusyProgress(25, "Connecting to FTP Server...")

    '        Try
    '            nPort = Int32.Parse(txtPort.Text)
    '        Catch ex As Exception
    '            nPort = 21
    '        End Try

    '        'FTP
    '        'FTP - SSL 3.1 (TLS)
    '        'FTP - SSL 3.0
    '        oFTP.PassiveTransfer = Me.chkPassive.Checked

    '        Select Case cmbFTPType.Text
    '            Case "FTP"
    '                oFTP.Connect(txtServerAddress.Text, nPort)
    '            Case "FTP - SSL 3.1 (TLS)"
    '                oFTP.Connect(txtServerAddress.Text, AuthenticationMethod.Tls, VerificationFlags.None, Nothing)
    '            Case "FTP - SSL 3.0"
    '                oFTP.Connect(txtServerAddress.Text, AuthenticationMethod.Ssl, VerificationFlags.None, Nothing)
    '        End Select

    '        oUI.BusyProgress(50, "Connected, logging in...")

    '        If oFTP.Connected = True Then
    '            oFTP.Login(txtUserName.Text, txtPassword.Text)
    '        End If

    '        oUI.BusyProgress(75, "Retrieving folder contents...")

    '        GetDirContent()

    '        oUI.BusyProgress(90, "Caching folder listing...")

    '        oUI.BusyProgress(100, , True)

    '        cmdConnect.Text = "Disconnect..."

    '        Return True
    '    Catch ex As Exception
    '        _ErrorHandle("Could not connect to the specified FTP Server.", _
    '        Err.Number, "ucFTPBrowser.Connect", _GetLineNumber(ex.StackTrace), _
    '        "Please check the provided port number and provided credentials")
    '        oUI.BusyProgress(, , True)
    '        Try
    '            oFTP.Disconnect()
    '        Catch
    '        End Try
    '        Return False
    '    End Try
    'End Function


    Public Sub Disconnect()
        Try

            oFTP.Disconnect()

            cmdConnect.Text = "Connect..."
        Catch : End Try
    End Sub

    Public Sub GetDirContent()
10:     Try
20:         If TypeOf oFTP Is Chilkat.Ftp2 Then
                Dim doc As XmlDocument

30:             doc = New XmlDocument

40:             doc.LoadXml(oFTP.GetXmlDirListing("*.*"))

                Dim oItems As FtpItemInfoList
                Dim oItem As FtpItemInfo

50:             lsvFTP.Items.Clear()

60:             lsvFTP.Items.Add("...", 0)

70:             lsvFTP.Items(0).Tag = "Up"

                Dim oNodes As XmlNodeList
                Dim oNode As XmlNode

80:             oNodes = doc.GetElementsByTagName("dir")

90:             For Each oNode In oNodes
100:                Dim oListItem As ListViewItem = New ListViewItem

110:                With oListItem
120:                    .Text = oNode.InnerText
130:                    .Tag = "Folder"
140:                    .ImageIndex = 1

150:                    With .SubItems
160:                        .Add("")
170:                        .Add("")
                        End With
                    End With

180:                lsvFTP.Items.Add(oListItem)
190:            Next

200:            oNodes = doc.GetElementsByTagName("file")

210:            For Each oNode In oNodes
                    Dim name As String
                    Dim size As String

220:                name = oNode.ChildNodes(0).InnerText
230:                size = oNode.ChildNodes(1).InnerText

                    Try
                        Dim nSize As Double = size

                        nSize = Math.Round(nSize / 1000, 2)

                        size = nSize

                        size &= " KB"
                    Catch : End Try

                    Dim fmonth, fdate, fyear, fhour, fminute As Integer

240:                fmonth = oNode.ChildNodes(2).Attributes("m").Value
250:                fdate = oNode.ChildNodes(2).Attributes("d").Value
260:                fyear = oNode.ChildNodes(2).Attributes("y").Value
270:                fhour = oNode.ChildNodes(2).Attributes("hh").Value
280:                fminute = oNode.ChildNodes(2).Attributes("mm").Value

290:                Dim fulDate As Date = New Date(fyear, fmonth, fdate, fhour, fminute, 0)
                    Dim sDate As String = ConDateTime(fulDate)

300:                Dim oListItem As ListViewItem = New ListViewItem

310:                With oListItem
320:                    .Text = name
330:                    .Tag = "File"
340:                    .ImageIndex = 3

350:                    With .SubItems
360:                        .Add(sDate)
370:                        .Add(size)
                        End With
                    End With

380:                lsvFTP.Items.Add(oListItem)
390:            Next

400:            lblPath.Text = oFTP.GetCurrentRemoteDir()
410:        Else
420:            lsvFTP.Items.Clear()

430:            lsvFTP.Items.Add("...", 0)

440:            lsvFTP.Items(0).Tag = "Up"

                Dim tmp As Rebex.Net.Sftp = oFTP
                Dim list As Rebex.Net.SftpItemCollection = tmp.GetList()

                'first get all the folders and sort them alphabetically
450:            Dim folders As ArrayList = New ArrayList

460:            For Each item As Rebex.Net.SftpItem In list
470:                If item.IsDirectory = True Then
480:                    folders.Add(item.Name)
                    End If
490:            Next

500:            folders.Sort()

510:            For Each s As String In folders
520:                Dim lsvItem As ListViewItem = New ListViewItem

530:                With lsvItem
540:                    .Text = s
550:                    .Tag = "Folder"
560:                    .ImageIndex = 1
570:                    .SubItems.Add("")
580:                    .SubItems.Add("")
                    End With

590:                lsvFTP.Items.Add(lsvItem)
600:            Next

                'and then get the files
610:            Dim files As ArrayList = New ArrayList

620:            For Each item As Rebex.Net.SftpItem In list
630:                If item.IsFile = True Then
640:                    files.Add(item.Name & "|" & item.Modified & "|" & item.Size)
                    End If
650:            Next

660:            files.Sort()

670:            For Each s As String In files
680:                Dim lsvItem As ListViewItem = New ListViewItem

690:                With lsvItem
700:                    .Text = s.Split("|")(0)
710:                    .Tag = "File"
720:                    .ImageIndex = 3
730:                    .SubItems.Add(ConDateTime(s.Split("|")(1)))

                        Dim size As String = s.Split("|")(2)

                        Try
                            Dim nz As Double = size

                            nz = Math.Round(nz / 1000, 2)

                            size = nz

                            size &= " KB"
                        Catch : End Try

740:                    .SubItems.Add(size)
                    End With

750:                lsvFTP.Items.Add(lsvItem)
760:            Next

770:            lblPath.Text = tmp.GetCurrentDirectory
            End If
780:    Catch ex As Exception
790:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    'Public Sub GetDirContent()

    '    Dim nImage As Int32 = 0
    '    Dim sTag As String

    '    lsvFTP.Items.Clear()


    '    Dim oItems As FtpItemInfoList
    '    Dim oItem As FtpItemInfo

    '    oItems = oFTP.GetFolderContents()

    '    lsvFTP.Items.Add("...", 0)

    '    lsvFTP.Items(0).Tag = "Up"

    '    For Each oItem In oItems
    '        If oItem.Type = FtpItemType.Folder Or oItem.Type = FtpItemType.Link Then
    '            nImage = 1
    '            sTag = "Folder"
    '        Else
    '            nImage = 3
    '            sTag = "File"
    '        End If

    '        Dim oListItem As ListViewItem = New ListViewItem

    '        With oListItem
    '            .Text = oItem.Name
    '            .Tag = sTag
    '            .ImageIndex = nImage

    '            With .SubItems
    '                .Add(oItem.DateTime)
    '                .Add(oItem.Size)
    '            End With
    '        End With

    '        lsvFTP.Items.Add(oListItem)
    '    Next

    'End Sub

    Private Sub txtServerAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtServerAddress.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtUserName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub lsvFTP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvFTP.SelectedIndexChanged
        If lsvFTP.SelectedItems.Count = 0 Then Return

        Dim lsvItem As ListViewItem = lsvFTP.SelectedItems(0)

        If lsvItem.Tag = "File" Then
            sFTPItem = lsvItem.Text
            'sFTPFolder = ""
            lblPath.Text = lblPath.Text
        ElseIf lsvItem.Tag = "Folder" Then
            sFTPFolder = lsvItem.Text
            'sFTPItem = ""
        Else
            sFTPItem = ""
            sFTPFolder = ""
        End If
    End Sub

    Private Sub lsvFTP_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvFTP.DoubleClick
        If lsvFTP.SelectedItems.Count = 0 Then Return

        If TypeOf oFTP Is Chilkat.Ftp2 Then
            If oFTP.IsConnected = False Then
                MessageBox.Show("CRD is not connected to the FTP Server", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Warning)
                Return
            End If

            Dim oListItem As ListViewItem = lsvFTP.SelectedItems(0)

            If oListItem.Tag = "Folder" Then
                oFTP.ChangeRemoteDir(oListItem.Text)

                'sPath &= oListItem.Text & "/"

                'lblPath.Text = sPath

                GetDirContent()
            ElseIf oListItem.Tag = "Up" Then
                Try

                    Dim sFolders As String() = oFTP.GetCurrentRemoteDir.Split("/")

                    sPath = ""

                    For I As Int32 = 0 To sFolders.GetUpperBound(0) - 1
                        sPath &= sFolders(I) & "/"
                    Next

                    lblPath.Text = sPath

                    oFTP.ChangeRemoteDir(sPath)

                    GetDirContent()
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            End If
        Else
            Dim ftp As Rebex.Net.Sftp = oFTP

            Try
                ftp.CheckConnectionState()
            Catch ex As Exception
                MessageBox.Show("CRD is not connected to the FTP Server", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Warning)
                Return
            End Try

            Dim oListItem As ListViewItem = lsvFTP.SelectedItems(0)

            If oListItem.Tag = "Folder" Then
                ftp.ChangeDirectory(oListItem.Text)

                sPath &= oListItem.Text & "/"

                lblPath.Text = sPath

                GetDirContent()
            ElseIf oListItem.Tag = "Up" Then
                Try

                    Dim sFolders As String() = ftp.GetCurrentDirectory.Split("/") 'sPath.Split("/")

                    sPath = ""

                    For I As Int32 = 0 To sFolders.GetUpperBound(0) - 1
                        sPath &= sFolders(I) & "/"
                    Next

                    lblPath.Text = sPath

                    ftp.ChangeDirectory(sPath)

                    GetDirContent()
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            End If
        End If
    End Sub

    Private Sub ucFTPBrowser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtServerAddress.ContextMenu = Me.mnuInserter
        Me.txtUserName.ContextMenu = Me.mnuInserter
        Me.txtPassword.ContextMenu = Me.mnuInserter
        Me.lblPath.ContextMenu = Me.mnuInserter
    End Sub

    'Private Sub oFTP_FileTransferStatus(ByVal sender As Object, ByVal e As Xceed.Ftp.FileTransferStatusEventArgs) Handles oFTP.FileTransferStatus
    '    oUI.BusyProgress(e.AllBytesPercent, "Transfering files...")
    'End Sub


    Private Sub cmbFTPType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFTPType.SelectedIndexChanged
        'If cmbFTPType.Text <> "FTP" Then
        '    If gnEdition < gEdition.ENTERPRISEPROPLUS Then
        '        _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
        '        cmbFTPType.SelectedIndex = 0
        '        Return
        '    End If
        'End If
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me.ParentForm, Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub lblPath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lblPath.TextChanged
        sPath = Me.lblPath.Text
    End Sub
End Class
