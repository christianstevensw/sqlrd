Imports Microsoft.Win32
Imports Microsoft.Win32.Registry
Public Class ucRegistryBrowser
    Inherits System.Windows.Forms.UserControl

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'UserControl overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tvRegistry As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents lblPath As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(ucRegistryBrowser))
        Me.tvRegistry = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lblPath = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'tvRegistry
        '
        Me.tvRegistry.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tvRegistry.ImageList = Me.ImageList1
        Me.tvRegistry.Indent = 19
        Me.tvRegistry.ItemHeight = 16
        Me.tvRegistry.Location = New System.Drawing.Point(8, 8)
        Me.tvRegistry.Name = "tvRegistry"
        Me.tvRegistry.Nodes.AddRange(New System.Windows.Forms.TreeNode() {New System.Windows.Forms.TreeNode("My Computer", 2, 2, New System.Windows.Forms.TreeNode() {New System.Windows.Forms.TreeNode("HKEY_LOCAL_MACHINE"), New System.Windows.Forms.TreeNode("HKEY_CURRENT_USER", 0, 0)})})
        Me.tvRegistry.Size = New System.Drawing.Size(384, 232)
        Me.tvRegistry.TabIndex = 0
        '
        'ImageList1
        '
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'lblPath
        '
        Me.lblPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblPath.Location = New System.Drawing.Point(8, 248)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.ReadOnly = True
        Me.lblPath.Size = New System.Drawing.Size(384, 21)
        Me.lblPath.TabIndex = 4
        Me.lblPath.Text = ""
        '
        'ucRegistryBrowser
        '
        Me.Controls.Add(Me.lblPath)
        Me.Controls.Add(Me.tvRegistry)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Name = "ucRegistryBrowser"
        Me.Size = New System.Drawing.Size(400, 280)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub tvRegistry_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvRegistry.AfterSelect
        On Error Resume Next
        Dim oUi As New clsMarsUI
        Dim oReg As RegistryKey
        Dim s As String
        Dim oNode As TreeNode
        Dim sPath As String
        Dim oChild As RegistryKey
        Dim I As Integer = 0
        Dim nVal As Integer

        oNode = tvRegistry.SelectedNode

        lblPath.Text = oNode.FullPath.Replace("My Computer\", "")

        Select Case oNode.Text.ToLower
            Case "hkey_local_machine"
                oReg = Registry.LocalMachine

                If oNode.Nodes.Count > 0 Then Exit Sub

                For Each s In oReg.GetSubKeyNames
                    Dim NewNode As New TreeNode

                    NewNode.ImageIndex = 1
                    newnode.SelectedImageIndex = 1
                    NewNode.Text = s
                    oNode.Nodes.Add(NewNode)

                    I += 1

                    nVal = (I / oReg.GetSubKeyNames.Length) * 100

                    oUi.BusyProgress(nVal, "Opening key...")

                Next

                oReg.Close()
            Case "hkey_current_user"
                oReg = Registry.CurrentUser

                If oNode.Nodes.Count > 0 Then Exit Sub

                For Each s In oReg.GetSubKeyNames
                    Dim NewNode As New TreeNode

                    NewNode.ImageIndex = 1
                    newnode.SelectedImageIndex = 1
                    NewNode.Text = s
                    oNode.Nodes.Add(NewNode)

                    I += 1

                    nVal = (I / oReg.GetSubKeyNames.Length) * 100

                    oUi.BusyProgress(nVal, "Opening key...")

                Next

                oReg.Close()
            Case "my computer"

            Case Else

                If oNode.Nodes.Count > 0 Then Exit Sub

                sPath = oNode.FullPath.ToLower
                Dim sParentKey As String = sPath.Split("\")(1).ToLower
                Dim sKey As String

                If sParentKey = "hkey_local_machine" Then
                    sKey = sPath.Replace("my computer\hkey_local_machine\", "")

                    oChild = Registry.LocalMachine.OpenSubKey(sKey)

                    For Each s In oChild.GetSubKeyNames
                        Dim NewNode As New TreeNode
                        NewNode.ImageIndex = 1
                        newnode.SelectedImageIndex = 1
                        NewNode.Text = s
                        oNode.Nodes.Add(NewNode)

                        I += 1

                        nVal = (I / oChild.GetSubKeyNames.Length) * 100

                        oUi.BusyProgress(nVal, "Opening key...")

                    Next

                    oChild.Close()
                ElseIf sParentKey = "hkey_current_user" Then
                    sKey = sPath.Replace("my computer\hkey_current_user\", "")

                    oChild = Registry.CurrentUser.OpenSubKey(sKey)

                    For Each s In oChild.GetSubKeyNames
                        Dim NewNode As New TreeNode
                        NewNode.ImageIndex = 1
                        newnode.SelectedImageIndex = 1
                        NewNode.Text = s
                        oNode.Nodes.Add(NewNode)

                        I += 1

                        nVal = (I / oChild.GetSubKeyNames.Length) * 100

                        oUi.BusyProgress(nVal, "Opening key...")

                    Next

                    oChild.Close()
                End If
        End Select

        oNode.Expand()

        oUi.BusyProgress(, , True)

        Return
Trap:
        Return
    End Sub

    
End Class
