Imports Microsoft.Win32
Public Class clsMarsSnapshots
    Dim oData As New clsMarsData

    Public Shared ReadOnly Property m_SnapShotsPath() As String
        Get
            Dim value As String

            value = clsMarsUI.MainUI.ReadRegistry("SnapshotsPath", sAppPath & "Snapshots")

            If value.EndsWith("\") = False Then value &= "\"

            Return value
        End Get
    End Property
    Public Overloads Shared Function CreateSnapshot(ByVal nPackID As Integer, ByVal sFiles() As String) As Boolean
        Dim SQL As String
        Dim execID As String = clsMarsData.CreateDataID("SnapshotsAttr", "execid")
        Dim cols As String
        Dim vals As String
        Dim snapPath As String

        If clsMarsData.IsDuplicate("ReportSnapshots", "PackID", nPackID, False) = False Then Return True

        cols = "AttrID,PackID,SnapPath,DateCreated,ExecID"

        Try


            For Each s As String In sFiles
                If s.Length = 0 Then Continue For

                snapPath = m_SnapShotsPath

                If IO.Directory.Exists(snapPath) = False Then
                    IO.Directory.CreateDirectory(snapPath)
                End If

                Dim stamp As String = "{" & clsMarsData.CreateDataID("", "") & "}"

                Dim fileName As String = ExtractFileName(s)
                Dim dot As Integer = fileName.IndexOf(".")
                Dim directory As String

                fileName = fileName.Insert(dot, stamp)

                snapPath &= fileName

                IO.File.Copy(s, snapPath)

                vals = clsMarsData.CreateDataID("SnapshotsAttr", "attrid") & "," & _
                nPackID & "," & _
                "'" & SQLPrepare(snapPath) & "'," & _
                "'" & ConDateTime(Date.Now) & "'," & _
                execID

                SQL = "INSERT INTO SnapshotsAttr (" & cols & ") VALUES (" & vals & ")"

                clsMarsData.WriteData(SQL, False)
            Next

            Return True
        Catch ex As Exception

            Return False
        End Try
    End Function

    Public Overloads Shared Function CreateSnapshot(ByVal nReportID As Integer, ByVal sPath As String, _
    Optional ByVal HTMLPath As String = "") As Boolean
        Dim SQL As String
        Dim Stamp As String = "{" & clsMarsData.CreateDataID("", "") & "}"
        Dim sCols As String
        Dim sVals As String
        Dim SnapPath As String

        If clsMarsData.IsDuplicate("ReportSnapshots", "ReportID", nReportID, False) = False Then Return True

        sCols = "AttrID,ReportID,SnapPath,DateCreated"

        Try
            SnapPath = m_SnapShotsPath

            If System.IO.Directory.Exists(SnapPath) = False Then
                System.IO.Directory.CreateDirectory(SnapPath)
            End If

            Dim sFilename As String = ExtractFileName(sPath)
            Dim nDot As Int32 = sFilename.IndexOf(".")
            Dim sDirectory As String

            sFilename = sFilename.Insert(nDot, Stamp)

            SnapPath &= sFilename

            System.IO.File.Copy(sPath, SnapPath)

            If HTMLPath.Length > 0 Then
                For Each s As String In IO.Directory.GetFiles(HTMLPath)
                    If s.EndsWith(".htm") = False Then
                        IO.File.Copy(s, GetDirectory(SnapPath) & ExtractFileName(s))
                    End If
                Next
            End If

            sVals = clsMarsData.CreateDataID("Snapshotsattr", "attrid") & "," & _
            nReportID & "," & _
            "'" & SQLPrepare(SnapPath) & "'," & _
            "'" & ConDateTime(Date.Now) & "'"

            SQL = "INSERT INTO SnapshotsAttr (" & sCols & ") VALUES (" & sVals & ")"

            Return clsMarsData.WriteData(SQL, False)
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub _ClearSnapshots()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim nKeep As Integer
        Dim nReportID As Integer
        Dim sDateDiff As String
        Dim sFilePath As String
        Dim nDateCreated As Date
        Dim nAttrID As Int32
        Dim nInterval As Integer

        If gConType = "DAT" Then
            sDateDiff = "'d'"
        Else
            sDateDiff = "d"
        End If

        SQL = "SELECT * FROM ReportSnapshots r INNER JOIN SnapshotsAttr s ON r.ReportID = s.ReportID"

        oRs = clsmarsdata.GetData(SQL)

        Try
            Do While oRs.EOF = False
                nKeep = oRs("keepsnap").Value
                'nReportID = oRs("reportid").Value
                sFilePath = oRs("snappath").Value
                nDateCreated = oRs("datecreated").Value
                nAttrID = oRs("attrid").Value

                nInterval = DateDiff(DateInterval.Day, nDateCreated.Date, Now.Date)

                If nInterval >= nKeep Then
                    SQL = "DELETE FROM SnapShotsAttr WHERE AttrID =" & nAttrID

                    clsMarsData.WriteData(SQL)

                    Try
                        IO.File.Delete(sFilePath)
                    Catch : End Try
                End If
                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Public Overloads Sub ExecuteSnapshot(ByVal packid As Integer, ByVal files() As String)
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim Stamp As String
        Dim sTempPath As String
        Dim oUI As New clsMarsUI
        Dim resultFiles() As String
        Dim I As Integer = 0

        ReDim resultFiles(I)

        For Each spath As String In files
            nStart = spath.IndexOf("{")
            nEnd = spath.IndexOf("}")

            ReDim Preserve resultFiles(I)

            Stamp = spath.Substring(nStart, nEnd - nStart) & "}"

            Dim sNewFile As String = spath.Replace(Stamp, String.Empty)

            sTempPath = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

            If sTempPath.EndsWith("\") = False Then sTempPath &= "\"

            sNewFile = ExtractFileName(sNewFile)

            If System.IO.File.Exists(sTempPath & sNewFile) = True Then
                System.IO.File.Delete(sTempPath & sNewFile)
            End If

            System.IO.File.Copy(spath, sTempPath & sNewFile)

            resultFiles(I) = sTempPath & sNewFile

            I += 1
        Next

        Dim oReport As New clsMarsReport

        Dim Ok As Boolean = oReport.RunPackageSchedule(packid, , , , True, resultFiles)

        If Ok = True Then
            MessageBox.Show("Snapshot executed successfully", _
            Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
        End If

    End Sub
    Public Overloads Function ExecuteSnapshot(ByVal nReportID As Integer, ByVal sPath As String) As Boolean
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim Stamp As String
        Dim sTempPath As String
        Dim oUI As New clsMarsUI

        Try
            nStart = sPath.IndexOf("{")
            nEnd = sPath.IndexOf("}")

            Stamp = sPath.Substring(nStart, nEnd - nStart) & "}"

            Dim sNewFile As String = sPath.Replace(Stamp, String.Empty)

            sTempPath = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

            If sTempPath.EndsWith("\") = False Then sTempPath &= "\"

            sNewFile = ExtractFileName(sNewFile)

            If System.IO.File.Exists(sTempPath & sNewFile) = True Then
                System.IO.File.Delete(sTempPath & sNewFile)
            End If

            System.IO.File.Copy(sPath, sTempPath & sNewFile)

            Dim oReport As New clsMarsReport

            Dim Ok As Boolean = oReport.RunSingleSchedule(nReportID, False, True, sTempPath & sNewFile)

            If Ok = True Then
                MessageBox.Show("Snapshot executed successfully", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, "clsMarsSnapshots._ExecuteSnapshot", _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Function
End Class
