Imports System.IO
Imports Microsoft.Win32
Imports Microsoft.Win32.Registry
Imports DZACTXLib

Public Class clsMarsTask
    Dim oData As clsMarsData = New clsMarsData
    Dim oUI As New clsMarsUI
    Dim ThreadFile As String
    Friend WithEvents oFtp As Xceed.Ftp.FtpClient



    Public Enum TaskDirection
        Up = 1
        Down = 2
    End Enum

    Public Enum enRunTime
        BEFORE = 0
        AFTERDEL = 1
        AFTERPROD = 2
        BOTH = 3
        NONE = 4
    End Enum

    Public Enum enAfterType
        DELIVERY = 0
        PRODUCTION = 1
    End Enum
    Public Function TaskPreProcessor(ByVal nScheduleID As Integer, ByVal oRunTime As enRunTime)
        Select Case oRunTime
            Case enRunTime.BEFORE
                ProcessTasks(nScheduleID, "BOTH", enAfterType.DELIVERY)
                ProcessTasks(nScheduleID, "BOTH", enAfterType.PRODUCTION)
                ProcessTasks(nScheduleID, "BEFORE")
            Case enRunTime.AFTERDEL
                ProcessTasks(nScheduleID, "AFTER", enAfterType.DELIVERY)
                ProcessTasks(nScheduleID, "BOTH", enAfterType.DELIVERY)
            Case enRunTime.AFTERPROD
                ProcessTasks(nScheduleID, "AFTER", enAfterType.PRODUCTION)
                ProcessTasks(nScheduleID, "BOTH", enAfterType.PRODUCTION)
        End Select
    End Function
    Public Function DeleteTask(ByVal nTaskID As Integer)
        Dim sWhere As String
        Dim SQL As String

        SQL = "DELETE FROM Tasks WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        SQL = "DELETE FROM TaskOptions WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        clsMarsUI.MainUI.DeleteDestination(nTaskID, 0, 0)

    End Function

    Public Function MoveTask(ByVal ScheduleID As Integer, ByVal nTaskID As Integer, _
    ByVal oDirection As TaskDirection)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nLoc As Integer

        SQL = "SELECT OrderID FROM Tasks WHERE TaskID = " & nTaskID

        oRs = clsmarsdata.GetData(SQL)

        nLoc = oRs("orderid").Value

        Select Case oDirection
            Case TaskDirection.Up
                If nLoc = 1 Then Exit Function

                clsmarsdata.WriteData("UPDATE Tasks SET OrderID = 99999 WHERE OrderID = " & nLoc - 1 & _
                    " AND ScheduleID = " & ScheduleID)

                clsmarsdata.WriteData("UPDATE Tasks SET OrderID = " & nLoc - 1 & " WHERE " & _
                    "TaskID = " & nTaskID)

                clsmarsdata.WriteData("UPDATE Tasks SET OrderID = " & nLoc & " WHERE OrderID = 99999")
            Case TaskDirection.Down
                If nLoc = GetOrderID(ScheduleID) - 1 Then Exit Function

                clsmarsdata.WriteData("UPDATE Tasks SET OrderID = 99999 WHERE OrderID = " & nLoc + 1 & _
                    " AND ScheduleID = " & ScheduleID)

                clsMarsData.WriteData("UPDATE Tasks SET OrderID = " & nLoc + 1 & " WHERE " & _
                    "TaskID = " & nTaskID)

                clsMarsData.WriteData("UPDATE Tasks SET OrderID = " & nLoc & " WHERE OrderID = 99999")
        End Select
    End Function

    Public Function GetOrderID(ByVal ScheduleID As Integer) As Integer
        Dim SQL As String
        Dim nOrderID As Integer

        SQL = "SELECT MAX(OrderID) FROM Tasks WHERE ScheduleID =" & ScheduleID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                If IsNull(oRs.Fields(0).Value) <> "" Then
                    nOrderID = oRs.Fields(0).Value
                Else
                    nOrderID = 0
                End If
            Else
                nOrderID = 0
            End If
        Else
            nOrderID = 0
        End If

        nOrderID += 1

        Return nOrderID
    End Function

    Public Function ProcessTasks(ByVal nScheduleID As Integer, _
           Optional ByVal sRunTime As String = "AFTER", _
           Optional ByVal oAfterType As enAfterType = enAfterType.DELIVERY) As Boolean
        Dim sTask As String
        Dim sTaskName As String
        Dim ok As Boolean = False

10:     gErrorSuggest = String.Empty

20:     Try
30:         Dim oData As New clsMarsData
40:         Dim SQL As String
50:         Dim sTaskType As String
60:         Dim sWhere As String
            Dim taskID As Integer

70:         sWhere = " WHERE ScheduleID = " & nScheduleID & " AND (EnabledStatus IS NULL OR EnabledStatus = 1) "

80:         Select Case sRunTime
                Case "AFTER"
90:                 Select Case oAfterType
                        Case enAfterType.DELIVERY
100:                        sWhere &= " AND RunWhen = 'AFTER' AND AfterType = 'Delivery'"
110:                    Case enAfterType.PRODUCTION
120:                        sWhere &= " AND RunWhen = 'AFTER' AND AfterType = 'Production'"
                    End Select
130:            Case "BOTH"
140:                Select Case oAfterType
                        Case enAfterType.DELIVERY
150:                        sWhere &= " AND RunWhen = 'BOTH' AND AfterType = 'Delivery'"
160:                    Case enAfterType.PRODUCTION
170:                        sWhere &= " AND RunWhen = 'BOTH' AND AfterType = 'Production'"
                    End Select
180:            Case "BEFORE"
190:                sWhere &= " AND RunWhen = 'BEFORE'"
200:            Case "NONE"
210:                sWhere &= ""
            End Select

220:        SQL = "SELECT * FROM Tasks " & sWhere & " ORDER BY OrderID"

230:        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

240:        If Not oRs Is Nothing Then

250:            Do While oRs.EOF = False

260:                If frmMain.m_UserCancel = True Then
270:                    Throw New Exception("Task execution cancelled by user")
                    End If

280:                sTaskType = oRs("tasktype").Value
290:                sTaskName = oRs("taskname").Value
                    taskID = oRs("taskid").Value

300:                If frmMain.m_UserCancel = True Then
310:                    Throw New Exception("Task " & sTaskName & " cancelled by user")
                    End If

320:                Select Case sTaskType.ToLower
                        Case "dbexport"
                            sTask = "Export Database"
                            Dim exporter As clsCustomReport = New clsCustomReport

                            ok = exporter.databaseExport(taskID)
                        Case "mergepdf"
330:                        sTask = "Merge PDF"
340:                        oUI.BusyProgress(75, "Custom actions: Merge PDF Files...")
350:                        MergePDFs(oRs("taskid").Value, oRs("programpath").Value, oRs("filelist").Value, oRs("replacefiles").Value)
360:                    Case "copyfile"
370:                        sTask = "Copy File"
380:                        oUI.BusyProgress(75, "Custom actions: Copy File...")
390:                        CopyFiles(oRs("programpath").Value, oRs("filelist").Value, oRs("replacefiles").Value, _
                                  Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))), _
                                  Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0)))))
400:                        ok = True
410:                    Case "deletefile"
420:                        sTask = "Delete File"
430:                        oUI.BusyProgress(75, "Custom actions: Delete File...")
440:                        DeleteFiles(oRs("filelist").Value, Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))))
450:                        ok = True
460:                    Case "sendmail"
470:                        sTask = "Send Mail"
480:                        oUI.BusyProgress(75, "Custom actions: Send Mail...")

490:                        Dim oMsg As clsMarsMessaging = New clsMarsMessaging
500:                        Dim oParse As New clsMarsParser
510:                        Dim Sendto As String
520:                        Dim Subject, senderName, senderAddress As String
530:                        Dim Msg As String
540:                        Dim Cc As String
550:                        Dim Bcc As String
560:                        Dim sMailFormat As String = ""
570:                        Dim sAttach As String = ""
580:                        Dim smtpServer As String = ""
590:                        Dim ForHTML As Boolean = False

600:                        sMailFormat = oRs("programpath").Value

610:                        If sMailFormat = "HTML" Then
620:                            ForHTML = True
630:                        End If

640:                        Sendto = oMsg.ResolveEmailAddress(oRs("sendto").Value)
650:                        Subject = oRs("subject").Value
660:                        Subject = oParse.ParseString(Subject)
670:                        Msg = oRs("msg").Value
680:                        Msg = oParse.ParseString(Msg, ForHTML)
690:                        Cc = oMsg.ResolveEmailAddress(oRs("cc").Value)
700:                        Bcc = oMsg.ResolveEmailAddress(oRs("bcc").Value)
710:                        ''console.writeLine(Msg)
720:                        sAttach = IsNull(oRs("filelist").Value)
730:                        sAttach = oParse.ParseString(sAttach)
740:                        smtpServer = IsNull(oRs("printername").Value, "Default")

                            Try
741:                            senderName = SQLPrepare(IsNull(oRs("sendername").Value))
742:                            senderAddress = SQLPrepare(IsNull(oRs("senderaddress").Value))
                            Catch : End Try

750:                        Sendto = oParse.ParseString(Sendto)
760:                        Cc = oParse.ParseString(Cc)
770:                        Bcc = oParse.ParseString(Bcc)
780:
790:                        If MailType = MarsGlobal.gMailType.MAPI Then

800:                            oMsg.SendMAPI(Sendto, Subject, _
                                      Msg, "Single", , , sAttach, _
                                      Cc, Bcc, False, , gScheduleName, True)

810:                        ElseIf MailType = MarsGlobal.gMailType.SMTP Or _
                                  MailType = MarsGlobal.gMailType.SQLRDMAIL Then

820:                            oMsg.SendSMTP(Sendto, Subject, _
                                      Msg, "Single", , , sAttach, _
                                      Cc, Bcc, gScheduleName, False, , True, _
                                      , sMailFormat, smtpServer, , senderName, senderAddress)
830:                        ElseIf MailType = gMailType.GROUPWISE Then
840:                            oMsg.SendGROUPWISE(Sendto, Cc, Bcc, Subject, Msg, "", "Single", sAttach, _
                                      , , , True, , , )
850:                        End If
860:                    Case "createfolder"
870:                        sTask = "Create Folder"
880:                        oUI.BusyProgress(75, "Custom actions: Create Folder...")
890:                        CreateFolder(oRs("programpath").Value)
900:                    Case "movefolder"
910:                        sTask = "Move Folder"
920:                        oUI.BusyProgress(75, "Custom actions: Rename/Move Folder...")
930:                        MoveFolder(oRs("programpath").Value, oRs("filelist").Value)
940:                    Case "pause"
950:                        sTask = "Pause"
960:                        oUI.BusyProgress(75, "Custom actions: Pause...")
970:                        _Delay(oRs("pauseint").Value)
980:                    Case "print"
990:                        sTask = "Print File"
1000:                       oUI.BusyProgress(75, "Custom actions: Print File...")
1010:                       PrintFiles(oRs("filelist").Value, oRs("printername").Value, Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))))
1020:                       ok = True
1030:                   Case "program"
1040:                       sTask = "Run Program"
1050:                       oUI.BusyProgress(75, "Custom actions: Run Program...")
1060:                       RunProgram(oRs("programpath").Value, oRs("programparameters").Value, oRs("windowstyle").Value)
1070:                   Case "renamefile"
1080:                       sTask = "Rename File"
1090:                       oUI.BusyProgress(75, "Custom actions: Rename File...")
1100:                       RenameFile(oRs("programpath").Value, oRs("filelist").Value, _
                                 oRs("replacefiles").Value)
1110:                   Case "webbrowse"
1120:                       sTask = "Web Browse"
1130:                       Dim oProc As New Process
1140:                       oUI.BusyProgress(75, "Custom actions: Web Browse...")
1150:                       oProc.StartInfo.FileName = oRs("programpath").Value

1160:                       oProc.Start()
1170:                   Case "writefile"
1180:                       sTask = "Write to File"
1190:                       oUI.BusyProgress(75, "Custom actions: Write to File...")
1200:                       WriteText(oRs("msg").Value, oRs("programpath").Value, _
                                 oRs("replacefiles").Value)
1210:                   Case "sqlscript"
1220:                       sTask = "Execute SQL Script"
1230:                       oUI.BusyProgress(75, "Custom actions: Execute SQL Script...")
1240:                       ExecuteFromFile(oRs("programpath").Value, oRs("programparameters").Value)
1250:                   Case "sqlproc"
1260:                       sTask = "Execute SQL Procedure"
1270:                       oUI.BusyProgress(75, "Custom actions: Execute SQL Proc...")
1280:                       ExecuteProc(oRs("printername").Value, oRs("programpath").Value, oRs("programparameters").Value)
1290:                   Case "dbupdate", "dbinsert", "dbdelete", "dbcreatetable", _
                             "dbdeletetable", "dbmodifytable_add", "dbmodifytable_drop"
1300:                       sTask = "DB Modify"
1310:                       oUI.BusyProgress(75, "Custom actions: Execute Query...")
1320:                       ExecuteQuery(oRs("msg").Value, oRs("programparameters").Value)

1330:                   Case "ftpupload"
1340:                       sTask = "FTP Upload"
1350:                       oUI.BusyProgress(75, "Custom actions: Upload file...")

                            Dim passive As Boolean
                            Dim FtpOptions As String

1360:                       Try
1370:                           passive = Convert.ToInt32(oRs("ftppassive").Value)
                            Catch : End Try

1380:                       Try
1390:                           FtpOptions = IsNull(oRs("ftpoptions").Value)
1400:                       Catch ex As Exception

                            End Try

1410:                       FTPUpload2(oRs("ftpserver").Value, oRs("ftpport").Value, _
                                 oRs("ftpuser").Value, oRs("ftppassword").Value, oRs("ftpdirectory").Value, _
                                 oRs("filelist").Value, IsNull(oRs("ftptype").Value, "FTP"), FtpOptions, _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))), _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0)))), passive)

1420:                       ok = True
1430:                   Case "ftpdownload"
1440:                       sTask = "FTP Download"
1450:                       oUI.BusyProgress(75, "Custom actions: Download File...")

                            Dim passive As Boolean

1460:                       Try
1470:                           passive = Convert.ToInt32(oRs("ftppassive").Value)
                            Catch : End Try

                            Dim FtpOptions As String

1480:                       Try
1490:                           FtpOptions = IsNull(oRs("ftpoptions").Value)
                            Catch : End Try

1500:                       FTPDownload(oRs("ftpserver").Value, oRs("ftpport").Value, _
                                 oRs("ftpuser").Value, oRs("ftppassword").Value, oRs("ftpdirectory").Value, _
                                 oRs("filelist").Value, oRs("programpath").Value, _
                                 Convert.ToBoolean(oRs("replacefiles").Value), IsNull(oRs("ftptype").Value, "FTP"), _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))), _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0)))), passive, FtpOptions)
1510:                       ok = True
1520:                   Case "ftpdeletefile"
1530:                       sTask = "FTP Delete File"
1540:                       oUI.BusyProgress(75, "Custom actions: Delete File (FTP)...")

                            Dim passive As Boolean

1550:                       Try
1560:                           passive = Convert.ToInt32(oRs("ftppassive").Value)
                            Catch : End Try

                            Dim FtpOptions As String

1570:                       Try
1580:                           FtpOptions = IsNull(oRs("ftpoptions").Value)
                            Catch : End Try

1590:                       FTPDeleteFile(oRs("ftpserver").Value, oRs("ftpport").Value, _
                                 oRs("ftpuser").Value, oRs("ftppassword").Value, oRs("ftpdirectory").Value, _
                                 oRs("filelist").Value, IsNull(oRs("ftptype").Value, "FTP"), passive, FtpOptions)
1600:                       ok = True
1610:                   Case "ftpcreatedirectory"
1620:                       sTask = "FTP Create Directory"
1630:                       oUI.BusyProgress(75, "Custom actions: Create Directory (FTP)...")

                            Dim passive As Boolean

1640:                       Try
1650:                           passive = Convert.ToInt32(oRs("ftppassive").Value)
                            Catch : End Try

                            Dim FtpOptions As String

1660:                       Try
1670:                           FtpOptions = IsNull(oRs("ftpoptions").Value)
                            Catch : End Try

1680:                       FTPCreateDirectory(oRs("ftpserver").Value, oRs("ftpport").Value, oRs("ftpuser").Value, oRs("ftppassword").Value, oRs("ftpdirectory").Value, _
                                 oRs("programpath").Value, IsNull(oRs("ftptype").Value, "FTP"), passive, FtpOptions)
1690:                       ok = True
1700:                   Case "ftpdeletedirectory"
1710:                       sTask = "FTP Delete Directory"
1720:                       oUI.BusyProgress(75, "Custom actions: Delete Directory (FTP)...")

                            Dim passive As Boolean

1730:                       Try
1740:                           passive = Convert.ToInt32(oRs("ftppassive").Value)
                            Catch : End Try

                            Dim FtpOptions As String

1750:                       Try
1760:                           FtpOptions = IsNull(oRs("ftpoptions").Value)
                            Catch : End Try

1770:                       FTPDeleteDirectory(oRs("ftpserver").Value, oRs("ftpport").Value, _
                                 oRs("ftpuser").Value, oRs("ftppassword").Value, oRs("ftpdirectory").Value, _
                                 oRs("filelist").Value, IsNull(oRs("ftptype").Value, "FTP"), passive, FtpOptions)
1780:                       ok = True

1790:                   Case "registrycreatekey"
1800:                       sTask = "Create Registry Key"
1810:                       oUI.BusyProgress(75, "Custom actions: Create Key...")
1820:                       CreateRegistryKey(oRs("programpath").Value, oRs("filelist").Value)
1830:                   Case "registrydeletekey"
1840:                       sTask = "Delete Registry Key"
1850:                       oUI.BusyProgress(75, "Custom actions: Delete Key...")
1860:                       DeleteRegistryKey(oRs("programpath").Value)
1870:                   Case "registrysetkey"
1880:                       sTask = "Set Registry Key"
1890:                       oUI.BusyProgress(75, "Custom actions: Set Key Value...")
1900:                       SetRegistryKey(oRs("programpath").Value, oRs("programparameters").Value, oRs("filelist").Value)
1910:                   Case "runschedule"
1920:                       sTask = "Execute Schedule"
1930:                       oUI.BusyProgress(75, "Custom actions: Run schedule...")

1940:                       Dim sType As String
1950:                       Dim nID As Integer

1960:                       sType = oRs("subject").Value

1970:                       nID = oRs("pauseint").Value

1980:                       RunSchedule(sType, nID)
1990:                   Case "zipfiles"
2000:                       sTask = "Zip File"
2010:                       oUI.BusyProgress(75, "Custom action: Zipping files...")

2020:                       ZipFiles(oRs("programpath").Value, oRs("filelist").Value, _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0)))), _
                                 Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0)))))
2030:                       ok = True
2040:                   Case "unzipfile"
2050:                       sTask = "Unzip File"
2060:                       oUI.BusyProgress(75, "Custom action: Unzipping files...")

2070:                       UnzipFiles(oRs("programpath").Value, oRs("filelist").Value)
2080:                   Case "sendsms"
2090:                       Dim oMsg As clsMarsMessaging = New clsMarsMessaging
2100:                       Dim sendTo, sMsg As String

2110:                       sendTo = IsNull(oRs("sendto").Value)
2120:                       sMsg = IsNull(oRs("msg").Value)

2130:                       sendTo = oMsg.ResolveEmailAddress(sendTo)
2140:                       sendTo = clsMarsParser.Parser.ParseString(sendTo)

2150:                       sMsg = clsMarsParser.Parser.ParseString(sMsg)

2160:                       ok = oMsg.SendSMS(sendTo, sMsg, "")

2170:                       End Select

Oi:
2180:               oRs.MoveNext()
2190:           Loop

2195:           oRs.Close()

2200:       End If

2210:       oUI.BusyProgress(, , True)

2230:       Return True
2240:   Catch ex As Exception
            gErrorDesc = gScheduleName & " - Task Execution: " & sTask & "[" & sTaskName & "] - " & ex.Message
2250:       gErrorNumber = Err.Number
2260:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2270:       gErrorLine = Erl()
2280:       _ErrorHandle(gErrorDesc, Err.Number, _
     Reflection.MethodBase.GetCurrentMethod.Name, Erl(), gErrorSuggest)
2290:       Return False
        End Try

    End Function

    Private Sub RunSchedule(ByVal sObject As String, ByVal nID As Integer)
        Dim oReport As New clsMarsReport
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        Select Case sObject.ToLower
            Case "report"
                SQL = "SELECT Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportID =" & nID

                oRs = clsmarsdata.GetData(SQL)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        If IsNull(oRs(0).Value, "0") = "1" Then
                            oReport.RunDynamicSchedule(nID)

                        ElseIf IsNull(oRs(2).Value, "0") = "1" Then
                            oReport.RunDataDrivenSchedule(nID)
                        Else
                            oReport.RunSingleSchedule(nID)
                        End If
                    End If
                End If
            Case "automation"
                Dim oAuto As New clsMarsAutoSchedule

                oAuto.ExecuteAutomationSchedule(nID)
            Case "package"
                SQL = "SELECT Dynamic,IsDataDriven FROM PackageAttr WHERE PackID =" & nID

                oRs = clsmarsdata.GetData(SQL)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        If IsNull(oRs(0).Value, "0") = "1" Then
                            oReport.RunDynamicPackageSchedule(nID)
                        ElseIf IsNull(oRs(1).Value, "0") = "1" Then
                            oReport.RunDataDrivenPackage(nID)
                        Else
                            oReport.RunPackageSchedule(nID)
                        End If
                    End If
                End If
            Case "event"
                Dim oEvent As New clsMarsEvent

                oEvent.RunEvents6(nID)
            Case "event-package"
                Dim oEvent As New clsMarsEvent

                oEvent.RunEventPackage(nID)
        End Select

    End Sub
    Private Sub SetRegistryKey(ByVal sPath As String, ByVal sKey As String, ByVal sVal As Object)

        Dim oUI As New clsMarsUI
        Dim sRegKey As String
        Dim sParent As String
        Dim sTemp As String

        sParent = sPath.Split("\")(0).ToLower

        Select Case sParent
            Case "hkey_local_machine"
                sRegKey = sPath.ToLower
                sRegKey = sRegKey.Replace("hkey_local_machine\", String.Empty)
                oUI.SaveRegistry(Registry.LocalMachine, sRegKey, sKey, sVal)
            Case "hkey_current_user"
                sRegKey = sPath.ToLower
                sRegKey = sRegKey.Replace("hkey_current_user\", String.Empty)
                oUI.SaveRegistry(Registry.CurrentUser, sRegKey, sKey, sVal)
        End Select
    End Sub
    Private Sub CreateRegistryKey(ByVal sPath As String, ByVal sKey As String)
        Dim oReg As RegistryKey
        Dim sParent As String

        sParent = sPath.Split("\")(0).ToLower

        sPath = sPath.ToLower

        If sParent = "hkey_local_machine" Then
            sPath = sPath.Replace("hkey_local_machine\", String.Empty)

            oReg = Registry.LocalMachine

            oReg = oReg.OpenSubKey(sPath, True)

            oReg.CreateSubKey(sKey)

            oReg.Close()
        Else
            sPath = sPath.Replace("hkey_current_user\", String.Empty)

            oReg = Registry.LocalMachine

            oReg = oReg.OpenSubKey(sPath, True)

            oReg.CreateSubKey(sKey)

            oReg.Close()
        End If
    End Sub

    Private Sub DeleteRegistryKey(ByVal sPath As String)
        Dim oReg As RegistryKey
        Dim sParent As String

        sParent = sPath.Split("\")(0).ToLower

        sPath = sPath.ToLower

        If sParent = "hkey_local_machine" Then
            sPath = sPath.Replace("hkey_local_machine\", String.Empty)

            oReg = Registry.LocalMachine

            oReg.DeleteSubKey(sPath)

            oReg.Close()
        Else
            sPath = sPath.Replace("hkey_current_user\", String.Empty)

            oReg = Registry.LocalMachine

            oReg.DeleteSubKey(sPath)

            oReg.Close()
        End If
    End Sub
    Private Sub ExecuteProc(ByVal sProc As String, ByVal sParameters As String, ByVal sConn As String)
        Dim sCommand As String
        Dim oParse As New clsMarsParser

        sCommand = oParse.ParseString(sProc, False, True) & " " & oParse.ParseString(sParameters, False, True)

        ''console.writeLine(sCommand)
        ExecuteQuery(sCommand, sConn)

    End Sub
    Private Sub ExecuteFromFile(ByVal sPath As String, ByVal sConn As String)
        Dim sCommand As String

        sCommand = ReadTextFromFile(sPath)

        If sCommand.Length = 0 Then Return

        ExecuteQuery(sCommand, sConn)

    End Sub
    Private Sub ExecuteQuery(ByVal sCommand As String, ByVal sConn As String)
        Dim oCon As New ADODB.Connection
        Dim sDSN As String
        Dim sUser As String
        Dim sPass As String
        Dim oparse As clsMarsParser = New clsMarsParser
        Dim nRetry As Integer = 0

        sDSN = GetDelimitedWord(sConn, 1, "|")
        sUser = GetDelimitedWord(sConn, 2, "|")
        sPass = GetDelimitedWord(sConn, 3, "|")

RETRY:
        Try
            oCon.Open(sDSN, sUser, sPass)

            If gScheduleName.ToLower = "customer email journal" Then
                SaveTextToFile(Now & ": " & oparse.ParseString(sCommand), sAppPath & gScheduleName & ".debug", , True, False)
            End If

            oCon.Execute(oparse.ParseString(sCommand, , True))

            oCon.Close()

            oCon = Nothing
        Catch ex As Exception
            Try : oCon.Close() : Catch : End Try

            If nRetry < 6 Then
                System.Threading.Thread.Sleep(5000)
                nRetry += 1
                GoTo RETRY
            End If

            Throw ex
        End Try
    End Sub
    Private Sub RenameFile(ByVal sOriginal As String, ByVal sNew As String, _
   ByVal nReplace As Integer)

        Dim oparse As New clsMarsParser

        sOriginal = oparse.ParseString(sOriginal)
        sNew = oparse.ParseString(sNew)

        Dim dir As String = GetDirectory(sNew)

        oparse.ParseDirectory(dir)

        If ExtractFileName(sOriginal).IndexOf("*") = -1 Then
            If Convert.ToBoolean(nReplace) = True Then
                If System.IO.File.Exists(sNew) = True Then
                    System.IO.File.Delete(sNew)
                End If
            Else
                If System.IO.File.Exists(sNew) = True Then
                    Return
                End If
            End If

            System.IO.File.Move(sOriginal, sNew)
        Else
            Dim fileName As String
            Dim filePath As String
            Dim fileDestination As String
            Dim extension As String = ""
            Dim newFile As String

            filePath = GetDirectory(sOriginal)
            fileName = ExtractFileName(sOriginal)
            fileDestination = GetDirectory(sNew)
            newFile = ExtractFileName(sNew)

            If newFile.IndexOf(".") > -1 Then
                extension = newFile.Split(".")(1)

                If extension = "*" Then extension = ""
            End If

            For Each s As String In IO.Directory.GetFiles(filePath)
                Dim tempFile As String = ExtractFileName(s)
                Dim tempFilename As String = ""
                Dim tempExtension As String = ""

                If tempFile Like fileName Then

                    If tempFile.IndexOf(".") > -1 Then
                        tempFilename = IO.Path.GetFileNameWithoutExtension(tempFile) '  tempFile.Split(".")(0)
                        tempExtension = IO.Path.GetExtension(tempFile)
                    End If

                    If extension.Length > 0 Then
                        tempFilename = tempFilename & "." & extension
                    Else
                        tempFilename = tempFilename & tempExtension
                    End If

                    If CType(nReplace, Boolean) = True Then
                        IO.File.Delete(fileDestination & tempFilename)
                    End If

                    Try
                        IO.File.Move(s, fileDestination & tempFilename)
                    Catch : End Try
                End If
            Next

        End If
    End Sub
    Private Sub RunProgram(ByVal sPath As String, ByVal sArgs As String, ByVal sWindowStyle As String)
        Dim oProc As New Process
        Dim oStyle As ProcessWindowStyle
        Dim oParse As New clsMarsParser

        oProc.StartInfo.FileName = oParse.ParseString(sPath)
        oProc.StartInfo.Arguments = oParse.ParseString(sArgs)

        Select Case sWindowStyle.ToLower
            Case "normal"
                oStyle = ProcessWindowStyle.Normal
            Case "maximized"
                oStyle = ProcessWindowStyle.Maximized
            Case "minimized"
                oStyle = ProcessWindowStyle.Minimized
            Case "hidden"
                oStyle = ProcessWindowStyle.Hidden
        End Select

        oProc.StartInfo.WindowStyle = oStyle

        oProc.Start()
    End Sub

    Private Sub MergePDFs(ByVal nTaskID As Integer, ByVal sDestination As String, ByVal sFileList As String, ByVal nReplace As Int32)
        Dim sGo() As String
        Dim I As Integer = 0

        If nReplace = 0 And IO.File.Exists(sDestination) Then Return

        If nReplace = 1 And IO.File.Exists(sDestination) Then
            IO.File.Delete(sDestination)
        End If

        For Each s As String In sFileList.Split("|")
            If s.Length > 0 Then
                If s.EndsWith("*.pdf") Then
                    For Each sFile As String In IO.Directory.GetFiles(s.Replace("*.pdf", String.Empty))
                        If sFile.EndsWith(".pdf") Then
                            ReDim Preserve sGo(I)

                            sGo(I) = sFile

                            I += 1
                        End If
                    Next
                Else
                    ReDim Preserve sGo(I)

                    sGo(I) = s

                    I += 1
                End If
            End If
        Next

        If sGo Is Nothing Then Return

        If sGo.Length > 1 Then
            Dim oPDF As New clsMarsPDF
            Dim sResult As String

            sResult = oPDF.MergePDFFiles(sGo, ExtractFileName(sDestination))

            IO.File.Copy(sResult, sDestination, True)

            Try
                For Each s As String In IO.Directory.GetFiles(clsMarsReport.m_OutputFolder)
                    If s.StartsWith("{") Then
                        IO.File.Delete(s)
                    End If
                Next
            Catch : End Try

            'lets process PDF properties
            Try
                Dim oRs As ADODB.Recordset = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

                If oRs.EOF = False Then
                    Dim sOwnerPass As String
                    Dim sUserPass As String
                    Dim nPerm As Integer
                    Dim oParse As New clsMarsParser

                    Try
                        Dim infoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer As String
                        Dim InfoCreated As Date

                        infoTitle = oParse.ParseString(oRs("infotitle").Value)
                        InfoAuthor = oParse.ParseString(oRs("infoauthor").Value)
                        InfoSubject = oParse.ParseString(oRs("infosubject").Value)
                        InfoKeywords = oParse.ParseString(oRs("infokeywords").Value)
                        InfoProducer = oParse.ParseString(oRs("infoproducer").Value)
                        InfoCreated = oRs("infocreated").Value

                        oPDF.SetPDFSummary(infoTitle, InfoAuthor, _
                        InfoSubject, InfoKeywords, InfoCreated, InfoProducer _
                       , sDestination)

                        oPDF.AddWatermark(sDestination, IsNull(oRs("pdfwatermark").Value), 0)
                    Catch : End Try

                    If IsNull(oRs("pdfsecurity").Value, 0) = 1 Then

                        sOwnerPass = oParse.ParseString(IsNull(oRs("pdfpassword").Value))
                        sUserPass = oParse.ParseString(IsNull(oRs("userpassword").Value))

                        oPDF.SetPDFPermissions(sDestination, oRs("canprint").Value, _
                        oRs("cancopy").Value, _
                        oRs("canedit").Value, _
                        oRs("cannotes").Value, oRs("canfill").Value, oRs("canaccess").Value, _
                        oRs("canassemble").Value, _
                        oRs("canprintfull").Value, sOwnerPass, sUserPass)
                    End If
                End If

                oRs.Close()
            Catch : End Try
        End If
    End Sub

    Private Sub CopyFiles( _
  ByVal sDestination As String, _
  ByVal sFile As String, _
  ByVal nReplace As Int32, _
  Optional ByVal sRecursive As Boolean = False, _
  Optional ByVal sRFS As Boolean = False)


        Dim sPath As String
        Dim sFiles() As String
        Dim sFileName As String
        Dim bReplace As Boolean

        gErrorSuggest = "Please make sure that the file being copied exists and that you have " & _
        "access rights to the original file as well the destination folder and drive"


        If sFile.Contains("|") = True Then
            sFiles = sFile.Split("|")

            sFile = ""

            bReplace = Convert.ToBoolean(nReplace)

            If sDestination.EndsWith("\") = False Then sDestination &= "\"

            Dim oparse As New clsMarsParser

            sDestination = oparse.ParseString(sDestination)
            oparse.ParseDirectory(sDestination)

            For Each sFile In sFiles
                If sFile.Length > 0 Then
                    If sFile.IndexOf("*") > -1 Then
                        Dim compareString As String = ExtractFileName(sFile)

                        For Each x As String In IO.Directory.GetFiles(GetDirectory(sFile))
                            If ExtractFileName(x) Like compareString Then
                                sFileName = ExtractFileName(x)

                                IO.File.Copy(x, sDestination & sFileName, bReplace)
                            End If
                        Next
                    Else
                        sFileName = ExtractFileName(sFile)

                        System.IO.File.Copy(sFile, sDestination & sFileName, bReplace)
                    End If

                End If
            Next
        Else

            bReplace = Convert.ToBoolean(nReplace)
            If sDestination.EndsWith("\") = False Then sDestination &= "\"
            Dim oparse As New clsMarsParser
            sDestination = oparse.ParseString(sDestination)
            sFile = oparse.ParseString(sFile)

            DoCopyFiles(sFile, sDestination, bReplace, sRecursive, sRFS)

        End If
    End Sub

    Private Sub DeleteFiles(ByVal sFile As String, Optional ByVal sRecursive As Boolean = False)
        Dim sFiles() As String
        If sFile.Contains("|") = True Then

            sFiles = sFile.Split("|")

            sFile = ""

            For Each sFile In sFiles
                If sFile = "" Then Continue For

                Dim fileName As String = ExtractFileName(sFile)

                If fileName.IndexOf("*") > -1 Then
                    Dim path = GetDirectory(sFile)

                    For Each s As String In IO.Directory.GetFiles(path)
                        If ExtractFileName(s).ToLower Like fileName.ToLower Then
                            IO.File.Delete(s)
                        End If
                    Next

                Else
                    If sFile.Length > 0 Then System.IO.File.Delete(sFile)
                End If
            Next
        Else
            Dim oparse As New clsMarsParser
            sFile = oparse.ParseString(sFile)
            DoDeleteFiles(sFile, sRecursive)
        End If
    End Sub

    Private Sub CreateFolder(ByVal sPath As String)
        Dim parse As clsMarsParser = New clsMarsParser

        sPath = parse.ParseString(sPath)

        parse.ParseDirectory(sPath)

        'If System.IO.Directory.Exists(sPath) = False Then
        '    System.IO.Directory.CreateDirectory(sPath)
        'End If
    End Sub

    Private Sub MoveFolder(ByVal sOriginal As String, ByVal sDestination As String)
        Dim sDirName As String
        Dim nLevels As Integer
        Dim tools As New clsSystemTools
        Dim cmd1 As String
        Dim args As String
        Dim sRenameDestination As String
        Dim Temp() As String
        Dim I As Integer

        nLevels = sOriginal.Split("\").GetUpperBound(0)
        sDirName = sOriginal.Split("\")(nLevels - 1)


        cmd1 = "Call Ren "
        args = Chr(34) & clsMarsParser.Parser.ParseString(sOriginal) & Chr(34) & " " & Chr(34) & clsMarsParser.Parser.ParseString(sDestination) & Chr(34)

        'check if the source directory exists
        If System.IO.Directory.Exists(clsMarsParser.parser.ParseString(sOriginal)) = False Then
            _ErrorHandle("Invalid Source Folder Path", "-953842731", Reflection.MethodBase.GetCurrentMethod.Name, 887, "The directory you wish to move or rename does not exist or cannot be accessed.  Please correct this and run the task again.", True)
            Return
        End If

        'if this is a move then process
        If clsMarsParser.parser.ParseString(sDestination & sDirName).Contains("\") = True Then

            If CopyFolder.DoCopyFolder(clsMarsParser.parser.ParseString(sOriginal), clsMarsParser.parser.ParseString(sDestination & sDirName), True) = True Then
                tools.deleteContents(clsMarsParser.parser.ParseString(sOriginal))
                System.IO.Directory.Delete(clsMarsParser.parser.ParseString(sOriginal))
            End If
            'if this is a rename then process
        Else
            'check if the new name folder already exists
            I = clsMarsParser.parser.ParseString(sOriginal).Split("\").GetUpperBound(0)
            Temp = clsMarsParser.parser.ParseString(sOriginal).Split("\")
            For x As Integer = 0 To I - 2
                sRenameDestination = sRenameDestination & Temp(x) & "\"
            Next
            sRenameDestination &= clsMarsParser.parser.ParseString(sDestination)

            If System.IO.Directory.Exists(sRenameDestination) = True Then
                _ErrorHandle("Directory already exists", "-953842732", Reflection.MethodBase.GetCurrentMethod.Name, 906, "You cannot rename a folder using the same name as an already existing folder.  Please enter another name and try again.", True)
                Return
            End If
            'create batch file, execute it and delete it
            If System.IO.File.Exists(sAppPath & "\renfolder.bat") = True Then
                System.IO.File.Delete(sAppPath & "\renfolder.bat")
            End If

            SaveTextToFile(cmd1 & args, sAppPath & "\renfolder.bat", False, True)
            Shell(sAppPath & "\renfolder.bat", vbHide)
            _Delay(5)
            System.IO.File.Delete(sAppPath & "\renfolder.bat")
            'check if the rename was successful
            If System.IO.Directory.Exists(sRenameDestination) = False Then
                _ErrorHandle("Unable to rename directory", "-953842733", Reflection.MethodBase.GetCurrentMethod.Name, 906, "Please ensure that you have full security rights to the directory you wish to rename.", True)
                Return
            End If

        End If
    End Sub
    Private Sub PrintFiles(ByVal sFile As String, ByVal sPrinter As String, Optional ByVal Recursive As Boolean = False)
        Dim sTemp As String
        Dim oPrint As clsPrinters = New clsPrinters
        Dim sFiles() As String

        sTemp = oPrint.GetDefaultPrinter

        oPrint.SetDefaultPrinter(sPrinter)
        If sFile.Contains("|") = True Then
            sFiles = sFile.Split("|")


            sFile = String.Empty

            For Each sFile In sFiles
                If sFile.Length > 0 Then
                    Dim oProcess As New Process
                    oProcess.StartInfo.FileName = sFile
                    oProcess.StartInfo.Verb = "Print"
                    oProcess.StartInfo.CreateNoWindow = True
                    oProcess.Start()
                End If
            Next

            oPrint.SetDefaultPrinter(sTemp)
        Else
            Dim oparse As New clsMarsParser
            sFile = oparse.ParseString(sFile)
            DoPrintFiles(sFile, sPrinter, Recursive)
        End If
    End Sub
    Private Sub WriteText(ByVal strData As String, _
     ByVal FullPath As String, _
    ByVal nReplace As Int32)

        Dim oParse As New clsMarsParser
        Dim Replace As Boolean

        strData = oParse.ParseString(strData)

        FullPath = oParse.ParseString(FullPath)

        oParse.ParseDirectory(GetDirectory(FullPath))

        Try
            Replace = nReplace
            SaveTextToFile(strData, FullPath, , Not Replace, False)
        Catch
            SaveTextToFile(strData, FullPath, , False, False)
        End Try
    End Sub
    Private Sub FTPDeleteDirectory(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
ByVal sPassword As String, ByVal sDirectory As String, ByVal sKillDirectory As String, _
ByVal ftpType As String, ByVal Passive As Boolean, ByVal FtpOptions As String)
        Dim oFtp As Object

        sServer = clsMarsParser.Parser.ParseString(sServer)
        sUser = clsMarsParser.Parser.ParseString(sUser)
        sPassword = clsMarsParser.Parser.ParseString(sPassword)
        sDirectory = clsMarsParser.Parser.ParseString(sDirectory)

        Dim ucFtp As ucFTPDetails = New ucFTPDetails

        'set up the Ftp options for connection
        With ucFtp
            .cmbFTPType.Text = ftpType
            .txtFTPServer.Text = sServer
            .txtUserName.Text = sUser
            .txtPassword.Text = sPassword
            .txtPort.Value = nPort
            .chkPassive.Checked = Passive
            .m_ftpOptions = FtpOptions

            oFtp = .ConnectFTP
        End With

        If oFtp Is Nothing Then Return

        If TypeOf oFtp Is Chilkat.Ftp2 Then
            If oFtp.RemoveRemoteDir(sKillDirectory) = False Then
                Throw New Exception(oFtp.LastErrorText)
            End If
        Else
            Dim ftp As Rebex.Net.Sftp = oFtp

            ftp.RemoveDirectory(sKillDirectory)
        End If

        oFtp.Disconnect()
    End Sub
    Private Sub FTPCreateDirectory(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
 ByVal sPassword As String, ByVal sDirectory As String, ByVal sNewDirectory As String, _
 ByVal ftpType As String, ByVal Passive As Boolean, ByVal FtpOptions As String)
        Dim oFtp As Object 'Chilkat.Ftp2


        sServer = clsMarsParser.Parser.ParseString(sServer)
        sUser = clsMarsParser.Parser.ParseString(sUser)
        sPassword = clsMarsParser.Parser.ParseString(sPassword)
        sDirectory = clsMarsParser.Parser.ParseString(sDirectory)
        sNewDirectory = clsMarsParser.Parser.ParseString(sNewDirectory)

        Dim ucFtp As ucFTPDetails = New ucFTPDetails

        'set up the Ftp options for connection
        With ucFtp
            .cmbFTPType.Text = ftpType
            .txtFTPServer.Text = sServer
            .txtUserName.Text = sUser
            .txtPassword.Text = sPassword
            .txtPort.Value = nPort
            .chkPassive.Checked = Passive
            .m_ftpOptions = FtpOptions

            oFtp = .ConnectFTP
        End With

        If oFtp Is Nothing Then Return

        Try
            If TypeOf oFtp Is Chilkat.Ftp2 Then

                Dim ok As Boolean

                ok = oFtp.ChangeRemoteDir(sDirectory)

                If ok = False Then
                    If sDirectory.Length > 0 Then
                        For Each s As String In sDirectory.Split("/")
                            If s.Length > 0 Then

                                Try
                                    oFtp.ChangeRemoteDir(s)
                                Catch ex As Exception
                                    oFtp.CreateRemoteDir(s)
                                    _Delay(1)
                                    oFtp.ChangeRemoteDir(s)
                                End Try

                            End If
                        Next
                    End If
                End If

                If oFtp.CreateRemoteDir(sNewDirectory) = False Then
                    Throw New Exception(oFtp.LastErrorText)
                End If

                oFtp.Disconnect()
            ElseIf TypeOf oFtp Is Rebex.Net.Sftp Then
                Dim ftp As Rebex.Net.Sftp = oFtp 'copying the oftp object into a rebex ftp object so that we get intelisense

                Try
                    ftp.ChangeDirectory(sDirectory)
                Catch
                    If sDirectory.Length > 0 Then
                        For Each s As String In sDirectory.Split("/")
                            If s.Length > 0 Then

                                Try
                                    ftp.ChangeDirectory(s)
                                Catch ex As Exception
                                    ftp.CreateDirectory(s)
                                    _Delay(1)
                                    ftp.ChangeDirectory(s)
                                End Try

                            End If
                        Next
                    End If
                End Try

                ftp.CreateDirectory(sNewDirectory)

                ftp.Disconnect()
            End If

        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorSuggest = ""
            gErrorLine = 0
        End Try

    End Sub
    Private Sub FTPDeleteFile(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
    ByVal sPassword As String, ByVal sDirectory As String, ByVal sFile As String, _
    ByVal ftpType As String, ByVal Passive As Boolean, ByVal FtpOptions As String)
        Dim oFtp As Object


        Dim sMask As String
        Dim sPath As String

        Dim ucFtp As ucFTPDetails = New ucFTPDetails

        'set up the Ftp options for connection
        With ucFtp
            .cmbFTPType.Text = ftpType
            .txtFTPServer.Text = sServer
            .txtUserName.Text = sUser
            .txtPassword.Text = sPassword
            .txtPort.Value = nPort
            .chkPassive.Checked = Passive
            .m_ftpOptions = FtpOptions

            oFtp = .ConnectFTP
        End With

        Dim Temp() As String
        Dim I As Integer

        I = sDirectory.Split("/").GetUpperBound(0)

        Temp = sDirectory.Split("/")

        sMask = Temp(I)
        sPath = ""

        For x As Int32 = 0 To I - 1
            If Temp(x) <> "" Then sPath = sPath & "/" & Temp(x)
        Next

        sPath = sPath & "/"

        Try
            If oFtp Is Nothing Then Return

            If TypeOf oFtp Is Chilkat.Ftp2 Then
                If sPath.Length > 0 Then
                    Dim ok As Boolean

                    ok = oFtp.ChangeRemoteDir(sPath)

                    If ok = False Then
                        For Each s As String In sPath.Split("/")
                            If s <> "" Then

                                ok = oFtp.ChangeRemoteDir(s)

                                If ok = False Then
                                    If oFtp.CreateRemoteDir(s) = True Then
                                        _Delay(1)
                                        oFtp.ChangeRemoteDir(s)
                                    Else
                                        Throw New Exception(oFtp.LastErrorText)
                                    End If
                                End If

                            End If
                        Next
                    End If
                End If

                If sMask.Contains("*") = False And sMask.Contains("?") = False And sMask.Contains("#") = False Then
                    If oFtp.DeleteRemoteFile(sDirectory) = False Then
                        Throw New Exception(oFtp.LastErrorText)
                    End If
                Else
                    Dim ftp As Chilkat.Ftp2 = oFtp

                    If ftp.DeleteMatching(sMask) = False Then
                        Throw New Exception(oFtp.LastErrorText)
                    End If
                End If
            ElseIf TypeOf oFtp Is Rebex.Net.Sftp Then
                Dim ftp As Rebex.Net.Sftp = oFtp

                Try
                    ftp.ChangeDirectory(sPath)
                Catch ex As Exception
                    If sPath.Length > 0 Then
                        For Each s As String In sPath.Split("/")
                            If s <> "" Then
                                Try
                                    ftp.ChangeDirectory(s)
                                Catch
                                    ftp.CreateDirectory(s)
                                    _Delay(1)
                                    ftp.ChangeDirectory(s)
                                End Try
                            End If
                        Next
                    End If
                End Try

                If sMask.Contains("*") = False And sMask.Contains("?") = False And sMask.Contains("#") = False Then
                    ftp.DeleteFile(sDirectory)
                Else
                    For Each it As Rebex.Net.SftpItem In oFtp.GetList
                        If it.Name.ToLower Like sMask.ToLower Then ftp.DeleteFile(it.Name)
                    Next
                End If
            End If

            oFtp.Disconnect()
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorSuggest = ""
            gErrorLine = 0
        End Try
    End Sub
    Private Sub FTPDownload(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
     ByVal sPassword As String, ByVal sDirectory As String, ByVal sFile As String, _
     ByVal sLocalDir As String, ByVal Overwrite As Boolean, ByVal ftpType As String, _
     ByVal sRecursive As Boolean, ByVal sRFS As Boolean, ByVal Passive As Boolean, ByVal FtpOptions As String)
        Try
            Dim oFtp As Object

            sServer = clsMarsParser.Parser.ParseString(sServer)
            sUser = clsMarsParser.Parser.ParseString(sUser)
            sPassword = clsMarsParser.Parser.ParseString(sPassword)
            sDirectory = clsMarsParser.Parser.ParseString(sDirectory)
            sLocalDir = clsMarsParser.Parser.ParseString(sLocalDir)
            sFile = clsMarsParser.Parser.ParseString(sFile)

            Dim ucFtp As ucFTPDetails = New ucFTPDetails

            'set up the Ftp options for connection
            With ucFtp
                .cmbFTPType.Text = ftpType
                .txtFTPServer.Text = sServer
                .txtUserName.Text = sUser
                .txtPassword.Text = sPassword
                .txtPort.Value = nPort
                .chkPassive.Checked = Passive
                .m_ftpOptions = FtpOptions

                oFtp = .ConnectFTP
            End With

            If sLocalDir.EndsWith("\") = False Then
                sLocalDir = sLocalDir & "\"
            End If

            If sDirectory.EndsWith("/") = False Then
                sDirectory = sDirectory & "/"
            End If

            If System.IO.File.Exists(sLocalDir & sFile) = True Then
                If Overwrite = False Then
                    Return
                End If
            End If


            If oFtp Is Nothing Then Return

            clsMarsParser.Parser.ParseDirectory(sLocalDir)

            If TypeOf oFtp Is Chilkat.Ftp2 Then
                Dim ok As Boolean

                If sDirectory.Length > 0 Then

                    'try to change to the provided directory
                    ok = oFtp.ChangeRemoteDir(sDirectory)

                    'if it fails then loop through each folder and create the missing ones
                    If ok = False Then
                        For Each s As String In sDirectory.Split("/")
                            If s <> "" Then

                                ok = oFtp.ChangeRemoteDir(s)

                                If ok = False Then
                                    If oFtp.CreateRemoteDir(s) = True Then
                                        _Delay(1)
                                        oFtp.ChangeRemoteDir(s)
                                    Else
                                        Throw New Exception(oFtp.LastErrorText)
                                    End If
                                End If

                            End If
                        Next
                    End If
                End If

                If System.IO.File.Exists(sLocalDir & sFile) = True Then
                    If Overwrite = True Then
                        System.IO.File.Delete(sLocalDir & sFile)
                    End If
                End If

                If sFile.Contains("*") = False Then
                    If oFtp.GetFile(sFile, sLocalDir & sFile) = False Then
                        Throw New Exception(oFtp.LastErrorText)
                    End If
                Else
                    If oFtp.MGetFiles(sFile, sLocalDir) = False Then
                        Throw New Exception(oFtp.LastErrorText)
                    End If
                End If
            Else
                Dim ftp As Rebex.Net.Sftp = oFtp

                Try
                    ftp.ChangeDirectory(sDirectory)
                Catch
                    If sDirectory.Length > 0 Then
                        For Each s As String In sDirectory.Split("/")
                            If s <> "" Then
                                Try
                                    ftp.ChangeDirectory(s)
                                Catch
                                    ftp.CreateDirectory(s)
                                    _Delay(1)
                                    ftp.ChangeDirectory(s)
                                End Try
                            End If
                        Next
                    End If
                End Try

                If System.IO.File.Exists(sLocalDir & sFile) = True Then
                    If Overwrite = True Then
                        System.IO.File.Delete(sLocalDir & sFile)
                    End If
                End If

                If sFile.Contains("*") = False Then
                    ftp.GetFile(sDirectory & sFile, sLocalDir & sFile)
                Else
                    For Each item As Rebex.Net.SftpItem In ftp.GetList
                        If item.Name.ToLower Like sFile.ToLower Then ftp.GetFile(item.Name, sLocalDir & item.Name)
                    Next
                End If
            End If
            oFtp.Disconnect()
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorSuggest = ""
            gErrorLine = 0
        End Try
    End Sub

    Public Overloads Function FTPUpload2(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
           ByVal sPassword As String, ByVal sDirectory As String, _
           ByVal sFile As String, Optional ByVal FTPType As String = "FTP", Optional ByVal FtpOptions As String = "", _
           Optional ByVal sRecursive As Boolean = False, Optional ByVal sRFS As Boolean = False, _
           Optional ByVal Passive As Boolean = False)
10:     Try
            Dim sFiles() As String
            Dim oFTPThread As Threading.Thread
            Dim sCurrentPath As String
            Dim oFtp As Object

20:         sServer = clsMarsParser.Parser.ParseString(sServer)
30:         sUser = clsMarsParser.Parser.ParseString(sUser)
40:         sPassword = clsMarsParser.Parser.ParseString(sPassword)
50:         sDirectory = clsMarsParser.Parser.ParseString(sDirectory)
            'slocaldir = clsMarsParser.parser.ParseString(sLocalDir)
60:         sFile = clsMarsParser.Parser.ParseString(sFile)

70:         Dim ucFtp As ucFTPDetails = New ucFTPDetails

            If sServer.Contains(":") Then
                nPort = sServer.Split(":")(1)
                sServer = sServer.Split(":")(0)
            End If

            'set up the Ftp options for connection
80:         With ucFtp
90:             .cmbFTPType.Text = FTPType
100:            .txtFTPServer.Text = sServer
110:            .txtUserName.Text = sUser
120:            .txtPassword.Text = sPassword
130:            .txtPort.Value = nPort
140:            .chkPassive.Checked = Passive
150:            .m_ftpOptions = FtpOptions

160:            oFtp = .ConnectFTP
            End With

170:        If sDirectory.EndsWith("/") = False Then
180:            sDirectory = sDirectory & "/"
            End If


200:        sFiles = sFile.Split("|")

210:        If sDirectory.Length > 0 Then
220:            If TypeOf oFtp Is Chilkat.Ftp2 Then
                    Dim ok As Boolean

230:                ok = oFtp.ChangeRemoteDir(sDirectory)

240:                If ok = False Then
250:                    For Each s As String In sDirectory.Split("/")
260:                        If s.Length > 0 Then
270:                            ok = oFtp.ChangeRemoteDir(s)

280:                            If ok = False Then
290:                                If oFtp.CreateRemoteDir(s) = True Then
300:                                    _Delay(1)
310:                                    oFtp.ChangeRemoteDir(s)
320:                                Else
330:                                    Throw New Exception(oFtp.LastErrorText)
                                    End If
                                End If
                            End If
340:                    Next
                    End If
350:            Else
                    Dim ftp As Rebex.Net.Sftp = oFtp

360:                Try
370:                    ftp.ChangeDirectory(sDirectory)
380:                Catch
390:                    For Each s As String In sDirectory.Split("/")
400:                        If s.Length > 0 Then
410:                            Try
420:                                oFtp.ChangeDirectory(s)
430:                            Catch ex As Exception
440:                                oFtp.CreateDirectory(s)
450:                                _Delay(1)
460:                                oFtp.ChangeDirectory(s)
                                End Try

                            End If
470:                    Next
                    End Try
                End If
480:        End If

490:        Dim sTemp() As String

500:        sFile = String.Empty

510:        If oFtp Is Nothing Then Exit Function

520:        For Each sFile In sFiles
530:            Application.DoEvents()

540:            If sFile.Length > 0 Then
550:                If sFile.IndexOf("*") > -1 Then
560:                    Dim compareString As String = ExtractFileName(sFile)
570:
580:                    For Each x As String In IO.Directory.GetFiles(GetDirectory((sFile)))
590:                        If ExtractFileName(x) Like compareString Then

600:                            If TypeOf oFtp Is Chilkat.Ftp2 Then
                                    Dim ok As Boolean = oFtp.PutFile(x, ExtractFileName(x))

                                    If ok = False Then
                                        Throw New Exception(oFtp.LastErrorText)
                                    End If
610:                                'oFtp.AsyncPutFileStart(x, ExtractFileName(x))

620:                                'Do
630:                                'Application.DoEvents()
640:                                'Loop Until oFtp.AsyncFinished = True
650:                            Else
660:                                oFtp.PutFile(x, sDirectory & ExtractFileName(x))
                                End If
670:                        End If
680:                    Next
690:                Else
700:                    If TypeOf oFtp Is Chilkat.Ftp2 Then
                            Dim ok As Boolean = oFtp.PutFile(sFile, ExtractFileName(sFile))

                            If ok = False Then
                                Throw New Exception(oFtp.LastErrorText)
                            End If

                            '710:                        If oFtp.AsyncPutFileStart(sFile, ExtractFileName(sFile)) = False Then
                            '720:                            Throw New Exception(oFtp.LastErrorText)
                            '                            End If

                            '730:                        While oFtp.AsyncFinished <> True
                            '740:                            clsMarsUI.BusyProgress(50, oFtp.AsyncBytesSent & " bytes sent")

                            '750:                            oFtp.SleepMs(1000)
                            '                            End While
760:                    Else
770:                        oFtp.PutFile(sFile, sDirectory & ExtractFileName(sFile))
                        End If
780:                End If
790:            End If
800:        Next
            ' if advanced

            'End If
1250:       oFtp.Disconnect()

1260:       oUI.BusyProgress(, , True)

1270:       Return True
1280:   Catch ex As Exception
1290:       gErrorDesc = ex.Message
1300:       gErrorNumber = Err.Number
1310:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
1320:       gErrorLine = Erl()

            Throw ex
1330:       Return False
1340:   Finally
1350:       Try
1360:           oFtp.Disconnect()
            Catch : End Try
        End Try
    End Function
    Public Function FTPUpload(ByVal sServer As String, ByVal nPort As Int32, ByVal sUser As String, _
       ByVal sPassword As String, ByVal sDirectory As String, _
       ByVal sFile As String, Optional ByVal FTPType As String = "FTP", Optional ByVal sRecursive As Boolean = False, Optional ByVal sRFS As Boolean = False)
        Try
            Dim sFiles() As String
            Dim oFTPThread As Threading.Thread
            Dim sCurrentPath As String
            Dim ConnectType As Xceed.Ftp.AuthenticationMethod
            sServer = clsMarsParser.parser.ParseString(sServer)
            sUser = clsMarsParser.parser.ParseString(sUser)
            sPassword = clsMarsParser.parser.ParseString(sPassword)
            sDirectory = clsMarsParser.parser.ParseString(sDirectory)
            'slocaldir = clsMarsParser.parser.ParseString(sLocalDir)
            sFile = clsMarsParser.parser.ParseString(sFile)

10:

20:         Dim authType As Xceed.Ftp.AuthenticationMethod

30:         Select Case FTPType
                Case "FTP"
40:                 authType = Xceed.Ftp.AuthenticationMethod.None
50:             Case "FTP - SSL 3.1 (TLS)"
60:                 authType = Xceed.Ftp.AuthenticationMethod.Tls
70:             Case "FTP - SSL 3.0"
80:                 authType = Xceed.Ftp.AuthenticationMethod.Ssl
90:                 End Select

            If sDirectory.EndsWith("/") = False Then
                sDirectory = sDirectory & "/"
            End If


            If sFile.Contains("|") = True Then


                sFiles = sFile.Split("|")
100:            oFtp = ConnectFTPServer(sServer, nPort, sUser, sPassword, authType)
110:            If sDirectory.Length > 0 Then
120:                For Each s As String In sDirectory.Split("/")
130:                    If s.Length > 0 Then

140:                        Try
150:                            oFtp.ChangeCurrentFolder(s)
160:                        Catch ex As Exception
170:                            oFtp.CreateFolder(s)
180:                            _Delay(1)
190:                            oFtp.ChangeCurrentFolder(s)
200:                        End Try

210:                    End If
220:                Next
230:            End If


240:            Dim sTemp() As String

250:            sFile = String.Empty

260:            If oFtp Is Nothing Then Exit Function

270:            For Each sFile In sFiles
280:                Application.DoEvents()

290:                If sFile.Length > 0 Then
300:                    If sFile.IndexOf("*") > -1 Then
310:                        Dim compareString As String = ExtractFileName(sFile)
320:
330:                        For Each x As String In IO.Directory.GetFiles(GetDirectory((sFile)))
340:                            If ExtractFileName(x) Like compareString Then
350:                                ThreadFile = x

360:                                oFTPThread = New Threading.Thread(AddressOf Me.FTPUploadThread)
370:                                oFTPThread.Start()

380:                                Do
390:                                    Application.DoEvents()
400:                                Loop Until oFTPThread.IsAlive = False
410:                            End If
420:                        Next
430:                    Else
440:                        ThreadFile = sFile
450:                        oFTPThread = New Threading.Thread(AddressOf Me.FTPUploadThread)
460:                        oFTPThread.Start()

470:                        Do
480:                            Application.DoEvents()
490:                        Loop Until oFTPThread.IsAlive = False
500:                    End If
510:                End If
520:            Next
                oFtp.Disconnect()
            End If

            ' if advanced
            If sFile.Contains("|") = False Then
                Try
                    Dim oFtp As Xceed.Ftp.FtpClient
                    oFtp = ConnectFTPServer(sServer, nPort, sUser, sPassword, authType)

530:                If sDirectory.Length > 0 Then
540:                    For Each s As String In sDirectory.Split("/")
550:                        If s.Length > 0 Then

560:                            Try
570:                                oFtp.ChangeCurrentFolder(s)
580:                            Catch ex As Exception
590:                                oFtp.CreateFolder(s)
600:                                _Delay(1)
610:                                oFtp.ChangeCurrentFolder(s)
620:                            End Try

630:                        End If
640:                    Next
650:                End If
                    oFtp.Disconnect()
                    oFtp = ConnectFTPServer(sServer, nPort, sUser, sPassword, authType, sDirectory)
                    If oFtp Is Nothing Then Exit Function

                    'If sFile.Contains(" ") = True Then
                    '    oFtp.ReceiveFile(sFile, sLocalDir & sFile)
                    'Else
                    oFtp.SendMultipleFiles(sFile, sRecursive, sRFS)
                    'End If
                    oFtp.Disconnect()
                Catch ex As Exception
                    gErrorDesc = ex.Message
                    gErrorNumber = Err.Number
                    gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
                    gErrorSuggest = ""
                    gErrorLine = 0
                End Try
            End If

670:

680:        oUI.BusyProgress(, , True)

            Return True
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = Erl()
            Return False
        End Try
    End Function


    Public Sub FTPUploadThread()

        If oFtp IsNot Nothing Then
            oFtp.SendFile(ThreadFile)
        End If

    End Sub
    Private Function ConnectFTPServer(ByVal sServer As String, ByVal nPort As Integer, _
    ByVal sUser As String, ByVal sPassword As String, ByVal authType As Xceed.Ftp.AuthenticationMethod, _
    Optional ByVal sDirectory As String = "") As Xceed.Ftp.FtpClient
        Dim oFtp As New Xceed.Ftp.FtpClient
        Dim cert As Xceed.Ftp.Certificate

        If authType <> Xceed.Ftp.AuthenticationMethod.None Then
            oFtp.Connect(sServer, authType, Xceed.Ftp.VerificationFlags.None, Nothing)
        Else
            oFtp.Connect(sServer, nPort)
        End If

        oFtp.Login(sUser, sPassword)

        If sDirectory.EndsWith("/") Then sDirectory = sDirectory.Substring(0, sDirectory.Length - 1)

        If sDirectory IsNot Nothing Then
            If sDirectory.Length > 0 Then oFtp.ChangeCurrentFolder(sDirectory)
        End If

        Return oFtp

    End Function

    Public Function _ZipFiles(ByVal sOutput As String, ByVal sFileList As String, _
    Optional ByVal Encrypt As Boolean = False, Optional ByVal sCode As String = "") As String
        Dim oZip As New dzactxctrl
        Dim sList As String
        Dim I As Integer
        Dim sName As String

        If System.IO.File.Exists(sOutput) = True Then
            System.IO.File.Delete(sOutput)
        End If

        For Each s As String In sFileList.Split("|")
            If s.Length > 0 Then sList &= Chr(34) & s & Chr(34) & " "
        Next

        With oZip
            .QuietFlag = True
            .ItemList = sList
            .NoDirectoryEntriesFlag = True
            .NoDirectoryNamesFlag = True
            .DeleteOriginalFlag = False
            .ZIPFile = sOutput
            .CompressionFactor = DZACTXLib.CompFactor.COMPFACTOR_9

            If Encrypt = True Then
                .EncryptFlag = True
                .EncryptCode = sCode
            End If

            .ActionDZ = DZACTION.ZIP_ADD
        End With

        oZip = Nothing

        Return sOutput
    End Function

    Public Function _UnzipFiles(ByVal sZip As String, ByVal sDirectory As String)
        Dim duz1 As CDUnZipNET

        duz1 = New CDUnZipNET
        duz1.AllQuiet = True
        duz1.ZIPFile = sZip
        duz1.Filespec = "*.*"
        duz1.Destination = sDirectory
        duz1.RecurseFlag = True
        duz1.NoDirectoryItemsFlag = True
        duz1.ActionDZ = CDUnZipNET.DUZACTION.UNZIP_EXTRACT

    End Function

    Private Sub oFtp_FileTransferStatus(ByVal sender As Object, ByVal e As Xceed.Ftp.FileTransferStatusEventArgs) Handles oFtp.FileTransferStatus
        oUI.BusyProgress(e.AllBytesPercent(), "Uploading files..." & e.AllBytesPercent & "%")
    End Sub

    Public Function UnzipFiles(ByVal sZip As String, ByVal sDirectory As String)
        Dim duz1 As CDUnZipNET
        Dim oparse As New clsMarsParser
        sZip = oparse.ParseString(sZip)
        sDirectory = oparse.ParseString(sDirectory)

        duz1 = New CDUnZipNET

        Dim fileName As String = ExtractFileName(sZip)
        Dim dir As String = GetDirectory(sZip)

        If fileName.IndexOf("*") = -1 Then
            duz1.QuietFlag = True
            duz1.AllQuiet = True
            duz1.ZIPFile = sZip
            duz1.Filespec = "*.*"
            duz1.Destination = sDirectory
            duz1.RecurseFlag = True
            duz1.NoDirectoryItemsFlag = False
            duz1.NoDirectoryNamesFlag = False
            duz1.OverwriteFlag = True
            duz1.ActionDZ = CDUnZipNET.DUZACTION.UNZIP_EXTRACT

            If duz1.ErrorCode <> 0 Then
                Throw New Exception("Error unzipping file: " & duz1.ErrorCode.ToString)
            End If
        Else
            For Each file As String In IO.Directory.GetFiles(dir)
                If file.ToLower Like fileName.ToLower Then
                    duz1.AllQuiet = True
                    duz1.QuietFlag = True
                    duz1.ZIPFile = file
                    duz1.Filespec = "*.*"
                    duz1.Destination = sDirectory
                    duz1.RecurseFlag = True
                    duz1.NoDirectoryItemsFlag = False
                    duz1.NoDirectoryNamesFlag = False
                    duz1.OverwriteFlag = True
                    duz1.ActionDZ = CDUnZipNET.DUZACTION.UNZIP_EXTRACT

                    If duz1.ErrorCode <> 0 Then
                        Throw New Exception("Error unzipping file: " & duz1.ErrorCode.ToString)
                    End If
                End If
            Next
        End If

    End Function
    Public Function ZipFiles(ByVal sOutput As String, _
  ByVal sFileList As String, _
  Optional ByVal Recursive As Boolean = False, _
  Optional ByVal RFS As Boolean = False, _
  Optional ByVal Encrypt As Boolean = False, _
  Optional ByVal sCode As String = "") As String

        Dim oZip As New dzactxctrl
        Dim sList As String
        Dim I As Integer
        Dim sName As String

        If System.IO.File.Exists(sOutput) = True Then
            System.IO.File.Delete(sOutput)
        End If
        If sFileList.Contains("|") = True Then
            For Each s As String In sFileList.Split("|")
                Dim fileName As String = ExtractFileName(s)
                Dim dir As String = GetDirectory(s)

                If dir.EndsWith("\") = False Then dir &= "\"

                If fileName.IndexOf("*") = -1 Then
                    If s.Length > 0 Then sList &= Chr(34) & s & Chr(34) & " "
                Else
                    For Each file As String In IO.Directory.GetFiles(dir)
                        If file Like fileName Then
                            sList &= Chr(34) & file & Chr(34) & " "
                        End If
                    Next
                End If
            Next
            Dim oparse As New clsMarsParser
            sOutput = oparse.ParseString(sOutput)
            With oZip
                .QuietFlag = True
                .ItemList = sList
                .NoDirectoryEntriesFlag = True
                .NoDirectoryNamesFlag = True
                .DeleteOriginalFlag = False
                .ZIPFile = sOutput
                .CompressionFactor = DZACTXLib.CompFactor.COMPFACTOR_9

                If Encrypt = True Then
                    .EncryptFlag = True
                    .EncryptCode = sCode
                End If

                .ActionDZ = DZACTION.ZIP_ADD

                If .ErrorCode <> 0 Then
                    Throw New Exception("Error unzipping file: " & .ErrorCode.ToString)
                End If
            End With

            oZip = Nothing

            Return sOutput
        Else
            'DoZipFiles(sFileList, sOutput, Recursive, , , RFS)
            Dim dirEntries As Boolean
            Dim dirNames As Boolean
            Dim SubOptions As Long
            Dim oparse As New clsMarsParser
            sFileList = oparse.ParseString(sFileList)
            sOutput = oparse.ParseString(sOutput)

            If RFS = True Then
                dirEntries = True
                dirNames = False
                SubOptions = 1

            Else
                dirEntries = True
                dirNames = True
                SubOptions = 0
            End If

            With oZip
                .QuietFlag = True
                .ZipSubOptions = SubOptions
                .ItemList = sFileList
                .NoDirectoryEntriesFlag = dirEntries
                .NoDirectoryNamesFlag = dirNames
                .RecurseFlag = Recursive
                .DeleteOriginalFlag = False
                .ZIPFile = sOutput
                .CompressionFactor = DZACTXLib.CompFactor.COMPFACTOR_9

                If Encrypt = True Then
                    .EncryptFlag = True
                    .EncryptCode = sCode
                End If

                .ActionDZ = DZACTION.ZIP_ADD

                If .ErrorCode <> 0 Then
                    Throw New Exception("Error zipping file: " & .ErrorCode.ToString)
                End If
            End With

            oZip = Nothing
            Return sOutput
        End If
    End Function
    
End Class
