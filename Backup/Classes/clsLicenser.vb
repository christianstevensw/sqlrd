Public Class clsLicenser

    Public Shared m_licenser As New clsLicenser

    Public Function CheckLicense(ByVal license As String, ByVal firstName As String, ByVal lastName As String, _
    ByVal company As String, ByVal custID As String) As Boolean
        Try
            Dim codeInfo As String
            Dim startDate As String
            Dim custID1 As String

            license = license.Replace("-", "").Trim

            codeInfo = Me.GetLicenseInfo(license, firstName & lastName, company)

            startDate = codeInfo.Split(",")(1).Trim

            custID1 = codeInfo.Split(",")(0).Trim

            If custID1 <> custID Then
                Return False
            End If

            Dim actualLicense = Me.CreateLicense(firstName & lastName, company, custID, startDate)

            If actualLicense <> license Then
                Return False
            End If

            Return True
        Catch ex As Exception

            Return False
        End Try
    End Function
    Public Function CreateLicense(ByVal fullName As String, ByVal companyName As String, _
    ByVal custID As String, ByVal startDate As String) As String
        Try
            Dim sIn As String
            Dim license As String = ""
            Dim num As Int64

            sIn = (custID & startDate).Trim

            num = Convert.ToInt64(sIn)

            Dim factorise As String = fullName & companyName

            Dim nVowels As Integer = HowManyVowels(factorise)

            Dim factor As Integer = (factorise.Length - nVowels) - nVowels

            If factor < 0 Then
                factor = factor * -1
            ElseIf factor = 0 Then
                factor = 1
            End If

            num = num * factor

            license = Hex(num)

            Return license
        Catch ex As Exception
            Return "XXX"
        End Try

    End Function

    Public Function GetLicenseInfo(ByVal license As String, ByVal fullName As String, _
    ByVal companyName As String) As String 'List(Of String)
        Try
            Dim num As Int64
            Dim licenseInfo As String

            license = license.Replace("-", "")

            Dim factorise As String = fullName & companyName

            Dim nVowels As Integer = HowManyVowels(factorise)

            Dim factor As Integer = (factorise.Length - nVowels) - nVowels

            If factor < 0 Then
                factor = factor * -1
            ElseIf factor = 0 Then
                factor = 1
            End If


            num = Convert.ToInt64(license, 16)

            num = num / factor

            licenseInfo = Convert.ToString(num)

            Dim custid As String = licenseInfo.Substring(0, licenseInfo.Length - 8)
            Dim startDate As String = licenseInfo.Substring(custid.Length, 8)

            Return custid & "," & startDate
        Catch
            Return " , "
        End Try
    End Function
    Public Function HowManyVowels(ByVal sIn As String)
        Dim i As Integer = 0

        'Dim check() As String = New String() {"a", "e", "i", "o", "u"}

        'For Each s As String In sIn.ToLower
        '    If Array.IndexOf(check, s) > -1 Then
        '        i += 1
        '    End If
        'Next

        sIn = sIn.ToLower

        For Each s As String In sIn

            Dim ascValue As Integer = Asc(s)

            Dim byteVal As Byte() = System.Text.Encoding.Unicode.GetBytes(s)

            Dim newS As String = System.Text.Encoding.Unicode.GetString(byteVal)

            Select Case newS.ToLower
                Case "a", "e", "i", "o", "u"
                    i += 1
            End Select
        Next

        Return i
    End Function
End Class
