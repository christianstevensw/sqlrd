Imports C1.Win.C1Schedule

Public Class clsMarsExpose
    Public Shared globalItem As New clsMarsExpose

    Public Enum enCalcType As Integer
        MEDIAN = 0
        AVERAGE = 1
        MINIMUM = 2
        MAXIMUM = 3
    End Enum


    Public Sub LoadSchedules(ByVal scheduleView As C1.Win.C1Schedule.C1Schedule, ByVal calcType As enCalcType, _
    Optional ByVal excludeTypes() As String = Nothing)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim reportID, packID, autoID, eventPackID As Integer
        Dim I As Integer = 1
        Try
10:         SQL = "SELECT * FROM scheduleAttr WHERE Status =1 AND Frequency <> 'None'"

20:         If excludeTypes IsNot Nothing Then
30:             For Each s As String In excludeTypes
40:                 Select Case s
                        Case "Report"
50:                         SQL &= " AND (ReportID  = 0 OR ReportID IN (SELECT ReportID FROM ReportAttr WHERE (Dynamic = 1 OR Bursting = 1 OR IsDataDriven = 1)))"
60:                     Case "Dynamic"
70:                         SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE Dynamic =1))"
80:                     Case "Bursting"
90:                         SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE Bursting =1))"
100:                    Case "Data-Driven"
110:                        SQL &= " AND (ReportID  = 0 OR ReportID NOT IN (SELECT ReportID FROM ReportAttr WHERE IsDataDriven =1))"
120:                    Case "Package"
130:                        SQL &= " AND (PackID = 0 OR PackID IN (SELECT PackID FROM PackageAttr WHERE Dynamic = 1))"
140:                    Case "Dynamic Package"
150:                        SQL &= " AND (PackID = 0 OR PackID NOT IN (SELECT PackID FROM PackageAttr WHERE Dynamic = 1))"
160:                    Case "Automation"
170:                        SQL &= " AND AutoID = 0"
180:                    Case "EventPackage"
190:                        SQL &= " AND (EventPackID = 0 OR EventPackID IS NULL)"
                    End Select
200:            Next
            End If

210:        scheduleView.DataStorage.AppointmentStorage.Appointments.Clear()

220:        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

230:        clsMarsUI.MainUI.BusyProgress(50, "Populating schedules...")

240:        scheduleView.BeginUpdate()

250:        Do While oRs.EOF = False
                'clsMarsUI.MainUI.BusyProgress((I / oRs.RecordCount) * 100, "Loading schedules into view")

                Dim apt As C1.C1Schedule.Appointment
                Dim startTime As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
                Dim scheduleName As String = clsMarsScheduler.globalItem.GetScheduleName(oRs("scheduleid").Value)
                Dim frequency As String = oRs("frequency").Value
260:            reportID = IsNull(oRs("reportid").Value, 0)
270:            packID = IsNull(oRs("packid").Value, 0)
280:            autoID = IsNull(oRs("autoid").Value, 0)
290:            eventPackID = IsNull(oRs("eventpackid").Value, 0)

                Dim scheduleDuration As Double = clsMarsScheduler.globalItem.getScheduleDuration(reportID, packID, _
                autoID, eventPackID, 0, clsMarsScheduler.enCalcType.MEDIAN)

300:            scheduleDuration = Math.Round(scheduleDuration, 2)

                Dim endTime As Date = startTime.AddMinutes(scheduleDuration)

310:            apt = New C1.C1Schedule.Appointment()
320:            apt.Start = startTime
330:            apt.Duration = System.TimeSpan.FromMinutes(scheduleDuration)
340:            apt.ReminderSet = False
                apt.Subject = scheduleName & " [Duration: " & scheduleDuration & " minutes]"
350:            apt.Body = apt.Subject

360:            Dim lbl As C1.C1Schedule.Label = New C1.C1Schedule.Label

370:            If reportID > 0 Then
380:                If clsMarsData.IsScheduleBursting(reportID) = True Then
390:                    lbl.Color = Color.Gainsboro
400:                ElseIf clsMarsData.IsScheduleDynamic(reportID, "report") Then
410:                    lbl.Color = Color.MediumOrchid
420:                ElseIf clsMarsData.IsScheduleDataDriven(reportID) Then
430:                    lbl.Color = Color.LightSkyBlue
440:                Else
450:                    lbl.Color = Color.NavajoWhite
                    End If

460:                apt.Tag = reportID
470:                lbl.Text = "Report"
480:            ElseIf packID > 0 Then
490:                If clsMarsData.IsScheduleDynamic(packID, "package") Then
500:                    lbl.Color = Color.Goldenrod
510:                Else
520:                    lbl.Color = Color.PaleGreen
                    End If

530:                apt.Tag = packID
540:                lbl.Text = "Package"
550:            ElseIf autoID > 0 Then
560:                apt.Tag = autoID
570:                lbl.Color = Color.LightSteelBlue
580:                lbl.Text = "Automation"
590:            ElseIf eventPackID > 0 Then
600:                apt.Tag = eventPackID
610:                lbl.Color = Color.Yellow
620:                lbl.Text = "EventPackage"
                End If

630:            apt.Label = lbl

640:            scheduleView.DataStorage.AppointmentStorage.Appointments.Add(apt)

650:            oRs.MoveNext()
660:            I += 1
670:        Loop

680:        oRs.Close()

690:        scheduleView.EndUpdate()

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
700:        clsMarsUI.MainUI.BusyProgress(0, , True)
        End Try
    End Sub

End Class
