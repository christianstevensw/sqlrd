Imports System.IO
Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Security.Cryptography
Public Class clsSystemTools
    Dim oData As New clsMarsData
    Dim ofd As New FolderBrowserDialog

    Public Shared Function CompareFiles(ByVal FileFullPath1 As String, _
    ByVal FileFullPath2 As String) As Boolean

        'returns true if two files passed to is are identical, false
        'otherwise

        'does byte comparison; works for both text and binary files

        'Throws exception on errors; you can change to just return 
        'false if you prefer


        Dim objMD5 As New MD5CryptoServiceProvider()
        Dim objEncoding As New System.Text.ASCIIEncoding()

        Dim aFile1() As Byte, aFile2() As Byte
        Dim strContents1, strContents2 As String
        Dim objReader As StreamReader
        Dim objFS As FileStream
        Dim bAns As Boolean
        If Not File.Exists(FileFullPath1) Then _
            Throw New Exception(FileFullPath1 & " doesn't exist")
        If Not File.Exists(FileFullPath2) Then _
         Throw New Exception(FileFullPath2 & " doesn't exist")

        Try

            objFS = New FileStream(FileFullPath1, FileMode.Open)
            objReader = New StreamReader(objFS)
            aFile1 = objEncoding.GetBytes(objReader.ReadToEnd)
            strContents1 = _
              objEncoding.GetString(objMD5.ComputeHash(aFile1))
            objReader.Close()
            objFS.Close()


            objFS = New FileStream(FileFullPath2, FileMode.Open)
            objReader = New StreamReader(objFS)
            aFile2 = objEncoding.GetBytes(objReader.ReadToEnd)
            strContents2 = _
             objEncoding.GetString(objMD5.ComputeHash(aFile2))

            bAns = strContents1 = strContents2
            objReader.Close()
            objFS.Close()
            aFile1 = Nothing
            aFile2 = Nothing

        Catch ex As Exception
            Return True
        End Try

        Return bAns
    End Function
    Public Enum configSaveTime As Integer
        onOpen = 0
        onClose = 1
    End Enum
    Public Shared Sub BackConfigFile(ByVal saveTime As configSaveTime)
        Try
            Dim backupDir As String = sAppPath & "configRestore\"

            If IO.Directory.Exists(backupDir) = False Then
                IO.Directory.CreateDirectory(backupDir)
            End If

            Dim backupName As String

            If saveTime = configSaveTime.onOpen Then
                backupName = "config_onopen.bak"
            Else
                backupName = "config_onclose.bak"
            End If

            Try : IO.File.Delete(backupDir & backupName) : Catch : End Try

            IO.File.Copy(sAppPath & "sqlrdlive.config", backupDir & backupName, True)
        Catch : End Try
    End Sub

    Public Shared Sub RestoreConfigFile(ByVal saveTime As configSaveTime)
        Try
            Dim backupDir As String = sAppPath & "configRestore\"

            If IO.Directory.Exists(backupDir) = False Then Return

            Dim backupName As String

            If saveTime = configSaveTime.onOpen Then
                backupName = "config_onopen.bak"
            Else
                backupName = "config_onclose.bak"
            End If

            If IO.File.Exists(backupDir & backupName) = False Then Return

            IO.File.Delete(sAppPath & "sqlrdlive.config")

            IO.File.Copy(backupDir & backupName, sAppPath & "sqlrdlive.config", True)

        Catch : End Try
    End Sub
    Public Sub _RefreshAll()
        Dim oReport As New clsMarsReport
        Dim SQL As String

        SQL = "SELECT ReportID FROM ReportAttr"

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        Do While oRs.EOF = False
            oReport.RefreshReport(oRs.Fields(0).Value)
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub
    Public Sub _DeactivateSystem(ByVal ShowPrompt As Boolean)
        Dim oRes As DialogResult
        Dim oSvc As New clsServiceController

        If ShowPrompt = True Then
            Dim sMsg As String

            sMsg = "Deactivation means you will no longer be able to use SQL-RD on this machine. Are you sure you want to Deactivate SQL-RD?"
            oRes = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        Else
            oRes = DialogResult.Yes
        End If

        If oRes = DialogResult.Yes Then
            'stop the scheduler
            oSvc.StopScheduler()

            'start the wizard
            Dim oProc As New Process

            With oProc
                With .StartInfo
                    .FileName = sAppPath & "link.exe"
                    .Arguments = "unregwiz"
                End With

                .Start()
            End With

            For Each o As Process In Process.GetProcessesByName("sqlrd")
                o.Kill()
            Next

            End
        Else
            Return
        End If

    End Sub
    Public Sub _ActivateSystem(ByVal ShowPrompt As Boolean, ByVal ExitAfter As Boolean)
        Dim oRes As DialogResult
        Dim oSvc As New clsServiceController

        If ShowPrompt = True Then
            oRes = MessageBox.Show("SQL-RD will now close all sessions and the scheduling services in order to complete this process.", Application.ProductName, MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        Else
            oRes = DialogResult.OK
        End If

        If oRes = DialogResult.OK Then
            'stop the scheduler
            oSvc.StopScheduler()

            'start the wizard
            Dim oProc As New Process

            With oProc
                With .StartInfo
                    .FileName = sAppPath & "link.exe"
                    .Arguments = "regwiz"
                End With

                .Start()
            End With

            If ExitAfter = True Then
                For Each o As Process In Process.GetProcessesByName("sqlrd")
                    o.Kill()
                Next
            End If

            End
        Else
            Return
        End If

    End Sub
    Public Sub _BackupSystem(Optional ByVal sLoc As String = "", Optional ByVal ShowMsg As Boolean = True, Optional ByVal UseDefDirName As Boolean = True)
10:     Try
20:         If clsMarsSecurity._HasGroupAccess("Backup & Restore") = False Then
30:             Return
            End If

40:         Dim oParse As New clsMarsParser
41:         Dim oUI As New clsMarsUI


50:         If gConType <> "DAT" Then
60:             Dim oSQL As New frmSQLAdmin

70:             'clsMigration.MigrateSystem(clsMigration.m_datConString, "", "")
            End If

80:         If sLoc = "" And RunEditor = True Then
90:             With ofd
100:                .Description = "Please select the folder to backup to"
110:                .ShowNewFolderButton = True
120:                .ShowDialog()
                End With

130:            sLoc = ofd.SelectedPath
            End If

140:        If sLoc = "" Or sLoc Is Nothing Then Exit Sub

150:        If sLoc.Substring(sLoc.Length - 1, 1) <> "\" Then
160:            sLoc += "\"
            End If

170:        sLoc = _CreateUNC(sLoc)

180:        oParse.ParseDirectory(sLoc)

190:        If sLoc.EndsWith("\") = False Then sLoc &= "\"
200:        If UseDefDirName = True Then

210:            sLoc &= "Backup" & Now.ToString("yyyyMMddHHmm") & "\"

            End If

220:        System.IO.Directory.CreateDirectory(sLoc)
230:        System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sLoc & "sqlrdlive.dat", True)

240:        If IO.File.Exists(sAppPath & "sqlrdlive.config") Then
250:            System.IO.File.Copy(sAppPath & "sqlrdlive.config", sLoc & "sqlrdlive.config", True)
            End If

260:        '  If System.IO.Directory.Exists(sLoc & "Cache") = False Then
270:        ' System.IO.Directory.CreateDirectory(sLoc & "Cache")
            'End If

271:        If IO.File.Exists(sAppPath & "eventlog.dat") Then
272:            IO.File.Copy(sAppPath & "eventlog.dat", sLoc & "eventlog.dat", True)
273:        End If

280:        If gConType <> "DAT" Then

                Dim srcDSN As String
                Dim conString As String
                Dim dsn As String
                Dim user As String
                Dim pwd As String

281:            conString = oUI.ReadRegistry("ConString", "", True)
                If conString.Length > 0 Then
                    dsn = conString.Split(";")(4).Split("=")(1)
                    user = conString.Split(";")(3).Split("=")(1)
                    pwd = conString.Split(";")(1).Split("=")(1)
                    srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
                End If

                Dim dm As New DataMigrator
282:            Dim ok As Boolean = dm.Export(srcDSN, sAppPath & "sqlrdlive.def")

                If ok = False Then
                    Throw New Exception("Could not create def file during backup")
                End If

283:            System.IO.File.Copy(sAppPath & "sqlrdlive.def", sLoc & "sqlrdlive.def", True)
            End If


            'copy all the files to the new directory
            Dim sFile As String

            '280:        For Each sFile In System.IO.Directory.GetFiles(clsMarsReport.m_CacheFolder)
            '                Dim sFileName As String = ExtractFileName(sFile)
            '290:            System.IO.File.Copy(sFile, sLoc & "Cache\" & sFileName, True)
            '300:        Next

310:        If IO.Directory.Exists(clsMarsEvent.m_CachedDataPath) And clsMarsEvent.m_CachedDataPath.Length > 1 Then

320:            If System.IO.Directory.Exists(sLoc & "Cached Data") = False Then
330:                IO.Directory.CreateDirectory(sLoc & "Cached Data")
                End If

                'copy all the folder and subitems
340:            For Each sFolder As String In IO.Directory.GetDirectories(clsMarsEvent.m_CachedDataPath)
                    Dim sFolderName As String = sFolder.Split("\")(sFolder.Split("\").GetUpperBound(0))
                    Dim sNewFolder As String = sLoc & "Cached Data\" & sFolderName

350:                If IO.Directory.Exists(sNewFolder) = False Then
360:                    IO.Directory.CreateDirectory(sNewFolder)
                    End If

370:                For Each sFile In IO.Directory.GetFiles(sFolder)
380:                    IO.File.Copy(sFile, sNewFolder & "\" & ExtractFileName(sFile), True)
390:                Next
400:            Next
            End If

420:        oUI.ExportRegistry(sKey, sLoc & "sqlrd.reg")

430:        If ShowMsg = True Then MessageBox.Show("System backed up successfully", Application.ProductName, _
       MessageBoxButtons.OK, MessageBoxIcon.Information)
440:    Catch ex As Exception
450:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Public Sub _RestoreSystem()
        Dim sPath As String
        Dim sFile As String
        Dim Fol
        Dim X As Integer
        Dim I As Integer

        If clsMarsSecurity._HasGroupAccess("Backup & Restore") = False Then
            Return
        End If

        With ofd
            .Description = "Please select the folder containing the backup files"
            .ShowNewFolderButton = False
            .ShowDialog()
        End With

        sPath = ofd.SelectedPath

        If sPath.Length = 0 Then Exit Sub

        If System.IO.File.Exists(sPath & "\sqlrdlive.dat") = False Then
            MessageBox.Show("Cannot find sqlrdlive.dat.This back up is invalid or corrupt.  Please try restoring from another backup", _
            Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Try
            If MessageBox.Show("This will overwrite the existing system files and cached report files. Continue to restore?", _
            Application.ProductName, MessageBoxButtons.YesNo, _
             MessageBoxIcon.Question) = DialogResult.Yes Then
                AppStatus(True)
                Dim oData As New clsMarsData

                oData.CloseMainDataConnection()

                System.IO.File.Copy(sPath & "\sqlrdlive.dat", sAppPath & "\sqlrdlive.dat", True)

                If IO.File.Exists(sPath & "sqlrdlive.config") Then
                    System.IO.File.Copy(sPath & "sqlrdlive.config", sAppPath & "sqlrdlive.config", True)
                End If


                If IO.Directory.Exists(sPath & "\Cached Data") Then
                    Dim sDatacache As String = clsMarsEvent.m_CachedDataPath

                    If IO.Directory.Exists(sDatacache) = True Then
                        IO.Directory.Delete(sDatacache, True)
                    End If

                    IO.Directory.CreateDirectory(sDatacache)

                    For Each s As String In IO.Directory.GetDirectories(sPath & "\Cached Data")
                        Dim sFolder As String = s.Split("\")(s.Split("\").GetUpperBound(0))

                        IO.Directory.CreateDirectory(sDatacache & sFolder)

                        For Each f As String In IO.Directory.GetFiles(s)
                            IO.File.Copy(f, sDatacache & sFolder & "\" & ExtractFileName(f))
                        Next
                    Next
                End If

            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please make sure that the scheduler has been stopped")
            Return
        End Try

        If IO.File.Exists(sPath & "\sqlrd.reg") = True Then
            Process.Start(sPath & "\sqlrd.reg")
        End If

        'copy to main database
        If gConType <> "DAT" Then
            MessageBox.Show("SQL-RD does not transfer from the dat file to your ODBC/SQL database. You should restore your database server backup instead.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            'Dim oSQL As New frmSQLAdmin
            'Dim sConn As String
            'Dim oUI As New clsMarsUI

            'sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Application.StartupPath & "\sqlrdlive.dat" & ";Persist Security Info=False"

            'oData.OpenMainDataConnection()

            'sConn = oUI.ReadRegistry("ConString", "", True)

            'If clsMigration.MigrateSystem(clsMigration.m_datConString, "", "") = True Then
            '    MessageBox.Show("Restore completed. SQL-RD will now exit so that the changes can be applied", _
            '                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Process.Start(Application.ExecutablePath)

            End
            'End If
        Else

            Process.Start(Application.ExecutablePath)

            End
        End If

    End Sub

    Public Sub _CompactSystem()

        If clsMarsSecurity._HasGroupAccess("Compact System") = False Then
            Return
        End If

        Dim oRes As DialogResult

        oRes = MessageBox.Show("To obtain optimum results, SQL-RD will shutdown the background scheduling applications. Proceed?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

        If oRes = DialogResult.No Then Exit Sub

        'get the service type
        Dim sType As String
        Dim oUI As New clsMarsUI
        Dim sProvider As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source="

        sType = oUI.ReadRegistry("SQL-RDService", "NONE")

        If sType = "WindowsApp" Then
            For Each o As Process In Process.GetProcesses
                If o.ProcessName = "sqlrdapp" Then
                    o.Kill()
                    Exit For
                End If
            Next
        ElseIf sType = "WindowsNT" Then
            Try
                Dim oSrv As New ServiceController("SQL-RD")

                If oSrv.Status = ServiceControllerStatus.Running Then oSrv.Stop()
            Catch : End Try
        End If

        Try
            If gConType <> "DAT" Then
                'Dim oSQL As New frmSQLAdmin

                'If oSQL._ReturntoDat(False) = False Then
                '    Return
                'End If

                Return
            End If

            Dim oDB As New JRO.JetEngine
            Dim oData As New clsMarsData

            oUI.BusyProgress(10, "Closing data connection...")
            oData.CloseMainDataConnection()

            AppStatus(True)

            Dim nTime As Date
            nTime = Now.AddSeconds(60)

            Do
                Application.DoEvents()
            Loop Until IO.File.Exists(sAppPath & "\sqlrdlive.ldb") = False Or nTime < Now

            oUI.BusyProgress(25, "Validating file system...")

            If System.IO.File.Exists(sAppPath & "sqlrdlive.bak") Then
                System.IO.File.Delete(sAppPath & "sqlrdlive.bak")
            End If

            If System.IO.File.Exists(sAppPath & "temp.tmp") Then
                System.IO.File.Delete(sAppPath & "temp.tmp")
            End If

            If System.IO.File.Exists(sAppPath & "temp1.tmp") Then
                System.IO.File.Delete(sAppPath & "temp1.tmp")
            End If

            oUI.BusyProgress(50, "Creating backup of system...")
            'first create a copy of the current database
            System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sAppPath & "sqlrdlive.bak")

            'create a copy of marslive.mdb for compacting
            oUI.BusyProgress(60, "Creating working file...")
            System.IO.File.Copy(sAppPath & "sqlrdlive.dat", sAppPath & "temp.tmp")

            'compact the file to temp1.mdb
            oUI.BusyProgress(80, "Compacting system database...")

            oDB.CompactDatabase(sProvider & sAppPath & "temp.tmp", sProvider & sAppPath & "temp1.tmp")

            'delete the older file
            oUI.BusyProgress(90, "Cleaning up...")
            System.IO.File.Delete(sAppPath & "temp.tmp")

            'delete the old marslive
            System.IO.File.Delete(sAppPath & "sqlrdlive.dat")

            'create a new marslive.mdb
            System.IO.File.Move(sAppPath & "temp1.tmp", sAppPath & "sqlrdlive.dat")

            oUI.BusyProgress(95, "Restoring data connection...")

            'oData._OpenMainDataConnection()

            oUI.BusyProgress(, , True)

            MessageBox.Show("System compacted and repaired successfully! SQL-RD will now exit so that the update can be applied", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            'restart services
            If sType = "WindowsApp" Then
                Try
                    Process.Start(sAppPath & "sqlrdapp.exe")
                Catch : End Try
            ElseIf sType = "WindowsNT" Then
                Try
                    Dim oSrv As New ServiceController("SQL-RD")

                    If oSrv.Status = ServiceControllerStatus.Stopped Then oSrv.Start()
                Catch : End Try
            End If

            Process.Start(Application.ExecutablePath)

            End
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please make sure that the background scheduler or the " & _
            "NT Service have been stopped. If this does not solve the problem then please restart your pc.")

            oData.OpenMainDataConnection()
        End Try
    End Sub
    Public Sub _CheckSysSize()
        Dim oUI As New clsMarsUI

        If gConType <> "DAT" Then Return

        If Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotAutoCompact", 0))) = True Then
            Return
        End If

        Dim oFil As New IO.FileInfo(sAppPath & "sqlrdlive.dat")

        Dim nSize As Double = oFil.Length()

        nSize /= 1000000

        If nSize > 50 Then
            If MessageBox.Show("The system database is now more than 50MB in size, would like to compact it now?", _
            Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                gRole = "administrator"
                _CompactSystem()
            End If
        End If
    End Sub

    Public Sub _CheckSysIntegrity()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim SysValid As Boolean
        Dim Value

        If gConType <> "DAT" Then Return

        SQL = "SELECT TOP 1 Status FROM ScheduleAttr "

        oRs = clsmarsdata.GetData(SQL)

        Try
            If oRs.EOF = False Then
                Value = oRs.Fields(0).Value
            End If

            oRs.Close()
            SysValid = True
        Catch ex As Exception
            SysValid = False
        End Try

        oRs = Nothing

        If SysValid = False Then
            If MessageBox.Show("The SQL-RD system files have become fragmented and this might be affecting system stability." & vbCrLf & _
            "Would you like to compact and defragment the system file now?", Application.ProductName, _
            MessageBoxButtons.YesNo) = DialogResult.Yes Then
                _CompactSystem()
            End If
        End If
    End Sub
    Public Function _HouseKeeping(Optional ByVal DestinationID As Integer = 0, _
    Optional ByVal sPath As String = "", Optional ByVal sTitle As String = "")

        Dim sValue As Integer
        Dim sUnit As String
        Dim Interval As Long
        Dim I As Integer
        Dim oUI As New clsMarsUI
        Dim sFile As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim LastMod As Date

        Try
            If DestinationID = 0 Then

                SQL = "SELECT * FROM HouseKeepingPaths"

                oRs = clsMarsData.GetData(SQL)

                Do While oRs.EOF = False
                    sValue = oRs("keepfor").Value
                    sUnit = oRs("keepunit").Value
                    sPath = oRs("folderpath").Value

                    Dim oParse As New clsMarsParser

                    sPath = oParse.ParseString(sPath)

                    Select Case sUnit
                        Case "Minutes"
                            sUnit = "n"
                        Case "Hours"
                            sUnit = "n"
                            sValue = sValue * 60
                        Case "Days"
                            sUnit = "d"
                        Case "Weeks"
                            sUnit = "ww"
                        Case "Months"
                            sUnit = "m"
                    End Select

                    For Each s As String In System.IO.Directory.GetFiles(sPath)

                        LastMod = System.IO.File.GetLastWriteTime(s)

                        Interval = DateDiff(sUnit, LastMod, Date.Now)

                        If Interval > sValue Then
                            System.IO.File.Delete(s)
                        End If
                    Next

                    For Each s As String In System.IO.Directory.GetDirectories(sPath)

                        LastMod = System.IO.Directory.GetLastWriteTime(s)

                        Interval = DateDiff(sUnit, LastMod, Date.Now)

                        If Interval > sValue Then
                            System.IO.Directory.Delete(s, True)
                        End If
                    Next

                    oRs.MoveNext()
                Loop

                oRs.Close()

            Else
                SQL = "SELECT * FROM HouseKeepingAttr WHERE DestinationID =" & DestinationID

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    sValue = oRs("agevalue").Value
                    sUnit = oRs("ageunit").Value
                Else
                    oRs.Close()
                    Return Nothing
                End If
            End If

            Select Case sUnit
                Case "Minutes"
                    sUnit = "n"
                Case "Hours"
                    sUnit = "n"
                    sValue = sValue * 60
                Case "Days"
                    sUnit = "d"
                Case "Weeks"
                    sUnit = "ww"
                Case "Months"
                    sUnit = "m"
            End Select


            For Each s As String In System.IO.Directory.GetFiles(sPath)

                LastMod = System.IO.File.GetLastWriteTime(s)

                Interval = DateDiff(sUnit, LastMod, Date.Now)

                If Interval > sValue Then
                    System.IO.File.Delete(s)
                End If
            Next

            For Each s As String In System.IO.Directory.GetDirectories(sPath)

                LastMod = System.IO.Directory.GetLastWriteTime(s)

                Interval = DateDiff(sUnit, LastMod, Date.Now)

                If Interval > sValue Then
                    System.IO.Directory.Delete(s, True)
                End If
            Next

            'for process tracker files
            Try
                Dim alivePath As String = Application.StartupPath & "\processAlive\"

                If IO.Directory.Exists(alivePath) = True Then
                    For Each s As String In alivePath
                        Dim procID As String = IO.Path.GetFileName(s)

                        Try
                            Dim p As Process = Process.GetProcessById(procID)
                        Catch ex As Exception
                            Try
                                IO.File.Delete(s)
                            Catch : End Try
                        End Try
                    Next
                End If
            Catch : End Try

        Catch :End Try

    End Function
    Public Function _CreateSysInfoFile(ByVal sPath As String) As Boolean
        Dim sOut As String
        Dim oFile As FileVersionInfo

        Try
            oFile = FileVersionInfo.GetVersionInfo(sAppPath & assemblyName)

            sOut = "Product Name: " & Application.ProductName & vbCrLf & _
            "Version: " & oFile.FileMajorPart & "." & oFile.FileMinorPart & "." & _
            oFile.FileBuildPart & "." & oFile.FilePrivatePart & vbCrLf & _
            "Build: " & oFile.Comments & vbCrLf & _
            "File Type: " & gConType

            SaveTextToFile(sOut, sPath, , False)

            Return True
        Catch
            Return False
        End Try
    End Function
    Public Function deleteContents(ByVal folderName As String) As Boolean
        For Each s As String In IO.Directory.GetFiles(folderName)
            IO.File.Delete(s)
        Next


        For Each s As String In IO.Directory.GetDirectories(folderName)
            deleteContents(s)


            IO.Directory.Delete(s)
        Next




    End Function

    Public Sub checkforUpdates(ByVal checkUrl As String, Optional ByVal ManualCheck As Boolean = False)
        Try
            Dim check As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("CheckForUpdates", 1))

            If check = False And ManualCheck = False Then Return

            Dim wb As WebBrowser = New WebBrowser

            wb.Navigate(checkUrl)

            Dim timeNow As Date = Now

            Do
                Application.DoEvents()
            Loop Until Now.Subtract(timeNow).TotalSeconds > 5

            Dim build

            Try
                build = wb.Document.Body.InnerHtml.Trim
            Catch : End Try

            If build Is Nothing Then build = "19770227"

            For Each s As String In build
                If IsNumeric(s) = False Then
                    build = build.Replace(s, "")
                End If
            Next

            Dim current As String = GetCurrentBuild()
            Dim skipBuild As String = clsMarsUI.MainUI.ReadRegistry("SkipUpdateForBuild", "19770227")

            If build = skipBuild And ManualCheck = False Then Return

            If (ToDate(current) < ToDate(build)) Then

                Dim Msg As frmUpdateAvail = New frmUpdateAvail

                Dim oRes As DialogResult = Msg.DoshowDialog(build, current)

                If oRes = DialogResult.Yes Then

                    Dim proc As Process = New Process

                    With proc
                        .StartInfo.FileName = "iexplore"
                        .StartInfo.Arguments = "http://download.christiansteven.com/sql-rd/SQL-RDSetup.exe"
                        .Start()
                    End With
                ElseIf oRes = DialogResult.Ignore Then
                    clsMarsUI.MainUI.SaveRegistry("SkipUpdateForBuild", build, , , True)
                End If
            Else
                If ManualCheck Then
                    MessageBox.Show("No updates are available at this time.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        Catch ex As Exception
            If ManualCheck Then _ErrorHandle("There was an error checking for new updates. The error was: " & ex.Message, Err.Number, _
            Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please make sure you have an active internet connection", True)
        End Try
    End Sub

    Private Function GetCurrentBuild() As String
        Dim sBuild As String
        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(Application.ExecutablePath)

        sBuild = oFile.Comments

        sBuild = sBuild.Split("/")(0).ToLower.Replace("build", String.Empty).Trim

        Return sBuild
    End Function
    Private Function ToDate(ByVal sIn As String) As Date
        Dim nYear As Integer
        Dim nMonth As Integer
        Dim nDay As Integer

        nYear = sIn.Substring(0, 4)
        nMonth = sIn.Substring(4, 2)
        nDay = sIn.Substring(6, 2)

        Dim nDate As Date = New Date(nYear, nMonth, nDay)

        Return nDate
    End Function
End Class
