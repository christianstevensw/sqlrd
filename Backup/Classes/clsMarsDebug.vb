Public Class clsMarsDebug
    Shared ReadOnly Property m_logsDir()
        Get
            If IO.Directory.Exists(sAppPath & "Logs\") = False Then
                IO.Directory.CreateDirectory(sAppPath & "Logs\")
            End If

            Return sAppPath & "Logs\"
        End Get
    End Property
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="fileName">the name of the file excluding directory</param>
    ''' <param name="message">the message to put into the debug - date will be appended to this message</param>
    ''' <param name="append">start creating a new debug file or append?</param>
    ''' <remarks></remarks>
    ''' 
    '<DebuggerStepThrough()> _
    Public Shared Sub writeToDebug(ByVal fileName As String, ByVal message As String, ByVal append As Boolean)
        If fileName = "" Then Return

        For Each s As String In IO.Path.GetInvalidFileNameChars
            fileName = fileName.Replace(s, "")
        Next

        Dim path As String = m_logsDir & fileName

        Try
            If IO.Path.GetExtension(path) = "" Then
                path = path & ".debug"
            End If
        Catch : End Try

        Dim fullMsg As String = Now & ": " & message

        SaveTextToFile(fullMsg, path, , append, True)
    End Sub
End Class
