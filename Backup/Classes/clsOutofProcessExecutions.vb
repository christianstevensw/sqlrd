Imports System.Net
Imports System.Net.Sockets
Imports System.Text

Public Class clsOutofProcessExecutions
    Public Shared currentID As String = 0
    Public Function processCommand()
        Try
            Dim oT As clsMarsThreading = New clsMarsThreading
            Dim command As String
            Dim scheduleType As String
            Dim ID As String
            Dim dataConnection As String

            RunEditor = True
            gVisible = True

            Dim args() As String = Environment.GetCommandLineArgs

            command = args(1)
            scheduleType = args(2)
            ID = args(3)

            dataConnection = ""

            For I As Integer = 4 To (args.Length - 1)
                dataConnection &= args(I) & " "
            Next

            If dataConnection.Trim <> sCon Then
                clsMarsData.DataItem.CloseMainDataConnection()

                clsMarsData.DataItem.OpenMainDataConnection(dataConnection)
            End If

            currentID = ID


            Select Case scheduleType
                Case "s" 'single schedule
                    oT.xReportID = ID
                    oT.SingleScheduleThread(True)
                Case "p" 'package/dynamic package
                    If ID.Contains(":") Then
                        oT.xPackID = ID.Split(":")(0)
                        oT.xReportID = ID.Split(":")(1)
                    Else
                        oT.xPackID = ID
                    End If

                    oT.PackageScheduleThread(True)
                Case "d" 'dynamic schedule
                    oT.xReportID = ID
                    oT.DynamicScheduleThread(True)
                Case "e" 'event based schedule
                    oT.xEventID = ID
                    oT.EventScheduleThread(True)
                Case "ep" 'event based package
                    oT.xPackID = ID
                    oT.EventPackageScheduleThread(True)
                Case "a" 'automation schedule
                    oT.xAutoID = ID
                    oT.AutoScheduleThread(True)
                Case "b" 'bursting schedule
                    oT.xReportID = ID
                    oT.BurstingScheduleThread(True)
            End Select

            End
        Catch ex As Exception
            End
        End Try
    End Function

    Public Shared Sub sendProgress(ByVal message As String, Optional ByVal port As Integer = 2476)
        Try
            Dim tcp As New TcpClient

            tcp.SendTimeout = 5

            tcp.Connect("localhost", port)

            Dim networkStream As NetworkStream = tcp.GetStream()

            If networkStream.CanWrite And networkStream.CanRead Then
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(message)

                networkStream.Write(sendBytes, 0, sendBytes.Length)

                networkStream.Close(30)
            End If
        Catch : End Try
    End Sub
End Class
