Public Class clsOfficeFileSummary
    Public Function _SetFileSummary(ByVal sFile As String, ByVal sAuthor As String, ByVal sTitle As String, _
    ByVal szSubject As String, ByVal sCompany As String, ByVal sComments As String) As Boolean
        Dim oDocument As New DSOFile.OleDocumentPropertiesClass
        Dim oProps As DSOFile.SummaryProperties

        Try
            Dim sPath As String
            Dim sTemp As String
            Dim oData As New clsMarsData

            'create a working file
            sPath = GetDirectory(sFile)

            sPath &= "{" & clsMarsData.CreateDataID("", "") & "}.doc"

            IO.File.Copy(sFile, sPath, True)

            oDocument.Open(sPath, False, DSOFile.dsoFileOpenOptions.dsoOptionOpenReadOnlyIfNoWriteAccess)

            oProps = oDocument.SummaryProperties

            With oProps
                .Author = sAuthor
                .Title = sTitle
                .Subject = szSubject
                .Comments = sComments
                .Company = sCompany
            End With

            oDocument.Save()

            oDocument.Close()

            oDocument = Nothing

            oProps = Nothing

            'overwrite old file

            System.IO.File.Delete(sFile)

            IO.File.Move(sPath, sFile)

            Return True
        Catch ex As Exception
            oDocument.Close()
            oDocument = Nothing
            ''console.writeLine(ex.Message)
            Return False
        End Try
    End Function

    Public Function _GetFileInfo(ByVal sFile As String, ByVal oRs As ADODB.Recordset, ByVal IsDynamic As Boolean, _
    ByVal nReportID As Integer, ByVal sKeyParameter As String, ByVal sKeyValue As String, _
    Optional ByVal IsWord As Boolean = False) As Boolean
        Try
            Dim infoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer As String
            Dim oDynamcData As New clsMarsDynamic
            Dim oParse As New clsMarsParser
            Dim oData As New clsMarsData

            infoTitle = oParse.ParseString(oRs("infotitle").Value)
            InfoAuthor = oParse.ParseString(oRs("infoauthor").Value)
            InfoSubject = oParse.ParseString(oRs("infosubject").Value)
            InfoKeywords = oParse.ParseString(oRs("infokeywords").Value)
            InfoProducer = oParse.ParseString(oRs("infoproducer").Value)

            If infoTitle = "" And InfoAuthor = "" And InfoSubject = "" And InfoKeywords = "" And InfoProducer = "" Then
                Return True
            End If

            If IsDynamic = True And infoTitle.IndexOf("<[x]") > -1 Then
                infoTitle = oDynamcData. _
                _CreateDynamicString(infoTitle, _
                nReportID, sKeyParameter, sKeyValue)
            End If

            If IsDynamic = True And InfoAuthor.IndexOf("<[x]") > -1 Then
                InfoAuthor = oDynamcData. _
                       _CreateDynamicString(InfoAuthor, _
                       nReportID, sKeyParameter, sKeyValue)
            End If

            If IsDynamic = True And InfoSubject.IndexOf("<[x]") > -1 Then
                InfoSubject = oDynamcData. _
                           _CreateDynamicString(InfoSubject, _
                           nReportID, sKeyParameter, sKeyValue)
            End If

            If IsDynamic = True And InfoKeywords.IndexOf("<[x]") > -1 Then
                InfoKeywords = oDynamcData. _
                           _CreateDynamicString(InfoKeywords, _
                           nReportID, sKeyParameter, sKeyValue)
            End If

            If IsDynamic = True And InfoProducer.IndexOf("<[x]") > -1 Then
                InfoProducer = oDynamcData. _
                           _CreateDynamicString(InfoProducer, _
                           nReportID, sKeyParameter, sKeyValue)
            End If

            Try
                If IsWord = True Then

                    Dim w As Object
                    Dim d As Object
                    Dim NewFile As String
                    Dim wordProcs As New ArrayList
                    Dim wordID As Integer

                    For Each p As Process In Process.GetProcessesByName("winword")
                        wordProcs.Add(p.Id)
                    Next

                    w = CreateObject("Word.Application")

                    Dim newList As New ArrayList

                    For Each p As Process In Process.GetProcessesByName("winword")
                        newList.Add(p.Id)
                    Next

                    'find new word instance
                    For Each n As Integer In newList
                        If wordProcs.IndexOf(n) = -1 Then
                            wordID = n
                            Exit For
                        End If
                    Next

                    d = w.Documents.Open(sFile)

                    NewFile = GetDirectory(sFile) & clsMarsData.CreateDataID("", "") & ".doc"

                    d.SaveAs(NewFile, 0)

                    d.Close(False)

                    w.Quit(False)

                    Try
                        Dim p As Process = Process.GetProcessById(wordID)

                        p.CloseMainWindow()

                        p.Kill()
                    Catch : End Try

                    IO.File.Delete(sFile)

                    IO.File.Move(NewFile, sFile)
                End If
            Catch : End Try

            Dim ok As Boolean = _SetFileSummary(sFile, InfoAuthor, infoTitle, InfoSubject, _
            InfoProducer, InfoKeywords)

            Return ok
        Catch : End Try
    End Function
End Class
