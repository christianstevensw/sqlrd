'This class is an extension of clsMarsReport


Imports sqlrd.clsMarsMessaging
Imports System.Xml

Partial Class clsMarsReport
    Public Shared m_dataDrivenCache As DataTable
    Public Shared m_pointer As Integer = 0
    Public Shared m_ddKeyColumn As String
    Public Shared m_ddKeyValue As String
    Dim oTask As clsMarsTask = New clsMarsTask
    Dim nScheduleID As Integer = 0
    Dim reportRunID As Integer = 0
    'datatable to hold emailfields used and the returned value
    Dim m_emailTable As DataTable
    Public Function resetErrors()
        gErrorDesc = ""
        gErrorNumber = 0
        gErrorSource = ""
        gErrorLine = 0
        gErrorSuggest = ""
    End Function

    Public Function RunDataDrivenPackage(ByVal nPackID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nScheduleID As Integer
        Dim gScheduleName As String
        Dim tempFolder As String
        Dim runPerSchedule As Boolean = False
        Dim ok As Boolean = False
        Dim failonOne As Boolean = False
        Dim groupReports As Boolean = True
        Dim skipResult As Boolean = False
        Dim keyColumn As String = ""
        Dim keyValue As String = ""
        Dim appendToFile As Boolean()

5:      clsMarsScheduler.ScheduleStart = Now
10:     SQL = "SELECT dynamicTasks,FailOnOne FROM PackageAttr WHERE PackID =" & nPackID

20:     oRs = clsMarsData.GetData(SQL)

30:     Try
40:         If IsNull(oRs(0).Value, 0) = 1 Then
50:             runPerSchedule = True
60:         Else
70:             runPerSchedule = False
            End If

80:         If IsNull(oRs(1).Value, 0) = 1 Then
90:             failonOne = True
100:        Else
110:            failonOne = False
            End If
120:        oRs.Close()
        Catch : End Try

130:    Me.m_HistoryID = clsMarsData.CreateDataID("ScheduleHistory", "HistoryID")

140:    nScheduleID = clsMarsScheduler.globalItem.GetScheduleID(, nPackID)
150:    gScheduleName = clsMarsScheduler.globalItem.GetScheduleName(nPackID, clsMarsScheduler.enScheduleType.PACKAGE)
155:    clsMarsReport.m_scheduleID = nScheduleID
160:    resetErrors()
170:    gErrorCollection = ""

        'plan of action
        '1. populate data driver
        '2. for each record in the data driver, generate all the reports
        '3. store the resulting files in a unique folder per record
        '4. process the package destinations
180:    Try
190:        SQL = "SELECT * FROM DataDrivenAttr WHERE PackID =" & nPackID

200:        oRs = clsMarsData.GetData(SQL)

210:        If oRs Is Nothing Then Throw New Exception("Could not initialize data for schedule")

220:        If oRs.EOF = True Then Throw New Exception("Could not initialize data for schedule")

            Dim ConString, DSN, sUser, sPassword, sQuery As String


230:        ConString = oRs.Fields("constring").Value
240:        sQuery = oRs.Fields("sqlquery").Value

250:        Try
260:            groupReports = IsNull(oRs("groupreports").Value, 1)
270:        Catch ex As Exception
280:            groupReports = True
            End Try

290:        Try
300:            keyColumn = IsNull(oRs("keycolumn").Value)
            Catch : End Try

310:        If Me.populateDataDrivenCache(sQuery, ConString, True) = 0 Then Return True

320:        oRs.Close()

330:        SQL = "SELECT * FROM PackageAttr p INNER JOIN DestinationAttr d ON " & _
      "p.PackID = d.PackID WHERE p.PackID = " & nPackID & " AND d.EnabledStatus = 1 ORDER BY d.DestOrderID"

340:        Dim tmpName As Int64 = Date.Now.Subtract(New Date(2000, 1, 1, 0, 0, 0)).TotalMilliseconds

350:        tempFolder = Me.m_OutputFolder & CType(tmpName, String) & "\"

360:        Dim paths As DataTable = New DataTable("Paths")

370:        With paths
380:            .Columns.Add("DestinationID")
390:            .Columns.Add("Directory")
400:            .Columns.Add("sendTo")
410:            .Columns.Add("cc")
420:            .Columns.Add("bcc")
                .Columns.Add("GroupOutput")
                .Columns.Add("KeyValue")
            End With

430:        clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & Me.m_dataDrivenCache.Rows.Count & " WHERE PackID =" & nPackID)

            'add looper for datacache
440:        Me.m_emailTable = New DataTable

450:        With Me.m_emailTable.Columns
460:            .Add("EmailField")
470:            .Add("EmailValue")
            End With

480:        If runPerSchedule = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)

            'loop through the records in the data-driver
490:        For x As Integer = 0 To Me.m_dataDrivenCache.Rows.Count - 1
                m_ddKeyColumn = ""
                m_ddKeyValue = ""

500:            rptFileNames = Nothing

510:            clsMarsUI.BusyProgress((x / Me.m_dataDrivenCache.Rows.Count) * 100, "Report " & (x + 1) & " of " & Me.m_dataDrivenCache.Rows.Count)

520:            Me.m_pointer = x

521:            SQL = "SELECT * FROM PackageAttr p INNER JOIN DestinationAttr d ON " & _
                "p.PackID = d.PackID WHERE p.PackID = " & nPackID & " AND d.EnabledStatus = 1 ORDER BY d.DestOrderID"

530:            oRs = clsMarsData.GetData(SQL)

540:            If keyColumn <> "" Then
550:                Try
560:                    keyValue = clsMarsParser.Parser.ParseString("<[r]" & keyColumn & ">", , , , , , m_ParametersTable)
570:                Catch ex As Exception
580:                    keyValue = "[Unavailable]"
                    End Try
590:            Else
600:                keyValue = "[Unavailable]"
                End If

                m_ddKeyColumn = keyColumn
                m_ddKeyValue = keyValue

610:            Do While oRs.EOF = False
611:                Dim destinationType As String = oRs("destinationtype").Value
612:                Dim nDestinationID As Integer = oRs("destinationid").Value
613:                Dim sDestName As String = oRs("destinationname").Value
                    Dim sExport() As String
                    Dim sPackage As String
                    Dim nRetry As Integer
                    Dim CheckBlank As Boolean
                    Dim adjustStamp As Integer
                    Dim sPackageStamp As String
                    Dim MergePDF As Boolean
                    Dim sMergePDFName As String
                    Dim MergeXL As Boolean
                    Dim sMergeXLName As String
                    Dim ToPrinter, ToFax As Boolean

620:                sPackage = oRs("PackageName").Value
630:                nRetry = IsNull(oRs("Retry").Value, 0)
640:                CheckBlank = Convert.ToBoolean(oRs("CheckBlank").Value)
650:                failonOne = Convert.ToBoolean(oRs("failonone").Value)
660:                gScheduleName = sPackage
670:                adjustStamp = IsNull(oRs("adjustpackagestamp").Value, 0)

680:                Try
690:                    If Convert.ToBoolean(oRs("datetimestamp").Value) = True Then
700:                        sPackageStamp = IsNull(oRs("stampformat").Value)
                        End If
                    Catch : End Try

710:                Try
720:                    MergePDF = Convert.ToBoolean(oRs("mergepdf").Value)
730:                    sMergePDFName = oRs("mergepdfname").Value
740:                Catch
750:                    MergePDF = False
                    End Try

760:                Try
770:                    MergeXL = Convert.ToBoolean(oRs("mergexl").Value)
780:                    sMergeXLName = oRs("mergexlname").Value
790:                Catch ex As Exception
800:                    MergeXL = False
                    End Try

                    'lets check if the user has set the package to be grouped AND merged by email address
                    Dim oRsMerge As ADODB.Recordset = clsMarsData.GetData("SELECT mergeallpdf, mergeallxl FROM datadrivenattr WHERE packid =" & nPackID)
                    Dim mergeAllPDF, mergeAllXL, doMergePdf, doMergeXL As Boolean

                    If oRsMerge IsNot Nothing Then
                        If oRsMerge.EOF = False Then
                            mergeAllPDF = IsNull(oRsMerge("mergeallpdf").Value, False)
                            mergeAllXL = IsNull(oRsMerge("mergeallxl").Value, False)
                        End If

                        oRsMerge.Close()
                    End If

                    If mergeAllPDF = True Or MergePDF = True Then
                        doMergePdf = True
                    End If

                    If mergeAllXL = True Or MergeXL = True Then
                        doMergeXL = True
                    End If

810:                skipResult = False

                    If destinationType = "Printer" Then
811:                    ToPrinter = True
                    End If

                    If destinationType = "Fax" Then
812:                    ToFax = True
                    End If
                    'if its email destination then create a folder for each recipient

                    'product the report and put the value into m_exportedFile
820:                sExport = Me.ProduceReportsForPackage(nPackID, gScheduleName, sPackageStamp, doMergePdf, doMergeXL, adjustStamp, failonOne, _
                    appendToFile, , ToPrinter, ToFax, , nRetry, , keyValue, True)

830:                If sExport Is Nothing Then
840:                    ok = False

                        If gErrorDesc <> "" Then
                            _ErrorHandle("KEY VALUE: " & keyValue & vbCrLf & gErrorDesc & vbCrLf & "RECORD #: " & Me.m_pointer, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, , True)

                            Dim sFullError As String

                            sFullError = "KEY VALUE: " & keyValue & vbCrLf & "RECORD #:" & m_pointer & vbCrLf & "Destination Error: " & sDestName & vbCrLf & vbCrLf & "+++++++++++++++++++++++" & vbCrLf & gErrorDesc & vbCrLf & _
                            "+++++++++++++++++++++++"

850:                        gErrorCollection &= sFullError & vbCrLf & vbCrLf

860:
900:                        Throw New Exception(gErrorCollection)
                        Else
                            ReDim sExport(0)
                            sExport(0) = ""
                            skipResult = True
                            ok = True
                        End If
910:                Else
920:                    ok = True
                    End If

                    If skipResult = False Then
                        Dim c As Integer = 0
930:                    ReDim rptFileNames(0)

940:                    For Each s As String In sExport
950:                        If s <> "" Then
960:                            gExportedFileName &= s & vbCrLf

970:                            ReDim Preserve rptFileNames(c)

980:                            rptFileNames(c) = s

990:                            c += 1
                            End If
1000:                   Next

1010:                   gExportedFileName = gExportedFileName.Remove(gExportedFileName.Length - 1, 1)

                        'check if pdfs should be merged...
1020:                   If MergePDF = True Then
1030:                       appendToFile = Nothing

1040:                       Dim oParse As clsMarsParser = New clsMarsParser

1050:                       PreparePDF(oParse.ParseString(sMergePDFName, , , , , , m_ParametersTable), True)

                            Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM PackageOptions WHERE PackID = " & nPackID)

1060:                       If oRs1.EOF = False Then
1070:                           Dim oPerm As New clsMarsPDF
                                Dim sOwnerPass As String
                                Dim sUserPass As String

1080:                           For n As Integer = 0 To rptFileNames.GetUpperBound(0)
1090:                               If rptFileNames(n).IndexOf(".pdf") > -1 Then

1100:                                   Try
                                            Dim infoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer, sWatermark As String
                                            Dim InfoCreated As Date

1110:                                       sOwnerPass = oRs1("pdfpassword").Value
1120:                                       sUserPass = oRs1("userpassword").Value

1130:                                       infoTitle = oParse.ParseString(oRs1("infotitle").Value, , , , , , m_ParametersTable)
1140:                                       InfoAuthor = oParse.ParseString(oRs1("infoauthor").Value, , , , , , m_ParametersTable)
1150:                                       InfoSubject = oParse.ParseString(oRs1("infosubject").Value, , , , , , m_ParametersTable)
1160:                                       InfoKeywords = oParse.ParseString(oRs1("infokeywords").Value, , , , , , m_ParametersTable)
1170:                                       InfoProducer = oParse.ParseString(oRs1("infoproducer").Value, , , , , , m_ParametersTable)
1180:                                       InfoCreated = oRs1("infocreated").Value
1190:                                       sWatermark = IsNull(oRs1("pdfwatermark").Value)

1200:                                       Me._PostProcessPDF(rptFileNames(n), infoTitle, InfoAuthor, InfoSubject, _
                                                InfoKeywords, InfoProducer, InfoCreated, False, 0, Nothing, _
                                                  Convert.ToBoolean(oRs1("pdfsecurity").Value), False, oRs1("canprint").Value, _
                                                oRs1("cancopy").Value, _
                                                oRs1("canedit").Value, _
                                                oRs1("cannotes").Value, oRs1("canfill").Value, oRs1("canaccess").Value, _
                                                oRs1("canassemble").Value, _
                                                oRs1("canprintfull").Value, sOwnerPass, sUserPass, sWatermark, 0)

1210:                                       oPerm.SetPDFSummary(infoTitle, InfoAuthor, _
                                                InfoSubject, InfoKeywords, InfoCreated, InfoProducer _
                                                , rptFileNames(n))
                                        Catch : End Try
                                    End If
1220:                           Next
                            End If

1230:                       oRs1.Close()
                        End If

1240:                   If frmMain.m_UserCancel = True Then
1250:                       Me.CancelExecution("RunPackageSchedule")
1260:                       Return False
                        End If

1270:                   If MergeXL = True Then
1280:                       appendToFile = Nothing

1290:                       Dim oParse As clsMarsParser = New clsMarsParser

1300:                       PrepareXLS(m_OutputFolder & oParse.ParseString(sMergeXLName, , , , , , m_ParametersTable))

1310:                       Try
                                Dim oRsx As ADODB.Recordset

                                'protect it
1320:                           SQL = "SELECT ProtectExcel, ExcelPassword FROM PackageOptions WHERE PackID = " & nPackID

1330:                           oRsx = clsMarsData.GetData(SQL)

1340:                           If oRsx.EOF = False Then
1350:                               If IsNull(oRsx.Fields(0).Value, "0") = "1" Then
1360:                                   Dim oXL As New ExcelMan.clsExcelMan(ExcelMan.clsExcelMan.XLFormats.Excel8)
1370:                                   oXL.SetWorkBookPassword(rptFileNames(rptFileNames.GetUpperBound(0)), _DecryptDBValue(oRsx(1).Value))
1380:                                   oXL.Dispose()
                                    End If
                                End If

1390:                           oRsx.Close()
                            Catch : End Try
                        End If

1400:                   Dim oPGP As New clsMarsPGP
                        Dim oPGPresult() As Object
                        Dim nFile As Integer = 0

1410:                   For Each s As String In rptFileNames
1420:                       If s.Length > 0 Then
1430:                           oPGPresult = oPGP.EncryptFile(s, nDestinationID)

1440:                           If oPGPresult(0) = False Then
                                    Throw New Exception("PGP Encryption Error: " & vbCrLf & Me._CreateErrorString)
                                End If

1450:                           rptFileNames(nFile) &= oPGPresult(1)

1460:                           nFile += 1
                            End If
1470:                   Next
                    End If

                    'first produce the reports and then move them into the relevant sub folder groups
1480:               If ok = True And skipResult = False Then
1490:                   Select Case destinationType.ToLower
                            Case "email"
                                Dim sendTo As String = oRs("sendto").Value
                                Dim cc As String = oRs("cc").Value
                                Dim bcc As String = oRs("bcc").Value
                                Dim I As Integer = 0
                                Dim dataValues() As String = Nothing

                                'find the data item in sendto
1500:                           For Each s As String In sendTo.Split(";")
1510:                               If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

1520:                                   If s.Contains("<[r]") Then
1530:                                       nstart = sendTo.IndexOf("<[r]")
1540:                                       nend = sendTo.IndexOf(">", nstart)
1550:                                       dataFunction = sendTo.Substring(nstart, (nend - nstart) + 1)
                                        End If

1560:                                   ReDim Preserve dataValues(I)

                                        dataValues(I) = "to:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        Dim row As DataRow = Me.m_emailTable.Rows.Add

1570:                                   row(0) = dataFunction.Replace("<[r]", "").Replace(">", "")
1580:                                   row(1) = clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

1590:                                   I += 1
                                    End If
1600:                           Next

                                'find the data item in cc
1610:                           For Each s As String In cc.Split(";")
1620:                               If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

1630:                                   If s.Contains("<[r]") Then
1640:                                       nstart = cc.IndexOf("<[r]")
1650:                                       nend = cc.IndexOf(">", nstart)
1660:                                       dataFunction = cc.Substring(nstart, (nend - nstart) + 1)
                                        End If

1670:                                   ReDim Preserve dataValues(I)

                                        dataValues(I) = "cc:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        'cc = cc.Remove(nstart, (nend - nstart) + 1)

1680:                                   I += 1
                                    End If
1690:                           Next

                                'find the data item in bcc
1700:                           For Each s As String In bcc.Split(";")
1710:                               If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

1720:                                   If s.Contains("<[r]") Then
1730:                                       nstart = bcc.IndexOf("<[r]")
1740:                                       nend = bcc.IndexOf(">", nstart)
1750:                                       dataFunction = bcc.Substring(nstart, (nend - nstart) + 1)
                                        End If

1760:                                   ReDim Preserve dataValues(I)

                                        dataValues(I) = "bcc:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        'bcc = bcc.Remove(nstart, (nend - nstart) + 1)

                                    End If
1770:                           Next

1780:                           sendTo = clsMarsParser.Parser.ParseString(sendTo, , , , , , m_ParametersTable)
1790:                           cc = clsMarsParser.Parser.ParseString(cc, , , , , , m_ParametersTable)
1800:                           bcc = clsMarsParser.Parser.ParseString(bcc, , , , , , m_ParametersTable)


1810:                           If dataValues IsNot Nothing Then
                                    Dim output As String = ""

1820:                               If groupReports = True Then
1830:                                   For Each s As String In dataValues
1840:                                       If s.Length > 0 Then
1850:                                           If s.EndsWith(";") Then s = s.Remove(s.Length - 1, 1)

1860:                                           output &= s & ";"
                                            End If
1870:                                   Next

1880:                                   output = output.Remove(output.Length - 1, 1)

1890:                                   output = output.GetHashCode

1900:                                   output = tempFolder & output & "\"
1910:                               Else
1920:                                   Do
1930:                                       output = IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)

1940:                                       output = tempFolder & output & "\"
1950:                                   Loop Until IO.Directory.Exists(output) = False
                                    End If


1960:                               IO.Directory.CreateDirectory(output.Replace(";", "_"))

1970:                               For Each s As String In rptFileNames
1980:                                   IO.File.Copy(s, output & ExtractFileName(s), True)
                                        System.Threading.Thread.Sleep(1000)
1990:                               Next

                                    Dim row As DataRow
                                    Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

2000:                               If rows.Length = 0 Then
2010:                                   row = paths.Rows.Add
2020:                                   row("DestinationID") = nDestinationID
2030:                                   row("Directory") = output
2040:                                   row("sendTo") = clsMarsParser.Parser.ParseString(oRs("sendTo").Value, , , , , , m_ParametersTable)
2050:                                   row("cc") = clsMarsParser.Parser.ParseString(oRs("cc").Value, , , , , , m_ParametersTable)
2060:                                   row("bcc") = clsMarsParser.Parser.ParseString(oRs("bcc").Value, , , , , , m_ParametersTable)
2061:                                   row("keyvalue") = keyValue
                                    End If
                                End If
                            Case "disk"
                                Dim outputPath As String = clsMarsParser.Parser.ParseString(oRs("outputpath").Value, , , , , , m_ParametersTable)
                                Dim output As String

                                If groupReports = True Then
                                    output = tempFolder & outputPath.ToLower.GetHashCode & "\"
                                Else
                                    output = tempFolder & "output" & x & "\"
                                End If

                                If IO.Directory.Exists(output) = False Then IO.Directory.CreateDirectory(output)

                                If rptFileNames IsNot Nothing Then
                                    Dim reportname As String = ""

                                    For Each s As String In rptFileNames
                                        reportname = ExtractFileName(s)

                                        IO.File.Copy(s, output & reportname, True)
                                    Next

                                    Dim row As DataRow
                                    Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

                                    If rows.Length = 0 Then
                                        row = paths.Rows.Add
                                        row("DestinationID") = nDestinationID
                                        row("Directory") = output
                                        row("sendTo") = ""
                                        row("cc") = ""
                                        row("bcc") = ""
                                        row("GroupOutput") = clsMarsParser.Parser.ParseString(outputPath, , , , , , m_ParametersTable)
                                        row("keyvalue") = keyValue
                                    End If

                                    gExportedFileName = output & reportname
                                End If
2070:                       Case Else
                                Dim output As String = tempFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & "\"

                                If IO.Directory.Exists(output) = False Then
2080:                               IO.Directory.CreateDirectory(output)
                                End If


2090:                           For Each s As String In rptFileNames
                                    Dim reportname As String = ExtractFileName(s)

2100:                               IO.File.Copy(s, output & reportname, True)
2110:                           Next

                                Dim row As DataRow
                                Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

2120:                           If rows.Length = 0 Then
2130:                               row = paths.Rows.Add
2140:                               row("DestinationID") = nDestinationID
2150:                               row("Directory") = output
2160:                               row("sendTo") = ""
2170:                               row("cc") = ""
2180:                               row("bcc") = ""
                                    row("keyvalue") = keyValue
                                End If

                                'gExportedFileName = output & reportname
                        End Select

2190:               Else
                        'TODO:
                        'printing and faxing
                        Dim output As String = tempFolder & IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName) & "\"
                        Dim row As DataRow
                        Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

2200:                   IO.Directory.CreateDirectory(output)

2210:                   If rows.Length = 0 Then
2220:                       row = paths.Rows.Add
2230:                       row("DestinationID") = nDestinationID
2240:                       row("Directory") = output
2250:                       row("sendTo") = ""
2260:                       row("cc") = ""
2270:                       row("bcc") = ""
                            row("keyvalue") = output
                        End If
                    End If
skipper:
2280:               oRs.MoveNext()
2290:           Loop

2300:           oRs.Close()

2310:           If runPerSchedule = True And ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)

2320:       Next

2330:       ok = processDataDrivenDestinations(nPackID, paths, runPerSchedule, groupReports, clsMarsScheduler.enScheduleType.PACKAGE)

2340:       If runPerSchedule = False And ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

2350:       If ok = False And gErrorCollection <> "" Then Throw New Exception(Me._CreateErrorString())

2360:       Return ok
2370:   Catch ex As Exception
2380:       gErrorDesc = ex.Message
2390:       gErrorNumber = Err.Number
2400:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2410:       gErrorLine = Erl()
2420:       gErrorSuggest = ""
2430:       Return False
        Finally
            Try
                Me.m_dataDrivenCache.Dispose()
                Me.m_dataDrivenCache = Nothing
                IO.Directory.Delete(tempFolder)
            Catch : End Try
        End Try

    End Function
    Private Function ProduceReportforDD(ByVal nReportID As Integer, ByVal nDestinationID As Integer, _
           Optional ByVal printDate As String = "") As String

        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sExport As String
        Dim sReportName As String
        Dim sFormat As String
        Dim sFileName As String
        Dim sDateStamp As String
        Dim sFile As String
        Dim sFormula As String
10:     Dim oTask As clsMarsTask = New clsMarsTask
20:     Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Int32 = 0
        'Dim IsDynamic As Boolean
        Dim nInclude As Integer
30:     Dim oParse As New clsMarsParser
        Dim Ok As Boolean
        Dim sDynamic() As String
        Dim sExt As String
        Dim HTMLSplit As Boolean
        Dim isDefer As Boolean
        Dim sReportPath As String
        Dim sPrintMethod As String
        Dim oRpt As ReportServer.ReportingService
        Dim oLogins() As ReportServer.DataSourceCredentials
        Dim sCluster As String
        Dim ExcelBurst As Boolean = False
        Dim sBurstColumn As String
        Dim oBlank As ADODB.Recordset
        Dim IsStatic As Boolean = False
        Dim TryCount As Int32 = 0
        Dim sTemp As String = ""
        Dim IncludeAttach As Boolean
        Dim PDFBookmark As Boolean = False
        Dim sDestName As String
        Dim adjustStamp As Double
        Dim CRFieldType As String = ""
40:     Dim oUI As clsMarsUI = New clsMarsUI
50:     Dim oDynamcData As New clsMarsDynamic
        Dim nRetry As Integer = 0
        Dim sUrl As String = ""
        Dim rptTimeout As Integer
        Dim reportStart As Date = Date.Now

60:     gCurrentTempOutputFile = ""

70:     clsMarsScheduler.ScheduleStart = Now

80:     Try
90:         SetReportParameters(nReportID)

100:        HTMLPath = ""

110:        gCurrentTempOutputFile = ""


120:        sCluster = oUI.ReadRegistry("SystemEnvironment", "")

130:        nScheduleID = oSchedule.GetScheduleID(nReportID)

140:        m_scheduleID = nScheduleID

150:        Me.m_DynamicSchedule = False

160:        oUI.BusyProgress(25, "Collecting data...")

170:        SQL = "SELECT * FROM ReportAttr r INNER JOIN DestinationAttr d ON " & _
                  "r.ReportID = d.ReportID " & _
                  "WHERE r.ReportID = " & nReportID & " AND " & _
                  "d.DestinationID =" & nDestinationID & " AND d.EnabledStatus = 1 ORDER BY d.DestOrderID"

180:        oRs = clsMarsData.GetData(SQL)

190:        If oRs Is Nothing Then Err.Raise(-102, , _
                  "Failed to obtain valid recordset (ReportAttr)")

200:        If oRs.EOF = True Then Err.Raise(-103, , _
                  "The recordset did not return any records (ReportAttr)")

            'get values from recordset before looping
210:        If oRs.EOF = False Then
220:            sReportName = oRs("ReportName").Value
230:            sUrl = oRs("databasepath").Value
240:            gScheduleName = sReportName
250:            nRetry = IsNull(oRs("retry").Value, 0)
260:            rptTimeout = IsNull(oRs("assumefail").Value, 30)

270:            If rptTimeout = 0 Then rptTimeout = 360

280:            gDoReportParsing = IsNull(oRs("parsereportfields").Value, 0)

290:            sReportPath = IsNull(CType(oRs.Fields("cachepath").Value, String))

300:
310:            Try
320:                IsStatic = Convert.ToBoolean(oRs("staticdest").Value)
330:            Catch
340:                IsStatic = False
                End Try

350:            oRpt = New ReportServer.ReportingService
360:            greportItem = oRpt

370:            oRpt.Timeout = -1 '3600000

380:            oRpt.Url = sUrl

390:            greportItem = oRpt

                'set up reportviewer (rv) if server version is 2005
400:            If rv IsNot Nothing Then
410:                rv = Nothing
420:            End If

430:            rv = New Microsoft.Reporting.WinForms.ReportViewer
440:            rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

                Dim serverUrl As String = ""

450:            For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
460:                serverUrl &= sUrl.Split("/")(I) & "/"
470:            Next

480:            rv.ServerReport.ReportServerUrl = New Uri(serverUrl)

490:            If Me.m_ServerVersion2005 = True Then
500:                rv.ServerReport.Timeout = -1 'oRpt.Timeout
                End If

510:            If frmMain.m_UserCancel = True Then
520:                Me.CancelExecution("ProduceReportforDD")
530:                Return False
540:            End If

550:            If Me.SetServerLogin(oRpt, nReportID) = False Then
560:                Return False
570:            End If

580:            oLogins = Me.CreateDatasources(nReportID)

590:            If frmMain.m_UserCancel = True Then
600:                Me.CancelExecution("ProduceReportforDD")
610:                Return False
                End If
620:        End If

630:        Do While oRs.EOF = False

640:            Try
650:                HTMLPath = String.Empty
660:                nDestinationID = oRs("destinationid").Value
670:                sExt = oParse.ParseString(IsNull(oRs("customext").Value), , , , , , m_ParametersTable)
680:                ExcelBurst = False
690:                PDFBookmark = False
700:                sDestName = IsNull(oRs("destinationname").Value, "")
710:                adjustStamp = IsNull(oRs("adjuststamp").Value, 0)

720:                sFormat = ""

730:                sFormat = clsMarsParser.Parser.ParseString(IsNull(oRs("outputformat").Value, ""), , , , , , m_ParametersTable)

                    If sFormat <> "Printer Format" And sFormat <> "Fax Format" Then
740:                    If Me.validateFormat(sFormat) = False Then
750:                        Throw New Exception("The format retrieved from the data driver ('" & sFormat & "') could not be understood by SQL-RD")
                        End If
                    End If

760:                If sFormat = "Fax Format" Then sFormat = "TIFF Fax (*.pdf)"

770:                sFileName = oRs("ReportTitle").Value

780:                For Each s As String In IO.Path.GetInvalidFileNameChars
790:                    sFileName = sFileName.Replace(s, "")
800:                Next

810:                If Convert.ToBoolean(oRs("AppendDateTime").Value) = True Then
820:                    sDateStamp = IsNull(oRs("DateTimeFormat").Value)

830:                    sDateStamp = (Date.Now).AddDays(adjustStamp).ToString(sDateStamp)
840:                Else
850:                    sDateStamp = ""
860:                End If
870:                oUI.BusyProgress(50, "Exporting report...")

880:                If oRs("destinationtype").Value = "Printer" Then
890:                    sFormat = "Acrobat Format (*.pdf)"
900:                    IncludeAttach = True
                    End If

                    Dim sSendTo As String = ""
                    Dim sSubject As String
                    Dim sCc As String
                    Dim sBcc As String
                    Dim sExtras As String
                    Dim sMsg As String
                    Dim sName As String
                    Dim sPath As String
910:                Dim oDynamicData As New clsMarsDynamic
                    Dim Embed As Boolean
                    Dim MailFormat As String
                    Dim ScheduleType As String = "Single"
                    Dim nDeferBy As Double
                    Dim SMTPServer As String
                    Dim ReadReceipt As Boolean = False

920:                Try
930:                    IncludeAttach = Me.DoIncludeAttach(nDestinationID)
940:                Catch ex As Exception
950:                    IncludeAttach = True
960:                End Try

970:                If IncludeAttach = True Then
980:                    sExport = ProcessReport(sFormat, oRpt, sDateStamp, nReportID, _
                        sTempPath & sFileName, sFileName, sReportPath, _
                        oLogins, "", oRs("destinationid").Value, sExt, False, False, 0, rptTimeout)
990:                Else
1000:                   sExport = "{skip}"
                    End If
1010:

1020:               If sExport.ToLower = "{error}" Then
                        Throw New Exception("Report Processing Error: " & vbCrLf & Me._CreateErrorString)
                    End If

1030:               If sExport.ToLower = "{skip}" Then
1040:                   Ok = True
1050:                   GoTo NextDest
                    End If

                    'customize the filename
1060:               If IsNull(oRs("CustomName").Value) <> "" And sExport <> "" Then
                        Dim illegalChars As String = IO.Path.GetInvalidFileNameChars

1070:                   sFileName = oParse.ParseString(oRs("CustomName").Value, , , , , , m_ParametersTable)

1080:                   If sFileName.IndexOf("[p]") = -1 And sFileName.IndexOf("[~]") = -1 Then

1090:                       For Each s As String In illegalChars
1100:                           sFileName = sFileName.Replace(s, "")
1110:                       Next

                        End If

1120:                   Dim tempOutputFile As String

1130:                   If HTMLPath <> "" Then
1140:                       tempOutputFile = HTMLPath
1150:                   Else
1160:                       tempOutputFile = sExport
                        End If

                        Dim newFile As String = IO.Path.GetDirectoryName(tempOutputFile)
                        Dim newExt As String = IO.Path.GetExtension(tempOutputFile)

1170:                   If newFile.EndsWith("\") = False Then newFile &= "\"

1180:                   newFile &= sFileName & sDateStamp & newExt

1181:                   IO.File.Delete(newFile)

1190:                   IO.File.Move(tempOutputFile, newFile)

1200:                   sExport = newFile

1210:               End If

                    'set the excel file password
1220:               Try
1230:                   If sFormat.ToLower.IndexOf("excel") > -1 Then
1240:                       Dim oRsExcel As ADODB.Recordset

1250:                       oRsExcel = clsMarsData.GetData("SELECT ProtectExcel,ExcelPassword FROM ReportOptions " & _
                                 "WHERE DestinationID =" & nDestinationID)

1260:                       If oRsExcel.EOF = False Then
1270:                           If IsNull(oRsExcel("protectexcel").Value, "0") = "1" Then
1280:                               Dim oXL As New ExcelMan.clsExcelMan(ExcelMan.clsExcelMan.XLFormats.Excel8)
1290:                               oXL.SetWorkBookPassword(sExport, _DecryptDBValue(oRsExcel("excelpassword").Value))
1300:                               oXL.Dispose()
1310:                           End If
1320:                       End If

1330:                       oRsExcel.Close()
1340:                   End If
                    Catch : End Try

1350:               If HTMLPath <> "" Then HTMLPath = GetDirectory(HTMLPath)

1360:               Dim oSnap As New clsMarsSnapshots

1370:               oSnap.CreateSnapshot(nReportID, sExport, HTMLPath)

1380:               gExportedFileName = ExtractFileName(sExport)

1390:               sExtras = oParse.ParseString(IsNull(oRs("extras").Value), , , , , , m_ParametersTable)

1400:               Dim oPGP As New clsMarsPGP
                    Dim oPGPResult()

1410:               oPGPResult = oPGP.EncryptFile(sExport, nDestinationID)

1420:               If oPGPResult(0) = False Then
1430:                   Throw New Exception("PGP Encryption Error: " & vbCrLf & Me._CreateErrorString)
                    End If

1440:               sExport &= oPGPResult(1)

1450:               ' oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)
1460:           Catch ex As Exception
1470:               Throw New Exception(ex.Message & ": Line - " & Erl())
                End Try
NextDest:
1480:           oRs.MoveNext()
1490:       Loop

1500:       oRs.Close()

1510:       oRs = Nothing

1520:       oUI.BusyProgress(95, "Cleaning up...")

1530:       oUI.BusyProgress(, , True)

1540:       trackDuration(nReportID, reportStart)

1550:       Return sExport

1560:   Catch ex As Exception
1570:       Dim sError As String
            Dim nClose As Integer = 0

1580:       oRpt = Nothing
1590:       sError = "Data-Driven Schedule Error: " & sReportName & vbCrLf & ex.Message
1600:       gErrorDesc = sError
1610:       gErrorNumber = Err.Number
1620:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
1630:       gErrorLine = Erl()
1640:       oUI.BusyProgress(, , True)

1650:       Return Nothing
1660:   Finally
            'gScheduleName = String.Empty
1670:       oRpt = Nothing
1680:       rv = Nothing
            'HTMLPath = Nothing

            'Me.parametersTable = Nothing
1690:   End Try
    End Function

    Public Function RunDataDrivenSchedule(ByVal nReportID As Integer, Optional ByVal printDate As String = "") As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim tempFolder As String
        Dim runPerSchedule As Boolean = False
        Dim ok As Boolean = False
        Dim failonOne As Boolean = False
        Dim groupReports As Boolean = True
        Dim skipResult As Boolean = False
        Dim keyColumn As String = ""
        Dim keyValue As String = ""

10:     gErrorCollection = ""
20:     HTMLPath = ""

30:     nScheduleID = clsMarsScheduler.globalItem.GetScheduleID(nReportID)
40:     gScheduleName = clsMarsScheduler.globalItem.GetScheduleName(nReportID, clsMarsScheduler.enScheduleType.REPORT)

50:     logDurationMaster(nReportID)

60:     SQL = "SELECT dynamicTasks FROM ReportAttr WHERE ReportID =" & nReportID

70:     oRs = clsMarsData.GetData(SQL)

80:     Try
90:         If IsNull(oRs(0).Value, 0) = 1 Then
100:            runPerSchedule = True
110:        Else
120:            runPerSchedule = False
            End If

130:        oRs.Close()
        Catch:End Try

140:    Try
150:        If runPerSchedule = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)

160:        SQL = "SELECT * FROM DataDrivenAttr WHERE ReportID = " & nReportID

170:        oRs = clsMarsData.GetData(SQL)

180:        If oRs Is Nothing Then Throw New Exception("Could not initialize data for schedule")

190:        If oRs.EOF = True Then Throw New Exception("Could not initialize data for schedule")

            Dim ConString, DSN, sUser, sPassword, sQuery As String

200:        ConString = oRs.Fields("constring").Value
210:        sQuery = oRs.Fields("sqlquery").Value
220:        failonOne = IsNull(oRs.Fields("failonone").Value, 0)

            Try
                groupReports = IsNull(oRs("groupreports").Value, 1)
            Catch ex As Exception
                groupReports = True
            End Try

            Try
                keyColumn = IsNull(oRs("keycolumn").Value)
            Catch : End Try

            clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Populate data cache...", True)

230:        If Me.populateDataDrivenCache(sQuery, ConString, True) = 0 Then Return True

240:        oRs.Close()

250:        SQL = "SELECT * FROM DestinationAttr WHERE ReportID =" & nReportID & " AND EnabledStatus =1"

260:        Dim tmpName As Int64 = Date.Now.Subtract(New Date(2000, 1, 1, 0, 0, 0)).TotalMilliseconds

270:        tempFolder = Me.m_OutputFolder & CType(tmpName, String) & "\"

280:        Dim paths As DataTable = New DataTable("Paths")

290:        With paths
300:            .Columns.Add("DestinationID")
310:            .Columns.Add("Directory")
320:            .Columns.Add("sendTo")
330:            .Columns.Add("cc")
340:            .Columns.Add("bcc")
                .Columns.Add("GroupOutput")
                .Columns.Add("KeyValue")
            End With

350:        clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & Me.m_dataDrivenCache.Rows.Count & " WHERE ReportID =" & nReportID)

            'add looper for datacache
360:        Me.m_emailTable = New DataTable

370:        With Me.m_emailTable.Columns
380:            .Add("EmailField")
390:            .Add("EmailValue")
            End With



400:        For x As Integer = 0 To Me.m_dataDrivenCache.Rows.Count - 1
                m_ddKeyColumn = ""
                m_ddKeyValue = ""

410:            clsMarsUI.BusyProgress((x / Me.m_dataDrivenCache.Rows.Count) * 100, "Report " & (x + 1) & " of " & Me.m_dataDrivenCache.Rows.Count)

420:            If runPerSchedule = False Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.BEFORE)

430:            Me.m_pointer = x

440:            oRs = clsMarsData.GetData(SQL)

                If keyColumn <> "" Then
                    Try
441:                    keyValue = clsMarsParser.Parser.ParseString("<[r]" & keyColumn & ">", , , , , , m_ParametersTable)
                    Catch ex As Exception
442:                    keyValue = "[Unavailable]"
                    End Try
                Else
                    keyValue = "[Unavailable]"
                End If

                m_ddKeyColumn = keyColumn
                m_ddKeyValue = keyValue

                clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Key Column: " & keyColumn & ". Key Value: " & keyValue, True)

450:            Do While oRs.EOF = False
                    Dim destinationType As String = oRs("destinationtype").Value
                    Dim nDestinationID As Integer = oRs("destinationid").Value
                    Dim sDestName As String = oRs("destinationname").Value
                    Dim sExport As String = ""

                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Processing for destination: " & sDestName, True)

                    skipResult = False

                    'if its email destination then create a folder for each recipient

                    'product the report and put the value into m_exportedFile
                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Exporting report", True)

460:                sExport = Me.ProduceReportforDD(nReportID, nDestinationID, printDate)

470:                If sExport Is Nothing Or sExport = "{error}" Then
480:                    ok = False

                        _ErrorHandle("KEY VALUE: " & keyValue & vbCrLf & gErrorDesc & vbCrLf & "RECORD #: " & Me.m_pointer, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, , True)

                        Dim sFullError As String

                        sFullError = "KEY VALUE: " & keyValue & vbCrLf & "RECORD #:" & m_pointer & vbCrLf & "Destination Error: " & sDestName & vbCrLf & vbCrLf & "+++++++++++++++++++++++" & vbCrLf & gErrorDesc & vbCrLf & _
                        "+++++++++++++++++++++++"

490:                    gErrorCollection &= sFullError & vbCrLf & vbCrLf

500:                    If failonOne = False Then
501:                        sExport = ""
510:                        ok = False
520:                    Else
530:                        Throw New Exception(gErrorCollection)
                        End If

540:                ElseIf sExport = "{skip}" Then
550:                    ok = True
560:                    skipResult = True
570:                Else
580:                    ok = True
                    End If

                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Report exported to " & sExport, True)

590:                gExportedFileName = sExport

                    'first produce the reports and then move them into the relevant sub folder groups
                    If ok = True Then
600:                    Select Case destinationType.ToLower
                            Case "email"
                                Dim sendTo As String = oRs("sendto").Value
                                Dim cc As String = oRs("cc").Value
                                Dim bcc As String = oRs("bcc").Value
                                Dim I As Integer = 0
                                Dim dataValues() As String = Nothing

                                'find the data item in sendto
610:                            For Each s As String In sendTo.Split(";")
620:                                If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

630:                                    If s.Contains("<[r]") Then
640:                                        nstart = sendTo.IndexOf("<[r]")
650:                                        nend = sendTo.IndexOf(">", nstart)
660:                                        dataFunction = sendTo.Substring(nstart, (nend - nstart) + 1)
                                        End If

670:                                    ReDim Preserve dataValues(I)

680:                                    dataValues(I) = "to:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        Dim row As DataRow = Me.m_emailTable.Rows.Add

690:                                    row(0) = dataFunction.Replace("<[r]", "").Replace(">", "")
700:                                    row(1) = clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

710:                                    I += 1
                                    End If
720:                            Next

                                'find the data item in cc
730:                            For Each s As String In cc.Split(";")
740:                                If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

750:                                    If s.Contains("<[r]") Then
760:                                        nstart = cc.IndexOf("<[r]")
770:                                        nend = cc.IndexOf(">", nstart)
780:                                        dataFunction = cc.Substring(nstart, (nend - nstart) + 1)
                                        End If

790:                                    ReDim Preserve dataValues(I)

800:                                    dataValues(I) = "cc:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        'cc = cc.Remove(nstart, (nend - nstart) + 1)

810:                                    I += 1
                                    End If
820:                            Next

                                'find the data item in bcc
830:                            For Each s As String In bcc.Split(";")
840:                                If s <> "" Then
                                        Dim dataFunction As String = s
                                        Dim nstart As Integer = 0
                                        Dim nend As Integer = 0

850:                                    If s.Contains("<[r]") Then
860:                                        nstart = bcc.IndexOf("<[r]")
870:                                        nend = bcc.IndexOf(">", nstart)
880:                                        dataFunction = bcc.Substring(nstart, (nend - nstart) + 1)
                                        End If

890:                                    ReDim Preserve dataValues(I)

900:                                    dataValues(I) = "bcc:" & clsMarsParser.Parser.ParseString(dataFunction, , , , , , m_ParametersTable)

                                        'bcc = bcc.Remove(nstart, (nend - nstart) + 1)

910:                                End If
920:                            Next

930:                            sendTo = clsMarsParser.Parser.ParseString(sendTo, , , , , , m_ParametersTable)
940:                            cc = clsMarsParser.Parser.ParseString(cc, , , , , , m_ParametersTable)
950:                            bcc = clsMarsParser.Parser.ParseString(bcc, , , , , , m_ParametersTable)


960:                            If dataValues IsNot Nothing Then
                                    Dim output As String = ""

                                    If groupReports = True Then
970:                                    For Each s As String In dataValues
980:                                        If s.Length > 0 Then
990:                                            If s.EndsWith(";") Then s = s.Remove(s.Length - 1, 1)

1000:                                           output &= s & ";"
                                            End If
1010:                                   Next

1020:                                   output = output.Remove(output.Length - 1, 1)

1030:                                   output = output.GetHashCode

1040:                                   output = tempFolder & output & "\"
                                    Else
                                        Do
1041:                                       output = IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)

1042:                                       output = tempFolder & output & "\"
                                        Loop Until IO.Directory.Exists(output) = False
                                    End If
                                    'output = tempFolder & s.Replace(";", "_") & "\"

1050:                               IO.Directory.CreateDirectory(output.Replace(";", "_"))

1060:                               If HTMLPath <> "" Then
                                        If sExport <> "{skip}" Then
1070:                                       For Each h As String In IO.Directory.GetFiles(HTMLPath)
                                                Dim fileName As String = ExtractFileName(h)

1080:                                           IO.File.Copy(h, output & fileName)
1090:                                       Next

1100:                                       gExportedFileName = ExtractFileName(sExport)
                                        End If
1110:                               Else
                                        If sExport <> "{skip}" Then
                                            Dim reportName As String = ExtractFileName(sExport)

1120:                                       IO.File.Copy(sExport, output & reportName, True)

1130:                                       gExportedFileName = reportName
                                        End If
                                    End If

                                    Dim row As DataRow
                                    Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

1140:                               If rows.Length = 0 Then
                                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Inserting into data table for Key " & keyValue & ", Destination " & sDestName & ", DestinationID " & nDestinationID, True)

1150:                                   row = paths.Rows.Add
1160:                                   row("DestinationID") = nDestinationID
1170:                                   row("Directory") = output
1180:                                   row("sendTo") = clsMarsParser.Parser.ParseString(oRs("sendTo").Value, , , , , , m_ParametersTable)
1190:                                   row("cc") = clsMarsParser.Parser.ParseString(oRs("cc").Value, , , , , , m_ParametersTable)
1200:                                   row("bcc") = clsMarsParser.Parser.ParseString(oRs("bcc").Value, , , , , , m_ParametersTable)
                                        row("groupoutput") = ""
                                        row("keyvalue") = keyValue
                                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Row added", True)
                                    End If
                                End If
1210:
1220:                       Case Else
                                Dim output As String '= tempFolder & "output" & x & "\"

                                Do
1221:                               output = IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName)

1222:                               output = tempFolder & output & "\"
1223:                           Loop Until IO.Directory.Exists(output) = False

1230:                           IO.Directory.CreateDirectory(output)

                                If sExport <> "{skip}" Then
                                    Dim reportname As String = ExtractFileName(sExport)

1240:                               IO.File.Copy(sExport, output & reportname)

                                    Dim row As DataRow
                                    Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

1250:                               If rows.Length = 0 Then
                                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Inserting into data table for Key " & keyValue & ", Destination " & sDestName & ", DestinationID " & nDestinationID, True)

1260:                                   row = paths.Rows.Add
1270:                                   row("DestinationID") = nDestinationID
1280:                                   row("Directory") = output
1290:                                   row("sendTo") = ""
1300:                                   row("cc") = ""
1310:                                   row("bcc") = ""
                                        row("groupoutput") = ""
                                        row("keyvalue") = keyValue
                                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Row added", True)

                                    End If

1320:                               gExportedFileName = output & reportname
                                End If
                        End Select
                        'Else
                        '    Dim output As String = tempFolder & "output" & x & "\"
                        '    Dim row As DataRow
                        '    Dim rows() As DataRow = paths.Select("Directory = '" & SQLPrepare(output) & "'")

                        '    IO.Directory.CreateDirectory(output)

                        '    If rows.Length = 0 Then
                        '        row = paths.Rows.Add
                        '        row("DestinationID") = nDestinationID
                        '        row("Directory") = output
                        '        row("sendTo") = ""
                        '        row("cc") = ""
                        '        row("bcc") = ""
                        '    End If
                    End If
1330:               oRs.MoveNext()
1340:           Loop

1350:           oRs.Close()

1360:           If runPerSchedule = False Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERPROD)

1370:       Next

            'then loop through the dataset/destinations again and actually process the destinations
1380:       ok = processDataDrivenDestinations(nReportID, paths, runPerSchedule, groupReports, clsMarsScheduler.enScheduleType.REPORT)

1390:       If runPerSchedule = True And ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

1400:       If ok = False Then Throw New Exception(Me._CreateErrorString())

1410:       Return True
1420:   Catch ex As Exception
1430:       gErrorDesc = ex.Message
1440:       gErrorNumber = Err.Number
1450:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
1460:       gErrorLine = Erl()

1470:       _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)

1480:       Return False
1490:   Finally
1500:       Try
1510:           Me.m_dataDrivenCache.Dispose()
1520:           Me.m_dataDrivenCache = Nothing
1530:           IO.Directory.Delete(tempFolder)
            Catch : End Try

1540:       HTMLPath = ""
            'gScheduleName = ""
        End Try
    End Function
    Private Function createEmbedFile(ByVal nReportID As Integer, ByVal nDestinationID As Integer, ByVal sFormat As String) As String
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String = "SELECT * FROM ReportAttr WHERE ReportID =" & nReportID

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim sReportName As String = oRs("ReportName").Value
                    Dim sUrl As String = oRs("databasepath").Value
                    Dim oRpt As sqlrd.ReportServer.ReportingService
                    Dim nRetry As String = IsNull(oRs("retry").Value, 0)
                    Dim rptTimeout As Integer = IsNull(oRs("assumefail").Value, 30)
                    Dim preserveLinks As Boolean = False

                    If sFormat.ToLower.Contains("html") Then
                        preserveLinks = True
                    End If

                    If rptTimeout = 0 Then rptTimeout = 360

                    Dim sReportPath = IsNull(CType(oRs.Fields("cachepath").Value, String))

                    If Not oRpt Is Nothing Then
                        oRpt = Nothing
                    End If

                    oRpt = New ReportServer.ReportingService
                    greportItem = oRpt

                    oRpt.Timeout = -1 '3600000

                    oRpt.Url = sUrl

                    'set up reportviewer (rv) if server version is 2005
                    If rv IsNot Nothing Then
                        rv = Nothing
                    End If

                    rv = New Microsoft.Reporting.WinForms.ReportViewer
                    rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote

                    Dim serverUrl As String = ""

                    For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
                        serverUrl &= sUrl.Split("/")(I) & "/"
                    Next

                    rv.ServerReport.ReportServerUrl = New Uri(serverUrl)

                    If Me.m_ServerVersion2005 = True Then
                        rv.ServerReport.Timeout = -1 'oRpt.Timeout
                    End If

                    If Me.SetServerLogin(oRpt, nReportID) = False Then
                        Return False
                    End If

                    Dim oLogins() As sqlrd.ReportServer.DataSourceCredentials = Me.CreateDatasources(nReportID)

                    EmbedPath = ProcessReport(sFormat, oRpt, "", nReportID, clsMarsData.CreateDataID("", "", True), sReportName, _
                    sReportPath, oLogins, , , , False, , , rptTimeout, preserveLinks)

                    Return EmbedPath
                End If
            End If
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function performDDMerging(ByVal nID As Integer) As Boolean
        Dim oRsMerge As ADODB.Recordset = clsMarsData.GetData("SELECT mergeallpdf,mergeallxl,mergeallpdfname,mergeallxlname FROM datadrivenattr WHERE packid =" & nID)
        Dim mergePDF, mergeXL As Boolean
        Dim mergePDFName, mergeXLName As String
        Dim SQL As String

1020:   If oRsMerge IsNot Nothing Then
1030:       If oRsMerge.EOF = False Then
1040:           mergePDF = IsNull(oRsMerge("mergeallpdf").Value, False)
1050:           mergeXL = IsNull(oRsMerge("mergeallxl").Value, False)
1060:           mergePDFName = IsNull(oRsMerge("mergeallpdfname").Value)
1070:           mergeXLName = IsNull(oRsMerge("mergeallxlname").Value)

1080:           mergePDFName = clsMarsParser.Parser.ParseString(mergePDFName, , , , , , m_ParametersTable)
1090:           mergeXLName = clsMarsParser.Parser.ParseString(mergeXLName, , , , , , m_ParametersTable)

1100:           If mergePDF = True Then
1110:               Me.PreparePDF(mergePDFName, True, True)

1120:               If mergePDFName.EndsWith(".pdf") = False Then mergePDFName &= ".pdf"

1130:               mergePDFName = clsMarsReport.m_OutputFolder & mergePDFName

                    'do the post pdf processing
                    Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM PackageOptions WHERE PackID = " & nID)

1140:               If oRs1.EOF = False Then
1150:                   Dim oPerm As New clsMarsPDF
                        Dim sOwnerPass As String
                        Dim sUserPass As String
1160:                   Dim oParse As New clsMarsParser

1170:                   Try
                            Dim infoTitle, InfoAuthor, InfoSubject, InfoKeywords, InfoProducer, sWatermark As String
                            Dim InfoCreated As Date

1180:                       sOwnerPass = oRs1("pdfpassword").Value
1190:                       sUserPass = oRs1("userpassword").Value

1200:                       infoTitle = oParse.ParseString(oRs1("infotitle").Value, , , , , , m_ParametersTable)
1210:                       InfoAuthor = oParse.ParseString(oRs1("infoauthor").Value, , , , , , m_ParametersTable)
1220:                       InfoSubject = oParse.ParseString(oRs1("infosubject").Value, , , , , , m_ParametersTable)
1230:                       InfoKeywords = oParse.ParseString(oRs1("infokeywords").Value, , , , , , m_ParametersTable)
1240:                       InfoProducer = oParse.ParseString(oRs1("infoproducer").Value, , , , , , m_ParametersTable)
1250:                       InfoCreated = oRs1("infocreated").Value
1260:                       sWatermark = IsNull(oRs1("pdfwatermark").Value)

1270:                       Me._PostProcessPDF(mergePDFName, infoTitle, InfoAuthor, InfoSubject, _
                            InfoKeywords, InfoProducer, InfoCreated, False, 0, Nothing, _
                            Convert.ToBoolean(oRs1("pdfsecurity").Value), False, oRs1("canprint").Value, _
                            oRs1("cancopy").Value, _
                            oRs1("canedit").Value, _
                            oRs1("cannotes").Value, oRs1("canfill").Value, oRs1("canaccess").Value, _
                            oRs1("canassemble").Value, _
                            oRs1("canprintfull").Value, sOwnerPass, sUserPass, sWatermark, 0)

1280:                       oPerm.SetPDFSummary(infoTitle, InfoAuthor, _
                            InfoSubject, InfoKeywords, InfoCreated, InfoProducer _
                            , mergePDFName)
                        Catch : End Try
                    End If

1290:               oRs1.Close()
1300:           ElseIf mergeXL = True Then
1310:               Me.PrepareXLS(m_OutputFolder & mergeXLName, True, True)

1320:               If mergeXLName.EndsWith(".xls") = False Then mergeXLName &= ".xls"

1330:               mergeXLName = rptFileNames(0)

1340:               Try
                        Dim oRsx As ADODB.Recordset

                        'protect it
1350:                   Sql = "SELECT ProtectExcel, ExcelPassword FROM PackageOptions WHERE PackID = " & nID

1360:                   oRsx = clsMarsData.GetData(Sql)

1370:                   If oRsx.EOF = False Then
1380:                       If IsNull(oRsx.Fields(0).Value, "0") = "1" Then
1390:                           Dim oXL As New ExcelMan.clsExcelMan(ExcelMan.clsExcelMan.XLFormats.Excel8)
1400:                           oXL.SetWorkBookPassword(mergeXLName, _DecryptDBValue(oRsx(1).Value))
1410:                           oXL.Dispose()
                            End If
                        End If

1420:                   oRsx.Close()
                    Catch : End Try
                End If
            End If
        End If
    End Function

    Private Function processDataDrivenDestinations(ByVal nID As Integer, _
           ByVal pathsList As DataTable, ByVal runTaskPerSchedule As Boolean, ByVal groupReports As Boolean, _
           ByVal oType As clsMarsScheduler.enScheduleType) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nDestinationID As Integer
        Dim ok As Boolean = False
        Dim appendToFile As Boolean
        Dim nReportID As Integer = 0
        Dim nPackID As Integer = 0
        Dim GroupOutput As String = ""
        Dim keyColumn, keyValue As String

        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Processing data driven destinations", True)

        keyColumn = ""
        keyValue = ""

        Try
            pathsList.WriteXml(sAppPath & "\Logs\datadriven_" & gScheduleName & ".xml")
        Catch : End Try

        Try
            If oType = clsMarsScheduler.enScheduleType.REPORT Then
                oRs = clsMarsData.GetData("SELECT KeyColumn FROM DataDrivenAttr WHERE ReportID = " & nID)
            Else
                oRs = clsMarsData.GetData("SELECT KeyColumn FROM DataDrivenAttr WHERE PackID = " & nID)
            End If

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    keyColumn = IsNull(oRs(0).Value)
                End If
            End If

36:         For I As Integer = 0 To Me.m_dataDrivenCache.Rows.Count - 1
                m_ddKeyColumn = ""
                m_ddKeyValue = ""

                If keyColumn <> "" Then
                    Try
37:                     keyValue = clsMarsParser.Parser.ParseString("<[r]" & keyColumn & ">", , , , , , m_ParametersTable)
                    Catch ex As Exception
38:                     keyValue = "[Unavailable]"
                    End Try
                Else
                    keyValue = "[Unavailable]"
                End If

39:             m_ddKeyColumn = keyColumn
40:             m_ddKeyValue = keyValue

41:             clsMarsUI.BusyProgress((I / Me.m_dataDrivenCache.Rows.Count) * 100, "Processing report " & (I + 1) & " of " & Me.m_dataDrivenCache.Rows.Count)

50:             m_pointer = I

                clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "***************Processing destinations for " & keyValue & "**************************", True)

55:             If oType = clsMarsScheduler.enScheduleType.REPORT Then
60:                 SQL = "SELECT * FROM DestinationAttr WHERE ReportID =" & nID & " AND EnabledStatus =1"
                    nReportID = nID
                Else
70:                 SQL = "SELECT * FROM DestinationAttr WHERE PackID =" & nID & " AND EnabledStatus =1"
                    nPackID = nID
                End If

75:             oRs = clsMarsData.GetData(SQL)

                Dim emailFieldCount As Integer = 0

80:             Do While oRs.EOF = False
                    Dim sSendTo As String = ""
                    Dim sSubject, senderName, senderAddress As String
                    Dim sCc As String
                    Dim sBcc As String
                    Dim sExtras As String
                    Dim sMsg As String
                    Dim sName As String
                    Dim sPath As String
                    Dim Embed As Boolean
                    Dim MailFormat As String
                    Dim ScheduleType As String = "Single"
                    Dim nDeferBy As Double
                    Dim SMTPServer As String
                    Dim ReadReceipt As Boolean = False
                    Dim IncludeAttach As Boolean
                    Dim compress As Boolean = False
                    Dim sPackage As String = gScheduleName
                    Dim row As DataRow
90:                 Dim sFormat As String = oRs("outputformat").Value

100:                nDestinationID = oRs("destinationid").Value
                    Dim destName As String = oRs("destinationname").Value

                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Destination Name: " & destName, True)

110:                Try
120:                    IncludeAttach = Me.DoIncludeAttach(nDestinationID)
130:                Catch ex As Exception
140:                    IncludeAttach = True
                    End Try

150:                Try
160:                    compress = oRs("compress").Value
170:                Catch ex As Exception
180:                    compress = False
                    End Try

                    '190:                If IncludeAttach = False Then
                    '200:                    pathsList = Nothing
                    '                    End If

                    Try
                        Embed = Convert.ToBoolean(oRs("embed").Value)
                    Catch ex As Exception
                        Embed = False
                    End Try
210:

220:                Dim rows() As DataRow

                    If keyValue = "[Unavailable]" Then
                        rows = pathsList.Select("DestinationID =" & nDestinationID)
                    Else
                        rows = pathsList.Select("DestinationID ='" & nDestinationID & "' AND KeyValue ='" & SQLPrepare(keyValue) & "'")
                    End If

230:                If rows.Length > 0 Then
                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Found " & rows.Length & " rows" & destName, True)

240:                    Dim x As Integer = 0

250:                    rptFileNames = Nothing

                        If keyValue = "[Unavailable]" Then
260:                        Try
270:                            row = rows(I)
280:                        Catch
290:                            GoTo noData
300:                        End Try
                        Else
                            Try
301:                            row = rows(0)
302:                        Catch
303:                            GoTo noData
304:                        End Try
                        End If

310:                    Dim s As String = row("Directory") 'pathsList(I)

                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Reading files from '" & s & "'", True)

320:                    For Each file As String In IO.Directory.GetFiles(s)
330:                        ReDim Preserve rptFileNames(x)

340:                        rptFileNames(x) = file

350:                        x += 1
360:                    Next

                        GroupOutput = IsNull(row("groupoutput"))
                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Group output: '" & GroupOutput & "'", True)
370:                Else
                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Found no data in data table", True)
380:                    ok = True
390:                    GoTo noData
400:                End If

410:                If compress = True Then

420:                    clsMarsUI.BusyProgress(60, "Zipping reports...")

430:                    Dim Encrypt As Boolean
                        Dim ZipCode As String = ""
                        Dim strNow As String
                        Dim sExt As String

440:                    Try
450:                        If oRs("appenddatetime").Value = 1 Then
460:                            strNow = Date.Now.ToString(oRs("datetimeformat").Value)
470:                        Else
480:                            strNow = String.Empty
490:                        End If

500:                        If IsNull(oRs("customname").Value, "") <> "" Then
510:                            sPackage = oRs("customname").Value
                            End If

520:                        If sPackage = "" Then sPackage = gScheduleName

530:                        sPackage = clsMarsParser.Parser.ParseString(sPackage, , , , , , m_ParametersTable)

540:                        If IsNull(oRs("customext").Value, "") <> "" Then
550:                            sExt = oRs("customext").Value
560:                        Else
570:                            sExt = ".zip"
580:                        End If

590:                        If sExt.StartsWith(".") = False Then
600:                            sExt = "." & sExt
                            End If
                        Catch : End Try

610:                    Try
620:                        Encrypt = oRs("encryptzip").Value
630:                        ZipCode = IsNull(oRs("encryptzipcode").Value, "")
                        Catch : Encrypt = False : End Try

640:                    ZipFiles("Package", m_OutputFolder & sPackage & strNow & sExt, , Encrypt, ZipCode)

650:                    ReDim rptFileNames(0)

660:                    rptFileNames(0) = m_OutputFolder & sPackage & strNow & sExt
                    End If


670:                Try
680:                    gExportedFileName = ""

690:                    For Each s As String In rptFileNames
700:                        gExportedFileName &= ExtractFileName(s) & vbCrLf
710:                    Next

720:                    gExportedFileName = gExportedFileName.Substring(0, gExportedFileName.Length - 2)
                    Catch : End Try

                    ' If I = 0 Then Return True

730:                Dim nDefer As Integer

740:                Try
750:                    nDefer = oRs("deferdelivery").Value
760:                Catch ex As Exception
770:                    nDefer = 0
                    End Try

780:                If nDefer = 1 And RunEditor = False Then
790:                    Dim fileCollection As String = ""
800:                    nDeferBy = oRs("deferby").Value

810:                    For Each s As String In rptFileNames
820:                        fileCollection &= s & "|"
830:                    Next

840:                    fileCollection = fileCollection.Substring(0, fileCollection.Length - 1)

850:                    ok = Me._DeferDelivery(nDestinationID, fileCollection, nDeferBy, sPackage)
860:                Else
                        Try
870:                        Select Case CType(oRs("DestinationType").Value, String).ToLower

                                Case "printer"
880:                                clsMarsUI.BusyProgress(75, "Printing reports...")
                                    ok = Me._PrintServer(rptFileNames, nDestinationID)
890:                            Case "email"

900:                                sSendTo = ResolveEmailAddress(row("sendto"))
910:                                sCc = ResolveEmailAddress(row("cc"))
920:                                sBcc = ResolveEmailAddress(row("bcc"))
                                    senderName = clsMarsParser.Parser.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
921:                                senderAddress = clsMarsParser.Parser.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)

930:                                If sSendTo = "" Then GoTo noData

                                    'pdf merging
931:                                If oType = clsMarsScheduler.enScheduleType.PACKAGE Then
932:                                    If groupReports = True Then
                                            performDDMerging(nID)
                                        End If
                                    End If

                                    Dim emailValue As String = sSendTo
                                    Dim emailField As String
                                    Dim usePointer As Boolean

940:                                If emailValue.EndsWith(";") Then
950:                                    emailValue = emailValue.Remove(emailValue.Length - 1, 1)
                                    End If

                                    Dim emailrows() As DataRow = Me.m_emailTable.Select("EmailValue ='" & SQLPrepare(emailValue) & "'")

960:                                If emailrows.Length = 0 Then
970:                                    usePointer = True
                                    ElseIf groupReports = False Then
                                        usePointer = True
980:                                Else
990:                                    Try
                                            Dim emailrow As DataRow = emailrows(0)

1000:                                       emailField = emailrow("EmailField")
1010:                                   Catch ex As Exception
1020:                                       usePointer = True
                                        End Try
                                    End If

                                    sSendTo = clsMarsParser.Parser.ParseString(sSendTo, , , usePointer, emailField, emailValue, m_ParametersTable)

                                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Send to'" & sSendTo & "'", True)

1030:                               Try
1040:                                   sSubject = clsMarsParser.Parser.ParseString(oRs("subject").Value, , , usePointer, emailField, emailValue, m_ParametersTable)
1050:                               Catch ex As Exception
1060:                                   sSubject = clsMarsParser.Parser.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
                                    End Try

                                    senderName = clsMarsParser.Parser.ParseString(IsNull(oRs("sendername").Value), , , , , , m_ParametersTable)
                                    senderAddress = clsMarsParser.Parser.ParseString(IsNull(oRs("senderaddress").Value), , , , , , m_ParametersTable)

1070:                               SMTPServer = IsNull(oRs("smtpserver").Value, "Default")

1080:                               sExtras = oRs("extras").Value

1090:                               Try
1100:                                   sMsg = clsMarsParser.Parser.ParseString(oRs("message").Value, , , usePointer, emailField, emailValue, m_ParametersTable)
1110:                               Catch
1120:                                   sMsg = clsMarsParser.Parser.ParseString(oRs("message").Value, , , , , , m_ParametersTable)
                                    End Try

1130:                               Try
1140:                                   ReadReceipt = Convert.ToBoolean(oRs("readreceipts").Value)
1150:                               Catch ex As Exception
1160:                                   ReadReceipt = False
                                    End Try

1170:                               clsMarsUI.BusyProgress(75, "Emailing report...")

                                    'email embedding
                                    Try
2500:                                   If Embed = True Then
                                            MailFormat = IsNull(oRs("mailformat").Value)

2510:                                       If MailType = MarsGlobal.gMailType.MAPI Or MailType = gMailType.GROUPWISE Then
2520:                                           MailFormat = "TEXT"
                                            End If

2530:                                       If MailFormat = "HTML" Or MailFormat = "HTML (Basic)" Then
2540:                                           EmbedPath = Me.createEmbedFile(nReportID, nDestinationID, "HTML (*.htm)")
2550:                                       Else
2560:                                           EmbedPath = Me.createEmbedFile(nReportID, nDestinationID, "Text (*.txt)")
                                            End If

2570:                                       If (EmbedPath = "{skip}" Or EmbedPath.Length = 0) And IncludeAttach = False Then
                                                GoTo noData
2580:                                           ok = True
2590:                                       End If
2600:                                   End If
                                    Catch : End Try

                                    clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Sending emails", True)

1180:                               If MailType = MarsGlobal.gMailType.MAPI Then
1190:                                   ok = SendMAPI( _
                                             sSendTo, sSubject, sMsg, "Package", _
                                             , I, sExtras, sCc, sBcc, Embed, , sPackage, , EmbedPath, ReadReceipt, nDestinationID)
1200:                               ElseIf MailType = MarsGlobal.gMailType.SMTP Or MailType = MarsGlobal.gMailType.SQLRDMAIL Then
                                        Dim numAttach As Integer

                                        Try
                                            numAttach = rptFileNames.Length
                                        Catch ex As Exception
                                            numAttach = 0
                                        End Try

                                        Dim objSender As clsMarsMessaging = New clsMarsMessaging


1210:                                   ok = objSender.SendSMTP( _
                                             sSendTo, sSubject, sMsg, "Package", _
                                             , numAttach, sExtras, sCc, sBcc, sPackage, Embed, , , IncludeAttach, IsNull(oRs("MailFormat").Value), _
                                             SMTPServer, EmbedPath, senderName, senderAddress)
1220:                               ElseIf MailType = gMailType.GROUPWISE Then
1230:                                   ok = SendGROUPWISE(sSendTo, sCc, sBcc, sSubject, sMsg, "", "Package", sExtras, _
                                             True, False, "", True, I, sPackage, "")
1240:                               Else
1250:                                   Throw New Exception("No messaging configuration found. Please set up your messaging options in 'Options'")
                                    End If

1260:                           Case "disk"
                                    Dim HouseTemp As String
1270:                               Dim oSys As New clsSystemTools
                                    Dim UseDUN As Boolean
                                    Dim sDUN As String
                                    Dim oNet As clsNetworking
                                    'if DUN is used...
1280:                               Try
1290:                                   UseDUN = Convert.ToBoolean(oRs("usedun").Value)
1300:                                   sDUN = IsNull(oRs("dunname").Value)
1310:                               Catch ex As Exception
1320:                                   UseDUN = False
                                    End Try

1330:                               If UseDUN = True Then
1340:                                   oNet = New clsNetworking

1350:                                   If oNet._DialConnection(sDUN, "DD Schedule Error: " & sPackage & ": ") = False Then
1360:                                       Throw New Exception(Me._CreateErrorString)
                                        End If
                                    End If

1370:                               SQL = "SELECT AppendToFile FROM ReportOptions WHERE DestinationID =" & nDestinationID

1380:                               Dim rsAppend As ADODB.Recordset = clsMarsData.GetData(SQL)

1390:                               If rsAppend IsNot Nothing Then
1400:                                   If rsAppend.EOF = False Then
1410:                                       appendToFile = IsNull(rsAppend("appendtofile").Value, 0)
1420:                                   End If
                                    End If

1430:                               Select Case sFormat
                                        Case "CSV (*.csv)", "Tab Separated (*.txt)", "Text (*.txt)", "Record Style (*.rec)"
1440:                                   Case Else
1450:                                       appendToFile = False
                                    End Select

                                    If oType = clsMarsScheduler.enScheduleType.PACKAGE Then
                                        If groupReports = True Then
                                            performDDMerging(nID)
                                        End If
                                    End If

1460:                               clsMarsUI.BusyProgress(75, "Copying reports...")

1470:                               For Each oFile As String In rptFileNames
1480:                                   sPath = IsNull(oRs("outputpath").Value, "")

                                        If groupReports = True Then
                                            If GroupOutput <> "" Then
                                                sPath = GroupOutput
                                            End If
                                        End If

                                        GroupOutput = ""

1490:                                   If sPath.EndsWith("|") Then sPath = sPath.Substring(0, sPath.Length - 1)

1500:                                   If sPath = "" Then GoTo noData

                                        sPath = clsMarsParser.Parser.ParseString(sPath, , , , , , m_ParametersTable)
                                        clsMarsDebug.writeToDebug("datadriven_" & gScheduleName, "Destination path: " & sPath, True)

1510:                                   For Each item As String In sPath.Split("|")
1520:                                       If item IsNot Nothing And item.Length > 0 Then
1530:                                           sPath = clsMarsParser.Parser.ParseString(item, , , , , , m_ParametersTable)

1540:                                           Dim sFileName As String = ExtractFileName(oFile)

1550:                                           If sPath.EndsWith("\") = False Then sPath &= "\"

1560:                                           sPath = _CreateUNC(sPath)

1570:                                           ok = clsMarsParser.Parser.ParseDirectory(sPath)

1580:                                           HouseTemp = sPath

1590:                                           sPath &= sFileName

1600:                                           If appendToFile = True And IO.File.Exists(sPath) Then
1610:                                               Dim oRead As String = ReadTextFromFile(oFile)

1620:                                               SaveTextToFile(oRead, sPath, , True, True)
1630:                                           Else
1640:                                               System.IO.File.Copy(oFile, sPath, True)
1650:                                           End If

1660:                                           Try
1670:                                               oSys._HouseKeeping(nDestinationID, HouseTemp, sFileName.Split(".")(0))
                                                Catch : End Try
                                            End If
1680:                                   Next
1690:                               Next

1700:                               Try
1710:                                   If UseDUN = True Then
1720:                                       oNet._Disconnect()
                                        End If
                                    Catch : End Try

1730:                           Case "ftp"
                                    Dim sFtp As String = ""
1740:                               Dim oFtp As New clsMarsTask
                                    Dim FTPServer As String
                                    Dim FTPUser As String
                                    Dim FTPPassword As String
                                    Dim FTPPath As String
                                    Dim FTPType As String
                                    Dim FtpPassives As String
                                    Dim FtpOptions As String

1750:                               FTPServer = clsMarsParser.Parser.ParseString(IsNull(oRs("ftpserver").Value), , , , , , m_ParametersTable)
1760:                               FTPUser = clsMarsParser.Parser.ParseString(oRs("ftpusername").Value, , , , , , m_ParametersTable)
1770:                               FTPPassword = clsMarsParser.Parser.ParseString(oRs("ftppassword").Value, , , , , , m_ParametersTable)
1780:                               FTPPath = clsMarsParser.Parser.ParseString(oRs("ftppath").Value, , , , , , m_ParametersTable)
1790:                               FTPType = IsNull(oRs("ftptype").Value, "FTP")
                                    FtpPassives = IsNull(oRs("ftppassive").Value, "")
                                    FtpOptions = IsNull(oRs("ftpoptions").Value, "")
                                    Dim ftpCount As Integer = FTPServer.Split("|").GetUpperBound(0)

1800:                               If ftpCount > 0 Then
1810:                                   ftpCount -= 1
1820:                               Else
1830:                                   ftpCount = 0
                                    End If

1840:                               If FTPServer = "" Or FTPUser = "" Or FTPPassword = "" Then GoTo noData


1850:                               clsMarsUI.BusyProgress(75, "Uploading reports...")

1860:                               For z As Integer = 0 To ftpCount
                                        Dim l_FTPServer As String = FTPServer.Split("|")(z)
                                        Dim l_FTPUser As String = FTPUser.Split("|")(z)
                                        Dim l_FTPPassword As String = _DecryptDBValue(FTPPassword.Split("|")(z))
                                        Dim l_FTPPath As String = FTPPath.Split("|")(z)
                                        Dim l_FTPType As String = FTPType.Split("|")(z)
                                        Dim l_FTPPort As Integer
                                        Dim l_FtpOptions As String
                                        Dim l_FtpPassive As Boolean

                                        Try
1861:                                       l_FtpOptions = FtpOptions.Split("|")(I)
                                        Catch
                                            l_FtpOptions = ""
                                        End Try
                                        Try
1862:                                       l_FtpPassive = Convert.ToBoolean(Convert.ToInt32(FtpPassives.Split("|")(I)))
                                        Catch ex As Exception
1863:                                       l_FtpPassive = False
                                        End Try

1870:                                   If l_FTPServer = "" Then Continue For

                                        If l_FTPServer.IndexOf(":") > -1 Then
1880:                                       Try
                                                l_FTPPort = l_FTPServer.Split(":")(l_FTPServer.Split(":").GetUpperBound(0))
1890:                                           l_FTPServer = l_FTPServer.Substring(0, l_FTPServer.Length - (CType(l_FTPPort, String).Length + 1))
1900:                                       Catch
1910:                                           l_FTPPort = 21
                                            End Try
1920:                                   Else
1930:                                       l_FTPPort = 21
                                        End If

1940:                                   For Each oFile As String In rptFileNames
1950:                                       sFtp &= oFile & "|"
1960:                                   Next

1970:                                   oFtp.FTPUpload2(l_FTPServer, l_FTPPort, l_FTPUser, l_FTPPassword, l_FTPPath, sFtp, l_FTPType, l_FtpOptions, , , l_FtpPassive)

1980:                                   ok = True

1990:                               Next
2000:                           Case "fax"

                                    Dim faxNumber As String
                                    Dim faxDevice As String
                                    Dim faxTo As String
                                    Dim faxFrom As String
                                    Dim faxComments As String
                                    Dim faxer As clsMarsMessaging = New clsMarsMessaging

2010:                               faxDevice = clsMarsParser.Parser.ParseString(oRs("subject").Value, , , , , , m_ParametersTable)
2020:                               faxTo = clsMarsParser.Parser.ParseString(oRs("cc").Value, , , , , , m_ParametersTable)
2030:                               faxFrom = clsMarsParser.Parser.ParseString(oRs("bcc").Value, , , , , , m_ParametersTable)
2040:                               faxComments = clsMarsParser.Parser.ParseString(oRs("message").Value, , , , , , m_ParametersTable)
2050:                               faxNumber = oRs("sendto").Value

2060:                               If faxNumber = "" Then GoTo noData

2070:                               ok = faxer.SendFax(faxNumber, faxDevice, "Package", faxTo, faxFrom, faxComments, "")

2080:                           Case "sms"
                                    Dim cellNumber As String
                                    Dim textMsg As String

2090:                               textMsg = clsMarsParser.Parser.ParseString(oRs("message").Value, , , , , , m_ParametersTable)

2100:                               cellNumber = clsMarsParser.Parser.ParseString(oRs("sendto").Value, , , , , , m_ParametersTable)

2110:                               If cellNumber = "" Then GoTo noData

2120:                               For Each s As String In rptFileNames
2130:                                   ok = SendSMS(cellNumber, textMsg, s)
2140:                               Next
                                Case "sharepoint"
                                    Dim sp As SharePointer.clsSharePoint = New SharePointer.clsSharePoint
                                    Dim oParse As clsMarsParser = New clsMarsParser

                                    Dim spServer As String
                                    Dim spUser As String
                                    Dim spPassword As String
                                    Dim spLib As String
                                    Dim spCount As Integer = 0

                                    spServer = oParse.ParseString(IsNull(oRs("ftpserver").Value))
                                    spUser = oParse.ParseString(oRs("ftpusername").Value)
                                    spPassword = oParse.ParseString(oRs("ftppassword").Value)
                                    spLib = oParse.ParseString(oRs("ftppath").Value)

                                    clsMarsUI.MainUI.BusyProgress(75, "Uploading report...")

                                    spCount = spServer.Split("|").GetUpperBound(0)

                                    If spCount > 0 Then
                                        spCount -= 1
                                    Else
                                        spCount = 0
                                    End If

                                    For z As Integer = 0 To spCount
                                        Dim l_spServer As String = spServer.Split("|")(z)
                                        Dim l_spUser As String = spUser.Split("|")(z)
                                        Dim l_spPassword As String = _DecryptDBValue(spPassword.Split("|")(z))
                                        Dim l_spLib As String = spLib.Split("|")(z)
                                        Dim errorInfo As Exception = Nothing

                                        If l_spServer = "" Then Continue For

                                        ok = sp.UploadMultipleDocuments(l_spServer, l_spLib, rptFileNames, l_spUser, l_spPassword, errorInfo)

                                        If ok = False And errorInfo IsNot Nothing Then
                                            Throw errorInfo
                                        End If
                                    Next
                                    ok = True
                            End Select
                        Catch ex As Exception
                            _ErrorHandle("Destination processing error - '" & destName & "'. Key Value: " & Me.m_ddKeyValue & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "Make sure all the Data-Driven inserts are correct", True, True)
                        End Try
                    End If

2150:               If runTaskPerSchedule = False And ok = True Then oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

noData:
2160:               oRs.MoveNext()
2170:           Loop
2180:           oRs.Close()
2190:       Next

2200:       Return ok
2210:   Catch ex As Exception
2220:       gErrorDesc = ex.Message
2230:       gErrorNumber = Err.Number
2240:       gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
2250:       gErrorLine = Erl()
2260:       Return False
        End Try
    End Function

    Public Shared Function populateDataDrivenCache(ByVal sQuery As String, ByVal conString As String, _
    ByVal donotraiseNoDataError As Boolean) As Integer
        Try
            Dim oRs As ADODB.Recordset
            Dim oCon As ADODB.Connection
            Dim I As Integer = 0

            clsMarsUI.BusyProgress(50, "Populating data cache...")
            oRs = New ADODB.Recordset
            oCon = New ADODB.Connection

            Dim DSN, user, password As String

            DSN = conString.Split("|")(0)
            user = conString.Split("|")(1)
            password = _DecryptDBValue(conString.Split("|")(2))

            oCon.Open(DSN, user, password)

            sQuery = clsMarsParser.Parser.ParseString(sQuery) ', , , , , , m_ParametersTable)

            oRs.Open(sQuery, oCon)

            If oRs.EOF = True And donotraiseNoDataError = False Then
                Throw New Exception("The data driver did not bring back any data")
            ElseIf oRs.EOF = True And donotraiseNoDataError = True Then
                Return 0
            End If


            clsMarsReport.m_dataDrivenCache = New DataTable

            For Each fld As ADODB.Field In oRs.Fields
                clsMarsReport.m_dataDrivenCache.Columns.Add(fld.Name)
            Next

            Do While oRs.EOF = False
                Dim row As DataRow = clsMarsReport.m_dataDrivenCache.Rows.Add

                For Each fld As ADODB.Field In oRs.Fields
                    row(fld.Name) = IsNull(fld.Value)
                Next

                I += 1

                oRs.MoveNext()
            Loop

            oRs.Close()
            oCon.Close()
            oCon = Nothing

            clsMarsReport.m_pointer = 0

            Return I
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please check your Data Driver's DSN and SQL definition")
            Return 0
        Finally
            clsMarsUI.BusyProgress(, , True)
        End Try
    End Function

    Public Shared Function getPackagedReports(ByVal packID As Integer) As ArrayList
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim idList As ArrayList = New ArrayList

        SQL = "SELECT ReportID FROM ReportAttr WHERE PackID =" & packID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return Nothing

        Do While oRs.EOF = False
            idList.Add(oRs(0).Value)

            oRs.MoveNext()
        Loop

        oRs.Close()

        Return idList
    End Function

    Public Shared Function moveintoPackage(ByVal packID As Integer, ByVal reportID() As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim orderID As Integer = 0
        Dim cols As String
        Dim vals As String

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & packID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            orderID = 0
        ElseIf oRs.EOF = True Then
            orderID = 0
        Else
            orderID = IsNull(oRs(0).Value, 0)
        End If

        orderID += 1

        For Each n As Integer In reportID
            SQL = "UPDATE ReportAttr SET " & _
            "Parent = 0," & _
            "PackID =" & packID & "," & _
            "ReportTitle ='" & orderID & ":' + ReportTitle, " & _
            "PackOrderID =" & orderID & _
            " WHERE ReportID = " & n

            clsMarsData.WriteData(SQL)

            cols = "AttrID,PackID,ReportID,OutputFormat,CustomExt," & _
               "AppendDateTime,DateTimeFormat,CustomName,Status,AdjustStamp"

            vals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
            packID & "," & _
            n & "," & _
            "OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,EnabledStatus,AdjustStamp"

            SQL = "INSERT INTO PackagedReportAttr (" & cols & ") SELECT TOP 1 " & vals & " " & _
            "FROM DestinationAttr WHERE ReportID =" & n

            clsMarsData.WriteData(SQL)

            'destinationattr
            Dim rsD As ADODB.Recordset = clsMarsData.GetData("SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & n)
            Dim destID As Integer = 0

            If rsD IsNot Nothing Then
                Do While rsD.EOF = False
                    destID = rsD(0).Value
                    clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID = 0, ReportID =" & n & " WHERE DestinationID =" & destID)
                    clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE ReportID =" & n)
                    rsD.MoveNext()
                Loop

                rsD.Close()
            End If

            'data driven data
            clsMarsData.WriteData("DELETE FROM DataDrivenAttr WHERE ReportID =" & n)

            Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(n)
            Dim where As String = " WHERE ScheduleID =" & scheduleID
            clsMarsData.WriteData("DELETE FROM ScheduleOptions " & where)
            clsMarsData.WriteData("DELETE FROM Tasks " & where)
            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE ReportID =" & n)
            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID =" & n)
            clsMarsData.WriteData("DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM " & _
            "ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory)", False)

            orderID += 1
        Next
    End Function

    Public Shared Function convertReportstoPackage(ByVal list As Hashtable, ByVal templateID As Integer) As Boolean

        Try

            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(templateID)
            Dim packID As Integer = clsMarsData.CreateDataID("packageattr", "packid")
            Dim cols As String
            Dim vals As String
            Dim I As Integer = 1
            Dim packageName As String = ""
            Dim retry As DialogResult

            clsMarsUI.BusyProgress(10, "Converting to package...")

            oRs = clsMarsData.GetData("SELECT * FROM ReportAttr WHERE ReportID =" & templateID)

            If oRs Is Nothing Then Return False

            If oRs.EOF = False Then

                packageName = oRs("reporttitle").Value

retryPoint:

                If clsMarsData.IsDuplicate("PackageAttr", "PackageName", packageName, True) = True Then
                    retry = MessageBox.Show("A package with that name already exists. Would you like to specify another name?", _
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                    If retry = DialogResult.No Then
                        Return False
                    Else
                        Dim x As Integer = 1

                        Do While clsMarsData.IsDuplicate("PackageAttr", "PackageName", packageName & " " & x, True) = True
                            x += 1
                        Loop

                        packageName = InputBox("Please enter the name of the new package", Application.ProductName, packageName & " " & x)

                        GoTo retryPoint
                    End If
                End If

                cols = "PackID,PackageName,Parent,Retry,AssumeFail," & _
                "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName,DateTimeStamp,StampFormat,Dynamic,AdjustPackageStamp"

                vals = packID & "," & _
                "'" & SQLPrepare(packageName) & "'," & _
                oRs("parent").Value & "," & _
                oRs("retry").Value & "," & _
                oRs("assumefail").Value & "," & _
                oRs("checkblank").Value & "," & _
                "'" & SQLPrepare(oRs("owner").Value) & "'," & _
                1 & "," & _
                "0," & _
                "0," & _
                "''," & _
                "''," & _
                "0," & _
                "''," & _
                "0," & _
                "0"

                SQL = "INSERT INTO PackageAttr(" & cols & ") VALUES (" & vals & ")"

                clsMarsUI.BusyProgress(20, "Converting to package...")

                If clsMarsData.WriteData(SQL) = True Then
                    For Each entry As DictionaryEntry In list
                        Dim reportID As Integer = entry.Value
                        Dim tmpScheduleID As Integer = clsMarsScheduler.GetScheduleID(reportID)
                        Dim reportTitle As String = I & ":" & clsMarsScheduler.globalItem.GetScheduleName(reportID, clsMarsScheduler.enScheduleType.REPORT)

                        SQL = "UPDATE ReportAttr SET " & _
                        "PackID =" & packID & ", " & _
                        "ReportTitle ='" & SQLPrepare(reportTitle) & "', " & _
                        "PackOrderID =" & I & " WHERE ReportID =" & reportID

                        clsMarsData.WriteData(SQL)

                        clsMarsUI.BusyProgress(65, "Converting to package...")

                        'scheduleattr
                        If reportID <> templateID Then
                            clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & tmpScheduleID)
                            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ScheduleID = " & tmpScheduleID)
                            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE ReportID =" & reportID)
                        Else
                            clsMarsData.WriteData("UPDATE ScheduleAttr SET ReportID = 0, PackID =" & packID & " WHERE ScheduleID =" & scheduleID)
                            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE ReportID =" & reportID)
                        End If

                        cols = "AttrID,PackID,ReportID,OutputFormat,CustomExt," & _
                           "AppendDateTime,DateTimeFormat,CustomName,Status,AdjustStamp"

                        vals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
                        packID & "," & _
                        reportID & "," & _
                        "OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,EnabledStatus,AdjustStamp"

                        clsMarsUI.BusyProgress(75, "Converting to package...")

                        SQL = "INSERT INTO PackagedReportAttr (" & cols & ") SELECT TOP 1 " & vals & " " & _
                        "FROM DestinationAttr WHERE ReportID =" & reportID

                        clsMarsData.WriteData(SQL)

                        'destinationattr
                        Dim rsD As ADODB.Recordset = clsMarsData.GetData("SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & reportID)
                        Dim destID As Integer = 0

                        If rsD IsNot Nothing Then
                            Do While rsD.EOF = False
                                destID = rsD(0).Value

                                clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID = 0, ReportID =" & reportID & " WHERE DestinationID =" & destID)

                                clsMarsUI.BusyProgress(85, "Converting to package...")

                                If reportID <> templateID Then
                                    clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE ReportID =" & reportID)
                                Else
                                    clsMarsData.WriteData("UPDATE DestinationAttr SET ReportID = 0, PackID =" & packID & ", OutputFormat = 'Package' WHERE ReportID = " & templateID)
                                End If

                                rsD.MoveNext()
                            Loop

                            rsD.Close()
                        End If

                        'data driven data
                        If reportID = templateID Then
                            clsMarsData.WriteData("UPDATE DataDrivenAttr SET packid = " & packID & ", reportid = 0 WHERE reportid =" & reportID)
                        Else
                            clsMarsData.WriteData("DELETE FROM DataDrivenAttr WHERE ReportID =" & reportID)
                        End If

                        I += 1
                    Next
                End If
            End If

            oRs.Close()

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        Finally
            clsMarsUI.BusyProgress(95, "Converting to package...")

            clsMarsUI.BusyProgress(10, "Converting to package...", True)
        End Try

    End Function

    Private Function validateFormat(ByRef format As String) As Boolean
        Try
            Dim s() As String = New String() {"Acrobat Format (*.pdf)", "CSV (*.csv)", _
            "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", _
            "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", _
            "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", _
            "MS Excel 97-2000 (*.xls)", _
            "Web Archive (*.mhtml)", _
            "TIFF (*.tif)", "XML (*.xml)", "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Text (*.txt)", "ODBC (*.odbc)"}

            
            Dim sl() As String = s

            Dim I As Integer = 0
            ReDim s(I)

            For Each x As String In sl
                ReDim Preserve s(I)

                s(I) = x.ToLower
                I += 1
            Next

            Dim tempFormat As String = format.Split("(")(0)

            Dim ext As String = ""

            Try
                ext = format.Split("(")(1)
            Catch : End Try

            'tempFormat = StrConv(tempFormat, VbStrConv.ProperCase)

            format = tempFormat & "(" & ext


            If s.IndexOf(s, format) = -1 And s.IndexOf(s, format.ToLower) = -1 Then
                If format.ToLower Like "*acrobat*" Or format.ToLower Like "*pdf*" Then
                    format = "Acrobat Format (*.pdf)"
                    Return True
                ElseIf format.ToLower Like "*web*" Or format.ToLower Like "*mhtml*" Then
                    format = "Web Archive (*.mhtml)"
                    Return True
                ElseIf format.ToLower Like "*csv*" Or format.ToLower Like "*character*" Then
                    format = "CSV (*.csv)"
                    Return True
                ElseIf format.ToLower Like "*html*" Or format.ToLower Like "*web*" Then
                    format = "HTML (*.htm)"
                    Return True
                ElseIf format.ToLower Like "*excel*" Or format.ToLower Like "*xls*" Then
                    format = "MS Excel 97-2000 (*.xls)"
                    Return True
                ElseIf format.ToLower Like "*word*" Or format.ToLower Like "*doc*" Then
                    format = "MS Word (*.doc)"
                    Return True
                ElseIf format.ToLower Like "*rich*" Or format.ToLower Like "*rtf*" Then
                    format = "Rich Text Format (*.rtf)"
                    Return True
                ElseIf format.ToLower Like "*tif*" Then
                    format = "TIFF (*.tif)"
                    Return True
                ElseIf format.ToLower Like "*xml*" Then
                    format = "XML (*.xml)"
                    Return True
                ElseIf format.ToLower Like "*text*" Or format.ToLower Like "*txt*" Then
                    format = "Text (*.txt)"
                    Return True
                ElseIf format.ToLower Like "*separated*" Then
                    format = "Tab Separated (*.txt)"
                    Return True
                ElseIf format.ToLower Like "*interchange*" Or format.ToLower Like "*dif*" Then
                    format = "Data Interchange Format (*.dif)"
                    Return True
                ElseIf format.ToLower Like "*lotus*" Or format.ToLower Like "*wk*" Then
                    format = "Lotus 1-2-3 (*.wks)"
                    Return True
                ElseIf format.ToLower Like "*dbase*" Or format.ToLower Like "*dbf*" Then
                    format = "dBase IV (*.dbf)"
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function getScheduleRetryInterval(ByVal scheduleID As Integer, ByVal type As clsMarsScheduler.enScheduleType) As Integer
        Dim table As String = ""
        Dim key As String = ""
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer

        Select Case type
            Case clsMarsScheduler.enScheduleType.REPORT
                table = "ReportAttr"
                key = "ReportID"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                table = "PackageAttr"
                key = "PackID"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                table = "EventAttr6"
                key = "EventID"
            Case Else
                Return 0
        End Select

        If type = clsMarsScheduler.enScheduleType.EVENTBASED Then
            nID = scheduleID
        Else
            SQL = "SELECT " & key & " FROM ScheduleAttr WHERE ScheduleID =" & scheduleID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return 0

            If oRs.EOF = True Then Return 0

            nID = oRs(0).Value

            oRs.Close()
        End If

        SQL = "SELECT RetryInterval FROM " & table & " WHERE " & key & " = " & nID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return 0

        If oRs.EOF = True Then Return 0

        Dim val As Integer = IsNull(oRs(0).Value, 0)

        oRs.Close()

        Return val
    End Function

    Public Shared Sub LogRetry(ByVal scheduleID As Integer, ByVal eventID As Integer)
        Try
            Dim cols As String
            Dim vals As String
            Dim SQL As String

            cols = "RetryID,ScheduleID,EntryDate,RetryNumber,EventID"

            If scheduleID <> 0 Then
                If clsMarsData.IsDuplicate("RetryTracker", "ScheduleID", scheduleID, False) = False Then
                    vals = clsMarsData.CreateDataID("retrytracker", "retryid") & "," & _
                    scheduleID & "," & _
                    "'" & ConDateTime(Now) & "'," & _
                    0 & "," & _
                    0

                    clsMarsData.DataItem.InsertData("RetryTracker", cols, vals, False)
                End If
            ElseIf eventID <> 0 Then
                If clsMarsData.IsDuplicate("RetryTracker", "EventID", eventID, False) = False Then
                    vals = clsMarsData.CreateDataID("retrytracker", "retryid") & "," & _
                    0 & "," & _
                    "'" & ConDateTime(Now) & "'," & _
                    0 & "," & _
                    eventID

                    clsMarsData.DataItem.InsertData("RetryTracker", cols, vals, False)
                End If
            End If
        Catch : End Try
    End Sub


    Public Sub trackDuration(ByVal reportID As Integer, ByVal startDate As Date)
        Dim cols As String
        Dim vals As String

        cols = "RunID, ReportID,StartDate,EndDate"

        vals = reportRunID & "," & _
        reportID & "," & _
        "'" & ConDateTime(startDate) & "'," & _
        "'" & ConDateTime(Now) & "'"

        clsMarsData.DataItem.InsertData("ReportDurationTracker", cols, vals, False)

    End Sub

    Public Sub logDurationMaster(ByVal reportID As Integer)
        Dim nID As Integer = clsMarsData.CreateDataID("reportduration", "runid")
        Dim cols As String = "RunID, ReportID, EntryDate"
        Dim vals As String = nID & "," & _
        reportID & "," & _
        "'" & ConDateTime(Now) & "'"

        clsMarsData.DataItem.InsertData("ReportDuration", cols, vals, False)

        Me.reportRunID = nID
    End Sub

    Public Function getserverReportParameters(ByVal svcUrl As String, ByVal reportPath As String, ByVal userName As String, _
           ByVal Password As String, ByVal rdlFile As String) As DataTable
10:     Try
            Dim serverUrl As String
20:         Dim rv As Microsoft.Reporting.WinForms.ReportViewer = New Microsoft.Reporting.WinForms.ReportViewer
30:         rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote



            'remove the reportservice.asmx from the url
40:         For I As Integer = 0 To svcUrl.Split("/").GetUpperBound(0) - 1
50:             serverUrl &= svcUrl.Split("/")(I) & "/"
60:         Next

            'set up the report viewer to open our report
70:         rv.ServerReport.ReportServerUrl = New Uri(serverUrl)
80:         rv.ServerReport.ReportPath = reportPath

            'if username is provided then set up the server credentials
            Try
9000:           If userName <> "" Then
                    Dim userDomain As String = ""

100:                If userName.Contains("\") Then
110:                    userDomain = userName.Split("\")(0)
                    End If

120:                rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, userName, Password, userDomain)
                End If
            Catch : End Try

            'start reading the parameters
121:        Dim p As Microsoft.Reporting.WinForms.ReportParameterInfo
122:        Dim ps As Microsoft.Reporting.WinForms.ReportParameterInfoCollection = rv.ServerReport.GetParameters()

130:        Dim pTable As DataTable = New DataTable("Parameters")

            'this table is not normalized and will contain an entry for each available value per parameter
140:        With pTable.Columns
150:            .Add("Name")
160:            .Add("Type")
170:            .Add("MultiValue")
180:            .Add("Label")
190:            .Add("Value")
            End With

            'populate the table with parameters and their values as well as respective labels and if necessary show hidden parameters
            Dim showHiddenPars As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ShowHiddenParameters", 0)))

200:        For Each p In ps

210:            If rdlFile <> "" Then
220:                If Me.IsParameterHidden(rdlFile, p.Name) = True And showHiddenPars = False Then Continue For
                End If

                Dim pList As IList = p.ValidValues

230:            If pList IsNot Nothing Then
240:                For Each vv As Microsoft.Reporting.WinForms.ValidValue In pList
                        Dim row As DataRow = pTable.Rows.Add
250:                    row("Name") = p.Name
260:                    row("Type") = p.DataType.ToString
270:                    row("MultiValue") = p.MultiValue.ToString
280:                    row("Label") = vv.Label
290:                    row("Value") = vv.Value
300:                Next
                ElseIf p IsNot Nothing Then
301:                Dim row As DataRow = pTable.Rows.Add
                    row("Name") = p.Name
                    row("Type") = p.DataType.ToString
                    row("MultiValue") = p.MultiValue.ToString
                    row("Label") = ""
                    row("Value") = ""
                End If
310:        Next

320:        Return pTable
330:    Catch ex As Exception
340:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)

350:        Return Nothing
        End Try
    End Function


    Public Function IsParameterHidden(ByVal sRDL As String, ByVal parameterName As String) As Boolean
        Dim hidden As Boolean = False

        Try
            Dim doc As XmlDocument = New XmlDocument

            doc.Load(sRDL)

            Dim nodeList As XmlNodeList = doc.GetElementsByTagName("ReportParameter")

            For Each node As XmlNode In nodeList
                hidden = node.Item("Hidden").InnerText
            Next
        Catch:End Try

        Return hidden
    End Function
End Class
