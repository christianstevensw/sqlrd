Imports System.ServiceProcess
Imports Microsoft.Win32
Imports ActiveDs

Public Class clsServiceController
    Dim oSec As New clsMarsSecurity
    Shared oUI As New clsMarsUI
    Shared sChar As Char = Chr(34)
    Shared oReg As RegistryKey = Registry.LocalMachine
    Public Shared itemGlobal As New clsServiceController
    Public Enum enum_svcType As Integer
        NT_SERVICE = 0
        BG_APPLICATION = 1
        NONE = 2
    End Enum

    Public Enum enum_svcStatus As Integer
        RUNNING = 0
        STOPPED = 1
    End Enum
    Public ReadOnly Property m_serviceType() As enum_svcType
        Get
            Dim svcType As String = clsMarsUI.MainUI.ReadRegistry("SQL-RDService", "NONE")

            Select Case svcType.ToLower
                Case "windowsnt"
                    Return enum_svcType.NT_SERVICE
                Case "windowsapp"
                    Return enum_svcType.BG_APPLICATION
                Case Else
                    Return enum_svcType.NONE
            End Select
        End Get
    End Property

    Public ReadOnly Property m_serviceStatus() As enum_svcStatus
        Get
            Select Case Me.m_serviceType
                Case enum_svcType.NT_SERVICE
                    Dim srv As ServiceController = New ServiceController("SQL-RD")

                    If srv.Status = ServiceControllerStatus.Running Then
                        Return enum_svcStatus.RUNNING
                    Else
                        Return enum_svcStatus.STOPPED
                    End If
                Case enum_svcType.BG_APPLICATION
                    If Process.GetProcessesByName("sqlrdapp").GetLength(0) > 0 Then
                        Return enum_svcStatus.RUNNING
                    Else
                        Return enum_svcStatus.STOPPED
                    End If
                Case enum_svcType.NONE
                    Return enum_svcStatus.STOPPED
            End Select
        End Get
    End Property

    Public Shared Sub RemoveNTService()
        On Error Resume Next

        Dim sParameters As String
        Dim srv As New ServiceController("SQL-RD")
        Dim srvmon As New ServiceController("SQL-RD Monitor")
        Dim procInstaller As Process = New Process
        'stop the services
        If srv.Status = ServiceControllerStatus.Running Then
            srv.Stop()
        End If

        If srvmon.Status = ServiceControllerStatus.Running Then
            srvmon.Stop()
        End If

        'remove the services
        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sChar & sAppPath & "sqlrdsvc.exe" & sChar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        'remove old services
        srv = New ServiceController("SQL-RD5")
        srvmon = New ServiceController("SQL-RD5 Monitor")

        If srv.Status = ServiceControllerStatus.Running Then
            srv.Stop()
        End If

        If srvmon.Status = ServiceControllerStatus.Running Then
            srvmon.Stop()
        End If

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sChar & sAppPath & "sqlrdsvc.exe" & sChar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar & " /u"
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        oUI.DeleteRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RD5SVC")
        oUI.DeleteRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RDSVC")

        IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) & "\SQL-RD Service Monitor.lnk")
        
        For Each o As Process In Process.GetProcessesByName("servicemonitor")
            o.Kill()
        Next

        '//Un-Register this computer as the scheduler
        UnRegisterService()

    End Sub

    Public Function InstallNTScheduler(ByVal sDomain As String, ByVal sUser As String, ByVal sPassword As String, _
    ByVal sMailType As String) As Boolean

        Dim srv As New ServiceController("SQL-RD")
        Dim srvmon As New ServiceController("SQL-RD Monitor")
        Dim sParameter As String
        Dim sCommand As String
        Dim procInstaller As Process
        Dim localMachineName As String = Environment.MachineName
        Dim regMachineName As String = GetRegisteredServiceName()

        '//Check to see if the scheduler service has already been 
        If (localMachineName <> regMachineName And regMachineName <> "") Then
            MessageBox.Show("The Scheduler Service has already been registered on " & regMachineName, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            AppStatus(False)
            Return False
        End If

        If oSec._VerifyLogin(sUser, sPassword, sDomain) = False Then

            MessageBox.Show("Failed to aunthenticate the provided user " & _
                "credentials. Please check the following:" & vbCrLf & vbCrLf & _
                "1. The user has Windows local security policy attribute 'Act as part of the operating system' on this " & _
                "PC." & vbCrLf & _
                "2. The user has Windows local security policy attribute 'Log on as a service' on this PC. This can be set up in Windows by going to Administrative Tools � Security Policies - User Rights Assignment." & vbCrLf & _
                "3. The user is a local administrator on this PC (member of the Administrators security group)" & vbCrLf & _
                "4. You have restarted your PC after making any security changes" & _
                "If you are using MAPI, make sure the user has full rights to the MAPI account you are going to be using." & vbCrLf & vbCrLf & _
                "Also please note:" & vbCrLf & _
                "During the service installation process:" & vbCrLf & _
                "- You must be logged into the domain you have just entered." & vbCrLf & _
                "- You must be logged in as the user you have just entered." & vbCrLf & _
                "This is a windows security requirement. " & _
                "Once the service is installed, you can log out and log back in as another user", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            AppStatus(False)

            Return False
        End If

        'remove the background scheduler
        RemoveBAS()

        AppStatus(False)

        If sMailType = "MAPI" Then
            Dim oMAPI As frmMAPIType = New frmMAPIType

            If oMAPI.MAPISetup = False Then Return False
        End If

        AppStatus(True)

        'stop services if applicable
        On Error Resume Next

        oUI.BusyProgress(25, "Interogating service control...")

        Dim s As String = srv.DisplayName

        If Err.Number = 0 Then

            If srv.Status = ServiceControllerStatus.Running Then
                srv.Stop()
            End If

            If srvmon.Status = ServiceControllerStatus.Running Then
                srvmon.Stop()
            End If

            'remove the services
            procInstaller = New Process

            With procInstaller
                With .StartInfo
                    .FileName = sAppPath & "installUtil.exe"
                    .Arguments = sChar & sAppPath & "sqlrdsvc.exe" & sChar & " /u"
                    .WindowStyle = ProcessWindowStyle.Hidden
                End With

                .Start()
                .WaitForExit()
            End With

            With procInstaller
                With .StartInfo
                    .FileName = sAppPath & "installUtil.exe"
                    .Arguments = sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar & " /u"
                    .WindowStyle = ProcessWindowStyle.Hidden
                End With

                .Start()
                .WaitForExit()
            End With

            'SuperShell(sChar & sAppPath & "installUtil.exe" & sChar & " " & sChar & sAppPath & "sqlrdsvc.exe" & sChar & " /u")

            'SuperShell(sChar & sAppPath & "installUtil.exe" & sChar & " " & sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar & " /u")
        End If

        oUI.BusyProgress(50, "Installing service...")

        'install the main service
        sParameter = " /username=" & sDomain & "\" & sUser
        sParameter &= " /password=" & sPassword
        sParameter &= " " & sChar & sAppPath & "sqlrdsvc.exe" & sChar
        sParameter &= " /LogFile=" & sChar & sAppPath & "sqlrdsvc.installLog" & sChar

        procInstaller = New Process

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sParameter
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        'sCommand = sChar & sAppPath & "installutil.exe" & sChar & sParameter
        'SuperShell(sCommand)
        '        _Delay(3)

        'install the monitoring service
        'sParameter = sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar
        sParameter = " /username=" & sDomain & "\" & sUser
        sParameter &= " /password=" & sPassword
        sParameter &= " " & sChar & sAppPath & "sqlrdsvcmonitor.exe" & sChar
        sParameter &= " /LogFile=" & sChar & sAppPath & "sqlrdsvcmonitor.installLog" & sChar

        With procInstaller
            With .StartInfo
                .FileName = sAppPath & "installUtil.exe"
                .Arguments = sParameter
                .WindowStyle = ProcessWindowStyle.Hidden
            End With

            .Start()
            .WaitForExit()
        End With

        'sCommand = sChar & sAppPath & "installutil.exe" & sChar & sParameter
        'SuperShell(sCommand)
        oUI.BusyProgress(95, "Cleaning up...")
        '_Delay(3)

        'add the details into the registry
        With oUI
            .SaveRegistry("NTLoginName", sUser, , , True)
            .SaveRegistry("NTLoginPassword", sPassword, True, , True)
            .SaveRegistry("NTDomain", sDomain, , , True)
        End With

        'update the service type
        oUI.SaveRegistry("SQL-RDService", "WindowsNT", , , True)

        ServiceType = "WindowsNT"

        '//Register this computer as the scheduler
        RegisterService()

#If Not Debug Then
        Kill(sAppPath & "sqlrdsvc.installlog")
#End If
        'delete autorun registry key
        oUI.DeleteRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RD5")

        'add the service monitor to autostartup
        If CreateShortCut(Environment.GetFolderPath(Environment.SpecialFolder.Startup), sAppPath & "ServiceMonitor.exe", "SQL-RD Service Monitor.lnk", "Monitors SQL-RD Service") = False Then
            oUI.SaveRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RDSVC", sAppPath & "ServiceMonitor.exe")
        End If

        'create share to sqlrd folder
        'Dim fshare As IADsFileService
        'Dim newShare As IADsFileShare

        'fshare = GetObject("WinNT://" & Environment.MachineName & "/lanmanserver")
        'newShare = fshare.Create("fileshare", "SQL-RD")
        'newShare.Path = Environment.CurrentDirectory
        'newShare.SetInfo()
        'newShare = Nothing

        'On Error GoTo Trap
        ''try reading from this share
        'Dim share As String = "\\" & Environment.MachineName & "\SQL-RD\"
        'Dim readTest As String = ReadTextFromFile(share & "sqlrdupgrade.dll")

        ''update system paths
        'Dim reportCache As String = oUI.ReadRegistry("CachePath", "")

        'If reportCache.ToLower = (sAppPath & "Cache\").ToLower Then
        '    oUI.SaveRegistry("CachePath", share & "Cache\")
        'End If

        'Dim cachedData As String = oUI.ReadRegistry("CachedDataPath", "")

        'If cachedData.ToLower = (sAppPath & "Cached Date\").ToLower Then
        '    oUI.SaveRegistry("CachedDataPath", share & "Cached Date\")
        'End If

        'Dim snapShots As String = oUI.ReadRegistry("SnapshotsPath", "")

        'If snapShots.ToLower = (sAppPath & "Snapshots\").ToLower Then
        '    oUI.SaveRegistry("SnapshotsPath", share & "Snapshots\")
        'End If

        'Dim tempFolder As String = clsMarsUI.MainUI.ReadRegistry("TempFolder", "")

        'If tempFolder.ToLower = (sAppPath & "Output\").ToLower Then
        '    oUI.SaveRegistry("TempFolder", share & "Output\")
        'End If

        oUI.BusyProgress(, , True)

        Dim oResponse As DialogResult = MessageBox.Show("Would you like to open the services control panel?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oResponse = DialogResult.Yes Then
            Dim tmp As String = Environment.SystemDirectory & "\services.msc"
            Process.Start(tmp)
        End If


        Return True

    End Function

    Public Shared Sub InstallBGScheduler()
        'enable the windows scheduler

        Dim localMachineName As String = Environment.MachineName
        Dim regMachineName As String = GetRegisteredServiceName()

        '//Check to see if the scheduler service has already been 
        If (localMachineName <> regMachineName And regMachineName <> "") Then
            MessageBox.Show("The Scheduler Service has already been registered on " & regMachineName, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            AppStatus(False)
            Exit Sub
        End If

        oUI.BusyProgress(50, "Installing application...")

        If CreateShortCut(Environment.GetFolderPath(Environment.SpecialFolder.Startup), sAppPath & "sqlrdapp.exe", "SQL-RD Scheduler.lnk", "SQL-RD Background Application Scheduler") = False Then
            oUI.SaveRegistry(Registry.CurrentUser, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RD", sChar & sAppPath & "sqlrdapp.exe" & sChar)
        End If

        If CreateShortCut(Environment.GetFolderPath(Environment.SpecialFolder.Startup), sAppPath & "sqlrdappmon.exe", "SQL-RD Monitor.lnk", "Monitors SQL-RD scheduler") = False Then
            oUI.SaveRegistry(Registry.CurrentUser, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RDMON", sChar & sAppPath & "sqlrdappmon.exe" & sChar)
        End If

        RemoveNTService()

        'install SQL-RD Monitor service
        oUI.BusyProgress(95, "Cleaning up...")

        'save the settings
        oUI.SaveRegistry("SQL-RDService", "WindowsApp", , , True)

        ServiceType = "WindowsApp"

        '//Register this computer as the scheduler
        RegisterService()

        oUI.BusyProgress(, , True)
    End Sub

    Public Sub StopScheduler()
        Dim sType As String

        sType = oUI.ReadRegistry("SQL-RDService", "NONE")

        If sType = "WindowsApp" Then
            For Each o As Process In Process.GetProcessesByName("sqlrdappmon")
                o.Kill()
            Next

            For Each o As Process In Process.GetProcessesByName("sqlrdapp")
                o.Kill()
            Next

            Try
                Dim refresh As systrayrefresh.Class1 = New systrayrefresh.Class1

                refresh.systrayrefresh()
            Catch : End Try
        ElseIf sType = "WindowsNT" Then
            Dim oSvc As New ServiceController("SQL-RD")

            Try
                If oSvc.Status <> ServiceControllerStatus.Stopped Then
                    oSvc.Stop()
                End If
            Catch : End Try

            oSvc = New ServiceController("SQL-RD Monitor")

            Try
                If oSvc.Status <> ServiceControllerStatus.Stopped Then
                    oSvc.Stop()
                End If
            Catch : End Try
        End If
    End Sub

    Public Shared Sub MigrateBAS()
        Try
            Dim sType As String = oUI.ReadRegistry("SQL-RDService", "NONE")

            If sType = "WindowsApp" Then
                InstallBGScheduler()

                oUI.DeleteRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run\", "SQL-RD5")
            End If
        Catch : End Try
    End Sub

    Public Shared Sub RemoveBAS()
        For Each oproc As Process In Process.GetProcessesByName("sqlrdappmon")
            oproc.Kill()
        Next

        For Each oproc As Process In Process.GetProcessesByName("sqlrdapp")
            oproc.Kill()
        Next

        'remove the background scheduler
        oUI.DeleteRegistry(oReg, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RD5")
        oUI.DeleteRegistry(Registry.CurrentUser, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RD")
        oUI.DeleteRegistry(Registry.CurrentUser, "Software\Microsoft\Windows\CurrentVersion\Run", "SQL-RDMON")

        IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) & "\SQL-RD Scheduler.lnk")
        IO.File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) & "\SQL-RD Monitor.lnk")

        '//Un-Register this computer as the scheduler
        UnRegisterService()

    End Sub
    Public Sub StartScheduler()
        Dim sType As String

        sType = oUI.ReadRegistry("SQL-RDService", "NONE")

        If sType = "WindowsApp" Then
            If Process.GetProcessesByName("sqlrdapp").GetLength(0) = 0 Then
                Process.Start(sAppPath & "sqlrdapp.exe")
            End If

            If Process.GetProcessesByName("sqlrdappmon").GetLength(0) = 0 Then
                Process.Start(sAppPath & "sqlrdappmon.exe")
            End If
        ElseIf sType = "WindowsNT" Then
            Dim oSvc As New ServiceController("SQL-RD")

            Try
                If oSvc.Status = ServiceControllerStatus.Stopped Then
                    oSvc.Start()
                End If
            Catch : End Try

            oSvc = New ServiceController("SQL-RD Monitor")

            Try
                If oSvc.Status = ServiceControllerStatus.Stopped Then
                    oSvc.Start()
                End If
            Catch : End Try
        End If
    End Sub

    Public Shared Function CreateShortCut(ByVal path As String, ByVal targetFile As String, _
    ByVal linkName As String, ByVal linkDesc As String, Optional ByVal Arguments As String = "", _
    Optional ByVal checkFileExists As Boolean = False) As Boolean

        Try
            Dim wshShell As IWshRuntimeLibrary.IWshShell_Class = New IWshRuntimeLibrary.IWshShell_Class

            If path.EndsWith("\") = False Then path &= "\"

            If checkFileExists = True Then
                If IO.File.Exists(path & linkName) = True Then
                    If MessageBox.Show("A shortcut with the specified name ('" & path & linkName & "') already exists. OK to overwrite?", _
                    Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = DialogResult.No Then
                        Return False
                    End If
                End If
            End If

            Dim shortcut As IWshRuntimeLibrary.IWshShortcut_Class = wshShell.CreateShortcut(path & linkName)

            shortcut.TargetPath = targetFile

            shortcut.IconLocation = sAppPath & assemblyName & ", 0"

            shortcut.Description = linkDesc

            If Arguments.Length > 0 Then
                shortcut.Arguments = Arguments
            End If

            shortcut.WorkingDirectory = sAppPath

            shortcut.Save()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function GetRegisteredServiceName() As String
        Dim retVal As String = ""
        Dim sql As String = "SELECT WorkstationID FROM SchedulerAttr"
        Dim rs As ADODB.Recordset

        '//The registered scheduler is the name of the computer
        '//where the service is running on. There can only one.

        rs = clsMarsData.GetData(sql)

        If (rs IsNot Nothing) Then
            If (rs.EOF = False And rs.RecordCount > 0) Then
                retVal = rs.Fields("workstationid").Value
            End If

            rs.Close()
        End If

        Return retVal
    End Function

    Public Shared Sub RegisterService()
        '//Make sure there are no entries in the SchedulerAttr
        '//table by calling UnRegisterService
        UnRegisterService()

        '//Insert this computer's name into the SchedulerAttr table
        Dim sql As String = "INSERT INTO SchedulerAttr (WorkstationId) VALUES ('" & Environment.MachineName & "')"
        clsMarsData.WriteData(sql)

    End Sub

    Public Shared Sub UnRegisterService()
        '//Since there is only one registered scheduler,
        '//unregister by removing all entries in the SchedulerAttr table
        Dim sql As String = "DELETE FROM SchedulerAttr"
        clsMarsData.WriteData(sql)
    End Sub
End Class
