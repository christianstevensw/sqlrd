Public Class clsMarsPGP

    Public Function EncryptFile(ByVal inFile As String, _
    ByVal nDestinationID As Integer) As Object()
        Dim oResult(1) As Object

        Try
10:         Dim oPGP As PGPEncode.PGPEncode = New PGPEncode.PGPEncode
            Dim sError As String
            Dim oSuccess As Boolean
            Dim sResult As String
            Dim oRs As ADODB.Recordset

20:         oRs = clsMarsData.GetData("SELECT * FROM PGPAttr WHERE DestinationID = " & nDestinationID)

            Dim sUserID As String
            Dim sKeyLoc As String
30:         Dim outFile As String = GetDirectory(inFile) & clsMarsData.CreateDataID("", "") & ".pgp"
            Dim AddExt As Boolean = False

40:         If oRs.EOF = False Then
50:             sUserID = oRs("pgpid").Value
60:             sKeyLoc = oRs("keylocation").Value

                Try
61:                 AddExt = oRs("pgpext").Value
                Catch : End Try

                If AddExt = True Then
62:                 oResult(1) = ".pgp"
                Else
                    oResult(1) = ""
                End If

                Dim PubRing As String
                Dim SecRing As String

70:             If sKeyLoc.IndexOf("|") > -1 Then
80:                 PubRing = sKeyLoc.Split("|")(0)
90:                 SecRing = sKeyLoc.Split("|")(1)
                Else
100:                If sKeyLoc.EndsWith("\") = False Then sKeyLoc &= "\"

110:                PubRing = sKeyLoc & "pubring.pkr"
120:                SecRing = sKeyLoc & "secring.skr"
                End If
                'encrypt the file
121:            sResult = oPGP.EncodeFile(inFile, outFile, sUserID, PubRing, SecRing)

122:            oSuccess = sResult.Split("|")(0)
123:            sError = sResult.Split("|")(1)

130:            If oSuccess = True Then
140:                If IO.File.Exists(inFile & oResult(1)) Then
141:                    IO.File.Delete(inFile & oResult(1))
142:                End If

150:                IO.File.Move(outFile, inFile & oResult(1))

151:                oResult(0) = True

160:                Return oResult
170:            Else
180:                gErrorDesc = sError
190:                gErrorNumber = -214765890
200:                gErrorSource = "clsMarsPGP.EncryptFile"
210:                gErrorLine = 9

211:                oResult(0) = False

220:                Return oResult
                End If
            Else
230:            oResult(0) = True
231:            oResult(1) = ""

232:            Return oResult
            End If
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = Erl()
            gErrorNumber = Err.Number

            oResult(0) = False
            oResult(1) = ""

            Return oResult
        End Try

    End Function
End Class
