Imports System.IO
Imports Quiksoft.EasyMail.POP3
Imports Quiksoft.EasyMail.Parse
Imports Quiksoft.EasyMail.SSL
Imports Quiksoft.EasyMail.IMAP4

Public Class clsMarsEvent
    Dim oData As New clsMarsData
    Shared nPointers() As Object
    Shared matchedMail As ArrayList
    Shared cleanerFiles As New ArrayList
    Shared cleanerFS As New ArrayList
    Shared pointer As Integer
    Public Shared m_resultsTable As Hashtable
    Public Shared currentEventID As Integer = 0
    Public Shared m_StartTime As Date = Now.Date
    Public m_HistoryID As Integer = 0
    Public Shared m_EventDataCache() As DataTable

    Public Enum m_mailServerType As Integer
        POP3 = 0
        IMAP4 = 1
    End Enum
    Public Shared ReadOnly Property m_eventID() As Integer
        Get
            Return currentEventID
        End Get
    End Property

    Public Shared ReadOnly Property m_CachedDataPath() As String
        Get
            Dim value As String

            value = clsMarsUI.MainUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Data\")

            If value.EndsWith("\") = False Then value &= "\"

            Return value
        End Get
    End Property

    Public Shared Property m_Pointer() As Integer
        Get
            Return pointer
        End Get
        Set(ByVal value As Integer)
            pointer = value
        End Set
    End Property
    Public Shared Function moveintoEventPackage(ByVal packID As Integer, ByVal eventID() As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim orderID As Integer = 0

        SQL = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & packID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            orderID = 0
        ElseIf oRs.EOF = True Then
            orderID = 0
        Else
            orderID = IsNull(oRs(0).Value, 0)
        End If

        orderID += 1

        For Each n As Integer In eventID
            SQL = "UPDATE EventAttr6 SET " & _
            "Parent = 0," & _
            "PackID =" & packID & "," & _
            "PackOrderID =" & orderID & _
            " WHERE EventID = " & n

            clsMarsData.WriteData(SQL)

            orderID += 1
        Next
    End Function
    Public Shared Function DoesRecordExists(ByVal name As String, ByVal query As String, ByVal connectionString As String, _
           ByVal keyColumn As String, ByVal selectType As String, ByVal cacheFolderName As String) As Object()


10:     Dim dsn As String = connectionString.Split("|")(0)
20:     Dim username As String = connectionString.Split("|")(1)
30:     Dim password As String = connectionString.Split("|")(2)
40:     Dim orsOld As ADODB.Recordset
50:     Dim dataPath As String
60:     Dim results As Object() = Nothing
70:     Dim I As Integer = 0
80:     Dim x As Integer = 1
90:     Dim oCon As ADODB.Connection = New ADODB.Connection
100:    Dim oTest As ADODB.Recordset = New ADODB.Recordset

110:    Try
            query = clsMarsParser.Parser.ParseString(query)

120:        If keyColumn.IndexOf(".") > -1 Then
130:            keyColumn = keyColumn.Split(".")(1)
140:        End If

150:        Select Case selectType
                Case "NEW"
160:                Dim newRecordFound As Boolean = False

170:                oCon.Open(dsn, username, _DecryptDBValue(password))

180:                oTest.Open(query, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

190:                dataPath = m_CachedDataPath & "dbe_" & cacheFolderName 'name.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

200:                dataPath &= "\datacache.xml"

210:                If oTest IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                        Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

220:                    clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oTest)

230:                    If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
240:                        clsMarsEvent.m_EventDataCache(ubound).TableName = name
                        End If
                    End If

250:                If IO.File.Exists(dataPath) = True And keyColumn.Length > 0 Then
260:                    orsOld = clsMarsData.GetXML(dataPath)

261:                    oTest.Close()
262:                    oTest.Open()

270:                    Do While oTest.EOF = False

280:                        clsMarsUI.MainUI.BusyProgress((x / oTest.RecordCount) * 100, "Checking records...")
290:                        x += 1

300:                        orsOld.Filter = ""

310:                        Try
320:                            If oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adDate Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adDBDate Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adLongVarChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adLongVarWChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adVarChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adVarWChar Or _
                                      oTest.Fields(keyColumn).Type = ADODB.DataTypeEnum.adWChar Then

330:                                orsOld.Filter = keyColumn & "='" & SQLPrepare(oTest.Fields(keyColumn).Value) & "'"
340:                            Else
                                    Dim sValue As String = IsNull(oTest.Fields(keyColumn).Value, 0)
                                    Dim sFilter As String = keyColumn & "=" & sValue

350:                                orsOld.Filter = sFilter 'keyColumn & "='" & SQLPrepare(oTest.Fields(keyColumn).Value) & "'"
360:                            End If
370:                        Catch e As Exception
380:                            GoTo SkipRecord
                            End Try

390:                        If orsOld.EOF = True Then

400:                            newRecordFound = True

410:                            ReDim Preserve results(I)

420:                            results(I) = oTest(keyColumn).Value

430:                            I += 1
440:                        End If
SkipRecord:
450:                        oTest.MoveNext()
460:                    Loop

470:                End If


480:                If newRecordFound = True Or IO.File.Exists(dataPath) = False Then
490:                    clsMarsData.CreateXML(oTest, dataPath)
500:                End If

510:                oCon.Close()
520:            Case "ALL"

530:                oCon.Open(dsn, username, _DecryptDBValue(password))

540:                oTest.Open(query, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

550:                If oTest IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                        Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

560:                    clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oTest)

570:                    If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
580:                        clsMarsEvent.m_EventDataCache(ubound).TableName = name
                        End If
                    End If

581:                oTest.Close()
582:                oTest.Open()

590:                Do While oTest.EOF = False

600:                    ReDim Preserve results(I)
610:                    Dim temp As String = keyColumn

620:                    If keyColumn.IndexOf(".") > -1 Then
630:                        temp = keyColumn.Split(".")(1)
640:                    End If

650:                    results(I) = oTest(temp).Value

660:                    I += 1

670:                    oTest.MoveNext()
680:                Loop

690:                oTest.Close()

700:                oCon.Close()
            End Select

710:        Return results
720:    Catch ex As Exception
730:        _ErrorHandle(gScheduleName & " " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "")
740:        Return Nothing
        End Try
    End Function

    Private Sub _GetMailValues(ByVal msgObj As Quiksoft.EasyMail.Parse.EmailMessage)
        Try
            gSubject = msgObj.Subject

            For Each ob As Quiksoft.EasyMail.Parse.BodyPart In msgObj.BodyParts
                gBody &= ob.BodyText
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.From
                gFrom &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.To
                gTo &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.CC
                gCc &= oA.EmailAddress & ";"
            Next

            For Each oA As Quiksoft.EasyMail.Parse.Address In msgObj.BCC
                gBcc &= oA.EmailAddress & ";"
            Next
        Catch : End Try
    End Sub

    Private Shared Sub ForwardMail(ByVal Messages As ArrayList, ByVal conditionID As Integer, ByVal emailData As DataTable)
        Dim nRetry As Integer = 0

RETRY:
10:     Try
            Dim msg As Chilkat.Email
            Dim SQL As String
            Dim oRs As ADODB.Recordset
20:         Dim oParse As clsMarsParser = New clsMarsParser
            Dim sPath As String
30:         Dim oMsg As New clsMarsMessaging
            Dim pointer As Integer = 0

40:         sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

50:         If sPath.EndsWith("\") = False Then sPath &= "\"

60:         sPath &= "Downloaded Emails\"

70:         SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

80:         oRs = clsMarsData.GetData(SQL)

90:         If oRs Is Nothing Then Return

100:        If oRs.EOF = True Then Return

            Dim forwardMail As Boolean = False

110:        Try
120:            forwardMail = Convert.ToBoolean(oRs("forwardmail").Value)
            Catch : End Try

130:        If forwardMail = False Then Return

140:        oRs.Close()

150:        SQL = "SELECT * FROM ForwardMailAttr WHERE ConditionID =" & conditionID

160:        oRs = clsMarsData.GetData(SQL)

170:        If oRs Is Nothing Then Return

180:        If oRs.EOF = True Then Return

            Dim sendTo, sendCC, sendBCC, mailSubject, mailBody, attachments, mailFormat, mailServer As String

190:        clsMarsUI.MainUI.BusyProgress(60, "Forwading emails...")

200:        For Each msg In Messages
                Dim senderAddress As String = msg.FromAddress
                Dim senderName As String = msg.FromName
                Dim sentDate As String = msg.LocalDate.ToString("dd MMMM yyyy HH:mm")
                Dim sentTo, sentCC As String
                Dim fullBody As String = ""
                Dim fullSubject As String = ""
                Dim newMsg As Chilkat.Email = msg


210:            clsMarsEvent.m_Pointer = pointer

                'initialise the data
220:            sendTo = oParse.ParseString(IsNull(oRs("sendto").Value))
230:            sendCC = oParse.ParseString(IsNull(oRs("sendcc").Value))
240:            sendBCC = oParse.ParseString(IsNull(oRs("sendbcc").Value))
250:            mailSubject = oParse.ParseString(IsNull(oRs("mailsubject").Value))
260:            mailBody = oParse.ParseString(IsNull(oRs("mailbody").Value))
270:            attachments = oParse.ParseString(IsNull(oRs("attachments").Value))
280:            mailFormat = IsNull(oRs("mailformat").Value, "HTML")
290:            mailServer = IsNull(oRs("mailserver").Value)

300:            sendTo = oMsg.ResolveEmailAddress(sendTo)
310:            sendCC = oMsg.ResolveEmailAddress(sendCC)
320:            sendBCC = oMsg.ResolveEmailAddress(sendBCC)

330:            fullBody = mailBody
340:            fullSubject = mailSubject

350:            sentTo = ""

360:            For n As Integer = 0 To msg.NumTo - 1
370:                sentTo &= msg.GetTo(n) & ";"
380:            Next

390:            sentCC = ""

400:            For n As Integer = 0 To msg.NumCC - 1
410:                sentCC &= msg.GetCC(n) & ";"
420:            Next

                Dim forwardSubject As String = msg.Subject
                Dim forwardBody As String = ""

                Dim header As String = ""

430:            header = "-----Original Message-----" & vbCrLf & _
                      "From: " & senderName & " [" & senderAddress & "]" & vbCrLf & _
                      "Sent: " & sentDate & vbCrLf & _
                      "To: " & sentTo & vbCrLf & _
                      "Cc: " & sentCC & vbCrLf & _
                      "Subject: " & forwardSubject & vbCrLf & vbCrLf

                If fullSubject = "" Then newMsg.Subject = "FW: " & msg.Subject Else newMsg.Subject = fullSubject

440:            fullBody &= vbCrLf & vbCrLf & header

                Dim htmlBody As String = msg.GetHtmlBody
                Dim textBody As String = msg.GetPlainTextBody

441:            If textBody Is Nothing Then textBody = newMsg.Body

450:            If (MailType = gMailType.SMTP Or MailType = gMailType.SQLRDMAIL) And mailFormat = "HTML" And htmlBody IsNot Nothing Then
460:                header = "<html><body><font face='Arial' size='2'>" & fullBody.Replace(vbCrLf, "<BR>") & "</body></html>"

470:                'newMsg.SetHtmlBody(header & htmlBody)
                    newMsg.AddHtmlAlternativeBody(header & htmlBody)
480:            End If

490:            Dim currentBody As String = textBody
491:            Dim allBody As String

493:            allBody = fullBody & vbCrLf & currentBody

494:            newMsg.Body = allBody


500:            newMsg.ClearBcc()
510:            newMsg.ClearCC()
520:            newMsg.ClearTo()

530:            For Each s As String In sendTo.Split(";")
540:                If s <> "" Then
550:                    newMsg.AddTo(s, s)
                    End If
560:            Next

570:            For Each s As String In sendCC.Split(";")
580:                If s <> "" Then
590:                    newMsg.AddCC(s, s)
                    End If
600:            Next

610:            For Each s As String In sendBCC.Split(";")
620:                If s <> "" Then
630:                    newMsg.AddBcc(s, s)
                    End If
640:            Next

                Dim numattach As Integer = 0

650:            If attachments IsNot Nothing Then
660:                For Each s As String In attachments.Split(";")
670:                    If s <> "" Then newMsg.AddFileAttachment(s)
680:                Next

690:                numattach = attachments.Split(";").GetUpperBound(0)
                End If


700:            Select Case MailType
                    Case gMailType.NONE
710:                    Throw New Exception("No messaging configuration found. Please set up your messaging in Options - Messaging")
720:                Case gMailType.SMTP, gMailType.SQLRDMAIL
                        Dim oSender As clsMarsMessaging = New clsMarsMessaging

730:                    oSender.SendSMTP(sendTo, fullSubject, fullBody, "Single", "", numattach, _
                              , sendCC, sendBCC, gScheduleName, False, mailFormat, True, True, mailFormat, mailServer, , , , newMsg)
740:                Case gMailType.MAPI
750:                    clsMarsMessaging.SendMAPI(sendTo, fullSubject, fullBody, "Single", "", numattach, , sendCC, sendBCC, _
                              False, "TEXT", gScheduleName, True)
760:                Case gMailType.GROUPWISE
770:                    clsMarsMessaging.SendGROUPWISE(sendTo, sendCC, sendBCC, fullSubject, fullBody, "", "Single", , True _
                              , , , , numattach, , mailFormat)
                End Select

780:            pointer += 1
790:        Next
800:    Catch ex As Exception
810:        If ex.Message.ToLower.IndexOf("output stream") > -1 Then
820:            If nRetry < 5 Then
830:                System.Threading.Thread.Sleep(5000)
840:                nRetry += 1
850:                GoTo RETRY
                End If
            End If

860:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub
    Private Shared Sub RedirectMail(ByVal Messages As ArrayList, ByVal ConditionID As Integer, ByVal emailData As DataTable)
10:     Try
            Dim msg As Chilkat.Email
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim sPath As String = ""
20:         Dim oMsg As New clsMarsMessaging
            Dim pointer As Integer = 0

30:         SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & ConditionID

40:         oRs = clsMarsData.GetData(SQL)

50:         If oRs Is Nothing Then Return

60:         If oRs.EOF = True Then Return

            Dim RedirectTo As String
            Dim RedirectMail As Boolean = False

70:         Try
80:             RedirectMail = Convert.ToBoolean(oRs("redirectmail").Value)
            Catch : End Try

90:         If RedirectMail = False Then Return

100:        clsMarsUI.MainUI.BusyProgress(60, "Redirecting emails...")

110:        RedirectTo = IsNull(oRs("redirectto").Value, "")

120:        If RedirectTo.Length = 0 Then Return

130:        Dim oParse As clsMarsParser = New clsMarsParser

140:        sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

150:        If sPath.EndsWith("\") = False Then sPath &= "\"

160:        sPath &= "Downloaded Emails\"

170:        For Each msg In Messages
180:            clsMarsEvent.m_Pointer = pointer

                Dim newMsg As Chilkat.Email

                newMsg = msg

                newMsg.ClearTo()
                newMsg.ClearBcc()
                newMsg.ClearCC()

400:            RedirectTo = oParse.ParseString(RedirectTo)
410:            RedirectTo = oMsg.ResolveEmailAddress(RedirectTo)

420:            For Each s As String In RedirectTo.Split(";")
430:                If s <> "" Then
440:                    newMsg.AddTo(s, s)
                    End If
450:            Next


560:            If MailType = gMailType.MAPI Or MailType = gMailType.GROUPWISE Or MailType = gMailType.NONE Then
570:                Throw New Exception("The selected CRD mail type does not support mail redirection. Please use SMTP ot CRDMail")
580:            Else
                    Dim objSender As clsMarsMessaging = New clsMarsMessaging
590:                objSender.SendSMTP(RedirectTo, "", "", "Single", 0, msg.NumAttachments, _
                          , , , gScheduleName, False, "", True, True, "", , , newMsg.FromName, newMsg.FromAddress, newMsg)
                End If

600:            pointer += 1
610:        Next
620:    Catch ex As Exception
630:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
        End Try
    End Sub

    Public Shared Function DoesEmailExist(ByVal mailserver As String _
           , ByVal username As String, ByVal password As String, ByVal Download As Boolean, _
           ByVal searchString As String, ByVal AnyAll As String, ByRef emailData As DataTable, _
           ByVal ConditionID As Integer, ByVal port As Integer, _
           ByVal useSSL As Boolean, ByVal serverType As String, ByVal imapBox As String, _
           ByVal saveAttachments As Boolean) As Boolean

10:     Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
20:     Quiksoft.EasyMail.Parse.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
30:     Quiksoft.EasyMail.IMAP4.License.Key = QuikSoftLicense ' "ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
40:     Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Developer)/13047341F666623200000F00262C5AAE73"

        Dim pop3 As Chilkat.MailMan
        Dim oIMAP4 As Chilkat.Imap
50:     Dim SSL As New SSL
        Dim sReturn(6) As Object
60:     Dim oData As New clsMarsData
        Dim msgCount As Integer
        Dim sPath As String
        Dim MarkForDelete As Boolean
        Dim downloadPath As String
        Dim nPointer As Integer = 0
        Dim emailFound As Boolean = False
        Dim X As Integer = 0
        Dim bodyParts As Hashtable
        Dim retryCount As Integer = 0
        Dim attachmentsPath As String = sAppPath & "Output\MailAttachments\"

70:     cleanerFiles = New ArrayList
80:     cleanerFS = New ArrayList
90:     matchedMail = New ArrayList

100:    Try


            'Message will be stored in files
            Dim fs As FileStream

110:        sPath = clsMarsUI.MainUI.ReadRegistry("TempFolder", sAppPath & "Output\")

120:        If sPath.EndsWith("\") = False Then sPath &= "\"

130:        sPath &= "Downloaded Emails\"

140:        If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

150:        If IO.Directory.Exists(sPath) = False Then
160:            IO.Directory.CreateDirectory(sPath)
            End If

170:        clsMarsUI.MainUI.BusyProgress(30, "Connecting to mail server...")

RETRY:

180:        If serverType = "POP3" Then
190:            pop3 = New Chilkat.MailMan

200:            pop3.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")
                'Connect to the POP3 mail server, if user has provided custom pop3 port then use that instead
210:            If port = 0 Then
220:                pop3.MailHost = mailserver
                    'pop3.Connect(mailserver)
230:            Else
240:                If useSSL = True Then
250:                    pop3.PopSsl = True
260:                    pop3.MailPort = port
270:                    pop3.MailHost = mailserver
                        'pop3.Connect(mailserver, port, SSL.GetInterface)
280:                Else
290:                    pop3.MailPort = port
300:                    pop3.MailHost = mailserver
                        'pop3.Connect(mailserver, port)
                    End If
                End If

310:            clsMarsUI.MainUI.BusyProgress(60, "Logging into mail server...")

320:            pop3.PopUsername = username
330:            pop3.PopPassword = password

                Dim ok As Boolean
                Dim retry As Integer = 0

340:            Do
350:                ok = pop3.VerifyPopConnection()

360:                If ok = False Then System.Threading.Thread.Sleep(3000)

370:                retry += 1
380:            Loop Until ok = True Or retry >= 3

390:            If ok = False Then
400:                Throw New Exception(pop3.LastErrorText)
                End If

410:            retry = 0

420:            Do
430:                ok = pop3.VerifyPopLogin

440:                If ok = False Then System.Threading.Thread.Sleep(3000)

450:                retry += 1
460:            Loop Until ok = True Or retry >= 3

470:            If ok = False Then
480:                Throw New Exception(pop3.LastErrorText)
                End If

490:            clsMarsUI.BusyProgress(70, "Downloading emails from server...")

                Dim bundle As Chilkat.EmailBundle = pop3.CopyMail


500:            If bundle Is Nothing Then
510:                Throw New Exception(pop3.LastErrorText)
                End If
                'pop3.Login(username, password, AuthMode.Plain)

520:            msgCount = bundle.MessageCount - 1 'pop3.GetMessageCount

                Dim msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, msgAttachments As String
                Dim msgDate As String = ""
                Dim msgDatesent As Date

530:            For I As Integer = 0 To msgCount
                    Dim email As Chilkat.Email

540:                email = bundle.GetEmail(I)
550:                msgFrom = ""
560:                msgTo = ""
570:                msgCc = ""
580:                msgBcc = ""
590:                msgSubject = ""
600:                msgBody = ""
610:                msgAttachments = ""
620:                msgDatesent = Nothing

630:                bodyParts = New Hashtable

640:                With bodyParts
650:                    .Add("TEXT", "")
660:                    .Add("HTML", "")
                    End With

670:                If msgCount > 0 Then clsMarsUI.MainUI.BusyProgress((I / msgCount) * 100, "Parsing emails..." & CType((I / msgCount) * 100, Integer) & "%")

680:                MarkForDelete = False

690:                msgFrom = email.From

700:                For n As Integer = 0 To email.NumTo - 1
710:                    msgTo &= email.GetTo(n) & ";"
720:                Next

730:                For n As Integer = 0 To email.NumCC - 1
740:                    msgCc &= email.GetCC(n) & ";"
750:                Next

760:                For n As Integer = 0 To email.NumBcc - 1
770:                    msgBcc &= email.GetBcc(n) & ";"
780:                Next

790:                msgSubject = email.Subject
                    msgDate = ConDateTime(email.LocalDate)

800:                Dim oClean As New clsMarsParser

810:                If email.HasHtmlBody = True Then
820:                    bodyParts.Item("HTML") = oClean.CleanString(email.GetHtmlBody)
830:                    msgBody &= oClean.CleanString(email.GetHtmlBody)
                    End If

840:                If email.HasPlainTextBody Then
850:                    bodyParts.Item("TEXT") = oClean.CleanString(email.GetPlainTextBody)
860:                    msgBody &= oClean.CleanString(email.GetPlainTextBody)
                    End If

870:                If searchEmail(searchString, msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, AnyAll, msgDate) = True Then
880:                    emailFound = True

890:                    matchedMail.Add(email)

                        Dim row As DataRow

900:                    row = emailData.Rows.Add

910:                    row.Item("MsgID") = I 'msg.MessageID
920:                    row.Item("MsgFrom") = msgFrom
930:                    row.Item("MsgTo") = msgTo
940:                    row.Item("MsgCc") = msgCc
950:                    row.Item("MsgBcc") = msgBcc
960:                    row.Item("MsgSubject") = msgSubject
970:                    row.Item("msgBody") = msgBody
980:                    row.Item("msgDate") = email.LocalDate
990:                    row.Item("msgTEXT") = bodyParts.Item("TEXT")
1000:                   row.Item("msgHTML") = bodyParts.Item("HTML")
1010:
                        'If saveAttachments = True Then

1020:                   attachmentsPath = attachmentsPath & Date.Now.ToString("yyyyMMddHHmmss") & "\"

1030:                   clsMarsParser.Parser.ParseDirectory(attachmentsPath)

1040:                   If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

1050:                   email.SaveAllAttachments(attachmentsPath)

1060:                   For n As Integer = 0 To email.NumAttachments - 1
                            Dim att As String = email.GetAttachmentFilename(n)

                            Dim fullFile As String = attachmentsPath & att

1070:                       msgAttachments &= fullFile & ";"
1080:                   Next

1090:                   For n As Integer = 0 To email.NumRelatedItems - 1
                            Dim fullfile As String = attachmentsPath & email.GetRelatedFilename(I)

1100:                       email.SaveRelatedItem(I, attachmentsPath)
1110:                   Next

1120:                   If msgAttachments.EndsWith(";") Then msgAttachments = msgAttachments.Remove(msgAttachments.Length - 1, 1)
                        'End If

1130:                   row.Item("Attachments") = msgAttachments
1140:                   row.Item("AttachmentsPath") = attachmentsPath

1150:                   If Download = True Then
1160:                       ReDim Preserve nPointers(nPointer)

1170:                       nPointers(nPointer) = email.Uidl

1180:                       nPointer += 1
                        End If
                    End If
                    'cleanerFiles.Add(downloadPath)
                    'cleanerFS.Add(fs)
1190:           Next

1200:           Try
1210:               bundle.Dispose()
1220:
                Catch : End Try
1230:       Else
                Dim I As Integer = 1
                Dim ok As Boolean
                Dim retry As Integer = 0

1240:           oIMAP4 = New Chilkat.Imap

1250:           oIMAP4.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")
                'Connect to the IMAP4 mail server, if user has provided custom pop3 port then use that instead
1260:           If port > 0 Then
1290:               If useSSL = True Then
1300:                   oIMAP4.Ssl = True
1310:                   oIMAP4.Port = port
1330:               Else
1340:                   oIMAP4.Port = port
1350:               End If
                End If

                Do
1351:               ok = oIMAP4.Connect(mailserver)

1352:               If ok = False Then System.Threading.Thread.Sleep(3000)

1353:               retry += 1
1354:           Loop Until ok = True Or retry > 3

1360:           If ok = False Then
1370:               Throw New Exception(oIMAP4.LastErrorText)
                End If

1380:           clsMarsUI.MainUI.BusyProgress(60, "Logging into mail server...")

                'Log onto the IMAP4 mail server
                retry = 0

1390:           Do
1400:               ok = oIMAP4.Login(username, password)

1410:               If ok = False Then System.Threading.Thread.Sleep(3000)

1420:               retry += 1
1430:           Loop Until ok = True Or retry >= 3

1440:           If ok = False Then
1450:               Throw New Exception(oIMAP4.LastErrorText)
                End If

#If DEBUG Then
                oIMAP4.PeekMode = True
#End If
1460:           ok = oIMAP4.SelectMailbox(imapBox)

1470:           If ok = False Then
1480:               Throw New Exception(oIMAP4.LastErrorText)
                End If

1490:           clsMarsUI.BusyProgress(70, "Downloading emails from server...")

                Dim result As Chilkat.MessageSet = oIMAP4.Search("UNSEEN", True)
                Dim msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, msgAttachments As String
                Dim msgDate As String = ""
                Dim msgDatesent As Date

1500:           If result.Count > 0 Then
                    Dim envs As Chilkat.EmailBundle = oIMAP4.FetchBundle(result)

1510:               For c As Integer = 0 To envs.MessageCount - 1
                        Dim email As Chilkat.Email = envs.GetEmail(c)

1520:                   If envs.MessageCount > 0 Then clsMarsUI.MainUI.BusyProgress((I / envs.MessageCount) * 100, "Parsing emails..." & CType((I / envs.MessageCount) * 100, Integer) & "%")

1530:                   msgFrom = ""
1540:                   msgTo = ""
1550:                   msgCc = ""
1560:                   msgBcc = ""
1570:                   msgSubject = ""
1580:                   msgBody = ""
1590:                   msgAttachments = ""
1600:                   msgDatesent = Nothing

1610:                   bodyParts = New Hashtable

1620:                   With bodyParts
1630:                       .Add("TEXT", "")
1640:                       .Add("HTML", "")
                        End With

1650:                   MarkForDelete = False

1660:                   msgFrom = email.From

1670:                   For n As Integer = 0 To email.NumTo - 1
1680:                       msgTo &= email.GetTo(n) & ";"
1690:                   Next

1700:                   For n As Integer = 0 To email.NumCC - 1
1710:                       msgCc &= email.GetCC(n) & ";"
1720:                   Next

1730:                   For n As Integer = 0 To email.NumBcc - 1
1740:                       msgBcc &= email.GetBcc(n) & ";"
1750:                   Next

1760:                   msgSubject = email.Subject
                        msgDate = ConDateTime(email.LocalDate)

1770:                   Dim oClean As New clsMarsParser

1780:                   If email.HasHtmlBody = True Then
1790:                       bodyParts.Item("HTML") = oClean.ParseString(email.GetHtmlBody)
                        End If

1800:                   If email.HasPlainTextBody = True Then
1810:                       bodyParts.Item("TEXT") = oClean.ParseString(email.GetPlainTextBody)
                        End If

1820:                   msgBody = oClean.CleanString(IsNull(email.GetPlainTextBody)) & vbCrLf & oClean.CleanString(IsNull(email.GetHtmlBody))

1830:                   If searchEmail(searchString, msgFrom, msgTo, msgCc, msgBcc, msgSubject, msgBody, AnyAll, msgDate) = True Then
1840:                       emailFound = True

1850:                       matchedMail.Add(email)

                            Dim row As DataRow

1860:                       row = emailData.Rows.Add

1870:                       row.Item("MsgID") = I
1880:                       row.Item("MsgFrom") = msgFrom
1890:                       row.Item("MsgTo") = msgTo
1900:                       row.Item("MsgCc") = msgCc
1910:                       row.Item("MsgBcc") = msgBcc
1920:                       row.Item("MsgSubject") = msgSubject
1930:                       row.Item("msgBody") = msgBody
1940:                       row.Item("msgDate") = ConDateTime(email.LocalDate)
1950:                       row.Item("msgTEXT") = bodyParts.Item("TEXT")
1960:                       row.Item("msgHTML") = bodyParts.Item("HTML")
1970:
                            'If saveAttachments = True Then

1980:                       attachmentsPath = attachmentsPath & Date.Now.ToString("yyyyMMddHHmmss") & "\" 'clsMarsParser.Parser.ParseString(attachmentsPath)
1990:                       clsMarsParser.Parser.ParseDirectory(attachmentsPath)

2000:                       If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

2010:                       email.SaveAllAttachments(attachmentsPath)

2020:                       For n As Integer = 0 To email.NumAttachments - 1
                                Dim att As String = email.GetAttachmentFilename(n)

                                Dim fullFile As String = attachmentsPath & att

2030:                           msgAttachments &= fullFile & ";"
2040:                       Next


2050:                       For n As Integer = 0 To email.NumRelatedItems - 1
                                Dim fName As String = email.GetRelatedFilename(n)

                                If fName = "" Then fName = IO.Path.GetRandomFileName

                                Dim fullfile As String = attachmentsPath & fName

2060:                           If email.SaveRelatedItem(n, attachmentsPath) = False Then
                                    ''console.writeLine(email.LastErrorText)
                                End If
2070:                       Next


2080:                       If msgAttachments.EndsWith(";") Then msgAttachments = msgAttachments.Remove(msgAttachments.Length - 1, 1)
                            'End If

2090:                       row.Item("Attachments") = msgAttachments
2100:                       row.Item("AttachmentsPath") = attachmentsPath

2110:                       If Download = True Then
2120:                           ReDim Preserve nPointers(nPointer)

2130:                           nPointers(nPointer) = email.GetImapUid

2140:                           nPointer += 1
                            End If
2150:                   Else
2160:                       ok = oIMAP4.SetMailFlag(email, "SEEN", 0)
                        End If

2170:                   I += 1
2180:               Next
                End If

2190:           Try
2200:               oIMAP4.Logout()
2210:               oIMAP4.Disconnect()
                Catch : End Try
            End If

2220:       Return emailFound
2230:   Catch ex As Exception
2240:       If retryCount < 6 Then
2250:

2260:           retryCount += 1
2270:           System.Threading.Thread.Sleep(5000)
2280:           GoTo RETRY
            End If

            _ErrorHandle(gScheduleName & ": " & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
2290:       Return False
        End Try

    End Function


    Private Shared Sub DeleteFoundMessages(ByVal emailData As DataTable, ByVal success As Boolean)

10:     clsMarsUI.MainUI.BusyProgress(75, "Clearing matched emails from inbox...")

20:     Quiksoft.EasyMail.POP3.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
30:     Quiksoft.EasyMail.Parse.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
40:     Quiksoft.EasyMail.IMAP4.License.Key = QuikSoftLicense '"ChristianSteven Software (Single Developer)/13046361F75871156FB5E30C56671B"
50:     Quiksoft.EasyMail.SSL.License.Key = QuikSoftSSLLicense '"ChristianSteven Software (Single Developer)/13047341F666623200000F00262C5AAE73"

        Dim pop3 As Chilkat.MailMan
        Dim oIMAP4 As Chilkat.Imap
60:     Dim SSL As SSL = New SSL
        Dim pointer As Integer = 0

70:     For Each row As DataRow In emailData.Rows
            Dim nRetry As Integer = 0
            Dim mailServer As String = row.Item("serverName")
            Dim userName As String = row.Item("userName")
            Dim password As String = row.Item("password")
            Dim Ordinals As String = row.Item("ordinalPos")
            Dim serverPort As Integer = row.Item("serverPort")
            Dim useSSL As Boolean = Convert.ToInt32(row.Item("useSSL"))
            Dim IMAPBox As String = row.Item("IMAPBox")
            Dim serverType As String = row.Item("ServerType")
            '70:         Dim nOrdinals As ArrayList = New ArrayList

            '80:         For Each s As String In Ordinals.Split(",")
            '90:             nOrdinals.Add(s)
            '100:        Next

RETRYPOINT:
80:         If serverType = "POP3" Then
90:             Try
100:                pop3 = New Chilkat.MailMan

110:                pop3.UnlockComponent("CHRISTMAILQ_quHwar0EpR6G")
                    'Connect to the POP3 mail server, if user has provided custom pop3 port then use that instead
120:                If serverPort = 0 Then
130:                    pop3.MailHost = mailServer
                        'pop3.Connect(mailserver)
140:                Else
150:                    If useSSL = True Then
160:                        pop3.PopSsl = True
170:                        pop3.MailPort = serverPort
180:                        pop3.MailHost = mailServer
                            'pop3.Connect(mailserver, port, SSL.GetInterface)
190:                    Else
200:                        pop3.MailPort = serverPort
210:                        pop3.MailHost = mailServer
                            'pop3.Connect(mailserver, port)
                        End If
                    End If

220:                clsMarsUI.MainUI.BusyProgress(60, "Deleting matched messages...")

230:                pop3.PopUsername = userName
240:                pop3.PopPassword = password

                    Dim ok As Boolean
                    Dim retry As Integer = 0

250:                Do
260:                    ok = pop3.VerifyPopConnection()

270:                    If ok = False Then System.Threading.Thread.Sleep(3000)

280:                    retry += 1
290:                Loop Until ok = True Or retry >= 3

300:                If ok = False Then
310:                    Throw New Exception(pop3.LastErrorText)
                    End If

320:                retry = 0

330:                Do
340:                    ok = pop3.VerifyPopLogin

350:                    If ok = False Then System.Threading.Thread.Sleep(3000)

360:                    retry += 1
370:                Loop Until ok = True Or retry >= 3

380:                If ok = False Then
390:                    Throw New Exception(pop3.LastErrorText)
                    End If

400:                For Each emailID As String In Ordinals.Split(",")
410:                    ok = pop3.DeleteByUidl(emailID)

420:                    If ok = False Then
430:                        ''console.writeLine(pop3.LastErrorText)
                        End If
440:                Next

450:                nPointers = Nothing
460:            Catch ex As Exception


470:                If nRetry < 5 Then
480:                    System.Threading.Thread.Sleep(30000)

490:                    GoTo RETRYPOINT
                    End If

500:                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
                End Try
510:        Else
                Dim ok As Boolean
                Dim retry As Integer = 0

520:            oIMAP4 = New Chilkat.Imap

530:            oIMAP4.UnlockComponent("CHRISTIMAPMAILQ_74Me7ddL2R3x")

540:            Try
550:                If serverPort > 0 Then
560:                    If useSSL = True Then
570:                        oIMAP4.Ssl = True
580:                        oIMAP4.Port = serverPort
590:                    Else
600:                        oIMAP4.Port = serverPort
                        End If
                    End If

610:                Do
620:                    ok = oIMAP4.Connect(mailServer)

630:                    If ok = False Then System.Threading.Thread.Sleep(3000)

640:                    retry += 1
650:                Loop Until ok = True Or retry > 3

660:                If ok = False Then
670:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

680:                retry = 0
                    'Log onto the IMAP mail server
690:                Do
700:                    ok = oIMAP4.Login(userName, password)

710:                    If ok = False Then System.Threading.Thread.Sleep(3000)

720:                    retry += 1
730:                Loop Until ok = True Or retry > 3

740:                If ok = False Then
750:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

760:                ok = oIMAP4.SelectMailbox(IMAPBox)

770:                If ok = False Then
780:                    Throw New Exception(oIMAP4.LastErrorText)
                    End If

790:                For Each emailID As String In Ordinals.Split(",")

                        Dim email As Chilkat.Email = oIMAP4.FetchSingle(emailID, True)


800:                    If email IsNot Nothing Then oIMAP4.SetMailFlag(email, "Deleted", 1)
810:                Next

820:                oIMAP4.ExpungeAndClose()

830:                oIMAP4.Logout()

840:                oIMAP4.Disconnect()
850:                nPointers = Nothing
860:            Catch ex As Exception
870:                Try
880:                    oIMAP4.Logout()
890:                    oIMAP4.Disconnect()
                    Catch : End Try

900:                If nRetry < 5 Then
910:                    System.Threading.Thread.Sleep(30000)

920:                    GoTo RETRYPOINT
                    End If

930:                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
                End Try
            End If
940:    Next
    End Sub

    Private Shared Function searchEmail(ByVal Query As String, ByVal msgFrom As String, ByVal msgTo As String, ByVal msgCc As String, ByVal msgBcc As String, _
    ByVal msgSubject As String, ByVal msgBody As String, ByVal AnyAll As String, ByVal msgDate As String) As Boolean
        Dim conditions() As String = Query.Split("|")
        Dim results() As Boolean = Nothing
        Dim I As Integer = 0

        For Each s As String In conditions
            If s.Length = 0 Then Continue For

            Dim field As String = ""
            Dim opera As String = ""
            Dim value As String = ""

            ReDim Preserve results(I)

            results(I) = False

            Dim x As Integer = 0

            For Each v As String In s.Split("_")
                If x = 0 Then
                    field = v.Trim
                ElseIf x = 1 Then
                    opera = v.Trim
                Else
                    value &= v.Trim & "_"
                End If

                x += 1
            Next

            value = value.Substring(0, value.Length - 1)

            'field = s.Split("_")(0).Trim
            'opera = s.Split("_")(1).Trim
            'value = s.Split("_")(2).Trim

            Dim searchField As String = ""

            Select Case field.ToLower
                Case "from"
                    searchField = msgFrom
                Case "to"
                    searchField = msgTo
                Case "cc"
                    searchField = msgCc
                Case "bcc"
                    searchField = msgBcc
                Case "subject"
                    searchField = msgSubject
                Case "message"
                    searchField = msgBody
                Case "date (yyyy-mm-dd hh:mm:ss)"
                    searchField = msgDate
            End Select

            Select Case opera.ToLower
                Case "="
                    If searchField.ToLower = value.ToLower Then
                        results(I) = True
                    End If
                Case "<>"
                    If searchField.ToLower <> value.ToLower Then
                        results(I) = True
                    End If
                Case "contains"
                    If searchField.ToLower Like "*" & value.ToLower & "*" Then
                        results(I) = True
                    End If
                Case "does not contain"
                    ''console.writeLine(searchField & " does not contain '" & value & "'")
                    If Not (searchField.ToLower Like "*" & value.ToLower & "*") Then
                        results(I) = True
                    End If
                Case "begins with"
                    If searchField.ToLower Like value.ToLower & "*" Then
                        results(I) = True
                    End If
                Case "ends with"
                    If searchField.ToLower Like "*" & value.ToLower Then
                        results(I) = True
                    End If
                Case ">"
                    If searchField.ToLower > value.ToLower Then
                        results(I) = True
                    End If
                Case "<"
                    If searchField.ToLower < value.ToLower Then
                        results(I) = True
                    End If
                Case "<="
                    If searchField.ToLower <= value.ToLower Then
                        results(I) = True
                    End If
                Case "=>"
                    If searchField.ToLower >= value.ToLower Then
                        results(I) = True
                    End If
            End Select

            I += 1
        Next

        If results IsNot Nothing Then
            If AnyAll = "ANY" Then
                If Array.IndexOf(results, True) > -1 Then
                    Return True
                Else
                    Return False
                End If
            ElseIf AnyAll = "ALL" Then
                If Array.IndexOf(results, False) > -1 Then
                    Return False
                Else
                    Return True
                End If
            End If
        Else
            Return False
        End If

    End Function
    Private Shared Function DoesWindowExist(ByVal windowName As String) As Process
        If windowName.IndexOf("*") > -1 Then
            For Each proc As Process In Process.GetProcesses
                If proc.MainWindowTitle.ToLower Like windowName.ToLower Then
                    Return proc
                End If
            Next
        Else
            For Each proc As Process In Process.GetProcesses
                If proc.MainWindowTitle.ToLower = windowName.ToLower Then
                    Return proc
                End If
            Next
        End If

        Return Nothing
    End Function

    Private Shared Function DoesProcessExist(ByVal processName As String) As Process
        If processName.IndexOf("*") > -1 Then
            For Each proc As Process In Process.GetProcesses
                If proc.ProcessName.ToLower Like processName.ToLower Then
                    Return proc
                End If
            Next
        Else
            For Each proc As Process In Process.GetProcesses
                If proc.ProcessName.ToLower = processName.ToLower Then
                    Return proc
                End If
            Next
        End If

        Return Nothing
    End Function

    Private Shared Sub CancelExecution(ByVal method As String)
        Application.DoEvents()
        gErrorDesc = "User Cancelled schedule execution"
        gErrorNumber = -2046082905
        gErrorSource = method
        gErrorLine = 0

        Dim ex As Exception = New Exception(gErrorDesc)

        Throw ex
    End Sub
    Public Shared Function RunEvents6(Optional ByVal eventID As Integer = 0, Optional ByVal IsRetry As Boolean = False, _
           Optional ByRef returnResult As Boolean = False, Optional ByVal MasterHistoryID As Integer = 0) As Boolean
        Dim scheduleName As String = ""
        Dim conditionName As String = ""

10:     Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim proceed() As Boolean
            Dim I As Integer = 0
            Dim anyAll As String
            Dim numTimes() As Integer
            Dim executionFlow As String = "AFTER"
            Dim emailRemover As DataTable = Nothing
            Dim retryInterval As Integer = 0
            Dim useOpHours As Boolean = False
            Dim OpHoursName As String = ""
            Dim packID As Integer = 0


20:         If eventID = 0 Then
30:             SQL = "SELECT * FROM EventAttr6 WHERE Status =1 AND (PackID IS NULL OR PackID =0) ORDER BY SchedulePriority"
40:         Else
50:             SQL = "SELECT * FROM EventAttr6 WHERE EventID =" & eventID
60:         End If

70:         oRs = clsMarsData.GetData(SQL)

80:         Dim scheduleType As String

90:         If frmMain.m_UserCancel = True Then
100:            CancelExecution("RunEvents6")
110:            Return False
            End If

120:        If oRs IsNot Nothing Then
130:            Do While oRs.EOF = False
                    m_EventDataCache = Nothing
140:                Dim retryCount As Integer = 0
                    Dim nRetry As Integer = 0
RETRY:
150:                If frmMain.m_UserCancel = True Then
160:                    CancelExecution("RunEvents6")
170:                    Return False
                    End If

180:                Try
190:                    clsMarsUI.MainUI.BusyProgress(10, "Loading condition details...")

200:                    m_StartTime = Now

210:                    eventID = oRs("eventid").Value
220:                    packID = IsNull(oRs("packid").Value, 0)
230:                    anyAll = oRs("anyall").Value
240:                    scheduleType = oRs("scheduletype").Value
250:                    executionFlow = IsNull(oRs("executionflow").Value, "AFTER")
260:                    scheduleName = oRs("eventname").Value
270:                    currentEventID = eventID
280:                    retryCount = IsNull(oRs("retrycount").Value, 5)
290:                    retryInterval = IsNull(oRs("retryinterval").Value, 0)
300:                    useOpHours = IsNull(oRs("useoperationhours").Value, 0)
310:                    OpHoursName = IsNull(oRs("operationname").Value, "")

320:                    If useOpHours = True And RunEditor = False And packID = 0 Then
330:                        If clsOperationalHours.withinOperationHours(Now, OpHoursName) = False Then
340:                            GoTo skip
350:                        End If
360:                    End If

370:                    If RunEditor = False Then
380:                        If packID = 0 Then
                                clsMarsScheduler.globalItem.TaskManager("Add", , , , , , eventID)
390:                            clsMarsScheduler.globalItem.ThreadManager(eventID, "add", clsMarsScheduler.enScheduleType.EVENTBASED)
                            End If
                        End If

400:                    If packID = 0 Then
410:                        gScheduleName = scheduleName
                            clsMarsScheduler.m_progressID = "Event:" & eventID
                        End If

420:                    Dim ors1 As ADODB.Recordset

430:                    ors1 = clsMarsData.GetData("SELECT * FROM EventConditions WHERE EventID =" & eventID)

440:                    If frmMain.m_UserCancel = True Then
450:                        CancelExecution("RunEvents6")
460:                        Return False
                        End If

470:                    m_resultsTable = New Hashtable()
480:                    I = 0
490:                    proceed = Nothing
500:                    numTimes = Nothing

510:                    Do While ors1.EOF = False
520:                        If frmMain.m_UserCancel = True Then
530:                            CancelExecution("RunEvents6")
540:                            Return False
                            End If

550:                        Dim conditionType As String = ors1("conditiontype").Value
560:                        Dim returnValue As Boolean = Convert.ToBoolean(ors1("returnvalue").Value)
570:                        conditionName = ors1("conditionname").Value
580:                        Dim conditionID As Integer = ors1("conditionid").Value

590:                        ReDim Preserve proceed(I)
600:                        ReDim Preserve numTimes(I)

610:                        clsMarsUI.MainUI.BusyProgress(30, "Processing condition: " & conditionType)

620:                        proceed(I) = False
630:                        numTimes(I) = 0

640:                        Select Case conditionType.ToLower
                                Case "database record exists"
650:                                If frmMain.m_UserCancel = True Then
660:                                    CancelExecution("RunEvents6")
670:                                    Return False
                                    End If

680:                                Dim keyValues As Object()
690:                                Dim searchCriteria As String = ors1("searchcriteria").Value
700:                                Dim connectionString As String = ors1("connectionstring").Value
710:                                Dim KeyColumn As String = ors1("keycolumn").Value
720:                                Dim sAnyAll As String = IsNull(ors1("anyall").Value)
                                    Dim cacheFolderName As String = IsNull(ors1("cachefoldername").Value, conditionName)
                                    ReDim Preserve clsMarsEvent.m_EventDataCache(I)

730:                                keyValues = DoesRecordExists(conditionName, searchCriteria, connectionString, _
                                          KeyColumn, sAnyAll, cacheFolderName)


740:                                If frmMain.m_UserCancel = True Then
750:                                    CancelExecution("RunEvents6")
760:                                    Return False
                                    End If

770:                                If keyValues IsNot Nothing Then

780:                                    If returnValue = True Then
790:                                        proceed(I) = True
800:                                    End If

810:                                    m_resultsTable.Add(conditionName, keyValues)
820:                                    numTimes(I) = keyValues.GetUpperBound(0)

830:                                    If packID = 0 Then
840:                                        clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventID =" & eventID)
850:                                    Else
860:                                        clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventPackID =" & eventID)
                                        End If
870:                                Else
880:                                    If returnValue = False Then
890:                                        proceed(I) = True
900:                                    End If
910:                                End If
920:                            Case "database record has changed"
930:                                Dim keyValues As Object()
940:                                Dim detectInserts As Boolean
950:                                Dim useConstraint As Boolean = Convert.ToInt32(IsNull(ors1("useconstraint").Value, 0))
960:                                Dim constraintValue As Integer = IsNull(ors1("constraintvalue").Value, 0)
970:                                Dim dataLastMod As String = IsNull(ors1("dataLastModified").Value, "")
                                    Dim searchCriteria As String = ors1("searchcriteria").Value
                                    Dim connString As String = ors1("connectionstring").Value
                                    Dim keyColumn As String = ors1("keycolumn").Value
                                    Dim dataModified As Boolean = False
                                    Dim lastPolled As Date = IsNull(ors1("lastpolled").Value, Date.Now)
                                    Dim runOnce As Boolean = IsNull(ors1("runonce").Value, 0)
                                    Dim ignore As Boolean = IsNull(ors1("ignorenow").Value, 0)
                                    Dim cacheFolderName As String = IsNull(ors1("cachefoldername").Value, conditionName)
980:                                detectInserts = ors1("detectinserts").Value

                                    ReDim Preserve clsMarsEvent.m_EventDataCache(I)

990:                                keyValues = HasDataChanged(conditionName, searchCriteria, connString, _
                                          keyColumn, detectInserts, returnValue, dataModified, cacheFolderName)

1000:                               Try
1010:                                   Date.Parse(dataLastMod)
1020:                               Catch ex As Exception
1030:                                   useConstraint = False
                                    End Try

1040:                               Dim doProceed As Boolean = False

1050:                               If useConstraint = False Or constraintValue = 0 Then
1060:                                   doProceed = True
1070:                               Else
1080:                                   If RunEditor = False Then
1090:                                       If Now.Subtract(lastPolled).TotalMinutes < constraintValue Then
1100:                                           GoTo skip
1110:                                       End If
1120:                                   ElseIf ignore = True And dataModified = False And RunEditor = False Then
1130:                                       GoTo 2080
                                        End If

1140:                                   Dim int As Integer = Date.Now.Subtract(dataLastMod).TotalMinutes

1150:                                   If returnValue = False Then
1160:                                       If int >= constraintValue Then
1170:                                           doProceed = True
1180:                                       End If
1190:                                   Else
1200:                                       If int <= constraintValue Then
1210:                                           doProceed = True
                                            End If
                                        End If
                                    End If

1220:                               If keyValues IsNot Nothing Then
1230:                                   If dataModified = True Then
1240:                                       clsMarsData.WriteData("UPDATE EventConditions SET dataLastModified ='" & ConDateTime(Date.Now) & "', IgnoreNow =0 WHERE ConditionID =" & conditionID)
1250:                                   ElseIf runOnce = True And doProceed = True Then
1260:                                       clsMarsData.WriteData("UPDATE EventConditions SET Ignorenow = 1 WHERE ConditionID =" & conditionID)
                                        End If
                                    End If

1270:                               If keyValues IsNot Nothing And doProceed = True Then
1280:                                   proceed(I) = True
1290:                                   numTimes(I) = keyValues.GetUpperBound(0)

1300:                                   m_resultsTable.Add(conditionName, keyValues)

1310:                                   If packID = 0 Then
1320:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventID =" & eventID)
1330:                                   Else
1340:                                       clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & keyValues.GetUpperBound(0) & " WHERE EventPackID =" & eventID)
                                        End If

                                    End If


1350:                               SQL = "UPDATE EventConditions SET LastPolled = [cd] WHERE ConditionID =" & conditionID


1360:                               If gConType = "DAT" Then
1370:                                   SQL = SQL.Replace("[cd]", "Now()")
1380:                               Else
1390:                                   SQL = SQL.Replace("[cd]", "GetDate()")
                                    End If

1400:                               clsMarsData.WriteData(SQL)

                                    '540:                                If keyValues IsNot Nothing Then

                                    '                                        clsMarsData.WriteData("UPDATE EventConditions SET dataLastModified ='" & ConDateTime(Date.Now) & "' WHERE ConditionID =" & conditionID)
                                    '543:
                                    '552:                                    If returnValue = True Then
                                    '560:                                        proceed(I) = True
                                    '570:                                        numTimes(I) = keyValues.GetUpperBound(0)
                                    '580:                                    End If

                                    '590:                                    m_resultsTable.Add(conditionName, keyValues)
                                    '600:                                Else
                                    '610:                                    If returnValue = False Then
                                    '620:                                        proceed(I) = True
                                    '630:                                    End If
                                    '                                    End If

1410:                           Case "file exists"
1420:                               If frmMain.m_UserCancel = True Then
1430:                                   CancelExecution("RunEvents6")
1440:                                   Return False
                                    End If

1450:                               Dim keyValues() As Object = Nothing
1460:                               Dim path As String = ors1("filepath").Value

1470:                               If IO.File.Exists(path) = returnValue Then
1480:                                   proceed(I) = True
1490:                               End If

1500:                               If frmMain.m_UserCancel = True Then
1510:                                   CancelExecution("RunEvents6")
1520:                                   Return False
                                    End If

1530:                               If proceed(I) = True Then

1540:                                   ReDim keyValues(4)

                                        If IO.File.Exists(path) = True Then
1550:                                       Dim fil As FileVersionInfo = FileVersionInfo.GetVersionInfo(path)

1560:                                       keyValues(0) = ExtractFileName(path)
1570:                                       keyValues(1) = GetDirectory(path)
                                            keyValues(2) = path
1580:                                       keyValues(3) = File.GetLastWriteTime(path)
1590:                                       keyValues(4) = File.GetCreationTime(path)
                                        Else
                                            keyValues(0) = ""
                                            keyValues(1) = ""
                                            keyValues(2) = ""
                                            keyValues(3) = ""
                                            keyValues(4) = ""
                                        End If

1600:                                   m_resultsTable.Add(conditionName, keyValues)
1610:                               End If
1620:                           Case "file has been modified"
1630:                               If frmMain.m_UserCancel = True Then
1640:                                   CancelExecution("RunEvents6")
1650:                                   Return False
                                    End If

1660:                               Dim keyValues() As Object

1670:                               Dim path As String
1680:                               Dim d As Date

1690:                               path = ors1("filepath").Value

1700:                               Dim OriginalDate As Date = ConDateTime(ors1("keycolumn").Value)

1710:                               d = ConDateTime(File.GetLastWriteTime(path))

1720:                               If frmMain.m_UserCancel = True Then
1730:                                   CancelExecution("RunEvents6")
1740:                                   Return False
                                    End If

1750:                               If (d <> OriginalDate) = returnValue Then
1760:                                   proceed(I) = True

1770:                                   clsMarsData.WriteData("UPDATE EventConditions SET KeyColumn = " & _
                                             "'" & d & "' WHERE ConditionID =" & conditionID)
1780:                               Else
1790:                                   proceed(I) = False
1800:                               End If

1810:                               If returnValue = True And proceed(I) = True Then

1820:                                   ReDim keyValues(4)

1830:                                   Dim fil As FileVersionInfo = FileVersionInfo.GetVersionInfo(path)

1840:                                   keyValues(0) = ExtractFileName(path)
1850:                                   keyValues(1) = GetDirectory(path)
                                        keyValues(2) = path
1860:                                   keyValues(3) = File.GetLastWriteTime(path)
1870:                                   keyValues(4) = File.GetCreationTime(path)

1880:                                   m_resultsTable.Add(conditionName, keyValues)
1890:                               End If
1900:                           Case "process exists"
1910:                               If frmMain.m_UserCancel = True Then
1920:                                   CancelExecution("RunEvents6")
1930:                                   Return False
                                    End If

1940:                               Dim processName As String
1950:                               Dim keyValues() As Object = Nothing
1960:                               Dim procFound As Process = Nothing

1970:                               processName = ors1("searchcriteria").Value

1980:                               procFound = DoesProcessExist(processName)

1990:                               If frmMain.m_UserCancel = True Then
2000:                                   CancelExecution("RunEvents6")
2010:                                   Return False
                                    End If

2020:                               If procFound IsNot Nothing Then
2030:                                   If returnValue = True Then
2040:                                       proceed(I) = True
2050:                                   Else
2060:                                       proceed(I) = False
2070:                                   End If
2080:                               ElseIf procFound Is Nothing Then
2090:                                   If returnValue = True Then
2100:                                       proceed(I) = False
2110:                                   Else
2120:                                       proceed(I) = True
2130:                                   End If
2140:                               End If

2150:                               If returnValue = True And proceed(I) = True Then
2160:                                   ReDim keyValues(6)

2170:                                   keyValues(0) = procFound.ProcessName
2180:                                   keyValues(1) = procFound.MainWindowTitle
2190:                                   keyValues(2) = procFound.TotalProcessorTime.TotalMinutes
2200:                                   keyValues(3) = procFound.UserProcessorTime.TotalMinutes
2210:                                   keyValues(4) = procFound.MaxWorkingSet
2220:                                   keyValues(5) = procFound.MinWorkingSet
2230:                                   keyValues(6) = procFound.Id
2240:                               End If

2250:                               m_resultsTable.Add(conditionName, keyValues)
2260:                           Case "unread email is present"
2270:                               If frmMain.m_UserCancel = True Then
2280:                                   CancelExecution("RunEvents6")
2290:                                   Return False
                                    End If

2300:                               Dim keyValues() As Object
2310:                               Dim emailFound As Boolean = False

2320:                               Dim pollingInterval As Integer = IsNull(ors1("pollinginterval").Value, 5)
2330:                               Dim lastPolled As Date = IsNull(ors1("lastpolled").Value, Date.Now)
2340:                               Dim popServer As String = ors1("popserver").Value
2350:                               Dim popUser As String = ors1("popuser").Value
2360:                               Dim popPassword As String = _DecryptDBValue(ors1("poppassword").Value)
2370:                               Dim popRemove As Boolean = Convert.ToBoolean(ors1("popremove").Value)
2380:                               Dim searchString As String = ors1("searchcriteria").Value
2390:                               Dim emailAnyAll As String = ors1("anyall").Value
                                    Dim serverPort As Integer = IsNull(ors1("serverport").Value, 0)
                                    Dim useSSL As Boolean = Convert.ToInt32(IsNull(ors1("usessl").Value, 0))
                                    Dim serverType As String = IsNull(ors1("mailservertype").Value, "POP3")
                                    Dim IMAPBox As String = IsNull(ors1("imapbox").Value)
                                    Dim saveAttachments As Boolean = IsNull(ors1("saveattachments").Value, 0)
                                    Dim attachmentsPath As String = IsNull(ors1("attachmentspath").Value, "")
2400:                               If serverType = "" Then serverType = "POP3"

2410:                               If RunEditor = False Then
2420:                                   If Now.Subtract(lastPolled).TotalMinutes < pollingInterval Then
#If Not Debug Then
2430:                                       GoTo skip
#End If
2440:                                   End If
2450:                               End If

2460:                               Dim emailData As New DataTable("EmailData")

2470:                               With emailData.Columns()
2480:                                   .Add("MsgID", System.Type.GetType("System.Int32"))
2490:                                   .Add("MsgFrom", System.Type.GetType("System.String"))
2500:                                   .Add("MsgTo", System.Type.GetType("System.String"))
2510:                                   .Add("MsgCc", System.Type.GetType("System.String"))
2520:                                   .Add("MsgBcc", System.Type.GetType("System.String"))
2530:                                   .Add("MsgSubject", System.Type.GetType("System.String"))
2540:                                   .Add("MsgBody", System.Type.GetType("System.String"))
2550:                                   .Add("MsgDate", System.Type.GetType("System.DateTime"))
2560:                                   .Add("MsgTEXT", System.Type.GetType("System.String"))
2570:                                   .Add("MsgHTML", System.Type.GetType("System.String"))
2580:                                   .Add("Attachments", System.Type.GetType("System.String"))
2590:                                   .Add("AttachmentsPath", System.Type.GetType("System.String"))
2600:                               End With

2610:                               emailFound = DoesEmailExist(popServer, popUser, popPassword, popRemove, _
                                         searchString, emailAnyAll, emailData, _
                                         conditionID, serverPort, useSSL, serverType, IMAPBox, saveAttachments)

2620:                               If frmMain.m_UserCancel = True Then
2630:                                   CancelExecution("RunEvents6")
2640:                                   Return False
                                    End If

2650:                               If emailFound = True Then
2660:                                   If returnValue = True Then
2670:                                       ReDim keyValues(0)

2680:                                       keyValues(0) = emailData

2690:                                       proceed(I) = True
2700:                                       numTimes(I) = IIf(emailData.Rows.Count = 0, 0, emailData.Rows.Count - 1)

2710:                                       If packID = 0 Then
2720:                                           clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & emailData.Rows.Count & " WHERE EventID =" & eventID)
2730:                                       Else
2740:                                           clsMarsData.WriteData("UPDATE ThreadManager SET DataCount =" & emailData.Rows.Count & " WHERE EventPackID =" & eventID)
                                            End If
2750:                                   End If

2760:                                   m_resultsTable.Add(conditionName, keyValues)

                                        'do save attachments, mail forwading and redirection
2770:                                   attachmentsPath = clsMarsParser.Parser.ParseString(attachmentsPath)

2780:                                   clsMarsParser.Parser.ParseDirectory(attachmentsPath)

                                        RedirectMail(matchedMail, conditionID, emailData)
2900:                                   ForwardMail(matchedMail, conditionID, emailData)

                                        Dim attachmentsFound As String = ""

                                        Try
2790:                                       If attachmentsPath IsNot Nothing Then
2800:                                           If attachmentsPath.EndsWith("\") = False Then attachmentsPath &= "\"

2810:                                           For Each row As DataRow In emailData.Rows
                                                    attachmentsFound = IsNull(row("Attachments"))
                                                    If attachmentsFound <> "" Then
2820:                                                   For Each s As String In attachmentsFound.Split(";")
                                                            If s <> "" Then
                                                                Dim attachment As String = IO.Path.GetFileName(s)

2830:                                                           IO.File.Copy(s, attachmentsPath & attachment, True)

2840:                                                           Try
2850:                                                               IO.File.Delete(s)
                                                                Catch : End Try
                                                            End If
2860:                                                   Next
                                                    End If
2870:                                               row("AttachmentsPath") = attachmentsPath
2880:                                           Next
                                            End If
                                        Catch ex As Exception
                                            _ErrorHandle("Error saving attachments - " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
                                        End Try

2890:

2910:                                   For Each fso As FileStream In cleanerFS
2920:                                       fso.Close()
2930:                                   Next

2940:                                   For Each s As String In cleanerFiles
2950:                                       Try
2960:                                           IO.File.Delete(s)
                                            Catch : End Try
2970:                                   Next

                                        'store the details of the server where email was found
2980:                                   If emailRemover Is Nothing Then
2990:                                       emailRemover = New DataTable("emailRemover")

3000:                                       With emailRemover.Columns
3010:                                           .Add("serverName", System.Type.GetType("System.String"))
3020:                                           .Add("userName", System.Type.GetType("System.String"))
3030:                                           .Add("password", System.Type.GetType("System.String"))
3040:                                           .Add("ordinalPos", System.Type.GetType("System.String"))
3050:                                           .Add("serverPort", System.Type.GetType("System.Int32"))
3060:                                           .Add("useSSL", System.Type.GetType("System.Int32"))
3070:                                           .Add("IMAPBox", System.Type.GetType("System.String"))
3080:                                           .Add("ServerType", System.Type.GetType("System.String"))
3090:                                       End With
3100:                                   End If

3110:                                   If nPointers IsNot Nothing Then
3120:                                       Dim ordinals As String = ""

3130:                                       For Each x As String In nPointers
3140:                                           ordinals &= x & ","
3150:                                       Next

3160:                                       ordinals = ordinals.Substring(0, ordinals.Length - 1)

3170:                                       Dim row As DataRow = emailRemover.NewRow

3180:                                       row.Item("serverName") = popServer
3190:                                       row.Item("userName") = popUser
3200:                                       row.Item("password") = popPassword
3210:                                       row.Item("ordinalPos") = ordinals
3220:                                       row.Item("serverPort") = serverPort
3230:                                       row.Item("useSSL") = Convert.ToInt32(useSSL)
3240:                                       row.Item("IMAPBox") = IMAPBox
3250:                                       row.Item("ServerType") = serverType

3260:                                       emailRemover.Rows.Add(row)
3270:                                   End If

3280:                                   cleanerFiles = Nothing
3290:                                   cleanerFS = Nothing
3300:                                   matchedMail = Nothing
3310:                               Else
3320:                                   If returnValue = False Then
3330:                                       proceed(I) = True
3340:                                   End If

3350:                                   Try
3360:                                       For Each fso As FileStream In cleanerFS
3370:                                           fso.Close()
3380:                                       Next

3390:                                       For Each s As String In cleanerFiles
3400:                                           IO.File.Delete(s)
3410:                                       Next
                                        Catch : End Try
                                    End If

3420:                               SQL = "UPDATE EventConditions SET LastPolled = [cd] WHERE ConditionID =" & conditionID

3430:                               If gConType = "DAT" Then
3440:                                   SQL = SQL.Replace("[cd]", "Now()")
3450:                               Else
3460:                                   SQL = SQL.Replace("[cd]", "GetDate()")
3470:                               End If

3480:                               clsMarsData.WriteData(SQL)

3490:                               If frmMain.m_UserCancel = True Then
3500:                                   CancelExecution("RunEvents6")
3510:                                   Return False
                                    End If

3520:                           Case "window is present"
3530:                               If frmMain.m_UserCancel = True Then
3540:                                   CancelExecution("RunEvents6")
3550:                                   Return False
                                    End If

3560:                               Dim windowName As String
3570:                               Dim keyValues() As Object = Nothing
3580:                               Dim procFound As Process = Nothing

3590:                               windowName = ors1("searchcriteria").Value

3600:                               procFound = DoesWindowExist(windowName)

3610:                               If procFound IsNot Nothing Then
3620:                                   If returnValue = True Then
3630:                                       proceed(I) = True
3640:                                   Else
3650:                                       proceed(I) = False
3660:                                   End If
3670:                               ElseIf procFound Is Nothing Then
3680:                                   If returnValue = True Then
3690:                                       proceed(I) = False
3700:                                   Else
3710:                                       proceed(I) = True
3720:                                   End If
3730:                               End If

3740:                               If returnValue = True And proceed(I) = True Then
3750:                                   ReDim keyValues(6)

3760:                                   keyValues(0) = procFound.ProcessName
3770:                                   keyValues(1) = procFound.MainWindowTitle
3780:                                   keyValues(2) = procFound.TotalProcessorTime
3790:                                   keyValues(3) = procFound.UserProcessorTime
3800:                                   keyValues(4) = procFound.MaxWorkingSet
3810:                                   keyValues(5) = procFound.MinWorkingSet
3820:                                   keyValues(6) = procFound.Id
3830:                               End If

3840:                               End Select

3850:                       I += 1
3860:                       If frmMain.m_UserCancel = True Then
3870:                           CancelExecution("RunEvents6")
3880:                           Return False
                            End If

3890:                       ors1.MoveNext()
3900:                   Loop

3910:                   If frmMain.m_UserCancel = True Then
3920:                       CancelExecution("RunEvents6")
3930:                       Return False
                        End If

3940:                   ors1.Close()

3950:                   clsMarsUI.MainUI.BusyProgress(40, "Processing results...")

3960:                   Dim proceeder As Boolean = False

3970:                   If proceed Is Nothing Then GoTo skip

3980:                   If anyAll = "ANY" Then
3990:                       If Array.IndexOf(proceed, True) > -1 Then
4000:                           proceeder = True
4010:                       End If
4020:                   ElseIf anyAll = "ALL" Then
4030:                       If Array.IndexOf(proceed, False) > -1 Then
4040:                           proceeder = False
4050:                       Else
4060:                           proceeder = True
4070:                       End If
4080:                   End If

4090:                   If frmMain.m_UserCancel = True Then
4100:                       CancelExecution("RunEvents6")
4110:                       Return False
                        End If

4120:                   Dim n As Integer = 0

4130:                   For Each nx As Integer In numTimes
4140:                       If nx > n Then
4150:                           n = nx
4160:                       End If
4170:                   Next

                        Dim ok As Boolean

4180:                   If proceeder = True Then
4190:                       If frmMain.m_UserCancel = True Then
4200:                           CancelExecution("RunEvents6")
4210:                           Return False
                            End If

4220:                       clsMarsUI.MainUI.BusyProgress(80, "Processing schedules...")
4230:                       Dim results As DataTable

4240:                       results = ProcessSchedules(eventID, oRs, n, executionFlow)

                            Dim okCount As Integer = 0
                            Dim failCount As Integer = 0
                            Dim msgTally As String = ""

4250:                       For Each row As DataRow In results.Rows
4260:                           If row.Item("Result") = 1 Then
4270:                               okCount += 1
4280:                           ElseIf row.Item("Result") = 0 Then
4290:                               msgTally &= row.Item("Msg") & ";"
4300:                               failCount += 1
                                End If
4310:                       Next

4320:                       If failCount > 0 Then
4330:                           ok = False
4340:                       Else
4350:                           ok = True
                            End If

4360:                       If emailRemover IsNot Nothing Then
4370:                           DeleteFoundMessages(emailRemover, ok)
4380:                       End If

                            Dim resultMsg As String = n + 1 & " items were found. " & okCount & " succeeded and " & failCount & " failed." & msgTally

4390:                       SaveEventHistory(eventID, ok, resultMsg, MasterHistoryID)

4400:                       returnResult = ok

4410:                       If ok = False And retryInterval > 0 And RunEditor = False And IsRetry = False Then
4420:                           clsMarsReport.LogRetry(0, eventID)
                            End If
4430:                   End If

4440:                   If frmMain.m_UserCancel = True Then
4450:                       CancelExecution("RunEvents6")
4460:                       Return False
                        End If

skip:
4470:                   emailRemover = Nothing
4480:                   nPointers = Nothing

4490:                   If frmMain.m_UserCancel = True Then
4500:                       CancelExecution("RunEvents6")
4510:                       Return False
                        End If

4520:               Catch ex As Exception
4530:                   If nRetry <= retryCount Then
4540:                       nRetry += 1
4550:                       System.Threading.Thread.Sleep(1000)
4560:                       emailRemover = Nothing
4570:                       GoTo RETRY
4580:                   Else
4590:                       Throw ex
4600:                   End If
4610:               End Try

4620:               If RunEditor = False Then
4630:                   clsMarsScheduler.globalItem.TaskManager("Remove", , , , , , eventID)
4640:                   clsMarsScheduler.globalItem.ThreadManager(eventID, "remove", clsMarsScheduler.enScheduleType.EVENTBASED)
                    End If

4650:               oRs.MoveNext()
4660:           Loop

4670:           clsMarsUI.MainUI.BusyProgress(, , True)

4680:           oRs.Close()

4690:           m_resultsTable = Nothing
4700:           currentEventID = 0
4710:       End If

4720:   Catch ex As Exception
4730:       If RunEditor = False Then
4740:           clsMarsScheduler.globalItem.TaskManager("Remove", , , , , , eventID)
4750:           clsMarsScheduler.globalItem.ThreadManager(eventID, "remove", clsMarsScheduler.enScheduleType.EVENTBASED)
            End If

            _ErrorHandle(scheduleName & ": " & conditionName & " [" & ex.Message & "]", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)

        End Try
    End Function



    Private Shared Function ProcessSchedules(ByVal eventID As Integer, ByVal oRs As ADODB.Recordset, ByVal loopController As Integer, _
    ByVal executionFlow As String) As DataTable
        Dim scheduleType As String
        Dim SQL As String
        Dim oRs1 As ADODB.Recordset
        Dim I As Integer = 0
        Dim ok As Boolean = True
        Dim reportTitle As String = ""
        Dim oTask As New clsMarsTask
        Dim results As DataTable = New DataTable("results")

        With results
            .Columns.Add("Pointer")
            .Columns.Add("Result")
            .Columns.Add("Msg")
        End With

        scheduleType = oRs("scheduletype").Value

        Select Case scheduleType.ToLower
            Case "none"
                For I = 0 To loopController
                    Dim progVal As Integer

                    Try
                        progVal = (I / loopController) * 100
                    Catch ex As Exception
                        progVal = 50
                    End Try

                    clsMarsUI.BusyProgress(progVal, "Processing result " & I & " of " & loopController)

                    clsMarsEvent.m_Pointer = I
                    ok = oTask.ProcessTasks(eventID, "NONE")

                    Dim row As DataRow = results.NewRow
                    Dim Msg As String = ""

                    If ok = False Then
                        Msg = gErrorDesc & " [" & gErrorNumber & "]"
                    End If

                    row.Item("Pointer") = I
                    row.Item("Result") = Convert.ToInt16(ok)
                    row.Item("Msg") = Msg

                    results.Rows.Add(row)
                Next

                Return results
            Case "new"
                For I = 0 To loopController

                    Dim progVal As Integer

                    Try
                        progVal = (I / loopController) * 100
                    Catch ex As Exception
                        progVal = 50
                    End Try

                    clsMarsUI.BusyProgress(progVal, "Processing result " & I & " of " & loopController)

                    clsMarsEvent.m_Pointer = I

                    SQL = "SELECT ReportID,ReportTitle FROM ReportAttr WHERE PackID =" & eventID

                    oRs1 = clsMarsData.GetData(SQL)

                    If oRs1 IsNot Nothing Then
                        Dim oReport As clsMarsReport = New clsMarsReport

                        If oRs1.EOF = False Then
                            Do While oRs1.EOF = False
                                reportTitle = oRs1("reporttitle").Value

                                Select Case executionFlow
                                    Case "AFTER"
                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)

                                        oTask.ProcessTasks(eventID, "NONE")

                                    Case "BEFORE"
                                        oTask.ProcessTasks(eventID, "NONE")

                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)
                                    Case "BOTH"
                                        oTask.ProcessTasks(eventID, "NONE")

                                        ok = oReport.RunSingleSchedule(oRs1(0).Value)

                                        oTask.ProcessTasks(eventID, "NONE")
                                End Select

                                Dim row As DataRow = results.NewRow
                                Dim Msg As String = ""

                                If ok = False Then
                                    Msg = " - " & reportTitle & " [" & gErrorDesc & " (" & gErrorNumber & ")]"
                                End If

                                row.Item("Pointer") = I
                                row.Item("Result") = Convert.ToInt16(ok)
                                row.Item("Msg") = Msg

                                results.Rows.Add(row)

                                'SaveEventHistory(eventID, ok, "- " & reportTitle & " executed successfully.")


                                oRs1.MoveNext()
                            Loop
                        Else
                            ok = oTask.ProcessTasks(eventID, "NONE")

                            Dim row As DataRow = results.NewRow
                            Dim Msg As String = ""

                            If ok = False Then
                                Msg = gErrorDesc & " [" & gErrorNumber & "]"
                            End If

                            row.Item("Pointer") = I
                            row.Item("Result") = Convert.ToInt16(ok)
                            row.Item("Msg") = Msg

                            results.Rows.Add(row)
                        End If

                        oRs1.Close()
                    End If
                Next

                Return results
            Case "existing"
                gRunByEvent = True

                For I = 0 To loopController

                    Dim progVal As Integer

                    Try
                        progVal = (I / loopController) * 100
                    Catch ex As Exception
                        progVal = 50
                    End Try

                    clsMarsUI.BusyProgress(progVal, "Processing result " & I & " of " & loopController)

                    clsMarsEvent.m_Pointer = I

                    Select Case executionFlow
                        Case "AFTER"
                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)

                            oTask.ProcessTasks(eventID, "NONE")
                        Case "BEFORE"
                            oTask.ProcessTasks(eventID, "NONE")

                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)
                        Case "BOTH"
                            oTask.ProcessTasks(eventID, "NONE")

                            ok = clsMarsEvent.ExecuteSchedule(oRs("eventname").Value, eventID)

                            oTask.ProcessTasks(eventID, "NONE")
                    End Select

                    Dim row As DataRow = results.NewRow
                    Dim Msg As String = ""

                    If ok = False Then
                        Msg = gErrorDesc & " [" & gErrorNumber & "]"
                    End If

                    row.Item("Pointer") = I
                    row.Item("Result") = Convert.ToInt16(ok)
                    row.Item("Msg") = Msg

                    results.Rows.Add(row)
                Next

                gRunByEvent = False

                Return results
        End Select
    End Function
    Public Function PastePackageSchedule(ByVal nPackID As Integer, ByVal nParent As Integer) As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String = " WHERE EventPackID = " & nPackID
        Dim sNew As String
        Dim CopyReports As Boolean
        Dim NewPackID As Int64
        Dim nScheduleID As Int64
        Dim oRs As ADODB.Recordset
        Dim oUI As clsMarsUI = New clsMarsUI


retry:
        Try
            sNew = InputBox("Please enter the new Event-Based Package name", Application.ProductName)

            If sNew.Length = 0 Then Return 0

            If clsMarsData.IsDuplicate("EventPackageAttr", "PackageName", sNew, True) Then
                Dim oRes As DialogResult = MessageBox.Show _
                ("An Event-Based Package with that name already exists in the system", _
                Application.ProductName, MessageBoxButtons.RetryCancel, _
                MessageBoxIcon.Exclamation)

                If oRes = DialogResult.Retry Then
                    GoTo retry
                Else
                    Return 0
                End If
            End If

            'copy package details

            oUI.BusyProgress(20, "Copying package attributes")

            NewPackID = clsMarsData.DataItem.CopyRow("EventPackageAttr", nPackID, sWhere, "EventPackID", "PackageName", _
            sNew, , , , , , , , nParent)

            If NewPackID = 0 Then GoTo RollbackTran


            oUI.BusyProgress(40, "Copying package schedule details")

            nScheduleID = clsMarsData.DataItem.CopyRow("ScheduleAttr", 0, sWhere, "ScheduleID", , , , , , , , , , , NewPackID)

            If nScheduleID = 0 Then GoTo RollbackTran

            oUI.BusyProgress(80, "Copying custom tasks")

            oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
            "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr WHERE " & _
            "EventPackID = " & nPackID & ")")

            Do While oRs.EOF = False

                If clsMarsData.DataItem.CopyRow("Tasks", oRs(0).Value, " WHERE TaskID =" & oRs(0).Value, "TaskID", , , , _
                  , , , nScheduleID, , , , NewPackID) = 0 Then GoTo RollbackTran

                oRs.MoveNext()
            Loop

            oRs.Close()

            'copy event based schedules
            oRs = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID)

            oUI.BusyProgress(85, "Copying schedules...")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False

                    Me.PasteSchedule(oRs.Fields(0).Value, NewPackID, "Package", NewPackID, False)
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If


        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GoTo RollbackTran
        End Try

        oUI.BusyProgress(100, "Cleaning up")

        oUI.BusyProgress(, , True)

        Return NewPackID
RollbackTran:
        With clsMarsData.DataItem
            .WriteData("DELETE FROM EventPackageAttr WHERE EventPackID = " & NewPackID)
            .WriteData("DELETE FROM ScheduleAttr WHERE EventPackID = " & NewPackID)
            .WriteData("DELETE FROM Tasks WHERE ScheduleID = " & nScheduleID)
            .WriteData("DELETE FROM EventAttr6 WHERE PackID =" & NewPackID)
        End With

        oUI.BusyProgress(, , True)

        Return 0
    End Function
    Public Sub PasteSchedule(ByVal nEventID As Integer, ByVal nParent As Integer, ByVal parentType As String, Optional ByVal PackID As Integer = 0, Optional ByVal promptName As Boolean = True)
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Int64
        Dim oRs As ADODB.Recordset
        Dim sOld As String = ""
        Dim sName As String = ""

        oRs = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID =" & nEventID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                sOld = oRs.Fields(0).Value
                'sType = oRs(1).Value
            End If

            oRs.Close()
        End If

Retry:
        If promptName = True Then
            sName = InputBox("Please enter the new event-based schedule name", _
            Application.ProductName, "Copy of " & sOld)

            If sName.Length = 0 Then Return

            If clsMarsData.IsDuplicate("EventAttr", "EventName", sName, False) = True Then
                If MessageBox.Show("An event-based schedule with this name already exists.", Application.ProductName, _
                 MessageBoxButtons.RetryCancel) = DialogResult.Retry Then
                    GoTo Retry
                Else
                    Return
                End If
            End If
        Else
            sName = sOld
        End If

        If parentType.ToLower = "folder" Then
            nID = clsMarsData.DataItem.CopyRow("EventAttr6", nEventID, " WHERE EventID = " & nEventID, "EventID", "EventName", sName, , PackID, , nEventID, _
            , , , nParent)
        Else
            nID = clsMarsData.DataItem.CopyRow("EventAttr6", nEventID, " WHERE EventID = " & nEventID, "EventID", "EventName", sName, , nParent, , nEventID, _
            , , , 0)
        End If

        SQL = "SELECT * FROM EventConditions WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim cols, vals As String
                Dim newID As Integer = clsMarsData.CreateDataID("eventconditions", "conditionid")
                Dim conditionName As String = oRs("conditionName").Value

                Dim cacheFolderName As String = conditionName & "." & newID  'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())

                cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,ConnectionString,KeyColumn,DetectInserts,FilePath," & _
                "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll,OrderID," & _
                "PollingInterval,LastPolled,RedirectMail,RedirectTo,ForwardMail," & _
                "serverPort,UseSSL,MailServerType,IMAPBox,UseConstraint,ConstraintValue,RunOnce,IgnoreNow,DataLastModified," & _
                "SaveAttachments,AttachmentsPath,CacheFolderName"

                vals = newID & "," & _
                nID & ",ConditionName,ConditionType,ReturnValue,ConnectionString,KeyColumn,DetectInserts,FilePath," & _
                "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll,OrderID," & _
                "PollingInterval,LastPolled,RedirectMail,RedirectTo,ForwardMail," & _
                "serverPort,UseSSL,MailServerType,IMAPBox,UseConstraint,ConstraintValue,RunOnce,IgnoreNow,DataLastModified," & _
                "SaveAttachments,AttachmentsPath,'" & SQLPrepare(cacheFolderName) & "'"


                SQL = "INSERT INTO EventConditions (" & cols & ") " & _
                "SELECT " & vals & " FROM EventConditions WHERE ConditionID =" & oRs("conditionid").Value

                If clsMarsData.WriteData(SQL) = False Then
                    Exit Do
                End If

                If oRs("conditiontype").Value = "datbase record exists" Then
                    CacheData(sName, oRs("searchcriteria").Value, oRs("connectionstring").Value, cacheFolderName)
                ElseIf oRs("conditiontype").Value = "database record has changed" Then
                    CacheData(sName, oRs("searchcriteria").Value, oRs("connectionstring").Value, oRs("keycolumn").Value, cacheFolderName)
                End If

                Dim ConditionID As Integer = oRs.Fields("conditionid").Value

                clsMarsData.DataItem.CopyRow("ForwardMailAttr", ConditionID, " WHERE ConditionID =" & ConditionID, _
                "ConditionID")

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "SELECT * FROM ReportAttr WHERE PackID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim reportID As Integer = oRs("reportid").Value

                reportID = clsMarsReport.oReport.CopyReport(reportID, 0, True)

                clsMarsData.WriteData("UPDATE ReportAttr SET PackID =" & nID & " WHERE ReportID =" & reportID)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


        SQL = "SELECT * FROM EventSchedule WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        sCols = "ID,EventID,ScheduleID,ScheduleType,Status"

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False

                sVals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                nID & "," & _
                "ScheduleID,ScheduleType,Status"

                SQL = "INSERT INTO EventSchedule " & _
                "(" & sCols & ") " & _
                "SELECT " & sVals & " FROM EventSchedule " & _
                "WHERE EventID = " & nEventID

                clsMarsData.WriteData(SQL)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oRs = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE ScheduleID =" & nEventID)

        Do While oRs.EOF = False

            clsMarsData.DataItem.CopyRow(" Tasks", oRs(0).Value, " WHERE TaskID =" & oRs(0).Value, "TaskID", , , , _
            , , , nID)

            oRs.MoveNext()
        Loop

        oRs.Close()

        If promptName = True Then
            Dim oForm As New frmEventProp

            oForm.txtName.Text = sName

            oForm.cmdCancel.Visible = False

            oForm.EditSchedule(nID)
        End If
    End Sub

    Public Sub RunEvents(Optional ByVal EventID As Integer = 0)
        Dim SQL As String
        Dim sType As String
        Dim CheckValue As Boolean
        Dim sPath As String
        Dim sCon As String
        Dim d As Date
        Dim oTest As ADODB.Recordset
        Dim oCon As ADODB.Connection
        Dim s1 As String
        Dim s2 As String
        Dim Proceed As Boolean
        Dim Success As Boolean
        Dim oParse As New clsMarsParser
        Dim nRecords As Integer = 0
        Dim oUI As New clsMarsUI


        If EventID = 0 Then
            SQL = "SELECT * FROM EventAttr WHERE Status = 1"
        Else
            SQL = "SELECT * FROM EventAttr WHERE EventID =" & EventID
        End If

        oUI.BusyProgress(10, "Collecting conditions...")

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False

                Proceed = False

                sType = oRs("eventtype").Value
                EventID = oRs("eventid").Value
                gScheduleName = oRs("eventname").Value

                Try
                    CheckValue = Convert.ToBoolean(oRs("firewhen").Value)
                Catch ex As Exception
                    CheckValue = True
                End Try

                oUI.BusyProgress(20, "Obtaining event attributes...")


                Select Case sType.ToLower
                    Case "file exists"

                        oUI.BusyProgress(30, "Condition: IF File Exists =" & CheckValue.ToString)

                        sPath = oParse.ParseString(oRs("filepath").Value)

                        If File.Exists(sPath) = CheckValue Then
                            Proceed = True
                        Else
                            Proceed = False
                        End If

                    Case "file has been modified"

                        oUI.BusyProgress(30, "Condition: IF File Has Been Modified =" & CheckValue.ToString)

                        sPath = oRs("filepath").Value

                        Dim OriginalDate As Date = oRs("moddatetime").Value

                        d = File.GetLastWriteTime(sPath)

                        OriginalDate = ConDate(OriginalDate) & " " & ConTime(OriginalDate)

                        d = ConDate(d) & " " & ConTime(d)

                        If (d <> OriginalDate) = CheckValue Then
                            Proceed = True

                            'Try
                            '    ExecuteSchedule(gScheduleName, EventID)
                            'Catch : End Try

                            clsMarsData.WriteData("UPDATE EventAttr SET ModDateTime = " & _
                            "'" & d & "' WHERE EventID =" & EventID)
                        Else
                            Proceed = False
                        End If

                    Case "database record exists"

                        oUI.BusyProgress(30, "Condition: IF Database Record Exists =" & CheckValue.ToString)

                        sCon = oRs("search1").Value

                        Dim sQuery As String = oRs("search2").Value
                        Dim sDsn As String = sCon.Split("|")(0)
                        Dim sUser As String = sCon.Split("|")(1)
                        Dim sPassword As String = sCon.Split("|")(2)
                        Dim oRsOld As ADODB.Recordset
                        Dim sDataPath As String
                        Dim sKey As String

                        sKey = IsNull(oRs("filepath").Value, "")

                        oCon = New ADODB.Connection
                        oTest = New ADODB.Recordset

                        oCon.Open(sDsn, sUser, sPassword)

                        oTest.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

                        sDataPath = clsMarsEvent.m_CachedDataPath & "dbe_" & gScheduleName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        sDataPath &= "\datacache.xml"

                        If IO.File.Exists(sDataPath) = True And sKey.Length > 0 Then
                            oRsOld = clsMarsData.GetXML(sDataPath)

                            Do While oTest.EOF = False

                                oRsOld.Filter = ""

                                If oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adDate Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adDBDate Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adLongVarChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adLongVarWChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adVarChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adVarWChar Or _
                                oTest.Fields(sKey).Type = ADODB.DataTypeEnum.adWChar Then
                                    oRsOld.Filter = sKey & "='" & SQLPrepare(oTest.Fields(sKey).Value) & "'"
                                Else
                                    oRsOld.Filter = sKey & "=" & SQLPrepare(oTest.Fields(sKey).Value)
                                End If

                                If oRsOld.EOF = CheckValue Then
                                    Proceed = True
                                    nRecords += 1
                                End If

                                oTest.MoveNext()
                            Loop
                        Else
                            If oTest.EOF = Not CheckValue Then
                                Proceed = True
                                nRecords = oTest.RecordCount
                            Else
                                Proceed = False
                            End If
                        End If

                        clsMarsData.CreateXML(oTest, sDataPath)

                    Case "database record has changed"

                        oUI.BusyProgress(30, "Condition: IF Database Record Has Been Modified =" & CheckValue.ToString)

                        sCon = oRs("search1").Value

                        Dim sQuery As String = oRs("search2").Value
                        Dim oReturn() As Object
                        Dim dataMod As Boolean
                        'oReturn = HasDataChanged(oRs("eventname").Value, sQuery, sCon, oRs("filepath").Value, _
                        'Convert.ToBoolean(oRs("popremove").Value), True, dataMod)

                        If Not oReturn Is Nothing Then
                            If oReturn(0) = CheckValue Then
                                Proceed = True
                                nRecords = oReturn(1)
                            Else
                                Proceed = False
                            End If
                        End If

                    Case "window is present"

                        oUI.BusyProgress(30, "Condition: IF Window Is Present =" & CheckValue.ToString)

                        s1 = oRs("search1").Value

                        For Each oProc As Process In Process.GetProcesses
                            If oProc.MainWindowTitle.Length > 0 Then
                                If (oProc.MainWindowTitle.ToLower. _
                                IndexOf(s1.ToLower) > -1) = CheckValue Then
                                    Proceed = True
                                    Exit For
                                Else
                                    Proceed = False
                                End If
                            End If
                        Next
                    Case "process exists"

                        oUI.BusyProgress(30, "Condition: IF Process Exists =" & CheckValue.ToString)

                        s1 = oRs("search1").Value

                        For Each oProc As Process In Process.GetProcesses
                            If oProc.ProcessName.Length > 0 Then
                                If (oProc.ProcessName.ToLower. _
                                IndexOf(s1.ToLower) > -1) = CheckValue Then
                                    Proceed = True
                                    Exit For
                                Else
                                    Proceed = False
                                End If
                            End If
                        Next
                    Case "unread email is present"

                        oUI.BusyProgress(30, "Condition: IF Unread Email Exists =" & CheckValue.ToString)

                        Dim oMsg As New clsMarsMessaging
                        Dim sServer As String
                        Dim sUser As String
                        Dim sPassword As String
                        Dim Found As Boolean
                        Dim Remove As Boolean

                        sServer = oRs("popserver").Value
                        sUser = oRs("popuserid").Value
                        sPassword = oRs("poppassword").Value
                        s1 = oRs("search1").Value
                        s2 = oRs("search2").Value

                        Try
                            Remove = Convert.ToBoolean(oRs("popremove").Value)
                        Catch ex As Exception
                            Remove = False
                        End Try

                        Found = oMsg.DownloadMessageToMemoryStream(sServer, sUser, sPassword, Remove, _
                        s1, s2)

                        If Found = CheckValue Then
                            Proceed = True
                        Else
                            Proceed = False
                        End If
                End Select

                If Proceed = True Then

                    oUI.BusyProgress(70, "Executing schedule...")

                    If nRecords = 0 Then nRecords = 1

                    For I As Integer = 1 To nRecords
                        Success = ExecuteSchedule(gScheduleName, EventID)
                    Next

                    If Success = True Or gErrorDesc.Length = 0 Or gErrorNumber = 0 Then
                        Dim oTask As New clsMarsTask

                        oTask.ProcessTasks(EventID, "NONE")

                        SaveEventHistory(EventID, Success)
                    Else
                        SaveEventHistory(EventID, Success, gErrorDesc & _
                        " [" & gErrorNumber & "]")
                    End If

                    If sType = "file has been modified" Then
                        clsMarsData.WriteData("UPDATE EventAttr SET ModDateTime = " & _
                        "'" & d & "' WHERE EventID =" & EventID)
                    End If
                End If

                oRs.MoveNext()
            Loop

            oUI.BusyProgress(100, "")

            oUI.BusyProgress(, , True)

            oRs.Close()
        Catch ex As Exception
            ''console.writeLine(ex.Message)

            oUI.BusyProgress(, , True)
        End Try

    End Sub

    Private Shared Function ExecuteSchedule(ByVal sEvent As String, ByVal nID As Integer) As Boolean
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sType As String
        Dim oReport As New clsMarsReport
        Dim oAuto As New clsMarsAutoSchedule
        Dim nKey As Integer
        Dim Success As Boolean
        Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Integer
        Dim scheduleName As String

        SQL = "SELECT * FROM EventSchedule WHERE EventID =" & nID

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                sType = oRs("scheduletype").Value
                nKey = oRs("scheduleid").Value

                Select Case sType.ToLower
                    Case "report"
                        Dim type As String = ""

                        ScheduleStart = Now

                        SQL = "SELECT Dynamic, Bursting,ReportTitle,IsDataDriven FROM ReportAttr WHERE ReportID =" & nKey

                        Dim oTest As ADODB.Recordset = clsMarsData.GetData(SQL)

                        'If gSubject.Length > 0 Then
                        '    gRunByEvent = True
                        '    gEventID = nID
                        'End If

                        scheduleName = oTest.Fields("reporttitle").Value

                        If IsNull(oTest.Fields(0).Value, 0) = 1 Then
                            Success = oReport.RunDynamicSchedule(nKey, False)
                            type = " - Dynamic schedule '"
                        ElseIf IsNull(oTest.Fields(3).Value, 0) = 1 Then
                            Success = oReport.RunDataDrivenSchedule(nKey)
                            type = " - Data-Driven schedule '"
                        Else
                            Success = oReport.RunSingleSchedule(nKey, False)
                            type = " - Single schedule '"
                        End If

                        'gRunByEvent = False
                        'gEventID = 0
                        'gSubject = String.Empty

                        If Success = True Then
                            oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, nKey)
                            SaveEventHistory(nID, True, type & scheduleName & "' executed successfully.")
                        Else
                            oSchedule.SetScheduleHistory(False, _
                            "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                            gErrorDesc & " [" & gErrorNumber & "]", nKey)
                            SaveEventHistory(nID, False, type & scheduleName & "' : " & gErrorDesc & " [" & gErrorNumber & "]")
                        End If

                    Case "package"

                        ScheduleStart = Now
                        Dim oTest As ADODB.Recordset = clsMarsData.GetData("SELECT PackageName FROM PackageAttr WHERE PackID =" & nKey)

                        If oTest IsNot Nothing Then
                            If oTest.EOF = False Then
                                scheduleName = oTest.Fields("packagename").Value
                            End If

                            oTest.Close()
                        End If

                        Dim type As String

                        If clsMarsData.IsScheduleDynamic(nKey, "Package") = True Then
                            Success = oReport.RunDynamicPackageSchedule(nKey)
                            type = " - Dynamic package schedule '"
                        Else
                            Success = oReport.RunPackageSchedule(nKey)
                            type = " - Package schedule '"
                        End If

                        If Success = True Then
                            oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, _
                            , nKey)
                            SaveEventHistory(nID, True, type & scheduleName & "' executed successfully.")
                        Else
                            oSchedule.SetScheduleHistory(False, _
                            "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                            gErrorDesc & " [" & gErrorNumber & "]", , nKey)
                            SaveEventHistory(nID, False, type & scheduleName & "' : " & gErrorDesc & " [" & gErrorNumber & "]")
                        End If
                    Case "automation"
                        Success = True
                        oAuto.ExecuteAutomationSchedule(nKey)
                        nScheduleID = clsMarsScheduler.GetScheduleID(, , nKey)

                        Dim oTest As ADODB.Recordset = clsMarsData.GetData("SELECT Autoname FROM AutomationAttr WHERE AutoID =" & nKey)

                        If oTest IsNot Nothing Then
                            If oTest.EOF = False Then
                                scheduleName = oTest.Fields("autoname").Value
                            End If

                            oTest.Close()
                        End If
                        If Success = True Then
                            oSchedule.SetScheduleHistory(True, "Schedule fired by Event-Based Schedule: " & sEvent, _
                            , , nKey)
                            SaveEventHistory(nID, True, " - Automation schedule '" & scheduleName & "' executed successfully.")
                        Else
                            oSchedule.SetScheduleHistory(False, _
                            "Schedule fired by Event-Based Schedule: " & sEvent & vbCrLf & _
                            gErrorDesc & " [" & gErrorNumber & "]", , , nKey)
                            SaveEventHistory(nID, False, "Automation schedule '" & scheduleName & "': " & gErrorDesc & " [" & gErrorNumber & "]")
                        End If
                End Select

                oRs.MoveNext()
            Loop

            Return Success
            oRs.Close()
        Catch ex As Exception
            gErrorNumber = Err.Number
            gErrorDesc = ex.Message
            Return False
        End Try
    End Function

    Public Shared Sub SaveEventHistory(ByVal nID As Integer, ByVal Status As Boolean, _
    Optional ByVal sMsg As String = "", Optional ByVal MasterHistoryID As Integer = 0)
        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim lastFired As Date

        sCols = "HistoryID,EventID,LastFired,Status,ErrMsg,StartDate,MasterHistoryID"

        If RunEditor = True Then
            lastFired = CTimeZ(Now, dateConvertType.WRITE)
        Else
            lastFired = Now
        End If

        sVals = clsMarsData.CreateDataID("eventhistory", "historyid") & "," & _
        nID & "," & _
        "'" & ConDateTime(lastFired) & "'," & _
        Convert.ToInt32(Status) & "," & _
        "'" & SQLPrepare(sMsg) & "'," & _
        "'" & ConDateTime(m_StartTime) & "'," & _
        MasterHistoryID

        SQL = "INSERT INTO EventHistory (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL, False, False) = False Then

        End If
    End Sub

    Public Sub DrawEventHistory(ByVal oTree As TreeView, _
    Optional ByVal nID As Integer = 0, Optional ByVal SortOrder As String = "ASC", _
    Optional ByVal Filter As Boolean = False)
        Dim SQL As String
        Dim sWhere As String = String.Empty
        Dim oRs As ADODB.Recordset
        Dim oChild As ADODB.Recordset
        Dim oMain As ADODB.Recordset

        oTree.Nodes.Clear()

        If nID > 0 Then
            sWhere = " WHERE EventID =" & nID
        End If

        If nID = 0 Then
            SQL = "SELECT EventID, EventName FROM EventAttr6 WHERE (PackID IS NULL OR PackID = 0)"

            If Filter = True Then
                SQL &= " AND Status = 1"
            End If
        Else
            SQL = "SELECT EventID, EventName FROM EventAttr6 WHERE EventID =" & nID

            If Filter = True Then
                SQL &= " AND Status =1"
            End If
        End If



        oMain = clsMarsData.GetData(SQL)

        Try
            Do While oMain.EOF = False

                Dim ParentNode As TreeNode = New TreeNode

                ParentNode.Text = oMain(1).Value
                ParentNode.ImageIndex = 0
                ParentNode.SelectedImageIndex = 0

                nID = oMain(0).Value

                SQL = "SELECT * FROM EventHistory WHERE EventID =" & nID & " ORDER BY LastFired " & SortOrder

                oChild = clsMarsData.GetData(SQL)

                Try
                    Do While oChild.EOF = False
                        Dim oChildNode As TreeNode = New TreeNode

                        If oChild("status").Value = 1 Then

                            oChildNode.Text = "Last Fired: " & CTimeZ(oChild("lastfired").Value, dateConvertType.READ) & " - [Success " & IsNull(oChild("errmsg").Value) & "]"
                            oChildNode.ImageIndex = 1
                            oChildNode.SelectedImageIndex = 1
                        Else
                            oChildNode.Text = "Last Fired: " & CTimeZ(oChild("lastfired").Value, dateConvertType.READ) & " [Failed " & oChild("errmsg").Value & "]"
                            oChildNode.ImageIndex = 2
                            oChildNode.SelectedImageIndex = 2
                        End If

                        ParentNode.Nodes.Add(oChildNode)
                        oChild.MoveNext()
                    Loop
                    oChild.Close()
                Catch : End Try

                oTree.Nodes.Add(ParentNode)

                oMain.MoveNext()
            Loop

            oMain.Close()

            oTree.Nodes(0).Expand()
        Catch : End Try
    End Sub
    Public Shared Function HasDataChanged(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, _
    ByVal sKeyColumn As String, ByVal IncludeNewData As Boolean, ByVal returnValue As Boolean, _
    ByRef dataModified As Boolean, ByVal cacheFolderName As String) As Object()
        Dim sPath As String
        Dim sKeyValue As String
        Dim sText As String
        Dim nCode As Int64
        Dim nRecords As Integer = 0
        Dim oReturn() As Object
        Dim HasChanged As Boolean
        Dim I As Integer = 0
        Dim z As Integer = 1

        Try
            sPath = clsMarsEvent.m_CachedDataPath

            If System.IO.Directory.Exists(sPath) = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            sPath &= cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

            If System.IO.Directory.Exists(sPath) = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            If System.IO.File.Exists(sPath & "\data.xml") = False Then Throw New Exception("Could not load cached data for this schedule (" & sName & ")")

            Dim oXML As New ADODB.Recordset

            oXML = clsMarsData.GetXML(sPath & "\data.xml")

            Dim oRs As New ADODB.Recordset
            Dim oCon As New ADODB.Connection

            oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly, 64)

            If oRs IsNot Nothing And clsMarsEvent.m_EventDataCache IsNot Nothing Then
                Dim ubound As Integer = clsMarsEvent.m_EventDataCache.GetUpperBound(0)

                clsMarsEvent.m_EventDataCache(ubound) = clsMarsData.DataItem.cacheRecordset(oRs)

                If clsMarsEvent.m_EventDataCache(ubound) IsNot Nothing Then
                    clsMarsEvent.m_EventDataCache(ubound).TableName = sName
                End If
            End If

            oRs.Close()
            oRs.Open()

            Do While oRs.EOF = False
                HasChanged = False

                clsMarsUI.MainUI.BusyProgress((z / oRs.RecordCount) * 100, "Checking records...")
                z += 1

                sText = String.Empty

                sKeyValue = IsNull(oRs(sKeyColumn).Value)

                For x As Integer = 0 To oRs.Fields.Count - 1
                    If oRs.Fields(x).Name <> sKeyColumn Then
                        Try
                            sText &= IsNull(oRs(x).Value) & "|"
                        Catch : End Try
                    End If
                Next

                nCode = sText.GetHashCode

                oXML.Filter = "IDColumn = '" & SQLPrepare(sKeyValue) & "'"

                If oXML.EOF = True Then
                    If IncludeNewData = True Then
                        HasChanged = True
                        dataModified = True
                    End If
                Else
                    If oXML.Fields("HashValue").Value <> nCode Then
                        HasChanged = True
                        dataModified = True
                    End If
                End If

                If HasChanged = returnValue Then
                    ReDim Preserve oReturn(I)
                    oReturn(I) = sKeyValue
                    I += 1
                End If

                oRs.MoveNext()
            Loop

            Try
                'lets detect deleted columns
                oXML.MoveFirst()
                oRs.MoveFirst()
                z = 0
                oXML.Filter = ""
                ''console.writeLine(oXML.RecordCount)

                If IncludeNewData = True Then
                    Do While oXML.EOF = False
                        HasChanged = False
                        clsMarsUI.MainUI.BusyProgress((z / oRs.RecordCount) * 100, "Checking records...")
                        z += 1
                        sText = ""

                        'get the key value
                        sKeyValue = IsNull(oXML("IDColumn").Value)
                        ''console.writeLine(sKeyValue)
                        'get the current hash code
                        Dim hashCode = oXML("hashvalue").Value

                        oRs.Filter = sKeyColumn & " = '" & SQLPrepare(sKeyValue) & "'"

                        If oRs.EOF = True Then
                            If IO.File.Exists(sPath & "deletedkeys.dat") = True Then
                                Dim prevValues As String = ReadTextFromFile(sPath & "\deletedkeys.dat")
                                Dim dup As Boolean = False

                                For Each s As String In prevValues.Split(";")
                                    If s = sKeyValue Then
                                        dup = True
                                        Exit For
                                    End If
                                Next

                                If dup = False Then
                                    HasChanged = True
                                    dataModified = True
                                End If
                            Else
                                HasChanged = True
                                dataModified = True

                                SaveTextToFile(sKeyValue & ";", sPath & "\deletedkeys.dat", , True, False)
                            End If
                        End If

                        If HasChanged = returnValue Then
                            ReDim Preserve oReturn(I)
                            oReturn(I) = sKeyValue
                            I += 1
                        End If

                        oXML.MoveNext()
                        oRs.Filter = ""
                    Loop

                End If
            Catch : End Try

            If (oReturn IsNot Nothing) Then
                CacheData(sName, sQuery, sCon, sKeyColumn, cacheFolderName)
            End If

            oRs.Close()
            oCon.Close()

            Return oReturn
        Catch ex As Exception
            gErrorDesc = ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            Return Nothing
        End Try


    End Function

    Public Overloads Shared Function CacheData(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, _
    ByVal sKeyColumn As String, ByVal cacheFolderName As String) As Boolean
        Try
            Dim oRs As New ADODB.Recordset
            Dim oCon As New ADODB.Connection
            Dim sKeyValue As String
            Dim sText As String
            Dim nCode As Int64
            Dim oXML As New ADODB.Recordset

            sKeyColumn = sKeyColumn.Replace("[", "").Replace("]", "")
            oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

            oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, 64)

            oXML.Fields.Append("IDColumn", ADODB.DataTypeEnum.adVarChar, 255)
            oXML.Fields.Append("HashValue", ADODB.DataTypeEnum.adInteger)
            oXML.Open()

            Do While oRs.EOF = False
                sText = String.Empty

                sKeyValue = IsNull(oRs(sKeyColumn).Value)

                For I As Integer = 0 To oRs.Fields.Count - 1

                    If oRs.Fields(I).Name <> sKeyColumn Then
                        Try
                            sText &= IsNull(oRs(I).Value) & "|"
                        Catch : End Try
                    End If
                Next

                nCode = sText.GetHashCode

                oXML.AddNew()
                oXML.Fields(0).Value = sKeyValue
                oXML.Fields(1).Value = nCode
                oXML.Update()

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim sPath As String

            sPath = clsMarsEvent.m_CachedDataPath

            If System.IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

            sPath &= cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

            If System.IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

            clsMarsData.CreateXML(oXML, sPath & "\data.xml")

            oCon.Close()

            oCon = Nothing

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        End Try
    End Function

    Public Overloads Shared Sub CacheData(ByVal sName As String, ByVal sQuery As String, ByVal sCon As String, ByVal cacheFolderName As String)
        Dim oRs As New ADODB.Recordset
        Dim oCon As New ADODB.Connection

        oCon.Open(sCon.Split("|")(0), sCon.Split("|")(1), _DecryptDBValue(sCon.Split("|")(2)))

        oRs.Open(sQuery, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly, 64)

        Dim sPath As String = clsMarsEvent.m_CachedDataPath

        If IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

        sPath &= "dbe_" & cacheFolderName 'sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

        If IO.Directory.Exists(sPath) = False Then IO.Directory.CreateDirectory(sPath)

        clsMarsData.CreateXML(oRs, sPath & "\datacache.xml")

        oCon.Close()

        oCon = Nothing
    End Sub

    Public Shared Sub DeleteEvent(ByVal eventID As Integer)
        Dim SQL As String

        SQL = "DELETE FROM EventSchedule WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        SQL = "SELECT ConditionID FROM EventConditions WHERE EventID =" & eventID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim conditionID As Integer = oRs(0).Value

                DeleteCondition(conditionID)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "DELETE FROM EventAttr6 WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM EventHistory WHERE EventID =" & eventID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID =" & eventID, False)

        oRs = Nothing
    End Sub

    Public Shared Sub DeleteEventPackage(ByVal nPackID As Integer)
        Dim SQL As String

        Dim nScheduleID As Integer
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler

        nScheduleID = clsMarsScheduler.GetScheduleID(, , , , nPackID)

        SQL = "DELETE FROM ScheduleOptions WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ScheduleAttr WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM Tasks WHERE ScheduleID =" & nScheduleID

        clsMarsData.WriteData(SQL)

        SQL = "SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                DeleteEvent(oRs("eventid").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        SQL = "DELETE FROM EventPackageAttr WHERE EventPackID =" & nPackID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ScheduleHistory WHERE EventPackID =" & nPackID

        clsMarsData.WriteData(SQL)

    End Sub
    Public Shared Sub DeleteCondition(ByVal conditionID As Integer)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim conditionType As String
        Dim conditionName As String
        Dim path As String = clsMarsEvent.m_CachedDataPath

        SQL = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                conditionType = oRs("conditiontype").Value
                conditionName = oRs("conditionname").Value

                Select Case conditionType.ToLower
                    Case "database record exists"

                        path &= "dbe_" & conditionName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        If IO.Directory.Exists(path) Then
                            IO.Directory.Delete(path, True)
                        End If

                    Case "database record has been modified"
                        path &= conditionName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                        If IO.Directory.Exists(path) Then
                            IO.Directory.Delete(path, True)
                        End If
                End Select
            End If
        End If

        SQL = "DELETE FROM EventConditions WHERE ConditionID =" & conditionID

        clsMarsData.WriteData(SQL)
    End Sub

    Public Shared Function IsConditionDuplicate(ByVal eventID As Integer, ByVal conditionName As String) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM EventConditions WHERE eventID =" & eventID & " AND ConditionName ='" & SQLPrepare(conditionName) & "'"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = True Then
                oRs.Close()
                Return False
            Else
                oRs.Close()
                Return True
            End If
        End If
    End Function

    Public Shared Sub Convert5to6()
        Try
            Dim SQL As String = "SELECT * FROM EventAttr"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim eventID As Integer = oRs("eventid").Value
                Dim eventName As String = oRs("eventname").Value
                Dim conditionType As String = oRs("eventType").Value
                Dim fireWhen As Integer = oRs("firewhen").Value
                Dim newID As Integer = clsMarsData.CreateDataID("eventattr6", "eventid")

                'copy the main event
                Dim cols As String = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate,ScheduleType,AnyAll"
                Dim vals As String = "EventID,EventName,Description,Keyword,Parent,Status,Owner,DisabledDate,'Existing','ALL'"

                SQL = "INSERT INTO EventAttr6 (" & cols & ") " & _
                "SELECT " & vals & " FROM EventAttr WHERE EventID =" & eventID

                If clsMarsData.WriteData(SQL, False) = True Then
                    cols = "ConditionID,EventID,ConditionName,ConditionType,ReturnValue,OrderID,"

                    vals = clsMarsData.CreateDataID("eventconditions", "conditionid") & "," & _
                    eventID & "," & _
                    "'" & SQLPrepare(eventName) & "'," & _
                    "'" & conditionType & "'," & _
                    fireWhen & "," & _
                    0 & ","

                    Select Case conditionType.ToLower
                        Case "file exists"
                            cols &= "Filepath"

                            vals &= "FilePath"
                        Case "file has been modified"
                            cols &= "Filepath,KeyColumn"

                            vals &= "Filepath,ModDateTime"

                        Case "database record exists"
                            Dim connection As String
                            Dim dsn As String
                            Dim user As String
                            Dim password As String

                            connection = oRs("search1").Value

                            dsn = connection.Split("|")(0)
                            user = connection.Split("|")(1)
                            password = connection.Split("|")(2)

                            password = _EncryptDBValue(password)

                            connection = dsn & "|" & user & "|" & password

                            cols &= "ConnectionString,SearchCriteria,KeyColumn,DetectInserts"

                            vals &= "'" & SQLPrepare(connection) & "',search2,Filepath,0"

                        Case "window is present"

                            cols &= "SearchCriteria"

                            vals &= "search1"
                        Case "process exists"
                            cols &= "SearchCriteria"

                            vals &= "search1"
                        Case "unread email is present"
                            Dim popPassword As String = oRs("poppassword").Value

                            popPassword = _EncryptDBValue(popPassword)

                            cols &= "SearchCriteria,PopServer,PopUser,PopPassword,PopRemove,AnyAll"

                            vals &= "'Message_CONTAINS_' + search1 + '|Subject_CONTAINS_' + search2,Popserver,Popuserid," & _
                            "'" & SQLPrepare(popPassword) & "',PopRemove,'ANY'"

                        Case "database record has changed"
                            Dim connection As String
                            Dim dsn As String
                            Dim user As String
                            Dim password As String

                            connection = oRs("search1").Value

                            dsn = connection.Split("|")(0)
                            user = connection.Split("|")(1)
                            password = connection.Split("|")(2)

                            password = _EncryptDBValue(password)

                            connection = dsn & "|" & user & "|" & password

                            cols &= "ConnectionString,SearchCriteria,KeyColumn,DetectInserts"

                            vals &= "'" & SQLPrepare(connection) & "',Search2,Filepath,Popremove"
                    End Select

                    SQL = "INSERT INTO EventConditions (" & cols & ") SELECT " & vals & " FROM EventAttr WHERE EventID =" & eventID

                    If clsMarsData.WriteData(SQL, False) = True Then
                        clsMarsData.WriteData("DELETE FROM EventAttr WHERE EventID =" & eventID, False)
                        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID=" & eventID, False)
                        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID NOT IN (SELECT EventID FROM EventAttr)", False)
                    End If
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Public Function RunEventPackage(ByVal nPackID As Integer) As Boolean
        Dim flowType As String = "AFTER"
        Dim packageName As String = ""

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            clsMarsUI.MainUI.BusyProgress(10, "Loading package information...")

            SQL = "SELECT * FROM EventPackageAttr WHERE EventPackID =" & nPackID

            oRs = clsMarsData.GetData(SQL)

            clsMarsScheduler.m_progressID = "Event-Package:" & nPackID

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then

                    Dim scheduleID As Integer = clsMarsScheduler.GetScheduleID(, , , , nPackID)
                    Dim ok As Boolean = False

                    packageName = oRs("packagename").Value
                    flowType = oRs("executionflow").Value

                    gScheduleName = packageName

                    clsMarsUI.MainUI.BusyProgress(40, "Getting schedules...")

                    Dim oRs1 As ADODB.Recordset = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE PackID =" & nPackID & " AND Status =1 ORDER BY PackOrderID")
                    Dim oTasks As clsMarsTask = New clsMarsTask

                    If flowType = "BEFORE" Or flowType = "BOTH" Then

                        clsMarsUI.MainUI.BusyProgress(60, "Processing custom tasks...")

                        ok = oTasks.ProcessTasks(scheduleID, "NONE")

                        If ok = False Then
                            Return False
                        End If
                    End If

                    clsMarsUI.MainUI.BusyProgress(80, "Running schedules...")

                    If oRs1 IsNot Nothing Then
                        Do While oRs1.EOF = False
                            Dim eventID As Integer

                            eventID = oRs1("eventid").Value

                            ok = RunEvents6(eventID, , , Me.m_HistoryID)

                            oRs1.MoveNext()
                        Loop

                        oRs1.Close()
                    End If

                    If flowType = "AFTER" Or flowType = "BOTH" Then
                        clsMarsUI.MainUI.BusyProgress(90, "Processing custom tasks...")

                        ok = oTasks.ProcessTasks(scheduleID, "NONE")

                        If ok = False Then Return False
                    End If

                    Return True
                Else
                    Throw New Exception("No event-based schedules were found in the package")
                End If

                oRs.Close()
            Else
                Throw New Exception("The query did not return a valid recordset")
            End If
        Catch ex As Exception
            gErrorDesc = "Event Package Error: " & packageName & " - " & ex.Message
            gErrorNumber = Err.Number
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            gErrorSuggest = ""

            _ErrorHandle("Event Package Error: " & packageName & " - " & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
            Return False
        Finally
            clsMarsUI.MainUI.BusyProgress(100, "", True)
        End Try
    End Function

    Public Shared Function GetEventBasedScheduleName(ByVal nID As Integer) As String
        Dim SQL As String = "SELECT EventName FROM EventAttr6 WHERE EventID =" & nID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return ""

        If oRs.EOF = True Then Return ""

        Return oRs("EventName").Value
    End Function
    Public Shared Sub RecacheAllDatasets()
        Dim SQL As String = "SELECT * FROM EventConditions WHERE (cacheFolderName = '' OR cacheFolderName IS NULL) AND " & _
        "(ConditionType ='database record exists' AND AnyAll = 'NEW')"
        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Dim conditionID As Integer = oRs("conditionid").Value
                Dim conditionName As String = oRs("conditionName").Value

                Dim cacheFolderName As String = conditionName & "." & conditionID 'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())

                If RefreshData(conditionID, cacheFolderName) = True Then
                    SQL = "UPDATE EventConditions SET cacheFoldername ='" & SQLPrepare(cacheFolderName) & "' WHERE ConditionID =" & conditionID

                    clsMarsData.WriteData(SQL)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


        SQL = "SELECT * FROM EventConditions WHERE (cacheFolderName = '' OR cacheFolderName IS NULL) AND ConditionType ='database record has changed'"

        oRs = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                'IO.Path.GetFileNameWithoutExtension(IO.Path.GetRandomFileName())
                Dim conditionName As String = oRs("conditionName").Value
                Dim conditionID As Integer = oRs("conditionid").Value
                Dim cacheFolderName As String = conditionName & "." & conditionID

                If RefreshData(conditionID, cacheFolderName) = True Then
                    SQL = "UPDATE EventConditions SET cacheFoldername ='" & SQLPrepare(cacheFolderName) & "' WHERE ConditionID =" & conditionID

                    clsMarsData.WriteData(SQL)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

    Private Shared Function RefreshData(ByVal conditionID As Integer, ByVal cacheFolderName As String) As Boolean
        Try
            Dim SQL As String = "SELECT * FROM EventConditions WHERE ConditionID =" & conditionID

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim type As String = oRs("conditiontype").Value
                    Dim sName As String = oRs("conditionname").Value

                    Dim connString As String = oRs("connectionstring").Value
                    SQL = oRs("searchcriteria").Value

                    Dim AnyAll As String = IsNull(oRs("anyall").Value)
                    Dim keyColumn As String = oRs("keycolumn").Value

                    If type = "database record exists" Then
                        If AnyAll = "NEW" Then clsMarsEvent.CacheData(sName, SQL, connString, cacheFolderName)
                    Else
                        clsMarsEvent.CacheData(sName, SQL, connString, keyColumn, cacheFolderName)
                    End If
                End If
            End If
            Return True
        Catch
            Return False
        End Try
    End Function
End Class
