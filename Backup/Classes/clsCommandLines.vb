

Public Class clsCommandLines
    Dim oData As New clsMarsData
    Dim oReport As New clsMarsReport
    Public Sub _Auto()

        Dim ors As ADODB.Recordset
        Dim SQL As String
        Dim Success As Boolean
        Dim rptTitle As String
        Dim sDatPath As String
        Dim AutoName As String
        Dim SendTo As String = ""
        Dim faxTo As String = ""
        Dim smsTo As String = ""
        Dim UserAlert As String = "no"
        Dim sPars() As String
        Dim sIdents() As String
        Dim sValues() As String
        Dim I As Integer
        Dim sIn As String
        Dim x As Integer = 0
        Dim autoID As Integer = 0
        Dim owner As String = ""

        For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
            sIn &= Environment.GetCommandLineArgs(I) & " "
        Next

        'lets define the parameters we want
        sIn = sIn.Replace("-a", String.Empty).Trim

        sPars = sIn.Split(";")

        sIdents = sIn.Split("=")

        ReDim sValues(sIdents.GetUpperBound(0))


        'the parameters passed to the application
        For I = 0 To sPars.GetUpperBound(0)
            sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
            sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
            sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
        Next

        For I = 0 To sIdents.GetUpperBound(0)
            Select Case sIdents(I).ToLower
                Case "schedulename"
                    AutoName = sValues(I)
                Case "showmsg"
                    UserAlert = sValues(I)
                Case "owner"
                    owner = sValues(I)
            End Select
        Next

        SQL = "SELECT AutoID FROM AutomationAttr WHERE AutoName LIKE '" & SQLPrepare(AutoName) & "'"

        If owner.Length > 0 Then
            SQL &= " AND Owner = '" & SQLPrepare(owner) & "'"
        End If

        ors = clsMarsData.GetData(SQL)

        Try
            If ors.EOF = False Then
                autoID = ors("autoid").Value

                Dim oAuto As clsMarsAutoSchedule = New clsMarsAutoSchedule

                If UserAlert = "yes" Then RunEditor = True Else RunEditor = False

                oAuto.ExecuteAutomationSchedule(autoID)

            End If

            ors.Close()
        Catch ex As Exception
            If UserAlert = "yes" Then gVisible = True

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        Application.Exit()
    End Sub

    Public Sub _Event()

        Dim ors As ADODB.Recordset
        Dim SQL As String
        Dim Success As Boolean
        Dim rptTitle As String
        Dim sDatPath As String
        Dim eventName As String
        Dim SendTo As String = ""
        Dim faxTo As String = ""
        Dim smsTo As String = ""
        Dim UserAlert As String = "no"
        Dim sPars() As String
        Dim sIdents() As String
        Dim sValues() As String
        Dim I As Integer
        Dim sIn As String
        Dim x As Integer = 0
        Dim eventID As Integer = 0
        Dim owner As String = ""
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
            sIn &= Environment.GetCommandLineArgs(I) & " "
        Next

        'lets define the parameters we want
        sIn = sIn.Replace("-e", String.Empty).Trim

        sPars = sIn.Split(";")

        sIdents = sIn.Split("=")

        ReDim sValues(sIdents.GetUpperBound(0))

        'the parameters passed to the application
        For I = 0 To sPars.GetUpperBound(0)
            sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
            sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
            sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
        Next

        For I = 0 To sIdents.GetUpperBound(0)
            Select Case sIdents(I).ToLower
                Case "schedulename"
                    eventName = sValues(I)
                Case "showmsg"
                    UserAlert = sValues(I)
                Case "owner"
                    owner = sValues(I)
            End Select
        Next

        SQL = "SELECT EventID FROM EventAttr6 WHERE EventName LIKE '" & SQLPrepare(eventName) & "'"

        If owner.Length > 0 Then
            SQL &= " AND Owner = '" & SQLPrepare(owner) & "'"
        End If

        ors = clsMarsData.GetData(SQL)

        Try
            If ors.EOF = False Then
                eventID = ors("eventid").Value

                If UserAlert = "yes" Then RunEditor = True Else RunEditor = False

                Success = oEvent.RunEvents6(eventID)
            End If

            ors.Close()

            If UserAlert = "yes" Then
                If Success = True Then
                    MessageBox.Show("The schedule execution has completed", Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)
                Else
                    gVisible = True
                    RunEditor = True

                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                End If
            Else
                If Success = False Then
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                End If
            End If

        Catch ex As Exception
            If UserAlert = "yes" Then gVisible = True

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        Application.Exit()
    End Sub

    Public Sub _EventPackage()

        Dim ors As ADODB.Recordset
        Dim SQL As String
        Dim Success As Boolean
        Dim rptTitle As String
        Dim sDatPath As String
        Dim packageName As String
        Dim SendTo As String = ""
        Dim faxTo As String = ""
        Dim smsTo As String = ""
        Dim UserAlert As String = "no"
        Dim sPars() As String
        Dim sIdents() As String
        Dim sValues() As String
        Dim I As Integer
        Dim sIn As String
        Dim x As Integer = 0
        Dim eventID As Integer = 0
        Dim owner As String = ""
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
            sIn &= Environment.GetCommandLineArgs(I) & " "
        Next

        'lets define the parameters we want
        sIn = sIn.Replace("-ep", String.Empty).Trim

        sPars = sIn.Split(";")

        sIdents = sIn.Split("=")

        ReDim sValues(sIdents.GetUpperBound(0))

        'the parameters passed to the application
        For I = 0 To sPars.GetUpperBound(0)
            sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
            sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
            sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
        Next

        For I = 0 To sIdents.GetUpperBound(0)
            Select Case sIdents(I).ToLower
                Case "schedulename"
                    packageName = sValues(I)
                Case "showmsg"
                    UserAlert = sValues(I)
                Case "owner"
                    owner = sValues(I)
            End Select
        Next

        SQL = "SELECT EventPackID FROM EventPackageAttr WHERE PackageName LIKE '" & SQLPrepare(packageName) & "'"

        If owner.Length > 0 Then
            SQL &= " AND Owner = '" & SQLPrepare(owner) & "'"
        End If

        ors = clsMarsData.GetData(SQL)

        Try
            If ors.EOF = False Then
                eventID = ors("eventpackid").Value

                If UserAlert = "yes" Then RunEditor = True Else RunEditor = False

                Success = oEvent.RunEventPackage(eventID)
            End If

            ors.Close()

            If UserAlert = "yes" Then
                If Success = True Then
                    MessageBox.Show("The schedule execution has completed", Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)

                    clsMarsScheduler.globalItem.SetScheduleHistory(True, , , , , , eventID)
                Else
                    gVisible = True

                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)

                    clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , , , eventID)
                End If
            Else
                If Success = False Then
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)

                    clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , , , , eventID)
                Else
                    clsMarsScheduler.globalItem.SetScheduleHistory(True, , , , , , eventID)
                End If
            End If

        Catch ex As Exception
            If UserAlert = "yes" Then gVisible = True

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        Application.Exit()
    End Sub
    Public Sub _Single()

        Dim ors As ADODB.Recordset
        Dim SQL As String
        Dim ReportSuccess As Boolean
        Dim rptTitle As String
        Dim sDatPath As String
        Dim ReportName As String
        Dim SendTo As String = ""
        Dim faxTo As String = ""
        Dim smsTo As String = ""
        Dim UserAlert As String = "no"
        Dim sPars() As String
        Dim sIdents() As String
        Dim sValues() As String
        Dim I As Integer
        Dim sIn As String
        Dim x As Integer = 0
        Dim reportID As Integer = 0
        Dim owner As String = ""

        For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
            sIn &= Environment.GetCommandLineArgs(I) & " "
        Next

        'lets define the parameters we want
        sIn = sIn.Replace("-s", String.Empty).Trim

        sPars = sIn.Split(";")

        sIdents = sIn.Split("=")

        ReDim sValues(sIdents.GetUpperBound(0))


        'the parameters passed to the application
        For I = 0 To sPars.GetUpperBound(0)
            sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
            sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
            sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
        Next

        For I = 0 To sIdents.GetUpperBound(0)
            Select Case sIdents(I).ToLower
                Case "schedulename"
                    ReportName = sValues(I)
                Case "sendto"
                    SendTo = sValues(I)
                Case "faxto"
                    faxTo = sValues(I)
                Case "smsto"
                    smsTo = sValues(I)
                Case "showmsg"
                    UserAlert = sValues(I)
                Case "parameter"
                    ReDim Preserve gParNames(x)
                    ReDim Preserve gParValues(x)

                    gParNames(x) = sValues(I).Split(":")(0)
                    gParValues(x) = sValues(I).Split(":")(1)

                    x += 1
                Case "owner"
                    owner = sValues(I)
            End Select
        Next

        If SendTo IsNot Nothing Then
            If SendTo.Length > 0 Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.cl1_AdvancedCommandLine) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                    Return
                End If
            End If
        End If

        If faxTo IsNot Nothing Then
            If faxTo.Length > 0 Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.cl1_AdvancedCommandLine) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPRO)
                    Return
                End If
            End If
        End If

        If smsTo IsNot Nothing Then
            If smsTo.Length > 0 Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.cl1_AdvancedCommandLine) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISEPRO)
                    Return
                End If
            End If
        End If

        SQL = "SELECT ReportID, Dynamic,Bursting,IsDataDriven FROM ReportAttr WHERE ReportTitle LIKE '" & SQLPrepare(ReportName) & "'"

        If owner <> "" Then
            SQL &= " AND Owner = '" & SQLPrepare(owner) & "'"
        End If

        ors = clsMarsData.GetData(SQL)

        Try
            If ors.EOF = False Then

                reportID = ors("reportid").Value

                If IsNull(ors("dynamic").Value, 0) = 1 Then
                    ReportSuccess = oReport.RunDynamicSchedule(ors(0).Value, False)
                ElseIf IsNull(ors("isdatadriven").Value, 0) = 1 Then
                    ReportSuccess = oReport.RunDataDrivenSchedule(ors(0).Value)
                Else
                    ReportSuccess = oReport.RunSingleSchedule(ors("reportid").Value, False, , , SendTo, faxTo, smsTo)
                End If
            End If

            ors.Close()
        Catch ex As Exception
            If UserAlert = "yes" Then gVisible = True

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try


        If UserAlert = "yes" Then
            If ReportSuccess = True Then
                MessageBox.Show("Report execution succeeded", Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)

                If reportID > 0 Then clsMarsScheduler.globalItem.SetScheduleHistory(True, "", reportID)
            Else
                gVisible = True
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)

                If reportID > 0 Then clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", reportID)
            End If
        Else
            If ReportSuccess = False Then
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)

                If reportID > 0 Then clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", reportID)
            Else
                If reportID > 0 Then clsMarsScheduler.globalItem.SetScheduleHistory(True, "", reportID)
            End If
        End If

        Application.Exit()
    End Sub

    Public Sub _Package()

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim BatchSuccess As Boolean
        Dim PackName As String
        Dim SendTo As String = ""
        Dim UserAlert As String = "no"
        Dim sPars() As String
        Dim sIdents() As String
        Dim sValues() As String
        Dim I As Integer
        Dim sIn As String
        Dim packID As Integer = 0
        Dim owner As String = ""

        For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
            sIn &= Environment.GetCommandLineArgs(I) & " "
        Next

        'lets define the parameters we want
        sIn = sIn.Replace("-p", String.Empty).Trim

        sPars = sIn.Split(";")

        sIdents = sIn.Split("=")

        ReDim sValues(sIdents.GetUpperBound(0))


        'the parameters passed to the application
        For I = 0 To sPars.GetUpperBound(0)
            sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
            sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
            sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
        Next

        For I = 0 To sIdents.GetUpperBound(0)
            Select Case sIdents(I).ToLower
                Case "schedulename"
                    PackName = sValues(I)
                Case "sendto"
                    SendTo = sValues(I)
                Case "showmsg"
                    UserAlert = sValues(I)
                Case "owner"
                    owner = sValues(I)
            End Select
        Next

        If Not SendTo Is Nothing Then
            If SendTo.Length > 0 Then
                If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.cl1_AdvancedCommandLine) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                    Return
                End If
            End If
        End If

        SQL = "SELECT * FROM PackageAttr WHERE PackageName LIKE '" & SQLPrepare(PackName) & "'"

        If owner <> "" Then
            SQL &= " AND Owner = '" & SQLPrepare(owner) & "'"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                packID = oRs("packid").Value
                If IsNull(oRs("dynamic").Value) = "1" Then
                    BatchSuccess = oReport.RunDynamicPackageSchedule(oRs("packid").Value, False)
                Else
                    BatchSuccess = oReport.RunPackageSchedule(oRs("packid").Value, , SendTo)
                End If

            End If

            oRs.Close()
        Catch ex As Exception
            If UserAlert = "yes" Then
                gVisible = True
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Else
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End If
        End Try

        If UserAlert = "yes" Then
            gVisible = True
            If BatchSuccess = True Then
                MessageBox.Show("Package executed successfully", Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)
                clsMarsScheduler.globalItem.SetScheduleHistory(True, , , packID, , , , oReport.m_HistoryID)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , packID, , , , oReport.m_HistoryID)
            End If
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
            clsMarsScheduler.globalItem.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , packID, , , , oReport.m_HistoryID)

        End If
    End Sub

    Public Sub _CreateSingleSchedule()

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.cl1_AdvancedCommandLine) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        Try
            Dim nReportID As Int32
            Dim sPrinters As String = ""
            Dim lsv As ListViewItem
            Dim WriteSuccess As Boolean
            Dim nInclude As Integer
            Dim sCols As String
            Dim sVals As String
            Dim oReport As New clsMarsReport
            Dim oRpt As Object


            'if everything is in order then lets save the report

            Dim SQL As String
            Dim oData As clsMarsData = New clsMarsData

            nReportID = clsMarsData.CreateDataID("ReportAttr", "ReportID")


            sCols = "ReportID,DatabasePath,ReportName," & _
                "Parent,ReportTitle," & _
                "Retry,AssumeFail,CheckBlank," & _
                "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
                "UseSavedData,CachePath,LastRefreshed,AutoRefresh,Bursting,ExcelBurst,BurstGroup"

            Dim ReportPath, ScheduleName, FolderName, Formula, CRDUser, CRDPassword, UserID, Password, _
            Frequency, RunAt, Description, Keyword As String
            Dim StartDate, EndDate As Date
            Dim Repeat As Integer = 0
            Dim RepeatInterval As Integer
            Dim RepeatUntil As String
            Dim Status As Integer = 1
            Dim FolderID As Integer
            Dim UseLogin As Integer = 0
            Dim UseSavedData As Integer = 0
            Dim Destination As String
            Dim DestinationName As String
            Dim SendTo, CC, Bcc, Subject, Message, Extras, MailFormat, SMTPServer, _
            OutputPath, Printername, Copies, FTPServer, FTPUserName, FTPPassword, FTPPath, _
            Format As String
            Dim Zip, Embed As Integer
            Dim sPars() As String
            Dim sIdents() As String
            Dim sValues() As String
            Dim I As Integer
            Dim sIn As String
            Dim sParName() As String
            Dim sParValue()
            Dim x As Integer = 0

            ReDim sParName(x)
            ReDim sParValue(x)

            For I = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
                sIn &= Environment.GetCommandLineArgs(I) & " "
            Next

            'lets define the parameters we want
            sIn = sIn.Replace("-x", String.Empty).Trim

            sPars = sIn.Split(";")

            sIdents = sIn.Split("=")

            ReDim sValues(sIdents.GetUpperBound(0))

            For I = 0 To sPars.GetUpperBound(0)
                sPars(I) = GetDelimitedWord(sIn, I + 1, ";")
                sIdents(I) = GetDelimitedWord(sPars(I), 1, "=")
                sValues(I) = GetDelimitedWord(sPars(I), 2, "=")
            Next

            For I = 0 To sIdents.GetUpperBound(0)
                Select Case sIdents(I).ToLower
                    Case "schedulename"
                        ScheduleName = sValues(I)
                    Case "reportpath"
                        ReportPath = sValues(I)
                    Case "foldername"
                        FolderName = sValues(I)
                    Case "sqlrduser"
                        CRDUser = sValues(I)
                    Case "sqlrdpassword"
                        CRDPassword = sValues(I)
                    Case "userid"
                        UserID = sValues(I)
                    Case "password"
                        Password = sValues(I)
                    Case "frequency"
                        Frequency = sValues(I)
                    Case "runat"
                        RunAt = sValues(I)
                    Case "description"
                        Description = sValues(I)
                    Case "keyword"
                        Keyword = sValues(I)
                    Case "startdate"
                        StartDate = sValues(I)
                    Case "enddate"
                        EndDate = sValues(I)
                    Case "repeat"
                        Repeat = sValues(I)
                    Case "repeatinterval"
                        RepeatInterval = sValues(I)
                    Case "repeatuntil"
                        RepeatUntil = sValues(I)
                    Case "status"
                        Status = sValues(I)
                    Case "uselogin"
                        UseLogin = sValues(I)
                    Case "usesaveddata"
                        UseSavedData = sValues(I)
                    Case "destination"
                        Destination = sValues(I)
                    Case "destinationname"
                        DestinationName = sValues(I)
                    Case "sento"
                        SendTo = sValues(I)
                    Case "cc"
                        CC = sValues(I)
                    Case "bcc"
                        Bcc = sValues(I)
                    Case "subject"
                        Subject = sValues(I)
                    Case "message"
                        Message = sValues(I)
                    Case "extras"
                        Extras = sValues(I)
                    Case "mailformat"
                        MailFormat = sValues(I)
                    Case "smtpserver"
                        SMTPServer = sValues(I)
                    Case "outputpath"
                        OutputPath = sValues(I)
                    Case "ftpserver"
                        FTPServer = sValues(I)
                    Case "ftpusername"
                        FTPUserName = sValues(I)
                    Case "ftppassword"
                        FTPPassword = sValues(I)
                    Case "ftppath"
                        FTPPath = sValues(I)
                    Case "format"
                        Format = sValues(I)
                    Case "zip"
                        Zip = sValues(I)
                    Case "embed"
                        Embed = sValues(I)
                    Case "parameter"
                        ReDim Preserve sParName(x)
                        ReDim Preserve sParValue(x)

                        sParName(x) = sValues(I).Split(":")(0)
                        sParValue(x) = sValues(I).Split(":")(1)

                        x += 1
                End Select
            Next

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT FolderID FROM Folders WHERE FolderName LIKE '" & SQLPrepare(FolderName) & "'")

            If oRs.EOF = False Then
                FolderID = oRs.Fields(0).Value
            End If

            oRs.Close()

            oRpt = oApp.OpenReport(ReportPath)
            greportItem = oRpt

            sVals = nReportID & "," & _
                "'" & SQLPrepare(ReportPath) & "'," & _
                "'" & SQLPrepare(ScheduleName) & "'," & _
                FolderID & "," & _
                "'" & SQLPrepare(ScheduleName) & "'," & _
                0 & "," & _
                0 & "," & _
                0 & "," & _
                "'" & SQLPrepare(oRpt.RecordSelectionFormula) & "'," & _
                "'" & gUser & "'," & _
                "'" & SQLPrepare(UserID) & "'," & _
                "'" & SQLPrepare(_EncryptDBValue(Password)) & "'," & _
                "''," & _
                "''," & _
                UseLogin & "," & _
                UseSavedData & "," & _
                "'" & SQLPrepare(oReport.CacheReport(ReportPath)) & "', '" & ConDateTime(Date.Now) & "'," & _
                0 & "," & _
                "0," & _
                0 & "," & _
                "''"

            SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

            WriteSuccess = clsMarsData.WriteData(SQL)

            If WriteSuccess = True Then
                Dim ScheduleID As Integer = clsMarsData.CreateDataID("ScheduleAttr", "ScheduleID")

                'save the schedule
                SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                    "StartTime,Repeat,RepeatInterval," & _
                    "RepeatUntil,Status,ReportID,Description,KeyWord,CalendarName) " & _
                    "VALUES(" & _
                    ScheduleID & "," & _
                    "'" & Frequency & "'," & _
                    "'" & ConDateTime(StartDate) & "'," & _
                    "'" & ConDateTime(EndDate) & "'," & _
                    "'" & ConDate(StartDate) & " " & ConTime(RunAt) & "'," & _
                    "'" & RunAt & "'," & _
                    Repeat & "," & _
                    "'" & RepeatInterval & "'," & _
                    "'" & RepeatUntil & "'," & _
                    Status & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(Description) & "'," & _
                    "'" & SQLPrepare(Keyword) & "'," & _
                    "'')"

                WriteSuccess = clsMarsData.WriteData(SQL)

                clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
                "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")

                If WriteSuccess = False Then
                    clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                    Exit Sub
                End If

                'save the destination data for the report

                SQL = "UPDATE DestinationAttr SET ReportID =" & nReportID & "," & _
                "PackID = 0 WHERE ReportID = 99999"

                WriteSuccess = clsMarsData.WriteData(SQL)

                If WriteSuccess = False Then
                    clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                    clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nReportID & " AND PackID = 0")
                    Exit Sub
                End If


                'save the parameters
                sCols = "ParID,ReportID,ParName,ParValue,ParType"

                clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID=" & nReportID)

                Dim sParType As Integer

                For I = 0 To sParValue.GetUpperBound(0)

                    If sParValue(I).indexof("|") > -1 Then
                        sParType = 111
                    Else
                        sParType = 0
                    End If

                    sVals = clsMarsData.CreateDataID("ReportParameter", "ParID") & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(sParName(I)) & "'," & _
                    "'" & SQLPrepare(sParValue(I)) & "'," & _
                    sParType

                    SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                Next

                Dim DestinationID As Integer = clsMarsData.CreateDataID("DestinationAttr", "DestinationID")

                'destinations
                Select Case Destination.ToLower
                    Case "email"
                        sCols &= "SendTo,CC,Bcc,Subject,Message,Extras,MailFormat,SMTPServer,"

                        sVals = DestinationID & "," & _
                        "'Email'," & _
                        "'" & SQLPrepare(DestinationName) & "'," & _
                        "'" & SQLPrepare(SendTo) & "'," & _
                        "'" & SQLPrepare(CC) & "'," & _
                        "'" & SQLPrepare(Bcc) & "'," & _
                        "'" & SQLPrepare(Subject) & "'," & _
                        "'" & SQLPrepare(Message) & "'," & _
                        "'" & SQLPrepare(Extras) & "'," & _
                        "'" & MailFormat & "'," & _
                        "'" & SQLPrepare(SMTPServer) & "'"

                    Case "disk"
                        sCols &= "OutputPath,"

                        sVals = DestinationID & "," & _
                        "'Disk'," & _
                        "'" & SQLPrepare(DestinationName) & "'," & _
                        "'" & SQLPrepare(OutputPath) & "'"
                    Case "printer"
                        sCols &= "Printername,Copies,"
                    Case "ftp"
                        sCols &= "FTPServer,FTPUserName,FTPPassword,FTPPath,"

                        sVals = DestinationID & "," & _
                        "'FTP'," & _
                        "'" & SQLPrepare(DestinationName) & "'," & _
                        "'" & SQLPrepare(FTPServer) & "'," & _
                        "'" & SQLPrepare(FTPUserName) & "'," & _
                        "'" & SQLPrepare(FTPPassword) & "'," & _
                        "'" & SQLPrepare(FTPPath) & "'"
                    Case "fax"

                        sCols &= "SendTo,Subject,Cc,Bcc,Message,"

                        sVals = DestinationID & "," & _
                        "'Fax'," & _
                        "'" & SQLPrepare(DestinationName) & "'," & _
                        "'" & SQLPrepare(SendTo) & "'," & _
                        "'" & SQLPrepare(Subject) & "'," & _
                        "'" & SQLPrepare(CC) & "'," & _
                        "'" & SQLPrepare(Bcc) & "'," & _
                        "'" & SQLPrepare(Message) & "'"

                End Select

                sCols &= "ReportID,PackID,OutputFormat,Customext,AppendDateTime," & _
                "DateTimeFormat,CustomName,IncludeAttach,Compress,Embed,HTMLSplit," & _
                "DeferDelivery,DeferBy"

                sVals &= "," & nReportID & "," & _
                            0 & "," & _
                            "'" & Format & "'," & _
                            "''," & _
                            0 & "," & _
                            "''," & _
                            "''," & _
                            0 & "," & _
                            Zip & "," & _
                            Embed & "," & _
                            0 & "," & _
                            0 & "," & _
                            "''"

                SQL = "INSERT INTO DestinationAttr (" & sCols & ") " & _
                "VALUES(" & sVals & ")"

                clsMarsData.WriteData(SQL)

            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

End Class
