Imports System.Data
Imports System.Data.Odbc
Imports System.Data.SqlClient

Public Class DataMigrator

    Public Function Migrate(ByVal srcDSN As String, ByVal dstDSN As String, Optional ByVal excludeTables As ArrayList = Nothing) As Boolean
        Dim rc As Boolean = False
        Try
            Dim ds As DataSet = New DataSet

            ExportToDataSet(srcDSN, ds, excludeTables)
            ImportFromDataSet(dstDSN, ds)

            rc = True
        Catch
        End Try

        Return rc
    End Function

    Public Function Export(ByVal srcDSN As String, ByVal fileName As String, Optional ByVal excludeTables As ArrayList = Nothing) As Boolean
        Dim rc As Boolean = False
        Try
            Dim ds As DataSet = New DataSet

            ExportToDataSet(srcDSN, ds, excludeTables)

            '//Write the dataset (including the table schema) as XML to the supplied file
            ds.WriteXml(fileName, XmlWriteMode.WriteSchema)

            rc = True
        Catch
        End Try

        Return rc
    End Function

    Public Function Import(ByVal dstDSN As String, ByVal fileName As String) As Boolean
        Dim rc As Boolean = False
        Try
            Dim ds As DataSet = New DataSet

            '//Construct the dataset using the schema and data info stored in the supplied XML file
            ds.ReadXml(fileName, XmlReadMode.ReadSchema)

            ImportFromDataSet(dstDSN, ds)

            rc = True
        Catch
        End Try

        Return rc
    End Function

    Private Sub ExportToDataSet(ByVal srcDSN As String, ByRef ds As DataSet, Optional ByVal excludeTables As ArrayList = Nothing)

        '//Create the source ODBC connection object
        Dim odbcCon As OdbcConnection = New OdbcConnection(srcDSN)

        Try
            '//Open a connection to the ODBC source
            odbcCon.Open()

            '//Retrieve the Table schema info
            Dim tbls As DataTable = odbcCon.GetSchema("Tables")

            '//For each user table, retrieve all the data and insert into our dataset
            For Each dr As DataRow In tbls.Rows

                Dim tableName As String = dr("TABLE_NAME")
                Dim tableType As String = dr("TABLE_TYPE")

                'Debug.Print(vbNewLine + "************ EXPORTING: " + tableName + "************")

                '//Only get the data for the user tables (non-system)
                If (tableType = "TABLE") Then

                    If (Not excludeTables Is Nothing) AndAlso (excludeTables.Contains(tableName) = True) Then Continue For

                    Dim ad As OdbcDataAdapter = New OdbcDataAdapter("SELECT * FROM [" + tableName + "]", odbcCon)

                    Try
                        '//Add any schema info to the dataset first. This allows for proper key creation later
                        ad.FillSchema(ds, SchemaType.Mapped, tableName)

                        '//Fill the dataset with the rows from the table
                        ad.Fill(ds, tableName)

                    Catch ex As OdbcException
                        Debug.Print(ex.Message)
                    Catch ex As Exception
                        Debug.Print(ex.Message)
                    End Try

                End If

            Next

            '//Close the connection
            odbcCon.Close()

        Catch ex As OdbcException
            Debug.Print(ex.Message)

        Catch ex As Exception
            Debug.Print(ex.Message)

        Finally
            '//Manually dispose of the odbc connection resource
            odbcCon.Dispose()
        End Try

    End Sub

    Private Sub ImportFromDataSet(ByVal dstDSN As String, ByRef ds As DataSet)

        '//Create the source SQL Server connection object
        Dim sqlCon As SqlConnection = New SqlConnection(dstDSN)

        Try

            '//Open a connection to the source
            sqlCon.Open()

            '//Loop through all the tables in the dataset
            For Each tb As DataTable In ds.Tables

                'Debug.Print(vbNewLine + "************ IMPORTING: " + tb.TableName + "************")

                '//Delete the exsisting table from the destination db
                DropTable(tb.TableName, sqlCon)

                '//Create the table
                If (CreateTable(tb, sqlCon)) Then
                    ImportDataToTable(tb, sqlCon)
                End If

            Next

            '//Close the connection
            sqlCon.Close()

        Catch ex As SqlException
            Debug.Print(ex.Message)
        Catch ex As Exception
            Debug.Print(ex.Message)
        Finally
            '//Manually dispose of the odbc connection resource
            sqlCon.Dispose()
        End Try

    End Sub

    Private Sub DropTable(ByVal tableName As String, ByVal sqlCon As SqlConnection)
        Try
            Dim delCmd As SqlCommand = New SqlCommand("DROP TABLE [" + tableName + "]", sqlCon)
            delCmd.ExecuteNonQuery()
        Catch
            '//Ignore drop table errors since the table may not exist
        End Try
    End Sub

    Private Function CreateTable(ByVal tb As DataTable, ByVal sqlCon As SqlConnection) As Boolean
        Dim retVal As Boolean = True

        Try
            '//Build the CREATE TABLE sql string
            Dim createSql As String = BuildCreateTableSQL(tb.TableName, tb)
            'Debug.Print(str)

            Dim createCmd As SqlCommand = New SqlCommand(createSql, sqlCon)
            createCmd.ExecuteNonQuery()

        Catch ex As Exception
            Debug.Print(ex.Message)
            retVal = False
        End Try

        Return retVal
    End Function

    Private Function ImportDataToTable(ByVal tb As DataTable, ByVal sqlCon As SqlConnection) As Boolean
        Dim retVal As Boolean = True

        Try
            Dim bcp As SqlBulkCopy = New SqlBulkCopy(sqlCon)

            '//NOTE: Brackets are required for table names with spaces!!!
            '//      Unexpected errors can occur otherwise.
            bcp.DestinationTableName = "[" + tb.TableName + "]"

            bcp.WriteToServer(tb)
            bcp.Close()

        Catch ex As Exception
            Debug.Print(ex.Message)
            retVal = False
        End Try

        Return retVal
    End Function

    Private Function BuildCreateTableSQL(ByVal tableName As String, ByVal table As DataTable) As String

        Dim sql As String = "CREATE TABLE [" + tableName + "] ("

        '//Columns
        For Each column As DataColumn In table.Columns
            sql += vbNewLine
            sql += "[" + column.ColumnName + "] " + SQLGetType(column) + ","
        Next

        sql = sql.TrimEnd(","c)

        '//Primary keys
        If (table.PrimaryKey.Length > 0) Then

            sql += vbNewLine
            sql += "CONSTRAINT [PK_" + tableName + "] PRIMARY KEY CLUSTERED ("

            For Each column As DataColumn In table.PrimaryKey
                sql += "[" + column.ColumnName + "],"
            Next

            sql = sql.TrimEnd(","c)
            sql += ")"

        End If

        sql += ")"

        Return sql

    End Function

    '// Overload based on DataColumn from DataTable type
    Private Function SQLGetType(ByVal column As DataColumn) As String
        Dim str As String = ""

        Try
            'str = SQLGetType(column.DataType, column.MaxLength, 10, 2)
            str = SQLGetType(column.DataType, column.MaxLength)
        Catch ex As Exception
            Dim msg As String = "An error occurred while retrieving type infomation for column: " + column.ColumnName
            Debug.Print(msg)
            Debug.Print(ex.Message)
        End Try

        Return str
    End Function

    '// Return T-SQL data type definition, based on schema definition for a column
    Private Function SQLGetType(ByVal type As Object, _
                                ByVal columnSize As Integer, _
                                Optional ByVal numericPrecision As Integer = 0, _
                                Optional ByVal numericScale As Integer = 0) As String

        Select Case (type.ToString())

            Case "System.Boolean"
                Return "bit"

            Case "System.Byte"
                Return "tinyint"

            Case "System.Int16"
                Return "smallint"

            Case "System.Int32"
                Return "int"

            Case "System.Int64"
                Return "bigint"

            Case "System.Byte[]"
                If (columnSize = -1 Or columnSize > 8000) Then
                    Return "varbinary(max)"
                Else
                    Return "varbinary(" & columnSize & ")"
                End If

            Case "System.Char[]"
                Return "char(" + columnSize + ")"

            Case "System.String"
                If (columnSize = -1) Then
                    Return "varchar(255)"
                ElseIf (columnSize >= &H3FFFFFFF) Then
                    Return "text"
                ElseIf (columnSize >= 536870910) Then
                    Return "text"
                Else
                    Return "varchar(" & columnSize & ")"
                End If

            Case "System.Single"
                Return "real"

            Case "System.Double"
                Return "float"

            Case "System.Decimal"
                If (numericPrecision > 0) Then
                    If (numericScale <= numericPrecision And numericScale >= 0) Then
                        Return "decimal(" & numericPrecision & "," & numericScale & ")"
                    Else
                        Return "decimal (" & numericPrecision & ")"
                    End If
                Else
                    Return "decimal"
                End If

            Case "System.DateTime"
                Return "datetime"

            Case "System.Guid"
                Return "uniqueidentifier"

            Case "System.Object"
                Return "sql_variant"

            Case Else
                Throw New Exception(type.ToString() + " not implemented.")

        End Select

    End Function

End Class
