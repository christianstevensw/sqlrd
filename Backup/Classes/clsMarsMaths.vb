Public Class clsMarsMaths
    Public Shared Function Minimum(ByVal DecimalArray As ArrayList) As Decimal
        Dim minValue As Double = Double.MaxValue

        For Each n As Double In DecimalArray
            If n < minValue Then
                minValue = n
            End If
        Next

        Return minValue
    End Function

    Public Shared Function Maximum(ByVal DecimalArray As ArrayList) As Decimal
        Dim maxValue As Double = 0

        For Each n As Double In DecimalArray
            If n > maxValue Then
                maxValue = n
            End If
        Next

        Return maxValue
    End Function
    Public Shared Function Average(ByVal DecimalArray As ArrayList) As Decimal
        Try
            Dim total As Double = 0

            For Each n As Integer In DecimalArray
                total += n
            Next

            Dim averageValue As Double = (total / DecimalArray.Count)

            Return averageValue
        Catch
            Return 1
        End Try
    End Function
    Public Shared Function Median(ByVal DecimalArray As ArrayList) As Decimal
        'Definition: "Middle value" of a list. The smallest number such that
        'at least half the numbers in the list are no greater than it. If the
        'list has an odd number of entries, the median is the middle entry in
        'the list after sorting the list into increasing order. If the list
        'has an even number of entries, the median is equal to the sum of the
        'two middle (after sorting) numbers divided by two.

        Dim Count As Integer

        Count = DecimalArray.Count

        If Count = 0 Then Return 0

        DecimalArray.Sort()
        'We need to sort the numbers to get the median.
        'DecimalArray.Sort(DecimalArray)

        'If divisible by two, add the two middle numbers together and return 
        'the average (mean!) of those.
        If Count Mod 2 = 0 Then
            Return (DecimalArray((Count / 2) - 1) + DecimalArray((Count / 2))) / 2
        Else
            Return DecimalArray(Count \ 2)
        End If
    End Function


End Class
