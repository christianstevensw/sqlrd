Public Class clsOperationalHours
    Public Shared Function withinOperationHours(ByVal checkDate As Date, ByVal operationName As String) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim OperationID As Integer = 0
        Dim dtDays As DataTable
        Dim result As Boolean = False

        Try
            SQL = "SELECT * FROM OperationDays WHERE OperationID = " & _
            "(SELECT OperationID FROM OperationAttr WHERE OperationName LIKE '" & SQLPrepare(operationName) & "')"

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                dtDays = New DataTable

                With dtDays.Columns
                    .Add("DayName")
                    .Add("DayIndex")
                    .Add("OpenAt")
                    .Add("CloseAt")
                End With

                If oRs.EOF = True Then Return True

                Do While oRs.EOF = False
                    Dim row As DataRow = dtDays.Rows.Add

                    row("dayname") = oRs("dayname").Value
                    row("dayindex") = oRs("dayindex").Value
                    row("openat") = oRs("openat").Value
                    row("closeat") = oRs("closeat").Value

                    oRs.MoveNext()
                Loop

                oRs.Close()
            Else
                Return True
            End If

            'check if today is part of operation hours
            Dim dayofWeek As String = checkDate.DayOfWeek.ToString

            Dim rows() As DataRow = dtDays.Select("DayName ='" & dayofWeek & "'")

            If rows.Length = 0 Then
                Return False
            End If

            'now that we have narrowed our operational hours to a day, lets see if the current time falls with range
            Dim dayRow As DataRow = rows(0)
            Dim currentTime As Date = ConDate(Now) & " " & ConTime(checkDate)
            Dim openTime As Date = ConDate(Now) & " " & ConTime(dayRow("openat"))
            Dim closeTime As Date = ConDate(Now) & " " & ConTime(dayRow("closeat"))

            If currentTime >= openTime And currentTime <= closeTime Then
                Return True
            Else
                Return False
            End If
        Catch
            Return True
        Finally
            dtDays = Nothing
            oRs = Nothing
        End Try
    End Function
End Class
