Public Class clsFolders
    Dim oUI As New clsMarsUI
    Public Sub ExecuteFolder(ByVal nFolderID As Integer)
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oReport As New clsMarsReport
        Dim Ok As Boolean = False
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler

        Dim sWhere As String = " WHERE Parent =" & nFolderID

        oRs = clsmarsdata.GetData("SELECT FolderID FROM Folders WHERE Parent =" & nFolderID)

        If oRs.EOF = False Then
            Do While oRs.EOF = False
                ExecuteFolder(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        oUI.BusyProgress(20, "Executing single schedule...")

        oRs = clsmarsdata.GetData("SELECT ReportID FROM ReportAttr WHERE Dynamic = 0 AND Parent =" & nFolderID)

        Do While oRs.EOF = False
            Ok = oReport.RunSingleSchedule(oRs.Fields(0).Value)

            Select Case Ok
                Case True
                    oSchedule.SetScheduleHistory(True, , oRs(0).Value)
                Case False
                    oSchedule.SetScheduleHistory(False, gErrorDesc, oRs(0).Value)
            End Select

            oRs.MoveNext()
        Loop

        oRs.Close()

        oUI.BusyProgress(40, "Executing dynamic schedules...")

        oRs = clsmarsdata.GetData("SELECT ReportID FROM ReportAttr WHERE Dynamic = 1 AND Parent =" & nFolderID)

        Do While oRs.EOF = False
            Ok = oReport.RunDynamicSchedule(oRs.Fields(0).Value)

            Select Case Ok
                Case True
                    oSchedule.SetScheduleHistory(True, , oRs(0).Value)
                Case False
                    oSchedule.SetScheduleHistory(False, gErrorDesc, oRs(0).Value)
            End Select

            oRs.MoveNext()
        Loop

        oRs.Close()

        oUI.BusyProgress(60, "Executing package schedules...")

        oRs = clsmarsdata.GetData("SELECT PackID, Dynamic FROM PackageAttr" & sWhere)

        Do While oRs.EOF = False
            If IsNull(oRs(1).Value) = "1" Then
                Ok = oReport.RunDynamicPackageSchedule(oRs(0).Value)
            Else
                Ok = oReport.RunPackageSchedule(oRs.Fields(0).Value)
            End If

            Select Case Ok
                Case True
                    oSchedule.SetScheduleHistory(True, , , oRs(0).Value, , , , oReport.m_HistoryID)
                Case False
                    oSchedule.SetScheduleHistory(False, gErrorDesc, , oRs(0).Value, , , , oReport.m_HistoryID)
            End Select

            oRs.MoveNext()
        Loop

        oRs.Close()

        oUI.BusyProgress(80, "Executing automation schedules...")

        oRs = clsmarsdata.GetData("SELECT AutoID FROM AutomationAttr" & sWhere)
        Dim oAuto As New clsMarsAutoSchedule

        Do While oRs.EOF = False
            oAuto.ExecuteAutomationSchedule(oRs.Fields(0).Value)

            oRs.MoveNext()
        Loop

        oRs.Close()

        oUI.BusyProgress(90, "Executing event-based schedules...")

        oRs = clsMarsData.GetData("SELECT EventID FROM EventAttr6" & sWhere)

        Dim oEvent As New clsMarsEvent

        Do While oRs.EOF = False
            oEvent.RunEvents6(oRs(0).Value)
            oRs.MoveNext()
        Loop

        oRs.Close()

        oUI.BusyProgress(95, "Executing event-based packages...")

        oRs = clsMarsData.GetData("SELECT EventPackID FROM EventPackageAttr" & sWhere)

        Do While oRs.EOF = False
            Ok = oEvent.RunEventPackage(oRs(0).Value)

            Select Case Ok
                Case True
                    oSchedule.SetScheduleHistory(True, , , , , , oRs(0).Value)
                Case False
                    oSchedule.SetScheduleHistory(False, gErrorDesc, , , , , oRs(0).Value)
            End Select

            oRs.MoveNext()
        Loop
        oUI.BusyProgress(100, , True)
    End Sub
    Public Sub EnableFolder(ByVal nFolderID As Integer)
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String

        oRs = clsmarsdata.GetData("SELECT FolderID FROM Folders WHERE Parent =" & nFolderID)

        If oRs.EOF = False Then
            Do While oRs.EOF = False
                EnableFolder(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        oUI.BusyProgress(20, "Processing single schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 1 WHERE ReportID IN " & _
        "(SELECT ReportID FROM ReportAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(40, "Processing package schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 1 WHERE PackID IN " & _
        "(SELECT PackID FROM PackageAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(60, "Processing automation schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 1 WHERE AutoID IN " & _
        "(SELECT AutoID FROM AutomationAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)
        oUI.BusyProgress(80, "Processing event-based schedules...")

        SQL = "UPDATE EventAttr6 SET Status = 1 WHERE Parent =" & nFolderID

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(90, "Processing event-based package schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 1 WHERE EventPackID IN " & _
        "(SELECT EventPackID FROM EventPackageAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(100, , True)

    End Sub
    Public Sub DisableFolder(ByVal nFolderID As Integer)
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String

        oRs = clsmarsdata.GetData("SELECT FolderID FROM Folders WHERE Parent =" & nFolderID)

        If oRs.EOF = False Then
            Do While oRs.EOF = False
                DisableFolder(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        oUI.BusyProgress(20, "Processing single schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 0, DisabledDate ='" & ConDateTime(Now) & "' WHERE ReportID IN " & _
        "(SELECT ReportID FROM ReportAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(40, "Processing package schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 0, DisabledDate ='" & ConDateTime(Now) & "' WHERE PackID IN " & _
        "(SELECT PackID FROM PackageAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(60, "Processing automation schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 0, DisabledDate ='" & ConDateTime(Now) & "' WHERE AutoID IN " & _
        "(SELECT AutoID FROM AutomationAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(80, "Processing event-based schedules...")

        SQL = "UPDATE EventAttr6 SET Status = 0, DisabledDate ='" & ConDateTime(Now) & "' WHERE Parent =" & nFolderID

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(90, "Processing event-based package schedules...")

        SQL = "UPDATE ScheduleAttr SET Status = 0, DisabledDate ='" & ConDateTime(Now) & "' WHERE EventPackID IN " & _
        "(SELECT EventPackID FROM EventPackageAttr WHERE Parent = " & nFolderID & ")"

        clsMarsData.WriteData(SQL)

        oUI.BusyProgress(100, , True)

    End Sub
    Public Sub RefreshFolder(ByVal nFolderID As Integer)

        'refresh reports
        Dim oReport As New clsMarsReport
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim oRs1 As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT FolderID FROM Folders WHERE Parent =" & nFolderID)

        If oRs.EOF = False Then
            Do While oRs.EOF = False
                RefreshFolder(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        oRs = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE Parent =" & nFolderID)

        oUI.BusyProgress(40, "Refreshing single schedules...")

        Try
            Do While oRs.EOF = False
                oReport.RefreshReport(oRs.Fields(0).Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GoTo Oooops
        End Try

        oRs = clsMarsData.GetData("SELECT PackID FROM PackageAttr WHERE Parent =" & nFolderID)

        oUI.BusyProgress(80, "Refreshing packages...")

        Try
            Do While oRs.EOF = False
                oRs1 = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE PackID =" & oRs.Fields(0).Value)

                Do While oRs1.EOF = False
                    oReport.RefreshReport(oRs1.Fields(0).Value)

                    oRs1.MoveNext()
                Loop

                oRs.MoveNext()
            Loop
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            GoTo Oooops
        End Try

        oUI.BusyProgress(, , True)

        Return
Oooops:
        Return
    End Sub
End Class
