Imports Microsoft.Win32
Imports DevComponents.DotNetBar
Imports sqlrd.clsSettingsManager
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary

Public Class clsMarsUI

    Public Shared Paused As Boolean
    Public Shared MainUI As New clsMarsUI
    Public Shared imgList As ImageList
    Public Shared imgListHistory As ImageList
    Public Shared renameErrorstring As String = "The specified schedule name is already in use by another schedule of this kind in this folder. Please choose another name and try again"

    Public Enum enScheduleType
        REPORT = 0
        PACKAGE = 1
        AUTOMATION = 2
        SMARTFOLDER = 3
        EVENTBASED = 4
        EVENTPACKAGE = 5
    End Enum

    Public Enum APPTHEMES
        None = 0
        Glass = 1
        XP = 2
        Corporate = 3
        Classic = 4
    End Enum
    Public Shared Function candoRename(ByVal sName As String, ByVal nParent As Integer, _
        ByVal scheduleType As clsMarsScheduler.enScheduleType, _
        Optional ByVal nID As Integer = 0, Optional ByVal IsDynamic As Boolean = False, Optional ByVal IsDataDriven As Boolean = False) As Boolean
        Dim tableName As String
        Dim nameCol As String
        Dim idCol As String

        Try
            Select Case scheduleType
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    tableName = "automationattr"
                    nameCol = "autoname"
                    idCol = "autoid"
                Case clsMarsScheduler.enScheduleType.EVENTBASED
                    tableName = "eventattr6"
                    nameCol = "eventname"
                    idCol = "eventid"
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    tableName = "eventpackageattr"
                    nameCol = "packagename"
                    idCol = "eventpackid"
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    tableName = "packageattr"
                    nameCol = "packagename"
                    idCol = "packid"
                Case clsMarsScheduler.enScheduleType.REPORT
                    tableName = "reportattr"
                    nameCol = "reporttitle"
                    idCol = "reportid"
                Case clsMarsScheduler.enScheduleType.SMARTFOLDER, clsMarsScheduler.enScheduleType.NONE
                    Return True
            End Select

            Dim SQL As String = "SELECT " & nameCol & " FROM " & tableName & " WHERE " & nameCol & " = '" & SQLPrepare(sName) & "' AND Parent =" & nParent

            If nID <> 0 Then
                SQL &= " AND " & idCol & " <> " & nID
            End If

            If IsDynamic = False And IsDataDriven = False And nID <> 0 And _
            (scheduleType = clsMarsScheduler.enScheduleType.REPORT Or scheduleType = clsMarsScheduler.enScheduleType.PACKAGE) Then
                Dim sType As String = IIf(scheduleType = clsMarsScheduler.enScheduleType.REPORT, "report", "package")

                IsDynamic = clsMarsData.IsScheduleDynamic(nID, sType)
                IsDataDriven = clsMarsData.IsScheduleDataDriven(nID, sType)

                If IsDynamic = True Then
                    SQL &= " AND Dynamic =1"
                ElseIf IsDataDriven = True Then
                    SQL &= " AND IsDataDriven = 1"
                End If

                If IsDynamic = False Then SQL &= " AND (Dynamic IS NULL OR Dynamic =0)"

                If IsDataDriven = False Then SQL &= " AND (IsDataDriven IS NULL OR IsDataDriven =0)"
            Else
                If IsDynamic = True Then
                    SQL &= " AND Dynamic =1"
                ElseIf IsDataDriven = True Then
                    SQL &= " AND IsDataDriven = 1"
                End If
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    candoRename = True
                Else
                    candoRename = False
                End If

                oRs.Close()
            Else
                Return True
            End If

            Return candoRename
        Catch ex As Exception
            Return True
        End Try
    End Function
    Public Function createScheduleShortcut(ByVal sName As String, ByVal type As enScheduleType, ByVal linkDestination As String) As Boolean
        Dim command As String

        Select Case type
            Case enScheduleType.REPORT
                command = "-s schedulename=" & sName
            Case enScheduleType.PACKAGE
                command = "-p schedulename=" & sName
            Case enScheduleType.AUTOMATION
                Dim SQL As String = "SELECT AutoID FROM AutomationAttr WHERE AutoName ='" & SQLPrepare(sName) & "'"
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
                Dim autoID As Integer

                If oRs Is Nothing Then Return False

                If oRs.EOF = True Then Return False

                autoID = oRs(0).Value

                oRs.Close()

                command = "!xa " & autoID
            Case enScheduleType.EVENTBASED
                Dim SQL As String = "SELECT EventID FROM EventAttr6 WHERE EventName ='" & SQLPrepare(sName) & "'"
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
                Dim eventID As Integer

                If oRs Is Nothing Then Return False

                If oRs.EOF = True Then Return False

                eventID = oRs(0).Value

                oRs.Close()

                command = "!xe " & eventID
            Case enScheduleType.EVENTPACKAGE
                Dim SQL As String = "SELECT EventPackID FROM EventPackageAttr WHERE PackageName ='" & SQLPrepare(sName) & "'"
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
                Dim packID As Integer

                If oRs Is Nothing Then Return False

                If oRs.EOF = True Then Return False

                packID = oRs(0).Value

                oRs.Close()

                command = "!xz " & packID
            Case Else
                Return False
        End Select

        Return clsServiceController.CreateShortCut(linkDestination, sAppPath & assemblyName, sName & ".lnk", "", command, True)

    End Function

    Public Sub InitImages(ByRef oTree As Control)
        imgList = New ImageList()

        Dim sz As Size = New Size(16, 16)

        imgList.ImageSize = sz
        imgList.ColorDepth = ColorDepth.Depth32Bit
        imgList.TransparentColor = Color.Transparent

        With imgList.Images
            .Add(My.Resources.home) '0
            .Add(My.Resources.folder_closed) '1
            .Add(My.Resources.folder) '2
            .Add(My.Resources.box_closed) '3
            .Add(My.Resources.box) '4
            .Add(My.Resources.folder_cubes) '5
            .Add(My.Resources.document_chart) '6
            .Add(My.Resources.document_gear) '7
            .Add(My.Resources.folder_closed1) '8
            .Add(My.Resources.box_closed_bw) '9
            .Add(My.Resources.box_bw) '10
            .Add(My.Resources.document_pulse) '11
            .Add(My.Resources.document_attachment) '12
            .Add(My.Resources.document_atoms) '13
            .Add(My.Resources.box_new) '14
            .Add(My.Resources.box_new_bw) '15
            .Add(My.Resources.cube_molecule) '16
            .Add(My.Resources.cube_molecule_bw) '17
            .Add(My.Resources.document_flash) '18
            .Add(My.Resources.document_flash_bw) '19
            .Add(My.Resources.data_driven_box) '20
            .Add(My.Resources.data_driven_box_bw) '21
        End With

        If oTree IsNot Nothing Then
            If TypeOf oTree Is TreeView Then
                Dim tv As TreeView = CType(oTree, TreeView)

                tv.ImageList = imgList
            ElseIf TypeOf oTree Is ListView Then
                Dim lsv As ListView = CType(oTree, ListView)

                lsv.LargeImageList = imgList
                lsv.SmallImageList = imgList
            ElseIf TypeOf oTree Is GlacialComponents.Controls.GlacialListView.GlacialListView Then
                Dim gl As GlacialComponents.Controls.GlacialListView.GlacialListView = CType(oTree, GlacialComponents.Controls.GlacialListView.GlacialListView)

                gl.ImageList = imgList
            End If
        End If

        'oTree.ImageList = imgList
    End Sub
    Public Shared Sub sortListView(ByVal lsv As ListView, ByVal sortString As String, _
    ByVal sortBy As Integer, Optional ByVal isDate As Boolean = False, Optional ByVal addGroups As Boolean = False)
        lsv.BeginUpdate()

        Dim dt As DataTable = New DataTable("schedules")
        Dim colCount As Integer = lsv.Columns.Count - 1

        For Each col As ColumnHeader In lsv.Columns
            If col.Index = sortBy Then
                If isDate = True Then
                    Dim newCol As DataColumn

                    newCol = New DataColumn(col.Text)
                    newCol.DataType = System.Type.GetType("System.DateTime")

                    dt.Columns.Add(newCol)
                Else
                    Dim newCol As DataColumn

                    newCol = New DataColumn(col.Text)

                    dt.Columns.Add(newCol)
                End If
            Else
                dt.Columns.Add(col.Text)
            End If

        Next

        dt.Columns.Add("Tag")
        dt.Columns.Add("Image")

        For Each ls As ListViewItem In lsv.Items
            Dim r As DataRow = dt.Rows.Add

            r(0) = ls.Text

            For I As Integer = 1 To (ls.SubItems.Count - 1)
                r(I) = ls.SubItems(I).Text
            Next

            Try
                r(colCount + 1) = ls.Tag
                r(colCount + 2) = ls.ImageIndex
            Catch : End Try
        Next

        lsv.Items.Clear()

        Dim itry As String = lsv.Columns(sortBy).Text

        Dim rs As DataRow() = dt.Select("", lsv.Columns(sortBy).Text & sortString)

        For Each r As DataRow In rs
            Dim ls As ListViewItem = lsv.Items.Add(r(0))

            For I As Integer = 1 To colCount
                Try
                    ls.SubItems.Add(r(I))
                Catch : End Try
            Next

            Try
                ls.Tag = r("Tag")
                ls.ImageIndex = r("Image")

                If addGroups = True Then
                    Dim tag As String = ls.Tag.ToString.Split(":")(0)

                    Select Case tag.ToLower
                        Case "report"
                            ls.Group = lsv.Groups("Single Schedules")
                        Case "package"
                            ls.Group = lsv.Groups("Package Schedules")
                        Case "event-package"
                            ls.Group = lsv.Groups("Event-Based Packages")
                        Case "automation"
                            ls.Group = lsv.Groups("Automation Schedules")
                        Case "event"
                            ls.Group = lsv.Groups("Event-Based Schedules")
                        Case "backup"
                            ls.Group = lsv.Groups("Other Processes")
                    End Select

                End If
            Catch : End Try
        Next

        For Each col As ColumnHeader In lsv.Columns
            If col.Index = sortBy Then
                If sortString.ToLower.Trim = "asc" Then
                    col.Text = col.Text.Replace(">", "").Replace("<", "").Trim
                    col.Text = "< " & col.Text
                Else
                    col.Text = col.Text.Replace(">", "").Replace("<", "").Trim
                    col.Text = "> " & col.Text
                End If
            Else
                col.Text = col.Text.Replace(">", "").Replace("<", "").Trim
            End If
        Next

        lsv.EndUpdate()

        dt.Dispose()
    End Sub
    Public Sub BuildTree(ByVal oTree As TreeView, _
           Optional ByVal AllDetail As Boolean = False, _
           Optional ByVal ShowSmart As Boolean = True)
10:     Try
            Dim SQL As String
            Dim pKey As String
            Dim tNode As TreeNode
            Dim oRs As ADODB.Recordset
            Dim oNewNode As TreeNode
            Dim sNotAdmin As String

            Try
20:             InitImages(oTree)
            Catch : End Try

30:         oTree.Nodes.Clear()

40:         oTree.HideSelection = False
50:         oTree.ShowLines = False
60:         oTree.ShowRootLines = False

70:         SQL = "SELECT FolderName, FolderID FROM Folders WHERE Parent = 0"

80:         If gConType = "DAT" Then
90:             SQL &= " ORDER BY FolderName"
100:        Else
110:            SQL &= " ORDER BY CAST(FolderName AS VARCHAR(55))"
            End If

120:        tNode = New TreeNode 'oTree.Nodes.Add("Desktop")

130:        tNode.Text = "Desktop"

            tNode.Tag = "Desktop:0"

140:        tNode.ImageIndex = 0

150:        oTree.Nodes.Add(tNode)

160:        oRs = clsMarsData.GetData(SQL)

170:        If Not oRs Is Nothing Then
180:            Do While oRs.EOF = False
190:                pKey = oRs("FolderID").Value

200:                oNewNode = tNode.Nodes.Add(oRs("FolderName").Value)

                    oNewNode.Tag = "Folder:" & pKey

210:                If oNewNode.Text.ToLower = ".recovered" Then
220:                    oNewNode.ForeColor = Color.Red
230:                    oNewNode.NodeFont = New Font("Tahoma", "8", FontStyle.Italic, GraphicsUnit.Point, 1, False)
240:                ElseIf oNewNode.Text.ToLower = ".archive" Then
250:                    oNewNode.ForeColor = Color.Gray
260:                    oNewNode.NodeFont = New Font("Tahoma", "8", FontStyle.Italic, GraphicsUnit.Point, 1, False)
                    End If

270:                oNewNode.ImageIndex = 1
280:                oNewNode.SelectedImageIndex = 2

290:                AddChildren(oNewNode, AllDetail)

300:                oRs.MoveNext()
310:            Loop
320:            oRs.Close()
            End If

            'If ShowSmart = True Then _AddSmartFolders(oTree, False)

330:        oTree.Nodes(0).Expand()

340:        oRs = Nothing

350:        Exit Sub
        Catch ex As Exception
            'MsgBox(Err.Description & " " & Erl())
        End Try
    End Sub


    Public Sub AddSmartFolders(ByVal oTree As TreeView, ByVal ClearAll As Boolean)

        Dim SQL As String
        Dim oRs As New ADODB.Recordset
        Dim sNotAdmin As String
        Dim oNode As TreeNode

        If ClearAll = True Then oTree.Nodes.Clear()

        SQL = "SELECT * FROM SmartFolders"

        If gRole.ToLower <> "administrator" Then
            sNotAdmin = " WHERE SmartID IN (SELECT SmartID FROM UserView WHERE " & _
            "UserID = '" & gUser & "')"

            SQL &= sNotAdmin
        End If

        SQL &= " ORDER BY SmartName"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            Do While oRs.EOF = False
                oNode = New TreeNode

                oNode.Text = oRs("smartname").Value
                oNode.Tag = "SmartFolder:" & oRs("smartid").Value

                oNode.SelectedImageIndex = 5
                oNode.ImageIndex = 5

                oTree.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        Dim addLines As Boolean = False

        oRs = clsMarsData.GetData("SELECT * FROM SystemFolders", ADODB.CursorTypeEnum.adOpenStatic)

        If oRs.RecordCount > 4 Or oRs.EOF = True Then
            clsMarsData.WriteData("DELETE FROM SystemFolders")
            addLines = True
        End If

        If addLines = True Then
            Dim sCols As String = "FolderID,FolderName,FolderSQL"
            Dim sVals As String

            sVals = "1,'Packages Due','SELECT * FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
            "PackageAttr.PackID = ScheduleAttr.PackID WHERE ScheduleAttr.NextRun <= Now() AND Status =1'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            sVals = "2,'Single Schedules Due','SELECT * FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE ScheduleAttr.NextRun <= Now() AND Status =1'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            SQL = "SELECT * FROM (PackageAttr INNER JOIN " & _
            "ScheduleHistory ON PackageAttr.PackID = ScheduleHistory.PackID) INNER JOIN " & _
            "ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID WHERE Success =0 AND " & _
            "(Year(entrydate) & '-' & month(entrydate) & '-' & day(entrydate)) = " & _
            "Year(Now()) & '-' & month(now()) & '-' & day(now())"

            sVals = "3,'Todays Failed Packages','" & SQLPrepare(SQL) & "'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)

            SQL = "SELECT * FROM (ReportAttr INNER JOIN " & _
            "ScheduleHistory ON ReportAttr.ReportID = ScheduleHistory.ReportID) INNER JOIN " & _
            "ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID WHERE Success =0 AND " & _
            "(Year(entrydate) & '-' & month(entrydate) & '-' & day(entrydate)) = " & _
            "Year(Now()) & '-' & month(now()) & '-' & day(now())"

            sVals = "4,'Todays Failed Single Schedules','" & SQLPrepare(SQL) & "'"

            clsMarsData.WriteData("INSERT INTO SystemFolders (" & sCols & ") VALUES (" & sVals & ")", False)
        End If

        oRs.Close()


    End Sub

    Public Sub AddSystemFolders(ByVal oTree As TreeView)
        Dim oRs As ADODB.Recordset
        Dim oNode As TreeNode

        oTree.Nodes.Clear()

        oRs = clsMarsData.GetData("SELECT * FROM SystemFolders")

        Do While oRs.EOF = False
            oNode = New TreeNode

            With oNode
                .Text = oRs("foldername").Value
                .Tag = "SystemFolder:" & oRs("folderid").Value
                .SelectedImageIndex = 11
                .ImageIndex = 11

                oTree.Nodes.Add(oNode)
            End With

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub
    Public Sub AddChildren(ByVal oParent As TreeNode, Optional ByVal AllDetail As Boolean = False)
        Try
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim pKey As String
            Dim oNewNode As TreeNode
            Dim rKey As String
            Dim nParent As Integer
            Dim sNotAdmin As String

            nParent = GetDelimitedWord(oParent.Tag, 2, ":")

            SQL = "SELECT FolderName, FolderID FROM Folders WHERE Parent = " & nParent

            'If gRole.ToLower <> "administrator" Then
            '    sNotAdmin = " AND FolderID IN (SELECT FolderID FROM UserView WHERE " & _
            '    "UserID = '" & gUser & "')"

            '    SQL &= sNotAdmin
            'End If

            If gConType = "DAT" Then SQL &= " ORDER BY FolderName"


            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    pKey = oRs("FolderID").Value

                    oNewNode = oParent.Nodes.Add(oRs("FolderName").Value)
                    oNewNode.Tag = "Folder:" & pKey
                    oNewNode.ImageIndex = 1
                    oNewNode.SelectedImageIndex = 2

                    AddChildren(oNewNode, AllDetail)
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            SQL = "SELECT PackageAttr.PackID, PackageName, Status, Dynamic FROM PackageAttr INNER " & _
            "JOIN ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID " & _
            "WHERE Parent = " & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    pKey = oRs("PackID").Value

                    oNewNode = oParent.Nodes.Add(oRs("PackageName").Value)
                    oNewNode.Tag = "Package:" & pKey

                    If IsNull(oRs("dynamic").Value, "0") = "1" Then
                        If oRs("status").Value = 1 Then
                            oNewNode.ImageIndex = 14
                            oNewNode.SelectedImageIndex = 14
                        Else
                            oNewNode.ImageIndex = 15
                            oNewNode.SelectedImageIndex = 15
                        End If
                    ElseIf clsMarsData.IsScheduleDataDriven(oRs("packid").Value, "package") = True Then
                        If oRs("status").Value = 1 Then
                            oNewNode.ImageIndex = 20
                            oNewNode.SelectedImageIndex = 20
                        Else
                            oNewNode.ImageIndex = 21
                            oNewNode.SelectedImageIndex = 21
                        End If
                    Else
                        If oRs("status").Value = 1 Then
                            oNewNode.ImageIndex = 3
                            oNewNode.SelectedImageIndex = 4
                        Else
                            oNewNode.ImageIndex = 9
                            oNewNode.SelectedImageIndex = 10
                        End If
                    End If

                    oRs.MoveNext()
                Loop
                oRs.Close()
            End If

            'add event based packages mkay
            SQL = "SELECT EventPackageAttr.EventPackID, PackageName, Status FROM EventPackageAttr INNER " & _
            "JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID " & _
            "WHERE Parent = " & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    pKey = oRs("EventPackID").Value

                    oNewNode = oParent.Nodes.Add(oRs("PackageName").Value)
                    oNewNode.Tag = "Event-Package:" & pKey

                    If oRs("status").Value = 1 Then
                        oNewNode.ImageIndex = 16
                        oNewNode.SelectedImageIndex = 16
                    Else
                        oNewNode.ImageIndex = 17
                        oNewNode.SelectedImageIndex = 17
                    End If

                    oRs.MoveNext()
                Loop
                oRs.Close()
            End If

            If AllDetail = True Then
                'add single schedules if need be
                SQL = "SELECT ReportID, ReportTitle,Dynamic,Bursting, IsDataDriven FROM ReportAttr WHERE Parent = " & nParent

                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing Then

                    Do While oRs.EOF = False
                        rKey = oRs("ReportID").Value

                        oNewNode = oParent.Nodes.Add(oRs("ReportTitle").Value)
                        oNewNode.Tag = "Report:" & rKey

                        If IsNull(oRs("dynamic").Value) = "1" Then
                            oNewNode.ImageIndex = 11
                            oNewNode.SelectedImageIndex = 11
                        ElseIf IsNull(oRs("bursting").Value) = "1" Then
                            oNewNode.ImageIndex = 12
                            oNewNode.SelectedImageIndex = 12
                        ElseIf IsNull(oRs("isdatadriven").Value) = "1" Then
                            oNewNode.ImageIndex = 18
                            oNewNode.SelectedImageIndex = 18
                        Else
                            oNewNode.ImageIndex = 6
                            oNewNode.SelectedImageIndex = 6
                        End If

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                'add automation schedules

                SQL = "SELECT AutoID, AutoName FROM AutomationAttr WHERE Parent = " & nParent

                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing Then

                    Do While oRs.EOF = False
                        rKey = oRs("AutoID").Value

                        oNewNode = oParent.Nodes.Add(oRs("autoname").Value)
                        oNewNode.Tag = "Automation:" & rKey
                        oNewNode.ImageIndex = 7
                        oNewNode.SelectedImageIndex = 7
                        oRs.MoveNext()
                    Loop
                    oRs.Close()
                End If

                SQL = "SELECT EventID,EventName FROM EventAttr6 WHERE Parent =" & nParent & " AND (PackID IS NULL OR PackID = 0)"

                oRs = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        rKey = oRs("eventid").Value

                        oNewNode = oParent.Nodes.Add(oRs("eventname").Value)
                        oNewNode.Tag = "Event:" & rKey
                        oNewNode.ImageIndex = 13
                        oNewNode.SelectedImageIndex = 13
                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If
            End If

            oRs = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Public Sub ViewSystemFolder(ByVal nID As Integer, ByVal oList As ListView)
        On Error GoTo Hell
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim I As Integer
        Dim olsv As ListViewItem
        Dim oUI As New clsMarsUI
        Dim sView As String = oUI.ReadRegistry("WindowViewStyle", "LargeIcon")
        Dim sName As String
        Dim NextRun As String = ""
        Dim nReportIDIndex As Integer = 0
        Dim nPackIDIndex As Integer = 0
        Dim nAutoIDIndex As Integer = 0
        Dim nEventIDIndex As Integer = 0
        Dim sType As String

        oRs = clsMarsData.GetData("SELECT * FROM SystemFolders WHERE FolderID = " & nID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                oWindow(nWindowCurrent).Text = oRs("foldername").Value

                Select Case CType(oRs("foldername").Value.ToLower, String)
                    Case "packages due"
                        SQL = "SELECT * FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
                        "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & _
                        "Status = 1 AND " & _
                        "DATEDIFF([d], [CurrentDate], NextRun) <=0"
                    Case "single schedules due"
                        SQL = "SELECT * FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
                        "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                        "Status = 1 AND " & _
                        "DATEDIFF([d], [CurrentDate],NextRun) <= 0"
                    Case "todays failed packages"
                        SQL = "SELECT * FROM (PackageAttr INNER JOIN ScheduleHistory ON " & _
                        "PackageAttr.PackID = ScheduleHistory.PackID) INNER JOIN ScheduleAttr ON " & _
                        "PackageAttr.PackID = ScheduleAttr.PackID WHERE Success =0 AND " & _
                        "DATEDIFF([d], [CurrentDate],EntryDate) = 0"
                    Case "todays failed single schedules"
                        SQL = "SELECT * FROM (ReportAttr INNER JOIN ScheduleHistory ON " & _
                        "ReportAttr.ReportID = ScheduleHistory.ReportID) INNER JOIN " & _
                        "ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                        "Success =0 AND " & _
                        "DATEDIFF([d], [CurrentDate],EntryDate) = 0"
                End Select

                If gConType = "DAT" Then
                    SQL = SQL.Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
                Else
                    SQL = SQL.Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
                End If
            End If
            oRs.Close()
        End If

        If oWindow(nWindowCurrent).Text.ToLower.IndexOf("package") > -1 Then
            sType = "package"
        Else
            sType = "single"
        End If

        BusyProgress(10, "Generating queries for action...")

        If sView = "Details" Then

            If sType = "single" Then
                BuildFolderDetails(0, oWindow(nWindowCurrent), SQL, , , , True, True)
            Else
                BuildFolderDetails(0, oWindow(nWindowCurrent), , SQL, , , True, True)
            End If

            Return
        End If

        oList.Items.Clear()

        'open each query and get the relevant schedules!

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then

            For I = 0 To oRs.Fields.Count - 1
                If oRs.Fields(I).Name.ToLower.IndexOf("reportid") > -1 Then
                    nReportIDIndex = I
                ElseIf oRs.Fields(I).Name.ToLower.IndexOf("packid") > -1 Then
                    nPackIDIndex = I
                ElseIf oRs.Fields(I).Name.ToLower.IndexOf("autoid") > -1 Then
                    nAutoIDIndex = I
                ElseIf oRs.Fields(I).Name.ToLower.IndexOf("eventid") > -1 Then
                    nEventIDIndex = I
                End If
            Next

            Do While oRs.EOF = False

                BusyProgress(90, "Adding items to smart folder...")

                If sType = "single" Then

                    olsv = New ListViewItem

                    olsv.Text = oRs("reporttitle").Value
                    olsv.Tag = "Report:" & oRs(nReportIDIndex).Value

                    If clsMarsData.IsScheduleBursting(oRs(nReportIDIndex).Value) = True Then
                        olsv.Group = oList.Groups("Bursting Schedules")

                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 11
                        Else
                            olsv.ImageIndex = 12
                        End If
                    ElseIf clsMarsData.IsScheduleDynamic(oRs(nReportIDIndex).Value, "Report") = True Then
                        olsv.Group = oList.Groups("Dynamic Schedules")

                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 7
                        Else
                            olsv.ImageIndex = 8
                        End If
                    Else
                        olsv.Group = oList.Groups("Single Schedules")

                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 1
                        Else
                            olsv.ImageIndex = 3
                        End If
                    End If

                Else
                    olsv = New ListViewItem

                    olsv.Text = oRs("packagename").Value
                    olsv.Tag = "Package:" & oRs(nPackIDIndex).Value

                    If clsMarsData.IsScheduleDynamic(oRs(nPackIDIndex).Value, "Package") = False Then
                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 2
                        Else
                            olsv.ImageIndex = 4
                        End If

                        olsv.Group = oList.Groups("Package Schedules")
                    Else
                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 13
                        Else
                            olsv.ImageIndex = 14
                        End If
                        olsv.Group = oList.Groups("Dynamic Package Schedules")
                    End If

                End If

                If Not olsv Is Nothing Then oList.Items.Add(olsv)
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        BusyProgress(100, "Closing...", True)
Hell:
        BusyProgress(100, "Closing...", True)
    End Sub
    Public Sub OpenSmartFolder(ByVal nID As Integer, ByVal oList As ListView)
        On Error GoTo Hell
        Dim SQL() As String
        Dim oRs As ADODB.Recordset
        Dim I, X As Integer
        Dim olsv As ListViewItem
        Dim oUI As New clsMarsUI
        Dim sView As String = oUI.ReadRegistry("WindowViewStyle", "LargeIcon")
        Dim sName As String
        Dim NextRun As String = ""
        Dim nReportIDIndex As Integer = 0
        Dim nPackIDIndex As Integer = 0
        Dim nAutoIDIndex As Integer = 0
        Dim nEventIDIndex As Integer = 0

        oRs = clsMarsData.GetData("SELECT * FROM SmartFolders WHERE SmartID =" & nID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                oWindow(nWindowCurrent).Text = oRs("smartname").Value
            End If

            oRs.Close()
        End If

        BusyProgress(10, "Generating queries for action...")

        SQL = SmartQueryBuilder(nID)

        If sView = "Details" Then
            BuildFolderDetails(0, oWindow(nWindowCurrent), SQL(0), SQL(1), SQL(2), SQL(3), SQL(4), True)

            Return
        End If

        oList.Items.Clear()

        I = 0

        'open each query and get the relevant schedules!

        For Each s As String In SQL
            ''console.writeLine(s)
            If s.Length > 0 Then
                BusyProgress(65, "Retrieving query data...")

                oRs = clsMarsData.GetData(s)

                If Not oRs Is Nothing Then

                    For X = 0 To oRs.Fields.Count - 1
                        If oRs.Fields(I).Name.ToLower.IndexOf("reportid") > -1 Then
                            nReportIDIndex = I
                        ElseIf oRs.Fields(I).Name.ToLower.IndexOf("packid") > -1 Then
                            nPackIDIndex = I
                        ElseIf oRs.Fields(I).Name.ToLower.IndexOf("autoid") > -1 Then
                            nAutoIDIndex = I
                        ElseIf oRs.Fields(I).Name.ToLower.IndexOf("eventid") > -1 Then
                            nEventIDIndex = I
                        End If
                    Next

                    Do While oRs.EOF = False

                        BusyProgress(90, "Adding items to smart folder...")

                        Select Case I
                            Case 0
                                olsv = New ListViewItem

                                olsv.Text = oRs("reporttitle").Value
                                olsv.Tag = "Report:" & oRs(nReportIDIndex).Value

                                If clsMarsData.IsScheduleBursting(oRs(nReportIDIndex).Value) = True Then
                                    olsv.Group = oList.Groups("Bursting Schedules")
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 11
                                    Else
                                        olsv.ImageIndex = 12
                                    End If
                                ElseIf clsMarsData.IsScheduleDynamic(oRs(nReportIDIndex).Value, "Report") = True Then
                                    olsv.Group = oList.Groups("Dynamic Schedules")
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 7
                                    Else
                                        olsv.ImageIndex = 8
                                    End If
                                ElseIf clsMarsData.IsScheduleDataDriven(oRs(nReportIDIndex).Value) = True Then
                                    olsv.Group = oList.Groups("Data-Driven Schedules")

                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 17
                                    Else
                                        olsv.ImageIndex = 18
                                    End If
                                Else
                                    olsv.Group = oList.Groups("Single Schedules")
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 1
                                    Else
                                        olsv.ImageIndex = 3
                                    End If
                                End If
                            Case 1
                                olsv = New ListViewItem

                                olsv.Text = oRs("packagename").Value
                                olsv.Tag = "Package:" & oRs(nPackIDIndex).Value

                                If clsMarsData.IsScheduleDynamic(oRs(nPackIDIndex).Value, "Package") = True Then
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 13
                                    Else
                                        olsv.ImageIndex = 14
                                    End If
                                    olsv.Group = oList.Groups("Package Schedules")
                                ElseIf clsMarsData.IsScheduleDataDriven(oRs(nPackIDIndex).Value, "Package") = True Then
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 19
                                    Else
                                        olsv.ImageIndex = 20
                                    End If
                                    olsv.Group = oList.Groups("Data-Driven Packages")
                                Else
                                    If oRs("status").Value = 1 Then
                                        olsv.ImageIndex = 2
                                    Else
                                        olsv.ImageIndex = 4
                                    End If
                                    olsv.Group = oList.Groups("Dynamic Package Schedules")
                                End If

                            Case 2
                                olsv = New ListViewItem

                                olsv.Text = oRs("autoname").Value
                                olsv.Tag = "Automation:" & oRs(nAutoIDIndex).Value

                                If oRs("status").Value = 1 Then
                                    olsv.ImageIndex = 5
                                Else
                                    olsv.ImageIndex = 6
                                End If
                                olsv.Group = oList.Groups("Automation Schedules")
                            Case 3
                                olsv = New ListViewItem

                                olsv.Text = oRs("eventname").Value
                                olsv.Tag = "event:" & oRs("eventid").Value

                                If oRs("status").Value = 1 Then
                                    olsv.ImageIndex = 9
                                Else
                                    olsv.ImageIndex = 10
                                End If
                                olsv.Group = oList.Groups("Event-Based Schedules")
                            Case 4
                                olsv = New ListViewItem

                                olsv.Text = oRs("packagename").Value
                                olsv.Tag = "event-package:" & oRs("eventpackid").Value

                                If oRs("status").Value = 1 Then
                                    olsv.ImageIndex = 15
                                Else
                                    olsv.ImageIndex = 16
                                End If
                                olsv.Group = oList.Groups("Event-Based Packages")
                        End Select

                        If Not olsv Is Nothing Then oList.Items.Add(olsv)
                        oRs.MoveNext()
                    Loop
                    oRs.Close()
                End If
            End If
            I += 1
        Next

        BusyProgress(100, "Closing...", True)
Hell:
        BusyProgress(100, "Closing...", True)
    End Sub
    Public Function SmartQueryBuilder(ByVal SmartID As Integer) As String()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim nID As Integer
        Dim sRptCriteria() As String
        Dim sPackCriteria() As String
        Dim sAutoCriteria() As String
        Dim sEventCriteria() As String
        Dim sEventPackCriteria() As String
        Dim I As Integer
        Dim sColumn() As String
        Dim sValue() As String
        Dim sQuery(4) As String
        Dim sType As String
        Dim sFinal As String
        Dim x As Integer = 0
        Dim sNotAdmin As String

        SQL = "SELECT * FROM SmartFolders WHERE SmartID =" & SmartID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            I = 0
            nID = oRs("smartid").Value
            sType = oRs("smarttype").Value

            SQL = "SELECT * FROM SmartFolderAttr WHERE SmartID = " & nID

            oRs1 = clsMarsData.GetData(SQL)

            If Not oRs1 Is Nothing Then

                Do While oRs1.EOF = False
                    ReDim Preserve sColumn(I)
                    ReDim Preserve sValue(I)

                    sColumn(I) = oRs1("smartcolumn").Value
                    sValue(I) = oRs1("smartcriteria").Value

                    oRs1.MoveNext()
                    I += 1
                Loop
            End If

            oRs1.Close()
        End If

        oRs.Close()

        'create criteria for reports
        For I = 0 To sColumn.GetUpperBound(0)

            Select Case sColumn(I).ToLower
                Case "status"
                    ReDim Preserve sRptCriteria(I)
                    sRptCriteria(I) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)

                Case "schedulename"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "ReportTitle" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportTitle IS NULL OR ReportTitle LIKE '')"
                        Else
                            sRptCriteria(I) = "ReportTitle IS NOT NULL AND ReportTitle NOT LIKE ''"
                        End If
                    End If
                Case "foldername"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sRptCriteria(I) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                Case "sendto", "subject", "message", "cc", "bcc"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE ''))"
                        Else
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM DestinationAttr WHERE " & sColumn(I) & " IS NOT NULL OR " & sColumn(I) & " NOT LIKE ''))"
                        End If
                    End If
                Case "errmsg"
                    ReDim Preserve sRptCriteria(I)
                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sRptCriteria(I) = "(ReportAttr.ReportID IN (SELECT ReportID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sRptCriteria(I)
                    If gConType = "DAT" Then
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I)
                    End If
                Case "scheduletype"
                    ReDim Preserve sRptCriteria(I)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sRptCriteria(I) = "ScheduleAttr.ReportID = 1"
                    End If
                Case Else
                    ReDim Preserve sRptCriteria(I)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sRptCriteria(I) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sRptCriteria(I) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sRptCriteria(I) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
            End Select
        Next


        'for packages
        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "destinationtype", _
                "printername", "ftpserver", "ftpusername", "description", "keyword"

                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sPackCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sPackCriteria(x)

                    If gConType = "DAT" Then
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "PackageName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageName IS NULL OR PackageName LIKE '')"
                        Else
                            sPackCriteria(x) = "PackageName IS NOT NULL AND PackageName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sPackCriteria(x)

                    sPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "sendto", "subject", "message", "cc", "bcc"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE ''))"
                        Else
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM DestinationAttr WHERE " & sColumn(I) & " IS NOT NULL OR " & sColumn(I) & " NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sPackCriteria(x) = "(PackageAttr.PackID IN (SELECT PackID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sPackCriteria(x) = "ScheduleAttr.ReportID = 1"
                    End If
                    x += 1
            End Select
        Next

        'for autoschedules
        x = 0
        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "description", "keyword"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sAutoCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sAutoCriteria(x)

                    If gConType = "DAT" Then
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sAutoCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "AutoName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(AutoName IS NULL OR AutoName LIKE '')"
                        Else
                            sAutoCriteria(x) = "AutoName IS NOT NULL AND AutoName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"

                    ReDim Preserve sAutoCriteria(x)
                    sAutoCriteria(x) = sColumn(x) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sAutoCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sAutoCriteria(x) = "(AutomationAttr.AutoID IN (SELECT AutoID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sAutoCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sAutoCriteria(x) = "ScheduleAttr.ReportID = 1"
                    End If
                    x += 1
            End Select
        Next

        'for event based schedules
        x = 0

        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "description", "keyword"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sEventCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = "EventName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(EventName IS NULL OR EventName LIKE '')"
                        Else
                            sEventCriteria(x) = "EventName IS NOT NULL AND EventName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sEventCriteria(x)
                    sEventCriteria(x) = sColumn(x) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sEventCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "scheduletype"
                    ReDim Preserve sEventCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sEventCriteria(x) = "EventID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sEventCriteria(x) = "EventID <> 0"
                    End If
                    x += 1

            End Select
        Next

        'for event-based packages
        x = 0

        For I = 0 To sColumn.GetUpperBound(0)
            Select Case sColumn(I).ToLower
                Case "frequency", "starttime", _
                "destinationtype", _
                "printername", "ftpserver", "ftpusername", "description", "keyword"

                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(" & sColumn(I) & " IS NULL OR " & sColumn(I) & " LIKE '')"
                        Else
                            sEventPackCriteria(x) = sColumn(I) & " IS NOT NULL AND " & sColumn(I) & " NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "startdate", "enddate", "nextrun"
                    ReDim Preserve sEventPackCriteria(x)

                    If gConType = "DAT" Then
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", "#")
                    Else
                        sEventPackCriteria(x) = sColumn(I) & " " & sValue(I)
                    End If

                    x += 1
                Case "schedulename"

                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "PackageName" & " " & sValue(I)
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(PackageName IS NULL OR PackageName LIKE '')"
                        Else
                            sEventPackCriteria(x) = "PackageName IS NOT NULL AND PackageName NOT LIKE ''"
                        End If
                    End If
                    x += 1
                Case "status"
                    ReDim Preserve sEventPackCriteria(x)

                    sEventPackCriteria(x) = sColumn(I) & " " & sValue(I).Replace("'", String.Empty)
                    x += 1
                Case "foldername"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NULL OR FolderName LIKE ''))"
                        Else
                            sEventPackCriteria(x) = "(Parent IN (SELECT FolderID FROM Folders WHERE FolderName IS NOT NULL OR FolderName NOT LIKE ''))"
                        End If
                    End If
                    x += 1
                Case "errmsg"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("empty") = -1 Then
                        sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg " & sValue(I) & "))"
                    Else
                        If sValue(I).ToLower.Trim = "is empty" Then
                            sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg IS NULL OR ErrMsg LIKE ''))"
                        Else
                            sEventPackCriteria(x) = "(EventPackageAttr.EventPackID IN (SELECT EventPackID FROM ScheduleHistory WHERE ErrMsg IS NOT NULL OR ErrMsg NOT LIKE ''))"
                        End If
                    End If

                    x += 1
                Case "scheduletype"
                    ReDim Preserve sEventPackCriteria(x)

                    If sValue(I).ToLower.IndexOf("single schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.ReportID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("package schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.PackID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("automation schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.AutoID <> 0"
                    ElseIf sValue(I).ToLower.IndexOf("event-based schedule") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.ReportID = 1"
                    ElseIf sValue(I).ToLower.IndexOf("event-based package") > -1 Then
                        sEventPackCriteria(x) = "ScheduleAttr.EventPackID <> 0"
                    End If
                    x += 1
            End Select
        Next

        If sType = "All" Then
            sType = " AND "
        Else
            sType = " OR "
        End If

        'create the full criteria for single schedules
        Try
            For I = 0 To sRptCriteria.GetUpperBound(0)
                If I < sRptCriteria.GetUpperBound(0) Then
                    sFinal &= sRptCriteria(I) & sType
                Else
                    sFinal &= sRptCriteria(I)
                End If
            Next

            sQuery(0) = "SELECT * FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND ReportAttr.ReportID IN (SELECT ReportID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery(0) &= sNotAdmin
            End If
        Catch
            sQuery(0) = String.Empty
        End Try

        'for packages
        sFinal = String.Empty

        Try
            For I = 0 To sPackCriteria.GetUpperBound(0)
                If I < sPackCriteria.GetUpperBound(0) Then
                    sFinal &= sPackCriteria(I) & sType
                Else
                    sFinal &= sPackCriteria(I)
                End If
            Next

            sQuery(1) = "SELECT * FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
            "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery(1) &= sNotAdmin
            End If
        Catch
            sQuery(1) = String.Empty
        End Try

        'for automation
        sFinal = String.Empty

        Try
            For I = 0 To sAutoCriteria.GetUpperBound(0)
                If I < sAutoCriteria.GetUpperBound(0) Then
                    sFinal &= sAutoCriteria(I) & sType
                Else
                    sFinal &= sAutoCriteria(I)
                End If
            Next

            sQuery(2) = "SELECT * FROM AutomationAttr INNER JOIN ScheduleAttr ON " & _
            "AutomationAttr.AutoID = ScheduleAttr.AutoID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND AutomationAttr.AutoID IN (SELECT AutoID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery(2) &= sNotAdmin
            End If
        Catch
            sQuery(2) = String.Empty
        End Try

        'for events
        sFinal = String.Empty

        Try
            For I = 0 To sEventCriteria.GetUpperBound(0)
                If I < sEventCriteria.GetUpperBound(0) Then
                    sFinal &= sEventCriteria(I) & sType
                Else
                    sFinal &= sEventCriteria(I)
                End If
            Next

            sQuery(3) = "SELECT * FROM EventAttr6 WHERE " & sFinal & " AND (PackID IS NULL OR PackID =0) "

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery(3) &= sNotAdmin
            End If
        Catch
            sQuery(3) = String.Empty
        End Try

        'for event-based packages
        sFinal = String.Empty

        Try
            For I = 0 To sEventPackCriteria.GetUpperBound(0)
                If I < sEventPackCriteria.GetUpperBound(0) Then
                    sFinal &= sEventPackCriteria(I) & sType
                Else
                    sFinal &= sEventPackCriteria(I)
                End If
            Next

            sQuery(4) = "SELECT * FROM EventPackageAttr INNER JOIN ScheduleAttr ON " & _
            "EventPackageAttr.EventPackID = ScheduleAttr.EventPackID WHERE " & sFinal

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                sQuery(4) &= sNotAdmin
            End If
        Catch
            sQuery(4) = String.Empty
        End Try


        Return sQuery

    End Function

    Public Sub BuildEventPackageDetails(ByVal nPackID As Integer, ByVal oForm As frmWindow)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String = " WHERE PackID = " & nPackID
        Dim oList As ListView
        Dim lsv As ListViewItem
        Dim UserColumns As Boolean = False
        Dim sDataCols() As String
        Dim I As Integer
        Dim sOwner As String
        Dim IsRpt As Boolean
        Dim sNotAdmin As String

        oList = oForm.lsvWindow

        SQL = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"

        oRs = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()

                With .Columns
                    .Add("Name", 100, HorizontalAlignment.Left)
                    .Add("Owner", 100, HorizontalAlignment.Left)
                End With

            End With

            UserColumns = False
        Else
            UserColumns = True

            ReDim sDataCols(0)

            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()

                .Columns.Add("Name", 100, HorizontalAlignment.Left)

                Do While oRs.EOF = False
                    Dim oCol As ColumnHeader = New ColumnHeader
                    ReDim Preserve sDataCols(I)

                    sDataCols(I) = oRs("datacolumn").Value

                    oCol.Text = oRs("caption").Value
                    oCol.TextAlign = HorizontalAlignment.Left
                    oCol.Width = 100


                    With .Columns
                        .Add(oCol)
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                .Columns.Add("Owner", 100, HorizontalAlignment.Left)
                oRs.Close()
            End With
        End If


        SQL = "SELECT * FROM EventAttr6 WHERE PackID = " & nPackID

        If gRole.ToLower <> "administrator" Then
            sNotAdmin = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
            "UserView WHERE UserID = '" & gUser & "')"

            SQL &= sNotAdmin
        End If


        If gConType = "DAT" Then
            SQL &= " ORDER BY EventName"
        Else
            SQL &= " ORDER BY CAST(EventName AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try

            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("eventname").Value)
                lsv.Tag = "Event:" & oRs("eventid").Value
                lsv.Group = oList.Groups("Event-Based Schedules")
                sOwner = IsNull(oRs("owner").Value, "sys")

                If oRs("status").Value = 1 Then
                    lsv.ImageIndex = 9
                Else
                    lsv.ImageIndex = 10
                End If

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    If oRs("status").Value = 1 Then
                                        lsv.SubItems.Add("On-Event")
                                    Else
                                        lsv.SubItems.Add("Disabled")
                                    End If
                                Case "description"
                                    lsv.SubItems.Add(oRs(s).Value)
                                Case "entrydate"
                                    lsv.SubItems.Add(Me.GetEventLastRun(oRs("eventid").Value))
                                Case "success"
                                    lsv.SubItems.Add(Me.GetEventLastResult(oRs("eventid").Value))
                                Case Else
                                    lsv.SubItems.Add("Event-Based")
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Public Sub OpenEventPackage(ByVal oParent As TreeNode, ByVal olist As ListView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oLsv As ListViewItem
        Dim PackID As Integer = GetDelimitedWord(oParent.Tag, 2, ":")

        olist.Items.Clear()

        SQL = "SELECT EventID, EventName, Status FROM EventAttr6 WHERE PackID = " & PackID & " ORDER BY PackOrderID"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False

                oLsv = olist.Items.Add(oRs("eventname").Value, 9)
                oLsv.Tag = "Event:" & oRs(0).Value
                oLsv.Group = olist.Groups("Event-Based Schedules")

                If oRs("status").Value = 1 Then
                    oLsv.ImageIndex = 9
                Else
                    oLsv.ImageIndex = 10
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Public Sub OpenPackage(ByVal oParent As TreeNode, ByVal olist As ListView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oLsv As ListViewItem
        Dim PackID As Integer = GetDelimitedWord(oParent.Tag, 2, ":")

        olist.Items.Clear()

        SQL = "SELECT R.ReportID, ReportTitle, Status FROM ReportAttr R " & _
        "INNER JOIN PackagedReportAttr P ON R.ReportID = P.ReportID WHERE R.PackID = " & PackID & " ORDER BY PackOrderID"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False

                oLsv = olist.Items.Add(oRs("ReportTitle").Value, 1)
                oLsv.Tag = "Packed Report:" & oRs(0).Value
                oLsv.Group = olist.Groups("Packaged Reports")

                If oRs("status").Value = 1 Then
                    oLsv.ImageIndex = 1
                Else
                    oLsv.ImageIndex = 3
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Public Sub CreateDeskTop(ByVal oParent As TreeNode, ByVal oList As ListView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim olsv As ListViewItem
        Dim nFolderID As Integer = GetDelimitedWord(oParent.Tag, 2, ":")
        Dim sNotAdmin As String
        Dim nStatus As Integer

        oList.Items.Clear()

        'lets add folders first
        SQL = "SELECT FolderID, FolderName FROM Folders WHERE " & _
            "Parent = " & nFolderID

        If gConType = "DAT" Then
            SQL &= " ORDER BY FolderName"
        Else
            SQL &= " ORDER BY CAST(FolderName AS VARCHAR(255))"
        End If

        'If gRole.ToLower <> "administrator" Then
        '    sNotAdmin = " AND FolderID IN (SELECT FolderID FROM UserView WHERE " & _
        '    "UserID = '" & gUser & "')"

        '    SQL &= sNotAdmin
        'End If



        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                olsv = oList.Items.Add(oRs("FolderName").Value, 0)
                olsv.Tag = "Folder:" & oRs("FolderID").Value
                olsv.Group = oList.Groups("Folders")
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        'if the folder is not desktop then lets get the child schedules and packages

        If nFolderID > 0 Then

            'add single schedules
            SQL = "SELECT ReportAttr.ReportID, ReportAttr.ReportTitle, ScheduleAttr.Status, " & _
            "ReportAttr.Dynamic, ReportAttr.Bursting, ReportAttr.IsDataDriven " & _
            "FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE Parent=" & nFolderID

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND ReportAttr.ReportID IN (SELECT ReportID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            ''console.writeLine(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    olsv = oList.Items.Add(oRs("ReportTitle").Value, 1)
                    olsv.Tag = "Report:" & oRs("ReportID").Value

                    nStatus = IsNull(oRs(2).Value, 1)

                    Dim nDynamic As Integer
                    Dim nBursting As Integer
                    Dim ndataDriven As Integer

                    Try
                        nDynamic = IsNull(oRs("dynamic").Value)
                    Catch ex As Exception
                        nDynamic = 0
                    End Try

                    Try
                        nBursting = IsNull(oRs("bursting").Value, 0)
                    Catch ex As Exception
                        nBursting = 0
                    End Try

                    Try
                        ndataDriven = IsNull(oRs("isdatadriven").Value, 0)
                    Catch ex As Exception
                        ndataDriven = 0
                    End Try

                    If nDynamic = 1 Then
                        If nStatus = 1 Then
                            olsv.ImageIndex = 7
                        Else
                            olsv.ImageIndex = 8
                        End If

                        olsv.Group = oList.Groups("Dynamic Schedules")
                    ElseIf nBursting = 1 Then
                        If nStatus = 1 Then
                            olsv.ImageIndex = 11
                        Else
                            olsv.ImageIndex = 12
                        End If
                        olsv.Group = oList.Groups("Bursting Schedules")
                    ElseIf ndataDriven = 1 Then
                        If nStatus = 1 Then
                            olsv.ImageIndex = 17
                        Else
                            olsv.ImageIndex = 18
                        End If

                        olsv.Group = oList.Groups("Data-Driven Schedules")
                    Else
                        If nStatus = 1 Then
                            olsv.ImageIndex = 1
                        Else
                            olsv.ImageIndex = 3
                        End If
                        olsv.Group = oList.Groups("Single Schedules")
                    End If

                    If nStatus = 0 Then olsv.ForeColor = System.Drawing.Color.DimGray

                    oRs.MoveNext()
                Loop
                oRs.Close()
            End If

            'add packages
            SQL = "SELECT ScheduleAttr.Status, PackageAttr.PackID, PackageAttr.PackageName FROM ScheduleAttr INNER JOIN PackageAttr ON ScheduleAttr.PackID = PackageAttr.PackID WHERE Parent=" & nFolderID

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If


            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    olsv = oList.Items.Add(oRs("PackageName").Value, 2)
                    olsv.Tag = "Package:" & oRs("PackID").Value

                    If clsMarsData.IsScheduleDynamic(oRs("packid").Value, "Package") = True Then
                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 13
                        Else
                            olsv.ImageIndex = 14
                            olsv.ForeColor = System.Drawing.Color.DimGray
                        End If
                        olsv.Group = oList.Groups("Dynamic Package Schedules")
                    ElseIf clsMarsData.IsScheduleDataDriven(oRs("packid").Value, "package") = True Then
                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 19
                        Else
                            olsv.ImageIndex = 20
                            olsv.ForeColor = System.Drawing.Color.DimGray
                        End If

                        olsv.Group = oList.Groups("Data-Driven Packages")
                    Else
                        If oRs("status").Value = 1 Then
                            olsv.ImageIndex = 2
                        Else
                            olsv.ImageIndex = 4
                            olsv.ForeColor = System.Drawing.Color.DimGray
                        End If
                        olsv.Group = oList.Groups("Package Schedules")
                    End If

                    oRs.MoveNext()
                Loop
                oRs.Close()
            End If

            'add automation schedules
            SQL = "SELECT ScheduleAttr.Status, AutomationAttr.AutoID, AutomationAttr.AutoName FROM ScheduleAttr INNER JOIN AutomationAttr ON AutomationAttr.AutoID = ScheduleAttr.AutoID WHERE Parent =" & nFolderID

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND AutomationAttr.AutoID IN (SELECT AutoID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    olsv = oList.Items.Add(oRs("autoname").Value, 5)
                    olsv.Tag = "Automation:" & oRs("AutoID").Value
                    olsv.Group = oList.Groups("Automation Schedules")

                    If oRs("status").Value = 1 Then
                        olsv.ImageIndex = 5
                    Else
                        olsv.ImageIndex = 6
                        olsv.ForeColor = System.Drawing.Color.DimGray
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If


            'and not finally, lets add event based schedules
            SQL = "SELECT * FROM EventAttr6 WHERE Parent = " & nFolderID & " AND (PackID IS NULL OR PackID = 0)"

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    olsv = oList.Items.Add(oRs("eventname").Value)
                    olsv.Tag = "Event:" & oRs("eventid").Value
                    olsv.Group = oList.Groups("Event-Based Schedules")
                    If oRs("status").Value = 1 Then
                        olsv.ImageIndex = 9
                    Else
                        olsv.ImageIndex = 10
                        olsv.ForeColor = System.Drawing.Color.DimGray
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            'finally lets add event based packages

            SQL = "SELECT EventPackageAttr.EventPackID, PackageName, Status FROM EventPackageAttr INNER " & _
                "JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID " & _
                "WHERE Parent = " & nFolderID

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    olsv = oList.Items.Add(oRs("packagename").Value, 5)
                    olsv.Tag = "Event-Package:" & oRs("EventPackID").Value
                    olsv.Group = oList.Groups("Event-Based Packages")

                    If oRs("status").Value = 1 Then
                        olsv.ImageIndex = 15
                    Else
                        olsv.ImageIndex = 16
                        olsv.ForeColor = System.Drawing.Color.DimGray
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        End If

    End Sub

    Public Sub OpenWindow(ByVal nFolderID As Int32, ByVal oList As ListView)

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oLsv As ListViewItem

        oList.Items.Clear()

        'lets add folders first
        SQL = "SELECT FolderID, FolderName FROM Folders WHERE " & _
            "Parent = " & nFolderID


        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oLsv = oList.Items.Add(oRs("FolderName").Value, 0)
                oLsv.Tag = "Folder:" & oRs("FolderID").Value
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        'add schedules
        SQL = "SELECT ReportID, ReportTitle FROM ReportAttr WHERE Parent = " & nFolderID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oLsv = oList.Items.Add(oRs("ReportTitle").Value, 1)
                oLsv.Tag = "Report:" & oRs("ReportID").Value
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If
    End Sub

    'Private NodeFound As Boolean = False
    ''' <summary>
    ''' Find a node in a treeview
    ''' </summary>
    ''' <param name="sSearch">the objects tag to search</param>
    ''' <param name="oTree">the tree view</param>
    ''' <param name="oSearchNode">optional node to search</param>
    ''' <remarks></remarks>
    Public Sub FindNode(ByVal sSearch As String, ByRef oTree As TreeView, _
    Optional ByVal oSearchNode As TreeNode = Nothing)

        Try
            Dim oNode As TreeNode

            If Not oSearchNode Is Nothing Then
                For Each oNode In oSearchNode.Nodes
                    If oNode.Tag = sSearch Then
                        oSearchNode.Expand()
                        oNode.Parent.Expand()
                        oTree.SelectedNode = oNode
                        oNode.EnsureVisible()
                        'NodeFound = True
                        Exit Sub
                    ElseIf oNode.GetNodeCount(False) > 0 Then
                        'If NodeFound = True Then Exit Sub
                        FindNode(sSearch, oTree, oNode)
                    End If
                Next
            Else
                For Each oNode In oTree.Nodes
                    If oNode.Tag = sSearch Then

                        If oNode.Parent IsNot Nothing Then oNode.Parent.Expand()
                        oTree.SelectedNode = oNode
                        oNode.EnsureVisible()
                        'NodeFound = True
                        Exit Sub
                    ElseIf oNode.GetNodeCount(False) > 0 Then
                        'If NodeFound = True Then Exit Sub
                        FindNode(sSearch, oTree, oNode)
                    End If
                Next
            End If
        Catch : End Try

    End Sub

    Public Sub ResetError(ByVal oSender As Object, ByVal ErrProv As ErrorProvider)
        ErrProv.SetError(oSender, "")
    End Sub
    Public Function ExportRegistry(ByVal sKeyToRead As String, ByVal sFilePath As String) As Boolean
        Dim oReg As RegistryKey
        Dim sOut As String
        Dim oValue As Object

        Try
            oReg = Registry.LocalMachine.OpenSubKey(sKeyToRead, True)

            Dim sValues() As String

            sValues = oReg.GetValueNames

            For Each s As String In sValues
                oValue = oReg.GetValue(s)

                If TypeOf oValue Is Integer Then
                    sOut &= Chr(34) & s & Chr(34) & "=dword:0000000" & oValue & vbCrLf
                ElseIf TypeOf oValue Is System.Array Or TypeOf oValue Is Byte Then
                    Application.DoEvents()
                Else
                    sOut &= Chr(34) & s & Chr(34) & "=" & Chr(34) & oReg.GetValue(s, "") & Chr(34) & vbCrLf
                End If
            Next

            Dim sHeader As String

            sHeader = "Windows Registry Editor Version 5.00" & vbCrLf & vbCrLf & _
            "[HKEY_LOCAL_MACHINE\" & sKeyToRead & "]" & vbCrLf

            sOut = sHeader & sOut

            SaveTextToFile(sOut, sFilePath, , False)
            Return True
        Catch
            Return False
        End Try
    End Function
    Public Function IsNotChild(ByVal nMoved As Integer, ByVal nTarget As Integer) As Boolean
        Try

            Dim oRs, oRs1 As ADODB.Recordset

            oRs = clsMarsData.GetData("SELECT FolderID FROM Folders WHERE Parent  =" & nMoved)

            If oRs.EOF = True Then Return True

            Do While oRs.EOF = False
                If oRs(0).Value = nTarget Then
                    oRs.Close()
                    Return False
                End If

                oRs1 = clsMarsData.GetData("SELECT FolderID FROM Folders WHERE Parent = " & oRs(0).Value)

                Do While oRs1.EOF = False
                    If oRs1(0).Value = nTarget Then
                        oRs1.Close()
                        Return False
                    End If

                    If IsNotChild(oRs1(0).Value, nTarget) = False Then
                        Return False
                    End If

                    oRs1.MoveNext()
                Loop

                oRs1.Close()
                oRs.MoveNext()
            Loop

            oRs.Close()

            Return True
        Catch
            Return True
        End Try
    End Function

    Public Overloads Function ReadRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByRef Value As Object) As Object

        Dim Key As RegistryKey
        Dim Temp As Object
        Dim oRemoteReg As New RemoteRegistry.clsRegistry

        Temp = Value

        If gPCName.Length = 0 Then
            Try
                'Open the registry key.
                Key = ParentKey.OpenSubKey(SubKey, True)

                If Key Is Nothing Then 'if the key doesn't exist
                    Return Temp
                End If

                'Get the value.
                Value = Key.GetValue(ValueName)

                Key.Close()

                If Value Is Nothing Then
                    Return Temp
                Else
                    Return Value
                End If
            Catch e As Exception
                Return Temp
            End Try
        Else
            Try
                If ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_local_machine" Then
                    Value = oRemoteReg.GetValue(RemoteRegistry.HKEYs.eHKEY_LOCAL_MACHINE, sKey, ValueName, gPCName)
                ElseIf ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_current_user" Then
                    Value = oRemoteReg.GetValue(RemoteRegistry.HKEYs.eHKEY_CURRENT_USER, sKey, ValueName, gPCName)
                End If
            Catch
                Value = Temp
            End Try

            If Value Is Nothing Then
                Value = Temp
            End If

            Return Value
        End If

    End Function

    Public Overloads Function DeleteRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String) As Boolean

        Dim Key As RegistryKey

        If gPCName.Length = 0 Then
            Try
                'Open the registry key.
                Key = ParentKey.OpenSubKey(SubKey, True)

                If Key Is Nothing Then Exit Function 'if the key doesn't exist

                Key.DeleteValue(ValueName, False)

                Key.Close()

                Return True
            Catch e As Exception

                Return False
            End Try
        Else
            Try
                Dim oRemoteReg As New RemoteRegistry.clsRegistry

                If ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_local_machine" Then
                    oRemoteReg.DeleteValueReg(RemoteRegistry.HKEYs.eHKEY_LOCAL_MACHINE, sKey, ValueName, gPCName)
                ElseIf ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_current_user" Then
                    oRemoteReg.DeleteValueReg(RemoteRegistry.HKEYs.eHKEY_CURRENT_USER, sKey, ValueName, gPCName)
                End If
            Catch : End Try
        End If

    End Function


    Public Overloads Function SaveRegistry(ByVal ParentKey As RegistryKey, ByVal SubKey As String, _
    ByVal ValueName As String, ByVal Value As Object) As Boolean

        Dim Key As RegistryKey
        Dim Temp As Object

        If Value Is Nothing Then Value = ""

        Temp = Value

        If gPCName.Length = 0 Then
            Try
                'Open the registry key.
                Key = ParentKey.OpenSubKey(SubKey, True)

                If Key Is Nothing Then 'if the key doesn't exist

                    Key = ParentKey

                    Key = Key.CreateSubKey(SubKey)
                End If

                Key.SetValue(ValueName, Value)

                Key.Close()

                Return True
            Catch e As Exception

                Return False
            End Try
        Else
            Try
                Dim oRemoteReg As New RemoteRegistry.clsRegistry

                If ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_local_machine" Then
                    oRemoteReg.SetValue(RemoteRegistry.HKEYs.eHKEY_LOCAL_MACHINE, sKey, ValueName, Value, gPCName)
                ElseIf ParentKey.ToString.ToLower.Split("[")(0).Trim = "hkey_current_user" Then
                    oRemoteReg.SetValue(RemoteRegistry.HKEYs.eHKEY_CURRENT_USER, sKey, ValueName, Value, gPCName)
                End If

                Return True
            Catch : Return False : End Try
        End If

    End Function
    Public Function ReadTextFile(ByVal sPath As String) As String
        On Error GoTo Trap
        Dim file As System.IO.StreamReader = _
        New System.IO.StreamReader(sPath)
        Dim sRead As String

        sRead = file.ReadToEnd

        file.Close()

        Return sRead
        Exit Function
Trap:
        Return ""
    End Function
    Public Sub DeleteEvent(ByVal nEventID As Integer)
        Dim SQL As String
        Dim sWhere As String = " WHERE EventID =" & nEventID
        Dim sName As String

        Try
            Dim sPath As String
            Dim oRs As ADODB.Recordset

            oRs = clsMarsData.GetData("SELECT EventName FROM EventAttr6 " & sWhere)

            If oRs.EOF = False Then
                sName = oRs.Fields(0).Value

                sName = sName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

                sPath = clsMarsEvent.m_CachedDataPath & sName

                System.IO.Directory.Delete(sPath, True)
            End If
        Catch : End Try

        SQL = "DELETE FROM EventAttr6 " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM EventSchedule " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ReportAttr WHERE PackID = " & nEventID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM EventConditions WHERE EventID =" & nEventID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID =" & nEventID, False)

        clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.DELETE)

    End Sub
    Public Sub DeleteReport(ByVal nReportID As Integer, Optional ByVal nPackID As Integer = 0)

        Dim sWhere As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer
        Dim sCache As String = ""
        Dim Dynamic As Boolean = False
        Dim Bursting As Boolean = False
        Dim sName As String

        If nPackID = 0 Then
            oRs = clsMarsData.GetData("SELECT * FROM TaskManager WHERE ReportID = " & nReportID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    Dim retryCancel As DialogResult

                    retryCancel = MessageBox.Show("The selected schedule cannot be deleted as it is in the middle is executing. Please wait and try again in a few minutes", _
                    Application.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation)

                    If retryCancel = DialogResult.Retry Then
                        DeleteReport(nReportID, nPackID)
                        Return
                    Else
                        Return
                    End If
                End If

                oRs.Close()
            End If
        End If

        oRs = clsMarsData.GetData("SELECT CachePath,Dynamic,Bursting, ReportName FROM ReportAttr WHERE ReportID =" & nReportID)

        Try
            sCache = IsNull(oRs.Fields(0).Value)

            Try
                Dynamic = Convert.ToBoolean(oRs(1).Value)
            Catch
                Dynamic = False
            End Try

            Try
                Bursting = Convert.ToBoolean(oRs(2).Value)
            Catch
                Bursting = False
            End Try

            sName = oRs(3).Value

            oRs.Close()
        Catch : End Try

        sWhere = " WHERE ReportID = " & nReportID '& " AND PackID = " & nPackID


        clsMarsData.WriteData("DELETE FROM ReportAttr " & sWhere, False)

        oRs = clsMarsData.GetData("SELECT ScheduleID FROM ScheduleAttr " & sWhere)

        If Not oRs Is Nothing Then
            Try
                nID = oRs.Fields(0).Value
            Catch
                nID = 0
            End Try
        End If

        oRs.Close()

        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID = " & nID, False)

        clsMarsData.WriteData("DELETE FROM ScheduleAttr " & sWhere, False)

        clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM ReportOptions WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM BlankReportAlert " & sWhere, False)

        clsMarsData.WriteData("DELETE FROM UserView WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM PackagedReportAttr WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID =" & nID, False)

        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID = " & nID, False)

        clsMarsData.WriteData("DELETE FROM SubReportParameters WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM SubReportLogin WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM CustomSections WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM RefresherAttr WHERE ObjectType = 'Report' AND ObjectID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM BurstAttr WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM ReportTable WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM SubReportTable WHERE ReportID = " & nReportID, False)

        clsMarsData.WriteData("DELETE FROM StampAttr WHERE ForeignID =" & nReportID)

        clsMarsData.WriteData("DELETE FROM DataDrivenAttr WHERE ReportID =" & nReportID, False)

        clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE ReportID =" & nReportID, False)

        DeleteDestination(nReportID, 0, 0)

        Try
            If sCache.IndexOf("{") > -1 And sCache.IndexOf("}") > -1 Then System.IO.File.Delete(sCache)
        Catch : End Try

        If nPackID > 0 Then
            clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.DELETE)
        ElseIf Dynamic = True Then
            clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.DELETE)
        ElseIf Bursting = True Then
            clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.DELETE)
        Else
            clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.DELETE)
        End If
    End Sub

    Public Sub DeleteDestination(ByVal reportID As Integer, ByVal packID As Integer, ByVal smartID As Integer)
        Dim SQL As String

        SQL = "SELECT DestinationID FROM DestinationAttr WHERE "

        If reportID > 0 Then
            SQL &= " ReportID =" & reportID
        ElseIf packID > 0 Then
            SQL &= " PackID =" & packID
        ElseIf smartID > 0 Then
            SQL &= " SmartID =" & smartID
        Else
            Return
        End If

        Dim destID As Integer = 0

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            destID = oRs("destinationid").Value

            Dim whereValue As String = " WHERE DestinationID =" & destID

            clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM HouseKeepingAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM PGPAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM PrinterAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM ReadReceiptsAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM ReadReceiptsLog" & whereValue)
            clsMarsData.WriteData("DELETE FROM ReportOptions" & whereValue)
            clsMarsData.WriteData("DELETE FROM DestinationAttr" & whereValue)
            clsMarsData.WriteData("DELETE FROM StampAttr WHERE ForeignID =" & destID)

            oRs.MoveNext()
        Loop

        oRs.Close()

    End Sub
    Public Sub DeletePackage(ByVal nPackID As Integer)
        Dim SQL As String
        Dim sWhere As String
        Dim oSchedule As New clsMarsScheduler
        Dim ScheduleID As Integer
        Dim sPackName As String
        Dim oRs As ADODB.Recordset

        oRs = clsMarsData.GetData("SELECT * FROM TaskManager WHERE packID = " & nPackID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Dim retryCancel As DialogResult

                retryCancel = MessageBox.Show("The selected package cannot be deleted as it is in the middle is executing. Please wait and try again in a few minutes", _
                Application.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation)

                If retryCancel = DialogResult.Retry Then
                    DeletePackage(nPackID)
                    Return
                Else
                    Return
                End If
            End If

            oRs.Close()
        End If

        oRs = clsMarsData.GetData("SELECT PackageName FROM PackageAttr WHERE PackID =" & nPackID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                sPackName = oRs(0).Value
            End If

            oRs.Close()
        End If

        ScheduleID = oSchedule.GetScheduleID(, nPackID)

        oRs = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE PackID = " & nPackID)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                DeleteReport(oRs("reportid").Value, nPackID)

                'clsmarsdata._writedata("DELETE FROM ReportParameter WHERE ReportID =" & oRs("reportid").Value)
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        sWhere = " WHERE PackID = " & nPackID

        SQL = "DELETE FROM ReportAttr " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM ScheduleAttr " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM BlankReportAlert " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM PackageAttr " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM UserView " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM PackagedReportAttr " & sWhere

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM Tasks WHERE ScheduleID =" & ScheduleID

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM RefresherAttr WHERE ObjectType = 'Package' AND ObjectID = " & nPackID

        clsMarsData.WriteData(SQL, False)

        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE PackID =" & nPackID, False)

        clsMarsData.WriteData("DELETE FROM DynamicLink WHERE PackID =" & nPackID, False)

        clsMarsData.WriteData("DELETE FROM StampAttr WHERE ForeignID =" & nPackID)

        clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE PackID =" & nPackID, False)

        DeleteDestination(0, nPackID, 0)

        clsMarsAudit._LogAudit(sPackName, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.DELETE)
    End Sub

    Public Sub AutoCompleteLeave(ByVal oCombo As ComboBox)
        Dim nFoundIndex As Integer
        nFoundIndex = oCombo.FindStringExact(oCombo.Text)
        oCombo.SelectedIndex = -1
        oCombo.SelectedIndex = nFoundIndex
    End Sub

    Public Sub AutoCompleteKeyUp(ByVal oCombo As ComboBox, ByVal e As KeyEventArgs)
        Dim sTyped As String
        Dim nFoundIndex As Integer
        Dim oFoundItem As Object
        Dim sFoundText As String
        Dim sAppendText As String

        Select Case e.KeyCode
            Case Keys.Back, Keys.Left, Keys.Right, Keys.Up, Keys.Delete, _
                Keys.Down, Keys.CapsLock
                Return
        End Select

        sTyped = oCombo.Text

        nFoundIndex = oCombo.FindString(sTyped)

        If nFoundIndex >= 0 Then
            oFoundItem = oCombo.Items(nFoundIndex)
            sFoundText = oCombo.GetItemText(oFoundItem)
            sAppendText = sFoundText.Substring(sTyped.Length)
            oCombo.Text = sTyped & sAppendText
            oCombo.Text = sTyped & sAppendText

            oCombo.SelectionStart = sTyped.Length
            oCombo.SelectionLength = sAppendText.Length
        End If
    End Sub

    <DebuggerStepThrough()> Public Shared Sub BusyProgress(Optional ByVal nValue As Integer = 0, Optional ByVal sMsg As String = "Ready", _
    Optional ByVal Finish As Boolean = False)

        On Error Resume Next

        If (Paused = True) Then Return

        Dim oProgress As ProgressBarItem = _
            oMain.DotNetBarManager1.GetItem("item_116")

        Dim oLabel As LabelItem = _
            oMain.DotNetBarManager1.GetItem("label105")

        Dim oCancel As ButtonItem = oMain.DotNetBarManager1.GetItem("item_264")

        If gVisible = True Then
            oProgress.Visible = True
            oCancel.Enabled = True

            oProgress.Value = nValue
            oLabel.Text = sMsg

            'oMain.StatusBar.progressBar.Visible = True
            'oMain.StatusBar.progressBar.Value = nValue
            'oMain.StatusBar.Panels(0).Text = sMsg

            oWindow(nWindowCurrent).IsBusy(True)

            Application.DoEvents()

            If Finish = True Then
                'oMain.StatusBar.progressBar.Value = 0
                'oMain.StatusBar.Panels(0).Text = "Ready"
                'oMain.StatusBar.progressBar.Visible = False

                oProgress.Value = 0
                oLabel.Text = "Ready"

                oCancel.Enabled = False

                oWindow(nWindowCurrent).IsBusy(False)
            End If
        End If


        If sqlrd.clsOutofProcessExecutions.currentID > 0 Then
            'Dim statusLog As String = sAppPath & "oopstatus.log"
            'clsMarsUI.MainUI.SaveRegistry(clsOutofProcessExecutions.currentID, sMsg & "|" & nValue, , statusLog)
            If sMsg.ToLower = "ready" Then Return

            sqlrd.clsOutofProcessExecutions.sendProgress(clsOutofProcessExecutions.currentID & "|" & sMsg & "|" & nValue)
        ElseIf RunEditor = False Then

            If sMsg.ToLower = "ready" Then Return

            Dim procID As Integer = Process.GetCurrentProcess.Id

            clsMarsData.IamAlive()

            If Finish = True Then
                sqlrd.clsOutofProcessExecutions.sendProgress("_", 2003)
            Else
                sqlrd.clsOutofProcessExecutions.sendProgress(clsMarsScheduler.m_progressID & "|" & sMsg & "|" & nValue & "|" & Process.GetCurrentProcess.Id, 2003)
            End If
        End If

    End Sub

    Public Sub SetViewStyle(ByVal oView As System.Windows.Forms.View)

        Dim oForm As frmWindow
        Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
        Dim nID As Integer
        Dim sType As String

        SaveRegistry("WindowViewStyle", oView.ToString)

        'oForm = oWindow(nWindowCurrent)

        For Each oForm In oWindow
            Try
                If oView = View.Details Then

                    If oForm.tvFolders.SelectedNode Is Nothing Then
                        nID = 0
                    Else
                        nID = GetDelimitedWord(oForm.tvFolders.SelectedNode.Tag, 2, ":")
                    End If

                    Try
                        sType = GetDelimitedWord(oForm.tvFolders.SelectedNode.Tag, 1, ":")
                    Catch
                        sType = "Desktop"
                    End Try

                    If sType.ToLower = "folder" Or sType.ToLower = "desktop" Then
                        BuildFolderDetails(nID, oForm)
                    ElseIf sType.ToLower = "smartfolder" Then
                        Dim oList As ListView = oForm.lsvWindow

                        With oList
                            .View = View.Details
                            .Items.Clear()
                            .Columns.Clear()

                            With .Columns
                                .Add("Name", 100, HorizontalAlignment.Left)
                                .Add("Frequency", 100, HorizontalAlignment.Left)
                                .Add("Next Run", 100, HorizontalAlignment.Left)
                                .Add("Owner", 100, HorizontalAlignment.Left)
                            End With
                        End With

                        OpenSmartFolder(nID, oForm.lsvWindow)

                    ElseIf sType.ToLower = "package" Then
                        BuildPackageDetails(nID, oForm)
                    End If

                Else
                    oForm.lsvWindow.View = oView

                    Me.RefreshView(oWindow(nWindowCurrent))
                End If
            Catch : End Try
        Next
    End Sub


    Public Function NewFolder(ByVal nParent As Integer) As Hashtable
        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            Return Nothing
        End If

        Dim sName As String = ""
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim FolderID As Int64 = clsMarsData.CreateDataID("folders", "folderid")
        Dim oRs As ADODB.Recordset

Retry:
        sName = InputBox("Please enter the new folder name", Application.ProductName, "Untitled Folder")

        If sName = "" Then Return Nothing

        SQL = "SELECT * FROM Folders WHERE FolderName ='" & SQLPrepare(sName) & "' AND Parent =" & nParent

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                If MessageBox.Show("A folder with the provided name already exists in this folder", _
                Application.ProductName, MessageBoxButtons.RetryCancel, MessageBoxIcon.Exclamation) = DialogResult.Retry Then
                    oRs.Close()
                    GoTo Retry
                Else
                    oRs.Close()
                    Return Nothing
                End If
            End If

            oRs.Close()
        Catch : End Try

        sCols = "FolderID,FolderName, Parent"

        sVals = FolderID & ",'" & SQLPrepare(sName) & "'," & _
            nParent

        SQL = "INSERT INTO Folders (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Dim oUser As New clsMarsUsers

        oUser.AssignView(FolderID, gUser, clsMarsUsers.enViewType.ViewFolder)

        clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.FOLDER, clsMarsAudit.AuditAction.CREATE)

        Dim result As New Hashtable

        result.Add("Name", sName)
        result.Add("Tag", "Folder:" & FolderID)

        Return result
    End Function

    Public Function MoveFolder(ByVal nFolderID As Integer) As Boolean
        Dim SQL As String
        Dim sParent() As String
        Dim nParent As Integer
        Dim oForm As frmFolders = New frmFolders

        oForm.SelectDeskTop = True

        sParent = oForm.GetFolder

        If sParent Is Nothing Then Exit Function

        nParent = sParent(1)

        If nFolderID = nParent Then
            MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        ElseIf IsMoveValid(nFolderID, nParent) = False Then
            MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If

        SQL = "UPDATE Folders SET Parent = " & nParent & " WHERE FolderID = " & nFolderID

        Return clsMarsData.WriteData(SQL)

    End Function
    Public Function IsMoveValid(ByVal folderID As Integer, ByVal parentID As Integer) As Boolean
        Try

            Dim SQL As String = "SELECT FolderID FROM Folders WHERE Parent=" & folderID
            Dim value As Boolean = True

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return True

            Do While oRs.EOF = False
                Dim nID As Integer = oRs("folderid").Value

                If nID = parentID Then
                    value = False
                    Exit Do
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()

            oRs = Nothing

            Return value
        Catch ex As Exception
            Return True
        End Try
    End Function
    Public Function DeleteFolder(ByVal nFolderID As Integer) As Boolean
        If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
            Return Nothing
        End If


        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT FolderID FROM Folders WHERE Parent = " & nFolderID)
        Dim sName As String = ""

        If oRs Is Nothing Then Exit Function

        If oRs.EOF = False Then
            MessageBox.Show("You cannot delete this folder as it has subfolders. Please delete the subfolders and try again.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            oRs.Close()
            Return Nothing

        End If

        oRs.Close()

        oRs = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID =" & nFolderID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                sName = oRs(0).Value
            End If

            oRs.Close()
        End If

        'delete any packages in this folder
        oRs = clsMarsData.GetData("SELECT PackID FROM PackageAttr WHERE Parent = " & nFolderID)

        Do While oRs.EOF = False
            DeletePackage(oRs("packid").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()

        'delete any reports in this folder
        oRs = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE Parent = " & nFolderID)

        Do While oRs.EOF = False
            DeleteReport(oRs("reportid").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()

        'delete any automation schedules in this folder
        oRs = clsMarsData.GetData("SELECT AutoID FROM AutomationAttr WHERE Parent =" & nFolderID)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                Me.DeleteAutoSchedule(oRs("autoid").Value)
                oRs.MoveNext()
            Loop
        End If

        oRs.Close()

        oRs = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE Parent =" & nFolderID)

        If oRs IsNot Nothing Then

            Do While oRs.EOF = False
                clsMarsEvent.DeleteEvent(oRs("eventid").Value)

                oRs.MoveNext()
            Loop
            oRs.Close()
        End If


        oRs = clsMarsData.GetData("SELECT EventPackID FROM EventPackageAttr WHERE Parent =" & nFolderID)

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                clsMarsEvent.DeleteEventPackage(oRs("eventpackid").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        clsMarsData.WriteData("DELETE FROM RefresherAttr WHERE ObjectType = 'Folder' AND ObjectID = " & nFolderID)

        clsMarsData.WriteData("DELETE FROM Folders WHERE FolderID = " & nFolderID)

        clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.FOLDER, clsMarsAudit.AuditAction.DELETE)

        Return True
    End Function

    Public Sub RefreshView(ByVal oForm As frmWindow)
        Try
            Dim sCurrent As String
            Dim Sel As String
            Dim sExpanded() As String
            Dim I As Integer = 0

            For Each onode As TreeNode In oForm.tvFolders.Nodes(0).Nodes
                If onode.IsExpanded = True Then
                    ReDim Preserve sExpanded(I)

                    sExpanded(I) = onode.Tag
                    I += 1
                End If
            Next

            If oForm.lsvWindow.SelectedItems.Count > 0 Then
                Sel = oForm.lsvWindow.SelectedItems(0).Tag
            End If

            If oForm.tvFolders.SelectedNode Is Nothing Then
                sCurrent = "Desktop:0"
            ElseIf oForm.tvFolders.SelectedNode.Tag.IndexOf("Desktop") > -1 Then
                sCurrent = "Desktop:0"
            Else
                sCurrent = oForm.tvFolders.SelectedNode.Tag
            End If

            oForm.tvFolders.BeginUpdate()
            BuildTree(oForm.tvFolders, , False)
            oForm.tvFolders.EndUpdate()

            oForm.tvSmartFolders.BeginUpdate()
            AddSmartFolders(oForm.tvSmartFolders, True)
            oForm.tvSmartFolders.EndUpdate()


            If sCurrent = "Desktop" Then
                oForm.tvFolders.SelectedNode = oForm.tvFolders.Nodes(0)
            Else
                FindNode(sCurrent, oForm.tvFolders, oForm.tvFolders.Nodes(0))
            End If

            Try
                ReDim oForm.gItemList(oForm.lsvWindow.Items.Count - 1)

                For I = 0 To oForm.lsvWindow.Items.Count - 1
                    oForm.gItemList(I) = oForm.lsvWindow.Items(I)
                Next

                'oForm.RestoreColumnData()
            Catch : End Try

            Try
                For Each oItem As ListViewItem In oForm.lsvWindow.Items
                    If oItem.Tag = Sel Then
                        oItem.Selected = True
                        Exit For
                    End If
                Next
            Catch : End Try

            Try
                For Each s As String In sExpanded
                    For Each onode As TreeNode In oForm.tvFolders.Nodes(0).Nodes
                        If onode.Tag = s Then
                            onode.Expand()
                        End If
                    Next
                Next
            Catch : End Try
        Catch : End Try
    End Sub
    Public Function GetLastRun(Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0, _
    Optional ByVal nAutoID As Integer = 0, Optional ByVal nEventPackID As Integer = 0) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValue As String
        Dim sWhere As String

        If nReportID > 0 Then
            sWhere = " WHERE ReportID = " & nReportID
        ElseIf nPackID > 0 Then
            sWhere = " WHERE PackID = " & nPackID
        ElseIf nAutoID > 0 Then
            sWhere = " WHERE AutoID = " & nAutoID
        ElseIf nEventPackID > 0 Then
            sWhere = " WHERE EventPackID = " & nEventPackID
        End If

        SQL = "SELECT MAX(EntryDate) FROM ScheduleHistory" & sWhere

        Try
            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                sValue = CTimeZ(oRs.Fields(0).Value, dateConvertType.READ)
            Else
                sValue = ""
            End If

            oRs.Close()

            If sValue.Length > 0 Then
                SQL = "SELECT StartDate, EntryDate FROM ScheduleHistory WHERE " & _
                "EntryDate = (SELECT MAX (EntryDate) FROM ScheduleHistory" & sWhere & ")"

                Dim StartDate As Date
                Dim EntryDate As Date

                oRs = clsMarsData.GetData(SQL)

                StartDate = CTimeZ(oRs.Fields(0).Value, dateConvertType.READ)
                EntryDate = CTimeZ(oRs.Fields(1).Value, dateConvertType.READ)

                sValue &= " (Duration: " & DateDiff(DateInterval.Second, StartDate, EntryDate) & " secs)"

                oRs.Close()
            End If

            Return sValue
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function GetEventLastRun(ByVal nEventID As Integer) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValue As String

        SQL = "SELECT MAX(LastFired) FROM EventHistory WHERE EventID =" & nEventID

        Try
            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                sValue = CTimeZ(oRs.Fields(0).Value, dateConvertType.READ)
            Else
                sValue = ""
            End If

            oRs.Close()

            If sValue.Length > 0 Then
                SQL = "SELECT StartDate, LastFired FROM EventHistory WHERE " & _
                "HistoryID = (SELECT MAX (HistoryID) FROM EventHistory WHERE EventID =" & nEventID & ")"

                Dim StartDate As Date
                Dim EntryDate As Date
                Dim duration As Integer = 0

                oRs = clsMarsData.GetData(SQL)

                StartDate = CTimeZ(IsNull(oRs.Fields(0).Value, Now), dateConvertType.READ)
                EntryDate = CTimeZ(IsNull(oRs.Fields(1).Value, Now), dateConvertType.READ)

                duration = DateDiff(DateInterval.Second, StartDate, EntryDate)


                sValue &= " (Duration: " & IIf(duration < 0, 0, duration) & " secs)"

                oRs.Close()
            End If

            Return sValue

        Catch
            Return ""
        End Try
    End Function
    Public Function GetEventLastResult(ByVal nEventID As Integer) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValue As String

        SQL = "SELECT Status,ErrMsg FROM EventHistory WHERE EventID =" & nEventID & " ORDER BY LastFired DESC"

        Try
            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                If oRs.Fields(0).Value = 1 Then
                    sValue = "Success"
                Else
                    sValue = "Failed: " & oRs("errmsg").Value
                End If
            Else
                sValue = ""
            End If

            oRs.Close()

            Return sValue
        Catch ex As Exception
            Return ""
        End Try
    End Function
    Public Function GetLastResult(Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0, _
    Optional ByVal nAutoID As Integer = 0, Optional ByVal nEventPackID As Integer = 0) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValue As String
        Dim sErrMsg As String
        Dim fSuccess As Integer
        Dim nHistoryId As Integer

        If nReportID > 0 Then
            SQL = "SELECT HistoryID,Success,ErrMsg FROM ScheduleHistory WHERE ReportID = " & nReportID & " ORDER BY EntryDate DESC"
        ElseIf nPackID > 0 Then
            SQL = "SELECT HistoryID,Success,ErrMsg FROM ScheduleHistory WHERE PackID = " & nPackID & " ORDER BY EntryDate DESC"
        ElseIf nAutoID > 0 Then
            SQL = "SELECT HistoryID,Success,ErrMsg FROM ScheduleHistory WHERE AutoID = " & nAutoID & " ORDER BY EntryDate DESC"
        ElseIf nEventPackID > 0 Then
            SQL = "SELECT HistoryID,Success,ErrMsg FROM ScheduleHistory WHERE EventPackID = " & nEventPackID & " ORDER BY EntryDate DESC"
        End If

        Try
            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                fSuccess = oRs.Fields("success").Value
                sErrMsg = oRs("errmsg").Value
                nHistoryId = oRs("historyid").Value

                oRs.Close()

                If fSuccess = 1 Then
                    sValue = "Success"

                    SQL = "SELECT Success FROM HistoryDetailAttr WHERE HistoryID = " & nHistoryId
                    oRs = clsMarsData.GetData(SQL)

                    If oRs.EOF = False Then
                        Do While oRs.EOF = False
                            If oRs.Fields("success").Value = 0 Then
                                sValue = "Partial Success"
                                Exit Do
                            End If

                            oRs.MoveNext()
                        Loop
                    End If

                    oRs.Close()
                Else
                    sValue = "Failed: " & sErrMsg
                End If

            Else
                sValue = ""
                oRs.Close()
            End If

            Return sValue

        Catch ex As Exception
            Return ""
        End Try

    End Function
    Public Function GetDestinationValues(ByVal sColumn As String, Optional ByVal nReportID As Integer = 0, Optional ByVal nPackID As Integer = 0) As String
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sValue As String = ""

        If nReportID > 0 And nPackID = 0 Then
            SQL = "SELECT " & sColumn & " FROM DestinationAttr WHERE ReportID =" & nReportID
        ElseIf nPackID > 0 Then
            If sColumn.ToLower = "outputformat" Then
                SQL = "SELECT OutputFormat FROM PackagedReportAttr WHERE ReportID =" & nReportID
            Else
                SQL = "SELECT " & sColumn & " FROM DestinationAttr WHERE PackID =" & nPackID
            End If
        End If

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            sValue &= oRs.Fields(0).Value & ","
            oRs.MoveNext()
        Loop

        oRs.Close()

        Try
            sValue = sValue.Substring(0, sValue.Length - 1)
        Catch
            sValue = ""
        End Try

        Return sValue
    End Function

    Private Function LoadFolderSettings() As Hashtable

        Try
            Dim fs As FileStream = New FileStream(sAppPath & "columns.dat", FileMode.Open, FileAccess.Read)

            Dim columns As Hashtable = New Hashtable
            Dim bf As BinaryFormatter = New BinaryFormatter

            columns = CType(bf.Deserialize(fs), Hashtable)

            Return columns
        Catch
            Return Nothing
        End Try
    End Function
    Public Sub BuildFolderDetails(ByVal nParent As Integer, ByVal oForm As frmWindow, _
    Optional ByVal SingleSQL As String = "", Optional ByVal PackageSQL As String = "", _
    Optional ByVal AutoSQL As String = "", Optional ByVal EventSQL As String = "", _
    Optional ByVal EventPackSQL As String = "", _
    Optional ByVal SmartFolder As Boolean = False, Optional ByVal SysFolder As Boolean = False)

        Dim sWhere As String = " WHERE Parent = " & nParent
        Dim SQL As String
        'Dim oForm As frmWindow
        Dim oList As ListView
        Dim lsv As ListViewItem
        Dim sNotAdmin As String
        Dim oRs As ADODB.Recordset
        Dim sDataCols() As String
        Dim I As Integer = 0
        Dim UserColumns As Boolean
        Dim sOwner As String
        Dim IsRpt As Boolean
        Dim columns As Hashtable = LoadFolderSettings()

        If columns Is Nothing Then columns = New Hashtable

        'set up the listview
        oForm.IsLoaded = False

        oList = oForm.lsvWindow

        SQL = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"

        oRs = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()

                With .Columns
                    .Add("Name", 100, HorizontalAlignment.Left)
                    .Add("Owner", 100, HorizontalAlignment.Left)
                End With

                If columns.ContainsKey("Name") Then .Columns(0).Width = columns.Item("Name")
                If columns.ContainsKey("Owner") Then .Columns(1).Width = columns.Item("Owner")

            End With

            UserColumns = False
        Else
            UserColumns = True

            ReDim sDataCols(0)

            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()
                .Columns.Add("Name", 100, HorizontalAlignment.Left)

                If columns.ContainsKey("Name") Then .Columns(0).Width = columns.Item("Name")

                Do While oRs.EOF = False
                    Dim oCol As ColumnHeader = New ColumnHeader
                    ReDim Preserve sDataCols(I)

                    sDataCols(I) = oRs("datacolumn").Value

                    oCol.Text = oRs("caption").Value
                    oCol.TextAlign = HorizontalAlignment.Left

                    If columns.ContainsKey(oCol.Text) Then
                        oCol.Width = columns.Item(oCol.Text)
                    Else
                        oCol.Width = 100
                    End If

                    With .Columns
                        .Add(oCol)
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                .Columns.Add("Owner", 100, HorizontalAlignment.Left)
                oRs.Close()
            End With
        End If
        '_____________________________________________________________________________________________________

        'then add folders to the mix if its not a smart folder
        If SmartFolder = False And SysFolder = False Then
            SQL = "SELECT foldername, folderid FROM folders " & sWhere

            If gConType = "DAT" Then
                SQL &= " ORDER BY FolderName"
            Else
                SQL &= " ORDER BY CAST(FolderName AS VARCHAR(50))"
            End If

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    lsv = oList.Items.Add(oRs("foldername").Value)
                    lsv.Tag = "Folder:" & oRs("folderid").Value
                    lsv.ImageIndex = 0
                    lsv.Group = oList.Groups("Folders")
                    oRs.MoveNext()
                Loop
            End If

            oRs.Close()
        End If

        '___________________________________________________________________________________________________________________

        'then add packages
        If PackageSQL.Length = 0 And SysFolder = False Then
            SQL = "SELECT * " & _
            "FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
            "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & _
            "PackageAttr.Parent =" & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND PackageAttr.PackID IN (SELECT PackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If
        Else
            SQL = PackageSQL
        End If

        ''console.writeLine(SQL)

        If gConType = "DAT" Then
            SQL &= " ORDER BY PackageName"
        Else
            SQL &= " ORDER BY CAST(PackageName AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("packagename").Value)
                lsv.Tag = "Package:" & oRs(0).Value

                Try
                    IsRpt = Convert.ToBoolean(oRs("repeat").Value)
                Catch ex As Exception
                    IsRpt = False
                End Try

                sOwner = IsNull(oRs("owner").Value, "sys")

                If clsMarsData.IsScheduleDynamic(oRs(0).Value, "Package") = True Then
                    If oRs("status").Value = 1 Then
                        lsv.ImageIndex = 13
                    Else
                        lsv.ImageIndex = 14
                    End If
                    lsv.Group = oList.Groups("Dynamic Package Schedules")
                ElseIf clsMarsData.IsScheduleDataDriven(oRs(0).Value, "Package") = True Then
                    If oRs("status").Value = 1 Then
                        lsv.ImageIndex = 19
                    Else
                        lsv.ImageIndex = 20
                    End If
                    lsv.Group = oList.Groups("Data-Driven Packages")
                Else
                    If oRs("status").Value = 1 Then
                        lsv.ImageIndex = 2
                    Else
                        lsv.ImageIndex = 4
                    End If
                    lsv.Group = oList.Groups("Package Schedules")
                End If

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    Try
                                        If ConDate(oRs(s).Value) > ConDate(oRs("enddate").Value) Then
                                            lsv.SubItems.Add("Ended")
                                        ElseIf oRs("status").Value = 1 Then
                                            Dim val As Date = CTimeZ(IsNull(oRs(s).Value), dateConvertType.READ)
                                            lsv.SubItems.Add(CType(val, String))
                                        Else
                                            lsv.SubItems.Add("Disabled")
                                        End If
                                    Catch
                                        lsv.SubItems.Add("")
                                    End Try
                                Case "startdate", "enddate"
                                    Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ).Date
                                    lsv.SubItems.Add(CType(val, String))
                                Case "databasepath", "outputformat", "lastrefreshed"
                                    lsv.SubItems.Add("Various")
                                Case "destinationtype"
                                    lsv.SubItems.Add(GetDestinationValues("destinationtype", , IsNull(oRs(0).Value)))
                                Case "entrydate"
                                    lsv.SubItems.Add(GetLastRun(, oRs(0).Value))
                                Case "success"
                                    lsv.SubItems.Add(GetLastResult(, oRs(0).Value))
                                Case "repeatinterval", "repeatuntil"
                                    If s.ToLower = "repeatuntil" Then
                                        Dim val As Date = CTimeZ(IsNull(oRs(s).Value, Now), dateConvertType.READ)
                                        s = ConTime(val)
                                    Else
                                        s = IsNull(oRs(s).Value, 0)

                                        s &= " " & IsNull(oRs("repeatunit").Value, "hours")
                                    End If

                                    If IsRpt = True Then
                                        lsv.SubItems.Add(s)
                                    Else
                                        lsv.SubItems.Add("-")
                                    End If
                                Case "starttime"
                                    Dim val As Date = CTimeZ(ConDate(Now) & " " & IsNull(oRs(s).Value, ConTime(Now)), dateConvertType.READ)
                                    lsv.SubItems.Add(ConTime(val))
                                Case Else
                                    lsv.SubItems.Add(IsNull(oRs(s).Value))
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If IsNull(oRs("status").Value) = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop
            oRs.Close()
        Catch : End Try

        '___________________________________________________________________________________________________________________

        'then add single reports
        If SingleSQL.Length = 0 And SysFolder = False Then
            SQL = "SELECT * " & _
            "FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
            "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
            "ReportAttr.Parent =" & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND ReportAttr.ReportID IN (SELECT ReportID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If
        Else
            SQL = SingleSQL
        End If

        If gConType = "DAT" Then
            SQL &= " ORDER BY ReportTitle"
        Else
            SQL &= " ORDER BY CAST(ReportTitle AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        ''console.writeLine(SQL)

        Try
            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("reporttitle").Value)
                lsv.Tag = "Report:" & oRs(0).Value
                sOwner = IsNull(oRs("owner").Value, "sys")

                Dim nDynamic As Integer
                Dim nBursting As Integer
                Dim nDataDriven As Integer

                Try
                    nBursting = IsNull(oRs("bursting").Value, 0)
                Catch ex As Exception
                    nBursting = 0
                End Try

                Try
                    IsRpt = Convert.ToBoolean(oRs("repeat").Value)
                Catch ex As Exception
                    IsRpt = False
                End Try

                Try
                    nDynamic = IsNull(oRs("dynamic").Value)
                Catch ex As Exception
                    nDynamic = 0
                End Try

                Try
                    nDataDriven = IsNull(oRs("isdatadriven").Value, 0)
                Catch ex As Exception
                    nDataDriven = 0
                End Try

                If oRs("status").Value = 1 Then
                    If nDynamic = 1 Then
                        lsv.ImageIndex = 7
                        lsv.Group = oList.Groups("Dynamic Schedules")
                    ElseIf nBursting = 1 Then
                        lsv.ImageIndex = 11
                        lsv.Group = oList.Groups("Bursting Schedules")
                    ElseIf nDataDriven = 1 Then
                        lsv.ImageIndex = 17
                        lsv.Group = oList.Groups("Data-Driven Schedules")
                    Else
                        lsv.ImageIndex = 1
                        lsv.Group = oList.Groups("Single Schedules")
                    End If
                Else
                    If nDynamic = 1 Then
                        lsv.ImageIndex = 8
                        lsv.Group = oList.Groups("Dynamic Schedules")
                    ElseIf nBursting = 1 Then
                        lsv.ImageIndex = 12
                        lsv.Group = oList.Groups("Bursting Schedules")
                    ElseIf nDataDriven = 1 Then
                        lsv.ImageIndex = 18
                        lsv.Group = oList.Groups("Data-Driven Schedules")
                    Else
                        lsv.ImageIndex = 3
                        lsv.Group = oList.Groups("Single Schedules")
                    End If
                End If

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    Try
                                        If ConDate(oRs(s).Value) > ConDate(oRs("enddate").Value) Then
                                            lsv.SubItems.Add("Ended")
                                        ElseIf IsNull(oRs("status").Value) = 1 Then
                                            Dim val As Date = CTimeZ(IsNull(oRs(s).Value), dateConvertType.READ)
                                            lsv.SubItems.Add(CType(val, String))
                                        Else
                                            lsv.SubItems.Add("Disabled")
                                        End If
                                    Catch
                                        lsv.SubItems.Add("")
                                    End Try
                                Case "startdate", "enddate", "lastrefreshed"
                                    Dim val As Date = CTimeZ(IsNull(oRs(s).Value), dateConvertType.READ)
                                    lsv.SubItems.Add(CType(val, String))
                                Case "outputformat", "destinationtype"
                                    lsv.SubItems.Add(GetDestinationValues(s, IsNull(oRs(0).Value)))
                                Case "entrydate"
                                    lsv.SubItems.Add(GetLastRun(oRs(0).Value))
                                Case "success"
                                    lsv.SubItems.Add(GetLastResult(oRs(0).Value))
                                Case "repeatinterval", "repeatuntil"
                                    If s.ToLower = "repeatuntil" Then
                                        Dim val As Date = CTimeZ(IsNull(oRs(s).Value, Now), dateConvertType.READ)
                                        s = ConTime(val)
                                    Else
                                        s = oRs(s).Value

                                        s &= " " & IsNull(oRs("repeatunit").Value, "hours")
                                    End If

                                    If IsRpt = True Then
                                        lsv.SubItems.Add(s)
                                    Else
                                        lsv.SubItems.Add("-")
                                    End If
                                Case "starttime"
                                    Dim val As Date = CTimeZ(ConDate(Now) & " " & IsNull(oRs(s).Value, ConTime(Now)), dateConvertType.READ)
                                    lsv.SubItems.Add(ConTime(val))
                                Case Else
                                    lsv.SubItems.Add(IsNull(oRs(s).Value))
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop
            oRs.Close()
        Catch : End Try

        '___________________________________________________________________________________________________

        'then also add automation schedules to the mix

        If AutoSQL.Length = 0 And SysFolder = False Then
            SQL = "SELECT * FROM " & _
            "AutomationAttr INNER JOIN ScheduleAttr ON " & _
            "AutomationAttr.AutoID = ScheduleAttr.AutoID WHERE " & _
            "AutomationAttr.Parent = " & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND AutomationAttr.AutoID IN (SELECT AutoID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If
        Else
            SQL = AutoSQL
        End If

        If gConType = "DAT" Then
            SQL &= " ORDER BY AutoName"
        Else
            SQL &= " ORDER BY CAST(AutoName AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("autoname").Value)
                lsv.Tag = "Automation:" & oRs(0).Value
                lsv.Group = oList.Groups("Automation Schedules")
                sOwner = IsNull(oRs("owner").Value, "sys")

                If oRs("status").Value = 1 Then
                    lsv.ImageIndex = 5
                Else
                    lsv.ImageIndex = 6
                End If

                Try
                    IsRpt = Convert.ToBoolean(oRs("repeat").Value)
                Catch ex As Exception
                    IsRpt = False
                End Try

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    Try
                                        If ConDate(oRs(s).Value) > ConDate(oRs("enddate").Value) Then
                                            lsv.SubItems.Add("Ended")
                                        ElseIf oRs("status").Value = 1 Then
                                            Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ)
                                            lsv.SubItems.Add(CType(val, String))
                                        Else
                                            lsv.SubItems.Add("Disabled")
                                        End If
                                    Catch
                                        lsv.SubItems.Add("")
                                    End Try
                                Case "startdate", "enddate"
                                    Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ).Date
                                    lsv.SubItems.Add(CType(val, String))
                                Case "repeatinterval", "repeatuntil"

                                    If s.ToLower = "repeatuntil" Then
                                        Dim val As Date = CTimeZ(IsNull(oRs(s).Value, Now), dateConvertType.READ)
                                        s = ConTime(val)
                                    Else
                                        s = oRs(s).Value

                                        s &= " " & IsNull(oRs("repeatunit").Value, "hours")
                                    End If

                                    If IsRpt = True Then
                                        lsv.SubItems.Add(s)
                                    Else
                                        lsv.SubItems.Add("-")
                                    End If
                                Case "outputformat", "destinationtype", "databasepath", "lastrefreshed"
                                    lsv.SubItems.Add("N/A")
                                Case "entrydate"
                                    lsv.SubItems.Add(GetLastRun(, , oRs(0).Value))
                                Case "success"
                                    lsv.SubItems.Add(GetLastResult(, , oRs(0).Value))
                                Case "starttime"
                                    Dim val As Date = CTimeZ(ConDate(Now) & " " & IsNull(oRs(s).Value, ConTime(Now)), dateConvertType.READ)
                                    lsv.SubItems.Add(ConTime(val))
                                Case Else
                                    lsv.SubItems.Add(oRs(s).Value)
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            ''console.writeLine(ex.Message)
        End Try

        '___________________________________________________________________________________________________________

        'also add event based schedules

        If EventSQL.Length = 0 And SysFolder = False Then
            SQL = "SELECT * FROM EventAttr6 WHERE Parent = " & nParent & " AND (PackID IS NULL OR PackID = 0)"

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventAttr6.EventID IN (SELECT EventID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If
        Else
            SQL = EventSQL
        End If

        If gConType = "DAT" Then
            SQL &= " ORDER BY EventName"
        Else
            SQL &= " ORDER BY CAST(EventName AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try

            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("eventname").Value)
                lsv.Tag = "Event:" & oRs("eventid").Value
                lsv.Group = oList.Groups("Event-Based Schedules")
                sOwner = IsNull(oRs("owner").Value, "sys")

                If oRs("status").Value = 1 Then
                    lsv.ImageIndex = 9
                Else
                    lsv.ImageIndex = 10
                End If

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    If oRs("status").Value = 1 Then
                                        lsv.SubItems.Add("On-Event")
                                    Else
                                        lsv.SubItems.Add("Disabled")
                                    End If
                                Case "description"
                                    lsv.SubItems.Add(oRs(s).Value)
                                Case "entrydate"
                                    lsv.SubItems.Add(Me.GetEventLastRun(oRs("eventid").Value))
                                Case "success"
                                    lsv.SubItems.Add(Me.GetEventLastResult(oRs("eventid").Value))
                                Case Else
                                    lsv.SubItems.Add("Event-Based")
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try

        'then add event based packages____________________________________________________________________________________

        If EventPackSQL.Length = 0 And SysFolder = False Then
            SQL = "SELECT * FROM EventPackageAttr INNER " & _
           "JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID " & _
           "WHERE Parent = " & nParent

            If gRole.ToLower <> "administrator" Then
                sNotAdmin = " AND EventPackageAttr.EventPackID IN (SELECT EventPackID FROM " & _
                "UserView WHERE UserID = '" & gUser & "')"

                SQL &= sNotAdmin
            End If
        Else
            SQL = EventPackSQL
        End If


        If gConType = "DAT" Then
            SQL &= " ORDER BY PackageName"
        Else
            SQL &= " ORDER BY CAST(PackageName AS VARCHAR(50))"
        End If

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("packagename").Value)
                lsv.Tag = "Event-Package:" & oRs(0).Value
                lsv.Group = oList.Groups("Event-Based Packages")
                sOwner = IsNull(oRs("owner").Value, "sys")

                If oRs("status").Value = 1 Then
                    lsv.ImageIndex = 15
                Else
                    lsv.ImageIndex = 16
                End If

                Try
                    IsRpt = Convert.ToBoolean(oRs("repeat").Value)
                Catch ex As Exception
                    IsRpt = False
                End Try

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    Try
                                        If ConDate(oRs(s).Value) > ConDate(oRs("enddate").Value) Then
                                            lsv.SubItems.Add("Ended")
                                        ElseIf oRs("status").Value = 1 Then
                                            Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ)
                                            lsv.SubItems.Add(CType(val, String))
                                        Else
                                            lsv.SubItems.Add("Disabled")
                                        End If
                                    Catch
                                        lsv.SubItems.Add("")
                                    End Try
                                Case "startdate", "enddate"
                                    Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ)
                                    lsv.SubItems.Add(CType(val.Date, String))
                                Case "repeatinterval", "repeatuntil"

                                    If s.ToLower = "repeatuntil" Then
                                        Dim val As Date = CTimeZ(IsNull(oRs(s).Value, Now), dateConvertType.READ)
                                        s = ConTime(val)
                                    Else
                                        s = oRs(s).Value

                                        s &= " " & IsNull(oRs("repeatunit").Value, "hours")
                                    End If

                                    If IsRpt = True Then
                                        lsv.SubItems.Add(s)
                                    Else
                                        lsv.SubItems.Add("-")
                                    End If
                                Case "outputformat", "destinationtype", "databasepath", "lastrefreshed"
                                    lsv.SubItems.Add("N/A")
                                Case "entrydate"
                                    lsv.SubItems.Add(GetLastRun(, , , oRs(0).Value))
                                Case "success"
                                    lsv.SubItems.Add(GetLastResult(, , , oRs(0).Value))
                                Case "starttime"
                                    Dim val As Date = CTimeZ(ConDate(Now) & " " & IsNull(oRs(s).Value, ConTime(Now)), dateConvertType.READ)
                                    lsv.SubItems.Add(ConTime(val))
                                Case Else
                                    lsv.SubItems.Add(oRs(s).Value)
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try


        BusyProgress(, , True)

        oForm.IsLoaded = True
    End Sub

    Public Sub BuildPackageDetails(ByVal nPackID As Integer, ByVal oForm As frmWindow)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sWhere As String = " WHERE PackID = " & nPackID
        Dim oList As ListView
        Dim lsv As ListViewItem
        Dim UserColumns As Boolean = False
        Dim sDataCols() As String
        Dim I As Integer
        Dim sOwner As String
        Dim IsRpt As Boolean

        oForm.IsLoaded = False

        oList = oForm.lsvWindow

        SQL = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"

        oRs = clsMarsData.GetData(SQL)

        If oRs.EOF = True Then
            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()

                With .Columns
                    .Add("Name", 100, HorizontalAlignment.Left)
                    .Add("Owner", 100, HorizontalAlignment.Left)
                End With

            End With

            UserColumns = False
        Else
            UserColumns = True

            ReDim sDataCols(0)

            With oList
                .View = View.Details
                .Items.Clear()
                .Columns.Clear()

                .Columns.Add("Name", 100, HorizontalAlignment.Left)

                Do While oRs.EOF = False
                    Dim oCol As ColumnHeader = New ColumnHeader
                    ReDim Preserve sDataCols(I)

                    sDataCols(I) = oRs("datacolumn").Value

                    oCol.Text = oRs("caption").Value
                    oCol.TextAlign = HorizontalAlignment.Left
                    oCol.Width = 100


                    With .Columns
                        .Add(oCol)
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                .Columns.Add("Owner", 100, HorizontalAlignment.Left)
                oRs.Close()
            End With
        End If

        SQL = "SELECT * " & _
            "FROM (PackageAttr INNER JOIN ReportAttr ON " & _
            "PackageAttr.PackID = ReportAttr.PackID) INNER JOIN " & _
            "ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID " & _
            "WHERE PackageAttr.PackID=" & nPackID & " ORDER BY PackOrderID"

        oRs = clsMarsData.GetData(SQL)

        Try
            Dim nIDIndex As Integer
            Dim nOwnerIndex As Integer

            If oRs.EOF = False Then
                For x As Integer = 0 To oRs.Fields.Count - 1
                    If oRs(x).Name.ToLower.IndexOf("reportid") > -1 Then
                        nIDIndex = x
                        Exit For
                    End If
                Next

                For x As Integer = 0 To oRs.Fields.Count - 1
                    If oRs(x).Name.ToLower.IndexOf("owner") > -1 Then
                        nOwnerIndex = x
                        Exit For
                    End If
                Next
            End If

            Do While oRs.EOF = False
                lsv = oList.Items.Add(oRs("reporttitle").Value)
                lsv.Tag = "Packed Report:" & oRs(nIDIndex).Value
                lsv.Group = oList.Groups("Packaged Reports")

                Try
                    IsRpt = Convert.ToBoolean(oRs("repeat").Value)
                Catch ex As Exception
                    IsRpt = False
                End Try

                Dim rsP As ADODB.Recordset = clsMarsData.GetData("SELECT Owner FROM PackageAttr WHERE PackID =" & nPackID)

                If rsP IsNot Nothing Then
                    If rsP.EOF = False Then
                        sOwner = rsP(0).Value
                    End If

                    rsP.Close()
                Else
                    sOwner = CType(IsNull(oRs(nOwnerIndex).Value, "sys"), String)
                End If


                Try
                    Dim oStatus As ADODB.Recordset = clsMarsData.GetData("SELECT Status FROM PackagedReportAttr WHERE ReportID =" & oRs(nIDIndex).Value)

                    If Not oStatus Is Nothing Then
                        If oStatus.EOF = False Then
                            If oStatus(0).Value = 1 Then
                                lsv.ImageIndex = 1
                            Else
                                lsv.ImageIndex = 3
                                lsv.ForeColor = System.Drawing.Color.Gray
                            End If
                        End If

                        oStatus.Close()
                    End If
                Catch : End Try

                If oRs("status").Value = 0 Then
                    lsv.ImageIndex = 3
                Else
                    lsv.ImageIndex = 1
                End If

                If UserColumns = True Then
                    For Each s As String In sDataCols
                        If s.Length > 0 Then
                            Select Case s.ToLower
                                Case "nextrun"
                                    If oRs("status").Value = 1 Then
                                        Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ)
                                        lsv.SubItems.Add(CType(val, String))
                                    Else
                                        lsv.SubItems.Add("Disabled")
                                    End If
                                Case "startdate", "enddate", "lastrefreshed"
                                    Try
                                        Dim val As Date = CTimeZ(IsNull(oRs(s).Value.Date, ""), dateConvertType.READ)

                                        lsv.SubItems.Add(CType(val, String))
                                    Catch
                                        lsv.SubItems.Add("")
                                    End Try
                                Case "outputformat", "destinationtype"
                                    lsv.SubItems.Add(GetDestinationValues(s, oRs(nIDIndex).Value, _
                                    oRs(0).Value))
                                Case "entrydate"
                                    lsv.SubItems.Add(GetLastRun(oRs(nIDIndex).Value, nPackID))
                                Case "success"
                                    lsv.SubItems.Add(GetLastResult(oRs(nIDIndex).Value, nPackID))
                                Case "repeatinterval", "repeatuntil"
                                    If s.ToLower = "repeatuntil" Then
                                        Dim val As Date = CTimeZ(oRs(s).Value, dateConvertType.READ)
                                        s = ConTime(val)
                                    Else
                                        s = oRs(s).Value
                                    End If

                                    If IsRpt = True Then
                                        lsv.SubItems.Add(s)
                                    Else
                                        lsv.SubItems.Add("-")
                                    End If
                                Case "starttime"
                                    Dim val As Date = CTimeZ(ConDate(Now) & " " & IsNull(oRs(s).Value, ConTime(Now)), dateConvertType.READ)
                                    lsv.SubItems.Add(ConTime(val))
                                Case Else
                                    lsv.SubItems.Add(oRs(s).Value)
                            End Select
                        End If
                    Next
                End If

                lsv.SubItems.Add(sOwner)

                If oRs("status").Value = 0 Then
                    For Each x As ListViewItem.ListViewSubItem In lsv.SubItems
                        x.ForeColor = System.Drawing.Color.DimGray
                    Next
                End If

                oRs.MoveNext()
            Loop
            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        oForm.IsLoaded = True
    End Sub
    Public Sub PopUpMenu(ByVal oDotNetBar As DotNetBarManager, ByVal sMenuName As String)
        Dim oItem As ButtonItem = CType(oDotNetBar.ContextMenus(sMenuName), ButtonItem)
        oItem.Displayed = False
        oItem.PopupMenu(Control.MousePosition)
    End Sub

    Public Sub DeleteAutoSchedule(ByVal nID As Integer)
        Dim SQL(4) As String
        Dim oSchedule As New clsMarsScheduler
        Dim nScheduleID As Integer = oSchedule.GetScheduleID(, , nID)
        Dim oTask As New clsMarsTask
        Dim sName As String = ""
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                sName = oRs(0).Value
            End If

            oRs.Close()
        End If

        SQL(0) = "DELETE FROM AutomationAttr WHERE AutoID = " & nID
        SQL(1) = "DELETE FROM ScheduleAttr WHERE AutoID =" & nID
        SQL(2) = "DELETE FROM Tasks WHERE ScheduleID =" & nScheduleID
        SQL(3) = "DELETE FROM UserView WHERE AutoID = " & nID
        SQL(4) = "DELETE FROM ScheduleHistory WHERE AutoID = " & nID

        For Each s As String In SQL
            If s.Length > 0 Then clsMarsData.WriteData(s)
        Next

        clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.DELETE)

    End Sub
    Public Sub DropSmartFolder(ByVal nID As Integer)
        Dim SQL(1) As String

        SQL(0) = "DELETE FROM SmartFolderAttr WHERE SmartID = " & nID
        SQL(1) = "DELETE FROM SmartFolders WHERE SmartID =" & nID

        For Each s As String In SQL
            If s.Length > 0 Then clsMarsData.WriteData(s)
        Next

        SQL(0) = "SELECT ScheduleID FROM ScheduleAttr WHERE SmartID = " & nID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL(0))
        Dim ScheduleID As Integer

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                ScheduleID = oRs(0).Value

                clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
            End If

            oRs.Close()
        End If

        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE SmartID =" & nID)

        DeleteDestination(0, 0, nID)

    End Sub

    Public Sub Archive()
        Dim SQL As String
        Dim nInterval As Integer
        Dim PerformArchive As Boolean
        Dim nAutoID As Integer
        Dim nPackID As Integer
        Dim nReportID As Integer
        Dim nEventID As Integer
        Dim sDateDiff As String
        Dim oRs As ADODB.Recordset

        If gConType = "DAT" Then
            sDateDiff = "'d'"
        Else
            sDateDiff = "d"
        End If

        Try
            PerformArchive = Convert.ToBoolean(Convert.ToInt32(ReadRegistry("Archive", 0)))
        Catch
            PerformArchive = False
        End Try


        If PerformArchive = False Then Return
        Dim folderID As Integer = clsMarsData.CreateDataID("folders", "folderid")

        SQL = "UPDATE ScheduleAttr SET DisabledDate = StartDate WHERE Status =0 AND (DisabledDate IS NULL)"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE Folders SET FolderName = '.archive' WHERE FolderName LIKE 'archive'"

        clsMarsData.WriteData(SQL)

        If clsMarsData.IsDuplicate("Folders", "FolderName", ".archive", False) = False Then
            Dim sCols As String
            Dim sVals As String


            sCols = "FolderID,FolderName,Parent"

            sVals = folderID & ",'.archive',0"

            SQL = "INSERT INTO Folders (" & sCols & ") VALUES (" & sVals & ")"

            If clsMarsData.WriteData(SQL, False) = False Then Return
        Else
            SQL = "SELECT folderID FROM Folders WHERE FolderName LIKE '.archive'"

            oRs = clsMarsData.GetData(SQL)
            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    folderID = oRs.Fields("folderid").Value
                End If
            End If

        End If

        nInterval = ReadRegistry("ArchiveAfter", 0)

        SQL = "SELECT * FROM ScheduleAttr WHERE Status =0 AND " & _
        "DateDiff(" & sDateDiff & ",DisabledDate,'" & ConDateTime(Date.Now) & "') >=" & nInterval

        oRs = clsMarsData.GetData(SQL)

        Try
            Do While oRs.EOF = False
                nReportID = IsNull(oRs("reportid").Value, 0)
                nPackID = IsNull(oRs("packid").Value, 0)
                nAutoID = IsNull(oRs("autoid").Value, 0)

                If nReportID > 0 Then
                    SQL = "UPDATE ReportAttr SET Parent = " & folderID & " WHERE " & _
                    "ReportID = " & nReportID
                ElseIf nPackID > 0 Then
                    SQL = "UPDATE PackageAttr SET Parent = " & folderID & " WHERE " & _
                    "PackID = " & nPackID
                ElseIf nAutoID > 0 Then
                    SQL = "UPDATE AutomationAttr SET Parent = " & folderID & " WHERE " & _
                    "AutoID = " & nAutoID
                End If

                clsMarsData.WriteData(SQL, False)

                oRs.MoveNext()
            Loop

            oRs.Close()

            SQL = "SELECT * FROM EventAttr6 WHERE Status =0 AND " & _
            "DateDiff(" & sDateDiff & ",DisabledDate,'" & ConDateTime(Date.Now) & "') >=" & nInterval

            oRs = clsMarsData.GetData(SQL)

            Do While oRs.EOF = False
                nEventID = oRs("eventid").Value

                SQL = "UPDATE EventAttr6 SET Parent = " & folderID & " WHERE EventID =" & nEventID

                clsMarsData.WriteData(SQL, False)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    <DebuggerStepThrough()> Public Overloads Function ReadRegistry(ByVal settingName As String, _
    ByVal defaultValue As Object, Optional ByVal decryptValue As Boolean = False, _
    Optional ByVal configFile As String = "", Optional ByVal directRead As Boolean = False) As Object
        Dim returnValue As Object

        If directRead = True Then
            If configFile.Length = 0 Then
                returnValue = Appconfig.GetSettingValue(settingName, defaultValue, gConfigFile, decryptValue)
            Else
                returnValue = Appconfig.GetSettingValue(settingName, defaultValue, configFile, decryptValue)
            End If
        Else
            returnValue = Appconfig.GetSettingValue(settingName, defaultValue, decryptValue)
        End If

        Return returnValue
    End Function

    <DebuggerStepThrough()> Public Overloads Function SaveRegistry(ByVal settingName As String, ByVal settingValue As Object _
    , Optional ByVal encryptValue As Boolean = False, Optional ByVal configFile As String = "", Optional ByVal directWrite As Boolean = False) As Boolean

        Dim saveCount As Integer = 0
        Dim returnValue As Boolean
        Dim filetoUse As String

        If directWrite = True Then
            If configFile.Length > 0 Then
                filetoUse = configFile
            Else
                filetoUse = gConfigFile
            End If

            'save to in memory hashtable
            Appconfig.SaveSettingValue(settingName, settingValue, encryptValue)

            'and also save to disk
            returnValue = Appconfig.SaveSettingValue(settingName, settingValue, filetoUse, encryptValue)

            Do While returnValue = False And saveCount < 6
                returnValue = Appconfig.SaveSettingValue(settingName, settingValue, filetoUse, encryptValue)

                saveCount += 1

                _Delay(0.5)
            Loop
        Else
            returnValue = Appconfig.SaveSettingValue(settingName, settingValue, encryptValue)
        End If

        Return returnValue
    End Function

    Public Overloads Function DeleteRegistry(ByVal settingName As String, Optional ByVal configFile As String = "") As Boolean
        If configFile.Length > 0 Then
            Return Appconfig.DeleteSetting(settingName, configFile)
        Else
            Return Appconfig.DeleteSetting(settingName, gConfigFile)
        End If
    End Function

    Public Shared Sub TransferSettings()
        Try
            Dim regKey As RegistryKey
            Dim oSec As clsMarsSecurity = New clsMarsSecurity
            Dim oUI As New clsMarsUI

            If oUI.ReadRegistry("XML Trans", 0) = 1 Then Return

            regKey = Registry.LocalMachine.OpenSubKey(sKey, True)

            Dim sValues() As String
            Dim oValue As Object

            sValues = regKey.GetValueNames

            For Each s As String In sValues

                Dim encryptValue As Boolean = False

                If s.Length > 0 Then
                    oValue = regKey.GetValue(s)

                    If TypeOf oValue Is Array Then
                        oValue = oSec._Decrypt(oValue)
                        encryptValue = True
                    End If

                    Appconfig.SaveSettingValue(s, oValue, gConfigFile, encryptValue)
                End If
            Next

            Try
                regKey = Registry.LocalMachine.OpenSubKey(sKey & "\Audit")

                If Not regKey Is Nothing Then

                    sValues = regKey.GetValueNames

                    For Each s As String In sValues
                        Dim encryptValue As Boolean = False

                        oValue = regKey.GetValue(s)

                        Select Case s.ToLower
                            Case "dsn"
                                Appconfig.SaveSettingValue("AuditDSN", oValue, gConfigFile)
                            Case "username"
                                Appconfig.SaveSettingValue("AuditUserName", oValue, gConfigFile)
                            Case "password"
                                Appconfig.SaveSettingValue("AuditPassword", oSec._Decrypt(oValue), gConfigFile, True)
                        End Select
                    Next
                End If
            Catch : End Try
        Catch

        Finally
            clsMarsUI.MainUI.SaveRegistry("XML Trans", 1, , , True)
        End Try
    End Sub


    Public Sub SetAppTheme(Optional ByVal theme As APPTHEMES = APPTHEMES.None)
        Try
            Dim style As eDotNetBarStyle
            Dim bubbleColor As Color

            If theme = APPTHEMES.None Then
                Dim themeName As String = Me.ReadRegistry("AppTheme", "Glass")

                Select Case themeName
                    Case "Glass"
                        theme = APPTHEMES.Glass
                    Case "XP"
                        theme = APPTHEMES.XP
                    Case "Corporate"
                        theme = APPTHEMES.Corporate
                    Case "Classic"
                        theme = APPTHEMES.Classic
                End Select
            End If

            Select Case theme
                Case APPTHEMES.XP
                    style = eDotNetBarStyle.Office2003
                    bubbleColor = System.Drawing.Color.FromArgb(191, 215, 249)
                Case APPTHEMES.Corporate
                    style = eDotNetBarStyle.VS2005
                    bubbleColor = Color.FromArgb(246, 246, 241)
                Case APPTHEMES.Glass
                    style = eDotNetBarStyle.Office2007
                    bubbleColor = Color.FromArgb(188, 213, 246) 'Color.FromArgb(210, 216, 230)
                Case APPTHEMES.Classic
                    style = eDotNetBarStyle.OfficeXP
                    bubbleColor = Color.FromArgb(236, 233, 216)
            End Select

            'For Each frm As frmWindow In oWindow
            '    frm.DotNetBarManager1.Style = style
            '    frm.exSplitter.BackColor = bubbleColor
            '    frm.exSplitter.Refresh()
            'Next



            oMain.DotNetBarManager1.Style = style
            oMain.BubbleBar.ButtonBackAreaStyle.BackColor = bubbleColor
            oMain.BubbleBar.Refresh()

            oMain.dock.BubbleBar.ButtonBackAreaStyle.BackColor = bubbleColor
            oMain.dock.BubbleBar.Refresh()
            oMain.dock.BackColor = bubbleColor
            oMain.dock.Refresh()
            'If db IsNot Nothing Then
            '    db.Style = style
            'End If

            'If splitter IsNot Nothing Then
            '    For Each s As ExpandableSplitter In splitter
            '        s.BackColor = bubbleColor
            '        s.Refresh()
            '    Next
            'End If

            Me.SaveRegistry("AppTheme", theme.ToString)

            For Each frm As Form In oMain.MdiChildren
                Select Case frm.Name.ToLower
                    Case "frmwindow"
                        Dim oform As frmWindow = frm

                        oform.DotNetBarManager1.Style = style
                        oform.exSplitter.BackColor = bubbleColor
                        oform.exSplitter.Refresh()

                        Select Case theme
                            Case APPTHEMES.Classic
                                oform.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Yahoo
                            Case APPTHEMES.Corporate
                                oform.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Black
                            Case APPTHEMES.Glass
                                oform.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Blue
                            Case APPTHEMES.None
                                oform.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.System
                            Case APPTHEMES.XP
                                oform.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.WinXPBlue
                        End Select
                    Case "frmaddressbook"
                        Dim oform As frmAddressBook = frm

                        oform.dtMan.Style = style
                        oform.ExpandableSplitter1.BackColor = bubbleColor
                        oform.ExpandableSplitter1.Refresh()
                    Case "frmcalendarexplorer"
                        Dim oform As frmCalendarExplorer = frm

                        oform.dbman.Style = style
                        oform.ExpandableSplitter1.BackColor = bubbleColor
                        oform.ExpandableSplitter2.BackColor = bubbleColor
                        oform.ExpandableSplitter1.Refresh()
                        oform.ExpandableSplitter2.Refresh()
                    Case "frmsmartreport"
                        Dim oform As frmSmartReport = frm

                        oform.dtMan.Style = style
                End Select
            Next
        Catch ex As Exception

        End Try
    End Sub

    Public Sub InitHistoryImages(ByRef oTree As TreeView, ByVal nodeImage As System.Drawing.Bitmap)
        imgListHistory = New ImageList()

        Dim sz As Size = New Size(16, 16)

        imgListHistory.ImageSize = sz
        imgListHistory.ColorDepth = ColorDepth.Depth32Bit
        imgListHistory.TransparentColor = Color.Transparent

        With imgListHistory.Images
            .Add(nodeImage)                             '0
            .Add(My.Resources.calendar)                 '1
            .Add(My.Resources.check2)                   '2
            .Add(My.Resources.delete2)                  '3
            .Add(My.Resources.about)                    '4
            .Add(My.Resources.date_time_preferences)    '5
            .Add(My.Resources.warning2)                 '6
        End With

        If oTree IsNot Nothing Then
            oTree.ImageList = imgListHistory
        End If

    End Sub

End Class