Public Class clsMarsUsers
    Dim oData As New clsMarsData
    Public Enum enViewType
        ViewSingle = 0
        ViewPackage = 1
        ViewAutomation = 2
        ViewFolder = 3
        ViewSmartFolder = 4
        ViewEvent = 5
        ViewEventPackage = 6
    End Enum

    Public Sub DeleteNTUser(ByVal sUserID As String)
        Dim SQL As String
        Dim sWhere As String

        sWhere = " WHERE DomainName = '" & SQLPrepare(sUserID) & "'"

        SQL = "DELETE FROM DomainAttr " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM UserColumns WHERE Owner = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM UserView WHERE UserID = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)
    End Sub

    Public Sub DeleteUser(ByVal sUserID As String)
        Dim SQL As String
        Dim sWhere As String

        sWhere = " WHERE UserID = '" & sUserID & "'"

        SQL = "DELETE FROM CRDUsers " & sWhere

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM UserColumns WHERE Owner = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)

        SQL = "DELETE FROM UserView WHERE UserID = '" & sUserID & "'"

        clsMarsData.WriteData(SQL, False)
    End Sub

    Public Function AddUser(ByVal sUserID As String, ByVal sPassword As String, _
    ByVal sFirstName As String, ByVal sLastName As String, ByVal sRole As String, ByVal AutoLogin As Boolean) _
    As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sCols As String
        Dim sVals As String
        Dim sWhere As String

        sWhere = " WHERE UserID = '" & sUserID & "'"

        SQL = "SELECT * FROM CRDUsers " & sWhere

        oRs = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                SQL = "[Password] = '" & SQLPrepare(Encrypt(sPassword, "qwer][po")) & "'," & _
                "[FirstName] = '" & SQLPrepare(sFirstName) & "'," & _
                "[LastName] = '" & SQLPrepare(sLastName) & "'," & _
                "[UserRole] ='" & SQLPrepare(sRole) & "'," & _
                "[LastModBy] = '" & SQLPrepare(gUser) & "'," & _
                "[LastModDate] = '" & ConDateTime(Date.Now) & "'," & _
                "[AutoLogin] = " & Convert.ToInt32(AutoLogin)

                SQL = "UPDATE CRDUsers SET " & SQL & sWhere
            Else
                sCols = "[UserID],[Password],[FirstName],[LastName],[UserRole],[LastModBy],[LastModDate],[AutoLogin],[UserNumber]"
                sVals = "'" & sUserID & "'," & _
                "'" & SQLPrepare(Encrypt(sPassword, "qwer][po")) & "'," & _
                "'" & SQLPrepare(sFirstName) & "'," & _
                "'" & SQLPrepare(sLastName) & "'," & _
                "'" & SQLPrepare(sRole) & "'," & _
                "'" & SQLPrepare(gUser) & "'," & _
                "'" & ConDateTime(Date.Now) & "'," & _
                Convert.ToInt32(AutoLogin) & "," & _
                clsMarsData.GetNewUserNumber("CRDUsers")

                SQL = "INSERT INTO CRDUsers (" & sCols & ") VALUES (" & sVals & ")"

            End If

            Dim oReturn As Boolean

            oReturn = clsmarsdata.WriteData(SQL)

            If AutoLogin = True Then
                oReturn = clsmarsdata.WriteData("UPDATE CRDUsers SET AutoLogin = 0 WHERE UserID <> '" & sUserID & "'")
            End If

            Return oReturn
        End If
    End Function

    Public Sub ListNTUsers(ByVal oTree As TreeView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim oParent As TreeNode

        SQL = "SELECT DISTINCT UserRole FROM DomainAttr"

        oRs = clsmarsdata.GetData(SQL)

        oTree.Nodes.Clear()

        oParent = oTree.Nodes.Add("NT Users")
        oParent.Tag = "Root"
        oParent.ImageIndex = 2
        oParent.SelectedImageIndex = 2


        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New TreeNode

                oNode.Text = oRs("userrole").Value
                oNode.ImageIndex = 0
                oNode.SelectedImageIndex = 0
                oNode.Tag = "Role"

                SQL = "SELECT DISTINCT(DomainName) FROM DomainAttr WHERE UserRole ='" & oNode.Text & "'"

                oRs1 = clsmarsdata.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New TreeNode

                        oChild.Text = oRs1("domainname").Value

                        oChild.ImageIndex = 1
                        oChild.SelectedImageIndex = 1
                        oChild.Tag = "UserID"
                        oNode.Nodes.Add(oChild)

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oParent.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)
        oData.DisposeRecordset(oRs1)

        oTree.Nodes(0).ExpandAll()

    End Sub

    Public Sub ListUsers(ByVal oTree As TreeView)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oRs1 As ADODB.Recordset
        Dim oParent As TreeNode

        SQL = "SELECT DISTINCT UserRole FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

        oRs = clsmarsdata.GetData(SQL)

        oTree.Nodes.Clear()

        oParent = oTree.Nodes.Add("SQL-RD Users")
        oParent.Tag = "Root"
        oParent.ImageIndex = 2
        oParent.SelectedImageIndex = 2


        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim oNode As New TreeNode

                oNode.Text = oRs("userrole").Value
                oNode.ImageIndex = 0
                oNode.SelectedImageIndex = 0
                oNode.Tag = "Role"

                SQL = "SELECT UserID FROM CRDUsers WHERE UserRole ='" & oNode.Text & "' AND UserID <> 'SQLRDAdmin'"
                oRs1 = clsmarsdata.GetData(SQL)

                If Not oRs1 Is Nothing Then
                    Do While oRs1.EOF = False
                        Dim oChild As New TreeNode

                        oChild.Text = oRs1("userid").Value

                        oChild.ImageIndex = 1
                        oChild.SelectedImageIndex = 1
                        oChild.Tag = "UserID:" & oRs1("userid").Value
                        oNode.Nodes.Add(oChild)

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()
                End If

                oParent.Nodes.Add(oNode)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        oData.DisposeRecordset(oRs)
        oData.DisposeRecordset(oRs1)

        oTree.Nodes(0).ExpandAll()

    End Sub

    Public Sub AssignView(ByVal nID As Integer, ByVal sUserID As String, ByVal sType As enViewType)
        Dim SQL As String
        Dim sCol As String
        Dim sCols As String
        Dim sVals As String

        Select Case sType
            Case enViewType.ViewSingle
                sCol = "ReportID"
            Case enViewType.ViewPackage
                sCol = "PackID"
            Case enViewType.ViewAutomation
                sCol = "AutoID"
            Case enViewType.ViewFolder
                sCol = "FolderID"
            Case enViewType.ViewSmartFolder
                sCol = "SmartID"
            Case enViewType.ViewEvent
                sCol = "EventID"
            Case enViewType.ViewEventPackage
                sCol = "EventPackID"
        End Select

        sCols = "ViewID,UserID," & sCol & ""

        sVals = clsMarsData.CreateDataID("userview", "viewid") & "," & _
        "'" & sUserID & "'," & nID

        SQL = "INSERT INTO UserView (" & sCols & ") VALUES (" & sVals & ")"

        clsmarsdata.WriteData(SQL)

    End Sub
End Class
