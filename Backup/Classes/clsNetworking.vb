Public Class clsNetworking
    Dim WithEvents connection As DialUpComponent.DialUpConnection
    Dim session As New DialUpComponent.DialUpSession
    Dim oUI As New clsMarsUI
    Dim sError As String = ""
    Dim nError As Integer = 0
    Dim IsDone As Boolean = False
    Sub New()
        session.LicenseKey = "34DB6914E0A1DBFA999498C0503FD2A9554ADC616F9265C237D063"
    End Sub
    Public Sub _Disconnect()
        Try
            If Not connection Is Nothing Then
                connection.HangUp()
                oUI.BusyProgress(, , True)
            End If
        Catch : End Try
    End Sub
    Public Function _DialConnection(ByVal sName As String, Optional ByVal ScheduleInfo As String = "") As Boolean
        Try
            connection = session.OpenConnection(sName)

            If Not connection Is Nothing Then
                AppStatus(True)
                connection.Dial()

                Do
                    Application.DoEvents()
                Loop Until IsDone = True

                oUI.BusyProgress(, , True)
                AppStatus(False)
            End If

            If sError.Length = 0 Then
                Return True
            Else
                gErrorDesc = ScheduleInfo & " " & sError
                gErrorSource = "_DialConnection"
                gErrorLine = 14
                gErrorNumber = nError
                Return False
            End If
        Catch ex As Exception
            gErrorDesc = ScheduleInfo & " " & ex.Message
            gErrorSource = Reflection.MethodBase.GetCurrentMethod.Name
            gErrorLine = _GetLineNumber(ex.StackTrace)
            gErrorNumber = Err.Number
            Return False
        End Try
    End Function

    Private Sub connection_Authenticating() Handles connection.Authenticating
        oUI.BusyProgress(50, "Authenticating...")
    End Sub

    Private Sub connection_Connected() Handles connection.Connected
        oUI.BusyProgress(50, "Connected to: " & connection.IPAddress)
        IsDone = True
    End Sub

    Private Sub connection_Dialing() Handles connection.Dialing
        oUI.BusyProgress(50, "Dialing " & connection.PhoneNumber & "...")
    End Sub

    Private Sub connection_Disconnected() Handles connection.Disconnected
        IsDone = True
    End Sub

    Private Sub connection_Error(ByVal Code As Integer, ByVal Description As String) Handles connection.Error
        sError = "Error - " & Description & " (" & Code & ")"
        nError = Code
    End Sub
End Class
