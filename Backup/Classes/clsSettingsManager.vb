Imports System
Imports System.Xml
Imports System.IO
Imports System.Security.Cryptography

Public Class clsSettingsManager

    Dim mProvider As TripleDESCryptoServiceProvider

    Public Shared Appconfig As New clsSettingsManager
    Public Shared m_configTable As Hashtable

    Sub New()
        mProvider = New TripleDESCryptoServiceProvider
        mProvider.Key = New Byte() {112, 223, 122, 83, 173, 22, 186, 153, 229, 183, 73, 133, 124, 124, 132, 13}
        mProvider.IV = New Byte() {173, 224, 14, 43, 253, 103, 82, 212}
    End Sub

    Public Sub populateConfigTable(ByVal sConfigFile As String)
        Dim retryCount As Integer = 0
10:     Try
RETRY:
20:         m_configTable = New Hashtable

30:         Dim oDoc As XmlDocument = New Xml.XmlDocument
            Dim oNodes As XmlNodeList
            Dim oNode As XmlNode

40:         If IO.File.Exists(sConfigFile) = False Then
50:             CreateSettingsFile(sConfigFile)
            End If

60:         oDoc.Load(sConfigFile)

70:         oNodes = oDoc.GetElementsByTagName("setting")

80:         For Each oNode In oNodes
                Dim Attr As String = ""

90:             Attr = oNode.Attributes("name").Value

100:            m_configTable.Add(Attr.ToLower, oNode.ChildNodes(0).InnerText)
110:        Next
120:    Catch ex As Exception
            If retryCount < 3 Then
                retryCount += 1

                _Delay(1)
                GoTo RETRY
            Else
                m_configTable = Nothing
130:            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True)
            End If
        End Try

    End Sub


    Public Sub savetoConfigFile(ByVal sConfigFile As String, Optional ByVal ignoreAutoCompact As Boolean = True)

        If RunEditor = False Then Return

        Dim oDoc As XmlDocument

        If m_configTable Is Nothing Then Return

10:     Try
20:         oDoc = New Xml.XmlDocument

30:         If IO.File.Exists(sConfigFile) = False Then
40:             CreateSettingsFile(sConfigFile)
            End If

50:         oDoc.Load(sConfigFile)
60:     Catch ex As Exception
70:         _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
80:         Return
        End Try

90:     For Each mDic As DictionaryEntry In Me.m_configTable
            Dim oNodes As XmlNodeList
            Dim oNode As XmlNode

100:        Try

110:            oNodes = oDoc.GetElementsByTagName("setting")

                Dim settingName As String = mDic.Key
                Dim settingValue As String = mDic.Value

                If settingName.ToLower = "compactdue" And ignoreAutoCompact = True Then Continue For

120:            If SettingExists(settingName, oDoc) = True Then
130:                For Each oNode In oNodes
                        Dim Attr As String

140:                    Attr = oNode.Attributes("name").Value

150:                    If Attr.ToLower = settingName.ToLower Then
160:                        oNode.ChildNodes(0).InnerText = settingValue

170:                        Exit For
                        End If
180:                Next
190:            Else
200:                If CreateSettingValue(oDoc, settingName, settingValue, sConfigFile) = False Then
210:                    Continue For
                    End If
                End If
220:
230:        Catch
240:            Continue For
            End Try
250:    Next

        Try
260:        oDoc.Save(sConfigFile)
        Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub

    <DebuggerStepThrough()> _
    Public Overloads Function GetSettingValue(ByVal settingName As String, ByVal defaultValue As Object, Optional ByVal decrypt As Boolean = False)
        If Me.m_configTable Is Nothing Then
            Me.populateConfigTable(gConfigFile)
        End If

        Try
            If Me.m_configTable.ContainsKey(settingName.ToLower) Then
                defaultValue = Me.m_configTable(settingName.ToLower)
            End If

            If decrypt = True Then
                defaultValue = DecryptString(defaultValue)
            End If
        Catch : End Try

        Return defaultValue
    End Function

    <DebuggerStepThrough()> _
    Public Overloads Function SaveSettingValue(ByVal settingName As String, ByVal settingValue As Object, Optional ByVal Encrypt As Boolean = False)
        If Me.m_configTable Is Nothing Then
            Me.populateConfigTable(gConfigFile)
        End If

        If Encrypt = True Then
            settingValue = Me.EncryptString(settingValue)
        End If

        If Me.m_configTable.ContainsKey(settingName.ToLower) = True Then
            Me.m_configTable(settingName.ToLower) = settingValue
        Else
            Me.m_configTable.Add(settingName.ToLower, settingValue)
        End If
    End Function


    <DebuggerStepThrough()> Public Overloads Function GetSettingValue(ByVal SettingName As String, _
    ByVal DefaultValue As Object, ByVal sConfigFile As String, Optional ByVal Decrypt As Boolean = False) As Object

        Dim oDoc As XmlDocument = New Xml.XmlDocument
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode


        If IO.File.Exists(sConfigFile) = False Then
            CreateSettingsFile(sConfigFile)
        End If

        oDoc.Load(sConfigFile)

        oNodes = oDoc.GetElementsByTagName("setting")

        For Each oNode In oNodes
            Dim Attr As String = ""

            Attr = oNode.Attributes("name").Value

            If Attr.ToLower = SettingName.ToLower Then
                DefaultValue = oNode.ChildNodes(0).InnerText

                If Decrypt = True Then
                    DefaultValue = DecryptString(DefaultValue)
                End If

                Exit For
            End If
        Next

        Return DefaultValue
    End Function

    Public Function DeleteSetting(ByVal SettingName As String, ByVal sConfigFile As String) As Boolean

        Try
            Dim oDoc As XmlDocument = New Xml.XmlDocument
            Dim oNodes As XmlNodeList
            Dim oNode As XmlNode
            Dim nodeExists As Boolean = False

            If IO.File.Exists(sConfigFile) = False Then
                CreateSettingsFile(sConfigFile)
            End If

            oDoc.Load(sConfigFile)

            oNodes = oDoc.GetElementsByTagName("setting")

            For Each oNode In oNodes
                Dim Attr As String

                Attr = oNode.Attributes("name").Value

                If Attr.ToLower = SettingName.ToLower Then
                    nodeExists = True
                    Exit For
                End If
            Next

            If Not oNode Is Nothing And nodeExists = True Then
                Dim ParentNode As XmlNodeList

                ParentNode = oDoc.GetElementsByTagName("settings")

                For Each iNode As XmlNode In ParentNode

                    iNode.RemoveChild(oNode)
                Next

                oDoc.Save(sConfigFile)
            End If

            Return True
        Catch
            Return False
        End Try

    End Function
    Private Function SettingExists(ByVal SettingName As String, ByVal oDoc As XmlDocument) As Boolean
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode

        oNodes = oDoc.GetElementsByTagName("setting")

        For Each oNode In oNodes
            Dim Attr As String

            Attr = oNode.Attributes("name").Value

            If Attr.ToLower = SettingName.ToLower Then
                Return True
            End If
        Next

        Return False
    End Function

    <DebuggerStepThrough()> Public Overloads Function SaveSettingValue(ByVal SettingName As String, ByVal SettingValue As Object, _
    ByVal sConfigFile As String, Optional ByVal Encrypt As Boolean = False) As Boolean

        Dim oDoc As XmlDocument = New Xml.XmlDocument
        Dim oNodes As XmlNodeList
        Dim oNode As XmlNode

        Try
            If IO.File.Exists(sConfigFile) = False Then
                CreateSettingsFile(sConfigFile)
            End If

            oDoc.Load(sConfigFile)

            oNodes = oDoc.GetElementsByTagName("setting")

            If Encrypt = True Then
                SettingValue = EncryptString(SettingValue)
            End If

            If SettingExists(SettingName, oDoc) = True Then
                For Each oNode In oNodes
                    Dim Attr As String

                    Attr = oNode.Attributes("name").Value

                    If Attr.ToLower = SettingName.ToLower Then
                        oNode.ChildNodes(0).InnerText = SettingValue

                        Exit For
                    End If
                Next
            Else
                If CreateSettingValue(oDoc, SettingName, SettingValue, sConfigFile) = False Then
                    Return False
                End If
            End If

            oDoc.Save(sConfigFile)

            Return True
        Catch
            'MsgBox(Err.Description)
            Return False
        End Try
    End Function

    Private Function CreateSettingValue(ByVal oDoc As Xml.XmlDocument, ByVal SettingName As String, ByVal SettingValue As Object, _
    ByVal sconfigfile As String) As Boolean

        Try
            Dim NewNode As XmlNode

            'create a new setting node
            NewNode = oDoc.CreateElement("setting")

            Dim NewAttr As XmlAttribute

            'add the name attribute
            NewAttr = oDoc.CreateAttribute("name")

            'give the attribute a value
            NewAttr.Value = SettingName

            NewNode.Attributes.Append(NewAttr)

            'give the setting its value
            Dim ValueNode As XmlNode

            ValueNode = oDoc.CreateElement("value")

            'add the value node to the setting node
            NewNode.AppendChild(ValueNode)

            'add the value of the setting
            ValueNode.AppendChild(oDoc.CreateTextNode(SettingValue))

            Dim ParentNode As XmlNodeList

            ParentNode = oDoc.GetElementsByTagName("settings")

            For Each oNode As XmlNode In ParentNode
                oNode.AppendChild(NewNode)
            Next

            oDoc.Save(sconfigfile)
        Catch
            Return False
        End Try
    End Function

    Public Function EncryptString(ByVal AString As String) As String
        If AString = String.Empty Then
            Return AString
        Else
            Dim encryptedData() As Byte
            Dim dataStream As MemoryStream

            Dim encryptor As ICryptoTransform
            encryptor = mProvider.CreateEncryptor()

            Try
                dataStream = New MemoryStream

                Dim encryptedStream As CryptoStream
                Try
                    'Create the encrypted stream
                    encryptedStream = New CryptoStream(dataStream, encryptor, CryptoStreamMode.Write)

                    Dim theWriter As StreamWriter
                    Try
                        'Write the string to memory via the encryption algorithm
                        theWriter = New StreamWriter(encryptedStream)
                        'Write the string to the memory stream
                        theWriter.Write(AString)

                        'End the writing
                        theWriter.Flush()
                        encryptedStream.FlushFinalBlock()

                        'Position back at start
                        dataStream.Position = 0

                        'Create area for data
                        ReDim encryptedData(CInt(dataStream.Length))

                        'Read data from memory
                        dataStream.Read(encryptedData, 0, CInt(dataStream.Length))

                        'Convert to String
                        Return Convert.ToBase64String(encryptedData, 0, encryptedData.Length)
                    Finally
                        theWriter.Close()
                    End Try
                Finally
                    encryptedStream.Close()
                End Try
            Finally
                dataStream.Close()
            End Try
        End If
    End Function
    Public Function DecryptString(ByVal AString As String) As String
        Try
            If AString = String.Empty Then
                Return AString
            Else
                Dim encryptedData() As Byte
                Dim dataStream As MemoryStream
                Dim encryptedStream As CryptoStream
                Dim strLen As Integer

                'Get the byte data
                encryptedData = Convert.FromBase64String(AString)

                Try
                    dataStream = New MemoryStream
                    Try
                        'Create decryptor and stream
                        Dim decryptor As ICryptoTransform
                        decryptor = mProvider.CreateDecryptor()
                        encryptedStream = New CryptoStream(dataStream, decryptor, CryptoStreamMode.Write)

                        'Write the decrypted data to the memory stream
                        encryptedStream.Write(encryptedData, 0, encryptedData.Length - 1)
                        encryptedStream.FlushFinalBlock()

                        'Position back at start
                        dataStream.Position = 0

                        'Determine length of decrypted string
                        strLen = CInt(dataStream.Length)

                        'Create area for data
                        ReDim encryptedData(strLen - 1)

                        'Read decrypted data to byte()
                        dataStream.Read(encryptedData, 0, strLen)

                        'Construct string from byte()
                        Dim retStr As String

                        Dim i As Integer
                        For i = 0 To strLen - 1
                            retStr += Chr(encryptedData(i))
                        Next

                        'Return result
                        Return retStr
                    Finally
                        encryptedStream.Close()
                    End Try
                Finally
                    dataStream.Close()
                End Try
            End If
        Catch ex As Exception
            Return AString
        End Try
    End Function

    Private Function CreateSettingsFile(ByVal sConfigFile As String) As Boolean
        Dim sInit As String

        sInit = "<?xml version=" & Chr(34) & "1.0" & Chr(34) & " encoding=" & Chr(34) & "utf-8" & Chr(34) & " ?>"

        sInit &= Environment.NewLine & "<settings>"
        sInit &= Environment.NewLine & "</settings>"

        Dim oWriter As StreamWriter

        oWriter = New StreamWriter(sConfigFile)

        oWriter.Write(sInit)

        oWriter.Close()
    End Function


End Class
