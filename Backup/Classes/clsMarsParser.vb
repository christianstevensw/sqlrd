
Public Class clsMarsParser
    Public Shared Parser As New clsMarsParser

    Public Function fixASMXfor2008(ByVal sUrl As String) As String
        Try
            'get the web service name from the url
            If sUrl.EndsWith(".asmx") Then
                Dim nCount As Integer = sUrl.Split("/").GetUpperBound(0)
                Dim svcName As String = sUrl.Split("/")(nCount)
                Dim temp As String = ""

                'if report service name is not reportservice2005.asmx
                If String.Compare("reportservice2005.asmx", svcName, True) <> 0 Then
                    For n As Integer = 0 To nCount - 1
                        temp &= sUrl.Split("/")(n) & "/"
                    Next

                    sUrl = temp & "reportservice2005.asmx"
                End If
            Else
                If sUrl.EndsWith("/") Then
                    sUrl &= "reportservice2005.asmx"
                Else
                    sUrl &= "/reportservice2005.asmx"
                End If
            End If

            Return sUrl
        Catch e As Exception
            Return sUrl
        End Try
    End Function
    Public Function removeASMX(ByVal sUrl As String) As String
        Dim output As String

        If sUrl.EndsWith(".asmx") Then
            For I As Integer = 0 To sUrl.Split("/").GetUpperBound(0) - 1
                output &= sUrl.Split("/")(I) & "/"
            Next

            Return output
        Else
            Return sUrl
        End If

    End Function

    ' <DebuggerStepThrough()> _
    Public Function ParseString(ByVal sIn As String, _
                                Optional ByVal ForHTML As Boolean = False, _
                                Optional ByVal ForSQL As Boolean = False, _
                                Optional ByVal usePointer As Boolean = True, _
                                Optional ByVal emailField As String = "", _
                                Optional ByVal emailValue As String = "", _
                                Optional ByVal parametersTable As Hashtable = Nothing) As String

        Dim sFunction As String
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim sValue As String

        'lets find SQL-RD constants and then replace them with our values

        Do While sIn.IndexOf("<[m]") > -1

            nStart = sIn.IndexOf("<[m]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim functionName As String = ""
            Dim adjustBy As String = ""
            Dim temp As String
            Dim dateFormat As String = ""

            temp = sFunction.Replace("<[m]", String.Empty).Replace(">", String.Empty)

            If sFunction.IndexOf(";") > -1 Then
                functionName = temp.Split(";")(0)
                adjustBy = temp.Split(";")(1)

                Try
                    dateFormat = temp.Split(";")(2)
                Catch
                    dateFormat = ""
                End Try

                Try
                    Int32.Parse(adjustBy)
                Catch ex As Exception
                    dateFormat = adjustBy
                    adjustBy = ""
                End Try
            Else
                functionName = temp
            End If


            sValue = CalculateValue(functionName, adjustBy, dateFormat)

            If ForSQL = True Then sValue = SQLPrepare(sValue)

            sIn = sIn.Replace(sFunction, sValue)
        Loop

        'lets find user defined constants and replace with the stored values
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        Do While sIn.IndexOf("<[u]") > -1
            nStart = sIn.IndexOf("<[u]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim s As String = sFunction.Replace("<[u]", String.Empty).Replace(">", String.Empty)

            SQL = "SELECT * FROM FormulaAttr WHERE " & _
            "FormulaName ='" & SQLPrepare(s) & "'"

            oRs = clsMarsData.GetData(SQL)

            Try
                If oRs.EOF = False Then
                    Dim definition As String = oRs("formuladef").Value

                    definition = Me.ParseString(definition, ForHTML, ForSQL)

                    If ForSQL = True Then definition = SQLPrepare(definition)

                    sIn = sIn.Replace(sFunction, definition)
                End If
            Catch ex As Exception
                Exit Do
            End Try

            oRs.Close()
        Loop

        Do While sIn.IndexOf("<[d]") > -1

            nStart = sIn.IndexOf("<[d]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim oItem As New frmDataItems

            sValue = oItem._GetDataItemValue(sFunction)

            If sValue Is Nothing Then
                Dim emptyItem As Exception = New Exception("The data item '" & sFunction & "' did not return any values.")

                Throw emptyItem
            Else
                If ForSQL = True Then sValue = SQLPrepare(sValue)
            End If

            sIn = sIn.Replace(sFunction, sValue)
        Loop

        Do While sIn.IndexOf("<[p]") > -1
            nStart = sIn.IndexOf("<[p]")
            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            If parametersTable IsNot Nothing Then

                Dim key As String = sFunction.Replace("<[p]", "").Replace(">", "")

                sValue = parametersTable.Item(key)

                If ForSQL = True Then sValue = SQLPrepare(sValue)

                sIn = sIn.Replace(sFunction, sValue)
            Else
                Dim newFunction As String = sFunction.Replace("[p]", "[~]")

                sIn = sIn.Replace(sFunction, newFunction)
            End If
        Loop

        Do While sIn.IndexOf("<[e]") > -1
            nStart = sIn.IndexOf("<[e]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            If clsMarsEvent.m_resultsTable IsNot Nothing Then
                sValue = ProcessEventData(sFunction, ForHTML)

                If ForSQL = True Then sValue = SQLPrepare(sValue)

                sIn = sIn.Replace(sFunction, sValue)
            Else
                sIn = sIn.Replace(sFunction, "")
            End If
        Loop

        Do While sIn.Contains("<[r]") = True
            nStart = sIn.IndexOf("<[r]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            If clsMarsReport.m_dataDrivenCache IsNot Nothing Then
                sValue = GetDataDrivenData(sFunction.Replace("<[r]", "").Replace(">", ""), usePointer, emailField, emailValue)

                sIn = sIn.Replace(sFunction, sValue)
            Else
                sIn = sIn.Replace(sFunction, "")
            End If
        Loop

        Return sIn
    End Function
    Private Function GetDataDrivenData(ByVal columnName As String, Optional ByVal usePointer As Boolean = True, Optional ByVal emailField As String = "", _
    Optional ByVal emailValue As String = "")

        If usePointer = True Or emailField = "" Or emailValue = "" Then
            If clsMarsReport.m_ddKeyColumn = "" Or clsMarsReport.m_ddKeyValue = "[Unavailable]" Then
                Dim row As DataRow = clsMarsReport.m_dataDrivenCache.Rows(clsMarsReport.m_pointer)

                If row IsNot Nothing Then
                    Return IsNull(row(columnName))
                End If
            Else
                Dim rows() As DataRow = clsMarsReport.m_dataDrivenCache.Select(clsMarsReport.m_ddKeyColumn & "='" & SQLPrepare(clsMarsReport.m_ddKeyValue) & "'")

                If rows Is Nothing Then
                    Dim row As DataRow = clsMarsReport.m_dataDrivenCache.Rows(clsMarsReport.m_pointer)

                    If row IsNot Nothing Then
                        Return IsNull(row(columnName))
                    End If
                ElseIf rows.Length = 0 Then
                    Dim row As DataRow = clsMarsReport.m_dataDrivenCache.Rows(clsMarsReport.m_pointer)

                    If row IsNot Nothing Then
                        Return IsNull(row(columnName))
                    End If
                Else
                    Dim row As DataRow = rows(0)

                    If row IsNot Nothing Then
                        Return IsNull(row(columnName))
                    End If
                End If
            End If
        Else
            Dim rows() As DataRow = clsMarsReport.m_dataDrivenCache.Select(emailField & " = '" & SQLPrepare(emailValue) & "'")

            If rows IsNot Nothing Then
                If rows.Length = 0 Then
                    Dim row As DataRow = clsMarsReport.m_dataDrivenCache.Rows(clsMarsReport.m_pointer)

                    If row IsNot Nothing Then
                        Return IsNull(row(columnName))
                    End If
                Else
                    Dim returnValue As String = ""

                    For Each row As DataRow In rows
                        returnValue &= IsNull(row(columnName)) & ", "
                    Next

                    returnValue = returnValue.Remove(returnValue.Length - 2, 2)

                    Return returnValue
                End If
            End If
        End If
    End Function
    Public Function CleanString(ByVal sIn As String) As String
        Dim sFunction As String
        Dim nStart As Integer
        Dim nEnd As Integer
        Dim sValue As String
        'lets find SQL-RD constants and then replace them with our values

        Do While sIn.IndexOf("<[m]") > -1

            nStart = sIn.IndexOf("<[m]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim functionName As String = ""
            Dim adjustBy As String = ""
            Dim temp As String
            Dim dateFormat As String = ""

            sValue = sFunction.Replace("<[m]", "![m]").Replace(">", "!")

            sIn = sIn.Replace(sFunction, sValue)
        Loop

        'lets find user defined constants and replace with the stored values
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        Do While sIn.IndexOf("<[u]") > -1
            nStart = sIn.IndexOf("<[u]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            Dim s As String = sFunction.Replace("<[u]", "![u]").Replace(">", "!")

            sIn = sIn.Replace(sFunction, s)
        Loop

        Do While sIn.IndexOf("<[d]") > -1

            nStart = sIn.IndexOf("<[d]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            sValue = sFunction.Replace("<[d]", "![d]").Replace(">", "!")

            sIn = sIn.Replace(sFunction, sValue)
        Loop

        Do While sIn.IndexOf("<[p]") > -1
            nStart = sIn.IndexOf("<[p]")
            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            sValue = sFunction.Replace("<[p]", "![p]").Replace(">", "!")


            sIn = sIn.Replace(sFunction, sValue)
        Loop

        Do While sIn.IndexOf("<[e]") > -1
            nStart = sIn.IndexOf("<[e]")

            nEnd = sIn.IndexOf(">", nStart) + 1

            sFunction = sIn.Substring(nStart, nEnd - nStart)

            sValue = sFunction.Replace("<[e]", "![e]").Replace(">", "!")

            sIn = sIn.Replace(sFunction, sValue)
        Loop

        Return sIn
    End Function

    Private Function ProcessEventData(ByVal functionName As String, Optional ByVal ForHTML As Boolean = False) As String

10:     Try
            Dim values() As Object
            Dim key As String
            Dim field As String
            Dim msgType As String = ""
            Dim attachIndex As Integer = -1

            If functionName = "" Then Return functionName

20:         functionName = functionName.Replace("<[e]", "").Replace(">", "")

30:         key = functionName.Split(";")(0)
40:         field = functionName.Split(";")(1)

50:         Try
60:             msgType = functionName.Split(";")(2)
70:         Catch ex As Exception
80:             msgType = ""
            End Try

90:         Try
100:            attachIndex = functionName.Split(";")(2)
110:        Catch ex As Exception
120:            attachIndex = -1
            End Try

130:        field = field.Replace("[", "").Replace("]", "")

140:        If clsMarsEvent.m_resultsTable.ContainsKey(key) = True Then
150:            values = clsMarsEvent.m_resultsTable.Item(key)
160:        Else
170:            Return ""
            End If

            Dim SQL As String = "SELECT * FROM EventConditions WHERE EventID =" & clsMarsEvent.m_eventID & " AND ConditionName ='" & SQLPrepare(key) & "'"
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim conditionType As String = ""
            Dim conditionName As String = ""

180:        If oRs Is Nothing Then Return ""
190:        If oRs.EOF = True Then Return ""

200:        conditionType = oRs("conditiontype").Value
            conditionName = IsNull(oRs("conditionname").Value)

210:        Select Case conditionType.ToLower
                Case "database record exists", "database record has changed"

                    Dim keyColumn As String = oRs("keycolumn").Value
                    Dim sValue As String
                    Dim dsn, user, password As String

220:                Try
                        Dim dt As DataTable

230:                    For Each tdt As DataTable In clsMarsEvent.m_EventDataCache
240:                        If tdt.TableName = key Then
250:                            dt = tdt
                            End If
260:                    Next

270:                    If dt IsNot Nothing Then

280:                        sValue = IsNull(values(clsMarsEvent.m_Pointer), "")

                            'try converting svalue to date
290:                        Try
300:                            Date.Parse(sValue)

310:                            sValue = "'" & ConDateTime(sValue) & "'"
320:                        Catch ex As Exception
330:                            sValue = "'" & SQLPrepare(sValue) & "'"
                            End Try

                            If keyColumn.StartsWith("[") = False And keyColumn.EndsWith("]") = False Then
                                If keyColumn.Contains(".") Then
                                    keyColumn = "[" & keyColumn.Split(".")(0) & "]." & "[" & keyColumn.Split(".")(1) & "]"
                                Else
                                    keyColumn = "[" & keyColumn & "]"
                                End If
                            End If

                            Dim rows As DataRow() = dt.Select(keyColumn & " = " & sValue)

340:                        If rows IsNot Nothing Then
350:                            If rows.Length > 0 Then
                                    Dim row As DataRow = rows(0)

360:                                sValue = IsNull(row(field), "")

370:                                If ForHTML = True Then
380:                                    sValue = sValue.Replace(vbCrLf, "<BR>")
                                    End If
390:                            Else
400:                                Throw New Exception("At the time of executing the query, no records matching the selection criteria were found.")
                                End If
410:                        Else
420:                            Throw New Exception("At the time of executing the query, no records matching the selection criteria were found.")
                            End If
                        End If
430:                Catch ex As Exception
                        '_ErrorHandle("Error reading eb cache for " & gScheduleName & "." & conditionName & ": " & ex.Message & " (" & functionName & ")", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 1)

440:                    Dim oCon As ADODB.Connection = New ADODB.Connection
450:                    Dim oRs1 As ADODB.Recordset = New ADODB.Recordset
460:                    SQL = oRs("searchcriteria").Value
                        Dim connection As String = oRs("connectionstring").Value

470:                    dsn = connection.Split("|")(0)
480:                    user = connection.Split("|")(1)
490:                    password = _DecryptDBValue(connection.Split("|")(2))

500:                    oCon.Open(dsn, user, password)

510:                    sValue = IsNull(values(clsMarsEvent.m_Pointer), "")

520:                    sValue = clsMarsData.ConvertToFieldType(SQL, oCon, keyColumn, sValue)

530:                    If SQL.ToLower.IndexOf("where") < 0 Then
540:                        SQL &= " WHERE " & keyColumn & " = " & sValue
550:                    Else
560:                        SQL &= " AND " & keyColumn & " = " & sValue
                        End If

570:                    oRs1.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

580:                    If oRs1.EOF = False Then
590:                        sValue = IsNull(oRs1.Fields(field).Value, "")

600:                        If ForHTML = True Then
610:                            sValue = sValue.Replace(vbCrLf, "<BR>")
                            End If
620:                    Else
630:                        Throw New Exception("At the time of executing the query, no records matching the selection criteria were found.")
                        End If

640:                    oRs1.Close()

650:                    oCon.Close()
                    End Try

660:                Return sValue
670:            Case "file exists", "file has been modified"
680:                Select Case field.ToLower
                        Case "filename"
690:                        Return values(0)
700:                    Case "filedirectory"
710:                        Return values(1)
720:                    Case "filepath"
730:                        Return values(2)
740:                    Case "datemodified"
750:                        Return values(3)
760:                    Case "datecreated"
770:                        Return values(4)
                    End Select
780:            Case "process exists", "window is present"
790:                Select Case field.ToLower
                        Case "processname"
800:                        Return values(0)
810:                    Case "mainwindowtitle"
820:                        Return values(1)
830:                    Case "totalprocessortime"
840:                        Return values(2)
850:                    Case "userprocessortime"
860:                        Return values(3)
870:                    Case "maxworkingset"
880:                        Return values(4)
890:                    Case "minworkingset"
900:                        Return values(5)
910:                    Case "id"
920:                        Return values(6)
                    End Select

930:            Case "unread email is present"
                    Dim emailData As DataTable

940:                emailData = values(0)

                    Dim emailRow As DataRow = emailData.Rows(clsMarsEvent.m_Pointer)

950:                Select Case field.ToLower
                        Case "from"
960:                        Return emailRow("MsgFrom")
970:                    Case "to"
980:                        Return emailRow("MsgTo")
990:                    Case "cc"
1000:                       Return emailRow("MsgCc")
1010:                   Case "bcc"
1020:                       Return emailRow("MsgBcc")
1030:                   Case "subject"
1040:                       Return emailRow("MsgSubject")
1050:                   Case "message"
                            Dim returnValue As String = ""

1060:                       If msgType <> "" Then
1070:                           Select Case msgType.ToLower
                                    Case "text"
1080:                                   returnValue = emailRow("msgTEXT")
1090:                               Case "html"
1100:                                   returnValue = emailRow("msgHTML")
1110:                               Case Else
1120:                                   returnValue = emailRow("MsgBody")
                                End Select
1130:                       Else
1140:                           returnValue = emailRow("MsgBody")
                            End If

1150:                       Return returnValue 'emailRow("MsgBody")
1160:                   Case "datesent"
1170:                       Return emailRow("MsgDate")
1180:                   Case "attachments"

1190:                       If attachIndex = -1 Then
1200:                           Return emailRow("Attachments").ToString.Trim
1210:                       Else
                                Dim attach As String = emailRow("Attachments")
1220:                           Try
1230:                               Return attach.Split(";")(attachIndex).Trim
1240:                           Catch ex As Exception
1250:                               Throw New Exception("No attachment found at the specified index (Index = " & attachIndex & ")")
                                End Try
                            End If
1260:                   Case "attachmentspath"
1270:                       If attachIndex = -1 Then
1280:                           Return emailRow("AttachmentsPath").ToString.Trim
1290:                       Else
                                Dim attach As String = emailRow("AttachmentsPath")

1300:                           Try
1310:                               Return attach.Split(";")(attachIndex).Trim
1320:                           Catch ex As Exception
1330:                               Throw New Exception("No attachment found at the specified index (Index = " & attachIndex & ")")
                                End Try
                            End If
1340:                   Case Else
1350:                       Return FindFieldValue(field, emailRow("MsgBody"))
                    End Select
1360:           Case Else
1370:               Return ""
            End Select

1380:       Try
1390:           oRs.Close()
            Catch : End Try
1400:   Catch ex As Exception
            Dim suggest As String = ""
            Dim donotEmail As Boolean = False

1410:       If ex.Message.ToLower.IndexOf("time of executing the query") > -1 Then
1420:           suggest = "Try running all your update queries in one task as part of a large sql command/stored procedure."
1430:           donotEmail = True
            End If

1440:       _ErrorHandle(ex.Message & " (" & functionName & ")", Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
     Erl(), suggest, donotEmail)

1450:       Return ""
        End Try
    End Function


    Private Function FindFieldValue(ByVal fieldMarker As String, ByVal fullText As String) As String

        Try
            If fieldMarker.EndsWith(":") = False Then fieldMarker &= ":"

            If fullText.ToLower.IndexOf(fieldMarker.ToLower) = -1 Then
                Return ""
            End If

            Dim nStart As Integer = fullText.ToLower.IndexOf(fieldMarker.ToLower) + fieldMarker.Length
            Dim nEnd As Integer = fullText.IndexOf(vbCrLf, nStart)
            Dim nEnd2 As Integer = fullText.IndexOf("</", nStart)

            If nEnd2 <> -1 And nEnd2 < nEnd Then nEnd = nEnd2

            If nEnd = -1 Then nEnd = fullText.Length

            Dim value As String = fullText.Substring(nStart, nEnd - nStart)

            Return value.Trim
        Catch
            Return ""
        End Try

    End Function
    Public Function CalculateValue(ByVal sName As String, Optional ByVal adjustBy As String = "", Optional ByVal dateFormat As String = "") As String
        Dim iToday As Integer
        Dim Temp As Date
        Dim nAdjust As Double

        iToday = Date.Now.DayOfWeek

        Try
            nAdjust = adjustBy
        Catch ex As Exception
            adjustBy = 0
        End Try

        Select Case sName
            Case "Yesterday"
                Temp = Date.Now.AddDays(-1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "CurrentDate"
                Temp = Date.Now

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "CurrentTime"
                Temp = Date.Now

                Temp = Temp.AddMinutes(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "HH:mm:ss"
                End If
            Case "CurrentDateTime"
                Temp = Date.Now

                Temp = Temp.AddMinutes(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd HH:mm:ss"
                End If
            Case "CurrentMonth"
                Dim nTemp As Integer = Date.Now.Month + nAdjust
                Dim sTemp As String = ""

                If nTemp = 0 Then
                    nTemp = 12
                End If

                Select Case dateFormat
                    Case "MM"
                        If nTemp < 10 Then
                            sTemp = "0" & nTemp.ToString
                        Else
                            sTemp = nTemp.ToString
                        End If
                    Case "MMMM"
                        Temp = New Date(Now.Year, nTemp, 1)

                        sTemp = Temp.ToString("MMMM")
                    Case Else
                        sTemp = nTemp.ToString
                End Select

                Return sTemp
            Case "CurrentScheduleName"
                Return gScheduleName
            Case "Exported File Name"
                Return gExportedFileName
            Case "Key Parameter Value"
                Return gKeyValue
            Case "CurrentMonthName"
                Return Date.Now.AddMonths(nAdjust).ToString("MMMM")
            Case "CurrentYear"
                Return Date.Now.Year + nAdjust
            Case "CurrentDay"
                Return Date.Now.AddDays(nAdjust).Day
            Case "CurrentWeekDayName"
                Return Date.Now.AddDays(nAdjust).DayOfWeek.ToString
            Case "Monday Last Week"

                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Monday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Monday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Monday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Tuesday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Tuesday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Tuesday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Tuesday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Wednesday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Wednesday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Wednesday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Wednesday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Thursday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Thursday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Thursday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Thursday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Friday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Friday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Friday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Friday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Saturday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Saturday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Saturday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Saturday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Sunday Last Week"
                Temp = Date.Now.AddDays(-7)

                If Temp.DayOfWeek > DayOfWeek.Sunday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Sunday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Sunday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Monday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Monday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Monday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Monday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Tuesday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Tuesday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Tuesday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Tuesday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Wednesday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Wednesday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Wednesday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Wednesday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Thursday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Thursday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Thursday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Thursday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Friday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Friday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Friday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Friday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Saturday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Saturday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Saturday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Saturday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Sunday This Week"
                Temp = Date.Now

                If Temp.DayOfWeek > DayOfWeek.Sunday Then
                    Do While Temp.DayOfWeek <> DayOfWeek.Sunday
                        Temp = Temp.AddDays(-1)
                    Loop
                Else
                    Do While Temp.DayOfWeek <> DayOfWeek.Sunday
                        Temp = Temp.AddDays(1)
                    Loop
                End If

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Month Start Last Month"
                Temp = Date.Now.AddMonths(-1)

                Temp = New Date(Temp.Year, Temp.Month, 1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Month End Last Month"
                Temp = Date.Now

                Temp = New Date(Temp.Year, Temp.Month, 1)

                Temp = Temp.AddDays(-1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Year Start Last Year"
                Temp = New Date(Date.Now.Year - 1, 1, 1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Start This Year"
                Temp = New Date(Date.Now.Year, 1, 1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "End Last Year"
                Temp = New Date(Date.Now.Year - 1, 12, 31)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Current Group Value"
                Return gGroupValue
            Case "Last Day of the Month"
                Temp = Date.Now.AddMonths(1)

                Temp = New Date(Temp.Year, Temp.Month, 1)

                Temp = Temp.AddDays(-1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If

            Case "First Day of Next Month"
                Temp = Date.Now.AddMonths(1)

                Temp = New Date(Temp.Year, Temp.Month, 1)

                Temp = Temp.AddDays(nAdjust)

                If dateFormat.Length = 0 Then
                    dateFormat = "yyyy-MM-dd"
                End If
            Case "Current Temp File Path"
                Return clsMarsReport.m_CurrentTempFile
            Case "LastRun"
                Return clsMarsUI.MainUI.GetLastRun(, , , clsMarsReport.m_scheduleID)
            Case "LastResult"
                Return clsMarsUI.MainUI.GetLastResult(, , , clsMarsReport.m_scheduleID)
            Case "LatestRunTime"
                Return ConDateTime(clsMarsScheduler.ScheduleStart)
            Case "LatestResult"
                If gErrorDesc.Length = 0 Then
                    Return "Success"
                Else
                    Dim errorMsg As String

                    errorMsg = "Error Description: " & gErrorDesc & vbCrLf & _
                    "Error Number: " & gErrorNumber & vbCrLf & _
                    "Error Source: " & gErrorSource & vbCrLf & _
                    "Error Line: " & gErrorLine

                    Return "Failed:" & vbCrLf & vbCrLf & errorMsg
                End If
        End Select

        If dateFormat.Length = 0 Then
            Return ConDate(Temp.Date)
        Else
            Return Temp.ToString(dateFormat)
        End If

    End Function

    Public Function ParseDirectory(ByRef sPath As String) As Boolean
        Dim sTemp As String

        If sPath.StartsWith("\\") Then
            Try
                Dim sRemotePath As String

                sRemotePath = sPath.Split("\")(0) & "\" & sPath.Split("\")(1) & "\" & sPath.Split("\")(2) & "\" & sPath.Split("\")(3) & "\"

                For I As Integer = 4 To sPath.Split("\").GetUpperBound(0)
                    sTemp = sPath.Split("\")(I)

                    If sTemp.Length > 0 Then
                        sTemp = sTemp.Replace("/", "").Replace _
                            ("*", "").Replace("""", _
                            "").Replace("?", _
                            "").Replace("<", _
                            "").Replace(">", _
                            "").Replace("|", "")

                        If System.IO.Directory.Exists(sRemotePath & sTemp) = False Then
                            System.IO.Directory.CreateDirectory(sRemotePath & sTemp)
                        End If

                        sRemotePath &= sTemp & "\"
                    End If
                Next

                sPath = sRemotePath
            Catch : End Try

            Return True
        Else
            Try
                For Each s As String In sPath.Split("\")
                    If s.Length > 0 Then

                        s = s.Replace("/", "").Replace _
                            ("*", "").Replace("""", _
                            "").Replace("?", _
                            "").Replace("<", _
                            "").Replace(">", _
                            "").Replace("|", "")

                        sTemp &= s & "\"

                        If System.IO.Directory.Exists(sTemp) = False Then
                            System.IO.Directory.CreateDirectory(sTemp)
                        End If
                    End If
                Next

                sPath = sTemp

                Return True
            Catch
                Return False
            End Try
        End If
    End Function
End Class
