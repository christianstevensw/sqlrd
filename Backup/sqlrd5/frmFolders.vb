Public Class frmFolders
    Inherits System.Windows.Forms.Form
    Dim isCancel As Boolean
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents btnNewFolder As System.Windows.Forms.Button
    Public SelectDeskTop As Boolean = False
    Public m_eventOnly As Boolean = False
    Public m_packageOnly As Boolean = False
    Public m_eventPackage As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tvFolders As System.Windows.Forms.TreeView
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFolders))
        Me.tvFolders = New System.Windows.Forms.TreeView
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.Footer1 = New WizardFooter.Footer
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.btnNewFolder = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'tvFolders
        '
        Me.tvFolders.BackColor = System.Drawing.Color.White
        Me.tvFolders.ImageIndex = 0
        Me.tvFolders.ImageList = Me.imgFolders
        Me.tvFolders.Indent = 19
        Me.tvFolders.ItemHeight = 16
        Me.tvFolders.Location = New System.Drawing.Point(8, 24)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.SelectedImageIndex = 0
        Me.tvFolders.Size = New System.Drawing.Size(296, 264)
        Me.tvFolders.TabIndex = 0
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        '
        'Label1
        '
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(296, 16)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Please select the folder..."
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 328)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(336, 16)
        Me.Footer1.TabIndex = 4
        Me.Footer1.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.Enabled = False
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(152, 344)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(232, 344)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'btnNewFolder
        '
        Me.btnNewFolder.Enabled = False
        Me.btnNewFolder.Image = CType(resources.GetObject("btnNewFolder.Image"), System.Drawing.Image)
        Me.btnNewFolder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewFolder.Location = New System.Drawing.Point(8, 294)
        Me.btnNewFolder.Name = "btnNewFolder"
        Me.btnNewFolder.Size = New System.Drawing.Size(87, 25)
        Me.btnNewFolder.TabIndex = 1
        Me.btnNewFolder.Text = "New Folder"
        Me.btnNewFolder.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewFolder.UseVisualStyleBackColor = True
        '
        'frmFolders
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(314, 378)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnNewFolder)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.tvFolders)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmFolders"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Folders"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        isCancel = True
        Me.Close()
    End Sub

    Private Sub frmFolders_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim oUI As clsMarsUI = New clsMarsUI
        FormatForWinXP(Me)
        oUI.BuildTree(tvFolders, m_eventOnly, False)
        tvFolders.Nodes(0).Expand()
    End Sub
    Public Function GetEventBasedSchedule() As Hashtable

        Me.ShowDialog()

        If isCancel = True Then Return Nothing

        Dim values As Hashtable = New Hashtable

        values.Add("Name", tvFolders.SelectedNode.Text)
        values.Add("ID", Convert.ToString(tvFolders.SelectedNode.Tag).Split(":")(1))

        Return values
    End Function
    Public Function GetFolder(Optional ByVal sTitle As String = "Please select the folder") As String()
        Dim sFolder(1) As String

        Label1.Text = sTitle

        Me.ShowDialog()

        If isCancel = True Then
            sFolder(0) = ""
            sFolder(1) = 0
            Me.Close()
            isCancel = False
            Return sFolder
        End If

        sFolder(0) = tvFolders.SelectedNode.FullPath
        sFolder(1) = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")

        Return sFolder
    End Function
    Public Function GetPackage() As Integer
        Me.ShowDialog()

        If isCancel = True Then Return 0

        Return tvFolders.SelectedNode.Tag.ToString.Split(":")(1)
    End Function

    Private Sub tvFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvFolders.AfterSelect
        Dim sType As String

        sType = GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":")

        If Me.m_packageOnly = True Then

            If sType = "Package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If

            btnNewFolder.Enabled = False
        ElseIf m_eventPackage = True Then
            If sType = "Event-Package" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        ElseIf m_eventOnly = False Then
            If sType = "Folder" Then
                cmdOK.Enabled = True
            ElseIf SelectDeskTop = True Then
                If sType = "Desktop" Then
                    cmdOK.Enabled = True
                End If
            Else
                cmdOK.Enabled = False
            End If

            If sType = "Folder" Or sType = "Desktop" Then
                btnNewFolder.Enabled = True
            Else
                btnNewFolder.Enabled = False
            End If
        Else
            btnNewFolder.Enabled = False

            If sType = "Event" Then
                cmdOK.Enabled = True
            Else
                cmdOK.Enabled = False
            End If
        End If
    End Sub

    Private Sub tvFolders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.Click
        On Error Resume Next

        If GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":") = "Folder" Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        
        Me.Close()
    End Sub

    Private Sub tvFolders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.DoubleClick
        If cmdOK.Enabled = True And tvFolders.SelectedNode.GetNodeCount(True) = 0 Then
            cmdOK_Click(sender, e)
        End If
    End Sub

    Private Sub btnNewFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNewFolder.Click
        On Error Resume Next
        Dim parent As Integer

        parent = Me.tvFolders.SelectedNode.Tag.split(":")(1)

        Dim newFolder As Hashtable = clsMarsUI.MainUI.NewFolder(parent)

        If newFolder Is Nothing Then Return

        clsMarsUI.MainUI.BuildTree(tvFolders, False, False)

        tvFolders.Nodes(0).Expand()

        clsMarsUI.MainUI.FindNode(newFolder.Item("Tag"), tvFolders)

        For Each o As frmWindow In oWindow
            clsMarsUI.MainUI.RefreshView(o)
        Next
    End Sub
End Class
