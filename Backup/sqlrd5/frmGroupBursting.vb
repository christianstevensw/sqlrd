
Public Class frmGroupBursting
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Dim DestinationID As Integer = 99999
    Friend WithEvents Faxing1 As DataTech.FaxManJr.Faxing
    Dim ep As ErrorProvider = New ErrorProvider

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbGroups As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents txtMessage As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtColumn As System.Windows.Forms.TextBox
    Friend WithEvents lsvBursting As System.Windows.Forms.ListView
    Friend WithEvents GroupName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtSendTo As System.Windows.Forms.TextBox
    Friend WithEvents txtCc As System.Windows.Forms.TextBox
    Friend WithEvents txtBcc As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Button1 As System.Windows.Forms.Button

    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdGet As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroupBursting))
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbGroups = New System.Windows.Forms.ComboBox
        Me.txtSendTo = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.txtMessage = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtColumn = New System.Windows.Forms.TextBox
        Me.lsvBursting = New System.Windows.Forms.ListView
        Me.GroupName = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.txtCc = New System.Windows.Forms.TextBox
        Me.txtBcc = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label8 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdGet = New System.Windows.Forms.Button
        Me.Faxing1 = New DataTech.FaxManJr.Faxing
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 80)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(176, 16)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Select Report Groups"
        '
        'cmbGroups
        '
        Me.cmbGroups.ItemHeight = 13
        Me.cmbGroups.Location = New System.Drawing.Point(8, 96)
        Me.cmbGroups.Name = "cmbGroups"
        Me.cmbGroups.Size = New System.Drawing.Size(224, 21)
        Me.cmbGroups.TabIndex = 0
        '
        'txtSendTo
        '
        Me.txtSendTo.Location = New System.Drawing.Point(48, 176)
        Me.txtSendTo.Name = "txtSendTo"
        Me.txtSendTo.Size = New System.Drawing.Size(232, 21)
        Me.txtSendTo.TabIndex = 2
        Me.txtSendTo.Tag = "memo"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 178)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(24, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "To:"
        '
        'cmdAdd
        '
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(304, 140)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(35, 23)
        Me.cmdAdd.TabIndex = 3
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(304, 236)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(32, 23)
        Me.cmdRemove.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 128)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(248, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Column group is based on e.g. Orders.OrderID"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 256)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 16)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Subject"
        '
        'txtSubject
        '
        Me.txtSubject.Location = New System.Drawing.Point(8, 272)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(272, 21)
        Me.txtSubject.TabIndex = 2
        Me.txtSubject.Tag = "memo"
        '
        'txtMessage
        '
        Me.txtMessage.Location = New System.Drawing.Point(8, 312)
        Me.txtMessage.Multiline = True
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMessage.Size = New System.Drawing.Size(272, 160)
        Me.txtMessage.TabIndex = 2
        Me.txtMessage.Tag = "memo"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 296)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Message"
        '
        'txtColumn
        '
        Me.txtColumn.Location = New System.Drawing.Point(8, 144)
        Me.txtColumn.Name = "txtColumn"
        Me.txtColumn.Size = New System.Drawing.Size(272, 21)
        Me.txtColumn.TabIndex = 2
        '
        'lsvBursting
        '
        Me.lsvBursting.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.GroupName, Me.ColumnHeader1})
        Me.lsvBursting.FullRowSelect = True
        Me.lsvBursting.GridLines = True
        Me.lsvBursting.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvBursting.Location = New System.Drawing.Point(352, 92)
        Me.lsvBursting.MultiSelect = False
        Me.lsvBursting.Name = "lsvBursting"
        Me.lsvBursting.Size = New System.Drawing.Size(328, 376)
        Me.lsvBursting.TabIndex = 4
        Me.lsvBursting.UseCompatibleStateImageBehavior = False
        Me.lsvBursting.View = System.Windows.Forms.View.Details
        '
        'GroupName
        '
        Me.GroupName.Text = "Group Value"
        Me.GroupName.Width = 150
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Send To"
        Me.ColumnHeader1.Width = 168
        '
        'txtCc
        '
        Me.txtCc.Location = New System.Drawing.Point(48, 200)
        Me.txtCc.Name = "txtCc"
        Me.txtCc.Size = New System.Drawing.Size(232, 21)
        Me.txtCc.TabIndex = 2
        '
        'txtBcc
        '
        Me.txtBcc.Location = New System.Drawing.Point(48, 224)
        Me.txtBcc.Name = "txtBcc"
        Me.txtBcc.Size = New System.Drawing.Size(232, 21)
        Me.txtBcc.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 202)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(24, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Cc:"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 226)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(32, 16)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Bcc:"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(608, 488)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Finish"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(704, 64)
        Me.Panel1.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Arial", 15.75!)
        Me.Label8.ForeColor = System.Drawing.Color.Navy
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(200, 23)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Group Bursting"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(624, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 48)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-8, 472)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(728, 8)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.White
        Me.GroupBox2.Location = New System.Drawing.Point(-24, 64)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(728, 8)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "3dline"
        '
        'cmdGet
        '
        Me.cmdGet.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdGet.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdGet.Location = New System.Drawing.Point(240, 96)
        Me.cmdGet.Name = "cmdGet"
        Me.cmdGet.Size = New System.Drawing.Size(40, 21)
        Me.cmdGet.TabIndex = 9
        Me.cmdGet.Text = "..."
        '
        'Faxing1
        '
        Me.Faxing1.LocalID = ""
        Me.Faxing1.ReceiveDir = ""
        '
        'frmGroupBursting
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(690, 520)
        Me.Controls.Add(Me.cmdGet)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lsvBursting)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.txtSendTo)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cmbGroups)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdRemove)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtSubject)
        Me.Controls.Add(Me.txtMessage)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtColumn)
        Me.Controls.Add(Me.txtCc)
        Me.Controls.Add(Me.txtBcc)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmGroupBursting"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Group Bursting"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If cmbGroups.Text.Length = 0 Then
            ep.SetError(cmbGroups, "Please select the group name")
            cmbGroups.Focus()
            Return
        ElseIf txtColumn.Text.Length = 0 Then
            ep.SetError(txtColumn, "Please specify the column the group is based on")
            txtColumn.Focus()
            Return
        ElseIf txtSendTo.Text.Length = 0 Then
            ep.SetError(txtSendTo, "Please specify the recipient email address")
            txtSendTo.Focus()
            Return
        End If

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer = clsMarsData.CreateDataID("burstingattr", "burstid")

        sCols = "BurstID,DestinationID,BurstType,GroupColumn,GroupValue,SendTo,Subject,Message,SendCC,SendBCC"

        sVals = nID & "," & _
        DestinationID & "," & _
        "'Email'," & _
        "'" & SQLPrepare(txtColumn.Text) & "'," & _
        "'" & SQLPrepare(cmbGroups.Text) & "'," & _
        "'" & SQLPrepare(txtSendTo.Text) & "'," & _
        "'" & SQLPrepare(txtSubject.Text) & "'," & _
        "'" & SQLPrepare(txtMessage.Text) & "'," & _
        "'" & SQLPrepare(txtCc.Text) & "'," & _
        "'" & SQLPrepare(txtBcc.Text) & "'"

        SQL = "INSERT INTO BurstingAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Dim oItem As New ListViewItem

            oItem.Text = cmbGroups.Text
            oItem.Tag = nID
            oItem.SubItems.Add(txtSendTo.Text)
            lsvBursting.Items.Add(oItem)
        End If

    End Sub


    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvBursting.SelectedItems.Count = 0 Then Return

        Dim SQL As String

        SQL = "DELETE FROM BurstingAttr WHERE BurstID =" & lsvBursting.SelectedItems(0).Tag

        If clsMarsData.WriteData(SQL) = True Then
            lsvBursting.SelectedItems(0).Remove()
        End If
    End Sub

    Public Sub ConfigureBursting(Optional ByVal nDestinationID As Integer = 99999)
        DestinationID = nDestinationID

        Dim SQL As String = "SELECT * FROM BurstingAttr WHERE DestinationID =" & DestinationID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim oItem As ListViewItem

        Do While oRs.EOF = False
            oItem = New ListViewItem

            oItem.Text = oRs("groupvalue").Value
            oItem.Tag = oRs("burstid").Value
            oItem.SubItems.Add(oRs("sendto").Value)

            lsvBursting.Items.Add(oItem)
            oRs.MoveNext()
        Loop

        oRs.Close()

        Me.ShowDialog()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Close()
    End Sub

    Private Sub frmGroupBursting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub


   
End Class
