Public Class frmEventWizard
    Inherits System.Windows.Forms.Form
    Dim nStep As Int16 = 1
    Const S1 As String = "Step 1: Schedule Name"
    Const S2 As String = "Step 2: Condition Definition"
    Const S3 As String = "Step 3: Select schedule(s)"
    Const S4 As String = "Step 4: Custom Tasks"
    Dim oUI As New clsMarsUI
    Dim oData As New clsMarsData
    Dim ep As New ErrorProvider
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents lblMain As System.Windows.Forms.Label
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents tvFolders As System.Windows.Forms.TreeView
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents UcEvents As sqlrd.ucEvents
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    Friend WithEvents Footer1 As WizardFooter.Footer
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventWizard))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblMain = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Step2 = New System.Windows.Forms.Panel
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.UcEvents = New sqlrd.ucEvents
        Me.Step3 = New System.Windows.Forms.Panel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.tvFolders = New System.Windows.Forms.TreeView
        Me.Step4 = New System.Windows.Forms.Panel
        Me.UcTasks = New sqlrd.ucTasks
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuParameters = New System.Windows.Forms.ContextMenu
        Me.mnuParameter = New System.Windows.Forms.MenuItem
        Me.Footer1 = New WizardFooter.Footer
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblMain)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(482, 64)
        Me.Panel1.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(424, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 50)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'lblMain
        '
        Me.lblMain.Font = New System.Drawing.Font("Tahoma", 14.25!)
        Me.lblMain.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMain.Location = New System.Drawing.Point(16, 24)
        Me.lblMain.Name = "lblMain"
        Me.lblMain.Size = New System.Drawing.Size(400, 23)
        Me.lblMain.TabIndex = 1
        Me.lblMain.Text = "Label1"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 64)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(482, 8)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Location = New System.Drawing.Point(0, 80)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(480, 424)
        Me.Step1.TabIndex = 2
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(8, 120)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(360, 88)
        Me.txtDesc.TabIndex = 31
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 216)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 232)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 28
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(312, 72)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 13
        Me.cmdLoc.Text = "...."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 72)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 12
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 24)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 10
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Schedule Name"
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.chkStatus)
        Me.Step2.Controls.Add(Me.UcEvents)
        Me.Step2.Location = New System.Drawing.Point(0, 80)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(480, 424)
        Me.Step2.TabIndex = 3
        '
        'chkStatus
        '
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStatus.Location = New System.Drawing.Point(8, 400)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(104, 24)
        Me.chkStatus.TabIndex = 13
        Me.chkStatus.Text = "Enabled"
        '
        'UcEvents
        '
        Me.UcEvents.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcEvents.Location = New System.Drawing.Point(8, 8)
        Me.UcEvents.Name = "UcEvents"
        Me.UcEvents.Size = New System.Drawing.Size(464, 400)
        Me.UcEvents.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.GroupBox3)
        Me.Step3.Location = New System.Drawing.Point(0, 80)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(480, 424)
        Me.Step3.TabIndex = 4
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Controls.Add(Me.Splitter1)
        Me.GroupBox3.Controls.Add(Me.tvFolders)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(464, 400)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lsvSchedules)
        Me.Panel2.Controls.Add(Me.cmdRemove)
        Me.Panel2.Controls.Add(Me.cmdAdd)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(171, 17)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(290, 380)
        Me.Panel2.TabIndex = 2
        '
        'lsvSchedules
        '
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Right
        Me.lsvSchedules.LargeImageList = Me.imgFolders
        Me.lsvSchedules.Location = New System.Drawing.Point(82, 0)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(208, 380)
        Me.lsvSchedules.SmallImageList = Me.imgFolders
        Me.lsvSchedules.TabIndex = 7
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 194
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        Me.imgFolders.Images.SetKeyName(12, "")
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(16, 136)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemove.TabIndex = 5
        Me.cmdRemove.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdAdd
        '
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(16, 80)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(40, 24)
        Me.cmdAdd.TabIndex = 6
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Splitter1
        '
        Me.Splitter1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Splitter1.Location = New System.Drawing.Point(168, 17)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 380)
        Me.Splitter1.TabIndex = 1
        Me.Splitter1.TabStop = False
        '
        'tvFolders
        '
        Me.tvFolders.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvFolders.ImageIndex = 0
        Me.tvFolders.ImageList = Me.imgFolders
        Me.tvFolders.Indent = 19
        Me.tvFolders.ItemHeight = 16
        Me.tvFolders.Location = New System.Drawing.Point(3, 17)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.SelectedImageIndex = 0
        Me.tvFolders.Size = New System.Drawing.Size(165, 380)
        Me.tvFolders.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.UcTasks)
        Me.Step4.Location = New System.Drawing.Point(0, 80)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(480, 424)
        Me.Step4.TabIndex = 5
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(8, 8)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(464, 400)
        Me.UcTasks.TabIndex = 0
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(401, 519)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 32)
        Me.cmdFinish.TabIndex = 27
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(401, 519)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 32)
        Me.cmdNext.TabIndex = 23
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(321, 519)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 32)
        Me.cmdCancel.TabIndex = 25
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(241, 519)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 32)
        Me.cmdBack.TabIndex = 24
        Me.cmdBack.Text = "&Back"
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 503)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(504, 16)
        Me.Footer1.TabIndex = 28
        '
        'frmEventWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(482, 559)
        Me.ControlBox = False
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Footer1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmEventWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Schedule Wizard"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    
    Private Sub frmEventWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.e1_BasicEventsPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Close()
            Return
        End If

        FormatForWinXP(Me)

        Step1.BringToFront()

        oUI.BuildTree(tvFolders, True)

        tvFolders.SelectedNode = tvFolders.Nodes(0)

        tvFolders.Nodes(0).Expand()

        lblMain.Text = S1

        UcTasks.ShowAfterType = False

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Select Case nStep
            Case 1
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please enter the name of the schedule")
                    txtName.Focus()
                    Return
                ElseIf txtFolder.Text.Length = 0 Then
                    ep.SetError(txtFolder, "Please select the schedule's destination folder")
                    txtFolder.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("EventAttr", "EventName", _
                txtName.Text, True) = True Then
                    ep.SetError(txtName, "An event based schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                Step2.BringToFront()
                lblMain.Text = S2
                cmdBack.Enabled = True
            Case 2

                With UcEvents
                    If .cmbValue.Text.Length = 0 Then
                        .ep.SetError(.cmbValue, "Please select desired result")
                        .cmbValue.Focus()
                        Return
                    End If

                    Select Case .cmbcondition.Text.ToLower
                        Case "file exists"
                            If .txtPath.Text.Length = 0 Then
                                .ep.SetError(.txtPath, "Please select the file path")
                                .txtPath.Focus()
                                Return
                            End If
                        Case "file has been modified"
                            If .txtModPath.Text.Length = 0 Then
                                .ep.SetError(.txtModPath, "Please select the file path")
                                .txtModPath.Focus()
                                Return
                            End If
                        Case "database file exists"
                            If .SQLValid = False Then
                                ep.SetError(.txtQuery, "Please parse the query")
                                .cmdParse.Focus()
                                Return
                            End If
                        Case "window is present"
                            If .cmbWindowName.Text.Length = 0 Then
                                .ep.SetError(.cmbWindowName, "Please enter the window name")
                                .cmbWindowName.Focus()
                                Return
                            End If
                        Case "process exists"
                            If .cmbProcess.Text.Length = 0 Then
                                .ep.SetError(.cmbProcess, "Please select the window name")
                                .cmbProcess.Focus()
                                Return
                            End If
                        Case "unread email is present"
                            If .txtServer.Text.Length = 0 Then
                                .ep.SetError(.txtServer, "Please enter the POP3 server name")
                                .txtServer.Focus()
                                Return
                            ElseIf .txtUserID.Text.Length = 0 Then
                                .ep.SetError(.txtUserID, "Please enter the POP3 userid")
                                .txtUserID.Focus()
                                Return
                            ElseIf .txtPassword.Text.Length = 0 Then
                                .ep.SetError(.txtPassword, "Please enter the POP3 password")
                                .txtPassword.Focus()
                                Return
                            ElseIf .txtMessage.Text.Length = 0 And .txtSubject.Text.Length = 0 Then
                                .ep.SetError(.txtMessage, "Please enter a search criteria for the subject or the message")
                                .txtMessage.Focus()
                                Return
                            End If

                            lsvSchedules.ContextMenu = mnuParameters

                        Case "database record has changed"

                            With .UcDSN1
                                If .cmbDSN.Text.Length = 0 Then
                                    .ep.SetError(.cmbDSN, "Please select the DSN data source")
                                    .cmbDSN.Focus()
                                    Return
                                End If
                            End With

                            If .optTable.Checked = True Then
                                If .cmbTableName.Text.Length = 0 Then
                                    .ep.SetError(.cmbTableName, "Please select the table name")
                                    .cmbTableName.Focus()
                                    Return
                                ElseIf .cmbColumn.Text.Length = 0 Then
                                    .ep.SetError(.cmbColumn, "Please select the key column")
                                    .cmbColumn.Focus()
                                    Return
                                End If
                            ElseIf .optRecordset.Checked = True Then
                                If .txtRsQuery.Text.Length = 0 Then
                                    .ep.SetError(.txtRsQuery, "Please enter the SQL query for the recordset")
                                    .txtRsQuery.Focus()
                                    Return
                                ElseIf .cmbRsColumn.Text.Length = 0 Then
                                    .ep.SetError(.cmbRsColumn, "Please select the key column")
                                    .cmbRsColumn.Focus()
                                    Return
                                End If
                            End If
                    End Select
                End With

                Step3.BringToFront()
                lblMain.Text = S3
            Case 3
                If lsvSchedules.Items.Count = 0 Then
                    ep.SetError(cmdAdd, "Please add a schedules to execute")
                    cmdAdd.Focus()
                    Return
                End If

                Step4.BringToFront()
                lblMain.Text = S4
                cmdNext.Visible = False
                cmdFinish.Visible = True

        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 4
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Step3.BringToFront()
                lblMain.Text = S3
            Case 3
                Step2.BringToFront()
                lblMain.Text = S2
            Case 2
                Step1.BringToFront()
                lblMain.Text = S1
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If tvFolders.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode
        Dim oItem As ListViewItem
        Dim sType As String

        oNode = tvFolders.SelectedNode

        sType = oNode.Tag

        sType = sType.Split(":")(0).ToLower

        If sType = "folder" Or sType = "desktop" Or sType = "smartfolder" Then Return

        For Each oItem In lsvSchedules.Items
            If oItem.Text = oNode.Text And oItem.Tag = oNode.Tag Then
                Return
            End If
        Next

        oItem = New ListViewItem

        oItem.Text = oNode.Text
        oItem.Tag = oNode.Tag
        oItem.ImageIndex = oNode.ImageIndex

        lsvSchedules.Items.Add(oItem)

    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Exit Sub

        For Each oItem As ListViewItem In lsvSchedules.SelectedItems
            clsmarsdata.WriteData("DELETE FROM EventSubAttr WHERE ReportID=" & oItem.Tag.split(":")(1), False)
            oItem.Remove()
        Next
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Try
            Dim SQL As String
            Dim sVals As String
            Dim sCols As String
            Dim nParent As Integer
            Dim sEvent As String = UcEvents.cmbcondition.Text.ToLower
            Dim nFire As Int16
            Dim I As Integer = 1

            If UcEvents.cmbValue.Text = "TRUE" Then
                nFire = 1
            Else
                nFire = 0
            End If

            cmdFinish.Enabled = False
            oUI.BusyProgress(40, "Configuring conditions...")

            nParent = txtFolder.Tag

            Select Case sEvent
                Case "file exists"
                    sCols = "filepath"

                    sVals = "'" & SQLPrepare(UcEvents.txtPath.Text) & "'"

                Case "file has been modified"
                    sCols = "filepath,moddatetime"

                    Dim d As Date

                    d = System.IO.File.GetLastWriteTime(UcEvents.txtModPath.Text)

                    sVals = "'" & SQLPrepare(UcEvents.txtModPath.Text) & "'," & _
                    "'" & d & "'"
                Case "database record exists"

                    Dim sCon As String

                    With UcEvents.UcDSN
                        sCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _
                        .txtPassword.Text & "|"
                    End With

                    sCols = "search1,search2,FilePath"

                    sVals = "'" & sCon & "'," & _
                    "'" & SQLPrepare(UcEvents.txtQuery.Text) & "'," & _
                    "'" & SQLPrepare(UcEvents.cmbKey.Text) & "'"

                    'cache the recordset
                    Dim oEvent As New clsMarsEvent

                    'oEvent.CacheData(txtName.Text, UcEvents.txtQuery.Text, sCon)

                Case "window is present"

                    sCols = "search1"

                    sVals = "'" & SQLPrepare(UcEvents.cmbWindowName.Text) & "'"
                Case "process exists"
                    sCols = "search1"

                    sVals = "'" & SQLPrepare(UcEvents.cmbProcess.Text) & "'"
                Case "unread email is present"
                    sCols = "search1,search2,popserver,popuserid,poppassword," & _
                    "popremove"

                    With UcEvents
                        sVals = "'" & SQLPrepare(.txtSubject.Text) & "'," & _
                        "'" & SQLPrepare(.txtMessage.Text) & "'," & _
                        "'" & SQLPrepare(.txtServer.Text) & "'," & _
                        "'" & SQLPrepare(.txtUserID.Text) & "'," & _
                        "'" & SQLPrepare(.txtPassword.Text) & "'," & _
                        Convert.ToInt32(.chkRemove.Checked)
                    End With
                Case "database record has changed"
                    Dim oEvent As New clsMarsEvent
                    Dim sCon As String
                    Dim sQuery As String
                    Dim KeyColumn As String
                    Dim sRecordType As String

                    With UcEvents.UcDSN1
                        sCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _
                        .txtPassword.Text & "|"
                    End With

                    If UcEvents.optTable.Checked = True Then
                        sQuery = "SELECT * FROM " & UcEvents.cmbTableName.Text
                        KeyColumn = UcEvents.cmbColumn.Text
                        sRecordType = "Table"
                    Else
                        sQuery = UcEvents.txtRsQuery.Text
                        KeyColumn = UcEvents.cmbRsColumn.Text
                        sRecordType = "Recordset"
                    End If

                    oUI.BusyProgress(60, "Caching data from selected datasource...")


                    'If oEvent.CacheData(txtName.Text, sQuery, scon, KeyColumn) = False Then
                    '    cmdFinish.Enabled = True
                    '    oUI.BusyProgress(, , True)
                    '    Exit Sub
                    'End If

                    sCols = "Search1, Search2,FilePath,PopRemove,PopServer"

                    'Search1 = Connection string
                    'Search2 = Query string
                    'FilePath = KeyColumn
                    'PopRemove = Include new data 
                    'PopServer = Recordtype

                    sVals = "'" & scon & "'," & _
                    "'" & SQLPrepare(sQuery) & "'," & _
                    "'" & KeyColumn & "'," & _
                    Convert.ToInt32(UcEvents.chkTreat.Checked) & "," & _
                    "'" & sRecordType & "'"

            End Select

            oUI.BusyProgress(70, "Saving conditions...")

            sCols = "status,eventid,eventname,parent," & _
            "eventtype,firewhen,owner,description,keyword," & _
            sCols

            Dim nEventID As Int64 = clsMarsData.CreateDataID("eventattr", "eventid")

            sVals = Convert.ToInt32(chkStatus.Checked) & "," & _
            nEventID & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            nParent & "," & _
            "'" & sEvent & "'," & _
            nFire & "," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(txtDesc.Text) & "'," & _
            "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
            sVals

            SQL = "INSERT INTO EventAttr(" & sCols & ") VALUES (" & sVals & ")"

            If clsmarsdata.WriteData(SQL) = True Then
                Dim nID As Integer
                Dim sType As String

                sCols = "ID,EventID,ScheduleID,ScheduleType"

                oUI.BusyProgress(90, "Saving schedules for event...")

                For Each oItem As ListViewItem In lsvSchedules.Items
                    nID = oItem.Tag.Split(":")(1)
                    sType = oItem.Tag.Split(":")(0)


                    sVals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                    nEventID & "," & _
                    nID & "," & _
                    "'" & sType & "'"

                    SQL = "INSERT INTO EventSchedule(" & sCols & ") VALUES(" & sVals & ")"

                    If clsmarsdata.WriteData(SQL) = False Then
                        Exit For
                    End If

                    I += 1
                Next
            End If

            oUI.BusyProgress(100, "Cleaning up...")

            clsMarsData.WriteData("UPDATE Tasks SET ScheduleID = " & nEventID & " WHERE " & _
            "ScheduleID =99999")

            clsMarsData.WriteData("UPDATE EventSubAttr SET EventID =" & nEventID & " WHERE EventID = 99999")

            Close()

            oUI.BusyProgress(10, , True)

            oUI.RefreshView(oWindow(nWindowCurrent))

            Try
                Dim nCount As Integer
                nCount = txtFolder.Text.Split("\").GetUpperBound(0)

                Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

                oUI.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))
            Catch

            End Try

            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.CREATE)
        Catch ex As Exception
            cmdFinish.Enabled = True
            oUI.BusyProgress(, , True)
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        oData.CleanDB()
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = lsvSchedules.SelectedItems(0)

        sType = oItem.Tag.Split(":")(0)
        nID = oItem.Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub

        oSub.SetSubstitution(nID, 99999)

    End Sub

    Private Sub lsvSchedules_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvSchedules.SelectedIndexChanged

    End Sub
End Class
