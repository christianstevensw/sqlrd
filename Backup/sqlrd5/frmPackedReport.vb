#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmPackedReport

    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oData As clsMarsData = New clsMarsData
    Public sDestination As String
    Dim ReportID As Integer = 99999
    Dim PackID As Integer = 99999
    Dim oField As TextBox
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim parCollection As Hashtable
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Dim ReportRestore() As Boolean
    Dim eventReport As Boolean = False
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable

    Friend WithEvents pnFormat As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtUrl As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents btnRefreshDS As System.Windows.Forms.Button
    Dim ucDest As sqlrd.ucDestination
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents cmdLoginTest2 As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Dim m_Mode As String = ""
    Public IsDataDriven As Boolean
    Public m_serverUrl As String = ""
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents tbPack As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtReportName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDBLocation As System.Windows.Forms.TextBox
    Friend WithEvents cmdDBLoc As System.Windows.Forms.Button
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkCustomExt As System.Windows.Forms.CheckBox
    Friend WithEvents txtCustomName As System.Windows.Forms.TextBox
    Friend WithEvents optNaming As System.Windows.Forms.RadioButton
    Friend WithEvents optCustomName As System.Windows.Forms.RadioButton
    Friend WithEvents txtCustomExt As System.Windows.Forms.TextBox
    Friend WithEvents chkAppendDateTime As System.Windows.Forms.CheckBox
    Friend WithEvents lsvParameters As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents cmbFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents chkEmbed As System.Windows.Forms.CheckBox
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents cmdParameters As System.Windows.Forms.Button
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents txtDefault As System.Windows.Forms.TextBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdLoginTest As System.Windows.Forms.Button
    Friend WithEvents cmdProps As System.Windows.Forms.Button
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdRequery As System.Windows.Forms.Button
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackedReport))
        Me.tbPack = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtUrl = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.pnFormat = New System.Windows.Forms.TableLayoutPanel
        Me.Label22 = New System.Windows.Forms.Label
        Me.cmbFormat = New System.Windows.Forms.ComboBox
        Me.cmdProps = New System.Windows.Forms.Button
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.chkEmbed = New System.Windows.Forms.CheckBox
        Me.cmdDBLoc = New System.Windows.Forms.Button
        Me.txtReportName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDBLocation = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.cmdLoginTest2 = New System.Windows.Forms.Button
        Me.cmdRequery = New System.Windows.Forms.Button
        Me.lsvParameters = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.cmdParameters = New System.Windows.Forms.Button
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.btnRefreshDS = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lsvDatasources = New System.Windows.Forms.ListView
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.cmdLoginTest = New System.Windows.Forms.Button
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown
        Me.Label26 = New System.Windows.Forms.Label
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.chkCustomExt = New System.Windows.Forms.CheckBox
        Me.txtCustomName = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.optNaming = New System.Windows.Forms.RadioButton
        Me.optCustomName = New System.Windows.Forms.RadioButton
        Me.txtCustomExt = New System.Windows.Forms.TextBox
        Me.chkAppendDateTime = New System.Windows.Forms.CheckBox
        Me.txtDefault = New System.Windows.Forms.TextBox
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Footer1 = New WizardFooter.Footer
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.bgWorker = New System.ComponentModel.BackgroundWorker
        Me.tbPack.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.pnFormat.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.mnuDatasources.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tbPack
        '
        Me.tbPack.Controls.Add(Me.TabPage1)
        Me.tbPack.Controls.Add(Me.TabPage2)
        Me.tbPack.Controls.Add(Me.TabPage4)
        Me.tbPack.Controls.Add(Me.TabPage3)
        Me.tbPack.Controls.Add(Me.TabPage5)
        Me.tbPack.ItemSize = New System.Drawing.Size(45, 18)
        Me.tbPack.Location = New System.Drawing.Point(8, 8)
        Me.tbPack.Name = "tbPack"
        Me.tbPack.SelectedIndex = 0
        Me.tbPack.Size = New System.Drawing.Size(451, 344)
        Me.tbPack.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(443, 318)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Report"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtUrl)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.pnFormat)
        Me.GroupBox1.Controls.Add(Me.chkStatus)
        Me.GroupBox1.Controls.Add(Me.chkEmbed)
        Me.GroupBox1.Controls.Add(Me.cmdDBLoc)
        Me.GroupBox1.Controls.Add(Me.txtReportName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtDBLocation)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(431, 304)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.txtUrl.Location = New System.Drawing.Point(6, 35)
        Me.txtUrl.Name = "txtUrl"
        Me.txtUrl.Size = New System.Drawing.Size(358, 21)
        Me.txtUrl.TabIndex = 0
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(6, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(229, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Report  Service URL"
        '
        'pnFormat
        '
        Me.pnFormat.ColumnCount = 2
        Me.pnFormat.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.78571!))
        Me.pnFormat.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.21428!))
        Me.pnFormat.Controls.Add(Me.Label22, 0, 0)
        Me.pnFormat.Controls.Add(Me.cmbFormat, 0, 1)
        Me.pnFormat.Controls.Add(Me.cmdProps, 1, 1)
        Me.pnFormat.Location = New System.Drawing.Point(6, 148)
        Me.pnFormat.Name = "pnFormat"
        Me.pnFormat.RowCount = 2
        Me.pnFormat.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnFormat.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.pnFormat.Size = New System.Drawing.Size(280, 45)
        Me.pnFormat.TabIndex = 4
        '
        'Label22
        '
        Me.Label22.BackColor = System.Drawing.Color.Transparent
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label22.ForeColor = System.Drawing.Color.Navy
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(3, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 16)
        Me.Label22.TabIndex = 10
        Me.Label22.Text = "Format"
        '
        'cmbFormat
        '
        Me.cmbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFormat.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmbFormat.ForeColor = System.Drawing.Color.Blue
        Me.cmbFormat.ItemHeight = 13
        Me.cmbFormat.Items.AddRange(New Object() {"Acrobat Format (*.pdf)", "CSV (*.csv)", "Custom (*.*)", "Data Interchange Format (*.dif)", "dBase II (*.dbf)", "dBase III (*.dbf)", "dBase IV (*.dbf)", "HTML (*.htm)", "Lotus 1-2-3 (*.wk1)", "Lotus 1-2-3 (*.wk3)", "Lotus 1-2-3 (*.wk4)", "Lotus 1-2-3 (*.wks)", "MS Excel 97-2000 (*.xls)", "MS Word (*.doc)", "Rich Text Format (*.rtf)", "Text (*.txt)", "TIFF (*.tif)", "Web Archive (*.mhtml)", "XML (*.xml)"})
        Me.cmbFormat.Location = New System.Drawing.Point(3, 19)
        Me.cmbFormat.Name = "cmbFormat"
        Me.cmbFormat.Size = New System.Drawing.Size(208, 21)
        Me.cmbFormat.Sorted = True
        Me.cmbFormat.TabIndex = 0
        '
        'cmdProps
        '
        Me.cmdProps.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdProps.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdProps.Location = New System.Drawing.Point(218, 19)
        Me.cmdProps.Name = "cmdProps"
        Me.cmdProps.Size = New System.Drawing.Size(32, 21)
        Me.cmdProps.TabIndex = 1
        Me.cmdProps.Text = "..."
        '
        'chkStatus
        '
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStatus.Location = New System.Drawing.Point(8, 199)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(104, 24)
        Me.chkStatus.TabIndex = 5
        Me.chkStatus.Text = "Enabled"
        '
        'chkEmbed
        '
        Me.chkEmbed.BackColor = System.Drawing.SystemColors.Control
        Me.chkEmbed.Enabled = False
        Me.chkEmbed.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkEmbed.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.chkEmbed.ForeColor = System.Drawing.Color.Navy
        Me.chkEmbed.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmbed.Location = New System.Drawing.Point(192, 280)
        Me.chkEmbed.Name = "chkEmbed"
        Me.chkEmbed.Size = New System.Drawing.Size(112, 16)
        Me.chkEmbed.TabIndex = 6
        Me.chkEmbed.Text = "Embed into email"
        Me.chkEmbed.UseVisualStyleBackColor = False
        Me.chkEmbed.Visible = False
        '
        'cmdDBLoc
        '
        Me.cmdDBLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDBLoc.Image = CType(resources.GetObject("cmdDBLoc.Image"), System.Drawing.Image)
        Me.cmdDBLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDBLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDBLoc.Location = New System.Drawing.Point(310, 78)
        Me.cmdDBLoc.Name = "cmdDBLoc"
        Me.cmdDBLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDBLoc.TabIndex = 2
        Me.cmdDBLoc.Text = "..."
        '
        'txtReportName
        '
        Me.txtReportName.ForeColor = System.Drawing.Color.Blue
        Me.txtReportName.Location = New System.Drawing.Point(6, 121)
        Me.txtReportName.Name = "txtReportName"
        Me.txtReportName.Size = New System.Drawing.Size(360, 21)
        Me.txtReportName.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 102)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(280, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Report Name"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Report Location"
        '
        'txtDBLocation
        '
        Me.txtDBLocation.ForeColor = System.Drawing.Color.Blue
        Me.txtDBLocation.Location = New System.Drawing.Point(6, 78)
        Me.txtDBLocation.Name = "txtDBLocation"
        Me.txtDBLocation.ReadOnly = True
        Me.txtDBLocation.Size = New System.Drawing.Size(298, 21)
        Me.txtDBLocation.TabIndex = 1
        Me.txtDBLocation.Tag = "memo"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.cmdLoginTest2)
        Me.TabPage2.Controls.Add(Me.cmdRequery)
        Me.TabPage2.Controls.Add(Me.lsvParameters)
        Me.TabPage2.Controls.Add(Me.cmdParameters)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(443, 318)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Parameters"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'cmdLoginTest2
        '
        Me.cmdLoginTest2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoginTest2.Image = CType(resources.GetObject("cmdLoginTest2.Image"), System.Drawing.Image)
        Me.cmdLoginTest2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoginTest2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest2.Location = New System.Drawing.Point(364, 286)
        Me.cmdLoginTest2.Name = "cmdLoginTest2"
        Me.cmdLoginTest2.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoginTest2.TabIndex = 3
        Me.cmdLoginTest2.Text = "Preview"
        Me.cmdLoginTest2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdRequery
        '
        Me.cmdRequery.Enabled = False
        Me.cmdRequery.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRequery.Image = CType(resources.GetObject("cmdRequery.Image"), System.Drawing.Image)
        Me.cmdRequery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRequery.Location = New System.Drawing.Point(320, 286)
        Me.cmdRequery.Name = "cmdRequery"
        Me.cmdRequery.Size = New System.Drawing.Size(40, 23)
        Me.cmdRequery.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.cmdRequery, "Requery the report file for parameters")
        '
        'lsvParameters
        '
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lsvParameters.ForeColor = System.Drawing.Color.Blue
        Me.lsvParameters.FullRowSelect = True
        Me.lsvParameters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvParameters.Location = New System.Drawing.Point(8, 8)
        Me.lsvParameters.Name = "lsvParameters"
        Me.lsvParameters.Size = New System.Drawing.Size(431, 272)
        Me.lsvParameters.TabIndex = 0
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Parameter Name"
        Me.ColumnHeader1.Width = 122
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Parameter Value"
        Me.ColumnHeader2.Width = 185
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Multi-Value"
        Me.ColumnHeader3.Width = 79
        '
        'cmdParameters
        '
        Me.cmdParameters.BackColor = System.Drawing.SystemColors.Control
        Me.cmdParameters.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdParameters.ForeColor = System.Drawing.Color.Navy
        Me.cmdParameters.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParameters.Location = New System.Drawing.Point(8, 286)
        Me.cmdParameters.Name = "cmdParameters"
        Me.cmdParameters.Size = New System.Drawing.Size(75, 23)
        Me.cmdParameters.TabIndex = 1
        Me.cmdParameters.Text = "Parameters"
        Me.cmdParameters.UseVisualStyleBackColor = False
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.btnRefreshDS)
        Me.TabPage4.Controls.Add(Me.GroupBox3)
        Me.TabPage4.Controls.Add(Me.cmdLoginTest)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(443, 318)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Datasources"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnRefreshDS
        '
        Me.btnRefreshDS.BackColor = System.Drawing.SystemColors.Control
        Me.btnRefreshDS.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRefreshDS.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.btnRefreshDS.ForeColor = System.Drawing.Color.Navy
        Me.btnRefreshDS.Image = CType(resources.GetObject("btnRefreshDS.Image"), System.Drawing.Image)
        Me.btnRefreshDS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRefreshDS.Location = New System.Drawing.Point(323, 286)
        Me.btnRefreshDS.Name = "btnRefreshDS"
        Me.btnRefreshDS.Size = New System.Drawing.Size(32, 23)
        Me.btnRefreshDS.TabIndex = 3
        Me.btnRefreshDS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.btnRefreshDS, "Refresh report datasources")
        Me.btnRefreshDS.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lsvDatasources)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(428, 272)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Report Datasources"
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.FullRowSelect = True
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.Location = New System.Drawing.Point(6, 20)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.lsvDatasources.Size = New System.Drawing.Size(405, 246)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
                    " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 0
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Datasource Name"
        Me.ColumnHeader4.Width = 274
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Name"
        Me.ColumnHeader5.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'cmdLoginTest
        '
        Me.cmdLoginTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoginTest.Image = CType(resources.GetObject("cmdLoginTest.Image"), System.Drawing.Image)
        Me.cmdLoginTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoginTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoginTest.Location = New System.Drawing.Point(361, 286)
        Me.cmdLoginTest.Name = "cmdLoginTest"
        Me.cmdLoginTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdLoginTest.TabIndex = 2
        Me.cmdLoginTest.Text = "Preview"
        Me.cmdLoginTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(443, 318)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Naming (Optional)"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox2.Controls.Add(Me.Label26)
        Me.GroupBox2.Controls.Add(Me.cmbDateTime)
        Me.GroupBox2.Controls.Add(Me.chkCustomExt)
        Me.GroupBox2.Controls.Add(Me.txtCustomName)
        Me.GroupBox2.Controls.Add(Me.optNaming)
        Me.GroupBox2.Controls.Add(Me.optCustomName)
        Me.GroupBox2.Controls.Add(Me.txtCustomExt)
        Me.GroupBox2.Controls.Add(Me.chkAppendDateTime)
        Me.GroupBox2.Controls.Add(Me.txtDefault)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(431, 304)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(253, 220)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 9
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(8, 224)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 19
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(253, 185)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(168, 21)
        Me.cmbDateTime.TabIndex = 17
        '
        'chkCustomExt
        '
        Me.chkCustomExt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCustomExt.Location = New System.Drawing.Point(8, 130)
        Me.chkCustomExt.Name = "chkCustomExt"
        Me.chkCustomExt.Size = New System.Drawing.Size(184, 24)
        Me.chkCustomExt.TabIndex = 5
        Me.chkCustomExt.Text = "Customize output extension"
        '
        'txtCustomName
        '
        Me.txtCustomName.ContextMenu = Me.mnuInserter
        Me.txtCustomName.Enabled = False
        Me.txtCustomName.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomName.Location = New System.Drawing.Point(253, 79)
        Me.txtCustomName.Name = "txtCustomName"
        Me.txtCustomName.Size = New System.Drawing.Size(168, 21)
        Me.txtCustomName.TabIndex = 4
        Me.txtCustomName.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'optNaming
        '
        Me.optNaming.Checked = True
        Me.optNaming.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNaming.Location = New System.Drawing.Point(8, 24)
        Me.optNaming.Name = "optNaming"
        Me.optNaming.Size = New System.Drawing.Size(200, 24)
        Me.optNaming.TabIndex = 0
        Me.optNaming.TabStop = True
        Me.optNaming.Text = "Use the default naming convention"
        '
        'optCustomName
        '
        Me.optCustomName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustomName.Location = New System.Drawing.Point(8, 77)
        Me.optCustomName.Name = "optCustomName"
        Me.optCustomName.Size = New System.Drawing.Size(184, 24)
        Me.optCustomName.TabIndex = 3
        Me.optCustomName.Text = "Customize the output file name"
        '
        'txtCustomExt
        '
        Me.txtCustomExt.ContextMenu = Me.mnuInserter
        Me.txtCustomExt.Enabled = False
        Me.txtCustomExt.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomExt.Location = New System.Drawing.Point(253, 132)
        Me.txtCustomExt.Name = "txtCustomExt"
        Me.txtCustomExt.Size = New System.Drawing.Size(168, 21)
        Me.txtCustomExt.TabIndex = 6
        '
        'chkAppendDateTime
        '
        Me.chkAppendDateTime.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAppendDateTime.Location = New System.Drawing.Point(8, 183)
        Me.chkAppendDateTime.Name = "chkAppendDateTime"
        Me.chkAppendDateTime.Size = New System.Drawing.Size(208, 24)
        Me.chkAppendDateTime.TabIndex = 7
        Me.chkAppendDateTime.Text = "Append date/time to report output"
        '
        'txtDefault
        '
        Me.txtDefault.ContextMenu = Me.mnuInserter
        Me.txtDefault.ForeColor = System.Drawing.Color.Blue
        Me.txtDefault.Location = New System.Drawing.Point(253, 26)
        Me.txtDefault.Name = "txtDefault"
        Me.txtDefault.ReadOnly = True
        Me.txtDefault.Size = New System.Drawing.Size(168, 21)
        Me.txtDefault.TabIndex = 1
        Me.txtDefault.TabStop = False
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.UcBlank)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(443, 318)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Blank Reports"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'UcBlank
        '
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlank.Location = New System.Drawing.Point(3, 3)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(436, 312)
        Me.UcBlank.TabIndex = 0
        '
        'cmdOK
        '
        Me.cmdOK.Enabled = False
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(304, 382)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(384, 382)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'txtFormula
        '
        Me.txtFormula.Location = New System.Drawing.Point(96, 400)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(100, 21)
        Me.txtFormula.TabIndex = 12
        Me.txtFormula.Visible = False
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "Crystal Reports|*.rpt|All Files|*.*"
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 360)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(473, 16)
        Me.Footer1.TabIndex = 8
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "data_table.png")
        Me.ImageList1.Images.SetKeyName(1, "key1.ico")
        Me.ImageList1.Images.SetKeyName(2, "view.ico")
        '
        'bgWorker
        '
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'frmPackedReport
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(463, 410)
        Me.ControlBox = False
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.txtFormula)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.tbPack)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmPackedReport"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Packaged Report Properties"
        Me.tbPack.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnFormat.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.mnuDatasources.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Public Property m_EventReport() As Boolean
        Get
            Return eventReport
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                AddDestinationPage()
            End If

            eventReport = value

            If value = True Then
                chkStatus.Visible = False
                pnFormat.Visible = False
            End If

        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return PackID
        End Get
        Set(ByVal value As Integer)
            PackID = value
        End Set
    End Property
    Private Sub AddDestinationPage()
        Dim tabPage As TabPage = New TabPage("Destination")

        ucDest = New sqlrd.ucDestination

        tbPack.TabPages.Add(tabPage)

        tabPage.Controls.Add(ucDest)

        ucDest.Dock = DockStyle.Fill

        tbPack.TabPages.Remove(tbPack.TabPages(2))
    End Sub

    Private Sub GetParameterAvailableValues()
        Try
            oRpt = New ReportServer.ReportingService

            clsMarsUI.MainUI.BusyProgress(10, "Connecting to server...")

            oRpt.Url = txtUrl.Text

            If serverUser = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            Else
                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(serverUser, serverPassword)

                oRpt.Credentials = cred
            End If

            Dim sRDLPath As String = sAppPath & Me.txtReportName.Text & ".rdl"
            Dim repDef() As Byte

            clsMarsUI.MainUI.BusyProgress(40, "Getting server definition...")

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLocation.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            clsMarsUI.MainUI.BusyProgress(70, "Loading values...")

            clsMarsReport._GetParameterValues(sRDLPath)

            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            parDefaults = New Hashtable

            clsMarsUI.MainUI.BusyProgress(90, "Populating parameters...")

            If sPars IsNot Nothing Then
                For Each s As String In sPars

                    parDefaults.Add(s.Split("|")(0), sParDefaults(I))

                    I += 1
                Next
            End If

            clsMarsUI.MainUI.BusyProgress(, , True)
        Catch
            clsMarsUI.MainUI.BusyProgress(, , True)
        End Try

    End Sub
    Public Function EditReport(ByVal nReportID As Integer, Optional ByRef serverParameters As DataTable = Nothing) As String()
        Dim SQL As String
        Dim sWhere As String = " WHERE ReportID = " & nReportID
        Dim rs As ADODB.Recordset
        Dim nIdent As Integer
        Dim sVals(1) As String
        Dim CachePath As String

        m_Mode = "Edit"

        Me.showTooltip = False

        SQL = "SELECT * FROM ReportAttr" & sWhere

        rs = clsMarsData.GetData(SQL)

        ReportID = nReportID

        cmdRequery.Enabled = True

        Try
            If rs.EOF = False Then
                If Me.m_EventReport = False Then
                    nIdent = GetDelimitedWord(rs("reporttitle").Value, 1, ":")
                Else
                    nIdent = 0
                End If

                txtReportName.Text = IIf(Me.m_EventReport = False, GetDelimitedWord(rs("reporttitle").Value, 2, ":"), rs("reporttitle").Value)
                txtDBLocation.Text = rs("cachepath").Value
                txtUrl.Text = rs("databasepath").Value
                txtFormula.Text = IsNull(rs("selectionformula").Value)
                CachePath = rs("cachepath").Value
                PackID = rs("packID").Value
                Me.ServerUser = IsNull(rs("rptuserid").Value)
                Me.ServerPassword = _DecryptDBValue(IsNull(rs("rptpassword").Value))

                If ucDest IsNot Nothing Then
                    With ucDest
                        .nReportID = nReportID
                        .nPackID = 0
                        .nSmartID = 0
                        ucDest.m_isPackage = False
                        ucDest.eventID = PackID
                        .LoadAll()
                    End With
                End If

                Try
                    UcBlank.chkBlankReport.Checked = Convert.ToBoolean(rs("checkblank").Value)
                Catch : End Try

            End If
        Catch : End Try

        rs.Close()

        SQL = "SELECT * FROM PackagedReportAttr " & sWhere

        rs = clsMarsData.GetData(SQL)

        Try
            If rs.EOF = False Then
                cmbFormat.Text = rs("outputformat").Value
                txtAdjustStamp.Value = IsNull(rs("adjuststamp").Value, 0)

                If rs("customext").Value.Length > 0 Then
                    txtCustomExt.Text = rs("customext").Value
                    chkCustomExt.Checked = True
                End If

                chkAppendDateTime.Checked = Convert.ToBoolean(Convert.ToInt32(rs("appenddatetime").Value))

                If chkAppendDateTime.Checked = True Then
                    cmbDateTime.Text = rs("datetimeformat").Value
                End If

                If rs("customname").Value.Length > 0 Then
                    txtCustomName.Text = rs("customname").Value
                    optCustomName.Checked = True
                Else
                    optNaming.Checked = True
                End If

                Try
                    chkStatus.Checked = Convert.ToBoolean(rs("status").Value)
                Catch
                    chkStatus.Checked = True
                End Try
            End If

            rs.Close()
        Catch ex As Exception
            ''console.writeLine(ex.ToString)
        End Try

        SQL = "SELECT * FROM ReportParameter WHERE ReportID =" & ReportID & " ORDER BY ParID"

        rs = clsMarsData.GetData(SQL)

        Dim I As Integer = 1

        UcBlank.m_ParametersList = New ArrayList

        Try
            Do While rs.EOF = False
                Dim oItem As New ListViewItem

                With oItem
                    .Text = rs("parname").Value
                    .Tag = rs("partype").Value
                    .SubItems.Add(rs("parvalue").Value)
                    .SubItems.Add(IsNull(rs("multivalue").Value, "false"))
                End With

                lsvParameters.Items.Add(oItem)

                UcBlank.m_ParametersList.Add(oItem.Text)

                rs.MoveNext()
                I += 1
            Loop

            rs.Close()
        Catch : End Try

        'custom sections
        SQL = "SELECT * FROM ReportDatasource WHERE ReportID =" & nReportID

        rs = clsMarsData.GetData(SQL)

        Try
            Dim oItem As ListViewItem

            If rs.EOF = False Then
                lsvDatasources.Items.Clear()
            End If

            Do While rs.EOF = False
                oItem = lsvDatasources.Items.Add(rs("datasourcename").Value)
                oItem.SubItems.Add(rs("rptuserid").Value)
                oItem.Tag = rs("datasourceid").Value
                rs.MoveNext()
            Loop

            rs.Close()
        Catch : End Try

        'blank reports
        If UcBlank.chkBlankReport.Checked = True Then
            rs = clsMarsData.GetData("SELECT * FROM BlankReportAlert " & sWhere)

            Try
                If rs.EOF = False Then
                    With UcBlank
                        .txtAlertTo.Text = rs("to").Value
                        .txtBlankMsg.Text = rs("msg").Value
                        .txtSubject.Text = IsNull(rs("subject").Value)

                        Select Case rs("type").Value
                            Case "Alert"
                                .optAlert.Checked = True
                            Case "AlertandReport"
                                .optAlertandReport.Checked = True
                            Case "Ignore"
                                .optIgnore.Checked = True
                        End Select

                        Dim query As String = IsNull(rs("customquerydetails").Value)

                        If query.Length > 0 Then
                            .chkCustomQuery.Checked = True


                            .m_customDSN = query.Split("|")(0)
                            .m_customUserID = query.Split("|")(1)
                            .m_customPassword = _DecryptDBValue(query.Split("|")(2))
                            .txtQuery.Text = query.Split("|")(3)
                        End If
                    End With

                    rs.Close()
                End If
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                _GetLineNumber(ex.StackTrace))
            End Try
        End If

        cmdOK.Enabled = True

        Me.showTooltip = True

        If Me.IsDataDriven = True Then
            Me.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
            Me.cmbFormat.ContextMenu = Me.mnuInserter
        End If

        Try
            bgWorker.RunWorkerAsync()
        Catch : End Try

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        SQL = "UPDATE ReportAttr SET " & _
            "DatabasePath = '" & SQLPrepare(txtUrl.Text) & "'," & _
            "ReportName = '" & SQLPrepare(txtReportName.Text) & "'," & _
            "ReportTitle = '" & IIf(Me.m_EventReport = False, nIdent & ":" & SQLPrepare(txtReportName.Text), SQLPrepare(txtReportName.Text)) & "'," & _
            "SelectionFormula = '" & SQLPrepare(txtFormula.Text) & "', " & _
            "CachePath = '" & SQLPrepare(txtDBLocation.Text) & "'," & _
            "UseSavedData = " & 0 & "," & _
            "RptUserID = '" & SQLPrepare(Me.ServerUser) & "'," & _
            "RptPassword = '" & SQLPrepare(_EncryptDBValue(Me.ServerPassword)) & "'," & _
            "RptServer = ''," & _
            "RptDatabase = ''," & _
            "UseLogin = " & 0 & "," & _
            "AutoRefresh =" & 0 & "," & _
            "RptDatabaseType =''," & _
            "CheckBlank =" & Convert.ToInt32(UcBlank.chkBlankReport.Checked) & " "

        SQL &= sWhere

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE PackagedReportAttr SET " & _
        "OutputFormat ='" & SQLPrepare(cmbFormat.Text) & "'," & _
        "CustomExt = '" & SQLPrepare(txtCustomExt.Text) & "'," & _
        "AppendDateTime =" & Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
        "DateTimeFormat ='" & SQLPrepare(cmbDateTime.Text) & "'," & _
        "CustomName ='" & SQLPrepare(txtCustomName.Text) & "'," & _
        "Status = " & Convert.ToInt32(chkStatus.Checked) & "," & _
        "AdjustStamp = " & txtAdjustStamp.Value

        SQL &= sWhere

        clsMarsData.WriteData(SQL)

        For Each item As ListViewItem In lsvDatasources.Items
            SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
            "DatasourceName = '" & SQLPrepare(item.Text) & "'"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    Dim sCols As String = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                    Dim sVal As String = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(item.Text) & "'," & _
                    "'default'," & _
                    "'<default>'"

                    SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVal & ")"

                    clsMarsData.WriteData(SQL)
                End If

                oRs.Close()
            End If
        Next

        'save the blank report attributes
        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE ReportID = " & nReportID)

        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,ReportID,Subject,CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
            "'" & sBlankType & "'," & _
            "'" & SQLPrepare(UcBlank.txtAlertTo.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.txtBlankMsg.Text) & "'," & _
            nReportID & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        If Me.m_EventReport = False Then
            sVals(0) = nIdent & ":" & txtReportName.Text
        Else
            sVals(0) = txtReportName.Text
        End If

        sVals(1) = cmbFormat.Text


        clsMarsAudit._LogAudit(txtReportName.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.EDIT)

        Return sVals

    End Function

    Public Function AddReport(ByVal nCount As Integer, Optional ByVal nPackID As Integer = 99999, _
    Optional ByRef reportPars As Hashtable = Nothing, Optional ByRef serverParameters As DataTable = Nothing, Optional ByVal sUrl As String = "") As String()
        Dim SQL As String
        Dim sVal(2) As String
        Dim nTempID As Int32
        Dim nReportID As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport

        m_Mode = "Add"

        PackID = nPackID

        If reportPars IsNot Nothing Then
            Me.parCollection = reportPars
        Else
            parCollection = New Hashtable
        End If

        If _CheckScheduleCount() = False Then Return sVal

        ReportID = clsMarsData.CreateDataID("reportattr", "reportid")

        nReportID = ReportID

        Try

            If ucDest IsNot Nothing Then
                ucDest.nReportID = nReportID
                ucDest.nPackID = 0
                ucDest.nSmartID = 0
                ucDest.m_isPackage = False
            End If

            cmdOK.Enabled = True

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        If Me.m_serverUrl = "" Or Me.m_serverUrl = "http://myReportServer/ReportServer/" Then
            txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")
        Else
            txtUrl.Text = Me.m_serverUrl
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

        If Me.IsDataDriven = True Then
            Me.cmbFormat.DropDownStyle = ComboBoxStyle.DropDown
            Me.cmbFormat.ContextMenu = Me.mnuInserter
        End If

        Me.ShowDialog()

        If UserCancel = True Then
            UserCancel = False
            Return sVal
            Exit Function
        End If

        If parCollection IsNot Nothing Then
            reportPars = parCollection
        End If

        'nTempid is used as a temporary place holder for a packaged report
        nTempID = nPackID

        nReportID = ReportID

        'save the report
        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry," & _
            "SelectionFormula,Owner,RptUserID,RptPassword,RptServer,RptDatabase,UseLogin," & _
            "UseSavedData,CachePath,AutoRefresh,PackOrderID,RptDatabaseType,CheckBlank"


        sVals = nReportID & "," & _
        nPackID & "," & _
        "'" & SQLPrepare(txtUrl.Text) & "'," & _
        "'" & SQLPrepare(txtReportName.Text) & "'," & _
        0 & "," & _
        "'" & IIf(Me.m_EventReport = False, nCount & ":" & SQLPrepare(txtReportName.Text), SQLPrepare(txtReportName.Text)) & "'," & _
        0 & "," & _
        "'" & SQLPrepare(txtFormula.Text) & "'," & _
        "'" & gUser & "'," & _
        "'" & SQLPrepare(ServerUser) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
        "''," & _
        "''," & _
        0 & "," & _
        0 & "," & _
        "'" & SQLPrepare(txtDBLocation.Text) & "'," & _
        0 & "," & _
        nCount & "," & _
        "''," & _
        Convert.ToInt32(UcBlank.chkBlankReport.Checked)

        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        'lets save the other report properties
        If clsMarsData.WriteData(SQL) = True Then
            sCols = "AttrID,PackID,ReportID,OutputFormat,CustomExt," & _
            "AppendDateTime,DateTimeFormat,CustomName,Status,DisabledDate,AdjustStamp"

            sVals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
            nPackID & "," & _
            nReportID & "," & _
            "'" & SQLPrepare(cmbFormat.Text) & "'," & _
            "'" & SQLPrepare(txtCustomExt.Text) & "'," & _
            Convert.ToInt32(chkAppendDateTime.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "'," & _
            "'" & SQLPrepare(txtCustomName.Text) & "'," & _
            Convert.ToInt32(chkStatus.Checked) & "," & _
            "'" & ConDateTime(Now) & "'," & _
            txtAdjustStamp.Value

            SQL = "INSERT INTO PackagedReportAttr(" & sCols & ") " & _
            "VALUES (" & sVals & ")"

            clsMarsData.WriteData(SQL)

        End If

        SQL = "UPDATE ReportParameter SET ReportID =" & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportOptions SET " & _
        "ReportID = " & nReportID & "," & _
        "DestinationID = 0 WHERE ReportID = 99999 OR DestinationID = 99999"

        clsMarsData.WriteData(SQL, False)

        SQL = "UPDATE SubReportParameters SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportLogin SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE StampAttr SET ForeignID =" & nReportID & " WHERE ForeignID=99999")

        SQL = "UPDATE ReportDatasource SET ReportID = " & nReportID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        For Each item As ListViewItem In lsvDatasources.Items
            SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & nReportID & " AND " & _
            "DatasourceName = '" & SQLPrepare(item.Text) & "'"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    sCols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                    sVals = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                    nReportID & "," & _
                    "'" & SQLPrepare(item.Text) & "'," & _
                    "'default'," & _
                    "'<default>'"

                    SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                    clsMarsData.WriteData(SQL)
                End If

                oRs.Close()
            End If
        Next

        'save blank report stuff
        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,ReportID,Subject,CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
            "'" & sBlankType & "'," & _
            "'" & UcBlank.txtAlertTo.Text.Replace("'", "''") & "'," & _
            "'" & UcBlank.txtBlankMsg.Text.Replace("'", "''") & "'," & _
            nReportID & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        sVal(0) = nCount & ":" & txtReportName.Text
        sVal(1) = nReportID
        sVal(2) = cmbFormat.Text

        clsMarsAudit._LogAudit(txtReportName.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.CREATE)

        serverParameters = Me.m_serverParametersTable
        Return sVal
    End Function

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        'validate user entry
        If txtReportName.Text = "" Then
            ErrProv.SetError(txtReportName, "Please provide a name for this report")
            txtReportName.Focus()
            Exit Sub
        ElseIf txtDBLocation.Text = "" Then
            ErrProv.SetError(txtDBLocation, "Please select the Access database containing the report")
            txtDBLocation.Focus()
            Exit Sub
        ElseIf cmbFormat.Text = "" And Me.m_EventReport = False Then
            ErrProv.SetError(cmbFormat, "Please select the output format")
            cmbFormat.Focus()
            Exit Sub
        ElseIf optCustomName.Checked = True And txtCustomName.Text = "" Then
            ErrProv.SetError(txtCustomName, "Please specify the custom output name")
            txtCustomName.Focus()
            Exit Sub
        ElseIf chkCustomExt.Checked = True And txtCustomExt.Text = "" Then
            ErrProv.SetError(txtCustomExt, "Please specify the custom extenstion")
            txtCustomExt.Focus()
            Exit Sub
        ElseIf chkAppendDateTime.Checked = True And cmbDateTime.Text = "" Then
            ErrProv.SetError(cmbDateTime, "Please select a date/time format")
            cmbDateTime.Focus()
            Exit Sub
        ElseIf Me.m_EventReport = True Then
            If ucDest IsNot Nothing Then
                If ucDest.lsvDestination.Items.Count = 0 Then
                    ErrProv.SetError(ucDest.lsvDestination, "Please add a destination for this report")
                    Me.tbPack.SelectedTab = Me.tbPack.TabPages(4)
                    Return
                End If
            End If
        ElseIf UcBlank.ValidateEntries = False Then
            Return
        End If

        Dim SQL As String = "SELECT * FROM ReportParameter WHERE ReportID = " & ReportID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
        Dim pars As New ArrayList

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                pars.Add(IsNull(oRs("parname").Value).ToString.ToLower)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        For Each oItem As ListViewItem In lsvParameters.Items
            Dim parValue As String = ""

            Try
                parValue = oItem.SubItems(1).Text
            Catch ex As Exception
                parValue = ""
            End Try


            If parValue = "" Then
                Dim oRes As DialogResult = _
                MessageBox.Show("One or more parameters has been " & _
                "left blank. Edit the parameters? Click 'Yes' to add a parameter value or 'No' to leave empty.", _
                Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question)

                If oRes = Windows.Forms.DialogResult.Yes Then
                    cmdParameters_Click(sender, e)
                    Return
                Else
                    If pars.IndexOf(oItem.Text.ToLower) = -1 Then

                        Dim sCols As String = "ParID,ReportID,ParName,ParValue,ParType,ParNull"
                        Dim sVals As String = clsMarsData.CreateDataID("reportparameter", "parid") & "," & _
                        ReportID & "," & _
                        "'" & SQLPrepare(oItem.Text) & "'," & _
                        "'" & SQLPrepare(parValue) & "'," & _
                        "'" & oItem.Tag & "'," & _
                        0
                        clsMarsData.DataItem.InsertData("ReportParameter", sCols, sVals, False)

                    End If

                End If
            End If
        Next

        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub txtReportName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportName.TextChanged
        Try
            oErr.ResetError(sender, ErrProv)

            If optNaming.Checked = True Then
                If cmbFormat.Text.Length > 0 Then
                    txtDefault.Text = txtReportName.Text & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
                End If
            Else
                txtDefault.Text = txtReportName.Text
            End If

            If ucDest IsNot Nothing Then
                ucDest.sReportTitle = txtReportName.Text
            End If
        Catch : End Try
    End Sub



    Private Sub txtCustomName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub txtCustomExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomExt.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        oErr.ResetError(sender, ErrProv)

        'If txtDefault.Text.Length > 0 Then
        '    Dim sTemp As String
        '    Dim sExt As String

        '    sTemp = txtDefault.Text.Substring(0, txtDefault.Text.Length - 4)

        '    sTemp &= Date.Now.ToString(cmbDateTime.Text)

        '    sExt = txtDefault.Text.Split(".")(txtDefault.Text.Split(".").GetUpperBound(0))




        '    txtDefault.Text = sTemp & "." & sExt
        'End If
    End Sub

    Private Sub cmbDateTime_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbDateTime.Click
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdDBLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDBLoc.Click
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword)

            If srsVersion = "2007" Then
                oRpt = New ReportServer_2005.ReportingService2005
            Else
                oRpt = New ReportServer.ReportingService
            End If

            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion = "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            oRpt.Url = txtUrl.Text

            'If sReturn Is Nothing Then
            'oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            'Else
            'ServerUser = sReturn(0)
            'ServerPassword = sReturn(1)

            'cred = New System.Net.NetworkCredential(ServerUser, ServerPassword)

            'oRpt.Credentials = cred
            'End If

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            If Me.m_Mode = "Edit" Then oServer.m_ReadOnly = True

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text)

            If Me.m_Mode = "Edit" Then Return

            'If Me.txtDBLoc.Text.Length = 0 Then

            '    showTooltip = False

            '    Dim oServerBrowse As frmReportServer = New frmReportServer

            '    AppStatus(True)

            '    sPath = oServerBrowse._GetReport(txtUrl.Text, cred)
            'Else
            '    sPath = txtDBLoc.Text
            'End If

            If sPath IsNot Nothing Then
                txtDBLocation.Text = sPath
                txtReportName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdOK.Enabled = True

                If txtDBLocation.Text.StartsWith("/") = False Then
                    txtDBLocation.Text = "/" & txtDBLocation.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtReportName.Text & ".rdl"
            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLocation.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLocation.Text, _
            ServerUser, ServerPassword, sRDLPath)

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            lsvParameters.Items.Clear()

            ParDefaults = New Hashtable

            If sPars IsNot Nothing Then
                For Each s As String In sPars
                    'check if the parameter already exists in the list
                    Dim parName As String = s.Split("|")(0)
                    Dim inList As Boolean

                    inList = False

                    For Each it As ListViewItem In lsvParameters.Items
                        If it.Text = parName Then
                            inList = True
                            Exit For
                        End If
                    Next

                    If inList = True Then Continue For

                    Dim oItem As ListViewItem = lsvParameters.Items.Add(s.Split("|")(0))

                    oItem.Tag = s.Split("|")(1)

                    Try
                        oItem.SubItems(1).Text = ""
                    Catch ex As Exception
                        oItem.SubItems.Add("")
                    End Try

                    Try
                        oItem.SubItems(2).Text = s.Split("|")(2)
                    Catch
                        oItem.SubItems.Add(s.Split("|")(2))
                    End Try

                    Try
                        ParDefaults.Add(oItem.Text, sParDefaults(I))
                        parCollection.Add(oItem.Text, "")
                    Catch : End Try

                    I += 1
                Next
            End If

            UcBlank.m_ParametersList = New ArrayList

            For Each it As ListViewItem In lsvParameters.Items
                UcBlank.m_ParametersList.Add(it.Text)
            Next

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            'txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtReportName.Focus()
            txtReportName.SelectAll()

            cmdOK.Enabled = True
            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLocation.Text = ""
                txtReportName.Text = ""
                'txtDesc.Text = ""
            End If
        End Try
    End Sub



    Private Sub optCustomName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustomName.CheckedChanged
        If optCustomName.Checked = True Then
            txtCustomName.Enabled = True
            txtDefault.Text = String.Empty
        Else
            txtCustomName.Enabled = False
            txtCustomName.Text = ""
        End If
    End Sub

    Private Sub chkCustomExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCustomExt.CheckedChanged
        If chkCustomExt.Checked = True Then
            txtCustomExt.Enabled = True
        Else
            txtCustomExt.Enabled = False
            txtCustomExt.Text = ""
        End If
    End Sub

    Private Sub chkAppendDateTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAppendDateTime.CheckedChanged
        If chkAppendDateTime.Checked = True Then
            cmbDateTime.Enabled = True

            Dim oUI As New clsMarsUI
            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")
            txtAdjustStamp.Enabled = True
        Else
            cmbDateTime.Enabled = False
            cmbDateTime.Text = ""
            txtAdjustStamp.Enabled = False
            txtAdjustStamp.Value = 0
        End If
    End Sub

    Private Sub lsvParameters_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvParameters.DoubleClick
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text

                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try

                .Item("ParType") = item.Tag
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, ReportID, lsvParameters.SelectedItems(0).Text, m_serverParametersTable, txtUrl.Text, Me.txtDBLocation.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub frmPackedReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        txtReportName.Focus()

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If
    End Sub



    Private Sub cmdParameters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParameters.Click
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, ReportID, , m_serverParametersTable, txtUrl.Text, Me.txtDBLocation.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub


    Private Sub txtCustomName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomName.GotFocus
        oField = txtCustomName
    End Sub

    Private Sub txtCustomExt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCustomExt.GotFocus
        oField = txtCustomExt
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        oField.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.PackID)
        oInsert.m_EventBased = Me.m_EventReport
        oInsert.m_EventID = PackID
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        oField.SelectedText = oItem._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub cmbFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFormat.SelectedIndexChanged, cmdProps.Click
        Try
            If optNaming.Checked = True And cmbFormat.Text.Length > 0 Then
                txtDefault.Text = txtReportName.Text & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
            End If
        Catch
            txtDefault.Text = String.Empty
        End Try

        Dim sWorksheet As String

        If txtCustomName.Text.Length = 0 Then
            sWorksheet = txtReportName.Text
        Else
            sWorksheet = txtCustomName.Text
        End If

        ErrProv.SetError(sender, String.Empty)

        If Me.Visible = True Then
            Dim opt As New frmRptOptions

            Dim format As String = ""

            If cmbFormat.Text.Contains("(") Then
                format = cmbFormat.Text.Split("(")(0).Trim()
            Else
                format = cmbFormat.Text
            End If

            Select Case cmbFormat.Text
                Case "Acrobat Format (*.pdf)"
                    opt.ReportOptions(format, "Packed", ReportID)
                Case "MS Excel 97-2000 (*.xls)"
                    opt.ReportOptions(format, "Packed", ReportID, , , sWorksheet)
                Case "MS Word (*.doc)"
                    opt.ReportOptions(format, "Packed", ReportID, )
                Case "CSV (*.csv)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Package", ReportID)
                Case "HTML (*.htm)"
                    opt.ReportOptions(cmbFormat.Text.Split("(")(0).Trim, "Package", ReportID)
                Case "TIFF (*.tif)"
                    opt.ReportOptions(format, "Packed", ReportID)
                Case "Custom"
                    Me.chkCustomExt.Checked = True
                    opt.ReportOptions(format, "Package", ReportID)
            End Select
        End If
    End Sub

    Private Sub optNaming_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNaming.CheckedChanged

        If optNaming.Checked = True And cmbFormat.Text.Length > 0 Then
            txtDefault.Text = txtReportName.Text & cmbFormat.Text.Split("*")(1).Replace(")", String.Empty).Trim
        End If

    End Sub


    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(ReportID, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub

    Private Sub cmdLoginTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest.Click
        Dim rv As Microsoft.Reporting.WinForms.ReportViewer
        Dim actualUrl As String = ""

        Try
            AppStatus(True)

            oRpt = New ReportServer.ReportingService
            rv = New Microsoft.Reporting.WinForms.ReportViewer

            With oRpt
                .Url = txtUrl.Text

                For x As Integer = 0 To txtUrl.Text.Split("/").GetUpperBound(0) - 1
                    actualUrl &= txtUrl.Text.Split("/")(x) & "/"
                Next

                rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote
                rv.ServerReport.ReportServerUrl = New Uri(actualUrl)

                If ServerPassword.Length = 0 And ServerUser.Length = 0 Then
                    .Credentials = System.Net.CredentialCache.DefaultCredentials
                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = System.Net.CredentialCache.DefaultCredentials
                Else
                    Dim oCred As System.Net.NetworkCredential = New System.Net.NetworkCredential(ServerUser, ServerPassword)

                    .Credentials = oCred

                    rv.ServerReport.ReportServerCredentials.NetworkCredentials = oCred
                End If
            End With

            Dim serverVersion As String = "2000"

            Try
                serverVersion = rv.ServerReport.GetServerVersion
                rv.ServerReport.ReportPath = Me.txtDBLocation.Text
            Catch ex As Exception
                serverVersion = "2000"
                rv = Nothing
            End Try

            Dim oPar() As ReportServer.ParameterValue = Nothing
            Dim reportPars() As Microsoft.Reporting.WinForms.ReportParameter = Nothing
            Dim oDataLogins() As ReportServer.DataSourceCredentials = Nothing
            Dim reportDS() As Microsoft.Reporting.WinForms.DataSourceCredentials
            Dim I As Integer = 0
            Dim oData As New clsMarsData

            'set the parameters
            Dim oParse As New clsMarsParser

            For Each oItem As ListViewItem In lsvParameters.Items
                If oItem.SubItems(1).Text.ToLower = "[sql-rddefault]" Then Continue For

                ReDim Preserve oPar(I)
                ReDim Preserve reportPars(I)

                oPar(I) = New ReportServer.ParameterValue
                oPar(I).Name = oItem.Text

                reportPars(I) = New Microsoft.Reporting.WinForms.ReportParameter
                reportPars(I).Name = oItem.Text

                If oItem.SubItems(1).Text.ToLower = "[sql-rdnull]" Then
                    oPar(I).Value = Nothing
                Else
                    oPar(I).Value = oParse.ParseString(oItem.SubItems(1).Text)

                    For Each s As String In oItem.SubItems(1).Text.Split("|")
                        If s.Length > 0 Then reportPars(I).Values.Add(oParse.ParseString(s))
                    Next
                End If


                I += 1
            Next

            If rv IsNot Nothing And reportPars IsNot Nothing Then rv.ServerReport.SetParameters(reportPars)

            I = 0

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID = " & 99999)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    ReDim Preserve oDataLogins(I)
                    ReDim Preserve reportDS(I)

                    oDataLogins(I) = New ReportServer.DataSourceCredentials
                    reportDS(I) = New Microsoft.Reporting.WinForms.DataSourceCredentials

                    With oDataLogins(I)
                        .DataSourceName = oRs("datasourcename").Value
                        .UserName = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    With reportDS(I)
                        .Name = oRs("datasourcename").Value
                        .UserId = oRs("rptuserid").Value
                        .Password = _DecryptDBValue(IsNull(oRs("rptpassword").Value))
                    End With

                    I += 1
                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            If rv IsNot Nothing And reportDS IsNot Nothing Then rv.ServerReport.SetDataSourceCredentials(reportDS)

            If serverVersion = "2000" Then
                Dim warnings As ReportServer.Warning() = Nothing
                Dim streamIDs As String() = Nothing

                Dim oResult As Byte()

                oResult = oRpt.Render(Me.txtDBLocation.Text, "PDF", Nothing, "", _
                    oPar, oDataLogins, Nothing, "", "", Nothing, _
                    warnings, streamIDs)

                Dim sFileName As String = sAppPath & clsMarsData.CreateDataID & ".pdf"

                'write the bytes to disk
                With System.IO.File.Create(sFileName, oResult.Length)
                    .Write(oResult, 0, oResult.Length)
                    .Close()
                End With

                'view the exported file
                Dim oView As New frmPreview

                AppStatus(False)

                oView.PreviewReport(sFileName, txtDBLocation.Text, True)

                Try : IO.File.Delete(sFileName) : Catch : End Try
            Else
                Dim oView As New frmPreview

                oView.PreviewReport(rv, txtDBLocation.Text, True)
            End If
        Catch ex As Exception
            AppStatus(False)
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), "Please check your datasource credentials and try again")
        End Try
    End Sub

    Private Sub cmdRequery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRequery.Click
        If (MessageBox.Show("Requery the report for parameters? This will remove the current parameters and respective values", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes) Then

            clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =" & Me.ReportID)

            clsMarsReport.GetReportParameters(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, ParDefaults, lsvParameters)

            UcBlank.m_ParametersList = New ArrayList

            For Each it As ListViewItem In Me.lsvParameters.Items
                UcBlank.m_ParametersList.Add(it.Text)
            Next
        End If
    End Sub

    Private Sub tbPack_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbPack.SelectedIndexChanged
        Select Case tbPack.SelectedIndex
            Case 3
                'add datasources from the report that have default credentials in case user wants to set them up
                If txtDBLocation.Text.Length > 0 And txtUrl.Text.Length > 0 And m_Mode = "Add" Then
                    clsMarsReport.GetReportDatasources(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, Me.lsvDatasources)
                End If
        End Select
    End Sub

    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If
    End Sub

    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvParameters.SelectedIndexChanged

    End Sub

    Private Sub lsvDatasources_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvDatasources.SelectedIndexChanged

    End Sub

    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub

    Private Sub btnRefreshDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshDS.Click
        clsMarsReport.GetReportDatasources(txtUrl.Text, ServerUser, ServerPassword, txtDBLocation.Text, txtReportName.Text, True, Me.lsvDatasources)
    End Sub


    Private Sub chkStatus_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStatus.CheckedChanged

    End Sub

    Private Sub txtDBLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLocation.TextChanged
        If txtDBLocation.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDBLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDBLoc)

            Me.cmdDBLoc.Image = Me.ImageList1.Images(1)
            Me.cmdDBLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLocation.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDBLoc, Nothing)

            Me.cmdDBLoc.Image = ImageList1.Images(2)
            Me.cmdDBLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdOK.Enabled = False
    End Sub

    Private Sub cmdLoginTest2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoginTest2.Click
        cmdLoginTest_Click(Nothing, Nothing)
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLocation.Text, _
        ServerUser, ServerPassword, "")
        Me.GetParameterAvailableValues()
    End Sub

  

    
    Private Sub txtUrl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.TextChanged
        m_serverUrl = txtUrl.Text
    End Sub
End Class
