Imports Microsoft.VisualBasic
Imports System.IO
Module MarsCommon

    Private Const WAIT_INFINITE As Object = -1&
    Private Const SYNCHRONIZE As Object = &H100000
    Private Declare Function CloseHandle Lib "kernel32" _
        (ByVal hObject As Long) As Long
    Private Declare Function OpenProcess Lib "kernel32" _
      (ByVal dwDesiredAccess As Long, _
       ByVal bInheritHandle As Long, _
       ByVal dwProcessId As Long) As Long
    Private Declare Function WaitForSingleObject Lib "kernel32" _
        (ByVal hHandle As Long, _
        ByVal dwMilliseconds As Long) As Long

    Public Function InitImages() As ImageList
        Try
            Dim imgList As ImageList = New ImageList()

            Dim sz As Size = New Size(16, 16)

            imgList.ImageSize = sz
            imgList.ColorDepth = ColorDepth.Depth32Bit
            imgList.TransparentColor = Color.Transparent

            With imgList.Images
                .Add(My.Resources.home) '0
                .Add(My.Resources.folder_closed) '1
                .Add(My.Resources.folder) '2
                .Add(My.Resources.box_closed) '3
                .Add(My.Resources.box) '4
                .Add(My.Resources.folder_cubes) '5
                .Add(My.Resources.document_chart) '6
                .Add(My.Resources.document_gear) '7
                .Add(My.Resources.folder_closed1) '8
                .Add(My.Resources.box_closed_bw) '9
                .Add(My.Resources.box_bw) '10
                .Add(My.Resources.document_pulse) '11
                .Add(My.Resources.document_attachment) '12
                .Add(My.Resources.document_atoms) '13
                .Add(My.Resources.box_new) '14
                .Add(My.Resources.box_new_bw) '15
                .Add(My.Resources.cube_molecule) '16
                .Add(My.Resources.document_flash)  '17
                .Add(My.Resources.document_flash_bw) '18
                .Add(My.Resources.data_driven_box) '19
                .Add(My.Resources.data_driven_box_bw) '20
            End With

            Return imgList
        Catch
            Return Nothing
        End Try
    End Function
    Public Function getUrlHistory() As AutoCompleteStringCollection
        Dim col As AutoCompleteStringCollection = New AutoCompleteStringCollection
        Dim cacheFile As String = sAppPath & "url.dat"
        Dim cache As String = IsNull(ReadTextFromFile(cacheFile))

        For Each item As String In cache.Split("|")
            If col.Contains(item) = False Then
                col.Add(item)
            End If
        Next

        Return col
    End Function
    Public Sub logUrlHistory(ByVal url As String)
        Try
            Dim cacheFile As String = sAppPath & "url.dat"
            Dim cache As String = IsNull(ReadTextFromFile(cacheFile))
            Dim items() As String = cache.Split("|")
            Dim exists As Boolean = False

            'only save the URL if it contains asmx
            If url.ToLower.EndsWith("asmx") = False Then Return

            For Each item As String In items
                If String.Compare(item, url, True) = 0 Then
                    exists = True
                    Exit For
                End If
            Next

            If exists = False Then
                Dim output As String

                output = cache & "|" & url

                SaveTextToFile(output, cacheFile, , False, False)
            End If
        Catch : End Try
    End Sub

    Private Function urlExists(ByVal url As String) As Boolean

    End Function
    '<DebuggerStepThrough()> _
    Public Function IsNonEnglishRegionWrite(ByVal actualValue As String) As String

        Dim DecimalSep As Char = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator

        'Dim result As Double = 25 / 6
        'Dim resultString As String = Convert.ToString(result)

        actualValue = actualValue.Replace(DecimalSep, ".")

        'If DecimalSep = "." Then
        '    actualValue = actualValue.Replace(".", ",")
        'Else
        '    actualValue = actualValue.Replace(",", ".")
        'End If

        Return actualValue
    End Function

    Public Function IsNonEnglishRegionRead(ByVal actualValue As String) As String

        Dim DecimalSep As Char = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator

        'Dim result As Double = 25 / 6
        'Dim resultString As String = Convert.ToString(result)

        actualValue = actualValue.Replace(".", DecimalSep)

        'If DecimalSep = "." Then
        '    actualValue = actualValue.Replace(".", ",")
        'Else
        '    actualValue = actualValue.Replace(",", ".")
        'End If

        Return actualValue
    End Function
    Public Function FixHTMLImageRefs(ByVal sPath As String, ByVal ImageIDs As String()) As Boolean
        Try
            Dim fullstring As String = ReadTextFromFile(sPath)

            For Each imageID As String In ImageIDs

                Dim imageLoc As Integer = fullstring.IndexOf(imageID)

                If imageLoc = -1 Then Continue For

                Dim I As Integer = imageLoc

                'example string: SRC="http://192.168.0.99/ReportServer?%2fSQL-RD+Sample+Reports%2fCommissionsReport&amp;rs%3aFormat=HTML4.0&amp;rs%3aImageID=C_729_S

                Do While I > 0
                    Dim substring As String = fullstring.Substring(I - 4, 4)

                    'console.writeLine(substring)

                    If substring.ToLower = "src=" Then Exit Do

                    I = I - 1
                Loop

                If I = 0 Then Continue For

                Dim startLoc As Integer = I + 1

                Dim removeString As String = fullstring.Substring(startLoc, (imageLoc - startLoc))

                fullstring = fullstring.Remove(startLoc, (imageLoc - startLoc))
            Next

            SaveTextToFile(fullstring, sPath, , False, False)

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    <DebuggerStepThrough()> Public Function SQLPrepare(ByVal sIn As Object) As String
        On Error Resume Next

        If sIn Is Nothing Then
            sIn = String.Empty
        End If

        Return sIn.Replace("'", "''")
    End Function
    Public Function GetDelimitedWord( _
    ByVal strIn As String, ByVal lngIndex As Long, ByVal chrDelimit As String) _
            As String

        On Error GoTo Trap
        Dim xx As Object

        xx = strIn.Split(chrDelimit)

        GetDelimitedWord = xx(lngIndex - 1)

        Exit Function
Trap:
        GetDelimitedWord = ""
    End Function
    Public Function GetDirectory(ByVal sIn As String) As String
        On Error Resume Next

        Dim sFile As String

        sFile = ExtractFileName(sIn)

        sIn = sIn.Replace(sFile, String.Empty)

        If sIn Is Nothing Then sIn = ""

        Return sIn

    End Function
    <DebuggerStepThrough()> Public Sub AppStatus(ByVal Busy As Boolean)
        On Error Resume Next

        Dim oForm As Form
        If Busy Then
            Cursor.Current = Cursors.WaitCursor

            For Each oForm In oWindow
                Cursor.Current = Cursors.WaitCursor
            Next

            For Each oForm In oMain.MdiChildren
                Cursor.Current = Cursors.WaitCursor
            Next
        Else
            Cursor.Current = Cursors.Default

            For Each oForm In oWindow
                Cursor.Current = Cursors.Default
            Next

            For Each oForm In oMain.MdiChildren
                Cursor.Current = Cursors.Default
            Next
        End If
    End Sub

    <DebuggerStepThrough()> Public Function IsNull(ByVal sIn As Object, Optional ByVal sDefault As Object = "") As Object
        Dim Temp As Object

        Try
            Temp = sIn & ""

            If Temp.length > 0 Then
                Return Temp
            Else
                Return sDefault
            End If
        Catch
            Return sIn
        End Try
    End Function
    Public Function FindOccurence(ByVal strIn As String, ByVal Delimiter As String) As Integer
        Dim arr() As Object
        Try
            arr = strIn.Split(Delimiter)

            FindOccurence = UBound(arr)
        Catch
            FindOccurence = 0
        End Try
    End Function

    Public Function Decrypt(ByVal EncText As String, ByVal WatchWord As String) As String

        'All the Dim things Follows
        Dim intKey
        Dim intX
        Dim strChar As String
        Dim DecText As String

        'The Few Lines that work for us.The Following three
        'Lines Create the key to Decrypt the Data.
        For intX = 1 To Len(WatchWord)
            intKey = intKey + Asc(Mid(WatchWord, intX)) / 20
        Next

        'Here is our Engine Room that Decrypts the Data by using the key.
        For intX = 1 To Len(EncText)
            strChar = Mid(EncText, intX, 1)
            DecText = DecText & Chr(Asc(strChar) Xor intKey)
        Next

        'Return the Data.
        Decrypt = DecText
    End Function
    <DebuggerStepThrough()> Public Function ExtractFileName(ByVal FilePath As String) As String
        Return System.IO.Path.GetFileName(FilePath)

        'Dim Temp() As String
        'Dim I As Integer

        'I = FilePath.Split("\").GetUpperBound(0)

        'Temp = FilePath.Split("\")

        'Return Temp(I)

        ''I = FindOccurence(CStr(FilePath), "\") + 1

        ''Temp = GetDelimitedWord(CStr(FilePath), I, "\")

        ''Return Temp
    End Function

    <DebuggerStepThrough()> _
    Public Function ConDateTime(ByVal iDate As Date) As String
        Return ConDate(iDate) & " " & ConTime(iDate)
    End Function

    <DebuggerStepThrough()> _
    Public Function ConTime(ByVal iTime As Date) As String
        'ConTime = Microsoft.VisualBasic.Format(iTime, "hh:mm")
        Try
            ConTime = iTime.ToString("HH:mm:ss")
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    <DebuggerStepThrough()> _
    Public Function ConDate(ByVal iDate As Date) As String
        On Error Resume Next
        Dim nYear1 As Integer
        Dim nYear2 As Integer

        ConDate = iDate.ToString("yyyy-MM-dd")

    End Function
    Public Function Encrypt(ByVal PlainText As String, ByVal WatchWord As String) As String

        'Other Dim Things
        Dim intKey
        Dim intX
        Dim strChar As String
        Dim EncText As String = ""

        'all Mighty visual Basic lines give us the Key
        For intX = 1 To Len(WatchWord)
            intKey = intKey + Asc(Mid(WatchWord, intX)) / 20
        Next

        'the brother of decrypt function.but it earns its
        'living by encrypting the data
        For intX = 1 To Len(PlainText)
            strChar = Mid(PlainText, intX, 1)
            EncText = EncText & Chr(Asc(strChar) Xor intKey)
        Next

        'the ever so generous functios should return something.
        Encrypt = EncText
    End Function

    Public Function ReadTextFromFile(ByVal sPath As String) As String
        On Error Resume Next

        Dim sr As StreamReader = File.OpenText(sPath)
        Dim sInput As String

        sInput = sr.ReadToEnd

        sr.Close()

        Return sInput

    End Function
    <DebuggerStepThrough()> _
    Public Function SaveTextToFile(ByVal strData As String, _
     ByVal FullPath As String, _
       Optional ByVal ErrInfo As String = "", Optional ByVal Append As Boolean = False, Optional ByVal addCR As Boolean = True) As Boolean

        Dim bAns As Boolean = False
        Dim objReader As StreamWriter

        Try
            If strData.StartsWith(vbCrLf) = False And strData.StartsWith(Environment.NewLine) = False And addCR = True Then
                strData = vbCrLf & strData
            End If

            objReader = New StreamWriter(FullPath, Append)

            objReader.Write(strData)
            'End If

            objReader.Close()
            bAns = True
        Catch Ex As Exception
            ErrInfo = Ex.Message
            bAns = False
        End Try

        Return bAns
    End Function

    Public Sub SuperShell(ByVal sCmd As String)
        On Error Resume Next
        Dim hProcess As Long
        Dim taskId As Long
        Dim cmdline As String

        Cursor.Current = Cursors.WaitCursor

        taskId = Shell(sCmd, vbHide)

        hProcess = OpenProcess(SYNCHRONIZE, True, taskId)
        Call WaitForSingleObject(hProcess, WAIT_INFINITE)
        CloseHandle(hProcess)

        Cursor.Current = Cursors.Default

    End Sub

    Public Function MergeHTMLFiles(ByVal sPath As String) As String
        Dim sTemp As String
        Dim oData As New clsMarsData
        Dim n As Integer
        Dim sRead As String

        For Each s As String In System.IO.Directory.GetFiles(sPath)
            sTemp = ReadTextFromFile(s)

            n = sTemp.ToLower.IndexOf(">first</a> <", 0)
            sTemp = sTemp.Substring(0, n)

            n = sTemp.Length

            Do While n > 0
                n -= 1
                If sTemp.Substring(n, 1) = "<" Then
                    Exit Do
                End If
            Loop

            sTemp = sTemp.Substring(0, n)

            sRead &= sTemp
        Next

        sPath &= clsMarsData.CreateDataID("", "") & ".html"

        SaveTextToFile(sRead, sPath, , False)

        Return sPath
    End Function

    Public Function _CheckScheduleCount() As Boolean
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim nCount As Integer

        If gnEdition >= MarsGlobal.gEdition.PLATINUM Or IsFeatEnabled(gEdition.NULL, featureCodes.g1_Unlimit_Sched) Then
            Return True
        Else
            SQL = "SELECT COUNT(*) FROM ReportAttr WHERE Status =1"

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            Try
                nCount = oRs.Fields(0).Value

                If nCount >= nMax Then
                    MessageBox.Show("You have now reached the maximum number of schedules " & _
                    "allowed in a " & gsEdition & " edition.", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                Return True
            End Try
        End If
    End Function

    Public Sub _NeedUpgrade(ByVal oEdition As gEdition, Optional ByVal Msg As String = "")
        Dim oRes As DialogResult
        Dim userMsg As String = ""

        If Msg <> "" Then
            userMsg = "The feature you are trying to access (" & Msg & ") is not available in " & _
                    "this edition of SQL-RD. You need to upgrade to " & oEdition.ToString & " Edition." & _
                    " Would you like to view your upgrade options now?"
        Else
            userMsg = "The feature you are trying to access is not available in " & _
            "this edition of SQL-RD. You need to upgrade to " & oEdition.ToString & " Edition." & _
            " Would you like to view your upgrade options now?"
        End If

        userMsg = "The functionality you are trying to access is not enabled on your system. " & vbCrLf & _
          "Would you like to go to the SQL-RD online store to purchase this functionality?"

        oRes = MessageBox.Show(userMsg, Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            clsWebProcess.Start("http://www.christiansteven.com/purchase_sql-rd.htm") ''
        End If
    End Sub

    Public Function IsFeatEnabled(ByVal edition As gEdition, ByVal code As featureCodes) As Boolean
        Dim custID As String
        Dim upgradeKey As String
        Dim keycustID As String
        Dim strCode As String = code.ToString
        Dim featEnabled As Boolean = False

        'SaveTextToFile(Date.Now & ": CurrentEdition Value =" & gnEdition & ", Feature Value =" & edition, sAppPath & "edition.debug", , False, True)

        If edition <> gEdition.NULL Then
            If gnEdition >= edition Then
                Return True
            End If
        End If

        If strCode.Contains("_") Then
            strCode = strCode.Split("_")(0)
        End If

        custID = clsMarsUI.MainUI.ReadRegistry("custNo", "")
        upgradeKey = clsMarsUI.MainUI.ReadRegistry("UpgradeKey", "", True)

        If upgradeKey <> "" And custID <> "" Then
            keycustID = upgradeKey.Split("|")(0)

            If keycustID <> custID Then Return False

            For Each s As String In upgradeKey.Split("|")
                If s = strCode Then
                    Return True
                    Exit For
                End If
            Next
        End If

        Return False
    End Function
    Public Function _GetLineNumber(ByVal sIn As String) As Integer
        Try
            Dim st As String = sIn
            Dim LineNr As Int32 = CType(st.Substring(st.LastIndexOf(":line") + 5), Int32)

            Return LineNr
        Catch
            Return 0
        End Try

    End Function

    Public Function _CreateUNC(ByVal sPath As String) As String

        Try
            Dim DoNotUNC As Boolean = False
            Dim resultPath As String = ""

            Try
                DoNotUNC = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("DoNotUNC", 0)))
            Catch
                DoNotUNC = False
            End Try

            Try
                Dim oUNC As New GetUNC.clsUNC

                resultPath = oUNC.GetUncFullPathFromMappedDrive(sPath)
            Catch ex As Exception
                resultPath = sPath
            End Try

            'Dim path As String = Trinet.Networking.ShareCollection.PathToUnc(sPath)

            If resultPath.ToLower = sPath.ToLower And sPath.StartsWith("\\") = False Then
                If DoNotUNC = False Then
                    
                    Dim resolver As ResolveToUNC.clsUNCName = New ResolveToUNC.clsUNCName

                    resultPath = resolver.GetUncName(resultPath)

                    If resultPath.ToLower = sPath.ToLower And resultPath.StartsWith("\\") = False Then
                        Dim s As String = resultPath

                        Dim drive As String = s.Split(":")(0) & "$"

                        Dim pcname = Environment.MachineName

                        Dim path As String = s.Split(":")(1)

                        Dim result As String = "\\" & pcname & "\" & drive & path

                        resultPath = result
                    End If
                Else
                    Return resultPath
                End If
            End If

            If IO.Directory.Exists(resultPath) = True Then
                Dim writeTest As Boolean = SaveTextToFile("test", resultPath & "\_sqlrdtest.log", , False, False)

                If writeTest = True Then
                    IO.File.Delete(resultPath & "\_sqlrdtest.log")
                    Return resultPath
                Else
                    Return sPath
                End If
            Else
                Return resultPath
            End If
        Catch ex As Exception
            Return sPath
        End Try
    End Function

    <DebuggerStepThrough()> _
    Public Function _EncryptDBValue(ByVal sValue As String) As String
        Dim sReturn As String
        Dim oSec As New clsSettingsManager

        sReturn = oSec.EncryptString(sValue) 'Encrypt(sValue, "preovitriolic")

        If sReturn Is Nothing Then
            Return String.Empty
        Else
            If sReturn.Length > 0 Then Return "$$" & sReturn & "$$" Else Return String.Empty
        End If
    End Function

    <DebuggerStepThrough()> _
    Public Function _DecryptDBValue(ByVal sValue As String) As String
        Dim sReturn As String

        If sValue.StartsWith("$$") And sValue.EndsWith("$$") Then
            sValue = sValue.Substring(2, sValue.Length - 2)
            sValue = sValue.Substring(0, sValue.Length - 2)

            Dim oSec As New clsSettingsManager

            sReturn = oSec.DecryptString(sValue)
        Else
            sReturn = Decrypt(sValue, "preovitriolic")
        End If

        If sReturn Is Nothing Then
            Return String.Empty
        Else
            Return sReturn
        End If
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filePath">The full path to the filename, can contain mask e.g *.*</param>
    ''' <param name="destFolder">The path to copy the file(s) to</param>
    ''' <param name="overWrite">Overwrite the destination if exists</param>
    ''' <param name="recursive">search sub folders for matches</param>
    ''' <param name="maintStruct">maintain folder structure of original file(s)</param>
    ''' <returns>results array list</returns>
    ''' <remarks></remarks>
    Public Function DoCopyFiles(ByVal filePath As String, ByVal destFolder As String, Optional ByVal overWrite As Boolean = True, _
    Optional ByVal recursive As Boolean = True, Optional ByVal maintStruct As Boolean = False) As Boolean
        Dim results As ArrayList = New ArrayList
        Dim startPath As String = ""
        Dim mask As String = ""

        If recursive = True Then
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try

            doSearch(startPath, mask, results)

            Dim temp As String = destFolder

            If maintStruct = True Then
                For Each s As String In results
                    Dim oldPath As String = ""
                    Dim fileName As String = ""
                    Dim newPath As String = ""

                    destFolder = temp
                    oldPath = New IO.FileInfo(s).DirectoryName
                    fileName = New IO.FileInfo(s).Name

                    If oldPath.EndsWith("\") = False Then oldPath &= "\"
                    If startPath.EndsWith("\") = False Then startPath &= "\"

                    If startPath = oldPath Then
                        newPath = ""
                    Else
                        newPath = oldPath.Replace(startPath, "")
                    End If

                    If newPath.StartsWith("\") = False Then newPath = "\" & newPath

                    destFolder = destFolder & newPath

                    If destFolder.EndsWith("\") = False Then destFolder &= "\"

                    clsMarsParser.parser.ParseDirectory(destFolder)

                    IO.File.Copy(s, destFolder & fileName, overWrite)
                Next
            Else

                For Each s As String In results
                    Dim fileName As String = New IO.FileInfo(s).Name

                    If destFolder.EndsWith("\") = False Then destFolder &= "\"

                    clsMarsParser.parser.ParseDirectory(destFolder)

                    IO.File.Copy(s, destFolder & fileName, overWrite)
                Next
            End If
        Else
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try
            If filePath.Contains("*") = True Then
                For Each s As String In IO.Directory.GetFiles(startPath)
                    'compare the file to the mask
                    Dim tmpfile As String = ExtractFileName(s)
                    If tmpfile Like mask Then
                        results.Add(s)
                    End If
                Next
            Else
                results.Add(filePath)
            End If
            If maintStruct = True Then
                For Each s As String In results
                    Dim oldpath As String = New IO.FileInfo(s).DirectoryName
                    Dim filename As String = New IO.FileInfo(s).Name

                    Dim root As String = IO.Directory.GetDirectoryRoot(oldpath)

                    Dim newPath As String = oldpath.Substring(root.Length, (oldpath.Length - root.Length))

                    If newPath.StartsWith("\") = False Then newPath = "\" & newPath

                    destFolder = destFolder & newPath

                    If destFolder.EndsWith("\") = False Then destFolder &= "\"

                    clsMarsParser.parser.ParseDirectory(destFolder)

                    IO.File.Copy(s, destFolder & filename, overWrite)
                Next
            Else
                For Each s As String In results
                    Dim fileName As String = New IO.FileInfo(s).Name

                    If destFolder.EndsWith("\") = False Then destFolder &= "\"

                    IO.File.Copy(s, destFolder & fileName, overWrite)
                Next
            End If
        End If

    End Function

    Private Function doSearch(ByVal startFolder As String, ByVal mask As String, ByRef results As ArrayList)

        If mask.Contains("*") Then
            For Each file As String In IO.Directory.GetFiles(startFolder)
                Dim info As IO.FileInfo = New IO.FileInfo(file)

                If info.Name.ToLower Like mask.ToLower Then
                    results.Add(file)
                End If
            Next

            For Each dir As String In IO.Directory.GetDirectories(startFolder)
                doSearch(dir, mask, results)
            Next
        Else
            For Each file As String In IO.Directory.GetFiles(startFolder)
                Dim info As IO.FileInfo = New IO.FileInfo(file)

                If info.Name.ToLower = mask.ToLower Then
                    results.Add(file)
                End If
            Next

            For Each dir As String In IO.Directory.GetDirectories(startFolder)
                doSearch(dir, mask, results)
            Next
        End If

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filePath">the path to the file you want to delete - can contain wild cards</param>
    ''' <param name="recursive">search file in sub folders</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DoDeleteFiles(ByVal filePath As String, _
    Optional ByVal recursive As Boolean = True) As Boolean
        Dim results As ArrayList = New ArrayList
        Dim startPath As String = ""
        Dim mask As String = ""

        If recursive = True Then
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try

            doSearch(startPath, mask, results)

            For Each s As String In results
                Dim fileName As String = New IO.FileInfo(s).Name

                System.IO.File.Delete(s)
            Next

        Else
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try
            If filePath.Contains("*") = True Then


                For Each s As String In IO.Directory.GetFiles(startPath)
                    'compare the file to the mask
                    Dim tmpfile As String = ExtractFileName(s)
                    If tmpfile Like mask Then
                        results.Add(s)
                    End If
                Next


            Else
                results.Add(filePath)
            End If

            For Each s As String In results

                IO.File.Delete(s)
            Next
        End If

        Return True

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="filePath">the path to the file you want to delete - can contain wild cards</param>
    ''' <param name="recursive">search file in sub folders</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function DoPrintFiles(ByVal filePath As String, _
    ByVal sPrinter As String, _
    Optional ByVal recursive As Boolean = True) As Boolean
        Dim sTemp As String
        Dim oPrint As clsPrinters = New clsPrinters
        Dim results As ArrayList = New ArrayList
        Dim startPath As String = ""
        Dim mask As String = ""

        sTemp = oPrint.GetDefaultPrinter
        oPrint.SetDefaultPrinter(sPrinter)

        If recursive = True Then
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try

            doSearch(startPath, mask, results)

            For Each s As String In results
                Dim fileName As String = New IO.FileInfo(s).Name

                If fileName.Length > 0 Then
                    Dim oProcess As New Process
                    oProcess.StartInfo.FileName = s
                    oProcess.StartInfo.Verb = "Print"
                    oProcess.StartInfo.CreateNoWindow = True
                    oProcess.Start()
                End If
            Next
            oPrint.SetDefaultPrinter(sTemp)
        Else
            Try
                startPath = New IO.FileInfo(filePath).DirectoryName
            Catch
                startPath = GetDirectory(filePath)
            End Try

            Try
                mask = New IO.FileInfo(filePath).Name
            Catch
                mask = ExtractFileName(filePath)
            End Try
            If filePath.Contains("*") = True Then
                For Each s As String In IO.Directory.GetFiles(startPath)

                    'compare the file to the mask
                    Dim tmpfile As String = ExtractFileName(s)
                    If tmpfile Like mask Then
                        results.Add(s)
                    End If

                Next
            Else
                results.Add(filePath)
            End If


            For Each s As String In results
                If s.Length > 0 Then
                    Dim oProcess As New Process
                    oProcess.StartInfo.FileName = s
                    oProcess.StartInfo.Verb = "Print"
                    oProcess.StartInfo.CreateNoWindow = True
                    oProcess.Start()
                End If
            Next
            oPrint.SetDefaultPrinter(sTemp)
        End If

        Return True

    End Function

    Public Enum dateConvertType As Integer
        READ = 0
        WRITE = 1
    End Enum
    <DebuggerStepThrough()> _
    Public Function CTimeZ(ByVal theDate As Date, ByVal type As dateConvertType) As Date
        Try
            Dim tz As New clsMarsTimeZones
            Dim result As Date

            If type = dateConvertType.READ Then
                result = tz.ToUZone(theDate)
            Else
                result = tz.ToMZone(theDate)
            End If

            Return result
        Catch
            Return theDate
        End Try
    End Function
End Module
