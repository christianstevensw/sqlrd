Imports System.Threading
Imports Microsoft.Win32
Imports DevComponents.DotNetBar
Imports SaveListView
Imports System.Drawing
Imports System.Collections
Imports System.Configuration
Imports System.ComponentModel
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports C1.Win.C1Schedule
Imports C1.C1Schedule
#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Or crystal_ver = 11.5 Or CRYSTAL_VER = 12 Then
Imports My.Crystal11
#End If

#If CRYSTAL_VER = 12 Then
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine
#End If
Public Class frmWindow
    Inherits System.Windows.Forms.Form
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Dim oSelTree As TreeView = tvFolders
    Private m_SortingColumn As ColumnHeader
    Dim WithEvents oTextBoxItem As TextBoxItem
    Public gItemList As ListViewItem()
    Dim sortBy As Integer = 0
    Dim sortOrder As SortOrder = SortOrder.Ascending
    Dim columns As Hashtable = Nothing
    Dim processOutlook As Boolean = False
    Dim oCharter As frmScheduleView = New frmScheduleView
    Dim ep As ErrorProvider = New ErrorProvider
    Dim m_selctedFolderItem As String = ""

#Region "Friends?"
    Friend WithEvents tbNav As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbCRD As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSmartFolders As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSystemFolders As DevComponents.DotNetBar.TabItem
    Friend WithEvents exSplitter As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents lsvWindow As System.Windows.Forms.ListView
    Friend WithEvents btnSingle As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamic As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnDynamicPackage As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnAutomation As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnEvent As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents btnBursting As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents BubbleButton1 As DevComponents.DotNetBar.BubbleButton
    Friend WithEvents BubbleButton2 As DevComponents.DotNetBar.BubbleButton

    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents mnuPackageRR As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleRR As System.Windows.Forms.MenuItem
    Friend IsLoaded As Boolean = False
    Friend IsFormLoaded As Boolean = False
    Dim oMulti As Boolean = False
    Friend WithEvents tmRefresh As System.Timers.Timer
    Friend WithEvents mnuEventPackage As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuAddSchedule As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem26 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPRename As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem30 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPStatus As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem32 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPExecute As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem37 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPProperties As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPSplit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPNew As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPExisting As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleShortCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageShortCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoShortcut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventShortcut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventPackageShortcut As System.Windows.Forms.MenuItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tbExpose As DevComponents.DotNetBar.TabItem
    Friend WithEvents c1sched As C1.Win.C1Schedule.C1Schedule
    Friend WithEvents lsvItems As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents optMonth As System.Windows.Forms.RadioButton
    Friend WithEvents optWeek As System.Windows.Forms.RadioButton
    Friend WithEvents optDay As System.Windows.Forms.RadioButton
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents lblInterval As System.Windows.Forms.Label
    Friend WithEvents TrackBar1 As System.Windows.Forms.TrackBar
    Friend WithEvents mnuSched As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ExecuteScheduleToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents PropertiesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gantt As C1.Win.C1Chart.C1Chart
    Friend WithEvents optGantt As System.Windows.Forms.RadioButton
    Friend WithEvents grpGantt As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents optMaximum As System.Windows.Forms.RadioButton
    Friend WithEvents optMinimum As System.Windows.Forms.RadioButton
    Friend WithEvents optAverage As System.Windows.Forms.RadioButton
    Friend WithEvents optMedian As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtStart As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtEnd As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnRedraw As System.Windows.Forms.Button
    Friend WithEvents tippy As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtFilter As System.Windows.Forms.TextBox
    Friend WithEvents navOutlook As DevExpress.XtraNavBar.NavBarControl
    Friend WithEvents NavBarGroup1 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupControlContainer1 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroupControlContainer2 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroupControlContainer3 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents NavBarGroup2 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroup3 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroup4 As DevExpress.XtraNavBar.NavBarGroup
    Friend WithEvents NavBarGroupControlContainer4 As DevExpress.XtraNavBar.NavBarGroupControlContainer
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents oCal As C1.Win.C1Schedule.C1Calendar
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Dim oMultiRes As DialogResult
#End Region
    Private Delegate Sub DeleteDesktopItem_A(ByVal sender As System.Object, ByVal e As System.EventArgs, ByRef shouldReturn As Boolean)

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents imgToolBar As System.Windows.Forms.ImageList
    Friend WithEvents ToolBarMenu As System.Windows.Forms.ContextMenu
    Friend WithEvents tvFolders As System.Windows.Forms.TreeView
    Friend WithEvents imgDesktopLarge As System.Windows.Forms.ImageList
    Friend WithEvents mnuSingleSchedule As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackagedSchedule As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleReport As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportRename As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportDisable As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportSingle As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleExecute As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportDelete As System.Windows.Forms.MenuItem
    Friend WithEvents mnuReportProperties As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackage As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageAdd As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageDisable As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageRename As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageExecute As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageDelete As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageProperties As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolder As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuFolderOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolderMove As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolderRename As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolderDelete As System.Windows.Forms.MenuItem
    Friend WithEvents imgDesktopSmall As System.Windows.Forms.ImageList
    Friend WithEvents mnuPackedReport As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAuto As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuAutoCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoRename As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoStatus As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem19 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoExecute As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoProperties As System.Windows.Forms.MenuItem
    Friend WithEvents tvSmartFolders As System.Windows.Forms.TreeView
    Friend WithEvents mnuEvent As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuEventCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventRename As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventStatus As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem21 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventDelete As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventProperties As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem20 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackagePaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageCopy As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToPackage As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSplitPackage As System.Windows.Forms.MenuItem
    Friend WithEvents DotNetBarManager1 As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents barLeftDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barRightDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barTopDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barBottomDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents mnuSingleRefresh As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem23 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageRefresh As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackedRefresh As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDisableAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExecuteAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRefreshAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEnableAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTestSchedule As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTestPackage As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEventTest As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem24 As System.Windows.Forms.MenuItem
    Friend WithEvents tvSystemFolders As System.Windows.Forms.TreeView
    Friend WithEvents mnuPackedStatus As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRecreateDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSystemFolders As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuViewSystem As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmWindow))
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Single Schedules"}, "document_chart.png", System.Drawing.Color.Black, System.Drawing.Color.NavajoWhite, Nothing)
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Package Schedules"}, "box.png", System.Drawing.Color.Black, System.Drawing.Color.PaleGreen, Nothing)
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Dynamic Schedules"}, 7, System.Drawing.Color.Empty, System.Drawing.Color.MediumOrchid, Nothing)
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Dynamic Packages"}, 13, System.Drawing.Color.Empty, System.Drawing.Color.Goldenrod, Nothing)
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Automation Schedules"}, "document_gear.png", System.Drawing.Color.Black, System.Drawing.Color.LightSteelBlue, Nothing)
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Event-Based Packages"}, "cube_molecule.png", System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer)), System.Drawing.Color.Yellow, Nothing)
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Data-Driven Schedules"}, 17, System.Drawing.Color.Empty, System.Drawing.Color.LightSkyBlue, Nothing)
        Dim ListViewGroup1 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Folders", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup2 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Automation Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup3 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Bursting Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup4 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Dynamic Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup5 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Dynamic Package Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup6 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Event-Based Packages", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup7 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Event-Based Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup8 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Packaged Reports", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup9 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Package Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup10 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Single Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup11 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Data-Driven Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup12 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Data-Driven Packages", System.Windows.Forms.HorizontalAlignment.Left)
        Me.ToolBarMenu = New System.Windows.Forms.ContextMenu
        Me.mnuSingleSchedule = New System.Windows.Forms.MenuItem
        Me.mnuPackagedSchedule = New System.Windows.Forms.MenuItem
        Me.imgToolBar = New System.Windows.Forms.ImageList(Me.components)
        Me.imgDesktopLarge = New System.Windows.Forms.ImageList(Me.components)
        Me.imgDesktopSmall = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuSingleReport = New System.Windows.Forms.ContextMenu
        Me.mnuReportCopy = New System.Windows.Forms.MenuItem
        Me.mnuReportPaste = New System.Windows.Forms.MenuItem
        Me.mnuReportRename = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuReportDisable = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSingleRefresh = New System.Windows.Forms.MenuItem
        Me.MenuItem23 = New System.Windows.Forms.MenuItem
        Me.mnuReportSingle = New System.Windows.Forms.MenuItem
        Me.mnuSingleExecute = New System.Windows.Forms.MenuItem
        Me.mnuReportDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.mnuReportProperties = New System.Windows.Forms.MenuItem
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.mnuToPackage = New System.Windows.Forms.MenuItem
        Me.mnuTestSchedule = New System.Windows.Forms.MenuItem
        Me.mnuSingleRR = New System.Windows.Forms.MenuItem
        Me.mnuSingleShortCut = New System.Windows.Forms.MenuItem
        Me.mnuPackage = New System.Windows.Forms.ContextMenu
        Me.mnuPackageAdd = New System.Windows.Forms.MenuItem
        Me.MenuItem20 = New System.Windows.Forms.MenuItem
        Me.mnuPackageCopy = New System.Windows.Forms.MenuItem
        Me.mnuPackagePaste = New System.Windows.Forms.MenuItem
        Me.mnuPackageRename = New System.Windows.Forms.MenuItem
        Me.MenuItem14 = New System.Windows.Forms.MenuItem
        Me.mnuPackageDisable = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuPackageRefresh = New System.Windows.Forms.MenuItem
        Me.mnuPackageOpen = New System.Windows.Forms.MenuItem
        Me.mnuPackageExecute = New System.Windows.Forms.MenuItem
        Me.mnuPackageDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.mnuPackageProperties = New System.Windows.Forms.MenuItem
        Me.mnuSplitPackage = New System.Windows.Forms.MenuItem
        Me.mnuTestPackage = New System.Windows.Forms.MenuItem
        Me.mnuPackageRR = New System.Windows.Forms.MenuItem
        Me.mnuPackageShortCut = New System.Windows.Forms.MenuItem
        Me.mnuFolder = New System.Windows.Forms.ContextMenu
        Me.mnuFolderOpen = New System.Windows.Forms.MenuItem
        Me.mnuFolderMove = New System.Windows.Forms.MenuItem
        Me.mnuFolderRename = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.mnuFolderDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.mnuRefreshAll = New System.Windows.Forms.MenuItem
        Me.mnuDisableAll = New System.Windows.Forms.MenuItem
        Me.mnuExecuteAll = New System.Windows.Forms.MenuItem
        Me.mnuEnableAll = New System.Windows.Forms.MenuItem
        Me.mnuPackedReport = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.mnuPackedStatus = New System.Windows.Forms.MenuItem
        Me.mnuPackedRefresh = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem24 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.mnuAuto = New System.Windows.Forms.ContextMenu
        Me.mnuAutoCopy = New System.Windows.Forms.MenuItem
        Me.mnuAutoPaste = New System.Windows.Forms.MenuItem
        Me.mnuAutoRename = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.mnuAutoStatus = New System.Windows.Forms.MenuItem
        Me.MenuItem19 = New System.Windows.Forms.MenuItem
        Me.mnuAutoExecute = New System.Windows.Forms.MenuItem
        Me.mnuAutoDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem22 = New System.Windows.Forms.MenuItem
        Me.mnuAutoProperties = New System.Windows.Forms.MenuItem
        Me.mnuAutoShortcut = New System.Windows.Forms.MenuItem
        Me.mnuSystemFolders = New System.Windows.Forms.ContextMenu
        Me.mnuViewSystem = New System.Windows.Forms.MenuItem
        Me.mnuEvent = New System.Windows.Forms.ContextMenu
        Me.mnuEventCopy = New System.Windows.Forms.MenuItem
        Me.mnuEventPaste = New System.Windows.Forms.MenuItem
        Me.mnuEventRename = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.mnuEventStatus = New System.Windows.Forms.MenuItem
        Me.MenuItem21 = New System.Windows.Forms.MenuItem
        Me.mnuEventDelete = New System.Windows.Forms.MenuItem
        Me.mnuEventProperties = New System.Windows.Forms.MenuItem
        Me.mnuEventTest = New System.Windows.Forms.MenuItem
        Me.mnuRecreateDB = New System.Windows.Forms.MenuItem
        Me.mnuEventShortcut = New System.Windows.Forms.MenuItem
        Me.DotNetBarManager1 = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.barBottomDockSite = New DevComponents.DotNetBar.DockSite
        Me.barLeftDockSite = New DevComponents.DotNetBar.DockSite
        Me.barRightDockSite = New DevComponents.DotNetBar.DockSite
        Me.barTopDockSite = New DevComponents.DotNetBar.DockSite
        Me.tvSystemFolders = New System.Windows.Forms.TreeView
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.tvSmartFolders = New System.Windows.Forms.TreeView
        Me.tbNav = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvFolders = New System.Windows.Forms.TreeView
        Me.tbCRD = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.navOutlook = New DevExpress.XtraNavBar.NavBarControl
        Me.NavBarGroup1 = New DevExpress.XtraNavBar.NavBarGroup
        Me.NavBarGroupControlContainer1 = New DevExpress.XtraNavBar.NavBarGroupControlContainer
        Me.NavBarGroupControlContainer2 = New DevExpress.XtraNavBar.NavBarGroupControlContainer
        Me.lsvItems = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.NavBarGroupControlContainer3 = New DevExpress.XtraNavBar.NavBarGroupControlContainer
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.optGantt = New System.Windows.Forms.RadioButton
        Me.optDay = New System.Windows.Forms.RadioButton
        Me.optMonth = New System.Windows.Forms.RadioButton
        Me.optWeek = New System.Windows.Forms.RadioButton
        Me.NavBarGroupControlContainer4 = New DevExpress.XtraNavBar.NavBarGroupControlContainer
        Me.lblInterval = New System.Windows.Forms.Label
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.TrackBar1 = New System.Windows.Forms.TrackBar
        Me.NavBarGroup2 = New DevExpress.XtraNavBar.NavBarGroup
        Me.NavBarGroup3 = New DevExpress.XtraNavBar.NavBarGroup
        Me.NavBarGroup4 = New DevExpress.XtraNavBar.NavBarGroup
        Me.tbExpose = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbSystemFolders = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbSmartFolders = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.exSplitter = New DevComponents.DotNetBar.ExpandableSplitter
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.grpGantt = New System.Windows.Forms.Panel
        Me.gantt = New C1.Win.C1Chart.C1Chart
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtFilter = New System.Windows.Forms.TextBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnRedraw = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtStart = New System.Windows.Forms.DateTimePicker
        Me.dtEnd = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.optMaximum = New System.Windows.Forms.RadioButton
        Me.optMinimum = New System.Windows.Forms.RadioButton
        Me.optAverage = New System.Windows.Forms.RadioButton
        Me.optMedian = New System.Windows.Forms.RadioButton
        Me.lsvWindow = New System.Windows.Forms.ListView
        Me.BubbleButton1 = New DevComponents.DotNetBar.BubbleButton
        Me.BubbleButton2 = New DevComponents.DotNetBar.BubbleButton
        Me.tmRefresh = New System.Timers.Timer
        Me.mnuEventPackage = New System.Windows.Forms.ContextMenu
        Me.mnuAddSchedule = New System.Windows.Forms.MenuItem
        Me.mnuEPNew = New System.Windows.Forms.MenuItem
        Me.mnuEPExisting = New System.Windows.Forms.MenuItem
        Me.MenuItem26 = New System.Windows.Forms.MenuItem
        Me.mnuEPCopy = New System.Windows.Forms.MenuItem
        Me.mnuEPPaste = New System.Windows.Forms.MenuItem
        Me.mnuEPRename = New System.Windows.Forms.MenuItem
        Me.MenuItem30 = New System.Windows.Forms.MenuItem
        Me.mnuEPStatus = New System.Windows.Forms.MenuItem
        Me.MenuItem32 = New System.Windows.Forms.MenuItem
        Me.mnuEPOpen = New System.Windows.Forms.MenuItem
        Me.mnuEPExecute = New System.Windows.Forms.MenuItem
        Me.mnuEPDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem37 = New System.Windows.Forms.MenuItem
        Me.mnuEPProperties = New System.Windows.Forms.MenuItem
        Me.mnuEPSplit = New System.Windows.Forms.MenuItem
        Me.mnuEventPackageShortcut = New System.Windows.Forms.MenuItem
        Me.mnuSched = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ExecuteScheduleToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.PropertiesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tippy = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSingle = New DevComponents.DotNetBar.BubbleButton
        Me.btnPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamic = New DevComponents.DotNetBar.BubbleButton
        Me.btnDynamicPackage = New DevComponents.DotNetBar.BubbleButton
        Me.btnAutomation = New DevComponents.DotNetBar.BubbleButton
        Me.btnEvent = New DevComponents.DotNetBar.BubbleButton
        Me.btnBursting = New DevComponents.DotNetBar.BubbleButton
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbNav.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.navOutlook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.navOutlook.SuspendLayout()
        Me.NavBarGroupControlContainer2.SuspendLayout()
        Me.NavBarGroupControlContainer3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.NavBarGroupControlContainer4.SuspendLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel3.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.grpGantt.SuspendLayout()
        CType(Me.gantt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.tmRefresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuSched.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolBarMenu
        '
        Me.ToolBarMenu.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSingleSchedule, Me.mnuPackagedSchedule})
        '
        'mnuSingleSchedule
        '
        Me.mnuSingleSchedule.Index = 0
        Me.mnuSingleSchedule.Text = "Single Schedule Wizard"
        '
        'mnuPackagedSchedule
        '
        Me.mnuPackagedSchedule.Index = 1
        Me.mnuPackagedSchedule.Text = "Packaged Schedule Wizard"
        '
        'imgToolBar
        '
        Me.imgToolBar.ImageStream = CType(resources.GetObject("imgToolBar.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgToolBar.TransparentColor = System.Drawing.Color.Transparent
        Me.imgToolBar.Images.SetKeyName(0, "")
        Me.imgToolBar.Images.SetKeyName(1, "")
        Me.imgToolBar.Images.SetKeyName(2, "")
        Me.imgToolBar.Images.SetKeyName(3, "")
        Me.imgToolBar.Images.SetKeyName(4, "")
        Me.imgToolBar.Images.SetKeyName(5, "")
        Me.imgToolBar.Images.SetKeyName(6, "")
        '
        'imgDesktopLarge
        '
        Me.imgDesktopLarge.ImageStream = CType(resources.GetObject("imgDesktopLarge.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgDesktopLarge.TransparentColor = System.Drawing.Color.Transparent
        Me.imgDesktopLarge.Images.SetKeyName(0, "folder.png")
        Me.imgDesktopLarge.Images.SetKeyName(1, "document_chart.png")
        Me.imgDesktopLarge.Images.SetKeyName(2, "box.png")
        Me.imgDesktopLarge.Images.SetKeyName(3, "")
        Me.imgDesktopLarge.Images.SetKeyName(4, "box_closed_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(5, "document_gear.png")
        Me.imgDesktopLarge.Images.SetKeyName(6, "document_gear_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(7, "document_pulse.png")
        Me.imgDesktopLarge.Images.SetKeyName(8, "")
        Me.imgDesktopLarge.Images.SetKeyName(9, "document_atoms.png")
        Me.imgDesktopLarge.Images.SetKeyName(10, "document_atoms_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(11, "document_attachment.png")
        Me.imgDesktopLarge.Images.SetKeyName(12, "")
        Me.imgDesktopLarge.Images.SetKeyName(13, "box_new.png")
        Me.imgDesktopLarge.Images.SetKeyName(14, "box_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(15, "cube_molecule.png")
        Me.imgDesktopLarge.Images.SetKeyName(16, "cube_molecule_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(17, "document_flash.png")
        Me.imgDesktopLarge.Images.SetKeyName(18, "document_flash_bw.png")
        Me.imgDesktopLarge.Images.SetKeyName(19, "data driven box.png")
        Me.imgDesktopLarge.Images.SetKeyName(20, "data driven box bw.png")
        '
        'imgDesktopSmall
        '
        Me.imgDesktopSmall.ImageStream = CType(resources.GetObject("imgDesktopSmall.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgDesktopSmall.TransparentColor = System.Drawing.Color.Transparent
        Me.imgDesktopSmall.Images.SetKeyName(0, "folder.png")
        Me.imgDesktopSmall.Images.SetKeyName(1, "document_chart.png")
        Me.imgDesktopSmall.Images.SetKeyName(2, "box.png")
        Me.imgDesktopSmall.Images.SetKeyName(3, "")
        Me.imgDesktopSmall.Images.SetKeyName(4, "box_closed_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(5, "document_gear.png")
        Me.imgDesktopSmall.Images.SetKeyName(6, "document_gear_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(7, "document_pulse.png")
        Me.imgDesktopSmall.Images.SetKeyName(8, "")
        Me.imgDesktopSmall.Images.SetKeyName(9, "document_atoms.png")
        Me.imgDesktopSmall.Images.SetKeyName(10, "document_atoms_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(11, "document_attachment.png")
        Me.imgDesktopSmall.Images.SetKeyName(12, "")
        Me.imgDesktopSmall.Images.SetKeyName(13, "box_new.png")
        Me.imgDesktopSmall.Images.SetKeyName(14, "box_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(15, "cube_molecule.png")
        Me.imgDesktopSmall.Images.SetKeyName(16, "cube_molecule_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(17, "document_flash.png")
        Me.imgDesktopSmall.Images.SetKeyName(18, "document_flash_bw.png")
        Me.imgDesktopSmall.Images.SetKeyName(19, "data driven box.png")
        Me.imgDesktopSmall.Images.SetKeyName(20, "data driven box bw.png")
        '
        'mnuSingleReport
        '
        Me.mnuSingleReport.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuReportCopy, Me.mnuReportPaste, Me.mnuReportRename, Me.MenuItem4, Me.mnuReportDisable, Me.MenuItem6, Me.mnuSingleRefresh, Me.MenuItem23, Me.mnuReportSingle, Me.mnuSingleExecute, Me.mnuReportDelete, Me.MenuItem10, Me.mnuReportProperties, Me.MenuItem15, Me.mnuToPackage, Me.mnuTestSchedule, Me.mnuSingleRR, Me.mnuSingleShortCut})
        '
        'mnuReportCopy
        '
        Me.mnuReportCopy.Index = 0
        Me.mnuReportCopy.Text = "Copy"
        '
        'mnuReportPaste
        '
        Me.mnuReportPaste.Enabled = False
        Me.mnuReportPaste.Index = 1
        Me.mnuReportPaste.Text = "Paste"
        '
        'mnuReportRename
        '
        Me.mnuReportRename.Index = 2
        Me.mnuReportRename.Text = "Rename"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 3
        Me.MenuItem4.Text = "-"
        '
        'mnuReportDisable
        '
        Me.mnuReportDisable.Index = 4
        Me.mnuReportDisable.Text = "Disable"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 5
        Me.MenuItem6.Text = "-"
        '
        'mnuSingleRefresh
        '
        Me.mnuSingleRefresh.Index = 6
        Me.mnuSingleRefresh.Text = "Refresh Schedule"
        '
        'MenuItem23
        '
        Me.MenuItem23.Index = 7
        Me.MenuItem23.Text = "-"
        '
        'mnuReportSingle
        '
        Me.mnuReportSingle.Index = 8
        Me.mnuReportSingle.Text = "Preview Report"
        '
        'mnuSingleExecute
        '
        Me.mnuSingleExecute.Index = 9
        Me.mnuSingleExecute.Text = "Execute Schedule"
        '
        'mnuReportDelete
        '
        Me.mnuReportDelete.Index = 10
        Me.mnuReportDelete.Text = "Delete Schedule"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 11
        Me.MenuItem10.Text = "-"
        '
        'mnuReportProperties
        '
        Me.mnuReportProperties.Index = 12
        Me.mnuReportProperties.Text = "Properties"
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 13
        Me.MenuItem15.Text = "-"
        '
        'mnuToPackage
        '
        Me.mnuToPackage.Index = 14
        Me.mnuToPackage.Text = "Convert To Package"
        '
        'mnuTestSchedule
        '
        Me.mnuTestSchedule.Index = 15
        Me.mnuTestSchedule.Text = "Test Schedule"
        '
        'mnuSingleRR
        '
        Me.mnuSingleRR.Index = 16
        Me.mnuSingleRR.Text = "DisableRR"
        '
        'mnuSingleShortCut
        '
        Me.mnuSingleShortCut.Index = 17
        Me.mnuSingleShortCut.Text = "Create Shortcut"
        '
        'mnuPackage
        '
        Me.mnuPackage.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuPackageAdd, Me.MenuItem20, Me.mnuPackageCopy, Me.mnuPackagePaste, Me.mnuPackageRename, Me.MenuItem14, Me.mnuPackageDisable, Me.MenuItem3, Me.mnuPackageRefresh, Me.mnuPackageOpen, Me.mnuPackageExecute, Me.mnuPackageDelete, Me.MenuItem12, Me.mnuPackageProperties, Me.mnuSplitPackage, Me.mnuTestPackage, Me.mnuPackageRR, Me.mnuPackageShortCut})
        '
        'mnuPackageAdd
        '
        Me.mnuPackageAdd.Index = 0
        Me.mnuPackageAdd.Text = "Add Report"
        '
        'MenuItem20
        '
        Me.MenuItem20.Index = 1
        Me.MenuItem20.Text = "-"
        '
        'mnuPackageCopy
        '
        Me.mnuPackageCopy.Index = 2
        Me.mnuPackageCopy.Text = "Copy"
        '
        'mnuPackagePaste
        '
        Me.mnuPackagePaste.Index = 3
        Me.mnuPackagePaste.Text = "Paste"
        '
        'mnuPackageRename
        '
        Me.mnuPackageRename.Index = 4
        Me.mnuPackageRename.Text = "Rename"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 5
        Me.MenuItem14.Text = "-"
        '
        'mnuPackageDisable
        '
        Me.mnuPackageDisable.Index = 6
        Me.mnuPackageDisable.Text = "Disable"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 7
        Me.MenuItem3.Text = "-"
        '
        'mnuPackageRefresh
        '
        Me.mnuPackageRefresh.Index = 8
        Me.mnuPackageRefresh.Text = "Refresh"
        '
        'mnuPackageOpen
        '
        Me.mnuPackageOpen.Index = 9
        Me.mnuPackageOpen.Text = "Open Package"
        '
        'mnuPackageExecute
        '
        Me.mnuPackageExecute.Index = 10
        Me.mnuPackageExecute.Text = "Execute Schedule"
        '
        'mnuPackageDelete
        '
        Me.mnuPackageDelete.Index = 11
        Me.mnuPackageDelete.Text = "Delete Package"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 12
        Me.MenuItem12.Text = "-"
        '
        'mnuPackageProperties
        '
        Me.mnuPackageProperties.Index = 13
        Me.mnuPackageProperties.Text = "Properties"
        '
        'mnuSplitPackage
        '
        Me.mnuSplitPackage.Index = 14
        Me.mnuSplitPackage.Text = "Split into Single Schedules"
        '
        'mnuTestPackage
        '
        Me.mnuTestPackage.Index = 15
        Me.mnuTestPackage.Text = "Test Schedule"
        '
        'mnuPackageRR
        '
        Me.mnuPackageRR.Index = 16
        Me.mnuPackageRR.Text = "Disable RR"
        '
        'mnuPackageShortCut
        '
        Me.mnuPackageShortCut.Index = 17
        Me.mnuPackageShortCut.Text = "Create Shortcut"
        '
        'mnuFolder
        '
        Me.mnuFolder.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFolderOpen, Me.mnuFolderMove, Me.mnuFolderRename, Me.MenuItem8, Me.mnuFolderDelete, Me.MenuItem16})
        '
        'mnuFolderOpen
        '
        Me.mnuFolderOpen.Index = 0
        Me.mnuFolderOpen.Text = "Open..."
        '
        'mnuFolderMove
        '
        Me.mnuFolderMove.Index = 1
        Me.mnuFolderMove.Text = "Move..."
        '
        'mnuFolderRename
        '
        Me.mnuFolderRename.Index = 2
        Me.mnuFolderRename.Text = "Rename..."
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'mnuFolderDelete
        '
        Me.mnuFolderDelete.Index = 4
        Me.mnuFolderDelete.Text = "Delete..."
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 5
        Me.MenuItem16.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuRefreshAll, Me.mnuDisableAll, Me.mnuExecuteAll, Me.mnuEnableAll})
        Me.MenuItem16.Text = "All"
        '
        'mnuRefreshAll
        '
        Me.mnuRefreshAll.Index = 0
        Me.mnuRefreshAll.Text = "Refresh"
        '
        'mnuDisableAll
        '
        Me.mnuDisableAll.Index = 1
        Me.mnuDisableAll.Text = "Disable"
        '
        'mnuExecuteAll
        '
        Me.mnuExecuteAll.Index = 2
        Me.mnuExecuteAll.Text = "Execute"
        '
        'mnuEnableAll
        '
        Me.mnuEnableAll.Index = 3
        Me.mnuEnableAll.Text = "Enable"
        '
        'mnuPackedReport
        '
        Me.mnuPackedReport.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem2, Me.mnuPackedStatus, Me.mnuPackedRefresh, Me.MenuItem5, Me.MenuItem7, Me.MenuItem24, Me.MenuItem9, Me.MenuItem11, Me.MenuItem13})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Add Repot"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 1
        Me.MenuItem2.Text = "-"
        '
        'mnuPackedStatus
        '
        Me.mnuPackedStatus.Index = 2
        Me.mnuPackedStatus.Text = "Enable"
        '
        'mnuPackedRefresh
        '
        Me.mnuPackedRefresh.Index = 3
        Me.mnuPackedRefresh.Text = "Refresh"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 4
        Me.MenuItem5.Text = "Rename"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Preview Report"
        '
        'MenuItem24
        '
        Me.MenuItem24.Index = 6
        Me.MenuItem24.Text = "Execute Report"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 7
        Me.MenuItem9.Text = "Delete Report"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 8
        Me.MenuItem11.Text = "-"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 9
        Me.MenuItem13.Text = "Properties"
        '
        'mnuAuto
        '
        Me.mnuAuto.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAutoCopy, Me.mnuAutoPaste, Me.mnuAutoRename, Me.MenuItem17, Me.mnuAutoStatus, Me.MenuItem19, Me.mnuAutoExecute, Me.mnuAutoDelete, Me.MenuItem22, Me.mnuAutoProperties, Me.mnuAutoShortcut})
        '
        'mnuAutoCopy
        '
        Me.mnuAutoCopy.Index = 0
        Me.mnuAutoCopy.Text = "Copy"
        '
        'mnuAutoPaste
        '
        Me.mnuAutoPaste.Index = 1
        Me.mnuAutoPaste.Text = "Paste"
        '
        'mnuAutoRename
        '
        Me.mnuAutoRename.Index = 2
        Me.mnuAutoRename.Text = "Rename"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 3
        Me.MenuItem17.Text = "-"
        '
        'mnuAutoStatus
        '
        Me.mnuAutoStatus.Index = 4
        Me.mnuAutoStatus.Text = "Enabled"
        '
        'MenuItem19
        '
        Me.MenuItem19.Index = 5
        Me.MenuItem19.Text = "-"
        '
        'mnuAutoExecute
        '
        Me.mnuAutoExecute.Index = 6
        Me.mnuAutoExecute.Text = "Execute Schedule"
        '
        'mnuAutoDelete
        '
        Me.mnuAutoDelete.Index = 7
        Me.mnuAutoDelete.Text = "Delete Schedule"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 8
        Me.MenuItem22.Text = "-"
        '
        'mnuAutoProperties
        '
        Me.mnuAutoProperties.Index = 9
        Me.mnuAutoProperties.Text = "Properties"
        '
        'mnuAutoShortcut
        '
        Me.mnuAutoShortcut.Index = 10
        Me.mnuAutoShortcut.Text = "Create Shortcut"
        '
        'mnuSystemFolders
        '
        Me.mnuSystemFolders.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuViewSystem})
        '
        'mnuViewSystem
        '
        Me.mnuViewSystem.Index = 0
        Me.mnuViewSystem.Text = "View as Report"
        '
        'mnuEvent
        '
        Me.mnuEvent.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEventCopy, Me.mnuEventPaste, Me.mnuEventRename, Me.MenuItem18, Me.mnuEventStatus, Me.MenuItem21, Me.mnuEventDelete, Me.mnuEventProperties, Me.mnuEventTest, Me.mnuRecreateDB, Me.mnuEventShortcut})
        '
        'mnuEventCopy
        '
        Me.mnuEventCopy.Index = 0
        Me.mnuEventCopy.Text = "Copy"
        '
        'mnuEventPaste
        '
        Me.mnuEventPaste.Index = 1
        Me.mnuEventPaste.Text = "Paste"
        '
        'mnuEventRename
        '
        Me.mnuEventRename.Index = 2
        Me.mnuEventRename.Text = "Rename"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 3
        Me.MenuItem18.Text = "-"
        '
        'mnuEventStatus
        '
        Me.mnuEventStatus.Index = 4
        Me.mnuEventStatus.Text = "Enabled"
        '
        'MenuItem21
        '
        Me.MenuItem21.Index = 5
        Me.MenuItem21.Text = "-"
        '
        'mnuEventDelete
        '
        Me.mnuEventDelete.Index = 6
        Me.mnuEventDelete.Text = "Delete Schedule"
        '
        'mnuEventProperties
        '
        Me.mnuEventProperties.Index = 7
        Me.mnuEventProperties.Text = "Properties"
        '
        'mnuEventTest
        '
        Me.mnuEventTest.Index = 8
        Me.mnuEventTest.Text = "Test"
        '
        'mnuRecreateDB
        '
        Me.mnuRecreateDB.Index = 9
        Me.mnuRecreateDB.Text = "Recreate Database Snapshot"
        '
        'mnuEventShortcut
        '
        Me.mnuEventShortcut.Index = 10
        Me.mnuEventShortcut.Text = "Create Shortcut"
        '
        'DotNetBarManager1
        '
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.DotNetBarManager1.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.DotNetBarManager1.BottomDockSite = Me.barBottomDockSite
        Me.DotNetBarManager1.DefinitionName = "frmWindow.DotNetBarManager1.xml"
        Me.DotNetBarManager1.LeftDockSite = Me.barLeftDockSite
        Me.DotNetBarManager1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.DotNetBarManager1.ParentForm = Me
        Me.DotNetBarManager1.RightDockSite = Me.barRightDockSite
        Me.DotNetBarManager1.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.DotNetBarManager1.ThemeAware = False
        Me.DotNetBarManager1.TopDockSite = Me.barTopDockSite
        '
        'barBottomDockSite
        '
        Me.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barBottomDockSite.Location = New System.Drawing.Point(0, 685)
        Me.barBottomDockSite.Name = "barBottomDockSite"
        Me.barBottomDockSite.NeedsLayout = False
        Me.barBottomDockSite.Size = New System.Drawing.Size(1129, 22)
        Me.barBottomDockSite.TabIndex = 19
        Me.barBottomDockSite.TabStop = False
        '
        'barLeftDockSite
        '
        Me.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left
        Me.barLeftDockSite.Location = New System.Drawing.Point(0, 51)
        Me.barLeftDockSite.Name = "barLeftDockSite"
        Me.barLeftDockSite.NeedsLayout = False
        Me.barLeftDockSite.Size = New System.Drawing.Size(0, 634)
        Me.barLeftDockSite.TabIndex = 16
        Me.barLeftDockSite.TabStop = False
        '
        'barRightDockSite
        '
        Me.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right
        Me.barRightDockSite.Location = New System.Drawing.Point(1129, 51)
        Me.barRightDockSite.Name = "barRightDockSite"
        Me.barRightDockSite.NeedsLayout = False
        Me.barRightDockSite.Size = New System.Drawing.Size(0, 634)
        Me.barRightDockSite.TabIndex = 17
        Me.barRightDockSite.TabStop = False
        '
        'barTopDockSite
        '
        Me.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top
        Me.barTopDockSite.Location = New System.Drawing.Point(0, 0)
        Me.barTopDockSite.Name = "barTopDockSite"
        Me.barTopDockSite.NeedsLayout = False
        Me.barTopDockSite.Size = New System.Drawing.Size(1129, 51)
        Me.barTopDockSite.TabIndex = 18
        Me.barTopDockSite.TabStop = False
        '
        'tvSystemFolders
        '
        Me.tvSystemFolders.BackColor = System.Drawing.Color.White
        Me.DotNetBarManager1.SetContextMenuEx(Me.tvSystemFolders, "mSystemFolders")
        Me.tvSystemFolders.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvSystemFolders.FullRowSelect = True
        Me.tvSystemFolders.HideSelection = False
        Me.tvSystemFolders.ImageIndex = 0
        Me.tvSystemFolders.ImageList = Me.imgFolders
        Me.tvSystemFolders.Indent = 19
        Me.tvSystemFolders.ItemHeight = 16
        Me.tvSystemFolders.Location = New System.Drawing.Point(1, 1)
        Me.tvSystemFolders.Name = "tvSystemFolders"
        Me.tvSystemFolders.SelectedImageIndex = 0
        Me.tvSystemFolders.ShowLines = False
        Me.tvSystemFolders.ShowRootLines = False
        Me.tvSystemFolders.Size = New System.Drawing.Size(229, 632)
        Me.tvSystemFolders.TabIndex = 0
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "home.ico")
        Me.imgFolders.Images.SetKeyName(1, "folder_closed.png")
        Me.imgFolders.Images.SetKeyName(2, "folder.png")
        Me.imgFolders.Images.SetKeyName(3, "box_closed.png")
        Me.imgFolders.Images.SetKeyName(4, "box.png")
        Me.imgFolders.Images.SetKeyName(5, "folder_cubes.png")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "folder_information.png")
        Me.imgFolders.Images.SetKeyName(12, "cube_molecule.png")
        Me.imgFolders.Images.SetKeyName(13, "cube_molecule_bw.png")
        '
        'tvSmartFolders
        '
        Me.tvSmartFolders.BackColor = System.Drawing.Color.White
        Me.tvSmartFolders.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DotNetBarManager1.SetContextMenuEx(Me.tvSmartFolders, "mSmartFolders")
        Me.tvSmartFolders.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvSmartFolders.HideSelection = False
        Me.tvSmartFolders.ImageIndex = 0
        Me.tvSmartFolders.ImageList = Me.imgFolders
        Me.tvSmartFolders.Indent = 19
        Me.tvSmartFolders.ItemHeight = 16
        Me.tvSmartFolders.LabelEdit = True
        Me.tvSmartFolders.Location = New System.Drawing.Point(1, 1)
        Me.tvSmartFolders.Name = "tvSmartFolders"
        Me.tvSmartFolders.SelectedImageIndex = 0
        Me.tvSmartFolders.ShowLines = False
        Me.tvSmartFolders.ShowRootLines = False
        Me.tvSmartFolders.Size = New System.Drawing.Size(229, 632)
        Me.tvSmartFolders.TabIndex = 0
        '
        'tbNav
        '
        Me.tbNav.CanReorderTabs = True
        Me.tbNav.Controls.Add(Me.TabControlPanel1)
        Me.tbNav.Controls.Add(Me.TabControlPanel4)
        Me.tbNav.Controls.Add(Me.TabControlPanel3)
        Me.tbNav.Controls.Add(Me.TabControlPanel2)
        Me.tbNav.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbNav.Location = New System.Drawing.Point(0, 51)
        Me.tbNav.Name = "tbNav"
        Me.tbNav.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbNav.SelectedTabIndex = 0
        Me.tbNav.Size = New System.Drawing.Size(256, 634)
        Me.tbNav.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Dock
        Me.tbNav.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbNav.TabIndex = 0
        Me.tbNav.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbNav.Tabs.Add(Me.tbCRD)
        Me.tbNav.Tabs.Add(Me.tbExpose)
        Me.tbNav.Tabs.Add(Me.tbSmartFolders)
        Me.tbNav.Tabs.Add(Me.tbSystemFolders)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.VS2005
        Me.TabControlPanel1.Controls.Add(Me.tvFolders)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(231, 634)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.WhiteSmoke
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.WhiteSmoke
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbCRD
        '
        'tvFolders
        '
        Me.tvFolders.AllowDrop = True
        Me.tvFolders.BackColor = System.Drawing.Color.White
        Me.tvFolders.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tvFolders.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvFolders.ForeColor = System.Drawing.Color.Navy
        Me.tvFolders.HideSelection = False
        Me.tvFolders.ImageIndex = 0
        Me.tvFolders.ImageList = Me.imgFolders
        Me.tvFolders.Indent = 19
        Me.tvFolders.ItemHeight = 16
        Me.tvFolders.LabelEdit = True
        Me.tvFolders.Location = New System.Drawing.Point(1, 1)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.SelectedImageIndex = 0
        Me.tvFolders.ShowLines = False
        Me.tvFolders.ShowRootLines = False
        Me.tvFolders.Size = New System.Drawing.Size(229, 632)
        Me.tvFolders.Sorted = True
        Me.tvFolders.TabIndex = 7
        '
        'tbCRD
        '
        Me.tbCRD.AttachedControl = Me.TabControlPanel1
        Me.tbCRD.BackColor = System.Drawing.Color.WhiteSmoke
        Me.tbCRD.BackColor2 = System.Drawing.Color.WhiteSmoke
        Me.tbCRD.Image = CType(resources.GetObject("tbCRD.Image"), System.Drawing.Image)
        Me.tbCRD.Name = "tbCRD"
        Me.tbCRD.Text = "SQL-RD Explorer"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.Panel2)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(231, 634)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbExpose
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.navOutlook)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(1, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(229, 632)
        Me.Panel2.TabIndex = 0
        '
        'navOutlook
        '
        Me.navOutlook.ActiveGroup = Me.NavBarGroup1
        Me.navOutlook.Appearance.GroupBackground.BackColor = System.Drawing.Color.White
        Me.navOutlook.Appearance.GroupBackground.Options.UseBackColor = True
        Me.navOutlook.BackColor = System.Drawing.Color.White
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer2)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer3)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer1)
        Me.navOutlook.Controls.Add(Me.NavBarGroupControlContainer4)
        Me.navOutlook.Dock = System.Windows.Forms.DockStyle.Fill
        Me.navOutlook.Groups.AddRange(New DevExpress.XtraNavBar.NavBarGroup() {Me.NavBarGroup1, Me.NavBarGroup2, Me.NavBarGroup3, Me.NavBarGroup4})
        Me.navOutlook.Location = New System.Drawing.Point(0, 0)
        Me.navOutlook.Name = "navOutlook"
        Me.navOutlook.Size = New System.Drawing.Size(229, 632)
        Me.navOutlook.TabIndex = 27
        Me.navOutlook.Text = "NavBarControl1"
        Me.navOutlook.View = New DevExpress.XtraNavBar.ViewInfo.StandardSkinExplorerBarViewInfoRegistrator("The Asphalt World")
        '
        'NavBarGroup1
        '
        Me.NavBarGroup1.Caption = "Calendar"
        Me.NavBarGroup1.ControlContainer = Me.NavBarGroupControlContainer1
        Me.NavBarGroup1.Expanded = True
        Me.NavBarGroup1.GroupClientHeight = 160
        Me.NavBarGroup1.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup1.Name = "NavBarGroup1"
        '
        'NavBarGroupControlContainer1
        '
        Me.NavBarGroupControlContainer1.BackgroundImage = CType(resources.GetObject("NavBarGroupControlContainer1.BackgroundImage"), System.Drawing.Image)
        Me.NavBarGroupControlContainer1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.NavBarGroupControlContainer1.Name = "NavBarGroupControlContainer1"
        Me.NavBarGroupControlContainer1.Size = New System.Drawing.Size(227, 156)
        Me.NavBarGroupControlContainer1.TabIndex = 0
        '
        'NavBarGroupControlContainer2
        '
        Me.NavBarGroupControlContainer2.Controls.Add(Me.lsvItems)
        Me.NavBarGroupControlContainer2.Name = "NavBarGroupControlContainer2"
        Me.NavBarGroupControlContainer2.Size = New System.Drawing.Size(227, 156)
        Me.NavBarGroupControlContainer2.TabIndex = 1
        '
        'lsvItems
        '
        Me.lsvItems.BackColor = System.Drawing.Color.White
        Me.lsvItems.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lsvItems.CheckBoxes = True
        Me.lsvItems.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvItems.FullRowSelect = True
        Me.lsvItems.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        ListViewItem1.Checked = True
        ListViewItem1.StateImageIndex = 1
        ListViewItem1.Tag = "Report"
        ListViewItem2.Checked = True
        ListViewItem2.StateImageIndex = 1
        ListViewItem2.Tag = "Package"
        ListViewItem3.Checked = True
        ListViewItem3.StateImageIndex = 1
        ListViewItem3.Tag = "Dynamic"
        ListViewItem4.Checked = True
        ListViewItem4.StateImageIndex = 1
        ListViewItem4.Tag = "Dynamic Package"
        ListViewItem5.Checked = True
        ListViewItem5.StateImageIndex = 1
        ListViewItem5.Tag = "Automation"
        ListViewItem6.Checked = True
        ListViewItem6.StateImageIndex = 1
        ListViewItem6.Tag = "EventPackage"
        ListViewItem7.Checked = True
        ListViewItem7.StateImageIndex = 1
        ListViewItem7.Tag = "Data-Driven"
        Me.lsvItems.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7})
        Me.lsvItems.LargeImageList = Me.imgDesktopLarge
        Me.lsvItems.Location = New System.Drawing.Point(6, 0)
        Me.lsvItems.Name = "lsvItems"
        Me.lsvItems.Size = New System.Drawing.Size(179, 154)
        Me.lsvItems.SmallImageList = Me.imgDesktopSmall
        Me.lsvItems.TabIndex = 1
        Me.lsvItems.UseCompatibleStateImageBehavior = False
        Me.lsvItems.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "View"
        Me.ColumnHeader1.Width = 160
        '
        'NavBarGroupControlContainer3
        '
        Me.NavBarGroupControlContainer3.Controls.Add(Me.TableLayoutPanel1)
        Me.NavBarGroupControlContainer3.Name = "NavBarGroupControlContainer3"
        Me.NavBarGroupControlContainer3.Size = New System.Drawing.Size(227, 116)
        Me.NavBarGroupControlContainer3.TabIndex = 2
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.optGantt, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.optDay, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optMonth, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.optWeek, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(227, 116)
        Me.TableLayoutPanel1.TabIndex = 27
        '
        'optGantt
        '
        Me.optGantt.AutoSize = True
        Me.optGantt.BackColor = System.Drawing.Color.Transparent
        Me.optGantt.Location = New System.Drawing.Point(3, 90)
        Me.optGantt.Name = "optGantt"
        Me.optGantt.Size = New System.Drawing.Size(80, 17)
        Me.optGantt.TabIndex = 3
        Me.optGantt.TabStop = True
        Me.optGantt.Text = "Gantt chart"
        Me.optGantt.UseVisualStyleBackColor = False
        '
        'optDay
        '
        Me.optDay.AutoSize = True
        Me.optDay.BackColor = System.Drawing.Color.Transparent
        Me.optDay.Checked = True
        Me.optDay.Location = New System.Drawing.Point(3, 3)
        Me.optDay.Name = "optDay"
        Me.optDay.Size = New System.Drawing.Size(69, 17)
        Me.optDay.TabIndex = 0
        Me.optDay.TabStop = True
        Me.optDay.Text = "Day View"
        Me.optDay.UseVisualStyleBackColor = False
        '
        'optMonth
        '
        Me.optMonth.AutoSize = True
        Me.optMonth.BackColor = System.Drawing.Color.Transparent
        Me.optMonth.Location = New System.Drawing.Point(3, 61)
        Me.optMonth.Name = "optMonth"
        Me.optMonth.Size = New System.Drawing.Size(80, 17)
        Me.optMonth.TabIndex = 2
        Me.optMonth.Text = "Month View"
        Me.optMonth.UseVisualStyleBackColor = False
        '
        'optWeek
        '
        Me.optWeek.AutoSize = True
        Me.optWeek.BackColor = System.Drawing.Color.Transparent
        Me.optWeek.Location = New System.Drawing.Point(3, 32)
        Me.optWeek.Name = "optWeek"
        Me.optWeek.Size = New System.Drawing.Size(77, 17)
        Me.optWeek.TabIndex = 1
        Me.optWeek.Text = "Week View"
        Me.optWeek.UseVisualStyleBackColor = False
        '
        'NavBarGroupControlContainer4
        '
        Me.NavBarGroupControlContainer4.Controls.Add(Me.lblInterval)
        Me.NavBarGroupControlContainer4.Controls.Add(Me.btnRefresh)
        Me.NavBarGroupControlContainer4.Controls.Add(Me.TrackBar1)
        Me.NavBarGroupControlContainer4.Name = "NavBarGroupControlContainer4"
        Me.NavBarGroupControlContainer4.Size = New System.Drawing.Size(227, 106)
        Me.NavBarGroupControlContainer4.TabIndex = 3
        '
        'lblInterval
        '
        Me.lblInterval.AutoSize = True
        Me.lblInterval.BackColor = System.Drawing.Color.Transparent
        Me.lblInterval.Location = New System.Drawing.Point(3, 51)
        Me.lblInterval.Name = "lblInterval"
        Me.lblInterval.Size = New System.Drawing.Size(75, 13)
        Me.lblInterval.TabIndex = 2
        Me.lblInterval.Text = "Thirty Minutes"
        '
        'btnRefresh
        '
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(63, 74)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(99, 23)
        Me.btnRefresh.TabIndex = 3
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'TrackBar1
        '
        Me.TrackBar1.BackColor = System.Drawing.Color.White
        Me.TrackBar1.LargeChange = 1
        Me.TrackBar1.Location = New System.Drawing.Point(6, 3)
        Me.TrackBar1.Maximum = 6
        Me.TrackBar1.Name = "TrackBar1"
        Me.TrackBar1.Size = New System.Drawing.Size(217, 45)
        Me.TrackBar1.TabIndex = 1
        '
        'NavBarGroup2
        '
        Me.NavBarGroup2.Caption = "Filter"
        Me.NavBarGroup2.ControlContainer = Me.NavBarGroupControlContainer2
        Me.NavBarGroup2.Expanded = True
        Me.NavBarGroup2.GroupClientHeight = 160
        Me.NavBarGroup2.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup2.Name = "NavBarGroup2"
        '
        'NavBarGroup3
        '
        Me.NavBarGroup3.Caption = "View Style"
        Me.NavBarGroup3.ControlContainer = Me.NavBarGroupControlContainer3
        Me.NavBarGroup3.Expanded = True
        Me.NavBarGroup3.GroupClientHeight = 120
        Me.NavBarGroup3.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup3.Name = "NavBarGroup3"
        '
        'NavBarGroup4
        '
        Me.NavBarGroup4.Caption = "Time Interval"
        Me.NavBarGroup4.ControlContainer = Me.NavBarGroupControlContainer4
        Me.NavBarGroup4.Expanded = True
        Me.NavBarGroup4.GroupClientHeight = 110
        Me.NavBarGroup4.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.ControlContainer
        Me.NavBarGroup4.Name = "NavBarGroup4"
        '
        'tbExpose
        '
        Me.tbExpose.AttachedControl = Me.TabControlPanel4
        Me.tbExpose.Image = CType(resources.GetObject("tbExpose.Image"), System.Drawing.Image)
        Me.tbExpose.Name = "tbExpose"
        Me.tbExpose.Text = "SQL-RD Outlook"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.tvSystemFolders)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(231, 634)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbSystemFolders
        '
        'tbSystemFolders
        '
        Me.tbSystemFolders.AttachedControl = Me.TabControlPanel3
        Me.tbSystemFolders.Image = CType(resources.GetObject("tbSystemFolders.Image"), System.Drawing.Image)
        Me.tbSystemFolders.Name = "tbSystemFolders"
        Me.tbSystemFolders.Text = "System Folders"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.tvSmartFolders)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(25, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(231, 634)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbSmartFolders
        '
        'tbSmartFolders
        '
        Me.tbSmartFolders.AttachedControl = Me.TabControlPanel2
        Me.tbSmartFolders.Image = CType(resources.GetObject("tbSmartFolders.Image"), System.Drawing.Image)
        Me.tbSmartFolders.Name = "tbSmartFolders"
        Me.tbSmartFolders.Text = "Smart Folders"
        '
        'exSplitter
        '
        Me.exSplitter.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.exSplitter.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.exSplitter.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.ExpandableControl = Me.tbNav
        Me.exSplitter.ExpandFillColor = System.Drawing.SystemColors.ControlDarkDark
        Me.exSplitter.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.ExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.GripDarkColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.exSplitter.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.exSplitter.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.exSplitter.HotBackColor2 = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(240, Byte), Integer))
        Me.exSplitter.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.exSplitter.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.exSplitter.HotExpandFillColor = System.Drawing.SystemColors.ControlDarkDark
        Me.exSplitter.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.HotExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.exSplitter.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.exSplitter.HotGripDarkColor = System.Drawing.SystemColors.ControlDarkDark
        Me.exSplitter.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.exSplitter.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.exSplitter.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.exSplitter.Location = New System.Drawing.Point(256, 51)
        Me.exSplitter.Name = "exSplitter"
        Me.exSplitter.Size = New System.Drawing.Size(5, 634)
        Me.exSplitter.TabIndex = 22
        Me.exSplitter.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.grpGantt)
        Me.Panel1.Controls.Add(Me.lsvWindow)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(261, 51)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(868, 634)
        Me.Panel1.TabIndex = 25
        '
        'grpGantt
        '
        Me.grpGantt.Controls.Add(Me.gantt)
        Me.grpGantt.Controls.Add(Me.Panel4)
        Me.grpGantt.Location = New System.Drawing.Point(41, 28)
        Me.grpGantt.Name = "grpGantt"
        Me.grpGantt.Size = New System.Drawing.Size(787, 487)
        Me.grpGantt.TabIndex = 26
        Me.grpGantt.Visible = False
        '
        'gantt
        '
        Me.gantt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gantt.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.gantt.Location = New System.Drawing.Point(0, 0)
        Me.gantt.Name = "gantt"
        Me.gantt.PropBag = resources.GetString("gantt.PropBag")
        Me.gantt.Size = New System.Drawing.Size(787, 402)
        Me.gantt.TabIndex = 25
        Me.tippy.SetToolTip(Me.gantt, "Double-click me to save the chart to image file")
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.Panel4.Controls.Add(Me.GroupBox5)
        Me.Panel4.Controls.Add(Me.GroupBox3)
        Me.Panel4.Controls.Add(Me.GroupBox4)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(0, 402)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(787, 85)
        Me.Panel4.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label3)
        Me.GroupBox5.Controls.Add(Me.txtFilter)
        Me.GroupBox5.Location = New System.Drawing.Point(532, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(210, 71)
        Me.GroupBox5.TabIndex = 6
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Filter Schedules"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Name contains"
        '
        'txtFilter
        '
        Me.txtFilter.Location = New System.Drawing.Point(6, 38)
        Me.txtFilter.Name = "txtFilter"
        Me.txtFilter.Size = New System.Drawing.Size(198, 21)
        Me.txtFilter.TabIndex = 0
        Me.tippy.SetToolTip(Me.txtFilter, "Use "";"" for multiple filters and press ""Enter"" to confirm")
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnRedraw)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.dtStart)
        Me.GroupBox3.Controls.Add(Me.dtEnd)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Location = New System.Drawing.Point(3, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(282, 71)
        Me.GroupBox3.TabIndex = 5
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Please select the time range to use"
        '
        'btnRedraw
        '
        Me.btnRedraw.Image = CType(resources.GetObject("btnRedraw.Image"), System.Drawing.Image)
        Me.btnRedraw.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRedraw.Location = New System.Drawing.Point(190, 38)
        Me.btnRedraw.Name = "btnRedraw"
        Me.btnRedraw.Size = New System.Drawing.Size(75, 23)
        Me.btnRedraw.TabIndex = 4
        Me.btnRedraw.Text = "&Zoom"
        Me.btnRedraw.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Start Time"
        '
        'dtStart
        '
        Me.dtStart.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtStart.Location = New System.Drawing.Point(20, 40)
        Me.dtStart.Name = "dtStart"
        Me.dtStart.ShowUpDown = True
        Me.dtStart.Size = New System.Drawing.Size(71, 21)
        Me.dtStart.TabIndex = 0
        Me.dtStart.Value = New Date(2007, 5, 21, 0, 0, 0, 0)
        '
        'dtEnd
        '
        Me.dtEnd.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtEnd.Location = New System.Drawing.Point(100, 40)
        Me.dtEnd.Name = "dtEnd"
        Me.dtEnd.ShowUpDown = True
        Me.dtEnd.Size = New System.Drawing.Size(71, 21)
        Me.dtEnd.TabIndex = 1
        Me.dtEnd.Value = New Date(2007, 5, 21, 23, 59, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(97, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "End Time"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.optMaximum)
        Me.GroupBox4.Controls.Add(Me.optMinimum)
        Me.GroupBox4.Controls.Add(Me.optAverage)
        Me.GroupBox4.Controls.Add(Me.optMedian)
        Me.GroupBox4.Location = New System.Drawing.Point(291, 8)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(235, 71)
        Me.GroupBox4.TabIndex = 1
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Value Type"
        '
        'optMaximum
        '
        Me.optMaximum.AutoSize = True
        Me.optMaximum.Location = New System.Drawing.Point(129, 42)
        Me.optMaximum.Name = "optMaximum"
        Me.optMaximum.Size = New System.Drawing.Size(103, 17)
        Me.optMaximum.TabIndex = 3
        Me.optMaximum.Text = "Maximum values"
        Me.optMaximum.UseVisualStyleBackColor = True
        '
        'optMinimum
        '
        Me.optMinimum.AutoSize = True
        Me.optMinimum.Location = New System.Drawing.Point(129, 21)
        Me.optMinimum.Name = "optMinimum"
        Me.optMinimum.Size = New System.Drawing.Size(99, 17)
        Me.optMinimum.TabIndex = 2
        Me.optMinimum.Text = "Minimum values"
        Me.optMinimum.UseVisualStyleBackColor = True
        '
        'optAverage
        '
        Me.optAverage.AutoSize = True
        Me.optAverage.Location = New System.Drawing.Point(7, 42)
        Me.optAverage.Name = "optAverage"
        Me.optAverage.Size = New System.Drawing.Size(100, 17)
        Me.optAverage.TabIndex = 1
        Me.optAverage.Text = "Average values"
        Me.optAverage.UseVisualStyleBackColor = True
        '
        'optMedian
        '
        Me.optMedian.AutoSize = True
        Me.optMedian.Checked = True
        Me.optMedian.Location = New System.Drawing.Point(7, 21)
        Me.optMedian.Name = "optMedian"
        Me.optMedian.Size = New System.Drawing.Size(93, 17)
        Me.optMedian.TabIndex = 0
        Me.optMedian.TabStop = True
        Me.optMedian.Text = "Median values"
        Me.optMedian.UseVisualStyleBackColor = True
        '
        'lsvWindow
        '
        Me.lsvWindow.BackgroundImageTiled = True
        Me.lsvWindow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvWindow.FullRowSelect = True
        ListViewGroup1.Header = "Folders"
        ListViewGroup1.Name = "Folders"
        ListViewGroup2.Header = "Automation Schedules"
        ListViewGroup2.Name = "Automation Schedules"
        ListViewGroup3.Header = "Bursting Schedules"
        ListViewGroup3.Name = "Bursting Schedules"
        ListViewGroup4.Header = "Dynamic Schedules"
        ListViewGroup4.Name = "Dynamic Schedules"
        ListViewGroup5.Header = "Dynamic Package Schedules"
        ListViewGroup5.Name = "Dynamic Package Schedules"
        ListViewGroup6.Header = "Event-Based Packages"
        ListViewGroup6.Name = "Event-Based Packages"
        ListViewGroup7.Header = "Event-Based Schedules"
        ListViewGroup7.Name = "Event-Based Schedules"
        ListViewGroup8.Header = "Packaged Reports"
        ListViewGroup8.Name = "Packaged Reports"
        ListViewGroup9.Header = "Package Schedules"
        ListViewGroup9.Name = "Package Schedules"
        ListViewGroup10.Header = "Single Schedules"
        ListViewGroup10.Name = "Single Schedules"
        ListViewGroup11.Header = "Data-Driven Schedules"
        ListViewGroup11.Name = "Data-Driven Schedules"
        ListViewGroup12.Header = "Data-Driven Packages"
        ListViewGroup12.Name = "Data-Driven Packages"
        Me.lsvWindow.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup1, ListViewGroup2, ListViewGroup3, ListViewGroup4, ListViewGroup5, ListViewGroup6, ListViewGroup7, ListViewGroup8, ListViewGroup9, ListViewGroup10, ListViewGroup11, ListViewGroup12})
        Me.lsvWindow.HideSelection = False
        Me.lsvWindow.LabelEdit = True
        Me.lsvWindow.LargeImageList = Me.imgDesktopLarge
        Me.lsvWindow.Location = New System.Drawing.Point(0, 0)
        Me.lsvWindow.Name = "lsvWindow"
        Me.lsvWindow.Size = New System.Drawing.Size(868, 634)
        Me.lsvWindow.SmallImageList = Me.imgDesktopSmall
        Me.lsvWindow.TabIndex = 23
        Me.lsvWindow.UseCompatibleStateImageBehavior = False
        '
        'BubbleButton1
        '
        Me.BubbleButton1.Name = "BubbleButton1"
        '
        'BubbleButton2
        '
        Me.BubbleButton2.Name = "BubbleButton2"
        '
        'tmRefresh
        '
        Me.tmRefresh.SynchronizingObject = Me
        '
        'mnuEventPackage
        '
        Me.mnuEventPackage.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAddSchedule, Me.MenuItem26, Me.mnuEPCopy, Me.mnuEPPaste, Me.mnuEPRename, Me.MenuItem30, Me.mnuEPStatus, Me.MenuItem32, Me.mnuEPOpen, Me.mnuEPExecute, Me.mnuEPDelete, Me.MenuItem37, Me.mnuEPProperties, Me.mnuEPSplit, Me.mnuEventPackageShortcut})
        '
        'mnuAddSchedule
        '
        Me.mnuAddSchedule.Index = 0
        Me.mnuAddSchedule.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEPNew, Me.mnuEPExisting})
        Me.mnuAddSchedule.Text = "Add Schedule"
        '
        'mnuEPNew
        '
        Me.mnuEPNew.Index = 0
        Me.mnuEPNew.Text = "New"
        '
        'mnuEPExisting
        '
        Me.mnuEPExisting.Index = 1
        Me.mnuEPExisting.Text = "Existing"
        '
        'MenuItem26
        '
        Me.MenuItem26.Index = 1
        Me.MenuItem26.Text = "-"
        '
        'mnuEPCopy
        '
        Me.mnuEPCopy.Index = 2
        Me.mnuEPCopy.Text = "Copy"
        '
        'mnuEPPaste
        '
        Me.mnuEPPaste.Index = 3
        Me.mnuEPPaste.Text = "Paste"
        '
        'mnuEPRename
        '
        Me.mnuEPRename.Index = 4
        Me.mnuEPRename.Text = "Rename"
        '
        'MenuItem30
        '
        Me.MenuItem30.Index = 5
        Me.MenuItem30.Text = "-"
        '
        'mnuEPStatus
        '
        Me.mnuEPStatus.Index = 6
        Me.mnuEPStatus.Text = "Disable"
        '
        'MenuItem32
        '
        Me.MenuItem32.Index = 7
        Me.MenuItem32.Text = "-"
        '
        'mnuEPOpen
        '
        Me.mnuEPOpen.Index = 8
        Me.mnuEPOpen.Text = "Open Package"
        '
        'mnuEPExecute
        '
        Me.mnuEPExecute.Index = 9
        Me.mnuEPExecute.Text = "Execute Schedule"
        '
        'mnuEPDelete
        '
        Me.mnuEPDelete.Index = 10
        Me.mnuEPDelete.Text = "Delete Package"
        '
        'MenuItem37
        '
        Me.MenuItem37.Index = 11
        Me.MenuItem37.Text = "-"
        '
        'mnuEPProperties
        '
        Me.mnuEPProperties.Index = 12
        Me.mnuEPProperties.Text = "Properties"
        '
        'mnuEPSplit
        '
        Me.mnuEPSplit.Index = 13
        Me.mnuEPSplit.Text = "Split into Single Schedules"
        '
        'mnuEventPackageShortcut
        '
        Me.mnuEventPackageShortcut.Index = 14
        Me.mnuEventPackageShortcut.Text = "Create Shortcut"
        '
        'mnuSched
        '
        Me.mnuSched.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExecuteScheduleToolStripMenuItem, Me.ToolStripMenuItem1, Me.PropertiesToolStripMenuItem})
        Me.mnuSched.Name = "mnuSched"
        Me.mnuSched.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuSched.Size = New System.Drawing.Size(166, 54)
        '
        'ExecuteScheduleToolStripMenuItem
        '
        Me.ExecuteScheduleToolStripMenuItem.Name = "ExecuteScheduleToolStripMenuItem"
        Me.ExecuteScheduleToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.ExecuteScheduleToolStripMenuItem.Text = "Execute Schedule"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(162, 6)
        '
        'PropertiesToolStripMenuItem
        '
        Me.PropertiesToolStripMenuItem.Name = "PropertiesToolStripMenuItem"
        Me.PropertiesToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.PropertiesToolStripMenuItem.Text = "Properties"
        '
        'btnSingle
        '
        Me.btnSingle.Image = CType(resources.GetObject("btnSingle.Image"), System.Drawing.Image)
        Me.btnSingle.ImageLarge = CType(resources.GetObject("btnSingle.ImageLarge"), System.Drawing.Image)
        Me.btnSingle.Name = "btnSingle"
        Me.btnSingle.TooltipText = "Single Schedule"
        '
        'btnPackage
        '
        Me.btnPackage.Image = CType(resources.GetObject("btnPackage.Image"), System.Drawing.Image)
        Me.btnPackage.ImageLarge = CType(resources.GetObject("btnPackage.ImageLarge"), System.Drawing.Image)
        Me.btnPackage.Name = "btnPackage"
        Me.btnPackage.TooltipText = "Packaged Reports Schedule"
        '
        'btnDynamic
        '
        Me.btnDynamic.Image = CType(resources.GetObject("btnDynamic.Image"), System.Drawing.Image)
        Me.btnDynamic.ImageLarge = CType(resources.GetObject("btnDynamic.ImageLarge"), System.Drawing.Image)
        Me.btnDynamic.Name = "btnDynamic"
        Me.btnDynamic.TooltipText = "Dynamic Schedule"
        '
        'btnDynamicPackage
        '
        Me.btnDynamicPackage.Image = CType(resources.GetObject("btnDynamicPackage.Image"), System.Drawing.Image)
        Me.btnDynamicPackage.ImageLarge = CType(resources.GetObject("btnDynamicPackage.ImageLarge"), System.Drawing.Image)
        Me.btnDynamicPackage.Name = "btnDynamicPackage"
        Me.btnDynamicPackage.TooltipText = "Dynamic Package Schedule"
        '
        'btnAutomation
        '
        Me.btnAutomation.Image = CType(resources.GetObject("btnAutomation.Image"), System.Drawing.Image)
        Me.btnAutomation.ImageLarge = CType(resources.GetObject("btnAutomation.ImageLarge"), System.Drawing.Image)
        Me.btnAutomation.Name = "btnAutomation"
        Me.btnAutomation.TooltipText = "Automation Schedule"
        '
        'btnEvent
        '
        Me.btnEvent.Image = CType(resources.GetObject("btnEvent.Image"), System.Drawing.Image)
        Me.btnEvent.ImageLarge = CType(resources.GetObject("btnEvent.ImageLarge"), System.Drawing.Image)
        Me.btnEvent.Name = "btnEvent"
        Me.btnEvent.TooltipText = "Event-Based Schedule"
        '
        'btnBursting
        '
        Me.btnBursting.Image = CType(resources.GetObject("btnBursting.Image"), System.Drawing.Image)
        Me.btnBursting.ImageLarge = CType(resources.GetObject("btnBursting.ImageLarge"), System.Drawing.Image)
        Me.btnBursting.Name = "btnBursting"
        Me.btnBursting.TooltipText = "Bursting Schedule"
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SelectAllToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(165, 26)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(164, 22)
        Me.SelectAllToolStripMenuItem.Text = "Select All"
        Me.SelectAllToolStripMenuItem.Visible = False
        '
        'frmWindow
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(1129, 707)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.exSplitter)
        Me.Controls.Add(Me.tbNav)
        Me.Controls.Add(Me.barLeftDockSite)
        Me.Controls.Add(Me.barRightDockSite)
        Me.Controls.Add(Me.barTopDockSite)
        Me.Controls.Add(Me.barBottomDockSite)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmWindow"
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbNav.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        CType(Me.navOutlook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.navOutlook.ResumeLayout(False)
        Me.NavBarGroupControlContainer2.ResumeLayout(False)
        Me.NavBarGroupControlContainer3.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.NavBarGroupControlContainer4.ResumeLayout(False)
        Me.NavBarGroupControlContainer4.PerformLayout()
        CType(Me.TrackBar1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.grpGantt.ResumeLayout(False)
        CType(Me.gantt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.tmRefresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuSched.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Enum enCalcType As Integer
        MEDIAN = 0
        AVERAGE = 1
        MINIMUM = 2
        MAXIMUM = 3
    End Enum

    Private Enum scheduleType
        SINGLES = 0
        PACKAGE = 1
        SINGLEDYNAMIC = 2
        PACKAGEDYNAMIC = 3
        AUTOMATION = 4
        EVENTBASED = 5
        BURSTING = 6
        EVENTPACKAGE = 7
    End Enum

    Private ReadOnly Property m_excludeTypes() As String()
        Get
            Try
                Dim results As String()
                Dim I As Integer = 0

                For Each item As ListViewItem In Me.lsvItems.Items
                    If item.Checked = False Then
                        ReDim Preserve results(I)

                        results(I) = item.Tag

                        I += 1
                    End If
                Next

                Return results
            Catch
                Return Nothing
            End Try
        End Get
    End Property

    Private ReadOnly Property m_CalcType() As enCalcType
        Get
            If Me.optAverage.Checked Then
                Return enCalcType.AVERAGE
            ElseIf Me.optMedian.Checked Then
                Return enCalcType.MEDIAN
            ElseIf Me.optMinimum.Checked Then
                Return enCalcType.MINIMUM
            ElseIf Me.optMaximum.Checked Then
                Return enCalcType.MAXIMUM
            End If
        End Get

    End Property
    Public Sub SaveColumnData()

        Try

            columns = New Hashtable

            For Each col As ColumnHeader In lsvWindow.Columns
                columns.Add(col.Text, col.Width)
            Next

        Catch ex As Exception
            ''console.writeLine(ex.Message)
        End Try
    End Sub

    Public Sub RestoreColumnData()

        IsLoaded = False

        Try
            If columns Is Nothing Then
                Dim fs As FileStream = New FileStream(sAppPath & "columns.dat", FileMode.Open, FileAccess.Read)

                Dim columns As Hashtable = New Hashtable
                Dim bf As BinaryFormatter = New BinaryFormatter

                columns = CType(bf.Deserialize(fs), Hashtable)

                fs.Close()
            End If

            For Each col As ColumnHeader In lsvWindow.Columns
                If columns.ContainsKey(col.Text) = True Then
                    col.Width = columns.Item(col.Text)
                End If
            Next

        Catch : End Try

        '

        Try

            If oMain.m_formSize IsNot Nothing Then
                Me.Width = oMain.m_formSize("Width")
                Me.Height = oMain.m_formSize("Height")
            Else
                Dim size As String = oUI.ReadRegistry("frmWindow", "824,638")

                Me.Width = size.Split(",")(0)
                Me.Height = size.Split(",")(1)
            End If

            tbNav.Width = oUI.ReadRegistry("NavBar", 160)
        Catch : End Try

        IsLoaded = True
    End Sub

    Private Sub frmWindow_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        On Error Resume Next

        nWindowCount -= 1

        If nWindowCount < 0 Then nWindowCount = 0

        nWindowCurrent = nWindowCount

        oUI.SaveRegistry("NavBar", tbNav.Width)

    End Sub

    Private Sub ProcessToolBars()
        Dim oBar As Bar
        Dim oShowTools As Boolean = True
        Dim oVMenu As ButtonItem

        oBar = Me.DotNetBarManager1.Bars("bar1")

        Try
            oShowTools = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShowToolBar", 1)))
        Catch ex As Exception
            oShowTools = True
        End Try

        oVMenu = oMain.DotNetBarManager1.GetItem("item_622", True)

        oBar.Visible = oShowTools

        If oVMenu IsNot Nothing Then
            oVMenu.Checked = oShowTools
        End If

        oBar = Me.DotNetBarManager1.Bars("bar3")

        Try
            oShowTools = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShowToolBar3", 1)))
        Catch ex As Exception
            oShowTools = True
        End Try

        oVMenu = oMain.DotNetBarManager1.GetItem("btnTools", True)

        oBar.Visible = oShowTools

        If oVMenu IsNot Nothing Then
            oVMenu.Checked = oShowTools
        End If

        oBar = Me.DotNetBarManager1.Bars("statusBar2")

        Try
            oShowTools = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShowStatusBar", 1)))
        Catch
            oShowTools = True
        End Try

        oVMenu = oMain.DotNetBarManager1.GetItem("btnStatusBar", True)

        oBar.Visible = oShowTools

        If oVMenu IsNot Nothing Then
            oVMenu.Checked = oShowTools
        End If
    End Sub

    Private Sub frmWindow_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        tmRefresh.Stop()
    End Sub

    Private Sub frmWindow_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim fs As FileStream = New FileStream(sAppPath & "columns.dat", FileMode.Create, FileAccess.ReadWrite)
            Dim bf As BinaryFormatter = New BinaryFormatter

            bf.Serialize(fs, columns)

            fs.Close()
        Catch : End Try
    End Sub
    Private Sub frmWindow_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim oNode As TreeNode
            Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName

10:         oUI.BuildTree(tvFolders, , False)

20:         oUI.AddSmartFolders(tvSmartFolders, True)

30:         oUI.AddSystemFolders(tvSystemFolders)

            Try : tvFolders.SelectedNode = tvFolders.Nodes(0) : Catch : End Try

40:         FormatForWinXP(Me)

50:         oTextBoxItem = DotNetBarManager1.GetItem("mSearch", True)

60:         ProcessToolBars()

70:         Try
80:             exSplitter.Expanded = Convert.ToBoolean(CType(oUI.ReadRegistry("ExpandSplitter", 1), Integer))
90:         Catch ex As Exception
100:            exSplitter.Expanded = True
            End Try

            Dim oBtn As ButtonItem

110:        If MailType = gMailType.SMTP Then
120:            oBtn = DotNetBarManager1.GetItem("btnSMTPManager", True)

130:            If MailType = gMailType.SMTP Then
140:                If oBtn IsNot Nothing Then
150:                    oBtn.Enabled = True
                    End If
                End If
            End If

160:        Me.Show()

170:        RestoreColumnData()

180:        Me.tbNav.Width = oUI.ReadRegistry("NavBar", tbNav.Width)

            Dim sView As String = oUI.ReadRegistry("WindowViewStyle", "LargeIcon")

190:        Select Case sView.ToLower
                Case "largeicon"
200:                oUI.SetViewStyle(View.LargeIcon)
210:            Case "smallicon"
220:                oUI.SetViewStyle(View.SmallIcon)
230:            Case "list"
240:                oUI.SetViewStyle(View.List)
250:            Case "tile"
260:                oUI.SetViewStyle(View.Tile)
            End Select

            Dim oLabel As LabelItem = DotNetBarManager1.GetItem("lblSys", True)

270:        If oLabel IsNot Nothing Then
280:            If gConType = "DAT" Then
290:                oLabel.Text = "File System"
300:            Else
310:                oLabel.Text = "SQL Server/ODBC"
                End If
            End If

320:        oLabel = DotNetBarManager1.GetItem("lblUser", True)

330:        If oLabel IsNot Nothing Then
340:            oLabel.Text = gRole & "\" & gUser
            End If

350:        If oUI.ReadRegistry("DTRefresh", "0") = "1" Then
                Dim interval As Integer = oUI.ReadRegistry("DTRefreshInterval", 180)

360:            With tmRefresh
370:                .Enabled = True
380:                .Interval = interval * 1000
390:                .Start()
                End With
            End If

            Dim windowState As String = oUI.ReadRegistry("MainWindowState", "Normal")

400:        Select Case windowState
                Case "Normal"
410:                Me.WindowState = FormWindowState.Normal

420:                If oMain.m_formSize IsNot Nothing Then
430:                    Me.Width = oMain.m_formSize("Width")
440:                    Me.Height = oMain.m_formSize("Height")
450:                Else
                        Dim size As String = oUI.ReadRegistry("frmWindow", "824,638")

460:                    Me.Width = size.Split(",")(0)
470:                    Me.Height = size.Split(",")(1)
                    End If

480:            Case "Maximized"
490:                Me.WindowState = FormWindowState.Maximized
500:            Case "Minimized"
510:                Me.WindowState = FormWindowState.Minimized
            End Select

520:        IsFormLoaded = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Private Sub tvFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvFolders.AfterSelect

        Dim sItem As String
        Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
        Dim sView As String = oUI.ReadRegistry("WindowViewStyle", "LargeIcon")
        Dim nID
        Dim oButton As ButtonItem

        oButton = DotNetBarManager1.GetItem("item_171", True)

        oButton.Enabled = False

        sItem = GetDelimitedWord(tvFolders.SelectedNode.Tag, 1, ":")

        nID = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")

        Me.m_selctedFolderItem = e.Node.Tag

        lsvWindow.BeginUpdate()

        Select Case sItem.ToLower
            Case "folder", "desktop"

                If sView.ToLower = "details" Then
                    oUI.SetViewStyle(View.Details)
                Else
                    oUI.CreateDeskTop(tvFolders.SelectedNode, lsvWindow)
                End If

                If sItem.ToLower = "folder" Then
                    DotNetBarManager1.SetContextMenuEx(tvFolders, "mTreeFolder")
                Else
                    DotNetBarManager1.SetContextMenuEx(tvFolders, Nothing)
                End If

            Case "package"
                If sView.ToLower = "details" Then
                    oUI.BuildPackageDetails(GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":"), Me)
                Else
                    oUI.OpenPackage(tvFolders.SelectedNode, lsvWindow)
                End If

                DotNetBarManager1.SetContextMenuEx(tvFolders, Nothing)
            Case "event-package"
                If sView.ToLower = "details" Then
                    oUI.BuildEventPackageDetails(CType(tvFolders.SelectedNode.Tag, String).Split(":")(1), Me)
                Else
                    oUI.OpenEventPackage(tvFolders.SelectedNode, lsvWindow)
                End If

                DotNetBarManager1.SetContextMenuEx(tvFolders, Nothing)
            Case "smartfolder"
                Dim oSmart As New clsMarsUI

                oSmart.OpenSmartFolder(nID, lsvWindow)

                DotNetBarManager1.SetContextMenuEx(tvFolders, "mSmartFolders")

        End Select

        Me.Text = tvFolders.SelectedNode.FullPath

        On Error Resume Next

        tvFolders.SelectedNode.Expand()

        ReDim gItemList(lsvWindow.Items.Count - 1)

        For I As Integer = 0 To lsvWindow.Items.Count - 1
            gItemList(I) = lsvWindow.Items(I)
        Next

        If sItem.ToLower = "folder" Then
            gParent = tvFolders.SelectedNode.FullPath
            gParentID = tvFolders.SelectedNode.Tag.split(":")(1)
        Else
            gParent = ""
            gParentID = 0
        End If

        oTextBoxItem.ControlText = String.Empty

        Dim oBtn As ButtonItem = DotNetBarManager1.GetItem("item_170")
        Dim oDesign As ButtonItem = oMain.DotNetBarManager1.GetItem("item_377")
        Dim oTask As ButtonItem = oMain.DotNetBarManager1.GetItem("item_113")

        oBtn.Enabled = False
        oTask.Enabled = False
        oDesign.Enabled = False

        oMain.btnDesign.Enabled = False
        oMain.btnProperties.Enabled = False

        Dim oLabel As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oLabel.Text = lsvWindow.Items.Count & " items"

        oBtn = DotNetBarManager1.GetItem("btnUp", True)

        If tvFolders.SelectedNode Is tvFolders.Nodes(0) Then
            oBtn.Enabled = False
        Else
            oBtn.Enabled = True
        End If

        Me.RestoreColumnData()

        lsvWindow.EndUpdate()
    End Sub


    Private Sub mnuSingleSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleSchedule.Click
        Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

        oSingle.ShowDialog()
    End Sub

    Private Sub mnuPackagedSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackagedSchedule.Click
        Dim oPackage As frmPackWizard = New frmPackWizard

        oPackage.ShowDialog()
    End Sub

    Private Sub mnuSingleExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleExecute.Click
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim ShowMsg As Boolean
        Dim oMsg As frmMarsMsg = New frmMarsMsg
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim nReportID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        nReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT Dynamic,Bursting FROM ReportAttr WHERE ReportID = " & nReportID)

        If oRs.EOF = False Then
            If IsNull(oRs.Fields(0).Value, 0) = 1 Then

                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.DynamicScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EXECUTE)
            ElseIf IsNull(oRs.Fields(1).Value, 0) = 1 Then

                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.BurstingScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EXECUTE)

            Else
                oExecute.xReportID = nReportID

                AppStatus(True)
                oExecute.SingleScheduleThread()
                AppStatus(False)

                clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EXECUTE)
            End If
        End If

        oRs.Close()

        oData.DisposeRecordset(oRs)

    End Sub

    Private Sub mnuReportDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportDelete.Click
        'If MessageBox.Show("Delete the '" & lsvWindow.SelectedItems(0).Text & "' schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
        '    oUI.DeleteReport(GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":").ToLower, 0)
        '    lsvWindow.SelectedItems(0).Remove()

        '    Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        '    oItems.Text = lsvWindow.Items.Count & " items"
        'End If
        DeleteDesktopItem(sender, e, New Boolean = False)
    End Sub



    Private Sub mnuPackageExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageExecute.Click
        Dim oPackage As clsMarsReport = New clsMarsReport
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim Success As Boolean
        Dim nPackID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        nPackID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        oExecute.xPackID = nPackID

        AppStatus(True)
        oExecute.PackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EXECUTE)
    End Sub

    Private Sub mnuPackageDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageDelete.Click
        'Dim sCurrentNode As String = tvFolders.SelectedNode.Tag

        'If MessageBox.Show("Delete the '" & lsvWindow.SelectedItems(0).Text & "' package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
        '    oUI.DeletePackage(GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":").ToLower)
        '    lsvWindow.SelectedItems(0).Remove()

        '    tvFolders.BeginUpdate()
        '    oUI.BuildTree(tvFolders, False, False)
        '    oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
        '    tvFolders.EndUpdate()
        'End If


        DeleteDesktopItem(sender, e, New Boolean = False)

    End Sub

    Private Sub mnuPackageRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageRename.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuReportRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportRename.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuFolderRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolderRename.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuReportProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportProperties.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        Dim nReportID As Integer
        Dim frmProp As frmSingleProp = New frmSingleProp
        Dim oNode As TreeNode
        Dim selectedItem As String

        If Not tvFolders.SelectedNode Is Nothing Then
            oNode = tvFolders.SelectedNode
        End If

        nReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        selectedItem = lsvWindow.SelectedItems(0).Text

        frmProp.EditSchedule(nReportID)

        If Not oNode Is Nothing Then
            For Each o As TreeNode In tvFolders.Nodes
                If o.Text = oNode.Text Then
                    tvFolders.SelectedNode = o
                    Exit For
                End If
            Next
        End If

    End Sub



    Private Sub mnuPackageProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageProperties.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        Dim frmProp As frmPackageProp = New frmPackageProp
        Dim nPackID As Int32

        nPackID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        frmProp.EditPackage(nPackID)
    End Sub


    Private Sub mnuReportCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportCopy.Click
        If lsvWindow.SelectedItems.Count > 0 Then
            gClipboard(0) = "Report"
            gClipboard(1) = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

            mnuReportPaste.Enabled = True

            Dim oItem As ButtonItem

            oItem = DotNetBarManager1.GetItem("mPaste", True)

            oItem.Enabled = True

            oItem = DotNetBarManager1.GetItem("mDesktopPaste", True)

            oItem.Enabled = True
        End If
    End Sub

    Private Sub mnuReportPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportPaste.Click
        Dim nCopy As Integer
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim nParent As Integer
        Dim sType As String

        oUI.BusyProgress(10, "Loading report...")

        Try
            sType = tvFolders.SelectedNode.Tag.SPlit(":")(0)
        Catch
            sType = ""
        End Try

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        End If

        nCopy = oReport.CopyReport(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmSingleProp = New frmSingleProp

            oUI.BusyProgress(99, "Loading new schedule properties...")

            oUI.BusyProgress(, , True)

            oProp.EditSchedule(nCopy)

            oMain.mnuViewRefresh_Click(sender, e)
        End If

    End Sub



    Private Sub mnuReportSingle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportSingle.Click
        Dim nReportID As Integer

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        nReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        Dim oThread As clsMarsThreading = New clsMarsThreading

        oThread.xReportID = nReportID

        oThread.PreviewReport()

    End Sub

    Private Sub mnuSingleReport_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleReport.Popup
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nReportID As Integer

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        nReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        SQL = "SELECT Status FROM ScheduleAttr WHERE PackID = 0 AND ReportID = " & nReportID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs("status").Value = 0 Then
                mnuReportDisable.Text = "Enable"
            Else
                mnuReportDisable.Text = "Disable"
            End If
        End If


        oRs.Close()
    End Sub

    Private Sub mnuReportDisable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReportDisable.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nReportID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem
        Dim NewImage As Integer

        olsv = lsvWindow.SelectedItems(0)

        oItem = DotNetBarManager1.GetItem("mStatus", True)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1

                If olsv.ImageIndex = 3 Then
                    NewImage = 1
                Else
                    NewImage = 7
                End If

                olsv.ImageIndex = NewImage

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0

                If olsv.ImageIndex = 1 Then
                    NewImage = 3
                Else
                    NewImage = 8
                End If

                olsv.ImageIndex = NewImage

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE PackID = 0 AND ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        'oUI.RefreshView(Me)

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        If clsMarsData.IsScheduleDynamic(nReportID, "Report") = True Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.DYNAMIC, oAction)
        ElseIf oData.IsScheduleBursting(nReportID) = True Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.BURST, oAction)
        Else
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.SINGLES, oAction)
        End If

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuPackageAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageAdd.Click
        Dim oForm As frmPackedReport
        Dim sReturn() As String
        Dim rs As ADODB.Recordset
        Dim SQL As String
        Dim nCount As Integer
        Dim oData As clsMarsData = New clsMarsData
        Dim nPackId As Integer

        nPackId = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")


        'SQL = "SELECT ReportTitle FROM ReportAttr " & _
        '"WHERE ReportID = (SELECT MAX(ReportID) FROM ReportAttr WHERE PackID =" & nPackId & ") AND PackID =" & nPackId

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID = " & nPackId

        rs = clsMarsData.GetData(SQL)

        If rs Is Nothing Then
            nCount = 1
        Else
            Try
                Dim sTemp As String = rs.Fields(0).Value

                nCount = sTemp

                nCount += 1
            Catch
                nCount = 1
            End Try
        End If

        rs.Close()

        oForm = New frmPackedReport

        sReturn = oForm.AddReport(nCount, nPackId)


        MessageBox.Show("Report(s) added successfully!", Application.ProductName, _
        MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub mnuPackage_Popup(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackage.Popup
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nPackID As Integer

        nPackID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        SQL = "SELECT Status FROM ScheduleAttr WHERE PackID = " & nPackID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            If oRs("status").Value = 0 Then
                mnuPackageDisable.Text = "Enable"
            Else
                mnuPackageDisable.Text = "Disable"
            End If
        End If

        oRs.Close()
    End Sub

    Private Sub mnuPackageDisable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageDisable.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nPackID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem

        oItem = DotNetBarManager1.GetItem("mPackStatus", True)
        olsv = lsvWindow.SelectedItems(0)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1
                olsv.ImageIndex = 2

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0
                olsv.ImageIndex = 4

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE PackID = " & nPackID

        clsMarsData.WriteData(SQL)

        Dim node As TreeNode = tvFolders.SelectedNode

        For Each n As TreeNode In node.Nodes
            If n.Tag = "Package:" & nPackID Then
                If nValue = 1 Then
                    n.ImageIndex = 3
                Else
                    n.ImageIndex = 9
                End If
            End If
        Next

        'oUI.RefreshView(oWindow(nWindowCurrent))

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.PACKED, oAction)

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuPackageOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageOpen.Click
        Dim sType As String = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":")

        Dim nFolderId As Int32 = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim sParent As String = lsvWindow.SelectedItems(0).Text
        Dim sParentTag As String = lsvWindow.SelectedItems(0).Tag
        Dim oNode As TreeNode


        nWindowCount += 1
        ReDim Preserve oWindow(nWindowCount)
        oWindow(nWindowCount) = New frmWindow
        oWindow(nWindowCount).MdiParent = oMain
        oWindow(nWindowCount).Show()
        oWindow(nWindowCount).Tag = nWindowCount
        nWindowCurrent = nWindowCount

        oUI.OpenWindow(nFolderId, oWindow(nWindowCount).lsvWindow)

        oWindow(nWindowCount).Text = sParent

        oUI.FindNode(sParentTag, oWindow(nWindowCount).tvFolders, oWindow(nWindowCount).tvFolders.Nodes(0))
    End Sub


    Private Sub mnuFolderDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolderDelete.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oResponse As DialogResult

        For Each oI As ListViewItem In lsvWindow.SelectedItems

            If oMulti = True Then
                oResponse = oMultiRes
            Else
                oResponse = MessageBox.Show("Delete the '" & oI.Text & "' folder? All contents will be lost.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            End If

            If oResponse = DialogResult.Yes Then
                oUI.DeleteFolder(GetDelimitedWord(oI.Tag, 2, ":"))
                oUI.RefreshView(Me)
            End If
        Next

        Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oItems.Text = lsvWindow.Items.Count & " items"
    End Sub



    Private Sub mnuFolderOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolderOpen.Click
        lsvWindow_DoubleClick(sender, e)
    End Sub


    Private Sub mnuFolderMove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolderMove.Click
        Dim nFolderID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        oUI.MoveFolder(nFolderID)
        oUI.RefreshView(Me)
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        Dim oReport As clsMarsReport = New clsMarsReport

        'oReport._PreviewReport(nID)

    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub


        Dim oUI As clsMarsUI = New clsMarsUI

        For Each oI As ListViewItem In lsvWindow.SelectedItems
            Dim nID As Integer = GetDelimitedWord(oI.Tag, 2, ":")
            Dim oData As clsMarsData = New clsMarsData
            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim Response As DialogResult

            If oMulti = True Then
                Response = oMultiRes
            Else
                Response = MessageBox.Show("Delete the " & oI.Text & " report?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            End If


            If Response = DialogResult.Yes Then
                SQL = "SELECT PackID FROM ReportAttr WHERE ReportID =" & nID

                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing And oRs.EOF = False Then
                    oUI.DeleteReport(nID, oRs("packid").Value)
                End If

                oData.DisposeRecordset(oRs)

                lsvWindow.Items.Remove(oI)
            End If
        Next

        Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oItems.Text = lsvWindow.Items.Count & " items"
    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        Dim oForm As frmPackedReport
        Dim nID As Integer = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        Dim oData As clsMarsData = New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sDestination As String
        Dim nValue As Integer = 0
        Dim nTemp As Integer = 0


        SQL = "SELECT DestinationType FROM DestinationAttr WHERE PackID = " & nID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Or oRs.EOF = True Then Exit Sub

        sDestination = oRs("destinationtype").Value

        oRs.Close()



        oRs = clsMarsData.GetData("SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID = " & nID)

        If oRs Is Nothing Then Exit Sub

        Do While oRs.EOF = False
            nTemp = oRs(0).Value

            If nTemp > nValue Then nValue = nTemp

            oRs.MoveNext()
        Loop

        oRs.Close()

        oData.DisposeRecordset(oRs)

        nValue += 1

        oForm = New frmPackedReport

        oForm.AddReport(nValue, nID)


        oUI.RefreshView(Me)

        oUI.FindNode(tvFolders.SelectedNode.Tag, tvFolders, tvFolders.Nodes(0))

    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem13.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub
        Dim oForm As frmPackedReport = New frmPackedReport
        Dim PackID As Integer
        Dim ReportID As Integer

        PackID = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        ReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        oForm.EditReport(ReportID)
    End Sub



    Private Sub DeleteDesktopItem(ByVal sender As System.Object, ByVal e As System.EventArgs, ByRef shouldReturn As Boolean)
        shouldReturn = False
        Dim i As Integer = 1
        Dim count As Integer = 1
        Dim sCurrentNode As String

        If lsvWindow.SelectedItems.Count = 0 Then Return

        If lsvWindow.SelectedItems.Count > 1 Then
            oMulti = True
        Else
            oMulti = False
        End If

        If tvFolders.SelectedNode Is Nothing Then
            sCurrentNode = "Desktop:0"
        Else
            sCurrentNode = tvFolders.SelectedNode.Tag
        End If

        count = lsvWindow.SelectedItems.Count

        If oMulti = True Then
            oMultiRes = MessageBox.Show("Are you sure you would like to delete these " & count & " items?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        End If

        lsvWindow.BeginUpdate()

        For Each oI As ListViewItem In lsvWindow.SelectedItems
            Dim type As String
            Dim nID As Integer

            oUI.BusyProgress(((i / count) * 100), "Deleting items...", False)

            type = oI.Tag.split(":")(0)
            nID = oI.Tag.split(":")(1)

            Select Case type.ToLower
                Case "report"
                    If oMulti = True Then
                        If oMultiRes = DialogResult.Yes Then
                            oUI.DeleteReport(nID, 0)
                            oI.Remove()
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            oUI.DeleteReport(nID, 0)
                            oI.Remove()
                        End If
                    End If
                Case "package"
                    If oMulti = True Then
                        If oMultiRes = DialogResult.Yes Then
                            oUI.DeletePackage(nID)
                            oI.Remove()

                            tvFolders.BeginUpdate()
                            oUI.BuildTree(tvFolders, False, False)
                            oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
                            tvFolders.EndUpdate()
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                            oUI.DeletePackage(nID)
                            oI.Remove()

                            tvFolders.BeginUpdate()
                            oUI.BuildTree(tvFolders, False, False)
                            oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
                            tvFolders.EndUpdate()
                        End If
                    End If
                Case "folder"
                    oUI.DeleteFolder(nID)
                    oI.Remove()
                    'mnuFolderDelete_Click(sender, e)
                Case "automation"
                    If oMulti = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            oUI.DeleteAutoSchedule(nID)
                            oI.Remove()
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' automation schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            oUI.DeleteAutoSchedule(nID)
                            oI.Remove()
                        End If
                    End If
                    shouldReturn = True
                Case "event"
                    If oMulti = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEvent(nID)
                            oI.Remove()
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' event-based schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEvent(nID)
                            oI.Remove()
                        End If
                    End If
                    shouldReturn = True
                Case "packed report"
                    Dim Sql As String = "SELECT PackID FROM ReportAttr WHERE ReportID =" & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(Sql)

                    If Not oRs Is Nothing And oRs.EOF = False Then
                        oUI.DeleteReport(nID, oRs("packid").Value)
                    End If

                    oRs.Close()
                    oI.Remove()
                    shouldReturn = True
                Case "event-package"
                    If oMulti = True Then
                        If oMultiRes = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEventPackage(nID)
                            oI.Remove()
                        End If
                    Else
                        If MessageBox.Show("Delete the '" & oI.Text & "' event-based package?", Application.ProductName, MessageBoxButtons.YesNo, _
                         MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                            clsMarsEvent.DeleteEventPackage(nID)
                            oI.Remove()
                        End If
                    End If
            End Select

            i += 1

        Next

        lsvWindow.EndUpdate()
        Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oItems.Text = lsvWindow.Items.Count & " items"

        oUI.BusyProgress(, , True)
    End Sub
    Private Sub DotNetBarManager1_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DotNetBarManager1.ItemClick
        Try
            Dim oItem As BaseItem
            Dim bar As Bar
            Dim SQL As String
            Dim oData As New clsMarsData
            Dim oResponse As DialogResult
            Dim sCurrentNode As String

            oMulti = False

            If tvFolders.SelectedNode Is Nothing Then
                sCurrentNode = "Desktop"
            Else
                sCurrentNode = tvFolders.SelectedNode.Tag
            End If

            If oSelTree Is Nothing Then
                oSelTree = tvFolders
            End If

            oItem = CType(sender, BaseItem)

            Select Case oItem.Text.ToLower.Replace("&", "")

                Case "user constants"
                    Dim oNew As frmFormulaEditor = New frmFormulaEditor

                    oNew.GetName()
                Case "operational hours"
                    Dim frmOp As frmOperationsExplorer = New frmOperationsExplorer

                    frmOp.MdiParent = oMain
                    frmOp.ShowInTaskbar = False

                    frmOp.Show()
                Case "custom calendar"
                    If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                        Dim oCalendar As New frmCalendarExplorer
                        oCalendar.MdiParent = oMain

                        oCalendar.Show()
                    End If
                Case "folder up"
                    If tvFolders.SelectedNode Is Nothing Then Return
                    If tbNav.SelectedTabIndex <> 0 Then Return

                    Dim oNode As TreeNode = tvFolders.SelectedNode
                    Dim oParent As TreeNode = oNode.Parent

                    If Not oParent Is Nothing Then
                        tvFolders.SelectedNode = oParent
                    End If
                Case "cancel search"
                    oTextBoxItem.ControlText = String.Empty
                Case "new schedule"
                    Dim scheduleType As String = oUI.ReadRegistry("LastScheduleType", "Single")

                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Select Case scheduleType
                            Case "Single"
                                Dim oNew As frmSingleScheduleWizard = New frmSingleScheduleWizard

                                oNew.ShowDialog(oMain)
                            Case "Package"
                                Dim oNew As frmPackWizard = New frmPackWizard

                                oNew.ShowDialog(oMain)
                            Case "Automation"
                                Dim oNew As frmAutoScheduleWizard = New frmAutoScheduleWizard

                                oNew.ShowDialog(oMain)
                            Case "Event-Based"
                                Dim oNew As frmEventWizard6 = New frmEventWizard6

                                oNew.ShowDialog(oMain)

                            Case "Dynamic"
                                Dim oNew As frmDynamicSchedule = New frmDynamicSchedule

                                oNew.ShowDialog(oMain)
                            Case "Dynamic-Package"
                                Dim oNew As frmDynamicPackage = New frmDynamicPackage

                                oNew.ShowDialog(oMain)
                            Case "Event-Based Package"
                                Dim oNew As frmEventPackage = New frmEventPackage

                                oNew.ShowDialog()
                            Case "Data-Driven Schedule"
                                If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.s5_DataDrivenSched) Then
                                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
                                    Return
                                End If

                                Dim oD As frmRDScheduleWizard = New frmRDScheduleWizard

                                oD.ShowDialog()
                            Case "Data-Driven Package"
                                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.s5_DataDrivenSched) = False Then
                                    _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
                                    Return
                                End If

                                Dim oD As frmDataDrivenPackage = New frmDataDrivenPackage

                                oD.ShowDialog()
                            Case Else
                                Dim oNew As frmSingleScheduleWizard = New frmSingleScheduleWizard

                                oNew.ShowDialog(oMain)
                        End Select
                    End If
                Case "smart folder"
                    If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
                        Dim oSmart As New frmSmartFolders

                        oSmart.ShowDialog(oMain)
                    End If
                Case "new folder", "folder"
                    Dim nParent As Integer
                    Dim sType As String
                    Dim sParent As String

                    Try
                        sType = tvFolders.SelectedNode.Tag.Split(":")(0)
                    Catch
                        sType = "xfolder"
                    End Try


                    If sType.ToLower = "folder" Or sType.ToLower = "new folder" Or _
                    sType.ToLower = "desktop" Then
                        nParent = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
                        sParent = tvFolders.SelectedNode.Text
                    Else
                        Dim oFolder As New frmFolders
                        Dim sTemp() As String

                        oFolder.SelectDeskTop = True
                        sTemp = oFolder.GetFolder("Please select the parent folder")

                        If sTemp Is Nothing Then Return

                        nParent = sTemp(1)
                        sParent = sTemp(0)

                        If sParent.Length = 0 Then Return

                        sParent = sParent.Split("\")(sParent.Split("\").GetUpperBound(0))
                    End If

                    oUI.NewFolder(nParent)

                    oUI.RefreshView(Me)

                    oUI.FindNode("Folder:" & nParent, tvFolders, tvFolders.Nodes(0))

                Case "delete"
                    If clsMarsSecurity._HasGroupAccess("Delete Schedules") = False Then
                        Return
                    End If

                    Dim lShouldReturn As Boolean = True

                    DeleteDesktopItem(sender, e, lShouldReturn)

                    If lShouldReturn Then
                        oUI.RefreshView(Me)
                        Return
                    End If
                Case "options"
                    If clsMarsSecurity._HasGroupAccess("Options") = True Then
                        Dim oOptions As frmOptions = New frmOptions

                        oOptions.ShowDialog()
                    End If
                Case "system monitor"

                    If clsMarsSecurity._HasGroupAccess("System Monitor") = True Then
                        If gSysmonInstance Is Nothing Then
                            Dim oSys As frmSystemMonitor = New frmSystemMonitor

                            gSysmonInstance = oSys

                            oSys.Show()
                        Else
                            gSysmonInstance.Focus()
                        End If
                    End If
                Case "preview"

                    Dim nReportID As Integer

                    If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

                    nReportID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    Dim oThread As clsMarsThreading = New clsMarsThreading

                    oThread.xReportID = nReportID

                    oThread.PreviewReport()
                Case "help"
                    Try
                        Process.Start(sAppPath & "sqlrd.chm")
                    Catch : End Try
                Case "single report schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oSingle As frmSingleScheduleWizard = New frmSingleScheduleWizard

                        oSingle.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Single")
                    End If
                Case "packaged reports schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oPackage As frmPackWizard = New frmPackWizard

                        oPackage.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Package")
                    End If
                Case "automation schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oAuto As New frmAutoScheduleWizard

                        oAuto.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Automation")
                    End If
                Case "dynamic schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oDynamic As New frmDynamicSchedule

                        oDynamic.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Dynamic")
                    End If
                Case "event-based schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oEvent As New frmEventWizard6

                        oEvent.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Event-Based")
                    End If
                Case "dynamic package schedule"
                    Dim oDynamic As New frmDynamicPackage

                    oDynamic.ShowDialog()

                    oUI.SaveRegistry("LastScheduleType", "Dynamic-Package")
                Case "event-based package"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        Dim oPackage As New frmEventPackage

                        oPackage.ShowDialog()

                        oUI.SaveRegistry("LastScheduleType", "Event-Based Package")
                    End If
                Case "data-driven schedule"
                    If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = True Then
                        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.s5_DataDrivenSched) Then
                            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
                            Return
                        End If

                        Dim oD As frmRDScheduleWizard = New frmRDScheduleWizard

                        oD.ShowDialog()

                    End If
                Case "data-driven package"
                    If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.s5_DataDrivenSched) = False Then
                        _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
                        Return
                    End If

                    Dim oD As frmDataDrivenPackage = New frmDataDrivenPackage

                    oD.ShowDialog()
                    'new shit
                Case "data items"
                    If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)

                        Return
                    End If

                    Dim dataItems As frmDataItems = New frmDataItems

                    dataItems.ViewDataItems()
                Case "user manager"
                    If clsMarsSecurity._HasGroupAccess("User Manager") = True Then
                        Dim oForm As New frmUserManager

                        oForm.MdiParent = oMain
                        oForm.Show()
                    End If
                Case "smtp servers"
                    If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False Then
                        _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
                        Return
                    End If

                    Dim oSMTP As New frmSMTPServers

                    oSMTP.IsDialog = False

                    oSMTP.MdiParent = oMain
                    oSMTP.Show()
                Case "connect"
                    If clsMarsSecurity._HasGroupAccess("Remote Administration") = True Then
                        Dim oRemote As New frmRemoteAdmin
                        oRemote.MdiParent = oMain
                        oRemote.Show()
                    End If
                Case "disconnect"

                    Dim oRemote As New frmRemoteAdmin

                    oRemote.Disconnect()
                Case "backup system"
                    Dim oSys As New clsSystemTools

                    oSys._BackupSystem()
                Case "restore system"
                    Dim oSys As New clsSystemTools

                    oSys._RestoreSystem()
                Case "compact system"
                    Dim oSys As New clsSystemTools

                    oSys._CompactSystem()
                Case "export schedules"
                    Dim oExport As New frmExportWizard

                    oExport.ShowDialog()
            End Select

Context:

            'for the context menus
            Select Case oItem.Name.ToLower
                'single report schedules------------------------------------------
                Case "mcopy"
                    mnuReportCopy_Click(sender, e)
                Case "mpaste"
                    mnuReportPaste_Click(sender, e)
                Case "mrename"
                    mnuReportRename_Click(sender, e)
                Case "mstatus"
                    mnuReportDisable_Click(sender, e)
                Case "mpreview"
                    mnuReportSingle_Click(sender, e)
                Case "mexecute"
                    mnuSingleExecute_Click(sender, e)
                Case "mdelete"
                    mnuReportDelete_Click(sender, e)
                Case "madhoc"
                    Dim frmAdHoc As New frmAdhocSend
                    Dim nID As Integer

                    nID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

                    frmAdHoc.AdHocSend(nID)
                Case "mtopackage"
                    mnuToPackage_Click(sender, e)
                Case "mproperties"
                    mnuReportProperties_Click(sender, e)
                Case "mrefresh"
                    mnuSingleRefresh_Click(sender, e)
                Case "msingletest"
                    mnuTestSchedule_Click(sender, e)
                Case "btnsinglerr"
                    Me.mnuSingleRR_Click(sender, e)
                Case "btnsingleshortcut"
                    Me.mnuSingleShortCut_Click(sender, e)
                    'package schedules------------------------------------------
                Case "mpackageadhoc"
                    Dim frmAdHoc As New frmAdhocSend
                    Dim nID As Integer

                    nID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

                    frmAdHoc.AdHocSend(, nID)
                Case "mpackaddreport"
                    mnuPackageAdd_Click(sender, e)
                Case "mpackstatus"
                    mnuPackageDisable_Click(sender, e)
                Case "mpackrename"
                    mnuPackageRename_Click(sender, e)
                Case "mpackopen"
                    mnuPackageOpen_Click(sender, e)
                Case "mpackexecute"
                    mnuPackageExecute_Click(sender, e)
                Case "mpackdelete"
                    mnuPackageDelete_Click(sender, e)
                Case "mpackproperties"
                    mnuPackageProperties_Click(sender, e)
                Case "mpackcopy"
                    mnuPackageCopy_Click(sender, e)
                Case "mpackpaste"
                    mnuPackagePaste_Click(sender, e)
                Case "msplitpackage"
                    mnuSplitPackage_Click(sender, e)
                Case "mpackagerefresh"
                    mnuPackageRefresh_Click(sender, e)
                Case "mpackagetest"
                    mnuTestPackage_Click(sender, e)
                Case "btnpackagerr"
                    Me.mnuPackageRR_Click(sender, e)
                Case "btnpackageshortcut"
                    Me.mnuPackageShortCut_Click(sender, e)
                Case "mfolderopen"
                    'folders------------------------------------------
                    mnuFolderOpen_Click(sender, e)
                Case "mfoldermove"
                    mnuFolderMove_Click(sender, e)
                Case "mfolderrename"
                    mnuFolderRename_Click(sender, e)
                Case "mfolderdelete"
                    mnuFolderDelete_Click(sender, e)
                Case "malldisable"
                    Me.mnuDisableAll_Click(sender, e)
                Case "mallenable"
                    Me.mnuEnableAll_Click(sender, e)
                Case "mallexecute"
                    Me.mnuExecuteAll_Click(sender, e)
                Case "mallrefresh"
                    Me.mnuRefreshAll_Click(sender, e)
                Case "mpackedadd"
                    'packaged reports------------------------------------------
                    MenuItem1_Click(sender, e)
                Case "mpackedrename"
                    MenuItem5_Click(sender, e)
                Case "mpackeddelete"
                    MenuItem9_Click(sender, e)
                Case "mpackedproperties"
                    MenuItem13_Click(sender, e)
                Case "mpackedrefresh"
                    mnuSingleRefresh_Click(sender, e)
                Case "mpackedexecute"
                    MenuItem24_Click(sender, e)
                Case "mpackedstatus"
                    mnuPackedStatus_Click(sender, e)
                Case "mautocopy"
                    'automation schedules------------------------------------------
                    mnuAutoCopy_Click(sender, e)
                Case "mautopaste"
                    mnuAutoPaste_Click(sender, e)
                Case "mautorename"
                    mnuAutoRename_Click(sender, e)
                Case "mautostatus"
                    mnuAutoStatus_Click(sender, e)
                Case "mautoexecute"
                    mnuAutoExecute_Click(sender, e)
                Case "mautodelete"
                    mnuAutoDelete_Click(sender, e)
                Case "mautoproperties"
                    mnuAutoProperties_Click(sender, e)
                Case "btnautoshortcut"
                    Me.mnuAutoShortcut_Click(sender, e)
                Case "msmartrename"
                    'smart folders------------------------------------------
                    oSelTree.SelectedNode.BeginEdit()
                Case "msmartdelete"
                    If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
                        Dim nID As Integer = oSelTree.SelectedNode.Tag.split(":")(1)

                        Dim oRes As DialogResult = MessageBox.Show("Delete the '" & oSelTree.SelectedNode.Text & "' smart folder?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then

                            oUI.DropSmartFolder(nID)

                            oUI.RefreshView(Me)
                        End If
                    End If
                Case "msmartproperties"
                    If clsMarsSecurity._HasGroupAccess("Smart Folder Mngmnt") = True Then
                        Dim oForm As New frmSmartProp

                        Dim nID As Integer = oSelTree.SelectedNode.Tag.split(":")(1)

                        oForm.EditSmartFolder(nID)
                    End If
                Case "msmartprint"
                    Try
                        Dim nID As Integer

                        nID = oSelTree.SelectedNode.Tag.Split(":")(1)

                        Dim SmartSQL() As String

                        SmartSQL = oUI.SmartQueryBuilder(nID)

                        Dim oSmart As New frmSmartReport

                        oSmart._RenderReport(SmartSQL, oSelTree.SelectedNode.Text)
                    Catch : End Try

                Case "meventcopy"
                    'event based schedules------------------------------------------
                    mnuEventCopy_Click(sender, e)
                Case "meventpaste"
                    mnuEventPaste_Click(sender, e)
                Case "meventrename"
                    mnuEventRename_Click(sender, e)
                Case "meventstatus"
                    mnuEventStatus_Click(sender, e)
                Case "meventdelete"
                    mnuEventDelete_Click(sender, e)
                Case "meventproperties"
                    mnuEventProperties_Click(sender, e)
                Case "meventexecute"
                    Me.mnuEventTest_Click(sender, e)
                Case "meventrecreate"
                    Me.mnuRecreateDB_Click(sender, e)
                Case "btneventshortcut"
                    Me.mnuEventShortcut_Click(sender, e)
                    'empty desktop--------------------------------------------------
                Case "mdesktoppaste"
                    Select Case gClipboard(0)
                        Case "Report"
                            mnuReportPaste_Click(sender, e)
                        Case "Automation"
                            mnuAutoPaste_Click(sender, e)
                        Case "Event"
                            mnuEventPaste_Click(sender, e)
                        Case "Package"
                            mnuPackagePaste_Click(sender, e)
                        Case "Event Package"
                            Me.mnuEPPaste_Click(sender, e)
                    End Select
                Case "mdesktoprefresh"
                    oUI.RefreshView(Me)
                    'treeview folders-----------------------------------------------
                Case "mtreeopen"
                    nWindowCount += 1
                    Dim sParent As String = tvFolders.SelectedNode.Tag
                    Dim oForm As frmWindow

                    ReDim Preserve oWindow(nWindowCount)

                    oWindow(nWindowCount) = New frmWindow

                    oForm = oWindow(nWindowCount)
                    nWindowCurrent = nWindowCount
                    oForm.MdiParent = oMain
                    oForm.Show()
                    oForm.Tag = nWindowCount

                    oForm.Text = tvFolders.SelectedNode.Text

                    oUI.FindNode(sParent, oForm.tvFolders, oForm.tvFolders.Nodes(0))
                Case "mtreemove"
                    Dim nFolderID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)
                    oUI.MoveFolder(nFolderID)
                    oUI.RefreshView(Me)
                Case "mtreerename"
                    tvFolders.LabelEdit = True
                    tvFolders.SelectedNode.BeginEdit()
                Case "mtreedelete"
                    oResponse = MessageBox.Show("Delete the '" & tvFolders.SelectedNode.Text & "' folder? All contents will be lost.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                    If oResponse = DialogResult.Yes Then
                        Dim oParent As TreeNode = tvFolders.SelectedNode.Parent
                        oUI.DeleteFolder(tvFolders.SelectedNode.Tag.Split(":")(1))

                        oUI.RefreshView(Me)

                        Try
                            tvFolders.SelectedNode = tvFolders.Nodes(oParent.Index)
                        Catch
                            tvFolders.SelectedNode = oParent
                        End Try
                    End If
                Case "malldisable1"
                    If tvFolders.SelectedNode Is Nothing Then Return

                    Dim oFolder As New clsFolders
                    Dim nID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)

                    oFolder.DisableFolder(nID)

                    oUI.RefreshView(Me)

                    MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
                Case "mallenable1"
                    If tvFolders.SelectedNode Is Nothing Then Return

                    Dim oFolder As New clsFolders
                    Dim nID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)

                    oFolder.EnableFolder(nID)

                    oUI.RefreshView(Me)

                    MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
                Case "mallrefresh1"
                    If tvFolders.SelectedNode Is Nothing Then Return

                    Dim oFolder As New clsFolders
                    Dim nID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)

                    oFolder.RefreshFolder(nID)

                    MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
                Case "mallexecute1"
                    If tvFolders.SelectedNode Is Nothing Then Return

                    Dim oFolder As New clsFolders
                    Dim nID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)

                    oFolder.ExecuteFolder(nID)

                    MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Information)
                    'smart folder all contents-----------------------------------------------------------------
                Case "msmartrefresh"
                    Dim sType As String
                    Dim nID As Integer
                    Dim oReport As New clsMarsReport

                    For Each o As ListViewItem In lsvWindow.Items
                        sType = o.Tag.Split(":")(0)
                        nID = o.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                oReport.RefreshReport(nID)
                            Case "package"
                                oReport.RefreshPackage(nID)
                        End Select
                    Next

                    MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case "msmartdisable"
                    Dim sType As String
                    Dim nID As Integer
                    Dim oReport As New clsMarsReport
                    Dim sTemp As String

                    sTemp = tvSmartFolders.SelectedNode.Tag

                    For Each o As ListViewItem In lsvWindow.Items
                        sType = o.Tag.Split(":")(0)
                        nID = o.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 0, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE ReportID = " & nID
                            Case "package"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 0, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE PackID = " & nID
                            Case "automation"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 0, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE AutoID = " & nID
                            Case "event"
                                SQL = "UPDATE EventAttr6 SET " & _
                                "Status = 0, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE EventID = " & nID
                            Case "event-package"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                 "Status = 0, " & _
                                 "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                 "WHERE EventPackID = " & nID
                        End Select

                        clsMarsData.WriteData(SQL)
                    Next

                    'oUI._RefreshView(Me)
                    tvSmartFolders.SelectedNode = Nothing
                    oUI.FindNode(sTemp, tvSmartFolders)

                    MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case "msmartenable"
                    Dim sType As String
                    Dim nID As Integer
                    Dim oReport As New clsMarsReport
                    Dim sTemp As String

                    sTemp = tvSmartFolders.SelectedNode.Tag

                    For Each o As ListViewItem In lsvWindow.Items
                        sType = o.Tag.Split(":")(0)
                        nID = o.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 1, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE ReportID = " & nID
                            Case "package"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 1, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE PackID = " & nID
                            Case "automation"
                                SQL = "UPDATE ScheduleAttr SET " & _
                                "Status = 1, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE AutoID = " & nID
                            Case "event"
                                SQL = "UPDATE EventAttr SET " & _
                                "Status = 1, " & _
                                "DisabledDate = '" & ConDateTime(Date.Now) & "' " & _
                                "WHERE EventID = " & nID
                        End Select

                        clsMarsData.WriteData(SQL)
                    Next

                    'oUI._RefreshView(Me)
                    tvSmartFolders.SelectedNode = Nothing

                    oUI.FindNode(sTemp, tvSmartFolders)

                    MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case "msmartexecute"
                    Dim sType As String
                    Dim nID As Integer
                    Dim oReport As New clsMarsReport
                    Dim oAuto As New clsMarsAutoSchedule
                    Dim oEvent As New clsMarsEvent
                    Dim oSchedule As New clsMarsScheduler

                    For Each o As ListViewItem In lsvWindow.Items
                        sType = o.Tag.Split(":")(0)
                        nID = o.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                Dim oRs As ADODB.Recordset

                                oRs = clsMarsData.GetData("SELECT Dynamic FROM ReportAttr WHERE ReportID =" & nID)

                                Try
                                    If oRs.EOF = False Then
                                        If IsNull(oRs.Fields(0).Value) = "1" Then
                                            If oReport.RunDynamicSchedule(nID) = True Then
                                                oSchedule.SetScheduleHistory(True, , nID)
                                            Else
                                                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                                            End If
                                        Else
                                            If oReport.RunSingleSchedule(nID) = True Then
                                                oSchedule.SetScheduleHistory(True, , nID)
                                            Else
                                                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                                            End If
                                        End If
                                    End If
                                Catch
                                    If oReport.RunSingleSchedule(nID) = True Then
                                        oSchedule.SetScheduleHistory(True, , nID)
                                    Else
                                        oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nID)
                                    End If
                                End Try
                            Case "package"

                                If clsMarsData.IsScheduleDynamic(nID, "Package") = True Then
                                    If oReport.RunDynamicPackageSchedule(nID, False) = True Then
                                        oSchedule.SetScheduleHistory(True, "", , nID, , , , oReport.m_HistoryID)
                                    Else
                                        oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , nID, , , , oReport.m_HistoryID)
                                    End If
                                Else
                                    If oReport.RunPackageSchedule(nID) = True Then
                                        oSchedule.SetScheduleHistory(True, , , nID, , , , oReport.m_HistoryID)
                                    Else
                                        oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", , nID, , , , oReport.m_HistoryID)
                                    End If
                                End If

                            Case "automation"
                                oAuto.ExecuteAutomationSchedule(nID)
                            Case "event"
                                oEvent.RunEvents(nID)

                        End Select
                    Next

                    MessageBox.Show("The requested operation completed successfully", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                Case "msystemview"
                    Me.mnuViewSystem_Click(sender, e)
                    '------------------event based packages
                Case "mepproperties"
                    mnuEPProperties_Click(sender, e)
                Case "mepexecute"
                    Me.mnuEPExecute_Click(sender, e)
                Case "mepstatus"
                    Me.mnuEPStatus_Click(sender, e)
                Case "meprename"
                    Me.mnuEPRename_Click(sender, e)
                Case "mepcopy"
                    Me.mnuEPCopy_Click(sender, e)
                Case "meppaste"
                    Me.mnuEPPaste_Click(sender, e)
                Case "mepaddnew"
                    Me.mnuEPNew_Click(sender, e)
                Case "mepaddexisting"
                    Me.mnuEPExisting_Click(sender, e)
                Case "mepsplit"
                    Me.mnuEPSplit_Click(sender, e)
                Case "mepdelete"
                    Me.mnuEPDelete_Click(sender, e)
                Case "mepopen"
                    Me.mnuEPOpen_Click(sender, e)
                Case "btneventpackageshortcut"
                    Me.mnuEventPackageShortcut_Click(sender, e)
                    '---------------------------------multiple selections
                Case "mltmove"
                    If lsvWindow.SelectedItems.Count = 0 Then Return

                    Dim oFolders As frmFolders = New frmFolders
                    Dim tempType As String = lsvWindow.SelectedItems(0).Tag.ToString.Split(":")(0)
                    Dim parent As Integer = 0

                    If tempType = "Packed Report" Then
                        oFolders.m_packageOnly = True

                        parent = oFolders.GetPackage()

                        If parent = 0 Then Return
                    Else
                        Dim resultVal() As String = oFolders.GetFolder("Please select the folder to move to")

                        If resultVal(0) = "" Then Return

                        parent = resultVal(1)
                    End If

                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim sType As String = it.Tag.split(":")(0)
                        Dim nID As Integer = it.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                SQL = "UPDATE ReportAttr SET Parent =" & parent & " WHERE ReportID =" & nID
                            Case "package"
                                SQL = "UPDATE PackageAttr SET Parent =" & parent & " WHERE PackID =" & nID
                            Case "automation"
                                SQL = "UPDATE AutomationAttr SET Parent =" & parent & " WHERE AutoID =" & nID
                            Case "event"
                                SQL = "UPDATE EventAttr6 SET Parent =" & parent & " WHERE EventID =" & nID
                            Case "event-package"
                                SQL = "UPDATE EventPackageAttr SET Parent =" & parent & " WHERE EventPackID =" & nID
                            Case "folder"
                                If nID = parent Then
                                    MessageBox.Show("A folder cannot be moved into itself", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Continue For
                                ElseIf clsMarsUI.MainUI.IsMoveValid(nID, parent) = False Then
                                    MessageBox.Show("Cannot move the folder as the destination is a subfolder of the source folder", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Continue For
                                End If

                                SQL = "UPDATE Folders SET Parent = " & parent & " WHERE FolderID = " & nID
                            Case "packed report"
                                Dim newID As Integer
                                Dim newName As String
                                Dim oldName As String = it.Text
                                Dim oRs As ADODB.Recordset

                                SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & parent

                                oRs = clsMarsData.GetData(SQL)

                                If oRs IsNot Nothing Then
                                    newID += 1
                                Else
                                    newID = 1
                                End If

                                oRs.Close()

                                newName = newID & ":" & oldName.Split(":")(1)

                                SQL = "UPDATE ReportAttr SET " & _
                                "PackID =" & parent & "," & _
                                "ReportTitle ='" & SQLPrepare(newName) & "'," & _
                                "PackOrderID =" & newID & " WHERE ReportID =" & nID
                            Case Else
                                Continue For
                        End Select

                        clsMarsData.WriteData(SQL)
                    Next

                    clsMarsUI.MainUI.RefreshView(Me)
                Case "mltrefresh"
                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim sType As String = it.Tag.split(":")(0)
                        Dim nID As Integer = it.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                clsMarsReport.oReport.RefreshReport(nID)
                            Case "packed report"
                                clsMarsReport.oReport.RefreshReport(nID)
                            Case "package"
                                clsMarsReport.oReport.RefreshPackage(nID)
                        End Select
                    Next

                    MessageBox.Show("The request has completed", Application.ProductName, MessageBoxButtons.OK)
                Case "mltexecute"
                    Dim limitMsg As String = "Your current SQL-RD edition only allows for the execution of " & nMax & " schedules at a time."

                    If IsFeatEnabled(gEdition.NULL, modFeatCodes.g1_Unlimit_Sched) = False Then
                        Select Case gnEdition
                            Case gEdition.BRONZE
                                If lsvWindow.SelectedItems.Count > nMax Then
                                    MessageBox.Show(limitMsg, _
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return
                                End If
                            Case gEdition.SILVER
                                If lsvWindow.SelectedItems.Count > nMax Then
                                    MessageBox.Show(limitMsg, _
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return
                                End If
                            Case gEdition.GOLD
                                If lsvWindow.SelectedItems.Count > nMax Then
                                    MessageBox.Show(limitMsg, _
                                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                    Return
                                End If
                        End Select
                    End If

                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim sType As String = it.Tag.split(":")(0)
                        Dim nID As Integer = it.Tag.Split(":")(1)

                        Select Case sType.ToLower
                            Case "report"
                                Dim oT As New clsMarsThreading

                                oT.xReportID = nID

                                If clsMarsData.IsScheduleBursting(nID) = True Then
                                    oT.BurstingScheduleThread()
                                ElseIf clsMarsData.IsScheduleDynamic(nID, "report") = True Then
                                    oT.DynamicScheduleThread()
                                Else
                                    oT.SingleScheduleThread()
                                End If
                            Case "packed report"
                                Dim oT As New clsMarsThreading
                                Dim nPackID As Integer = tvFolders.SelectedNode.Tag.Split(":")(1)

                                oT.xPackID = nPackID
                                oT.xReportID = nID

                                oT.PackageScheduleThread()
                            Case "package"
                                Dim oT As New clsMarsThreading
                                oT.xPackID = nID
                                oT.PackageScheduleThread()
                            Case "automation"
                                Dim oT As New clsMarsThreading
                                oT.xAutoID = nID
                                oT.AutoScheduleThread()
                            Case "event"
                                Dim oT As New clsMarsThreading
                                oT.xEventID = nID
                                oT.EventScheduleThread()
                            Case "event-package"
                                Dim oT As New clsMarsThreading
                                oT.xPackID = nID
                                oT.EventPackageScheduleThread()
                        End Select

                        _Delay(1.5)
                    Next

                Case "mltproperties"
                    '<schedulesTable>
                    '<ID>Integer</ID>
                    '<ScheduleType>String</ScheduleType>
                    '<ScheduleID>Integer</ScheduleID>
                    '</schedulesTable>
                    Dim schedulesTable As DataTable = New DataTable

                    With schedulesTable
                        .Columns.Add("ID")
                        .Columns.Add("scheduleType")
                        .Columns.Add("scheduleID")
                    End With

                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim sType As String = it.Tag.Split(":")(0)
                        Dim nID As Integer = it.Tag.Split(":")(1)

                        Dim row As DataRow = schedulesTable.Rows.Add

                        row("ID") = nID
                        row("scheduleType") = sType
                        row("scheduleID") = ""
                    Next

                    Dim mltProp As frmCollectiveProp = New frmCollectiveProp

                    mltProp.editSchedules(schedulesTable)
                Case "mltconverttopackage"
                    Dim schedules As Hashtable = New Hashtable

                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim sType As String = it.Tag.Split(":")(0)
                        Dim nID As Integer = it.Tag.Split(":")(1)

                        If sType.ToLower = "report" Then
                            schedules.Add(it.Text, nID)
                        End If
                    Next

                    Dim selektor As frmItemSelector = New frmItemSelector

                    Dim templateID As Integer = selektor.chooseTemplate(schedules)


                    If templateID > 0 Then
                        If clsMarsReport.convertReportstoPackage(schedules, templateID) = True Then
                            clsMarsUI.MainUI.RefreshView(Me)
                        End If
                    End If
                Case "mltmovetopackage"
                    'first check if all the items are reports or event-based schedules
                    Dim itemType As String = ""

                    For Each it As ListViewItem In lsvWindow.SelectedItems
                        Dim type As String = it.Tag.ToString.Split(":")(0)

                        If itemType = "" Then
                            itemType = type
                        ElseIf itemType <> type Then
                            MessageBox.Show("Please select either all Single Report Schedules or all Event-Based Schedules. You cannot mix and match okay?", _
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return
                        End If
                    Next

                    If itemType.ToLower = "report" Then
                        Dim oFolder As frmFolders = New frmFolders
                        oFolder.m_packageOnly = True
                        Dim result As String() = oFolder.GetFolder("Please select the package to move the schedules into")

                        If result(0) = "" Then Return

                        Dim packID = result(1)
                        Dim reportIDs() As Integer
                        Dim I As Integer = 0

                        For Each it As ListViewItem In lsvWindow.SelectedItems
                            Dim type As String = it.Tag.ToString.Split(":")(0)
                            Dim nID As Integer = it.Tag.ToString.Split(":")(1)

                            If type.ToLower = "report" Then
                                ReDim Preserve reportIDs(I)

                                reportIDs(I) = nID

                                I += 1
                            End If
                        Next

                        clsMarsReport.moveintoPackage(packID, reportIDs)

                        clsMarsUI.MainUI.FindNode("Package:" & packID, tvFolders, tvFolders.Nodes(0))
                    ElseIf itemType.ToLower = "event" Then
                        Dim oFolder As frmFolders = New frmFolders
                        oFolder.m_eventPackage = True

                        Dim result As String() = oFolder.GetFolder("Please select the Event-Based Package to move the schedules into")

                        If result(0) = "" Then Return

                        Dim packID = result(1)
                        Dim eventIDs() As Integer
                        Dim I As Integer = 0

                        For Each it As ListViewItem In lsvWindow.SelectedItems
                            Dim type As String = it.Tag.ToString.Split(":")(0)
                            Dim nID As Integer = it.Tag.ToString.Split(":")(1)

                            If type.ToLower = "event" Then
                                ReDim Preserve eventIDs(I)

                                eventIDs(I) = nID

                                I += 1
                            End If
                        Next

                        clsMarsEvent.moveintoEventPackage(packID, eventIDs)

                        clsMarsUI.MainUI.FindNode("Event-Package:" & packID, tvFolders, tvFolders.Nodes(0))
                    End If
            End Select
        Catch ex As Exception
            ''console.writeLine(ex.Message)
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            '"Please try carrying out the action again")
        End Try

        oUI.BusyProgress(, , True)
    End Sub


    Private Sub DotNetBarManager1_PopupOpen(ByVal sender As System.Object, _
    ByVal e As DevComponents.DotNetBar.PopupOpenEventArgs) Handles DotNetBarManager1.PopupOpen
        Dim oItem As BaseItem
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nID As Integer
        Dim oStatus As ButtonItem

        Try
            If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

            oItem = CType(sender, BaseItem)

            Select Case oItem.Name.ToLower
                Case "msinglereport"

                    nID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    SQL = "SELECT Status FROM ScheduleAttr WHERE ReportID = " & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    Try
                        oStatus = DotNetBarManager1.GetItem("mStatus", True)

                        If Not oRs Is Nothing Then
                            If oRs("status").Value = 0 Then
                                oStatus.Checked = False
                            Else
                                oStatus.Checked = True
                            End If

                            oRs.Close()
                        End If

                        oRs = clsMarsData.GetData("SELECT Dynamic,Bursting FROM ReportAttr WHERE ReportID = " & nID)

                        oStatus = DotNetBarManager1.GetItem("item_229", True)

                        If IsNull(oRs.Fields(0).Value, 0) = 1 Or IsNull(oRs.Fields(1).Value, 0) = 1 Then
                            oStatus.Visible = False
                        Else
                            oStatus.Visible = True
                        End If

                        oRs.Close()

                        oRs = clsMarsData.GetData("SELECT * FROM DestinationAttr WHERE ReportID =" & nID & " AND ReadReceipts = 1")

                        If oRs IsNot Nothing Then
                            Dim btnRR As ButtonItem

                            btnRR = DotNetBarManager1.GetItem("btnSingleRR", True)

                            If oRs.EOF = False Then
                                btnRR.Visible = True
                            Else
                                btnRR.Visible = False
                            End If

                            oRs.Close()
                        End If
                    Catch : End Try

                Case "mpackageschedule"
                    oStatus = DotNetBarManager1.GetItem("mPackStatus", True)

                    nID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    SQL = "SELECT Status FROM ScheduleAttr WHERE PackID = " & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    Try
                        If Not oRs Is Nothing And oRs.EOF = False Then
                            If oRs("status").Value = 0 Then
                                oStatus.Checked = False
                            Else
                                oStatus.Checked = True
                            End If
                        End If

                        oRs.Close()

                        oRs = clsMarsData.GetData("SELECT * FROM DestinationAttr WHERE PackID =" & nID & " AND ReadReceipts = 1")

                        If oRs IsNot Nothing Then
                            Dim btnRR As ButtonItem

                            btnRR = DotNetBarManager1.GetItem("btnPackageRR", True)

                            If oRs.EOF = False Then
                                btnRR.Visible = True
                            Else
                                btnRR.Visible = False
                            End If

                            oRs.Close()
                        End If
                    Catch : End Try
                Case "mautomationschedule"
                    oStatus = DotNetBarManager1.GetItem("mAutoStatus", True)

                    nID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    SQL = "SELECT Status FROM ScheduleAttr WHERE AutoID = " & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then
                            If oRs("status").Value = 0 Then
                                oStatus.Checked = False
                            Else
                                oStatus.Checked = True
                            End If
                        End If

                        oRs.Close()
                    End If


                Case "meventschedules"

                    oStatus = DotNetBarManager1.GetItem("mEventStatus", True)

                    nID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    SQL = "SELECT Status FROM EventAttr6 WHERE EventID = " & nID

                    Dim ors As ADODB.Recordset = clsMarsData.GetData(SQL)

                    Try
                        If ors.EOF = False Then
                            If ors("status").Value = 0 Then
                                oStatus.Checked = False
                            Else
                                oStatus.Checked = True
                            End If
                        End If
                    Catch ex As Exception
                    End Try
                Case "mpackedreport"
                    oStatus = DotNetBarManager1.GetItem("mPackedStatus", True)

                    nID = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

                    SQL = "SELECT Status FROM PackagedReportAttr WHERE ReportID = " & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    Try
                        If Not oRs Is Nothing Then
                            If oRs.EOF = False Then
                                If oRs("status").Value = 0 Then
                                    oStatus.Checked = False
                                Else
                                    oStatus.Checked = True
                                End If
                            End If

                            oRs.Close()
                        End If
                    Catch : End Try
                Case "meventpackage"
                    oStatus = DotNetBarManager1.GetItem("mEPStatus", True)

                    nID = Convert.ToString(lsvWindow.SelectedItems(0).Tag).Split(":")(1)

                    SQL = "SELECT Status FROM ScheduleAttr WHERE EventPackID=" & nID

                    Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                    Try
                        If oRs IsNot Nothing Then
                            If oRs.EOF = False Then
                                If oRs("status").Value = 0 Then
                                    oStatus.Checked = False
                                Else
                                    oStatus.Checked = True
                                End If
                            End If
                            oRs.Close()
                        End If
                    Catch : End Try
            End Select
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub mnuAutoDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoDelete.Click
        'Dim nID As Integer

        'If lsvWindow.SelectedItems.Count = 0 Then Return

        'For Each oI As ListViewItem In lsvWindow.SelectedItems
        '    Dim oResponse As DialogResult
        '    If oMulti = True Then
        '        oResponse = oMultiRes
        '    Else
        '        oResponse = MessageBox.Show("Delete the '" & oI.Text & "' automation schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
        '    End If

        '    If oResponse = DialogResult.Yes Then

        '        nID = GetDelimitedWord(oI.Tag, 2, ":")

        '        oUI.DeleteAutoSchedule(nID)

        '        oI.Remove()
        '    End If
        'Next

        'Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        'oItems.Text = lsvWindow.Items.Count & " items"

        DeleteDesktopItem(sender, e, New Boolean = False)

    End Sub

    Private Sub mnuAutoStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoStatus.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem

        olsv = lsvWindow.SelectedItems(0)

        oItem = DotNetBarManager1.GetItem("mAutoStatus", True)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1
                olsv.ImageIndex = 5

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0
                olsv.ImageIndex = 6

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE AutoID =" & nID

        clsMarsData.WriteData(SQL)

        If nValue = 1 Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.ENABLE)
        Else
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.DISABLE)
        End If

        clsMarsUI.MainUI.RefreshView(Me)

    End Sub


    Private Sub mnuAutoExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoExecute.Click
        Dim oThread As New clsMarsThreading

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        oThread.xAutoID = nID

        AppStatus(True)

        oThread.AutoScheduleThread()

        AppStatus(False)

        clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EXECUTE)

    End Sub

    Private Sub mnuAutoRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoRename.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuAutoCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoCopy.Click
        If lsvWindow.SelectedItems.Count > 0 Then
            gClipboard(0) = "Automation"
            gClipboard(1) = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
            mnuAutoPaste.Enabled = True
            Dim oItem As ButtonItem

            oItem = DotNetBarManager1.GetItem("mAutoPaste", True)

            oItem.Enabled = True

            oItem = DotNetBarManager1.GetItem("mDesktopPaste", True)

            oItem.Enabled = True
        End If
    End Sub

    Private Sub mnuAutoPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoPaste.Click
        Dim nCopy As Integer
        Dim oAuto As clsMarsAutoSchedule = New clsMarsAutoSchedule
        Dim nParent As Integer
        Dim sType As String

        sType = tvFolders.SelectedNode.Tag.Split(":")(0)

        oUI.BusyProgress(10, "Loading schedule...")

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        End If

        nCopy = oAuto.CopyAuto(gClipboard(1), nParent)

    End Sub

    Private Sub mnuAutoProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoProperties.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oProp As New frmAutoProp

        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

        oProp.EditSchedule(nID)

    End Sub

    Private Sub tvFolders_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles tvFolders.AfterLabelEdit
        Dim sType As String
        Dim nID As Integer
        Dim oData As New clsMarsData
        Dim sOld As String
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sTable As String = String.Empty
        Dim sCol As String
        Dim sKey As String

        sOld = e.Node.Text

        sType = e.Node.Tag.split(":")(0).tolower
        nID = e.Node.Tag.split(":")(1)

        If e.Label Is Nothing Then e.CancelEdit = True : Return

        If e.Label.Length = 0 Then e.CancelEdit = True : Return



        Select Case sType
            Case "folder"
                SQL = "SELECT * FROM Folders WHERE " & _
                "FolderName LIKE '" & SQLPrepare(e.Label) & "'"
                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        _ErrorHandle("A folder with that name already exists", _
                        -91, "frmWindow.tvFolders_AfterLabelEdit", 1979)
                        e.CancelEdit = True
                        oRs.Close()
                        Return
                    End If

                    oRs.Close()
                End If


                sTable = "Folders"
                sCol = "FolderName"
                sKey = "FolderID"
            Case "package"
                SQL = "SELECT * FROM PackageAttr WHERE " & _
                "PackageName LIKE '" & SQLPrepare(e.Label) & "'"

                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        _ErrorHandle("A package with that name already exists", _
                        -91, "frmWindow.tvFolders_AfterLabelEdit", 1999)
                        e.CancelEdit = True
                        oRs.Close()
                        Return
                    End If
                End If

                oRs.Close()

                sTable = "PackageAttr"
                sCol = "PackageName"
                sKey = "PackID"
            Case "smartfolder"

                If clsMarsData.IsDuplicate("smartfolders", "smartname", e.Label, False) = True Then
                    _ErrorHandle("A smart folder with that name already exists", _
                    -91, "frmWindow.tvFolders_AfterLabelEdit", 2019)
                    e.CancelEdit = True

                    Return
                End If



                sTable = "smartfolders"
                sCol = "SmartName"
                sKey = "SmartID"
        End Select

        If sTable.Length > 0 Then
            SQL = "UPDATE " & sTable & " SET " & _
            sCol & " = '" & SQLPrepare(e.Label) & "' WHERE " & _
            sKey & " = " & nID

            clsMarsData.WriteData(SQL)
        End If

    End Sub



    Private Sub tvFolders_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvFolders.MouseUp
        'If e.Button <> MouseButtons.Right Then Return

        'oSelTree = tvFolders

        'If oSelTree.SelectedNode Is Nothing Then Return

        'Dim oUI As clsMarsUI = New clsMarsUI

        'Select Case oSelTree.SelectedNode.Tag.split(":")(0).ToLower
        '    Case "smartfolder"
        '        oUI._PopUpMenu(DotNetBarManager1, "mSmartFolders")
        '    Case "folder"
        '        If oSelTree.SelectedNode.Text.ToLower <> "desktop" Then oUI._PopUpMenu(DotNetBarManager1, "mTreeFolder")
        'End Select
    End Sub

    Private Sub lsvWindow_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnWidthChangedEventArgs) Handles lsvWindow.ColumnWidthChanged
        If IsLoaded = True Then
            SaveColumnData()
        End If
    End Sub

    Private Sub lsvWindow_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvWindow.SelectedIndexChanged
        Dim sItem As String
        Dim oBtn As ButtonItem = DotNetBarManager1.GetItem("item_170")
        Dim oTask As ButtonItem = oMain.DotNetBarManager1.GetItem("item_113")
        Dim oDesign As ButtonItem = oMain.DotNetBarManager1.GetItem("item_377", True)

        Dim oButton As ButtonItem

        oButton = DotNetBarManager1.GetItem("item_171", True)

        oButton.Enabled = True

        If lsvWindow.SelectedItems.Count = 0 Then
            oMain.btnDesign.Enabled = False
            oMain.btnProperties.Enabled = False
            oTask.Enabled = False
            oDesign.Enabled = False
            Return
        End If

        sItem = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":").ToLower

        Select Case sItem
            Case "report", "package", "packed report", "automation", "event", "event package"
                oTask.Enabled = True

                If sItem = "report" Or sItem = "packed report" Then
                    oBtn.Enabled = True
                    oMain.dock.btnDesign.Enabled = True
                    oMain.dock.btnProperties.Enabled = True
                    oTask.Enabled = True
                    oDesign.Enabled = True
                ElseIf sItem = "package" Then
                    oMain.btnProperties.Enabled = True
                    oMain.dock.btnDesign.Enabled = False
                    oTask.Enabled = True
                    oDesign.Enabled = False
                Else
                    oBtn.Enabled = False
                    oMain.dock.btnDesign.Enabled = False
                    oMain.dock.btnProperties.Enabled = True
                    oTask.Enabled = True
                    oDesign.Enabled = False
                End If
            Case Else
                oTask.Enabled = False
                oBtn.Enabled = False
                oMain.dock.btnDesign.Enabled = False
                oMain.dock.btnProperties.Enabled = False
                oTask.Enabled = False
                oDesign.Enabled = False
        End Select

        Dim oLabel As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        If lsvWindow.SelectedItems.Count > 0 Then
            oLabel.Text = lsvWindow.SelectedItems.Count & " items selected"
        Else
            oLabel.Text = lsvWindow.Items.Count & " items"
        End If
    End Sub

    Private Sub lsvWindow_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) _
    Handles lsvWindow.MouseUp
        Dim oUI As clsMarsUI = New clsMarsUI

        If e.Button <> MouseButtons.Right Then Return

        If lsvWindow.SelectedItems.Count = 0 Then
            oUI.PopUpMenu(DotNetBarManager1, "mDeskTop")
        ElseIf lsvWindow.SelectedItems.Count > 1 Then
            Dim btn As DevComponents.DotNetBar.ButtonItem = DotNetBarManager1.GetItem("mltConverttoPackage", True)
            Dim allSingles As Boolean = True

            For Each it As ListViewItem In lsvWindow.SelectedItems
                Dim sType = it.Tag.Split(":")(0)
                Dim nID As Integer = it.Tag.Split(":")(1)

                If sType.ToString.ToLower = "report" Then
                    If clsMarsData.IsScheduleDynamic(nID, "report") Then
                        allSingles = False
                    ElseIf clsMarsData.IsScheduleBursting(nID) Then
                        allSingles = False
                    ElseIf clsMarsData.IsScheduleDataDriven(nID) Then
                        allSingles = False
                    End If
                Else
                    allSingles = False
                End If
            Next

            btn.Enabled = allSingles

            oUI.PopUpMenu(DotNetBarManager1, "mMultipleItems")
        Else
            Select Case GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":").ToLower
                Case "report"
                    oUI.PopUpMenu(DotNetBarManager1, "mSingleReport")
                Case "package"
                    oUI.PopUpMenu(DotNetBarManager1, "mPackageSchedule")
                Case "folder"
                    oUI.PopUpMenu(DotNetBarManager1, "mFolder")
                Case "packed report"
                    oUI.PopUpMenu(DotNetBarManager1, "mPackedReport")
                Case "automation"
                    oUI.PopUpMenu(DotNetBarManager1, "mAutomationSchedule")
                Case "event"
                    oUI.PopUpMenu(DotNetBarManager1, "mEventSchedules")
                Case "event-package"
                    oUI.PopUpMenu(DotNetBarManager1, "mEventPackage")
            End Select
        End If


    End Sub

    Private Sub lsvWindow_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles lsvWindow.DoubleClick

        'On Error Resume Next
        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        Dim sType As String = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":")

        Dim oForm As frmWindow
        Dim nFolderId As Int32 = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim sParent As String = lsvWindow.SelectedItems(0).Tag
        Dim oNode As TreeNode

        Select Case sType.ToLower
            Case "folder"
                nWindowCount += 1

                ReDim Preserve oWindow(nWindowCount)

                oWindow(nWindowCount) = New frmWindow

                oForm = oWindow(nWindowCount)
                nWindowCurrent = nWindowCount
                oForm.MdiParent = oMain
                oForm.lsvWindow.Items.Clear()
                oForm.Show()
                oForm.Tag = nWindowCount

                oUI.FindNode(sParent, oForm.tvFolders)

                oForm.Text = oForm.tvFolders.SelectedNode.FullPath

                nWindowCurrent = nWindowCount
            Case "report"
                Dim oProp As New frmSingleProp

                oProp.EditSchedule(nFolderId)
            Case "event"
                Dim oProp As New frmEventProp

                oProp.EditSchedule(nFolderId)
            Case "automation"
                Dim oProp As New frmAutoProp

                oProp.EditSchedule(nFolderId)
            Case "package"
                Dim oProp As New frmPackageProp

                oProp.EditPackage(nFolderId)
            Case "packed report"
                Dim oProp As New frmPackedReport

                oProp.EditReport(nFolderId)
            Case "event-package"
                Dim oProp As frmEventPackageProp = New frmEventPackageProp

                oProp.EditPackage(nFolderId)
        End Select

    End Sub

    Private Sub lsvWindow_AfterLabelEdit(ByVal sender As System.Object, _
    ByVal e As System.Windows.Forms.LabelEditEventArgs) Handles lsvWindow.AfterLabelEdit
        On Error Resume Next

        If lsvWindow.SelectedItems.Count = 0 Then Exit Sub

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim sSelType As String = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":").ToLower
        Dim sCurrentNode As String
        Dim oRs As New ADODB.Recordset
        Dim sEvent As String
        Dim sType As String
        Dim sName As String


        sCurrentNode = tvFolders.SelectedNode.Tag

        If e.Label Is Nothing Then Exit Sub

        If e.Label.Length = 0 Then e.CancelEdit = True : Return

        Dim nParent As Integer = sCurrentNode.Split(":")(1)

        Select Case sSelType
            Case "report", "packed report"

                Dim nCount As Integer
                Dim snewName As String = e.Label

                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.CancelEdit = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.Label, nParent, clsMarsScheduler.enScheduleType.REPORT, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    e.CancelEdit = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT ReportTitle,PackID,Dynamic,Bursting FROM ReportAttr WHERE ReportID =" & nID)

                Dim nPackID As Integer = 0
                Dim nDynamic As Integer
                Dim nBursting As Integer

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                        nPackID = IsNull(oRs(1).Value, 0)
                        nDynamic = IsNull(oRs(2).Value, 0)
                        nBursting = IsNull(oRs(3).Value, 0)
                    End If

                    oRs.Close()
                End If

                If sSelType = "packed report" And e.Label.Contains(":") = False Then
                    SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & nPackID

                    oRs = clsMarsData.GetData(SQL)

                    If oRs Is Nothing Then
                        nCount = 1
                    Else
                        If oRs.EOF = False Then
                            Dim sTemp As String = oRs.Fields(0).Value

                            nCount = sTemp

                            nCount += 1
                        Else
                            nCount = 1
                        End If
                    End If

                    snewName = nCount & ":" & snewName

                    oRs.Close()
                End If

                SQL = "UPDATE ReportAttr SET ReportTitle = '" & SQLPrepare(snewName) & "' WHERE " & _
                                    "ReportID = " & nID

                If nPackID > 0 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.RENAME)
                ElseIf nDynamic = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.RENAME)
                ElseIf nBursting = 1 Then
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.RENAME)
                Else
                    clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.RENAME)
                End If

            Case "package"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.CancelEdit = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.Label, nParent, clsMarsScheduler.enScheduleType.REPORT, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    e.CancelEdit = True
                    Return
                End If

                SQL = "UPDATE PackageAttr SET PackageName = '" & SQLPrepare(e.Label) & "' WHERE " & _
                    "PackID = " & nID

                oRs = clsMarsData.GetData("SELECT PackageName FROM PackageAttr WHERE PackID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.RENAME)
            Case "folder"
                If clsMarsSecurity._HasGroupAccess("Folder Management") = False Then
                    e.CancelEdit = True
                    Return
                End If

                SQL = "UPDATE Folders SET FolderName = '" & SQLPrepare(e.Label) & "' WHERE " & _
                "FolderID = " & nID

                oRs = clsMarsData.GetData("SELECT FolderName FROM FolderAttr WHERE FolderID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.FOLDER, clsMarsAudit.AuditAction.RENAME)
            Case "automation"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.CancelEdit = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.Label, nParent, clsMarsScheduler.enScheduleType.AUTOMATION, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.CancelEdit = True
                    Return
                End If

                SQL = "UPDATE AutomationAttr SET AutoName = '" & SQLPrepare(e.Label) & "' WHERE " & _
                "AutoID = " & nID

                oRs = clsMarsData.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID =" & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sName = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                clsMarsAudit._LogAudit(sName, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.RENAME)
            Case "event"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.CancelEdit = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.Label, nParent, clsMarsScheduler.enScheduleType.EVENTBASED, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.CancelEdit = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID = " & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sEvent = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                SQL = "UPDATE EventAttr6 SET EventName ='" & SQLPrepare(e.Label) & "' WHERE EventID = " & nID


                clsMarsAudit._LogAudit(sEvent, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.RENAME)
            Case "event-package"
                If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
                    e.CancelEdit = True
                    Return
                End If

                'check to see if another with the same name exists in this folder
                If clsMarsUI.candoRename(e.Label, nParent, clsMarsScheduler.enScheduleType.EVENTPACKAGE, nID) = False Then
                    MessageBox.Show(clsMarsUI.renameErrorstring, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                    e.CancelEdit = True
                    Return
                End If

                oRs = clsMarsData.GetData("SELECT PackageName FROM EventPackageAttr WHERE EventPackID = " & nID)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        sEvent = oRs(0).Value
                    End If

                    oRs.Close()
                End If

                SQL = "UPDATE EventPackageAttr SET PackageName ='" & SQLPrepare(e.Label) & "' WHERE EventPackID = " & nID

                clsMarsAudit._LogAudit(sEvent, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.RENAME)
        End Select

        clsMarsData.WriteData(SQL)

        If sSelType = "folder" Then
            oUI.BuildTree(tvFolders, False, False)
            oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
        End If
    End Sub


    Private Sub tvSmartFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvSmartFolders.AfterSelect
        Dim nID As Integer
        Dim sType As String

        nID = Convert.ToString(tvSmartFolders.SelectedNode.Tag).Split(":")(1)
        sType = tvSmartFolders.SelectedNode.Tag.SPlit(":")(0)

        Dim oUI As New clsMarsUI

        Select Case sType.ToLower
            Case "smartfolder"
                oUI.OpenSmartFolder(nID, lsvWindow)
            Case "systemfolder"
                oUI.ViewSystemFolder(nID, lsvWindow)
        End Select

        ReDim gItemList(lsvWindow.Items.Count - 1)

        For I As Integer = 0 To lsvWindow.Items.Count - 1
            gItemList(I) = lsvWindow.Items(I)
        Next

        Dim oLabel As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oLabel.Text = lsvWindow.Items.Count & " items"

        oMain.btnDesign.Enabled = False
        oMain.btnProperties.Enabled = False
    End Sub


    Private Sub mnuEventCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventCopy.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nID As Integer

        nID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        gClipboard(0) = "Event"
        gClipboard(1) = nID

        Dim oItem As ButtonItem

        oItem = DotNetBarManager1.GetItem("mEventPaste", True)

        oItem.Enabled = True

        oItem = DotNetBarManager1.GetItem("mDesktopPaste", True)

        oItem.Enabled = True
    End Sub

    Private Sub mnuEventRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventRename.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuEventPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventPaste.Click
        Dim oEvent As New clsMarsEvent

        oEvent.PasteSchedule(gClipboard(1), CType(tvFolders.SelectedNode.Tag, String).Split(":")(1), CType(tvFolders.SelectedNode.Tag, String).Split(":")(0))
    End Sub

    Private Sub mnuEventStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventStatus.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem

        olsv = lsvWindow.SelectedItems(0)

        oItem = DotNetBarManager1.GetItem("mEventStatus", True)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1
                olsv.ImageIndex = 9

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0
                olsv.ImageIndex = 10

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE EventAttr6 SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE EventID =" & nID

        clsMarsData.WriteData(SQL)

        'oUI.RefreshView(Me)

        If nValue = 1 Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.ENABLE)
        Else
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.DISABLE)
        End If

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuEventDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventDelete.Click

        'If lsvWindow.SelectedItems.Count = 0 Then Return

        'For Each oI As ListViewItem In lsvWindow.SelectedItems
        '    Dim nID As Integer = GetDelimitedWord(oI.Tag, 2, ":")

        '    Dim oEvent As New clsMarsEvent

        '    Dim oRes As DialogResult

        '    If oMulti = True Then
        '        oRes = oMultiRes
        '    Else
        '        oRes = MessageBox.Show("Delete the '" & oI.Text & _
        '                    "' event-based schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        '                    MessageBoxIcon.Question)
        '    End If

        '    If oRes = DialogResult.Yes Then
        '        clsMarsEvent.DeleteEvent(nID)

        '        oI.Remove()
        '    End If
        'Next

        'Dim oItems As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        'oItems.Text = lsvWindow.Items.Count & " items"

        DeleteDesktopItem(sender, e, New Boolean = False)

    End Sub

    Private Sub mnuEventProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventProperties.Click
        If clsMarsSecurity._HasGroupAccess("Create/Edit Schedules") = False Then
            Return
        End If

        Dim oProp As New frmEventProp

        Try
            Dim nID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
            oProp.EditSchedule(nID)
        Catch ex As Exception

        End Try

    End Sub

    Private Sub tvSmartFolders_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tvSmartFolders.MouseUp
        'If e.Button <> MouseButtons.Right Then Return

        'oSelTree = tvSmartFolders

        'If oSelTree.SelectedNode Is Nothing Then Return

        'Dim oUI As clsMarsUI = New clsMarsUI

        'Select Case oSelTree.SelectedNode.Tag.split(":")(0).ToLower
        '    Case "smartfolder"
        '        oUI._PopUpMenu(DotNetBarManager1, "mSmartFolders")
        'End Select
    End Sub


    Private Sub lsvWindow_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
    Handles lsvWindow.KeyUp
        Dim sCurrentNode As String

        If lsvWindow.SelectedItems.Count = 0 Then Return

        If tvFolders.SelectedNode Is Nothing Then
            sCurrentNode = "Desktop:0"
        Else
            sCurrentNode = tvFolders.SelectedNode.Tag
        End If

        If e.KeyCode = Keys.Delete Then

            If lsvWindow.SelectedItems.Count > 1 Then
                oMulti = True
            Else
                oMulti = False
            End If

            If oMulti = True Then
                oMultiRes = MessageBox.Show("Are you sure you would like to delete these " & lsvWindow.SelectedItems.Count & " items?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            End If

            For Each oI As ListViewItem In lsvWindow.SelectedItems
                Select Case GetDelimitedWord(oI.Tag, 1, ":").ToLower
                    Case "report"
                        If oMulti = True Then
                            If oMultiRes = DialogResult.Yes Then
                                oUI.DeleteReport(GetDelimitedWord(oI.Tag, 2, ":").ToLower, 0)
                                oI.Remove()
                            End If
                        Else
                            If MessageBox.Show("Delete the '" & oI.Text & "' schedule?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                                oUI.DeleteReport(GetDelimitedWord(oI.Tag, 2, ":").ToLower, 0)
                                oI.Remove()
                            End If
                        End If
                    Case "package"
                        If oMulti = True Then
                            If oMultiRes = DialogResult.Yes Then
                                oUI.DeletePackage(GetDelimitedWord(oI.Tag, 2, ":").ToLower)
                                oI.Remove()

                                tvFolders.BeginUpdate()
                                oUI.BuildTree(tvFolders, , False)
                                oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
                                tvFolders.EndUpdate()
                            End If
                        Else
                            If MessageBox.Show("Delete the '" & oI.Text & "' package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                                oUI.DeletePackage(GetDelimitedWord(oI.Tag, 2, ":").ToLower)
                                oI.Remove()

                                tvFolders.BeginUpdate()
                                oUI.BuildTree(tvFolders, , False)
                                oUI.FindNode(sCurrentNode, tvFolders, tvFolders.Nodes(0))
                                tvFolders.EndUpdate()
                            End If
                        End If
                    Case "folder"
                        mnuFolderDelete_Click(sender, e)
                    Case "automation"
                        mnuAutoDelete_Click(sender, e)
                    Case "event"
                        mnuEventDelete_Click(sender, e)
                End Select
            Next
        ElseIf e.KeyCode = Keys.Enter Then

            Dim nID As Integer = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

            Select Case lsvWindow.SelectedItems(0).Tag.Split(":")(0).ToLower
                Case "report"
                    Dim oForm As New frmSingleProp

                    oForm.EditSchedule(nID)
                Case "package"
                    Dim oForm As New frmPackageProp

                    oForm.EditPackage(nID)
                Case "event"
                    Dim oForm As New frmEventProp

                    oForm.EditSchedule(nID)
                Case "automation"
                    Dim oForm As New frmAutoProp

                    oForm.EditSchedule(nID)
                Case "packaged report"
                    Dim oForm As New frmPackedReport

                    oForm.EditReport(nID)
            End Select
        End If


    End Sub


    Private Sub lsvWindow_ColumnClick(ByVal sender As System.Object, _
    ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvWindow.ColumnClick

        sortBy = e.Column

        Dim sortString As String

        If sortOrder = Windows.Forms.SortOrder.Ascending Then
            sortOrder = Windows.Forms.SortOrder.Descending
            sortString = " DESC"
        Else
            sortOrder = Windows.Forms.SortOrder.Ascending
            sortString = " ASC"
        End If

        clsMarsUI.sortListView(lsvWindow, sortString, e.Column)

        '        Me.lsvWindow.ListViewItemSorter = New ListViewComparer(e.Column)

        'lsvWindow.Sort()
        'clsMarsUI.MainUI.RefreshView(Me)

        'Dim col As ColumnHeader = lsvWindow.Columns(e.Column)

        'If col.Text.StartsWith(">") = False Then col.Text = "> " & col.Text

        'For Each col2 As ColumnHeader In lsvWindow.Columns
        '    If col2.Index <> e.Column Then
        '        col2.Text = col2.Text.Replace(">", "").Trim
        '    End If
        'Next

        'restore columns
        Try
            IsLoaded = False
            Dim fs As FileStream = New FileStream("columns.dat", FileMode.Open, FileAccess.Read)

            Dim columns As Hashtable = New Hashtable
            Dim bf As BinaryFormatter = New BinaryFormatter

            columns = CType(bf.Deserialize(fs), Hashtable)

            For Each col2 As ColumnHeader In lsvWindow.Columns
                If columns.ContainsKey(col2.Text) = True Then
                    col2.Width = columns.Item(col2.Text)
                End If
            Next

            fs.Close()
        Catch
        Finally
            IsLoaded = True
        End Try
    End Sub

    Private Sub frmWindow_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        nWindowCurrent = Me.Tag
    End Sub


    Private Sub mnuPackageCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageCopy.Click
        If lsvWindow.SelectedItems.Count > 0 Then
            gClipboard(0) = "Package"
            gClipboard(1) = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

            mnuPackagePaste.Enabled = True

            Dim oItem As ButtonItem

            oItem = DotNetBarManager1.GetItem("mPackPaste", True)

            oItem.Enabled = True

            oItem = DotNetBarManager1.GetItem("mDesktopPaste", True)

            oItem.Enabled = True
        End If
    End Sub

    Private Sub mnuPackagePaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackagePaste.Click

        If gClipboard(0) <> "Package" Then Return

        Dim nCopy As Integer
        Dim oReport As clsMarsReport = New clsMarsReport
        Dim nParent As Integer
        Dim sType As String

        sType = tvFolders.SelectedNode.Tag.SPlit(":")(0)

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        End If

        nCopy = oReport.CopyPackage(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmPackageProp = New frmPackageProp

            oProp.tabProperties.SelectedTabIndex = 2

            oProp.EditPackage(nCopy)

            oUI.RefreshView(Me)
        End If
    End Sub

    Private Sub mnuToPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToPackage.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nReportID As Integer
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As New clsMarsData
        Dim nPackID As Int64 = clsMarsData.CreateDataID("packageattr", "packid")
        Dim sName As String
        Dim oUI As New clsMarsUI

        nReportID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)
        sName = lsvWindow.SelectedItems(0).Text

        Dim sWhere As String = " WHERE ReportID =" & nReportID

        'check for embed
        SQL = "SELECT d.Embed FROM DestinationAttr d INNER JOIN ReportAttr r ON " & _
        "d.ReportID = r.ReportID WHERE r.ReportID = " & nReportID & " AND DestinationType = 'Email'"

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                If oRs(0).Value = 1 Then
                    MessageBox.Show("You cannot convert this schedule to a package as the report is embeded in an email. " & _
                    "Please remove this property and then try again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    oRs.Close()
                    Return
                End If
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Do While clsMarsData.IsDuplicate("PackageAttr", "PackageName", sName, True) = True
            sName = InputBox("A package with the provided name already " & _
            "exists. Please provide a name for the package.", _
            Application.ProductName)

            If sName.Length = 0 Then Return
        Loop


        oUI.BusyProgress(10, "Creating Package...")


        'insert into packageattr
        sCols = "PackID,PackageName,Parent,Retry,AssumeFail,CheckBlank,Owner,FailOnOne"

        sVals = nPackID & "," & _
        "'" & SQLPrepare(sName) & "'," & _
        "Parent,Retry,AssumeFail,CheckBlank,'" & gUser & "',0"

        SQL = "INSERT INTO PackageAttr (" & sCols & ") " & _
        "SELECT " & sVals & " FROM ReportAttr " & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        oUI.BusyProgress(30, "Updating report attributes")

        'update reportattr with packid
        SQL = "UPDATE ReportAttr SET " & _
        "ReportTitle = '1:" & SQLPrepare(sName) & "', PackOrderID =1, PackID =" & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'update scheduleattr with packid and reportid to 0
        oUI.BusyProgress(50, "Updating schedule attributes")

        SQL = "UPDATE ScheduleAttr SET ReportID =0, PackID = " & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'insert into packagedreportattr from reportattr
        oUI.BusyProgress(70, "Creating package attributes")

        sCols = "AttrID,PackID,ReportID,OutputFormat,CustomExt,AppendDateTime," & _
        "DateTimeFormat,CustomName,Status"

        sVals = clsMarsData.CreateDataID("packagedreportattr", "attrid") & "," & _
        nPackID & "," & _
        nReportID & "," & _
        "OutputFormat,CustomExt,AppendDateTime,DateTimeFormat,CustomName,1"

        SQL = "INSERT INTO PackagedReportAttr (" & sCols & ") " & _
        "SELECT TOP 1 " & sVals & " FROM DestinationAttr " & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        'update destinationattr with packid and reportid to 0
        oUI.BusyProgress(80, "Updating destination attributes")

        SQL = "UPDATE DestinationAttr SET OutputFormat = 'Package', " & _
        "ReportID =0, PackID =" & nPackID & sWhere

        If clsMarsData.WriteData(SQL) = False Then GoTo RollbackTran

        oUI.BusyProgress(95, "Cleaning up")
        oUI.RefreshView(Me)

        oUI.BusyProgress(, , True)

        Return
RollbackTran:
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID =" & nPackID)
        clsMarsData.WriteData("UPDATE ReportAttr SET PackID =0 WHERE ReportID =" & nReportID)
        clsMarsData.WriteData("UPDATE ScheduleAttr SET PackID =0, ReportID =" & nReportID & _
        " WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM PackagedReportAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("UPDATE DestinationAttr SET PackID =0, ReportID = " & nReportID & _
        " WHERE PackID = " & nPackID)
        oUI.BusyProgress(, , True)
    End Sub

    Private Sub mnuSplitPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSplitPackage.Click
        'get all reports and set packid to 0
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oUI As New clsMarsUI
        Dim oRs, oRs2 As ADODB.Recordset
        Dim sName As String
        Dim nReportID As Integer
        Dim sWhere As String
        Dim sCols, sVals As String
        Dim nParent As Integer
        Dim I As Int32 = 1

        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nPackID As Integer

        nPackID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        sWhere = " WHERE PackID = " & nPackID

        SQL = "SELECT * FROM ReportAttr " & sWhere

        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

        Try
            oUI.BusyProgress(10, "Retrieving packaged reports...")

            If oRs.EOF = True Then
                MessageBox.Show("You cannot split a package that does not contain any reports", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                oRs.Close()
                Return
            End If

            Do While oRs.EOF = False
                nReportID = oRs("reportid").Value
                sName = oRs("reporttitle").Value
                sName = sName.Split(":")(1)
                nParent = tvFolders.SelectedNode.Tag.Split(":")(1)

                Do While clsMarsData.IsDuplicate("ReportAttr", "ReportTitle", sName, True) = True
                    sName = InputBox("Please provide a new name for the single schedule", _
                    Application.ProductName, sName & "1")

                    If sName.Length = 0 Then Return
                Loop

                oUI.BusyProgress(30, "Converting report " & I & " of " & oRs.RecordCount & " to single schedule")

                SQL = "UPDATE ReportAttr SET " & _
                "PackID = 0, " & _
                "ReportTitle = '" & SQLPrepare(sName) & "'," & _
                "Parent = " & nParent & " WHERE " & _
                "ReportID = " & nReportID

                clsMarsData.WriteData(SQL)

                'copy the destination for the report
                Dim OutputFormat, CustomExt, DateTimeFormat, CustomName As String
                Dim AppendDateTime As Integer


                oUI.BusyProgress(50, "Updating report attributes")

                oRs2 = clsMarsData.GetData("SELECT * FROM PackagedReportAttr " & _
                "WHERE ReportID = " & nReportID)

                If oRs2.EOF = False Then
                    OutputFormat = oRs2("outputformat").Value
                    CustomExt = oRs2("customext").Value
                    DateTimeFormat = oRs2("datetimeformat").Value
                    CustomName = oRs2("customname").Value
                    AppendDateTime = oRs2("appenddatetime").Value
                End If

                oRs2.Close()

                oUI.BusyProgress(70, "Copying package destinations")

                oRs2 = clsMarsData.GetData("SELECT * FROM DestinationAttr" & sWhere)

                Do While oRs2.EOF = False
                    Dim newID As Integer = clsMarsData.CreateDataID("destinationattr", "destinationid")

                    sCols = ""

                    For Each field As ADODB.Field In oRs2.Fields
                        Select Case field.Name.ToLower
                            Case "collate"
                                sCols &= "[Collate],"
                            Case "type"
                                sCols &= "[Type],"
                            Case "to"
                                sCols &= "[To],"
                            Case "subject"
                                sCols &= "[Subject],"
                            Case Else
                                sCols &= field.Name & ","
                        End Select
                    Next

                    sCols = sCols.Substring(0, sCols.Length - 1).ToLower

                    sVals = sCols

                    sVals = sVals.Replace("outputformat", "'" & OutputFormat & "'")
                    sVals = sVals.Replace("customext", "'" & CustomExt & "'")
                    sVals = sVals.Replace("datetimeformat", "'" & DateTimeFormat & "'")
                    sVals = sVals.Replace("customname", "'" & CustomName & "'")
                    sVals = sVals.Replace("appenddatetime", AppendDateTime)
                    sVals = sVals.Replace("packid", 0)
                    sVals = sVals.Replace("destinationid", newID)
                    sVals = sVals.Replace("reportid", nReportID)

                    SQL = "INSERT INTO DestinationAttr (" & sCols & ") SELECT " & sVals & " FROM DestinationAttr WHERE DestinationID =" & oRs2(0).Value

                    If clsMarsData.WriteData(SQL) = False Then

                    End If

                    oRs2.MoveNext()
                Loop

                oRs2.Close()

                'copy the packages schedule details

                oUI.BusyProgress(80, "Copying schedule attributes")

                sCols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,ReportID," & _
                "PackID,AutoID,Description,Keyword,CalendarName,SmartID,UseException,ExceptionCalendar"

                Dim nScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

                sVals = nScheduleID & "," & _
                "Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status," & _
                nReportID & "," & _
                "0,AutoID,Description,Keyword,CalendarName,SmartID,UseException,ExceptionCalendar"


                SQL = "INSERT INTO ScheduleAttr (" & sCols & ") " & _
                "SELECT " & sVals & " FROM ScheduleAttr " & sWhere

                clsMarsData.WriteData(SQL)

                sCols = "OptionID,WeekNo,DayNo,MonthsIn,nCount,ScheduleID,ScheduleTYpe,UseOptions"

                sVals = clsMarsData.CreateDataID("scheduleoptions", "optionid") & "," & _
                "WeekNo,DayNo,MonthsIn,nCount," & _
                nScheduleID & ",ScheduleTYpe,UseOptions"

                SQL = "INSERT INTO ScheduleOptions (" & sCols & ") " & _
                "SELECT " & sVals & " FROM ScheduleOptions WHERE ScheduleID =" & _
                "(SELECT ScheduleID FROM ScheduleAttr" & sWhere & ")"

                clsMarsData.WriteData(SQL)

                'copy the tasks

                oUI.BusyProgress(90, "Copying custom tasks")

                oRs2 = clsMarsData.GetData("SELECT TaskID FROM Tasks WHERE " & _
                "ScheduleID =(SELECT ScheduleID FROM ScheduleAttr" & sWhere & ")")

                Do While oRs2.EOF = False
                    sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath," & _
                    "ProgramParameters,WindowStyle,PrinterName,FileList," & _
                    "PauseInt,ReplaceFiles,Msg,SendTo,CC,Bcc,Subject,OrderID," & _
                    "FTPServer,FTPPort,FTPUser,FTPPassword,FTPDirectory," & _
                    "RunWhen,AfterType"

                    sVals = clsMarsData.CreateDataID("tasks", "taskid") & "," & _
                    nScheduleID & "," & _
                    "TaskType,TaskName,ProgramPath," & _
                    "ProgramParameters,WindowStyle,PrinterName,FileList," & _
                    "PauseInt,ReplaceFiles,Msg,SendTo,CC,Bcc,Subject,OrderID," & _
                    "FTPServer,FTPPort,FTPUser,FTPPassword,FTPDirectory," & _
                    "RunWhen,AfterType"

                    SQL = "INSERT INTO Tasks (" & sCols & ") " & _
                    "SELECT " & sVals & " FROM Tasks WHERE " & _
                    "TaskID =" & oRs2.Fields(0).Value

                    clsMarsData.WriteData(SQL)

                    oRs2.MoveNext()
                Loop

                oRs2.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()

            oUI.BusyProgress(95, "Deleting package")

            oUI.DeletePackage(nPackID)

            oUI.BusyProgress(100, "Cleaning up")

            oUI.RefreshView(Me)

            oUI.BusyProgress(, , True)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, "frmWindow.mnuSplitPackage_Click", _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub


    Private Sub oTextBoxItem_InputTextChanged(ByVal sender As Object) Handles oTextBoxItem.InputTextChanged
        Dim oUI As New clsMarsUI
        'Dim oText As TextBoxItem

        'oTextBoxItem = CType(sender, TextBoxItem)

        If oTextBoxItem.ControlText.Length = 0 Or gItemList Is Nothing Then
            Select Case tbNav.SelectedTabIndex
                Case 0
                    Dim oTmpNode As TreeNode

                    oTmpNode = tvFolders.SelectedNode

                    tvFolders.SelectedNode = Nothing

                    tvFolders.SelectedNode = oTmpNode
                Case 1
                    Dim oTmpNode As TreeNode

                    oTmpNode = tvSmartFolders.SelectedNode

                    tvSmartFolders.SelectedNode = Nothing

                    tvSmartFolders.SelectedNode = oTmpNode

                Case 2
                    Dim oTmpNode As TreeNode

                    oTmpNode = tvSystemFolders.SelectedNode

                    tvSystemFolders.SelectedNode = Nothing

                    tvSystemFolders.SelectedNode = oTmpNode
            End Select
        Else
            lsvWindow.BeginUpdate()

            lsvWindow.Items.Clear()

            For Each oItem As ListViewItem In gItemList
                lsvWindow.Items.Add(oItem)
            Next

            For Each oItem As ListViewItem In lsvWindow.Items
                If oItem.Text.ToLower.IndexOf(oTextBoxItem.ControlText.ToLower) = -1 Then
                    oItem.Remove()
                End If
            Next

            lsvWindow.EndUpdate()
        End If
    End Sub


    Private Sub mnuSingleRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleRefresh.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nReportID As Integer

        nReportID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        Dim oReport As New clsMarsReport

        If oReport.RefreshReport(nReportID) = True Then
            MessageBox.Show("Report refreshed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please reselect the source reports and then try refreshing the schedule again.")
        End If
    End Sub

    Private Sub mnuPackageRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageRefresh.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nPackID As Integer
        Dim nReportID As Integer
        Dim oData As New clsMarsData
        Dim oReport As New clsMarsReport
        Dim Ok As Boolean
        Dim I As Integer = 1

        nPackID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        Ok = oReport.RefreshPackage(nPackID)

        If Ok = True Then
            MessageBox.Show("All reports in the package have been refreshed successfully", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)

            oUI.BusyProgress(, , True)
        Else
            _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
        End If
    End Sub

    Private Sub mnuPackedRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackedRefresh.Click
        mnuSingleRefresh_Click(sender, e)
    End Sub

    Private Sub mnuRefreshAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRefreshAll.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        oFolder.RefreshFolder(nID)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub mnuDisableAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDisableAll.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        oFolder.DisableFolder(nID)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub mnuExecuteAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExecuteAll.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        oFolder.ExecuteFolder(nID)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub

    Private Sub mnuEnableAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEnableAll.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oFolder As New clsFolders

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nID As Integer = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        oFolder.EnableFolder(nID)

        MessageBox.Show("The requested task has completed", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)
    End Sub


    Private Sub mnuTestSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTestSchedule.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oTest As New frmTestDestination

        oTest._SelectDestination(lsvWindow.SelectedItems(0).Tag.split(":")(1))
    End Sub

    Private Sub mnuTestPackage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTestPackage.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oTest As New frmTestDestination

        oTest._SelectDestination(, lsvWindow.SelectedItems(0).Tag.Split(":")(1))
    End Sub

    Private Sub tvFolders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.Click
        oSelTree = tvFolders
    End Sub

    Private Sub tvSmartFolders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvSmartFolders.Click
        oSelTree = tvSmartFolders
    End Sub


    Private Sub mnuEventTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventTest.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oT As New clsMarsThreading

        Dim nEventID As Integer

        nEventID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        oT.xEventID = nEventID

        oT.EventScheduleThread()

        'clsMarsEvent.RunEvents6(nEventID)

        clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.EXECUTE)
    End Sub

    Private Sub MenuItem24_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem24.Click
        Dim nPackID As Integer
        Dim nReportID As Integer

        If lsvWindow.SelectedItems.Count = 0 Then Return

        nReportID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        nPackID = tvFolders.SelectedNode.Tag.Split(":")(1)

        Dim oT As New clsMarsThreading

        oT.xReportID = nReportID
        oT.xPackID = nPackID


        AppStatus(True)
        oT.PackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.EXECUTE)

    End Sub

    Private m_node As TreeNode

    Private Sub lsvWindow_ItemDrag(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) _
    Handles lsvWindow.ItemDrag
        Try
            m_node = tvFolders.SelectedNode
            ''console.writeLine(m_node.Text)
        Catch : End Try

        DoDragDrop(e.Item, DragDropEffects.Move)
    End Sub

    Private Sub tvFolders_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvFolders.DragDrop
        Dim oData As New clsMarsData


        Try
            Dim position As New Point(e.X, e.Y)

            position = tvFolders.PointToClient(position)

            Dim dropNode As TreeNode = tvFolders.GetNodeAt(position)

            If Not (dropNode Is Nothing) Then
                If e.Data.GetDataPresent(GetType(TreeNode)) Then
                    ' moving the treenode to another node.
                    Dim node As TreeNode = CType(e.Data.GetData(GetType(TreeNode)), TreeNode)
                    Dim sType As String
                    Dim nID As Integer
                    Dim sParent As String
                    Dim nParent As Integer
                    Dim SQL As String
                    Dim sTemp As String

                    sType = node.Tag.Split(":")(0)
                    nID = node.Tag.Split(":")(1)
                    sTemp = node.Tag

                    sParent = dropNode.Tag.split(":")(0)
                    nParent = dropNode.Tag.split(":")(1)

                    If oUI.IsNotChild(nID, nParent) = False Then
                        ResetTreeviewNodeColor()
                        oUI.RefreshView(Me)
                        Return
                    End If

                    If dropNode.Text.ToLower = "desktop" And sType.ToLower <> "folder" Then Return

                    If sType.ToLower <> "folder" And sParent.ToLower = "desktop" Then Return

                    Select Case sType.ToLower
                        Case "folder"
                            If nID = nParent Then
                                ResetTreeviewNodeColor(dropNode)
                                oUI.RefreshView(Me)
                                Return
                            End If

                            SQL = "UPDATE Folders SET Parent =" & nParent & " WHERE FolderID =" & nID
                        Case "package"
                            If nID = nParent Then
                                ResetTreeviewNodeColor(dropNode)
                                oUI.RefreshView(Me)
                                Return
                            End If

                            SQL = "UPDATE PackageAttr SET Parent =" & nParent & " WHERE PackID =" & nID
                        Case Else
                            ResetTreeviewNodeColor(dropNode)
                            oUI.RefreshView(Me)
                            Return
                    End Select

                    If clsMarsData.WriteData(SQL) = True Then
                        ResetTreeviewNodeColor()
                        oUI.RefreshView(Me)
                        oUI.FindNode(sTemp, tvFolders, tvFolders.Nodes(0))

                        'node.Remove()
                        'Dim index As Integer = dropNode.Nodes.Add(node)
                        'If Not dropNode.IsExpanded Then
                        '    dropNode.Expand()
                        'End If

                        'tvFolders.SelectedNode = dropNode.Nodes(index)
                    End If


                ElseIf e.Data.GetDataPresent(DataFormats.FileDrop) Then
                    Dim filenames As String() = CType(e.Data.GetData(DataFormats.FileDrop), String())
                    For Each file As String In filenames
                        Dim path As String = CStr(dropNode.Tag) & IO.Path.DirectorySeparatorChar & IO.Path.GetFileName(file)
                        If IO.File.Exists(path) Then
                            MsgBox(IO.Path.GetFileName(path) & " already exists. Ignoring.", MsgBoxStyle.Information Or MsgBoxStyle.OkOnly, "File Already Exists")
                        Else
                            IO.File.Move(file, path)
                        End If
                    Next
                ElseIf e.Data.GetDataPresent(GetType(ListViewItem)) Then
                    Dim item As ListViewItem = CType(e.Data.GetData(GetType(ListViewItem)), ListViewItem)

                    Dim sType As String
                    Dim nID As Integer
                    Dim SQL As String
                    Dim nParent As Integer
                    Dim sParent As String

                    sParent = dropNode.Tag.Split(":")(0)
                    nParent = dropNode.Tag.Split(":")(1)

                    sType = item.Tag.Split(":")(0)
                    nID = item.Tag.Split(":")(1)

                    If oUI.IsNotChild(nID, nParent) = False Then
                        ResetTreeviewNodeColor()
                        oUI.RefreshView(Me)
                        Return
                    End If

                    If sParent.ToLower <> "folder" And sParent.ToLower <> "desktop" Then
                        If sType.ToLower <> "packed report" Then
                            ResetTreeviewNodeColor()
                            oUI.RefreshView(Me)
                            Return
                        ElseIf sType.ToLower = "packed report" And sParent.ToLower <> "package" Then
                            ResetTreeviewNodeColor()
                            oUI.RefreshView(Me)
                            Return
                        End If
                    End If

                    If sParent.ToLower = "desktop" And sType.ToLower <> "folder" Then
                        ResetTreeviewNodeColor()
                        oUI.RefreshView(Me)
                        Return
                    End If

                    Select Case sType.ToLower
                        Case "folder"
                            If nID = nParent Then
                                ResetTreeviewNodeColor()
                                oUI.RefreshView(Me)
                                Return
                            End If

                            SQL = "UPDATE Folders SET Parent =" & nParent & " WHERE FolderID =" & nID
                        Case "report"
                            If clsMarsUI.candoRename(item.Text, nParent, clsMarsScheduler.enScheduleType.REPORT, nID) = False Then
                                MessageBox.Show("MARS could complete the move request because a same-typed schedule with this name already exists in the destination folder", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            SQL = "UPDATE ReportAttr SET Parent =" & nParent & " WHERE ReportID =" & nID
                        Case "package"
                            If clsMarsUI.candoRename(item.Text, nParent, clsMarsScheduler.enScheduleType.PACKAGE, nID) = False Then
                                MessageBox.Show("MARS could complete the move request because a same-typed schedule with this name already exists in the destination folder", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            SQL = "UPDATE PackageAttr SET Parent =" & nParent & " WHERE PackID =" & nID
                        Case "event"
                            If clsMarsUI.candoRename(item.Text, nParent, clsMarsScheduler.enScheduleType.EVENTBASED, nID) = False Then
                                MessageBox.Show("MARS could complete the move request because a same-typed schedule with this name already exists in the destination folder", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            SQL = "UPDATE EventAttr6 SET Parent =" & nParent & " WHERE EventID =" & nID
                        Case "automation"
                            If clsMarsUI.candoRename(item.Text, nParent, clsMarsScheduler.enScheduleType.AUTOMATION, nID) = False Then
                                MessageBox.Show("MARS could complete the move request because a same-typed schedule with this name already exists in the destination folder", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            SQL = "UPDATE AutomationAttr SET Parent =" & nParent & " WHERE AutoID =" & nID
                        Case "event-package"
                            If clsMarsUI.candoRename(item.Text, nParent, clsMarsScheduler.enScheduleType.EVENTPACKAGE, nID) = False Then
                                MessageBox.Show("MARS could complete the move request because a same-typed schedule with this name already exists in the destination folder", _
                                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            SQL = "UPDATE EventPackageAttr SET Parent =" & nParent & " WHERE EventPackID =" & nID

                        Case "packed report"
                            If sParent.ToLower <> "package" Then
                                ResetTreeviewNodeColor()
                                Return
                            End If

                            Dim newID As Integer
                            Dim newName As String
                            Dim oldName As String = item.Text
                            Dim oRs As ADODB.Recordset

                            SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & nParent

                            oRs = clsMarsData.GetData(SQL)

                            If oRs IsNot Nothing Then
                                newID += 1
                            Else
                                newID = 1
                            End If

                            oRs.Close()

                            newName = newID & ":" & oldName.Split(":")(1)

                            SQL = "UPDATE ReportAttr SET " & _
                            "PackID =" & nParent & "," & _
                            "ReportTitle ='" & SQLPrepare(newName) & "'," & _
                            "PackOrderID =" & newID & " WHERE ReportID =" & nID

                        Case Else
                            ResetTreeviewNodeColor()
                            Return
                    End Select

                    If clsMarsData.WriteData(SQL) = True Then
                        oUI.RefreshView(Me)
                    End If

                End If
            End If

        Catch ex As Exception

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

        End Try

        ResetTreeviewNodeColor()

        tvFolders.SelectedNode = Nothing
        oUI.FindNode(m_node.Tag, tvFolders)
    End Sub

    Private Sub tvFolders_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvFolders.DragOver
        If TypeOf sender Is TreeView Then

            Dim tv As TreeView = CType(sender, TreeView)
            Dim pt As Point = tv.PointToClient(New Point(e.X, e.Y))
            Dim delta As Integer = tv.Height - pt.Y
            If delta < tv.Height / 2 And delta > 0 Then
                Dim tn As TreeNode = tv.GetNodeAt(pt.X, pt.Y)
                If Not (tn.NextVisibleNode Is Nothing) Then
                    tn.NextVisibleNode.EnsureVisible()
                End If
            End If
            If delta > tv.Height / 2 And delta < tv.Height Then
                Dim tn As TreeNode = tv.GetNodeAt(pt.X, pt.Y)
                If Not (tn.PrevVisibleNode Is Nothing) Then
                    tn.PrevVisibleNode.EnsureVisible()
                End If
            End If

            Dim position As New Point(e.X, e.Y)
            position = tv.PointToClient(position)
            Dim dropNode As TreeNode = tv.GetNodeAt(position)

            If Not dropNode.BackColor.Equals(SystemColors.Highlight) Then
                ResetTreeviewNodeColor(tv.Nodes(0))
                dropNode.BackColor = SystemColors.Highlight
                dropNode.ForeColor = SystemColors.HighlightText
            End If

        End If
    End Sub

    Private Sub tvFolders_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles tvFolders.DragEnter
        m_node = tvFolders.SelectedNode

        'console.writeLine(m_node.Text)

        tvFolders.SelectedNode = Nothing
        If e.Data.GetDataPresent(GetType(TreeNode)) Then
            e.Effect = DragDropEffects.Move
        ElseIf e.Data.GetDataPresent(GetType(ListViewItem)) Then
            e.Effect = DragDropEffects.Move
        ElseIf e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub tvFolders_ItemDrag(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemDragEventArgs) Handles tvFolders.ItemDrag
        DoDragDrop(e.Item, DragDropEffects.Move)
    End Sub

    Private Sub ResetTreeviewNodeColor(Optional ByVal node As TreeNode = Nothing)

        Try
            If Not node Is Nothing Then
                If Not node.BackColor.Equals(Color.Empty) Then
                    node.BackColor = Color.Empty
                    node.ForeColor = Color.Empty
                End If

                If Not node.FirstNode Is Nothing AndAlso node.IsExpanded Then
                    For Each child As TreeNode In node.Nodes
                        If Not child.BackColor.Equals(Color.Empty) Then
                            child.BackColor = Color.Empty
                            child.ForeColor = Color.Empty
                        End If

                        If Not child.FirstNode Is Nothing AndAlso child.IsExpanded Then
                            ResetTreeviewNodeColor(child)
                        End If
                    Next
                End If
            Else
                For Each oNode As TreeNode In tvFolders.Nodes

                    oNode.BackColor = Color.Empty
                    oNode.ForeColor = Color.Empty

                    If oNode.Nodes.Count > 0 Then
                        ResetTreeviewNodeColor(oNode)
                    End If
                Next
            End If
        Catch : End Try


    End Sub

    Private Sub tvFolders_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.DragLeave
        ResetTreeviewNodeColor()
    End Sub


    Private Sub tvSystemFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvSystemFolders.AfterSelect
        Dim oSys As New clsMarsUI
        Dim nID As Integer

        If tvSystemFolders.SelectedNode Is Nothing Then Return

        nID = tvSystemFolders.SelectedNode.Tag.Split(":")(1)

        oSys.ViewSystemFolder(nID, lsvWindow)

        ReDim gItemList(lsvWindow.Items.Count - 1)

        For I As Integer = 0 To lsvWindow.Items.Count - 1
            gItemList(I) = lsvWindow.Items(I)
        Next

        Dim oLabel As LabelItem = DotNetBarManager1.GetItem("lblStatus", True)

        oLabel.Text = lsvWindow.Items.Count & " items"

        oMain.btnDesign.Enabled = False
        oMain.btnProperties.Enabled = False
    End Sub


    Private Sub mnuPackedStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackedStatus.Click

        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nReportID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem

        oItem = DotNetBarManager1.GetItem("mPackedStatus", True)
        olsv = lsvWindow.SelectedItems(0)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1
                olsv.ImageIndex = 1

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0
                olsv.ImageIndex = 3

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE PackagedReportAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE ReportID = " & nReportID

        clsMarsData.WriteData(SQL)

        'oUI.RefreshView(oWindow(nWindowCurrent))

        If nValue = 1 Then
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.ENABLE)
        Else
            clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.PACKED, clsMarsAudit.AuditAction.DISABLE)
        End If

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuRecreateDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRecreateDB.Click
        Dim nID As Integer

        'Search1 = Connection string
        'Search2 = Query string
        'FilePath = KeyColumn
        'PopRemove = Include new data 
        'PopServer = Recordtype

        nID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        Dim SQL As String = "SELECT * FROM EventAttr WHERE EventID =" & nID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Return
        ElseIf oRs.EOF = True Then
            Return
        Else
            Dim sType As String

            sType = oRs("eventtype").Value

            Select Case sType.ToLower
                Case "database record has changed", "database record exists"
                    Dim sCon As String = IsNull(oRs("search1").Value)
                    Dim sQuery As String = IsNull(oRs("search2").Value)
                    Dim sKeycolumn As String = IsNull(oRs("filepath").Value)
                    Dim RecordType As String = IsNull(oRs("popserver").Value)

                    Dim oEvent As New clsMarsEvent

                    'If oEvent.CacheData(lsvWindow.SelectedItems(0).Text, sQuery, sCon, sKeycolumn) = True Then
                    '    MessageBox.Show("Database snapshot recreated successfully!", Application.ProductName, _
                    '    MessageBoxButtons.OK, MessageBoxIcon.Information)

                    '    Return
                    'End If
                Case Else
                    MessageBox.Show("The selected action is not valid for this type of Event-Based Schedule", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Return
            End Select

        End If

    End Sub

    Private Sub mnuViewSystem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuViewSystem.Click
        If tvSystemFolders.SelectedNode Is Nothing Then Return
        Dim SQL(4) As String
        Dim x As Integer

        Select Case tvSystemFolders.SelectedNode.Text.ToLower

            Case "packages due"
                SQL(0) = ""
                SQL(2) = ""
                SQL(3) = ""
                SQL(4) = ""
                x = 1
                SQL(1) = "SELECT * FROM PackageAttr INNER JOIN ScheduleAttr ON " & _
                "PackageAttr.PackID = ScheduleAttr.PackID WHERE " & _
                "Status = 1 AND " & _
                "DATEDIFF([d], [CurrentDate], NextRun) <=0"
            Case "single schedules due"
                SQL(1) = ""
                SQL(2) = ""
                SQL(3) = ""
                SQL(4) = ""
                x = 0
                SQL(0) = "SELECT * FROM ReportAttr INNER JOIN ScheduleAttr ON " & _
                "ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                "Status = 1 AND " & _
                "DATEDIFF([d], [CurrentDate],NextRun) <= 0"
            Case "todays failed packages"
                SQL(0) = ""
                SQL(2) = ""
                SQL(3) = ""
                SQL(4) = ""
                x = 1
                SQL(1) = "SELECT * FROM (PackageAttr INNER JOIN ScheduleHistory ON " & _
                "PackageAttr.PackID = ScheduleHistory.PackID) INNER JOIN ScheduleAttr ON " & _
                "PackageAttr.PackID = ScheduleAttr.PackID WHERE Success =0 AND " & _
                "DATEDIFF([d], [CurrentDate],EntryDate) = 0"
            Case "todays failed single schedules"
                SQL(1) = ""
                SQL(2) = ""
                SQL(3) = ""
                SQL(4) = ""
                x = 0
                SQL(0) = "SELECT * FROM (ReportAttr INNER JOIN ScheduleHistory ON " & _
                "ReportAttr.ReportID = ScheduleHistory.ReportID) INNER JOIN " & _
                "ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID WHERE " & _
                "Success =0 AND " & _
                "DATEDIFF([d], [CurrentDate],EntryDate) = 0"
        End Select

        If gConType = "DAT" Then
            SQL(x) = SQL(x).Replace("[CurrentDate]", "Now()").Replace("[d]", "'d'")
        Else
            SQL(x) = SQL(x).Replace("[CurrentDate]", "GetDate()").Replace("[d]", "d")
        End If

        Dim oReport As frmSmartReport = New frmSmartReport

        oReport._RenderReport(SQL, tvSystemFolders.SelectedNode.Text, True, , True)
    End Sub

    Private Sub tvSmartFolders_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.NodeLabelEditEventArgs) Handles tvSmartFolders.AfterLabelEdit

        Dim sOld As String = e.Node.Text

        Dim sType As String = e.Node.Tag.split(":")(0).tolower

        Dim nID As Integer = e.Node.Tag.split(":")(1)

        If e.Label Is Nothing Then e.CancelEdit = True : Return

        If e.Label.Length = 0 Then e.CancelEdit = True : Return

        Dim oData As New clsMarsData

        If clsMarsData.IsDuplicate("smartfolders", "smartname", e.Label, False) = True Then
            _ErrorHandle("A smart folder with that name already exists", _
            -91, "frmWindow.tvFolders_AfterLabelEdit", 2019)
            e.CancelEdit = True

            Return
        End If

        Dim SQL As String

        SQL = "UPDATE SmartFolders SET " & _
        "SmartName = '" & SQLPrepare(e.Label) & "' WHERE " & _
        "SmartID = " & nID

        If clsMarsData.WriteData(SQL) = False Then
            e.CancelEdit = True
        End If

    End Sub

    Private Sub exSplitter_ExpandedChanged(ByVal sender As System.Object, ByVal e As DevComponents.DotNetBar.ExpandedChangeEventArgs) Handles exSplitter.ExpandedChanged
        If exSplitter.Expanded = True Then
            oUI.SaveRegistry("ExpandSplitter", 1)
        Else
            oUI.SaveRegistry("ExpandSplitter", 0)
        End If
    End Sub

    Private Sub tbNav_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tbNav.SelectedTabChanged
        On Error Resume Next
        If IsLoaded = False Then Return

        Select Case e.NewTab.Name.ToLower
            Case "tbcrd"
                On Error Resume Next
                Dim oNode As TreeNode = tvFolders.Nodes(0)

                'tvFolders.SelectedNode = oNode.Nodes(0)

                If Me.m_selctedFolderItem <> "" Then oUI.FindNode(Me.m_selctedFolderItem, tvFolders, oNode)

                tvSmartFolders.SelectedNode = Nothing
                tvSystemFolders.SelectedNode = Nothing

                If Me.tvFolders.SelectedNode Is Nothing Then
                    tvFolders.SelectedNode = tvFolders.Nodes(0)
                End If

                Dim oBtn As ButtonItem = DotNetBarManager1.GetItem("btnUp", True)

                oBtn.Enabled = True

                lsvWindow.Visible = True
                c1sched.Visible = False
                grpGantt.Visible = False
                Me.processOutlook = False
            Case "tbsmartfolders"
                On Error Resume Next

                If tvSmartFolders.Nodes.Count = 0 Then
                    lsvWindow.Items.Clear()
                Else
                    tvSmartFolders.SelectedNode = tvSmartFolders.Nodes(0)
                End If

                tvFolders.SelectedNode = Nothing
                tvSystemFolders.SelectedNode = Nothing

                Dim oBtn As ButtonItem = DotNetBarManager1.GetItem("btnUp", True)

                oBtn.Enabled = False


                lsvWindow.Visible = True
                c1sched.Visible = False
                grpGantt.Visible = False
                Me.processOutlook = False
            Case "tbsystemfolders"
                On Error Resume Next

                tvSystemFolders.SelectedNode = tvSystemFolders.Nodes(0)

                tvSmartFolders.SelectedNode = Nothing
                tvFolders.SelectedNode = Nothing

                Dim oBtn As ButtonItem = DotNetBarManager1.GetItem("btnUp", True)

                oBtn.Enabled = False


                lsvWindow.Visible = True
                c1sched.Visible = False
                grpGantt.Visible = False
                Me.processOutlook = False
            Case "tbexpose"
                lsvWindow.Visible = False

                If Me.c1sched Is Nothing Then Me.initOutlookView()

                If optGantt.Checked = False Then
                    c1sched.Visible = True
                    c1sched.Dock = DockStyle.Fill

                    clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType)

                    TrackBar1.Value = 5
                Else
                    Me.grpGantt.Visible = True
                    Me.grpGantt.Dock = DockStyle.Fill
                    Dim selDates() As Date = oCal.SelectedDates

                    oCharter.setChartRange(Me.gantt, selDates(0), Me.dtStart.Value, Me.dtEnd.Value, Me.m_CalcType, Me.m_excludeTypes)
                End If

                Me.processOutlook = True

        End Select
    End Sub


    Private Sub tbNav_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbNav.SizeChanged
        If IsLoaded = True Then
            oUI.SaveRegistry("NavBar", tbNav.Width)
        End If
    End Sub


    Private Sub frmWindow_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Try
            If IsFormLoaded = False Then Return

            If oMain.m_formSize IsNot Nothing Then
                oMain.m_formSize.Item("Width") = Me.Width
                oMain.m_formSize.Item("Height") = Me.Height
            End If
        Catch : End Try
    End Sub

    Private Sub frmWindow_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If IsFormLoaded = False Then Return

        Select Case Me.WindowState
            Case FormWindowState.Maximized
                oUI.SaveRegistry("MainWindowState", "Maximized")
            Case FormWindowState.Normal
                oUI.SaveRegistry("MainWindowState", "Normal")

                If oMain.m_formSize IsNot Nothing Then
                    oMain.m_formSize.Item("Width") = Me.Width
                    oMain.m_formSize.Item("Height") = Me.Height
                End If
            Case FormWindowState.Minimized
                oUI.SaveRegistry("MainWindowState", "Minimized")
        End Select


    End Sub


    Private Sub mnuPackageRR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageRR.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim packID As Integer = 0

        If lsvWindow.SelectedItems.Count = 0 Then Return

        packID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        SQL = "SELECT * FROM DestinationAttr WHERE PackID =" & packID & " AND ReadReceipts  = 1"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            Dim destID As Integer = oRs("destinationid").Value

            clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus = 'Disabled' WHERE DestinationID =" & destID)

            clsMarsData.WriteData("UPDATE DestinationAttr SET ReadReceipts = 0 WHERE DestinationID =" & destID)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Private Sub mnuSingleRR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleRR.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim reportID As Integer = 0

        If lsvWindow.SelectedItems.Count = 0 Then Return

        reportID = lsvWindow.SelectedItems(0).Tag.Split(":")(1)

        SQL = "SELECT * FROM DestinationAttr WHERE reportID =" & reportID & " AND ReadReceipts  = 1"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            Dim destID As Integer = oRs("destinationid").Value

            clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus = 'Disabled' WHERE DestinationID =" & destID)

            clsMarsData.WriteData("UPDATE DestinationAttr SET ReadReceipts = 0 WHERE DestinationID =" & destID)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub


    Private Sub tmRefresh_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmRefresh.Elapsed
        On Error Resume Next
        lsvWindow.BeginUpdate()
        oUI.RefreshView(Me)
        lsvWindow.EndUpdate()
    End Sub


    'Private Sub mainWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) 'Handles mainWorker.DoWork
    '    Dim oT As clsMarsThreading = New clsMarsThreading

    '    Select Case Me.m_scheduleType
    '        Case scheduleType.SINGLES
    '            oT.xReportID = Me.m_currentScheduleID
    '            oT.SingleScheduleThread()
    '        Case scheduleType.PACKAGE
    '            oT.xPackID = Me.m_currentScheduleID
    '            oT.PackageScheduleThread()
    '        Case scheduleType.SINGLEDYNAMIC
    '            oT.xReportID = Me.m_currentScheduleID
    '            oT.DynamicScheduleThread()
    '        Case scheduleType.PACKAGEDYNAMIC
    '            oT.xPackID = Me.m_currentScheduleID
    '            oT.PackageScheduleThread()
    '        Case scheduleType.AUTOMATION
    '            oT.xAutoID = Me.m_currentScheduleID
    '            oT.AutoScheduleThread()
    '        Case scheduleType.BURSTING
    '            oT.xReportID = Me.m_currentScheduleID
    '            oT.BurstingScheduleThread()
    '        Case scheduleType.EVENTBASED
    '            oT.xEventID = Me.m_currentScheduleID
    '            oT.EventScheduleThread()
    '        Case scheduleType.EVENTPACKAGE
    '            oT.xPackID = Me.m_currentScheduleID
    '            oT.EventPackageScheduleThread()
    '    End Select
    'End Sub

    Private Sub mnuEPProperties_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPProperties.Click
        If Me.lsvWindow.SelectedItems.Count = 0 Then Return

        Dim packID As Integer = CType(lsvWindow.SelectedItems(0).Tag, String).Split(":")(1)

        Dim oProp As frmEventPackageProp = New frmEventPackageProp

        oProp.EditPackage(packID)

    End Sub

    Private Sub mnuEPExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPExecute.Click
        Dim oEvent As clsMarsEvent = New clsMarsEvent
        Dim oSchedule As clsMarsScheduler = New clsMarsScheduler
        Dim Success As Boolean
        Dim nPackID As Integer
        Dim oExecute As clsMarsThreading = New clsMarsThreading

        nPackID = CType(lsvWindow.SelectedItems(0).Tag, String).Split(":")(1)

        oExecute.xPackID = nPackID
        AppStatus(True)
        oExecute.EventPackageScheduleThread()
        AppStatus(False)

        clsMarsAudit._LogAudit(lsvWindow.SelectedItems(0).Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.EXECUTE)
    End Sub

    Private Sub mnuEPCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPCopy.Click
        If lsvWindow.SelectedItems.Count > 0 Then
            gClipboard(0) = "Event Package"
            gClipboard(1) = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")

            mnuEPPaste.Enabled = True

            Dim oItem As ButtonItem

            oItem = DotNetBarManager1.GetItem("mEPPaste", True)

            oItem.Enabled = True

            oItem = DotNetBarManager1.GetItem("mDesktopPaste", True)

            oItem.Enabled = True
        End If
    End Sub

    Private Sub mnuEPRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPRename.Click
        lsvWindow.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub mnuEPStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPStatus.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nValue As Integer
        Dim nPackID As Integer = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim oItem As ButtonItem
        Dim olsv As ListViewItem

        oItem = DotNetBarManager1.GetItem("mEPStatus", True)
        olsv = lsvWindow.SelectedItems(0)

        If oItem Is Nothing Then Return

        Select Case oItem.Checked
            Case False
                nValue = 1
                olsv.ImageIndex = 15

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Navy
                Next
            Case True
                nValue = 0
                olsv.ImageIndex = 16

                For Each i As ListViewItem.ListViewSubItem In olsv.SubItems
                    i.ForeColor = Color.Gray
                Next
        End Select

        SQL = "UPDATE ScheduleAttr SET " & _
            "Status = " & nValue & "," & _
            "DisabledDate = '" & ConDateTime(Date.Now) & "'" & _
            " WHERE EventPackID = " & nPackID

        clsMarsData.WriteData(SQL)

        'Dim node As TreeNode = tvFolders.SelectedNode

        'For Each n As TreeNode In node.Nodes
        '    If n.Tag = "Package:" & nPackID Then
        '        If nValue = 1 Then
        '            n.ImageIndex = 3
        '        Else
        '            n.ImageIndex = 9
        '        End If
        '    End If
        'Next

        'oUI.RefreshView(oWindow(nWindowCurrent))

        Dim oAction As clsMarsAudit.AuditAction

        If nValue = 0 Then
            oAction = clsMarsAudit.AuditAction.DISABLE
        Else
            oAction = clsMarsAudit.AuditAction.ENABLE
        End If

        clsMarsAudit._LogAudit(olsv.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, oAction)

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuEPPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPPaste.Click
        If gClipboard(0) <> "Event Package" Then Return

        Dim nCopy As Integer
        Dim nParent As Integer
        Dim sType As String
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        sType = tvFolders.SelectedNode.Tag.SPlit(":")(0)

        If sType.ToLower <> "folder" Then
            Dim oFolder As New frmFolders
            Dim nTemp() As String

            nTemp = oFolder.GetFolder()

            nParent = nTemp(1)
        Else
            nParent = GetDelimitedWord(tvFolders.SelectedNode.Tag, 2, ":")
        End If

        nCopy = oEvent.PastePackageSchedule(gClipboard(1), nParent)

        If nCopy > 0 Then
            Dim oProp As frmEventPackageProp = New frmEventPackageProp

            oProp.tabProperties.SelectedTab = oProp.tbGeneral

            oProp.EditPackage(nCopy)

            oUI.RefreshView(Me)
        End If
    End Sub

    Private Sub mnuEPNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPNew.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim oEvent As New frmEventWizard6
        Dim eventDtls As Hashtable
        Dim eventName As String = ""
        Dim eventID As Integer = 0

        Dim nPackID As Integer = CType(lsvWindow.SelectedItems(0).Tag, String).Split(":")(1)
        Dim SQL As String = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & nPackID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        Dim PackOrderID As Integer

        PackOrderID = IsNull(oRs.Fields(0).Value, -1) + 1

        oRs.Close()

        oEvent.txtLocation.Tag = "99999"
        oEvent.txtLocation.Text = "\Package"
        oEvent.txtLocation.Enabled = False
        oEvent.btnBrowse.Enabled = False
        eventDtls = oEvent.AddSchedule(PackOrderID)

        If eventDtls IsNot Nothing Then
            eventID = eventDtls.Item("ID")

            SQL = "UPDATE EventAttr6 SET PackID =" & nPackID & " WHERE EventID =" & eventID

            clsMarsData.WriteData(SQL)

            clsMarsUI.MainUI.RefreshView(Me)

            MessageBox.Show("Schedule added successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub mnuEPExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPExisting.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim values As Hashtable
        Dim getEvents As frmFolders = New frmFolders

        getEvents.m_eventOnly = True

        values = getEvents.GetEventBasedSchedule

        If values IsNot Nothing Then
            Dim nPackID As Integer = CType(lsvWindow.SelectedItems(0).Tag, String).Split(":")(1)
            Dim SQL As String = "SELECT MAX(PackOrderID) FROM EventAttr6 WHERE PackID =" & nPackID
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Dim PackOrderID As Integer

            PackOrderID = IsNull(oRs.Fields(0).Value, -1) + 1

            oRs.Close()

            Dim eventID As Integer = values.Item("ID")

            SQL = "UPDATE EventAttr6 SET PackID =" & nPackID & ",PackOrderID =" & PackOrderID & " WHERE EventID =" & eventID

            clsMarsData.WriteData(SQL)

            clsMarsUI.MainUI.RefreshView(Me)

            MessageBox.Show("Schedule added successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub mnuEPSplit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPSplit.Click
        Dim oRes As DialogResult = MessageBox.Show("Delete the package after splitting it?", Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Cancel Then Return

        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim nPackID As Integer = CType(lsvWindow.SelectedItems(0).Tag, String).Split(":")(1)

        Dim nParent As Integer = CType(tvFolders.SelectedNode.Tag, String).Split(":")(1)

        Dim SQL As String = "UPDATE EventAttr6 SET PackID = 0, Parent = " & nParent & " WHERE PackID =" & nPackID

        clsMarsData.WriteData(SQL)

        If oRes = Windows.Forms.DialogResult.Yes Then
            Dim oEvent As New clsMarsEvent

            oEvent.DeleteEventPackage(nPackID)
        End If

        MessageBox.Show("The package has been split successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        clsMarsUI.MainUI.RefreshView(Me)
    End Sub

    Private Sub mnuEPDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPDelete.Click
        DeleteDesktopItem(sender, e, New Boolean = False)
    End Sub

    Private Sub mnuEPOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPOpen.Click
        Dim sType As String = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 1, ":")

        Dim nFolderId As Int32 = GetDelimitedWord(lsvWindow.SelectedItems(0).Tag, 2, ":")
        Dim sParent As String = lsvWindow.SelectedItems(0).Text
        Dim sParentTag As String = lsvWindow.SelectedItems(0).Tag
        Dim oNode As TreeNode


        nWindowCount += 1
        ReDim Preserve oWindow(nWindowCount)
        oWindow(nWindowCount) = New frmWindow
        oWindow(nWindowCount).MdiParent = oMain
        oWindow(nWindowCount).Show()
        oWindow(nWindowCount).Tag = nWindowCount
        nWindowCurrent = nWindowCount

        oUI.OpenWindow(nFolderId, oWindow(nWindowCount).lsvWindow)

        oWindow(nWindowCount).Text = sParent

        oUI.FindNode(sParentTag, oWindow(nWindowCount).tvFolders, oWindow(nWindowCount).tvFolders.Nodes(0))
    End Sub

    Private Sub mnuSingleShortCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleShortCut.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(lsvWindow.SelectedItems(0).Text, clsMarsUI.enScheduleType.REPORT, fbd.SelectedPath) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub mnuPackageShortCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageShortCut.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(lsvWindow.SelectedItems(0).Text, clsMarsUI.enScheduleType.PACKAGE, fbd.SelectedPath) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub mnuAutoShortcut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoShortcut.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(lsvWindow.SelectedItems(0).Text, clsMarsUI.enScheduleType.AUTOMATION, fbd.SelectedPath) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub mnuEventShortcut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventShortcut.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(lsvWindow.SelectedItems(0).Text, clsMarsUI.enScheduleType.EVENTBASED, fbd.SelectedPath) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub mnuEventPackageShortcut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEventPackageShortcut.Click
        If lsvWindow.SelectedItems.Count = 0 Then Return

        Dim fbd As New FolderBrowserDialog

        fbd.ShowNewFolderButton = True

        fbd.Description = "Please select the folder to place the shortcut"

        If fbd.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            If clsMarsUI.MainUI.createScheduleShortcut(lsvWindow.SelectedItems(0).Text, clsMarsUI.enScheduleType.EVENTPACKAGE, fbd.SelectedPath) = True Then
                MessageBox.Show("The shortcut has been created successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub

    Private Sub optDay_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDay.CheckedChanged, optWeek.CheckedChanged, optMonth.CheckedChanged, optGantt.CheckedChanged
        If IsLoaded = False Then Return

        If optGantt.Checked = True Then
            c1sched.Visible = False
            grpGantt.Visible = True
            grpGantt.Dock = DockStyle.Fill
        Else
            grpGantt.Visible = False

            c1sched.Visible = True
            c1sched.Dock = DockStyle.Fill
        End If

        If optDay.Checked Then
            c1sched.ViewType = ScheduleViewEnum.DayView
        ElseIf optWeek.Checked Then
            c1sched.ViewType = ScheduleViewEnum.WeekView
        ElseIf optMonth.Checked = True Then
            c1sched.ViewType = ScheduleViewEnum.MonthView
        ElseIf optGantt.Checked = True Then
            Dim tit As C1.Win.C1Chart.Title = gantt.Header
            Dim selDates() As Date = oCal.SelectedDates

            tit.Text = selDates(0)

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes)
        End If

        TrackBar1.Enabled = optDay.Checked
        lblInterval.Enabled = TrackBar1.Enabled
    End Sub

    Private Sub c1sched_AppointmentChanged(ByVal sender As Object, ByVal e As C1.C1Schedule.AppointmentEventArgs) Handles c1sched.AppointmentChanged
        If Me.processOutlook = True Then
            Dim apt As C1.C1Schedule.Appointment = e.Appointment
            Dim lbl As C1.C1Schedule.Label = apt.Label
            Dim nID As Integer = apt.Tag
            Dim type As String = lbl.Text
            Dim newStart As Date = CTimeZ(apt.Start, dateConvertType.WRITE)
            Dim scheduleID As Integer

            Select Case type
                Case "Report"
                    scheduleID = clsMarsScheduler.GetScheduleID(nID)
                Case "Package"
                    scheduleID = clsMarsScheduler.GetScheduleID(, nID)
                Case "Automation"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , nID)
                Case "EventPackage"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)
                Case Else
                    Return
            End Select

            Dim SQL As String = "UPDATE ScheduleAttr SET " & _
            "StartTime = '" & ConTime(newStart) & "'," & _
            "NextRun = '" & ConDate(newStart) & " " & ConTime(newStart) & "' " & _
            "WHERE ScheduleID = " & scheduleID

            If clsMarsData.WriteData(SQL) = True Then
                MessageBox.Show("The schedule has been modified successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        End If
    End Sub


    Private Sub c1sched_BeforeAppointmentShow(ByVal sender As Object, ByVal e As C1.C1Schedule.CancelAppointmentEventArgs) Handles c1sched.BeforeAppointmentShow
        e.Cancel = True

        Dim apt As C1.C1Schedule.Appointment = e.Appointment

        Dim nID As Integer = apt.Tag
        Dim lbl As C1.C1Schedule.Label = apt.Label
        Dim type As String = lbl.Text

        Select Case type
            Case "Report"
                Dim oProp As frmSingleProp = New frmSingleProp

                oProp.EditSchedule(nID)
            Case "Package"
                Dim oProp As frmPackageProp = New frmPackageProp

                oProp.EditPackage(nID)
            Case "Automation"
                Dim oProp As frmAutoProp = New frmAutoProp

                oProp.EditSchedule(nID)
            Case "EventPackage"
                Dim oProp As frmEventPackageProp = New frmEventPackageProp

                oProp.EditPackage(nID)
        End Select


    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If Me.optGantt.Checked = False Then
            Me.processOutlook = False
            clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType, Me.m_excludeTypes)
            Me.processOutlook = True
        Else
            Dim selDates() As Date = oCal.SelectedDates

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub

    'Private Sub cmbInterval_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Select Case cmbInterval.Text
    '        Case "Five Minutes"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FiveMinutes
    '        Case "Six Minutes"
    '            Me.c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.SixMinutes
    '        Case "Ten Minutes"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TenMinutes
    '        Case "Fifteen Minutes"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FifteenMinutes
    '        Case "Twenty Minutes"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TwentyMinutes
    '        Case "Thirty Minutes"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.ThirtyMinutes
    '        Case "One Hour"
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.OneHour
    '        Case Else
    '            c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FiveMinutes
    '    End Select
    'End Sub

    Private Sub TrackBar1_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TrackBar1.Scroll
        Select Case TrackBar1.Value
            Case 0
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FiveMinutes
                Me.lblInterval.Text = "Five Minutes"
            Case 1
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.SixMinutes
                Me.lblInterval.Text = "Six Minutes"
            Case 2
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TenMinutes
                Me.lblInterval.Text = "Ten Minutes"
            Case 3
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.FifteenMinutes
                Me.lblInterval.Text = "Fifteen Minutes"
            Case 4
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.TwentyMinutes
                Me.lblInterval.Text = "Twenty Minutes"
            Case 5
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.ThirtyMinutes
                Me.lblInterval.Text = "Thirty Minutes"
            Case 6
                c1sched.CalendarInfo.TimeInterval = TimeScaleEnum.OneHour
                Me.lblInterval.Text = "One Hour"
        End Select



    End Sub

    Private Sub lsvItems_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvItems.ItemChecked
        If optGantt.Checked = False Then
            If processOutlook = True Then
                Me.processOutlook = False
                clsMarsExpose.globalItem.LoadSchedules(Me.c1sched, Me.m_CalcType, Me.m_excludeTypes)
                Me.processOutlook = True
            End If
        Else
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.drawChart(gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes)
        End If

    End Sub

    Private Sub iCal_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs)
        Dim selDates() As Date = oCal.SelectedDates

        If optGantt.Checked = False Then
            Try

                c1sched.GoToDate(selDates(0))
            Catch : End Try
        Else
            Dim tit As C1.Win.C1Chart.Title = gantt.Header

            tit.Text = selDates(0)

            oCharter.drawChart(gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If

    End Sub


    Private Sub btnRedraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRedraw.Click
        If dtStart.Value > dtEnd.Value Then
            ep.SetError(dtEnd, "Please select a valid time range")
        Else
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.setChartRange(Me.gantt, selDates(0), Me.dtStart.Value, Me.dtEnd.Value, m_CalcType, Me.m_excludeTypes, _
            txtFilter.Text)
        End If
    End Sub

    Private Sub optMedian_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMedian.CheckedChanged, optAverage.CheckedChanged, optMinimum.CheckedChanged, optMaximum.CheckedChanged
        If Me.processOutlook = True Then
            Dim selDates() As Date = oCal.SelectedDates

            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub

    Private Sub dtEnd_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dtEnd.KeyDown
        If e.KeyCode = Keys.Return Or e.KeyCode = Keys.Enter Then
            Me.btnRedraw_Click(sender, e)
        End If
    End Sub

    Private Sub dtStart_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtStart.ValueChanged, dtEnd.ValueChanged
        ep.SetError(dtEnd, "")
    End Sub

    Private Sub gantt_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles gantt.DoubleClick
        Dim img As System.Drawing.Image = gantt.GetImage()
        Dim sfd As New SaveFileDialog

        With sfd
            .CheckPathExists = True
            .OverwritePrompt = True
            .Title = "Save Gantt Chart image as..."
            .Filter = "Portable Network Graphics Image|*.png"
            .DefaultExt = "png"

            If .ShowDialog = Windows.Forms.DialogResult.OK Then
                img.Save(.FileName)
            End If
        End With

    End Sub

    Private Sub txtFilter_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtFilter.KeyDown
        If (e.KeyCode = Keys.Enter Or e.KeyCode = Keys.Return) Then
            Dim selDates() As Date = oCal.SelectedDates
            oCharter.drawChart(Me.gantt, selDates(0), Me.m_CalcType, Me.m_excludeTypes, txtFilter.Text)
        End If
    End Sub

    <DebuggerStepThrough()> _
    Public Sub IsBusy(ByVal busy As Boolean)
        If busy = True Then
            Me.Cursor = Cursors.WaitCursor
            Me.MdiParent.Cursor = Cursors.WaitCursor
        Else
            Me.Cursor = Cursors.Default
            Me.MdiParent.Cursor = Cursors.Default
        End If
    End Sub

    Private Sub initOutlookView()
10:     Try
            'sort out the calendar
20:         Me.oCal = New C1.Win.C1Schedule.C1Calendar
30:         Me.NavBarGroupControlContainer1.Controls.Add(Me.oCal)
40:         Me.oCal.BoldedDates = New Date(-1) {}
50:         Me.oCal.BorderStyle = System.Windows.Forms.BorderStyle.None
60:         Me.oCal.Location = New System.Drawing.Point(7, 3)
70:         Me.oCal.Name = "oCal"

80:         Me.oCal.ShowWeekNumbers = False
90:         Me.oCal.Size = New System.Drawing.Size(214, 140)
100:        Me.oCal.TabIndex = 0
110:        Me.oCal.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Blue

120:        oUI.BusyProgress(10, "Loading Outlook view...")
130:        Me.c1sched = New C1.Win.C1Schedule.C1Schedule

140:        CType(Me.c1sched, System.ComponentModel.ISupportInitialize).BeginInit()
150:        CType(Me.c1sched.DataStorage.AppointmentStorage, System.ComponentModel.ISupportInitialize).BeginInit()
160:        CType(Me.c1sched.DataStorage.CategoryStorage, System.ComponentModel.ISupportInitialize).BeginInit()
170:        CType(Me.c1sched.DataStorage.ContactStorage, System.ComponentModel.ISupportInitialize).BeginInit()
180:        CType(Me.c1sched.DataStorage.LabelStorage, System.ComponentModel.ISupportInitialize).BeginInit()
190:        CType(Me.c1sched.DataStorage.ResourceStorage, System.ComponentModel.ISupportInitialize).BeginInit()
200:        CType(Me.c1sched.DataStorage.StatusStorage, System.ComponentModel.ISupportInitialize).BeginInit()

210:        Me.oCal.Schedule = Me.c1sched

220:        oUI.BusyProgress(40, "Configuring Outlook view...")
230:        Me.c1sched.CalendarInfo.DateFormatString = "M/d/yyyy"
            Me.c1sched.CalendarInfo.EndDayTime = System.TimeSpan.Parse("19:00:00")
240:        Me.c1sched.CalendarInfo.FirstDate = Date.Now.AddMonths(-12)
250:        Me.c1sched.CalendarInfo.LastDate = Date.Now.AddMonths(12)
            Me.c1sched.CalendarInfo.StartDayTime = System.TimeSpan.Parse("07:00:00")
260:        Me.c1sched.CalendarInfo.TimeFormatString = "tt"
            Me.c1sched.CalendarInfo.TimeScale = System.TimeSpan.Parse("00:30:00")
270:        Me.c1sched.CalendarInfo.WorkDays.AddRange(New System.DayOfWeek() {System.DayOfWeek.Monday, System.DayOfWeek.Tuesday, System.DayOfWeek.Wednesday, System.DayOfWeek.Thursday, System.DayOfWeek.Friday})
            '
            '
            '
280:        oUI.BusyProgress(60, "Finalizing Outlook view...")
290:        Me.c1sched.EditOptions = CType((C1.Win.C1Schedule.EditOptions.AllowDrag Or C1.Win.C1Schedule.EditOptions.AllowAppointmentEdit), C1.Win.C1Schedule.EditOptions)
300:        Me.c1sched.Location = New System.Drawing.Point(27, 28)
310:        Me.c1sched.Name = "c1sched"
320:        Me.c1sched.ShowContextMenu = False
330:        Me.c1sched.ShowReminderForm = False
340:        Me.c1sched.Size = New System.Drawing.Size(271, 185)
350:        Me.c1sched.TabIndex = 24
360:        Me.c1sched.Visible = False
370:        Me.c1sched.VisualStyle = C1.Win.C1Schedule.UI.VisualStyle.Office2007Black

380:        Me.Panel1.Controls.Add(Me.c1sched)

390:        CType(Me.c1sched.DataStorage.AppointmentStorage, System.ComponentModel.ISupportInitialize).EndInit()
400:        CType(Me.c1sched.DataStorage.CategoryStorage, System.ComponentModel.ISupportInitialize).EndInit()
410:        CType(Me.c1sched.DataStorage.ContactStorage, System.ComponentModel.ISupportInitialize).EndInit()
420:        CType(Me.c1sched.DataStorage.LabelStorage, System.ComponentModel.ISupportInitialize).EndInit()
430:        CType(Me.c1sched.DataStorage.ResourceStorage, System.ComponentModel.ISupportInitialize).EndInit()
440:        CType(Me.c1sched.DataStorage.StatusStorage, System.ComponentModel.ISupportInitialize).EndInit()
450:        CType(Me.c1sched, System.ComponentModel.ISupportInitialize).EndInit()

460:        Dim themeName As String = clsMarsUI.MainUI.ReadRegistry("AppTheme", "Glass")
            Dim theme As clsMarsUI.APPTHEMES


470:        oUI.BusyProgress(80, "Loading Outlook theme...")

480:        Select Case themeName
                Case "Glass"
490:                theme = clsMarsUI.APPTHEMES.Glass
500:            Case "XP"
510:                theme = clsMarsUI.APPTHEMES.XP
520:            Case "Corporate"
530:                theme = clsMarsUI.APPTHEMES.Corporate
540:            Case "Classic"
550:                theme = clsMarsUI.APPTHEMES.Classic
            End Select

560:        clsMarsUI.MainUI.SetAppTheme(theme)
570:    Catch ex As Exception
580:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
590:    Finally
600:        oUI.BusyProgress(100, "", True)
        End Try
    End Sub


    
    Private Sub lsvItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvItems.SelectedIndexChanged

    End Sub
End Class
