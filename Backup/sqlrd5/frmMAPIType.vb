Imports Microsoft.Win32
Public Class frmMAPIType
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbMapiType As System.Windows.Forms.ComboBox
    Friend WithEvents grpExchange As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtAlias As System.Windows.Forms.TextBox
    Friend WithEvents txtServer As System.Windows.Forms.TextBox
    Friend WithEvents cmdVerify As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMAPIType))
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbMapiType = New System.Windows.Forms.ComboBox
        Me.grpExchange = New System.Windows.Forms.GroupBox
        Me.cmdVerify = New System.Windows.Forms.Button
        Me.txtAlias = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtServer = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.grpExchange.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(200, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "MAPI Type"
        '
        'cmbMapiType
        '
        Me.cmbMapiType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMapiType.ItemHeight = 13
        Me.cmbMapiType.Items.AddRange(New Object() {"Stand-Alone Outlook", "Exchange Server"})
        Me.cmbMapiType.Location = New System.Drawing.Point(8, 24)
        Me.cmbMapiType.Name = "cmbMapiType"
        Me.cmbMapiType.Size = New System.Drawing.Size(272, 21)
        Me.cmbMapiType.TabIndex = 0
        '
        'grpExchange
        '
        Me.grpExchange.Controls.Add(Me.cmdVerify)
        Me.grpExchange.Controls.Add(Me.txtAlias)
        Me.grpExchange.Controls.Add(Me.Label2)
        Me.grpExchange.Controls.Add(Me.Label3)
        Me.grpExchange.Controls.Add(Me.Label4)
        Me.grpExchange.Controls.Add(Me.txtServer)
        Me.grpExchange.Location = New System.Drawing.Point(8, 48)
        Me.grpExchange.Name = "grpExchange"
        Me.grpExchange.Size = New System.Drawing.Size(272, 152)
        Me.grpExchange.TabIndex = 2
        Me.grpExchange.TabStop = False
        Me.grpExchange.Visible = False
        '
        'cmdVerify
        '
        Me.cmdVerify.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdVerify.Location = New System.Drawing.Point(96, 120)
        Me.cmdVerify.Name = "cmdVerify"
        Me.cmdVerify.Size = New System.Drawing.Size(75, 23)
        Me.cmdVerify.TabIndex = 2
        Me.cmdVerify.Text = "Verify"
        '
        'txtAlias
        '
        Me.txtAlias.Location = New System.Drawing.Point(8, 88)
        Me.txtAlias.Name = "txtAlias"
        Me.txtAlias.Size = New System.Drawing.Size(120, 21)
        Me.txtAlias.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 48)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "To use MAPI with the NT service scheduler, you must provide your exchange server " & _
            "name and MAPI alias"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(104, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "MAPI Alias"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(144, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Exchange Server Name"
        '
        'txtServer
        '
        Me.txtServer.Location = New System.Drawing.Point(144, 88)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(120, 21)
        Me.txtServer.TabIndex = 1
        '
        'cmdOK
        '
        Me.cmdOK.Enabled = False
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(288, 24)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(288, 56)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'frmMAPIType
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(370, 213)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.grpExchange)
        Me.Controls.Add(Me.cmbMapiType)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmMAPIType"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Advanced MAPI Settings"
        Me.grpExchange.ResumeLayout(False)
        Me.grpExchange.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim isCancel As Boolean
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oReg As RegistryKey = Registry.LocalMachine
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Dim MAPIType As String
    Private Sub changeHeight(ByVal newHeight As Integer)
        Dim current As Integer = Me.Height

        If newHeight > current Then
            For I As Integer = current To newHeight
                Me.Height = I
                _Delay(0.00125)
            Next
        Else
            Dim I As Integer = Me.Height

            Do While Me.Height > newHeight
                Me.Height = I
                I -= 1
                _Delay(0.00125)
            Loop
        End If
    End Sub
    Private Sub frmMAPIType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim sMsg As String
        Dim CrLf = Environment.NewLine

        FormatForWinXP(Me)

        sMsg = "To obtain your MAPI alias:" & CrLf & _
        "- Go into into control panels" & CrLf & _
        "- Double-click 'Mail' to open it" & CrLf & _
        "- Click the 'Email Accounts' button" & CrLf & _
        "- Select 'View or change existing email accounts'" & CrLf & _
        "- Click  'Next' button" & CrLf & _
        "- Select your Microsoft Exchange Server account" & CrLf & _
        "- Click  'Change' button" & CrLf & _
        "- Your MAPI alias is in the field called 'User Name'"

        ToolTip1.SetToolTip(txtAlias, sMsg)

        sMsg = "To obtain your Microsoft Exchange Server name:" & CrLf & _
        "- Go into into control panels" & CrLf & _
        "- Double-click 'Mail' to open it" & CrLf & _
        "- Click the 'Email Accounts' button" & CrLf & _
        "- Select 'View or change existing email accounts'" & CrLf & _
        "- Click 'Next' button" & CrLf & _
        "- Select your Microsoft Exchange Server account" & CrLf & _
        "- Click 'Change' button" & CrLf & _
        "- Your MS Exchange server name is in the field called 'Microsoft Exchange Server'"

        ToolTip1.SetToolTip(txtServer, sMsg)

        txtAlias.Text = oUI.ReadRegistry("MAPIAlias", "")
        txtServer.Text = oUI.ReadRegistry("MAPIServer", "")
    End Sub

    Private Sub cmbMapiType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMapiType.SelectedIndexChanged

        If cmbMapiType.Text = "Stand-Alone Outlook" Then
            grpExchange.Visible = False
            'Me.Height = 112
            changeHeight(112)
            MAPIType = "Single"
            cmdOK.Enabled = True
        ElseIf cmbMapiType.Text = "Exchange Server" Then
            grpExchange.Visible = True
            'Me.Height = 224 
            changeHeight(224)
            MAPIType = "Server"
            cmdOK.Enabled = False
        End If
    End Sub

    Private Sub cmdVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdVerify.Click
        On Error Resume Next

        Dim cdoSession As Object 'MAPI.Session

        cdoSession = CreateObject("Redemption.RDOSession") 'New MAPI.Session

        cdoSession.LogonExchangeMailbox(txtAlias.Text, txtServer.Text)
        'nomail:=True, _
        'profileinfo:=txtServer.Text & vbLf & txtAlias.Text)

        If Err.Number = 0 Then
            MessageBox.Show("MAPI test completed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = True
        Else
            MessageBox.Show("MAPI test failed with the following reason:" & vbCrLf & _
                    "Error Message: " & Err.Description & vbCrLf & _
                    "Error Number: " & Err.Number, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
            cmdOK.Enabled = False
        End If
    End Sub

    Public Function MAPISetup() As Boolean
        txtAlias.Text = oUI.ReadRegistry("MAPIAlias", "")
        txtServer.Text = oUI.ReadRegistry("MAPIServer", "")
        MAPIType = oUI.ReadRegistry("MAPIType", "")

        Select Case MAPIType
            Case "Single"
                cmbMapiType.Text = "Stand-Alone Outlook"
                grpExchange.Visible = False
                Me.Height = 112
                cmdOK.Enabled = True
            Case "Server"
                cmbMapiType.Text = "Exchange Server"
                grpExchange.Visible = True
                Me.Height = 224
                cmdOK.Enabled = False
        End Select

        Me.ShowDialog()

        If isCancel = True Then
            isCancel = False
            Return False
            Exit Function
        End If

        oUI.SaveRegistry("MAPIAlias", txtAlias.Text, , , True)
        oUI.SaveRegistry("MAPIServer", txtServer.Text, , , True)
        oUI.SaveRegistry("MAPIType", MAPIType, , , True)

        Return True

        Me.Close()
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        isCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Close()
    End Sub
End Class
