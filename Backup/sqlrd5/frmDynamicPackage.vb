Imports Microsoft.Win32



Public Class frmDynamicPackage
    Inherits System.Windows.Forms.Form
    Dim nStep As Int32
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim ep As New ErrorProvider
    Dim oUI As New clsMarsUI
    Dim oData As New clsMarsData
    Dim sLink As String = String.Empty
    Dim m_PackID As Integer = clsMarsData.CreateDataID("packageattr", "packid")
    Dim reportPars As Hashtable = New Hashtable
    'wizard lables
    Const S1 As String = "Step 1: Package Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Add Reports into Package"
    Const S4 As String = "Step 4: Select Key Parameter"
    Const S5 As String = "Step 5: Linking"
    Const S6 As String = "Step 6: Destination Setup"
    Const S7 As String = "Step 7: Error Handling"
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents chkAutoResume As System.Windows.Forms.CheckBox
    Const S8 As String = "Step 8: Custom Tasks"
    Dim m_serverParametersTableas As DataTable

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents lsvReports As System.Windows.Forms.ListView
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddReport As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveReport As System.Windows.Forms.Button
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents cmdEditReport As System.Windows.Forms.Button
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkMergePDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkMergeXL As System.Windows.Forms.CheckBox
    Friend WithEvents txtMergePDF As System.Windows.Forms.TextBox
    Friend WithEvents txtMergeXL As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As System.Windows.Forms.CheckBox
    Friend WithEvents cmdUp As System.Windows.Forms.Button
    Friend WithEvents cmdDown As System.Windows.Forms.Button
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbDestination As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents chkStaticDest As System.Windows.Forms.CheckBox
    Friend WithEvents grpDynamicDest As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDynamicPackage))
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblStep = New System.Windows.Forms.Label
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.cmbDestination = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.ucSet = New sqlrd.ucScheduleSet
        Me.Step6 = New System.Windows.Forms.Panel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.chkAutoResume = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.chkDTStamp = New System.Windows.Forms.CheckBox
        Me.UcDest = New sqlrd.ucDestination
        Me.Step3 = New System.Windows.Forms.Panel
        Me.cmdUp = New System.Windows.Forms.Button
        Me.cmdDown = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtMergePDF = New System.Windows.Forms.TextBox
        Me.chkMergeXL = New System.Windows.Forms.CheckBox
        Me.chkMergePDF = New System.Windows.Forms.CheckBox
        Me.txtMergeXL = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmdAddReport = New System.Windows.Forms.Button
        Me.lsvReports = New System.Windows.Forms.ListView
        Me.ReportName = New System.Windows.Forms.ColumnHeader
        Me.Format = New System.Windows.Forms.ColumnHeader
        Me.cmdRemoveReport = New System.Windows.Forms.Button
        Me.cmdEditReport = New System.Windows.Forms.Button
        Me.Step7 = New System.Windows.Forms.Panel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.cmdNext = New System.Windows.Forms.Button
        Me.Step8 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optOnce = New System.Windows.Forms.RadioButton
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.oTask = New sqlrd.ucTasks
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.Step4 = New System.Windows.Forms.Panel
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.optDynamic = New System.Windows.Forms.RadioButton
        Me.optStatic = New System.Windows.Forms.RadioButton
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Step5 = New System.Windows.Forms.Panel
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.chkStaticDest = New System.Windows.Forms.CheckBox
        Me.grpDynamicDest = New System.Windows.Forms.GroupBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmdTest = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.cmbTable = New System.Windows.Forms.ComboBox
        Me.Footer1 = New WizardFooter.Footer
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Step7.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.grpDynamicDest.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(376, 462)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(400, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(288, 462)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(200, 462)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "MS Access Database|*.mdb|All Files|*.*"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdLoc.ForeColor = System.Drawing.Color.Navy
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(328, 80)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 80)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Package Name"
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(416, 32)
        Me.lblStep.TabIndex = 2
        Me.lblStep.Text = "Step 1: Package Setup"
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.SystemColors.Control
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Step1.ForeColor = System.Drawing.Color.Navy
        Me.Step1.Location = New System.Drawing.Point(0, 72)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(456, 366)
        Me.Step1.TabIndex = 0
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(8, 128)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(376, 88)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 224)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 240)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.cmbDestination)
        Me.Step2.Controls.Add(Me.Label9)
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Location = New System.Drawing.Point(0, 72)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(456, 366)
        Me.Step2.TabIndex = 0
        Me.Step2.Visible = False
        '
        'cmbDestination
        '
        Me.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestination.ItemHeight = 13
        Me.cmbDestination.Items.AddRange(New Object() {"Disk", "Email", "Fax", "FTP", "Printer", "SMS"})
        Me.cmbDestination.Location = New System.Drawing.Point(128, 5)
        Me.cmbDestination.Name = "cmbDestination"
        Me.cmbDestination.Size = New System.Drawing.Size(160, 21)
        Me.cmbDestination.Sorted = True
        Me.cmbDestination.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(16, 9)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(61, 13)
        Me.Label9.TabIndex = 3
        Me.Label9.Text = "Destination"
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(8, 24)
        Me.ucSet.m_RepeatUnit = ""
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(440, 342)
        Me.ucSet.TabIndex = 1
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.SystemColors.Control
        Me.Step6.Controls.Add(Me.GroupBox2)
        Me.Step6.Controls.Add(Me.GroupBox5)
        Me.Step6.Controls.Add(Me.UcDest)
        Me.Step6.Location = New System.Drawing.Point(0, 72)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(456, 366)
        Me.Step6.TabIndex = 0
        Me.Step6.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtCacheExpiry)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.chkAutoResume)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 264)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(440, 74)
        Me.GroupBox2.TabIndex = 18
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resume with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(213, 41)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 2
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(257, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "minutes"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 45)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(168, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Expire dynamic cached data after"
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        Me.chkAutoResume.Location = New System.Drawing.Point(6, 20)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(263, 17)
        Me.chkAutoResume.TabIndex = 0
        Me.chkAutoResume.Text = "Resume failed/errored schedule with cached data"
        Me.ToolTip1.SetToolTip(Me.chkAutoResume, "If the schedule fails mid-flight, next time it runs, it will resume from where it" & _
                " left off instead of starting from the beginning")
        Me.chkAutoResume.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cmbDateTime)
        Me.GroupBox5.Controls.Add(Me.chkDTStamp)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 211)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(440, 48)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Date/Time Stamp"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(232, 16)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(152, 21)
        Me.cmbDateTime.TabIndex = 1
        '
        'chkDTStamp
        '
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(16, 16)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 0
        Me.chkDTStamp.Text = "Append date/time stamp"
        '
        'UcDest
        '
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(8, 8)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(440, 202)
        Me.UcDest.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.SystemColors.Control
        Me.Step3.Controls.Add(Me.cmdUp)
        Me.Step3.Controls.Add(Me.cmdDown)
        Me.Step3.Controls.Add(Me.GroupBox3)
        Me.Step3.Controls.Add(Me.cmdAddReport)
        Me.Step3.Controls.Add(Me.lsvReports)
        Me.Step3.Controls.Add(Me.cmdRemoveReport)
        Me.Step3.Controls.Add(Me.cmdEditReport)
        Me.Step3.Location = New System.Drawing.Point(0, 72)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(456, 366)
        Me.Step3.TabIndex = 0
        Me.Step3.Visible = False
        '
        'cmdUp
        '
        Me.cmdUp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(376, 176)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 4
        Me.cmdUp.Text = "&Up"
        Me.cmdUp.UseVisualStyleBackColor = False
        '
        'cmdDown
        '
        Me.cmdDown.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(376, 208)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 5
        Me.cmdDown.Text = "&Down"
        Me.cmdDown.UseVisualStyleBackColor = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.txtMergePDF)
        Me.GroupBox3.Controls.Add(Me.chkMergeXL)
        Me.GroupBox3.Controls.Add(Me.chkMergePDF)
        Me.GroupBox3.Controls.Add(Me.txtMergeXL)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 240)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(440, 104)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Merging"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(384, 34)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = ".pdf"
        '
        'txtMergePDF
        '
        Me.txtMergePDF.Enabled = False
        Me.txtMergePDF.Location = New System.Drawing.Point(232, 32)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.txtMergePDF.Size = New System.Drawing.Size(144, 21)
        Me.txtMergePDF.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.txtMergePDF, "Enter the resulting PDF file name")
        '
        'chkMergeXL
        '
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(8, 64)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.chkMergeXL.Size = New System.Drawing.Size(208, 32)
        Me.chkMergeXL.TabIndex = 2
        Me.chkMergeXL.Text = "Merge all Excel outputs into a single workbook"
        Me.ToolTip1.SetToolTip(Me.chkMergeXL, "All reports in Excel format will be merged into a single Excel workbook")
        '
        'chkMergePDF
        '
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(8, 24)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.chkMergePDF.Size = New System.Drawing.Size(208, 32)
        Me.chkMergePDF.TabIndex = 0
        Me.chkMergePDF.Text = "Merge all PDF outputs into a single PDF file"
        Me.ToolTip1.SetToolTip(Me.chkMergePDF, "All reports in PDF format will be merged into a single PDF file")
        '
        'txtMergeXL
        '
        Me.txtMergeXL.Enabled = False
        Me.txtMergeXL.Location = New System.Drawing.Point(232, 70)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.txtMergeXL.Size = New System.Drawing.Size(144, 21)
        Me.txtMergeXL.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.txtMergeXL, "Enter the resulting PDF file name")
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(384, 72)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = ".xls"
        '
        'cmdAddReport
        '
        Me.cmdAddReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddReport.Image = CType(resources.GetObject("cmdAddReport.Image"), System.Drawing.Image)
        Me.cmdAddReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(376, 8)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 0
        Me.cmdAddReport.Text = "&Add"
        Me.cmdAddReport.UseVisualStyleBackColor = False
        '
        'lsvReports
        '
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(8, 8)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.lsvReports.Size = New System.Drawing.Size(360, 224)
        Me.lsvReports.TabIndex = 1
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 150
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveReport.Image = CType(resources.GetObject("cmdRemoveReport.Image"), System.Drawing.Image)
        Me.cmdRemoveReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(376, 72)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 3
        Me.cmdRemoveReport.Text = "&Remove"
        Me.cmdRemoveReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveReport.UseVisualStyleBackColor = False
        '
        'cmdEditReport
        '
        Me.cmdEditReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEditReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEditReport.Image = CType(resources.GetObject("cmdEditReport.Image"), System.Drawing.Image)
        Me.cmdEditReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(376, 40)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdEditReport.TabIndex = 2
        Me.cmdEditReport.Text = "&Edit"
        Me.cmdEditReport.UseVisualStyleBackColor = False
        '
        'Step7
        '
        Me.Step7.BackColor = System.Drawing.SystemColors.Control
        Me.Step7.Controls.Add(Me.UcError)
        Me.Step7.Controls.Add(Me.UcBlank)
        Me.Step7.Location = New System.Drawing.Point(0, 72)
        Me.Step7.Name = "Step7"
        Me.Step7.Size = New System.Drawing.Size(456, 366)
        Me.Step7.TabIndex = 0
        Me.Step7.Visible = False
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(8, 9)
        Me.UcError.m_showFailOnOne = True
        Me.UcError.MinimumSize = New System.Drawing.Size(0, 93)
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(445, 93)
        Me.UcError.TabIndex = 3
        '
        'UcBlank
        '
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlank.Location = New System.Drawing.Point(14, 107)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(434, 237)
        Me.UcBlank.TabIndex = 1
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(376, 462)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'Step8
        '
        Me.Step8.BackColor = System.Drawing.SystemColors.Control
        Me.Step8.Controls.Add(Me.GroupBox1)
        Me.Step8.Controls.Add(Me.oTask)
        Me.Step8.Location = New System.Drawing.Point(0, 72)
        Me.Step8.Name = "Step8"
        Me.Step8.Size = New System.Drawing.Size(456, 366)
        Me.Step8.TabIndex = 0
        Me.Step8.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optOnce)
        Me.GroupBox1.Controls.Add(Me.optAll)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 282)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(414, 74)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 1
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 0
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.SystemColors.Control
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.oTask.Location = New System.Drawing.Point(8, 8)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.Name = "oTask"
        Me.oTask.Size = New System.Drawing.Size(440, 268)
        Me.oTask.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(458, 64)
        Me.Panel1.TabIndex = 21
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'Step4
        '
        Me.Step4.Controls.Add(Me.GroupBox7)
        Me.Step4.Location = New System.Drawing.Point(0, 72)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(456, 366)
        Me.Step4.TabIndex = 0
        Me.Step4.Visible = False
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.optDynamic)
        Me.GroupBox7.Controls.Add(Me.optStatic)
        Me.GroupBox7.Controls.Add(Me.cmbKey)
        Me.GroupBox7.Controls.Add(Me.Label11)
        Me.GroupBox7.Controls.Add(Me.Label6)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(432, 346)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Parameters"
        '
        'optDynamic
        '
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(8, 104)
        Me.optDynamic.Name = "optDynamic"
        Me.optDynamic.Size = New System.Drawing.Size(352, 24)
        Me.optDynamic.TabIndex = 2
        Me.optDynamic.Text = "Populate key parameter with data from a database using a query"
        '
        'optStatic
        '
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(8, 80)
        Me.optStatic.Name = "optStatic"
        Me.optStatic.Size = New System.Drawing.Size(224, 16)
        Me.optStatic.TabIndex = 1
        Me.optStatic.Text = "Populate key parameter with static data"
        '
        'cmbKey
        '
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(8, 48)
        Me.cmbKey.Name = "cmbKey"
        Me.cmbKey.Size = New System.Drawing.Size(232, 21)
        Me.cmbKey.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(416, 32)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Please select the report parameter  that will be used to look up information from" & _
            " your database"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Info
        Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(248, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(176, 32)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Applies to all reports in the package"
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.SystemColors.Control
        Me.Step5.Controls.Add(Me.GroupBox6)
        Me.Step5.Location = New System.Drawing.Point(0, 72)
        Me.Step5.Name = "Step5"
        Me.Step5.Size = New System.Drawing.Size(456, 366)
        Me.Step5.TabIndex = 0
        Me.Step5.Visible = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.chkStaticDest)
        Me.GroupBox6.Controls.Add(Me.grpDynamicDest)
        Me.GroupBox6.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox6.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(432, 354)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        '
        'chkStaticDest
        '
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(8, 16)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 0
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        '
        'grpDynamicDest
        '
        Me.grpDynamicDest.Controls.Add(Me.Label10)
        Me.grpDynamicDest.Controls.Add(Me.cmdValidate)
        Me.grpDynamicDest.Controls.Add(Me.UcDSN)
        Me.grpDynamicDest.Controls.Add(Me.cmbColumn)
        Me.grpDynamicDest.Controls.Add(Me.cmbValue)
        Me.grpDynamicDest.Controls.Add(Me.Label15)
        Me.grpDynamicDest.Controls.Add(Me.cmdTest)
        Me.grpDynamicDest.Controls.Add(Me.Label13)
        Me.grpDynamicDest.Controls.Add(Me.cmbTable)
        Me.grpDynamicDest.Location = New System.Drawing.Point(8, 40)
        Me.grpDynamicDest.Name = "grpDynamicDest"
        Me.grpDynamicDest.Size = New System.Drawing.Size(416, 304)
        Me.grpDynamicDest.TabIndex = 1
        Me.grpDynamicDest.TabStop = False
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(384, 32)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Please specify the table and column that will be used to gather the xxx from the " & _
            "database"
        '
        'cmdValidate
        '
        Me.cmdValidate.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 176)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 56)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 112)
        Me.UcDSN.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 216)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 3
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 264)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 248)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(360, 16)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Please select the column that holds the xxx"
        '
        'cmdTest
        '
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(272, 264)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 21)
        Me.cmdTest.TabIndex = 5
        Me.cmdTest.Text = "Test"
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 200)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(392, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Please select the table and column that must equal the key parameter"
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 216)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 2
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 444)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(472, 16)
        Me.Footer1.TabIndex = 26
        Me.Footer1.TabStop = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(458, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 27
        '
        'frmDynamicPackage
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(458, 494)
        Me.ControlBox = False
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step7)
        Me.Controls.Add(Me.Step8)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmDynamicPackage"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dynamic Package Schedule Wizard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.Step2.PerformLayout()
        Me.Step6.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Step7.ResumeLayout(False)
        Me.Step8.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.grpDynamicDest.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If

    End Sub



    Private Sub frmPackWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s2_DynamicSched) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
            Close()
            Return
        End If

        If _CheckScheduleCount() = False Then Return

        Dim I As Int32 = 0

        nStep = 1

        ucSet.cmbRpt.Text = 0

        UcDest.isPackage = True
        UcDest.isDynamic = True
        UcDest.nSmartID = 0
        UcDest.nPackID = Me.m_PackID

        ucSet.EndDate.Value = Now.AddYears(100)

        FormatForWinXP(Me)

        lblStep.Text = S1

        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        End If

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If

        Dim oData As New clsMarsData

        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        Step4.Visible = False
        Step5.Visible = False
        Step6.Visible = False
        Step7.Visible = False
        Step8.Visible = False

        Step1.BringToFront()
        txtName.Focus()


        txtMergePDF.ContextMenu = Me.mnuInserter
        txtMergeXL.ContextMenu = Me.mnuInserter
        oData.CleanDB()
    End Sub


    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub


    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 8
                Step8.Visible = False
                Step7.Visible = True
                Step7.BringToFront()
                lblStep.Text = S7
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Me.AcceptButton = cmdNext
            Case 7
                Step7.Visible = False
                Step6.Visible = True
                Step6.BringToFront()
                lblStep.Text = S6
            Case 6
                Step6.Visible = False
                Step5.Visible = True
                Step5.BringToFront()
                lblStep.Text = S5
            Case 5
                Step5.Visible = False
                Step4.Visible = True
                Step4.BringToFront()
                lblStep.Text = S4

            Case 4
                Step4.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                lblStep.Text = S3
            Case 3
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                lblStep.Text = S2
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                cmdBack.Enabled = False
                lblStep.Text = S1
            Case Else
                Exit Sub
        End Select

        nStep -= 1
    End Sub
    Public Sub _GetParameters(ByVal cmb As ComboBox)
        cmbKey.Items.Clear()

        Dim pars As New ArrayList

        For Each de As DictionaryEntry In Me.reportPars
            cmbKey.Items.Add(de.Key)
            pars.Add(de.Key)
        Next

        UcBlank.m_ParametersList = pars
    End Sub
    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case nStep
            Case 1
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE, , True) = False Then
                    ErrProv.SetError(txtName, "A package schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                Step1.Visible = False
                Step2.Visible = True
                cmbDestination.Focus()
                lblStep.Text = S2
                cmdBack.Enabled = True
            Case 2
               
                If ucSet.Validate = False Then
                    Return
                ElseIf cmbDestination.Text.Length = 0 Then
                    ep.SetError(cmbDestination, "Please select the schedule destination.")
                    cmbDestination.Focus()
                    Return
                End If

                Select Case cmbDestination.Text
                    Case "Email"
                        Label10.Text = Label10.Text.Replace("xxx", "email address")
                        Label9.Text = Label9.Text.Replace("xxx", "email address")
                        Label15.Text = Label15.Text.Replace("xxx", "email address")
                    Case "Printer"
                        Label10.Text = Label10.Text.Replace("xxx", "printer")
                        Label9.Text = Label9.Text.Replace("xxx", "printer")
                        Label15.Text = Label15.Text.Replace("xxx", "printer")
                    Case "Disk"
                        Label10.Text = Label10.Text.Replace("xxx", "directory")
                        Label9.Text = Label9.Text.Replace("xxx", "directory")
                        Label15.Text = Label15.Text.Replace("xxx", "directory")
                    Case "FTP"
                        Label10.Text = Label10.Text.Replace("xxx", "FTP server")
                        Label9.Text = Label9.Text.Replace("xxx", "FTP server")
                        Label15.Text = Label15.Text.Replace("xxx", "FTP server")
                    Case "Fax"
                        Label10.Text = Label10.Text.Replace("xxx", "fax number")
                        Label9.Text = Label9.Text.Replace("xxx", "fax number")
                        Label15.Text = Label15.Text.Replace("xxx", "fax number")
                    Case "SMS"
                        Label10.Text = Label10.Text.Replace("xxx", "cell phone number")
                        Label9.Text = Label9.Text.Replace("xxx", "cell phone number")
                        Label15.Text = Label15.Text.Replace("xxx", "cell phone number")
                End Select

                Step2.Visible = False
                Step3.Visible = True
                Me.cmdAddReport.Focus()
                lblStep.Text = S3
            Case 3
                If lsvReports.Items.Count = 0 Then
                    ep.SetError(lsvReports, "Please add at least one report for the dynamic package")
                    lsvReports.Focus()
                    Return
                ElseIf chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
                    ErrProv.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
                    txtMergePDF.Focus()
                    Return
                ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
                    ErrProv.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
                    txtMergeXL.Focus()
                    Return
                ElseIf chkDTStamp.Checked = True And cmbDateTime.Text.Length = 0 Then
                    ErrProv.SetError(cmbDateTime, "Please select the datetime stamp")
                    cmbDateTime.Focus()
                    Return
                End If

                _GetParameters(cmbKey)

                Step3.Visible = False
                Step4.Visible = True
                cmbKey.Focus()
                lblStep.Text = S4
            Case 4
                If cmbKey.Text.Length = 0 Then
                    ep.SetError(cmbKey, "Please select the key parameter")
                    cmbKey.Focus()
                    Return
                ElseIf optStatic.Checked = False And optDynamic.Checked = False Then
                    ep.SetError(optStatic, "Please select how the key parameter is populated")
                    Return
                End If

                Step4.Visible = False
                Step5.Visible = True
                chkStaticDest.Focus()
                lblStep.Text = S5

            Case 5
                If chkStaticDest.Checked = False Then
                    If cmbTable.Text.Length = 0 Then
                        ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                        cmbTable.Focus()
                        Return
                    ElseIf cmbColumn.Text.Length = 0 Then
                        ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                        cmbColumn.Focus()
                        Return
                    ElseIf cmbValue.Text.Length = 0 Then
                        ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                        cmbValue.Focus()
                        Return
                    ElseIf cmbValue.Text = "<Advanced>" And sLink = String.Empty Then
                        ep.SetError(cmbValue, "Please set up the query for the value")
                        cmbValue.Focus()
                        Return
                    End If
                End If

                Step5.Visible = False
                Step6.Visible = True
                UcDest.Focus()
                lblStep.Text = S6

                UcDest.cmdAdd_Click(sender, e)
            Case 6
                If UcDest.lsvDestination.Items.Count = 0 Then
                    ep.SetError(UcDest.lsvDestination, "Please add a destination for the schedule")
                    Return
                ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
                    ep.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
                    Me.cmbDateTime.Focus()
                    Return
                End If

                Step6.Visible = False
                Step7.Visible = True
                UcError.cmbRetry.Focus()
                lblStep.Text = S7

            Case 7
                If UcBlank.chkBlankReport.Checked = True Then
                    If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                    UcBlank.txtAlertTo.Text.Length = 0 Then
                        UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please enter the alert recipient")
                        UcBlank.txtAlertTo.Focus()
                        Return
                    End If
                End If

                Step7.Visible = False
                Step8.Visible = True
                oTask.Focus()
                lblStep.Text = S8
                cmdNext.Visible = False
                cmdFinish.Visible = True
                Me.AcceptButton = cmdFinish
            Case Else
                Exit Sub
        End Select
        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        Dim sReturn(2) As String
        Dim oNewReport As frmPackedReport


        oNewReport = New frmPackedReport

        Dim tmpTable As DataTable

        sReturn = oNewReport.AddReport(lsvReports.Items.Count + 1, Me.m_PackID, Me.reportPars, tmpTable)

        Try
            If Me.m_serverParametersTableas Is Nothing Then
                Me.m_serverParametersTableas = tmpTable
            Else
                Dim newT As DataTable = tmpTable
                Dim newR As DataRow

                For Each row As DataRow In newT.Rows
                    newR = Me.m_serverParametersTableas.Rows.Add

                    newR("Name") = row("Name")
                    newR("Type") = row("Type")
                    newR("MultiValue") = row("MultiValue")
                    newR("Label") = row("Label")
                    newR("Value") = row("Value")
                Next
            End If
        Catch : End Try

        If sReturn(0) = "" Then Exit Sub

        Dim lsvItem As ListViewItem = New ListViewItem
        Dim lsvSub As ListViewItem.ListViewSubItem = New ListViewItem.ListViewSubItem

        lsvItem.Text = sReturn(0)
        lsvItem.Tag = sReturn(1)
        lsvSub.Text = sReturn(2)

        lsvItem.SubItems.Add(lsvSub)

        lsvReports.Items.Add(lsvItem)

    End Sub



    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nPackID As Integer
        Dim WriteSuccess As Boolean
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim I As Integer = 1
        Dim dynamicTasks As Integer
        'save the package

        oErr.BusyProgress(10, "Saving package data...")

        cmdFinish.Enabled = False

        nPackID = Me.m_PackID 'clsMarsData.CreateDataID

        If optAll.Checked Then
            dynamicTasks = 0
        Else
            dynamicTasks = 1
        End If

        SQL = "INSERT INTO PackageAttr(PackID,PackageName,Parent,Retry,AssumeFail," & _
            "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName," & _
            "MergeXLName,DateTimeStamp,StampFormat,Dynamic,StaticDest,DynamicTasks,AutoCalc,RetryInterval) VALUES(" & _
            nPackID & "," & _
            "'" & txtName.Text & "'," & _
            txtFolder.Tag & "," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter(True) & "," & _
            Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
            "'" & gUser & "'," & _
            Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
            Convert.ToInt32(chkMergePDF.Checked) & "," & _
            Convert.ToInt32(chkMergeXL.Checked) & "," & _
            "'" & SQLPrepare(txtMergePDF.Text) & "'," & _
            "'" & SQLPrepare(txtMergeXL.Text) & "'," & _
            Convert.ToInt32(chkDTStamp.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "',1," & _
            Convert.ToInt32(Me.chkStaticDest.Checked) & "," & _
            dynamicTasks & "," & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value & ")"

        If clsMarsData.WriteData(SQL) = False Then Exit Sub

        oErr.BusyProgress(30, "Saving schedule data...")

        'save the schedule
        Dim ScheduleID As Int64 = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        With ucSet
            SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status," & _
                "PackID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                "VALUES(" & _
                ScheduleID & "," & _
                "'" & .sFrequency & "'," & _
                "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                "'" & ConDate(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                "'" & CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "'" & Convert.ToString(.cmbRpt.Text).Replace(",", ".") & "'," & _
                "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.chkStatus.Checked) & "," & _
                nPackID & "," & _
                "'" & SQLPrepare(txtDesc.Text) & "'," & _
                "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                Convert.ToInt32(.chkException.Checked) & "," & _
                "'" & SQLPrepare(.cmbException.Text) & "'," & _
                "'" & ucSet.m_RepeatUnit & "')"
        End With

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
            "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        Me.oTask.CommitDeletions()

        'save the destination

        oErr.BusyProgress(50, "Saving destination data...")

        SQL = "UPDATE DestinationAttr SET ReportID =0, SmartID =0, PackID = " & nPackID & " WHERE PackID = " & Me.m_PackID

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE PackageOptions SET PackID =" & nPackID & " WHERE PackID  = " & Me.m_PackID)

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        oErr.BusyProgress(50, "Saving printers...")

        'save the tasks

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        'save the blank report alert information
        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            oErr.BusyProgress(70, "Saving error handling data...")

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,PackID,Subject,CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
            "'" & sBlankType & "'," & _
            "'" & UcBlank.txtAlertTo.Text & "'," & _
            "'" & UcBlank.txtBlankMsg.Text & "'," & _
            nPackID & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        oErr.BusyProgress(90, "Saving reports data...")

        'update the report for this package
        'SQL = "UPDATE ReportAttr SET PackID = " & nPackID & " WHERE PackID  = " & Me.m_PackID

        'If clsMarsData.WriteData(SQL) = False Then GoTo RollBackTransaction

        SQL = "UPDATE PackagedReportAttr SET PackID = " & nPackID & " WHERE PackID  = " & Me.m_PackID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID =0 WHERE DestinationID =99999")

        'for dynamic stuffings
        SQL = "UPDATE DynamicAttr SET PackID = " & nPackID & ", ReportID = 0," & _
        "AutoResume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
        "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
        " WHERE PackID = " & 99999

        clsMarsData.WriteData(SQL)

        oErr.BusyProgress(95, "Saving dynamic queries...")

        If cmbValue.Text = "<Advanced>" Then
            SQL = "UPDATE DynamicLink SET PackID = " & nPackID & ", ReportID = 0 WHERE PackID  = " & 99999
        Else

            Dim sCols, sVals As String

            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE PackID = " & 99999)

            sLink = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & _
                " FROM " & cmbTable.Text

            sCols = "LinkID,PackID,LinkSQL,ConString,KeyColumn,KeyParameter,ValueColumn,ReportID"

            sVals = clsMarsData.CreateDataID("dynamiclink", "linkid") & "," & _
            nPackID & "," & _
            "'" & SQLPrepare(sLink) & "'," & _
            "'" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
            "'" & SQLPrepare(cmbKey.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbValue.Text) & "',0"

            SQL = "INSERT INTO DynamicLink (" & sCols & ") VALUES (" & sVals & ")"

        End If

        clsMarsData.WriteData(SQL)

        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nPackID, gUser, clsMarsUsers.enViewType.ViewPackage)
        End If

        oErr.BusyProgress(, , True)

        On Error Resume Next

        oErr.RefreshView(oWindow(nWindowCurrent))

        On Error Resume Next

        Dim nCount As Integer

        nCount = txtFolder.Text.Split("\").GetUpperBound(0)

        Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

        oErr.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))

        Me.Close()
        Exit Sub
RollBackTransaction:
        oErr.BusyProgress(, , True)
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE PackID =" & nPackID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Dim oData As clsMarsData = New clsMarsData

        oData.CleanDB(, Me.m_PackID)

        Dim SQL As String
        Try
            For Each oitem As ListViewItem In lsvReports.Items
                SQL = "DELETE FROM ReportParameter WHERE ReportID =" & oitem.Tag

                clsMarsData.WriteData(SQL)
            Next
        Catch
        End Try

        Me.Close()
    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As ListViewItem = lsvReports.SelectedItems(0)

        nReportID = lsv.Tag

        Dim SQL As String = "DELETE FROM ReportAttr WHERE PackID = " & Me.m_PackID & " AND " & _
                "ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ReportParameter WHERE ReportID=" & nReportID

        clsMarsData.WriteData(SQL)

        lsv.Remove()
    End Sub


    

    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmPackedReport
        Dim sVals() As String

        sVals = oEdit.EditReport(lsvReports.SelectedItems(0).Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)

            oItem.SubItems(1).Text = sVals(1)

            lsvReports.Refresh()
        End If


    End Sub

    Private Sub lsvReports_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvReports.SelectedIndexChanged

    End Sub

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        cmdEditReport_Click(sender, e)
    End Sub

    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergePDF.Checked = False
            Return
        End If

        txtMergePDF.Enabled = chkMergePDF.Checked

        If chkMergePDF.Checked = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("PDF", Me.m_PackID)
        End If

    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.xl1_AdvancedXLPack) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("XLS", Me.m_PackID)
        End If
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergePDF.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeXL.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
        Else
            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")
        End If
    End Sub


    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up
        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = " & Me.m_PackID & " ORDER BY PackOrderID"


        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = " & Me.m_PackID & " ORDER BY PackOrderID"



        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub




    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""
        Dim oRpt As Object
        Dim Found As Boolean = False

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        Dim oVal As New frmKeyParameter

        sVal = oVal._GetParameterValue(cmbKey.Text, , Me.reportPars(cmbKey.Text), Me.m_serverParametersTableas)

        If sVal.Length = 0 Then Return

        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE PackID=99999")

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
        "Query,PackID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(sVal) & "'," & _
        "'_'," & _
        "'_'," & _
        99999

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub



    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "package", , Me.m_PackID)

        If sParameter.Length = 0 Then Return

    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1226)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
               UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        gTables(0) = cmbTable.Text

        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        gsCon = UcDSN.cmbDSN.Text & "|" & _
        UcDSN.txtUserID.Text & "|" & _
        UcDSN.txtPassword.Text & "|"

        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            sLink = oForm._AdvancedDynamicLinking(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmbDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDestination.SelectedIndexChanged
        If cmbDestination.Text.ToLower = "sms" And gnEdition < gEdition.ENTERPRISEPROPLUS Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            cmbDestination.SelectedIndex = 0
            Return
        End If

        UcDest.DynamicDestination = cmbDestination.Text
    End Sub



    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        grpDynamicDest.Enabled = Not chkStaticDest.Checked
        UcDest.StaticDest = chkStaticDest.Checked
        UcDest.m_CanDisable = chkStaticDest.Checked
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter")

        If ParValue.Length = 0 Then Return

        If Not ParValue.StartsWith("'") And Not ParValue.EndsWith("'") Then
            Try
                Int32.Parse(ParValue)
            Catch ex As Exception
                ParValue = "'" & SQLPrepare(ParValue) & "'"
            End Try
        End If

        Try
            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE PackID= " & Me.m_PackID

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)
            Else
                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text & _
                    " WHERE " & cmbColumn.Text & " = " & ParValue

                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

            End If

            oRs = New ADODB.Recordset

            Dim oLinkCon As New ADODB.Connection

            oLinkCon.Open(sDSN, sUser, sPassword)

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Undo()

    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim intruder As frmInserter = New frmInserter(99999)

        intruder.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim getData As frmDataItems = New frmDataItems

        Dim ctrl As TextBox = CType(Me.ActiveControl, TextBox)

        ctrl.Text = getData._GetDataItem(99999)
    End Sub

    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged

    End Sub

    Private Sub cmbDateTime_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbDateTime.Validating
        For Each s As String In IO.Path.GetInvalidFileNameChars
            cmbDateTime.Text = cmbDateTime.Text.Replace(s, "")
        Next
    End Sub
End Class
