#If CRYSTAL_VER = 8 Then
    imports My.Crystal85

#ElseIf CRYSTAL_VER = 9 Then
    imports My.Crystal9 

#ElseIf CRYSTAL_VER = 10 Then
    imports My.Crystal10

#ElseIf CRYSTAL_VER = 11 Or CRYSTAL_VER = 11.5 Or CRYSTAL_VER = 12 Then
Imports My.Crystal11
#End If
Public Class frmInserter
    Inherits System.Windows.Forms.Form
    Public sCon As String
    Public sTables() As String
    Dim oData As New clsMarsData
    Dim ep As New ErrorProvider
    Public UserCancel As Boolean
    Private HidePars As Boolean = False
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.TableLayoutPanel
    Dim HideUserConstants As Boolean = False
    Dim eventBased As Boolean = False
    Friend WithEvents Panel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtCustom As System.Windows.Forms.TextBox
    Dim eventID As Integer = 99999
    Friend WithEvents Panel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbDateFormat As System.Windows.Forms.ComboBox
    Dim mode As e_Mode
    Dim m_Container As ContainerControl
    Friend WithEvents pnMsgFormat As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents optText As System.Windows.Forms.RadioButton
    Friend WithEvents optHTML As System.Windows.Forms.RadioButton
    Friend WithEvents txtIndex As System.Windows.Forms.NumericUpDown
    Friend WithEvents pnAttachIndex As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents tvInserter As System.Windows.Forms.TreeView
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Public m_ParameterList As ArrayList = Nothing
    Dim Dialog As Boolean = False


#Region " Windows Form Designer generated code "

    Public Sub New(ByVal m_EventID As Integer)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        eventID = m_EventID
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbType As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbName As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSelect As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInserter))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.pnAttachIndex = New System.Windows.Forms.FlowLayoutPanel
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtIndex = New System.Windows.Forms.NumericUpDown
        Me.pnMsgFormat = New System.Windows.Forms.FlowLayoutPanel
        Me.optText = New System.Windows.Forms.RadioButton
        Me.optHTML = New System.Windows.Forms.RadioButton
        Me.Panel3 = New System.Windows.Forms.TableLayoutPanel
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbDateFormat = New System.Windows.Forms.ComboBox
        Me.Panel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtCustom = New System.Windows.Forms.TextBox
        Me.Panel1 = New System.Windows.Forms.TableLayoutPanel
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbType = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbName = New System.Windows.Forms.ComboBox
        Me.cmdSelect = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.tvInserter = New System.Windows.Forms.TreeView
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.pnAttachIndex.SuspendLayout()
        CType(Me.txtIndex, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnMsgFormat.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Controls.Add(Me.pnAttachIndex)
        Me.GroupBox1.Controls.Add(Me.pnMsgFormat)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmbType)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmbName)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(336, 146)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'pnAttachIndex
        '
        Me.pnAttachIndex.Controls.Add(Me.Label5)
        Me.pnAttachIndex.Controls.Add(Me.txtIndex)
        Me.pnAttachIndex.Location = New System.Drawing.Point(64, 75)
        Me.pnAttachIndex.Name = "pnAttachIndex"
        Me.pnAttachIndex.Size = New System.Drawing.Size(156, 30)
        Me.pnAttachIndex.TabIndex = 5
        Me.pnAttachIndex.Visible = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(94, 27)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Attachment Index"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtIndex
        '
        Me.txtIndex.Location = New System.Drawing.Point(103, 3)
        Me.txtIndex.Minimum = New Decimal(New Integer() {1, 0, 0, -2147483648})
        Me.txtIndex.Name = "txtIndex"
        Me.txtIndex.Size = New System.Drawing.Size(44, 21)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtIndex, New DevComponents.DotNetBar.SuperTooltipInfo("The position of the attachment in the email", "", resources.GetString("txtIndex.SuperTooltip"), Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.txtIndex.TabIndex = 4
        Me.txtIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtIndex.Value = New Decimal(New Integer() {1, 0, 0, -2147483648})
        '
        'pnMsgFormat
        '
        Me.pnMsgFormat.Controls.Add(Me.optText)
        Me.pnMsgFormat.Controls.Add(Me.optHTML)
        Me.pnMsgFormat.Location = New System.Drawing.Point(64, 75)
        Me.pnMsgFormat.Name = "pnMsgFormat"
        Me.pnMsgFormat.Size = New System.Drawing.Size(200, 24)
        Me.pnMsgFormat.TabIndex = 7
        Me.pnMsgFormat.Visible = False
        '
        'optText
        '
        Me.optText.AutoSize = True
        Me.optText.Location = New System.Drawing.Point(3, 3)
        Me.optText.Name = "optText"
        Me.optText.Size = New System.Drawing.Size(49, 17)
        Me.optText.TabIndex = 1
        Me.optText.Text = "TEXT"
        Me.optText.UseVisualStyleBackColor = True
        '
        'optHTML
        '
        Me.optHTML.AutoSize = True
        Me.optHTML.Location = New System.Drawing.Point(58, 3)
        Me.optHTML.Name = "optHTML"
        Me.optHTML.Size = New System.Drawing.Size(51, 17)
        Me.optHTML.TabIndex = 0
        Me.optHTML.TabStop = True
        Me.optHTML.Text = "HTML"
        Me.optHTML.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.ColumnCount = 2
        Me.Panel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.Panel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.Panel3.Controls.Add(Me.Label4, 0, 0)
        Me.Panel3.Controls.Add(Me.cmbDateFormat, 1, 0)
        Me.Panel3.Location = New System.Drawing.Point(64, 109)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.RowCount = 1
        Me.Panel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.Panel3.Size = New System.Drawing.Size(245, 28)
        Me.Panel3.TabIndex = 9
        Me.Panel3.Visible = False
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(101, 28)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Date/Time Format"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbDateFormat
        '
        Me.cmbDateFormat.FormattingEnabled = True
        Me.cmbDateFormat.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "hh:mm:ss", "HHmm", "HHmmss", "MM", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "MMMM", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyy-MM-dd", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss"})
        Me.cmbDateFormat.Location = New System.Drawing.Point(110, 3)
        Me.cmbDateFormat.Name = "cmbDateFormat"
        Me.cmbDateFormat.Size = New System.Drawing.Size(135, 21)
        Me.cmbDateFormat.Sorted = True
        Me.cmbDateFormat.TabIndex = 9
        '
        'Panel2
        '
        Me.Panel2.ColumnCount = 2
        Me.Panel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.Panel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.Panel2.Controls.Add(Me.Label3, 0, 0)
        Me.Panel2.Controls.Add(Me.txtCustom, 1, 0)
        Me.Panel2.Location = New System.Drawing.Point(64, 75)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.RowCount = 1
        Me.Panel2.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.Panel2.Size = New System.Drawing.Size(245, 28)
        Me.Panel2.TabIndex = 7
        Me.Panel2.Visible = False
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.Location = New System.Drawing.Point(3, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 28)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Custom Field Descriptor"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCustom
        '
        Me.txtCustom.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCustom.Location = New System.Drawing.Point(145, 3)
        Me.txtCustom.Name = "txtCustom"
        Me.txtCustom.Size = New System.Drawing.Size(97, 21)
        Me.txtCustom.TabIndex = 8
        '
        'Panel1
        '
        Me.Panel1.ColumnCount = 2
        Me.Panel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.32258!))
        Me.Panel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.67742!))
        Me.Panel1.Controls.Add(Me.txtAdjustStamp, 1, 0)
        Me.Panel1.Controls.Add(Me.Label26, 0, 0)
        Me.Panel1.Location = New System.Drawing.Point(64, 75)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.RowCount = 1
        Me.Panel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.Panel1.Size = New System.Drawing.Size(248, 27)
        Me.Panel1.TabIndex = 8
        Me.Panel1.Visible = False
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAdjustStamp.Location = New System.Drawing.Point(196, 3)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 20
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label26.Location = New System.Drawing.Point(3, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(136, 27)
        Me.Label26.TabIndex = 19
        Me.Label26.Text = "Adjust constant by (days)"
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Type"
        '
        'cmbType
        '
        Me.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbType.ItemHeight = 13
        Me.cmbType.Location = New System.Drawing.Point(64, 24)
        Me.cmbType.Name = "cmbType"
        Me.cmbType.Size = New System.Drawing.Size(248, 21)
        Me.cmbType.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Name"
        '
        'cmbName
        '
        Me.cmbName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbName.ItemHeight = 13
        Me.cmbName.Location = New System.Drawing.Point(64, 48)
        Me.cmbName.Name = "cmbName"
        Me.cmbName.Size = New System.Drawing.Size(248, 21)
        Me.cmbName.TabIndex = 1
        '
        'cmdSelect
        '
        Me.cmdSelect.Image = CType(resources.GetObject("cmdSelect.Image"), System.Drawing.Image)
        Me.cmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSelect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSelect.Location = New System.Drawing.Point(352, 16)
        Me.cmdSelect.Name = "cmdSelect"
        Me.cmdSelect.Size = New System.Drawing.Size(75, 23)
        Me.cmdSelect.TabIndex = 1
        Me.cmdSelect.Text = "&Insert"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(352, 48)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Done"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("Tip", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0))
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'tvInserter
        '
        Me.tvInserter.ImageIndex = 0
        Me.tvInserter.ImageList = Me.ImageList1
        Me.tvInserter.Location = New System.Drawing.Point(8, 160)
        Me.tvInserter.Name = "tvInserter"
        Me.tvInserter.SelectedImageIndex = 0
        Me.tvInserter.ShowLines = False
        Me.tvInserter.Size = New System.Drawing.Size(306, 429)
        Me.tvInserter.TabIndex = 3
        Me.tvInserter.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "breakpoint.png")
        Me.ImageList1.Images.SetKeyName(1, "element_into.png")
        '
        'frmInserter
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(434, 157)
        Me.Controls.Add(Me.tvInserter)
        Me.Controls.Add(Me.cmdSelect)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmInserter"
        Me.ShowInTaskbar = False
        Me.Text = "Insert"
        Me.GroupBox1.ResumeLayout(False)
        Me.pnAttachIndex.ResumeLayout(False)
        Me.pnAttachIndex.PerformLayout()
        CType(Me.txtIndex, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnMsgFormat.ResumeLayout(False)
        Me.pnMsgFormat.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Enum e_Mode As Integer
        DATA = 0
        CONSTANT = 1
    End Enum
    Public Property m_HideUserConstants() As Boolean
        Get
            Return HideUserConstants
        End Get
        Set(ByVal value As Boolean)
            HideUserConstants = value
        End Set
    End Property
    Public Property m_HideParameters() As Boolean
        Get
            Return HidePars
        End Get
        Set(ByVal value As Boolean)
            HidePars = True
        End Set
    End Property

    Public Property m_EventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = True
        End Set
    End Property

    Public Property m_EventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Private Property m_ReturnType() As e_Mode
        Get
            Return mode
        End Get
        Set(ByVal value As e_Mode)
            mode = value
        End Set
    End Property


    Private Sub frmInserter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.i1_Inserts) = False And _
        IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s3_EventBasedScheds) = False And _
        IsFeatEnabled(gEdition.ENTERPRISE, modFeatCodes.s5_DataDrivenSched) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Close()
            Return
        End If

        FormatForWinXP(Me)
    End Sub



    Public Function GetDatabaseField(ByVal sTables() As String, ByVal sDataCon As String, ByVal owner As Form) As String
        sCon = sDataCon

        Try
            For Each s As String In sTables
                If s.Length > 0 Then cmbType.Items.Add(s)
            Next
        Catch
        End Try

        Label1.Text = "Table"
        Label2.Text = "Column"

        Me.m_ReturnType = e_Mode.DATA

        Me.Owner = owner
        Me.Show()

        If UserCancel = True Then Return String.Empty

        '        Return "<[x]" & cmbType.Text & "." & cmbName.Text & ">"

    End Function

    Public Overloads Function GetConstants() As String
        With cmbType.Items
            .Clear()
            '.Add("@@Crystal Constants")
            .Add("@@SQL-RD Constants")

            ' Me.tvInserter.Nodes.Add("@@Crystal Constants")
            Me.tvInserter.Nodes.Add("@@SQL-RD Constants")

            If Me.m_HideUserConstants = False Then
                .Add("@@User Defined Constants")
                Me.tvInserter.Nodes.Add("@@User Defined Constants")
            End If

            If Me.m_HideParameters = False And greportItem IsNot Nothing Then
                .Add("@@Parameters")
                Me.tvInserter.Nodes.Add("@@Parameters")
            End If

            'If Me.m_EventBased = True Then
            .Add("@@Event-Based Data")
            Me.tvInserter.Nodes.Add("@@Event-Based Data")
            'End If

            If frmRDScheduleWizard.m_DataFields IsNot Nothing Then
                .Add("@@Data-Driven Data")
                Me.tvInserter.Nodes.Add("@@Data-Driven Data")
            End If


            If greportItem IsNot Nothing And gDoReportParsing = True Then
                ' .Add("@@Crystal Report Field")
                'Me.tvInserter.Nodes.Add("@@Crystal Report Field")
            End If

            .Add("@@Data Items")
        End With

        Me.m_ReturnType = e_Mode.CONSTANT
        Me.Owner = Owner

        If Container IsNot Nothing Then
            m_Container = Container
        End If

        Dialog = True

        Me.ShowDialog()

        If UserCancel = True Then Return String.Empty

        Try
            If cmbType.Text.Length = 0 Then
                ep.SetError(cmbType, "Please select the type")
                cmbType.Focus()
                Return String.Empty
            ElseIf cmbName.Text.Length = 0 Or cmbName.Text = "[New...]" Then
                ep.SetError(cmbName, "Please select the name")
                cmbName.Focus()
                Return String.Empty
            ElseIf cmbName.Text.IndexOf(";Custom") > -1 And txtCustom.Text.Length = 0 Then
                ep.SetError(txtCustom, "Please specify the custom search field")
                txtCustom.Focus()
                Return String.Empty
            End If

            Select Case Me.m_ReturnType
                Case e_Mode.CONSTANT
                    If cmbType.Text = "@@SQL-RD Constants" Then
                        Dim value As String

                        value = cmbName.Text

                        If txtAdjustStamp.Value <> 0 Then
                            value &= ";" & txtAdjustStamp.Value
                        End If

                        If Panel3.Visible = True And cmbDateFormat.Text.Length > 0 Then
                            value &= ";" & cmbDateFormat.Text
                        End If

                        Return "<[m]" & value & ">"

                        'If txtAdjustStamp.Value = 0 Then
                        '    ctrl.selectedText = "<[m]" & cmbName.Text & ">"
                        'ElseIf cmbDateFormat.Text.Length = 0 Then
                        '    ctrl.selectedText = "<[m]" & cmbName.Text & ";" & txtAdjustStamp.Value & ">"

                        'End If
                    ElseIf cmbType.Text = "@@User Defined Constants" Then
                        Return "<[u]" & cmbName.Text & ">"
                    ElseIf cmbType.Text = "@@Parameters" Then
                        Return "<[p]" & cmbName.Text & ">"
                    ElseIf cmbType.Text = "@@Event-Based Data" Then
                        Return "<[e]" & cmbName.Text & ">"
                    ElseIf cmbType.Text = "@@Crystal Report Field" Then
                        Return "<[c]" & cmbName.Text & ">"
                    ElseIf cmbType.Text = "@@Data-Driven Data" Then
                        Return "<[r]" & cmbName.Text & ">"
                    Else
                        Return cmbName.Text
                    End If
                Case e_Mode.DATA
                    Return "<[x]" & cmbType.Text & "." & cmbName.Text & ">"
            End Select
        Catch : End Try

    End Function
    Public Overloads Function GetConstants(ByVal owner As Form, _
    Optional ByVal container As ContainerControl = Nothing) As String
        With cmbType.Items
            .Clear()
            '.Add("@@Crystal Constants")
            .Add("@@SQL-RD Constants")

            If Me.m_HideUserConstants = False Then
                .Add("@@User Defined Constants")
            End If

            If Me.m_HideParameters = False Then
                .Add("@@Parameters")
            End If

            'If Me.m_EventBased = True Then
            .Add("@@Event-Based Data")
            'End If

            If frmRDScheduleWizard.m_DataFields IsNot Nothing Then .Add("@@Data-Driven Data")

            If greportItem IsNot Nothing And gDoReportParsing = True Then
                '.Add("@@Crystal Report Field")
                'Me.tvInserter.Nodes.Add("@@Crystal Report Field")
            End If

            .Add("@@Data Items")
        End With

        Dim obj As ComboBox.ObjectCollection = cmbType.Items

        For Each o As Object In obj
            Dim node As TreeNode = New TreeNode(o, 0, 0)
            node.Tag = "type"
            tvInserter.Nodes.Add(node)
        Next

        Me.m_ReturnType = e_Mode.CONSTANT
        Me.Owner = owner

        If container IsNot Nothing Then
            m_Container = container
        End If

        Dialog = False

        Me.Show()

        If UserCancel = True Then Return String.Empty

    End Function

    Private Sub cmbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbType.SelectedIndexChanged
        cmbName.Text = ""

        Select Case cmbType.Text
            Case "@@Crystal Constants"
                With cmbName.Items
                    .Clear()
                    .Add("CurrentDate")
                    .Add("CurrentTime")
                    .Add("CurrentDateTime")
                    .Add("WeekToDateFromSun")
                    .Add("MonthToDate")
                    .Add("YearToDate")
                    .Add("Last7Days")
                    .Add("Last4WeeksToSun")
                    .Add("LastFullWeek")
                    .Add("LastFullMonth")
                    .Add("AllDatesToToday")
                    .Add("AllDatesToYesterday")
                    .Add("AllDatesFromToday")
                    .Add("AllDatesFromTomorrow")
                    .Add("Aged0To30Days")
                    .Add("Aged31To60Days")
                    .Add("Aged61To90Days")
                    .Add("Over90Days")
                    .Add("Next30Days")
                    .Add("Next31To60Days")
                    .Add("Next61To90Days")
                    .Add("Next91To365Days")
                    .Add("Calendar1stQtr")
                    .Add("Calendar2ndQtr")
                    .Add("Calendar3rdQtr")
                    .Add("Calendar4thQtr")
                    .Add("Calendar1stHalf")
                    .Add("Calendar2ndHalf")
                    .Add("LastYearMTD")
                    .Add("LastYearYTD")
                End With

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@SQL-RD Constants"
                With cmbName.Items
                    .Clear()
                    .Add("CurrentDate")
                    .Add("CurrentTime")
                    .Add("CurrentDateTime")
                    .Add("CurrentMonth")
                    .Add("CurrentMonthName")
                    .Add("CurrentScheduleName")
                    .Add("CurrentYear")
                    .Add("CurrentDay")
                    .Add("CurrentWeekDayName")
                    .Add("Monday Last Week")
                    .Add("Tuesday Last Week")
                    .Add("Wednesday Last Week")
                    .Add("Thursday Last Week")
                    .Add("Friday Last Week")
                    .Add("Saturday Last Week")
                    .Add("Sunday Last Week")
                    .Add("Monday This Week")
                    .Add("Tuesday This Week")
                    .Add("Wednesday This Week")
                    .Add("Thursday This Week")
                    .Add("Friday This Week")
                    .Add("Saturday This Week")
                    .Add("Sunday This Week")
                    .Add("Month Start Last Month")
                    .Add("Month End Last Month")
                    .Add("Year Start Last Year")
                    .Add("Start This Year")
                    .Add("End Last Year")
                    .Add("Exported File Name")
                    .Add("Key Parameter Value")
                    .Add("Current Group Value")
                    .Add("Yesterday")
                    .Add("Last Day of the Month")
                    .Add("First Day of Next Month")
                    .Add("Current Temp File Path")
                End With

                Panel2.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@User Defined Constants"
                With cmbName.Items
                    .Clear()
                    .Add("[New/Edit...]")

                    Dim oRs As ADODB.Recordset
                    Dim SQL As String

                    SQL = "SELECT * FROM FormulaAttr"

                    oRs = clsMarsData.GetData(SQL)

                    Try
                        Do While oRs.EOF = False
                            .Add(oRs("FormulaName").Value)
                            oRs.MoveNext()
                        Loop
                        oRs.Close()
                    Catch ex As Exception

                    End Try
                End With
                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@Parameters"
                If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) = False Then
                    _NeedUpgrade(gEdition.ENTERPRISE)
                    cmbType.SelectedIndex = 0
                    Return
                End If

                cmbName.Items.Clear()

                If Me.m_ParameterList IsNot Nothing Then
                    For Each s As String In m_ParameterList
                        cmbName.Items.Add(s)
                    Next
                End If

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@Event-Based Data"
                cmbName.Items.Clear()

                GetEventData()

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@Data-Driven Data"
                cmbName.Items.Clear()

                For Each entry As DictionaryEntry In frmRDScheduleWizard.m_DataFields
                    cmbName.Items.Add(entry.Value)
                Next
            
            Case "@@Data Items"

                cmbName.Items.Clear()

                Dim SQL As String = "SELECT DISTINCT ItemName FROM DataItems"
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        cmbName.Items.Add(oRs(0).Value)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case Else
                cmbName.Items.Clear()

                Dim sUser As String
                Dim sDsn As String
                Dim sPassword As String

                sDsn = sCon.Split("|")(0)
                sUser = sCon.Split("|")(1)
                sPassword = sCon.Split("|")(2)

                oData.GetColumns(cmbName, sDsn, cmbType.Text, sUser, sPassword)

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
        End Select
    End Sub

    Private Sub GetEventData()

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim nID As Integer = 0

            If clsMarsEvent.currentEventID = 0 Then
                nID = eventID
            Else
                nID = clsMarsEvent.currentEventID
            End If

            SQL = "SELECT * FROM EventConditions WHERE EventID = " & nID

            oRs = clsMarsData.GetData(SQL)

            Dim oNode As TreeNode = Me.tvInserter.SelectedNode

            If oNode IsNot Nothing Then oNode.Nodes.Clear()

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim conditionType As String = oRs("conditiontype").Value
                    Dim conditionName As String = oRs("conditionname").Value

                    Select Case conditionType.ToLower
                        Case "database record exists", "database record has changed"
                            Dim oRs1 As ADODB.Recordset = New ADODB.Recordset
                            Dim oCon As ADODB.Connection = New ADODB.Connection
                            Dim connectionString As String
                            Dim user As String
                            Dim password As String
                            Dim dsn As String

                            SQL = oRs("searchcriteria").Value
                            connectionString = oRs("connectionstring").Value

                            dsn = connectionString.Split("|")(0)
                            user = connectionString.Split("|")(1)
                            password = connectionString.Split("|")(2)

                            password = _DecryptDBValue(password)

                            oCon.Open(dsn, user, password)

                            oRs1.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic)

                            For Each field As ADODB.Field In oRs1.Fields
                                cmbName.Items.Add(conditionName & ";[" & field.Name & "]")

                                If oNode IsNot Nothing Then oNode.Nodes.Add(conditionName & ";[" & field.Name & "]")
                            Next

                            oRs1.Close()

                            oCon.Close()
                        Case "file exists", "file has been modified"
                            With cmbName.Items
                                .Add(conditionName & ";FileName")
                                .Add(conditionName & ";FileDirectory")
                                .Add(conditionName & ";FilePath")
                                .Add(conditionName & ";DateModified")
                                .Add(conditionName & ";DateCreated")
                            End With

                            If oNode IsNot Nothing Then
                                Dim obj As ComboBox.ObjectCollection = cmbName.Items

                                For Each o As Object In obj
                                    oNode.Nodes.Add(o)
                                Next
                            End If

                        Case "process exists", "window is present"
                            With cmbName.Items
                                .Add(conditionName & ";ProcessName")
                                .Add(conditionName & ";MainWindowTitle")
                                .Add(conditionName & ";TotalProcessorTime")
                                .Add(conditionName & ";UserProcessorTime")
                                .Add(conditionName & ";MaxWorkingSet")
                                .Add(conditionName & ";MinWorkingSet")
                                .Add(conditionName & ";ID")
                            End With

                            If oNode IsNot Nothing Then
                                Dim obj As ComboBox.ObjectCollection = cmbName.Items

                                For Each o As Object In obj
                                    oNode.Nodes.Add(o)
                                Next
                            End If
                        Case "unread email is present"
                            With cmbName.Items
                                .Add(conditionName & ";From")
                                .Add(conditionName & ";To")
                                .Add(conditionName & ";Cc")
                                .Add(conditionName & ";Bcc")
                                .Add(conditionName & ";Subject")
                                .Add(conditionName & ";Message")
                                .Add(conditionName & ";DateSent")
                                .Add(conditionName & ";Attachments")
                                .Add(conditionName & ";AttachmentsPath")
                                .Add(conditionName & ";Custom")
                            End With

                            If oNode IsNot Nothing Then
                                Dim obj As ComboBox.ObjectCollection = cmbName.Items

                                For Each o As Object In obj
                                    oNode.Nodes.Add(o)
                                Next
                            End If
                    End Select

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, ex.TargetSite.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmbName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbName.SelectedIndexChanged
        If cmbName.Text = "[New/Edit...]" Then

            Dim oNew As New frmFormulaEditor
            Dim sConstant As String = ""

            sConstant = oNew.GetName()

            With cmbName.Items
                .Clear()
                .Add("[New/Edit...]")

                Dim oRs As ADODB.Recordset
                Dim SQL As String

                SQL = "SELECT * FROM FormulaAttr"

                oRs = clsMarsData.GetData(SQL)

                Try
                    Do While oRs.EOF = False
                        .Add(oRs("FormulaName").Value)
                        oRs.MoveNext()
                    Loop
                    oRs.Close()
                Catch ex As Exception

                End Try
            End With

            If sConstant.Length > 0 Then
                cmbName.Text = sConstant
            End If
        Else
            If cmbName.Text.IndexOf(";Custom") > -1 Then
                If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.e2_AdvancedEventsPack) = False Then
                    _NeedUpgrade(gEdition.CORPORATE)
                    cmbName.SelectedIndex = 0
                    Return
                End If

                Panel2.Visible = True
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
                Me.pnAttachIndex.Visible = False

                cmbName.DropDownStyle = ComboBoxStyle.DropDown
            ElseIf cmbName.Text.IndexOf(";Message") > -1 Then
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = True
                Me.pnAttachIndex.Visible = False
            ElseIf cmbName.Text.IndexOf(";Attachments") > -1 Then
                Me.pnAttachIndex.Visible = True
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Else
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
                Me.pnAttachIndex.Visible = False

                cmbName.DropDownStyle = ComboBoxStyle.DropDownList
                cmbDateFormat.Text = ""

                Select Case cmbName.Text
                    Case "CurrentScheduleName", "Exported File Name", "Key Parameter Value", "Current Group Value", "Current Temp File Path"
                        txtAdjustStamp.Enabled = False
                        txtAdjustStamp.Value = 0
                        Panel1.Visible = False
                    Case Else
                        If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
                            txtAdjustStamp.Enabled = False
                            txtAdjustStamp.Value = 0
                            Panel1.Visible = False
                        End If

                        Select Case cmbName.Text
                            Case "CurrentWeekDayName", "CurrentDay"
                                Label26.Text = "Adjust value by (days)"
                                Panel3.Visible = False
                                Panel1.Visible = True
                            Case "CurrentTime", "CurrentDateTime"
                                Label26.Text = "Adjust value by (minutes)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                            Case "CurrentMonthName"
                                Label26.Text = "Adjust value by (months)"
                                Panel1.Visible = True
                            Case "CurrentYear"
                                Label26.Text = "Adjust value by (years)"
                                Panel1.Visible = True
                            Case "CurrentMonth"
                                Label26.Text = "Adjust value by (months)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                            Case Else
                                Label26.Text = "Adjust value by (days)"
                                Panel3.Visible = True
                                Panel1.Visible = True
                        End Select

                        ' If gnEdition >= gEdition.ENTERPRISE Then txtAdjustStamp.Enabled = True
                End Select

                If cmbType.Text <> "@@SQL-RD Constants" Then
                    Panel1.Visible = False
                    Panel3.Visible = False
                End If

            End If
        End If
    End Sub

    Private Sub cmdSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSelect.Click

        Try
            If cmbType.Text.Length = 0 Then
                ep.SetError(cmbType, "Please select the type")
                cmbType.Focus()
                Return
            ElseIf cmbName.Text.Length = 0 Or cmbName.Text = "[New...]" Then
                ep.SetError(cmbName, "Please select the name")
                cmbName.Focus()
                Return
            ElseIf cmbName.Text.IndexOf(";Custom") > -1 And txtCustom.Text.Length = 0 Then
                ep.SetError(txtCustom, "Please specify the custom search field")
                txtCustom.Focus()
                Return
            End If

            If Dialog = True Then Close()

            Dim ctrl As Object

            If TypeOf Owner.ActiveControl Is ucDSN Then
                Dim dsn As ucDSN = Owner.ActiveControl

                If TypeOf dsn.ActiveControl Is TextBox Then
                    ctrl = CType(dsn.ActiveControl, TextBox)
                ElseIf TypeOf dsn.ActiveControl Is ComboBox Then
                    ctrl = CType(dsn.ActiveControl, ComboBox)
                End If
            ElseIf m_Container Is Nothing Then
                If TypeOf Owner.ActiveControl Is TextBox Then
                    ctrl = CType(Me.Owner.ActiveControl, TextBox)
                ElseIf TypeOf Owner.ActiveControl Is ComboBox Then
                    ctrl = CType(Owner.ActiveControl, ComboBox)
                End If
            Else
                If TypeOf m_Container.ActiveControl Is TextBox Then
                    ctrl = CType(m_Container.ActiveControl, TextBox)
                ElseIf TypeOf m_Container.ActiveControl Is ComboBox Then
                    ctrl = CType(m_Container.ActiveControl, ComboBox)
                End If
            End If

            Select Case Me.m_ReturnType
                Case e_Mode.CONSTANT
                    If cmbType.Text = "@@SQL-RD Constants" Then
                        Dim value As String

                        value = cmbName.Text

                        If txtAdjustStamp.Value <> 0 Then
                            value &= ";" & txtAdjustStamp.Value
                        End If

                        If Panel3.Visible = True Then
                            If cmbDateFormat.Text = "" Then cmbDateFormat.Text = "yyyy-MM-dd"

                            value &= ";" & cmbDateFormat.Text
                        End If

                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[m]" & value & ">"
                        Else
                            ctrl.SelectedText = "<[m]" & value & ">"
                        End If

                        'If txtAdjustStamp.Value = 0 Then
                        '    ctrl.selectedText = "<[m]" & cmbName.Text & ">"
                        'ElseIf cmbDateFormat.Text.Length = 0 Then
                        '    ctrl.selectedText = "<[m]" & cmbName.Text & ";" & txtAdjustStamp.Value & ">"

                        'End If
                    ElseIf cmbType.Text = "@@User Defined Constants" Then
                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[u]" & cmbName.Text & ">"
                        Else
                            ctrl.selectedText = "<[u]" & cmbName.Text & ">"
                        End If
                    ElseIf cmbType.Text = "@@Parameters" Then
                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[p]" & cmbName.Text & ">"
                        Else
                            ctrl.selectedText = "<[p]" & cmbName.Text & ">"
                        End If
                    ElseIf cmbType.Text = "@@Event-Based Data" Then
                        Dim value As String = cmbName.Text

                        If Me.pnMsgFormat.Visible = True Then
                            If optHTML.Checked = True Then
                                value &= ";HTML"
                            ElseIf optText.Checked = True Then
                                value &= ";TEXT"
                            End If
                        ElseIf Me.pnAttachIndex.Visible = True Then
                            value &= ";" & Me.txtIndex.Value
                        End If

                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[e]" & value & ">"
                        Else
                            ctrl.selectedText = "<[e]" & value & ">"
                        End If
                    ElseIf cmbType.Text = "@@Data-Driven Data" Then
                        Dim value As String = cmbName.Text

                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[r]" & value & ">"
                        Else
                            ctrl.selectedText = "<[r]" & value & ">"
                        End If
                    ElseIf cmbType.Text = "@@Crystal Report Field" Then
                        Dim value As String = cmbName.Text

                        If TypeOf ctrl Is ComboBox Then
                            ctrl.Text = "<[c]" & value & ">"
                        Else
                            ctrl.selectedText = "<[c]" & value & ">"
                        End If
                    ElseIf cmbType.Text = "@@Data Items" Then
                        Dim value As String = cmbName.Text

                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = "<[d]" & value & ">"
                        Else
                            ctrl.selectedText = "<[d]" & value & ">"
                        End If
                    Else
                        If TypeOf ctrl Is ComboBox Then
                            ctrl.text = cmbName.Text
                        Else
                            ctrl.selectedText = cmbName.Text
                        End If
                    End If

                Case e_Mode.DATA
                    If TypeOf ctrl Is ComboBox Then
                        ctrl.text = "<[x]" & cmbType.Text & "." & cmbName.Text & ">"
                    Else
                        ctrl.selectedText = "<[x]" & cmbType.Text & "." & cmbName.Text & ">"
                    End If
            End Select
        Catch : End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub txtCustom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustom.TextChanged
        Dim conditionStart As String = cmbName.Text.Split(";")(0)

        If txtCustom.Text.Length > 0 Then

            cmbName.Text = conditionStart & ";" & txtCustom.Text
        Else
            cmbName.Text = conditionStart & ";Custom"
        End If
    End Sub

    Private Sub tvInserter_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvInserter.AfterSelect
        Dim oNode As TreeNode = Me.tvInserter.SelectedNode

        If oNode Is Nothing Then Return

        If oNode.Tag <> "type" Then Return

        Select Case oNode.Text
            Case "@@Crystal Constants"
                If oNode.Nodes.Count = 0 Then
                    With oNode.Nodes
                        .Add("CurrentDate")
                        .Add("CurrentTime")
                        .Add("CurrentDateTime")
                        .Add("WeekToDateFromSun")
                        .Add("MonthToDate")
                        .Add("YearToDate")
                        .Add("Last7Days")
                        .Add("Last4WeeksToSun")
                        .Add("LastFullWeek")
                        .Add("LastFullMonth")
                        .Add("AllDatesToToday")
                        .Add("AllDatesToYesterday")
                        .Add("AllDatesFromToday")
                        .Add("AllDatesFromTomorrow")
                        .Add("Aged0To30Days")
                        .Add("Aged31To60Days")
                        .Add("Aged61To90Days")
                        .Add("Over90Days")
                        .Add("Next30Days")
                        .Add("Next31To60Days")
                        .Add("Next61To90Days")
                        .Add("Next91To365Days")
                        .Add("Calendar1stQtr")
                        .Add("Calendar2ndQtr")
                        .Add("Calendar3rdQtr")
                        .Add("Calendar4thQtr")
                        .Add("Calendar1stHalf")
                        .Add("Calendar2ndHalf")
                        .Add("LastYearMTD")
                        .Add("LastYearYTD")
                    End With
                End If
            Case "@@SQL-RD Constants"
                If oNode.Nodes.Count = 0 Then
                    With oNode.Nodes
                        .Add("CurrentDate")
                        .Add("CurrentTime")
                        .Add("CurrentDateTime")
                        .Add("CurrentMonth")
                        .Add("CurrentMonthName")
                        .Add("CurrentScheduleName")
                        .Add("CurrentYear")
                        .Add("CurrentDay")
                        .Add("CurrentWeekDayName")
                        .Add("Monday Last Week")
                        .Add("Tuesday Last Week")
                        .Add("Wednesday Last Week")
                        .Add("Thursday Last Week")
                        .Add("Friday Last Week")
                        .Add("Saturday Last Week")
                        .Add("Sunday Last Week")
                        .Add("Monday This Week")
                        .Add("Tuesday This Week")
                        .Add("Wednesday This Week")
                        .Add("Thursday This Week")
                        .Add("Friday This Week")
                        .Add("Saturday This Week")
                        .Add("Sunday This Week")
                        .Add("Month Start Last Month")
                        .Add("Month End Last Month")
                        .Add("Year Start Last Year")
                        .Add("Start This Year")
                        .Add("End Last Year")
                        .Add("Exported File Name")
                        .Add("Key Parameter Value")
                        .Add("Current Group Value")
                        .Add("Yesterday")
                        .Add("Last Day of the Month")
                        .Add("First Day of Next Month")
                        .Add("Current Temp File Path")
                    End With
                End If
            Case "@@User Defined Constants"
                If oNode.Nodes.Count = 0 Then
                    Dim oRs As ADODB.Recordset
                    Dim SQL As String

                    SQL = "SELECT * FROM FormulaAttr"

                    oRs = clsMarsData.GetData(SQL)

                    Try
                        Do While oRs.EOF = False
                            oNode.Nodes.Add(oRs("FormulaName").Value)
                            oRs.MoveNext()
                        Loop
                        oRs.Close()
                    Catch : End Try
                End If
            Case "@@Parameters"
                If Not IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.i1_Inserts) Then
                    _NeedUpgrade(gEdition.ENTERPRISE)
                    Return
                End If

                If Me.m_ParameterList IsNot Nothing Then
                    For Each s As String In m_ParameterList
                        cmbName.Items.Add(s)
                    Next
                End If

            Case "@@Event-Based Data"
                GetEventData()

                Panel1.Visible = False
                Panel2.Visible = False
                Panel3.Visible = False
                Me.pnMsgFormat.Visible = False
            Case "@@Data-Driven Data"
                If oNode.Nodes.Count = 0 Then

                    For Each entry As DictionaryEntry In frmRDScheduleWizard.m_DataFields
                        oNode.Nodes.Add(entry.Value)
                    Next
                End If
           
        End Select

        If oNode IsNot Nothing Then
            For Each node As TreeNode In oNode.Nodes
                node.ImageIndex = 1
                node.SelectedImageIndex = 1
            Next

            oNode.ExpandAll()
        End If

        tvInserter.Sort()
    End Sub
End Class
