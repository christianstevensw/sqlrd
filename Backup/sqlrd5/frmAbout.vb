Imports Microsoft.Win32
Imports sqlrd.clsLicenser
Imports System.Windows.Forms
Public Class frmAbout
    Inherits System.Windows.Forms.Form
    Dim oUI As New clsMarsUI

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblEdition As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCompany As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCustNo As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtLicense As System.Windows.Forms.MaskedTextBox
    Friend WithEvents txtLast As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFingerprint As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAbout))
        Me.lblEdition = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.txtLicense = New System.Windows.Forms.MaskedTextBox
        Me.txtLast = New System.Windows.Forms.TextBox
        Me.txtUser = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtCompany = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtCustNo = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label2 = New System.Windows.Forms.Label
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.txtFingerprint = New System.Windows.Forms.TextBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.cmdApply = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblEdition
        '
        Me.lblEdition.AutoSize = True
        Me.lblEdition.BackColor = System.Drawing.Color.Transparent
        Me.lblEdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblEdition.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblEdition.Location = New System.Drawing.Point(12, 264)
        Me.lblEdition.Name = "lblEdition"
        Me.lblEdition.Size = New System.Drawing.Size(107, 13)
        Me.lblEdition.TabIndex = 11
        Me.lblEdition.Text = "Evaluation Edition"
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.Transparent
        Me.lblVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblVersion.Location = New System.Drawing.Point(334, 262)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(302, 16)
        Me.lblVersion.TabIndex = 8
        Me.lblVersion.Text = "Label2"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtLicense
        '
        Me.txtLicense.Location = New System.Drawing.Point(113, 92)
        Me.txtLicense.Mask = ">AAAA-AAAA-AAAAAAAAAA"
        Me.txtLicense.Name = "txtLicense"
        Me.txtLicense.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txtLicense.ReadOnly = True
        Me.txtLicense.Size = New System.Drawing.Size(161, 21)
        Me.txtLicense.TabIndex = 4
        '
        'txtLast
        '
        Me.txtLast.Location = New System.Drawing.Point(83, 3)
        Me.txtLast.Name = "txtLast"
        Me.txtLast.Size = New System.Drawing.Size(75, 21)
        Me.txtLast.TabIndex = 1
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(3, 3)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(74, 21)
        Me.txtUser.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 35)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "First && Last Name"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCompany
        '
        Me.txtCompany.Location = New System.Drawing.Point(113, 38)
        Me.txtCompany.Name = "txtCompany"
        Me.txtCompany.Size = New System.Drawing.Size(161, 21)
        Me.txtCompany.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 35)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 27)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Company Name"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(3, 89)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 28)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "License Key"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCustNo
        '
        Me.txtCustNo.Location = New System.Drawing.Point(113, 65)
        Me.txtCustNo.Name = "txtCustNo"
        Me.txtCustNo.Size = New System.Drawing.Size(161, 21)
        Me.txtCustNo.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(3, 62)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 27)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Customer Number:"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(0, 295)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(637, 14)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "SQL-RD is a trademark of ChristianSteven Software Ltd.  All other applications ar" & _
            "e the trademarks of their respective owners."
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.0722!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.9278!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtLicense, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCompany, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFingerprint, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCustNo, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 46)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(277, 146)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 29)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "HW Fingerprint"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.txtUser, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtLast, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(113, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(161, 29)
        Me.TableLayoutPanel2.TabIndex = 15
        '
        'txtFingerprint
        '
        Me.txtFingerprint.Location = New System.Drawing.Point(113, 120)
        Me.txtFingerprint.Name = "txtFingerprint"
        Me.txtFingerprint.ReadOnly = True
        Me.txtFingerprint.Size = New System.Drawing.Size(161, 21)
        Me.txtFingerprint.TabIndex = 5
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.sqlrd.My.Resources.Resources.strip
        Me.PictureBox2.Location = New System.Drawing.Point(14, 257)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(623, 5)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 15
        Me.PictureBox2.TabStop = False
        '
        'cmdApply
        '
        Me.cmdApply.Image = CType(resources.GetObject("cmdApply.Image"), System.Drawing.Image)
        Me.cmdApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(121, 197)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(168, 23)
        Me.cmdApply.TabIndex = 0
        Me.cmdApply.Text = "Enter License..."
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.sqlrd.My.Resources.Resources.sqlrd_aboutb
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(8, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(630, 291)
        Me.PictureBox1.TabIndex = 3
        Me.PictureBox1.TabStop = False
        '
        'frmAbout
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(637, 309)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblEdition)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAbout"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "About SQL-RD"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private Sub frmAbout_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        LoadPic()

        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & "sqlrd.exe")

        lblVersion.Text = oFile.FileMajorPart & "." & oFile.FileMinorPart & " " & oFile.Comments

        lblEdition.Text = gsEdition & " Edition"

        FormatForWinXP(Me)

        Dim oUI As New clsMarsUI

        Dim Custno As String
        Dim firstName, lastName As String
        Dim UserCo As String
        Dim oSec As New clsMarsSecurity

        With oUI
            Custno = .ReadRegistry("CustNo", "")
            firstName = .ReadRegistry("RegFirstName", "")
            lastName = .ReadRegistry("RegLastName", "")
            UserCo = .ReadRegistry("RegCo", "")
        End With

        txtCustNo.Text = Custno
        txtUser.Text = firstName
        txtLast.Text = lastName
        txtCompany.Text = UserCo

        Dim sLicense As String = ""

        sLicense = oUI.ReadRegistry("RegNo", "")

        If m_licenser.CheckLicense(sLicense, firstName, lastName, UserCo, Custno) = True Then
            txtLicense.Text = sLicense
        End If

        Try
            txtFingerprint.Text = gSystemInfo.Item("FingerPrint")
        Catch : End Try

    End Sub
    Private Sub LoadPic()
        On Error Resume Next
        Dim LastPic As String
        Dim oUI As clsMarsUI = New clsMarsUI

        LastPic = oUI.ReadRegistry("LastPic", "0")

        If LastPic = "0" Then
            PictureBox1.Image = My.Resources.sqlrd_abouta  'Image.FromFile(sAppPath & "crd_logoa.jpg")
            oUI.SaveRegistry("LastPic", "10")
        ElseIf LastPic = "10" Then
            PictureBox1.Image = My.Resources.sqlrd_abouta 'Image.FromFile(sAppPath & "crd_logoa.jpg")
            oUI.SaveRegistry("LastPic", "1")
        Else
            PictureBox1.Image = My.Resources.sqlrd_aboutb 'Image.FromFile(sAppPath & "crd_logob.jpg")
            oUI.SaveRegistry("LastPic", "0")
        End If
    End Sub

    Private Sub frmAbout_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Click
        CloseInStyle()
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        CloseInStyle()
    End Sub


    Public Sub CloseInStyle()
        'Dim I As Double = 1
        'Dim nDelay As Double = 0.0625


        'While (Me.Opacity > 0)
        '    Me.Opacity -= nDelay
        '    System.Threading.Thread.Sleep(94)
        'End While

        Me.Close()
    End Sub



    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Dim oSec As New clsMarsSecurity
        Dim FullReg As String

        If cmdApply.Text = "Enter License..." Then
            txtLicense.ReadOnly = False
            txtLicense.BackColor = System.Drawing.Color.White

            cmdApply.Text = "Apply License"
        Else
            Try

                FullReg = txtLicense.Text

                If m_licenser.CheckLicense(FullReg, txtUser.Text, txtLast.Text, txtCompany.Text, txtCustNo.Text) = True Then

                    MessageBox.Show("Thank you for purchasing and registering SQL-RD", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    oUI.SaveRegistry("RegNo", _
                    FullReg, , , True)

                    oUI.SaveRegistry("RegCo", _
                    txtCompany.Text, , , True)

                    oUI.SaveRegistry("RegUser", _
                    txtUser.Text & " " & txtLast.Text, , , True)

                    oUI.SaveRegistry("RegFirstName", txtUser.Text, , , True)

                    oUI.SaveRegistry("RegLastName", txtLast.Text, , , True)

                    oUI.SaveRegistry("CustNo", txtCustNo.Text, , , True)

                    Close()
                Else
                    MessageBox.Show("Invalid license number. Please check to make sure " & _
                    "that all fields have been entered correctly " & _
                    "(fields are case sensitive)", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Catch ex As Exception

                MessageBox.Show("Invalid license number. Please check to make sure " & _
                    "that all fields have been entered correctly " & _
                    "(fields are case sensitive)" & vbCrLf & vbCrLf & ex.Message, Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End Try
        End If
    End Sub

    Public Function ProvideLicense() As Boolean
        Me.ShowInTaskbar = True
        Text = "Please enter your SQL-RD License Information"
        Me.ShowDialog()

        Dim sReg As String
        Dim sCompany As String
        Dim firstName, lastName As String
        Dim custID As String

        sReg = oUI.ReadRegistry("RegNo", "")
        sCompany = oUI.ReadRegistry("RegCo", "")
        firstName = oUI.ReadRegistry("RegFirstName", "")
        lastName = oUI.ReadRegistry("RegLastName", "")
        custID = oUI.ReadRegistry("CustNo", "")

        Try
            If m_licenser.CheckLicense(sReg, firstName, lastName, sCompany, custID) = True Then
                Close()
            Else
                End
            End If
        Catch
            End
        End Try

    End Function

End Class
