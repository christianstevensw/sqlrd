Public Class frmEventProp
    Inherits System.Windows.Forms.Form
    Dim ep As New ErrorProvider
    Friend WithEvents tbProp As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents UcConditions1 As sqlrd.ucConditions
    Friend WithEvents pnExisting As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents optNew As System.Windows.Forms.RadioButton
    Friend WithEvents optExisting As System.Windows.Forms.RadioButton
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents chkStatus As System.Windows.Forms.CheckBox
    Friend WithEvents pnNew As System.Windows.Forms.Panel
    Friend WithEvents UcReports1 As sqlrd.ucReports
    Friend WithEvents UcExisting As sqlrd.ucExistingReports
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents ucFlow As sqlrd.ucExecutionPath
    Friend WithEvents TabItem6 As DevComponents.DotNetBar.TabItem
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem7 As DevComponents.DotNetBar.TabItem
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Dim NameChange As Boolean = False
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents DividerLabel4 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel3 As sqlrd.DividerLabel
    Friend WithEvents cmbOpHrs As System.Windows.Forms.ComboBox
    Friend WithEvents chkOpHrs As System.Windows.Forms.CheckBox
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents DividerLabel5 As sqlrd.DividerLabel
    Dim packed As Boolean = False
    Dim schedulePriority As String = "3 - Normal"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents imgHistory As System.Windows.Forms.ImageList
    Friend WithEvents grpSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents cmdEventclear As System.Windows.Forms.Button
    Friend WithEvents cmdEventRefresh As System.Windows.Forms.Button
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEventProp))
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtID = New System.Windows.Forms.TextBox
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdEventclear = New System.Windows.Forms.Button
        Me.cmdEventRefresh = New System.Windows.Forms.Button
        Me.grpSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.imgHistory = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuParameters = New System.Windows.Forms.ContextMenu
        Me.mnuParameter = New System.Windows.Forms.MenuItem
        Me.tbProp = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel
        Me.Step6 = New System.Windows.Forms.Panel
        Me.cmbPriority = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.DividerLabel5 = New sqlrd.DividerLabel
        Me.cmbOpHrs = New System.Windows.Forms.ComboBox
        Me.chkOpHrs = New System.Windows.Forms.CheckBox
        Me.DividerLabel3 = New sqlrd.DividerLabel
        Me.DividerLabel4 = New sqlrd.DividerLabel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.TabItem7 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcConditions1 = New sqlrd.ucConditions
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.chkStatus = New System.Windows.Forms.CheckBox
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.pnExisting = New System.Windows.Forms.Panel
        Me.UcExisting = New sqlrd.ucExistingReports
        Me.pnNew = New System.Windows.Forms.Panel
        Me.UcReports1 = New sqlrd.ucReports
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.optNew = New System.Windows.Forms.RadioButton
        Me.optExisting = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.ucFlow = New sqlrd.ucExecutionPath
        Me.TabItem6 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcTasks = New sqlrd.ucTasks
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.grpSort.SuspendLayout()
        CType(Me.tbProp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbProp.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.pnExisting.SuspendLayout()
        Me.pnNew.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(473, 355)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(385, 355)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(297, 355)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(12, 357)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 21)
        Me.txtID.TabIndex = 13
        Me.txtID.Visible = False
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(4, 122)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(360, 159)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 106)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(4, 284)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 33
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(4, 300)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(308, 74)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(4, 74)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(4, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(4, 26)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 10)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Schedule Name"
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        Me.imgFolders.Images.SetKeyName(12, "")
        '
        'cmdEventclear
        '
        Me.cmdEventclear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEventclear.Image = CType(resources.GetObject("cmdEventclear.Image"), System.Drawing.Image)
        Me.cmdEventclear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEventclear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEventclear.Location = New System.Drawing.Point(268, 322)
        Me.cmdEventclear.Name = "cmdEventclear"
        Me.cmdEventclear.Size = New System.Drawing.Size(75, 25)
        Me.cmdEventclear.TabIndex = 3
        Me.cmdEventclear.Text = "Clear"
        '
        'cmdEventRefresh
        '
        Me.cmdEventRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEventRefresh.Image = CType(resources.GetObject("cmdEventRefresh.Image"), System.Drawing.Image)
        Me.cmdEventRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEventRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEventRefresh.Location = New System.Drawing.Point(356, 322)
        Me.cmdEventRefresh.Name = "cmdEventRefresh"
        Me.cmdEventRefresh.Size = New System.Drawing.Size(75, 25)
        Me.cmdEventRefresh.TabIndex = 2
        Me.cmdEventRefresh.Text = "Refresh"
        Me.cmdEventRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpSort
        '
        Me.grpSort.BackColor = System.Drawing.Color.Transparent
        Me.grpSort.Controls.Add(Me.optAsc)
        Me.grpSort.Controls.Add(Me.optDesc)
        Me.grpSort.Location = New System.Drawing.Point(7, 10)
        Me.grpSort.Name = "grpSort"
        Me.grpSort.Size = New System.Drawing.Size(272, 48)
        Me.grpSort.TabIndex = 0
        Me.grpSort.TabStop = False
        Me.grpSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 0
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 1
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'tvHistory
        '
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgHistory
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(7, 64)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.tvHistory.Size = New System.Drawing.Size(424, 252)
        Me.tvHistory.TabIndex = 1
        '
        'imgHistory
        '
        Me.imgHistory.ImageStream = CType(resources.GetObject("imgHistory.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgHistory.TransparentColor = System.Drawing.Color.Transparent
        Me.imgHistory.Images.SetKeyName(0, "")
        Me.imgHistory.Images.SetKeyName(1, "")
        Me.imgHistory.Images.SetKeyName(2, "")
        Me.imgHistory.Images.SetKeyName(3, "")
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'tbProp
        '
        Me.tbProp.CanReorderTabs = True
        Me.tbProp.Controls.Add(Me.TabControlPanel1)
        Me.tbProp.Controls.Add(Me.TabControlPanel7)
        Me.tbProp.Controls.Add(Me.TabControlPanel2)
        Me.tbProp.Controls.Add(Me.TabControlPanel3)
        Me.tbProp.Controls.Add(Me.TabControlPanel4)
        Me.tbProp.Controls.Add(Me.TabControlPanel6)
        Me.tbProp.Controls.Add(Me.TabControlPanel5)
        Me.tbProp.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbProp.Location = New System.Drawing.Point(0, 0)
        Me.tbProp.Name = "tbProp"
        Me.tbProp.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbProp.SelectedTabIndex = 0
        Me.tbProp.Size = New System.Drawing.Size(571, 351)
        Me.tbProp.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tbProp.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbProp.TabIndex = 22
        Me.tbProp.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbProp.Tabs.Add(Me.TabItem1)
        Me.tbProp.Tabs.Add(Me.TabItem2)
        Me.tbProp.Tabs.Add(Me.TabItem3)
        Me.tbProp.Tabs.Add(Me.TabItem7)
        Me.tbProp.Tabs.Add(Me.TabItem5)
        Me.tbProp.Tabs.Add(Me.TabItem6)
        Me.tbProp.Tabs.Add(Me.TabItem4)
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.Step6)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.TabItem7
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.Color.Transparent
        Me.Step6.Controls.Add(Me.cmbPriority)
        Me.Step6.Controls.Add(Me.Label6)
        Me.Step6.Controls.Add(Me.DividerLabel5)
        Me.Step6.Controls.Add(Me.cmbOpHrs)
        Me.Step6.Controls.Add(Me.chkOpHrs)
        Me.Step6.Controls.Add(Me.DividerLabel3)
        Me.Step6.Controls.Add(Me.DividerLabel4)
        Me.Step6.Controls.Add(Me.UcError)
        Me.Step6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Step6.Location = New System.Drawing.Point(1, 1)
        Me.Step6.Name = "Step6"
        Me.Step6.Size = New System.Drawing.Size(445, 349)
        Me.Step6.TabIndex = 0
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"1 - High", "2 - Above Nornal", "3 - Normal", "4 - Below Normal", "5 - Low "})
        Me.cmbPriority.Location = New System.Drawing.Point(211, 154)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(218, 21)
        Me.cmbPriority.TabIndex = 10
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(9, 157)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(175, 13)
        Me.Label6.TabIndex = 9
        Me.Label6.Text = "Set the priority of this schedules to"
        '
        'DividerLabel5
        '
        Me.DividerLabel5.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel5.Location = New System.Drawing.Point(9, 138)
        Me.DividerLabel5.Name = "DividerLabel5"
        Me.DividerLabel5.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel5.Spacing = 0
        Me.DividerLabel5.TabIndex = 8
        Me.DividerLabel5.Text = "Schedule Priority"
        '
        'cmbOpHrs
        '
        Me.cmbOpHrs.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOpHrs.Enabled = False
        Me.cmbOpHrs.FormattingEnabled = True
        Me.cmbOpHrs.Location = New System.Drawing.Point(212, 108)
        Me.cmbOpHrs.Name = "cmbOpHrs"
        Me.cmbOpHrs.Size = New System.Drawing.Size(218, 21)
        Me.cmbOpHrs.TabIndex = 6
        '
        'chkOpHrs
        '
        Me.chkOpHrs.AutoSize = True
        Me.chkOpHrs.Location = New System.Drawing.Point(9, 108)
        Me.chkOpHrs.Name = "chkOpHrs"
        Me.chkOpHrs.Size = New System.Drawing.Size(173, 17)
        Me.chkOpHrs.TabIndex = 5
        Me.chkOpHrs.Text = "Use custom hours of operation"
        Me.chkOpHrs.UseVisualStyleBackColor = True
        '
        'DividerLabel3
        '
        Me.DividerLabel3.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel3.Location = New System.Drawing.Point(3, 6)
        Me.DividerLabel3.Name = "DividerLabel3"
        Me.DividerLabel3.Size = New System.Drawing.Size(435, 13)
        Me.DividerLabel3.Spacing = 0
        Me.DividerLabel3.TabIndex = 4
        Me.DividerLabel3.Text = "Exception Handling"
        '
        'DividerLabel4
        '
        Me.DividerLabel4.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel4.Location = New System.Drawing.Point(9, 86)
        Me.DividerLabel4.Name = "DividerLabel4"
        Me.DividerLabel4.Size = New System.Drawing.Size(429, 19)
        Me.DividerLabel4.Spacing = 0
        Me.DividerLabel4.TabIndex = 3
        Me.DividerLabel4.Text = "Hours of Operation"
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(3, 21)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(440, 62)
        Me.UcError.TabIndex = 0
        '
        'TabItem7
        '
        Me.TabItem7.AttachedControl = Me.TabControlPanel7
        Me.TabItem7.Image = CType(resources.GetObject("TabItem7.Image"), System.Drawing.Image)
        Me.TabItem7.Name = "TabItem7"
        Me.TabItem7.Text = "Schedule Options"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.txtDesc)
        Me.TabControlPanel1.Controls.Add(Me.Label2)
        Me.TabControlPanel1.Controls.Add(Me.Label3)
        Me.TabControlPanel1.Controls.Add(Me.txtName)
        Me.TabControlPanel1.Controls.Add(Me.Label7)
        Me.TabControlPanel1.Controls.Add(Me.Label4)
        Me.TabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.TabControlPanel1.Controls.Add(Me.txtFolder)
        Me.TabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem1
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel1
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "General"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.UcConditions1)
        Me.TabControlPanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.TabControlPanel2.Controls.Add(Me.TableLayoutPanel2)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem2
        '
        'UcConditions1
        '
        Me.UcConditions1.BackColor = System.Drawing.Color.Transparent
        Me.UcConditions1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcConditions1.Location = New System.Drawing.Point(1, 32)
        Me.UcConditions1.m_EventID = 99999
        Me.UcConditions1.m_OrderID = 0
        Me.UcConditions1.Name = "UcConditions1"
        Me.UcConditions1.Size = New System.Drawing.Size(438, 289)
        Me.UcConditions1.TabIndex = 1
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.chkStatus)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(1, 323)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(438, 27)
        Me.FlowLayoutPanel1.TabIndex = 2
        '
        'chkStatus
        '
        Me.chkStatus.AutoSize = True
        Me.chkStatus.Checked = True
        Me.chkStatus.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.chkStatus.Location = New System.Drawing.Point(3, 3)
        Me.chkStatus.Name = "chkStatus"
        Me.chkStatus.Size = New System.Drawing.Size(64, 17)
        Me.chkStatus.TabIndex = 0
        Me.chkStatus.Text = "Enabled"
        Me.chkStatus.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.23622!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.11927!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.78899!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbAnyAll, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 2, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(438, 26)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 26)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Fulfill"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.FormattingEnabled = True
        Me.cmbAnyAll.Items.AddRange(New Object() {"ANY", "ALL"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(47, 3)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.Location = New System.Drawing.Point(126, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(195, 26)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "of the following conditions"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel2
        Me.TabItem2.Image = CType(resources.GetObject("TabItem2.Image"), System.Drawing.Image)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Conditions"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.pnExisting)
        Me.TabControlPanel3.Controls.Add(Me.pnNew)
        Me.TabControlPanel3.Controls.Add(Me.TableLayoutPanel1)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem3
        '
        'pnExisting
        '
        Me.pnExisting.BackColor = System.Drawing.Color.Transparent
        Me.pnExisting.Controls.Add(Me.UcExisting)
        Me.pnExisting.Location = New System.Drawing.Point(7, 27)
        Me.pnExisting.Name = "pnExisting"
        Me.pnExisting.Size = New System.Drawing.Size(429, 320)
        Me.pnExisting.TabIndex = 7
        Me.pnExisting.Visible = False
        '
        'UcExisting
        '
        Me.UcExisting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcExisting.Location = New System.Drawing.Point(0, 0)
        Me.UcExisting.m_eventID = 99999
        Me.UcExisting.Name = "UcExisting"
        Me.UcExisting.Size = New System.Drawing.Size(429, 320)
        Me.UcExisting.TabIndex = 1
        '
        'pnNew
        '
        Me.pnNew.BackColor = System.Drawing.Color.Transparent
        Me.pnNew.Controls.Add(Me.UcReports1)
        Me.pnNew.Location = New System.Drawing.Point(7, 27)
        Me.pnNew.Name = "pnNew"
        Me.pnNew.Size = New System.Drawing.Size(429, 320)
        Me.pnNew.TabIndex = 12
        Me.pnNew.Visible = False
        '
        'UcReports1
        '
        Me.UcReports1.Dock = System.Windows.Forms.DockStyle.Top
        Me.UcReports1.Location = New System.Drawing.Point(0, 0)
        Me.UcReports1.m_MasterID = 99999
        Me.UcReports1.Name = "UcReports1"
        Me.UcReports1.Size = New System.Drawing.Size(429, 316)
        Me.UcReports1.TabIndex = 11
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 234.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.optNew, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optExisting, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.optNone, 2, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(445, 24)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'optNew
        '
        Me.optNew.AutoSize = True
        Me.optNew.BackColor = System.Drawing.Color.Transparent
        Me.optNew.Location = New System.Drawing.Point(3, 3)
        Me.optNew.Name = "optNew"
        Me.optNew.Size = New System.Drawing.Size(87, 17)
        Me.optNew.TabIndex = 0
        Me.optNew.Text = "New Reports"
        Me.optNew.UseVisualStyleBackColor = False
        '
        'optExisting
        '
        Me.optExisting.AutoSize = True
        Me.optExisting.BackColor = System.Drawing.Color.Transparent
        Me.optExisting.Location = New System.Drawing.Point(96, 3)
        Me.optExisting.Name = "optExisting"
        Me.optExisting.Size = New System.Drawing.Size(112, 17)
        Me.optExisting.TabIndex = 1
        Me.optExisting.Text = "Existing schedules"
        Me.optExisting.UseVisualStyleBackColor = False
        '
        'optNone
        '
        Me.optNone.AutoSize = True
        Me.optNone.BackColor = System.Drawing.Color.Transparent
        Me.optNone.Location = New System.Drawing.Point(214, 3)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(50, 17)
        Me.optNone.TabIndex = 2
        Me.optNone.Text = "None"
        Me.optNone.UseVisualStyleBackColor = False
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel3
        Me.TabItem3.Image = CType(resources.GetObject("TabItem3.Image"), System.Drawing.Image)
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "Schedules"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.cmdEventclear)
        Me.TabControlPanel4.Controls.Add(Me.grpSort)
        Me.TabControlPanel4.Controls.Add(Me.cmdEventRefresh)
        Me.TabControlPanel4.Controls.Add(Me.tvHistory)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.TabItem4
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel4
        Me.TabItem4.Image = CType(resources.GetObject("TabItem4.Image"), System.Drawing.Image)
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "History"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.ucFlow)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.TabItem6
        '
        'ucFlow
        '
        Me.ucFlow.BackColor = System.Drawing.Color.Transparent
        Me.ucFlow.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ucFlow.Location = New System.Drawing.Point(4, 12)
        Me.ucFlow.m_ExecutionFlow = "AFTER"
        Me.ucFlow.Name = "ucFlow"
        Me.ucFlow.Size = New System.Drawing.Size(406, 171)
        Me.ucFlow.TabIndex = 0
        '
        'TabItem6
        '
        Me.TabItem6.AttachedControl = Me.TabControlPanel6
        Me.TabItem6.Image = CType(resources.GetObject("TabItem6.Image"), System.Drawing.Image)
        Me.TabItem6.Name = "TabItem6"
        Me.TabItem6.Text = "Execution Flow"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.UcTasks)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(124, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(447, 351)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.TabItem5
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(1, 1)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(445, 349)
        Me.UcTasks.TabIndex = 0
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel5
        Me.TabItem5.Image = CType(resources.GetObject("TabItem5.Image"), System.Drawing.Image)
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "Tasks"
        '
        'frmEventProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(571, 383)
        Me.Controls.Add(Me.tbProp)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmEventProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Event-Based Properties"
        Me.grpSort.ResumeLayout(False)
        CType(Me.tbProp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbProp.ResumeLayout(False)
        Me.TabControlPanel7.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        Me.Step6.PerformLayout()
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.TabControlPanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.pnExisting.ResumeLayout(False)
        Me.pnNew.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControlPanel4.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        Me.TabControlPanel5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private ReadOnly Property m_ScheduleType() As String
        Get
            If Me.optExisting.Checked Then
                Return "Existing"
            ElseIf optNone.Checked Then
                Return "None"
            Else
                Return "New"
            End If
        End Get
    End Property
    Public Property m_Packed() As Boolean
        Get
            Return packed
        End Get
        Set(ByVal value As Boolean)
            If value = True Then
                Me.txtFolder.Text = "\Package"
                Me.txtFolder.Visible = False
                Me.cmdLoc.Visible = False
                Me.Label4.Visible = False
            End If
        End Set
    End Property

    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(0, 0, 0, 0, txtID.Text, clsMarsScheduler.enCalcType.MEDIAN, 3)
        End Get
    End Property

    Private Sub frmEventProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)

        Dim oUI As New clsMarsUI

        oUI.BuildTree(UcExisting.tvSchedules, True)

        UcExisting.tvSchedules.SelectedNode = UcExisting.tvSchedules.Nodes(0)

        UcExisting.tvSchedules.Nodes(0).Expand()

        UcTasks.ShowAfterType = False
        UcTasks.lsvTasks.HeaderStyle = ColumnHeaderStyle.None

        Me.cmbPriority.Text = schedulePriority
    End Sub


    Public Function EditSchedule(ByVal nEventID As Integer) As Boolean
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim nImage As Integer
        Dim scheduleType As String
        Dim autoCalc As Boolean
        Dim packID As Integer = 0

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            Return True
        End If

        txtID.Text = nEventID

        clsMarsEvent.currentEventID = nEventID

        SQL = "SELECT * FROM EventAttr6 WHERE EventID =" & nEventID

        oRs = clsMarsData.GetData(SQL)

        Me.UcExisting.parentID = txtID.Text
        Me.UcExisting.m_eventID = nEventID

        UcTasks.ScheduleID = nEventID
        UcTasks.m_eventID = nEventID
        UcTasks.m_eventBased = True
        UcTasks.LoadTasks()

        Me.UcConditions1.m_EventID = nEventID
        Me.UcConditions1.LoadConditions(nEventID)

        Me.UcReports1.m_MasterID = nEventID

        Try
            If oRs.EOF = True Then Return True

            txtName.Text = oRs("eventname").Value

            Me.Text = "Event-Based Schedule Properties - " & txtName.Text

            packID = IsNull(oRs("packid").Value, 0)

            autoCalc = IsNull(oRs("autocalc").Value, 0)
            Me.chkOpHrs.Checked = IsNull(oRs("useoperationhours").Value, 0)

            If Me.chkOpHrs.Checked = True Then
                Me.cmbOpHrs_DropDown(Nothing, Nothing)

                Me.cmbOpHrs.Text = IsNull(oRs("operationname").Value, "")
            End If

            If packID > 0 Then
                Me.DividerLabel4.Visible = False
                Me.chkOpHrs.Visible = False
                Me.cmbOpHrs.Visible = False

                Me.DividerLabel5.Visible = False
                Me.Label6.Visible = False
                Me.cmbPriority.Visible = False
            End If

            Dim rsP As ADODB.Recordset = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & oRs("Parent").Value)

            If rsP.EOF = False Then
                txtFolder.Text = rsP.Fields(0).Value

                txtFolder.Tag = oRs("Parent").Value

            Else
                txtFolder.Text = "\Package"
                txtFolder.Tag = oRs("packid").Value
            End If

            rsP.Close()
            'Dim sType As String = oRs("eventtype").Value

            If oRs("status").Value = 0 Then
                chkStatus.Checked = False
            Else
                chkStatus.Checked = True
            End If

            scheduleType = oRs("scheduletype").Value

            If scheduleType = "Existing" Then
                optExisting.Checked = True
            ElseIf scheduleType = "None" Then
                optNone.Checked = True
            Else
                optNew.Checked = True
            End If

            'schedule priority
            Try
                schedulePriority = IsNull(oRs("schedulepriority").Value, "2 - Normal")
            Catch ex As Exception
                schedulePriority = "3 - Normal"
            End Try

            ucFlow.m_ExecutionFlow = IsNull(oRs("executionflow").Value, "AFTER")

            txtDesc.Text = IsNull(oRs("description").Value)
            txtKeyWord.Text = IsNull(oRs("keyword").Value)

            cmbAnyAll.Text = oRs("anyall").Value

            Try
                UcError.m_autoFailAfter = IsNull(oRs("autofailafter").Value, 30)
                UcError.cmbRetry.Value = IsNull(oRs("retrycount").Value, 5)
                UcError.txtRetryInterval.Value = IsNull(oRs("retryinterval").Value, 0)
            Catch ex As Exception
                UcError.cmbRetry.Value = 5
                UcError.m_autoFailAfter = 30
            End Try

            UcError.chkAutoCalc.Checked = autoCalc

            If autoCalc = True Then
                Dim temp As Decimal = Me.m_autoCalcValue
                UcError.chkAutoCalc.Checked = True
                UcError.m_autoFailAfter = IIf(temp < 1, 1, temp)
                UcError.cmbAssumeFail.Enabled = False
            End If

            oRs.Close()

            If UcTasks.lsvTasks.Items.Count = 0 Then
                ucFlow.Enabled = False
            Else
                ucFlow.Enabled = True
            End If

            Me.ShowDialog()
        Catch ex As Exception
            ''console.writeLine(ex.Message)
        End Try

        Return chkStatus.Checked
    End Function

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click

        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter the name of the schedule")
            tbProp.SelectedTab = tbProp.Tabs(0)

            txtName.Focus()
            Return
        ElseIf txtFolder.Text.Length = 0 Then
            ep.SetError(txtFolder, "Please select the schedule's destination folder")
            tbProp.SelectedTab = tbProp.Tabs(0)

            txtFolder.Focus()
            Return
        ElseIf Me.chkOpHrs.Checked Then
            If Me.cmbOpHrs.Text = "" Or Me.cmbOpHrs.Text = "<New...>" Then
                ep.SetError(Me.cmbOpHrs, "Please select a valid entry...")
                Me.cmbOpHrs.Focus()
                Return
            End If
        End If


        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim nFire As Integer
        Dim OldName As String
        Dim newName As String = txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")

        Try
            Dim oRename As ADODB.Recordset

            oRename = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID =" & txtID.Text)

            If oRename.EOF = False Then
                OldName = oRename.Fields(0).Value

                OldName = OldName.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")
            End If

            oRename.Close()
        Catch
            OldName = txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "")
        End Try

        sVals = "EventName ='" & SQLPrepare(newName) & "'," & _
        "Description ='" & SQLPrepare(txtDesc.Text) & "'," & _
        "Keyword ='" & SQLPrepare(txtKeyWord.Text) & "'," & _
        "Parent =" & txtFolder.Tag & "," & _
        "Status =" & Convert.ToInt32(chkStatus.Checked) & "," & _
        "ScheduleType='" & Me.m_ScheduleType & "', " & _
        "AnyAll = '" & cmbAnyAll.Text & "', " & _
        "ExecutionFlow = '" & ucFlow.m_ExecutionFlow & "', " & _
        "RetryCount = " & UcError.cmbRetry.Value & "," & _
        "AutoFailAfter =" & UcError.m_autoFailAfter(True) & "," & _
        "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        "RetryInterval =" & UcError.txtRetryInterval.Value & "," & _
        "UseOperationHours =" & Convert.ToInt32(Me.chkOpHrs.Checked) & "," & _
        "OperationName = '" & SQLPrepare(Me.cmbOpHrs.Text) & "'," & _
        "SchedulePriority = '" & Me.cmbPriority.Text & "'"

        SQL = "UPDATE EventAttr6 SET " & sVals & " WHERE EventID =" & txtID.Text

        If clsMarsData.WriteData(SQL) = True Then
            Dim nID As Integer
            Dim sType As String
            Dim I As Integer = 1

            sCols = "ID,EventID,ScheduleID,ScheduleType"

            clsMarsData.WriteData("DELETE FROM EventSchedule WHERE EventID =" & txtID.Text)

            For Each oItem As ListViewItem In UcExisting.lsvSchedules.Items
                nID = oItem.Tag.Split(":")(1)
                sType = oItem.Tag.Split(":")(0)


                sVals = clsMarsData.CreateDataID("eventschedule", "id") & "," & _
                txtID.Text & "," & _
                nID & "," & _
                "'" & sType & "'"

                SQL = "INSERT INTO EventSchedule(" & sCols & ") VALUES(" & sVals & ")"

                If clsMarsData.WriteData(SQL) = False Then
                    Exit For
                End If
            Next
        End If

        Try
            Dim sPath As String

            If OldName <> txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", "") Then
                sPath = clsMarsEvent.m_CachedDataPath & OldName

                System.IO.Directory.Move(sPath, clsMarsEvent.m_CachedDataPath & txtName.Text.Replace("\", "").Replace("/", "").Replace("?", "").Replace("|", "").Replace("*", "").Replace(":", ""))
            End If

        Catch : End Try

        UcTasks.CommitDeletions()

        cmdApply.Enabled = False
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)
        Close()
        Dim oUI As New clsMarsUI

        oUI.RefreshView(oWindow(nWindowCurrent))

        clsMarsEvent.currentEventID = 99999

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTS, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
        NameChange = True
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged
        Try
            If optAsc.Checked = True Then
                Dim oEvent As New clsMarsEvent
                oEvent.DrawEventHistory(tvHistory, txtID.Text, "ASC")
            End If
        Catch
        End Try
    End Sub

    Private Sub optDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDesc.CheckedChanged
        Try
            If optDesc.Checked = True Then
                Dim oEvent As New clsMarsEvent
                oEvent.DrawEventHistory(tvHistory, txtID.Text, "DESC")
            End If
        Catch
        End Try
    End Sub

    Private Sub cmdEventRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEventRefresh.Click

        Dim oEvent As New clsMarsEvent

        If optAsc.Checked = True Then
            oEvent.DrawEventHistory(tvHistory, txtID.Text, "ASC")
        Else
            oEvent.DrawEventHistory(tvHistory, txtID.Text, "DESC")
        End If
    End Sub

    Private Sub cmdEventclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEventclear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim oData As New clsMarsData

            clsMarsData.WriteData("DELETE FROM EventHistory WHERE EventID =" & txtID.Text, False)

            _Delay(1)

            cmdEventRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If UcExisting.lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = UcExisting.lsvSchedules.SelectedItems(0)

        sType = oItem.Tag.Split(":")(0)
        nID = oItem.Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub
        oSub.m_eventID = txtID.Text
        oSub.SetSubstitution(nID, txtID.Text)
    End Sub

    Private Sub optNew_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNew.CheckedChanged, optExisting.CheckedChanged, optNone.CheckedChanged
        If txtID.Text.Length = 0 Then Return

        Try
            If optNew.Checked = True Then
                pnNew.Visible = True
                pnExisting.Visible = False

                With UcReports1
                    .m_MasterID = txtID.Text
                    .LoadReports()
                End With

                tbProp.Tabs(2).Text = "Reports"
            ElseIf optExisting.Checked Then
                pnExisting.Visible = True
                pnNew.Visible = False

                With UcExisting
                    .m_eventID = txtID.Text
                    .LoadSelectedReports()
                    clsMarsUI.MainUI.BuildTree(.tvSchedules, True)
                    .lsvSchedules.ContextMenu = .mnuParameters
                End With

                tbProp.Tabs(2).Text = "Schedules"
            ElseIf optNone.Checked = True Then
                pnNew.Visible = False
                pnExisting.Visible = False

            End If
        Catch : End Try
    End Sub

    Private Sub tbProp_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tbProp.SelectedTabChanged
        Select Case e.NewTab.Text
            Case "History"
                Dim oEvent As New clsMarsEvent
                oEvent.DrawEventHistory(tvHistory, txtID.Text, "ASC")
        End Select
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        clsMarsEvent.currentEventID = 99999
    End Sub
    Private Sub chkOpHrs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOpHrs.CheckedChanged
        Me.cmbOpHrs.Enabled = Me.chkOpHrs.Checked
    End Sub

    Private Sub cmbOpHrs_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOpHrs.SelectedIndexChanged
        ep.SetError(sender, "")

        If cmbOpHrs.Text = "<New...>" Then
            Dim opAdd As frmOperationalHours = New frmOperationalHours
            Dim sNew As String

            sNew = opAdd.AddOperationalHours

            If sNew <> "" Then
                Me.cmbOpHrs.Items.Add(sNew)

                Me.cmbOpHrs.Text = sNew
            End If
        End If
    End Sub

    Private Sub cmbOpHrs_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbOpHrs.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        Me.cmbOpHrs.Items.Clear()

        SQL = "SELECT * FROM OperationAttr"

        oRs = clsMarsData.GetData(SQL)

        Me.cmbOpHrs.Items.Add("<New...>")

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                cmbOpHrs.Items.Add(oRs("operationname").Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
End Class
