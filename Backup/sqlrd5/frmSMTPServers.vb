Imports Microsoft.Win32

Public Class frmSMTPServers
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Public IsDialog As Boolean
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Dim oReg As RegistryKey = Registry.LocalMachine
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lsvSMTP As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents mnuTest As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefault As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSMTP As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuEdit As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSMTPServers))
        Me.lsvSMTP = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.mnuSMTP = New System.Windows.Forms.ContextMenu
        Me.mnuEdit = New System.Windows.Forms.MenuItem
        Me.mnuTest = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuDefault = New System.Windows.Forms.MenuItem
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvSMTP
        '
        Me.lsvSMTP.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvSMTP.CheckBoxes = True
        Me.lsvSMTP.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvSMTP.ContextMenu = Me.mnuSMTP
        Me.lsvSMTP.FullRowSelect = True
        Me.lsvSMTP.GridLines = True
        Me.lsvSMTP.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvSMTP.Location = New System.Drawing.Point(8, 80)
        Me.lsvSMTP.Name = "lsvSMTP"
        Me.lsvSMTP.Size = New System.Drawing.Size(496, 432)
        Me.lsvSMTP.TabIndex = 0
        Me.lsvSMTP.UseCompatibleStateImageBehavior = False
        Me.lsvSMTP.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 184
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Server"
        Me.ColumnHeader2.Width = 147
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Sender Name"
        Me.ColumnHeader3.Width = 155
        '
        'mnuSMTP
        '
        Me.mnuSMTP.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEdit, Me.mnuTest, Me.MenuItem1, Me.mnuDefault})
        '
        'mnuEdit
        '
        Me.mnuEdit.Index = 0
        Me.mnuEdit.Text = "Edit"
        '
        'mnuTest
        '
        Me.mnuTest.Index = 1
        Me.mnuTest.Text = "Test"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.Text = "-"
        '
        'mnuDefault
        '
        Me.mnuDefault.Index = 3
        Me.mnuDefault.Text = "Set as Default"
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(152, 540)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(64, 23)
        Me.cmdDelete.TabIndex = 10
        Me.cmdDelete.Text = "&Delete"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdEdit
        '
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(80, 540)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(64, 23)
        Me.cmdEdit.TabIndex = 9
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(8, 540)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(64, 23)
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(426, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox1.TabIndex = 11
        Me.PictureBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(514, 64)
        Me.Panel1.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.Label1.ForeColor = System.Drawing.Color.Navy
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(256, 32)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "SMTP Servers"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Location = New System.Drawing.Point(-8, 518)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(560, 8)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(432, 536)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 14
        Me.cmdCancel.Text = "&Done"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(514, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 15
        '
        'frmSMTPServers
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(514, 571)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.cmdEdit)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.lsvSMTP)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmSMTPServers"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SMTP Servers"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub LoadAll()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM SMTPServers"

        oRs = clsMarsData.GetData(SQL)

        lsvSMTP.Items.Clear()

        Do While oRs.EOF = False
            oItem = New ListViewItem

            oItem.Text = oRs("smtpname").Value
            oItem.Tag = oRs("smtpid").Value
            oItem.SubItems.Add(oRs("smtpserver").Value)
            oItem.SubItems.Add(oRs("smtpsendername").Value)

            oItem.Checked = Convert.ToBoolean(oRs("isbackup").Value)

            lsvSMTP.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Private Sub frmSMTPServers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        If IsDialog = True Then
            mnuDefault.Enabled = False
        Else
            mnuDefault.Enabled = True
        End If

        LoadAll()
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim oSMTP As New frmSMTP

        oSMTP._AddServer()

        LoadAll()
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        If lsvSMTP.SelectedItems.Count = 0 Then Return

        Dim oSMTP As New frmSMTP

        oSMTP._EditServer(lsvSMTP.SelectedItems(0).Tag)

        LoadAll()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvSMTP.SelectedItems.Count = 0 Then Return

        clsMarsData.WriteData("DELETE FROM SMTPServers WHERE SMTPID =" & lsvSMTP.SelectedItems(0).Tag)

        lsvSMTP.SelectedItems(0).Remove()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub mnuTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTest.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        If lsvSMTP.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = lsvSMTP.SelectedItems(0)

        SQL = "SELECT * FROM SMTPServers WHERE SMTPID =" & oItem.Tag

        oRs = clsMarsData.GetData(SQL)

        If oRs.EOF = False Then
            Dim oTest As New clsMarsMessaging

            oTest.TestSMTP(oRs("smtpserver").Value, oRs("smtpuser").Value, Decrypt(oRs("smtppassword").Value, "amani�$%^&*"), _
            oRs("smtptimeout").Value, oRs("smtpsendername").Value, oRs("smtpsenderaddress").Value)

        End If

        oRs.Close()
    End Sub

    Private Sub mnuDefault_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefault.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        If lsvSMTP.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = lsvSMTP.SelectedItems(0)

        SQL = "SELECT * FROM SMTPServers WHERE SMTPID =" & oItem.Tag

        oRs = clsMarsData.GetData(SQL)

        If oRs.EOF = False Then
            Dim oUI As New clsMarsUI
            Dim oSec As New clsMarsSecurity
            Dim sUser As String = oRs("smtpuser").Value
            Dim sPass As String = Decrypt(oRs("smtppassword").Value, "amani�$%^&*")
            Dim szServer As String = oRs("smtpserver").Value
            Dim szSenderAddress As String = oRs("smtpsenderaddress").Value
            Dim szSenderName As String = oRs("smtpsendername").Value
            Dim nTimeout As Integer = oRs("smtptimeout").Value

            If sPass Is Nothing Then sPass = String.Empty

            With oUI
                .SaveRegistry("SMTPUserID", sUser)

                If sPass.Length > 0 Then
                    .SaveRegistry("SMTPPassword", sPass, True)
                End If

                .SaveRegistry("SMTPServer", szServer)
                .SaveRegistry("SMTPSenderAddress", szSenderAddress)
                .SaveRegistry("SMTPSenderName", szSenderName)
                .SaveRegistry("SMTPTimeout", nTimeout)
            End With
        End If

        oRs.Close()
    End Sub

    Private Sub mnuEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEdit.Click
        cmdEdit_Click(sender, e)
    End Sub

    Private Sub lsvSMTP_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvSMTP.DoubleClick
        Dim lsv As ListView = CType(sender, ListView)
        Dim Item As ListViewItem

        If lsv IsNot Nothing Then

            Item = lsv.SelectedItems(0)

            If Item IsNot Nothing Then
                If Item.Checked Then
                    Item.Checked = False
                Else
                    Item.Checked = True
                End If
            End If
        End If

        cmdEdit_Click(sender, e)
    End Sub

    Private Sub lsvSMTP_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvSMTP.SelectedIndexChanged

    End Sub

    Private Sub lsvSMTP_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles lsvSMTP.ItemCheck
        Dim oItem As ListViewItem
        Dim SQL As String

        If Me.Visible = False Then Return

        Try
            oItem = lsvSMTP.Items(e.Index)

            If e.NewValue = CheckState.Checked Then
                SQL = "UPDATE SMTPServers SET IsBackup = 1 WHERE SMTPID = " & oItem.Tag
            ElseIf e.NewValue = CheckState.Unchecked Then
                SQL = "UPDATE SMTPServers SET IsBackup = 0 WHERE SMTPID = " & oItem.Tag
            End If

            clsMarsData.WriteData(SQL)
        Catch : End Try
    End Sub
End Class
