#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
Imports System.Windows.Forms
#End If

Public Class frmPackageProp
    Inherits System.Windows.Forms.Form
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oData As clsMarsData = New clsMarsData
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim ScheduleID As Integer
    Dim m_ParametersRead As Boolean = False
    Dim IsDynamic As Boolean
    Dim owner As String = ""
#Region "Controls"
    Friend WithEvents tabProperties As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbReports As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbOutput As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbException As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbHistory As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbTasks As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbLinking As DevComponents.DotNetBar.TabItem
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSnapshots As DevComponents.DotNetBar.TabItem
    Friend WithEvents cmdView As System.Windows.Forms.Button
    Friend WithEvents chkSnapshots As System.Windows.Forms.CheckBox
    Friend WithEvents cmdExecute As System.Windows.Forms.Button
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents lsvSnapshots As System.Windows.Forms.ListView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvPacks As System.Windows.Forms.TreeView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents grpDynamicTasks As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents btnTest As System.Windows.Forms.Button
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
#End Region
    Dim OkayToClose As Boolean
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents grpSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents grpSelectReport As System.Windows.Forms.GroupBox
    Friend WithEvents chkSelectAll As System.Windows.Forms.CheckBox
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grpAutoResume As System.Windows.Forms.GroupBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents chkAutoResume As System.Windows.Forms.CheckBox
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbDataDriven As DevComponents.DotNetBar.TabItem
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents chkGroupReports As System.Windows.Forms.CheckBox
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents UcDSNDD As sqlrd.ucDSN
    Dim IsLoaded As Boolean = False
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents txtMergeAllXL As System.Windows.Forms.TextBox
    Friend WithEvents txtMergeAllPDF As System.Windows.Forms.TextBox
    Friend WithEvents chkMergeAllXL As System.Windows.Forms.CheckBox
    Friend WithEvents chkMergeAllPDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkMultithreaded As System.Windows.Forms.CheckBox

    Dim IsDataDriven As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdAddReport As System.Windows.Forms.Button
    Friend WithEvents lsvReports As System.Windows.Forms.ListView
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveReport As System.Windows.Forms.Button
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents imgTools As System.Windows.Forms.ImageList
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents ucUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents imgPackage As System.Windows.Forms.ImageList
    Friend WithEvents txtMergePDF As System.Windows.Forms.TextBox
    Friend WithEvents chkMergeXL As System.Windows.Forms.CheckBox
    Friend WithEvents chkMergePDF As System.Windows.Forms.CheckBox
    Friend WithEvents txtMergeXL As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As System.Windows.Forms.CheckBox
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents cmdUp As System.Windows.Forms.Button
    Friend WithEvents cmdDown As System.Windows.Forms.Button
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents chkStaticDest As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackageProp))
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdUp = New System.Windows.Forms.Button
        Me.cmdDown = New System.Windows.Forms.Button
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.chkDTStamp = New System.Windows.Forms.CheckBox
        Me.cmdAddReport = New System.Windows.Forms.Button
        Me.lsvReports = New System.Windows.Forms.ListView
        Me.ReportName = New System.Windows.Forms.ColumnHeader
        Me.Format = New System.Windows.Forms.ColumnHeader
        Me.cmdRemoveReport = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.txtID = New System.Windows.Forms.TextBox
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtMergePDF = New System.Windows.Forms.TextBox
        Me.chkMergeXL = New System.Windows.Forms.CheckBox
        Me.chkMergePDF = New System.Windows.Forms.CheckBox
        Me.txtMergeXL = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdRefresh = New System.Windows.Forms.Button
        Me.imgPackage = New System.Windows.Forms.ImageList(Me.components)
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.imgTools = New System.Windows.Forms.ImageList(Me.components)
        Me.btnView = New System.Windows.Forms.Button
        Me.chkStaticDest = New System.Windows.Forms.CheckBox
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnClear = New System.Windows.Forms.Button
        Me.btnTest = New System.Windows.Forms.Button
        Me.Label21 = New System.Windows.Forms.Label
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbTable = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.optStatic = New System.Windows.Forms.RadioButton
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.optDynamic = New System.Windows.Forms.RadioButton
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.chkAutoResume = New System.Windows.Forms.CheckBox
        Me.chkMergeAllXL = New System.Windows.Forms.CheckBox
        Me.chkMergeAllPDF = New System.Windows.Forms.CheckBox
        Me.tabProperties = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter
        Me.tvPacks = New System.Windows.Forms.TreeView
        Me.lsvSnapshots = New System.Windows.Forms.ListView
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.cmdView = New System.Windows.Forms.Button
        Me.chkSnapshots = New System.Windows.Forms.CheckBox
        Me.cmdExecute = New System.Windows.Forms.Button
        Me.Label11 = New System.Windows.Forms.Label
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown
        Me.tbSnapshots = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel
        Me.Step3 = New System.Windows.Forms.Panel
        Me.txtMergeAllXL = New System.Windows.Forms.TextBox
        Me.txtMergeAllPDF = New System.Windows.Forms.TextBox
        Me.UcDSNDD = New sqlrd.ucDSN
        Me.chkGroupReports = New System.Windows.Forms.CheckBox
        Me.btnConnect = New System.Windows.Forms.Button
        Me.grpQuery = New System.Windows.Forms.GroupBox
        Me.cmbDDKey = New System.Windows.Forms.ComboBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.btnBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tbDataDriven = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbGeneral = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpDynamicTasks = New System.Windows.Forms.GroupBox
        Me.optOnce = New System.Windows.Forms.RadioButton
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.oTask = New sqlrd.ucTasks
        Me.tbTasks = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbLinking = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown
        Me.grpAutoResume = New System.Windows.Forms.GroupBox
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.UcDest = New sqlrd.ucDestination
        Me.tbOutput = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpSelectReport = New System.Windows.Forms.GroupBox
        Me.chkSelectAll = New System.Windows.Forms.CheckBox
        Me.tbReports = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.tbException = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.grpSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.tbHistory = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.ucUpdate = New sqlrd.ucScheduleUpdate
        Me.tbSchedule = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.chkMultithreaded = New System.Windows.Forms.CheckBox
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel10.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.grpQuery.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        Me.grpDynamicTasks.SuspendLayout()
        Me.TabControlPanel8.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpAutoResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel3.SuspendLayout()
        Me.grpSelectReport.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.grpSort.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtDesc
        '
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(4, 122)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(376, 247)
        Me.txtDesc.TabIndex = 4
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 106)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label7, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(4, 382)
        Me.Label7.Name = "Label7"
        Me.HelpProvider1.SetShowHelp(Me.Label7, True)
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 21
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(4, 398)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 5
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdLoc.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(324, 74)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 3
        Me.cmdLoc.Text = "..."
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(4, 74)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 2
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(4, 58)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(4, 26)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "packageGeneral.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 10)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Package Name"
        '
        'cmdUp
        '
        Me.cmdUp.BackColor = System.Drawing.Color.Transparent
        Me.cmdUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdUp, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdUp, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(364, 277)
        Me.cmdUp.Name = "cmdUp"
        Me.HelpProvider1.SetShowHelp(Me.cmdUp, True)
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 5
        Me.cmdUp.Text = "&Up"
        Me.cmdUp.UseVisualStyleBackColor = False
        '
        'cmdDown
        '
        Me.cmdDown.BackColor = System.Drawing.Color.Transparent
        Me.cmdDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDown, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDown, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(364, 306)
        Me.cmdDown.Name = "cmdDown"
        Me.HelpProvider1.SetShowHelp(Me.cmdDown, True)
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 6
        Me.cmdDown.Text = "&Down"
        Me.cmdDown.UseVisualStyleBackColor = False
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.cmbDateTime, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbDateTime, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(213, 12)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.HelpProvider1.SetShowHelp(Me.cmbDateTime, True)
        Me.cmbDateTime.Size = New System.Drawing.Size(178, 21)
        Me.cmbDateTime.TabIndex = 18
        '
        'chkDTStamp
        '
        Me.chkDTStamp.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkDTStamp, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkDTStamp, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(6, 9)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.HelpProvider1.SetShowHelp(Me.chkDTStamp, True)
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 1
        Me.chkDTStamp.Text = "Append date/time stamp"
        Me.chkDTStamp.UseVisualStyleBackColor = False
        '
        'cmdAddReport
        '
        Me.cmdAddReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdAddReport, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdAddReport, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdAddReport.Image = CType(resources.GetObject("cmdAddReport.Image"), System.Drawing.Image)
        Me.cmdAddReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(364, 13)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.HelpProvider1.SetShowHelp(Me.cmdAddReport, True)
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 2
        Me.cmdAddReport.Text = "&Add"
        Me.cmdAddReport.UseVisualStyleBackColor = False
        '
        'lsvReports
        '
        Me.lsvReports.CheckBoxes = True
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvReports, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvReports, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(4, 32)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.HelpProvider1.SetShowHelp(Me.lsvReports, True)
        Me.lsvReports.Size = New System.Drawing.Size(354, 299)
        Me.lsvReports.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.lsvReports, "Checked = Enabled, Unchecked = Disabled")
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 125
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRemoveReport, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRemoveReport, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRemoveReport.Image = CType(resources.GetObject("cmdRemoveReport.Image"), System.Drawing.Image)
        Me.cmdRemoveReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(364, 71)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.HelpProvider1.SetShowHelp(Me.cmdRemoveReport, True)
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 4
        Me.cmdRemoveReport.Text = "&Remove"
        Me.cmdRemoveReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveReport.UseVisualStyleBackColor = False
        '
        'cmdEdit
        '
        Me.cmdEdit.BackColor = System.Drawing.Color.Transparent
        Me.cmdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdEdit, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdEdit, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(364, 42)
        Me.cmdEdit.Name = "cmdEdit"
        Me.HelpProvider1.SetShowHelp(Me.cmdEdit, True)
        Me.cmdEdit.Size = New System.Drawing.Size(75, 24)
        Me.cmdEdit.TabIndex = 3
        Me.cmdEdit.Text = "&Edit"
        Me.cmdEdit.UseVisualStyleBackColor = False
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(73, 432)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(216, 21)
        Me.txtID.TabIndex = 14
        Me.txtID.Visible = False
        '
        'cmdApply
        '
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(555, 435)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(467, 435)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(379, 435)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label5, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(449, 345)
        Me.Label5.Name = "Label5"
        Me.HelpProvider1.SetShowHelp(Me.Label5, True)
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = ".pdf"
        '
        'txtMergePDF
        '
        Me.txtMergePDF.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtMergePDF, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtMergePDF, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtMergePDF.Location = New System.Drawing.Point(282, 343)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.HelpProvider1.SetShowHelp(Me.txtMergePDF, True)
        Me.txtMergePDF.Size = New System.Drawing.Size(157, 21)
        Me.txtMergePDF.TabIndex = 8
        Me.txtMergePDF.Tag = "memo"
        '
        'chkMergeXL
        '
        Me.chkMergeXL.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkMergeXL, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMergeXL, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(4, 366)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.HelpProvider1.SetShowHelp(Me.chkMergeXL, True)
        Me.chkMergeXL.Size = New System.Drawing.Size(253, 32)
        Me.chkMergeXL.TabIndex = 9
        Me.chkMergeXL.Text = "Merge all Excel outputs into a single workbook"
        Me.chkMergeXL.UseVisualStyleBackColor = False
        '
        'chkMergePDF
        '
        Me.chkMergePDF.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkMergePDF, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMergePDF, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(4, 338)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.HelpProvider1.SetShowHelp(Me.chkMergePDF, True)
        Me.chkMergePDF.Size = New System.Drawing.Size(262, 31)
        Me.chkMergePDF.TabIndex = 7
        Me.chkMergePDF.Text = "Merge all PDF outputs into a single  file"
        Me.chkMergePDF.UseVisualStyleBackColor = False
        '
        'txtMergeXL
        '
        Me.txtMergeXL.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtMergeXL, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtMergeXL, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtMergeXL.Location = New System.Drawing.Point(282, 372)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.HelpProvider1.SetShowHelp(Me.txtMergeXL, True)
        Me.txtMergeXL.Size = New System.Drawing.Size(157, 21)
        Me.txtMergeXL.TabIndex = 10
        Me.txtMergeXL.Tag = "memo"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label6, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(449, 374)
        Me.Label6.Name = "Label6"
        Me.HelpProvider1.SetShowHelp(Me.Label6, True)
        Me.Label6.Size = New System.Drawing.Size(32, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = ".xls"
        '
        'cmdClear
        '
        Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdClear, "packageHistory.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdClear, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdClear.Image = CType(resources.GetObject("cmdClear.Image"), System.Drawing.Image)
        Me.cmdClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdClear.Name = "cmdClear"
        Me.HelpProvider1.SetShowHelp(Me.cmdClear, True)
        Me.cmdClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdClear.TabIndex = 0
        Me.cmdClear.Text = "Clear"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRefresh, "packageHistory.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRefresh, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRefresh.Image = CType(resources.GetObject("cmdRefresh.Image"), System.Drawing.Image)
        Me.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRefresh.Location = New System.Drawing.Point(91, 3)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.HelpProvider1.SetShowHelp(Me.cmdRefresh, True)
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 1
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'imgPackage
        '
        Me.imgPackage.ImageStream = CType(resources.GetObject("imgPackage.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgPackage.TransparentColor = System.Drawing.Color.Transparent
        Me.imgPackage.Images.SetKeyName(0, "")
        Me.imgPackage.Images.SetKeyName(1, "")
        Me.imgPackage.Images.SetKeyName(2, "")
        Me.imgPackage.Images.SetKeyName(3, "")
        Me.imgPackage.Images.SetKeyName(4, "")
        Me.imgPackage.Images.SetKeyName(5, "")
        Me.imgPackage.Images.SetKeyName(6, "camera2.ico")
        '
        'imgTools
        '
        Me.imgTools.ImageStream = CType(resources.GetObject("imgTools.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTools.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTools.Images.SetKeyName(0, "")
        Me.imgTools.Images.SetKeyName(1, "")
        Me.imgTools.Images.SetKeyName(2, "")
        Me.imgTools.Images.SetKeyName(3, "")
        Me.imgTools.Images.SetKeyName(4, "")
        '
        'btnView
        '
        Me.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.btnView, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnView, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnView.Location = New System.Drawing.Point(351, 55)
        Me.btnView.Name = "btnView"
        Me.HelpProvider1.SetShowHelp(Me.btnView, True)
        Me.btnView.Size = New System.Drawing.Size(40, 16)
        Me.btnView.TabIndex = 4
        Me.btnView.Text = "..."
        '
        'chkStaticDest
        '
        Me.chkStaticDest.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkStaticDest, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkStaticDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(4, 98)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.HelpProvider1.SetShowHelp(Me.chkStaticDest, True)
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 5
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        Me.chkStaticDest.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.UcDSN)
        Me.GroupBox6.Controls.Add(Me.cmdValidate)
        Me.GroupBox6.Controls.Add(Me.GroupBox2)
        Me.HelpProvider1.SetHelpKeyword(Me.GroupBox6, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox6, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox6.Location = New System.Drawing.Point(4, 128)
        Me.GroupBox6.Name = "GroupBox6"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox6, True)
        Me.GroupBox6.Size = New System.Drawing.Size(416, 288)
        Me.GroupBox6.TabIndex = 17
        Me.GroupBox6.TabStop = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 112)
        Me.UcDSN.TabIndex = 6
        '
        'cmdValidate
        '
        Me.cmdValidate.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 136)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 7
        Me.cmdValidate.Text = "Connect..."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnClear)
        Me.GroupBox2.Controls.Add(Me.btnTest)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbTable)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 160)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(400, 112)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        '
        'btnClear
        '
        Me.btnClear.Location = New System.Drawing.Point(282, 79)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(70, 21)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear"
        Me.btnClear.UseVisualStyleBackColor = True
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(184, 80)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(83, 21)
        Me.btnTest.TabIndex = 11
        Me.btnTest.Text = "Test"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'Label21
        '
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 64)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(352, 16)
        Me.Label21.TabIndex = 9
        Me.Label21.Text = "Please select the column that holds the required xxx"
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 80)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 10
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 32)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 9
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 32)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(384, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Please select the table and column that must equal the provided parameter"
        '
        'optStatic
        '
        Me.optStatic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optStatic, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optStatic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(4, 52)
        Me.optStatic.Name = "optStatic"
        Me.HelpProvider1.SetShowHelp(Me.optStatic, True)
        Me.optStatic.Size = New System.Drawing.Size(149, 22)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate from static data"
        Me.optStatic.UseVisualStyleBackColor = False
        '
        'cmbKey
        '
        Me.HelpProvider1.SetHelpKeyword(Me.cmbKey, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbKey, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(4, 25)
        Me.cmbKey.Name = "cmbKey"
        Me.HelpProvider1.SetShowHelp(Me.cmbKey, True)
        Me.cmbKey.Size = New System.Drawing.Size(296, 21)
        Me.cmbKey.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label23, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label23, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(1, 4)
        Me.Label23.Name = "Label23"
        Me.HelpProvider1.SetShowHelp(Me.Label23, True)
        Me.Label23.Size = New System.Drawing.Size(81, 18)
        Me.Label23.TabIndex = 11
        Me.Label23.Text = "Key Parameter"
        '
        'optDynamic
        '
        Me.optDynamic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optDynamic, "Linking_-_Dynamic_Package_Properties.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDynamic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(159, 52)
        Me.optDynamic.Name = "optDynamic"
        Me.HelpProvider1.SetShowHelp(Me.optDynamic, True)
        Me.optDynamic.Size = New System.Drawing.Size(213, 22)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.TabStop = True
        Me.optDynamic.Text = "Populate with data from database"
        Me.optDynamic.UseVisualStyleBackColor = False
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        Me.chkAutoResume.Location = New System.Drawing.Point(6, 20)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(263, 17)
        Me.chkAutoResume.TabIndex = 0
        Me.chkAutoResume.Text = "Resume failed/errored schedule with cached data"
        Me.ToolTip1.SetToolTip(Me.chkAutoResume, "If the schedule fails mid-flight, next time it runs, it will resume from where it" & _
                " left off instead of starting from the beginning")
        Me.chkAutoResume.UseVisualStyleBackColor = True
        '
        'chkMergeAllXL
        '
        Me.chkMergeAllXL.AutoSize = True
        Me.chkMergeAllXL.Location = New System.Drawing.Point(34, 351)
        Me.chkMergeAllXL.Name = "chkMergeAllXL"
        Me.chkMergeAllXL.Size = New System.Drawing.Size(249, 17)
        Me.chkMergeAllXL.TabIndex = 27
        Me.chkMergeAllXL.Text = "Merge the group into a single Excel Workbook:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllXL, "Selecting this option will over-ride the XL merging settings for the individual r" & _
                "eports (per record merging) .")
        Me.chkMergeAllXL.UseVisualStyleBackColor = True
        '
        'chkMergeAllPDF
        '
        Me.chkMergeAllPDF.AutoSize = True
        Me.chkMergeAllPDF.Location = New System.Drawing.Point(34, 331)
        Me.chkMergeAllPDF.Name = "chkMergeAllPDF"
        Me.chkMergeAllPDF.Size = New System.Drawing.Size(209, 17)
        Me.chkMergeAllPDF.TabIndex = 28
        Me.chkMergeAllPDF.Text = "Merge the group into a single PDF file:"
        Me.ToolTip1.SetToolTip(Me.chkMergeAllPDF, "Selecting this option will over-ride the PDF merging settings for the individual " & _
                "reports (per record merging) .")
        Me.chkMergeAllPDF.UseVisualStyleBackColor = True
        '
        'tabProperties
        '
        Me.tabProperties.CanReorderTabs = True
        Me.tabProperties.Controls.Add(Me.TabControlPanel3)
        Me.tabProperties.Controls.Add(Me.TabControlPanel1)
        Me.tabProperties.Controls.Add(Me.TabControlPanel9)
        Me.tabProperties.Controls.Add(Me.TabControlPanel10)
        Me.tabProperties.Controls.Add(Me.TabControlPanel7)
        Me.tabProperties.Controls.Add(Me.TabControlPanel8)
        Me.tabProperties.Controls.Add(Me.TabControlPanel4)
        Me.tabProperties.Controls.Add(Me.TabControlPanel5)
        Me.tabProperties.Controls.Add(Me.TabControlPanel6)
        Me.tabProperties.Controls.Add(Me.TabControlPanel2)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(642, 429)
        Me.tabProperties.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabIndex = 20
        Me.tabProperties.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabProperties.Tabs.Add(Me.tbGeneral)
        Me.tabProperties.Tabs.Add(Me.tbSchedule)
        Me.tabProperties.Tabs.Add(Me.tbReports)
        Me.tabProperties.Tabs.Add(Me.tbOutput)
        Me.tabProperties.Tabs.Add(Me.tbException)
        Me.tabProperties.Tabs.Add(Me.tbHistory)
        Me.tabProperties.Tabs.Add(Me.tbTasks)
        Me.tabProperties.Tabs.Add(Me.tbLinking)
        Me.tabProperties.Tabs.Add(Me.tbSnapshots)
        Me.tabProperties.Tabs.Add(Me.tbDataDriven)
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.Panel1)
        Me.TabControlPanel9.Controls.Add(Me.cmdView)
        Me.TabControlPanel9.Controls.Add(Me.chkSnapshots)
        Me.TabControlPanel9.Controls.Add(Me.cmdExecute)
        Me.TabControlPanel9.Controls.Add(Me.Label11)
        Me.TabControlPanel9.Controls.Add(Me.cmdDelete)
        Me.TabControlPanel9.Controls.Add(Me.txtSnapshots)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.TabIndex = 9
        Me.TabControlPanel9.TabItem = Me.tbSnapshots
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.ExpandableSplitter1)
        Me.Panel1.Controls.Add(Me.lsvSnapshots)
        Me.Panel1.Controls.Add(Me.tvPacks)
        Me.Panel1.Location = New System.Drawing.Point(4, 38)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(473, 351)
        Me.Panel1.TabIndex = 16
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tvPacks
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(121, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 351)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 16
        Me.ExpandableSplitter1.TabStop = False
        '
        'tvPacks
        '
        Me.tvPacks.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvPacks.FullRowSelect = True
        Me.HelpProvider1.SetHelpKeyword(Me.tvPacks, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.tvPacks, System.Windows.Forms.HelpNavigator.Topic)
        Me.tvPacks.HideSelection = False
        Me.tvPacks.ImageIndex = 0
        Me.tvPacks.ImageList = Me.imgPackage
        Me.tvPacks.Location = New System.Drawing.Point(0, 0)
        Me.tvPacks.Name = "tvPacks"
        Me.tvPacks.SelectedImageIndex = 0
        Me.HelpProvider1.SetShowHelp(Me.tvPacks, True)
        Me.tvPacks.Size = New System.Drawing.Size(121, 351)
        Me.tvPacks.TabIndex = 3
        '
        'lsvSnapshots
        '
        Me.lsvSnapshots.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lsvSnapshots.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSnapshots.FullRowSelect = True
        Me.lsvSnapshots.GridLines = True
        Me.lsvSnapshots.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvSnapshots.HideSelection = False
        Me.lsvSnapshots.Location = New System.Drawing.Point(121, 0)
        Me.lsvSnapshots.Name = "lsvSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.lsvSnapshots, True)
        Me.lsvSnapshots.Size = New System.Drawing.Size(352, 351)
        Me.lsvSnapshots.TabIndex = 4
        Me.lsvSnapshots.UseCompatibleStateImageBehavior = False
        Me.lsvSnapshots.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Entry Date"
        Me.ColumnHeader5.Width = 76
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Path"
        Me.ColumnHeader6.Width = 170
        '
        'cmdView
        '
        Me.cmdView.Enabled = False
        Me.cmdView.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdView, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdView, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdView.Image = CType(resources.GetObject("cmdView.Image"), System.Drawing.Image)
        Me.cmdView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdView.Location = New System.Drawing.Point(4, 395)
        Me.cmdView.Name = "cmdView"
        Me.HelpProvider1.SetShowHelp(Me.cmdView, True)
        Me.cmdView.Size = New System.Drawing.Size(75, 23)
        Me.cmdView.TabIndex = 5
        Me.cmdView.Text = "View"
        '
        'chkSnapshots
        '
        Me.chkSnapshots.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(7, 5)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 1
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        Me.chkSnapshots.UseVisualStyleBackColor = False
        '
        'cmdExecute
        '
        Me.cmdExecute.Enabled = False
        Me.cmdExecute.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdExecute, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdExecute, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdExecute.Image = CType(resources.GetObject("cmdExecute.Image"), System.Drawing.Image)
        Me.cmdExecute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdExecute.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExecute.Location = New System.Drawing.Point(92, 395)
        Me.cmdExecute.Name = "cmdExecute"
        Me.HelpProvider1.SetShowHelp(Me.cmdExecute, True)
        Me.cmdExecute.Size = New System.Drawing.Size(75, 23)
        Me.cmdExecute.TabIndex = 6
        Me.cmdExecute.Text = "Execute"
        Me.cmdExecute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label11, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label11, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(319, 9)
        Me.Label11.Name = "Label11"
        Me.HelpProvider1.SetShowHelp(Me.Label11, True)
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Days"
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDelete, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDelete, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(180, 395)
        Me.cmdDelete.Name = "cmdDelete"
        Me.HelpProvider1.SetShowHelp(Me.cmdDelete, True)
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "Delete"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Snapshots_-_Packages.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(255, 7)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 2
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'tbSnapshots
        '
        Me.tbSnapshots.AttachedControl = Me.TabControlPanel9
        Me.tbSnapshots.Image = CType(resources.GetObject("tbSnapshots.Image"), System.Drawing.Image)
        Me.tbSnapshots.Name = "tbSnapshots"
        Me.tbSnapshots.Text = "Snapshots"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.Step3)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel10.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.TabIndex = 10
        Me.TabControlPanel10.TabItem = Me.tbDataDriven
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.Color.Transparent
        Me.Step3.Controls.Add(Me.txtMergeAllXL)
        Me.Step3.Controls.Add(Me.txtMergeAllPDF)
        Me.Step3.Controls.Add(Me.chkMergeAllXL)
        Me.Step3.Controls.Add(Me.chkMergeAllPDF)
        Me.Step3.Controls.Add(Me.UcDSNDD)
        Me.Step3.Controls.Add(Me.chkGroupReports)
        Me.Step3.Controls.Add(Me.btnConnect)
        Me.Step3.Controls.Add(Me.grpQuery)
        Me.Step3.Controls.Add(Me.Label9)
        Me.Step3.Location = New System.Drawing.Point(33, 23)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(440, 383)
        Me.Step3.TabIndex = 22
        '
        'txtMergeAllXL
        '
        Me.txtMergeAllXL.Enabled = False
        Me.txtMergeAllXL.Location = New System.Drawing.Point(280, 351)
        Me.txtMergeAllXL.Name = "txtMergeAllXL"
        Me.txtMergeAllXL.Size = New System.Drawing.Size(143, 21)
        Me.txtMergeAllXL.TabIndex = 29
        '
        'txtMergeAllPDF
        '
        Me.txtMergeAllPDF.Enabled = False
        Me.txtMergeAllPDF.Location = New System.Drawing.Point(280, 328)
        Me.txtMergeAllPDF.Name = "txtMergeAllPDF"
        Me.txtMergeAllPDF.Size = New System.Drawing.Size(143, 21)
        Me.txtMergeAllPDF.TabIndex = 30
        '
        'UcDSNDD
        '
        Me.UcDSNDD.BackColor = System.Drawing.Color.White
        Me.UcDSNDD.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSNDD.ForeColor = System.Drawing.Color.Navy
        Me.UcDSNDD.Location = New System.Drawing.Point(11, 27)
        Me.UcDSNDD.Name = "UcDSNDD"
        Me.UcDSNDD.Size = New System.Drawing.Size(422, 112)
        Me.UcDSNDD.TabIndex = 6
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        Me.chkGroupReports.Location = New System.Drawing.Point(17, 308)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(404, 17)
        Me.chkGroupReports.TabIndex = 5
        Me.chkGroupReports.Text = "Group reports together by email address/directory (email/disk destination only)"
        Me.chkGroupReports.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(185, 141)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 3
        Me.btnConnect.Text = "&Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'grpQuery
        '
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label10)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(8, 164)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(425, 138)
        Me.grpQuery.TabIndex = 2
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(79, 113)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(171, 21)
        Me.cmbDDKey.TabIndex = 5
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 117)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(63, 13)
        Me.Label10.TabIndex = 4
        Me.Label10.Text = "Key Column"
        '
        'btnBuild
        '
        Me.btnBuild.Location = New System.Drawing.Point(177, 87)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(410, 65)
        Me.txtQuery.TabIndex = 0
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(5, 11)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(141, 13)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "Database Connection Setup"
        '
        'tbDataDriven
        '
        Me.tbDataDriven.AttachedControl = Me.TabControlPanel10
        Me.tbDataDriven.Image = CType(resources.GetObject("tbDataDriven.Image"), System.Drawing.Image)
        Me.tbDataDriven.Name = "tbDataDriven"
        Me.tbDataDriven.Text = "Data Driver"
        Me.tbDataDriven.Visible = False
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.txtID)
        Me.TabControlPanel1.Controls.Add(Me.txtDesc)
        Me.TabControlPanel1.Controls.Add(Me.Label2)
        Me.TabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.TabControlPanel1.Controls.Add(Me.Label3)
        Me.TabControlPanel1.Controls.Add(Me.txtFolder)
        Me.TabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.TabControlPanel1.Controls.Add(Me.txtName)
        Me.TabControlPanel1.Controls.Add(Me.Label4)
        Me.TabControlPanel1.Controls.Add(Me.Label7)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbGeneral
        '
        'tbGeneral
        '
        Me.tbGeneral.AttachedControl = Me.TabControlPanel1
        Me.tbGeneral.Image = CType(resources.GetObject("tbGeneral.Image"), System.Drawing.Image)
        Me.tbGeneral.Name = "tbGeneral"
        Me.tbGeneral.Text = "General"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.grpDynamicTasks)
        Me.TabControlPanel7.Controls.Add(Me.oTask)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.tbTasks
        '
        'grpDynamicTasks
        '
        Me.grpDynamicTasks.BackColor = System.Drawing.Color.Transparent
        Me.grpDynamicTasks.Controls.Add(Me.optOnce)
        Me.grpDynamicTasks.Controls.Add(Me.optAll)
        Me.grpDynamicTasks.Enabled = False
        Me.grpDynamicTasks.Location = New System.Drawing.Point(12, 349)
        Me.grpDynamicTasks.Name = "grpDynamicTasks"
        Me.grpDynamicTasks.Size = New System.Drawing.Size(462, 74)
        Me.grpDynamicTasks.TabIndex = 18
        Me.grpDynamicTasks.TabStop = False
        Me.grpDynamicTasks.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.optOnce, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optOnce, System.Windows.Forms.HelpNavigator.Topic)
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.HelpProvider1.SetShowHelp(Me.optOnce, True)
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.TabStop = True
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAll, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAll, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.HelpProvider1.SetShowHelp(Me.optAll, True)
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 1
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.Transparent
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.oTask, "packageTasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.oTask, System.Windows.Forms.HelpNavigator.Topic)
        Me.oTask.Location = New System.Drawing.Point(7, 6)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.Name = "oTask"
        Me.HelpProvider1.SetShowHelp(Me.oTask, True)
        Me.oTask.Size = New System.Drawing.Size(474, 343)
        Me.oTask.TabIndex = 0
        '
        'tbTasks
        '
        Me.tbTasks.AttachedControl = Me.TabControlPanel7
        Me.tbTasks.Image = CType(resources.GetObject("tbTasks.Image"), System.Drawing.Image)
        Me.tbTasks.Name = "tbTasks"
        Me.tbTasks.Text = "Tasks"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.GroupBox6)
        Me.TabControlPanel8.Controls.Add(Me.chkStaticDest)
        Me.TabControlPanel8.Controls.Add(Me.btnView)
        Me.TabControlPanel8.Controls.Add(Me.Label23)
        Me.TabControlPanel8.Controls.Add(Me.cmbKey)
        Me.TabControlPanel8.Controls.Add(Me.optStatic)
        Me.TabControlPanel8.Controls.Add(Me.optDynamic)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.TabIndex = 8
        Me.TabControlPanel8.TabItem = Me.tbLinking
        '
        'tbLinking
        '
        Me.tbLinking.AttachedControl = Me.TabControlPanel8
        Me.tbLinking.Image = CType(resources.GetObject("tbLinking.Image"), System.Drawing.Image)
        Me.tbLinking.Name = "tbLinking"
        Me.tbLinking.Text = "Linking"
        Me.tbLinking.Visible = False
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.TabControlPanel4.Controls.Add(Me.grpAutoResume)
        Me.TabControlPanel4.Controls.Add(Me.UcDest)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbOutput
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.chkDTStamp)
        Me.GroupBox1.Controls.Add(Me.cmbDateTime)
        Me.GroupBox1.Controls.Add(Me.Label26)
        Me.GroupBox1.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 337)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(488, 73)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label26, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label26, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label26.Location = New System.Drawing.Point(3, 49)
        Me.Label26.Name = "Label26"
        Me.HelpProvider1.SetShowHelp(Me.Label26, True)
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtAdjustStamp, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtAdjustStamp, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtAdjustStamp.Location = New System.Drawing.Point(213, 43)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.HelpProvider1.SetShowHelp(Me.txtAdjustStamp, True)
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 3
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'grpAutoResume
        '
        Me.grpAutoResume.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpAutoResume.Controls.Add(Me.Label1)
        Me.grpAutoResume.Controls.Add(Me.Label12)
        Me.grpAutoResume.Controls.Add(Me.chkAutoResume)
        Me.grpAutoResume.Enabled = False
        Me.grpAutoResume.Location = New System.Drawing.Point(7, 257)
        Me.grpAutoResume.Name = "grpAutoResume"
        Me.grpAutoResume.Size = New System.Drawing.Size(488, 74)
        Me.grpAutoResume.TabIndex = 23
        Me.grpAutoResume.TabStop = False
        Me.grpAutoResume.Text = "Resume with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(213, 41)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 2
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(257, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "minutes"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 45)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(168, 13)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "Expire dynamic cached data after"
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "packageOutput.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(7, 8)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(488, 243)
        Me.UcDest.TabIndex = 0
        '
        'tbOutput
        '
        Me.tbOutput.AttachedControl = Me.TabControlPanel4
        Me.tbOutput.Image = CType(resources.GetObject("tbOutput.Image"), System.Drawing.Image)
        Me.tbOutput.Name = "tbOutput"
        Me.tbOutput.Text = "Output"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.chkMultithreaded)
        Me.TabControlPanel3.Controls.Add(Me.grpSelectReport)
        Me.TabControlPanel3.Controls.Add(Me.Label5)
        Me.TabControlPanel3.Controls.Add(Me.cmdUp)
        Me.TabControlPanel3.Controls.Add(Me.lsvReports)
        Me.TabControlPanel3.Controls.Add(Me.txtMergePDF)
        Me.TabControlPanel3.Controls.Add(Me.cmdDown)
        Me.TabControlPanel3.Controls.Add(Me.chkMergePDF)
        Me.TabControlPanel3.Controls.Add(Me.chkMergeXL)
        Me.TabControlPanel3.Controls.Add(Me.cmdAddReport)
        Me.TabControlPanel3.Controls.Add(Me.cmdEdit)
        Me.TabControlPanel3.Controls.Add(Me.Label6)
        Me.TabControlPanel3.Controls.Add(Me.cmdRemoveReport)
        Me.TabControlPanel3.Controls.Add(Me.txtMergeXL)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbReports
        '
        'grpSelectReport
        '
        Me.grpSelectReport.BackColor = System.Drawing.Color.Transparent
        Me.grpSelectReport.Controls.Add(Me.chkSelectAll)
        Me.grpSelectReport.Location = New System.Drawing.Point(4, -5)
        Me.grpSelectReport.Name = "grpSelectReport"
        Me.grpSelectReport.Size = New System.Drawing.Size(354, 34)
        Me.grpSelectReport.TabIndex = 0
        Me.grpSelectReport.TabStop = False
        '
        'chkSelectAll
        '
        Me.chkSelectAll.AutoSize = True
        Me.chkSelectAll.Checked = True
        Me.chkSelectAll.CheckState = System.Windows.Forms.CheckState.Indeterminate
        Me.chkSelectAll.Location = New System.Drawing.Point(5, 11)
        Me.chkSelectAll.Name = "chkSelectAll"
        Me.chkSelectAll.Size = New System.Drawing.Size(69, 17)
        Me.chkSelectAll.TabIndex = 0
        Me.chkSelectAll.Text = "Select All"
        Me.chkSelectAll.UseVisualStyleBackColor = True
        '
        'tbReports
        '
        Me.tbReports.AttachedControl = Me.TabControlPanel3
        Me.tbReports.Image = CType(resources.GetObject("tbReports.Image"), System.Drawing.Image)
        Me.tbReports.Name = "tbReports"
        Me.tbReports.Text = "Reports"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.UcError)
        Me.TabControlPanel5.Controls.Add(Me.UcBlank)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.tbException
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(4, 5)
        Me.UcError.m_showFailOnOne = True
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(496, 86)
        Me.UcError.TabIndex = 6
        '
        'UcBlank
        '
        Me.UcBlank.BackColor = System.Drawing.Color.Transparent
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcBlank, "packageError_Handling.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcBlank, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcBlank.Location = New System.Drawing.Point(4, 97)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.HelpProvider1.SetShowHelp(Me.UcBlank, True)
        Me.UcBlank.Size = New System.Drawing.Size(491, 324)
        Me.UcBlank.TabIndex = 5
        '
        'tbException
        '
        Me.tbException.AttachedControl = Me.TabControlPanel5
        Me.tbException.Image = CType(resources.GetObject("tbException.Image"), System.Drawing.Image)
        Me.tbException.Name = "tbException"
        Me.tbException.Text = "Exception Handling"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.tvHistory)
        Me.TabControlPanel6.Controls.Add(Me.grpSort)
        Me.TabControlPanel6.Controls.Add(Me.Panel2)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.tbHistory
        '
        'tvHistory
        '
        Me.tvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.tvHistory, "packageHistory.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.tvHistory, System.Windows.Forms.HelpNavigator.Topic)
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgPackage
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(1, 49)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.HelpProvider1.SetShowHelp(Me.tvHistory, True)
        Me.tvHistory.Size = New System.Drawing.Size(505, 346)
        Me.tvHistory.TabIndex = 1
        '
        'grpSort
        '
        Me.grpSort.BackColor = System.Drawing.Color.Transparent
        Me.grpSort.Controls.Add(Me.optAsc)
        Me.grpSort.Controls.Add(Me.optDesc)
        Me.grpSort.Dock = System.Windows.Forms.DockStyle.Top
        Me.grpSort.Location = New System.Drawing.Point(1, 1)
        Me.grpSort.Name = "grpSort"
        Me.grpSort.Size = New System.Drawing.Size(505, 48)
        Me.grpSort.TabIndex = 0
        Me.grpSort.TabStop = False
        Me.grpSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAsc, "packageHistory.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAsc, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.HelpProvider1.SetShowHelp(Me.optAsc, True)
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 0
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.HelpProvider1.SetHelpKeyword(Me.optDesc, "packageHistory.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.HelpProvider1.SetShowHelp(Me.optDesc, True)
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 1
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.cmdClear)
        Me.Panel2.Controls.Add(Me.cmdRefresh)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(1, 395)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(505, 33)
        Me.Panel2.TabIndex = 2
        '
        'tbHistory
        '
        Me.tbHistory.AttachedControl = Me.TabControlPanel6
        Me.tbHistory.Image = CType(resources.GetObject("tbHistory.Image"), System.Drawing.Image)
        Me.tbHistory.Name = "tbHistory"
        Me.tbHistory.Text = "History"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.ucUpdate)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(507, 429)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbSchedule
        '
        'ucUpdate
        '
        Me.ucUpdate.BackColor = System.Drawing.Color.Transparent
        Me.ucUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.ucUpdate, "packageSchedule.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.ucUpdate, System.Windows.Forms.HelpNavigator.Topic)
        Me.ucUpdate.Location = New System.Drawing.Point(7, 3)
        Me.ucUpdate.m_RepeatUnit = ""
        Me.ucUpdate.Name = "ucUpdate"
        Me.HelpProvider1.SetShowHelp(Me.ucUpdate, True)
        Me.ucUpdate.Size = New System.Drawing.Size(503, 376)
        Me.ucUpdate.TabIndex = 1
        '
        'tbSchedule
        '
        Me.tbSchedule.AttachedControl = Me.TabControlPanel2
        Me.tbSchedule.Image = CType(resources.GetObject("tbSchedule.Image"), System.Drawing.Image)
        Me.tbSchedule.Name = "tbSchedule"
        Me.tbSchedule.Text = "Schedule"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'chkMultithreaded
        '
        Me.chkMultithreaded.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkMultithreaded, "packageReport.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkMultithreaded, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkMultithreaded.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMultithreaded.Location = New System.Drawing.Point(4, 395)
        Me.chkMultithreaded.Name = "chkMultithreaded"
        Me.HelpProvider1.SetShowHelp(Me.chkMultithreaded, True)
        Me.chkMultithreaded.Size = New System.Drawing.Size(354, 32)
        Me.chkMultithreaded.TabIndex = 11
        Me.chkMultithreaded.Text = "Run Package Multi-Threaded"
        Me.chkMultithreaded.UseVisualStyleBackColor = False
        '
        'frmPackageProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(642, 467)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.tabProperties)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPackageProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Package Properties"
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.TabControlPanel9.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel10.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.Step3.PerformLayout()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.TabControlPanel7.ResumeLayout(False)
        Me.grpDynamicTasks.ResumeLayout(False)
        Me.grpDynamicTasks.PerformLayout()
        Me.TabControlPanel8.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpAutoResume.ResumeLayout(False)
        Me.grpAutoResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel3.PerformLayout()
        Me.grpSelectReport.ResumeLayout(False)
        Me.grpSelectReport.PerformLayout()
        Me.TabControlPanel5.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        Me.grpSort.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(0, Me.txtID.Text, 0, 0, 0, clsMarsScheduler.enCalcType.MEDIAN, 3)

            Return val
        End Get
    End Property

    Private Sub frmPackageProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error Resume Next
        HelpProvider1.HelpNamespace = gHelpPath

        UcDest.isPackage = True

        FormatForWinXP(Me)

        txtMergePDF.ContextMenu = Me.mnuInserter
        txtMergeXL.ContextMenu = Me.mnuInserter
        Me.txtMergeAllPDF.ContextMenu = Me.mnuInserter
        Me.txtMergePDF.ContextMenu = Me.mnuInserter

        clsMarsUI.MainUI.InitHistoryImages(tvHistory, My.Resources.box)

        IsLoaded = True
    End Sub
    Public Function _GetParameters(ByVal cmb As ComboBox)

    End Function

    Private Sub ViewSnapshots()
        Try
            Dim SQL As String
            Dim parentNode As TreeNode
            Dim oRs As ADODB.Recordset

            SQL = "SELECT * FROM ReportSnapshots WHERE PackID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            If oRs.EOF = False Then
                chkSnapshots.Checked = True
                txtSnapshots.Value = oRs("keepsnap").Value
            End If

            oRs.Close()

            parentNode = tvPacks.Nodes.Add("Snapshots")
            parentNode.Tag = 0
            parentNode.ImageIndex = 6
            parentNode.SelectedImageIndex = 6

            SQL = "SELECT DISTINCT ExecID FROM SnapshotsAttr WHERE PackID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim nodeItem As TreeNode = parentNode.Nodes.Add("InstanceID: " & oRs("execid").Value)
                nodeItem.Tag = oRs("execid").Value
                nodeItem.ImageIndex = 0
                nodeItem.SelectedImageIndex = 0

                oRs.MoveNext()
            Loop

            oRs.Close()

            parentNode.Expand()
        Catch : End Try
    End Sub

    Public Function EditPackage(ByVal nPackID As Int32)
        'On Error Resume Next
        Dim SQL As String
        Dim sWhere As String
        Dim rs As ADODB.Recordset
        Dim I As Integer = 0
        Dim rsP As ADODB.Recordset
        Dim autoCalc As Boolean

        Try
            chkMultithreaded.Checked = False
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
                chkMultithreaded.Enabled = False
            End If

            sWhere = " WHERE PackID = " & nPackID

            SQL = "SELECT * FROM PackageAttr " & sWhere

            rs = clsMarsData.GetData(SQL)

            If rs.EOF = False Then

                txtID.Text = rs("packid").Value
                owner = rs("owner").Value

                UcDest.nReportID = 0
                UcDest.nSmartID = 0
                UcDest.nPackID = txtID.Text
                UcDest.LoadAll()
                autoCalc = IsNull(rs("autocalc").Value, 0)

                txtName.Text = rs("packagename").Value

                Me.Text = "Package Properties - " & txtName.Text

                txtFolder.Tag = rs("parent").Value

                rsP = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & rs("Parent").Value)

                txtFolder.Text = rsP("FolderName").Value

                rsP.Close()

                Dim sDestination

                rsP = clsMarsData.GetData("SELECT DestinationType FROM DestinationAttr WHERE " & _
                "PackID = " & nPackID)

                Try
                    If rsP.EOF = False Then
                        sDestination = rsP.Fields(0).Value
                    End If
                    rsP.Close()

                    Select Case sDestination.ToLower
                        Case "email"
                            Label21.Text = Label21.Text.Replace("xxx", "email address")
                        Case "disk"
                            Label21.Text = Label21.Text.Replace("xxx", "directory")
                        Case "printer"
                            Label21.Text = Label21.Text.Replace("xxx", "printer name")
                        Case "ftp"
                            Label21.Text = Label21.Text.Replace("xxx", "FTP Server")
                    End Select
                Catch : End Try

                Try
                    UcError.cmbRetry.Text = rs("retry").Value
                Catch
                    UcError.cmbRetry.Value = 3
                End Try

                Try
                    UcError.m_autoFailAfter = IIf(rs("assumefail").Value < 2, UcError.m_autoFailAfter = 30, rs("assumefail").Value)
                Catch
                    UcError.m_autoFailAfter = 30
                End Try

                UcError.chkAssumeFail.Checked = True

                UcError.chkAutoCalc.Checked = autoCalc

                If autoCalc = True Then
                    Dim temp As Decimal = Me.m_autoCalcValue

                    UcError.chkAutoCalc.Checked = True
                    UcError.m_autoFailAfter = IIf(temp < 1, 1, temp)
                    UcError.cmbAssumeFail.Enabled = False
                End If

                UcError.txtRetryInterval.Value = IsNull(rs("retryinterval").Value, 0)

                UcError.chkFailonOne.Checked = Convert.ToBoolean(rs("failonone").Value)

                Try
                    chkMergePDF.Checked = Convert.ToBoolean(rs("mergepdf").Value)
                    txtMergePDF.Text = rs("mergepdfname").Value
                Catch : End Try

                Try
                    chkMergeXL.Checked = rs("mergexl").Value
                    txtMergeXL.Text = rs("mergexlname").Value
                Catch : End Try

                If IsNull(rs("dynamictasks").Value, 0) = 0 Then
                    optAll.Checked = True
                Else
                    optOnce.Checked = True
                End If

                UcBlank.chkBlankReport.Checked = rs("checkblank").Value

                Try
                    chkDTStamp.Checked = Convert.ToBoolean(rs("datetimestamp").Value)
                Catch : chkDTStamp.Checked = False : End Try

                cmbDateTime.Text = IsNull(rs("stampformat").Value)

                Try
                    IsDynamic = Convert.ToBoolean(rs("dynamic").Value)
                Catch ex As Exception
                    IsDynamic = False
                End Try

                If IsNull(rs("isdatadriven").Value, "0") = "1" Then
                    IsDataDriven = True
                Else
                    IsDataDriven = False
                End If

                Try
                    Me.chkStaticDest.Checked = Convert.ToBoolean(rs("staticdest").Value)
                Catch ex As Exception
                    Me.chkStaticDest.Checked = False
                End Try

                txtAdjustStamp.Value = IsNull(rs("adjustpackagestamp").Value, 0)

                If (IsDynamic = False And IsDataDriven = False) Then
                    Try
                        If (chkMultithreaded.Enabled = True) Then
                            chkMultithreaded.Checked = rs("multithreaded").Value
                        End If
                    Catch : End Try
                Else
                    chkMultithreaded.Visible = False
                End If

            End If

            rs.Close()

            UcDest.isDynamic = IsDynamic

            If IsDynamic = True Then
                Me.Text = "Dynamic Package Properties"
                Me.grpAutoResume.Enabled = True
            Else
                Me.grpAutoResume.Enabled = False
            End If

            'get the blank report attributes
            If UcBlank.chkBlankReport.Checked = True Then
                rs = clsMarsData.GetData("SELECT * FROM BlankReportAlert " & sWhere)

                Try
                    If rs.EOF = False Then
                        With UcBlank
                            .txtAlertTo.Text = rs("to").Value
                            .txtBlankMsg.Text = rs("msg").Value
                            .txtSubject.Text = IsNull(rs("subject").Value)

                            Select Case rs("type").Value
                                Case "Alert"
                                    .optAlert.Checked = True
                                Case "AlertandReport"
                                    .optAlertandReport.Checked = True
                                Case "Ignore"
                                    .optIgnore.Checked = True
                            End Select

                            Dim query As String = IsNull(rs("customquerydetails").Value)

                            If query.Length > 0 Then
                                .chkCustomQuery.Checked = True
                                .m_customDSN = query.Split("|")(0)
                                .m_customUserID = query.Split("|")(1)
                                .m_customPassword = _DecryptDBValue(query.Split("|")(2))
                                .txtQuery.Text = query.Split("|")(3)
                            End If

                        End With
                        rs.Close()
                    End If
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, "frmSingleProp.EditSchedule", _
                    _GetLineNumber(ex.StackTrace))
                End Try
            End If

            SQL = "SELECT * FROM ScheduleAttr " & sWhere

            rs = clsMarsData.GetData(SQL)

            If Not rs Is Nothing And rs.EOF = False Then
                ScheduleID = rs("scheduleid").Value

                Select Case rs("frequency").Value.ToLower
                    Case "daily"
                        ucUpdate.optDaily.Checked = True
                    Case "weekly"
                        ucUpdate.optWeekly.Checked = True
                    Case "week-daily"
                        ucUpdate.optWeekDaily.Checked = True
                    Case "monthly"
                        ucUpdate.optMonthly.Checked = True
                    Case "yearly"
                        ucUpdate.optYearly.Checked = True
                    Case "custom"
                        ucUpdate.optCustom.Checked = True
                        ucUpdate.cmbCustom.Text = rs("calendarname").Value
                    Case "other"
                        ucUpdate.optOther.Checked = True
                    Case "none"
                        ucUpdate.optNone.Checked = True
                End Select

                ucUpdate.m_fireOthers = True
                ucUpdate.chkRepeat.Enabled = ucUpdate.m_EnableRepeat(ScheduleID)

                If rs("status").Value = 0 Then
                    ucUpdate.chkStatus.Checked = False
                Else
                    ucUpdate.chkStatus.Checked = True
                End If

                Try
                    ucUpdate.m_RepeatUnit = IsNull(rs("repeatunit").Value, "hours")
                Catch ex As Exception
                    ucUpdate.m_RepeatUnit = "hours"
                End Try

                oTask.ScheduleID = rs("scheduleid").Value

                ucUpdate.ScheduleID = rs("scheduleid").Value

                txtDesc.Text = IsNull(rs("description").Value)
                txtKeyWord.Text = IsNull(rs("keyword").Value)

                oTask.LoadTasks()

                Try
                    Dim d As Date

                    d = ConDate(rs("enddate").Value)

                    If d.Year >= 3004 Then
                        ucUpdate.EndDate.Enabled = False
                        ucUpdate.chkNoEnd.Checked = True
                    End If

                Catch : End Try

                Try
                    Dim repeatUntil As String = IsNull(rs("repeatuntil").Value, Now)

                    If repeatUntil.Length > 8 Then
                        ucUpdate.RepeatUntil.Value = ConDateTime(CTimeZ(repeatUntil, dateConvertType.READ))
                    Else
                        ucUpdate.RepeatUntil.Value = CTimeZ(Now.Date & " " & repeatUntil, dateConvertType.READ)
                    End If
                Catch : End Try

                ucUpdate.NextRun.Value = CTimeZ(rs("nextrun").Value, dateConvertType.READ)
                ucUpdate.EndDate.Value = CTimeZ(rs("enddate").Value, dateConvertType.READ)
                ucUpdate.RunAt.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & rs("starttime").Value), dateConvertType.READ)
                ucUpdate.NextRunTime.Value = CTimeZ(rs("nextrun").Value, dateConvertType.READ)
                ucUpdate.chkRepeat.Checked = Convert.ToBoolean(rs("repeat").Value)
                ucUpdate.cmbRpt.Text = IsNonEnglishRegionRead(rs("repeatinterval").Value)
                ucUpdate.StartDate.Value = CTimeZ(ConDate(rs("startdate").Value), dateConvertType.READ)

                ucUpdate.cmbException.Text = IsNull(rs("exceptioncalendar").Value)

                Try
                    ucUpdate.chkException.Checked = Convert.ToBoolean(rs("useexception").Value)
                Catch
                    ucUpdate.chkException.Checked = False
                End Try
            End If

            If ucUpdate.cmbRpt.Text = "" Then ucUpdate.cmbRpt.Text = 0

            rs.Close()

            _LoadReports()

            'dynamic package details
            If IsDynamic = True Then

                grpDynamicTasks.Enabled = True

                tabProperties.Tabs(7).Visible = True
                tabProperties.Tabs(8).Visible = False

                SQL = "SELECT * FROM DynamicLink WHERE PackID =" & nPackID

                rs = clsMarsData.GetData(SQL)

                If Not rs Is Nothing Then
                    If rs.EOF = False Then
                        With UcDSN
                            .cmbDSN.Text = Convert.ToString(rs("constring").Value).Split("|")(0)
                            .txtUserID.Text = Convert.ToString(rs("constring").Value).Split("|")(1)
                            .txtPassword.Text = Convert.ToString(rs("constring").Value).Split("|")(2)
                            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
                        End With


                        cmbTable.Text = Convert.ToString(rs("keycolumn").Value).Split(".")(0)
                        cmbColumn.Text = Convert.ToString(rs("keycolumn").Value).Split(".")(1)

                        Dim valueColumn As String = IsNull(rs("valuecolumn").Value)
                        Dim valueTable As String = ""

                        Try
                            valueTable = valueColumn.Split(".")(0)
                        Catch ex As Exception
                            valueTable = cmbTable.Text
                        End Try

                        If valueTable = cmbTable.Text Then
                            Try
                                cmbValue.Text = Convert.ToString(rs("valuecolumn").Value).Split(".")(1)
                            Catch
                                cmbValue.Text = Convert.ToString(rs("valuecolumn").Value)
                            End Try
                        Else
                            cmbValue.Text = "<Advanced>"
                        End If

                        gTables(0) = cmbTable.Text

                        If IsNull(rs("table2").Value).Length > 0 Then _
                            gTables(1) = rs("table2").Value

                    End If

                    rs.Close()

                    rs = clsMarsData.GetData("SELECT KeyParameter, Query,AutoResume,ExpireCacheAfter FROM DynamicAttr WHERE " & _
                    "PackID = " & nPackID)

                    If Not rs Is Nothing Then
                        If rs.EOF = False Then
                            cmbKey.Text = rs("keyparameter").Value
                            Me.chkAutoResume.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(rs("autoresume").Value, 0)))
                            Me.txtCacheExpiry.Value = IsNull(rs("expirecacheafter").Value, 0)

                            Try
                                If rs("query").Value = "_" Then
                                    optStatic.Checked = True
                                Else
                                    optDynamic.Checked = True
                                End If
                            Catch ex As Exception
                                optStatic.Checked = True
                            End Try
                        End If
                    End If

                End If

            End If

            Try
                'add data-driven schedule data
3120:           If IsDataDriven = True Then
3130:               UcError.m_showFailOnOne = True
3140:               grpDynamicTasks.Enabled = True
3150:               Me.UcDest.isDataDriven = True
                    Me.tbSnapshots.Visible = False
                    Me.tbLinking.Visible = False

3160:               Text = "Data-Driven Package Properties"

3170:               rs = clsMarsData.GetData("SELECT * FROM DataDrivenAttr WHERE PackID =" & nPackID)

3180:               If rs IsNot Nothing Then
3190:                   If rs.EOF = False Then
3200:                       tbDataDriven.Visible = True
3210:                       tbSnapshots.Visible = False

                            Dim sQuery As String
                            Dim conString As String

3220:                       sQuery = rs("sqlquery").Value
3230:                       conString = rs("constring").Value

3240:                       txtQuery.Text = sQuery

3250:                       Try
3260:                           UcError.chkFailonOne.Checked = IsNull(rs("failonone").Value, 0)
3270:                       Catch ex As Exception
3280:                           UcError.chkFailonOne.Checked = False
                            End Try

3290:                       clsMarsReport.populateDataDrivenCache(sQuery, conString, True)

                            Dim DSN, sUser, sPassword As String

3300:                       DSN = conString.Split("|")(0)
3310:                       sUser = conString.Split("|")(1)
3320:                       sPassword = _DecryptDBValue(conString.Split("|")(2))

3330:                       With UcDSNDD
3340:                           .cmbDSN.Text = DSN
3350:                           .txtUserID.Text = sUser
3360:                           .txtPassword.Text = sPassword
                            End With

                            Try
                                Me.chkGroupReports.Checked = IsNull(rs("groupreports").Value, 1)
                            Catch
                                Me.chkGroupReports.Checked = True
                            End Try

                            Try
                                Me.chkMergeAllPDF.Checked = IsNull(rs("mergeallpdf").Value, 0)
                                Me.txtMergeAllPDF.Text = IsNull(rs("mergeallpdfname").Value)
                            Catch : End Try

                            Try
                                Me.chkMergeAllXL.Checked = IsNull(rs("mergeallxl").Value, 0)
                                Me.txtMergeAllXL.Text = IsNull(rs("mergeallxlname").Value)
                            Catch : End Try

3370:                       Dim rsDD As ADODB.Recordset = New ADODB.Recordset
3380:                       Dim ddCon As ADODB.Connection = New ADODB.Connection

3390:                       ddCon.Open(DSN, sUser, sPassword)

3400:                       rsDD.Open(sQuery, ddCon)

                            Dim z As Integer = 0

                            Me.cmbDDKey.Items.Clear()

3410:                       frmRDScheduleWizard.m_DataFields = New Hashtable

3420:                       For Each fld As ADODB.Field In rsDD.Fields
3430:                           frmRDScheduleWizard.m_DataFields.Add(z, fld.Name)
                                Me.cmbDDKey.Items.Add(fld.Name)
3440:                           z += 1
3450:                       Next

                            Try
                                Me.cmbDDKey.Text = IsNull(rs("keycolumn").Value)
                            Catch : End Try

3460:                       rsDD.Close()

3470:                       ddCon.Close()
3480:                       ddCon = Nothing
                        End If

3490:                   rs.Close()
                    End If
                End If
            Catch : End Try

            ViewSnapshots()

            ucUpdate.IsLoaded = True

            Me.ShowDialog(oMain)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Function

    Private Function _LoadReports()
        Dim SQL As String
        Dim nPackID As Integer = txtID.Text
        Dim rs As ADODB.Recordset

        SQL = "SELECT r.ReportTitle, r.ReportID, p.OutputFormat, p.Status FROM " & _
            "ReportAttr r INNER JOIN " & _
            "PackagedReportAttr p ON r.ReportID = p.ReportID WHERE " & _
            "r.PackID =" & nPackID

        SQL &= " ORDER BY PackOrderID"

        rs = clsMarsData.GetData(SQL)

        Dim oListItem As ListViewItem

        If Not rs Is Nothing Then

            lsvReports.Items.Clear()

            Do While rs.EOF = False
                oListItem = lsvReports.Items.Add(rs("reporttitle").Value)

                oListItem.Tag = rs("reportid").Value

                oListItem.SubItems.Add(rs("outputformat").Value)

                If IsNull(rs("status").Value, "0") = "1" Then
                    oListItem.Checked = True
                Else
                    oListItem.Checked = False
                    oListItem.ForeColor = System.Drawing.Color.Gray
                End If

                rs.MoveNext()
            Loop
        End If

        rs.Close()
    End Function

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        On Error Resume Next
        Dim olsv As ListViewItem
        Dim sNewName As String = ""
        Dim sVals As String()

        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim frmPackProp As frmPackedReport = New frmPackedReport
        frmPackProp.IsDataDriven = Me.IsDataDriven

        olsv = lsvReports.SelectedItems(0)

        sVals = frmPackProp.EditReport(olsv.Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)
            oItem.SubItems(1).Text = sVals(1)
        End If

        _LoadReports()
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        On Error Resume Next
        Dim sWhere As String
        Dim SQL As String
        Dim nRepeat As Double = 0
        Dim sPrinters As String = ""

        'validate user entry

        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter a name for the package")
            txtName.Focus()

            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Exit Sub
        ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE, txtID.Text, IsDynamic, IsDataDriven) = False Then
            ep.SetError(txtName, "A package with this name already exists in this folder. Please use a different name.")
            txtName.Focus()
            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Return
        ElseIf txtFolder.Text = "" Then

            ep.SetError(txtFolder, "Please select the parent folder")
            txtFolder.Focus()
            tabProperties.SelectedTabIndex = 0
            OkayToClose = False
            Exit Sub

        ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Text = "" Then
            tabProperties.SelectedTabIndex = 4
            ep.SetError(UcError.cmbAssumeFail, "Please select the amount before a schedule is assumed as failed")
            UcError.cmbAssumeFail.Focus()
            OkayToClose = False
            Exit Sub
        ElseIf UcBlank.ValidateEntries = False Then
            'If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
            'UcBlank.txtAlertTo.Text = "" Then
            '    tabProperties.SelectedTabIndex = 4
            '    ep.SetError(UcBlank.txtAlertTo, "Please specify the blank report alert recipients")
            '    UcBlank.txtAlertTo.Focus()

            '    OkayToClose = False
            '    Exit Sub
            'End If
            OkayToClose = False
            Return
        ElseIf UcDest.lsvDestination.Items.Count = 0 Then
            UcDest.ep.SetError(UcDest.lsvDestination, "Please add at least one destination")
            tabProperties.SelectedTabIndex = 3
            OkayToClose = False
            Return
        ElseIf UcDest.lsvDestination.CheckedItems.Count = 0 Then
            UcDest.ep.SetError(UcDest.lsvDestination, "Please enable at least one destination")
            tabProperties.SelectedTabIndex = 3
            OkayToClose = False
            Return
        ElseIf chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
            ep.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
            tabProperties.SelectedTabIndex = 2
            txtMergePDF.Focus()
            OkayToClose = False
            Return
        ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
            ep.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
            tabProperties.SelectedTabIndex = 2
            txtMergeXL.Focus()
            OkayToClose = False
            Return
        ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Value = 0 Then
            ep.SetError(UcError.cmbAssumeFail, "Please provide a value greater than zero")
            tabProperties.SelectedTabIndex = 4
            UcError.cmbAssumeFail.Focus()
            OkayToClose = False
            Return
        ElseIf Me.ucUpdate.ValidateSchedule(Me.tabProperties, Me.ScheduleID) = False Then
            OkayToClose = False
            Return
        ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
            ep.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
            Me.cmbDateTime.Focus()
            tabProperties.SelectedTab = Me.tbOutput
            Return
        ElseIf IsDynamic = True Then
            If Me.chkStaticDest.Checked = False Then
                If cmbTable.Text.Length = 0 Then
                    ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                    tabProperties.SelectedTab = Me.tbLinking
                    cmbTable.Focus()
                    Return
                ElseIf cmbColumn.Text.Length = 0 Then
                    ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                    tabProperties.SelectedTab = Me.tbLinking
                    cmbColumn.Focus()
                    Return
                ElseIf cmbValue.Text.Length = 0 Then
                    ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                    tabProperties.SelectedTab = Me.tbLinking
                    cmbValue.Focus()
                    Return
                ElseIf UcDSN._Validate = False Then
                    tabProperties.SelectedTab = Me.tbLinking
                    UcDSN.cmbDSN.Focus()
                    Return
                End If
            End If

            If cmbKey.Text.Length = 0 Then
                ep.SetError(cmbKey, "Please select the key parameter")
                cmbKey.Focus()
                Me.tabProperties.SelectedTab = Me.tbLinking
                Return
            ElseIf Me.optDynamic.Checked = False And Me.optStatic.Checked = False Then
                ep.SetError(optStatic, "Please select how the key parameter value is populated")
                optStatic.Focus()
                Return
            End If
        End If

        OkayToClose = True

        sWhere = " WHERE PackID = " & txtID.Text

        Dim dynamicTasks As Integer

        If optAll.Checked Then
            dynamicTasks = 0
        Else
            dynamicTasks = 1
        End If

        SQL = "UPDATE PackageAttr SET " & _
        "PackageName = '" & txtName.Text.Replace("'", "''") & "'," & _
        "Parent = " & txtFolder.Tag & "," & _
        "Retry = " & UcError.cmbRetry.Text.Replace("'", "''") & "," & _
        "AssumeFail = " & UcError.m_autoFailAfter & "," & _
        "CheckBlank = " & Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
        "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
        "MergePDF = " & Convert.ToInt32(chkMergePDF.Checked) & "," & _
        "MergeXL = " & Convert.ToInt32(chkMergeXL.Checked) & "," & _
        "MergePDFName = '" & SQLPrepare(txtMergePDF.Text) & "'," & _
        "MergeXLName = '" & SQLPrepare(txtMergeXL.Text) & "'," & _
        "DateTimeStamp =" & Convert.ToInt32(chkDTStamp.Checked) & "," & _
        "StampFormat ='" & SQLPrepare(cmbDateTime.Text) & "'," & _
        "StaticDest =" & Convert.ToInt32(Me.chkStaticDest.Checked) & ", " & _
        "AdjustPackageStamp =" & txtAdjustStamp.Value & ", " & _
        "DynamicTasks = " & dynamicTasks & "," & _
        "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
        "RetryInterval =" & UcError.txtRetryInterval.Value & "," & _
        "Multithreaded = " & Convert.ToInt32(chkMultithreaded.Checked)

        SQL &= sWhere

        clsMarsData.WriteData(SQL)

        'delete destinations if any have been marked to be deleted
        UcDest.CommitDeletions()

        With ucUpdate
            If .chkRepeat.Checked = True Then
                nRepeat = .cmbRpt.Text
            End If

            If .chkNoEnd.Checked = True Then
                'Dim d As Date = New Date(Now.Year + 1000, Now.Month, Now.Day)
                Dim d As Date = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            SQL = "UPDATE ScheduleAttr SET " & _
                "Frequency = '" & .sFrequency & "', " & _
                "EndDate = '" & .m_endDate & "', " & _
                "NextRun = '" & .m_nextRun & "', " & _
                "StartTime = '" & .m_startTime & "', " & _
                "StartDate = '" & .m_startDate & "'," & _
                "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "RepeatInterval = '" & nRepeat & "'," & _
                "Status = " & Convert.ToInt32(.chkStatus.Checked) & "," & _
                "RepeatUntil = '" & .m_repeatUntil & "'," & _
                "Description = '" & SQLPrepare(txtDesc.Text) & "'," & _
                "KeyWord = '" & SQLPrepare(txtKeyWord.Text) & "', " & _
                "CalendarName ='" & SQLPrepare(.cmbCustom.Text) & "', " & _
                "UseException = " & Convert.ToInt32(.chkException.Checked) & "," & _
                "ExceptionCalendar ='" & SQLPrepare(.cmbException.Text) & "'," & _
                "RepeatUnit ='" & ucUpdate.m_RepeatUnit & "' "

            SQL &= sWhere
        End With

        clsMarsData.WriteData(SQL)

        'save the blank report attributes

        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE PackID = " & txtID.Text)

        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,PackID,Subject,CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("BlankReportAlert", "BlankID") & "," & _
            "'" & sBlankType & "'," & _
            "'" & UcBlank.txtAlertTo.Text.Replace("'", "''") & "'," & _
            "'" & UcBlank.txtBlankMsg.Text.Replace("'", "''") & "'," & _
            txtID.Text & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        'for dynamic
        'for dynamic data
        Dim ValueCol As String
        Dim LinkSQL As String

        If cmbValue.Text.ToLower = "<advanced>" Then
            ValueCol = "<Advanced>"
        Else
            ValueCol = cmbTable.Text & "." & cmbValue.Text
        End If

        SQL = "UPDATE DynamicAttr SET " & _
         "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "', " & _
         "Autoresume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
         "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
         " WHERE PackID =" & txtID.Text

        clsMarsData.WriteData(SQL)

        If ValueCol <> "<Advanced>" Then
            LinkSQL = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & " FROM " & cmbTable.Text

            SQL = "UPDATE DynamicLink SET " & _
            "KeyColumn ='" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
            "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "'," & _
            "LinkSQL ='" & SQLPrepare(LinkSQL) & "'," & _
            "ConString ='" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|") & "'," & _
            "ValueColumn ='" & SQLPrepare(ValueCol) & "' WHERE " & _
            "PackID =" & txtID.Text

            clsMarsData.WriteData(SQL)
        End If

        With UcDSN
            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
        End With

        For Each oItem As ListViewItem In lsvReports.Items
            SQL = "UPDATE PackagedReportAttr SET Status =" & Convert.ToInt32(oItem.Checked) & " WHERE ReportID = " & oItem.Tag

            clsMarsData.WriteData(SQL)
        Next

        If IsDataDriven = True Then
            If Me.cmbDDKey.Text = "" Then
                ep.SetError(Me.cmbDDKey, "Please select the key column")
                Me.cmbDDKey.Focus()

                Me.tabProperties.SelectedTab = Me.tbDataDriven
                Return
            ElseIf Me.chkMergeAllPDF.Checked And Me.txtMergeAllPDF.Text = "" Then
                ep.SetError(Me.txtMergeAllPDF, "Please provide a name of the merged PDF file")
                Me.tabProperties.SelectedTab = Me.tbDataDriven
                Me.txtMergeAllPDF.Focus()
                Return
            ElseIf Me.chkMergeAllXL.Checked And Me.txtMergeAllXL.Text = "" Then
                ep.SetError(Me.txtMergeAllXL, "Please provide a name of the merged Excel file")
                Me.tabProperties.SelectedTab = Me.tbDataDriven
                Me.txtMergeAllXL.Focus()
                Return
            End If

            'check if the selected key column contains unique values only. Going to do this by reading from the data cache

            For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                Dim rows() As DataRow

                'use select to get all the rows where the key column matches the dupKey
                rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                If rows IsNot Nothing Then
                    If rows.Length > 1 Then
                        ep.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                        cmbDDKey.Focus()
                        Me.tabProperties.SelectedTab = Me.tbDataDriven
                        Return
                    End If
                End If
            Next

            SQL = "UPDATE DataDrivenAttr SET " & _
            "SQLQuery = '" & SQLPrepare(txtQuery.Text) & "'," & _
            "ConString = '" & SQLPrepare(Me.UcDSNDD.m_conString) & "', " & _
            "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & ", " & _
            "GroupReports = " & Convert.ToInt32(Me.chkGroupReports.Checked) & ", " & _
            "KeyColumn = '" & SQLPrepare(Me.cmbDDKey.Text) & "', " & _
            "MergeAllPDF =" & Convert.ToInt32(Me.chkMergeAllPDF.Checked) & "," & _
            "MergeAllPDFName = '" & SQLPrepare(Me.txtMergeAllPDF.Text) & "'," & _
            "MergeAllXL = " & Convert.ToInt32(Me.chkMergeAllXL.Checked) & "," & _
            "MergeAllXLName = '" & SQLPrepare(Me.txtMergeAllXL.Text) & "' " & _
            "WHERE PackID =" & txtID.Text

            clsMarsData.WriteData(SQL)
        End If

        'snapshots
        If chkSnapshots.Checked = True Then
            clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE PackID =" & txtID.Text)

            SQL = "INSERT INTO ReportSnapshots (SnapID,PackID,KeepSnap) "

            SQL &= "VALUES (" & clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
            txtID.Text & "," & _
            txtSnapshots.Value & ")"

            clsMarsData.WriteData(SQL)
        Else
            clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE PackID =" & txtID.Text)

            Dim rsd As ADODB.Recordset

            rsd = clsMarsData.GetData("SELECT SnapPath FROM SnapshotsAttr WHERE PackID =" & txtID.Text)

            Do While rsd.EOF = False

                System.IO.File.Delete(rsd("snappath").Value)

                rsd.MoveNext()
            Loop

            rsd.Close()


            clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE PackID =" & txtID.Text)
        End If

        cmdApply.Enabled = False

        _LoadReports()

        Me.oTask.CommitDeletions()

        OkayToClose = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        On Error Resume Next
        Dim lsv As ListViewItem
        Dim SQL As String
        Dim I As Integer = 1

        cmdApply_Click(sender, e)

        If OkayToClose = False Then Return

        Me.Close()

        Dim oUI As New clsMarsUI

        oUI.RefreshView(oWindow(nWindowCurrent))

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        On Error Resume Next
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        On Error Resume Next
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If

        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        On Error Resume Next
        oUI.ResetError(lsvReports, ep)
        cmdApply.Enabled = True

        Dim oForm As frmPackedReport
        Dim sReturn() As String
        Dim rs As ADODB.Recordset
        Dim SQL As String
        Dim nCount As Integer

        SQL = "SELECT MAX(PackOrderID) FROM ReportAttr WHERE PackID =" & txtID.Text

        rs = clsMarsData.GetData(SQL)

        If rs Is Nothing Then
            nCount = 1
        Else
            If rs.EOF = False Then
                Dim sTemp As String = rs.Fields(0).Value

                nCount = sTemp

                nCount += 1
            Else
                nCount = 1
            End If
        End If

        rs.Close()

        oForm = New frmPackedReport
        oForm.IsDataDriven = Me.IsDataDriven
        sReturn = oForm.AddReport(nCount, txtID.Text)

        If sReturn(0).Length > 0 Then
            Dim lsv As ListViewItem = lsvReports.Items.Add(sReturn(0))

            lsv.Tag = sReturn(1)

            lsv.SubItems.Add(sReturn(2))
        End If

        Me._LoadReports()
    End Sub



    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub


    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim SQL As String
        Dim sWhere As String
        Dim Response As DialogResult


        Response = MessageBox.Show("Delete the selected report from the package?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If Response = DialogResult.Yes Then

            oUI.DeleteReport(lsvReports.SelectedItems(0).Tag, txtID.Text)

            lsvReports.Items.Remove(lsvReports.SelectedItems(0))

        End If
    End Sub



    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        txtMergePDF.Enabled = chkMergePDF.Checked

        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergePDF.Checked = False
            Return
        End If

        If chkMergePDF.Checked = True And IsLoaded = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("PDF", txtID.Text)
        End If

    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.xl1_AdvancedXLPack) Then
            If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True And IsLoaded = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("XLS", txtID.Text)
        End If
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMergePDF.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtMergeXL.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub


    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked
        txtAdjustStamp.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Value = 0
        Else
            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")
        End If
    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim oData As New clsMarsData

            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE PackID =" & txtID.Text, False)

            clsMarsData.WriteData("DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory)")

            _Delay(1)

            Dim res As DialogResult = MessageBox.Show("Would you like to remove this schedule's report duration information?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT ReportID FROM ReportAttr WHERE PackID=" & txtID.Text)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim reportID As Integer = oRs("reportid").Value

                    If res = Windows.Forms.DialogResult.Yes Then
                        clsMarsData.WriteData("DELETE FROM ReportDuration WHERE ReportID =" & reportID)
                        clsMarsData.WriteData("DELETE FROM ReportDurationTracker WHERE ReportID =" & reportID)
                    End If

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            cmdRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        If txtID.Text.Length = 0 Then Return

        grpSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        If optDesc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.PACKAGE, txtID.Text, " DESC ")
        Else
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.PACKAGE, txtID.Text, " ASC ")
        End If

        grpSort.Enabled = True
    End Sub

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Index + 1 'Text.Split(":")(0)
        OldID = olsv.Index + 1  '.Text.Split(":")(0)

        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "'," & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        Me._LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                olsv.EnsureVisible()
                Exit For
            End If
        Next
    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Index + 1  'Text.Split(":")(0)
        OldID = olsv.Index + 1 'Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        Me._LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                olsv.EnsureVisible()
                Exit For
            End If
        Next
    End Sub



    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        Dim oGet As New frmKeyParameter


        sVal = oGet._GetParameterValue(cmbKey.Text, sVal)

        If sVal.Length = 0 Then Return


        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE PackID=" & txtID.Text)

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
        "Query,PackID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(sVal) & "'," & _
        "'_'," & _
        "'_'," & _
        txtID.Text

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Private Sub optDynamic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDynamic.CheckedChanged

    End Sub

    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "package", , txtID.Text)

        If sParameter.Length = 0 Then Return


    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If optStatic.Checked = True Then
            optStatic_Click(sender, e)
        ElseIf optDynamic.Checked = True Then
            optDynamic_Click(sender, e)
        End If
    End Sub

    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        GroupBox6.Enabled = Not chkStaticDest.Checked

        UcDest.StaticDest = chkStaticDest.Checked
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)

            oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            cmbValue.Items.Add("<Advanced>")
            cmbValue.Sorted = True
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1912)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)


        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            oForm._AdvancedDynamicEdit(txtID.Text, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        lsvReports_DoubleClick(sender, e)
    End Sub

    Private Sub tabProperties_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "history"
                Me.cmdRefresh_Click(sender, e)
        End Select
    End Sub

    Private Sub tvPacks_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvPacks.AfterSelect
        Try
            Dim node As TreeNode = e.Node
            Dim execID As Integer = node.Tag
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            lsvSnapshots.Items.Clear()

            SQL = "SELECT * FROM SnapshotsAttr WHERE ExecID = " & execID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim item As ListViewItem = lsvSnapshots.Items.Add(oRs("datecreated").Value)
                Dim file As String = oRs("snappath").Value

                If IO.File.Exists(file) = True Then
                    item.SubItems.Add(file)
                Else
                    clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE AttrID =" & oRs("attrid").Value)
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Private Sub cmdView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdView.Click
        If lsvSnapshots.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In lsvSnapshots.SelectedItems
            Process.Start(item.SubItems(1).Text)
        Next
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If tvPacks.SelectedNode Is Nothing Then Return

        If MessageBox.Show("Delete the selected package snapshot?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) _
        = Windows.Forms.DialogResult.Yes Then
            Dim SQL As String = "DELETE FROM SnapshotsAttr WHERE ExecID =" & tvPacks.SelectedNode.Tag

            clsMarsData.WriteData(SQL)
            tvPacks.SelectedNode.Remove()
            lsvSnapshots.Items.Clear()
        End If
    End Sub

    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If Not (IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa8_Snapshots)) And chkSnapshots.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPRO)
            chkSnapshots.Checked = False
            Return
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
        cmdExecute.Enabled = chkSnapshots.Checked
        cmdDelete.Enabled = chkSnapshots.Checked
        cmdView.Enabled = chkSnapshots.Checked
    End Sub

    Private Sub cmdExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExecute.Click
        Try
            If tvPacks.SelectedNode Is Nothing Then Return

            Dim SQL As String = "SELECT * FROM SnapshotsAttr WHERE ExecID =" & tvPacks.SelectedNode.Tag
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)
            Dim files() As String
            Dim packid As Integer
            Dim i As Integer = 0

            If oRs Is Nothing Then Return

            ReDim files(0)

            Do While oRs.EOF = False
                packid = oRs("packid").Value

                ReDim Preserve files(i)

                files(i) = oRs("snappath").Value

                i += 1

                oRs.MoveNext()
            Loop

            oRs.Close()

            Dim osnap As New clsMarsSnapshots

            osnap.ExecuteSnapshot(packid, files)
        Catch ex As Exception
            _ErrorHandle("The snapshot is either missing or corrupt - " & vbCrLf & ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), "")
        End Try
    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE PackID= " & txtID.Text

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)

                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Undo()

    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        Dim ctrl As TextBox

        ctrl = CType(Me.ActiveControl, TextBox)

        ctrl.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        'On Error Resume Next
        'Dim intruder As frmInserter = New frmInserter(99999)
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.m_ParameterList = Nothing
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)

        'intruder.GetConstants(Me, Me.ActiveControl)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim getData As frmDataItems = New frmDataItems

        Dim ctrl As TextBox = CType(Me.ActiveControl, TextBox)

        ctrl.Text = getData._GetDataItem(99999)
    End Sub

    Private Sub cmbKey_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbKey.DropDown
        If Me.m_ParametersRead = False Then

            Dim saveCursor As Cursor = Cursor.Current
            Try
                Cursor.Current = Cursors.WaitCursor
                Me._GetParameters(Me.cmbKey)
            Finally
                Cursor.Current = saveCursor
            End Try

            Me.m_ParametersRead = True
        End If
    End Sub




    Private Sub TabControlPanel9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabControlPanel9.Click

    End Sub

    Private Sub tvHistory_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvHistory.AfterSelect

    End Sub

    Private Sub chkSelectAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelectAll.CheckedChanged
        If chkSelectAll.Checked = True And chkSelectAll.CheckState = CheckState.Checked Then
            Dim olsv As ListViewItem
            For Each olsv In lsvReports.Items
                olsv.Checked = True
            Next
        ElseIf chkSelectAll.Checked = False And chkSelectAll.CheckState = CheckState.Unchecked Then
            Dim olsv As ListViewItem
            For Each olsv In lsvReports.Items
                olsv.Checked = False
            Next
        End If
    End Sub
    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        grpQuery.Enabled = UcDSNDD._Validate
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""
        Dim temp As String = txtQuery.Text

        If UcDSNDD.cmbDSN.Text.Length > 0 Then
            sCon = UcDSNDD.cmbDSN.Text & "|" & UcDSNDD.txtUserID.Text & "|" & UcDSNDD.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSNDD.cmbDSN.Text = sCon.Split("|")(0)
        UcDSNDD.txtUserID.Text = sCon.Split("|")(1)
        UcDSNDD.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSNDD.cmbDSN.Text, UcDSNDD.txtUserID.Text, UcDSNDD.txtPassword.Text)

        oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Dim I As Integer = 0

        Me.cmbDDKey.Items.Clear()

        frmRDScheduleWizard.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            frmRDScheduleWizard.m_DataFields.Add(I, fld.Name)
            Me.cmbDDKey.Items.Add(fld.Name)
            I += 1
        Next

        Dim tmpCon As String = ""

        With UcDSNDD
            tmpCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        If txtQuery.Text.ToLower <> temp.ToLower Then
            clsMarsReport.populateDataDrivenCache(txtQuery.Text, tmpCon, True)
        End If

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing
    End Sub

    Private Sub cmbDDKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDDKey.SelectedIndexChanged
        ep.SetError(Me.cmbDDKey, "")
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to clear all the Dynamic linking information for this schedule?", _
       Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE PackID=" & Me.txtID.Text)

            With UcDSN
                .cmbDSN.Text = ""
                .txtPassword.Text = ""
                .txtUserID.Text = ""
            End With

            Me.cmbTable.Text = ""
            cmbValue.Text = ""
            cmbColumn.Text = ""
        End If
    End Sub
    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.chkMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.chkMergeAllXL.Enabled = chkGroupReports.Checked

        Me.txtMergeAllPDF.Enabled = chkGroupReports.Checked
        Me.txtMergeAllXL.Enabled = Me.chkGroupReports.Checked
    End Sub

    Private Sub chkMergeAllPDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllPDF.CheckedChanged
        Me.txtMergeAllPDF.Enabled = Me.chkMergeAllPDF.Checked
        Me.chkMergePDF.Enabled = Not Me.chkMergeAllPDF.Checked

        If chkMergeAllPDF.Checked Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.pd1_AdvancedPDFPack) = False Then
                If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                chkMergeAllPDF.Checked = False
                Return
            End If

            Me.chkMergePDF.Checked = False

            If IsLoaded = True Then
                Dim oOptions As New frmRptOptions

                oOptions.PackageOptions("PDF", Me.txtID.Text)
            End If
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub

    Private Sub chkMergeAllXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeAllXL.CheckedChanged
        Me.txtMergeAllXL.Enabled = Me.chkMergeAllXL.Checked
        Me.chkMergeXL.Enabled = Not chkMergeAllXL.Checked

        If chkMergeAllXL.Checked = True Then
            If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.xl1_AdvancedXLPack) = False Then
                If IsLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                chkMergeAllXL.Checked = False
                Return
            End If

            Me.chkMergeXL.Checked = False

            If IsLoaded = True Then
                Dim oOptions As New frmRptOptions

                oOptions.PackageOptions("XLS", Me.txtID.Text)
            End If
            'Else
            '    clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID = " & Me.m_PackID, False)
        End If
    End Sub

    Private Sub txtMergeAllPDF_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.Validated, txtMergeAllXL.Validated
        Dim txt As TextBox = CType(sender, TextBox)

        If txt.Text.Contains("<[r]") Then
            MessageBox.Show("Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            txt.Focus()
            ep.SetError(txt, "Using a Data-Driven Constant in this field is not advisable as the schedule will always use the value from last record in the Data-Driver")
        End If
    End Sub

    Private Sub txtMergeAllPDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeAllPDF.TextChanged, txtMergeAllXL.TextChanged
        ep.SetError(CType(sender, TextBox), "")
    End Sub

    Private Sub chkMultithreaded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.chkMultithreaded.Checked And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, "Multi-Threading")
            Me.chkMultithreaded.Checked = False
        End If
    End Sub
End Class
