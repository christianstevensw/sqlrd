Public Class frmAdhocSend
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
    Dim PackID As Integer
    Dim ReportID As Integer
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtBody As System.Windows.Forms.TextBox
    Friend WithEvents cmdSend As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbMailFormat As System.Windows.Forms.ComboBox
    Friend WithEvents cmdSpell As System.Windows.Forms.Button
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAdhocSend))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtBody = New System.Windows.Forms.TextBox
        Me.cmdSend = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.mnuDB = New System.Windows.Forms.MenuItem
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbMailFormat = New System.Windows.Forms.ComboBox
        Me.cmdSpell = New System.Windows.Forms.Button
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtSubject)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtBody)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 192)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtSubject
        '
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(80, 16)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(360, 21)
        Me.txtSubject.TabIndex = 0
        Me.txtSubject.Tag = "memo"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 16)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Subject"
        '
        'txtBody
        '
        Me.txtBody.Location = New System.Drawing.Point(8, 48)
        Me.txtBody.Multiline = True
        Me.txtBody.Name = "txtBody"
        Me.txtBody.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtBody.Size = New System.Drawing.Size(432, 136)
        Me.txtBody.TabIndex = 1
        Me.txtBody.Tag = "memo"
        '
        'cmdSend
        '
        Me.cmdSend.Image = CType(resources.GetObject("cmdSend.Image"), System.Drawing.Image)
        Me.cmdSend.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSend.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSend.Location = New System.Drawing.Point(336, 208)
        Me.cmdSend.Name = "cmdSend"
        Me.cmdSend.Size = New System.Drawing.Size(64, 23)
        Me.cmdSend.TabIndex = 50
        Me.cmdSend.Text = "Send"
        Me.cmdSend.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdCancel
        '
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(408, 208)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(64, 23)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 210)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 16)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Mail Format"
        '
        'cmbMailFormat
        '
        Me.cmbMailFormat.ItemHeight = 13
        Me.cmbMailFormat.Items.AddRange(New Object() {"TEXT", "HTML"})
        Me.cmbMailFormat.Location = New System.Drawing.Point(80, 208)
        Me.cmbMailFormat.Name = "cmbMailFormat"
        Me.cmbMailFormat.Size = New System.Drawing.Size(128, 21)
        Me.cmbMailFormat.TabIndex = 1
        '
        'cmdSpell
        '
        Me.cmdSpell.Image = CType(resources.GetObject("cmdSpell.Image"), System.Drawing.Image)
        Me.cmdSpell.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSpell.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSpell.Location = New System.Drawing.Point(240, 208)
        Me.cmdSpell.Name = "cmdSpell"
        Me.cmdSpell.Size = New System.Drawing.Size(88, 23)
        Me.cmdSpell.TabIndex = 2
        Me.cmdSpell.Text = "Spell Check"
        Me.cmdSpell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'frmAdhocSend
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(498, 240)
        Me.Controls.Add(Me.cmdSpell)
        Me.Controls.Add(Me.cmbMailFormat)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdSend)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAdhocSend"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ad-Hoc Email to All Schedule Recipients"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAdhocSend_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        If MailType = MarsGlobal.gMailType.MAPI Or MailType = gMailType.GROUPWISE Then
            cmbMailFormat.Text = "TEXT"
            cmbMailFormat.Enabled = False
        Else
            cmbMailFormat.Text = "TEXT"
        End If
    End Sub

    Public Sub AdHocSend(Optional ByVal nReportID As Integer = 0, _
          Optional ByVal nPackID As Integer = 0)
        ReportID = nReportID
        PackID = nPackID

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sID As String
        Dim SQL As String
        Dim nCount As Integer

        If PackID > 0 Then
            SQL = "SELECT COUNT(*) FROM DestinationAttr WHERE DestinationType = 'Email' AND PackID = " & nPackID
        Else
            SQL = "SELECT COUNT(*) FROM DestinationAttr WHERE DestinationType = 'Email' AND ReportID =" & nReportID
        End If

        oRs = clsMarsData.GetData(SQL)

        Try
            nCount = oRs.Fields(0).Value
        Catch ex As Exception
            nCount = 0
        End Try

        If nCount = 0 Then
            MessageBox.Show("The selected schedule does not have any email address", _
            Application.ProductName, MessageBoxButtons.OK, _
             MessageBoxIcon.Exclamation)
            Return
        Else
            Me.ShowDialog()
        End If
    End Sub

    Private Sub cmdSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSend.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim sTo As String
        Dim sCc As String
        Dim sBcc As String
        Dim sExtras As String
        Dim oMsg As New clsMarsMessaging
        Dim oParse As New clsMarsParser
        Dim ok As Boolean

        If PackID > 0 Then
            SQL = "SELECT Sendto,Cc,Bcc,Extras FROM DestinationAttr WHERE " & _
            "DestinationType = 'Email' AND PackID=" & PackID
        Else
            SQL = "SELECT Sendto,Cc,Bcc,Extras FROM DestinationAttr WHERE " & _
            "DestinationType = 'Email' AND ReportID=" & ReportID
        End If

        oRs = clsMarsData.GetData(SQL)

        Try

            Do While oRs.EOF = False
                sTo = oMsg.ResolveEmailAddress(oRs("sendto").Value)
                sCc = oMsg.ResolveEmailAddress(oRs("cc").Value)
                sBcc = oMsg.ResolveEmailAddress(oRs("bcc").Value)
                sExtras = oParse.ParseString(oRs("extras").Value)

                If MailType = MarsGlobal.gMailType.MAPI Then
                    ok = oMsg.SendMAPI(sTo, _
                    txtSubject.Text, txtBody.Text, "", , 0, sExtras, _
                        sCc, sBcc, False, "", "", True)
                ElseIf MailType = MarsGlobal.gMailType.SMTP Then
                    ok = oMsg.SendSMTP(sTo, txtSubject.Text, _
                    txtBody.Text, "Single", , 0, sExtras, sCc, _
                    sBcc, "", False, "", True, False, _
                    cmbMailFormat.Text)
                ElseIf MailType = gMailType.GROUPWISE Then
                    ok = oMsg.SendGROUPWISE(sTo, sCc, sBcc, txtSubject.Text, txtBody.Text, "", "Single", sExtras, _
                    , , , , , , cmbMailFormat.Text)
                End If
                oRs.MoveNext()
            Loop

            oRs.Close()

            If ok = True Then
                MessageBox.Show("Emails sent successfully", Application.ProductName, _
                 MessageBoxButtons.OK, MessageBoxIcon.Information)
                Close()
            Else
                MessageBox.Show("Email not sent: " & gErrorDesc, Application.ProductName, _
                 MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If


        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSpell.Click
        With Speller
            .TextBoxBaseToCheck = txtBody
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub
End Class
