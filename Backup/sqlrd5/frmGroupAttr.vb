Imports sqlrd.clsMarsData
Public Class frmGroupAttr
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtGroupName As System.Windows.Forms.TextBox
    Friend WithEvents txtGroupDesc As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tbGroups As System.Windows.Forms.TabControl
    Friend WithEvents chkAll As System.Windows.Forms.CheckBox
    Friend WithEvents chkSystemMonitor As System.Windows.Forms.CheckBox
    Friend WithEvents chkOptions As System.Windows.Forms.CheckBox
    Friend WithEvents chkMigration As System.Windows.Forms.CheckBox
    Friend WithEvents chkUserManager As System.Windows.Forms.CheckBox
    Friend WithEvents chkCompact As System.Windows.Forms.CheckBox
    Friend WithEvents chkBackup As System.Windows.Forms.CheckBox
    Friend WithEvents chkCustomCalendar As System.Windows.Forms.CheckBox
    Friend WithEvents chkRemoteAdmin As System.Windows.Forms.CheckBox
    Friend WithEvents chkCreateSchedules As System.Windows.Forms.CheckBox
    Friend WithEvents chkDeleteSchedules As System.Windows.Forms.CheckBox
    Friend WithEvents chkFolderManager As System.Windows.Forms.CheckBox
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents chkSmartFolders As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmGroupAttr))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtGroupName = New System.Windows.Forms.TextBox
        Me.txtGroupDesc = New System.Windows.Forms.TextBox
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblStep = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.tbGroups = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkAll = New System.Windows.Forms.CheckBox
        Me.chkSystemMonitor = New System.Windows.Forms.CheckBox
        Me.chkOptions = New System.Windows.Forms.CheckBox
        Me.chkMigration = New System.Windows.Forms.CheckBox
        Me.chkUserManager = New System.Windows.Forms.CheckBox
        Me.chkCompact = New System.Windows.Forms.CheckBox
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.chkCustomCalendar = New System.Windows.Forms.CheckBox
        Me.chkRemoteAdmin = New System.Windows.Forms.CheckBox
        Me.chkCreateSchedules = New System.Windows.Forms.CheckBox
        Me.chkDeleteSchedules = New System.Windows.Forms.CheckBox
        Me.chkFolderManager = New System.Windows.Forms.CheckBox
        Me.chkSmartFolders = New System.Windows.Forms.CheckBox
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbGroups.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Description"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Group Name"
        '
        'txtGroupName
        '
        Me.txtGroupName.Location = New System.Drawing.Point(8, 24)
        Me.txtGroupName.Name = "txtGroupName"
        Me.txtGroupName.Size = New System.Drawing.Size(288, 21)
        Me.txtGroupName.TabIndex = 0
        '
        'txtGroupDesc
        '
        Me.txtGroupDesc.Location = New System.Drawing.Point(8, 72)
        Me.txtGroupDesc.Multiline = True
        Me.txtGroupDesc.Name = "txtGroupDesc"
        Me.txtGroupDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtGroupDesc.Size = New System.Drawing.Size(288, 184)
        Me.txtGroupDesc.TabIndex = 1
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(280, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(346, 64)
        Me.Panel1.TabIndex = 2
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(232, 32)
        Me.lblStep.TabIndex = 3
        Me.lblStep.Text = "Group Editor"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(184, 392)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(264, 392)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 59
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.SystemColors.Control
        Me.GroupBox2.Location = New System.Drawing.Point(-8, 376)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(368, 8)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "3dline"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'tbGroups
        '
        Me.tbGroups.Controls.Add(Me.TabPage1)
        Me.tbGroups.Controls.Add(Me.TabPage2)
        Me.tbGroups.Location = New System.Drawing.Point(8, 72)
        Me.tbGroups.Name = "tbGroups"
        Me.tbGroups.SelectedIndex = 0
        Me.tbGroups.Size = New System.Drawing.Size(328, 296)
        Me.tbGroups.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.txtGroupName)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.txtGroupDesc)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(320, 270)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Controls.Add(Me.chkAll)
        Me.TabPage2.Controls.Add(Me.chkSystemMonitor)
        Me.TabPage2.Controls.Add(Me.chkOptions)
        Me.TabPage2.Controls.Add(Me.chkMigration)
        Me.TabPage2.Controls.Add(Me.chkUserManager)
        Me.TabPage2.Controls.Add(Me.chkCompact)
        Me.TabPage2.Controls.Add(Me.chkBackup)
        Me.TabPage2.Controls.Add(Me.chkCustomCalendar)
        Me.TabPage2.Controls.Add(Me.chkRemoteAdmin)
        Me.TabPage2.Controls.Add(Me.chkCreateSchedules)
        Me.TabPage2.Controls.Add(Me.chkDeleteSchedules)
        Me.TabPage2.Controls.Add(Me.chkFolderManager)
        Me.TabPage2.Controls.Add(Me.chkSmartFolders)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(320, 270)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Permissions"
        '
        'GroupBox3
        '
        Me.GroupBox3.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(304, 8)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Tag = "3dline"
        '
        'chkAll
        '
        Me.chkAll.Location = New System.Drawing.Point(8, 8)
        Me.chkAll.Name = "chkAll"
        Me.chkAll.Size = New System.Drawing.Size(104, 24)
        Me.chkAll.TabIndex = 0
        Me.chkAll.Text = "All"
        '
        'chkSystemMonitor
        '
        Me.chkSystemMonitor.Location = New System.Drawing.Point(8, 48)
        Me.chkSystemMonitor.Name = "chkSystemMonitor"
        Me.chkSystemMonitor.Size = New System.Drawing.Size(104, 24)
        Me.chkSystemMonitor.TabIndex = 1
        Me.chkSystemMonitor.Text = "System Monitor"
        '
        'chkOptions
        '
        Me.chkOptions.Location = New System.Drawing.Point(8, 80)
        Me.chkOptions.Name = "chkOptions"
        Me.chkOptions.Size = New System.Drawing.Size(104, 24)
        Me.chkOptions.TabIndex = 3
        Me.chkOptions.Text = "Options"
        '
        'chkMigration
        '
        Me.chkMigration.Location = New System.Drawing.Point(8, 112)
        Me.chkMigration.Name = "chkMigration"
        Me.chkMigration.Size = New System.Drawing.Size(112, 24)
        Me.chkMigration.TabIndex = 5
        Me.chkMigration.Text = "System Migration"
        '
        'chkUserManager
        '
        Me.chkUserManager.Location = New System.Drawing.Point(8, 144)
        Me.chkUserManager.Name = "chkUserManager"
        Me.chkUserManager.Size = New System.Drawing.Size(104, 24)
        Me.chkUserManager.TabIndex = 7
        Me.chkUserManager.Text = "User Manager"
        '
        'chkCompact
        '
        Me.chkCompact.Location = New System.Drawing.Point(8, 176)
        Me.chkCompact.Name = "chkCompact"
        Me.chkCompact.Size = New System.Drawing.Size(112, 24)
        Me.chkCompact.TabIndex = 9
        Me.chkCompact.Text = "Compact System"
        '
        'chkBackup
        '
        Me.chkBackup.Location = New System.Drawing.Point(8, 208)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.Size = New System.Drawing.Size(120, 24)
        Me.chkBackup.TabIndex = 11
        Me.chkBackup.Text = "Backup && Restore"
        '
        'chkCustomCalendar
        '
        Me.chkCustomCalendar.Location = New System.Drawing.Point(168, 48)
        Me.chkCustomCalendar.Name = "chkCustomCalendar"
        Me.chkCustomCalendar.Size = New System.Drawing.Size(136, 24)
        Me.chkCustomCalendar.TabIndex = 2
        Me.chkCustomCalendar.Text = "Custom Calendar"
        '
        'chkRemoteAdmin
        '
        Me.chkRemoteAdmin.Location = New System.Drawing.Point(168, 80)
        Me.chkRemoteAdmin.Name = "chkRemoteAdmin"
        Me.chkRemoteAdmin.Size = New System.Drawing.Size(144, 24)
        Me.chkRemoteAdmin.TabIndex = 4
        Me.chkRemoteAdmin.Text = "Remote Administration"
        '
        'chkCreateSchedules
        '
        Me.chkCreateSchedules.Location = New System.Drawing.Point(168, 112)
        Me.chkCreateSchedules.Name = "chkCreateSchedules"
        Me.chkCreateSchedules.Size = New System.Drawing.Size(136, 24)
        Me.chkCreateSchedules.TabIndex = 6
        Me.chkCreateSchedules.Text = "Create/Edit Schedules"
        '
        'chkDeleteSchedules
        '
        Me.chkDeleteSchedules.Location = New System.Drawing.Point(168, 144)
        Me.chkDeleteSchedules.Name = "chkDeleteSchedules"
        Me.chkDeleteSchedules.Size = New System.Drawing.Size(136, 24)
        Me.chkDeleteSchedules.TabIndex = 8
        Me.chkDeleteSchedules.Text = "Delete Schedules"
        '
        'chkFolderManager
        '
        Me.chkFolderManager.Location = New System.Drawing.Point(168, 176)
        Me.chkFolderManager.Name = "chkFolderManager"
        Me.chkFolderManager.Size = New System.Drawing.Size(136, 24)
        Me.chkFolderManager.TabIndex = 10
        Me.chkFolderManager.Text = "Folder Management"
        '
        'chkSmartFolders
        '
        Me.chkSmartFolders.Location = New System.Drawing.Point(168, 208)
        Me.chkSmartFolders.Name = "chkSmartFolders"
        Me.chkSmartFolders.Size = New System.Drawing.Size(136, 24)
        Me.chkSmartFolders.TabIndex = 12
        Me.chkSmartFolders.Text = "Smart Folder Mngmnt"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(346, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 11
        '
        'frmGroupAttr
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(346, 424)
        Me.Controls.Add(Me.tbGroups)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmGroupAttr"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Group Editor"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbGroups.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim UserCancel As Boolean
    Dim sMode As String
    Dim nID As Integer
    Public Sub EditGroup(ByVal nID As Integer)
        sMode = "Edit"

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oldGroupName As String = ""

        SQL = "SELECT * FROM GroupAttr WHERE GroupID =" & nID

        oRs = GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                txtGroupName.Text = oRs("groupname").Value
                txtGroupDesc.Text = oRs("groupdesc").Value
            End If

            oRs.Close()
        End If

        Me._GetGroupPermissions(nID)

        oldGroupName = txtGroupName.Text

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        SQL = "UPDATE GroupAttr SET " & _
        "GroupName = '" & txtGroupName.Text & "'," & _
        "GroupDesc = '" & txtGroupDesc.Text & "' " & _
        "WHERE GroupID =" & nID

        WriteData(SQL)

        Me._SetGroupPermissions(nID)

        SQL = "UPDATE CRDUsers SET UserRole ='" & SQLPrepare(txtGroupName.Text) & "' WHERE UserRole = '" & SQLPrepare(oldGroupName) & "'"

        WriteData(SQL)
    End Sub
    Public Sub AddGroup()
        sMode = "Add"

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nGroupID As Integer = clsMarsData.CreateDataID("groupattr", "groupid")

        sCols = "GroupID,GroupName,GroupDesc"

        sVals = nGroupID & "," & _
        "'" & SQLPrepare(txtGroupName.Text) & "'," & _
        "'" & SQLPrepare(txtGroupDesc.Text) & "'"

        SQL = "INSERT INTO GroupAttr (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        Me._SetGroupPermissions(nGroupID)
    End Sub

    Private Sub frmGroupAttr_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtGroupName.Text.Length = 0 Then
            ep.SetError(txtGroupName, "Please provide a unique group name")
            txtGroupName.Focus()
            Return
        ElseIf txtGroupName.Text.ToLower = "administrator" Or txtGroupName.Text.ToLower = "user" Then
            ep.SetError(txtGroupName, "The provided group name is an internal SQL-RD group name, please pick a different name and try again")
            txtGroupName.Focus()
            Return
        ElseIf clsMarsData.IsDuplicate("GroupAttr", "GroupName", txtGroupName.Text, False) = True _
        And sMode = "Add" Then
            ep.SetError(txtGroupName, "A group with this name already exists")
            txtGroupName.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        For Each oC As Control In tbGroups.TabPages(1).Controls
            If TypeOf oC Is CheckBox Then
                Dim oCheck As CheckBox = CType(oC, CheckBox)

                oCheck.Checked = chkAll.Checked
            End If
        Next
    End Sub

    Private Sub _GetGroupPermissions(ByVal nGroupID As Integer)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oCheck As CheckBox
        Dim sText As String
        Dim nValue As Integer

        SQL = "SELECT * FROM GroupPermissions WHERE GroupID =" & nGroupID

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Return
        Else
            Do While oRs.EOF = False
                sText = _DecryptDBValue(oRs("permdesc").Value)
                nValue = oRs("permvalue").Value

                For Each oC As Control In tbGroups.TabPages(1).Controls
                    If TypeOf oC Is CheckBox Then
                        oCheck = oC

                        If oCheck.Text = sText Then
                            If nValue = 1458 Then
                                oCheck.Checked = True
                            Else
                                oCheck.Checked = False
                            End If
                        End If
                    End If
                Next
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub
    Private Sub _SetGroupPermissions(ByVal nGroupID As Integer)
        Dim oCheck As CheckBox
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nPerm As Integer

        sCols = "PermID,GroupID,PermDesc,PermValue"

        clsMarsData.WriteData("DELETE FROM GroupPermissions WHERE GroupID =" & nGroupID)

        For Each oC As Control In tbGroups.TabPages(1).Controls
            If TypeOf oC Is CheckBox And oC.Name <> "chkAll" Then
                oCheck = oC

                If oCheck.Checked = True Then
                    nPerm = 1458
                Else
                    nPerm = 54532
                End If

                sVals = clsMarsData.CreateDataID("grouppermissions", "permid") & "," & _
                nGroupID & "," & _
                "'" & SQLPrepare(_EncryptDBValue(oCheck.Text)) & "'," & _
                nPerm

                SQL = "INSERT INTO GroupPermissions (" & sCols & ") VALUES (" & sVals & ")"

                If clsMarsData.WriteData(SQL) = False Then
                    Return
                End If
            End If
        Next
    End Sub



    Private Sub txtGroupName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtGroupName.KeyPress
        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) And _
        e.KeyChar <> " "c Then
            e.Handled = True
        End If
    End Sub


    Private Sub chkDeleteSchedules_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeleteSchedules.CheckedChanged

    End Sub
End Class
