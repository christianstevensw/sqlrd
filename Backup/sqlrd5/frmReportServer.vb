Public Class frmReportServer
    Inherits System.Windows.Forms.Form
    Public m_oRpt As Object 'ReportServer.ReportingService
    Public showOwner As Boolean = False
    Dim UserCancel As Boolean = False
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtserverPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtserverUser As System.Windows.Forms.TextBox
    Friend WithEvents btnGo As System.Windows.Forms.Button
    Friend WithEvents grpReports As System.Windows.Forms.GroupBox
    Friend WithEvents pgServer As System.Windows.Forms.ProgressBar
    Dim oUI As New clsMarsUI
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Dim m_serverUrl As String
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Dim readOnlyMode As Boolean = False
    Dim firstAttempt As Boolean = True

    Public Property m_ReadOnly() As Boolean
        Get
            Return Me.readOnlyMode
        End Get
        Set(ByVal value As Boolean)
            Me.readOnlyMode = value
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Public WithEvents tvFolders As System.Windows.Forms.TreeView
    Friend WithEvents imgReports As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReportServer))
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.tvFolders = New System.Windows.Forms.TreeView
        Me.imgReports = New System.Windows.Forms.ImageList(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtserverPassword = New System.Windows.Forms.TextBox
        Me.txtserverUser = New System.Windows.Forms.TextBox
        Me.btnGo = New System.Windows.Forms.Button
        Me.grpReports = New System.Windows.Forms.GroupBox
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.pgServer = New System.Windows.Forms.ProgressBar
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.grpReports.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.Enabled = False
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(279, 440)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(359, 440)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'tvFolders
        '
        Me.tvFolders.HideSelection = False
        Me.tvFolders.ImageIndex = 0
        Me.tvFolders.ImageList = Me.imgReports
        Me.tvFolders.Indent = 19
        Me.tvFolders.ItemHeight = 16
        Me.tvFolders.Location = New System.Drawing.Point(7, 20)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.PathSeparator = "/"
        Me.tvFolders.SelectedImageIndex = 0
        Me.tvFolders.Size = New System.Drawing.Size(415, 262)
        Me.tvFolders.TabIndex = 0
        '
        'imgReports
        '
        Me.imgReports.ImageStream = CType(resources.GetObject("imgReports.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgReports.TransparentColor = System.Drawing.Color.Transparent
        Me.imgReports.Images.SetKeyName(0, "")
        Me.imgReports.Images.SetKeyName(1, "")
        Me.imgReports.Images.SetKeyName(2, "")
        Me.imgReports.Images.SetKeyName(3, "")
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(165, 25)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Domain Name\User ID"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(174, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 25)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtserverPassword
        '
        Me.txtserverPassword.Location = New System.Drawing.Point(174, 28)
        Me.txtserverPassword.Name = "txtserverPassword"
        Me.txtserverPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtserverPassword.Size = New System.Drawing.Size(165, 21)
        Me.txtserverPassword.TabIndex = 1
        '
        'txtserverUser
        '
        Me.txtserverUser.Location = New System.Drawing.Point(3, 28)
        Me.txtserverUser.Name = "txtserverUser"
        Me.txtserverUser.Size = New System.Drawing.Size(165, 21)
        Me.txtserverUser.TabIndex = 0
        '
        'btnGo
        '
        Me.btnGo.Image = CType(resources.GetObject("btnGo.Image"), System.Drawing.Image)
        Me.btnGo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGo.Location = New System.Drawing.Point(345, 28)
        Me.btnGo.Name = "btnGo"
        Me.btnGo.Size = New System.Drawing.Size(71, 23)
        Me.btnGo.TabIndex = 2
        Me.btnGo.Text = "Connect"
        Me.btnGo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGo.UseVisualStyleBackColor = True
        '
        'grpReports
        '
        Me.grpReports.Controls.Add(Me.tvFolders)
        Me.grpReports.Controls.Add(Me.txtPath)
        Me.grpReports.Controls.Add(Me.pgServer)
        Me.grpReports.Location = New System.Drawing.Point(5, 103)
        Me.grpReports.Name = "grpReports"
        Me.grpReports.Size = New System.Drawing.Size(429, 331)
        Me.grpReports.TabIndex = 0
        Me.grpReports.TabStop = False
        Me.grpReports.Text = "Please select the report"
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(6, 288)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(415, 21)
        Me.txtPath.TabIndex = 1
        Me.txtPath.Tag = "memo"
        '
        'pgServer
        '
        Me.pgServer.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pgServer.Location = New System.Drawing.Point(3, 313)
        Me.pgServer.Name = "pgServer"
        Me.pgServer.Size = New System.Drawing.Size(423, 15)
        Me.pgServer.TabIndex = 6
        Me.pgServer.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnGo, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtserverUser, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtserverPassword, 1, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(12, 46)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(421, 54)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(422, 38)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "Please enter the credentials used to access your report server (if any) and then " & _
            "click the ""Connect"" button to browse the server for the required report"
        '
        'frmReportServer
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(439, 470)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.grpReports)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmReportServer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reports Server Browser"
        Me.grpReports.ResumeLayout(False)
        Me.grpReports.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmReportServer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Overloads Function newGetReports(ByRef oRpt As ReportServer_2005.ReportingService2005, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "") As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice2005.asmx"
            Else
                sUrl &= "reportservice2005.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword

        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text

        If txtPath.Text = "" Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function

    Public Overloads Function newGetReports(ByRef oRpt As ReportServer.ReportingService, ByRef sUrl As String, Optional ByRef serverUser As String = "", _
    Optional ByRef serverPassword As String = "", Optional ByVal currentPath As String = "") As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice.asmx"
            Else
                sUrl &= "reportservice.asmx"
            End If
        End If

        Me.Text &= " - " & sUrl

        Me.m_oRpt = oRpt
        Me.m_serverUrl = sUrl

        oRpt.Url = sUrl

        Me.txtserverUser.Text = serverUser
        Me.txtserverPassword.Text = serverPassword

        If currentPath <> "" Then
            Me.btnGo_Click(Nothing, Nothing)
            txtPath.Text = currentPath
        End If

        txtPath.ReadOnly = Me.m_ReadOnly

        Me.btnGo_Click(Nothing, Nothing)

        firstAttempt = False

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        serverUser = Me.txtserverUser.Text
        serverPassword = Me.txtserverPassword.Text

        If txtPath.Text = "" Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function

    'this function is no longer used
    Public Function _GetReport(ByVal sUrl As String, Optional ByVal cred As System.Net.NetworkCredential = Nothing) As String
        If sUrl.ToLower.EndsWith(".asmx") = False Then
            If sUrl.EndsWith("/") = False Then
                sUrl &= "/reportservice.asmx"
            Else
                sUrl &= "reportservice.asmx"
            End If
        End If

        m_oRpt = New ReportServer.ReportingService

        m_oRpt.Url = sUrl

        If cred Is Nothing Then
            m_oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
        Else
            m_oRpt.Credentials = cred
        End If

        AppStatus(True)

        If LoadServer(sUrl) = False Then
            Return Nothing
        End If

        AppStatus(False)

        oUI.BusyProgress(, , True)

        Me.ShowDialog()

        If UserCancel = True Then Return Nothing

        If tvFolders.SelectedNode Is Nothing Then Return Nothing

        Return txtPath.Text 'tvFolders.SelectedNode.FullPath.Replace(sUrl, String.Empty)
    End Function
    Public Function LoadServer(ByVal sUrl As String) As Boolean

        Try
            tvFolders.Nodes.Clear()

            Dim oNode As TreeNode
            Dim oMaster As TreeNode
            Dim sName As String
            Dim sPath As String
            Dim NodeFound As Boolean
            Dim I As Integer = 1

            oUI.BusyProgress(10, "Loading folders...")

            oMaster = tvFolders.Nodes.Add(sUrl)

            oMaster.ImageIndex = 2
            oMaster.SelectedImageIndex = 2
            oMaster.Tag = "master"

            Dim oArr As ArrayList = ReadFolders()

            For Each o As Object In oArr

                oUI.BusyProgress((I / oArr.Count) * 100, "Loading folders...")

                I += 1

                sName = Convert.ToString(o).Split("|")(0)
                sPath = Convert.ToString(o).Split("|")(1)

                If FindOccurence(sPath, "/") = 1 Then
                    oNode = oMaster.Nodes.Add(sName)
                    oNode.ImageIndex = 0
                    oNode.SelectedImageIndex = 3
                    oNode.Tag = "folder"
                Else
                    Dim StartNode As TreeNode = tvFolders.Nodes(0)
                    Dim x As TreeNode

                    For Each s As String In sPath.Split("/")
                        If s.Length > 0 Then

                            NodeFound = False

                            For Each x In StartNode.Nodes
                                If x.Text = s And x.Tag = "folder" Then
                                    NodeFound = True
                                    Exit For
                                End If
                            Next

                            If NodeFound = False Then
                                oNode = StartNode.Nodes.Add(s)
                                oNode.ImageIndex = 0
                                oNode.SelectedImageIndex = 3
                                oNode.Tag = "folder"

                                StartNode = oNode
                            Else
                                StartNode = x
                            End If
                        End If
                    Next

                End If

            Next

            AddReports(oMaster)

            oMaster.Expand()

            Return True
        Catch ex As Exception
            Dim suggest As String

            suggest = "1. Please provide a valid user name and password" & vbCrLf & _
            "2. Check to make sure that the report server is online" & vbCrLf & _
            "3. Check your network connection and make sure that you can connect to the report server"

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), suggest)
            Return False
        End Try

    End Function

    Private Sub AddReports(ByVal oNode As TreeNode)
        For Each o As TreeNode In oNode.Nodes
            If o.Tag = "folder" Then
                AddReports(o)
            End If
        Next

        Dim oChild As TreeNode
        Dim sPath As String = oNode.FullPath.Replace(tvFolders.Nodes(0).Text, "")

        If sPath.Length = 0 Then Return

        Dim oArr As ArrayList = ReadReports(sPath)
        Dim I As Integer = 1

        For Each x As Object In oArr

            oUI.BusyProgress((I / oArr.Count) * 100, "Loading reports...")

            I += 1

            oChild = oNode.Nodes.Add(x)

            oChild.ImageIndex = 1
            oChild.SelectedImageIndex = 1
            oChild.Tag = "report"
        Next
    End Sub
    Private Function ReadFolders() As ArrayList
        Dim srcFolderCond As ReportServer.SearchCondition
        Dim srchResults As ReportServer.CatalogItem()
        Dim srchResult As ReportServer.CatalogItem
        Dim aryResults As New ArrayList

        With m_oRpt

            srcFolderCond = New ReportServer.SearchCondition

            With srcFolderCond
                .ConditionSpecified = False
                .Condition = ReportServer.ConditionEnum.Equals
                .Name = "Name"
            End With

            srchResults = .FindItems("/", ReportServer.BooleanOperatorEnum.And, New ReportServer.SearchCondition() {srcFolderCond})

            For Each srchResult In srchResults

                If srchResult.Type = ReportServer.ItemTypeEnum.Folder Then
                    If srchResult.Name <> "/" Then aryResults.Add(srchResult.Name & "|" & srchResult.Path)
                End If
            Next
        End With

        Return aryResults
    End Function

    Private Function ReadReports(ByVal Folder As String) As ArrayList
        Dim srcFolderCond As ReportServer.SearchCondition
        Dim srchResults As ReportServer.CatalogItem()
        Dim srchResult As ReportServer.CatalogItem
        Dim aryResults As New ArrayList

        With m_oRpt

            srcFolderCond = New ReportServer.SearchCondition
            With srcFolderCond
                .ConditionSpecified = False
                .Condition = ReportServer.ConditionEnum.Equals
                .Name = "Name"
            End With

            'srchResults = .FindItems(Folder, ReportServer.BooleanOperatorEnum.And, New ReportServer.SearchCondition() {srcFolderCond})

            srchResults = .ListChildren(Folder, False)

            For Each srchResult In srchResults
                If srchResult.Type = ReportServer.ItemTypeEnum.Report Then
                    Dim PathOnly As String = ""

                    For I As Integer = 0 To srchResult.Path.Split("/").GetUpperBound(0) - 1
                        PathOnly &= srchResult.Path.Split("/")(I) & "/"
                    Next


                    If PathOnly = Folder & "/" Then
                        If showOwner = True Then
                            aryResults.Add(srchResult.CreatedBy & "." & srchResult.Name)
                        Else
                            aryResults.Add(srchResult.Name)
                        End If
                    End If

                End If
            Next
        End With

        Return aryResults
    End Function



    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub tvFolders_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvFolders.AfterSelect
        Try
            If tvFolders.SelectedNode Is Nothing Then
                cmdOK.Enabled = False
                Return
            ElseIf Me.m_ReadOnly = True Then
                cmdOK.Enabled = True
                Me.grpReports.Enabled = False
                Return
            End If

            Select Case tvFolders.SelectedNode.Tag
                Case "folder", "master"
                    'cmdOK.Enabled = False
                    txtPath.Text = ""
                Case "report"
                    'cmdOK.Enabled = True
                    txtPath.Text = tvFolders.SelectedNode.FullPath.Replace(Me.m_serverUrl, "")
                    Return
                Case Else
                    'cmdOK.Enabled = False
                    txtPath.Text = ""
                    Return
            End Select

            Dim path As String = tvFolders.SelectedNode.FullPath.Replace(tvFolders.Nodes(0).Text, "")
            Dim selNode As TreeNode = tvFolders.SelectedNode

            If path = "" Then Return

            If selNode.Tag <> "folder" Then
                Return
            End If

            If selNode.Nodes.Count > 0 Then Return

            Dim catItems As Object

            If TypeOf m_oRpt Is ReportServer.ReportingService Then
                catItems = New ReportServer.CatalogItem
            Else
                catItems = New ReportServer_2005.CatalogItem
            End If

            catItems = m_oRpt.ListChildren(path, False)

            Me.grpReports.Enabled = False
            'tvFolders.BeginUpdate()

            Dim I As Integer = 0
            Dim count As Integer

            pgServer.Visible = True

            Dim items As DataTable = New DataTable("ServerItems")

            items.Columns.Add("Name")
            items.Columns.Add("Type")

            For Each catitem As Object In catItems
                Dim row As DataRow

                If catitem.Type = 1 Then 'a folder
                    row = items.Rows.Add
                    row.Item("Name") = catitem.Name
                    row.Item("Type") = "folder"
                    'ElseIf catitem.Type = 2 Or catitem.Type = 4 Then 'a report/linked report
                ElseIf catitem.Type = 2 Then 'report
                    row = items.Rows.Add
                    row.Item("Name") = catitem.Name
                    row.Item("Type") = "report"
                End If
            Next

            Dim foundRows() As DataRow = items.Select("", "TYPE ASC")
            count = foundRows.GetLength(0)

            For Each row As DataRow In foundRows
                Dim childNode As TreeNode = selNode.Nodes.Add(row.Item("Name"))
                childNode.Tag = row.Item("Type")

                If childNode.Tag = "report" Then
                    childNode.ImageIndex = 1
                    childNode.SelectedImageIndex = 1
                Else
                    childNode.ImageIndex = 0
                    childNode.SelectedImageIndex = 3
                End If

                pgServer.Value = (I / count) * 100
                Application.DoEvents()
                System.Threading.Thread.Sleep(25)
                I += 1
            Next

            'For Each catitem As ReportServer.CatalogItem In catItems
            '    If catitem.Type = ReportServer.ItemTypeEnum.Report Then
            '        Dim childNode As TreeNode = selNode.Nodes.Add(catitem.Name)
            '        childNode.Tag = "report"
            '        childNode.ImageIndex = 1
            '        childNode.SelectedImageIndex = 1
            '    ElseIf catitem.Type = ReportServer.ItemTypeEnum.Folder Then
            '        Dim childNode As TreeNode = selNode.Nodes.Add(catitem.Name)
            '        childNode.Tag = "folder"
            '        childNode.ImageIndex = 0
            '        childNode.SelectedImageIndex = 3
            '    End If

            '    pgServer.Value = (I / count) * 100
            '    Application.DoEvents()
            '    System.Threading.Thread.Sleep(25)
            '    I += 1
            'Next

            'tvFolders.EndUpdate()
            pgServer.Value = 0
            pgServer.Visible = False
            selNode.Expand()
            Me.grpReports.Enabled = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtPath.Text = "" Then
            MessageBox.Show("Please select a report", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        ElseIf Me.m_ReadOnly = True Then
            Try
                Dim cat As ReportServer.CatalogItem() = Me.m_oRpt.ListChildren("/", False)
            Catch ex As Exception
                MessageBox.Show("The user name or password you have provided for the report server is invalid. Please try again", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
            End Try
        End If

        Close()
    End Sub

    Private Sub tvFolders_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.DoubleClick
        If tvFolders.SelectedNode Is Nothing Then
            cmdOK.Enabled = False
            Return
        End If

        Select Case tvFolders.SelectedNode.Tag
            Case "folder", "master"
                Return
            Case "report"
                cmdOK_Click(sender, e)
            Case Else
                Return
        End Select
    End Sub

    Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try

            Dim oMaster As TreeNode
            Dim I As Integer = 0

            If tvFolders.Nodes.Count = 0 Then
                oMaster = tvFolders.Nodes.Add(Me.m_serverUrl)
                oMaster.ImageIndex = 2
                oMaster.SelectedImageIndex = 2
                oMaster.Tag = "master"
            End If

            If Me.txtserverUser.Text = "" Then
                m_oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            Else
                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(Me.txtserverUser.Text, Me.txtserverPassword.Text)
                m_oRpt.Credentials = cred
            End If

            pgServer.Visible = True

            If oMaster IsNot Nothing Then
                Dim catItems As Object

                If TypeOf m_oRpt Is ReportServer.ReportingService Then
                    catItems = New ReportServer.CatalogItem
                Else
                    catItems = New ReportServer_2005.CatalogItem
                End If

                catItems = m_oRpt.ListChildren("/", False)

                Dim count As Integer = catItems.Length

                For Each catitem As Object In catItems
                    If catitem.Type = 1 Then 'a folder
                        Dim child As TreeNode = oMaster.Nodes.Add(catitem.Name)

                        child.Tag = "folder"
                    End If

                    pgServer.Value = (I / count) * 100
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(25)
                    I += 1
                Next
            End If

            pgServer.Value = 0
            pgServer.Visible = False

            Me.grpReports.Enabled = Not (Me.m_ReadOnly)

            If oMaster IsNot Nothing Then oMaster.Expand()
        Catch ex As Exception
            If firstAttempt = False Then
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End If

            tvFolders.Nodes.Clear()
        End Try
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        If txtPath.Text.Length = 0 Then
            cmdOK.Enabled = False
        Else
            cmdOK.Enabled = True
        End If
    End Sub
End Class
