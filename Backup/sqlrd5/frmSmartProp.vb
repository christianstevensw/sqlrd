Public Class frmSmartProp
    Inherits System.Windows.Forms.Form
    Dim SmartFolderID As Integer
    Friend WithEvents tbSmart As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem4 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem5 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem3 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Dim ScheduleID As Integer

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvCriteria As System.Windows.Forms.ListView
    Friend WithEvents cmdAddWhere As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveWhere As System.Windows.Forms.Button
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents UcScheduleUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents grSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents imgSingle As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSmartProp))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lsvCriteria = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddWhere = New System.Windows.Forms.Button
        Me.cmdRemoveWhere = New System.Windows.Forms.Button
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbOperator = New System.Windows.Forms.ComboBox
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.UcScheduleUpdate = New sqlrd.ucScheduleUpdate
        Me.UcDest = New sqlrd.ucDestination
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdRefresh = New System.Windows.Forms.Button
        Me.grSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.imgSingle = New System.Windows.Forms.ImageList(Me.components)
        Me.tbSmart = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem5 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem4 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem3 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grSort.SuspendLayout()
        CType(Me.tbSmart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbSmart.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.lsvCriteria)
        Me.GroupBox2.Controls.Add(Me.cmdAddWhere)
        Me.GroupBox2.Controls.Add(Me.cmdRemoveWhere)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbOperator)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 33)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 320)
        Me.GroupBox2.TabIndex = 13
        Me.GroupBox2.TabStop = False
        '
        'lsvCriteria
        '
        Me.lsvCriteria.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvCriteria.FullRowSelect = True
        Me.lsvCriteria.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvCriteria.Location = New System.Drawing.Point(8, 72)
        Me.lsvCriteria.Name = "lsvCriteria"
        Me.lsvCriteria.Size = New System.Drawing.Size(400, 240)
        Me.lsvCriteria.TabIndex = 15
        Me.lsvCriteria.UseCompatibleStateImageBehavior = False
        Me.lsvCriteria.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Criteria"
        Me.ColumnHeader1.Width = 226
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = ""
        Me.ColumnHeader2.Width = 168
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(368, 40)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 14
        Me.cmdAddWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(256, 40)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 13
        Me.cmdRemoveWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbColumn
        '
        Me.cmbColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Items.AddRange(New Object() {"Bcc", "Cc", "Description", "EndDate", "ErrMsg", "FolderName", "Frequency", "Keyword", "Message", "NextRun", "ReportTitle", "ScheduleName", "ScheduleType", "SendTo", "StartDate", "StartTime", "Status", "Subject"})
        Me.cmbColumn.Location = New System.Drawing.Point(8, 16)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(112, 21)
        Me.cmbColumn.Sorted = True
        Me.cmbColumn.TabIndex = 8
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", ">=", "<=", ">", "<", "starts with", "ends with", "contains", "does not contain"})
        Me.cmbOperator.Location = New System.Drawing.Point(136, 16)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(104, 21)
        Me.cmbOperator.TabIndex = 8
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbValue.Location = New System.Drawing.Point(256, 16)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(152, 21)
        Me.cmbValue.TabIndex = 8
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.ItemHeight = 13
        Me.cmbAnyAll.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(44, 6)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(80, 21)
        Me.cmbAnyAll.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(4, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Match"
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(132, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(288, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "of these conditions:"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(4, 25)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(400, 21)
        Me.txtName.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Name"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(4, 67)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(400, 224)
        Me.txtDesc.TabIndex = 5
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Description"
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(481, 370)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 12
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(393, 370)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 11
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(305, 370)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 10
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'UcScheduleUpdate
        '
        Me.UcScheduleUpdate.BackColor = System.Drawing.Color.Transparent
        Me.UcScheduleUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcScheduleUpdate.Location = New System.Drawing.Point(4, 4)
        Me.UcScheduleUpdate.Name = "UcScheduleUpdate"
        Me.UcScheduleUpdate.Size = New System.Drawing.Size(494, 360)
        Me.UcScheduleUpdate.TabIndex = 0
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(7, 4)
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(416, 352)
        Me.UcDest.TabIndex = 0
        '
        'cmdClear
        '
        Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdClear.Image = CType(resources.GetObject("cmdClear.Image"), System.Drawing.Image)
        Me.cmdClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(260, 330)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdClear.TabIndex = 13
        Me.cmdClear.Text = "Clear"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRefresh.Image = CType(resources.GetObject("cmdRefresh.Image"), System.Drawing.Image)
        Me.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRefresh.Location = New System.Drawing.Point(348, 330)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 12
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grSort
        '
        Me.grSort.BackColor = System.Drawing.Color.Transparent
        Me.grSort.Controls.Add(Me.optAsc)
        Me.grSort.Controls.Add(Me.optDesc)
        Me.grSort.Location = New System.Drawing.Point(7, 8)
        Me.grSort.Name = "grSort"
        Me.grSort.Size = New System.Drawing.Size(272, 48)
        Me.grSort.TabIndex = 11
        Me.grSort.TabStop = False
        Me.grSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 1
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 0
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'tvHistory
        '
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgSingle
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(7, 62)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.tvHistory.Size = New System.Drawing.Size(416, 262)
        Me.tvHistory.TabIndex = 10
        '
        'imgSingle
        '
        Me.imgSingle.ImageStream = CType(resources.GetObject("imgSingle.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgSingle.TransparentColor = System.Drawing.Color.Transparent
        Me.imgSingle.Images.SetKeyName(0, "")
        Me.imgSingle.Images.SetKeyName(1, "")
        Me.imgSingle.Images.SetKeyName(2, "")
        Me.imgSingle.Images.SetKeyName(3, "")
        Me.imgSingle.Images.SetKeyName(4, "")
        Me.imgSingle.Images.SetKeyName(5, "")
        '
        'tbSmart
        '
        Me.tbSmart.CanReorderTabs = True
        Me.tbSmart.Controls.Add(Me.TabControlPanel2)
        Me.tbSmart.Controls.Add(Me.TabControlPanel1)
        Me.tbSmart.Controls.Add(Me.TabControlPanel5)
        Me.tbSmart.Controls.Add(Me.TabControlPanel4)
        Me.tbSmart.Controls.Add(Me.TabControlPanel3)
        Me.tbSmart.Dock = System.Windows.Forms.DockStyle.Top
        Me.tbSmart.Location = New System.Drawing.Point(0, 0)
        Me.tbSmart.Name = "tbSmart"
        Me.tbSmart.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbSmart.SelectedTabIndex = 0
        Me.tbSmart.Size = New System.Drawing.Size(567, 358)
        Me.tbSmart.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tbSmart.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbSmart.TabIndex = 16
        Me.tbSmart.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbSmart.Tabs.Add(Me.TabItem1)
        Me.tbSmart.Tabs.Add(Me.TabItem2)
        Me.tbSmart.Tabs.Add(Me.TabItem3)
        Me.tbSmart.Tabs.Add(Me.TabItem4)
        Me.tbSmart.Tabs.Add(Me.TabItem5)
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.cmdClear)
        Me.TabControlPanel5.Controls.Add(Me.grSort)
        Me.TabControlPanel5.Controls.Add(Me.cmdRefresh)
        Me.TabControlPanel5.Controls.Add(Me.tvHistory)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(87, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(480, 358)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.TabItem5
        '
        'TabItem5
        '
        Me.TabItem5.AttachedControl = Me.TabControlPanel5
        Me.TabItem5.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem5.Image = CType(resources.GetObject("TabItem5.Image"), System.Drawing.Image)
        Me.TabItem5.Name = "TabItem5"
        Me.TabItem5.Text = "History"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.UcDest)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(87, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(480, 358)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.TabItem4
        '
        'TabItem4
        '
        Me.TabItem4.AttachedControl = Me.TabControlPanel4
        Me.TabItem4.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem4.Image = CType(resources.GetObject("TabItem4.Image"), System.Drawing.Image)
        Me.TabItem4.Name = "TabItem4"
        Me.TabItem4.Text = "Output"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.UcScheduleUpdate)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(87, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(480, 358)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.TabItem3
        '
        'TabItem3
        '
        Me.TabItem3.AttachedControl = Me.TabControlPanel3
        Me.TabItem3.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem3.Image = CType(resources.GetObject("TabItem3.Image"), System.Drawing.Image)
        Me.TabItem3.Name = "TabItem3"
        Me.TabItem3.Text = "Schedule"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.GroupBox2)
        Me.TabControlPanel2.Controls.Add(Me.cmbAnyAll)
        Me.TabControlPanel2.Controls.Add(Me.Label5)
        Me.TabControlPanel2.Controls.Add(Me.Label4)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(87, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(480, 358)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem2
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel2
        Me.TabItem2.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem2.Image = CType(resources.GetObject("TabItem2.Image"), System.Drawing.Image)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Definition"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.txtDesc)
        Me.TabControlPanel1.Controls.Add(Me.txtName)
        Me.TabControlPanel1.Controls.Add(Me.Label3)
        Me.TabControlPanel1.Controls.Add(Me.Label2)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(87, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(480, 358)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem1
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel1
        Me.TabItem1.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "General"
        '
        'frmSmartProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(567, 398)
        Me.Controls.Add(Me.tbSmart)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSmartProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Smart Folder Properties"
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grSort.ResumeLayout(False)
        CType(Me.tbSmart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbSmart.ResumeLayout(False)
        Me.TabControlPanel5.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region



    Private Sub frmSmartProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        clsMarsUI.MainUI.InitHistoryImages(tvHistory, My.Resources.folder_cubes)
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn.SelectedIndexChanged
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL(3) As String
        Dim oRs As ADODB.Recordset

        cmbValue.Items.Clear()
        cmbOperator.Items.Clear()

        Select Case cmbColumn.Text
            Case "Status", "ScheduleType"
                With cmbOperator.Items
                    .Add("=")
                End With
            Case "Message", "Description", "Keyword", "SendTo", "ReportTitle", "ScheduleName", _
            "Cc", "Bcc", "Subject", "ErrMsg"
                With cmbOperator.Items
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                    .Add("IS EMPTY")
                    .Add("IS NOT EMPTY")
                End With
            Case "ScheduleType", "Frequency", "DestinationType", "PrinterName", _
                 "OutputFormat", "FTPServer", "FTPUserName", "FolderName"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
            Case "StartDate", "EndDate", "NextRun"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                End With
            Case Else
                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
        End Select

        Select Case cmbColumn.Text.ToLower
            Case "frequency"
                With cmbValue.Items
                    .Add("Daily")
                    .Add("Weekly")
                    .Add("Monthly")
                    .Add("Yearly")
                    .Add("Custom")
                    .Add("None")
                End With
            Case "status"
                With cmbValue.Items
                    .Add("0")
                    .Add("1")
                End With
            Case "destinationtype"
                With cmbValue.Items
                    .Add("Email")
                    .Add("Disk")
                    .Add("Printer")
                    .Add("FTP")
                End With
            Case "printername"
                Dim strItem As String
                Dim oPrint As New System.Drawing.Printing.PrinterSettings

                For Each strItem In oPrint.InstalledPrinters
                    cmbValue.Items.Add(strItem)
                Next
            Case "outputformat"
                With cmbValue.Items
                    .Add("Acrobat Format (*.pdf)")
                    '.Add("Crystal Reports (*.rpt)")
                    .Add("CSV (*.csv)")
                    .Add("Data Interchange Format (*.dif)")
                    .Add("dBase II (*.dbf)")
                    .Add("dBase III (*.dbf)")
                    .Add("dBase IV (*.dbf)")
                    .Add("HTML (*.htm)")
                    .Add("Lotus 1-2-3 (*.wk1)")
                    .Add("Lotus 1-2-3 (*.wk3)")
                    .Add("Lotus 1-2-3 (*.wk4)")
                    .Add("Lotus 1-2-3 (*.wks)")
                    .Add("MS Excel - Data Only (*.xls)")
                    .Add("MS Excel 7 (*.xls)")
                    .Add("MS Excel 8 (*.xls)")
                    .Add("MS Excel 97-2000 (*.xls)")
                    .Add("MS Word (*.doc)")
                    .Add("ODBC (*.odbc)")
                    .Add("Rich Text Format (*.rtf)")
                    .Add("Tab Seperated (*.txt)")
                    .Add("Text (*.txt)")
                    .Add("TIFF (*.tif)")
                    .Add("XML (*.xml)")
                End With
            Case "schedulename"
                SQL(0) = "SELECT DISTINCT ReportTitle FROM ReportAttr"
                SQL(1) = "SELECT DISTINCT PackageName FROM PackageAttr"
                SQL(2) = "SELECT DISTINCT AutoName FROM AutomationAttr"
                SQL(3) = "SELECT DISTINCT EventName FROM EventAttr"

                For Each s As String In SQL
                    oRs = clsMarsData.GetData(s)

                    If Not oRs Is Nothing Then
                        Do While oRs.EOF = False
                            cmbValue.Items.Add(oRs.Fields(0).Value)
                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If
                Next
            Case "foldername"
                SQL(0) = "SELECT DISTINCT FolderName FROM Folders"

                oRs = clsMarsData.GetData(SQL(0))

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        cmbValue.Items.Add(oRs(0).Value)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()

                End If
            Case "scheduletype"
                With cmbValue.Items
                    .Add("Single Schedule")
                    .Add("Package Schedule")
                    .Add("Automation Schedule")
                    .Add("Event-Based Schedule")
                End With
        End Select
    End Sub

    

    Public Sub EditSmartFolder(ByVal nID As Integer)
        SmartFolderID = nID

        UcDest.nSmartID = nID
        UcDest.nPackID = 0
        UcDest.nReportID = 0

        UcScheduleUpdate.ScheduleID = 99999

        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM SmartFolders WHERE SmartID =" & nID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("smartname").Value
            txtDesc.Text = oRs("smartdesc").Value
            cmbAnyAll.Text = oRs("smarttype").Value
        End If

        UcDest.sReportTitle = txtName.Text

        oRs.Close()

        SQL = "SELECT * FROM SmartFolderAttr WHERE SmartID =" & nID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                Dim olsv As ListViewItem = New ListViewItem

                olsv.Text = oRs("smartcolumn").Value

                olsv.SubItems.Add(oRs("smartcriteria").Value)

                lsvCriteria.Items.Add(olsv)

                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        Try
            SQL = "SELECT * FROM ScheduleAttr WHERE SmartID = " & SmartFolderID

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            If oRs.EOF = False Then
                ScheduleID = oRs("scheduleid").Value

                Select Case oRs("frequency").Value.ToLower
                    Case "daily"
                        UcScheduleUpdate.optDaily.Checked = True
                    Case "weekly"
                        UcScheduleUpdate.optWeekly.Checked = True
                    Case "week-daily"
                        UcScheduleUpdate.optWeekDaily.Checked = True
                    Case "monthly"
                        UcScheduleUpdate.optMonthly.Checked = True
                    Case "yearly"
                        UcScheduleUpdate.optYearly.Checked = True
                    Case "none"
                        UcScheduleUpdate.optNone.Checked = True
                    Case "custom"
                        UcScheduleUpdate.optCustom.Checked = True
                        UcScheduleUpdate.cmbCustom.Text = IsNull(oRs("calendarname").Value)
                    Case "other"
                        UcScheduleUpdate.optOther.Checked = True
                End Select

                UcScheduleUpdate.m_fireOthers = True

                Try
                    If oRs("status").Value = 1 Then
                        UcScheduleUpdate.chkStatus.Checked = True
                    Else
                        UcScheduleUpdate.chkStatus.Checked = False
                    End If
                Catch ex As Exception
                    UcScheduleUpdate.chkStatus.Checked = True
                End Try

                Try
                    UcScheduleUpdate.m_RepeatUnit = IsNull(oRs("repeatunit").Value, "hours")
                Catch ex As Exception
                    UcScheduleUpdate.m_RepeatUnit = "hours"
                End Try

                UcScheduleUpdate.btnApply = Me.cmdApply

                UcScheduleUpdate.ScheduleID = oRs("scheduleid").Value

                Try
                    Dim d As Date

                    d = oRs("enddate").Value

                    If d.Year >= 3004 Then
                        UcScheduleUpdate.EndDate.Enabled = False
                        UcScheduleUpdate.chkNoEnd.Checked = True
                    End If

                Catch : End Try

                UcScheduleUpdate.NextRun.Value = oRs("nextrun").Value
                UcScheduleUpdate.EndDate.Value = oRs("enddate").Value
                UcScheduleUpdate.RunAt.Value = Convert.ToDateTime(Date.Now.Date & " " & oRs("starttime").Value)
                UcScheduleUpdate.NextRunTime.Value = oRs("nextrun").Value
                UcScheduleUpdate.chkRepeat.Checked = Convert.ToBoolean(oRs("repeat").Value)
                UcScheduleUpdate.cmbRpt.Text = oRs("repeatinterval").Value

                If UcScheduleUpdate.EndDate.Value = Date.Now.Date Then
                    UcScheduleUpdate.EndDate.Value = Now.Date.AddMonths(12)
                End If

                Try
                    UcScheduleUpdate.RepeatUntil.Value = oRs("repeatuntil").Value
                Catch : End Try

                UcScheduleUpdate.StartDate.Value = ConDate(oRs("startdate").Value)
            End If

            If UcScheduleUpdate.cmbRpt.Text = "" Then UcScheduleUpdate.cmbRpt.Text = 0.25

            txtDesc.Text = IsNull(oRs("description").Value)

            UcDest.LoadAll()
        Catch : End Try

        oRs.Close()
        Me.ShowDialog()
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCol As String
        Dim sVal As String
        Dim I As Integer = 1

        sCol = "SmartName = '" & SQLPrepare(txtName.Text) & "'," & _
            "SmartDesc = '" & SQLPrepare(txtDesc.Text) & "'," & _
            "SmartType = '" & cmbAnyAll.Text & "'"

        SQL = "UPDATE SmartFolders SET " & sCol & _
            " WHERE SmartID = " & SmartFolderID

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("DELETE FROM SmartFolderAttr WHERE SmartID = " & SmartFolderID)

        sCol = "SmartDefID,SmartID,SmartColumn,SmartCriteria"

        For Each olsv As ListViewItem In lsvCriteria.Items
            sVal = clsMarsData.CreateDataID("smartfolderattr", "smartdefid") & "," & _
            SmartFolderID & "," & _
            "'" & SQLPrepare(olsv.Text) & "'," & _
            "'" & SQLPrepare(olsv.SubItems(1).Text) & "'"

            SQL = "INSERT INTO SmartFolderAttr (" & sCol & ") VALUES (" & sVal & ")"

            clsMarsData.WriteData(SQL)

            I += 1
        Next

        '_Delay(1)

        With UcScheduleUpdate
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE SmartID = " & SmartFolderID)
            Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            If Not oRs Is Nothing Then
                If oRs.EOF = True Then
                    With UcScheduleUpdate
                        SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                            "StartTime,Repeat,RepeatInterval," & _
                            "RepeatUntil,Status,SmartID,Description,KeyWord,CalendarName," & _
                            "UseException,ExceptionCalendar,RepeatUnit) " & _
                            "VALUES(" & _
                            ScheduleID & "," & _
                            "'" & .sFrequency & "'," & _
                            "'" & ConDateTime(.StartDate.Value) & "'," & _
                            "'" & ConDateTime(.EndDate.Value) & "'," & _
                            "'" & ConDate(.StartDate.Value.Date) & " " & ConTime(.RunAt.Value) & "'," & _
                            "'" & .RunAt.Value.ToShortTimeString & "'," & _
                            Convert.ToInt32(.chkRepeat.Checked) & "," & _
                            "'" & .cmbRpt.Value & "'," & _
                            "'" & .RepeatUntil.Value.ToShortTimeString & "'," & _
                            Convert.ToInt32(.chkStatus.Checked) & "," & _
                            SmartFolderID & "," & _
                            "'" & SQLPrepare(txtDesc.Text) & "'," & _
                            "''," & _
                            "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                            Convert.ToInt32(.chkException.Checked) & "," & _
                            "'" & SQLPrepare(.cmbException.Text) & "'," & _
                            "'" & .m_RepeatUnit & "')"
                    End With
                Else
                    Dim nRepeat As Double

                    If UcScheduleUpdate.chkRepeat.Checked = False Then
                        nRepeat = 0
                    Else
                        nRepeat = UcScheduleUpdate.cmbRpt.Text
                    End If

                    SQL = "UPDATE ScheduleAttr SET " & _
                    "Frequency = '" & .sFrequency & "', " & _
                    "EndDate = '" & ConDateTime(.EndDate.Value) & "', " & _
                    "StartDate = '" & ConDateTime(.StartDate.Value) & "'," & _
                    "NextRun = '" & ConDate(.NextRun.Value) & " " & ConTime(.RunAt.Value) & "', " & _
                    "StartTime = '" & ConTime(.RunAt.Value) & "', " & _
                    "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                    "RepeatInterval = '" & nRepeat & "'," & _
                    "Status = " & Convert.ToInt32(.chkStatus.Checked) & "," & _
                    "RepeatUntil = '" & .RepeatUntil.Value & "', " & _
                    "Description = '" & SQLPrepare(txtDesc.Text) & "'," & _
                    "KeyWord = '', " & _
                    "CalendarName = '" & SQLPrepare(.cmbCustom.Text) & "' " & _
                    "WHERE SmartID = " & SmartFolderID

                End If

                clsMarsData.WriteData(SQL)

                oRs.Close()

                clsMarsData.WriteData("UPDATE ScheduleOptions SET ScheduleID = " & ScheduleID & " WHERE " & _
                "ScheduleID = 99999")

                clsMarsData.WriteData("UPDATE DestinationAttr SET ReportID=0,PackID=0 WHERE SmartID =" & SmartFolderID)

            End If

        End With

        cmdApply.Enabled = False
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim sName As String = txtName.Text

        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter a name for this smart folder")
            tbSmart.SelectedTabIndex = 0
            txtName.Focus()
            Return
        ElseIf lsvCriteria.Items.Count = 0 Then

            tbSmart.SelectedTabIndex = 1
            ep.SetError(lsvCriteria, "Please build the selection criteria")
            Return
        ElseIf UcScheduleUpdate.chkStatus.Checked = True And UcDest.lsvDestination.Items.Count = 0 Then

            tbSmart.SelectedTabIndex = 3
            ep.SetError(UcDest.lsvDestination, "Please add a destination for the schedule")
            Return
        End If

        cmdApply_Click(sender, e)

        Me.Close()

        Dim oUi As New clsMarsUI

        oUi.RefreshView(oWindow(nWindowCurrent))


        Try
            Dim oWin As frmWindow = oWindow(nWindowCurrent)
            Dim tv As TreeView = oWin.tvSmartFolders

            For Each o As TreeNode In tv.Nodes
                If o.Text = sName Then
                    tv.SelectedNode = o
                End If
            Next
        Catch : End Try
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim olsv As ListViewItem
        Dim sCriteria As String

        If cmbColumn.Text.Length = 0 Then
            ep.SetError(cmbColumn, "Please select the column for the criteria")
            cmbColumn.Focus()
            Return
        ElseIf cmbOperator.Text.Length = 0 Then
            ep.SetError(cmbOperator, "Please select the operator the criteria")
            cmbOperator.Focus()
            Return
        End If

        olsv = New ListViewItem
        olsv.Text = cmbColumn.Text

        Select Case cmbOperator.Text.ToLower
            Case "starts with"
                sCriteria = "LIKE '" & cmbValue.Text & "%'"
            Case "ends with"
                sCriteria = "LIKE '%" & cmbValue.Text & "'"
            Case "contains"
                sCriteria = "LIKE '%" & cmbValue.Text & "%'"
            Case "does not contain"
                sCriteria = " NOT LIKE '%" & cmbValue.Text & "%'"
            Case "is empty"
                sCriteria = " IS EMPTY"
            Case "is not empty"
                sCriteria = " IS NOT EMPTY"
            Case Else
                sCriteria = cmbOperator.Text & "'" & cmbValue.Text & "'"
        End Select

        olsv.SubItems.Add(sCriteria)

        lsvCriteria.Items.Add(olsv)

        cmdApply.Enabled = True
        ep.SetError(lsvCriteria, String.Empty)
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvCriteria.SelectedItems.Count = 0 Then Return

        lsvCriteria.SelectedItems(0).Remove()
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, String.Empty)
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDesc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDesc.TextChanged
        cmdApply.Enabled = True
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        If cmbOperator.Text.ToLower.IndexOf("empty") > -1 Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub

    

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim oData As New clsMarsData

            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE SmartID =" & SmartFolderID, False)

            _Delay(1)

            cmdRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        If optDesc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.SMARTFOLDER, SmartFolderID, " DESC ")
        Else
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.SMARTFOLDER, SmartFolderID, " ASC ")
        End If

        grSort.Enabled = True
    End Sub

    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged
        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.SMARTFOLDER, SmartFolderID, " ASC ")

        grSort.Enabled = True
    End Sub

    Private Sub optDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDesc.CheckedChanged
        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.SMARTFOLDER, SmartFolderID, " DESC ")

        grSort.Enabled = True
    End Sub

    Private Sub tbSmart_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tbSmart.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "history"
                Dim oSchedule As New clsMarsScheduler

                oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.SMARTFOLDER, SmartFolderID, " ASC ")
        End Select
    End Sub


    
End Class
