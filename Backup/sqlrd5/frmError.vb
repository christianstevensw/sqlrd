Public Class frmError
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtErrorNumber As System.Windows.Forms.TextBox
    Friend WithEvents txtModuleName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtLineNumber As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdCopy As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtErrorDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFix As System.Windows.Forms.TextBox
    Friend WithEvents ContextMenu1 As System.Windows.Forms.ContextMenu
    Friend WithEvents txtCopy As System.Windows.Forms.TextBox
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmError))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtCopy = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCopy = New System.Windows.Forms.Button
        Me.txtErrorNumber = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.txtModuleName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtLineNumber = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtErrorDesc = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtFix = New System.Windows.Forms.TextBox
        Me.ContextMenu1 = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtCopy)
        Me.GroupBox1.Controls.Add(Me.cmdOK)
        Me.GroupBox1.Controls.Add(Me.cmdCopy)
        Me.GroupBox1.Controls.Add(Me.txtErrorNumber)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.txtModuleName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtLineNumber)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtErrorDesc)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtFix)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox1.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(376, 408)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtCopy
        '
        Me.txtCopy.Location = New System.Drawing.Point(193, 382)
        Me.txtCopy.Name = "txtCopy"
        Me.txtCopy.Size = New System.Drawing.Size(100, 21)
        Me.txtCopy.TabIndex = 6
        Me.txtCopy.Visible = False
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(304, 376)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(64, 23)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&OK"
        '
        'cmdCopy
        '
        Me.cmdCopy.Image = CType(resources.GetObject("cmdCopy.Image"), System.Drawing.Image)
        Me.cmdCopy.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCopy.Location = New System.Drawing.Point(8, 376)
        Me.cmdCopy.Name = "cmdCopy"
        Me.cmdCopy.Size = New System.Drawing.Size(120, 23)
        Me.cmdCopy.TabIndex = 0
        Me.cmdCopy.Text = "&Copy to Clipboard"
        Me.cmdCopy.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtErrorNumber
        '
        Me.txtErrorNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtErrorNumber.Location = New System.Drawing.Point(104, 72)
        Me.txtErrorNumber.Name = "txtErrorNumber"
        Me.txtErrorNumber.ReadOnly = True
        Me.txtErrorNumber.Size = New System.Drawing.Size(264, 21)
        Me.txtErrorNumber.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Error Number:"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(64, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(304, 56)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Apologies for the inconvenience, but SQL-RD has encountered an exception. Please " & _
            "find the details below:"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(16, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(40, 40)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'txtModuleName
        '
        Me.txtModuleName.ForeColor = System.Drawing.Color.Blue
        Me.txtModuleName.Location = New System.Drawing.Point(104, 104)
        Me.txtModuleName.Name = "txtModuleName"
        Me.txtModuleName.ReadOnly = True
        Me.txtModuleName.Size = New System.Drawing.Size(264, 21)
        Me.txtModuleName.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Module Name:"
        '
        'txtLineNumber
        '
        Me.txtLineNumber.ForeColor = System.Drawing.Color.Blue
        Me.txtLineNumber.Location = New System.Drawing.Point(104, 136)
        Me.txtLineNumber.Name = "txtLineNumber"
        Me.txtLineNumber.ReadOnly = True
        Me.txtLineNumber.Size = New System.Drawing.Size(264, 21)
        Me.txtLineNumber.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 138)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Line Number:"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 168)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Error Description:"
        '
        'txtErrorDesc
        '
        Me.txtErrorDesc.BackColor = System.Drawing.Color.White
        Me.txtErrorDesc.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.txtErrorDesc.ForeColor = System.Drawing.Color.Red
        Me.txtErrorDesc.Location = New System.Drawing.Point(8, 184)
        Me.txtErrorDesc.Multiline = True
        Me.txtErrorDesc.Name = "txtErrorDesc"
        Me.txtErrorDesc.ReadOnly = True
        Me.txtErrorDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtErrorDesc.Size = New System.Drawing.Size(360, 80)
        Me.txtErrorDesc.TabIndex = 5
        Me.txtErrorDesc.Tag = "red"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 272)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Suggestion for Fix"
        '
        'txtFix
        '
        Me.txtFix.BackColor = System.Drawing.SystemColors.Control
        Me.txtFix.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFix.ForeColor = System.Drawing.Color.Blue
        Me.txtFix.Location = New System.Drawing.Point(8, 288)
        Me.txtFix.Multiline = True
        Me.txtFix.Name = "txtFix"
        Me.txtFix.ReadOnly = True
        Me.txtFix.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFix.Size = New System.Drawing.Size(360, 80)
        Me.txtFix.TabIndex = 6
        '
        'ContextMenu1
        '
        Me.ContextMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Select All"
        '
        'frmError
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(394, 416)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmError"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Oops..."
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Delegate Function Copier() As Boolean

    Private Function CopytoClipboard() As Boolean
        Try
            Dim sMsg As String

            sMsg = "Error Number: " & txtErrorNumber.Text & Environment.NewLine & _
                "Module Name: " & txtModuleName.Text & Environment.NewLine & _
                "Line Number: " & txtLineNumber.Text & Environment.NewLine & _
                "Error Description: " & txtErrorDesc.Text & Environment.NewLine & _
                "Suggestion: " & txtFix.Text & Environment.NewLine

            txtCopy.Text = sMsg

            txtCopy.Focus()
            txtCopy.SelectAll()
            txtCopy.Copy()

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function
    Private Sub cmdCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCopy.Click
        Dim newCopier As New Copier(AddressOf CopytoClipboard)

        Me.Invoke(newCopier)
    End Sub

    Public Sub ShowError(ByVal nError As Long, ByVal sModule As String, ByVal sDesc As String, _
    Optional ByVal sLine As String = "Unavailable", Optional ByVal sFix As String = "Unavailable")
        txtErrorNumber.Text = nError
        txtModuleName.Text = sModule
        txtLineNumber.Text = sLine
        txtErrorDesc.Text = sDesc
        txtFix.Text = sFix

        Beep()

        Me.ShowDialog()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Me.Close()
    End Sub

    Private Sub frmError_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'FormatForWinXP(Me)

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        txtErrorDesc.SelectAll()
    End Sub
End Class
