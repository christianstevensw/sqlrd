Public Class frmSmartFolders
    Inherits System.Windows.Forms.Form
    Const S1 As String = "Step 1: Smart Folder Name"
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Const S2 As String = "Step 2: Smart Folder Definition"
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbAnyAll As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbOperator As System.Windows.Forms.ComboBox
    Friend WithEvents cmdAddWhere As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveWhere As System.Windows.Forms.Button
    Friend WithEvents lsvCriteria As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblMain As System.Windows.Forms.Label
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSmartFolders))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.Step2 = New System.Windows.Forms.Panel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lsvCriteria = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddWhere = New System.Windows.Forms.Button
        Me.cmdRemoveWhere = New System.Windows.Forms.Button
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbOperator = New System.Windows.Forms.ComboBox
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.cmbAnyAll = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblMain = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Footer1 = New WizardFooter.Footer
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(376, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.SystemColors.Control
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Location = New System.Drawing.Point(0, 72)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(432, 352)
        Me.Step1.TabIndex = 1
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(16, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(392, 21)
        Me.txtName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Name"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(16, 96)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(392, 184)
        Me.txtDesc.TabIndex = 1
        Me.txtDesc.Tag = "memo"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 80)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Description"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(349, 442)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 14
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(269, 442)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 15
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(189, 442)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(73, 25)
        Me.cmdBack.TabIndex = 16
        Me.cmdBack.Text = "&Back"
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(349, 442)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 20
        Me.cmdNext.Text = "&Next"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.GroupBox2)
        Me.Step2.Controls.Add(Me.cmbAnyAll)
        Me.Step2.Controls.Add(Me.Label4)
        Me.Step2.Controls.Add(Me.Label5)
        Me.Step2.Location = New System.Drawing.Point(0, 72)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(432, 352)
        Me.Step2.TabIndex = 21
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lsvCriteria)
        Me.GroupBox2.Controls.Add(Me.cmdAddWhere)
        Me.GroupBox2.Controls.Add(Me.cmdRemoveWhere)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbOperator)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 48)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 296)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        '
        'lsvCriteria
        '
        Me.lsvCriteria.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvCriteria.FullRowSelect = True
        Me.lsvCriteria.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvCriteria.Location = New System.Drawing.Point(8, 72)
        Me.lsvCriteria.Name = "lsvCriteria"
        Me.lsvCriteria.Size = New System.Drawing.Size(384, 216)
        Me.lsvCriteria.TabIndex = 15
        Me.lsvCriteria.UseCompatibleStateImageBehavior = False
        Me.lsvCriteria.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Criteria"
        Me.ColumnHeader1.Width = 210
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = ""
        Me.ColumnHeader2.Width = 168
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(352, 40)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdAddWhere.TabIndex = 14
        Me.cmdAddWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(256, 40)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemoveWhere.TabIndex = 13
        Me.cmdRemoveWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmbColumn
        '
        Me.cmbColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Items.AddRange(New Object() {"Bcc", "Cc", "Description", "EndDate", "ErrMsg", "FolderName", "Frequency", "Keyword", "Message", "NextRun", "ReportTitle", "ScheduleName", "ScheduleType", "SendTo", "StartDate", "StartTime", "Status", "Subject"})
        Me.cmbColumn.Location = New System.Drawing.Point(8, 16)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(112, 21)
        Me.cmbColumn.Sorted = True
        Me.cmbColumn.TabIndex = 8
        '
        'cmbOperator
        '
        Me.cmbOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbOperator.ItemHeight = 13
        Me.cmbOperator.Items.AddRange(New Object() {"=", "<>", ">=", "<=", ">", "<", "STARTS WITH", "ENDS WITH", "CONTAINS", "DOES NOT CONTAIN"})
        Me.cmbOperator.Location = New System.Drawing.Point(136, 16)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(104, 21)
        Me.cmbOperator.TabIndex = 8
        '
        'cmbValue
        '
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbValue.Location = New System.Drawing.Point(256, 16)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(136, 21)
        Me.cmbValue.TabIndex = 8
        '
        'cmbAnyAll
        '
        Me.cmbAnyAll.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAnyAll.ItemHeight = 13
        Me.cmbAnyAll.Items.AddRange(New Object() {"Any", "All"})
        Me.cmbAnyAll.Location = New System.Drawing.Point(64, 14)
        Me.cmbAnyAll.Name = "cmbAnyAll"
        Me.cmbAnyAll.Size = New System.Drawing.Size(64, 21)
        Me.cmbAnyAll.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(48, 16)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Match"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(136, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(288, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "of these conditions:"
        '
        'lblMain
        '
        Me.lblMain.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblMain.ForeColor = System.Drawing.Color.Navy
        Me.lblMain.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lblMain.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblMain.Location = New System.Drawing.Point(8, 16)
        Me.lblMain.Name = "lblMain"
        Me.lblMain.Size = New System.Drawing.Size(336, 32)
        Me.lblMain.TabIndex = 6
        Me.lblMain.Text = "Step 2: Smart Folder Definition"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblMain)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(434, 64)
        Me.Panel1.TabIndex = 22
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 424)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(440, 16)
        Me.Footer1.TabIndex = 24
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(434, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 25
        '
        'frmSmartFolders
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(434, 479)
        Me.ControlBox = False
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "frmSmartFolders"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Smart Folder Wizard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim nStep As Integer = 0
    Private Sub frmSmartFolders_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Step1.BringToFront()
        lblMain.Text = S1
        cmbAnyAll.Text = "All"
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumn.SelectedIndexChanged
        Dim oData As New clsMarsData
        Dim SQL(3) As String
        Dim oRs As ADODB.Recordset

        cmbValue.Items.Clear()
        cmbOperator.Items.Clear()

        ep.SetError(sender, String.Empty)

        Select Case cmbColumn.Text
            Case "Status", "ScheduleType"
                With cmbOperator.Items
                    .Add("=")
                End With
            Case "Message", "Description", "Keyword", "ScheduleName", "ReportTitle", "SendTo", _
                "Cc", "Bcc", "Subject", "ErrMsg"
                With cmbOperator.Items
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                    .Add("IS EMPTY")
                    .Add("IS NOT EMPTY")
                End With
            Case "ScheduleType", "Frequency", "DestinationType", "PrinterName", "OutputFormat", _
                 "FTPServer", _
                 "FTPUserName", "FolderName"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
            Case "StartDate", "EndDate", "NextRun"

                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                End With
            Case Else
                With cmbOperator.Items
                    .Add("=")
                    .Add("<>")
                    .Add(">=")
                    .Add("<=")
                    .Add(">")
                    .Add("<")
                    .Add("STARTS WITH")
                    .Add("ENDS WITH")
                    .Add("CONTAINS")
                    .Add("DOES NOT CONTAIN")
                End With
        End Select

        Select Case cmbColumn.Text.ToLower
            Case "frequency"
                With cmbValue.Items
                    .Add("Daily")
                    .Add("Weekly")
                    .Add("Monthly")
                    .Add("Yearly")
                    .Add("Custom")
                    .Add("None")
                End With
            Case "status"
                With cmbValue.Items
                    .Add("0")
                    .Add("1")
                End With
            Case "destinationtype"
                With cmbValue.Items
                    .Add("Email")
                    .Add("Disk")
                    .Add("Printer")
                    .Add("FTP")
                End With
            Case "printername"
                Dim strItem As String
                Dim oPrint As New System.Drawing.Printing.PrinterSettings

                For Each strItem In Printing.PrinterSettings.InstalledPrinters
                    cmbValue.Items.Add(strItem)
                Next
            Case "outputformat"
                With cmbValue.Items
                    .Add("Acrobat Format (*.pdf)")
                    '.Add("Crystal Reports (*.rpt)")
                    .Add("CSV (*.csv)")
                    .Add("Data Interchange Format (*.dif)")
                    .Add("dBase II (*.dbf)")
                    .Add("dBase III (*.dbf)")
                    .Add("dBase IV (*.dbf)")
                    .Add("HTML (*.htm)")
                    .Add("Lotus 1-2-3 (*.wk1)")
                    .Add("Lotus 1-2-3 (*.wk3)")
                    .Add("Lotus 1-2-3 (*.wk4)")
                    .Add("Lotus 1-2-3 (*.wks)")
                    .Add("MS Excel - Data Only (*.xls)")
                    .Add("MS Excel 7 (*.xls)")
                    .Add("MS Excel 8 (*.xls)")
                    .Add("MS Excel 97-2000 (*.xls)")
                    .Add("MS Word (*.doc)")
                    .Add("ODBC (*.odbc)")
                    .Add("Rich Text Format (*.rtf)")
                    .Add("Tab Seperated (*.txt)")
                    .Add("Text (*.txt)")
                    .Add("TIFF (*.tif)")
                    .Add("XML (*.xml)")
                End With
            Case "schedulename"
                SQL(0) = "SELECT DISTINCT ReportTitle FROM ReportAttr"
                SQL(1) = "SELECT DISTINCT PackageName FROM PackageAttr"
                SQL(2) = "SELECT DISTINCT AutoName FROM AutomationAttr"
                SQL(3) = "SELECT DISTINCT EventName FROM EventAttr"

                For Each s As String In SQL
                    oRs = clsMarsData.GetData(s)

                    If Not oRs Is Nothing Then
                        Do While oRs.EOF = False
                            cmbValue.Items.Add(oRs.Fields(0).Value)
                            oRs.MoveNext()
                        Loop

                        oRs.Close()
                    End If
                Next
            Case "foldername"
                SQL(0) = "SELECT DISTINCT FolderName FROM Folders"

                oRs = clsMarsData.GetData(SQL(0))

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        cmbValue.Items.Add(oRs(0).Value)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()

                End If
            Case "scheduletype"
                With cmbValue.Items
                    .Add("Single Schedule")
                    .Add("Package Schedule")
                    .Add("Automation Schedule")
                    .Add("Event-Based Schedule")
                End With
        End Select
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click
        Dim olsv As ListViewItem
        Dim sCriteria As String

        If cmbColumn.Text.Length = 0 Then
            ep.SetError(cmbColumn, "Please select the column for the criteria")
            cmbColumn.Focus()
            Return
        ElseIf cmbOperator.Text.Length = 0 Then
            ep.SetError(cmbOperator, "Please select the operator the criteria")
            cmbOperator.Focus()
            Return
        End If

        olsv = New ListViewItem
        olsv.Text = cmbColumn.Text

        Select Case cmbOperator.Text.ToLower
            Case "starts with"
                sCriteria = "LIKE '" & cmbValue.Text & "%'"
            Case "ends with"
                sCriteria = "LIKE '%" & cmbValue.Text & "'"
            Case "contains"
                sCriteria = "LIKE '%" & cmbValue.Text & "%'"
            Case "does not contain"
                sCriteria = " NOT LIKE '%" & cmbValue.Text & "%'"
            Case "is empty"
                sCriteria = " IS EMPTY"
            Case "is not empty"
                sCriteria = " IS NOT EMPTY"
            Case Else
                sCriteria = cmbOperator.Text & " '" & cmbValue.Text & "'"
        End Select

        olsv.SubItems.Add(sCriteria)

        lsvCriteria.Items.Add(olsv)
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvCriteria.SelectedItems.Count = 0 Then Return

        lsvCriteria.SelectedItems(0).Remove()
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Dim oData As New clsMarsData

        Select Case nStep
            Case 0
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please enter the name for the smart folder")
                    txtName.Focus()
                    Return
                ElseIf clsMarsData.IsDuplicate("SmartFolders", "SmartName", _
                    txtName.Text, True) Then

                    ep.SetError(txtName, "A smart folder with this name already exists")
                    txtName.Focus()

                    Return
                End If

                Step2.BringToFront()
                cmdBack.Enabled = True
                cmdFinish.Visible = True
                cmdNext.Visible = False
                lblMain.Text = S2
        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 1
                Step1.BringToFront()
                cmdBack.Enabled = False
                cmdNext.Visible = True
                cmdFinish.Visible = False
                lblMain.Text = S1
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        If lsvCriteria.Items.Count = 0 Then
            ep.SetError(lsvCriteria, "Please create the selection criteria")
            Return
        End If

        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim SQL As String
        Dim Success As Boolean
        Dim nID As Integer
        Dim I As Integer = 1

        cmdFinish.Enabled = False

        nID = clsMarsData.CreateDataID("smartfolders", "smartid")

        sCols = "SmartID,SmartName,SmartDesc,SmartType,Owner"

        sVals = nID & ",'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(txtDesc.Text) & "'," & _
            "'" & cmbAnyAll.Text & "'," & _
            "'" & gUser & "'"

        SQL = "INSERT INTO SmartFolders (" & sCols & ") VALUES (" & sVals & ")"


        Success = clsMarsData.WriteData(SQL)

        If Success = False Then Return

        sCols = "SmartDefID,SmartID,SmartColumn, SmartCriteria"

        For Each olsv As ListViewItem In lsvCriteria.Items
            sVals = clsMarsData.CreateDataID("smartfolderattr", "smartdefid") & "," & _
            nID & "," & _
            "'" & SQLPrepare(olsv.Text) & "'," & _
            "'" & SQLPrepare(olsv.SubItems(1).Text) & "'"

            SQL = "INSERT INTO SmartFolderAttr (" & sCols & ") VALUES (" & sVals & ")"

            Success = clsMarsData.WriteData(SQL)

            If Success = False Then GoTo Hell
            I = I + 1
        Next

        Dim oUser As New clsMarsUsers

        oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewSmartFolder)

        On Error Resume Next

        Dim oForm As frmWindow
        Dim oUI As New clsMarsUI

        oForm = oWindow(nWindowCurrent)

        If Not oForm Is Nothing Then

            oUI.AddSmartFolders(oForm.tvSmartFolders, True)

            oForm.tbNav.SelectedTabIndex = 2

            oForm.lsvWindow.Items.Clear()

            oUI.FindNode("SmartFolder:" & nID, oForm.tvSmartFolders)

            'For Each oNode As TreeNode In oForm.tvSmartFolders.Nodes
            '    If oNode.Text = txtName.Text Then
            '        oForm.tvSmartFolders.SelectedNode = oNode
            '        Exit For
            '    End If
            'Next
        End If

        Close()

        Return
Hell:
        clsMarsData.WriteData("DELETE FROM SmartFolders WHERE SmartID = " & nID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub cmbAnyAll_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbAnyAll.SelectedIndexChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub cmbOperator_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbOperator.SelectedIndexChanged
        ep.SetError(sender, String.Empty)

        If cmbOperator.Text.ToLower.IndexOf("empty") > -1 Then
            cmbValue.Enabled = False
        Else
            cmbValue.Enabled = True
        End If
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        ep.SetError(sender, String.Empty)
    End Sub
End Class
