Imports sqlrd.clsMarsData

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmEventSub
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
    Dim ep As New ErrorProvider
    Public m_eventID As Integer = 0

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbParName As System.Windows.Forms.ComboBox
    Friend WithEvents cmbParValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdAddSchedule As System.Windows.Forms.Button
    Friend WithEvents lsvSubs As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lsvSubs = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddSchedule = New System.Windows.Forms.Button
        Me.cmbParName = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbParValue = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lsvSubs)
        Me.GroupBox1.Controls.Add(Me.cmdAddSchedule)
        Me.GroupBox1.Controls.Add(Me.cmbParName)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmbParValue)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(504, 328)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'lsvSubs
        '
        Me.lsvSubs.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvSubs.FullRowSelect = True
        Me.lsvSubs.GridLines = True
        Me.lsvSubs.HideSelection = False
        Me.lsvSubs.Location = New System.Drawing.Point(8, 96)
        Me.lsvSubs.Name = "lsvSubs"
        Me.lsvSubs.Size = New System.Drawing.Size(488, 224)
        Me.lsvSubs.TabIndex = 7
        Me.lsvSubs.UseCompatibleStateImageBehavior = False
        Me.lsvSubs.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Parameter Name"
        Me.ColumnHeader1.Width = 242
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Substitution Value"
        Me.ColumnHeader2.Width = 238
        '
        'cmdAddSchedule
        '
        Me.cmdAddSchedule.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAddSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddSchedule.Location = New System.Drawing.Point(256, 64)
        Me.cmdAddSchedule.Name = "cmdAddSchedule"
        Me.cmdAddSchedule.Size = New System.Drawing.Size(72, 23)
        Me.cmdAddSchedule.TabIndex = 5
        Me.cmdAddSchedule.Text = "Add"
        Me.cmdAddSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbParName
        '
        Me.cmbParName.Location = New System.Drawing.Point(8, 32)
        Me.cmbParName.Name = "cmbParName"
        Me.cmbParName.Size = New System.Drawing.Size(240, 21)
        Me.cmbParName.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the parameter from the list"
        '
        'cmbParValue
        '
        Me.cmbParValue.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbParValue.Items.AddRange(New Object() {"[Email Bcc]", "[Email Body]", "[Email Cc]", "[Email From]", "[Email Subject]", "[Email To]"})
        Me.cmbParValue.Location = New System.Drawing.Point(256, 32)
        Me.cmbParValue.Name = "cmbParValue"
        Me.cmbParValue.Size = New System.Drawing.Size(240, 21)
        Me.cmbParValue.Sorted = True
        Me.cmbParValue.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(256, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(232, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Substitution Value"
        '
        'cmdRemove
        '
        Me.cmdRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(176, 64)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(72, 23)
        Me.cmdRemove.TabIndex = 6
        Me.cmdRemove.Text = "Remove"
        Me.cmdRemove.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdOK
        '
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(352, 344)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(437, 344)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'frmEventSub
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(522, 376)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmEventSub"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parameter Substitution"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Private Sub GetEventData()

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim nID As Integer = Me.m_eventID


            SQL = "SELECT * FROM EventConditions WHERE EventID = " & nID

            oRs = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    Dim conditionType As String = oRs("conditiontype").Value
                    Dim conditionName As String = oRs("conditionname").Value

                    Select Case conditionType.ToLower
                        Case "database record exists", "database record has changed"
                            Dim oRs1 As ADODB.Recordset = New ADODB.Recordset
                            Dim oCon As ADODB.Connection = New ADODB.Connection
                            Dim connectionString As String
                            Dim user As String
                            Dim password As String
                            Dim dsn As String

                            SQL = oRs("searchcriteria").Value
                            connectionString = oRs("connectionstring").Value

                            dsn = connectionString.Split("|")(0)
                            user = connectionString.Split("|")(1)
                            password = connectionString.Split("|")(2)

                            password = _DecryptDBValue(password)

                            oCon.Open(dsn, user, password)

                            oRs1.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenStatic)

                            For Each field As ADODB.Field In oRs1.Fields
                                cmbParValue.Items.Add(conditionName & ";[" & field.Name & "]")
                            Next

                            oRs1.Close()

                            oCon.Close()
                        Case "file exists", "file has been modified"
                            With cmbParValue.Items
                                .Add(conditionName & ";FileName")
                                .Add(conditionName & ";FileDirectory")
                                .Add(conditionName & ";FilePath")
                                .Add(conditionName & ";DateModified")
                                .Add(conditionName & ";DateCreated")
                            End With

                        Case "process exists", "window is present"
                            With cmbParValue.Items
                                .Add(conditionName & ";ProcessName")
                                .Add(conditionName & ";MainWindowTitle")
                                .Add(conditionName & ";TotalProcessorTime")
                                .Add(conditionName & ";UserProcessorTime")
                                .Add(conditionName & ";MaxWorkingSet")
                                .Add(conditionName & ";MinWorkingSet")
                                .Add(conditionName & ";ID")
                            End With

                        Case "unread email is present"
                            With cmbParValue.Items
                                .Add(conditionName & ";From")
                                .Add(conditionName & ";To")
                                .Add(conditionName & ";Cc")
                                .Add(conditionName & ";Bcc")
                                .Add(conditionName & ";Subject")
                                .Add(conditionName & ";Message")
                                .Add(conditionName & ";DateSent")
                                .Add(conditionName & ";Custom")
                            End With
                    End Select

                    oRs.MoveNext()
                Loop

                Dim col As ComboBox.ObjectCollection = cmbParValue.Items
                Dim items As ArrayList = New ArrayList

                For Each s As String In col
                    s = "<[e]" & s & ">"
                    items.Add(s)
                Next

                cmbParValue.Items.Clear()

                For Each s As String In items
                    cmbParValue.Items.Add(s)
                Next

                oRs.Close()
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, ex.TargetSite.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Private Sub frmEventSub_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub SetSubstitution(ByVal nReportID As Integer, ByVal nEventID As Integer)
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.e1_BasicEventsPack) Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim sReport As String
            Dim sName As String

            oRs = clsMarsData.GetData("SELECT * FROM ReportParameter WHERE ReportID =" & nReportID)

            If oRs Is Nothing Then
                Return
            ElseIf oRs.EOF = True Then
                MessageBox.Show("This report does not contain any parameters", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            Else
                cmbParName.Items.Clear()

                Do While oRs.EOF = False
                    Me.cmbParName.Items.Add(oRs("parname").Value)
                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRs = GetData("SELECT * FROM EventSubAttr WHERE ReportID = " & nReportID & " AND EventID =" & nEventID)

                If Not oRs Is Nothing Then
                    Do While oRs.EOF = False
                        Dim oItem As ListViewItem = New ListViewItem

                        oItem.Text = oRs("parname").Value
                        oItem.SubItems.Add(oRs("parvalue").Value)

                        lsvSubs.Items.Add(oItem)

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                Me.ShowDialog()
            End If

            If UserCancel = True Then
                Return
            End If

            SQL = "DELETE FROM EventSubAttr WHERE ReportID = " & nReportID & " AND EventID =" & nEventID

            WriteData(SQL)

            Dim sCols As String
            Dim sVals As String

            sCols = "EventSubID, ReportID, EventID, ParName, ParValue"

            For Each oItem As ListViewItem In lsvSubs.Items
                sVals = CreateDataID("eventsubattr", "eventsubid") & "," & _
                nReportID & "," & _
                nEventID & "," & _
                "'" & SQLPrepare(oItem.Text) & "'," & _
                "'" & SQLPrepare(oItem.SubItems(1).Text) & "'"

                SQL = "INSERT INTO EventSubAttr (" & sCols & ") VALUES (" & sVals & ")"

                If WriteData(SQL) = False Then
                    Return
                End If
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click

        Close()
    End Sub

    Private Sub cmdAddSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddSchedule.Click
        If cmbParName.Text.Length = 0 Then
            ep.SetError(cmbParName, "Please select the parameter")
            cmbParName.Focus()
            Return
        ElseIf cmbParValue.Text.Length = 0 Then
            ep.SetError(cmbParValue, "Please select the substitute value")
            cmbParValue.Focus()
            Return
        End If

        Dim oItem As ListViewItem

        For Each oItem In lsvSubs.Items
            If oItem.Text = cmbParName.Text Then
                If oItem.SubItems(1).Text = cmbParValue.Text Then
                    Return
                Else
                    oItem.Remove()
                End If
            End If
        Next

        oItem = New ListViewItem

        oItem.Text = cmbParName.Text
        oItem.SubItems.Add(cmbParValue.Text)

        lsvSubs.Items.Add(oItem)
    End Sub

    Private Sub cmbParName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbParName.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmbParValue_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbParValue.DropDown
        cmbParValue.Items.Clear()

        Me.GetEventData()
    End Sub

    Private Sub cmbParValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbParValue.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        For Each oItem As ListViewItem In lsvSubs.SelectedItems
            oItem.Remove()
        Next
    End Sub
End Class
