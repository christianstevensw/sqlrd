Public Class frmSupportFiles
    Inherits System.Windows.Forms.Form
    Dim ep As ErrorProvider = New ErrorProvider
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents grpExcept As System.Windows.Forms.GroupBox
    Friend WithEvents CheckBox4 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox5 As System.Windows.Forms.CheckBox
    Dim UserCancel As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Footer1 As WizardFooter.Footer
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtLoc As System.Windows.Forms.TextBox
    Friend WithEvents chkEncrypt As System.Windows.Forms.CheckBox
    Friend WithEvents txtExt As System.Windows.Forms.TextBox
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chkExt As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSupportFiles))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Footer1 = New WizardFooter.Footer
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.grpExcept = New System.Windows.Forms.GroupBox
        Me.CheckBox5 = New System.Windows.Forms.CheckBox
        Me.CheckBox4 = New System.Windows.Forms.CheckBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.txtExt = New System.Windows.Forms.TextBox
        Me.chkEncrypt = New System.Windows.Forms.CheckBox
        Me.chkExt = New System.Windows.Forms.CheckBox
        Me.txtLoc = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.grpExcept.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(496, 64)
        Me.Panel1.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Arial", 15.75!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(240, 23)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Support Files"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(432, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(56, 48)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(8, 310)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(320, 16)
        Me.Footer1.TabIndex = 2
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.grpExcept)
        Me.GroupBox2.Controls.Add(Me.cmdBrowse)
        Me.GroupBox2.Controls.Add(Me.txtExt)
        Me.GroupBox2.Controls.Add(Me.chkEncrypt)
        Me.GroupBox2.Controls.Add(Me.chkExt)
        Me.GroupBox2.Controls.Add(Me.txtLoc)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 72)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(480, 232)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        '
        'grpExcept
        '
        Me.grpExcept.Controls.Add(Me.CheckBox5)
        Me.grpExcept.Controls.Add(Me.CheckBox4)
        Me.grpExcept.Controls.Add(Me.CheckBox3)
        Me.grpExcept.Controls.Add(Me.CheckBox2)
        Me.grpExcept.Controls.Add(Me.CheckBox1)
        Me.grpExcept.Enabled = False
        Me.grpExcept.Location = New System.Drawing.Point(8, 132)
        Me.grpExcept.Name = "grpExcept"
        Me.grpExcept.Size = New System.Drawing.Size(456, 94)
        Me.grpExcept.TabIndex = 6
        Me.grpExcept.TabStop = False
        Me.grpExcept.Text = "Do not export the following tables"
        '
        'CheckBox5
        '
        Me.CheckBox5.AutoSize = True
        Me.CheckBox5.Location = New System.Drawing.Point(152, 43)
        Me.CheckBox5.Name = "CheckBox5"
        Me.CheckBox5.Size = New System.Drawing.Size(85, 17)
        Me.CheckBox5.TabIndex = 0
        Me.CheckBox5.Tag = "sendwait"
        Me.CheckBox5.Text = "Email Queue"
        Me.CheckBox5.UseVisualStyleBackColor = True
        '
        'CheckBox4
        '
        Me.CheckBox4.AutoSize = True
        Me.CheckBox4.Location = New System.Drawing.Point(152, 20)
        Me.CheckBox4.Name = "CheckBox4"
        Me.CheckBox4.Size = New System.Drawing.Size(120, 17)
        Me.CheckBox4.TabIndex = 0
        Me.CheckBox4.Tag = "readreceiptslog"
        Me.CheckBox4.Text = "Read Receipts Logs"
        Me.CheckBox4.UseVisualStyleBackColor = True
        '
        'CheckBox3
        '
        Me.CheckBox3.AutoSize = True
        Me.CheckBox3.Location = New System.Drawing.Point(6, 66)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(128, 17)
        Me.CheckBox3.TabIndex = 0
        Me.CheckBox3.Tag = "reportdurationtracker,reportduration"
        Me.CheckBox3.Text = "Report Duration Logs"
        Me.CheckBox3.UseVisualStyleBackColor = True
        '
        'CheckBox2
        '
        Me.CheckBox2.AutoSize = True
        Me.CheckBox2.Location = New System.Drawing.Point(6, 43)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(106, 17)
        Me.CheckBox2.TabIndex = 0
        Me.CheckBox2.Tag = "historydetailattr,schedulehistory"
        Me.CheckBox2.Text = "Schedule History"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.Location = New System.Drawing.Point(6, 20)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(70, 17)
        Me.CheckBox1.TabIndex = 0
        Me.CheckBox1.Tag = "emaillog"
        Me.CheckBox1.Text = "Email Log"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'cmdBrowse
        '
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(408, 32)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(56, 23)
        Me.cmdBrowse.TabIndex = 5
        Me.cmdBrowse.Text = "..."
        '
        'txtExt
        '
        Me.txtExt.Enabled = False
        Me.txtExt.Location = New System.Drawing.Point(160, 104)
        Me.txtExt.Name = "txtExt"
        Me.txtExt.Size = New System.Drawing.Size(96, 21)
        Me.txtExt.TabIndex = 4
        '
        'chkEncrypt
        '
        Me.chkEncrypt.Location = New System.Drawing.Point(8, 64)
        Me.chkEncrypt.Name = "chkEncrypt"
        Me.chkEncrypt.Size = New System.Drawing.Size(464, 32)
        Me.chkEncrypt.TabIndex = 3
        Me.chkEncrypt.Text = "Encrypt files (do not encrypt if your email security does not permit  " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "you to se" & _
            "nd encrypted files)"
        '
        'chkExt
        '
        Me.chkExt.Location = New System.Drawing.Point(8, 104)
        Me.chkExt.Name = "chkExt"
        Me.chkExt.Size = New System.Drawing.Size(152, 24)
        Me.chkExt.TabIndex = 2
        Me.chkExt.Text = "Change file extension"
        '
        'txtLoc
        '
        Me.txtLoc.Location = New System.Drawing.Point(8, 32)
        Me.txtLoc.Name = "txtLoc"
        Me.txtLoc.Size = New System.Drawing.Size(376, 21)
        Me.txtLoc.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(232, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Output location for support files"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(336, 310)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 4
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(416, 310)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 5
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(496, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 11
        '
        'frmSupportFiles
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(496, 338)
        Me.ControlBox = False
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmSupportFiles"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Create Support Files"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.grpExcept.ResumeLayout(False)
        Me.grpExcept.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmSupportFiles_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim fbd As FolderBrowserDialog = New FolderBrowserDialog

        With fbd
            .Description = "Please select the output folder"
            .ShowNewFolderButton = True

            If .ShowDialog = DialogResult.OK Then
                txtLoc.Text = .SelectedPath

                If txtLoc.Text.EndsWith("\") = False Then
                    txtLoc.Text &= "\"
                End If
            End If
        End With
    End Sub

    Private Sub chkExt_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExt.CheckedChanged
        txtExt.Enabled = chkExt.Checked
        ep.SetError(txtExt, "")
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtLoc.Text.Length = 0 Then
            ep.SetError(txtLoc, "Please select the path to save the support files")
            txtLoc.Focus()
            Return
        ElseIf chkExt.Checked And txtExt.Text.Length = 0 Then
            ep.SetError(txtExt, "Please provide the extension for the support files")
            txtExt.Focus()
            Return
        End If

        Close()
    End Sub

    Private Sub txtLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLoc.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtExt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtExt.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Public Sub _CreateSupportFiles()
        If gConType <> "DAT" Then
            Me.grpExcept.Enabled = True
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return
        Dim ok As Boolean
        Dim sFile As String = txtLoc.Text
        Dim oUI As New clsMarsUI

        Dim srcDSN As String
        Dim conString As String
        Dim dsn As String
        Dim user As String
        Dim pwd As String

        conString = oUI.ReadRegistry("ConString", "", True)
        If conString.Length > 0 Then
            dsn = conString.Split(";")(4).Split("=")(1)
            user = conString.Split(";")(3).Split("=")(1)
            pwd = conString.Split(";")(1).Split("=")(1)
            srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
        End If

        Try
            If gConType <> "DAT" Then
                Dim exceptTables As ArrayList = New ArrayList

                For Each ctrl As Control In Me.grpExcept.Controls
                    Dim chk As CheckBox = CType(ctrl, CheckBox)

                    If chk.Checked Then
                        For Each s As String In chk.Tag.ToString.Split(",")
                            If s <> "" Then exceptTables.Add(chk.Tag)
                        Next
                    End If
                Next

                If exceptTables.Count = 0 Then exceptTables = Nothing

                oUI.BusyProgress(20, "Exporting data to file...")

                'ok = clsMigration.MigrateSystem(clsMigration.m_datConString, "", "", exceptTables)
                Dim dm As New DataMigrator
                ok = dm.Export(srcDSN, sAppPath & "sqlrdlive.def", exceptTables)

                If ok = False Then
                    Throw New Exception("Could not create def file")
                End If
            Else
                ok = True
            End If

            Dim FilezToZip As String
            Dim sRegFile As String

            oUI.BusyProgress(60, "Adding files to archive...")

            If gConType <> "DAT" Then
                FilezToZip = sAppPath & "sqlrdlive.dat"
            Else
                FilezToZip = sAppPath & "sqlrdlive.dat"
            End If

            If IO.File.Exists(sAppPath & "sqlrderror.log") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrderror.log"
            End If

            If IO.File.Exists(sAppPath & "sqlrdlive.def") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdlive.def"
            End If

            If oUI.ExportRegistry(sKey, sAppPath & "sqlrdreg.txt") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdreg.txt"
            End If

            Dim oTools As New clsSystemTools

            If oTools._CreateSysInfoFile(sAppPath & "sqlrdinfo.txt") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdinfo.txt"
            End If

            If IO.File.Exists(sAppPath & "sqlrdlive.config") = True Then
                FilezToZip &= "|" & sAppPath & "sqlrdlive.config"
            End If

            If IO.File.Exists(sAppPath & "eventlog.dat") Then
                FilezToZip &= "|" & sAppPath & "eventlog.dat"
            End If

            For Each s As String In IO.Directory.GetFiles(sAppPath)
                If s.EndsWith(".debug") Then
                    FilezToZip &= "|" & s
                End If
            Next

            oUI.BusyProgress(80, "Reading customer information...")

            Dim nCustID As String

            nCustID = oUI.ReadRegistry("CustNo", 0)

            Dim oTask As New clsMarsTask

            Dim ZipFile As String = "SQL-RD_" & nCustID

            oUI.BusyProgress(90, "Zipping up files...")

            If chkEncrypt.Checked Then
                oTask._ZipFiles(sFile & ZipFile, FilezToZip, True, "SQLRDAdmin")
            Else
                oTask._ZipFiles(sFile & ZipFile, FilezToZip)
            End If

            If chkExt.Checked Then

                If txtExt.Text.StartsWith(".") = False Then
                    txtExt.Text = "." & txtExt.Text
                End If

                oUI.BusyProgress(90, "Renaming file...")

                Dim originalFile As String = sFile & ZipFile & ".zip"
                Dim newFile As String = sFile & ZipFile & txtExt.Text

                IO.File.Move(originalFile, newFile)
            End If

            oUI.BusyProgress(, , True)

            MessageBox.Show("Support files created successfully.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
        End Try
    End Sub
End Class
