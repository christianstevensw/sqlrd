Public Class frmSMTP
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Dim oData As New clsMarsData
    Dim ep As New ErrorProvider
    Friend WithEvents txtSMTPPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents cmdAdvanced As System.Windows.Forms.Button
    Dim Mode As String
    Dim m_serverID As Integer
    Dim m_tblValues As DataTable

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpSMTP As System.Windows.Forms.GroupBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPServer As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPSenderAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPSenderName As System.Windows.Forms.TextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents chkBackup As System.Windows.Forms.CheckBox
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmbSMTPTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label2 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSMTP))
        Me.grpSMTP = New System.Windows.Forms.GroupBox
        Me.cmdAdvanced = New System.Windows.Forms.Button
        Me.txtSMTPPort = New System.Windows.Forms.NumericUpDown
        Me.Label49 = New System.Windows.Forms.Label
        Me.cmbSMTPTimeout = New System.Windows.Forms.NumericUpDown
        Me.chkBackup = New System.Windows.Forms.CheckBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtSMTPUserID = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtSMTPPassword = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtSMTPServer = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSMTPSenderAddress = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtSMTPSenderName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdTest = New System.Windows.Forms.Button
        Me.grpSMTP.SuspendLayout()
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grpSMTP
        '
        Me.grpSMTP.Controls.Add(Me.cmdAdvanced)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPort)
        Me.grpSMTP.Controls.Add(Me.Label49)
        Me.grpSMTP.Controls.Add(Me.cmbSMTPTimeout)
        Me.grpSMTP.Controls.Add(Me.chkBackup)
        Me.grpSMTP.Controls.Add(Me.Label19)
        Me.grpSMTP.Controls.Add(Me.txtSMTPUserID)
        Me.grpSMTP.Controls.Add(Me.Label8)
        Me.grpSMTP.Controls.Add(Me.Label9)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPassword)
        Me.grpSMTP.Controls.Add(Me.Label10)
        Me.grpSMTP.Controls.Add(Me.txtSMTPServer)
        Me.grpSMTP.Controls.Add(Me.Label11)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderAddress)
        Me.grpSMTP.Controls.Add(Me.Label12)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderName)
        Me.grpSMTP.Controls.Add(Me.Label1)
        Me.grpSMTP.Controls.Add(Me.txtName)
        Me.grpSMTP.Controls.Add(Me.Label2)
        Me.grpSMTP.Location = New System.Drawing.Point(8, 7)
        Me.grpSMTP.Name = "grpSMTP"
        Me.grpSMTP.Size = New System.Drawing.Size(400, 253)
        Me.grpSMTP.TabIndex = 0
        Me.grpSMTP.TabStop = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.Location = New System.Drawing.Point(293, 193)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdvanced.TabIndex = 8
        Me.cmdAdvanced.Text = "Advanced"
        Me.cmdAdvanced.UseVisualStyleBackColor = True
        '
        'txtSMTPPort
        '
        Me.txtSMTPPort.Location = New System.Drawing.Point(310, 104)
        Me.txtSMTPPort.Maximum = New Decimal(New Integer() {80000, 0, 0, 0})
        Me.txtSMTPPort.Name = "txtSMTPPort"
        Me.txtSMTPPort.Size = New System.Drawing.Size(58, 20)
        Me.txtSMTPPort.TabIndex = 4
        Me.txtSMTPPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSMTPPort.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'Label49
        '
        Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label49.Location = New System.Drawing.Point(261, 107)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(43, 16)
        Me.Label49.TabIndex = 9
        Me.Label49.Text = "Port"
        '
        'cmbSMTPTimeout
        '
        Me.cmbSMTPTimeout.Location = New System.Drawing.Point(136, 193)
        Me.cmbSMTPTimeout.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.cmbSMTPTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbSMTPTimeout.Name = "cmbSMTPTimeout"
        Me.cmbSMTPTimeout.Size = New System.Drawing.Size(80, 20)
        Me.cmbSMTPTimeout.TabIndex = 7
        Me.cmbSMTPTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbSMTPTimeout.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'chkBackup
        '
        Me.chkBackup.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkBackup.Location = New System.Drawing.Point(136, 223)
        Me.chkBackup.Name = "chkBackup"
        Me.chkBackup.Size = New System.Drawing.Size(240, 22)
        Me.chkBackup.TabIndex = 9
        Me.chkBackup.Text = "Use as backup server"
        '
        'Label19
        '
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 195)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(96, 15)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "SMTP Timeout"
        '
        'txtSMTPUserID
        '
        Me.txtSMTPUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPUserID.Location = New System.Drawing.Point(136, 45)
        Me.txtSMTPUserID.Name = "txtSMTPUserID"
        Me.txtSMTPUserID.Size = New System.Drawing.Size(232, 20)
        Me.txtSMTPUserID.TabIndex = 1
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 14)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "User ID"
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 74)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 15)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Password"
        '
        'txtSMTPPassword
        '
        Me.txtSMTPPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMTPPassword.Location = New System.Drawing.Point(136, 74)
        Me.txtSMTPPassword.Name = "txtSMTPPassword"
        Me.txtSMTPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMTPPassword.Size = New System.Drawing.Size(232, 20)
        Me.txtSMTPPassword.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 104)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 15)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Server Name"
        '
        'txtSMTPServer
        '
        Me.txtSMTPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPServer.Location = New System.Drawing.Point(136, 104)
        Me.txtSMTPServer.Name = "txtSMTPServer"
        Me.txtSMTPServer.Size = New System.Drawing.Size(119, 20)
        Me.txtSMTPServer.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 134)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 15)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Sender Address"
        '
        'txtSMTPSenderAddress
        '
        Me.txtSMTPSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderAddress.Location = New System.Drawing.Point(136, 134)
        Me.txtSMTPSenderAddress.Name = "txtSMTPSenderAddress"
        Me.txtSMTPSenderAddress.Size = New System.Drawing.Size(232, 20)
        Me.txtSMTPSenderAddress.TabIndex = 5
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 163)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 15)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Sender Name"
        '
        'txtSMTPSenderName
        '
        Me.txtSMTPSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderName.Location = New System.Drawing.Point(136, 163)
        Me.txtSMTPSenderName.Name = "txtSMTPSenderName"
        Me.txtSMTPSenderName.Size = New System.Drawing.Size(232, 20)
        Me.txtSMTPSenderName.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Config Name"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(136, 15)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(232, 20)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(224, 195)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "seconds"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(248, 267)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 22)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(336, 267)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 22)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdTest
        '
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(8, 267)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 22)
        Me.cmdTest.TabIndex = 1
        Me.cmdTest.Text = "Test"
        '
        'frmSMTP
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(418, 297)
        Me.Controls.Add(Me.cmdTest)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.grpSMTP)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmSMTP"
        Me.Text = "SMTP Server Configuration"
        Me.grpSMTP.ResumeLayout(False)
        Me.grpSMTP.PerformLayout()
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Public Sub _AddServer()
        Mode = "Add"

        Me.cmdOK.Enabled = False

        Me.m_serverID = clsMarsData.CreateDataID("smtpservers", "smtpid")

        Me.m_tblValues = New DataTable

        With Me.m_tblValues.Columns
            .Add("smtppoplogin")
            .Add("smtppopserver")
            .Add("smtppopuser")
            .Add("smtppoppassword")
            .Add("smtppopport")
            .Add("smtppopssl")
            .Add("smtpssl")
            .Add("smtpstarttls")
            .Add("smtpauthmode")
        End With

        Dim irow As DataRow = Me.m_tblValues.Rows.Add

        irow("smtppoplogin") = 0
        irow("smtppopserver") = ""
        irow("smtppopuser") = ""
        irow("smtppoppassword") = ""
        irow("smtppopport") = 110
        irow("smtppopssl") = 0
        irow("smtpssl") = 0
        irow("smtpstarttls") = 0
        irow("smtpauthmode") = "LOGIN"

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim sCols As String
        Dim sVals As String

        sCols = "SMTPID,SMTPName,SMTPServer,SMTPUser,SMTPPassword," & _
        "SMTPSenderName,SMTPSenderAddress,SMTPTimeout,IsBackup,SMTPPort,SmtpPopLogin,SmtpPopServer,SmtpPopUser,SmtpPopPassword,SmtpPopPort," & _
        "SmtpPopSsl,SmtpSSL,SmtpStartTLS,SmtpAuthMode"

        irow = Me.m_tblValues.Rows(0)

        sVals = Me.m_serverID & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & SQLPrepare(txtSMTPServer.Text) & "'," & _
        "'" & SQLPrepare(txtSMTPUserID.Text) & "'," & _
        "'" & SQLPrepare(Encrypt(txtSMTPPassword.Text, "KalEidoscOpe")) & "'," & _
        "'" & SQLPrepare(txtSMTPSenderName.Text) & "'," & _
        "'" & SQLPrepare(txtSMTPSenderAddress.Text) & "'," & _
        cmbSMTPTimeout.Value & "," & _
        Convert.ToInt32(Me.chkBackup.Checked) & "," & _
        txtSMTPPort.Value & "," & _
        irow("smtppoplogin") & "," & _
        "'" & SQLPrepare(irow("smtppopserver")) & "'," & _
        "'" & SQLPrepare(irow("smtppopuser")) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(irow("smtppoppassword"))) & "'," & _
        irow("smtppopport") & "," & _
        irow("smtppopssl") & "," & _
        irow("smtpssl") & "," & _
        irow("smtpstarttls") & "," & _
        "'" & irow("smtpauthmode") & "' "

        oData.InsertData("SMTPServers", sCols, sVals)

    End Sub

    Public Sub _EditServer(ByVal nServerID As Integer)
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        Mode = "Edit"

        m_serverID = nServerID

        SQL = "SELECT * FROM SMTPServers WHERE SMTPID =" & nServerID

        oRs = clsmarsdata.GetData(SQL)

        If oRs.EOF = False Then
            cmdOK.Enabled = True

            txtName.Text = oRs("smtpname").Value
            txtSMTPServer.Text = oRs("smtpserver").Value
            txtSMTPUserID.Text = oRs("smtpuser").Value
            txtSMTPPassword.Text = Decrypt(oRs("smtppassword").Value, "KalEidoscOpe")
            txtSMTPSenderName.Text = oRs("smtpsendername").Value
            txtSMTPSenderAddress.Text = oRs("smtpsenderaddress").Value
            cmbSMTPTimeout.Value = oRs("smtptimeout").Value
            chkBackup.Checked = Convert.ToBoolean(oRs("isbackup").Value)
            Me.txtSMTPPort.Value = IsNull(oRs("smtpport").Value, 25)

            Me.m_tblValues = New DataTable

            With Me.m_tblValues.Columns
                .Add("smtppoplogin")
                .Add("smtppopserver")
                .Add("smtppopuser")
                .Add("smtppoppassword")
                .Add("smtppopport")
                .Add("smtppopssl")
                .Add("smtpssl")
                .Add("smtpstarttls")
                .Add("smtpauthmode")
            End With

            Dim irow As DataRow = Me.m_tblValues.Rows.Add

            irow("smtppoplogin") = IsNull(oRs("smtppoplogin").Value, 0)
            irow("smtppopserver") = IsNull(oRs("smtppopserver").Value)
            irow("smtppopuser") = IsNull(oRs("smtppopuser").Value)
            irow("smtppoppassword") = _DecryptDBValue(IsNull(oRs("smtppoppassword").Value))
            irow("smtppopport") = IsNull(oRs("smtppopport").Value, 110)
            irow("smtppopssl") = IsNull(oRs("smtppopssl").Value, 0)
            irow("smtpssl") = IsNull(oRs("smtpssl").Value, 0)
            irow("smtpstarttls") = IsNull(oRs("smtpstarttls").Value, 0)
            irow("smtpauthmode") = IsNull(oRs("smtpauthmode").Value, "LOGIN")

            cmdOK.Enabled = True
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim row As DataRow = Me.m_tblValues.Rows(0)

        SQL = "SMTPName = '" & SQLPrepare(txtName.Text) & "'," & _
        "SMTPServer = '" & SQLPrepare(txtSMTPServer.Text) & "'," & _
        "SMTPUser = '" & SQLPrepare(txtSMTPUserID.Text) & "'," & _
        "SMTPPassword = '" & SQLPrepare(Encrypt(txtSMTPPassword.Text, "KalEidoscOpe")) & "'," & _
        "SMTPSenderName = '" & SQLPrepare(txtSMTPSenderName.Text) & "'," & _
        "SMTPSenderAddress = '" & SQLPrepare(txtSMTPSenderAddress.Text) & "'," & _
        "SMTPTimeout = " & cmbSMTPTimeout.Value & "," & _
        "IsBackup = " & Convert.ToInt32(chkBackup.Checked) & "," & _
        "SMTPPort = " & Me.txtSMTPPort.Value & "," & _
        "SMTPPOPLogin = " & row("smtppoplogin") & "," & _
        "SMTPPOPServer = '" & SQLPrepare(row("smtppopserver")) & "'," & _
        "SMTPPOPUser = '" & SQLPrepare(row("smtppopuser")) & "'," & _
        "SMTPPOPPassword = '" & SQLPrepare(_EncryptDBValue(row("smtppoppassword"))) & "'," & _
        "SMTPPOPPort = " & row("smtppopport") & "," & _
        "SMTPPOPSSL = " & row("smtppopssl") & "," & _
        "SMTPSSL = " & row("smtpssl") & "," & _
        "SMTPStartTLS = " & row("smtpstarttls") & "," & _
        "SMTPAuthMode ='" & row("smtpauthmode") & "' "

        SQL = "UPDATE SMTPServers SET " & SQL & " WHERE SMTPID = " & nServerID

        clsMarsData.WriteData(SQL)


    End Sub
    Private Sub frmSMTP_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        Dim oTest As New clsMarsMessaging
        Dim row As DataRow

        If Me.m_tblValues IsNot Nothing Then
            If Me.m_tblValues.Rows.Count > 0 Then
                row = Me.m_tblValues.Rows(0)
            End If
        End If

        If oTest.TestSMTP(txtSMTPServer.Text, txtSMTPUserID.Text, txtSMTPPassword.Text, cmbSMTPTimeout.Value, _
        txtSMTPSenderName.Text, txtSMTPSenderAddress.Text, Me.txtSMTPPort.Value, row("smtpssl"), row("smtpstarttls"), row("smtpauthmode")) = True Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If Mode = "Add" Then
            If clsMarsData.IsDuplicate("SMTPServers", "SMTPName", txtName.Text, False) = True Then
                ep.SetError(txtName, "An SMTP Server with this name already exists. Please change the name and try again")
                txtName.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, String.Empty)
    End Sub
    Private Sub txtSMTPUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtSMTPUserID.TextChanged, txtSMTPPassword.TextChanged, txtSMTPServer.TextChanged
        cmdOK.Enabled = False
    End Sub

    Private Sub cmdAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdvanced.Click
        Dim oSMTP As frmSMTPAdvanced = New frmSMTPAdvanced

        oSMTP.SMTPAdvanced(True, Me.m_tblValues)
    End Sub
End Class
