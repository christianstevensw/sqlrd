Imports Microsoft.Reporting.WinForms
#If CRYSTAL_VER = 8 Then
    imports My.Crystal85
    imports My.Crystal85.CRAXDRT.CRDiscreteOrRangeKind 
    imports My.Crystal85.CRAXDRT.CRFieldValueType 
    imports My.Crystal85.CRAXDRT.CRRangeInfo 
#ElseIf CRYSTAL_VER = 9 Then
    imports My.Crystal9 
    imports My.Crystal9.CRAXDRT.CRDiscreteOrRangeKind 
    imports My.Crystal9.CRAXDRT.CRFieldValueType 
    imports My.Crystal9.CRAXDRT.CRRangeInfo 
#ElseIf CRYSTAL_VER = 10 Then
    imports My.Crystal10
    imports My.Crystal10.CRAXDRT.CRDiscreteOrRangeKind 
    imports My.Crystal10.CRAXDRT.CRFieldValueType 
    imports My.Crystal10.CRAXDRT.CRRangeInfo 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
Imports My.Crystal11.CRAXDRT.CRDiscreteOrRangeKind
Imports My.Crystal11.CRAXDRT.CRFieldValueType
Imports My.Crystal11.CRAXDRT.CRRangeInfo
#End If


'Imports CRAXDRT.CRDiscreteOrRangeKind
'Imports CRAXDRT.CRFieldValueType
'Imports CRAXDRT.CRRangeInfo
Friend Class frmParameters
    Inherits System.Windows.Forms.Form
    Public sParName() As String
    Public sParValue() As String
    Public sParValue2() As String
    Public sParType() As String
    Public isDynamic As Boolean = False
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean
    Dim nID As Integer
    Dim IsRange As Boolean
    Dim oText As ComboBox
    Dim eventBased As Boolean = False
    Friend WithEvents superTip As DevComponents.DotNetBar.SuperTooltip
    Public eventID As Integer = 99999
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents grpSetValue As System.Windows.Forms.GroupBox
    Friend WithEvents chkCurrent As System.Windows.Forms.RadioButton
    Friend WithEvents chkNull As System.Windows.Forms.RadioButton
    Friend WithEvents chkDefault As System.Windows.Forms.RadioButton
    Friend WithEvents pnValues As System.Windows.Forms.GroupBox
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Dim parDefaults As Hashtable
    Dim m_serverParametersTable As DataTable
    Dim disableEvent As Boolean = False
    Friend WithEvents cmdMultiAll As System.Windows.Forms.Button
    Friend WithEvents cmdRemAll As System.Windows.Forms.Button

    'server details
    Dim m_serverUrl, m_reportPath, m_serverUser, m_serverPassword As String
    Dim m_rv As Microsoft.Reporting.WinForms.ReportViewer
    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lsvParameters As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdDB As System.Windows.Forms.Button
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents cmdInsert As System.Windows.Forms.Button
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents txtValue As System.Windows.Forms.ComboBox
    Friend WithEvents chkLBound As System.Windows.Forms.CheckBox
    Friend WithEvents chkUBound As System.Windows.Forms.CheckBox
    Friend WithEvents chkLBoundInc As System.Windows.Forms.CheckBox
    Friend WithEvents chkUBoundInc As System.Windows.Forms.CheckBox
    Friend WithEvents lsValues As System.Windows.Forms.ListBox
    Friend WithEvents cmdRem As System.Windows.Forms.Button
    Friend WithEvents cmdMulti As System.Windows.Forms.Button
    Friend WithEvents txtValue2 As System.Windows.Forms.ComboBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmParameters))
        Me.lsvParameters = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdRemAll = New System.Windows.Forms.Button
        Me.cmdMultiAll = New System.Windows.Forms.Button
        Me.cmdRem = New System.Windows.Forms.Button
        Me.cmdMulti = New System.Windows.Forms.Button
        Me.txtValue = New System.Windows.Forms.ComboBox
        Me.cmdInsert = New System.Windows.Forms.Button
        Me.grpSetValue = New System.Windows.Forms.GroupBox
        Me.chkCurrent = New System.Windows.Forms.RadioButton
        Me.chkNull = New System.Windows.Forms.RadioButton
        Me.chkDefault = New System.Windows.Forms.RadioButton
        Me.pnValues = New System.Windows.Forms.GroupBox
        Me.lsValues = New System.Windows.Forms.ListBox
        Me.chkLBound = New System.Windows.Forms.CheckBox
        Me.chkLBoundInc = New System.Windows.Forms.CheckBox
        Me.txtValue2 = New System.Windows.Forms.ComboBox
        Me.chkUBound = New System.Windows.Forms.CheckBox
        Me.chkUBoundInc = New System.Windows.Forms.CheckBox
        Me.cmdDB = New System.Windows.Forms.Button
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.superTip = New DevComponents.DotNetBar.SuperTooltip
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        Me.grpSetValue.SuspendLayout()
        Me.pnValues.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'lsvParameters
        '
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvParameters.FullRowSelect = True
        Me.lsvParameters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvParameters.HideSelection = False
        Me.lsvParameters.Location = New System.Drawing.Point(8, 16)
        Me.lsvParameters.Name = "lsvParameters"
        Me.lsvParameters.Size = New System.Drawing.Size(408, 128)
        Me.lsvParameters.TabIndex = 0
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 169
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Value"
        Me.ColumnHeader2.Width = 163
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Multi-Value"
        Me.ColumnHeader3.Width = 67
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuCut
        '
        Me.mnuCut.Index = 0
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 1
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 2
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 3
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 5
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 6
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 7
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(267, 400)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(355, 400)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(24, 176)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Available Values"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.pnValues)
        Me.GroupBox1.Controls.Add(Me.cmdRemAll)
        Me.GroupBox1.Controls.Add(Me.cmdMultiAll)
        Me.GroupBox1.Controls.Add(Me.cmdRem)
        Me.GroupBox1.Controls.Add(Me.cmdMulti)
        Me.GroupBox1.Controls.Add(Me.txtValue)
        Me.GroupBox1.Controls.Add(Me.cmdInsert)
        Me.GroupBox1.Controls.Add(Me.lsvParameters)
        Me.GroupBox1.Controls.Add(Me.grpSetValue)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 386)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'cmdRemAll
        '
        Me.cmdRemAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemAll.Image = CType(resources.GetObject("cmdRemAll.Image"), System.Drawing.Image)
        Me.cmdRemAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemAll.Location = New System.Drawing.Point(16, 209)
        Me.cmdRemAll.Name = "cmdRemAll"
        Me.cmdRemAll.Size = New System.Drawing.Size(28, 23)
        Me.cmdRemAll.TabIndex = 24
        '
        'cmdMultiAll
        '
        Me.cmdMultiAll.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdMultiAll.Image = CType(resources.GetObject("cmdMultiAll.Image"), System.Drawing.Image)
        Me.cmdMultiAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMultiAll.Location = New System.Drawing.Point(319, 209)
        Me.cmdMultiAll.Name = "cmdMultiAll"
        Me.cmdMultiAll.Size = New System.Drawing.Size(28, 23)
        Me.cmdMultiAll.TabIndex = 24
        Me.ToolTip1.SetToolTip(Me.cmdMultiAll, "Select all values")
        '
        'cmdRem
        '
        Me.cmdRem.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRem.Image = CType(resources.GetObject("cmdRem.Image"), System.Drawing.Image)
        Me.cmdRem.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRem.Location = New System.Drawing.Point(46, 209)
        Me.cmdRem.Name = "cmdRem"
        Me.cmdRem.Size = New System.Drawing.Size(28, 23)
        Me.cmdRem.TabIndex = 2
        '
        'cmdMulti
        '
        Me.cmdMulti.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdMulti.Image = CType(resources.GetObject("cmdMulti.Image"), System.Drawing.Image)
        Me.cmdMulti.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMulti.Location = New System.Drawing.Point(289, 209)
        Me.cmdMulti.Name = "cmdMulti"
        Me.cmdMulti.Size = New System.Drawing.Size(28, 23)
        Me.cmdMulti.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.cmdMulti, "Add selected value")
        '
        'txtValue
        '
        Me.txtValue.ContextMenu = Me.mnuInserter
        Me.txtValue.ItemHeight = 13
        Me.txtValue.Location = New System.Drawing.Point(16, 184)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(330, 21)
        Me.txtValue.TabIndex = 0
        Me.txtValue.Tag = "memo"
        '
        'cmdInsert
        '
        Me.cmdInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdInsert.Image = CType(resources.GetObject("cmdInsert.Image"), System.Drawing.Image)
        Me.cmdInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(384, 184)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(28, 23)
        Me.cmdInsert.TabIndex = 25
        Me.cmdInsert.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpSetValue
        '
        Me.grpSetValue.Controls.Add(Me.chkCurrent)
        Me.grpSetValue.Controls.Add(Me.chkNull)
        Me.grpSetValue.Controls.Add(Me.chkDefault)
        Me.grpSetValue.Location = New System.Drawing.Point(16, 211)
        Me.grpSetValue.Name = "grpSetValue"
        Me.grpSetValue.Size = New System.Drawing.Size(384, 94)
        Me.grpSetValue.TabIndex = 33
        Me.grpSetValue.TabStop = False
        Me.grpSetValue.Text = "Set value as: "
        '
        'chkCurrent
        '
        Me.chkCurrent.AutoSize = True
        Me.chkCurrent.Checked = True
        Me.chkCurrent.Location = New System.Drawing.Point(10, 20)
        Me.chkCurrent.Name = "chkCurrent"
        Me.chkCurrent.Size = New System.Drawing.Size(132, 17)
        Me.chkCurrent.TabIndex = 1
        Me.chkCurrent.TabStop = True
        Me.chkCurrent.Text = "Set as specified value "
        Me.chkCurrent.UseVisualStyleBackColor = True
        '
        'chkNull
        '
        Me.chkNull.AutoSize = True
        Me.chkNull.Location = New System.Drawing.Point(10, 43)
        Me.chkNull.Name = "chkNull"
        Me.chkNull.Size = New System.Drawing.Size(75, 17)
        Me.chkNull.TabIndex = 2
        Me.chkNull.Text = "Set as Null"
        Me.chkNull.UseVisualStyleBackColor = True
        '
        'chkDefault
        '
        Me.chkDefault.AutoSize = True
        Me.chkDefault.Location = New System.Drawing.Point(10, 66)
        Me.chkDefault.Name = "chkDefault"
        Me.chkDefault.Size = New System.Drawing.Size(93, 17)
        Me.chkDefault.TabIndex = 3
        Me.chkDefault.Text = "Set as Default"
        Me.chkDefault.UseVisualStyleBackColor = True
        '
        'pnValues
        '
        Me.pnValues.Controls.Add(Me.lsValues)
        Me.pnValues.Location = New System.Drawing.Point(16, 238)
        Me.pnValues.Name = "pnValues"
        Me.pnValues.Size = New System.Drawing.Size(384, 142)
        Me.pnValues.TabIndex = 24
        Me.pnValues.TabStop = False
        Me.pnValues.Text = "Multiple values for parameter"
        '
        'lsValues
        '
        Me.lsValues.Location = New System.Drawing.Point(7, 20)
        Me.lsValues.Name = "lsValues"
        Me.lsValues.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lsValues.Size = New System.Drawing.Size(371, 108)
        Me.lsValues.TabIndex = 4
        '
        'chkLBound
        '
        Me.chkLBound.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkLBound.Location = New System.Drawing.Point(236, 50)
        Me.chkLBound.Name = "chkLBound"
        Me.chkLBound.Size = New System.Drawing.Size(112, 24)
        Me.chkLBound.TabIndex = 27
        Me.chkLBound.Text = "No Lower Bound"
        Me.chkLBound.Visible = False
        '
        'chkLBoundInc
        '
        Me.chkLBoundInc.Checked = True
        Me.chkLBoundInc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkLBoundInc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkLBoundInc.Location = New System.Drawing.Point(124, 50)
        Me.chkLBoundInc.Name = "chkLBoundInc"
        Me.chkLBoundInc.Size = New System.Drawing.Size(96, 24)
        Me.chkLBoundInc.TabIndex = 27
        Me.chkLBoundInc.Text = "Include Value"
        Me.chkLBoundInc.Visible = False
        '
        'txtValue2
        '
        Me.txtValue2.ContextMenu = Me.mnuInserter
        Me.txtValue2.ItemHeight = 13
        Me.txtValue2.Location = New System.Drawing.Point(235, 23)
        Me.txtValue2.Name = "txtValue2"
        Me.txtValue2.Size = New System.Drawing.Size(161, 21)
        Me.txtValue2.TabIndex = 26
        Me.txtValue2.Tag = "memo"
        Me.txtValue2.Visible = False
        '
        'chkUBound
        '
        Me.chkUBound.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUBound.Location = New System.Drawing.Point(6, 50)
        Me.chkUBound.Name = "chkUBound"
        Me.chkUBound.Size = New System.Drawing.Size(112, 24)
        Me.chkUBound.TabIndex = 27
        Me.chkUBound.Text = "No Upper Bound"
        Me.chkUBound.Visible = False
        '
        'chkUBoundInc
        '
        Me.chkUBoundInc.Checked = True
        Me.chkUBoundInc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUBoundInc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUBoundInc.Location = New System.Drawing.Point(6, 20)
        Me.chkUBoundInc.Name = "chkUBoundInc"
        Me.chkUBoundInc.Size = New System.Drawing.Size(96, 24)
        Me.chkUBoundInc.TabIndex = 27
        Me.chkUBoundInc.Text = "Include Value"
        Me.chkUBoundInc.Visible = False
        '
        'cmdDB
        '
        Me.cmdDB.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDB.Image = CType(resources.GetObject("cmdDB.Image"), System.Drawing.Image)
        Me.cmdDB.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDB.Location = New System.Drawing.Point(16, 488)
        Me.cmdDB.Name = "cmdDB"
        Me.cmdDB.Size = New System.Drawing.Size(40, 21)
        Me.cmdDB.TabIndex = 1
        Me.cmdDB.Visible = False
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'superTip
        '
        Me.superTip.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.superTip.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.superTip.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkUBoundInc)
        Me.GroupBox3.Controls.Add(Me.chkUBound)
        Me.GroupBox3.Controls.Add(Me.chkLBound)
        Me.GroupBox3.Controls.Add(Me.txtValue2)
        Me.GroupBox3.Controls.Add(Me.chkLBoundInc)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 535)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(412, 100)
        Me.GroupBox3.TabIndex = 23
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Visible = False
        '
        'frmParameters
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(442, 429)
        Me.ControlBox = False
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdDB)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmParameters"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Parameters"
        Me.GroupBox1.ResumeLayout(False)
        Me.grpSetValue.ResumeLayout(False)
        Me.grpSetValue.PerformLayout()
        Me.pnValues.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmParameters_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Try
            If lsvParameters.SelectedItems.Count = 0 Then lsvParameters.Items(0).Selected = True
        Catch ex As Exception
        End Try

        'If txtValue.Text.StartsWith("[") And txtValue.Text.EndsWith("]") Then
        '    txtValue.Enabled = False
        '    cmdInsert.Enabled = False
        'End If

    End Sub

    Private Sub getParameterValidValues()
        Dim parIndex As Integer
        Dim pValues As IList
        Dim p As ReportParameterInfo

        If m_rv Is Nothing Then
            Return
        ElseIf lsvParameters.SelectedItems(0) Is Nothing Then
            Return
        End If

        'clear the list of items
        txtValue.Items.Clear()

        'get the parameter index
        parIndex = lsvParameters.SelectedItems(0).Index

        If parIndex = 0 Then 'its the first parameter so we can just read the available values
            Dim ps As ReportParameterInfoCollection = m_rv.ServerReport.GetParameters() 'get the parameters collection from the serverreport

            For Each p In ps 'loop through the parameters
                If p.Name = lsvParameters.SelectedItems(0).Text Then
                    pValues = p.ValidValues

                    If p.ValidValues IsNot Nothing Then
                        For Each val As ValidValue In pValues
                            txtValue.Items.Add(val.Label)
                        Next
                    End If

                    Exit For
                End If
            Next
        Else
            'we need to set the previous parameters first before getting the available values for this one
            Dim reportPars() As ReportParameter
            ReDim reportPars(parIndex - 1)

            For I As Integer = 0 To parIndex - 1
                reportPars(I) = New ReportParameter

                reportPars(I).Name = lsvParameters.Items(I).Text

                Dim value As String = ""

                Try
                    value = lsvParameters.Items(I).SubItems(1).Text
                Catch : End Try

                If value.Contains("|") = False Then 'does the value contain multiple values?
                    reportPars(I).Values.Add(value)
                Else
                    For Each s As String In value.Split("|")
                        If s <> "" Then reportPars(I).Values.Add(s)
                    Next
                End If
            Next

            'lets set the parameters that we have values for
            m_rv.ServerReport.SetParameters(reportPars)

            'now we can get the values of our target parameter i.e. at parIndex
            Dim ps As ReportParameterInfoCollection = m_rv.ServerReport.GetParameters
            
            For Each p In ps
                If p.Name = lsvParameters.SelectedItems(0).Text Then
                    pValues = p.ValidValues

                    If pValues IsNot Nothing Then
                        For Each val As ValidValue In pValues
                            txtValue.Items.Add(val.Label)
                        Next
                    End If

                    Exit For
                End If
            Next
        End If


        'we need to update the m_serverParameters datatable with the new found values
        If pValues IsNot Nothing And p IsNot Nothing Then
            'delete the existing values for this parameter
            Dim rows() As DataRow = Me.m_serverParametersTable.Select("Name =  '" & SQLPrepare(lsvParameters.SelectedItems(0).Text) & "'")
            Dim row As DataRow

            If rows IsNot Nothing Then
                For Each row In rows
                    row.Delete()
                Next
            End If

            For Each val As ValidValue In pValues
                row = Me.m_serverParametersTable.Rows.Add
                row("Name") = p.Name
                row("Type") = p.DataType.ToString
                row("MultiValue") = p.MultiValue.ToString
                row("Label") = val.Label
                row("Value") = val.Value
            Next
        End If
    End Sub
    Public Function SetParameters(ByVal dtParameters As DataTable, ByVal defValues As Hashtable, Optional ByVal nReportID As Integer = 99999, _
    Optional ByVal Selected As String = "", Optional ByVal serverParameters As DataTable = Nothing, Optional ByVal serverUrl As String = "", _
    Optional ByVal reportPath As String = "", Optional ByVal serverUser As String = "", _
    Optional ByVal serverPassword As String = "") As DataTable
        Try
            nID = nReportID

            'set the properties
            m_reportPath = reportPath
            m_serverUser = serverUser
            m_serverPassword = serverPassword

            If serverUrl <> "" Then
                For I As Integer = 0 To serverUrl.Split("/").GetUpperBound(0) - 1
                    m_serverUrl &= serverUrl.Split("/")(I) & "/"
                Next

                'set up the report viewer with the report we are accessing
                m_rv = New Microsoft.Reporting.WinForms.ReportViewer
                m_rv.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote
                m_rv.ServerReport.ReportServerUrl = New Uri(m_serverUrl)

                If serverUser <> "" Then
                    Dim userDomain As String = ""

                    If serverUser.Contains("\") Then
                        userDomain = serverUser.Split("\")(0)
                    End If

                    m_rv.ServerReport.ReportServerCredentials.SetFormsCredentials(Nothing, serverUser, serverPassword, userDomain)
                End If

                m_rv.ServerReport.ReportPath = reportPath

                m_serverParametersTable = serverParameters
            Else
                m_serverParametersTable = serverParameters
            End If

            For Each dataRow As DataRow In dtParameters.Rows
                Dim oItem As ListViewItem = New ListViewItem

                oItem.Text = dataRow.Item("ParName")
                oItem.Tag = dataRow.Item("ParType")

                Try
                    oItem.SubItems(1).Text = dataRow.Item("ParValue")
                Catch
                    oItem.SubItems.Add(dataRow.Item("ParValue"))
                End Try

                Try
                    oItem.SubItems(2).Text = dataRow.Item("MultiValue")
                Catch ex As Exception
                    oItem.SubItems.Add(dataRow.Item("MultiValue"))
                End Try


                lsvParameters.Items.Add(oItem)
            Next

            If Selected.Length > 0 Then
                For Each o As ListViewItem In lsvParameters.Items
                    If o.Text.ToLower = Selected.ToLower Then
                        o.Selected = True
                        Exit For
                    Else
                        o.Selected = False
                    End If
                Next
            End If

            parDefaults = defValues

            If lsvParameters.Items.Count > 0 Then
                lsvParameters.Items(0).Selected = True
            End If

            Me.ShowDialog()

            If UserCancel = True Then Return Nothing

            Dim sCols As String
            Dim sVals As String
            Dim SQL As String

            sCols = "ParID,ReportID,ParName,ParValue,ParType,ParNull,MultiValue"

            clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID=" & nReportID)

            _Delay(1)

            dtParameters.Rows.Clear()

            Dim Row As DataRow

            For Each oItem As ListViewItem In Me.lsvParameters.Items

                sVals = clsMarsData.CreateDataID("ReportParameter", "ParID") & "," & _
                nReportID & "," & _
                "'" & SQLPrepare(oItem.Text) & "'," & _
                "'" & SQLPrepare(oItem.SubItems(1).Text) & "'," & _
                "'" & oItem.Tag & "'," & _
                Convert.ToInt32(Me.chkNull.Checked) & "," & _
                "'" & oItem.SubItems(2).Text & "'"

                SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                clsMarsData.WriteData(SQL)

                Row = dtParameters.NewRow

                With Row
                    .Item("ParName") = oItem.Text
                    .Item("ParValue") = oItem.SubItems(1).Text
                    .Item("ParType") = oItem.Tag

                    Try
                        .Item("MultiValue") = oItem.SubItems(2).Text
                    Catch
                        .Item("MultiValue") = "false"
                    End Try
                End With

                dtParameters.Rows.Add(Row)
            Next

            Return dtParameters
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return Nothing
        End Try

    End Function


    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvParameters.SelectedIndexChanged
        Try
            If lsvParameters.SelectedItems.Count = 0 Then Return

            Dim selItem As ListViewItem = lsvParameters.SelectedItems(0)

            'clear the current values
            txtValue.Items.Clear()
            'get new values
            getParameterValidValues()

            Try
                If txtValue.Items.Count = 0 Then 'if we still have no values in the list then lets fall back to old methods
                    If Me.m_serverParametersTable Is Nothing Then
                        txtValue.Items.Clear()

                        Dim defaults As String = parDefaults.Item(selItem.Text)

                        For Each s As String In defaults.Split("|")
                            If s.Length > 0 Then txtValue.Items.Add(s)
                        Next
                    Else
                        txtValue.Items.Clear()

                        Dim rows() As DataRow = Me.m_serverParametersTable.Select("Name = '" & SQLPrepare(selItem.Text) & "'")
                    End If
                End If
            Catch : End Try

            txtValue2.Visible = False
            chkLBound.Visible = False
            chkUBound.Visible = False
            chkLBoundInc.Visible = False
            chkUBoundInc.Visible = False
            Label1.Text = "Available Values"
            IsRange = False
            Dim values As String = selItem.SubItems(1).Text

            If values.Contains("|") = False Then
                txtValue.Text = selItem.SubItems(1).Text
                lsValues.Items.Clear()
            Else
                lsValues.Items.Clear()

                For Each s As String In values.Split("|")
                    If s.Length > 0 Then
                        lsValues.Items.Add(s)
                    End If
                Next
            End If


            If txtValue.Text.StartsWith("~") = False And txtValue.Text.EndsWith("~") = False Then
                txtValue.Enabled = True
                cmdRem.Enabled = True
                cmdMulti.Enabled = True
                lsValues.Enabled = True
            Else
                txtValue.Enabled = False
                cmdRem.Enabled = False
                cmdMulti.Enabled = False
                lsValues.Enabled = False
                'Me.grpSetValue.Enabled = False
            End If

            If txtValue.Text = "[SQL-RDNull]" Then
                Me.chkNull.Checked = True
                Me.txtValue.Enabled = False
            ElseIf Me.txtValue.Text = "[SQL-RDDefault]" Then
                Me.chkDefault.Checked = True
                Me.txtValue.Enabled = False
            Else
                Me.chkCurrent.Checked = True
                Me.txtValue.Enabled = True
            End If

            'cmdMulti.Visible = False
            'Label2.Visible = False
            'pnValues.Visible = False
            'cmdRem.Visible = False

            If selItem.SubItems(2).Text = "true" Then
                pnValues.Visible = True
                cmdMulti.Visible = True
                grpSetValue.Visible = False
                Me.chkCurrent.Checked = True
                cmdRem.Visible = True
                cmdRemAll.Visible = True
                cmdMultiAll.Visible = True
            Else
                pnValues.Visible = False
                cmdMulti.Visible = False
                grpSetValue.Visible = True
                'Me.chkCurrent.Checked = True
                cmdRem.Visible = False
                cmdMulti.Visible = False
                cmdRemAll.Visible = False
                cmdMultiAll.Visible = False
            End If

            'txtValue.Items.Clear()
            txtValue.Focus()

        Catch : End Try

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDB.Click


        If isDynamic = True Then
            Dim oForm As New frmDynamicParameter

            Dim sVal As String = oForm.AddParameterDynamicQuery(lsvParameters.SelectedItems(0).Text, "report", nID)

            If sVal.Length > 0 Then txtValue.Text = sVal
        Else
            Dim oItem As New frmDataItems
            Dim sTemp As String

            sTemp = oItem._GetDataItem(Me.eventID)

            If sTemp.Length > 0 Then
                txtValue.Text = sTemp
            End If
        End If
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If chkLBound.Visible = True Then
            If txtValue2.Text.Length = 0 Then
                MessageBox.Show("Please provide the upper bound value", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtValue2.Focus()
                Return
            ElseIf txtValue.Text.Length = 0 Then
                MessageBox.Show("Please provide the lower bound value", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtValue.Focus()
                Return
            End If
        End If

        Close()
    End Sub


    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        oText.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        oText.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_HideParameters = True
        oInsert.m_EventID = Me.eventID

        oInsert.GetConstants(Me)

        'If TypeOf Me.ActiveControl Is ComboBox Then
        '    Dim cmb As ComboBox = Me.ActiveControl
        '    Dim s As String = oInsert.GetConstants()
        '    cmb.SelectedText = s
        'End If
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        If Me.isDynamic = False Then
            Dim oItem As New frmDataItems

            oText.SelectedText = oItem._GetDataItem(Me.eventID)
        Else
            Dim oIntruder As New frmInserter(Me.eventID)

            oIntruder.GetDatabaseField(gTables, gsCon, Me)
        End If
    End Sub

    Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click
        Dim info As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo

        info.BodyText = "To insert SQL-RD constants or Data Items, right-click in the parameter value field and go to the Insert menu."
        info.Color = DevComponents.DotNetBar.eTooltipColor.Lemon
        info.HeaderText = "Insert"

        With superTip
            .SetSuperTooltip(sender, info)
            .ShowTooltip(sender)
        End With
    End Sub


    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub txtValue_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtValue.KeyPress
        If e.KeyChar = "|" Then e.Handled = True
    End Sub

    Private Sub txtValue_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtValue.KeyUp
        Dim multiValue As Boolean

        Try
            If lsvParameters.SelectedItems(0).SubItems(2).Text = "true" Then
                multiValue = True
            Else
                multiValue = False
            End If
        Catch ex As Exception
            multiValue = False
        End Try

        If e.KeyCode = Keys.Enter Then
            If multiValue = False Then
                Try
                    Dim selectedItem As Integer = lsvParameters.SelectedItems(0).Index

                    If selectedItem = lsvParameters.Items.Count - 1 Then
                        lsvParameters.Items(lsvParameters.Items.Count - 1).Selected = False
                        lsvParameters.Items(0).Selected = True
                    Else
                        lsvParameters.Items(selectedItem).Selected = False
                        lsvParameters.Items(selectedItem + 1).Selected = True
                    End If
                Catch : End Try
            Else
                cmdMulti_Click(Nothing, Nothing)
            End If
        End If
    End Sub



    Private Sub txtValue_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtValue.TextChanged
        Try

            Dim nIndex As Integer = lsvParameters.SelectedItems(0).Index

            Try
                If lsvParameters.SelectedItems(0).SubItems(1).Text.Contains("|") Then
                    Return
                ElseIf Me.disableEvent = True Then
                    Return
                End If
            Catch : End Try

            If txtValue.Text.StartsWith("~") And txtValue.Text.EndsWith("~") Then
                txtValue.Enabled = False
                cmdInsert.Enabled = False
                cmdRem.Enabled = False
                cmdMulti.Enabled = False
                ToolTip1.SetToolTip(txtValue, "This value is for the key parameter. It can only be modified by going to the appropriate screen")

                If txtValue.Text.ToLower = "[sql-rdnull]" Then
                    chkNull.Checked = True
                ElseIf txtValue.Text.ToLower = "[sql-rddefault]" Then
                    chkDefault.Checked = True
                End If

                'Else
                '    chkCurrent.Checked = True
                '    txtValue.Enabled = True
                '    cmdInsert.Enabled = True
                '    ToolTip1.SetToolTip(txtValue, String.Empty)
            End If

            If Me.m_serverParametersTable Is Nothing Then
                lsvParameters.SelectedItems(0).SubItems(1).Text = txtValue.Text
            Else
                lsvParameters.SelectedItems(0).SubItems(1).Text = Me.getserverParameterValue(Me.txtValue.Text)
            End If
        Catch ex As Exception
            Beep()
        End Try
    End Sub


    Private Sub cmdMulti_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMulti.Click
        Try

            
            Dim nIndex As Integer = lsvParameters.SelectedItems(0).Index
            Dim I As Integer
            Dim oItem As ListBox.ObjectCollection

            oItem = lsValues.Items

            For x As Integer = 0 To oItem.Count - 1
                If oItem.Item(x) = Me.getserverParameterValue(txtValue.Text) Then
                    Return
                End If
            Next

            lsValues.Items.Add(Me.getserverParameterValue(txtValue.Text))
            ' txtValue.Items.Remove(txtValue.Text)
            'select the next value
            Dim tempIndex As Integer = txtValue.Items.IndexOf(txtValue.Text)

            Try
                Me.disableEvent = True
                Dim comboItems As ComboBox.ObjectCollection = txtValue.Items
                txtValue.Text = comboItems.Item(tempIndex + 1)
                Me.disableEvent = False
            Catch
                txtValue.Text = ""
            End Try

            'sParValue(nIndex) = ""

            'For I = 0 To oItem.Count - 1
            '    sParValue(nIndex) &= oItem(I) & "|"
            'Next

            Dim parItem As ListViewItem = lsvParameters.SelectedItems(0)

            If parItem.SubItems(1) Is Nothing Then
                Dim values As String = ""

                For Each s As String In lsValues.Items
                    values &= s & "|"
                Next

                parItem.SubItems.Add(values)
            Else
                Dim values As String = ""

                For Each s As String In lsValues.Items
                    values &= s & "|"
                Next

                parItem.SubItems(1).Text = values
            End If

        Catch : End Try
    End Sub

    Private Sub cmdRem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRem.Click
        If lsValues.SelectedItems.Count = 0 Then Return
        Dim selectedItems As ArrayList = New ArrayList

        Dim oItems As ListBox.ObjectCollection

        oItems = lsValues.Items

        Dim selItems As ListBox.SelectedObjectCollection

        selItems = lsValues.SelectedItems

        Dim nIndex As Integer = lsvParameters.SelectedItems(0).Index

        Dim I As Integer


        For I = 0 To selItems.Count - 1
            selectedItems.Add(selItems(I))
        Next

        Try
            For Each s As String In selectedItems
                I = oItems.IndexOf(s)

                If I > -1 Then lsValues.Items.RemoveAt(I)
            Next
        Catch : End Try

        Try
            lsValues.SelectedItem = oItems.Item(I)
        Catch : End Try

        'Do While selItems.Count > 0
        '    lsValues.Items.Remove(selItems.Item(selItems.Count - 1))
        'Loop

        'For I As Integer = 0 To selItems.Count - 1
        '    lsValues.Items.Remove(selItems.Item(I))
        'Next

        'lsValues.Items.Remove(lsValues.SelectedItem)

        oItems = lsValues.Items

        Dim parItem As ListViewItem = lsvParameters.SelectedItems(0)

        If parItem.SubItems(1) Is Nothing Then
            Dim values As String = ""

            For Each s As String In lsValues.Items
                values &= s & "|"
            Next

            parItem.SubItems.Add(values)
        Else
            Dim values As String = ""

            For Each s As String In lsValues.Items
                values &= s & "|"
            Next

            parItem.SubItems(1).Text = values
        End If


    End Sub


    Private Sub lsvParameters_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lsvParameters.MouseUp
        txtValue.Focus()
    End Sub



    Private Sub txtValue_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtValue.GotFocus, txtValue2.GotFocus
        oText = CType(sender, ComboBox)

        Dim info As DevComponents.DotNetBar.SuperTooltipInfo = New DevComponents.DotNetBar.SuperTooltipInfo

        info.BodyImage = My.Resources.warning
        info.BodyText = "Please note that parameter values in SSRS are case-sensitive e.g. MYVALUE <> myvalue." & vbCrLf & _
        "If your report has a list of available values then your selected value must match one of the values in the list."
        info.Color = DevComponents.DotNetBar.eTooltipColor.Lemon

        Me.superTip.SetSuperTooltip(txtValue, info)
        Me.superTip.TooltipDuration = 0
        Me.superTip.ShowTooltip(txtValue)

    End Sub


    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        Clipboard.SetDataObject(oText.SelectedText)

        oText.SelectedText = String.Empty
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        Clipboard.SetDataObject(oText.SelectedText)
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        Dim o As IDataObject

        o = Clipboard.GetDataObject

        oText.SelectedText = o.GetData(DataFormats.Text, True)
    End Sub


    'Private Sub chkNull_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNull.CheckedChanged
    '    txtValue.Enabled = Not (chkNull.Checked)

    '    If chkNull.Checked = True Then
    '        txtValue.Text = "[SQL-RDNull]"
    '    Else
    '        txtValue.Text = ""
    '    End If
    'End Sub

    'Private Sub chkCurrent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCurrent.CheckedChanged
    '    txtValue.Enabled = True

    '    If chkCurrent.Checked = True Then
    '        txtValue.Text = ""
    '    End If
    'End Sub

    Private Sub chkCurrent_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkCurrent.CheckedChanged

        If Me.chkCurrent.Checked Then
            'txtValue.Text = ""
            txtValue.Enabled = True
        End If

    End Sub

    Private Sub chkNull_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkNull.CheckedChanged
        
        If Me.chkNull.Checked Then
            txtValue.Text = "[SQL-RDNull]"
            txtValue.Enabled = False
        End If
        
    End Sub

    Private Sub chkDefault_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDefault.CheckedChanged

        
        If Me.chkDefault.Checked Then
            txtValue.Text = "[SQL-RDDefault]"
            txtValue.Enabled = False
        End If
         
    End Sub


    Private Sub txtValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtValue.SelectedIndexChanged
        Dim multiValue As String

        Try
            multiValue = lsvParameters.SelectedItems(0).SubItems(2).Text
        Catch ex As Exception
            multiValue = "false"
        End Try

        If multiValue = "true" And Me.disableEvent = False Then
            cmdMulti_Click(sender, e)
        End If
    End Sub

    Private Sub cmdMultiAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMultiAll.Click
        Try

            Dim nIndex As Integer = lsvParameters.SelectedItems(0).Index
            Dim I As Integer
            Dim oItem As ListBox.ObjectCollection
            Dim comboItems As ComboBox.ObjectCollection = txtValue.Items

            'clear the list
            Me.lsValues.Items.Clear()

            For n As Integer = 0 To comboItems.Count - 1
                oItem = lsValues.Items

                lsValues.Items.Add(Me.getserverParameterValue(comboItems(n)))

                'txtValue.Items.Remove(Me.getserverParameterValue(comboItems(n)))
            Next

            Dim parItem As ListViewItem = lsvParameters.SelectedItems(0)

            If parItem.SubItems(1) Is Nothing Then
                Dim values As String = ""

                For Each s As String In lsValues.Items
                    values &= s & "|"
                Next

                parItem.SubItems.Add(values)
            Else
                Dim values As String = ""

                For Each s As String In lsValues.Items
                    values &= s & "|"
                Next

                parItem.SubItems(1).Text = values
            End If

        Catch : End Try
    End Sub

    Private Sub cmdRemAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemAll.Click
        
        Dim oItems As ListBox.ObjectCollection = lsValues.Items

        Dim nIndex As Integer = lsvParameters.SelectedItems(0).Index

        lsValues.Items.Clear()

        'For I As Integer = 0 To oItems.Count - 1
        '    Dim value As String = Me.getserverParameterLabel(oItems(I))

        '    For n As Integer = 0 To txtValue.Items.Count - 1
        '        Dim items As ComboBox.ObjectCollection = txtValue.Items
        '        Dim exists As Boolean = False

        '        If items(n) = value Then
        '            exists = True
        '            Exit For
        '        End If

        '        If exists = False Then
        '            txtValue.Items.Add(value)
        '        End If

        '        lsValues.Items.Remove(oItems(I))
        '    Next
        'Next

        'For I As Integer = 0 To selItems.Count - 1
        '    lsValues.Items.Remove(selItems.Item(I))
        'Next

        'lsValues.Items.Remove(lsValues.SelectedItem)
        oItems = lsValues.Items

        Dim parItem As ListViewItem = lsvParameters.SelectedItems(0)

        If parItem.SubItems(1) Is Nothing Then
            Dim values As String = ""

            For Each s As String In lsValues.Items
                values &= s & "|"
            Next

            parItem.SubItems.Add("")
        Else
            Dim values As String = ""

            For Each s As String In lsValues.Items
                values &= s & "|"
            Next

            parItem.SubItems(1).Text = ""
        End If
    End Sub

    Private Function getserverParameterValue(ByVal label As String) As String
        Try
            Dim rows() As DataRow = Me.m_serverParametersTable.Select("Name ='" & SQLPrepare(lsvParameters.SelectedItems(0).Text) & "' AND Label = '" & SQLPrepare(label) & "'")

            If rows.Length > 0 Then
                For Each row As DataRow In rows
                    Return row("value")
                Next
            Else
                Return label
            End If
        Catch
            Return label
        End Try
    End Function

    Private Function getserverParameterLabel(ByVal value As String) As String
        Try
            Dim rows() As DataRow = Me.m_serverParametersTable.Select("Name ='" & SQLPrepare(lsvParameters.SelectedItems(0).Text) & "' AND value = '" & SQLPrepare(value) & "'")

            If rows.Length > 0 Then
                For Each row As DataRow In rows
                    Return row("label")
                Next
            Else
                Return value
            End If
        Catch
            Return value
        End Try
    End Function
End Class
