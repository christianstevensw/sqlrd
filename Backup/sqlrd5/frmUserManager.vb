Imports Microsoft.Win32
Public Class frmUserManager
    Inherits System.Windows.Forms.Form
    Dim sPrev As String = String.Empty
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents imgUsers As System.Windows.Forms.ImageList
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtFirstName As System.Windows.Forms.TextBox
    Friend WithEvents cmbRole As System.Windows.Forms.ComboBox
    Friend WithEvents txtLastName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lblLastMod As System.Windows.Forms.Label
    Friend WithEvents tvUsers As System.Windows.Forms.TreeView
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents cmdAbandon As System.Windows.Forms.Button
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdAddUser As System.Windows.Forms.Button
    Friend WithEvents cmdPermit As System.Windows.Forms.Button
    Friend WithEvents chkAutoLogin As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkNTAuth As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbNTGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbNTRole As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdNTAdd As System.Windows.Forms.Button
    Friend WithEvents cmdNTRemove As System.Windows.Forms.Button
    Friend WithEvents cmdNTClose As System.Windows.Forms.Button
    Friend WithEvents cmdNTAbandon As System.Windows.Forms.Button
    Friend WithEvents cmdNTApply As System.Windows.Forms.Button
    Friend WithEvents cmbNTUser As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cmbNTDomain As System.Windows.Forms.ComboBox
    Friend WithEvents tvNTUsers As System.Windows.Forms.TreeView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lsvGroups As System.Windows.Forms.ListView
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdEdit As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents lblNTModified As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents cmdNTUserSchedules As System.Windows.Forms.Button



    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserManager))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdAddUser = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkAutoLogin = New System.Windows.Forms.CheckBox
        Me.cmdPermit = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lblLastMod = New System.Windows.Forms.Label
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.cmbRole = New System.Windows.Forms.ComboBox
        Me.txtFirstName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtLastName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tvUsers = New System.Windows.Forms.TreeView
        Me.imgUsers = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdClose = New System.Windows.Forms.Button
        Me.cmdAbandon = New System.Windows.Forms.Button
        Me.cmdApply = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.cmdNTClose = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.tvNTUsers = New System.Windows.Forms.TreeView
        Me.cmdNTAdd = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.cmdNTUserSchedules = New System.Windows.Forms.Button
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.lblNTModified = New System.Windows.Forms.Label
        Me.cmbNTGroup = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cmbNTUser = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmbNTRole = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cmbNTDomain = New System.Windows.Forms.ComboBox
        Me.cmdNTRemove = New System.Windows.Forms.Button
        Me.chkNTAuth = New System.Windows.Forms.CheckBox
        Me.cmdNTAbandon = New System.Windows.Forms.Button
        Me.cmdNTApply = New System.Windows.Forms.Button
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.lsvGroups = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdEdit = New System.Windows.Forms.Button
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblStep = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdAddUser)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.tvUsers)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(480, 416)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmdAddUser
        '
        Me.cmdAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddUser.Image = CType(resources.GetObject("cmdAddUser.Image"), System.Drawing.Image)
        Me.cmdAddUser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddUser.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddUser.Location = New System.Drawing.Point(80, 376)
        Me.cmdAddUser.Name = "cmdAddUser"
        Me.cmdAddUser.Size = New System.Drawing.Size(80, 23)
        Me.cmdAddUser.TabIndex = 1
        Me.cmdAddUser.Text = "Add &User"
        Me.cmdAddUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(168, 376)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(80, 23)
        Me.cmdRemove.TabIndex = 2
        Me.cmdRemove.Text = "&Remove"
        Me.cmdRemove.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkAutoLogin)
        Me.GroupBox2.Controls.Add(Me.cmdPermit)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.txtUserID)
        Me.GroupBox2.Controls.Add(Me.cmbRole)
        Me.GroupBox2.Controls.Add(Me.txtFirstName)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtLastName)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.txtPassword)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Location = New System.Drawing.Point(256, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(216, 400)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'chkAutoLogin
        '
        Me.chkAutoLogin.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoLogin.Location = New System.Drawing.Point(8, 224)
        Me.chkAutoLogin.Name = "chkAutoLogin"
        Me.chkAutoLogin.Size = New System.Drawing.Size(200, 40)
        Me.chkAutoLogin.TabIndex = 6
        Me.chkAutoLogin.Text = "Logon automatically with this user"
        '
        'cmdPermit
        '
        Me.cmdPermit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdPermit.Image = CType(resources.GetObject("cmdPermit.Image"), System.Drawing.Image)
        Me.cmdPermit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPermit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPermit.Location = New System.Drawing.Point(32, 368)
        Me.cmdPermit.Name = "cmdPermit"
        Me.cmdPermit.Size = New System.Drawing.Size(136, 23)
        Me.cmdPermit.TabIndex = 5
        Me.cmdPermit.Text = "Assigned Schedules"
        Me.cmdPermit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblLastMod)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 264)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 64)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Last  Modified"
        '
        'lblLastMod
        '
        Me.lblLastMod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblLastMod.Location = New System.Drawing.Point(8, 24)
        Me.lblLastMod.Name = "lblLastMod"
        Me.lblLastMod.Size = New System.Drawing.Size(184, 32)
        Me.lblLastMod.TabIndex = 0
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(8, 112)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(184, 21)
        Me.txtUserID.TabIndex = 2
        '
        'cmbRole
        '
        Me.cmbRole.ItemHeight = 13
        Me.cmbRole.Items.AddRange(New Object() {"Administrator", "User"})
        Me.cmbRole.Location = New System.Drawing.Point(8, 192)
        Me.cmbRole.Name = "cmbRole"
        Me.cmbRole.Size = New System.Drawing.Size(184, 21)
        Me.cmbRole.Sorted = True
        Me.cmbRole.TabIndex = 4
        '
        'txtFirstName
        '
        Me.txtFirstName.Location = New System.Drawing.Point(8, 32)
        Me.txtFirstName.Name = "txtFirstName"
        Me.txtFirstName.Size = New System.Drawing.Size(184, 21)
        Me.txtFirstName.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "First Name"
        '
        'txtLastName
        '
        Me.txtLastName.Location = New System.Drawing.Point(8, 72)
        Me.txtLastName.Name = "txtLastName"
        Me.txtLastName.Size = New System.Drawing.Size(184, 21)
        Me.txtLastName.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Last Name"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User ID"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 136)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(100, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Password"
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(8, 152)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(184, 21)
        Me.txtPassword.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 176)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Security Role"
        '
        'tvUsers
        '
        Me.tvUsers.HideSelection = False
        Me.tvUsers.ImageIndex = 0
        Me.tvUsers.ImageList = Me.imgUsers
        Me.tvUsers.Indent = 19
        Me.tvUsers.ItemHeight = 16
        Me.tvUsers.Location = New System.Drawing.Point(8, 16)
        Me.tvUsers.Name = "tvUsers"
        Me.tvUsers.SelectedImageIndex = 0
        Me.tvUsers.Size = New System.Drawing.Size(240, 352)
        Me.tvUsers.TabIndex = 0
        '
        'imgUsers
        '
        Me.imgUsers.ImageStream = CType(resources.GetObject("imgUsers.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgUsers.TransparentColor = System.Drawing.Color.Transparent
        Me.imgUsers.Images.SetKeyName(0, "")
        Me.imgUsers.Images.SetKeyName(1, "")
        Me.imgUsers.Images.SetKeyName(2, "")
        '
        'cmdClose
        '
        Me.cmdClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClose.Location = New System.Drawing.Point(424, 432)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.Size = New System.Drawing.Size(64, 23)
        Me.cmdClose.TabIndex = 2
        Me.cmdClose.Text = "&Close"
        '
        'cmdAbandon
        '
        Me.cmdAbandon.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdAbandon.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAbandon.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAbandon.Location = New System.Drawing.Point(352, 432)
        Me.cmdAbandon.Name = "cmdAbandon"
        Me.cmdAbandon.Size = New System.Drawing.Size(64, 23)
        Me.cmdAbandon.TabIndex = 1
        Me.cmdAbandon.Text = "A&bandon"
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(280, 432)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(64, 23)
        Me.cmdApply.TabIndex = 0
        Me.cmdApply.Text = "&Apply"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(8, 77)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(504, 488)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.cmdClose)
        Me.TabPage1.Controls.Add(Me.cmdAbandon)
        Me.TabPage1.Controls.Add(Me.cmdApply)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(496, 462)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "SQL-RD Aunthentication"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(496, 462)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Windows Aunthentication"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.GroupBox8)
        Me.GroupBox4.Controls.Add(Me.cmdNTClose)
        Me.GroupBox4.Controls.Add(Me.GroupBox6)
        Me.GroupBox4.Controls.Add(Me.chkNTAuth)
        Me.GroupBox4.Controls.Add(Me.cmdNTAbandon)
        Me.GroupBox4.Controls.Add(Me.cmdNTApply)
        Me.GroupBox4.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(480, 448)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'GroupBox8
        '
        Me.GroupBox8.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(464, 8)
        Me.GroupBox8.TabIndex = 4
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Tag = "3dline"
        '
        'cmdNTClose
        '
        Me.cmdNTClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTClose.Location = New System.Drawing.Point(408, 416)
        Me.cmdNTClose.Name = "cmdNTClose"
        Me.cmdNTClose.Size = New System.Drawing.Size(64, 23)
        Me.cmdNTClose.TabIndex = 3
        Me.cmdNTClose.Text = "&Close"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.tvNTUsers)
        Me.GroupBox6.Controls.Add(Me.cmdNTAdd)
        Me.GroupBox6.Controls.Add(Me.GroupBox5)
        Me.GroupBox6.Controls.Add(Me.cmdNTRemove)
        Me.GroupBox6.Enabled = False
        Me.GroupBox6.Location = New System.Drawing.Point(8, 56)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(464, 352)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Allow access to the following users"
        '
        'tvNTUsers
        '
        Me.tvNTUsers.ImageIndex = 0
        Me.tvNTUsers.ImageList = Me.imgUsers
        Me.tvNTUsers.Location = New System.Drawing.Point(8, 24)
        Me.tvNTUsers.Name = "tvNTUsers"
        Me.tvNTUsers.SelectedImageIndex = 0
        Me.tvNTUsers.Size = New System.Drawing.Size(216, 280)
        Me.tvNTUsers.TabIndex = 5
        '
        'cmdNTAdd
        '
        Me.cmdNTAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTAdd.Image = CType(resources.GetObject("cmdNTAdd.Image"), System.Drawing.Image)
        Me.cmdNTAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdNTAdd.Location = New System.Drawing.Point(72, 312)
        Me.cmdNTAdd.Name = "cmdNTAdd"
        Me.cmdNTAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdNTAdd.TabIndex = 4
        Me.cmdNTAdd.Text = "Add User"
        Me.cmdNTAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.cmdNTUserSchedules)
        Me.GroupBox5.Controls.Add(Me.GroupBox9)
        Me.GroupBox5.Controls.Add(Me.cmbNTGroup)
        Me.GroupBox5.Controls.Add(Me.Label6)
        Me.GroupBox5.Controls.Add(Me.cmbNTUser)
        Me.GroupBox5.Controls.Add(Me.Label7)
        Me.GroupBox5.Controls.Add(Me.Label8)
        Me.GroupBox5.Controls.Add(Me.cmbNTRole)
        Me.GroupBox5.Controls.Add(Me.Label9)
        Me.GroupBox5.Controls.Add(Me.cmbNTDomain)
        Me.GroupBox5.Location = New System.Drawing.Point(232, 16)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(224, 328)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        '
        'cmdNTUserSchedules
        '
        Me.cmdNTUserSchedules.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTUserSchedules.Image = CType(resources.GetObject("cmdNTUserSchedules.Image"), System.Drawing.Image)
        Me.cmdNTUserSchedules.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdNTUserSchedules.Location = New System.Drawing.Point(32, 296)
        Me.cmdNTUserSchedules.Name = "cmdNTUserSchedules"
        Me.cmdNTUserSchedules.Size = New System.Drawing.Size(144, 23)
        Me.cmdNTUserSchedules.TabIndex = 3
        Me.cmdNTUserSchedules.Text = "Assigned Schedules"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.lblNTModified)
        Me.GroupBox9.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(192, 80)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Last  Modified"
        '
        'lblNTModified
        '
        Me.lblNTModified.Location = New System.Drawing.Point(8, 24)
        Me.lblNTModified.Name = "lblNTModified"
        Me.lblNTModified.Size = New System.Drawing.Size(176, 48)
        Me.lblNTModified.TabIndex = 0
        '
        'cmbNTGroup
        '
        Me.cmbNTGroup.Location = New System.Drawing.Point(8, 80)
        Me.cmbNTGroup.Name = "cmbNTGroup"
        Me.cmbNTGroup.Size = New System.Drawing.Size(192, 21)
        Me.cmbNTGroup.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.Location = New System.Drawing.Point(8, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Group"
        '
        'cmbNTUser
        '
        Me.cmbNTUser.Location = New System.Drawing.Point(8, 128)
        Me.cmbNTUser.Name = "cmbNTUser"
        Me.cmbNTUser.Size = New System.Drawing.Size(192, 21)
        Me.cmbNTUser.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.Location = New System.Drawing.Point(8, 112)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "User Name"
        '
        'Label8
        '
        Me.Label8.Location = New System.Drawing.Point(8, 160)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(100, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "Security Role"
        '
        'cmbNTRole
        '
        Me.cmbNTRole.Items.AddRange(New Object() {"Administrator", "User"})
        Me.cmbNTRole.Location = New System.Drawing.Point(8, 176)
        Me.cmbNTRole.Name = "cmbNTRole"
        Me.cmbNTRole.Size = New System.Drawing.Size(192, 21)
        Me.cmbNTRole.Sorted = True
        Me.cmbNTRole.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Domain"
        '
        'cmbNTDomain
        '
        Me.cmbNTDomain.Location = New System.Drawing.Point(8, 32)
        Me.cmbNTDomain.Name = "cmbNTDomain"
        Me.cmbNTDomain.Size = New System.Drawing.Size(192, 21)
        Me.cmbNTDomain.TabIndex = 1
        '
        'cmdNTRemove
        '
        Me.cmdNTRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTRemove.Image = CType(resources.GetObject("cmdNTRemove.Image"), System.Drawing.Image)
        Me.cmdNTRemove.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdNTRemove.Location = New System.Drawing.Point(152, 312)
        Me.cmdNTRemove.Name = "cmdNTRemove"
        Me.cmdNTRemove.Size = New System.Drawing.Size(75, 23)
        Me.cmdNTRemove.TabIndex = 4
        Me.cmdNTRemove.Text = "Remove"
        Me.cmdNTRemove.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkNTAuth
        '
        Me.chkNTAuth.Location = New System.Drawing.Point(8, 16)
        Me.chkNTAuth.Name = "chkNTAuth"
        Me.chkNTAuth.Size = New System.Drawing.Size(440, 24)
        Me.chkNTAuth.TabIndex = 0
        Me.chkNTAuth.Text = "Enable Windows Aunthentication"
        '
        'cmdNTAbandon
        '
        Me.cmdNTAbandon.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTAbandon.Location = New System.Drawing.Point(336, 416)
        Me.cmdNTAbandon.Name = "cmdNTAbandon"
        Me.cmdNTAbandon.Size = New System.Drawing.Size(64, 23)
        Me.cmdNTAbandon.TabIndex = 3
        Me.cmdNTAbandon.Text = "&Abandon"
        '
        'cmdNTApply
        '
        Me.cmdNTApply.Enabled = False
        Me.cmdNTApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNTApply.Location = New System.Drawing.Point(264, 416)
        Me.cmdNTApply.Name = "cmdNTApply"
        Me.cmdNTApply.Size = New System.Drawing.Size(64, 23)
        Me.cmdNTApply.TabIndex = 3
        Me.cmdNTApply.Text = "&Apply"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox7)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(496, 462)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Groups"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.lsvGroups)
        Me.GroupBox7.Controls.Add(Me.cmdDelete)
        Me.GroupBox7.Controls.Add(Me.cmdEdit)
        Me.GroupBox7.Controls.Add(Me.cmdAdd)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(480, 448)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.TabStop = False
        '
        'lsvGroups
        '
        Me.lsvGroups.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2})
        Me.lsvGroups.FullRowSelect = True
        Me.lsvGroups.HideSelection = False
        Me.lsvGroups.LargeImageList = Me.imgUsers
        Me.lsvGroups.Location = New System.Drawing.Point(8, 16)
        Me.lsvGroups.Name = "lsvGroups"
        Me.lsvGroups.Size = New System.Drawing.Size(464, 392)
        Me.lsvGroups.SmallImageList = Me.imgUsers
        Me.lsvGroups.TabIndex = 0
        Me.lsvGroups.UseCompatibleStateImageBehavior = False
        Me.lsvGroups.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Group Name"
        Me.ColumnHeader1.Width = 138
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Description"
        Me.ColumnHeader2.Width = 315
        '
        'cmdDelete
        '
        Me.cmdDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(184, 416)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 15
        Me.cmdDelete.Text = "&Delete"
        '
        'cmdEdit
        '
        Me.cmdEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEdit.Image = CType(resources.GetObject("cmdEdit.Image"), System.Drawing.Image)
        Me.cmdEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEdit.Location = New System.Drawing.Point(96, 416)
        Me.cmdEdit.Name = "cmdEdit"
        Me.cmdEdit.Size = New System.Drawing.Size(75, 23)
        Me.cmdEdit.TabIndex = 16
        Me.cmdEdit.Text = "&Edit"
        '
        'cmdAdd
        '
        Me.cmdAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(8, 416)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdd.TabIndex = 14
        Me.cmdAdd.Text = "&Add"
        '
        'TabPage4
        '
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(456, 374)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Groups"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(522, 67)
        Me.Panel1.TabIndex = 4
        '
        'lblStep
        '
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(15, 17)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(360, 32)
        Me.lblStep.TabIndex = 3
        Me.lblStep.Text = "User Manager"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(452, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 67)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(522, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 5
        '
        'frmUserManager
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(522, 571)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmUserManager"
        Me.Text = "User Manager"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub _LoadGroups()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM GroupAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvGroups.Items.Clear()

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oItem = New ListViewItem

                For Each o As ListViewItem In lsvGroups.Items
                    If o.Text = oRs("groupname").Value Then
                        GoTo skip
                    End If
                Next

                oItem.Text = oRs("groupname").Value
                oItem.SubItems.Add(oRs("groupdesc").Value)
                oItem.Tag = oRs("groupid").Value
                oItem.ImageIndex = 0

                lsvGroups.Items.Add(oItem)

skip:
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If


    End Sub
    Private Sub frmUserManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Dim oUser As New clsMarsUsers

        oUser.ListUsers(tvUsers)

        oUser.ListNTUsers(tvNTUsers)

        Me._LoadGroups()

        cmbNTDomain.Text = Environment.UserDomainName

        Dim oUI As New clsMarsUI

        chkNTAuth.Checked = oUI.ReadRegistry("UnifiedLogin", 0)

    End Sub

    Private Sub tvUsers_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvUsers.AfterSelect

        If tvUsers.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        oNode = tvUsers.SelectedNode

        sPrev = oNode.Text

        Dim type As String = CType(oNode.Tag, String).Split(":")(0).ToLower

        Select Case type
            Case "userid"
                SQL = "SELECT * FROM CRDUsers WHERE UserID ='" & oNode.Text & "'"
                oRs = clsMarsData.GetData(SQL)

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        txtFirstName.Text = oRs("firstname").Value
                        txtLastName.Text = oRs("lastname").Value
                        txtUserID.Text = oRs("userid").Value
                        txtPassword.Text = Decrypt(IsNull(oRs("password").Value), "qwer][po")

                        cmbRole.Text = oRs("userrole").Value 'cmbRole.Items.IndexOf(oRs("userrole").Value)

                        lblLastMod.Text = oRs("lastmodby").Value & " " & _
                        Convert.ToString(oRs("lastmoddate").Value)

                        Try
                            chkAutoLogin.Checked = Convert.ToBoolean(oRs("autologin").Value)
                        Catch
                            chkAutoLogin.Checked = False
                        End Try
                    End If

                    oRs.Close()
                End If

                If oNode.Text.ToLower = gUser.ToLower Then
                    For Each txt As Control In GroupBox2.Controls
                        If TypeOf txt Is TextBox Then
                            Dim oT As TextBox

                            oT = CType(txt, TextBox)

                            oT.ReadOnly = True
                            cmdRemove.Enabled = False
                            cmbRole.Enabled = False
                            If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = False
                        End If
                    Next

                    txtPassword.ReadOnly = False
                Else
                    For Each txt As Control In GroupBox2.Controls
                        If TypeOf txt Is TextBox Then
                            Dim oT As TextBox

                            oT = CType(txt, TextBox)

                            oT.ReadOnly = False
                            cmdRemove.Enabled = True
                            cmbRole.Enabled = True
                            If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = True
                        End If
                    Next
                End If
        End Select

        cmdApply.Enabled = False
    End Sub


    Private Sub cmdAddUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddUser.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oData As New clsMarsData

        SQL = "SELECT COUNT(*) FROM CRDUsers WHERE UserID <> 'SQLRDAdmin'"

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.Fields(0).Value >= 1 Then

                If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa4_SecureUserLogon) = False Then
                    _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                    Return
                End If

            End If
        Catch ex As Exception

        End Try

        For Each txt As Control In GroupBox2.Controls
            If TypeOf txt Is TextBox Then
                Dim oT As TextBox

                oT = CType(txt, TextBox)

                oT.Text = String.Empty
                oT.ReadOnly = False
                cmbRole.Enabled = True
                cmbRole.Text = "Users"
            End If
        Next

        chkAutoLogin.Checked = False
        cmdAddUser.Enabled = False
        Me.cmdRemove.Enabled = False

        If cmbRole.Text <> "Adminisrator" Then cmdPermit.Enabled = False
    End Sub

    Private Sub txtFirstName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFirstName.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtLastName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastName.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserID.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub cmbRole_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRole.SelectedIndexChanged
        cmdApply.Enabled = True
        ep.SetError(sender, String.Empty)

        If cmbRole.Text = "Administrator" Then
            cmdPermit.Enabled = False
        End If

    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Dim oUser As New clsMarsUsers
        Dim oUI As New clsMarsUI

        'validate user entry first
        If txtFirstName.Text.Length = 0 Then
            ep.SetError(txtFirstName, "Please enter the user's first name")
            txtFirstName.Focus()
            Return
        ElseIf txtLastName.Text.Length = 0 Then
            ep.SetError(txtLastName, "Please enter the user's last name")
            txtLastName.Focus()
            Return
        ElseIf txtUserID.Text.Length = 0 Then
            ep.SetError(txtUserID, "Please enter the user id")
            txtUserID.Focus()
            Return
        ElseIf cmbRole.Text.Length = 0 Then
            ep.SetError(cmbRole, "Please assign the user to a role")
            cmbRole.Focus()
            Return
        ElseIf txtUserID.Text.ToLower = "sqlrdadmin" Then
            ep.SetError(txtUserID, "You cannot use the specified user id as it is already in use by SQL-RD")
            txtUserID.Focus()
            Return
        End If

        If oUser.AddUser(txtUserID.Text, txtPassword.Text, txtFirstName.Text, txtLastName.Text, _
            cmbRole.Text, chkAutoLogin.Checked) = True Then
            oUser.ListUsers(tvUsers)

            oUI.FindNode("UserID:" & txtUserID.Text, tvUsers, tvUsers.Nodes(0))
            cmdApply.Enabled = False
        Else
            MessageBox.Show("Failed to add the user. Please check that the user does not already exist in the system", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = True

        cmdAddUser.Enabled = True
        cmdRemove.Enabled = True
    End Sub

    Private Sub cmdAbandon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAbandon.Click
        cmdApply.Enabled = False

        Dim oUI As New clsMarsUI
        Dim sTemp As String

        sTemp = sPrev

        tvUsers.SelectedNode = tvUsers.Nodes(0)

        oUI.FindNode("UserID:" & sTemp, tvUsers, tvUsers.Nodes(0))

        If cmbRole.Text <> "Administrator" Then cmdPermit.Enabled = True

        cmdAddUser.Enabled = True
        cmdRemove.Enabled = True
    End Sub

    Private Sub cmdClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClose.Click
        Dim oRes As DialogResult

        If cmdApply.Enabled = True Then
            oRes = MessageBox.Show("Abandon the current changes?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                Close()
            Else
                Return
            End If
        Else
            Close()
        End If
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim oRes As DialogResult

        If tvUsers.SelectedNode Is Nothing Then Return

        If tvUsers.SelectedNode Is Nothing Or tvUsers.SelectedNode.Tag = "Role" Or _
        tvUsers.SelectedNode.Tag = "Parent" Then Return

        If gUser.ToLower = tvUsers.SelectedNode.Text.ToLower Then Return

        oRes = MessageBox.Show("Delete the user '" & tvUsers.SelectedNode.Text & "'?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            Dim oUser As New clsMarsUsers

            oUser.DeleteUser(tvUsers.SelectedNode.Text)

            _Delay(1)

            oUser.ListUsers(tvUsers)

            For Each o As Control In GroupBox2.Controls
                If TypeOf o Is TextBox Or TypeOf o Is ComboBox Then
                    o.Text = String.Empty
                End If
            Next

            cmdApply.Enabled = False
        End If

    End Sub


    Private Sub cmdPermit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPermit.Click

        If tvUsers.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode = tvUsers.SelectedNode

        If oNode.Parent Is Nothing Then Return

        If oNode.Parent.Text.ToLower = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(oNode.Text)
    End Sub

    Private Sub txtUserID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUserID.KeyPress
        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) Then
            e.Handled = True
        End If
    End Sub

    Private Sub chkAutoLogin_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoLogin.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbNTGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNTGroup.SelectedIndexChanged
        On Error Resume Next

        Dim oGroup, Member As Object
        Dim sMyParent As String

        If cmbNTGroup.Text.Length = 0 Or cmbNTDomain.Text.Length = 0 Then Return

        oGroup = GetObject("WinNT://" & cmbNTDomain.Text & "/" & cmbNTGroup.Text)

        cmbNTUser.Items.Clear()

        For Each Member In oGroup.Members

            On Error Resume Next

            sMyParent = Member.Parent
            sMyParent = Microsoft.VisualBasic.Right(sMyParent, Len(sMyParent) - InStrRev(sMyParent, "/"))

            If Member.Class = "User" Then
                cmbNTUser.Items.Add(Member.Name)
            End If
        Next

        ep.SetError(sender, "")
    End Sub

    Private Sub cmbNTGroup_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbNTGroup.DropDown

        On Error Resume Next

        If cmbNTGroup.Items.Count > 0 Or cmbNTDomain.Text.Length = 0 Then Return

        AppStatus(True)

        Dim oDomain, Member As Object

        oDomain = GetObject("WinNT://" & cmbNTDomain.Text)

        For Each Member In oDomain
            If Member.Class = "Group" Then
                cmbNTGroup.Items.Add(Member.Name)
            End If
        Next

        AppStatus(False)
    End Sub

    Private Sub chkNTAuth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNTAuth.CheckedChanged
        If chkNTAuth.Checked = True And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa4_SecureUserLogon) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkNTAuth.Checked = False
            Return
        End If

        GroupBox6.Enabled = chkNTAuth.Checked
        cmdNTApply.Enabled = chkNTAuth.Checked
        cmdNTAbandon.Enabled = chkNTAuth.Checked

        GroupBox1.Enabled = Not chkNTAuth.Checked
        cmdApply.Enabled = Not chkNTAuth.Checked
        cmdAbandon.Enabled = Not chkNTAuth.Checked

        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("UnifiedLogin", Convert.ToInt32(chkNTAuth.Checked))

        Dim oRs As ADODB.Recordset
        Dim nCount As Integer

        oRs = clsMarsData.GetData("SELECT COUNT(*) FROM DomainAttr")

        If Not oRs Is Nothing Then
            nCount = oRs(0).Value

            oRs.Close()
        End If

        If chkNTAuth.Checked = True And nCount = 0 Then
            Dim SQL As String
            Dim sCols As String
            Dim sVals As String
            Dim nID As Int64
            Dim sUser As String

            sCols = "DomainID,DomainName,UserRole,UserNumber"

            nID = clsMarsData.CreateDataID("domainattr", "domainid")

            sUser = Environment.UserDomainName & "\" & Environment.UserName

            sVals = nID & "," & _
            "'" & SQLPrepare(sUser) & "','Administrator'," & clsMarsData.GetNewUserNumber("DomainAttr")

            SQL = "INSERT INTO DomainAttr (" & sCols & ") VALUES (" & sVals & ")"

            If clsMarsData.WriteData(SQL) = True Then
                Dim oUsers As New clsMarsUsers

                oUsers.ListNTUsers(tvNTUsers)
            End If
        End If

        'cmdNTApply.Enabled = True
    End Sub

    Private Sub cmdNTAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTAdd.Click
        cmbNTUser.Text = ""
        cmbNTGroup.Text = ""
        cmbNTRole.Text = ""
        cmdNTApply.Enabled = True
    End Sub

    Private Sub cmdNTApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTApply.Click
        Dim sUser As String = cmbNTDomain.Text & "\" & cmbNTUser.Text
        Dim oEdit As Boolean

        If cmbNTDomain.Text.Length = 0 Then
            ep.SetError(cmbNTDomain, "Please provide the user domain")
            cmbNTDomain.Focus()
            Return
        ElseIf cmbNTUser.Text.Length = 0 Then
            ep.SetError(cmbNTUser, "Please provide the Windows User name")
            cmbNTDomain.Focus()
            Return
        ElseIf cmbNTRole.Text.Length = 0 Then
            ep.SetError(cmbNTRole, "Please select the security role")
            cmbNTRole.Focus()
            Return
        ElseIf clsMarsData.IsDuplicate("DomainAttr", "DomainName", sUser, False) = True Then
            oEdit = True
        End If

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        If oEdit = False Then
            sCols = "DomainID,DomainName,UserRole,LastModDate,LastModBy,UserNumber"

            sVals = clsMarsData.CreateDataID("domainattr", "domainid") & "," & _
            "'" & sUser & "'," & _
            "'" & SQLPrepare(cmbNTRole.Text) & "'," & _
            "'" & ConDateTime(Date.Now) & "'," & _
            "'" & gUser & "'," & _
            clsMarsData.GetNewUserNumber("DomainAttr")

            SQL = "INSERT INTO DomainAttr (" & sCols & ") VALUES (" & sVals & ")"
        Else
            SQL = "UPDATE DomainAttr SET " & _
            "UserRole ='" & SQLPrepare(cmbNTRole.Text) & "'," & _
            "LastModDate='" & ConDateTime(Date.Now) & "'," & _
            "LastModBy ='" & gUser & "' " & _
            "WHERE DomainName ='" & SQLPrepare(sUser) & "'"
        End If

        If clsMarsData.WriteData(SQL) = True Then
            Dim oUser As New clsMarsUsers

            oUser.ListNTUsers(tvNTUsers)

            cmdNTApply.Enabled = False
        End If
    End Sub

    Private Sub cmbNTDomain_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNTDomain.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmbNTUser_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNTUser.SelectedIndexChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmbNTRole_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbNTRole.SelectedIndexChanged
        ep.SetError(sender, "")
        cmdNTApply.Enabled = True
    End Sub

    Private Sub cmdNTRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTRemove.Click
        Dim oRes As DialogResult

        If tvNTUsers.SelectedNode Is Nothing Then Return

        If tvNTUsers.SelectedNode.Tag = "Role" Or _
        tvNTUsers.SelectedNode.Tag = "Parent" Then Return

        oRes = MessageBox.Show("Remove the user '" & tvNTUsers.SelectedNode.Text & "'?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            Dim oUser As New clsMarsUsers

            oUser.DeleteNTUser(tvNTUsers.SelectedNode.Text)

            _Delay(1)

            oUser.ListNTUsers(tvNTUsers)

            For Each o As Control In GroupBox5.Controls
                If TypeOf o Is TextBox Or TypeOf o Is ComboBox Then
                    o.Text = String.Empty
                End If
            Next

            cmdNTApply.Enabled = False
        End If
    End Sub

    Private Sub cmdNTClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTClose.Click
        Dim oRes As DialogResult

        If cmdNTApply.Enabled = True Then
            oRes = MessageBox.Show("Abandon the current changes?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If oRes = DialogResult.Yes Then
                Close()
            Else
                Return
            End If
        Else
            Close()
        End If
    End Sub

    Private Sub cmbNTDomain_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbNTDomain.DropDown
        If cmbNTDomain.Items.Count = 0 Then
            cmbNTDomain.Items.Add(Environment.UserDomainName)
        End If
    End Sub

    Private Sub tvNTUsers_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvNTUsers.AfterSelect
        Dim oNode As TreeNode

        If tvNTUsers.SelectedNode Is Nothing Then Return

        oNode = tvNTUsers.SelectedNode

        If oNode.Text.ToLower = (Environment.UserDomainName & "\" & Environment.UserName).ToLower Then
            cmdNTRemove.Enabled = False
        Else
            cmdNTRemove.Enabled = True
        End If

        Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM DomainAttr WHERE DomainName ='" & SQLPrepare(tvNTUsers.SelectedNode.Text) & "'")

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                cmbNTDomain.Text = Convert.ToString(oRs("domainname").Value).Split("\")(0)
                cmbNTUser.Text = Convert.ToString(oRs("domainname").Value).Split("\")(1)
                cmbNTRole.Text = oRs("userrole").Value
                lblNTModified.Text = oRs("lastmodby").Value & " " & IsNull(oRs("lastmoddate").Value, "")
            End If

            oRs.Close()
        End If

        cmdNTApply.Enabled = False
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa4_SecureUserLogon) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        Dim oGroup As frmGroupAttr = New frmGroupAttr

        oGroup.AddGroup()

        Me._LoadGroups()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        If MessageBox.Show("Delete the selected group(s)? This cannot be undone.", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each oItem As ListViewItem In lsvGroups.SelectedItems
                oRs = clsMarsData.GetData("SELECT * FROM CRDUsers WHERE UserRole ='" & SQLPrepare(oItem.Text) & "'")

                If Not oRs Is Nothing Then
                    If oRs.EOF = False Then
                        MessageBox.Show("Cannot delete the '" & oItem.Text & "' group as it has members. Remove all members from the group and try again", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Else
                        clsMarsData.WriteData("DELETE FROM GroupAttr WHERE GroupID =" & oItem.Tag)
                        clsMarsData.WriteData("DELETE FROM GroupPermissions WHERE GroupID =" & oItem.Tag)

                        oItem.Remove()
                    End If
                    oRs.Close()
                End If
            Next
        End If

    End Sub

    Private Sub cmdEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEdit.Click
        Dim oGroup As New frmGroupAttr

        If lsvGroups.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvGroups.SelectedItems(0)

        oGroup.EditGroup(oItem.Tag)

        Me._LoadGroups()

        Dim oUser As New clsMarsUsers

        oUser.ListUsers(tvUsers)

    End Sub

    Private Sub cmbRole_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbRole.DropDown, cmbNTRole.DropDown
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oCombo As ComboBox

        oCombo = CType(sender, ComboBox)

        oCombo.Items.Clear()

        SQL = "SELECT GroupName FROM GroupAttr"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                oCombo.Items.Add(oRs(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Dim s As String() = New String() {"Administrator", "User"}

        oCombo.Items.AddRange(s)
    End Sub

    Private Sub cmdNTAbandon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTAbandon.Click
        cmdNTApply.Enabled = False
    End Sub

    Private Sub cmdNTUserSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNTUserSchedules.Click
        If tvNTUsers.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode = tvNTUsers.SelectedNode

        If oNode.Parent Is Nothing Then Return

        If oNode.Parent.Text.ToLower = "administrators" Then Return

        Dim oForm As New frmUserSchedules

        oForm.AssignSchedules(oNode.Text)
    End Sub

    Private Sub lsvGroups_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvGroups.DoubleClick
        cmdEdit_Click(sender, e)
    End Sub
End Class
