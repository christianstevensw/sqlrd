Imports DevComponents.DotNetBar
Public Class frmAddressBook
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Friend WithEvents tbNav As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGroups As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbContacts As DevComponents.DotNetBar.TabItem
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Dim oList As ListView
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents lsvContacts As System.Windows.Forms.ListView
    Friend WithEvents lsvDetails As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents sfg As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ofg As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lsvGroups As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents dtMan As DevComponents.DotNetBar.DotNetBarManager
    Friend WithEvents barLeftDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barRightDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barTopDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents barBottomDockSite As DevComponents.DotNetBar.DockSite
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddressBook))
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.lsvContacts = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.lsvGroups = New System.Windows.Forms.ListView
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.lsvDetails = New System.Windows.Forms.ListView
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.sfg = New System.Windows.Forms.SaveFileDialog
        Me.ofg = New System.Windows.Forms.OpenFileDialog
        Me.dtMan = New DevComponents.DotNetBar.DotNetBarManager(Me.components)
        Me.barBottomDockSite = New DevComponents.DotNetBar.DockSite
        Me.barLeftDockSite = New DevComponents.DotNetBar.DockSite
        Me.barRightDockSite = New DevComponents.DotNetBar.DockSite
        Me.barTopDockSite = New DevComponents.DotNetBar.DockSite
        Me.tbNav = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbContacts = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbGroups = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbNav.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabItem1
        '
        Me.TabItem1.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'lsvContacts
        '
        Me.lsvContacts.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lsvContacts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvContacts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvContacts.FullRowSelect = True
        Me.lsvContacts.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvContacts.HideSelection = False
        Me.lsvContacts.LargeImageList = Me.ImageList1
        Me.lsvContacts.Location = New System.Drawing.Point(1, 1)
        Me.lsvContacts.Name = "lsvContacts"
        Me.lsvContacts.Size = New System.Drawing.Size(185, 492)
        Me.lsvContacts.SmallImageList = Me.ImageList1
        Me.lsvContacts.TabIndex = 1
        Me.lsvContacts.UseCompatibleStateImageBehavior = False
        Me.lsvContacts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Contact"
        Me.ColumnHeader1.Width = 154
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "id_card2.png")
        Me.ImageList1.Images.SetKeyName(1, "businessman2.png")
        Me.ImageList1.Images.SetKeyName(2, "users_family.png")
        Me.ImageList1.Images.SetKeyName(3, "address_book3.png")
        '
        'lsvGroups
        '
        Me.lsvGroups.BackColor = System.Drawing.Color.WhiteSmoke
        Me.lsvGroups.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5})
        Me.lsvGroups.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvGroups.FullRowSelect = True
        Me.lsvGroups.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvGroups.HideSelection = False
        Me.lsvGroups.LargeImageList = Me.ImageList1
        Me.lsvGroups.Location = New System.Drawing.Point(1, 1)
        Me.lsvGroups.Name = "lsvGroups"
        Me.lsvGroups.Size = New System.Drawing.Size(185, 492)
        Me.lsvGroups.SmallImageList = Me.ImageList1
        Me.lsvGroups.TabIndex = 0
        Me.lsvGroups.UseCompatibleStateImageBehavior = False
        Me.lsvGroups.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "ColumnHeader"
        Me.ColumnHeader5.Width = 153
        '
        'lsvDetails
        '
        Me.lsvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader6, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDetails.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvDetails.Location = New System.Drawing.Point(221, 25)
        Me.lsvDetails.Name = "lsvDetails"
        Me.lsvDetails.Size = New System.Drawing.Size(653, 494)
        Me.lsvDetails.SmallImageList = Me.ImageList1
        Me.lsvDetails.TabIndex = 6
        Me.lsvDetails.UseCompatibleStateImageBehavior = False
        Me.lsvDetails.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Contact Name"
        Me.ColumnHeader6.Width = 156
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Email Address"
        Me.ColumnHeader2.Width = 236
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Phone Number"
        Me.ColumnHeader3.Width = 114
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Fax Number"
        Me.ColumnHeader4.Width = 121
        '
        'sfg
        '
        Me.sfg.Filter = "Text File|*.txt|XML File|*.xml"
        '
        'dtMan
        '
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.F1)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlC)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlA)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlV)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlX)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.CtrlZ)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Del)
        Me.dtMan.AutoDispatchShortcuts.Add(DevComponents.DotNetBar.eShortcut.Ins)
        Me.dtMan.BottomDockSite = Me.barBottomDockSite
        Me.dtMan.DefinitionName = "frmAddressBook.dtMan.xml"
        Me.dtMan.LeftDockSite = Me.barLeftDockSite
        Me.dtMan.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.dtMan.ParentForm = Me
        Me.dtMan.RightDockSite = Me.barRightDockSite
        Me.dtMan.Style = DevComponents.DotNetBar.eDotNetBarStyle.OfficeXP
        Me.dtMan.ThemeAware = False
        Me.dtMan.TopDockSite = Me.barTopDockSite
        '
        'barBottomDockSite
        '
        Me.barBottomDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barBottomDockSite.BackgroundImageAlpha = CType(255, Byte)
        Me.barBottomDockSite.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barBottomDockSite.DocumentDockContainer = Nothing
        Me.barBottomDockSite.Location = New System.Drawing.Point(0, 519)
        Me.barBottomDockSite.Name = "barBottomDockSite"
        Me.barBottomDockSite.NeedsLayout = False
        Me.barBottomDockSite.Size = New System.Drawing.Size(874, 18)
        Me.barBottomDockSite.TabIndex = 10
        Me.barBottomDockSite.TabStop = False
        '
        'barLeftDockSite
        '
        Me.barLeftDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barLeftDockSite.BackgroundImageAlpha = CType(255, Byte)
        Me.barLeftDockSite.Dock = System.Windows.Forms.DockStyle.Left
        Me.barLeftDockSite.DocumentDockContainer = Nothing
        Me.barLeftDockSite.Location = New System.Drawing.Point(0, 25)
        Me.barLeftDockSite.Name = "barLeftDockSite"
        Me.barLeftDockSite.NeedsLayout = False
        Me.barLeftDockSite.Size = New System.Drawing.Size(0, 494)
        Me.barLeftDockSite.TabIndex = 7
        Me.barLeftDockSite.TabStop = False
        '
        'barRightDockSite
        '
        Me.barRightDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barRightDockSite.BackgroundImageAlpha = CType(255, Byte)
        Me.barRightDockSite.Dock = System.Windows.Forms.DockStyle.Right
        Me.barRightDockSite.DocumentDockContainer = Nothing
        Me.barRightDockSite.Location = New System.Drawing.Point(874, 25)
        Me.barRightDockSite.Name = "barRightDockSite"
        Me.barRightDockSite.NeedsLayout = False
        Me.barRightDockSite.Size = New System.Drawing.Size(0, 494)
        Me.barRightDockSite.TabIndex = 8
        Me.barRightDockSite.TabStop = False
        '
        'barTopDockSite
        '
        Me.barTopDockSite.AccessibleRole = System.Windows.Forms.AccessibleRole.Window
        Me.barTopDockSite.BackgroundImageAlpha = CType(255, Byte)
        Me.barTopDockSite.Dock = System.Windows.Forms.DockStyle.Top
        Me.barTopDockSite.DocumentDockContainer = Nothing
        Me.barTopDockSite.Location = New System.Drawing.Point(0, 0)
        Me.barTopDockSite.Name = "barTopDockSite"
        Me.barTopDockSite.NeedsLayout = False
        Me.barTopDockSite.Size = New System.Drawing.Size(874, 25)
        Me.barTopDockSite.TabIndex = 9
        Me.barTopDockSite.TabStop = False
        '
        'tbNav
        '
        Me.tbNav.CanReorderTabs = True
        Me.tbNav.Controls.Add(Me.TabControlPanel2)
        Me.tbNav.Controls.Add(Me.TabControlPanel1)
        Me.tbNav.Dock = System.Windows.Forms.DockStyle.Left
        Me.tbNav.Location = New System.Drawing.Point(0, 25)
        Me.tbNav.Name = "tbNav"
        Me.tbNav.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tbNav.SelectedTabIndex = 0
        Me.tbNav.Size = New System.Drawing.Size(216, 494)
        Me.tbNav.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Dock
        Me.tbNav.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tbNav.TabIndex = 12
        Me.tbNav.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tbNav.Tabs.Add(Me.tbContacts)
        Me.tbNav.Tabs.Add(Me.tbGroups)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.lsvContacts)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(29, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(187, 494)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbContacts
        '
        'tbContacts
        '
        Me.tbContacts.AttachedControl = Me.TabControlPanel1
        Me.tbContacts.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.tbContacts.Image = CType(resources.GetObject("tbContacts.Image"), System.Drawing.Image)
        Me.tbContacts.Name = "tbContacts"
        Me.tbContacts.Text = "Contacts"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.lsvGroups)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(29, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(187, 494)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbGroups
        '
        'tbGroups
        '
        Me.tbGroups.AttachedControl = Me.TabControlPanel2
        Me.tbGroups.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.tbGroups.Image = CType(resources.GetObject("tbGroups.Image"), System.Drawing.Image)
        Me.tbGroups.Name = "tbGroups"
        Me.tbGroups.Text = "Groups"
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(216, Byte), Integer), CType(CType(230, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(193, Byte), Integer), CType(CType(213, Byte), Integer))
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tbNav
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(142, Byte), Integer), CType(CType(75, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(207, Byte), Integer), CType(CType(139, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground2
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.ControlText
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemText
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(150, Byte), Integer))
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(223, Byte), Integer), CType(CType(237, Byte), Integer), CType(CType(254, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.BarBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(216, 25)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(5, 494)
        Me.ExpandableSplitter1.TabIndex = 13
        Me.ExpandableSplitter1.TabStop = False
        '
        'frmAddressBook
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(874, 537)
        Me.Controls.Add(Me.lsvDetails)
        Me.Controls.Add(Me.ExpandableSplitter1)
        Me.Controls.Add(Me.tbNav)
        Me.Controls.Add(Me.barLeftDockSite)
        Me.Controls.Add(Me.barRightDockSite)
        Me.Controls.Add(Me.barTopDockSite)
        Me.Controls.Add(Me.barBottomDockSite)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmAddressBook"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Address Book"
        CType(Me.tbNav, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbNav.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub OpenContact(ByVal nID As Integer, ByVal sType As String)
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sEmail As String = ""
        Dim sFax As String
        Dim sPhone As String
        Dim sName As String
        Dim oRs1 As ADODB.Recordset

        '_Delay(1)

        SQL = "SELECT * FROM ContactDetail c INNER JOIN ContactAttr x ON " & _
                "c.ContactID = x.ContactID WHERE c.ContactID =" & nID

        oRs = clsMarsData.GetData(SQL)

        lsvDetails.Items.Clear()

        If oRs Is Nothing Then Return

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            sName = oRs("contactname").Value
            sEmail = oRs("emailaddress").Value
            sPhone = IsNull(oRs("phonenumber").Value)
            sFax = IsNull(oRs("faxnumber").Value)

            If sEmail.IndexOf("@") < 0 Then
                sName = sEmail
                oRs1 = clsMarsData.GetData("SELECT * FROM " & _
                "ContactDetail c INNER JOIN ContactAttr x ON " & _
                "c.ContactID = x.ContactID WHERE " & _
                "x.ContactName ='" & SQLPrepare(sEmail) & "'")

                Try

                    If oRs1.EOF = False Then
                        sEmail = oRs1("emailaddress").Value
                        sPhone = oRs1("phonenumber").Value
                        sFax = oRs1("faxnumber").Value
                    End If

                    oRs1.Close()
                Catch ex As Exception
                End Try
            End If

            With oItem
                .ImageIndex = 0
                .Text = sName
                .Tag = oRs("detailid").Value

                With .SubItems
                    .Add(sEmail)
                    .Add(sPhone)
                    .Add(sFax)
                End With
            End With

            lsvDetails.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub
    Private Sub DrawContacts()
        Dim SQL As String
        Dim oRs As ADODB.Recordset


        SQL = "SELECT * FROM ContactAttr WHERE ContactType ='contact' " & _
        "ORDER BY ContactName ASC"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        lsvContacts.Items.Clear()

        Application.DoEvents()

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            With oItem
                .Text = oRs("contactname").Value
                .Tag = oRs("contacttype").Value & ":" & oRs("contactid").Value

                If oRs("contacttype").Value = "contact" Then
                    .ImageIndex = 1
                Else
                    .ImageIndex = 2
                End If
            End With

            lsvContacts.Items.Add(oItem)
            oRs.MoveNext()
        Loop

        oRs.Close()

        SQL = "SELECT * FROM ContactAttr WHERE ContactType ='group' " & _
        "ORDER BY ContactName ASC"

        oRs = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return

        lsvGroups.Items.Clear()

        Application.DoEvents()

        Do While oRs.EOF = False
            Dim oItem As ListViewItem = New ListViewItem

            With oItem
                .Text = oRs("contactname").Value
                .Tag = oRs("contacttype").Value & ":" & oRs("contactid").Value
                If oRs("contacttype").Value = "contact" Then
                    .ImageIndex = 1
                Else
                    .ImageIndex = 2
                End If
            End With

            lsvGroups.Items.Add(oItem)

            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Public Sub DeleteContact(ByVal nID As Integer, ByVal sType As String)
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim sMsg As String
        Dim Exist As Boolean = False
        Dim sName As String

        If sType.ToLower = "contact" Then
            sName = lsvContacts.SelectedItems(0).Text
        Else
            sName = lsvGroups.SelectedItems(0).Text
        End If

        SQL = "SELECT r.ReportTitle, d.ReportID FROM DestinationAttr d " & _
        "INNER JOIN ReportAttr r ON d.ReportID = r.ReportID WHERE d.SendTo LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
        "d.Cc LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
        "d.Bcc LIKE '%" & SQLPrepare(sName) & ">%'"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Exist = True
                sMsg = "The following schedules currently reference this contact/group:" & vbCrLf & _
                "----------------------------------------------------------------------" & vbCrLf
            End If

            Do While oRs.EOF = False
                sMsg &= oRs(0).Value & vbCrLf
                oRs.MoveNext()
            Loop
            oRs.Close()
        End If

        SQL = "SELECT p.PackageName, d.PackID FROM DestinationAttr d " & _
                "INNER JOIN PackageAttr p ON d.PackID = p.PackID WHERE d.SendTo LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
                "d.Cc LIKE '%" & SQLPrepare(sName) & ">%' OR " & _
                "d.Bcc LIKE '%" & SQLPrepare(sName) & ">%'"


        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                Exist = True
                sMsg &= vbCrLf & "The following packages currently reference this contact/group:" & vbCrLf & _
                "----------------------------------------------------------------------" & vbCrLf
            End If

            Do While oRs.EOF = False
                sMsg &= oRs(0).Value & vbCrLf
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        If Exist = True Then
            MessageBox.Show(sMsg & vbCrLf & vbCrLf & "Please remove the references from the above schedules/packages and then try again.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        clsMarsData.WriteData("DELETE FROM ContactDetail WHERE ContactID = " & nID)
        clsMarsData.WriteData("DELETE FROM ContactAttr WHERE ContactID = " & nID)
        clsMarsData.WriteData("DELETE FROM ContactDetail WHERE EmailAddress  LIKE '" & SQLPrepare(sName) & "'")

        oList.SelectedItems(0).Remove()
        lsvDetails.Items.Clear()
    End Sub


    Private Sub frmAddressBook_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        FormatForWinXP(Me)
        DrawContacts()

        oList = lsvContacts

        clsMarsUI.MainUI.SetAppTheme()
    End Sub

    Private Sub dtMan_ItemClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtMan.ItemClick
        Try
            Dim oItem As BaseItem
            Dim nID As Integer
            Dim sType As String
            Dim oRes As DialogResult

            oItem = CType(sender, BaseItem)

            Select Case oItem.Text.ToLower
                Case "new contact"
                    Dim oContact As New frmAddContact
                    oContact.AddContact()

                Case "new group"
                    Dim oGroup As New frmAddContact
                    oGroup.AddGroup()
                Case "delete"
                    If oList.SelectedItems.Count = 0 Then Return

                    sType = oList.SelectedItems(0).Tag.split(":")(0)

                    If oList.SelectedItems.Count = 1 Then

                        nID = oList.SelectedItems(0).Tag.split(":")(1)

                        oRes = MessageBox.Show("Delete the " & sType & " '" & _
                        oList.SelectedItems(0).Text & "'?", _
                        Application.ProductName, MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then
                            DeleteContact(nID, sType)
                        End If
                    Else
                        oRes = MessageBox.Show("Delete the selected" & sType & "s " & _
                        "from the address book?", Application.ProductName, _
                        MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then

                            Dim oUI As New clsMarsUI

                            oUI.BusyProgress(50, "Deleting...")
                            For Each lsv As ListViewItem In oList.SelectedItems
                                nID = lsv.Tag.split(":")(1)

                                DeleteContact(nID, sType)
                            Next

                            oUI.BusyProgress(80, "Ceaning up...")

                            oUI.BusyProgress(, , True)
                        End If
                    End If
                Case "edit"

                    If oList.SelectedItems.Count = 0 Then Return

                    nID = oList.SelectedItems(0).Tag.split(":")(1)
                    sType = oList.SelectedItems(0).Tag.split(":")(0)

                    Dim nSel As Integer = oList.SelectedItems(0).Index

                    Dim oForm As New frmAddContact

                    oForm.isEdit = True

                    Select Case sType
                        Case "contact"
                            oForm.EditContact(nID)
                        Case "group"
                            oForm.EditGroup(nID)
                    End Select

                    OpenContact(nID, sType)
                Case "export"
                    ExportAddressBook()
                Case "import"
                    ImportContacts()
            End Select

            If oList.Name.ToLower = "lsvcontacts" Then
                tbNav.SelectedTabIndex = 0
            Else
                tbNav.SelectedTabIndex = 1
            End If

            DrawContacts()
        Catch : End Try
    End Sub
    Private Sub ImportContacts()
        Dim sChar As String = InputBox("Please enter the character used as the field delimiter", Application.ProductName)
        Dim oUI As New clsMarsUI
        Dim oData As New clsMarsData
        Dim sName As String
        Dim sAddress As String
        Dim sAddressEntry As String
        Dim sPhone As String
        Dim sFax As String
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer
        Dim oRes As DialogResult

        If sChar.Length = 0 Then Exit Sub

        ofg.Title = "Please select the file to import"

        ofg.ShowDialog()

        If ofg.FileName.Length = 0 Then Return

        Dim sImport As String = ReadTextFromFile(ofg.FileName)

        If sImport.EndsWith(sChar) = False Then
            sImport &= sChar
        End If

        Dim nCount As Integer = FindOccurence(sImport, Environment.NewLine) + 1

        For I As Integer = 1 To nCount

            oUI.BusyProgress((I / nCount) * 100, "Importing...")

            sAddressEntry = GetDelimitedWord(sImport, I, Environment.NewLine)

            sName = GetDelimitedWord(sAddressEntry, 1, sChar).Trim
            sAddress = GetDelimitedWord(sAddressEntry, 2, sChar).Trim
            sPhone = GetDelimitedWord(sAddressEntry, 3, sChar).Trim
            sFax = GetDelimitedWord(sAddressEntry, 4, sChar).Trim

            If sAddress.Length > 0 And sName.Length > 1 Then

                If clsMarsData.IsDuplicate("ContactAttr", "ContactName", _
                sName, False) = True Then
Hell:
                    oRes = MessageBox.Show("The name '" & sName & "' already " & _
                    "exists in the address book." & Environment.NewLine & _
                    "Press 'Yes' to overwrite it, 'No' to rename it or " & _
                    "'Cancel' to skip this entry'", Application.ProductName, _
                     MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)

                    Select Case oRes

                        Case DialogResult.Yes
                            clsMarsData.WriteData("DELETE FROM ContactAttr " & _
                            "WHERE ContactName ='" & SQLPrepare(sName) & "'")
                        Case DialogResult.No
                            sName = _
                            InputBox("Please enter the new name", _
                            Application.ProductName)

                            If sName.Length = 0 Then
                                GoTo Here
                            ElseIf clsMarsData.IsDuplicate("ContactAttr", _
                            "ContactName", sName, False) = True Then
                                GoTo Hell
                            End If
                        Case DialogResult.Cancel
                            GoTo Here
                    End Select

                End If

                nID = clsMarsData.CreateDataID("contactattr", "contactid")

                sCols = "ContactID, ContactName,ContactType"

                sVals = nID & "," & _
                "'" & SQLPrepare(sName) & "'," & _
                "'contact'"

                SQL = "INSERT INTO ContactAttr (" & sCols & ") VALUES(" & sVals & ")"

                If clsMarsData.WriteData(SQL) = True Then
                    sCols = "DetailID,ContactID,EmailAddress,PhoneNumber,FaxNumber"

                    sVals = clsMarsData.CreateDataID("contactdetail", "detailid") & "," & _
                    nID & "," & _
                    "'" & SQLPrepare(sAddress) & "'," & _
                    "'" & SQLPrepare(sPhone) & "'," & _
                    "'" & SQLPrepare(sFax) & "'"

                    SQL = "INSERT INTO ContactDetail(" & sCols & ") " & _
                    "VALUES (" & sVals & ")"

                    clsMarsData.WriteData(SQL)

                End If


            End If
Here:
        Next
        oUI.BusyProgress(, , True)
        DrawContacts()
    End Sub
    Private Sub ExportAddressBook()
        Dim sAddress As String
        Dim oRs As ADODB.Recordset
        Dim I As Integer
        Dim oUi As New clsMarsUI
        Dim sName As String
        Dim xmlBody As String = "<?xml version='1.0' encoding='utf-8'?>" & vbCrLf & "<AddressBook>" & vbCrLf

        sfg.OverwritePrompt = True
        sfg.Title = "Save export file as..."
        sfg.ShowDialog()

        sAddress = sfg.FileName

        If sAddress.Length = 0 Then Exit Sub

        Dim SQL As String = "SELECT * FROM  ContactAttr c INNER JOIN " & _
        "ContactDetail x ON c.ContactID = x.ContactID"

        oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

        If oRs Is Nothing Then Return

        With oRs
            'sName = "ContactName,Email,Phone Number,Fax Number" & Environment.NewLine

            Do While .EOF = False
                I += I
                oUi.BusyProgress((I / .RecordCount) * 100, "Exporting...", False)

                Dim contactid As Integer = oRs(0).Value
                Dim contactName As String = oRs("contactname").Value
                Dim emailAddress As String = IsNull(oRs("emailAddress").Value)
                Dim phoneNumber As String = IsNull(oRs("phonenumber").Value)
                Dim faxNumber As String = IsNull(oRs("faxNumber").Value)

                xmlBody &= "<Address ID='" & contactid & "'>" & vbCrLf & _
                "<ContactName>" & contactName & "</ContactName>" & vbCrLf & _
                "<EmailAddress>" & emailAddress & "</EmailAddress>" & vbCrLf & _
                "<PhoneNumber>" & phoneNumber & "</PhoneNumber>" & vbCrLf & _
                "<FaxNumber>" & faxNumber & "</FaxNumber>" & vbCrLf & _
                "</Address>" & vbCrLf

                sName &= oRs("contactname").Value & "," & _
                    oRs("emailaddress").Value & "," & _
                    IsNull(oRs("phonenumber").Value) & "," & _
                    IsNull(oRs("faxnumber").Value) & Environment.NewLine

                .MoveNext()
            Loop

            xmlBody &= "</AddressBook>"

            .Close()

            oUi.BusyProgress(, , True)

        End With

        If sAddress.EndsWith(".txt") Then
            SaveTextToFile(sName, sAddress)
        Else
            SaveTextToFile(xmlBody, sAddress, , , False)
        End If

        MessageBox.Show("Contacts exported successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub
    Private Sub lsvContacts_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvContacts.SelectedIndexChanged
        Dim nID As Integer
        Dim sType As String

        If lsvContacts.SelectedItems.Count = 0 Then Return

        nID = lsvContacts.SelectedItems(0).Tag.Split(":")(1)
        sType = lsvContacts.SelectedItems(0).Tag.Split(":")(0)

        OpenContact(nID, sType)

    End Sub

    Private Sub lsvGroups_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvGroups.SelectedIndexChanged
        Dim nID As Integer
        Dim sType As String

        If lsvGroups.SelectedItems.Count = 0 Then Return

        nID = lsvGroups.SelectedItems(0).Tag.Split(":")(1)
        sType = lsvGroups.SelectedItems(0).Tag.Split(":")(0)

        OpenContact(nID, sType)

    End Sub

    Private Sub pnContacts_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oList = lsvContacts

        lsvDetails.Items.Clear()

        lsvContacts_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub pnGroups_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        oList = lsvGroups
        lsvDetails.Items.Clear()
        lsvGroups_SelectedIndexChanged(sender, e)
    End Sub

    Private Sub lsvContacts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvContacts.DoubleClick
        Dim nID As Integer
        Dim sType As String


        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
        DrawContacts()
    End Sub

    Private Sub lsvGroups_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvGroups.DoubleClick
        Dim nID As Integer
        Dim sType As String


        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
        DrawContacts()
    End Sub

    Private Sub lsvDetails_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDetails.DoubleClick
        Dim nID As Integer
        Dim sType As String

        If oList.SelectedItems.Count = 0 Then Return

        nID = oList.SelectedItems(0).Tag.split(":")(1)
        sType = oList.SelectedItems(0).Tag.split(":")(0)

        Dim nSel As Integer = oList.SelectedItems(0).Index

        Dim oForm As New frmAddContact

        oForm.isEdit = True

        Select Case sType
            Case "contact"
                oForm.EditContact(nID)
            Case "group"
                oForm.EditGroup(nID)
        End Select

        OpenContact(nID, sType)
    End Sub

   
    Private Sub tbNav_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tbNav.SelectedTabChanged
        On Error Resume Next
        Select Case e.NewTab.Name.ToLower
            Case "tbcontacts"
                If lsvGroups.SelectedItems.Count > 0 Then lsvGroups.SelectedItems(0).Selected = False
                If lsvContacts.Items.Count > 0 Then lsvContacts.Items(0).Selected = True

                oList = lsvContacts

                If lsvContacts.Items.Count = 0 Then Me.lsvDetails.Items.Clear()
            Case "tbgroups"
                If lsvContacts.SelectedItems.Count > 0 Then lsvContacts.SelectedItems(0).Selected = False

                If lsvGroups.Items.Count > 0 Then lsvGroups.Items(0).Selected = True

                oList = lsvGroups

                If lsvGroups.Items.Count = 0 Then Me.lsvDetails.Items.Clear()
        End Select
    End Sub

    
End Class
