Imports System.IO
Imports sqlrd.clsMarsData
Imports Microsoft.Win32
Public Class frmAddHouseKeeper
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
    Dim sMode As String = ""
    Dim ep As New ErrorProvider

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cmbAgedUnit As System.Windows.Forms.ComboBox
    Friend WithEvents cmdPurge As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmdTempFolder As System.Windows.Forms.Button
    Friend WithEvents txtTempFolder As System.Windows.Forms.TextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmbAgedValue As System.Windows.Forms.NumericUpDown
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddHouseKeeper))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmbAgedValue = New System.Windows.Forms.NumericUpDown
        Me.cmbAgedUnit = New System.Windows.Forms.ComboBox
        Me.cmdPurge = New System.Windows.Forms.Button
        Me.Label17 = New System.Windows.Forms.Label
        Me.cmdTempFolder = New System.Windows.Forms.Button
        Me.txtTempFolder = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.Label16 = New System.Windows.Forms.Label
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        CType(Me.cmbAgedValue, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbAgedValue)
        Me.GroupBox1.Controls.Add(Me.cmbAgedUnit)
        Me.GroupBox1.Controls.Add(Me.cmdPurge)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.cmdTempFolder)
        Me.GroupBox1.Controls.Add(Me.txtTempFolder)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 96)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'cmbAgedValue
        '
        Me.cmbAgedValue.Location = New System.Drawing.Point(152, 64)
        Me.cmbAgedValue.Maximum = New Decimal(New Integer() {250, 0, 0, 0})
        Me.cmbAgedValue.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbAgedValue.Name = "cmbAgedValue"
        Me.cmbAgedValue.Size = New System.Drawing.Size(80, 21)
        Me.cmbAgedValue.TabIndex = 2
        Me.cmbAgedValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbAgedValue.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbAgedUnit
        '
        Me.cmbAgedUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbAgedUnit.ForeColor = System.Drawing.Color.Blue
        Me.cmbAgedUnit.ItemHeight = 13
        Me.cmbAgedUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days", "Weeks", "Months"})
        Me.cmbAgedUnit.Location = New System.Drawing.Point(240, 64)
        Me.cmbAgedUnit.Name = "cmbAgedUnit"
        Me.cmbAgedUnit.Size = New System.Drawing.Size(88, 21)
        Me.cmbAgedUnit.TabIndex = 3
        '
        'cmdPurge
        '
        Me.cmdPurge.Image = CType(resources.GetObject("cmdPurge.Image"), System.Drawing.Image)
        Me.cmdPurge.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPurge.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPurge.Location = New System.Drawing.Point(432, 64)
        Me.cmdPurge.Name = "cmdPurge"
        Me.cmdPurge.Size = New System.Drawing.Size(64, 21)
        Me.cmdPurge.TabIndex = 4
        Me.cmdPurge.Text = "Purge"
        Me.cmdPurge.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label17
        '
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(8, 67)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(144, 14)
        Me.Label17.TabIndex = 13
        Me.Label17.Text = "Delete items older than"
        '
        'cmdTempFolder
        '
        Me.cmdTempFolder.Image = CType(resources.GetObject("cmdTempFolder.Image"), System.Drawing.Image)
        Me.cmdTempFolder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdTempFolder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTempFolder.Location = New System.Drawing.Point(432, 32)
        Me.cmdTempFolder.Name = "cmdTempFolder"
        Me.cmdTempFolder.Size = New System.Drawing.Size(64, 21)
        Me.cmdTempFolder.TabIndex = 1
        Me.cmdTempFolder.Text = "..."
        '
        'txtTempFolder
        '
        Me.txtTempFolder.BackColor = System.Drawing.Color.White
        Me.txtTempFolder.ContextMenu = Me.mnuInserter
        Me.txtTempFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtTempFolder.Location = New System.Drawing.Point(8, 32)
        Me.txtTempFolder.Name = "txtTempFolder"
        Me.txtTempFolder.ReadOnly = True
        Me.txtTempFolder.Size = New System.Drawing.Size(408, 21)
        Me.txtTempFolder.TabIndex = 0
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'Label16
        '
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 16)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(264, 16)
        Me.Label16.TabIndex = 9
        Me.Label16.Text = "Folder Path"
        '
        'cmdOK
        '
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(359, 112)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(440, 112)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(64, 24)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmAddHouseKeeper
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(538, 144)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmAddHouseKeeper"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Housekeeping Path"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.cmbAgedValue, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAddHouseKeeper_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub _AddPath()

        sMode = "Add"

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        '        CREATE TABLE HouseKeepingPaths (
        'EntryID			INTEGER PRIMARY KEY,
        'FolderPath		VARCHAR(255),
        'KeepFor			INTEGER,
        '        KeepUnit(VARCHAR(55))
        ');

        sCols = "EntryID,FolderPath,KeepFor,KeepUnit"

        Dim EntryID As Integer = CreateDataID("housekeepingpaths", "entryid")

        sVals = EntryID & "," & _
        "'" & SQLPrepare(_CreateUNC(txtTempFolder.Text)) & "'," & _
        cmbAgedValue.Value & "," & _
        "'" & cmbAgedUnit.Text & "'"


        SQL = "INSERT INTO HouseKeepingPaths (" & sCols & ") VALUES (" & sVals & ")"

        WriteData(SQL)

    End Sub

    Public Sub _EditPath(ByVal PathID As Integer)
        Dim oRs As ADODB.Recordset

        oRs = GetData("SELECT * FROM HouseKeepingPaths WHERE EntryID =" & PathID)

        If Not oRs Is Nothing Then
            If oRs.EOF = False Then
                txtTempFolder.Text = oRs("folderpath").Value
                cmbAgedValue.Value = oRs("keepfor").Value
                cmbAgedUnit.Text = oRs("keepunit").Value
            End If

            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim SQL As String

        SQL = "UPDATE HouseKeepingPaths SET " & _
        "FolderPath = '" & SQLPrepare(_CreateUNC(txtTempFolder.Text)) & "'," & _
        "KeepFor =" & cmbAgedValue.Value & "," & _
        "KeepUnit ='" & cmbAgedUnit.Text & "' " & _
        " WHERE EntryID =" & PathID

        WriteData(SQL)

    End Sub
    Private Sub cmdTempFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTempFolder.Click
        On Error Resume Next

        With ofd
            .Description = "Please select the temporary output folder..."
            .ShowNewFolderButton = True
            .ShowDialog()

            If .SelectedPath = "" Then
                Exit Sub
            Else
                txtTempFolder.Text = .SelectedPath

                If txtTempFolder.Text.Substring(txtTempFolder.Text.Length - 1, 1) <> "\" Then
                    txtTempFolder.Text += "\"
                End If
            End If
        End With
    End Sub

    Private Sub cmdPurge_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPurge.Click
        On Error Resume Next

        If MessageBox.Show("All folders and files in the '" & txtTempFolder.Text & "' will be deleted. Proceed?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            For Each s As String In IO.Directory.GetDirectories(txtTempFolder.Text)
                IO.Directory.Delete(s, True)
            Next

            For Each s As String In IO.Directory.GetFiles(txtTempFolder.Text)
                IO.File.Delete(s)
            Next

            MessageBox.Show("All temporary files have been purged", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtTempFolder.Text.Length = 0 Then
            ep.SetError(txtTempFolder, "Please provide the folder path.")
            txtTempFolder.Focus()
            Return
        ElseIf cmbAgedValue.Value = 0 Then
            ep.SetError(cmbAgedValue, "Please specify the duration to keep files")
            cmbAgedValue.Focus()
            Return
        ElseIf cmbAgedUnit.Text.Length = 0 Then
            ep.SetError(cmbAgedUnit, "Please select the unit")
            cmbAgedUnit.Focus()
            Return
        ElseIf sMode = "Add" Then
            If clsMarsData.IsDuplicate("HouseKeepingPaths", "FolderPath", txtTempFolder.Text, False) = True Then
                ep.SetError(txtTempFolder, "This folder has already a housekeeping rule added")
                txtTempFolder.Focus()
                Return
            End If
        End If

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Return
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next
        txtTempFolder.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        txtTempFolder.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        txtTempFolder.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        txtTempFolder.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        txtTempFolder.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        txtTempFolder.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)

        oInsert.GetConstants(Me)

        If txtTempFolder.Text.EndsWith("\") = False Then
            txtTempFolder.Text = txtTempFolder.Text & "\"
        End If
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems

        txtTempFolder.SelectedText = oItem._GetDataItem(0)

        If txtTempFolder.Text.EndsWith("\") = False Then
            txtTempFolder.Text = txtTempFolder.Text & "\"
        End If
    End Sub
End Class
