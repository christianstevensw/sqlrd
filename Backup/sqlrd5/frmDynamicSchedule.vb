Imports DevComponents
Imports DevComponents.DotNetBar
Imports Microsoft.Win32

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmDynamicSchedule
    Inherits System.Windows.Forms.Form
    Dim oErr As New clsMarsUI
    Dim nStep As Integer = 0
    Dim oData As New clsMarsData
    Dim sLink As String = String.Empty
    Dim oMsg As New clsMarsMessaging
    Dim oRpt As Object 'ReportServer.ReportingService
    Dim sTempKey As String = ""
    Dim oUI As New clsMarsUI
    Dim ServerUser As String = ""
    Dim ServerPassword As String = ""
    Dim ParDefaults As Hashtable
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim m_serverParametersTable As DataTable
    Const S1 As String = "Step 1: Report Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Key Parameter"
    Const S4 As String = "Step 4: Parameter to Database Linking"
    Const S5 As String = "Step 5: Report Destination"
    Const S6 As String = "Step 6: Report Options"
    Const S7 As String = "Step 7: Exception Handling"
    Const S8 As String = "Step 8: Custom Tasks"
    Dim HasCancelled As Boolean = False
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents txtUrl As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents UcErrorHandler1 As sqlrd.ucErrorHandler
    Friend WithEvents grpResume As System.Windows.Forms.GroupBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkAutoResume As System.Windows.Forms.CheckBox
    Friend WithEvents Label6 As System.Windows.Forms.Label


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdDbLoc As System.Windows.Forms.Button
    Friend WithEvents txtDBLoc As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvParameters As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdParameters As System.Windows.Forms.Button
    Friend WithEvents cmdAdvanced As System.Windows.Forms.Button
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents UcTasks1 As sqlrd.ucTasks
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmbDestination As System.Windows.Forms.ComboBox
    Friend WithEvents HelpTip As System.Windows.Forms.ToolTip
    Friend WithEvents HelpLink As System.Windows.Forms.LinkLabel
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents cmdSubreports As System.Windows.Forms.Button
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    Friend WithEvents Step8 As System.Windows.Forms.Panel
    Friend WithEvents Step7 As System.Windows.Forms.Panel
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents grpDynamicDest As System.Windows.Forms.GroupBox
    Friend WithEvents chkStaticDest As System.Windows.Forms.CheckBox
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDynamicSchedule))
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtUrl = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdDbLoc = New System.Windows.Forms.Button
        Me.txtDBLoc = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblStep = New System.Windows.Forms.Label
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.cmbDestination = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.ucSet = New sqlrd.ucScheduleSet
        Me.Step3 = New System.Windows.Forms.Panel
        Me.cmdSubreports = New System.Windows.Forms.Button
        Me.cmdParameters = New System.Windows.Forms.Button
        Me.cmdAdvanced = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lsvParameters = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.HelpLink = New System.Windows.Forms.LinkLabel
        Me.optDynamic = New System.Windows.Forms.RadioButton
        Me.optStatic = New System.Windows.Forms.RadioButton
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.Step4 = New System.Windows.Forms.Panel
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.chkStaticDest = New System.Windows.Forms.CheckBox
        Me.grpDynamicDest = New System.Windows.Forms.GroupBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmdTest = New System.Windows.Forms.Button
        Me.Label13 = New System.Windows.Forms.Label
        Me.cmbTable = New System.Windows.Forms.ComboBox
        Me.Step5 = New System.Windows.Forms.Panel
        Me.UcDest = New sqlrd.ucDestination
        Me.Step8 = New System.Windows.Forms.Panel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optOnce = New System.Windows.Forms.RadioButton
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.UcTasks1 = New sqlrd.ucTasks
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Step6 = New System.Windows.Forms.Panel
        Me.grpResume = New System.Windows.Forms.GroupBox
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.chkAutoResume = New System.Windows.Forms.CheckBox
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.lsvDatasources = New System.Windows.Forms.ListView
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Step7 = New System.Windows.Forms.Panel
        Me.UcErrorHandler1 = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.HelpTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.Footer1 = New WizardFooter.Footer
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Step1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Step4.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.grpDynamicDest.SuspendLayout()
        Me.Step5.SuspendLayout()
        Me.Step8.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.grpResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.mnuDatasources.SuspendLayout()
        Me.Step7.SuspendLayout()
        Me.SuspendLayout()
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.SystemColors.Control
        Me.Step1.Controls.Add(Me.txtUrl)
        Me.Step1.Controls.Add(Me.Label6)
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label10)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdDbLoc)
        Me.Step1.Controls.Add(Me.txtDBLoc)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.HelpProvider1.SetHelpKeyword(Me.Step1, "Dynamic_Schedule.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Step1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step1.Location = New System.Drawing.Point(0, 72)
        Me.Step1.Name = "Step1"
        Me.HelpProvider1.SetShowHelp(Me.Step1, True)
        Me.Step1.Size = New System.Drawing.Size(448, 400)
        Me.Step1.TabIndex = 0
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtUrl, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtUrl, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtUrl.Location = New System.Drawing.Point(8, 69)
        Me.txtUrl.Name = "txtUrl"
        Me.HelpProvider1.SetShowHelp(Me.txtUrl, True)
        Me.txtUrl.Size = New System.Drawing.Size(358, 21)
        Me.txtUrl.TabIndex = 2
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label6, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(5, 53)
        Me.Label6.Name = "Label6"
        Me.HelpProvider1.SetShowHelp(Me.Label6, True)
        Me.Label6.Size = New System.Drawing.Size(283, 16)
        Me.Label6.TabIndex = 26
        Me.Label6.Text = "Report  Service URL"
        '
        'txtDesc
        '
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(8, 198)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(360, 118)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label1, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(5, 179)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label10
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label10, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label10, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(5, 319)
        Me.Label10.Name = "Label10"
        Me.HelpProvider1.SetShowHelp(Me.Label10, True)
        Me.Label10.Size = New System.Drawing.Size(208, 16)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 336)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDbLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDbLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDbLoc.Image = CType(resources.GetObject("cmdDbLoc.Image"), System.Drawing.Image)
        Me.cmdDbLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(310, 110)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdDbLoc, True)
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 4
        Me.cmdDbLoc.Text = "..."
        Me.HelpTip.SetToolTip(Me.cmdDbLoc, "Browse")
        Me.cmdDbLoc.UseVisualStyleBackColor = False
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtDBLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDBLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDBLoc.Location = New System.Drawing.Point(8, 110)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtDBLoc, True)
        Me.txtDBLoc.Size = New System.Drawing.Size(296, 21)
        Me.txtDBLoc.TabIndex = 3
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(312, 24)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 1
        Me.cmdLoc.Text = "...."
        Me.HelpTip.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(8, 24)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 0
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 8)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(5, 96)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Report  Location"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(8, 152)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 5
        '
        'Label2
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "Dynamic_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(5, 134)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'lblStep
        '
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(384, 32)
        Me.lblStep.TabIndex = 2
        Me.lblStep.Text = "Step 1: Report Setup"
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(365, 490)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(285, 490)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(205, 490)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(387, 10)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.cmbDestination)
        Me.Step2.Controls.Add(Me.Label7)
        Me.Step2.Controls.Add(Me.ucSet)
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Dynamic_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 72)
        Me.Step2.Name = "Step2"
        Me.Step2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(448, 400)
        Me.Step2.TabIndex = 0
        Me.Step2.Visible = False
        '
        'cmbDestination
        '
        Me.cmbDestination.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDestination.ItemHeight = 13
        Me.cmbDestination.Items.AddRange(New Object() {"Disk", "Email", "Fax", "FTP", "Printer", "SMS"})
        Me.cmbDestination.Location = New System.Drawing.Point(128, 8)
        Me.cmbDestination.Name = "cmbDestination"
        Me.cmbDestination.Size = New System.Drawing.Size(160, 21)
        Me.cmbDestination.Sorted = True
        Me.cmbDestination.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(16, 10)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 17)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "Destination"
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(8, 29)
        Me.ucSet.m_RepeatUnit = ""
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(421, 366)
        Me.ucSet.TabIndex = 2
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.SystemColors.Control
        Me.Step3.Controls.Add(Me.cmdSubreports)
        Me.Step3.Controls.Add(Me.cmdParameters)
        Me.Step3.Controls.Add(Me.cmdAdvanced)
        Me.Step3.Controls.Add(Me.GroupBox3)
        Me.Step3.Controls.Add(Me.GroupBox2)
        Me.HelpProvider1.SetHelpKeyword(Me.Step3, "Dynamic_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.Step3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step3.Location = New System.Drawing.Point(0, 72)
        Me.Step3.Name = "Step3"
        Me.HelpProvider1.SetShowHelp(Me.Step3, True)
        Me.Step3.Size = New System.Drawing.Size(448, 400)
        Me.Step3.TabIndex = 16
        Me.Step3.Visible = False
        '
        'cmdSubreports
        '
        Me.cmdSubreports.BackColor = System.Drawing.SystemColors.Control
        Me.cmdSubreports.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdSubreports.ForeColor = System.Drawing.Color.Navy
        Me.cmdSubreports.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSubreports.Location = New System.Drawing.Point(168, 367)
        Me.cmdSubreports.Name = "cmdSubreports"
        Me.cmdSubreports.Size = New System.Drawing.Size(75, 23)
        Me.cmdSubreports.TabIndex = 8
        Me.cmdSubreports.Text = "Subreports"
        Me.cmdSubreports.UseVisualStyleBackColor = False
        Me.cmdSubreports.Visible = False
        '
        'cmdParameters
        '
        Me.cmdParameters.BackColor = System.Drawing.SystemColors.Control
        Me.cmdParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdParameters.ForeColor = System.Drawing.Color.Navy
        Me.cmdParameters.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParameters.Location = New System.Drawing.Point(341, 367)
        Me.cmdParameters.Name = "cmdParameters"
        Me.cmdParameters.Size = New System.Drawing.Size(75, 23)
        Me.cmdParameters.TabIndex = 1
        Me.cmdParameters.Text = "Parameters"
        Me.cmdParameters.UseVisualStyleBackColor = False
        '
        'cmdAdvanced
        '
        Me.cmdAdvanced.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdvanced.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdAdvanced.ForeColor = System.Drawing.Color.Navy
        Me.cmdAdvanced.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdvanced.Location = New System.Drawing.Point(8, 367)
        Me.cmdAdvanced.Name = "cmdAdvanced"
        Me.cmdAdvanced.Size = New System.Drawing.Size(75, 23)
        Me.cmdAdvanced.TabIndex = 6
        Me.cmdAdvanced.Text = "Advanced"
        Me.cmdAdvanced.UseVisualStyleBackColor = False
        Me.cmdAdvanced.Visible = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lsvParameters)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 152)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(432, 209)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Report Parameters"
        '
        'lsvParameters
        '
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader5})
        Me.lsvParameters.Location = New System.Drawing.Point(8, 16)
        Me.lsvParameters.Name = "lsvParameters"
        Me.lsvParameters.Size = New System.Drawing.Size(400, 184)
        Me.lsvParameters.TabIndex = 5
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 133
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Value"
        Me.ColumnHeader2.Width = 186
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Multi-Value"
        Me.ColumnHeader5.Width = 70
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.HelpLink)
        Me.GroupBox2.Controls.Add(Me.optDynamic)
        Me.GroupBox2.Controls.Add(Me.optStatic)
        Me.GroupBox2.Controls.Add(Me.cmbKey)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(432, 136)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Parameters"
        '
        'HelpLink
        '
        Me.HelpLink.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.HelpLink.LinkArea = New System.Windows.Forms.LinkArea(0, 19)
        Me.HelpLink.Location = New System.Drawing.Point(248, 48)
        Me.HelpLink.Name = "HelpLink"
        Me.HelpLink.Size = New System.Drawing.Size(100, 16)
        Me.HelpLink.TabIndex = 1
        Me.HelpLink.TabStop = True
        Me.HelpLink.Text = "Click Here for Help"
        '
        'optDynamic
        '
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(8, 104)
        Me.optDynamic.Name = "optDynamic"
        Me.optDynamic.Size = New System.Drawing.Size(352, 24)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.Text = "Populate key parameter with data from a database using a query"
        '
        'optStatic
        '
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(8, 80)
        Me.optStatic.Name = "optStatic"
        Me.optStatic.Size = New System.Drawing.Size(224, 16)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate key parameter with static data"
        '
        'cmbKey
        '
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(8, 48)
        Me.cmbKey.Name = "cmbKey"
        Me.cmbKey.Size = New System.Drawing.Size(232, 21)
        Me.cmbKey.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 16)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(416, 32)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Please select the report parameter that will be used to look up information from " & _
            "your database"
        '
        'txtFormula
        '
        Me.txtFormula.Location = New System.Drawing.Point(32, 416)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(32, 21)
        Me.txtFormula.TabIndex = 15
        Me.txtFormula.Visible = False
        '
        'Step4
        '
        Me.Step4.BackColor = System.Drawing.SystemColors.Control
        Me.Step4.Controls.Add(Me.GroupBox4)
        Me.Step4.Location = New System.Drawing.Point(0, 72)
        Me.Step4.Name = "Step4"
        Me.Step4.Size = New System.Drawing.Size(448, 400)
        Me.Step4.TabIndex = 17
        Me.Step4.Visible = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkStaticDest)
        Me.GroupBox4.Controls.Add(Me.grpDynamicDest)
        Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.GroupBox4.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(432, 395)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        '
        'chkStaticDest
        '
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(8, 16)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 0
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        '
        'grpDynamicDest
        '
        Me.grpDynamicDest.Controls.Add(Me.Label9)
        Me.grpDynamicDest.Controls.Add(Me.cmdValidate)
        Me.grpDynamicDest.Controls.Add(Me.UcDSN)
        Me.grpDynamicDest.Controls.Add(Me.cmbColumn)
        Me.grpDynamicDest.Controls.Add(Me.cmbValue)
        Me.grpDynamicDest.Controls.Add(Me.Label15)
        Me.grpDynamicDest.Controls.Add(Me.cmdTest)
        Me.grpDynamicDest.Controls.Add(Me.Label13)
        Me.grpDynamicDest.Controls.Add(Me.cmbTable)
        Me.grpDynamicDest.Location = New System.Drawing.Point(8, 40)
        Me.grpDynamicDest.Name = "grpDynamicDest"
        Me.grpDynamicDest.Size = New System.Drawing.Size(416, 349)
        Me.grpDynamicDest.TabIndex = 1
        Me.grpDynamicDest.TabStop = False
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 16)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(384, 32)
        Me.Label9.TabIndex = 4
        Me.Label9.Text = "Please specify the table and column that will be used to gather the xxx from the " & _
            "database"
        '
        'cmdValidate
        '
        Me.cmdValidate.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 207)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 56)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(384, 145)
        Me.UcDSN.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 273)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 3
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 321)
        Me.cmbValue.Name = "cmbValue"
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 305)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(360, 16)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Please select the column that holds the xxx"
        '
        'cmdTest
        '
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(272, 321)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 21)
        Me.cmdTest.TabIndex = 5
        Me.cmdTest.Text = "Test"
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 257)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(392, 16)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Please select the table and column that must equal the key parameter"
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 273)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 2
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.SystemColors.Control
        Me.Step5.Controls.Add(Me.UcDest)
        Me.HelpProvider1.SetHelpKeyword(Me.Step5, "Dynamic_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Step5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step5.Location = New System.Drawing.Point(0, 64)
        Me.Step5.Name = "Step5"
        Me.HelpProvider1.SetShowHelp(Me.Step5, True)
        Me.Step5.Size = New System.Drawing.Size(448, 400)
        Me.Step5.TabIndex = 18
        Me.Step5.Visible = False
        '
        'UcDest
        '
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(8, 8)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(432, 382)
        Me.UcDest.TabIndex = 0
        '
        'Step8
        '
        Me.Step8.BackColor = System.Drawing.SystemColors.Control
        Me.Step8.Controls.Add(Me.GroupBox1)
        Me.Step8.Controls.Add(Me.UcTasks1)
        Me.HelpProvider1.SetHelpKeyword(Me.Step8, "Dynamic_Schedule.htm#Step8")
        Me.HelpProvider1.SetHelpNavigator(Me.Step8, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step8.Location = New System.Drawing.Point(0, 72)
        Me.Step8.Name = "Step8"
        Me.HelpProvider1.SetShowHelp(Me.Step8, True)
        Me.Step8.Size = New System.Drawing.Size(448, 400)
        Me.Step8.TabIndex = 21
        Me.Step8.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optOnce)
        Me.GroupBox1.Controls.Add(Me.optAll)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 316)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(414, 74)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 0
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'UcTasks1
        '
        Me.UcTasks1.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks1.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks1.Location = New System.Drawing.Point(8, 8)
        Me.UcTasks1.m_defaultTaks = False
        Me.UcTasks1.m_eventBased = False
        Me.UcTasks1.m_eventID = 99999
        Me.UcTasks1.Name = "UcTasks1"
        Me.UcTasks1.Size = New System.Drawing.Size(432, 299)
        Me.UcTasks1.TabIndex = 0
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(365, 490)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.rpt"
        Me.ofd.Filter = "Crystal Reports|*.rpt|All Files|*.*"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.lblStep)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(442, 64)
        Me.Panel2.TabIndex = 23
        '
        'Step6
        '
        Me.Step6.Controls.Add(Me.grpResume)
        Me.Step6.Controls.Add(Me.GroupBox6)
        Me.HelpProvider1.SetHelpKeyword(Me.Step6, "Dynamic_Schedule.htm#Step6")
        Me.HelpProvider1.SetHelpNavigator(Me.Step6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step6.Location = New System.Drawing.Point(0, 72)
        Me.Step6.Name = "Step6"
        Me.HelpProvider1.SetShowHelp(Me.Step6, True)
        Me.Step6.Size = New System.Drawing.Size(448, 400)
        Me.Step6.TabIndex = 26
        Me.Step6.Visible = False
        '
        'grpResume
        '
        Me.grpResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpResume.Controls.Add(Me.Label5)
        Me.grpResume.Controls.Add(Me.Label8)
        Me.grpResume.Controls.Add(Me.chkAutoResume)
        Me.grpResume.Location = New System.Drawing.Point(8, 315)
        Me.grpResume.Name = "grpResume"
        Me.grpResume.Size = New System.Drawing.Size(422, 74)
        Me.grpResume.TabIndex = 18
        Me.grpResume.TabStop = False
        Me.grpResume.Text = "Resume with cached data"
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(213, 41)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 2
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(257, 45)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "minutes"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(3, 45)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(168, 13)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Expire dynamic cached data after"
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        Me.chkAutoResume.Location = New System.Drawing.Point(6, 20)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(268, 17)
        Me.chkAutoResume.TabIndex = 0
        Me.chkAutoResume.Text = "Resume failed/errored schedules with cached data"
        Me.HelpTip.SetToolTip(Me.chkAutoResume, "If the schedule fails mid-flight, next time it runs, it will resume from where it" & _
                " left off instead of starting from the beginning")
        Me.chkAutoResume.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.lsvDatasources)
        Me.GroupBox6.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(422, 299)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Report Datasources"
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.lsvDatasources.FullRowSelect = True
        Me.lsvDatasources.HideSelection = False
        Me.lsvDatasources.Location = New System.Drawing.Point(6, 16)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.lsvDatasources.Size = New System.Drawing.Size(405, 270)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvDatasources, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Double-click on a datasource to set its login credentials. Leaving a datasource's" & _
                    " credentials not set will result in errors if they are required.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon))
        Me.lsvDatasources.TabIndex = 1
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Datasource Name"
        Me.ColumnHeader3.Width = 274
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Name"
        Me.ColumnHeader4.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'Step7
        '
        Me.Step7.Controls.Add(Me.UcErrorHandler1)
        Me.Step7.Controls.Add(Me.UcBlank)
        Me.HelpProvider1.SetHelpKeyword(Me.Step7, "Dynamic_Schedule.htm#Step7")
        Me.HelpProvider1.SetHelpNavigator(Me.Step7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step7.Location = New System.Drawing.Point(0, 72)
        Me.Step7.Name = "Step7"
        Me.HelpProvider1.SetShowHelp(Me.Step7, True)
        Me.Step7.Size = New System.Drawing.Size(448, 400)
        Me.Step7.TabIndex = 27
        Me.Step7.Visible = False
        '
        'UcErrorHandler1
        '
        Me.UcErrorHandler1.BackColor = System.Drawing.Color.Transparent
        Me.UcErrorHandler1.Location = New System.Drawing.Point(13, 4)
        Me.UcErrorHandler1.m_showFailOnOne = False
        Me.UcErrorHandler1.Name = "UcErrorHandler1"
        Me.UcErrorHandler1.Size = New System.Drawing.Size(430, 65)
        Me.UcErrorHandler1.TabIndex = 2
        '
        'UcBlank
        '
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlank.Location = New System.Drawing.Point(12, 69)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(424, 326)
        Me.UcBlank.TabIndex = 1
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 473)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(472, 16)
        Me.Footer1.TabIndex = 28
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "data_table.png")
        Me.ImageList1.Images.SetKeyName(1, "key1.ico")
        Me.ImageList1.Images.SetKeyName(2, "view.ico")
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(442, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 29
        '
        'frmDynamicSchedule
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(442, 520)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.Step7)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step8)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.txtFormula)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmDynamicSchedule"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dynamic Schedule Wizard"
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.Step4.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.grpDynamicDest.ResumeLayout(False)
        Me.Step5.ResumeLayout(False)
        Me.Step8.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        Me.grpResume.ResumeLayout(False)
        Me.grpResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.mnuDatasources.ResumeLayout(False)
        Me.Step7.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property
    Private ReadOnly Property m_ParametersList() As ArrayList
        Get
            Dim list As ArrayList = New ArrayList

            For Each item As ListViewItem In lsvParameters.Items
                list.Add(item.Text)
            Next

            Return list
        End Get
    End Property

    Private Sub frmDynamicSchedule_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If HasCancelled = False Then
            cmdCancel_Click(Nothing, Nothing)
        End If
    End Sub
    Private Sub frmDynamicSchedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.s2_DynamicSched) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
            Close()
            Return
        End If

        FormatForWinXP(Me)
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        Step4.Visible = False
        Step5.Visible = False
        Step6.Visible = False
        Step7.Visible = False
        Step8.Visible = False
        Step1.BringToFront()
        txtFolder.Focus()
        lblStep.Text = S1

        UcDest.isDynamic = True

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If

        txtUrl.Text = clsMarsUI.MainUI.ReadRegistry("ReportsLoc", "")

        If txtUrl.Text.Length = 0 Then
            txtUrl.Text = "http://myReportServer/ReportServer/Reportservice.asmx"
            txtUrl.ForeColor = Color.Gray
        End If

        'set up auto-complete data
        With Me.txtUrl
            .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            .AutoCompleteSource = AutoCompleteSource.CustomSource
            .AutoCompleteCustomSource = getUrlHistory()
        End With

        oData.CleanDB()
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, ServerUser, ServerPassword)

            If srsVersion = "2007" Then
                oRpt = New ReportServer_2005.ReportingService2005
            Else
                oRpt = New ReportServer.ReportingService
            End If

            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion = "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            oRpt.Url = txtUrl.Text

            'If sReturn Is Nothing Then
            'oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            'Else
            'ServerUser = sReturn(0)
            'ServerPassword = sReturn(1)

            'cred = New System.Net.NetworkCredential(ServerUser, ServerPassword)

            'oRpt.Credentials = cred
            'End If

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            sPath = oServer.newGetReports(oRpt, txtUrl.Text, ServerUser, ServerPassword, txtDBLoc.Text)

            'If Me.txtDBLoc.Text.Length = 0 Then

            '    showTooltip = False

            '    Dim oServerBrowse As frmReportServer = New frmReportServer

            '    AppStatus(True)

            '    sPath = oServerBrowse._GetReport(txtUrl.Text, cred)
            'Else
            '    sPath = txtDBLoc.Text
            'End If

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdNext.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtName.Text & ".rdl"
            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLoc.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLoc.Text, _
            ServerUser, ServerPassword, sRDLPath)

            lsvParameters.Items.Clear()

            ParDefaults = New Hashtable

            If sPars IsNot Nothing Then
                For Each s As String In sPars
                    Dim oItem As ListViewItem = lsvParameters.Items.Add(s.Split("|")(0))

                    oItem.Tag = s.Split("|")(1)

                    Try
                        oItem.SubItems(1).Text = ""
                    Catch ex As Exception
                        oItem.SubItems.Add("")
                    End Try

                    Try
                        oItem.SubItems(2).Text = s.Split("|")(2)
                    Catch
                        oItem.SubItems.Add(s.Split("|")(2))
                    End Try

                    ParDefaults.Add(oItem.Text, sParDefaults(I))
                    cmbKey.Items.Add(oItem.Text)
                    I += 1
                Next
            End If

            UcDest.m_ParameterList = Me.m_ParametersList
            Me.UcBlank.m_ParametersList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdNext.Enabled = True
            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtDBLoc.Text = ""
                txtName.Text = ""
                txtDesc.Text = ""
            End If
        End Try
    End Sub




    Private Sub cmdParameters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdParameters.Click
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag.ToString.Split("|")(0)

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, , , m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Select Case nStep
            Case 0
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Return
                ElseIf txtFolder.Text.Length = 0 Then
                    ep.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Return
                ElseIf txtDBLoc.Text.Length = 0 Then
                    ep.SetError(cmdDbLoc, "Please select the access database for the report")
                    cmdDbLoc.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT, , True) = False Then
                    ep.SetError(txtName, "A dynamic schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                Step2.BringToFront()
                Step2.Visible = True
                Step1.Visible = False
                cmbDestination.Focus()
                cmdBack.Enabled = True
                lblStep.Text = S2
            Case 1
                'If ucSet.StartDate.Value > ucSet.EndDate.Value Then
                '    ep.SetError(ucSet.EndDate, "Please select a valid date range")
                '    Return
                'ElseIf ucSet.chkRepeat.Checked = True Then
                '    If ucSet.cmbRpt.Text = 0 Then
                '        ep.SetError(ucSet.cmbRpt, "Please select the repeat interval")
                '        Return
                '    ElseIf ucSet.RunAt.Value > ucSet.RepeatUntil.Value Then
                '        ep.SetError(ucSet.RunAt, "The execution time cannot be after the repeat until time")
                '        Return
                '    End If
                'ElseIf ucSet.optCustom.Checked = True And _
                '    (ucSet.cmbCustom.Text.Length = 0 Or _
                '    ucSet.cmbCustom.Text = "[New...]") Then
                '    ep.SetError(ucSet.cmbCustom, "Please select a valid calendar")
                '    ucSet.cmbCustom.Focus()
                '    Return
                'Else
                'ElseIf ucSet.chkException.Checked And ucSet.cmbException.Text.Length = 0 Then
                '    ep.SetError(ucSet.cmbException, "Please select an exception calendar")
                '    ucSet.cmbException.Focus()
                '    Return
                'End If
                If cmbDestination.Text.Length = 0 Then
                    ep.SetError(cmbDestination, "Please select the destination")
                    cmbDestination.Focus()
                    Return
                ElseIf ucSet.ValidateSchedule = False Then
                    Return
                End If

                Select Case cmbDestination.Text
                    Case "Email"
                        Label11.Text = Label11.Text.Replace("information", "the email address")
                        Label9.Text = Label9.Text.Replace("xxx", "email address")
                        Label15.Text = Label15.Text.Replace("xxx", "email address")
                    Case "Printer"
                        Label11.Text = Label11.Text.Replace("informaton", "the printer")
                        Label9.Text = Label9.Text.Replace("xxx", "printer")
                        Label15.Text = Label15.Text.Replace("xxx", "printer")
                    Case "Disk"
                        Label11.Text = Label11.Text.Replace("informaton", "the directory")
                        Label9.Text = Label9.Text.Replace("xxx", "directory")
                        Label15.Text = Label15.Text.Replace("xxx", "directory")
                    Case "FTP"
                        Label11.Text = Label11.Text.Replace("information", "the FTP Server")
                        Label9.Text = Label9.Text.Replace("xxx", "FTP Server")
                        Label15.Text = Label15.Text.Replace("xxx", "FTP Server")
                    Case "SMS"
                        Label11.Text = Label11.Text.Replace("information", "the cell phone number")
                        Label9.Text = Label9.Text.Replace("xxx", "cell phone number")
                        Label15.Text = Label15.Text.Replace("xxx", "cell phone number")
                    Case "Fax"
                        Label11.Text = Label11.Text.Replace("information", "the fax number")
                        Label9.Text = Label9.Text.Replace("xxx", "fax number")
                        Label15.Text = Label15.Text.Replace("xxx", "fax number")
                End Select

                Step3.BringToFront()
                Step3.Visible = True
                Step2.Visible = False
                cmdBack.Enabled = True
                cmbKey.Focus()
                lblStep.Text = S3
            Case 2
                If cmbKey.Text.Length = 0 Then
                    ep.SetError(cmbKey, "Please select the key parameter")
                    cmbKey.Focus()
                    Return
                ElseIf optStatic.Checked = False And optDynamic.Checked = False Then
                    ep.SetError(optStatic, "Please select how the above key parameter is populated")
                    'optStatic.Focus()
                    Return
                Else
                    For Each o As ListViewItem In lsvParameters.Items
                        Try
                            Dim s As String = o.SubItems(1).Text

                            If s.Length = 0 Then
                                Throw New Exception("blank parameter found man")
                            End If
                        Catch ex As Exception
                            ep.SetError(lsvParameters, "Please provide values for all the parameters")
                            cmdParameters_Click(sender, e)
                            Return
                        End Try
                    Next
                End If

                clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =99999")

                Dim sVals As String
                Dim sCols As String
                Dim I As Integer = 1
                Dim nParType As Integer

                Dim SQL As String
                sCols = "ParId, ReportID, ParName,ParValue, ParType"

                For Each oItemx As ListViewItem In lsvParameters.Items

                    sVals = clsMarsData.CreateDataID("reportparameter", "parid") & "," & _
                    99999 & "," & _
                    "'" & SQLPrepare(oItemx.Text) & "'," & _
                    "'" & SQLPrepare(oItemx.SubItems(1).Text) & "'," & _
                    "'" & oItemx.Tag & "'"

                    SQL = "INSERT INTO ReportParameter (" & sCols & ") VALUES(" & sVals & ")"

                    clsMarsData.WriteData(SQL)

                    I += 1
                Next

                Step4.BringToFront()
                Step4.Visible = True
                Step3.Visible = False
                chkStaticDest.Focus()
                lblStep.Text = S4
            Case 3
                If chkStaticDest.Checked = False Then
                    If cmbTable.Text.Length = 0 Then
                        ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                        cmbTable.Focus()
                        Return
                    ElseIf cmbColumn.Text.Length = 0 Then
                        ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                        cmbColumn.Focus()
                        Return
                    ElseIf cmbValue.Text.Length = 0 Then
                        ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                        cmbValue.Focus()
                        Return
                    ElseIf cmbValue.Text = "<Advanced>" And sLink = String.Empty Then
                        ep.SetError(cmbValue, "Please set up the query for the value")
                        cmbValue.Focus()
                        Return
                    End If
                End If

                Step5.BringToFront()
                Step5.Visible = True
                Step4.Visible = False
                lblStep.Text = S5

                UcDest.cmdAdd_Click(sender, e)
            Case 4
                If UcDest.lsvDestination.Items.Count = 0 Then
                    UcDest.ep.SetError(UcDest.lsvDestination, "Please add a destination for the report(s)")
                    Return
                End If

                Step6.BringToFront()
                Step6.Visible = True
                Step5.Visible = False
                lblStep.Text = S6
            Case 5


                Step7.BringToFront()
                Step7.Visible = True
                Step6.Visible = False
                UcErrorHandler1.cmbRetry.Focus()
                lblStep.Text = S7
            Case 6
                'If UcBlank.chkBlankReport.Checked = True Then
                '    If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                '    UcBlank.txtAlertTo.Text.Length = 0 Then
                '        UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please specify the alert recipient")
                '        UcBlank.txtAlertTo.Focus()
                '        Exit Sub
                '    End If
                'End If
                If UcBlank.ValidateEntries = False Then Return

                Step8.BringToFront()
                Step8.Visible = True
                Step7.Visible = False
                lblStep.Text = S8
                cmdNext.Visible = False
                cmdFinish.Visible = True
        End Select

        nStep += 1
    End Sub

    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        Dim oVal As New frmKeyParameter

        For Each oItem As ListViewItem In lsvParameters.Items
            If oItem.Text = cmbKey.Text Then
                Try
                    sVal = oItem.SubItems(1).Text
                Catch : End Try
                Exit For
            End If
        Next

        sVal = oVal._GetParameterValue(cmbKey.Text, sVal, ParDefaults.Item(cmbKey.Text), m_serverParametersTable)

        If sVal.Length = 0 Then Return

        For Each oItem As ListViewItem In lsvParameters.Items
            If oItem.Text = cmbKey.Text Then
                Try
                    oItem.SubItems(1).Text = sVal
                Catch
                    oItem.SubItems.Add(sVal)
                End Try

                Exit For
            End If
        Next

        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID=99999")

        Dim sCols As String
        Dim sVals As String
        Dim SQL As String

        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
        "Query,ReportID"

        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(sVal) & "'," & _
        "'_'," & _
        "'_'," & _
        99999

        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub



    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "report")

        If sParameter.Length = 0 Then Return

        For Each oItem As ListViewItem In lsvParameters.Items
            If oItem.Text = cmbKey.Text Then
                Try
                    oItem.SubItems(1).Text = sParameter
                Catch
                    oItem.SubItems.Add(sParameter)
                End Try

                Exit For
            End If
        Next
    End Sub

    Private Sub optStatic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optStatic.CheckedChanged
        ep.SetError(optStatic, "")
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1226)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        gTables(0) = cmbTable.Text

        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 7
                Step7.BringToFront()
                Step7.Visible = True
                lblStep.Text = S7
                cmdFinish.Visible = False
                cmdNext.Visible = True
            Case 6
                Step6.BringToFront()
                Step6.Visible = True
                lblStep.Text = S6

            Case 5
                Step5.BringToFront()
                Step5.Visible = True
                lblStep.Text = S5
            Case 4
                Step4.BringToFront()
                Step4.Visible = True
                lblStep.Text = S4
            Case 3
                Step3.BringToFront()
                Step3.Visible = True
                lblStep.Text = S3
            Case 2
                Step2.BringToFront()
                Step2.Visible = True
                lblStep.Text = S2
            Case 1
                Step1.BringToFront()
                Step1.Visible = True
                lblStep.Text = S1
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub



    Private Sub Step7_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Step8.Paint

    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged

        gsCon = UcDSN.cmbDSN.Text & "|" & _
        UcDSN.txtUserID.Text & "|" & _
        UcDSN.txtPassword.Text & "|"

        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            sLink = oForm._AdvancedDynamicLinking(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim WriteSuccess As Boolean
        Dim nID As Integer
        Dim sCols As String
        Dim sVals As String
        Dim oReport As New clsMarsReport
        Dim dynamicTasks As Integer

        If optAll.Checked = True Then
            dynamicTasks = 0
        Else
            dynamicTasks = 1
        End If

        If ucSet.chkNoEnd.Checked = True Then ucSet.EndDate.Value = Now.AddYears(1000)

        oErr.BusyProgress(30, "Saving report data...")

        'save the report 
        cmdFinish.Enabled = False

        nID = clsMarsData.CreateDataID("reportattr", "reportid")

        sCols = "ReportID,PackID,DatabasePath,ReportName," & _
            "Parent,ReportTitle," & _
            "Retry,AssumeFail,CheckBlank," & _
            "SelectionFormula," & _
            "Dynamic,Owner," & _
            "RptUserID,RptPassword,RptServer,RptDatabase,UseLogin,UseSavedData," & _
            "CachePath, LastRefreshed,StaticDest,DynamicTasks,RptDatabaseType," & _
            "AutoCalc,RetryInterval"


        sVals = nID & ",0," & _
            "'" & SQLPrepare(txtUrl.Text) & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            UcErrorHandler1.cmbRetry.Text & "," & _
            UcErrorHandler1.m_autoFailAfter & "," & _
            Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
            "'" & SQLPrepare(txtFormula.Text) & "',1," & _
            "'" & gUser & "'," & _
            "'" & SQLPrepare(ServerUser) & "'," & _
            "'" & SQLPrepare(_EncryptDBValue(ServerPassword)) & "'," & _
            "''," & _
            "''," & _
            "0,0," & _
            "'" & SQLPrepare(txtDBLoc.Text) & "', " & _
            "'" & ConDateTime(Date.Now) & "'," & _
            Convert.ToInt32(chkStaticDest.Checked) & "," & _
            dynamicTasks & "," & _
            "''," & _
            Convert.ToInt32(UcErrorHandler1.chkAutoCalc.Checked) & "," & _
            UcErrorHandler1.txtRetryInterval.Value


        SQL = "INSERT INTO ReportAttr(" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)


        If WriteSuccess = False Then GoTo RollbackTran

        oErr.BusyProgress(50, "Saving destination data...")

        UcDest.CommitDeletions()

        SQL = "UPDATE DestinationAttr SET PackID =0, SmartID = 0, ReportID = " & nID & " WHERE ReportID = 99999"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = False Then GoTo RollbackTran

        oErr.BusyProgress(60, "Saving schedule data...")

        Dim ScheduleID As Integer

        ScheduleID = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        sCols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,ReportID," & _
                "Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit"

        With ucSet
            sVals = ScheduleID & "," & _
                     "'" & .sFrequency & "'," & _
                     "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                     "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                     "'" & ConDate(CTimeZ(.StartDate.Value.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                     "'" & ConTime(CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE)) & "'," & _
                     Convert.ToInt32(.chkRepeat.Checked) & "," & _
                     "'" & Convert.ToString(.cmbRpt.Value).Replace(",", ".") & "'," & _
                     "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                     Convert.ToInt32(.chkStatus.Checked) & "," & _
                     nID & "," & _
                     "'" & SQLPrepare(txtDesc.Text) & "'," & _
                     "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                     "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                     Convert.ToInt32(.chkException.Checked) & "," & _
                     "'" & SQLPrepare(.cmbException.Text) & "'," & _
                     "'" & ucSet.m_RepeatUnit & "'"
        End With

        SQL = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"

        WriteSuccess = clsMarsData.WriteData(SQL)

        If WriteSuccess = False Then GoTo RollbackTran

        oErr.BusyProgress(80, "Saving custom tasks...")

        Me.UcTasks1.CommitDeletions()

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE DynamicAttr SET ReportID = " & nID & ", PackID = 0, " & _
       "AutoResume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
       "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
       " WHERE ReportID = " & 99999

        clsMarsData.WriteData(SQL)

        oErr.BusyProgress(95, "Saving dynamic queries...")

        If cmbValue.Text = "<Advanced>" Then
            SQL = "UPDATE DynamicLink SET ReportID = " & nID & " WHERE ReportID = 99999"
        Else

            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID = 99999")

            sLink = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & _
                " FROM " & cmbTable.Text

            sCols = "LinkID,ReportID,LinkSQL,ConString,KeyColumn,KeyParameter,ValueColumn"

            sVals = clsMarsData.CreateDataID("dynamiclink", "linkid") & "," & _
            nID & "," & _
            "'" & SQLPrepare(sLink) & "'," & _
            "'" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
            "'" & SQLPrepare(cmbKey.Text) & "'," & _
            "'" & SQLPrepare(cmbTable.Text & "." & cmbValue.Text) & "'"

            SQL = "INSERT INTO DynamicLink (" & sCols & ") VALUES (" & sVals & ")"

        End If

        clsMarsData.WriteData(SQL)

        'save parameters
        SQL = "UPDATE ReportParameter SET ReportID =" & nID & " WHERE " & _
           "ReportID = 99999"

        clsMarsData.WriteData(SQL)

        'update subreport parameters
        SQL = "UPDATE SubReportParameters SET ReportID =" & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportLogin SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET ReportID = 0 WHERE ReportID =99999")

        'set report table logins
        SQL = "UPDATE ReportTable SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE SubReportTable SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportDatasource SET ReportID = " & nID & " WHERE ReportID = 99999"

        clsMarsData.WriteData(SQL)

        'save blank report alert information if available
        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String = ""

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            oErr.BusyProgress(95, "Cleaning up...")

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,ReportID,[Subject],CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("BlankReportAlert", "BlankID") & "," & _
            "'" & sBlankType & "'," & _
            "'" & UcBlank.txtAlertTo.Text.Replace("'", "''") & "'," & _
            "'" & UcBlank.txtBlankMsg.Text.Replace("'", "''") & "'," & _
            nID & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewSingle)
        End If

        Close()

        oErr.BusyProgress(, , True)

        oErr.RefreshView(oWindow(nWindowCurrent))

        Try
            Dim nCount As Integer
            nCount = txtFolder.Text.Split("\").GetUpperBound(0)

            Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

            oErr.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))
        Catch

        End Try

        oWindow(nWindowCurrent).lsvWindow.Focus()

        For Each item As ListViewItem In oWindow(nWindowCurrent).lsvWindow.Items
            If item.Text = txtName.Text Then
                item.Selected = True
                item.Focused = True
            Else
                item.Selected = False
                item.Focused = False
            End If
        Next


        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.CREATE)
        Exit Sub

RollbackTran:

        clsMarsData.WriteData("DELETE FROM ReportAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID = " & nID)
        clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID = " & nID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        HasCancelled = True
        oData.CleanDB()
    End Sub


    Private Sub HelpLink_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles HelpLink.LinkClicked
        Dim sKeyMsg As String

        sKeyMsg = "Dim KeyMsg As String"

        sKeyMsg = "In a Dynamic Schedule, you tell SQL-RD to" & vbCrLf & _
            "- Reel through a list of possible values for a 'Key' parameter." & vbCrLf & _
            "- For each value in the list, SQL-RD must generate a unique report using that " & _
            "value as the parameter value, and then email the report to a recipient, " & _
            "or export it to a specified folder." & vbCrLf & _
            "- For email, the recipient email address is read from a database " & _
            "(or can be entered as a static value)" & vbCrLf & _
            "- For folder exports, the destination folder is read from a database " & _
            "(or can be entered as a static value)" & vbCrLf & vbCrLf & _
            "The Key Parameter" & vbCrLf & vbCrLf & _
            "The Key Parameter is used by SQL-RD to determine:" & vbCrLf & _
            "1. The list of parameter values to reel through" & vbCrLf & _
            "2. The corresponding recipient email address or destination output folder, " & _
            "for each value in the above list." & vbCrLf & vbCrLf

        sKeyMsg &= "Example " & vbCrLf & vbCrLf & _
            "I want to send a report to all sales people telling them the number of " & _
            "cars they sold.  Each sales person should get a unique report showing only that " & _
            "individual�s sales. " & _
            "The report should be emailed to that individual only." & vbCrLf & vbCrLf & _
            "In my database:" & vbCrLf & _
            "1. RepNo = Sales person�s id number" & vbCrLf & _
            "2. CarsSold = Number of cars sold" & vbCrLf & _
            "3. RepEmail = Rep�s email address" & vbCrLf & vbCrLf & _
            "In the MS Access report:" & vbCrLf & _
            "1. Create a parameter called {RepNumber}" & vbCrLf & _
            "2. In the Select Expert, set RepNo = {RepNumber}" & vbCrLf & _
            "3. In the report, show the field CarsSold." & vbCrLf & _
            "In the above report, if I manually enter a sales person�s number," & _
            "it will show me the number of cars sold." & vbCrLf & vbCrLf & _
            "The Key Parameter in the above example is {RepNumber} because " & _
            "I will instruct SQL-RD with the list of possible values that SQL-RD " & _
            "will enter into this parameter and generate reports. " & _
            "E.g. select RepNo where state = �Kansas� will give SQL-RD a list of all sales people in Kansas." & _
            "- I will then instruct how, using each value, SQL-RD can determine the email address to send the report to. " & _
            "E.g. select RepEmail where RepNo = {?RepNumber} will give SQL-RD the RepEmail for each RepNo that is in the above list"

        Dim m_AlertOnLoad As AlertCustom = New AlertCustom

        Dim r As Rectangle = Screen.GetWorkingArea(Me)
        m_AlertOnLoad.Location = New Point(r.Right - m_AlertOnLoad.Width, r.Bottom - m_AlertOnLoad.Height)
        m_AlertOnLoad.AutoClose = True
        m_AlertOnLoad.Label1.Text = sKeyMsg
        m_AlertOnLoad.AutoCloseTimeOut = 60
        m_AlertOnLoad.AlertAnimation = eAlertAnimation.BottomToTop
        m_AlertOnLoad.AlertAnimationDuration = 300
        m_AlertOnLoad.Show(False)
    End Sub

    Private Sub cmbDestination_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDestination.SelectedIndexChanged
        If cmbDestination.Text.ToLower = "sms" And gnEdition < gEdition.ENTERPRISEPROPLUS Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            cmbDestination.SelectedIndex = 0
            Return
        End If

        UcDest.DynamicDestination = cmbDestination.Text
    End Sub

    Private Sub optDynamic_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDynamic.CheckedChanged
        ep.SetError(optStatic, "")
    End Sub



    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvParameters.SelectedIndexChanged

    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        'If Not ParValue.StartsWith("'") And Not ParValue.EndsWith("'") Then
        '    Try
        '        Int32.Parse(ParValue)
        '    Catch ex As Exception
        '        ParValue = "'" & SQLPrepare(ParValue) & "'"
        '    End Try
        'End If

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE ReportID= 99999"

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)


                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub

   

    Private Sub lsvParameters_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvParameters.DoubleClick
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch ex As Exception
                    .Item("MultiValue") = "false"
                End Try
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, ParDefaults, , lsvParameters.SelectedItems(0).Text, m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, ServerUser, ServerPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub cmbKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbKey.SelectedIndexChanged
        If lsvParameters.Items.Count = 0 Then Return

        ep.SetError(sender, "")

        If cmbKey.Text.Length > 0 And sTempKey.Length > 0 Then
            MessageBox.Show("You are changing the key parameter. All your parameter values will be reset", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        End If

        sTempKey = cmbKey.Text

        For Each oitem As ListViewItem In lsvParameters.Items
            Try
                oitem.SubItems(1).Text = String.Empty
            Catch : End Try
        Next
    End Sub

    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        grpDynamicDest.Enabled = Not chkStaticDest.Checked
        UcDest.StaticDest = chkStaticDest.Checked
        UcDest.m_CanDisable = chkStaticDest.Checked
    End Sub
    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(99999, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub


    Private Sub txtUrl_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUrl.GotFocus
        If txtUrl.ForeColor = Color.Gray Then
            txtUrl.Text = ""
            txtUrl.ForeColor = Color.Blue
        End If

    End Sub

    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub

    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub


    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLoc.TextChanged
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 200)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            Me.cmdDbLoc.Image = ImageList1.Images(2)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If
    End Sub
    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub

  
End Class
