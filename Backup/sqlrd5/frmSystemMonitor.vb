Imports Microsoft.Win32
Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Public Class frmSystemMonitor
    Inherits System.Windows.Forms.Form
    Dim oData As clsMarsData = New clsMarsData
    Dim oUI As New clsMarsUI
    Dim ep As New ErrorProvider
    Dim oPersist As String
    Dim oSchedule As New clsMarsScheduler
    Dim IsLoaded As Boolean = False
    Dim sortOrder As SortOrder = sortOrder.Ascending
    Dim logsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim sortBy As Integer = 1
    Dim logsortBy As Integer = 1
    Dim errlogsortBy As Integer = 1
    Dim errlogsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim retrylogsortOrder As SortOrder = Windows.Forms.SortOrder.Descending
    Dim retrylogSortBy As Integer = 1
    Dim emailLogRow() As Integer
    Public m_eventID As Integer = 99999
    Dim oTs As ArrayList = New ArrayList
    Dim tcpListener As TcpListener
    Dim gItemList() As ListViewItem
    Dim supportMode As Boolean = False
    Dim gerrItemList() As ListViewItem
    Public standAlone As Boolean = False
    Dim WithEvents progressTable As DataTable
    '''''Dim WithEvents emailLogFilterMan As DgvFilterPopup.DgvFilterManager
    Dim emailLogFilter As Hashtable = New Hashtable

#Region "Friends"
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents CRDSystem As DevComponents.DotNetBar.TabItem
    Friend WithEvents tabCRD As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbManager As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel13 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbEvent As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbMailLog As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbMailQ As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbErrorLog As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbDefDelivery As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSystemBackup As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbRefreshSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel11 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbAuditTrail As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tabHistory As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel14 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbAutomation As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSingleFilter As System.Windows.Forms.CheckBox
    Friend WithEvents grpSingleSort As System.Windows.Forms.GroupBox
    Friend WithEvents optSingleAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optSingleDesc As System.Windows.Forms.RadioButton
    Friend WithEvents txtKeepSingle As System.Windows.Forms.NumericUpDown
    Friend WithEvents tvSingle As System.Windows.Forms.TreeView
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cmdSingleClear As System.Windows.Forms.Button
    Friend WithEvents cmdSingleRefresh As System.Windows.Forms.Button
    Friend WithEvents tbSingle As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel12 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPackageFilter As System.Windows.Forms.CheckBox
    Friend WithEvents cmdPackClear As System.Windows.Forms.Button
    Friend WithEvents cmdPackRefresh As System.Windows.Forms.Button
    Friend WithEvents grpPackageSort As System.Windows.Forms.GroupBox
    Friend WithEvents optPackageAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optPackageDesc As System.Windows.Forms.RadioButton
    Friend WithEvents txtKeepPack As System.Windows.Forms.NumericUpDown
    Friend WithEvents tvPackage As System.Windows.Forms.TreeView
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents tbPackages As DevComponents.DotNetBar.TabItem
    Friend WithEvents ScheduleHistory As DevComponents.DotNetBar.TabItem
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel3 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel6 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel7 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel4 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdEventClear As System.Windows.Forms.Button
    Friend WithEvents cmdEventRefresh As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtKeepEvent As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FlowLayoutPanel2 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents cmdAutoClear As System.Windows.Forms.Button
    Friend WithEvents cmdAutoRefresh As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtAutoKeep As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel5 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel8 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel9 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents FlowLayoutPanel10 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TableLayoutPanel9 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel11 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents TabControlPanel15 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents lsvReadReciepts As System.Windows.Forms.ListView
    Friend WithEvents TableLayoutPanel10 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cmbFilterRR As System.Windows.Forms.ComboBox
    Friend WithEvents ColumnHeader29 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader31 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader32 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader33 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader34 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader35 As System.Windows.Forms.ColumnHeader
    Friend WithEvents imgRR As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader30 As System.Windows.Forms.ColumnHeader
    Friend WithEvents muRR As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuReceived As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuPending As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblScheduler As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel11 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtDeleteOlder As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbValueUnit As System.Windows.Forms.ComboBox
    Friend WithEvents TabControlPanel16 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents tvEPHistory As System.Windows.Forms.TreeView
    Friend WithEvents FlowLayoutPanel12 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnEPClear As System.Windows.Forms.Button
    Friend WithEvents btnEPRefresh As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel12 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtEPKeepHistory As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel13 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEPFilter As System.Windows.Forms.CheckBox
    Friend WithEvents grpEPSort As System.Windows.Forms.GroupBox
    Friend WithEvents optEPAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optEPDesc As System.Windows.Forms.RadioButton
    Friend WithEvents cmnuEPClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuEPClearAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuEPClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents imgEP As System.Windows.Forms.ImageList
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents grpBackup As System.Windows.Forms.GroupBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents grpSimple As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKeep As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtBackup As System.Windows.Forms.DateTimePicker
    Friend WithEvents optDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optYearly As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents grpAdvanced As System.Windows.Forms.GroupBox
    Friend WithEvents txtZipName As System.Windows.Forms.TextBox
    Friend WithEvents UcUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents chkZip As System.Windows.Forms.CheckBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents btnSingleHistory As System.Windows.Forms.Button
    Friend WithEvents btnPackageHistory As System.Windows.Forms.Button
    Friend WithEvents btnAutoHistory As System.Windows.Forms.Button
    Friend WithEvents btnEventHistory As System.Windows.Forms.Button
    Friend WithEvents btnEventPackHistory As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader36 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuClearEmailLog As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteSelectedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmScheduleMonCleaner As System.Timers.Timer
    Friend WithEvents ColumnHeader37 As System.Windows.Forms.ColumnHeader
    Friend WithEvents bgEmailLog As System.ComponentModel.BackgroundWorker
    Friend WithEvents tmEmailLog As System.Timers.Timer
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents tmSlider As System.Windows.Forms.TrackBar
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents txtSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btnResume As System.Windows.Forms.Button
    Friend WithEvents btnStop As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Splitter1 As System.Windows.Forms.Splitter
    Friend WithEvents lsvErrorLog As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader40 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader41 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader42 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txterrLine As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txterrSource As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txterrNo As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txterrSearch As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader43 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader44 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader45 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader46 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader47 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtErrorlog As System.Windows.Forms.TextBox
    Friend WithEvents txterrSuggest As System.Windows.Forms.TextBox
    Friend WithEvents TabControlPanel17 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbScheduleRetries As DevComponents.DotNetBar.TabItem
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents TableLayoutPanel14 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lsvRetry As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader48 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader49 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader50 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnRefreshRetry As System.Windows.Forms.Button
    Friend WithEvents mnuRetryTracker As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnCopy As System.Windows.Forms.Button
    Friend WithEvents chkGroup As System.Windows.Forms.CheckBox
    Friend WithEvents lsvProcessing As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader51 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader52 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader53 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader54 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader55 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader56 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvTaskManager As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader17 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader18 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader19 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader20 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents Splitter2 As System.Windows.Forms.Splitter
    Friend WithEvents mnuProcMan As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuProcProperties As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProcLoc As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuProcEnd As System.Windows.Forms.MenuItem
    Friend WithEvents dgvEmailLog As System.Windows.Forms.DataGridView
    Friend WithEvents btnRefreshEmailLog As System.Windows.Forms.Button
#End Region
    Private m_SortingColumn As ColumnHeader

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lsvLog As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents lsvEmailQueue As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader8 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader9 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader10 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveOne As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveAll As System.Windows.Forms.Button
    Friend WithEvents cmdSendNow As System.Windows.Forms.Button
    Friend WithEvents imgSingle As System.Windows.Forms.ImageList
    Friend WithEvents imgPackage As System.Windows.Forms.ImageList
    Friend WithEvents grpSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tvAutoHistory As System.Windows.Forms.TreeView
    Friend WithEvents optAutoAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optAutoDesc As System.Windows.Forms.RadioButton
    Friend WithEvents mnuTaskMan As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuRemove As System.Windows.Forms.MenuItem
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbFilter As System.Windows.Forms.ComboBox
    Friend WithEvents imgAuto As System.Windows.Forms.ImageList
    Friend WithEvents lsvDefer As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader11 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader12 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader13 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents optDeferAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDeferDesc As System.Windows.Forms.RadioButton
    Friend WithEvents mnuDefer As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuDeferRemove As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDeferDeliver As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cmdSaveBackup As System.Windows.Forms.Button
    Friend WithEvents chkBackups As System.Windows.Forms.CheckBox
    Friend WithEvents tmScheduleMon As System.Timers.Timer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmdAddSchedule As System.Windows.Forms.Button
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents tvObjects As System.Windows.Forms.TreeView
    Friend WithEvents ColumnHeader14 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader15 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader16 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader21 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader22 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtzRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents dtRunTime As System.Windows.Forms.DateTimePicker
    Friend WithEvents optrDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optrWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optrMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optrYearly As System.Windows.Forms.RadioButton
    Friend WithEvents mnuRefresh As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents ColumnHeader24 As System.Windows.Forms.ColumnHeader
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tmEmailQueue As System.Timers.Timer
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkEventFilter As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoFilter As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoRefresh As System.Windows.Forms.CheckBox
    Friend WithEvents cmnuSingleClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuSingleClearAll As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSingleClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents cmnuPackageClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuPackageClear As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPackageClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents cmnuAutoClear As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuAutoClear As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAutoClearSel As System.Windows.Forms.MenuItem
    Friend WithEvents chkOneEmailPerSchedule As System.Windows.Forms.CheckBox
    Friend WithEvents cmdErrorLog As System.Windows.Forms.Button
    Friend WithEvents imgHistory As System.Windows.Forms.ImageList
    Friend WithEvents txtKeepLogs As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExecute As System.Windows.Forms.MenuItem
    Friend WithEvents imgTasks As System.Windows.Forms.ImageList
    Friend WithEvents lsvAudit As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader23 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader25 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader26 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader27 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader28 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cmdRemoveSchedule As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSystemMonitor))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim ListViewGroup7 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Single Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup8 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Package Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup9 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Automation Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup10 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Event-Based Packages", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup11 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Event-Based Schedules", System.Windows.Forms.HorizontalAlignment.Left)
        Dim ListViewGroup12 As System.Windows.Forms.ListViewGroup = New System.Windows.Forms.ListViewGroup("Other Processes", System.Windows.Forms.HorizontalAlignment.Left)
        Me.imgSingle = New System.Windows.Forms.ImageList(Me.components)
        Me.imgPackage = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuTaskMan = New System.Windows.Forms.ContextMenu
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.mnuRemove = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuExecute = New System.Windows.Forms.MenuItem
        Me.muRR = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuReceived = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuPending = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.imgTasks = New System.Windows.Forms.ImageList(Me.components)
        Me.imgHistory = New System.Windows.Forms.ImageList(Me.components)
        Me.imgAuto = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuDefer = New System.Windows.Forms.ContextMenu
        Me.mnuDeferDeliver = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.mnuDeferRemove = New System.Windows.Forms.MenuItem
        Me.tmScheduleMon = New System.Timers.Timer
        Me.mnuRefresh = New System.Windows.Forms.ContextMenu
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.tmEmailQueue = New System.Timers.Timer
        Me.cmnuSingleClear = New System.Windows.Forms.ContextMenu
        Me.mnuSingleClearAll = New System.Windows.Forms.MenuItem
        Me.mnuSingleClearSel = New System.Windows.Forms.MenuItem
        Me.cmnuPackageClear = New System.Windows.Forms.ContextMenu
        Me.mnuPackageClear = New System.Windows.Forms.MenuItem
        Me.mnuPackageClearSel = New System.Windows.Forms.MenuItem
        Me.cmnuAutoClear = New System.Windows.Forms.ContextMenu
        Me.mnuAutoClear = New System.Windows.Forms.MenuItem
        Me.mnuAutoClearSel = New System.Windows.Forms.MenuItem
        Me.imgRR = New System.Windows.Forms.ImageList(Me.components)
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.tabCRD = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.dgvEmailLog = New System.Windows.Forms.DataGridView
        Me.lsvLog = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader36 = New System.Windows.Forms.ColumnHeader
        Me.mnuClearEmailLog = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteSelectedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel
        Me.btnRefreshEmailLog = New System.Windows.Forms.Button
        Me.txtKeepLogs = New System.Windows.Forms.NumericUpDown
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtSearch = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.tbMailLog = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.lsvTaskManager = New System.Windows.Forms.ListView
        Me.ColumnHeader17 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader18 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader19 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader20 = New System.Windows.Forms.ColumnHeader
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.chkGroup = New System.Windows.Forms.CheckBox
        Me.cmbFilter = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.tmSlider = New System.Windows.Forms.TrackBar
        Me.Splitter2 = New System.Windows.Forms.Splitter
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.lsvProcessing = New System.Windows.Forms.ListView
        Me.ColumnHeader51 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader52 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader53 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader54 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader55 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader56 = New System.Windows.Forms.ColumnHeader
        Me.mnuProcMan = New System.Windows.Forms.ContextMenu
        Me.mnuProcProperties = New System.Windows.Forms.MenuItem
        Me.mnuProcLoc = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.mnuProcEnd = New System.Windows.Forms.MenuItem
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.btnResume = New System.Windows.Forms.Button
        Me.btnStop = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.lblScheduler = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.tbManager = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.btnCopy = New System.Windows.Forms.Button
        Me.txterrSuggest = New System.Windows.Forms.TextBox
        Me.txtErrorlog = New System.Windows.Forms.TextBox
        Me.txterrLine = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.txterrSource = New System.Windows.Forms.TextBox
        Me.Label25 = New System.Windows.Forms.Label
        Me.txterrNo = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Splitter1 = New System.Windows.Forms.Splitter
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.lsvErrorLog = New System.Windows.Forms.ListView
        Me.ColumnHeader42 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader40 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader41 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader43 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader44 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader45 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader46 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader47 = New System.Windows.Forms.ColumnHeader
        Me.FlowLayoutPanel8 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdErrorLog = New System.Windows.Forms.Button
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel
        Me.Label27 = New System.Windows.Forms.Label
        Me.txterrSearch = New System.Windows.Forms.TextBox
        Me.tbErrorLog = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel17 = New DevComponents.DotNetBar.TabControlPanel
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.lsvRetry = New System.Windows.Forms.ListView
        Me.ColumnHeader48 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader49 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader50 = New System.Windows.Forms.ColumnHeader
        Me.mnuRetryTracker = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.TableLayoutPanel14 = New System.Windows.Forms.TableLayoutPanel
        Me.btnRefreshRetry = New System.Windows.Forms.Button
        Me.tbScheduleRetries = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.lsvEmailQueue = New System.Windows.Forms.ListView
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader8 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader10 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader9 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader37 = New System.Windows.Forms.ColumnHeader
        Me.TableLayoutPanel11 = New System.Windows.Forms.TableLayoutPanel
        Me.Label17 = New System.Windows.Forms.Label
        Me.txtDeleteOlder = New System.Windows.Forms.NumericUpDown
        Me.cmbValueUnit = New System.Windows.Forms.ComboBox
        Me.FlowLayoutPanel5 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdSendNow = New System.Windows.Forms.Button
        Me.cmdRemoveOne = New System.Windows.Forms.Button
        Me.cmdRemoveAll = New System.Windows.Forms.Button
        Me.chkOneEmailPerSchedule = New System.Windows.Forms.CheckBox
        Me.tbMailQ = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel15 = New DevComponents.DotNetBar.TabControlPanel
        Me.lsvReadReciepts = New System.Windows.Forms.ListView
        Me.ColumnHeader29 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader31 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader32 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader33 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader34 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader35 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader30 = New System.Windows.Forms.ColumnHeader
        Me.TableLayoutPanel10 = New System.Windows.Forms.TableLayoutPanel
        Me.Label15 = New System.Windows.Forms.Label
        Me.cmbFilterRR = New System.Windows.Forms.ComboBox
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel11 = New DevComponents.DotNetBar.TabControlPanel
        Me.lsvAudit = New System.Windows.Forms.ListView
        Me.ColumnHeader23 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader25 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader26 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader27 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader28 = New System.Windows.Forms.ColumnHeader
        Me.tbAuditTrail = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpBackup = New System.Windows.Forms.GroupBox
        Me.grpAdvanced = New System.Windows.Forms.GroupBox
        Me.txtZipName = New System.Windows.Forms.TextBox
        Me.UcUpdate = New sqlrd.ucScheduleUpdate
        Me.Label21 = New System.Windows.Forms.Label
        Me.chkZip = New System.Windows.Forms.CheckBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.grpSimple = New System.Windows.Forms.GroupBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtInterval = New System.Windows.Forms.NumericUpDown
        Me.Label3 = New System.Windows.Forms.Label
        Me.dtBackup = New System.Windows.Forms.DateTimePicker
        Me.optDaily = New System.Windows.Forms.RadioButton
        Me.optWeekly = New System.Windows.Forms.RadioButton
        Me.optMonthly = New System.Windows.Forms.RadioButton
        Me.optYearly = New System.Windows.Forms.RadioButton
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtKeep = New System.Windows.Forms.NumericUpDown
        Me.rbAdvanced = New System.Windows.Forms.RadioButton
        Me.rbSimple = New System.Windows.Forms.RadioButton
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.cmdSaveBackup = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.chkBackups = New System.Windows.Forms.CheckBox
        Me.tbSystemBackup = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader14 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader15 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader16 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader21 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader22 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader24 = New System.Windows.Forms.ColumnHeader
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel
        Me.cmdRemoveSchedule = New System.Windows.Forms.Button
        Me.cmdAddSchedule = New System.Windows.Forms.Button
        Me.FlowLayoutPanel11 = New System.Windows.Forms.FlowLayoutPanel
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.tvObjects = New System.Windows.Forms.TreeView
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.txtzRpt = New System.Windows.Forms.NumericUpDown
        Me.Label10 = New System.Windows.Forms.Label
        Me.dtRunTime = New System.Windows.Forms.DateTimePicker
        Me.optrDaily = New System.Windows.Forms.RadioButton
        Me.optrWeekly = New System.Windows.Forms.RadioButton
        Me.optrMonthly = New System.Windows.Forms.RadioButton
        Me.optrYearly = New System.Windows.Forms.RadioButton
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.FlowLayoutPanel10 = New System.Windows.Forms.FlowLayoutPanel
        Me.Label7 = New System.Windows.Forms.Label
        Me.chkAutoRefresh = New System.Windows.Forms.CheckBox
        Me.tbRefreshSchedule = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel
        Me.lsvDefer = New System.Windows.Forms.ListView
        Me.ColumnHeader11 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader12 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader13 = New System.Windows.Forms.ColumnHeader
        Me.FlowLayoutPanel9 = New System.Windows.Forms.FlowLayoutPanel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.optDeferAsc = New System.Windows.Forms.RadioButton
        Me.optDeferDesc = New System.Windows.Forms.RadioButton
        Me.tbDefDelivery = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.CRDSystem = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.tabHistory = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvSingle = New System.Windows.Forms.TreeView
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdSingleClear = New System.Windows.Forms.Button
        Me.cmdSingleRefresh = New System.Windows.Forms.Button
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel
        Me.txtKeepSingle = New System.Windows.Forms.NumericUpDown
        Me.Label13 = New System.Windows.Forms.Label
        Me.btnSingleHistory = New System.Windows.Forms.Button
        Me.FlowLayoutPanel6 = New System.Windows.Forms.FlowLayoutPanel
        Me.grpSingleSort = New System.Windows.Forms.GroupBox
        Me.optSingleAsc = New System.Windows.Forms.RadioButton
        Me.optSingleDesc = New System.Windows.Forms.RadioButton
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.chkSingleFilter = New System.Windows.Forms.CheckBox
        Me.tbSingle = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel13 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdEventClear = New System.Windows.Forms.Button
        Me.cmdEventRefresh = New System.Windows.Forms.Button
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.txtKeepEvent = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnEventHistory = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.chkEventFilter = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.optAutoAsc = New System.Windows.Forms.RadioButton
        Me.optAutoDesc = New System.Windows.Forms.RadioButton
        Me.tbEvent = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel16 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvEPHistory = New System.Windows.Forms.TreeView
        Me.imgEP = New System.Windows.Forms.ImageList(Me.components)
        Me.FlowLayoutPanel12 = New System.Windows.Forms.FlowLayoutPanel
        Me.btnEPClear = New System.Windows.Forms.Button
        Me.btnEPRefresh = New System.Windows.Forms.Button
        Me.TableLayoutPanel12 = New System.Windows.Forms.TableLayoutPanel
        Me.txtEPKeepHistory = New System.Windows.Forms.NumericUpDown
        Me.Label18 = New System.Windows.Forms.Label
        Me.btnEventPackHistory = New System.Windows.Forms.Button
        Me.TableLayoutPanel13 = New System.Windows.Forms.TableLayoutPanel
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.chkEPFilter = New System.Windows.Forms.CheckBox
        Me.grpEPSort = New System.Windows.Forms.GroupBox
        Me.optEPAsc = New System.Windows.Forms.RadioButton
        Me.optEPDesc = New System.Windows.Forms.RadioButton
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel14 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvAutoHistory = New System.Windows.Forms.TreeView
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdAutoClear = New System.Windows.Forms.Button
        Me.cmdAutoRefresh = New System.Windows.Forms.Button
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel
        Me.txtAutoKeep = New System.Windows.Forms.NumericUpDown
        Me.Label14 = New System.Windows.Forms.Label
        Me.btnAutoHistory = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.chkAutoFilter = New System.Windows.Forms.CheckBox
        Me.grpSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.tbAutomation = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel12 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvPackage = New System.Windows.Forms.TreeView
        Me.FlowLayoutPanel7 = New System.Windows.Forms.FlowLayoutPanel
        Me.cmdPackClear = New System.Windows.Forms.Button
        Me.cmdPackRefresh = New System.Windows.Forms.Button
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.txtKeepPack = New System.Windows.Forms.NumericUpDown
        Me.Label16 = New System.Windows.Forms.Label
        Me.btnPackageHistory = New System.Windows.Forms.Button
        Me.FlowLayoutPanel4 = New System.Windows.Forms.FlowLayoutPanel
        Me.grpPackageSort = New System.Windows.Forms.GroupBox
        Me.optPackageAsc = New System.Windows.Forms.RadioButton
        Me.optPackageDesc = New System.Windows.Forms.RadioButton
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.chkPackageFilter = New System.Windows.Forms.CheckBox
        Me.tbPackages = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ScheduleHistory = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.cmnuEPClear = New System.Windows.Forms.ContextMenu
        Me.mnuEPClearAll = New System.Windows.Forms.MenuItem
        Me.mnuEPClearSel = New System.Windows.Forms.MenuItem
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.tmScheduleMonCleaner = New System.Timers.Timer
        Me.bgEmailLog = New System.ComponentModel.BackgroundWorker
        Me.tmEmailLog = New System.Timers.Timer
        Me.muRR.SuspendLayout()
        CType(Me.tmScheduleMon, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEmailQueue, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        CType(Me.tabCRD, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCRD.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        CType(Me.dgvEmailLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuClearEmailLog.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        CType(Me.txtKeepLogs, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel3.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.tmSlider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox14.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel7.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.FlowLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TabControlPanel17.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.mnuRetryTracker.SuspendLayout()
        Me.TableLayoutPanel14.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.TableLayoutPanel11.SuspendLayout()
        CType(Me.txtDeleteOlder, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel5.SuspendLayout()
        Me.TabControlPanel15.SuspendLayout()
        Me.TableLayoutPanel10.SuspendLayout()
        Me.TabControlPanel11.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        Me.grpBackup.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        Me.grpSimple.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.txtInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtKeep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel10.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        Me.FlowLayoutPanel11.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.txtzRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel10.SuspendLayout()
        Me.TabControlPanel8.SuspendLayout()
        Me.FlowLayoutPanel9.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        CType(Me.tabHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabHistory.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.FlowLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel6.SuspendLayout()
        CType(Me.txtKeepSingle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel6.SuspendLayout()
        Me.grpSingleSort.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.TabControlPanel13.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        CType(Me.txtKeepEvent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControlPanel16.SuspendLayout()
        Me.FlowLayoutPanel12.SuspendLayout()
        Me.TableLayoutPanel12.SuspendLayout()
        CType(Me.txtEPKeepHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel13.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.grpEPSort.SuspendLayout()
        Me.TabControlPanel14.SuspendLayout()
        Me.FlowLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        CType(Me.txtAutoKeep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.grpSort.SuspendLayout()
        Me.TabControlPanel12.SuspendLayout()
        Me.FlowLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.txtKeepPack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FlowLayoutPanel4.SuspendLayout()
        Me.grpPackageSort.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        CType(Me.tmScheduleMonCleaner, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tmEmailLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgSingle
        '
        Me.imgSingle.ImageStream = CType(resources.GetObject("imgSingle.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgSingle.TransparentColor = System.Drawing.Color.Transparent
        Me.imgSingle.Images.SetKeyName(0, "document_chart.png")
        Me.imgSingle.Images.SetKeyName(1, "calendar.png")
        Me.imgSingle.Images.SetKeyName(2, "check2.png")
        Me.imgSingle.Images.SetKeyName(3, "delete2.png")
        Me.imgSingle.Images.SetKeyName(4, "about.png")
        Me.imgSingle.Images.SetKeyName(5, "date-time.png")
        '
        'imgPackage
        '
        Me.imgPackage.ImageStream = CType(resources.GetObject("imgPackage.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgPackage.TransparentColor = System.Drawing.Color.Transparent
        Me.imgPackage.Images.SetKeyName(0, "box_closed.png")
        Me.imgPackage.Images.SetKeyName(1, "calendar.png")
        Me.imgPackage.Images.SetKeyName(2, "check2.png")
        Me.imgPackage.Images.SetKeyName(3, "delete2.png")
        Me.imgPackage.Images.SetKeyName(4, "about.png")
        Me.imgPackage.Images.SetKeyName(5, "date-time.png")
        '
        'mnuTaskMan
        '
        Me.mnuTaskMan.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem11, Me.MenuItem12, Me.MenuItem13, Me.mnuRemove, Me.MenuItem3, Me.mnuExecute})
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 0
        Me.MenuItem11.Tag = "MenuItem11"
        Me.MenuItem11.Text = "Properties"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 1
        Me.MenuItem12.Tag = "MenuItem12"
        Me.MenuItem12.Text = "Show in Explorer"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 2
        Me.MenuItem13.Text = "-"
        '
        'mnuRemove
        '
        Me.mnuRemove.Index = 3
        Me.mnuRemove.Tag = "mnuRemove"
        Me.mnuRemove.Text = "Remove From Queue"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 4
        Me.MenuItem3.Text = "-"
        '
        'mnuExecute
        '
        Me.mnuExecute.Index = 5
        Me.mnuExecute.Text = "Execute Schedule"
        '
        'muRR
        '
        Me.muRR.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuReceived, Me.ToolStripSeparator1, Me.mnuPending, Me.ToolStripSeparator2, Me.mnuDelete})
        Me.muRR.Name = "muRR"
        Me.muRR.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.muRR.Size = New System.Drawing.Size(172, 82)
        '
        'mnuReceived
        '
        Me.mnuReceived.Name = "mnuReceived"
        Me.mnuReceived.Size = New System.Drawing.Size(171, 22)
        Me.mnuReceived.Text = "Mark as 'Received'"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(168, 6)
        '
        'mnuPending
        '
        Me.mnuPending.Name = "mnuPending"
        Me.mnuPending.Size = New System.Drawing.Size(171, 22)
        Me.mnuPending.Text = "Mark as 'Pending'"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(168, 6)
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(171, 22)
        Me.mnuDelete.Text = "Delete"
        '
        'imgTasks
        '
        Me.imgTasks.ImageStream = CType(resources.GetObject("imgTasks.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTasks.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTasks.Images.SetKeyName(0, "")
        Me.imgTasks.Images.SetKeyName(1, "")
        Me.imgTasks.Images.SetKeyName(2, "document_gear.png")
        Me.imgTasks.Images.SetKeyName(3, "box_new.png")
        Me.imgTasks.Images.SetKeyName(4, "document_attachment.png")
        Me.imgTasks.Images.SetKeyName(5, "document_pulse.png")
        Me.imgTasks.Images.SetKeyName(6, "cube_molecule.png")
        Me.imgTasks.Images.SetKeyName(7, "atom.png")
        Me.imgTasks.Images.SetKeyName(8, "server_into.png")
        Me.imgTasks.Images.SetKeyName(9, "data driven box.png")
        Me.imgTasks.Images.SetKeyName(10, "document_flash.png")
        Me.imgTasks.Images.SetKeyName(11, "gear_refresh.png")
        '
        'imgHistory
        '
        Me.imgHistory.ImageStream = CType(resources.GetObject("imgHistory.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgHistory.TransparentColor = System.Drawing.Color.Transparent
        Me.imgHistory.Images.SetKeyName(0, "document_atoms.png")
        Me.imgHistory.Images.SetKeyName(1, "")
        Me.imgHistory.Images.SetKeyName(2, "")
        Me.imgHistory.Images.SetKeyName(3, "")
        Me.imgHistory.Images.SetKeyName(4, "mail_forward.png")
        '
        'imgAuto
        '
        Me.imgAuto.ImageStream = CType(resources.GetObject("imgAuto.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgAuto.TransparentColor = System.Drawing.Color.Transparent
        Me.imgAuto.Images.SetKeyName(0, "document_gear.png")
        Me.imgAuto.Images.SetKeyName(1, "")
        Me.imgAuto.Images.SetKeyName(2, "")
        Me.imgAuto.Images.SetKeyName(3, "")
        Me.imgAuto.Images.SetKeyName(4, "")
        Me.imgAuto.Images.SetKeyName(5, "")
        '
        'mnuDefer
        '
        Me.mnuDefer.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuDeferDeliver, Me.MenuItem1, Me.mnuDeferRemove})
        '
        'mnuDeferDeliver
        '
        Me.mnuDeferDeliver.Index = 0
        Me.mnuDeferDeliver.Text = "Deliver Now"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 1
        Me.MenuItem1.Text = "-"
        '
        'mnuDeferRemove
        '
        Me.mnuDeferRemove.Index = 2
        Me.mnuDeferRemove.Text = "Remove"
        '
        'tmScheduleMon
        '
        Me.tmScheduleMon.Interval = 2000
        Me.tmScheduleMon.SynchronizingObject = Me
        '
        'mnuRefresh
        '
        Me.mnuRefresh.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Delete"
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "document_gear.png")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "cube_molecule.png")
        '
        'tmEmailQueue
        '
        Me.tmEmailQueue.Interval = 10000
        Me.tmEmailQueue.SynchronizingObject = Me
        '
        'cmnuSingleClear
        '
        Me.cmnuSingleClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuSingleClearAll, Me.mnuSingleClearSel})
        '
        'mnuSingleClearAll
        '
        Me.mnuSingleClearAll.Index = 0
        Me.mnuSingleClearAll.Text = "Clear All"
        '
        'mnuSingleClearSel
        '
        Me.mnuSingleClearSel.Index = 1
        Me.mnuSingleClearSel.Text = "Clear Selected"
        '
        'cmnuPackageClear
        '
        Me.cmnuPackageClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuPackageClear, Me.mnuPackageClearSel})
        '
        'mnuPackageClear
        '
        Me.mnuPackageClear.Index = 0
        Me.mnuPackageClear.Text = "Clear All"
        '
        'mnuPackageClearSel
        '
        Me.mnuPackageClearSel.Index = 1
        Me.mnuPackageClearSel.Text = "Clear Selected"
        '
        'cmnuAutoClear
        '
        Me.cmnuAutoClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuAutoClear, Me.mnuAutoClearSel})
        '
        'mnuAutoClear
        '
        Me.mnuAutoClear.Index = 0
        Me.mnuAutoClear.Text = "Clear All"
        '
        'mnuAutoClearSel
        '
        Me.mnuAutoClearSel.Index = 1
        Me.mnuAutoClearSel.Text = "Clear Selected"
        '
        'imgRR
        '
        Me.imgRR.ImageStream = CType(resources.GetObject("imgRR.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgRR.TransparentColor = System.Drawing.Color.Transparent
        Me.imgRR.Images.SetKeyName(0, "document_chart.png")
        Me.imgRR.Images.SetKeyName(1, "box.png")
        '
        'TabControl1
        '
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.Controls.Add(Me.TabControlPanel1)
        Me.TabControl1.Controls.Add(Me.TabControlPanel2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.SelectedTabIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1150, 688)
        Me.TabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Document
        Me.TabControl1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.TabControl1.TabIndex = 1
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl1.Tabs.Add(Me.CRDSystem)
        Me.TabControl1.Tabs.Add(Me.ScheduleHistory)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.tabCRD)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(26, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(1124, 688)
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.CRDSystem
        '
        'tabCRD
        '
        Me.tabCRD.CanReorderTabs = True
        Me.tabCRD.Controls.Add(Me.TabControlPanel3)
        Me.tabCRD.Controls.Add(Me.TabControlPanel5)
        Me.tabCRD.Controls.Add(Me.TabControlPanel7)
        Me.tabCRD.Controls.Add(Me.TabControlPanel17)
        Me.tabCRD.Controls.Add(Me.TabControlPanel6)
        Me.tabCRD.Controls.Add(Me.TabControlPanel15)
        Me.tabCRD.Controls.Add(Me.TabControlPanel11)
        Me.tabCRD.Controls.Add(Me.TabControlPanel9)
        Me.tabCRD.Controls.Add(Me.TabControlPanel10)
        Me.tabCRD.Controls.Add(Me.TabControlPanel8)
        Me.tabCRD.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabCRD.Location = New System.Drawing.Point(1, 1)
        Me.tabCRD.Name = "tabCRD"
        Me.tabCRD.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabCRD.SelectedTabIndex = 0
        Me.tabCRD.Size = New System.Drawing.Size(1122, 686)
        Me.tabCRD.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabCRD.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabCRD.TabIndex = 0
        Me.tabCRD.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabCRD.Tabs.Add(Me.tbManager)
        Me.tabCRD.Tabs.Add(Me.tbMailLog)
        Me.tabCRD.Tabs.Add(Me.tbMailQ)
        Me.tabCRD.Tabs.Add(Me.tbErrorLog)
        Me.tabCRD.Tabs.Add(Me.tbDefDelivery)
        Me.tabCRD.Tabs.Add(Me.tbSystemBackup)
        Me.tabCRD.Tabs.Add(Me.tbRefreshSchedule)
        Me.tabCRD.Tabs.Add(Me.tbAuditTrail)
        Me.tabCRD.Tabs.Add(Me.TabItem1)
        Me.tabCRD.Tabs.Add(Me.tbScheduleRetries)
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.dgvEmailLog)
        Me.TabControlPanel5.Controls.Add(Me.lsvLog)
        Me.TabControlPanel5.Controls.Add(Me.TableLayoutPanel8)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 2
        Me.TabControlPanel5.TabItem = Me.tbMailLog
        '
        'dgvEmailLog
        '
        Me.dgvEmailLog.AllowUserToAddRows = False
        Me.dgvEmailLog.AllowUserToDeleteRows = False
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.AliceBlue
        Me.dgvEmailLog.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.dgvEmailLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells
        Me.dgvEmailLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmailLog.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvEmailLog.Location = New System.Drawing.Point(250, 319)
        Me.dgvEmailLog.Name = "dgvEmailLog"
        Me.dgvEmailLog.ReadOnly = True
        Me.dgvEmailLog.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEmailLog.Size = New System.Drawing.Size(240, 150)
        Me.dgvEmailLog.TabIndex = 23
        Me.dgvEmailLog.VirtualMode = True
        Me.dgvEmailLog.Visible = False
        '
        'lsvLog
        '
        Me.lsvLog.BackColor = System.Drawing.Color.White
        Me.lsvLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader36})
        Me.lsvLog.ContextMenuStrip = Me.mnuClearEmailLog
        Me.lsvLog.FullRowSelect = True
        Me.lsvLog.GridLines = True
        Me.lsvLog.HideSelection = False
        Me.lsvLog.LargeImageList = Me.imgHistory
        Me.lsvLog.Location = New System.Drawing.Point(250, 70)
        Me.lsvLog.Name = "lsvLog"
        Me.lsvLog.Size = New System.Drawing.Size(322, 213)
        Me.lsvLog.SmallImageList = Me.imgHistory
        Me.lsvLog.TabIndex = 0
        Me.lsvLog.UseCompatibleStateImageBehavior = False
        Me.lsvLog.View = System.Windows.Forms.View.Details
        Me.lsvLog.Visible = False
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 99
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Sent To"
        Me.ColumnHeader2.Width = 85
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Subject"
        Me.ColumnHeader3.Width = 172
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Date Sent"
        Me.ColumnHeader4.Width = 110
        '
        'ColumnHeader36
        '
        Me.ColumnHeader36.Text = "File Name"
        Me.ColumnHeader36.Width = 218
        '
        'mnuClearEmailLog
        '
        Me.mnuClearEmailLog.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OpenToolStripMenuItem, Me.DeleteSelectedToolStripMenuItem, Me.DeleteAllToolStripMenuItem})
        Me.mnuClearEmailLog.Name = "mnuClearEmailLog"
        Me.mnuClearEmailLog.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuClearEmailLog.Size = New System.Drawing.Size(155, 70)
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.OpenToolStripMenuItem.Text = "&Open"
        Me.OpenToolStripMenuItem.Visible = False
        '
        'DeleteSelectedToolStripMenuItem
        '
        Me.DeleteSelectedToolStripMenuItem.Name = "DeleteSelectedToolStripMenuItem"
        Me.DeleteSelectedToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.DeleteSelectedToolStripMenuItem.Text = "Delete &Selected"
        '
        'DeleteAllToolStripMenuItem
        '
        Me.DeleteAllToolStripMenuItem.Name = "DeleteAllToolStripMenuItem"
        Me.DeleteAllToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.DeleteAllToolStripMenuItem.Text = "Delete &All"
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel8.ColumnCount = 5
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 520.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.btnRefreshEmailLog, 4, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.txtKeepLogs, 1, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Label20, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.txtSearch, 3, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.Label23, 2, 0)
        Me.TableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 1
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(989, 25)
        Me.TableLayoutPanel8.TabIndex = 22
        '
        'btnRefreshEmailLog
        '
        Me.btnRefreshEmailLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRefreshEmailLog.Image = CType(resources.GetObject("btnRefreshEmailLog.Image"), System.Drawing.Image)
        Me.btnRefreshEmailLog.Location = New System.Drawing.Point(472, 3)
        Me.btnRefreshEmailLog.Name = "btnRefreshEmailLog"
        Me.btnRefreshEmailLog.Size = New System.Drawing.Size(43, 23)
        Me.btnRefreshEmailLog.TabIndex = 3
        Me.btnRefreshEmailLog.UseVisualStyleBackColor = True
        Me.btnRefreshEmailLog.Visible = False
        '
        'txtKeepLogs
        '
        Me.txtKeepLogs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepLogs.Location = New System.Drawing.Point(134, 3)
        Me.txtKeepLogs.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepLogs.Name = "txtKeepLogs"
        Me.txtKeepLogs.Size = New System.Drawing.Size(56, 21)
        Me.txtKeepLogs.TabIndex = 18
        Me.txtKeepLogs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label20
        '
        Me.Label20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label20.BackColor = System.Drawing.Color.Transparent
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(3, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(125, 29)
        Me.Label20.TabIndex = 17
        Me.Label20.Text = "Keep history for (days)"
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtSearch
        '
        Me.txtSearch.Location = New System.Drawing.Point(261, 3)
        Me.txtSearch.Name = "txtSearch"
        Me.txtSearch.Size = New System.Drawing.Size(205, 21)
        Me.txtSearch.TabIndex = 19
        '
        'Label23
        '
        Me.Label23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.Image = Global.sqlrd.My.Resources.Resources.view
        Me.Label23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label23.Location = New System.Drawing.Point(196, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(59, 29)
        Me.Label23.TabIndex = 20
        Me.Label23.Text = "Search"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbMailLog
        '
        Me.tbMailLog.AttachedControl = Me.TabControlPanel5
        Me.tbMailLog.Image = CType(resources.GetObject("tbMailLog.Image"), System.Drawing.Image)
        Me.tbMailLog.Name = "tbMailLog"
        Me.tbMailLog.Text = "Email Log"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.GroupBox13)
        Me.TabControlPanel3.Controls.Add(Me.Splitter2)
        Me.TabControlPanel3.Controls.Add(Me.GroupBox14)
        Me.TabControlPanel3.Controls.Add(Me.Panel3)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 1
        Me.TabControlPanel3.TabItem = Me.tbManager
        '
        'GroupBox13
        '
        Me.GroupBox13.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox13.Controls.Add(Me.lsvTaskManager)
        Me.GroupBox13.Controls.Add(Me.Panel2)
        Me.GroupBox13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox13.Location = New System.Drawing.Point(1, 172)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(989, 410)
        Me.GroupBox13.TabIndex = 7
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Waiting in Queue"
        '
        'lsvTaskManager
        '
        Me.lsvTaskManager.BackColor = System.Drawing.Color.White
        Me.lsvTaskManager.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader17, Me.ColumnHeader18, Me.ColumnHeader19, Me.ColumnHeader20})
        Me.lsvTaskManager.ContextMenu = Me.mnuTaskMan
        Me.lsvTaskManager.ContextMenuStrip = Me.muRR
        Me.lsvTaskManager.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvTaskManager.FullRowSelect = True
        Me.lsvTaskManager.GridLines = True
        ListViewGroup7.Header = "Single Schedules"
        ListViewGroup7.Name = "Single Schedules"
        ListViewGroup8.Header = "Package Schedules"
        ListViewGroup8.Name = "Package Schedules"
        ListViewGroup9.Header = "Automation Schedules"
        ListViewGroup9.Name = "Automation Schedules"
        ListViewGroup10.Header = "Event-Based Packages"
        ListViewGroup10.Name = "Event-Based Packages"
        ListViewGroup11.Header = "Event-Based Schedules"
        ListViewGroup11.Name = "Event-Based Schedules"
        ListViewGroup12.Header = "Other Processes"
        ListViewGroup12.Name = "Other Processes"
        Me.lsvTaskManager.Groups.AddRange(New System.Windows.Forms.ListViewGroup() {ListViewGroup7, ListViewGroup8, ListViewGroup9, ListViewGroup10, ListViewGroup11, ListViewGroup12})
        Me.lsvTaskManager.HideSelection = False
        Me.lsvTaskManager.LargeImageList = Me.imgTasks
        Me.lsvTaskManager.Location = New System.Drawing.Point(3, 99)
        Me.lsvTaskManager.MultiSelect = False
        Me.lsvTaskManager.Name = "lsvTaskManager"
        Me.lsvTaskManager.Size = New System.Drawing.Size(983, 308)
        Me.lsvTaskManager.SmallImageList = Me.imgTasks
        Me.lsvTaskManager.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.lsvTaskManager, "Press F7 to pause/begin auto-refreshing")
        Me.lsvTaskManager.UseCompatibleStateImageBehavior = False
        Me.lsvTaskManager.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader17
        '
        Me.ColumnHeader17.Text = "Schedule Name"
        Me.ColumnHeader17.Width = 120
        '
        'ColumnHeader18
        '
        Me.ColumnHeader18.Text = "Due Date Time"
        Me.ColumnHeader18.Width = 132
        '
        'ColumnHeader19
        '
        Me.ColumnHeader19.Text = "PC Name"
        Me.ColumnHeader19.Width = 139
        '
        'ColumnHeader20
        '
        Me.ColumnHeader20.Text = "Status"
        Me.ColumnHeader20.Width = 147
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.chkGroup)
        Me.Panel2.Controls.Add(Me.cmbFilter)
        Me.Panel2.Controls.Add(Me.Label22)
        Me.Panel2.Controls.Add(Me.Label2)
        Me.Panel2.Controls.Add(Me.tmSlider)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 17)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(983, 82)
        Me.Panel2.TabIndex = 4
        '
        'chkGroup
        '
        Me.chkGroup.AutoSize = True
        Me.chkGroup.Location = New System.Drawing.Point(11, 58)
        Me.chkGroup.Name = "chkGroup"
        Me.chkGroup.Size = New System.Drawing.Size(143, 17)
        Me.chkGroup.TabIndex = 6
        Me.chkGroup.Text = "Group By Schedule Type"
        Me.chkGroup.UseVisualStyleBackColor = True
        '
        'cmbFilter
        '
        Me.cmbFilter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFilter.ItemHeight = 13
        Me.cmbFilter.Items.AddRange(New Object() {"None", "Due Today - All", "Due Today - Next 5 Minutes", "Due Today - Next 30 Minutes", "Due Today - Next 60 Minutes", "Due in Next 24 Hours"})
        Me.cmbFilter.Location = New System.Drawing.Point(11, 27)
        Me.cmbFilter.Name = "cmbFilter"
        Me.cmbFilter.Size = New System.Drawing.Size(256, 21)
        Me.cmbFilter.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(333, 7)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(100, 16)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Refresh Interval"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(9, 11)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Filter"
        '
        'tmSlider
        '
        Me.tmSlider.BackColor = System.Drawing.Color.White
        Me.tmSlider.Location = New System.Drawing.Point(333, 21)
        Me.tmSlider.Maximum = 60
        Me.tmSlider.Minimum = 2
        Me.tmSlider.Name = "tmSlider"
        Me.tmSlider.Size = New System.Drawing.Size(357, 45)
        Me.tmSlider.TabIndex = 4
        Me.tmSlider.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        Me.tmSlider.Value = 2
        '
        'Splitter2
        '
        Me.Splitter2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Splitter2.Location = New System.Drawing.Point(1, 169)
        Me.Splitter2.Name = "Splitter2"
        Me.Splitter2.Size = New System.Drawing.Size(989, 3)
        Me.Splitter2.TabIndex = 9
        Me.Splitter2.TabStop = False
        '
        'GroupBox14
        '
        Me.GroupBox14.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox14.Controls.Add(Me.lsvProcessing)
        Me.GroupBox14.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox14.Location = New System.Drawing.Point(1, 1)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(989, 168)
        Me.GroupBox14.TabIndex = 8
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "Executing"
        '
        'lsvProcessing
        '
        Me.lsvProcessing.BackColor = System.Drawing.Color.White
        Me.lsvProcessing.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader51, Me.ColumnHeader52, Me.ColumnHeader53, Me.ColumnHeader54, Me.ColumnHeader55, Me.ColumnHeader56})
        Me.lsvProcessing.ContextMenu = Me.mnuProcMan
        Me.lsvProcessing.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvProcessing.FullRowSelect = True
        Me.lsvProcessing.GridLines = True
        Me.lsvProcessing.HideSelection = False
        Me.lsvProcessing.LargeImageList = Me.imgTasks
        Me.lsvProcessing.Location = New System.Drawing.Point(3, 17)
        Me.lsvProcessing.MultiSelect = False
        Me.lsvProcessing.Name = "lsvProcessing"
        Me.lsvProcessing.Size = New System.Drawing.Size(983, 148)
        Me.lsvProcessing.SmallImageList = Me.imgTasks
        Me.lsvProcessing.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.lsvProcessing, "Press F7 to pause/begin auto-refreshing")
        Me.lsvProcessing.UseCompatibleStateImageBehavior = False
        Me.lsvProcessing.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader51
        '
        Me.ColumnHeader51.Text = "Schedule Name"
        Me.ColumnHeader51.Width = 120
        '
        'ColumnHeader52
        '
        Me.ColumnHeader52.Text = "Start Date Time"
        Me.ColumnHeader52.Width = 132
        '
        'ColumnHeader53
        '
        Me.ColumnHeader53.Text = "PC Name"
        Me.ColumnHeader53.Width = 139
        '
        'ColumnHeader54
        '
        Me.ColumnHeader54.Text = "Status"
        Me.ColumnHeader54.Width = 147
        '
        'ColumnHeader55
        '
        Me.ColumnHeader55.Text = "Process ID"
        Me.ColumnHeader55.Width = 79
        '
        'ColumnHeader56
        '
        Me.ColumnHeader56.Text = "Duration"
        '
        'mnuProcMan
        '
        Me.mnuProcMan.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuProcProperties, Me.mnuProcLoc, Me.MenuItem16, Me.mnuProcEnd})
        '
        'mnuProcProperties
        '
        Me.mnuProcProperties.Index = 0
        Me.mnuProcProperties.Text = "Properties"
        '
        'mnuProcLoc
        '
        Me.mnuProcLoc.Index = 1
        Me.mnuProcLoc.Text = "Show in Explorer"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 2
        Me.MenuItem16.Text = "-"
        '
        'mnuProcEnd
        '
        Me.mnuProcEnd.Index = 3
        Me.mnuProcEnd.Text = "Cancel Execution"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.Yellow
        Me.Panel3.Controls.Add(Me.GroupBox12)
        Me.Panel3.Controls.Add(Me.GroupBox6)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(1, 582)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(989, 103)
        Me.Panel3.TabIndex = 5
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.SystemColors.Info
        Me.GroupBox12.Controls.Add(Me.btnResume)
        Me.GroupBox12.Controls.Add(Me.btnStop)
        Me.GroupBox12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox12.Location = New System.Drawing.Point(449, 0)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(540, 103)
        Me.GroupBox12.TabIndex = 6
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Scheduler Control"
        '
        'btnResume
        '
        Me.btnResume.Image = CType(resources.GetObject("btnResume.Image"), System.Drawing.Image)
        Me.btnResume.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnResume.Location = New System.Drawing.Point(89, 20)
        Me.btnResume.Name = "btnResume"
        Me.btnResume.Size = New System.Drawing.Size(75, 23)
        Me.btnResume.TabIndex = 3
        Me.btnResume.Text = "Start"
        Me.btnResume.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.Image = CType(resources.GetObject("btnStop.Image"), System.Drawing.Image)
        Me.btnStop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnStop.Location = New System.Drawing.Point(8, 20)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(75, 23)
        Me.btnStop.TabIndex = 1
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.SystemColors.Info
        Me.GroupBox6.Controls.Add(Me.lblScheduler)
        Me.GroupBox6.Controls.Add(Me.PictureBox1)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox6.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(449, 103)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Scheduler Status"
        '
        'lblScheduler
        '
        Me.lblScheduler.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblScheduler.Location = New System.Drawing.Point(62, 17)
        Me.lblScheduler.Name = "lblScheduler"
        Me.lblScheduler.Size = New System.Drawing.Size(384, 83)
        Me.lblScheduler.TabIndex = 0
        Me.lblScheduler.Text = "Scheduler Status"
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(3, 17)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(59, 83)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'tbManager
        '
        Me.tbManager.AttachedControl = Me.TabControlPanel3
        Me.tbManager.Image = CType(resources.GetObject("tbManager.Image"), System.Drawing.Image)
        Me.tbManager.Name = "tbManager"
        Me.tbManager.Text = "Schedule Manager"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.Panel5)
        Me.TabControlPanel7.Controls.Add(Me.Splitter1)
        Me.TabControlPanel7.Controls.Add(Me.Panel4)
        Me.TabControlPanel7.Controls.Add(Me.FlowLayoutPanel8)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.TabIndex = 4
        Me.TabControlPanel7.TabItem = Me.tbErrorLog
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.Transparent
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.btnCopy)
        Me.Panel5.Controls.Add(Me.txterrSuggest)
        Me.Panel5.Controls.Add(Me.txtErrorlog)
        Me.Panel5.Controls.Add(Me.txterrLine)
        Me.Panel5.Controls.Add(Me.Label29)
        Me.Panel5.Controls.Add(Me.Label28)
        Me.Panel5.Controls.Add(Me.Label26)
        Me.Panel5.Controls.Add(Me.txterrSource)
        Me.Panel5.Controls.Add(Me.Label25)
        Me.Panel5.Controls.Add(Me.txterrNo)
        Me.Panel5.Controls.Add(Me.Label24)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(329, 38)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(661, 647)
        Me.Panel5.TabIndex = 7
        '
        'btnCopy
        '
        Me.btnCopy.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnCopy.Location = New System.Drawing.Point(9, 621)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(75, 23)
        Me.btnCopy.TabIndex = 6
        Me.btnCopy.Text = "Copy Error"
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'txterrSuggest
        '
        Me.txterrSuggest.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrSuggest.Location = New System.Drawing.Point(9, 136)
        Me.txterrSuggest.Multiline = True
        Me.txterrSuggest.Name = "txterrSuggest"
        Me.txterrSuggest.Size = New System.Drawing.Size(641, 92)
        Me.txterrSuggest.TabIndex = 3
        '
        'txtErrorlog
        '
        Me.txtErrorlog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtErrorlog.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txtErrorlog.Location = New System.Drawing.Point(7, 248)
        Me.txtErrorlog.Multiline = True
        Me.txtErrorlog.Name = "txtErrorlog"
        Me.txtErrorlog.Size = New System.Drawing.Size(643, 367)
        Me.txtErrorlog.TabIndex = 4
        '
        'txterrLine
        '
        Me.txterrLine.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrLine.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txterrLine.Location = New System.Drawing.Point(8, 96)
        Me.txterrLine.Name = "txterrLine"
        Me.txterrLine.ReadOnly = True
        Me.txterrLine.Size = New System.Drawing.Size(643, 21)
        Me.txterrLine.TabIndex = 2
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(5, 235)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(87, 13)
        Me.Label29.TabIndex = 1
        Me.Label29.Text = "Error Description"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(6, 120)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(60, 13)
        Me.Label28.TabIndex = 1
        Me.Label28.Text = "Suggestion"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(6, 83)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(66, 13)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "Line Number"
        '
        'txterrSource
        '
        Me.txterrSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrSource.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txterrSource.Location = New System.Drawing.Point(8, 58)
        Me.txterrSource.Name = "txterrSource"
        Me.txterrSource.ReadOnly = True
        Me.txterrSource.Size = New System.Drawing.Size(643, 21)
        Me.txterrSource.TabIndex = 1
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(6, 45)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(67, 13)
        Me.Label25.TabIndex = 1
        Me.Label25.Text = "Error Source"
        '
        'txterrNo
        '
        Me.txterrNo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txterrNo.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.txterrNo.Location = New System.Drawing.Point(8, 18)
        Me.txterrNo.Name = "txterrNo"
        Me.txterrNo.ReadOnly = True
        Me.txterrNo.Size = New System.Drawing.Size(643, 21)
        Me.txterrNo.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(6, 5)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(71, 13)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Error Number"
        '
        'Splitter1
        '
        Me.Splitter1.Location = New System.Drawing.Point(326, 38)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(3, 647)
        Me.Splitter1.TabIndex = 6
        Me.Splitter1.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Transparent
        Me.Panel4.Controls.Add(Me.lsvErrorLog)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel4.Location = New System.Drawing.Point(1, 38)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(325, 647)
        Me.Panel4.TabIndex = 5
        '
        'lsvErrorLog
        '
        Me.lsvErrorLog.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader42, Me.ColumnHeader40, Me.ColumnHeader41, Me.ColumnHeader43, Me.ColumnHeader44, Me.ColumnHeader45, Me.ColumnHeader46, Me.ColumnHeader47})
        Me.lsvErrorLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvErrorLog.FullRowSelect = True
        Me.lsvErrorLog.GridLines = True
        Me.lsvErrorLog.HideSelection = False
        Me.lsvErrorLog.Location = New System.Drawing.Point(0, 0)
        Me.lsvErrorLog.MultiSelect = False
        Me.lsvErrorLog.Name = "lsvErrorLog"
        Me.lsvErrorLog.Size = New System.Drawing.Size(325, 647)
        Me.lsvErrorLog.TabIndex = 0
        Me.lsvErrorLog.UseCompatibleStateImageBehavior = False
        Me.lsvErrorLog.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader42
        '
        Me.ColumnHeader42.Text = "PC Name"
        Me.ColumnHeader42.Width = 100
        '
        'ColumnHeader40
        '
        Me.ColumnHeader40.Text = "Entry Date"
        Me.ColumnHeader40.Width = 100
        '
        'ColumnHeader41
        '
        Me.ColumnHeader41.Text = "Schedule Name"
        Me.ColumnHeader41.Width = 100
        '
        'ColumnHeader43
        '
        Me.ColumnHeader43.Text = ""
        Me.ColumnHeader43.Width = 0
        '
        'ColumnHeader44
        '
        Me.ColumnHeader44.Text = ""
        Me.ColumnHeader44.Width = 0
        '
        'ColumnHeader45
        '
        Me.ColumnHeader45.Text = ""
        Me.ColumnHeader45.Width = 0
        '
        'ColumnHeader46
        '
        Me.ColumnHeader46.Text = ""
        Me.ColumnHeader46.Width = 0
        '
        'ColumnHeader47
        '
        Me.ColumnHeader47.Text = ""
        Me.ColumnHeader47.Width = 0
        '
        'FlowLayoutPanel8
        '
        Me.FlowLayoutPanel8.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel8.Controls.Add(Me.cmdErrorLog)
        Me.FlowLayoutPanel8.Controls.Add(Me.TableLayoutPanel5)
        Me.FlowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel8.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel8.Name = "FlowLayoutPanel8"
        Me.FlowLayoutPanel8.Size = New System.Drawing.Size(989, 37)
        Me.FlowLayoutPanel8.TabIndex = 4
        '
        'cmdErrorLog
        '
        Me.cmdErrorLog.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdErrorLog.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cmdErrorLog.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdErrorLog.Location = New System.Drawing.Point(3, 3)
        Me.cmdErrorLog.Name = "cmdErrorLog"
        Me.cmdErrorLog.Size = New System.Drawing.Size(75, 29)
        Me.cmdErrorLog.TabIndex = 2
        Me.cmdErrorLog.Text = "Clear Log"
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 3
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel5.Controls.Add(Me.Label27, 1, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.txterrSearch, 2, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(84, 3)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(245, 29)
        Me.TableLayoutPanel5.TabIndex = 3
        '
        'Label27
        '
        Me.Label27.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label27.Image = Global.sqlrd.My.Resources.Resources.view
        Me.Label27.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label27.Location = New System.Drawing.Point(3, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(72, 29)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "Search"
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txterrSearch
        '
        Me.txterrSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txterrSearch.Location = New System.Drawing.Point(81, 3)
        Me.txterrSearch.Name = "txterrSearch"
        Me.txterrSearch.Size = New System.Drawing.Size(161, 21)
        Me.txterrSearch.TabIndex = 1
        '
        'tbErrorLog
        '
        Me.tbErrorLog.AttachedControl = Me.TabControlPanel7
        Me.tbErrorLog.Image = CType(resources.GetObject("tbErrorLog.Image"), System.Drawing.Image)
        Me.tbErrorLog.Name = "tbErrorLog"
        Me.tbErrorLog.Text = "Error Log"
        '
        'TabControlPanel17
        '
        Me.TabControlPanel17.Controls.Add(Me.Panel6)
        Me.TabControlPanel17.Controls.Add(Me.TableLayoutPanel14)
        Me.TabControlPanel17.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel17.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel17.Name = "TabControlPanel17"
        Me.TabControlPanel17.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel17.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel17.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel17.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel17.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel17.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel17.TabIndex = 10
        Me.TabControlPanel17.TabItem = Me.tbScheduleRetries
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.Color.White
        Me.Panel6.Controls.Add(Me.lsvRetry)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel6.Location = New System.Drawing.Point(1, 30)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(989, 655)
        Me.Panel6.TabIndex = 1
        '
        'lsvRetry
        '
        Me.lsvRetry.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader48, Me.ColumnHeader49, Me.ColumnHeader50})
        Me.lsvRetry.ContextMenuStrip = Me.mnuRetryTracker
        Me.lsvRetry.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvRetry.FullRowSelect = True
        Me.lsvRetry.HideSelection = False
        Me.lsvRetry.LargeImageList = Me.imgTasks
        Me.lsvRetry.Location = New System.Drawing.Point(0, 0)
        Me.lsvRetry.Name = "lsvRetry"
        Me.lsvRetry.Size = New System.Drawing.Size(989, 655)
        Me.lsvRetry.SmallImageList = Me.imgTasks
        Me.lsvRetry.TabIndex = 0
        Me.lsvRetry.UseCompatibleStateImageBehavior = False
        Me.lsvRetry.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader48
        '
        Me.ColumnHeader48.Text = "Name"
        Me.ColumnHeader48.Width = 348
        '
        'ColumnHeader49
        '
        Me.ColumnHeader49.Text = "Last Retry"
        Me.ColumnHeader49.Width = 155
        '
        'ColumnHeader50
        '
        Me.ColumnHeader50.Text = "Retry #"
        Me.ColumnHeader50.Width = 86
        '
        'mnuRetryTracker
        '
        Me.mnuRetryTracker.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteToolStripMenuItem})
        Me.mnuRetryTracker.Name = "mnuRetryTracker"
        Me.mnuRetryTracker.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuRetryTracker.Size = New System.Drawing.Size(108, 26)
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(107, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'TableLayoutPanel14
        '
        Me.TableLayoutPanel14.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel14.ColumnCount = 2
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Controls.Add(Me.btnRefreshRetry, 0, 0)
        Me.TableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel14.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel14.Name = "TableLayoutPanel14"
        Me.TableLayoutPanel14.RowCount = 1
        Me.TableLayoutPanel14.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel14.Size = New System.Drawing.Size(989, 29)
        Me.TableLayoutPanel14.TabIndex = 0
        '
        'btnRefreshRetry
        '
        Me.btnRefreshRetry.Image = CType(resources.GetObject("btnRefreshRetry.Image"), System.Drawing.Image)
        Me.btnRefreshRetry.Location = New System.Drawing.Point(3, 3)
        Me.btnRefreshRetry.Name = "btnRefreshRetry"
        Me.btnRefreshRetry.Size = New System.Drawing.Size(75, 23)
        Me.btnRefreshRetry.TabIndex = 0
        Me.btnRefreshRetry.UseVisualStyleBackColor = True
        '
        'tbScheduleRetries
        '
        Me.tbScheduleRetries.AttachedControl = Me.TabControlPanel17
        Me.tbScheduleRetries.Image = CType(resources.GetObject("tbScheduleRetries.Image"), System.Drawing.Image)
        Me.tbScheduleRetries.Name = "tbScheduleRetries"
        Me.tbScheduleRetries.Text = "Schedule Retries"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.lsvEmailQueue)
        Me.TabControlPanel6.Controls.Add(Me.TableLayoutPanel11)
        Me.TabControlPanel6.Controls.Add(Me.FlowLayoutPanel5)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 3
        Me.TabControlPanel6.TabItem = Me.tbMailQ
        '
        'lsvEmailQueue
        '
        Me.lsvEmailQueue.BackColor = System.Drawing.Color.White
        Me.lsvEmailQueue.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7, Me.ColumnHeader5, Me.ColumnHeader8, Me.ColumnHeader6, Me.ColumnHeader10, Me.ColumnHeader9, Me.ColumnHeader37})
        Me.lsvEmailQueue.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvEmailQueue.FullRowSelect = True
        Me.lsvEmailQueue.GridLines = True
        Me.lsvEmailQueue.Location = New System.Drawing.Point(1, 64)
        Me.lsvEmailQueue.Name = "lsvEmailQueue"
        Me.lsvEmailQueue.Size = New System.Drawing.Size(989, 621)
        Me.lsvEmailQueue.TabIndex = 1
        Me.lsvEmailQueue.UseCompatibleStateImageBehavior = False
        Me.lsvEmailQueue.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Schedule Name"
        Me.ColumnHeader7.Width = 98
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Addresses"
        Me.ColumnHeader5.Width = 86
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "CC"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Subject"
        Me.ColumnHeader6.Width = 86
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "Last Attempt"
        Me.ColumnHeader10.Width = 91
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "Last Result"
        Me.ColumnHeader9.Width = 105
        '
        'ColumnHeader37
        '
        Me.ColumnHeader37.Text = "Path"
        Me.ColumnHeader37.Width = 240
        '
        'TableLayoutPanel11
        '
        Me.TableLayoutPanel11.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel11.ColumnCount = 3
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel11.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel11.Controls.Add(Me.Label17, 0, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.txtDeleteOlder, 1, 0)
        Me.TableLayoutPanel11.Controls.Add(Me.cmbValueUnit, 2, 0)
        Me.TableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel11.Location = New System.Drawing.Point(1, 33)
        Me.TableLayoutPanel11.Name = "TableLayoutPanel11"
        Me.TableLayoutPanel11.RowCount = 1
        Me.TableLayoutPanel11.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel11.Size = New System.Drawing.Size(989, 31)
        Me.TableLayoutPanel11.TabIndex = 8
        '
        'Label17
        '
        Me.Label17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(3, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(245, 31)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Remove items from the queue that are older than"
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDeleteOlder
        '
        Me.txtDeleteOlder.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtDeleteOlder.Location = New System.Drawing.Point(254, 3)
        Me.txtDeleteOlder.Name = "txtDeleteOlder"
        Me.txtDeleteOlder.Size = New System.Drawing.Size(68, 21)
        Me.txtDeleteOlder.TabIndex = 1
        Me.txtDeleteOlder.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDeleteOlder.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'cmbValueUnit
        '
        Me.cmbValueUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbValueUnit.FormattingEnabled = True
        Me.cmbValueUnit.Items.AddRange(New Object() {"Minutes", "Hours", "Days"})
        Me.cmbValueUnit.Location = New System.Drawing.Point(328, 3)
        Me.cmbValueUnit.Name = "cmbValueUnit"
        Me.cmbValueUnit.Size = New System.Drawing.Size(121, 21)
        Me.cmbValueUnit.TabIndex = 2
        Me.cmbValueUnit.Tag = "unsorted"
        '
        'FlowLayoutPanel5
        '
        Me.FlowLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdSendNow)
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdRemoveOne)
        Me.FlowLayoutPanel5.Controls.Add(Me.cmdRemoveAll)
        Me.FlowLayoutPanel5.Controls.Add(Me.chkOneEmailPerSchedule)
        Me.FlowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel5.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel5.Name = "FlowLayoutPanel5"
        Me.FlowLayoutPanel5.Size = New System.Drawing.Size(989, 32)
        Me.FlowLayoutPanel5.TabIndex = 7
        '
        'cmdSendNow
        '
        Me.cmdSendNow.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSendNow.Location = New System.Drawing.Point(3, 3)
        Me.cmdSendNow.Name = "cmdSendNow"
        Me.cmdSendNow.Size = New System.Drawing.Size(104, 23)
        Me.cmdSendNow.TabIndex = 4
        Me.cmdSendNow.Text = "Send Now"
        '
        'cmdRemoveOne
        '
        Me.cmdRemoveOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveOne.Location = New System.Drawing.Point(113, 3)
        Me.cmdRemoveOne.Name = "cmdRemoveOne"
        Me.cmdRemoveOne.Size = New System.Drawing.Size(104, 23)
        Me.cmdRemoveOne.TabIndex = 2
        Me.cmdRemoveOne.Text = "Remove Selected"
        '
        'cmdRemoveAll
        '
        Me.cmdRemoveAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveAll.Location = New System.Drawing.Point(223, 3)
        Me.cmdRemoveAll.Name = "cmdRemoveAll"
        Me.cmdRemoveAll.Size = New System.Drawing.Size(104, 23)
        Me.cmdRemoveAll.TabIndex = 3
        Me.cmdRemoveAll.Text = "Remove All"
        '
        'chkOneEmailPerSchedule
        '
        Me.chkOneEmailPerSchedule.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkOneEmailPerSchedule.BackColor = System.Drawing.Color.Transparent
        Me.chkOneEmailPerSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkOneEmailPerSchedule.Location = New System.Drawing.Point(333, 3)
        Me.chkOneEmailPerSchedule.Name = "chkOneEmailPerSchedule"
        Me.chkOneEmailPerSchedule.Size = New System.Drawing.Size(231, 24)
        Me.chkOneEmailPerSchedule.TabIndex = 5
        Me.chkOneEmailPerSchedule.Text = "Only send one email alert per email failure"
        Me.chkOneEmailPerSchedule.UseVisualStyleBackColor = False
        '
        'tbMailQ
        '
        Me.tbMailQ.AttachedControl = Me.TabControlPanel6
        Me.tbMailQ.Image = CType(resources.GetObject("tbMailQ.Image"), System.Drawing.Image)
        Me.tbMailQ.Name = "tbMailQ"
        Me.tbMailQ.Text = "Email Queue"
        '
        'TabControlPanel15
        '
        Me.TabControlPanel15.Controls.Add(Me.lsvReadReciepts)
        Me.TabControlPanel15.Controls.Add(Me.TableLayoutPanel10)
        Me.TabControlPanel15.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel15.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel15.Name = "TabControlPanel15"
        Me.TabControlPanel15.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel15.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel15.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel15.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel15.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel15.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel15.TabIndex = 9
        Me.TabControlPanel15.TabItem = Me.TabItem1
        '
        'lsvReadReciepts
        '
        Me.lsvReadReciepts.BackColor = System.Drawing.Color.White
        Me.lsvReadReciepts.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader29, Me.ColumnHeader31, Me.ColumnHeader32, Me.ColumnHeader33, Me.ColumnHeader34, Me.ColumnHeader35, Me.ColumnHeader30})
        Me.lsvReadReciepts.ContextMenuStrip = Me.muRR
        Me.lsvReadReciepts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvReadReciepts.FullRowSelect = True
        Me.lsvReadReciepts.GridLines = True
        Me.lsvReadReciepts.HideSelection = False
        Me.lsvReadReciepts.LargeImageList = Me.imgRR
        Me.lsvReadReciepts.Location = New System.Drawing.Point(1, 30)
        Me.lsvReadReciepts.Name = "lsvReadReciepts"
        Me.lsvReadReciepts.Size = New System.Drawing.Size(989, 655)
        Me.lsvReadReciepts.SmallImageList = Me.imgRR
        Me.lsvReadReciepts.TabIndex = 1
        Me.lsvReadReciepts.UseCompatibleStateImageBehavior = False
        Me.lsvReadReciepts.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader29
        '
        Me.ColumnHeader29.Text = "Schedule Name"
        Me.ColumnHeader29.Width = 111
        '
        'ColumnHeader31
        '
        Me.ColumnHeader31.Text = "Email Subject"
        Me.ColumnHeader31.Width = 118
        '
        'ColumnHeader32
        '
        Me.ColumnHeader32.Text = "Sent To"
        Me.ColumnHeader32.Width = 87
        '
        'ColumnHeader33
        '
        Me.ColumnHeader33.Text = "Date Sent"
        Me.ColumnHeader33.Width = 80
        '
        'ColumnHeader34
        '
        Me.ColumnHeader34.Text = "Next Check"
        Me.ColumnHeader34.Width = 95
        '
        'ColumnHeader35
        '
        Me.ColumnHeader35.Text = "Status"
        Me.ColumnHeader35.Width = 99
        '
        'ColumnHeader30
        '
        Me.ColumnHeader30.Text = "Last Checked"
        Me.ColumnHeader30.Width = 95
        '
        'TableLayoutPanel10
        '
        Me.TableLayoutPanel10.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel10.ColumnCount = 2
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel10.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel10.Controls.Add(Me.Label15, 0, 0)
        Me.TableLayoutPanel10.Controls.Add(Me.cmbFilterRR, 1, 0)
        Me.TableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel10.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel10.Name = "TableLayoutPanel10"
        Me.TableLayoutPanel10.RowCount = 1
        Me.TableLayoutPanel10.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel10.Size = New System.Drawing.Size(989, 29)
        Me.TableLayoutPanel10.TabIndex = 0
        '
        'Label15
        '
        Me.Label15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label15.Location = New System.Drawing.Point(3, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(45, 29)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Filter"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbFilterRR
        '
        Me.cmbFilterRR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFilterRR.FormattingEnabled = True
        Me.cmbFilterRR.Items.AddRange(New Object() {"Received", "Pending", "None"})
        Me.cmbFilterRR.Location = New System.Drawing.Point(54, 3)
        Me.cmbFilterRR.Name = "cmbFilterRR"
        Me.cmbFilterRR.Size = New System.Drawing.Size(121, 21)
        Me.cmbFilterRR.TabIndex = 1
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel15
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Read Receipts"
        '
        'TabControlPanel11
        '
        Me.TabControlPanel11.Controls.Add(Me.lsvAudit)
        Me.TabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel11.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel11.Name = "TabControlPanel11"
        Me.TabControlPanel11.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel11.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel11.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel11.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel11.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel11.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel11.TabIndex = 8
        Me.TabControlPanel11.TabItem = Me.tbAuditTrail
        '
        'lsvAudit
        '
        Me.lsvAudit.BackColor = System.Drawing.Color.White
        Me.lsvAudit.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader23, Me.ColumnHeader25, Me.ColumnHeader26, Me.ColumnHeader27, Me.ColumnHeader28})
        Me.lsvAudit.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvAudit.FullRowSelect = True
        Me.lsvAudit.GridLines = True
        Me.lsvAudit.HideSelection = False
        Me.lsvAudit.Location = New System.Drawing.Point(1, 1)
        Me.lsvAudit.Name = "lsvAudit"
        Me.lsvAudit.Size = New System.Drawing.Size(989, 684)
        Me.lsvAudit.TabIndex = 0
        Me.lsvAudit.UseCompatibleStateImageBehavior = False
        Me.lsvAudit.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader23
        '
        Me.ColumnHeader23.Text = "Entry Date"
        Me.ColumnHeader23.Width = 148
        '
        'ColumnHeader25
        '
        Me.ColumnHeader25.Text = "User ID"
        Me.ColumnHeader25.Width = 66
        '
        'ColumnHeader26
        '
        Me.ColumnHeader26.Text = "Object Type"
        Me.ColumnHeader26.Width = 93
        '
        'ColumnHeader27
        '
        Me.ColumnHeader27.Text = "Object Name"
        Me.ColumnHeader27.Width = 161
        '
        'ColumnHeader28
        '
        Me.ColumnHeader28.Text = "Event"
        Me.ColumnHeader28.Width = 146
        '
        'tbAuditTrail
        '
        Me.tbAuditTrail.AttachedControl = Me.TabControlPanel11
        Me.tbAuditTrail.Image = CType(resources.GetObject("tbAuditTrail.Image"), System.Drawing.Image)
        Me.tbAuditTrail.Name = "tbAuditTrail"
        Me.tbAuditTrail.Text = "Audit Trail"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.grpBackup)
        Me.TabControlPanel9.Controls.Add(Me.chkBackups)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.TabIndex = 6
        Me.TabControlPanel9.TabItem = Me.tbSystemBackup
        '
        'grpBackup
        '
        Me.grpBackup.BackColor = System.Drawing.Color.Transparent
        Me.grpBackup.Controls.Add(Me.grpAdvanced)
        Me.grpBackup.Controls.Add(Me.grpSimple)
        Me.grpBackup.Controls.Add(Me.txtKeep)
        Me.grpBackup.Controls.Add(Me.rbAdvanced)
        Me.grpBackup.Controls.Add(Me.rbSimple)
        Me.grpBackup.Controls.Add(Me.txtPath)
        Me.grpBackup.Controls.Add(Me.cmdSaveBackup)
        Me.grpBackup.Controls.Add(Me.Label6)
        Me.grpBackup.Controls.Add(Me.Button1)
        Me.grpBackup.Controls.Add(Me.Label8)
        Me.grpBackup.Controls.Add(Me.Label9)
        Me.grpBackup.Enabled = False
        Me.grpBackup.Location = New System.Drawing.Point(26, 25)
        Me.grpBackup.Name = "grpBackup"
        Me.grpBackup.Size = New System.Drawing.Size(552, 476)
        Me.grpBackup.TabIndex = 1
        Me.grpBackup.TabStop = False
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.txtZipName)
        Me.grpAdvanced.Controls.Add(Me.UcUpdate)
        Me.grpAdvanced.Controls.Add(Me.Label21)
        Me.grpAdvanced.Controls.Add(Me.chkZip)
        Me.grpAdvanced.Controls.Add(Me.Label19)
        Me.grpAdvanced.Location = New System.Drawing.Point(20, 36)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(506, 369)
        Me.grpAdvanced.TabIndex = 2
        Me.grpAdvanced.TabStop = False
        '
        'txtZipName
        '
        Me.txtZipName.Enabled = False
        Me.txtZipName.Location = New System.Drawing.Point(86, 335)
        Me.txtZipName.Name = "txtZipName"
        Me.txtZipName.Size = New System.Drawing.Size(311, 21)
        Me.txtZipName.TabIndex = 2
        '
        'UcUpdate
        '
        Me.UcUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcUpdate.Location = New System.Drawing.Point(6, 9)
        Me.UcUpdate.m_RepeatUnit = ""
        Me.UcUpdate.Name = "UcUpdate"
        Me.UcUpdate.Size = New System.Drawing.Size(493, 299)
        Me.UcUpdate.TabIndex = 0
        '
        'Label21
        '
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(398, 339)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(62, 17)
        Me.Label21.TabIndex = 13
        Me.Label21.Text = ".zip"
        '
        'chkZip
        '
        Me.chkZip.AutoSize = True
        Me.chkZip.Location = New System.Drawing.Point(17, 314)
        Me.chkZip.Name = "chkZip"
        Me.chkZip.Size = New System.Drawing.Size(135, 17)
        Me.chkZip.TabIndex = 1
        Me.chkZip.Text = "Compress (Zip) Backup"
        Me.chkZip.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(45, 339)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(62, 17)
        Me.Label19.TabIndex = 12
        Me.Label19.Text = "Name"
        '
        'grpSimple
        '
        Me.grpSimple.Controls.Add(Me.GroupBox3)
        Me.grpSimple.Location = New System.Drawing.Point(20, 36)
        Me.grpSimple.Name = "grpSimple"
        Me.grpSimple.Size = New System.Drawing.Size(506, 369)
        Me.grpSimple.TabIndex = 2
        Me.grpSimple.TabStop = False
        '
        'GroupBox3
        '
        Me.GroupBox3.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox3.Controls.Add(Me.txtInterval)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.dtBackup)
        Me.GroupBox3.Controls.Add(Me.optDaily)
        Me.GroupBox3.Controls.Add(Me.optWeekly)
        Me.GroupBox3.Controls.Add(Me.optMonthly)
        Me.GroupBox3.Controls.Add(Me.optYearly)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 15)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(490, 342)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Perform a system backup every"
        '
        'txtInterval
        '
        Me.txtInterval.Location = New System.Drawing.Point(159, 201)
        Me.txtInterval.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtInterval.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtInterval.Name = "txtInterval"
        Me.txtInterval.Size = New System.Drawing.Size(80, 21)
        Me.txtInterval.TabIndex = 5
        Me.txtInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtInterval.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(31, 173)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(24, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "At"
        '
        'dtBackup
        '
        Me.dtBackup.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtBackup.Location = New System.Drawing.Point(159, 171)
        Me.dtBackup.Name = "dtBackup"
        Me.dtBackup.ShowUpDown = True
        Me.dtBackup.Size = New System.Drawing.Size(80, 21)
        Me.dtBackup.TabIndex = 4
        '
        'optDaily
        '
        Me.optDaily.Checked = True
        Me.optDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDaily.Location = New System.Drawing.Point(31, 35)
        Me.optDaily.Name = "optDaily"
        Me.optDaily.Size = New System.Drawing.Size(104, 24)
        Me.optDaily.TabIndex = 0
        Me.optDaily.TabStop = True
        Me.optDaily.Text = "Day"
        '
        'optWeekly
        '
        Me.optWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekly.Location = New System.Drawing.Point(31, 67)
        Me.optWeekly.Name = "optWeekly"
        Me.optWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optWeekly.TabIndex = 1
        Me.optWeekly.Text = "Week"
        '
        'optMonthly
        '
        Me.optMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optMonthly.Location = New System.Drawing.Point(31, 99)
        Me.optMonthly.Name = "optMonthly"
        Me.optMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optMonthly.TabIndex = 2
        Me.optMonthly.Text = "Month"
        '
        'optYearly
        '
        Me.optYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optYearly.Location = New System.Drawing.Point(31, 131)
        Me.optYearly.Name = "optYearly"
        Me.optYearly.Size = New System.Drawing.Size(104, 24)
        Me.optYearly.TabIndex = 3
        Me.optYearly.Text = "Year"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(31, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(96, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "And repeat every "
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(247, 203)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(96, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "days"
        '
        'txtKeep
        '
        Me.txtKeep.Location = New System.Drawing.Point(235, 444)
        Me.txtKeep.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtKeep.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtKeep.Name = "txtKeep"
        Me.txtKeep.Size = New System.Drawing.Size(80, 21)
        Me.txtKeep.TabIndex = 5
        Me.txtKeep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtKeep.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.Location = New System.Drawing.Point(78, 14)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(73, 17)
        Me.rbAdvanced.TabIndex = 1
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = True
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.Checked = True
        Me.rbSimple.Location = New System.Drawing.Point(15, 13)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(55, 17)
        Me.rbSimple.TabIndex = 0
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        Me.txtPath.Location = New System.Drawing.Point(106, 411)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(312, 21)
        Me.txtPath.TabIndex = 3
        Me.txtPath.Tag = "memo"
        '
        'cmdSaveBackup
        '
        Me.cmdSaveBackup.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSaveBackup.Image = CType(resources.GetObject("cmdSaveBackup.Image"), System.Drawing.Image)
        Me.cmdSaveBackup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSaveBackup.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSaveBackup.Location = New System.Drawing.Point(468, 447)
        Me.cmdSaveBackup.Name = "cmdSaveBackup"
        Me.cmdSaveBackup.Size = New System.Drawing.Size(75, 23)
        Me.cmdSaveBackup.TabIndex = 6
        Me.cmdSaveBackup.Text = "Sa&ve"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(34, 413)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Backup Path"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(442, 411)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(48, 21)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "..."
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(106, 446)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(120, 16)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "Only keep backups for "
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(322, 446)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(96, 16)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "days"
        '
        'chkBackups
        '
        Me.chkBackups.BackColor = System.Drawing.Color.Transparent
        Me.chkBackups.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkBackups.Location = New System.Drawing.Point(4, 7)
        Me.chkBackups.Name = "chkBackups"
        Me.chkBackups.Size = New System.Drawing.Size(464, 24)
        Me.chkBackups.TabIndex = 0
        Me.chkBackups.Text = "Enable scheduled system backups"
        Me.chkBackups.UseVisualStyleBackColor = False
        '
        'tbSystemBackup
        '
        Me.tbSystemBackup.AttachedControl = Me.TabControlPanel9
        Me.tbSystemBackup.Image = CType(resources.GetObject("tbSystemBackup.Image"), System.Drawing.Image)
        Me.tbSystemBackup.Name = "tbSystemBackup"
        Me.tbSystemBackup.Text = "Scheduled Backup"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.Panel1)
        Me.TabControlPanel10.Controls.Add(Me.FlowLayoutPanel10)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel10.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.TabIndex = 7
        Me.TabControlPanel10.TabItem = Me.tbRefreshSchedule
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.lsvSchedules)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel9)
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel11)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(1, 70)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(989, 615)
        Me.Panel1.TabIndex = 11
        '
        'lsvSchedules
        '
        Me.lsvSchedules.BackColor = System.Drawing.Color.White
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader14, Me.ColumnHeader15, Me.ColumnHeader16, Me.ColumnHeader21, Me.ColumnHeader22, Me.ColumnHeader24})
        Me.lsvSchedules.ContextMenu = Me.mnuRefresh
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSchedules.ForeColor = System.Drawing.Color.Navy
        Me.lsvSchedules.FullRowSelect = True
        Me.lsvSchedules.GridLines = True
        Me.lsvSchedules.Location = New System.Drawing.Point(0, 243)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(989, 372)
        Me.lsvSchedules.TabIndex = 6
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader14
        '
        Me.ColumnHeader14.Text = "Type"
        Me.ColumnHeader14.Width = 74
        '
        'ColumnHeader15
        '
        Me.ColumnHeader15.Text = "Name"
        Me.ColumnHeader15.Width = 117
        '
        'ColumnHeader16
        '
        Me.ColumnHeader16.Text = "Frequency"
        Me.ColumnHeader16.Width = 74
        '
        'ColumnHeader21
        '
        Me.ColumnHeader21.Text = "Last Run"
        Me.ColumnHeader21.Width = 87
        '
        'ColumnHeader22
        '
        Me.ColumnHeader22.Text = "Due Next"
        Me.ColumnHeader22.Width = 116
        '
        'ColumnHeader24
        '
        Me.ColumnHeader24.Text = "Repeat Interval"
        Me.ColumnHeader24.Width = 95
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 2
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 41.90341!))
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 58.09659!))
        Me.TableLayoutPanel9.Controls.Add(Me.cmdRemoveSchedule, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.cmdAddSchedule, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(0, 210)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(989, 33)
        Me.TableLayoutPanel9.TabIndex = 10
        '
        'cmdRemoveSchedule
        '
        Me.cmdRemoveSchedule.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.cmdRemoveSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveSchedule.Image = CType(resources.GetObject("cmdRemoveSchedule.Image"), System.Drawing.Image)
        Me.cmdRemoveSchedule.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveSchedule.Location = New System.Drawing.Point(339, 5)
        Me.cmdRemoveSchedule.Name = "cmdRemoveSchedule"
        Me.cmdRemoveSchedule.Size = New System.Drawing.Size(72, 23)
        Me.cmdRemoveSchedule.TabIndex = 5
        Me.cmdRemoveSchedule.Text = "Remove"
        Me.cmdRemoveSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdAddSchedule
        '
        Me.cmdAddSchedule.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cmdAddSchedule.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddSchedule.Image = CType(resources.GetObject("cmdAddSchedule.Image"), System.Drawing.Image)
        Me.cmdAddSchedule.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAddSchedule.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddSchedule.Location = New System.Drawing.Point(417, 5)
        Me.cmdAddSchedule.Name = "cmdAddSchedule"
        Me.cmdAddSchedule.Size = New System.Drawing.Size(72, 23)
        Me.cmdAddSchedule.TabIndex = 5
        Me.cmdAddSchedule.Text = "Add"
        Me.cmdAddSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'FlowLayoutPanel11
        '
        Me.FlowLayoutPanel11.Controls.Add(Me.GroupBox5)
        Me.FlowLayoutPanel11.Controls.Add(Me.GroupBox4)
        Me.FlowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel11.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel11.Name = "FlowLayoutPanel11"
        Me.FlowLayoutPanel11.Size = New System.Drawing.Size(989, 210)
        Me.FlowLayoutPanel11.TabIndex = 9
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.tvObjects)
        Me.GroupBox5.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(288, 200)
        Me.GroupBox5.TabIndex = 8
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Select object to refresh"
        '
        'tvObjects
        '
        Me.tvObjects.ImageIndex = 0
        Me.tvObjects.ImageList = Me.imgFolders
        Me.tvObjects.Indent = 19
        Me.tvObjects.ItemHeight = 16
        Me.tvObjects.Location = New System.Drawing.Point(8, 16)
        Me.tvObjects.Name = "tvObjects"
        Me.tvObjects.SelectedImageIndex = 0
        Me.tvObjects.Size = New System.Drawing.Size(272, 176)
        Me.tvObjects.TabIndex = 7
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtzRpt)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.dtRunTime)
        Me.GroupBox4.Controls.Add(Me.optrDaily)
        Me.GroupBox4.Controls.Add(Me.optrWeekly)
        Me.GroupBox4.Controls.Add(Me.optrMonthly)
        Me.GroupBox4.Controls.Add(Me.optrYearly)
        Me.GroupBox4.Controls.Add(Me.Label11)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Location = New System.Drawing.Point(297, 3)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(288, 200)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Refresh the schedule every"
        '
        'txtzRpt
        '
        Me.txtzRpt.Location = New System.Drawing.Point(112, 142)
        Me.txtzRpt.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.txtzRpt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtzRpt.Name = "txtzRpt"
        Me.txtzRpt.Size = New System.Drawing.Size(80, 21)
        Me.txtzRpt.TabIndex = 3
        Me.txtzRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtzRpt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 120)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(24, 16)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "At"
        '
        'dtRunTime
        '
        Me.dtRunTime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtRunTime.Location = New System.Drawing.Point(112, 120)
        Me.dtRunTime.Name = "dtRunTime"
        Me.dtRunTime.ShowUpDown = True
        Me.dtRunTime.Size = New System.Drawing.Size(80, 21)
        Me.dtRunTime.TabIndex = 1
        '
        'optrDaily
        '
        Me.optrDaily.Checked = True
        Me.optrDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrDaily.Location = New System.Drawing.Point(8, 24)
        Me.optrDaily.Name = "optrDaily"
        Me.optrDaily.Size = New System.Drawing.Size(104, 24)
        Me.optrDaily.TabIndex = 0
        Me.optrDaily.TabStop = True
        Me.optrDaily.Text = "Day"
        '
        'optrWeekly
        '
        Me.optrWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrWeekly.Location = New System.Drawing.Point(8, 48)
        Me.optrWeekly.Name = "optrWeekly"
        Me.optrWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optrWeekly.TabIndex = 0
        Me.optrWeekly.Text = "Week"
        '
        'optrMonthly
        '
        Me.optrMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrMonthly.Location = New System.Drawing.Point(8, 72)
        Me.optrMonthly.Name = "optrMonthly"
        Me.optrMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optrMonthly.TabIndex = 0
        Me.optrMonthly.Text = "Month"
        '
        'optrYearly
        '
        Me.optrYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optrYearly.Location = New System.Drawing.Point(8, 96)
        Me.optrYearly.Name = "optrYearly"
        Me.optrYearly.Size = New System.Drawing.Size(104, 24)
        Me.optrYearly.TabIndex = 0
        Me.optrYearly.Text = "Year"
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 144)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 16)
        Me.Label11.TabIndex = 2
        Me.Label11.Text = "And repeat every "
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(200, 144)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(72, 16)
        Me.Label12.TabIndex = 2
        Me.Label12.Text = "days"
        '
        'FlowLayoutPanel10
        '
        Me.FlowLayoutPanel10.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel10.Controls.Add(Me.Label7)
        Me.FlowLayoutPanel10.Controls.Add(Me.chkAutoRefresh)
        Me.FlowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel10.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel10.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel10.Name = "FlowLayoutPanel10"
        Me.FlowLayoutPanel10.Size = New System.Drawing.Size(989, 69)
        Me.FlowLayoutPanel10.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(616, 32)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = resources.GetString("Label7.Text")
        '
        'chkAutoRefresh
        '
        Me.chkAutoRefresh.BackColor = System.Drawing.Color.Transparent
        Me.chkAutoRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoRefresh.Location = New System.Drawing.Point(3, 35)
        Me.chkAutoRefresh.Name = "chkAutoRefresh"
        Me.chkAutoRefresh.Size = New System.Drawing.Size(408, 24)
        Me.chkAutoRefresh.TabIndex = 10
        Me.chkAutoRefresh.Text = "Enable schedule auto-refresh"
        Me.chkAutoRefresh.UseVisualStyleBackColor = False
        '
        'tbRefreshSchedule
        '
        Me.tbRefreshSchedule.AttachedControl = Me.TabControlPanel10
        Me.tbRefreshSchedule.Image = CType(resources.GetObject("tbRefreshSchedule.Image"), System.Drawing.Image)
        Me.tbRefreshSchedule.Name = "tbRefreshSchedule"
        Me.tbRefreshSchedule.Text = "Scheduled Refresh"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.lsvDefer)
        Me.TabControlPanel8.Controls.Add(Me.FlowLayoutPanel9)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(131, 0)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(991, 686)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.TabIndex = 5
        Me.TabControlPanel8.TabItem = Me.tbDefDelivery
        '
        'lsvDefer
        '
        Me.lsvDefer.BackColor = System.Drawing.Color.White
        Me.lsvDefer.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader11, Me.ColumnHeader12, Me.ColumnHeader13})
        Me.lsvDefer.ContextMenu = Me.mnuDefer
        Me.lsvDefer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvDefer.FullRowSelect = True
        Me.lsvDefer.GridLines = True
        Me.lsvDefer.Location = New System.Drawing.Point(1, 60)
        Me.lsvDefer.Name = "lsvDefer"
        Me.lsvDefer.Size = New System.Drawing.Size(989, 625)
        Me.lsvDefer.TabIndex = 0
        Me.lsvDefer.UseCompatibleStateImageBehavior = False
        Me.lsvDefer.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader11
        '
        Me.ColumnHeader11.Text = "Report Name"
        Me.ColumnHeader11.Width = 140
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "File Path"
        Me.ColumnHeader12.Width = 323
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "Due Date"
        Me.ColumnHeader13.Width = 126
        '
        'FlowLayoutPanel9
        '
        Me.FlowLayoutPanel9.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel9.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel9.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel9.Name = "FlowLayoutPanel9"
        Me.FlowLayoutPanel9.Size = New System.Drawing.Size(989, 59)
        Me.FlowLayoutPanel9.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.optDeferAsc)
        Me.GroupBox2.Controls.Add(Me.optDeferDesc)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(272, 48)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Sort"
        '
        'optDeferAsc
        '
        Me.optDeferAsc.Checked = True
        Me.optDeferAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDeferAsc.Location = New System.Drawing.Point(16, 16)
        Me.optDeferAsc.Name = "optDeferAsc"
        Me.optDeferAsc.Size = New System.Drawing.Size(120, 24)
        Me.optDeferAsc.TabIndex = 1
        Me.optDeferAsc.TabStop = True
        Me.optDeferAsc.Tag = "ASC"
        Me.optDeferAsc.Text = "Ascending Order"
        '
        'optDeferDesc
        '
        Me.optDeferDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDeferDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDeferDesc.Name = "optDeferDesc"
        Me.optDeferDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDeferDesc.TabIndex = 0
        Me.optDeferDesc.Tag = "DESC"
        Me.optDeferDesc.Text = "Descending Order"
        '
        'tbDefDelivery
        '
        Me.tbDefDelivery.AttachedControl = Me.TabControlPanel8
        Me.tbDefDelivery.Image = CType(resources.GetObject("tbDefDelivery.Image"), System.Drawing.Image)
        Me.tbDefDelivery.Name = "tbDefDelivery"
        Me.tbDefDelivery.Text = "Deferred Delivery"
        '
        'CRDSystem
        '
        Me.CRDSystem.AttachedControl = Me.TabControlPanel1
        Me.CRDSystem.Image = CType(resources.GetObject("CRDSystem.Image"), System.Drawing.Image)
        Me.CRDSystem.Name = "CRDSystem"
        Me.CRDSystem.Text = "CRD System"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.tabHistory)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(26, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(1124, 688)
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(157, Byte), Integer), CType(CType(185, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.ScheduleHistory
        '
        'tabHistory
        '
        Me.tabHistory.CanReorderTabs = True
        Me.tabHistory.Controls.Add(Me.TabControlPanel4)
        Me.tabHistory.Controls.Add(Me.TabControlPanel13)
        Me.tabHistory.Controls.Add(Me.TabControlPanel16)
        Me.tabHistory.Controls.Add(Me.TabControlPanel14)
        Me.tabHistory.Controls.Add(Me.TabControlPanel12)
        Me.tabHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tabHistory.Location = New System.Drawing.Point(1, 1)
        Me.tabHistory.Name = "tabHistory"
        Me.tabHistory.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabHistory.SelectedTabIndex = 0
        Me.tabHistory.Size = New System.Drawing.Size(1122, 686)
        Me.tabHistory.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabHistory.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabHistory.TabIndex = 0
        Me.tabHistory.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabHistory.Tabs.Add(Me.tbSingle)
        Me.tabHistory.Tabs.Add(Me.tbPackages)
        Me.tabHistory.Tabs.Add(Me.tbEvent)
        Me.tabHistory.Tabs.Add(Me.tbAutomation)
        Me.tabHistory.Tabs.Add(Me.TabItem2)
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.tvSingle)
        Me.TabControlPanel4.Controls.Add(Me.FlowLayoutPanel3)
        Me.TabControlPanel4.Controls.Add(Me.FlowLayoutPanel6)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(970, 686)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 1
        Me.TabControlPanel4.TabItem = Me.tbSingle
        '
        'tvSingle
        '
        Me.tvSingle.BackColor = System.Drawing.Color.White
        Me.tvSingle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvSingle.HideSelection = False
        Me.tvSingle.ImageIndex = 0
        Me.tvSingle.ImageList = Me.imgSingle
        Me.tvSingle.Indent = 19
        Me.tvSingle.ItemHeight = 16
        Me.tvSingle.Location = New System.Drawing.Point(1, 59)
        Me.tvSingle.Name = "tvSingle"
        Me.tvSingle.SelectedImageIndex = 0
        Me.tvSingle.Size = New System.Drawing.Size(968, 594)
        Me.tvSingle.TabIndex = 0
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdSingleClear)
        Me.FlowLayoutPanel3.Controls.Add(Me.cmdSingleRefresh)
        Me.FlowLayoutPanel3.Controls.Add(Me.TableLayoutPanel6)
        Me.FlowLayoutPanel3.Controls.Add(Me.btnSingleHistory)
        Me.FlowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(1, 653)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(968, 32)
        Me.FlowLayoutPanel3.TabIndex = 11
        '
        'cmdSingleClear
        '
        Me.cmdSingleClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSingleClear.Image = CType(resources.GetObject("cmdSingleClear.Image"), System.Drawing.Image)
        Me.cmdSingleClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSingleClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSingleClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdSingleClear.Name = "cmdSingleClear"
        Me.cmdSingleClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdSingleClear.TabIndex = 9
        Me.cmdSingleClear.Text = "Clear"
        Me.cmdSingleClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdSingleRefresh
        '
        Me.cmdSingleRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSingleRefresh.Image = CType(resources.GetObject("cmdSingleRefresh.Image"), System.Drawing.Image)
        Me.cmdSingleRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSingleRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSingleRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdSingleRefresh.Name = "cmdSingleRefresh"
        Me.cmdSingleRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdSingleRefresh.TabIndex = 8
        Me.cmdSingleRefresh.Text = "Refresh"
        Me.cmdSingleRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel6.Controls.Add(Me.txtKeepSingle, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Label13, 0, 0)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(165, 3)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(273, 23)
        Me.TableLayoutPanel6.TabIndex = 17
        '
        'txtKeepSingle
        '
        Me.txtKeepSingle.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepSingle.Location = New System.Drawing.Point(131, 3)
        Me.txtKeepSingle.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepSingle.Name = "txtKeepSingle"
        Me.txtKeepSingle.Size = New System.Drawing.Size(56, 21)
        Me.txtKeepSingle.TabIndex = 12
        Me.txtKeepSingle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(3, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(122, 23)
        Me.Label13.TabIndex = 11
        Me.Label13.Text = "Keep history for (days)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSingleHistory
        '
        Me.btnSingleHistory.Image = CType(resources.GetObject("btnSingleHistory.Image"), System.Drawing.Image)
        Me.btnSingleHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSingleHistory.Location = New System.Drawing.Point(444, 3)
        Me.btnSingleHistory.Name = "btnSingleHistory"
        Me.btnSingleHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnSingleHistory.TabIndex = 18
        Me.btnSingleHistory.Text = "More..."
        Me.btnSingleHistory.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel6
        '
        Me.FlowLayoutPanel6.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel6.Controls.Add(Me.grpSingleSort)
        Me.FlowLayoutPanel6.Controls.Add(Me.GroupBox8)
        Me.FlowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel6.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel6.Name = "FlowLayoutPanel6"
        Me.FlowLayoutPanel6.Size = New System.Drawing.Size(968, 58)
        Me.FlowLayoutPanel6.TabIndex = 16
        '
        'grpSingleSort
        '
        Me.grpSingleSort.BackColor = System.Drawing.Color.Transparent
        Me.grpSingleSort.Controls.Add(Me.optSingleAsc)
        Me.grpSingleSort.Controls.Add(Me.optSingleDesc)
        Me.grpSingleSort.Location = New System.Drawing.Point(3, 3)
        Me.grpSingleSort.Name = "grpSingleSort"
        Me.grpSingleSort.Size = New System.Drawing.Size(272, 48)
        Me.grpSingleSort.TabIndex = 2
        Me.grpSingleSort.TabStop = False
        Me.grpSingleSort.Text = "Sort"
        '
        'optSingleAsc
        '
        Me.optSingleAsc.Checked = True
        Me.optSingleAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optSingleAsc.Location = New System.Drawing.Point(16, 16)
        Me.optSingleAsc.Name = "optSingleAsc"
        Me.optSingleAsc.Size = New System.Drawing.Size(120, 24)
        Me.optSingleAsc.TabIndex = 1
        Me.optSingleAsc.TabStop = True
        Me.optSingleAsc.Tag = "ASC"
        Me.optSingleAsc.Text = "Ascending Order"
        '
        'optSingleDesc
        '
        Me.optSingleDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optSingleDesc.Location = New System.Drawing.Point(144, 16)
        Me.optSingleDesc.Name = "optSingleDesc"
        Me.optSingleDesc.Size = New System.Drawing.Size(120, 24)
        Me.optSingleDesc.TabIndex = 0
        Me.optSingleDesc.Tag = "DESC"
        Me.optSingleDesc.Text = "Descending Order"
        '
        'GroupBox8
        '
        Me.GroupBox8.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox8.Controls.Add(Me.chkSingleFilter)
        Me.GroupBox8.Location = New System.Drawing.Point(281, 3)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(272, 48)
        Me.GroupBox8.TabIndex = 10
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Filter"
        '
        'chkSingleFilter
        '
        Me.chkSingleFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSingleFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkSingleFilter.Name = "chkSingleFilter"
        Me.chkSingleFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkSingleFilter.TabIndex = 0
        Me.chkSingleFilter.Text = "Do not show disabled schedules"
        '
        'tbSingle
        '
        Me.tbSingle.AttachedControl = Me.TabControlPanel4
        Me.tbSingle.Image = CType(resources.GetObject("tbSingle.Image"), System.Drawing.Image)
        Me.tbSingle.Name = "tbSingle"
        Me.tbSingle.Text = "Single"
        '
        'TabControlPanel13
        '
        Me.TabControlPanel13.Controls.Add(Me.tvHistory)
        Me.TabControlPanel13.Controls.Add(Me.FlowLayoutPanel1)
        Me.TabControlPanel13.Controls.Add(Me.TableLayoutPanel2)
        Me.TabControlPanel13.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel13.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel13.Name = "TabControlPanel13"
        Me.TabControlPanel13.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel13.Size = New System.Drawing.Size(970, 686)
        Me.TabControlPanel13.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel13.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel13.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel13.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel13.TabIndex = 3
        Me.TabControlPanel13.TabItem = Me.tbEvent
        '
        'tvHistory
        '
        Me.tvHistory.BackColor = System.Drawing.Color.White
        Me.tvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgHistory
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(1, 60)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.tvHistory.Size = New System.Drawing.Size(968, 591)
        Me.tvHistory.TabIndex = 0
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdEventClear)
        Me.FlowLayoutPanel1.Controls.Add(Me.cmdEventRefresh)
        Me.FlowLayoutPanel1.Controls.Add(Me.TableLayoutPanel3)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnEventHistory)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(1, 651)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(968, 34)
        Me.FlowLayoutPanel1.TabIndex = 21
        '
        'cmdEventClear
        '
        Me.cmdEventClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEventClear.Image = CType(resources.GetObject("cmdEventClear.Image"), System.Drawing.Image)
        Me.cmdEventClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEventClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEventClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdEventClear.Name = "cmdEventClear"
        Me.cmdEventClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdEventClear.TabIndex = 7
        Me.cmdEventClear.Text = "Clear"
        Me.cmdEventClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdEventRefresh
        '
        Me.cmdEventRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEventRefresh.Image = CType(resources.GetObject("cmdEventRefresh.Image"), System.Drawing.Image)
        Me.cmdEventRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEventRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEventRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdEventRefresh.Name = "cmdEventRefresh"
        Me.cmdEventRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdEventRefresh.TabIndex = 6
        Me.cmdEventRefresh.Text = "Refresh"
        Me.cmdEventRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.txtKeepEvent, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(165, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(267, 27)
        Me.TableLayoutPanel3.TabIndex = 21
        '
        'txtKeepEvent
        '
        Me.txtKeepEvent.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepEvent.Location = New System.Drawing.Point(136, 3)
        Me.txtKeepEvent.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepEvent.Name = "txtKeepEvent"
        Me.txtKeepEvent.Size = New System.Drawing.Size(56, 21)
        Me.txtKeepEvent.TabIndex = 15
        Me.txtKeepEvent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 27)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Keep history for (days)"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEventHistory
        '
        Me.btnEventHistory.Image = CType(resources.GetObject("btnEventHistory.Image"), System.Drawing.Image)
        Me.btnEventHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEventHistory.Location = New System.Drawing.Point(438, 3)
        Me.btnEventHistory.Name = "btnEventHistory"
        Me.btnEventHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnEventHistory.TabIndex = 22
        Me.btnEventHistory.Text = "More..."
        Me.btnEventHistory.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox7, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.GroupBox1, 0, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(968, 59)
        Me.TableLayoutPanel2.TabIndex = 8
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Controls.Add(Me.chkEventFilter)
        Me.GroupBox7.Location = New System.Drawing.Point(281, 3)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(272, 48)
        Me.GroupBox7.TabIndex = 6
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Filter"
        '
        'chkEventFilter
        '
        Me.chkEventFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEventFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkEventFilter.Name = "chkEventFilter"
        Me.chkEventFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkEventFilter.TabIndex = 0
        Me.chkEventFilter.Text = "Do not show disabled schedules"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.optAutoAsc)
        Me.GroupBox1.Controls.Add(Me.optAutoDesc)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(272, 48)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sort"
        '
        'optAutoAsc
        '
        Me.optAutoAsc.Checked = True
        Me.optAutoAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAutoAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAutoAsc.Name = "optAutoAsc"
        Me.optAutoAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAutoAsc.TabIndex = 1
        Me.optAutoAsc.TabStop = True
        Me.optAutoAsc.Tag = "ASC"
        Me.optAutoAsc.Text = "Ascending Order"
        '
        'optAutoDesc
        '
        Me.optAutoDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAutoDesc.Location = New System.Drawing.Point(144, 16)
        Me.optAutoDesc.Name = "optAutoDesc"
        Me.optAutoDesc.Size = New System.Drawing.Size(120, 24)
        Me.optAutoDesc.TabIndex = 0
        Me.optAutoDesc.Tag = "DESC"
        Me.optAutoDesc.Text = "Descending Order"
        '
        'tbEvent
        '
        Me.tbEvent.AttachedControl = Me.TabControlPanel13
        Me.tbEvent.Image = CType(resources.GetObject("tbEvent.Image"), System.Drawing.Image)
        Me.tbEvent.Name = "tbEvent"
        Me.tbEvent.Text = "Event-Based"
        '
        'TabControlPanel16
        '
        Me.TabControlPanel16.Controls.Add(Me.tvEPHistory)
        Me.TabControlPanel16.Controls.Add(Me.FlowLayoutPanel12)
        Me.TabControlPanel16.Controls.Add(Me.TableLayoutPanel13)
        Me.TabControlPanel16.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel16.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel16.Name = "TabControlPanel16"
        Me.TabControlPanel16.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel16.Size = New System.Drawing.Size(970, 686)
        Me.TabControlPanel16.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel16.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel16.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel16.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel16.TabIndex = 5
        Me.TabControlPanel16.TabItem = Me.TabItem2
        '
        'tvEPHistory
        '
        Me.tvEPHistory.BackColor = System.Drawing.Color.White
        Me.tvEPHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvEPHistory.HideSelection = False
        Me.tvEPHistory.ImageIndex = 0
        Me.tvEPHistory.ImageList = Me.imgEP
        Me.tvEPHistory.Indent = 19
        Me.tvEPHistory.ItemHeight = 16
        Me.tvEPHistory.Location = New System.Drawing.Point(1, 58)
        Me.tvEPHistory.Name = "tvEPHistory"
        Me.tvEPHistory.SelectedImageIndex = 0
        Me.tvEPHistory.Size = New System.Drawing.Size(968, 593)
        Me.tvEPHistory.TabIndex = 23
        '
        'imgEP
        '
        Me.imgEP.ImageStream = CType(resources.GetObject("imgEP.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgEP.TransparentColor = System.Drawing.Color.Transparent
        Me.imgEP.Images.SetKeyName(0, "cube_molecule.ico")
        Me.imgEP.Images.SetKeyName(1, "")
        Me.imgEP.Images.SetKeyName(2, "")
        Me.imgEP.Images.SetKeyName(3, "")
        Me.imgEP.Images.SetKeyName(4, "")
        Me.imgEP.Images.SetKeyName(5, "")
        '
        'FlowLayoutPanel12
        '
        Me.FlowLayoutPanel12.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel12.Controls.Add(Me.btnEPClear)
        Me.FlowLayoutPanel12.Controls.Add(Me.btnEPRefresh)
        Me.FlowLayoutPanel12.Controls.Add(Me.TableLayoutPanel12)
        Me.FlowLayoutPanel12.Controls.Add(Me.btnEventPackHistory)
        Me.FlowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel12.Location = New System.Drawing.Point(1, 651)
        Me.FlowLayoutPanel12.Name = "FlowLayoutPanel12"
        Me.FlowLayoutPanel12.Size = New System.Drawing.Size(968, 34)
        Me.FlowLayoutPanel12.TabIndex = 25
        '
        'btnEPClear
        '
        Me.btnEPClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEPClear.Image = CType(resources.GetObject("btnEPClear.Image"), System.Drawing.Image)
        Me.btnEPClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEPClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnEPClear.Location = New System.Drawing.Point(3, 3)
        Me.btnEPClear.Name = "btnEPClear"
        Me.btnEPClear.Size = New System.Drawing.Size(75, 23)
        Me.btnEPClear.TabIndex = 7
        Me.btnEPClear.Text = "Clear"
        Me.btnEPClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'btnEPRefresh
        '
        Me.btnEPRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnEPRefresh.Image = CType(resources.GetObject("btnEPRefresh.Image"), System.Drawing.Image)
        Me.btnEPRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEPRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnEPRefresh.Location = New System.Drawing.Point(84, 3)
        Me.btnEPRefresh.Name = "btnEPRefresh"
        Me.btnEPRefresh.Size = New System.Drawing.Size(75, 23)
        Me.btnEPRefresh.TabIndex = 6
        Me.btnEPRefresh.Text = "Refresh"
        Me.btnEPRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel12
        '
        Me.TableLayoutPanel12.ColumnCount = 2
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.Controls.Add(Me.txtEPKeepHistory, 1, 0)
        Me.TableLayoutPanel12.Controls.Add(Me.Label18, 0, 0)
        Me.TableLayoutPanel12.Location = New System.Drawing.Point(165, 3)
        Me.TableLayoutPanel12.Name = "TableLayoutPanel12"
        Me.TableLayoutPanel12.RowCount = 1
        Me.TableLayoutPanel12.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel12.Size = New System.Drawing.Size(267, 27)
        Me.TableLayoutPanel12.TabIndex = 21
        '
        'txtEPKeepHistory
        '
        Me.txtEPKeepHistory.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtEPKeepHistory.Location = New System.Drawing.Point(136, 3)
        Me.txtEPKeepHistory.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtEPKeepHistory.Name = "txtEPKeepHistory"
        Me.txtEPKeepHistory.Size = New System.Drawing.Size(56, 21)
        Me.txtEPKeepHistory.TabIndex = 15
        Me.txtEPKeepHistory.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label18
        '
        Me.Label18.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(3, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(127, 27)
        Me.Label18.TabIndex = 14
        Me.Label18.Text = "Keep history for (days)"
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEventPackHistory
        '
        Me.btnEventPackHistory.Image = CType(resources.GetObject("btnEventPackHistory.Image"), System.Drawing.Image)
        Me.btnEventPackHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEventPackHistory.Location = New System.Drawing.Point(438, 3)
        Me.btnEventPackHistory.Name = "btnEventPackHistory"
        Me.btnEventPackHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnEventPackHistory.TabIndex = 22
        Me.btnEventPackHistory.Text = "More..."
        Me.btnEventPackHistory.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel13
        '
        Me.TableLayoutPanel13.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel13.ColumnCount = 2
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel13.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel13.Controls.Add(Me.GroupBox11, 1, 0)
        Me.TableLayoutPanel13.Controls.Add(Me.grpEPSort, 0, 0)
        Me.TableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel13.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel13.Name = "TableLayoutPanel13"
        Me.TableLayoutPanel13.RowCount = 1
        Me.TableLayoutPanel13.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel13.Size = New System.Drawing.Size(968, 57)
        Me.TableLayoutPanel13.TabIndex = 24
        '
        'GroupBox11
        '
        Me.GroupBox11.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox11.Controls.Add(Me.chkEPFilter)
        Me.GroupBox11.Location = New System.Drawing.Point(281, 3)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(272, 47)
        Me.GroupBox11.TabIndex = 12
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Filter"
        '
        'chkEPFilter
        '
        Me.chkEPFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEPFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkEPFilter.Name = "chkEPFilter"
        Me.chkEPFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkEPFilter.TabIndex = 0
        Me.chkEPFilter.Text = "Do not show disabled schedules"
        '
        'grpEPSort
        '
        Me.grpEPSort.BackColor = System.Drawing.Color.Transparent
        Me.grpEPSort.Controls.Add(Me.optEPAsc)
        Me.grpEPSort.Controls.Add(Me.optEPDesc)
        Me.grpEPSort.Location = New System.Drawing.Point(3, 3)
        Me.grpEPSort.Name = "grpEPSort"
        Me.grpEPSort.Size = New System.Drawing.Size(272, 47)
        Me.grpEPSort.TabIndex = 3
        Me.grpEPSort.TabStop = False
        Me.grpEPSort.Text = "Sort"
        '
        'optEPAsc
        '
        Me.optEPAsc.Checked = True
        Me.optEPAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optEPAsc.Location = New System.Drawing.Point(16, 16)
        Me.optEPAsc.Name = "optEPAsc"
        Me.optEPAsc.Size = New System.Drawing.Size(120, 24)
        Me.optEPAsc.TabIndex = 1
        Me.optEPAsc.TabStop = True
        Me.optEPAsc.Tag = "ASC"
        Me.optEPAsc.Text = "Ascending Order"
        '
        'optEPDesc
        '
        Me.optEPDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optEPDesc.Location = New System.Drawing.Point(144, 16)
        Me.optEPDesc.Name = "optEPDesc"
        Me.optEPDesc.Size = New System.Drawing.Size(120, 24)
        Me.optEPDesc.TabIndex = 0
        Me.optEPDesc.Tag = "DESC"
        Me.optEPDesc.Text = "Descending Order"
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel16
        Me.TabItem2.Image = CType(resources.GetObject("TabItem2.Image"), System.Drawing.Image)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Event-Based Packages"
        '
        'TabControlPanel14
        '
        Me.TabControlPanel14.Controls.Add(Me.tvAutoHistory)
        Me.TabControlPanel14.Controls.Add(Me.FlowLayoutPanel2)
        Me.TabControlPanel14.Controls.Add(Me.TableLayoutPanel1)
        Me.TabControlPanel14.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel14.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel14.Name = "TabControlPanel14"
        Me.TabControlPanel14.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel14.Size = New System.Drawing.Size(970, 686)
        Me.TabControlPanel14.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel14.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel14.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel14.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel14.TabIndex = 4
        Me.TabControlPanel14.TabItem = Me.tbAutomation
        '
        'tvAutoHistory
        '
        Me.tvAutoHistory.BackColor = System.Drawing.Color.White
        Me.tvAutoHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvAutoHistory.HideSelection = False
        Me.tvAutoHistory.ImageIndex = 0
        Me.tvAutoHistory.ImageList = Me.imgAuto
        Me.tvAutoHistory.Indent = 19
        Me.tvAutoHistory.ItemHeight = 16
        Me.tvAutoHistory.Location = New System.Drawing.Point(1, 58)
        Me.tvAutoHistory.Name = "tvAutoHistory"
        Me.tvAutoHistory.SelectedImageIndex = 0
        Me.tvAutoHistory.Size = New System.Drawing.Size(968, 593)
        Me.tvAutoHistory.TabIndex = 0
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdAutoClear)
        Me.FlowLayoutPanel2.Controls.Add(Me.cmdAutoRefresh)
        Me.FlowLayoutPanel2.Controls.Add(Me.TableLayoutPanel7)
        Me.FlowLayoutPanel2.Controls.Add(Me.btnAutoHistory)
        Me.FlowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(1, 651)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(968, 34)
        Me.FlowLayoutPanel2.TabIndex = 22
        '
        'cmdAutoClear
        '
        Me.cmdAutoClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAutoClear.Image = CType(resources.GetObject("cmdAutoClear.Image"), System.Drawing.Image)
        Me.cmdAutoClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAutoClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAutoClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdAutoClear.Name = "cmdAutoClear"
        Me.cmdAutoClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdAutoClear.TabIndex = 7
        Me.cmdAutoClear.Text = "Clear"
        Me.cmdAutoClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdAutoRefresh
        '
        Me.cmdAutoRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAutoRefresh.Image = CType(resources.GetObject("cmdAutoRefresh.Image"), System.Drawing.Image)
        Me.cmdAutoRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAutoRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAutoRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdAutoRefresh.Name = "cmdAutoRefresh"
        Me.cmdAutoRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdAutoRefresh.TabIndex = 6
        Me.cmdAutoRefresh.Text = "Refresh"
        Me.cmdAutoRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.txtAutoKeep, 1, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.Label14, 0, 0)
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(165, 3)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(267, 27)
        Me.TableLayoutPanel7.TabIndex = 21
        '
        'txtAutoKeep
        '
        Me.txtAutoKeep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtAutoKeep.Location = New System.Drawing.Point(136, 3)
        Me.txtAutoKeep.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtAutoKeep.Name = "txtAutoKeep"
        Me.txtAutoKeep.Size = New System.Drawing.Size(56, 21)
        Me.txtAutoKeep.TabIndex = 15
        Me.txtAutoKeep.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(3, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(127, 27)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Keep history for (days)"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnAutoHistory
        '
        Me.btnAutoHistory.Image = CType(resources.GetObject("btnAutoHistory.Image"), System.Drawing.Image)
        Me.btnAutoHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAutoHistory.Location = New System.Drawing.Point(438, 3)
        Me.btnAutoHistory.Name = "btnAutoHistory"
        Me.btnAutoHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnAutoHistory.TabIndex = 22
        Me.btnAutoHistory.Text = "More..."
        Me.btnAutoHistory.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox10, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.grpSort, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 1)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(968, 57)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'GroupBox10
        '
        Me.GroupBox10.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox10.Controls.Add(Me.chkAutoFilter)
        Me.GroupBox10.Location = New System.Drawing.Point(281, 3)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(272, 47)
        Me.GroupBox10.TabIndex = 12
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Filter"
        '
        'chkAutoFilter
        '
        Me.chkAutoFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkAutoFilter.Name = "chkAutoFilter"
        Me.chkAutoFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkAutoFilter.TabIndex = 0
        Me.chkAutoFilter.Text = "Do not show disabled schedules"
        '
        'grpSort
        '
        Me.grpSort.BackColor = System.Drawing.Color.Transparent
        Me.grpSort.Controls.Add(Me.optAsc)
        Me.grpSort.Controls.Add(Me.optDesc)
        Me.grpSort.Location = New System.Drawing.Point(3, 3)
        Me.grpSort.Name = "grpSort"
        Me.grpSort.Size = New System.Drawing.Size(272, 47)
        Me.grpSort.TabIndex = 3
        Me.grpSort.TabStop = False
        Me.grpSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 1
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 0
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'tbAutomation
        '
        Me.tbAutomation.AttachedControl = Me.TabControlPanel14
        Me.tbAutomation.Image = CType(resources.GetObject("tbAutomation.Image"), System.Drawing.Image)
        Me.tbAutomation.Name = "tbAutomation"
        Me.tbAutomation.Text = "Automation"
        '
        'TabControlPanel12
        '
        Me.TabControlPanel12.Controls.Add(Me.tvPackage)
        Me.TabControlPanel12.Controls.Add(Me.FlowLayoutPanel7)
        Me.TabControlPanel12.Controls.Add(Me.FlowLayoutPanel4)
        Me.TabControlPanel12.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel12.Location = New System.Drawing.Point(152, 0)
        Me.TabControlPanel12.Name = "TabControlPanel12"
        Me.TabControlPanel12.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel12.Size = New System.Drawing.Size(970, 686)
        Me.TabControlPanel12.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel12.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel12.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel12.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel12.TabIndex = 2
        Me.TabControlPanel12.TabItem = Me.tbPackages
        '
        'tvPackage
        '
        Me.tvPackage.BackColor = System.Drawing.Color.White
        Me.tvPackage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tvPackage.HideSelection = False
        Me.tvPackage.ImageIndex = 0
        Me.tvPackage.ImageList = Me.imgPackage
        Me.tvPackage.Indent = 19
        Me.tvPackage.ItemHeight = 16
        Me.tvPackage.Location = New System.Drawing.Point(1, 56)
        Me.tvPackage.Name = "tvPackage"
        Me.tvPackage.SelectedImageIndex = 0
        Me.tvPackage.Size = New System.Drawing.Size(968, 595)
        Me.tvPackage.TabIndex = 0
        '
        'FlowLayoutPanel7
        '
        Me.FlowLayoutPanel7.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdPackClear)
        Me.FlowLayoutPanel7.Controls.Add(Me.cmdPackRefresh)
        Me.FlowLayoutPanel7.Controls.Add(Me.TableLayoutPanel4)
        Me.FlowLayoutPanel7.Controls.Add(Me.btnPackageHistory)
        Me.FlowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.FlowLayoutPanel7.Location = New System.Drawing.Point(1, 651)
        Me.FlowLayoutPanel7.Name = "FlowLayoutPanel7"
        Me.FlowLayoutPanel7.Size = New System.Drawing.Size(968, 34)
        Me.FlowLayoutPanel7.TabIndex = 20
        '
        'cmdPackClear
        '
        Me.cmdPackClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdPackClear.Image = CType(resources.GetObject("cmdPackClear.Image"), System.Drawing.Image)
        Me.cmdPackClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPackClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPackClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdPackClear.Name = "cmdPackClear"
        Me.cmdPackClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdPackClear.TabIndex = 7
        Me.cmdPackClear.Text = "Clear"
        Me.cmdPackClear.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdPackRefresh
        '
        Me.cmdPackRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdPackRefresh.Image = CType(resources.GetObject("cmdPackRefresh.Image"), System.Drawing.Image)
        Me.cmdPackRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPackRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdPackRefresh.Location = New System.Drawing.Point(84, 3)
        Me.cmdPackRefresh.Name = "cmdPackRefresh"
        Me.cmdPackRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdPackRefresh.TabIndex = 6
        Me.cmdPackRefresh.Text = "Refresh"
        Me.cmdPackRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.txtKeepPack, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label16, 0, 0)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(165, 3)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(267, 27)
        Me.TableLayoutPanel4.TabIndex = 21
        '
        'txtKeepPack
        '
        Me.txtKeepPack.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtKeepPack.Location = New System.Drawing.Point(136, 3)
        Me.txtKeepPack.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtKeepPack.Name = "txtKeepPack"
        Me.txtKeepPack.Size = New System.Drawing.Size(56, 21)
        Me.txtKeepPack.TabIndex = 15
        Me.txtKeepPack.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label16
        '
        Me.Label16.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(3, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(127, 27)
        Me.Label16.TabIndex = 14
        Me.Label16.Text = "Keep history for (days)"
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnPackageHistory
        '
        Me.btnPackageHistory.Image = CType(resources.GetObject("btnPackageHistory.Image"), System.Drawing.Image)
        Me.btnPackageHistory.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPackageHistory.Location = New System.Drawing.Point(438, 3)
        Me.btnPackageHistory.Name = "btnPackageHistory"
        Me.btnPackageHistory.Size = New System.Drawing.Size(85, 23)
        Me.btnPackageHistory.TabIndex = 22
        Me.btnPackageHistory.Text = "More..."
        Me.btnPackageHistory.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel4
        '
        Me.FlowLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel4.Controls.Add(Me.grpPackageSort)
        Me.FlowLayoutPanel4.Controls.Add(Me.GroupBox9)
        Me.FlowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.FlowLayoutPanel4.Location = New System.Drawing.Point(1, 1)
        Me.FlowLayoutPanel4.Name = "FlowLayoutPanel4"
        Me.FlowLayoutPanel4.Size = New System.Drawing.Size(968, 55)
        Me.FlowLayoutPanel4.TabIndex = 19
        '
        'grpPackageSort
        '
        Me.grpPackageSort.BackColor = System.Drawing.Color.Transparent
        Me.grpPackageSort.Controls.Add(Me.optPackageAsc)
        Me.grpPackageSort.Controls.Add(Me.optPackageDesc)
        Me.grpPackageSort.Location = New System.Drawing.Point(3, 3)
        Me.grpPackageSort.Name = "grpPackageSort"
        Me.grpPackageSort.Size = New System.Drawing.Size(261, 44)
        Me.grpPackageSort.TabIndex = 1
        Me.grpPackageSort.TabStop = False
        Me.grpPackageSort.Text = "Sort"
        '
        'optPackageAsc
        '
        Me.optPackageAsc.Checked = True
        Me.optPackageAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPackageAsc.Location = New System.Drawing.Point(16, 16)
        Me.optPackageAsc.Name = "optPackageAsc"
        Me.optPackageAsc.Size = New System.Drawing.Size(120, 24)
        Me.optPackageAsc.TabIndex = 1
        Me.optPackageAsc.TabStop = True
        Me.optPackageAsc.Tag = "ASC"
        Me.optPackageAsc.Text = "Ascending Order"
        '
        'optPackageDesc
        '
        Me.optPackageDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optPackageDesc.Location = New System.Drawing.Point(144, 16)
        Me.optPackageDesc.Name = "optPackageDesc"
        Me.optPackageDesc.Size = New System.Drawing.Size(120, 24)
        Me.optPackageDesc.TabIndex = 0
        Me.optPackageDesc.Tag = "DESC"
        Me.optPackageDesc.Text = "Descending Order"
        '
        'GroupBox9
        '
        Me.GroupBox9.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox9.Controls.Add(Me.chkPackageFilter)
        Me.GroupBox9.Location = New System.Drawing.Point(270, 3)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(261, 44)
        Me.GroupBox9.TabIndex = 11
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Filter"
        '
        'chkPackageFilter
        '
        Me.chkPackageFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPackageFilter.Location = New System.Drawing.Point(8, 16)
        Me.chkPackageFilter.Name = "chkPackageFilter"
        Me.chkPackageFilter.Size = New System.Drawing.Size(256, 24)
        Me.chkPackageFilter.TabIndex = 0
        Me.chkPackageFilter.Text = "Do not show disabled schedules"
        '
        'tbPackages
        '
        Me.tbPackages.AttachedControl = Me.TabControlPanel12
        Me.tbPackages.Image = CType(resources.GetObject("tbPackages.Image"), System.Drawing.Image)
        Me.tbPackages.Name = "tbPackages"
        Me.tbPackages.Text = "Packages"
        '
        'ScheduleHistory
        '
        Me.ScheduleHistory.AttachedControl = Me.TabControlPanel2
        Me.ScheduleHistory.Image = CType(resources.GetObject("ScheduleHistory.Image"), System.Drawing.Image)
        Me.ScheduleHistory.Name = "ScheduleHistory"
        Me.ScheduleHistory.Text = "Schedule History"
        '
        'cmnuEPClear
        '
        Me.cmnuEPClear.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuEPClearAll, Me.mnuEPClearSel})
        '
        'mnuEPClearAll
        '
        Me.mnuEPClearAll.Index = 0
        Me.mnuEPClearAll.Text = "Clear All"
        '
        'mnuEPClearSel
        '
        Me.mnuEPClearSel.Index = 1
        Me.mnuEPClearSel.Text = "Clear Selected"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem4, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.MenuItem5, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem8})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 5
        Me.MenuItem5.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 9
        Me.MenuItem8.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem9, Me.MenuItem10, Me.mnuDatabase})
        Me.MenuItem8.Text = "Insert"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 0
        Me.MenuItem9.Text = "Constants"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 1
        Me.MenuItem10.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'tmScheduleMonCleaner
        '
        Me.tmScheduleMonCleaner.Enabled = True
        Me.tmScheduleMonCleaner.Interval = 500
        Me.tmScheduleMonCleaner.SynchronizingObject = Me
        '
        'bgEmailLog
        '
        '
        'tmEmailLog
        '
        Me.tmEmailLog.Interval = 30000
        Me.tmEmailLog.SynchronizingObject = Me
        '
        'frmSystemMonitor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(1150, 688)
        Me.Controls.Add(Me.TabControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSystemMonitor"
        Me.Text = "System Monitor"
        Me.muRR.ResumeLayout(False)
        CType(Me.tmScheduleMon, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEmailQueue, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        CType(Me.tabCRD, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCRD.ResumeLayout(False)
        Me.TabControlPanel5.ResumeLayout(False)
        CType(Me.dgvEmailLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuClearEmailLog.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel8.PerformLayout()
        CType(Me.txtKeepLogs, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.tmSlider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox14.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel7.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.FlowLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel5.PerformLayout()
        Me.TabControlPanel17.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.mnuRetryTracker.ResumeLayout(False)
        Me.TableLayoutPanel14.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        Me.TableLayoutPanel11.ResumeLayout(False)
        Me.TableLayoutPanel11.PerformLayout()
        CType(Me.txtDeleteOlder, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel5.ResumeLayout(False)
        Me.TabControlPanel15.ResumeLayout(False)
        Me.TableLayoutPanel10.ResumeLayout(False)
        Me.TabControlPanel11.ResumeLayout(False)
        Me.TabControlPanel9.ResumeLayout(False)
        Me.grpBackup.ResumeLayout(False)
        Me.grpBackup.PerformLayout()
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        Me.grpSimple.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.txtInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtKeep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel10.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TableLayoutPanel9.ResumeLayout(False)
        Me.FlowLayoutPanel11.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.txtzRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel10.ResumeLayout(False)
        Me.TabControlPanel8.ResumeLayout(False)
        Me.FlowLayoutPanel9.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        CType(Me.tabHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabHistory.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.FlowLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel6.ResumeLayout(False)
        CType(Me.txtKeepSingle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel6.ResumeLayout(False)
        Me.grpSingleSort.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        Me.TabControlPanel13.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        CType(Me.txtKeepEvent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TabControlPanel16.ResumeLayout(False)
        Me.FlowLayoutPanel12.ResumeLayout(False)
        Me.TableLayoutPanel12.ResumeLayout(False)
        CType(Me.txtEPKeepHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel13.ResumeLayout(False)
        Me.GroupBox11.ResumeLayout(False)
        Me.grpEPSort.ResumeLayout(False)
        Me.TabControlPanel14.ResumeLayout(False)
        Me.FlowLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        CType(Me.txtAutoKeep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.grpSort.ResumeLayout(False)
        Me.TabControlPanel12.ResumeLayout(False)
        Me.FlowLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.txtKeepPack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FlowLayoutPanel4.ResumeLayout(False)
        Me.grpPackageSort.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.tmScheduleMonCleaner, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tmEmailLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Public Sub Listen()

        Try
            tcpListener.Start()
        Catch
            Return
        End Try

        'Accept the pending client connection and return             'a TcpClient initialized for communication. 
        Dim tcpClient As TcpClient

        Try
            tcpClient = tcpListener.AcceptTcpClient()
        Catch
            Return
        End Try

        Try

            Dim networkStream As NetworkStream = tcpClient.GetStream()

            ' Read the stream into a byte array
            Dim bytes(tcpClient.ReceiveBufferSize) As Byte

            networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))

            Dim clientdata As String = Encoding.ASCII.GetString(bytes)

            'Close TcpListener and TcpClient.
            logDataFromClient(clientdata)

            tcpClient.Close()
            tcpListener.Stop()

            Dim recycledThread As Boolean = False

            For Each t As Threading.Thread In oTs
                If t.IsAlive = False Then
                    recycledThread = True
                    t = New Threading.Thread(AddressOf Listen)
                    t.Start()
                    Exit For
                End If
            Next

            If recycledThread = False Then
                Dim oT As New Threading.Thread(AddressOf Listen)

                oT.Start()

                oTs.Add(oT)
            End If
        Catch : End Try

    End Sub

    Private Sub logDataFromClient(ByVal data As String)
        Try
            Dim sID As String = data.Split("|")(0) ' item ID
            Dim msg As String = data.Split("|")(1) ' status
            Dim value As Integer = data.Split("|")(2) ' progress amount 
            Dim processID As Integer = data.Split("|")(3) ' the process id 
            Dim itemFound As Boolean = False


            Dim sType As String = sID.Split(":")(0)
            Dim nID As Integer = sID.Split(":")(1)
            Dim SQL As String
            Dim sWhere As String = ""
            Dim sMsg As String = msg & " (" & value & "%)"



            'we also update or insert the progressid in the progressTable data table
            Dim rows() As DataRow = Me.progressTable.Select("processid =" & processID)

            If rows IsNot Nothing Then
                If rows.Length = 0 Then
                    Dim row As DataRow = Me.progressTable.Rows.Add

                    row("processid") = processID
                    row("status") = sMsg
                Else
                    Dim row As DataRow = rows(0)

                    row("status") = sMsg
                End If
            End If

            'clean up dead processes
            Try
                For Each row As DataRow In Me.progressTable.Rows
                    Dim procID As Integer = row("processid")

                    Try
                        Dim proc As Process = Process.GetProcessById(procID)
                    Catch ex As Exception
                        row.Delete()
                    End Try
                Next
            Catch : End Try


        Catch : End Try
    End Sub

    Private Function GetProcessID(ByVal nID As Integer, ByVal nType As clsMarsScheduler.enScheduleType) As Integer
        Dim column As String = ""

        Select Case nType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                column = "AutoID"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                column = "EventID"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                column = "EventPackID"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                column = "PackID"
            Case clsMarsScheduler.enScheduleType.REPORT
                column = "ReportID"
            Case Else
                Return 0
        End Select

        Dim SQL As String = "SELECT ProcessID FROM ThreadManager WHERE " & column & " = " & nID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then Return 0

        If oRs.EOF = False Then
            Dim processID As Integer = oRs(0).Value
            oRs.Close()
            Return processID
        Else
            oRs.Close()
            Return 0
        End If

    End Function

    Private Sub frmSystemMonitor_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Enter
        If IsLoaded = False Then Return

        Select Case tabCRD.SelectedTab.Text.ToLower
            Case "schedule manager"
                Me.tmScheduleMon.Enabled = True
                Me.tmScheduleMonCleaner.Enabled = True
            Case "email log"
                Me.tmEmailLog.Enabled = True
            Case "email queue"
                Me.tmEmailQueue.Enabled = False
        End Select
    End Sub
    Private Sub frmSystemMonitor_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        Try
            For Each t As Threading.Thread In oTs
                Try
                    'console.writeline(t.IsAlive)
                    t.Abort()
                    t = Nothing
                Catch : End Try
            Next

            tcpListener.Stop()
            tcpListener = Nothing

            clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile, False)

            gSysmonInstance = Nothing

            Me.tmEmailQueue.Stop()
            Me.tmScheduleMon.Stop()
            Me.tmEmailLog.Stop()
            Me.tmScheduleMonCleaner.Stop()

            Me.progressTable.Dispose()
        Catch : End Try
    End Sub



    Private Sub frmSystemMonitor_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Leave
        Me.tmEmailQueue.Enabled = False
        Me.tmScheduleMon.Enabled = False
        Me.tmEmailLog.Enabled = False
        Me.tmScheduleMonCleaner.Enabled = False
    End Sub

    Private Sub frmSystemMonitor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MdiParent = oMain

        Try
            tcpListener = New TcpListener(IPAddress.Any, 2003)
        Catch : End Try
        'Try

        '    For Each o As frmWindow In oWindow
        '        o.WindowState = FormWindowState.Normal
        '    Next

        'Catch : End Try

        'Me.WindowState = FormWindowState.Normal

        Application.DoEvents()

        'Me.ScheduleMonitor()

        FormatForWinXP(Me)
        Me.txtZipName.ContextMenu = Me.mnuInserter
        Me.txtPath.ContextMenu = Me.mnuInserter

        cmbFilter.Text = "None"

        Try
            Dim n As Integer

            n = oUI.ReadRegistry("AutoBackUp", 0)

            chkBackups.Checked = Convert.ToBoolean(n)
        Catch
            chkBackups.Checked = False
        End Try

        Try
            For Each grp As GroupBox In Me.Controls
                If grp.Tag = "resize" Then
                    grp.Width = 630
                    grp.Height = 502

                    grp.Top = 48
                    grp.Left = 152

                    grp.Anchor = AnchorStyles.Top
                    grp.Anchor = AnchorStyles.Bottom
                    grp.Anchor = AnchorStyles.Left
                    grp.Anchor = AnchorStyles.Right
                End If
            Next
        Catch ex As Exception

        End Try

        tmScheduleMon.Enabled = True

        Try
            chkAutoRefresh.Checked = oUI.ReadRegistry("AutoRefresh", 0)
        Catch ex As Exception
            chkAutoRefresh.Checked = False
        End Try

        Panel1.Enabled = chkAutoRefresh.Checked

        Dim Audit As Boolean

        Try
            Audit = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AuditTrail", 0)))
        Catch ex As Exception
            Audit = False
        End Try

        If Audit = False Then
            tabCRD.Tabs(7).Visible = False
        End If

        Try
            chkOneEmailPerSchedule.Checked = oUI.ReadRegistry("OneEmailPerFailure", 0)
        Catch ex As Exception
            chkOneEmailPerSchedule.Checked = False
        End Try

        Dim size As String = clsMarsUI.MainUI.ReadRegistry("SysMonSize", "879,419")

        Me.Width = size.Split(",")(0)
        Me.Height = size.Split(",")(1)

        Try
            Dim value As String = oUI.ReadRegistry("EmailQueueDeleteAfter", "30 Days")
            Me.txtDeleteOlder.Value = value.Split(" ")(0)
            Me.cmbValueUnit.Text = value.Split(" ")(1)
        Catch : End Try

        Try
            txtKeepSingle.Value = oUI.ReadRegistry("KeepSingle", 14)
        Catch
            txtKeepSingle.Value = 14
        End Try

        Me.progressTable = New DataTable

        With Me.progressTable.Columns
            .Add("ProcessID")
            .Add("Status")
        End With

        RestoreColumns(Me.lsvTaskManager)
        RestoreColumns(Me.lsvProcessing)

        IsLoaded = True

        Dim oT As System.Threading.Thread

        oT = New Threading.Thread(AddressOf Me.Listen)

        oT.Start()

        oTs.Add(oT)

        'set up the scheduler  control buttons
        Try
            Dim svc As clsServiceController = New clsServiceController

            If svc.m_serviceType <> clsServiceController.enum_svcType.NONE Then
                If svc.m_serviceStatus = clsServiceController.enum_svcStatus.RUNNING Then
                    Me.btnStop.Enabled = True
                    Me.btnResume.Enabled = False
                Else
                    Me.btnStop.Enabled = False
                    Me.btnResume.Enabled = True
                End If
            Else
                GroupBox12.Enabled = False
            End If
        Catch : End Try

        Me.tmSlider.Value = clsMarsUI.MainUI.ReadRegistry("TaskManagerRefreshInt", 30)
        Me.tmScheduleMon.Interval = Me.tmSlider.Value * 1000
        chkGroup.Checked = clsMarsUI.MainUI.ReadRegistry("GroupScheduleMon", 1)
        Me.GroupBox14.Height = clsMarsUI.MainUI.ReadRegistry("TaskManagerLayout", 168)
    End Sub

    Private Function searchListView(ByVal sTag As String) As Boolean
        Try
            For Each it As ListViewItem In Me.lsvProcessing.Items
                If it.Tag.ToLower = sTag.ToLower Then
                    Return True
                End If
            Next

            Return False
        Catch : End Try
    End Function

    Private Function cleanListView()
        Try
            For Each it As ListViewItem In Me.lsvTaskManager.Items

                If it.SubItems(3).Text = "Waiting..." Then Continue For

                Dim sTag As String = it.Tag
                Dim type As String = sTag.Split(":")(0)
                Dim nID As Integer = sTag.Split(":")(1)
                Dim SQL, Col As String

                Select Case type.ToLower
                    Case "report"
                        Col = "ReportID"
                    Case "package"
                        Col = "PackID"
                    Case "automation"
                        Col = "AutoID"
                    Case "event-package"
                        Col = "EventPackID"
                    Case "event"
                        Col = "EventID"
                    Case "backup"
                        Col = "AutoID"
                End Select

                SQL = "SELECT * FROM TaskManager WHERE " & Col & " = " & nID

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs.EOF = True Then
                    it.Remove()
                End If

                oRs.Close()
            Next
        Catch : End Try
    End Function

    Private Function getTaskStatus(ByVal processID As Integer) As String
        Try
            Dim rows() As DataRow = Me.progressTable.Select("processid =" & processID)
            Dim status As String = "Executing..."

            If rows IsNot Nothing Then
                If rows.Length > 0 Then
                    Dim row As DataRow = rows(0)

                    status = IsNull(row("status"), "Executing...")
                End If
            End If


            'Dim SQL As String = "SELECT status FROM TaskStatusTracker WHERE ProcessID =" & processID
            'Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)


            'If oRs IsNot Nothing Then
            '    If oRs.EOF = False Then
            '        status = IsNull(oRs("status").Value, "Executing...")
            '    End If

            '    oRs.Close()
            'End If

            Return status
        Catch
            Return "Executing..."
        End Try
    End Function

    'add proceses that are executing
    Private Sub ProcessMonitor()

10:     If IsLoaded = False Then Return


        Dim SQL() As String
        Dim oRs As ADODB.Recordset
        Dim lsv As ListViewItem
        Dim I As Integer
        Dim nCheck As Integer = 0
        Dim sUnit As String = "d"

220:    If gConType = "DAT" Then
230:        sUnit = "'" & sUnit & "'"
        End If

240:    Me.lsvProcessing.BeginUpdate()

250:

        'add all the items that are being processed

260:    Try

270:        ReDim SQL(0)
            'back ups
280:        SQL(0) = "SELECT * FROM TaskManager WHERE AutoID = 99999"

290:        oRs = clsMarsData.GetData(SQL(0))

300:        If oRs IsNot Nothing Then
310:            If oRs.EOF = False Then
                    Dim sTag As String = "Backup:99999"
                    If searchListView(sTag) = False Then
320:                    lsv = lsvProcessing.Items.Add("System maintenance") 'name
330:                    lsv.Tag = sTag
340:                    lsv.ImageIndex = 8

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

350:                    With lsv.SubItems
360:                        .Add(Convert.ToString(val)) 'start time
370:                        .Add(IsNull(oRs("pcname").Value)) 'pc name
380:                        .Add("Collecting data...") 'status
390:                        .Add(IsNull(oRs("processid").Value, "")) 'process id
400:                        .Add(duration) 'process duration
                        End With
                    End If
                End If

410:            oRs.Close()
            End If

            'schedule retries
420:        SQL(0) = "SELECT * FROM TaskManager WHERE AutoID = 99998"

430:        oRs = clsMarsData.GetData(SQL(0))

440:        If oRs IsNot Nothing Then
450:            If oRs.EOF = False Then
                    Dim sTag As String = "Retry:99998"

                    If searchListView(sTag) = False Then
460:                    lsv = lsvProcessing.Items.Add("Schedule Retry") 'name
470:                    lsv.Tag = sTag
480:                    lsv.ImageIndex = 11

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

490:                    With lsv.SubItems
500:                        .Add(Convert.ToString(val)) 'start time
510:                        .Add(IsNull(oRs("pcname").Value)) 'pc name
520:                        .Add("Collecting data...") 'status
530:                        .Add(IsNull(oRs("processid").Value)) 'process id
540:                        .Add(duration) 'process duration
                        End With
                    End If
                End If

550:            oRs.Close()
            End If

            'single based schedules
560:        SQL(0) = "SELECT ReportAttr.ReportTitle, ReportAttr.ReportID, NextRun,PCName,TaskManager.Status AS Status, " & _
                  "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                  "(ReportAttr INNER JOIN ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID) " & _
                  "INNER JOIN TaskManager ON ReportAttr.ReportID = TaskManager.ReportID"

570:        oRs = clsMarsData.GetData(SQL(0))

580:        If Not oRs Is Nothing Then
590:            Do While oRs.EOF = False
                    Dim sTag As String = "Report:" & oRs(1).Value

600:                If searchListView(sTag) = False Then
610:                    lsv = lsvProcessing.Items.Add(oRs("reporttitle").Value)
620:                    lsv.Tag = sTag

630:                    If clsMarsData.IsScheduleBursting(oRs(1).Value) = True Then
640:                        lsv.ImageIndex = 4
650:                    ElseIf clsMarsData.IsScheduleDynamic(oRs(1).Value, "Report") = True Then
660:                        lsv.ImageIndex = 5
670:                    ElseIf clsMarsData.IsScheduleDataDriven(oRs(1).Value, "Report") = True Then
680:                        lsv.ImageIndex = 10
690:                    Else
700:                        lsv.ImageIndex = 1
                        End If

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

710:                    With lsv.SubItems
720:                        .Add(Convert.ToString((val)))
730:                        .Add(IsNull(oRs("pcname").Value))
740:                        .Add(Me.getTaskStatus(IsNull(oRs("processid").Value))) '(IsNull(oRs("status").Value))
750:                        .Add(IsNull(oRs("processid").Value))
760:                        .Add(duration)
                        End With
                    End If
770:                oRs.MoveNext()
780:            Loop
790:            oRs.Close()
            End If

            'packages
800:        SQL(0) = "SELECT PackageAttr.PackageName, PackageAttr.PackID, NextRun,PCName,TaskManager.Status AS Status, " & _
                  "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                  "(PackageAttr INNER JOIN ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID) " & _
                  "INNER JOIN TaskManager ON PackageAttr.PackID = TaskManager.PackID"

810:        oRs = clsMarsData.GetData(SQL(0))

820:        If Not oRs Is Nothing Then
830:            Do While oRs.EOF = False
                    Dim sTag As String = "Package:" & oRs(1).Value

840:                If searchListView(sTag) = False Then
850:                    lsv = New ListViewItem
860:                    lsv.Text = oRs("packagename").Value
                        lsv.Tag = "Package:" & oRs(1).Value

870:                    If clsMarsData.IsScheduleDynamic(oRs(1).Value, "Package") = True Then
880:                        lsv.ImageIndex = 3
890:                    ElseIf clsMarsData.IsScheduleDataDriven(oRs(1).Value, "Package") = True Then
900:                        lsv.ImageIndex = 9
910:                    Else
920:                        lsv.ImageIndex = 0
                        End If

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

930:                    With lsv.SubItems
940:                        .Add(Convert.ToString(val))
950:                        .Add(oRs("pcname").Value)
960:                        .Add(Me.getTaskStatus(IsNull(oRs("processid").Value))) '(oRs("status").Value)
970:                        .Add(IsNull(oRs("processid").Value))
980:                        .Add(duration)
                        End With

990:                    lsvProcessing.Items.Add(lsv)
                    End If

1000:               oRs.MoveNext()
1010:           Loop
1020:           oRs.Close()
            End If


            'event based packages
1030:       SQL(0) = "SELECT EventPackageAttr.PackageName, EventPackageAttr.EventPackID, NextRun,PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "(EventPackageAttr INNER JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) " & _
                 "INNER JOIN TaskManager ON EventPackageAttr.EventPackID = TaskManager.EventPackID"


1040:       oRs = clsMarsData.GetData(SQL(0))

1050:       If Not oRs Is Nothing Then
1060:           Do While oRs.EOF = False
                    Dim sTag As String = "Event-Package:" & oRs(1).Value

1070:               If searchListView(sTag) = False Then
1080:                   lsv = New ListViewItem
1090:                   lsv.Text = oRs("packagename").Value
1100:                   lsv.Tag = sTag

1110:                   lsv.ImageIndex = 6

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

1120:                   With lsv.SubItems
1130:                       .Add(Convert.ToString(val))
1140:                       .Add(oRs("pcname").Value)
1150:                       .Add(Me.getTaskStatus(IsNull(oRs("processid").Value))) '(oRs("status").Value)
1160:                       .Add(IsNull(oRs("processid").Value))
1170:                       .Add(duration)
                        End With

1180:                   lsvProcessing.Items.Add(lsv)
                    End If
1190:               oRs.MoveNext()
1200:           Loop
1210:           oRs.Close()
            End If

            'automation schedules
1220:       SQL(0) = "SELECT AutomationAttr.AutoName, AutomationAttr.AutoID, NextRun,PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "(AutomationAttr INNER JOIN ScheduleAttr ON AutomationAttr.AutoID = ScheduleAttr.AutoID) " & _
                 "INNER JOIN TaskManager ON AutomationAttr.AutoID = TaskManager.AutoID"

1230:       oRs = clsMarsData.GetData(SQL(0))

1240:       If oRs IsNot Nothing Then
1250:           Do While oRs.EOF = False
                    Dim sTag As String = "Automation:" & oRs(1).Value
                    Dim nIndex As Integer = 1 'clsMarsData.GetColumnIndex("autoid", oRs)

1260:               If searchListView(sTag) = False Then
1270:                   lsv = New ListViewItem
1280:                   lsv.Text = oRs("autoname").Value
                        lsv.Tag = "Automation:" & oRs(nIndex).Value

1290:                   lsv.ImageIndex = 2

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

1300:                   With lsv.SubItems
1310:                       .Add(Convert.ToString(val))
1320:                       .Add(oRs("pcname").Value)
1330:                       .Add(Me.getTaskStatus(IsNull(oRs("processid").Value))) '(oRs("status").Value)
1340:                       .Add(IsNull(oRs("processid").Value))
1350:                       .Add(duration)
                        End With

1360:                   lsvProcessing.Items.Add(lsv)
                    End If

1370:               oRs.MoveNext()
1380:           Loop

1390:           oRs.Close()
            End If

            'event-based schedules
1400:       SQL(0) = "SELECT EventAttr6.EventName, EventAttr6.EventID, PCName,TaskManager.Status AS Status, " & _
                 "TaskManager.ProcessID AS ProcessID, TaskManager.EntryDate AS EntryDate FROM " & _
                 "EventAttr6 INNER JOIN TaskManager ON EventAttr6.EventID = TaskManager.EventID WHERE (EventAttr6.PackID =0 OR EventAttr6.PackID IS NULL)"

1410:       oRs = clsMarsData.GetData(SQL(0))

1420:       If oRs IsNot Nothing Then
1430:           Do While oRs.EOF = False
                    Dim nIndex As Integer = 1
                    Dim sTag As String = "Event:" & oRs(nIndex).Value

1440:               If searchListView(sTag) = False Then
1450:                   lsv = New ListViewItem
1460:                   lsv.Text = oRs(0).Value
                        lsv.Tag = "Event:" & oRs(nIndex).Value

1470:                   lsv.ImageIndex = 7

                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)
                        Dim duration As Double = Math.Round(CTimeZ(Date.Now, dateConvertType.READ).Subtract(val).TotalMinutes, 2)

1480:                   With lsv.SubItems
1490:                       .Add(Convert.ToString(val))
1500:                       .Add(oRs("pcname").Value)
1510:                       .Add(Me.getTaskStatus(IsNull(oRs("processid").Value))) '(oRs("status").Value)
1520:                       .Add(IsNull(oRs("processid").Value))
1530:                       .Add(duration)
                        End With

1540:                   lsvProcessing.Items.Add(lsv)
                    End If

1550:               oRs.MoveNext()
1560:           Loop

1570:           oRs.Close()
            End If

        Catch ex As Exception
            '_ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        Finally
            Me.lsvProcessing.EndUpdate()
        End Try

    End Sub
    Public Sub ScheduleMonitor(Optional ByVal sFilter As String = "None")

        clsMarsData.WriteData("DELETE FROM TaskManager WHERE monitorid IS NULL", False)

10:     If IsLoaded = False Then Return

        Try
            Dim SQL() As String
            Dim oRs As ADODB.Recordset
            Dim lsv As ListViewItem
            Dim I As Integer
            Dim nCheck As Integer = 0
            Dim KwikExit As Boolean = False
            Dim sUnit As String = "d"

20:         Select Case sFilter
                Case "None"
30:                 sUnit = "d"
40:                 nCheck = 0
50:             Case "Being Processed"
60:                 KwikExit = True
70:             Case "Due Today -All"
80:                 sUnit = "d"
90:                 nCheck = 0
100:            Case "Due Today - Next 5 Minutes"
110:                sUnit = "n"
120:                nCheck = 5
130:            Case "Due Today - Next 30 Minutes"
140:                sUnit = "n"
150:                nCheck = 30
160:            Case "Due Today - Next 60 Minutes"
170:                sUnit = "n"
180:                nCheck = 60
190:            Case "Due in Next 24 Hours"
200:                sUnit = "n"
210:                nCheck = 1440
            End Select

220:        If gConType = "DAT" Then
230:            sUnit = "'" & sUnit & "'"
            End If

240:        Me.lsvTaskManager.BeginUpdate()

250:        Me.lsvTaskManager.Items.Clear()

            ReDim SQL(3)

            'now add the schedules which are waiting to get processed
1600:       SQL(0) = "SELECT ReportTitle,ReportAttr.ReportID, NextRun FROM ReportAttr, ScheduleAttr WHERE " & _
                 "(ReportAttr.ReportID = ScheduleAttr.ReportID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
                 "ReportAttr.ReportID NOT IN (SELECT ReportID FROM TaskManager) AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE'"


1610:       SQL(1) = "SELECT PackageName, PackageAttr.PackID, NextRun FROM PackageAttr, ScheduleAttr WHERE " & _
                 "(PackageAttr.PackID = ScheduleAttr.PackID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
                 "PackageAttr.PackID NOT IN (SELECT PackID FROM TaskManager) AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE'"

1620:       SQL(2) = "SELECT AutoName, AutomationAttr.AutoID, NextRun FROM AutomationAttr, ScheduleAttr WHERE " & _
                 "(AutomationAttr.AutoID = ScheduleAttr.AutoID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE' AND " & _
                 "AutomationAttr.AutoID NOT IN (SELECT AutoID FROM TaskManager)"

1630:       SQL(3) = "SELECT PackageName, EventPackageAttr.EventPackID, NextRun FROM EventPackageAttr, ScheduleAttr WHERE " & _
                 "(EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) AND " & _
                 "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
                 "AND Frequency <> 'NONE' AND " & _
                 "EventPackageAttr.EventPackID NOT IN (SELECT EventPackID FROM TaskManager)"

1640:       For I = 0 To 3

1650:           If gConType = "DAT" Then
1660:               SQL(I) = SQL(I).Replace("[CurrentDate]", "Now()")
1670:           Else
1680:               SQL(I) = SQL(I).Replace("[CurrentDate]", "GetDate()")
                End If

1690:           oRs = clsMarsData.GetData(SQL(I))


1700:           If Not oRs Is Nothing Then
1710:               Do While oRs.EOF = False
                        Dim value As String = oRs.Fields(0).Value
1720:                   lsv = lsvTaskManager.Items.Add(value)

1730:                   If I = 0 Then
                            lsv.Tag = "Report:" & oRs.Fields(1).Value

1740:                       If clsMarsData.IsScheduleBursting(oRs(1).Value) = True Then
1750:                           lsv.ImageIndex = 4
1760:                       ElseIf clsMarsData.IsScheduleDynamic(oRs(1).Value, "Report") = True Then
1770:                           lsv.ImageIndex = 5
                            ElseIf clsMarsData.IsScheduleDataDriven(oRs(1).Value, "Report") = True Then
                                lsv.ImageIndex = 10
1780:                       Else
1790:                           lsv.ImageIndex = 1
                            End If

1800:                   ElseIf I = 1 Then
                            lsv.Tag = "Package:" & oRs.Fields(1).Value

1810:                       If clsMarsData.IsScheduleDynamic(oRs(1).Value, "Package") = True Then
1820:                           lsv.ImageIndex = 3
1830:                       ElseIf clsMarsData.IsScheduleDataDriven(oRs(1).Value, "Package") = True Then
1840:                           lsv.ImageIndex = 9
1850:                       Else
1860:                           lsv.ImageIndex = 0
                            End If


1870:                   ElseIf I = 2 Then
                            lsv.Tag = "Automation:" & oRs.Fields(1).Value
1880:                       lsv.ImageIndex = 2


1890:                   ElseIf I = 3 Then
                            lsv.Tag = "Event-Package:" & oRs.Fields(1).Value
1900:                       lsv.ImageIndex = 6
                        End If

1910:                   With lsv.SubItems
1920:                       Dim val As Date = CTimeZ(oRs("NextRun").Value, dateConvertType.READ)
1930:                       .Add(Convert.ToString(val))
1940:                       .Add(Environment.MachineName)
1950:                       .Add("Waiting...")
1960:                       .Add("")
1970:                       .Add("")
                        End With
1980:                   oRs.MoveNext()
1990:               Loop

2000:               oRs.Close()
                End If
2010:       Next

            Dim sortString As String

2020:       If sortOrder = Windows.Forms.SortOrder.Ascending Then
2030:           sortString = " ASC"
2040:       Else
2050:           sortString = " DESC"
            End If

2060:       Try
2070:           clsMarsUI.sortListView(Me.lsvTaskManager, sortString, sortBy, IIf(sortBy = 1, True, False), chkGroup.Checked)
            Catch : End Try

2080:       Try
2090:           If oPersist <> "" Then
2100:               For Each o As ListViewItem In lsvTaskManager.Items
2110:                   If o.Text = oPersist Then
2120:                       o.Selected = True
2130:                       Exit For
                        End If
2140:               Next
                End If
2150:       Catch ex As Exception

            End Try

KWIK_EXIT:
2160:       Me.lsvTaskManager.EndUpdate()
            'scheduler info
2170:       Try
                Dim serviceType As String = gServiceType

2180:           If serviceType = "" Then serviceType = oUI.ReadRegistry("CRDService", "NONE")

                Dim scheduleInfo As String = ""
                Dim ebInfo As String = ""

2190:           Select Case serviceType.ToLower
                    Case "windowsnt"
2200:                   scheduleInfo = ReadTextFromFile(sAppPath & "ntservice.info")

2210:                   If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Last Poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim
                        End If
2220:               Case "windowsapp"
2230:                   scheduleInfo = ReadTextFromFile(sAppPath & "bgservice.info")

2240:                   If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Last Poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim & " seconds"
                        End If
2250:               Case "none"
2260:                   scheduleInfo = "No scheduler is configured. You can configure the scheduler from Options -> Scheduler"
                End Select

2270:           Me.lblScheduler.Text = "SCHEDULER INFO" & vbCrLf & vbCrLf & scheduleInfo & vbCrLf & ebInfo
            Catch : End Try
2280:   Catch ex As Exception

        Finally
2290:       Try
2300:           Me.lsvTaskManager.EndUpdate()
            Catch : End Try
        End Try

    End Sub


    Public Sub ScheduleMonitor_old(Optional ByVal sFilter As String = "None")

10:     If IsLoaded = False Then Return

        Dim SQL() As String
        Dim oRs As ADODB.Recordset
        Dim lsv As ListViewItem
        Dim I As Integer
        Dim nCheck As Integer = 0
        Dim KwikExit As Boolean = False
        Dim sUnit As String = "d"

20:     Select Case sFilter
            Case "None"
30:             sUnit = "d"
40:             nCheck = 0
50:         Case "Being Processed"
60:             KwikExit = True
70:         Case "Due Today -All"
80:             sUnit = "d"
90:             nCheck = 0
100:        Case "Due Today - Next 5 Minutes"
110:            sUnit = "n"
120:            nCheck = 5
130:        Case "Due Today - Next 30 Minutes"
140:            sUnit = "n"
150:            nCheck = 30
160:        Case "Due Today - Next 60 Minutes"
170:            sUnit = "n"
180:            nCheck = 60
190:        Case "Due in Next 24 Hours"
200:            sUnit = "n"
210:            nCheck = 1440
        End Select

220:    If gConType = "DAT" Then
230:        sUnit = "'" & sUnit & "'"
        End If

240:    Try
250:        lsvTaskManager.BeginUpdate()

260:        lsvTaskManager.Items.Clear()

270:        ReDim SQL(0)

280:        SQL(0) = "SELECT ReportAttr.ReportTitle, ReportAttr.ReportID, NextRun,PCName,TaskManager.Status AS Status FROM " & _
      "(ReportAttr INNER JOIN ScheduleAttr ON ReportAttr.ReportID = ScheduleAttr.ReportID) " & _
      "INNER JOIN TaskManager ON ReportAttr.ReportID = TaskManager.ReportID"

            'SQL(0) = "SELECT * FROM TaskManager, ReportAttr WHERE " & _
            '   "(ReportAttr.ReportID = TaskManager.ReportID) AND ReportAttr.PackID = 0"

290:        oRs = clsMarsData.GetData(SQL(0))

300:        If Not oRs Is Nothing Then
310:            Do While oRs.EOF = False
320:                lsv = lsvTaskManager.Items.Add(oRs("reporttitle").Value)
                    lsv.Tag = "Report:" & oRs(1).Value

330:                If clsMarsData.IsScheduleBursting(oRs(1).Value) = True Then
340:                    lsv.ImageIndex = 4
350:                ElseIf clsMarsData.IsScheduleDynamic(oRs(1).Value, "Report") = True Then
360:                    lsv.ImageIndex = 5
370:                Else
380:                    lsv.ImageIndex = 1
                    End If

390:                With lsv.SubItems
                        Dim val As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
400:                    .Add(Convert.ToString((val)))
410:                    .Add(IsNull(oRs("pcname").Value))
420:                    .Add(IsNull(oRs("status").Value))
                    End With

430:                oRs.MoveNext()
440:            Loop
450:            oRs.Close()
            End If

            'SQL(0) = "SELECT * FROM TaskManager, PackageAttr WHERE " & _
            '"(PackageAttr.PackID = TaskManager.PackID)"

460:        SQL(0) = "SELECT PackageAttr.PackageName, PackageAttr.PackID, NextRun,PCName,TaskManager.Status AS Status FROM " & _
      "(PackageAttr INNER JOIN ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID) " & _
      "INNER JOIN TaskManager ON PackageAttr.PackID = TaskManager.PackID"

470:        oRs = clsMarsData.GetData(SQL(0))

480:        If Not oRs Is Nothing Then
490:            Do While oRs.EOF = False
500:                lsv = New ListViewItem
510:                lsv.Text = oRs("packagename").Value
                    lsv.Tag = "Package:" & oRs(1).Value

520:                If clsMarsData.IsScheduleDynamic(oRs(1).Value, "Package") = False Then
530:                    lsv.ImageIndex = 0
540:                Else
550:                    lsv.ImageIndex = 3
                    End If

560:                With lsv.SubItems
                        Dim val As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
570:                    .Add(Convert.ToString(val))
580:                    .Add(oRs("pcname").Value)
590:                    .Add(oRs("status").Value)
                    End With



600:                lsvTaskManager.Items.Add(lsv)

610:                oRs.MoveNext()
620:            Loop
630:            oRs.Close()
            End If


            'SQL(0) = "SELECT * FROM TaskManager, EventPackageAttr WHERE " & _
            '"(EventPackageAttr.EventPackID = TaskManager.EventPackID)"

640:        SQL(0) = "SELECT EventPackageAttr.PackageName, EventPackageAttr.EventPackID, NextRun,PCName,TaskManager.Status AS Status FROM " & _
      "(EventPackageAttr INNER JOIN ScheduleAttr ON EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) " & _
      "INNER JOIN TaskManager ON EventPackageAttr.EventPackID = TaskManager.EventPackID"


650:        oRs = clsMarsData.GetData(SQL(0))

660:        If Not oRs Is Nothing Then
670:            Do While oRs.EOF = False
680:                lsv = New ListViewItem
690:                lsv.Text = oRs("packagename").Value
                    lsv.Tag = "Event-Package:" & oRs(1).Value

700:                lsv.ImageIndex = 6

710:                With lsv.SubItems
                        Dim val As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
720:                    .Add(Convert.ToString(val))
730:                    .Add(oRs("pcname").Value)
740:                    .Add(oRs("status").Value)
                    End With



750:                lsvTaskManager.Items.Add(lsv)

760:                oRs.MoveNext()
770:            Loop
780:            oRs.Close()
            End If

790:        SQL(0) = "SELECT AutomationAttr.AutoName, AutomationAttr.AutoID, NextRun,PCName,TaskManager.Status AS Status FROM " & _
      "(AutomationAttr INNER JOIN ScheduleAttr ON AutomationAttr.AutoID = ScheduleAttr.AutoID) " & _
      "INNER JOIN TaskManager ON AutomationAttr.AutoID = TaskManager.AutoID"

800:        oRs = clsMarsData.GetData(SQL(0))

810:        If oRs IsNot Nothing Then
820:            Do While oRs.EOF = False
                    Dim nIndex As Integer = 1 'clsMarsData.GetColumnIndex("autoid", oRs)

830:                lsv = New ListViewItem
840:                lsv.Text = oRs("autoname").Value
                    lsv.Tag = "Automation:" & oRs(nIndex).Value

850:                lsv.ImageIndex = 2

860:                With lsv.SubItems
                        Dim val As Date = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
870:                    .Add(Convert.ToString(val))
880:                    .Add(oRs("pcname").Value)
890:                    .Add(oRs("status").Value)
                    End With



900:                lsvTaskManager.Items.Add(lsv)

910:                oRs.MoveNext()
920:            Loop

930:            oRs.Close()
            End If

940:        SQL(0) = "SELECT EventAttr6.EventName, EventAttr6.EventID, PCName,TaskManager.Status AS Status,EntryDate FROM " & _
      "EventAttr6 INNER JOIN TaskManager ON EventAttr6.EventID = TaskManager.EventID"

950:        oRs = clsMarsData.GetData(SQL(0))

960:        If oRs IsNot Nothing Then
970:            Do While oRs.EOF = False
                    Dim nIndex As Integer = 1 'clsMarsData.GetColumnIndex("autoid", oRs)

980:                lsv = New ListViewItem
990:                lsv.Text = oRs(0).Value
                    lsv.Tag = "Event:" & oRs(nIndex).Value

1000:               lsv.ImageIndex = 7

1010:               With lsv.SubItems
                        Dim val As Date = CTimeZ(oRs("entrydate").Value, dateConvertType.READ)

1020:                   .Add(Convert.ToString(val))
1030:                   .Add(oRs("pcname").Value)
1040:                   .Add(oRs("status").Value)
                    End With



1050:               lsvTaskManager.Items.Add(lsv)

1060:               oRs.MoveNext()
1070:           Loop

1080:           oRs.Close()
            End If

1090:       If KwikExit = True Then Exit Sub

1100:       ReDim SQL(3)

1110:       SQL(0) = "SELECT ReportTitle,ReportAttr.ReportID, NextRun FROM ReportAttr, ScheduleAttr WHERE " & _
     "(ReportAttr.ReportID = ScheduleAttr.ReportID) AND " & _
     "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
     "ReportAttr.ReportID NOT IN (SELECT ReportID FROM TaskManager) AND NextRun <= EndDate " & _
     "AND Frequency <> 'NONE'"


1120:       SQL(1) = "SELECT PackageName, PackageAttr.PackID, NextRun FROM PackageAttr, ScheduleAttr WHERE " & _
     "(PackageAttr.PackID = ScheduleAttr.PackID) AND " & _
     "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND " & _
     "PackageAttr.PackID NOT IN (SELECT PackID FROM TaskManager) AND NextRun <= EndDate " & _
     "AND Frequency <> 'NONE'"

1130:       SQL(2) = "SELECT AutoName, AutomationAttr.AutoID, NextRun FROM AutomationAttr, ScheduleAttr WHERE " & _
     "(AutomationAttr.AutoID = ScheduleAttr.AutoID) AND " & _
     "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
     "AND Frequency <> 'NONE' AND " & _
     "AutomationAttr.AutoID NOT IN (SELECT AutoID FROM TaskManager)"

1140:       SQL(3) = "SELECT PackageName, EventPackageAttr.EventPackID, NextRun FROM EventPackageAttr, ScheduleAttr WHERE " & _
     "(EventPackageAttr.EventPackID = ScheduleAttr.EventPackID) AND " & _
     "DATEDIFF(" & sUnit & ", [CurrentDate], NextRun) <= " & nCheck & " AND Status =1 AND NextRun <= EndDate " & _
     "AND Frequency <> 'NONE' AND " & _
     "EventPackageAttr.EventPackID NOT IN (SELECT EventPackID FROM TaskManager)"

1150:       For I = 0 To 3

1160:           If gConType = "DAT" Then
1170:               SQL(I) = SQL(I).Replace("[CurrentDate]", "Now()")
1180:           Else
1190:               SQL(I) = SQL(I).Replace("[CurrentDate]", "GetDate()")
                End If

1200:           oRs = clsMarsData.GetData(SQL(I))


1210:           If Not oRs Is Nothing Then
1220:               Do While oRs.EOF = False
                        Dim value As String = oRs.Fields(0).Value
1230:                   lsv = lsvTaskManager.Items.Add(value)

1240:                   If I = 0 Then
                            lsv.Tag = "Report:" & oRs.Fields(1).Value

1250:                       If clsMarsData.IsScheduleBursting(oRs(1).Value) = True Then
1260:                           lsv.ImageIndex = 4
1270:                       ElseIf clsMarsData.IsScheduleDynamic(oRs(1).Value, "Report") = True Then
1280:                           lsv.ImageIndex = 5
1290:                       Else
1300:                           lsv.ImageIndex = 1
                            End If


1310:                   ElseIf I = 1 Then
                            lsv.Tag = "Package:" & oRs.Fields(1).Value

1320:                       If clsMarsData.IsScheduleDynamic(oRs(1).Value, "Package") = False Then
1330:                           lsv.ImageIndex = 0
1340:                       Else
1350:                           lsv.ImageIndex = 3
                            End If


1360:                   ElseIf I = 2 Then
                            lsv.Tag = "Automation:" & oRs.Fields(1).Value
1370:                       lsv.ImageIndex = 2


1380:                   ElseIf I = 3 Then
                            lsv.Tag = "Event-Package:" & oRs.Fields(1).Value
1390:                       lsv.ImageIndex = 6
                        End If

1400:                   With lsv.SubItems
1405:                       Dim val As Date = CTimeZ(oRs("NextRun").Value, dateConvertType.READ)
1410:                       .Add(Convert.ToString(val))
1420:                       .Add(Environment.MachineName)
1430:                       .Add("Waiting...")
                        End With



1440:                   oRs.MoveNext()
1450:               Loop

1460:               oRs.Close()
                End If
1470:       Next

            Dim sortString As String

1480:       If sortOrder = Windows.Forms.SortOrder.Ascending Then
1490:           sortString = " ASC"
1500:       Else
1510:           sortString = " DESC"
            End If

1520:       clsMarsUI.sortListView(Me.lsvTaskManager, sortString, sortBy, IIf(sortBy = 1, True, False), True)

1530:       Try
1540:           If oPersist.Length > 0 Then
1550:               For Each o As ListViewItem In lsvTaskManager.Items
1560:                   If o.Text = oPersist Then
1570:                       o.Selected = True
1580:                       Exit For
                        End If
1590:               Next
                End If
1600:       Catch ex As Exception

            End Try

            'scheduler info
            Try
                Dim serviceType As String = gServiceType

                If serviceType = "" Then serviceType = oUI.ReadRegistry("CRDService", "NONE")

                Dim scheduleInfo As String = ""
                Dim ebInfo As String = ""

1610:           Select Case serviceType.ToLower
                    Case "windowsnt"
1620:                   scheduleInfo = ReadTextFromFile(sAppPath & "ntservice.info")

                        If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Last Poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim
                        End If
1630:               Case "windowsapp"
1640:                   scheduleInfo = ReadTextFromFile(sAppPath & "bgservice.info")

                        If IO.File.Exists(sAppPath & "ebservice.info") Then
                            ebInfo = "Time to next poll (Event-Based Schedules) : " & ReadTextFromFile(sAppPath & "ebservice.info").Trim & " seconds"
                        End If
1650:               Case "none"
1660:                   scheduleInfo = "No scheduler is configured. You can configure the scheduler from Options -> Scheduler"
                End Select

1670:           Me.lblScheduler.Text = "SCHEDULER INFO" & vbCrLf & vbCrLf & scheduleInfo & vbCrLf & ebInfo
            Catch : End Try
1680:   Catch ex As Exception
1690:       If Err.Number <> -2147217885 Then
1700:           _ErrorHandle(ex.Message, Err.Number, _
     "frmSystemMonitor.ScheduleMonitor", Erl())
            End If
        End Try

1710:   lsvTaskManager.EndUpdate()


    End Sub

    Public Sub EmailQueue()
        If IsLoaded = False Then Return
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim lsv As ListViewItem

            SQL = "SELECT * FROM SendWait"

            oRs = clsMarsData.GetData(SQL)

            lsvEmailQueue.Items.Clear()

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    lsv = lsvEmailQueue.Items.Add(oRs("sname").Value)

                    With lsv
                        .Tag = oRs("queuenumber").Value

                        With .SubItems
                            .Add(oRs("addresses").Value)
                            .Add(oRs("sendcc").Value)
                            .Add(oRs("subject").Value)

                            Dim val As Date = CTimeZ(oRs("lastattempt").Value, dateConvertType.READ)
                            .Add(Convert.ToString(val))
                            .Add(oRs("lastresult").Value)

                            Try
                                .Add(IsNull(oRs("messagefile").Value))
                            Catch : End Try
                        End With
                    End With

                    oRs.MoveNext()
                Loop
            End If

            oRs.Close()

            oData.DisposeRecordset(oRs)
            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Sub LoadEmailLog()

        'If gConType = "DAT" Then
            Me.lsvLog.Visible = True
            Me.dgvEmailLog.Visible = False
            Me.lsvLog.Dock = DockStyle.Fill
            LoadEmailLogLSV()
        'Else
        '    Me.dgvEmailLog.Visible = True
        '    Me.lsvLog.Visible = False
        '    Me.dgvEmailLog.Dock = DockStyle.Fill
        '    Me.txtSearch.Enabled = False
        '    LoadEmailLogDGV()
        'End If
    End Sub

    
    Public Sub LoadEmailLogDGV()
        Dim SQL As String = "SELECT logid AS ID, schedulename AS [Schedule Name], sendto AS Recipient, Subject, datesent AS [Date Sent], messagefile AS [Email File] FROM EmailLog ORDER BY logid DESC"

        '//add an unbound column for the icon
        dgvEmailLog.CellBorderStyle = DataGridViewCellBorderStyle.SingleVertical

        clsMarsUI.BusyProgress(10, "Loading email log")

        If Me.dgvEmailLog.DataSource Is Nothing Then
            Try
                Dim col As DataGridViewImageColumn = New DataGridViewImageColumn()
                col.DefaultCellStyle.BackColor = Color.White
                col.DefaultCellStyle.ForeColor = Color.Black
                col.DefaultCellStyle.SelectionBackColor = Color.White
                col.DefaultCellStyle.SelectionForeColor = Color.Black
                col.HeaderText = ""

                Me.dgvEmailLog.Columns.Add(col)

                clsMarsUI.BusyProgress(40, "Loading email log")

                Dim bs As BindingSource
                Dim ds As DataSet = New DataSet

                clsMarsUI.BusyProgress(60, "Loading email log")

                '''''clsMarsData.fillDataset(ds, SQL)

                bs = New BindingSource(ds, ds.Tables(0).TableName)

                Me.dgvEmailLog.VirtualMode = True
                Me.dgvEmailLog.RowCount = 500
                Me.dgvEmailLog.DataSource = bs

                '''''emailLogFilterMan = New DgvFilterPopup.DgvFilterManager(Me.dgvEmailLog)

                Me.dgvEmailLog.Refresh()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            End Try
        Else
            Try
                clsMarsUI.BusyProgress(10, "Updating email log")

                'select the row
                Dim bs As BindingSource = Me.dgvEmailLog.DataSource
                Dim ds As DataSet = New DataSet

                clsMarsUI.BusyProgress(50, "Updating email log")

                '''''clsMarsData.fillDataset(ds, SQL)

                bs.DataSource = ds
                bs.DataMember = ds.Tables(0).TableName
                bs.ResetBindings(False)

                clsMarsUI.BusyProgress(90, "Updating email log")

                Try
                    For Each n As Integer In emailLogRow
                        Me.dgvEmailLog.Rows(n).Selected = True
                    Next

                    dgvEmailLog.FirstDisplayedScrollingRowIndex = emailLogRow(emailLogRow.GetUpperBound(0))
                Catch : End Try

                '//reset the filter
                'emailLogFilterMan.
                For Each entry As DictionaryEntry In emailLogFilter
                    '''''Me.emailLogFilterMan.Item(entry.Key) = entry.Value
                Next
            Catch : End Try
        End If

        clsMarsUI.BusyProgress(100, "Loading email log", True)

    End Sub
    Public Sub LoadEmailLogLSV()
10:     If IsLoaded = False Then Return


20:     If lsvLog.Items.Count = 0 Then
30:         gItemList = Nothing

40:         Try
                Dim SQL As String
                Dim oRs As ADODB.Recordset
                Dim lsv As ListViewItem

50:             lsvLog.BeginUpdate()

60:             SQL = "SELECT * FROM EmailLog ORDER BY DateSent DESC"

70:             oRs = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)

                Dim nTotal As Integer = oRs.RecordCount
                Dim I As Integer = 1

80:             If Not oRs Is Nothing Then
90:                 Do While oRs.EOF = False
100:                    clsMarsUI.BusyProgress(((I / nTotal) * 100), "Loading email log...")

110:                    ReDim Preserve gItemList(I - 1)

120:                    lsv = lsvLog.Items.Add(oRs("schedulename").Value)

130:                    With lsv
140:                        .Tag = oRs("logid").Value

150:                        With .SubItems
160:                            .Add(IsNull(oRs("sendto").Value, ""))
170:                            .Add(IsNull(oRs("subject").Value, ""))

                                Dim val As Date = CTimeZ(IsNull(oRs("datesent").Value, Now), dateConvertType.READ)

180:                            .Add(Convert.ToString(val))

190:                            Try
200:                                .Add(IsNull(oRs("messagefile").Value, ""))
210:                            Catch ex As Exception
220:                                .Add("")
                                End Try
                            End With

230:                        .ImageIndex = 4
                        End With

240:                    gItemList(I - 1) = lsv

250:                    oRs.MoveNext()

260:                    I += 1
270:                Loop

280:                oRs.Close()
                End If

290:            oData.DisposeRecordset(oRs)
300:            lsvLog.EndUpdate()

310:            Exit Sub
320:        Catch ex As Exception
330:            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl())
340:        Finally
350:            clsMarsUI.BusyProgress(, , True)
            End Try
360:    Else
370:        Try
                'create a list of existing log ids to exclude from the query
                Dim logList As String = ""

                For Each it As ListViewItem In Me.lsvLog.Items
                    logList &= it.Tag & ","
                Next

                If logList = "" Then Return

                'remove the last comma
                logList = logList.Remove(logList.Length - 1, 1)
                logList = "(" & logList & ")"

                Dim SQL As String = "SELECT * FROM EmailLog WHERE LogID NOT IN " & logList & " ORDER BY DateSent DESC"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)
                Dim nTotal As Integer = oRs.RecordCount
                Dim I As Integer = 1

380:            If oRs IsNot Nothing Then
390:                Do While oRs.EOF = False
                        Dim sTag As String = oRs("logid").Value
                        Dim found As Boolean = False

                        clsMarsUI.BusyProgress((I / nTotal) * 100, "Refreshing Email log...")


460:                    ReDim Preserve gItemList(gItemList.Length)

470:                    lsvLog.BeginUpdate()

                        Dim lsv As ListViewItem = lsvLog.Items.Add(oRs("schedulename").Value)

480:                    With lsv
490:                        .Tag = oRs("logid").Value

500:                        With .SubItems
510:                            .Add(IsNull(oRs("sendto").Value))
520:                            .Add(IsNull(oRs("subject").Value))

                                Dim val As Date = CTimeZ(IsNull(oRs("datesent").Value, Now), dateConvertType.READ)
530:                            .Add(Convert.ToString(val))

540:                            Try
550:                                .Add(IsNull(oRs("messagefile").Value, ""))
560:                            Catch ex As Exception
570:                                .Add("")
                                End Try
                            End With

580:                        .ImageIndex = 4
                        End With

590:                    gItemList(gItemList.Length - 1) = lsv

                        Dim sortString As String = " ASC"

600:                    If logsortOrder = Windows.Forms.SortOrder.Ascending Then
610:                        sortString = " ASC"
620:                    Else
630:                        sortString = " DESC"
                        End If

                        Dim isDate As Boolean = False

640:                    If logsortBy = 3 Then isDate = True

                        Try
650:                        clsMarsUI.sortListView(Me.lsvLog, sortString, logsortBy, isDate)
                        Catch : End Try

660:                    lsvLog.EndUpdate()

                        I += 1
670:                    oRs.MoveNext()
680:                Loop

                    clsMarsUI.BusyProgress(, , True)

690:                oRs.Close()
                End If
700:        Catch ex As Exception
710:            ' _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Finally
                clsMarsUI.BusyProgress(, , True)
            End Try
        End If
    End Sub

    Public Sub SingleTree()
        If IsLoaded = False Then Return
        Try
            Dim SQL As String
            Dim sWhere As String
            Dim oRs As ADODB.Recordset
            Dim oRs1 As ADODB.Recordset
            Dim oRs2 As ADODB.Recordset
            Dim oFreq, oReport, oHistory, oRec As TreeNode
            Dim sOrder As String

            If optSingleDesc.Checked = True Then
                sOrder = " ORDER BY entrydate DESC"
            Else
                sOrder = " ORDER BY entrydate ASC"
            End If

            oRs = clsMarsData.GetData("SELECT DISTINCT(Frequency) FROM ScheduleAttr WHERE ReportID <> 0")

            tvSingle.BeginUpdate()

            grpSingleSort.Enabled = False

            tvSingle.Nodes.Clear()

            If Not oRs Is Nothing And oRs.EOF = False Then
                Do While oRs.EOF = False
                    oFreq = tvSingle.Nodes.Add(oRs("frequency").Value)

                    oFreq.ImageIndex = 2
                    oFreq.SelectedImageIndex = 2

                    oRs1 = clsMarsData.GetData("SELECT ReportTitle, R.ReportID, NextRun FROM " & _
                        "ReportAttr R " & _
                        "INNER JOIN " & _
                        "ScheduleAttr S " & _
                        "ON R.ReportID = S.ReportID " & _
                        "WHERE R.PackID = 0 AND Frequency = '" & oFreq.Text & "'")

                    Do While oRs1.EOF = False
                        oReport = oFreq.Nodes.Add(oRs1("reporttitle").Value)

                        oReport.ImageIndex = 0
                        oReport.SelectedImageIndex = 0

                        oRec = oReport.Nodes.Add("Recurring " & CTimeZ(oRs1("nextrun").Value, dateConvertType.READ))

                        oRec.ImageIndex = 5
                        oRec.SelectedImageIndex = 5

                        oRs2 = clsMarsData.GetData("SELECT * FROM ScheduleHistory " & _
                        "WHERE PackID = 0 AND ReportID = " & oRs1("reportid").Value & sOrder)

                        Do While oRs2.EOF = False
                            If oRs2("success").Value = 1 Then
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - Success")
                                oHistory.ImageIndex = 3
                                oHistory.SelectedImageIndex = 3
                            Else
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - " & oRs2("errmsg").Value)
                                oHistory.ImageIndex = 4
                                oHistory.SelectedImageIndex = 4
                            End If

                            oRs2.MoveNext()
                        Loop

                        oRs2.Close()

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()


                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            Dim oNode As TreeNode

            For Each oNode In tvSingle.Nodes
                oNode.ExpandAll()
            Next

            tvSingle.EndUpdate()

            grpSingleSort.Enabled = True

            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Public Sub PackageTree()
        If IsLoaded = False Then Return
        Try
            Dim SQL As String
            Dim sWhere As String
            Dim oRs As ADODB.Recordset
            Dim oRs1 As ADODB.Recordset
            Dim oRs2 As ADODB.Recordset
            Dim oFreq, oReport, oHistory, oRec As TreeNode
            Dim sOrder As String

            If optPackageDesc.Checked = True Then
                sOrder = " ORDER BY entrydate DESC"
            Else
                sOrder = " ORDER BY entrydate ASC"
            End If

            oRs = clsMarsData.GetData("SELECT DISTINCT(Frequency) FROM ScheduleAttr WHERE PackID <> 0")

            tvPackage.BeginUpdate()

            grpPackageSort.Enabled = False

            tvPackage.Nodes.Clear()

            If Not oRs Is Nothing And oRs.EOF = False Then
                Do While oRs.EOF = False
                    oFreq = tvPackage.Nodes.Add(oRs("frequency").Value)

                    oFreq.ImageIndex = 2
                    oFreq.SelectedImageIndex = 2

                    oRs1 = clsMarsData.GetData("SELECT PackageName, P.PackID, NextRun FROM " & _
                        "PackageAttr P " & _
                        "INNER JOIN " & _
                        "ScheduleAttr S " & _
                        "ON P.PackID = S.PackID " & _
                        "WHERE Frequency = '" & oFreq.Text & "'")

                    Do While oRs1.EOF = False
                        oReport = oFreq.Nodes.Add(oRs1("packagename").Value)

                        oReport.ImageIndex = 1
                        oReport.SelectedImageIndex = 1

                        oRec = oReport.Nodes.Add("Recurring " & CTimeZ(oRs1("nextrun").Value, dateConvertType.READ))

                        oRec.ImageIndex = 5
                        oRec.SelectedImageIndex = 5

                        oRs2 = clsMarsData.GetData("SELECT * FROM ScheduleHistory " & _
                        "WHERE PackID = " & oRs1("packid").Value & sOrder)

                        Do While oRs2.EOF = False
                            If oRs2("success").Value = 1 Then
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - Success")
                                oHistory.ImageIndex = 3
                                oHistory.SelectedImageIndex = 3
                            Else
                                oHistory = oReport.Nodes.Add(CTimeZ(oRs2("entrydate").Value, dateConvertType.READ) & " - " & oRs2("errmsg").Value)
                                oHistory.ImageIndex = 4
                                oHistory.SelectedImageIndex = 4
                            End If

                            oRs2.MoveNext()
                        Loop

                        oRs2.Close()

                        oRs1.MoveNext()
                    Loop

                    oRs1.Close()


                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If


            Dim oNode As TreeNode

            For Each oNode In tvPackage.Nodes
                oNode.ExpandAll()
            Next

            tvPackage.EndUpdate()

            grpPackageSort.Enabled = True
            Exit Sub
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub optSingleAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optSingleAsc.CheckedChanged
        If IsLoaded = False Then Return

        grpSingleSort.Enabled = False

        oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " ASC ", _
        chkSingleFilter.Checked)
        grpSingleSort.Enabled = True
    End Sub

    Private Sub optSingleDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optSingleDesc.CheckedChanged

        If IsLoaded = False Then Return

        grpSingleSort.Enabled = False

        oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " DESC ", _
        chkSingleFilter.Checked)

        grpSingleSort.Enabled = True
    End Sub

    Private Sub optPackageAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPackageAsc.CheckedChanged
        If Me.Visible = False Then Return

        If optPackageAsc.Checked = True Then


            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " ASC ", _
            chkPackageFilter.Checked)
            grpSingleSort.Enabled = True
        End If
    End Sub

    Private Sub optPackageDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPackageDesc.CheckedChanged
        If Me.Visible = False Then Return

        If optPackageDesc.Checked = True Then


            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " DESC ", _
            chkPackageFilter.Checked)

            grpSingleSort.Enabled = True
        End If
    End Sub



    Private Sub cmdRemoveOne_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveOne.Click
        If lsvEmailQueue.SelectedItems.Count = 0 Then Exit Sub

        Dim SQL As String
        Dim lsv As ListViewItem

        For Each lsv In lsvEmailQueue.SelectedItems
            Try
                Dim path As String = lsv.SubItems(6).Text

                IO.File.Delete(path)
            Catch : End Try

            SQL = "DELETE FROM SendWait WHERE QueueNumber = " & lsv.Tag

            clsMarsData.WriteData(SQL)

            lsvEmailQueue.Items.Remove(lsv)
        Next
    End Sub

    Private Sub cmdRemoveAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveAll.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim Response As DialogResult

        Response = MessageBox.Show("Clear all mails from the queue?", Application.ProductName, MessageBoxButtons.YesNo)

        If Response = Windows.Forms.DialogResult.Yes Then
            For Each lsv As ListViewItem In Me.lsvEmailQueue.Items
                Try
                    Dim path As String = lsv.SubItems(6).Text

                    Try
                        IO.File.Delete(path)
                    Catch : End Try
                Catch : End Try
            Next

            SQL = "DELETE FROM SendWait"

            clsMarsData.WriteData(SQL)
        End If

        lsvEmailQueue.Items.Clear()
    End Sub

    Private Sub cmdSendNow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSendNow.Click
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim lsv As ListViewItem
        Dim oMsg As clsMarsMessaging = New clsMarsMessaging
        Dim Success As Boolean

        'database values
        Dim Addresses As String
        Dim Subject As String
        Dim Body As String
        Dim SendType As String
        Dim Attach As String
        Dim NumAttach As Integer
        Dim Extras As String
        Dim SendCC As String
        Dim SendBcc As String
        Dim sName As String
        Dim InBody As String
        Dim sFormat As String
        Dim sFiles As String

        If lsvEmailQueue.SelectedItems.Count = 0 Then Exit Sub

        For Each lsv In lsvEmailQueue.SelectedItems
            SQL = "SELECT * FROM SendWait WHERE QueueNumber = " & lsv.Tag

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing And oRs.EOF = False Then
                Addresses = IsNull(oRs("addresses").Value)
                Subject = IsNull(oRs("subject").Value)
                Body = IsNull(oRs("body").Value)
                SendType = IsNull(oRs("sendtype").Value)
                Attach = IsNull(oRs("attach").Value)
                NumAttach = IsNull(oRs("numattach").Value)
                Extras = IsNull(oRs("extras").Value)
                SendCC = IsNull(oRs("sendcc").Value)
                SendBcc = IsNull(oRs("sendbcc").Value)
                sName = IsNull(oRs("sname").Value)
                InBody = IsNull(oRs("inbody").Value)
                sFormat = IsNull(oRs("sformat").Value)
                sFiles = IsNull(oRs("filelist").Value)
                rptFileNames = sFiles.Split("|")
            End If

            Select Case MailType
                Case MarsGlobal.gMailType.MAPI
                    Success = clsMarsMessaging.SendMAPI(Addresses, Subject, Body, SendType, Attach, _
                    NumAttach, Extras, SendCC, SendBcc, Convert.ToBoolean(InBody), _
                    sFormat, sName, False)

                Case MarsGlobal.gMailType.SMTP, MarsGlobal.gMailType.SQLRDMAIL
                    Dim objSender As clsMarsMessaging = New clsMarsMessaging

                    Success = objSender.SendSMTP(Addresses, Subject, Body, SendType, Attach, _
                    NumAttach, Extras, SendCC, SendBcc, sName, Convert.ToBoolean(InBody), _
                    sFormat, False)
            End Select

            If Success = True Then
                clsMarsData.WriteData("DELETE FROM SendWait WHERE QueueNumber =" & lsv.Tag)

                lsvEmailQueue.Items.Remove(lsv)
            End If
        Next

    End Sub


    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged
        If IsLoaded = False Then Return

        Dim oEvent As New clsMarsEvent

        If optAsc.Checked = True Then _
        oEvent.DrawEventHistory(tvHistory, , "ASC", chkEventFilter.Checked)
    End Sub

    Private Sub optDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDesc.CheckedChanged
        If IsLoaded = False Then Return
        Dim oEvent As New clsMarsEvent

        If optDesc.Checked = True Then _
        oEvent.DrawEventHistory(tvHistory, , "DESC", chkEventFilter.Checked)
    End Sub


    Private Sub optAutoAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAutoAsc.CheckedChanged
        If IsLoaded = False Then Return

        If optAutoAsc.Checked = True Then

            Dim sOrder As String

            oSchedule.DrawScheduleHistory(tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , " ASC ", _
            chkAutoFilter.Checked)
        End If
    End Sub

    Private Sub optAutoDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAutoDesc.CheckedChanged
        If IsLoaded = False Then Return

        If optAutoDesc.Checked = True Then

            Dim sOrder As String

            oSchedule.DrawScheduleHistory(tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , _
            " DESC ", chkAutoFilter.Checked)
        End If
    End Sub

    Private Sub cmdClearLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim SQL As String
        SQL = "DELETE FROM EmailLog"

        clsMarsData.WriteData(SQL)

        lsvLog.Items.Clear()

        Dim emailPath As String = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages")

        If IO.Directory.Exists(emailPath) = True Then
            For Each s As String In IO.Directory.GetFiles(emailPath)
                Try
                    IO.File.Delete(s)
                Catch : End Try
            Next
        End If
    End Sub

    Private Sub killThread(ByVal nID As Integer, ByVal type As clsMarsScheduler.enScheduleType)
        Try

            Dim colName As String

            Select Case type
                Case clsMarsScheduler.enScheduleType.AUTOMATION
                    colName = "AutoID"
                Case clsMarsScheduler.enScheduleType.EVENTBASED
                    colName = "EventID"
                Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                    colName = "EventPackID"
                Case clsMarsScheduler.enScheduleType.PACKAGE
                    colName = "PackID"
                Case clsMarsScheduler.enScheduleType.REPORT
                    colName = "ReportID"
                Case Else
                    Return
            End Select

            Dim SQL As String = "SELECT ProcessID FROM ThreadManager WHERE " & colName & " = " & nID
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    Dim procID As Integer = IsNull(oRs("processid").Value, 0)

                    Try
                        Dim proc As Process = Process.GetProcessById(procID, Environment.MachineName)

                        proc.Kill()
                    Catch : End Try
                End If

                oRs.Close()
            End If

        Catch : End Try
    End Sub

    Private Sub _RemoveFromQueue(ByVal sName As String, ByVal nID As Integer, ByVal sType As String, ByVal Log As Boolean)
        Try
            Dim SQL As String
            Dim ScheduleID As Integer
            Dim oRs As ADODB.Recordset
            Dim NextRun As Date

            Select Case sType.ToLower
                Case "report"

                    sType = "single report"

                    If clsMarsData.IsDuplicate("TaskManager", "ReportID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.REPORT)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(nID)


                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = lsvTaskManager.SelectedItems(0).SubItems(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                Else
                                    Force2Moro = True
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try


                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))



                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                             ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE ReportID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE ReportID = " & nID, False)
                        End If
                    End If
                Case "package"
                    If clsMarsData.IsDuplicate("TaskManager", "PackID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.PACKAGE)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(, nID)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = lsvTaskManager.SelectedItems(0).SubItems(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                            ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE PackID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE PackID = " & nID, False)
                        End If
                    End If
                Case "automation", "backup", "retry"
                    If clsMarsData.IsDuplicate("TaskManager", "AutoID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.AUTOMATION)
                    End If

                    If nID = 99999 Or nID = 9998 Then GoTo ClearData

                    oSchedule.TaskManager("Remove", , , nID)
                    oSchedule.ThreadManager(nID, "Remove", clsMarsScheduler.enScheduleType.AUTOMATION)

                    ScheduleID = clsMarsScheduler.GetScheduleID(, , nID)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = lsvTaskManager.SelectedItems(0).SubItems(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                             ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            If Log = True Then
                                oSchedule.SetScheduleHistory(False, "User remove schedule from queue", , nID)
                            End If

ClearData:
                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE AutoID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE AutoID = " & nID, False)
                        End If
                    End If
                Case "event-package"
                    If clsMarsData.IsDuplicate("TaskManager", "EventPackID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.EVENTPACKAGE)
                    End If

                    ScheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)

                    oSchedule.TaskManager("Remove", , , , Log, nID)

                    oRs = clsMarsData.GetData("SELECT * FROM ScheduleAttr WHERE " & _
                    "ScheduleID = " & ScheduleID)

                    If Not oRs Is Nothing Then
                        If oRs.EOF = False Then

                            Dim DueTime As String = ""
                            Dim ForceNext As Boolean = False
                            Dim Force2Moro As Boolean = False

                            Try
                                Dim Repeat As Boolean = Convert.ToBoolean(oRs("repeat").Value)
                                Dim CurrentDue As String = lsvTaskManager.SelectedItems(0).SubItems(1).Text
                                Dim CurrentDueDate As Date = Convert.ToDateTime(CurrentDue)

                                If Repeat = True And CurrentDueDate > Now Then
                                    Dim RptUntil As Date = Convert.ToDateTime(Now.Date & " " & ConTime(oRs("repeatuntil").Value))

                                    If CurrentDueDate < RptUntil Then
                                        DueTime = ConDateTime(CurrentDueDate)

                                        ForceNext = True
                                    Else
                                        DueTime = ""
                                        ForceNext = False
                                        Force2Moro = True
                                    End If
                                End If
                            Catch
                                DueTime = ""
                                ForceNext = False
                                Force2Moro = False
                            End Try

                            NextRun = oSchedule.GetNextRun(ScheduleID, oRs("frequency").Value, _
                                oRs("startdate").Value, _
                                oRs("starttime").Value, _
                                Convert.ToBoolean(oRs("repeat").Value), _
                                oRs("repeatinterval").Value, oRs("repeatuntil").Value, _
                                IsNull(oRs("calendarname").Value), Force2Moro, ForceNext, DueTime, IsNull(oRs("repeatunit").Value, "hours"))

                            SQL = "UPDATE ScheduleAttr SET NextRun ='" & _
                            ConDate(NextRun) & " " & ConTime(NextRun) & "' WHERE ScheduleID = " & ScheduleID

                            clsMarsData.WriteData(SQL)

                            clsMarsData.WriteData("DELETE FROM ThreadManager WHERE EventPackID = " & nID, False)
                            clsMarsData.WriteData("DELETE FROM TaskManager WHERE EventPackID = " & nID, False)
                        End If
                    End If
                Case "event"
                    If clsMarsData.IsDuplicate("TaskManager", "EventID", nID, False) = True Then
                        Me.killThread(nID, clsMarsScheduler.enScheduleType.EVENTBASED)
                    End If

                    clsMarsScheduler.globalItem.TaskManager("Remove", , , , Log, , nID)

                    clsMarsData.WriteData("DELETE FROM ThreadManager WHERE EventID =" & nID, False)
                    clsMarsData.WriteData("DELETE FROM TaskManager WHERE EventID = " & nID, False)

                    clsMarsEvent.SaveEventHistory(nID, False, "Schedule execution was manually cancelled by user")
            End Select

            Dim errorMsg As String = sType & " schedule:".ToUpper & " " & sName & ": Schedule manually remove from queue."

            _ErrorHandle(errorMsg, -214790210, "_RemoveFromQueue", 0, "", True, True)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Private Sub mnuRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemove.Click, mnuProcEnd.Click
        Dim lsv As ListView
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "mnuremove", True) = 0 Then
            lsv = Me.lsvTaskManager
        Else
            lsv = Me.lsvProcessing
        End If

        If lsv.SelectedItems.Count = 0 Then Return

        Dim sType As String
        Dim nID As Integer

        For Each oItem As ListViewItem In lsv.SelectedItems
            sType = oItem.Tag.Split(":")(0)
            nID = oItem.Tag.Split(":")(1)

            _RemoveFromQueue(oItem.Text, nID, sType, True)

            oItem.Remove()
        Next
    End Sub

    Private Sub cmbFilter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFilter.SelectedIndexChanged
        ScheduleMonitor(cmbFilter.Text)
    End Sub

    Public Sub DeferedList(Optional ByVal SortOrder As String = "ASC")
        If IsLoaded = False Then Return

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM DeferDeliveryAttr ORDER BY DueDate " & SortOrder

        oRs = clsMarsData.GetData(SQL)

        lsvDefer.Items.Clear()

        lsvDefer.BeginUpdate()

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                With oItem
                    .Text = oRs("filename").Value
                    .Tag = oRs("deferid").Value

                    With .SubItems
                        .Add(oRs("filepath").Value)
                        .Add(Convert.ToString(CTimeZ(oRs("duedate").Value, dateConvertType.READ)))
                    End With

                    lsvDefer.Items.Add(oItem)
                End With

                oRs.MoveNext()
            Loop

            lsvDefer.EndUpdate()

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub optDeferAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDeferAsc.CheckedChanged
        If IsLoaded = False Then Return

        If optDeferAsc.Checked = True Then
            DeferedList("ASC")
        End If
    End Sub

    Private Sub optDeferDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDeferDesc.CheckedChanged
        If IsLoaded = False Then Return

        If optDeferDesc.Checked = True Then
            DeferedList("DESC")
        End If
    End Sub

    Private Sub mnuDeferDeliver_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeferDeliver.Click
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim oReport As New clsMarsReport
        Dim Ok As Boolean

        Dim nReportID As Integer = 0
        Dim nPackID As Integer = 0
        Dim oTask As New clsMarsTask
        Dim oUI As New clsMarsUI
        Dim nScheduleID As Integer
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        For Each oItem As ListViewItem In lsvDefer.SelectedItems
            oUI.BusyProgress(60, "Performing the requested action")

            SQL = "SELECT x.ReportID,x.PackID FROM DeferDeliveryAttr d INNER JOIN DestinationAttr x ON d.DestinationID = " & _
                                        "x.DestinationID WHERE d.DeferID =" & oItem.Tag

            oRs = clsMarsData.GetData(SQL)

            Try
                If oRs.EOF = False Then
                    nReportID = oRs.Fields(0).Value
                    nPackID = oRs.Fields(1).Value
                End If

                oRs.Close()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try

            Ok = oReport.DeliverDefered(lsvDefer.SelectedItems(0).Tag)

            If Ok = True Then
                oUI.BusyProgress(75, "Getting schedule properties...")

                If nReportID > 0 Then
                    nScheduleID = clsMarsScheduler.GetScheduleID(nReportID)
                Else
                    nScheduleID = clsMarsScheduler.GetScheduleID(, nPackID)
                End If

                oUI.BusyProgress(90, "Setting schedule history...")

                oSchedule.SetScheduleHistory(True, "Deferred delivery peformed successfully", nReportID, nPackID)

                oUI.BusyProgress(95, "Performing custom actions...")

                oTask.TaskPreProcessor(nScheduleID, clsMarsTask.enRunTime.AFTERDEL)

                oUI.BusyProgress(100, "Cleaning up...")

                clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr WHERE DeferID =" & oItem.Tag, False)

                oItem.Remove()

                oUI.BusyProgress(, , True)

                MessageBox.Show("Report delivered successfully!", Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)
            Else
                _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine)
                oSchedule.SetScheduleHistory(False, gErrorDesc & " [" & gErrorNumber & "]", nReportID)
            End If
        Next
    End Sub

    Private Sub mnuDeferRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDeferRemove.Click
        If lsvDefer.SelectedItems.Count = 0 Then Return

        Dim oRes As DialogResult

        oRes = MessageBox.Show("Remove the selected delivery from the list?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Yes Then
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            Dim nScheduleID As Integer
            Dim nReportID As Integer

            For Each oItem As ListViewItem In lsvDefer.SelectedItems

                SQL = "SELECT x.ReportID FROM DeferDeliveryAttr d INNER JOIN DestinationAttr x ON d.DestinationID = " & _
                "x.DestinationID WHERE d.DeferID =" & oItem.Tag

                oRs = clsMarsData.GetData(SQL)

                Try
                    If oRs.EOF = False Then
                        nReportID = oRs.Fields(0).Value
                    End If

                    oRs.Close()

                    nScheduleID = clsMarsScheduler.GetScheduleID(nReportID)

                    oSchedule.SetScheduleHistory(False, "Deferred delivery removed from queue by user", nReportID)

                    clsMarsData.WriteData("DELETE FROM DeferDeliveryAttr WHERE DeferID =" & oItem.Tag, False)

                    oItem.Remove()

                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            Next
        End If
    End Sub

    Private Sub optDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optDaily.Checked = True Then
            Label5.Text = "days"
            Label9.Text = "days"
        End If
    End Sub

    Private Sub optWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optWeekly.Checked = True Then
            Label5.Text = "weeks"
            Label9.Text = "weeks"
        End If
    End Sub

    Private Sub optMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optMonthly.Checked = True Then
            Label5.Text = "months"
            Label9.Text = "months"
        End If
    End Sub

    Private Sub optYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If optYearly.Checked = True Then
            Label5.Text = "years"
            Label9.Text = "years"
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim fbd As New FolderBrowserDialog

        fbd.Description = "Please select the path to backup to (a subfolder will be created for each backup)"

        fbd.ShowNewFolderButton = False

        fbd.ShowDialog()

        If fbd.SelectedPath.Length = 0 Then
            Return
        Else
            txtPath.Text = _CreateUNC(fbd.SelectedPath)
        End If

    End Sub

    Private Sub cmdSaveBackup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSaveBackup.Click
        ' for status
        '0 = no scheduling
        '1 = simple
        '2 = advanced
        '3 = advanced with zip

        Dim sPath As String = sAppPath & "backup.xml"
        Dim SQL As String

        If txtPath.Text.Length = 0 Then
            ep.SetError(txtPath, "Please select the backup path")
            txtPath.Focus()
            Return
        End If

        If System.IO.File.Exists(sPath) Then
            Try : System.IO.File.Delete(sPath) : Catch : End Try
        End If
        'if simple is selected
        'If rbSimple.Checked = True Then

        Dim oRs As New ADODB.Recordset

        With oRs
            With .Fields()
                .Append("Frequency", ADODB.DataTypeEnum.adVarChar, 50)
                .Append("RunTime", ADODB.DataTypeEnum.adVarChar, 50)
                .Append("RepeatEvery", ADODB.DataTypeEnum.adInteger)
                .Append("BackupPath", ADODB.DataTypeEnum.adVarWChar, 255)
                .Append("NextRun", ADODB.DataTypeEnum.adDate, 50)
                .Append("KeepFor", ADODB.DataTypeEnum.adInteger)
            End With

            .Open()

            .AddNew()

            If optDaily.Checked = True Then
                .Fields("frequency").Value = "Daily"
            ElseIf optWeekly.Checked = True Then
                .Fields("frequency").Value = "Weekly"
            ElseIf optMonthly.Checked = True Then
                .Fields("frequency").Value = "Monthly"
            ElseIf optYearly.Checked = True Then
                .Fields("frequency").Value = "Yearly"
            End If

            .Fields("runtime").Value = dtBackup.Value.ToString("HH:mm:ss")
            .Fields("nextrun").Value = Now.Date & " " & dtBackup.Value.ToString("HH:mm:ss")
            .Fields("repeatevery").Value = txtInterval.Value
            .Fields("backuppath").Value = txtPath.Text
            .Fields("KeepFor").Value = txtKeep.Value

            .Update()

            clsMarsData.CreateXML(oRs, sPath)

            ''update the scheduleattr table
            'SQL = "Delete from ScheduleAttr where ScheduleID = 11"
            'clsMarsData.WriteData(SQL)

            'Dim sCols As String = "ScheduleID,ReportID,PackID,AutoID"
            'Dim sVals As String = "11,0,0,0"
            'Dim SQL2 As String = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"
            'clsMarsData.WriteData(SQL2)

            'MessageBox.Show("Scheduled backup saved successfully", Application.ProductName, MessageBoxButtons.OK, _
            'MessageBoxIcon.Information)
        End With
        'End If
        'if advanced is selected


        'If rbAdvanced.Checked = True Then
        SQL = "Delete from ScheduleAttr where ScheduleID = 11"
        clsMarsData.WriteData(SQL)
        Dim sCols As String = "ScheduleID,ReportID,PackID,AutoID"
        Dim sVals As String = "11,0,0,0"
        Dim SQL2 As String = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"
        clsMarsData.WriteData(SQL2)

        Dim nRepeat As String
        If UcUpdate.chkRepeat.Checked = False Then
            nRepeat = "0"
        Else
            nRepeat = Convert.ToString(UcUpdate.cmbRpt.Value).Replace(",", ".")
        End If

        With UcUpdate
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            Dim sStatus As Int32
            If Me.chkBackups.Checked = False Then
                sStatus = 0
            ElseIf Me.chkBackups.Checked = True And rbSimple.Checked = True Then
                sStatus = 1
            ElseIf chkBackups.Checked = True And rbAdvanced.Checked = True And chkZip.Checked = False Then
                sStatus = 2
            ElseIf chkBackups.Checked = True And rbAdvanced.Checked = True And chkZip.Checked = True Then
                sStatus = 3
            End If

            SQL = "UPDATE ScheduleAttr SET " & _
                "Frequency = '" & .sFrequency & "', " & _
                "EndDate = '" & .m_endDate & "', " & _
                "StartDate = '" & .m_startDate & "'," & _
                "NextRun = '" & .m_nextRun & "', " & _
                "StartTime = '" & .m_startTime & "', " & _
                "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "RepeatInterval = '" & nRepeat & "'," & _
                "Status = " & sStatus & "," & _
                "RepeatUntil = '" & .m_repeatUntil & "'," & _
                "BPath = '" & SQLPrepare(txtPath.Text) & "'," & _
                "BZipName = '" & SQLPrepare(txtZipName.Text) & "', " & _
                "CalendarName = '" & SQLPrepare(.cmbCustom.Text) & "', " & _
                "UseException = " & Convert.ToInt32(.chkException.Checked) & "," & _
                "ExceptionCalendar ='" & SQLPrepare(.cmbException.Text) & "', " & _
                "RepeatUnit ='" & UcUpdate.m_RepeatUnit & "' " & _
                "where scheduleid = 11"

            clsMarsData.WriteData(SQL)

        End With

        MessageBox.Show("Scheduled backup saved successfully", Application.ProductName, MessageBoxButtons.OK, _
        MessageBoxIcon.Information)

        'End If
    End Sub

    Private Sub txtPath_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPath.TextChanged
        ep.SetError(sender, String.Empty)
    End Sub

    Private Sub chkBackups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBackups.CheckedChanged


        Me.grpBackup.Enabled = chkBackups.Checked
        cmdSaveBackup.Enabled = chkBackups.Checked

        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("AutoBackUp", Convert.ToInt32(chkBackups.Checked))

        If chkBackups.Checked = False Then
            clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 0 WHERE ScheduleID =11")
        Else
            If Me.rbAdvanced.Checked = True Then
                clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 1 WHERE ScheduleID =11")
            End If
        End If
    End Sub

    Private Sub tmScheduleMon_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmScheduleMon.Elapsed
        Dim selItem As ListViewItem

        If lsvTaskManager.SelectedItems.Count > 0 Then
            selItem = Me.lsvTaskManager.SelectedItems(0)
        End If

        ScheduleMonitor(cmbFilter.Text)
        Me.ProcessMonitor()

        If selItem IsNot Nothing Then
            For Each item As ListViewItem In Me.lsvTaskManager.Items
                If item.Tag = selItem.Tag Then
                    item.Selected = True
                    item.EnsureVisible()
                    Exit For
                End If
            Next
        End If

    End Sub

    Private Sub lsvTaskManager_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvProcessing.ColumnClick, lsvTaskManager.ColumnClick
        Me.tmScheduleMon.Stop()

        Dim lView As ListView = CType(sender, ListView)

        lView.BeginUpdate()

        sortBy = e.Column

        Dim sortString As String = " ASC"

        If sortOrder = Windows.Forms.SortOrder.Ascending Then
            sortOrder = Windows.Forms.SortOrder.Descending
            sortString = " DESC"
        Else
            sortOrder = Windows.Forms.SortOrder.Ascending
            sortString = " ASC"
        End If

        'clsMarsUI.sortListView(Me.lsvTaskManager, sortString, e.Column)
        Dim useGroups As Boolean

        If lView.Groups.Count > 0 Then
            useGroups = Me.chkGroup.Checked
        Else
            useGroups = False
        End If

        clsMarsUI.sortListView(lView, sortString, e.Column, IIf(e.Column = 1, True, False), useGroups)

        lView.EndUpdate()
        Me.tmScheduleMon.Start()
    End Sub

    Private Sub RestoreColumns(ByVal lsv As ListView)
        Try
            Dim keyFile As String = sAppPath & lsv.Name & ".cols"

            If IO.File.Exists(keyFile) = False Then Return

            Dim dt As DataTable = New DataTable("Columns")

            dt.ReadXml(keyFile)

            For I As Integer = 0 To lsv.Columns.Count - 1
                Dim rows() As DataRow = dt.Select("ColumnID =" & I)

                If rows IsNot Nothing AndAlso rows.Length > 0 Then
                    Dim row As DataRow = rows(0)

                    lsv.Columns(I).Width = row("ColumnWidth")
                End If
            Next
        Catch : End Try
    End Sub
    Private Sub lsvProcessing_ColumnWidthChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnWidthChangedEventArgs) Handles lsvProcessing.ColumnWidthChanged, lsvTaskManager.ColumnWidthChanged
        If IsLoaded = True Then
            IsLoaded = False

            Dim lsv As ListView = CType(sender, ListView)

            Dim keyFile As String = lsv.Name & ".cols"

            Try
                Dim dt As DataTable = New DataTable("Columns")

                With dt.Columns
                    .Add("ColumnID")
                    .Add("ColumnWidth")
                End With


                Dim hs As Hashtable = New Hashtable

                For I As Integer = 0 To lsv.Columns.Count - 1
                    Dim row As DataRow = dt.Rows.Add
                    row("ColumnID") = I
                    row("ColumnWidth") = lsv.Columns(I).Width
                Next

                If IO.File.Exists(sAppPath & keyFile) Then
                    Try
                        IO.File.Delete(sAppPath & keyFile)
                    Catch : End Try
                End If

                dt.WriteXml(sAppPath & keyFile, XmlWriteMode.WriteSchema)

                dt.Dispose()
            Catch
            Finally
                IsLoaded = True
            End Try
        End If
    End Sub


    Private Sub lsvTaskManager_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvProcessing.SelectedIndexChanged, lsvTaskManager.SelectedIndexChanged
        Dim lView As ListView = CType(sender, ListView)

        If lView.SelectedItems.Count > 0 Then
            oPersist = lView.SelectedItems(0).Text
        Else
            oPersist = String.Empty
        End If
    End Sub

    Private Sub cmdEventRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdEventRefresh.Click
        Dim oEvent As New clsMarsEvent

        If optAsc.Checked = True Then
            oEvent.DrawEventHistory(tvHistory, , "ASC")
        Else
            oEvent.DrawEventHistory(tvHistory, , "DESC")
        End If
    End Sub

    Private Sub cmdEventclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdEventClear.Click
        Dim oRes As DialogResult
        oRes = MessageBox.Show("Delete all Event-Based Schedules' history?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM EventHistory", False)

            cmdEventRefresh_Click(sender, e)
        End If

        MessageBox.Show("Event History cleared!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Private Sub cmdPackClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageClear.Click
        Dim oRes As DialogResult
        oRes = MessageBox.Show("Delete all Package history?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            oSchedule.DeleteAllHistory(clsMarsScheduler.enScheduleType.PACKAGE)

            cmdPackRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdPackRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPackRefresh.Click
        If optPackageAsc.Checked = True Then

            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " ASC ")

            grpSingleSort.Enabled = True
        Else
            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " DESC ")

            grpSingleSort.Enabled = True
        End If
    End Sub

    Private Sub cmdSingleRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSingleRefresh.Click
        If optSingleAsc.Checked = True Then

            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " ASC ")

            grpSingleSort.Enabled = True
        Else
            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " DESC ")

            grpSingleSort.Enabled = True
        End If
    End Sub

    Private Sub cmdAutoClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoClear.Click
        Dim oRes As DialogResult
        oRes = MessageBox.Show("Delete all Automation Schedules' history?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            oSchedule.DeleteAllHistory(clsMarsScheduler.enScheduleType.AUTOMATION)

            cmdAutoRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdAutoRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdAutoRefresh.Click
        If optAutoAsc.Checked = True Then

            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(Me.tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , " ASC ")

            grpSingleSort.Enabled = True
        Else
            grpSingleSort.Enabled = False

            oSchedule.DrawScheduleHistory(Me.tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , " DESC ")

            grpSingleSort.Enabled = True
        End If
    End Sub

    Private Sub cmdAddSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddSchedule.Click
        If tvObjects.SelectedNode Is Nothing Then Return

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim sFreq As String
        Dim sType As String
        Dim nID As Integer
        Dim nLast As Date
        Dim nNext As Date
        Dim ObjectID As Integer

        sType = tvObjects.SelectedNode.Tag.Split(":")(0)
        nID = tvObjects.SelectedNode.Tag.Split(":")(1)

        Select Case sType.ToLower
            Case "folder", "desktop", "report", "package"
                If optrDaily.Checked = True Then
                    sFreq = "Daily"
                ElseIf optrWeekly.Checked = True Then
                    sFreq = "Weekly"
                ElseIf optrMonthly.Checked = True Then
                    sFreq = "Monthly"
                ElseIf optrYearly.Checked = True Then
                    sFreq = "Yearly"
                End If

                nLast = ConDate(Now.Date) & " " & ConTime(dtRunTime.Value)

                nNext = CTimeZ(nLast, dateConvertType.WRITE)
            Case Else
                MessageBox.Show("The selected object does not require refreshing. Please select a folder, report or package", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return
        End Select


        sCols = "RefreshID,ObjectType,Frequency,ObjectName,ObjectID,NextRun,RepeatEvery,StartTime"

        ObjectID = clsMarsData.CreateDataID("refresherattr", "refreshid")

        sVals = ObjectID & "," & _
        "'" & sType & "'," & _
        "'" & sFreq & "'," & _
        "'" & SQLPrepare(tvObjects.SelectedNode.Text) & "'," & _
        nID & "," & _
        "'" & ConDateTime(nNext) & "'," & _
        txtzRpt.Value & "," & _
        "'" & ConTime(CTimeZ(dtRunTime.Value, dateConvertType.WRITE)) & "'"

        SQL = "INSERT INTO  RefresherAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Dim oItem As New ListViewItem

            oItem.Text = sType
            oItem.Tag = ObjectID

            With oItem.SubItems
                .Add(tvObjects.SelectedNode.Text)
                .Add(sFreq)
                .Add("")
                .Add(CType(nNext, String))
                .Add(txtzRpt.Value)
            End With

            lsvSchedules.Items.Add(oItem)
        End If
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Return

        For Each olsv As ListViewItem In lsvSchedules.SelectedItems
            Dim nID As Integer = olsv.Tag

            Dim SQL As String = "DELETE FROM RefresherAttr WHERE RefreshID =" & nID

            If clsMarsData.WriteData(SQL) = True Then
                olsv.Remove()
            End If
        Next
    End Sub

    Private Sub optrDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrDaily.CheckedChanged
        If optrDaily.Checked = True Then
            Label12.Text = "days"
        End If
    End Sub

    Private Sub optrWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrWeekly.CheckedChanged
        If optrWeekly.Checked = True Then
            Label12.Text = "weeks"
        End If
    End Sub

    Private Sub optrMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrMonthly.CheckedChanged
        If optrMonthly.Checked = True Then
            Label12.Text = "months"
        End If
    End Sub

    Private Sub optrYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optrYearly.CheckedChanged
        If optrYearly.Checked = True Then
            Label12.Text = "years"
        End If
    End Sub


    Private Sub lsvSchedules_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvSchedules.ColumnClick

        Me.lsvSchedules.ListViewItemSorter = New ListViewComparer(e.Column)

        'Dim new_sorting_column As ColumnHeader = _
        'lsvSchedules.Columns(e.Column)

        '' Figure out the new sorting order.
        'Dim sort_order As System.Windows.Forms.SortOrder
        'If m_SortingColumn Is Nothing Then
        '    ' New column. Sort ascending.
        '    sort_order = SortOrder.Ascending
        'Else
        '    ' See if this is the same column.
        '    If new_sorting_column.Equals(m_SortingColumn) Then
        '        ' Same column. Switch the sort order.
        '        If m_SortingColumn.Text.StartsWith("> ") Then
        '            sort_order = SortOrder.Descending
        '        Else
        '            sort_order = SortOrder.Ascending
        '        End If
        '    Else
        '        ' New column. Sort ascending.
        '        sort_order = SortOrder.Ascending
        '    End If

        '    ' Remove the old sort indicator.
        '    m_SortingColumn.Text = _
        '        m_SortingColumn.Text.Substring(2)
        'End If

        '' Display the new sort order.
        'm_SortingColumn = new_sorting_column
        'If sort_order = SortOrder.Ascending Then
        '    m_SortingColumn.Text = "> " & m_SortingColumn.Text
        'Else
        '    m_SortingColumn.Text = "< " & m_SortingColumn.Text
        'End If

        '' Create a comparer.
        'lsvSchedules.ListViewItemSorter = New _
        '    ListViewComparer(e.Column, sort_order)

        '' Sort.
        'lsvSchedules.Sort()
    End Sub

    Private Sub tmEmailQueue_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmEmailQueue.Elapsed
        EmailQueue()
    End Sub



    Private Sub chkEventFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEventFilter.CheckedChanged
        Dim oEvent As New clsMarsEvent

        If optDesc.Checked = True Then
            oEvent.DrawEventHistory(tvHistory, , " DESC ", chkEventFilter.Checked)
        Else
            oEvent.DrawEventHistory(tvHistory, , " ASC ", chkEventFilter.Checked)
        End If
    End Sub

    Private Sub chkSingleSort_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSingleFilter.CheckedChanged
        If optSingleDesc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " DESC ", _
            chkSingleFilter.Checked)
        Else
            oSchedule.DrawScheduleHistory(tvSingle, clsMarsScheduler.enScheduleType.REPORT, , " ASC ", _
            chkSingleFilter.Checked)
        End If
    End Sub

    Private Sub chkPackageFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPackageFilter.CheckedChanged
        If optPackageAsc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " ASC ", _
            chkPackageFilter.Checked)
        Else
            oSchedule.DrawScheduleHistory(tvPackage, clsMarsScheduler.enScheduleType.PACKAGE, , " DESC ", _
            chkPackageFilter.Checked)
        End If
    End Sub

    Private Sub chkAutoFilter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoFilter.CheckedChanged
        If optAutoAsc.Checked = True Then

            Dim sOrder As String

            oSchedule.DrawScheduleHistory(tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , " ASC ", _
            chkAutoFilter.Checked)
        Else
            oSchedule.DrawScheduleHistory(tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION, , " DESC ", _
            chkAutoFilter.Checked)
        End If
    End Sub

    Private Sub frmSystemMonitor_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Me.tmEmailQueue.Enabled = False
        Me.tmScheduleMon.Enabled = False
    End Sub

    Private Sub chkAutoRefresh_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoRefresh.CheckedChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("AutoRefresh", Convert.ToInt32(chkAutoRefresh.Checked))

        Panel1.Enabled = chkAutoRefresh.Checked
    End Sub

    Private Sub cmdSingleClear_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdSingleClear.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        cmnuSingleClear.Show(cmdSingleClear, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuSingleClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleClearAll.Click
        Dim oRes As DialogResult
        oRes = MessageBox.Show("Delete all Single Schedule history?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            oSchedule.DeleteAllHistory(clsMarsScheduler.enScheduleType.REPORT)

            cmdSingleRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub mnuSingleClearSel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSingleClearSel.Click
        If tvSingle.SelectedNode Is Nothing Then Return

        Dim nID As Integer

        Try
            Int32.Parse(tvSingle.SelectedNode.Tag)

            nID = tvSingle.SelectedNode.Tag
        Catch ex As Exception
            Return
        End Try

        Dim SQL As String

        SQL = "DELETE FROM ScheduleHistory WHERE ReportID =" & nID

        clsMarsData.WriteData(SQL)

        Try
            tvSingle.SelectedNode.Nodes.Clear()
        Catch : End Try
    End Sub

    Private Sub mnuPackageClearSel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPackageClearSel.Click
        If tvPackage.SelectedNode Is Nothing Then Return

        Dim nID As Integer

        Try
            Int32.Parse(tvPackage.SelectedNode.Tag)

            nID = tvPackage.SelectedNode.Tag
        Catch ex As Exception
            Return
        End Try

        Dim SQL As String

        SQL = "DELETE FROM ScheduleHistory WHERE PackID =" & nID

        clsMarsData.WriteData(SQL)

        Try
            tvPackage.SelectedNode.Nodes.Clear()
        Catch : End Try
    End Sub

    Private Sub cmdPackClear_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdPackClear.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        cmnuPackageClear.Show(cmdPackClear, New Point(e.X, e.Y))
    End Sub

    Private Sub cmdAutoClear_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) _
    Handles cmdAutoClear.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        cmnuAutoClear.Show(cmdAutoClear, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuAutoClearSel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAutoClearSel.Click
        If tvAutoHistory.SelectedNode Is Nothing Then Return

        Dim nID As Integer

        Try
            Int32.Parse(tvAutoHistory.SelectedNode.Tag)

            nID = tvAutoHistory.SelectedNode.Tag
        Catch ex As Exception
            Return
        End Try

        Dim SQL As String

        SQL = "DELETE FROM ScheduleHistory WHERE AutoID =" & nID

        clsMarsData.WriteData(SQL)

        Try
            tvHistory.SelectedNode.Nodes.Clear()
        Catch : End Try
    End Sub

    Private Sub chkOneEmailPerSchedule_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOneEmailPerSchedule.CheckedChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("OneEmailPerFailure", Convert.ToInt32(chkOneEmailPerSchedule.Checked))
    End Sub

    Private Sub txtKeepSingle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepSingle.TextChanged, txtKeepSingle.ValueChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("KeepSingle", txtKeepSingle.Value)
    End Sub

    Private Sub txtKeepPack_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepPack.TextChanged, txtKeepPack.ValueChanged
        oUI.SaveRegistry("KeepPackage", txtKeepPack.Value)
    End Sub

    Private Sub txtKeepAuto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtAutoKeep.TextChanged, txtAutoKeep.ValueChanged
        oUI.SaveRegistry("KeepAuto", txtAutoKeep.Value)
    End Sub

    Private Sub cmdErrorLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdErrorLog.Click
        If MessageBox.Show("Clear the entire error log? This cannot be undone.", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            Dim sPath As String = Application.StartupPath & "\eventlog.dat"

            Dim conString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

            Dim oCon As ADODB.Connection = New ADODB.Connection

            oCon.Open(conString)

            oCon.Execute("DELETE FROM EventLogAttr")

            oCon.Close()

            oCon = Nothing

            Me.lsvErrorLog.Items.Clear()

            For Each ctrl As Control In Panel5.Controls
                If TypeOf ctrl Is TextBox Then
                    Dim txt As TextBox = CType(ctrl, TextBox)

                    txt.Text = ""
                End If
            Next
        End If
    End Sub

    Private Sub txtKeepLogs_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtKeepLogs.TextChanged, txtKeepLogs.ValueChanged
        oUI.SaveRegistry("KeepLogs", txtKeepLogs.Value)
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExecute.Click
        Dim sType As String
        Dim nID As Int32
        Dim oItem As ListViewItem
        Dim oT As New clsMarsThreading

        Try
            If lsvTaskManager.SelectedItems.Count = 0 Then Return

            AppStatus(True)

            For Each oItem In lsvTaskManager.SelectedItems
                sType = oItem.Tag.Split(":")(0)
                nID = oItem.Tag.Split(":")(1)

                oItem.SubItems(3).Text = "Manual execution..."

                Select Case sType.ToLower
                    Case "report"
                        oT.xReportID = nID

                        oSchedule.TaskManager("Add", nID)

                        Dim oRs As ADODB.Recordset
                        Dim SQL As String

                        SQL = "SELECT Dynamic, Bursting FROM ReportAttr WHERE ReportID =" & nID

                        oRs = clsMarsData.GetData(SQL)

                        If Not oRs Is Nothing Then
                            If oRs.EOF = False Then
                                If IsNull(oRs(0).Value, 0) = "1" Then
                                    oT.DynamicScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EXECUTE)
                                ElseIf IsNull(oRs(1).Value, 0) = "1" Then
                                    oT.BurstingScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EXECUTE)
                                Else
                                    oT.SingleScheduleThread()
                                    clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EXECUTE)
                                End If
                            End If
                        End If
                    Case "package"
                        oT.xPackID = nID

                        oSchedule.TaskManager("Add", , nID)

                        oT.PackageScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.EXECUTE)
                    Case "automation"
                        oT.xAutoID = nID

                        oT.AutoScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EXECUTE)
                    Case "event-package"
                        oT.xPackID = nID

                        oT.EventPackageScheduleThread()

                        clsMarsAudit._LogAudit(oItem.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.EXECUTE)
                    Case "event"
                        MessageBox.Show("The selected schedule is already executing!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Return
                End Select

                _RemoveFromQueue(oItem.Text, nID, sType, False)
            Next
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        Finally
            AppStatus(False)
        End Try
    End Sub

    Private Sub txtKeepEvent_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtKeepEvent.TextChanged, txtKeepEvent.ValueChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("KeepEvent", txtKeepEvent.Value)

    End Sub

    Private Sub cmdRemoveSchedule_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveSchedule.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub tabHistory_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tabHistory.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "single"
                If IsLoaded = False Then Return

                oSchedule.DrawScheduleHistory(Me.tvSingle, clsMarsScheduler.enScheduleType.REPORT)

                Try
                    txtKeepSingle.Value = oUI.ReadRegistry("KeepSingle", 14)
                Catch
                    txtKeepSingle.Value = 14
                End Try
            Case "packages"
                If IsLoaded = False Then Return

                oSchedule.DrawScheduleHistory(Me.tvPackage, clsMarsScheduler.enScheduleType.PACKAGE)

                Try
                    txtKeepPack.Value = oUI.ReadRegistry("KeepPackage", 14)
                Catch
                    txtKeepPack.Value = 14
                End Try
            Case "event-based"
                If IsLoaded = False Then Return

                Dim oEvent As New clsMarsEvent
                oEvent.DrawEventHistory(tvHistory)

                Dim oUI As New clsMarsUI

                Try
                    txtKeepEvent.Value = oUI.ReadRegistry("KeepEvent", 14)
                Catch
                    txtKeepEvent.Value = 14
                End Try
            Case "automation"
                If IsLoaded = False Then Return
                Dim sOrder As String

                oSchedule.DrawScheduleHistory(tvAutoHistory, clsMarsScheduler.enScheduleType.AUTOMATION)

                Try
                    txtAutoKeep.Value = oUI.ReadRegistry("KeepAuto", 14)
                Catch
                    txtAutoKeep.Value = 14
                End Try
            Case "event-based packages"
                If IsLoaded = False Then Return

                Dim sOrder As String

                oSchedule.DrawScheduleHistory(Me.tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE)

                Try
                    Me.txtEPKeepHistory.Value = oUI.ReadRegistry("KeepEP", 14)
                Catch
                    txtEPKeepHistory.Value = 14
                End Try
        End Select
    End Sub

    Private Sub readErrorLog()
        Try
            Dim SQL As String
            Dim conString As String
            Dim sPath As String
            Dim oCon As ADODB.Connection = New ADODB.Connection
            Dim oRs As ADODB.Recordset = New ADODB.Recordset
            Dim I As Integer = 0

            sPath = sAppPath & "eventlog.dat"

            conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

            oCon.Open(conString)

            If supportMode = False Then
                SQL = "SELECT * FROM EventLogAttr WHERE ErrorSeverity = 0 ORDER BY EntryDate DESC"
            Else
                SQL = "SELECT * FROM EventLogAttr ORDER BY EntryDate DESC"
            End If

            oRs.Open(SQL, oCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            gerrItemList = Nothing

            Me.lsvErrorLog.Items.Clear()

            Do While oRs.EOF = False
                Dim oItem As ListViewItem = New ListViewItem
                ReDim Preserve Me.gerrItemList(I)

                With oItem
                    .Text = IsNull(oRs("pcname").Value, "")
                    .Tag = oRs("eventid").Value

                    With .SubItems
                        .Add(CType(oRs("entrydate").Value, String)) '1
                        .Add(IsNull(oRs("schedulename").Value, "")) '2
                        .Add(IsNull(oRs("errordesc").Value, "")) '3
                        .Add(IsNull(oRs("errornumber").Value, 0)) '4
                        .Add(IsNull(oRs("errorsource").Value, "")) '5
                        .Add(IsNull(oRs("errorline").Value, "")) '6 
                        .Add(IsNull(oRs("errorsuggestion").Value)) '7
                    End With
                End With

                Me.lsvErrorLog.Items.Add(oItem)

                Me.gerrItemList(I) = oItem

                I += 1
                oRs.MoveNext()
            Loop

            oRs.Close()
            oCon.Close()

            oRs = Nothing
            oCon = Nothing
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub tabCRD_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tabCRD.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "schedule manager"
                tmEmailQueue.Enabled = False
                Me.tmEmailLog.Enabled = False
                Me.tmScheduleMon.Enabled = True
                Me.tmScheduleMonCleaner.Enabled = True
            Case "email queue"
                Me.tmScheduleMon.Enabled = False
                Me.tmEmailLog.Enabled = False
                tmEmailQueue.Enabled = True
                Me.tmScheduleMonCleaner.Enabled = False
            Case "email log"
                Me.tmScheduleMon.Enabled = False
                tmEmailQueue.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False

                Dim sort As String = clsMarsUI.MainUI.ReadRegistry("EmailLogSort", "ASC")

                LoadEmailLog()

                txtKeepLogs.Value = oUI.ReadRegistry("KeepLogs", 14)

                Me.tmEmailLog.Enabled = True

            Case "error log"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                tmEmailQueue.Enabled = False
                tmEmailLog.Enabled = False

                If System.IO.File.Exists(sAppPath & "eventlog.dat") Then
                    Me.readErrorLog()
                    If Me.lsvErrorLog.Items.Count > 0 Then
                        Me.lsvErrorLog.Items(0).Selected = True
                    End If
                End If
            Case "deferred delivery"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                tmEmailQueue.Enabled = False

                DeferedList("ASC")
            Case "scheduled backup"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                tmEmailQueue.Enabled = False
                tmEmailLog.Enabled = False

                ViewScheduledBackups()
            Case "scheduled refresh"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                tmEmailQueue.Enabled = False
                tmEmailLog.Enabled = False

                ViewRefreshSchedule()
            Case "audit trail"
                Me.tmScheduleMon.Enabled = False
                tmEmailQueue.Enabled = False
                tmEmailLog.Enabled = False

                Dim oAudit As New clsMarsAudit

                oAudit._ViewTrail(Me.lsvAudit)
            Case "read receipts"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                tmEmailQueue.Enabled = False
                tmEmailLog.Enabled = False

                Me.cmbFilterRR.SelectedIndex = 0
                ListReceipts()
            Case "schedule retries"
                Me.tmScheduleMon.Enabled = False
                Me.tmScheduleMonCleaner.Enabled = False
                Me.tmEmailLog.Enabled = False
                tmEmailQueue.Enabled = False

                LoadScheduleRetries()

        End Select
    End Sub

    Private Sub LoadScheduleRetries()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sCol As String

10:     SQL = "SELECT * FROM RetryTracker r INNER JOIN ScheduleAttr s on r.ScheduleID = s.ScheduleID"

20:     oRs = clsMarsData.GetData(SQL)

30:     Me.lsvRetry.Items.Clear()

40:     Me.lsvRetry.BeginUpdate()

50:     Try
60:         If oRs IsNot Nothing Then
70:             Do While oRs.EOF = False
80:                 Dim item As ListViewItem = New ListViewItem
                    Dim imageIndex As Integer = 0
                    Dim scheduleName As String = ""
                    Dim retryID As Integer = oRs("retryid").Value
                    Dim scheduleID As Integer
                    Dim retryNumber, retryCount As Integer
                    Dim nID As Integer = 0
                    Dim lastRetry As String = ConDateTime(oRs("entrydate").Value)

                    Dim reportID, packID, autoID, eventPackID As Integer

90:                 reportID = IsNull(oRs("reportid").Value, 0)
100:                packID = IsNull(oRs("packid").Value, 0)
110:                autoID = IsNull(oRs("autoid").Value, 0)
120:                eventPackID = IsNull(oRs("eventpackid").Value, 0)
130:                retryNumber = IsNull(oRs("retrynumber").Value, 1)

140:                If reportID <> 0 Then
150:                    scheduleName = clsMarsScheduler.globalItem.GetScheduleName(reportID, clsMarsScheduler.enScheduleType.REPORT)
160:                    If clsMarsData.IsScheduleDataDriven(reportID, "report") Then
170:                        imageIndex = 10
180:                    ElseIf clsMarsData.IsScheduleDynamic(reportID, "report") Then
190:                        imageIndex = 5
200:                    ElseIf clsMarsData.IsScheduleBursting(reportID) Then
210:                        imageIndex = 4
220:                    Else
230:                        imageIndex = 1
                        End If

240:                    scheduleID = clsMarsScheduler.GetScheduleID(reportID)

250:                    SQL = "SELECT Retry FROM ReportAttr WHERE ReportID =" & reportID

                        sCol = "ReportID"
                        nID = reportID
260:                ElseIf packID <> 0 Then
270:                    scheduleName = clsMarsScheduler.globalItem.GetScheduleName(packID, clsMarsScheduler.enScheduleType.PACKAGE)

280:                    If clsMarsData.IsScheduleDataDriven(packID, "package") Then
290:                        imageIndex = 9
300:                    ElseIf clsMarsData.IsScheduleDynamic(packID, "package") Then
310:                        imageIndex = 3
320:                    Else
330:                        imageIndex = 0
                        End If

340:                    scheduleID = clsMarsScheduler.GetScheduleID(, packID)

350:                    SQL = "SELECT Retry FROM PackageAttr WHERE PackID =" & packID

                        sCol = "PackID"
                        nID = packID
360:                Else
370:                    GoTo skip
                    End If

380:                item.Text = scheduleName
390:                item.ImageIndex = imageIndex

                    Dim rsRetry As ADODB.Recordset = clsMarsData.GetData(SQL)
                    Dim lastResult As String = ""

400:                If rsRetry IsNot Nothing Then
410:                    If rsRetry.EOF = False Then
420:                        retryCount = IsNull(rsRetry("retry").Value, 3)
                        End If

430:                    rsRetry.Close()
                    End If

                    Dim rsHis As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM ScheduleHistory WHERE " & sCol & " = " & nID & _
                    " AND Success = 0 ORDER BY EntryDate DESC")

440:                If rsHis IsNot Nothing Then
450:                    If rsHis.EOF = False Then
460:                        lastResult = IsNull(rsHis("errmsg").Value)
470:                        item.SubItems.Add(lastRetry)
480:                        item.SubItems.Add(retryNumber & " of " & retryCount)
490:                        item.SubItems.Add(lastResult)
500:                        item.Tag = retryID
510:                        Me.lsvRetry.Items.Add(item)
                        End If
                    End If
skip:
520:                oRs.MoveNext()
530:            Loop

540:            oRs.Close()
            End If

550:        SQL = "SELECT * FROM RetryTracker r INNER JOIN EventAttr6 e ON r.EventID = e.EventID"

560:        oRs = clsMarsData.GetData(SQL)

570:        If oRs IsNot Nothing Then
580:            Do While oRs.EOF = False
590:                Dim item As ListViewItem = New ListViewItem
                    Dim eventName As String = oRs("eventname").Value
                    Dim eventID As Integer = clsMarsData.GetColumnIndex("eventid", oRs)
                    Dim retryID = oRs("retryID").Value
                    Dim retryNumber, retryCount As Integer
                    Dim lastResult As String = ""
                    Dim lastRetry As String = ConDateTime(oRs("entrydate").Value)

600:                eventID = oRs(eventID).Value
610:                retryCount = oRs("retrycount").Value
620:                retryNumber = oRs("retrynumber").Value

630:                item.ImageIndex = 7
640:                item.Tag = retryID
650:                item.Text = eventName

                    Dim rsHis As ADODB.Recordset = clsMarsData.GetData("SELECT TOP 1 * FROM EventHistory WHERE " & _
           "EventID=" & eventID & " AND  Status =0 ORDER BY LastFired DESC")

660:                If rsHis IsNot Nothing Then
670:                    If rsHis.EOF = False Then
680:                        lastResult = IsNull(rsHis("errmsg").Value)
690:                        item.SubItems.Add(lastRetry)
700:                        item.SubItems.Add(retryNumber & " of " & retryCount)
710:                        item.SubItems.Add(lastResult)

720:                        Me.lsvRetry.Items.Add(item)
                        End If
                    End If

730:                oRs.MoveNext()
740:            Loop

750:            oRs.Close()
            End If

            Dim sortString As String

            If Me.retrylogsortOrder = Windows.Forms.SortOrder.Ascending Then
                sortString = " ASC "
            Else
                sortString = " DESC "
            End If

            Try
                clsMarsUI.sortListView(Me.lsvRetry, sortString, retrylogSortBy, IIf(Me.retrylogSortBy = 1, True, False), False)
            Catch ex As Exception

            End Try
760:    Catch ex As Exception
770:        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, , True, True, 1)
780:    Finally
790:        Me.lsvRetry.EndUpdate()
        End Try

    End Sub

    Private Sub ViewScheduledBackups()
        Try
            Dim oRs As New ADODB.Recordset
            Dim sPath As String = sAppPath & "backup.xml"

            If System.IO.File.Exists(sPath) Then
                oRs = oData.GetXML(sPath)

                Select Case CType(oRs("frequency").Value, String).ToLower
                    Case "daily"
                        optDaily.Checked = True
                    Case "weekly"
                        optWeekly.Checked = True
                    Case "monthly"
                        optMonthly.Checked = True
                    Case "yearly"
                        optYearly.Checked = True
                End Select

                dtBackup.Value = ConDate(Now.Date) & " " & ConTime(oRs("runtime").Value)
                txtInterval.Value = oRs("repeatevery").Value
                txtPath.Text = oRs("backuppath").Value

                Try
                    txtKeep.Text = oRs("keepfor").Value
                Catch ex As Exception
                    txtKeep.Text = 5
                End Try
            End If


            'Pull the backup information for ScheduleAttr
            Dim SQL1 As String = "Select * from ScheduleAttr where ScheduleID = 11"
            Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL1)
            Try

                If Not oRs1 Is Nothing Then
                    If oRs1.EOF = True Then
                        UcUpdate.EndDate.Value = Date.Now.AddYears(10)

                    Else
                        Select Case oRs1("frequency").Value.ToLower
                            Case "daily"
                                UcUpdate.optDaily.Checked = True
                            Case "weekly"
                                UcUpdate.optWeekly.Checked = True
                            Case "week-daily"
                                UcUpdate.optWeekDaily.Checked = True
                            Case "monthly"
                                UcUpdate.optMonthly.Checked = True
                            Case "yearly"
                                UcUpdate.optYearly.Checked = True
                            Case "custom"
                                UcUpdate.optCustom.Checked = True
                                UcUpdate.cmbCustom.Text = IsNull(oRs1("calendarname").Value)
                            Case "none"
                                UcUpdate.optNone.Checked = True
                        End Select

                        Try
                            If IsNull(oRs1("status").Value, 5) = 1 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = True
                                Me.rbAdvanced.Checked = False
                                Me.grpAdvanced.Visible = False
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            ElseIf IsNull(oRs1("status").Value, 5) = 2 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = False
                                Me.rbAdvanced.Checked = True
                                Me.grpAdvanced.Visible = True
                                Me.grpAdvanced.BringToFront()
                                Me.grpSimple.Visible = False
                                Me.chkZip.Checked = False
                                Me.txtZipName.Enabled = False
                            ElseIf IsNull(oRs1("status").Value, 5) = 3 Then
                                Me.chkBackups.Checked = True
                                Me.rbSimple.Checked = False
                                Me.rbAdvanced.Checked = True
                                Me.grpAdvanced.Visible = True
                                Me.grpAdvanced.BringToFront()
                                Me.grpSimple.Visible = False
                                Me.chkZip.Checked = True
                                Me.txtZipName.Enabled = True
                            ElseIf IsNull(oRs1("status").Value, 5) = 0 Then
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            ElseIf IsNull(oRs1("status").Value, 5) = 5 Then
                                Me.grpSimple.Visible = True
                                Me.grpSimple.BringToFront()
                            End If
                        Catch ex As Exception
                            Me.chkBackups.Checked = True
                        End Try

                        grpBackup.Enabled = chkBackups.Checked
                        cmdSaveBackup.Enabled = chkBackups.Checked

                        Try
                            UcUpdate.m_RepeatUnit = IsNull(oRs1("repeatunit").Value, "hours")
                        Catch ex As Exception
                            UcUpdate.m_RepeatUnit = "hours"
                        End Try

                        UcUpdate.btnApply = Me.cmdSaveBackup

                        UcUpdate.ScheduleID = 11


                        Try
                            Dim d As Date

                            d = oRs1("enddate").Value

                            If d.Year >= 3004 Then
                                UcUpdate.EndDate.Enabled = False
                                UcUpdate.chkNoEnd.Checked = True
                            End If

                        Catch : End Try

                        Try
                            Dim repeatUntil As String = IsNull(oRs1("repeatuntil").Value, Now)

                            If repeatUntil.Length > 8 Then
                                UcUpdate.RepeatUntil.Value = ConDateTime(CTimeZ(repeatUntil, dateConvertType.READ))
                            Else
                                UcUpdate.RepeatUntil.Value = CTimeZ(Now.Date & " " & repeatUntil, dateConvertType.READ)
                            End If
                        Catch : End Try

                        UcUpdate.NextRun.Value = CTimeZ(oRs1("nextrun").Value, dateConvertType.READ)
                        UcUpdate.EndDate.Value = CTimeZ(oRs1("enddate").Value, dateConvertType.READ)
                        UcUpdate.RunAt.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & oRs1("starttime").Value), dateConvertType.READ)
                        UcUpdate.NextRunTime.Value = CTimeZ(oRs1("nextrun").Value, dateConvertType.READ)
                        UcUpdate.chkRepeat.Checked = Convert.ToBoolean(oRs1("repeat").Value)
                        UcUpdate.cmbRpt.Text = IsNonEnglishRegionRead(oRs1("repeatinterval").Value)

                        UcUpdate.StartDate.Value = ConDate(oRs1("startdate").Value)

                        UcUpdate.cmbException.Text = IsNull(oRs1("exceptioncalendar").Value)

                        Try
                            UcUpdate.chkException.Checked = Convert.ToBoolean(oRs1("useexception").Value)
                        Catch
                            UcUpdate.chkException.Checked = False
                        End Try
                    End If

                    If UcUpdate.cmbRpt.Text = "" Then UcUpdate.cmbRpt.Text = 0.25

                    Me.txtZipName.Text = IsNull(oRs1("bzipname").Value)


                    oRs1.Close()

                End If
            Catch : End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub
    Private Sub ViewRefreshSchedule()
        Dim oUI As New clsMarsUI
        Dim oItem As ListViewItem

        oUI.BuildTree(tvObjects, True, False)

        Dim oRs As ADODB.Recordset

        Dim SQL As String

        SQL = "SELECT * FROM RefresherAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvSchedules.Items.Clear()

        Do While oRs.EOF = False
            oItem = lsvSchedules.Items.Add(oRs("objecttype").Value)

            With oItem
                With .SubItems
                    .Add(oRs("objectname").Value)
                    .Add(oRs("frequency").Value)
                    .Add(CType(IsNull(oRs("lastrun").Value), String))

                    Try
                        .Add(CType(CTimeZ(oRs("nextrun").Value, dateConvertType.READ), String))
                    Catch
                        .Add(ConDateTime(Now))
                    End Try

                    .Add(CType(oRs("repeatevery").Value, String))
                End With

                .Tag = oRs("refreshid").Value
            End With
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Private Sub ListReceipts(Optional ByVal filter As String = "")
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset
            Dim imagePos As Integer = 0
            Dim scheduleName As String = ""

            lsvReadReciepts.Items.Clear()


            SQL = "SELECT * FROM ReadReceiptsLog r INNER JOIN DestinationAttr d ON r.DestinationID = d.DestinationID"

            If filter.Length > 0 And filter <> "None" Then
                SQL &= " WHERE RRStatus ='" & filter & "'"
            End If

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                SQL = ""

                Dim item As ListViewItem = New ListViewItem

                Dim nReportID As Integer = IsNull(oRs("reportid").Value, 0)
                Dim nPackID As Integer = IsNull(oRs("packid").Value, 0)

                If nReportID > 0 Then
                    imagePos = 0

                    SQL = "SELECT ReportTitle FROM ReportAttr WHERE ReportID =" & nReportID
                ElseIf nPackID > 0 Then

                    SQL = "SELECT PackageName FROM PackageAttr WHERE PackID =" & nPackID

                    imagePos = 1
                End If

                If SQL.Length = 0 Then Continue Do

                Dim oRs1 As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs1 Is Nothing Then Continue Do

                If oRs1.EOF = True Then Continue Do

                scheduleName = oRs1.Fields(0).Value

                oRs1.Close()

                item.Text = scheduleName
                item.ImageIndex = imagePos

                With item.SubItems
                    .Add(oRs("emailsubject").Value)
                    .Add(oRs("sentto").Value)
                    .Add(CType(CTimeZ(oRs("entrydate").Value, dateConvertType.READ), String))
                    .Add(CType(CTimeZ(oRs("nextcheck").Value, dateConvertType.READ), String))
                    .Add(oRs("rrstatus").Value)
                    .Add(IsNull(CTimeZ(oRs("lastcheck").Value, dateConvertType.READ), ""))
                End With

                item.Tag = oRs("logid").Value

                lsvReadReciepts.Items.Add(item)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub mnuReceived_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuReceived.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus ='Received' WHERE LogID =" & logid) = True Then

                item.SubItems(5).Text = "Received"

            Else
                Exit For
            End If
        Next

        Me.ListReceipts(cmbFilterRR.Text)
    End Sub

    Private Sub mnuPending_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPending.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus ='Pending' WHERE LogID =" & logid) = True Then

                item.SubItems(5).Text = "Pending"

            Else
                Exit For
            End If
        Next

        Me.ListReceipts(cmbFilterRR.Text)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Me.lsvReadReciepts.SelectedItems.Count = 0 Then Return

        For Each item As ListViewItem In Me.lsvReadReciepts.SelectedItems
            Dim logid As Integer = item.Tag

            If clsMarsData.WriteData("DELETE FROM ReadReceiptsLog WHERE LogID =" & logid) = True Then

                item.Remove()
            Else
                Exit For
            End If
        Next
    End Sub


    Private Sub cmbFilterRR_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFilterRR.SelectedIndexChanged

        Me.ListReceipts(cmbFilterRR.Text)


    End Sub


    Private Sub frmSystemMonitor_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        On Error Resume Next
        clsMarsUI.MainUI.SaveRegistry("SysMonSize", Me.Width & "," & Me.Height)
    End Sub

    Private Sub txtDeleteOlder_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDeleteOlder.ValueChanged, txtDeleteOlder.TextChanged, cmbValueUnit.SelectedIndexChanged
        If IsLoaded = False Then Return

        clsMarsUI.MainUI.SaveRegistry("EmailQueueDeleteAfter", txtDeleteOlder.Text & " " & Me.cmbValueUnit.Text)
    End Sub

    Private Sub optEPAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optEPAsc.CheckedChanged
        If Me.Visible = False Then Return

        If optEPAsc.Checked = True Then

            grpEPSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " ASC ", _
            chkEPFilter.Checked)
            grpEPSort.Enabled = True
        End If
    End Sub

    Private Sub optEPDesc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optEPDesc.CheckedChanged
        If Me.Visible = False Then Return

        If optEPDesc.Checked = True Then

            grpEPSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " DESC ", _
            chkEPFilter.Checked)
            grpEPSort.Enabled = True
        End If
    End Sub

    Private Sub chkEPFilter_CheckStateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkEPFilter.CheckStateChanged
        If optEPAsc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " ASC ", _
            chkEPFilter.Checked)
        Else
            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " DESC ", _
            chkEPFilter.Checked)
        End If
    End Sub

    Private Sub btnEPRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEPRefresh.Click
        If optEPAsc.Checked = True Then

            grpEPSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " ASC ")

            grpEPSort.Enabled = True
        Else
            grpEPSort.Enabled = False

            oSchedule.DrawScheduleHistory(tvEPHistory, clsMarsScheduler.enScheduleType.EVENTPACKAGE, , " DESC ")

            grpEPSort.Enabled = True
        End If
    End Sub

    Private Sub txtEPKeepHistory_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtEPKeepHistory.ValueChanged, txtEPKeepHistory.TextChanged
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("KeepEP", txtEPKeepHistory.Value)
    End Sub

    Private Sub btnEPClear_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnEPClear.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        cmnuEPClear.Show(btnEPClear, New Point(e.X, e.Y))
    End Sub

    Private Sub mnuEPClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuEPClearAll.Click
        Dim oRes As DialogResult
        oRes = MessageBox.Show("Delete all Event-Based Package history?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            oSchedule.DeleteAllHistory(clsMarsScheduler.enScheduleType.EVENTPACKAGE)

            btnEPRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub mnuEPClearSel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuEPClearSel.Click
        If Me.tvEPHistory.SelectedNode Is Nothing Then Return

        Dim nID As Integer

        Try
            Int32.Parse(tvEPHistory.SelectedNode.Tag)

            nID = tvEPHistory.SelectedNode.Tag
        Catch ex As Exception
            Return
        End Try

        Dim SQL As String

        SQL = "DELETE FROM ScheduleHistory WHERE EventPackID =" & nID

        clsMarsData.WriteData(SQL)

        Try
            tvEPHistory.SelectedNode.Nodes.Clear()
        Catch : End Try
    End Sub
    Private Sub lsvTaskManager_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvTaskManager.KeyDown
        If e.KeyCode = Keys.F7 Then
            If Me.tmScheduleMon.Enabled = True Then
                Me.tmScheduleMon.Enabled = False
            Else
                Me.tmScheduleMon.Enabled = True
            End If
        End If
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub menuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub


    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            grpAdvanced.Visible = False
            grpSimple.Visible = True
            grpSimple.BringToFront()
            oUI.SaveRegistry("AdvancedBackupSchedule", 0)
        End If

    End Sub

    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            grpAdvanced.Visible = True
            grpSimple.Visible = False
            grpAdvanced.BringToFront()
            Label9.Text = "days"
            oUI.SaveRegistry("AdvancedBackupSchedule", 1)
        End If
    End Sub

    Private Sub chkZip_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkZip.CheckedChanged
        If chkZip.Checked = True Then
            Me.txtZipName.Enabled = True
        End If
        If chkZip.Checked = False Then
            Me.txtZipName.Enabled = False
        End If
    End Sub

    Private Sub btnSingleHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSingleHistory.Click
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.REPORT)
    End Sub

    Private Sub btnPackageHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPackageHistory.Click
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.PACKAGE)
    End Sub

    Private Sub btnEventHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.EVENTBASED)
    End Sub

    Private Sub btnAutoHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.AUTOMATION)
    End Sub

    Private Sub btnEventPackHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEventPackHistory.Click
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.EVENTPACKAGE)
    End Sub

    Private Sub lsvLog_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvLog.ColumnClick

        Me.lsvLog.BeginUpdate()
        logsortBy = e.Column

        Dim sortString As String = " ASC"

        If logsortOrder = Windows.Forms.SortOrder.Ascending Then
            logsortOrder = Windows.Forms.SortOrder.Descending
            sortString = " DESC"
        Else
            logsortOrder = Windows.Forms.SortOrder.Ascending
            sortString = " ASC"
        End If

        clsMarsUI.sortListView(Me.lsvLog, sortString, logsortBy, IIf(logsortBy = 3, True, False))

        Me.lsvLog.EndUpdate()
        Me.tmScheduleMon.Start()
    End Sub

    Private Sub lsvLog_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvLog.DoubleClick
        If lsvLog.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvLog.SelectedItems(0)
        Dim path As String = ""

        Try
            path = oItem.SubItems(4).Text
        Catch ex As Exception
            Return
        End Try

        Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

        If path <> "" Then
            Try
                If convertToMsg = False Then
                    Process.Start(path)
                Else
                    path = clsMarsMessaging.convertToMsg(path)

                    If path IsNot Nothing Then Process.Start(path)
                End If
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6565)
            End Try
        End If

    End Sub


    Private Sub DeleteSelectedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteSelectedToolStripMenuItem.Click
        Dim res As DialogResult = MessageBox.Show("Delete the selected messages?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            Dim items As ArrayList = New ArrayList

            If lsvLog.Visible = True Then
                For Each item As ListViewItem In Me.lsvLog.SelectedItems
                    Dim nID As Integer = item.Tag
                    Dim filePath As String = item.SubItems(4).Text

                    If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & nID) = True Then
                        Try
                            If filePath <> "" Then
                                IO.File.Delete(filePath)
                            End If
                        Catch : End Try

                        items.Add(item)
                    End If
                Next

                For Each item As ListViewItem In items
                    Me.lsvLog.Items.Remove(item)
                Next

                gItemList = Nothing
                Dim I As Integer = 0

                For Each item As ListViewItem In lsvLog.Items
                    ReDim Preserve gItemList(I)

                    gItemList(I) = item

                    I += 1
                Next
            Else
                For Each r As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                    Dim emailID As Integer = r.Cells(0).Value
                    Dim filePath As String = r.Cells(5).Value

                    If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & emailID) = True Then
                        Try
                            If filePath <> "" Then
                                IO.File.Delete(filePath)
                            End If
                        Catch : End Try
                    End If
                Next

                Me.LoadEmailLogDGV()
            End If
        End If
    End Sub

    Private Sub DeleteAllToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteAllToolStripMenuItem.Click
        Dim res As DialogResult = MessageBox.Show("Delete all the messages in the log?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            If clsMarsData.WriteData("DELETE FROM EmailLog") = True Then
                Dim fileDir As String = clsMarsUI.MainUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages\")

                If IO.Directory.Exists(fileDir) Then
                    For Each s As String In IO.Directory.GetFiles(fileDir)
                        Try
                            IO.File.Delete(s)
                        Catch : End Try
                    Next
                End If

                Me.lsvLog.Items.Clear()
            End If
        End If
    End Sub

    Private Sub OpenToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OpenToolStripMenuItem.Click
        If Me.lsvLog.Visible = True Then
            For Each item As ListViewItem In Me.lsvLog.SelectedItems
                Dim path As String = item.SubItems(4).Text

                If path <> "" Then
                    Try
                        Process.Start(path)
                    Catch ex As Exception
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6625)
                    End Try
                End If
            Next
        End If

    End Sub


    Private Sub tmScheduleMonCleaner_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmScheduleMonCleaner.Elapsed
        For Each it As ListViewItem In Me.lsvProcessing.Items
            Dim procID As Integer = it.SubItems(4).Text

            'check the process exists
            Try
                Dim proc As Process = Process.GetProcessById(procID)
            Catch
                clsMarsData.WriteData("DELETE FROM TaskManager WHERE ProcessID =" & procID)
                it.Remove()
            End Try

            'check the ID exists in the TaskManager table
            Dim tag As String = it.Tag
            Dim type As String = tag.Split(":")(0).ToLower
            Dim ID As Integer = tag.Split(":")(1)
            Dim idCol As String = ""

            Dim SQL As String

            Select Case type
                Case "report"
                    idCol = "reportid"
                Case "package"
                    idCol = "packid"
                Case "event-package"
                    idCol = "eventpackid"
                Case "event"
                    idCol = "eventid"
                Case "automation", "backup", "retry"
                    idCol = "autoid"
            End Select

            SQL = "SELECT * FROM taskmanager WHERE " & idCol & " = " & ID

            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

            If oRs IsNot Nothing Then
                If oRs.EOF = True Then
                    it.Remove()
                    clsMarsData.WriteData("DELETE FROM TaskManager WHERE " & idCol & "=" & ID)
                End If

                oRs.Close()
            End If
        Next
    End Sub

    Private Sub lsvEmailQueue_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvEmailQueue.DoubleClick

        If lsvEmailQueue.SelectedItems.Count = 0 Then Return

        For Each lsv As ListViewItem In Me.lsvEmailQueue.SelectedItems
            Try
                Dim path As String = lsv.SubItems(6).Text

                Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

                If convertToMsg = False Then
                    If path <> "" Then Process.Start(path)
                Else
                    If path <> "" Then
                        path = clsMarsMessaging.convertToMsg(path)

                        If path IsNot Nothing Then
                            Process.Start(path)
                        End If
                    End If
                End If
            Catch : End Try
        Next
    End Sub


    Private Sub bgScheduleMon_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgEmailLog.DoWork
        Me.ScheduleMonitor(cmbFilter.Text)
    End Sub

    Private Sub tmEmailLog_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmEmailLog.Elapsed


        Me.tmEmailLog.Stop()
        LoadEmailLog()
        Me.tmEmailLog.Start()
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click, mnuProcProperties.Click
        Dim tag As String
        Dim type As String
        Dim nID As Integer
        Dim lsv As ListView
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "menuitem11", True) = 0 Then
            lsv = Me.lsvTaskManager
        Else
            lsv = Me.lsvProcessing
        End If

        If lsv.SelectedItems.Count = 0 Then Return

        Dim item As ListViewItem = lsv.SelectedItems(0)

        tag = item.Tag
        type = tag.Split(":")(0).ToLower
        nID = tag.Split(":")(1)

        Select Case type
            Case "report"
                Dim prop As frmSingleProp = New frmSingleProp

                prop.EditSchedule(nID)
            Case "package"
                Dim prop As frmPackageProp = New frmPackageProp

                prop.EditPackage(nID)
            Case "event"
                Dim prop As frmEventProp = New frmEventProp

                prop.EditSchedule(nID)
            Case "event-package"
                Dim prop As frmEventPackageProp = New frmEventPackageProp

                prop.EditPackage(nID)
            Case "automation"
                Dim prop As frmAutoProp = New frmAutoProp

                prop.EditSchedule(nID)
        End Select
    End Sub

    Private Sub MenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click, mnuProcLoc.Click
        Dim tag, type, table, SQL, col As String
        Dim nID, nParent As Integer
        Dim lsv As ListView
        Dim menu As MenuItem = CType(sender, MenuItem)

        'find which listview to use
        If String.Compare(menu.Tag, "menuitem12", True) = 0 Then
            lsv = Me.lsvTaskManager
        Else
            lsv = Me.lsvProcessing
        End If

        If lsv.SelectedItems.Count = 0 Then Return

        Dim item As ListViewItem = lsv.SelectedItems(0)

        tag = item.Tag
        type = tag.Split(":")(0).ToLower
        nID = tag.Split(":")(1)

        Select Case type
            Case "report"
                table = "ReportAttr"
                col = "ReportID"
            Case "package"
                table = "PackageAttr"
                col = "PackID"
            Case "event"
                table = "EventAttr6"
                col = "EventID"
            Case "event-package"
                table = "EventPackageAttr"
                col = "EventPackID"
            Case "automation"
                table = "AutomationAttr"
                col = "AutoID"
        End Select

        SQL = "SELECT Parent FROM " & table & " WHERE " & col & " = " & nID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing Then
            If oRs.EOF = False Then
                nParent = oRs(0).Value

                Dim folderTag As String = "Folder:" & nParent
                Dim frmW As frmWindow

                If nWindowCurrent > 0 Then
                    frmW = oWindow(nWindowCurrent)
                    frmW.BringToFront()
                Else
                    nWindowCount += 1
                    ReDim Preserve oWindow(nWindowCount)
                    oWindow(nWindowCount) = New frmWindow
                    oWindow(nWindowCount).MdiParent = oMain
                    oWindow(nWindowCount).Show()
                    oWindow(nWindowCount).Tag = nWindowCount
                    oWindow(nWindowCount).Text = "Desktop"
                    nWindowCurrent = nWindowCount

                    frmW = oWindow(nWindowCount)
                    frmW.BringToFront()
                End If

                clsMarsUI.MainUI.FindNode(folderTag, frmW.tvFolders)

                For Each it As ListViewItem In frmW.lsvWindow.Items
                    If it.Tag = tag Then
                        it.Selected = True
                        it.EnsureVisible()
                        Exit For
                    End If
                Next
            End If

            oRs.Close()
        End If

    End Sub

    Private Sub tmSlider_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmSlider.Scroll
        Me.tmScheduleMon.Interval = tmSlider.Value * 1000

        Me.ToolTip1.SetToolTip(tmSlider, tmSlider.Value & " seconds")

        Me.ToolTip1.Show(tmSlider.Value & " seconds", tmSlider, 3000)

        clsMarsUI.MainUI.SaveRegistry("TaskManagerRefreshInt", tmSlider.Value)
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        If txtSearch.Text <> "" Then
            tmEmailLog.Stop()
            lsvLog.BeginUpdate()
            lsvLog.Items.Clear()

            For Each it As ListViewItem In gItemList
                If it IsNot Nothing Then
                    lsvLog.Items.Add(it)
                End If
            Next

            For Each it As ListViewItem In Me.lsvLog.Items
                Dim search(4) As String

                For I As Integer = 0 To 4
                    search(I) = it.SubItems(I).Text.ToLower
                Next

                Dim match As Boolean = False

                For I As Integer = 0 To 4
                    If search(I).Contains(txtSearch.Text.ToLower) = True Then
                        match = True
                    End If
                Next

                If match = False Then
                    it.Remove()
                End If
            Next

            lsvLog.EndUpdate()
        Else
            Me.tmEmailLog.Stop()
            lsvLog.BeginUpdate()
            lsvLog.Items.Clear()

            LoadEmailLog()
            lsvLog.EndUpdate()
            Me.tmEmailLog.Start()
        End If
    End Sub

    Private Sub btnStop_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Dim svc As clsServiceController = New clsServiceController

        svc.StopScheduler()
        btnStop.Enabled = False
        btnResume.Enabled = True
    End Sub

    Private Sub btnResume_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResume.Click
        Dim svc As clsServiceController = New clsServiceController

        svc.StartScheduler()

        btnResume.Enabled = False
        btnStop.Enabled = True
    End Sub

    Private Sub lsvErrorLog_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvErrorLog.ColumnClick
        If Me.lsvErrorLog.Items.Count = 0 Then Return

        Dim oItem As ListViewItem

        If Me.lsvErrorLog.SelectedItems.Count <> 0 Then
            oItem = Me.lsvErrorLog.SelectedItems(0)
        End If

        If Me.errlogsortOrder = Windows.Forms.SortOrder.Descending Then
            clsMarsUI.sortListView(Me.lsvErrorLog, " ASC", e.Column, IIf(e.Column = 1, True, False), False)
            errlogsortOrder = Windows.Forms.SortOrder.Ascending
        Else
            clsMarsUI.sortListView(Me.lsvErrorLog, " DESC", e.Column, IIf(e.Column = 1, True, False), False)
            errlogsortOrder = Windows.Forms.SortOrder.Descending
        End If

        If oItem Is Nothing Then
            Me.lsvErrorLog.Items(0).Selected = True
        Else
            For Each oIt As ListViewItem In Me.lsvErrorLog.Items
                If oIt.Equals(oItem) Then
                    oIt.Selected = True
                    oIt.EnsureVisible()
                End If
            Next
        End If
    End Sub

    Private Sub lsvErrorLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvErrorLog.SelectedIndexChanged
        If Me.lsvErrorLog.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem = lsvErrorLog.SelectedItems(0)

        '.Add(CType(oRs("entrydate").Value, String)) '1
        '.Add(IsNull(oRs("schedulename").Value, "")) '2
        '.Add(IsNull(oRs("errordesc").Value, "")) '3
        '.Add(IsNull(oRs("errornumber").Value, 0)) '4
        '.Add(IsNull(oRs("errorsource").Value, "")) '5
        '.Add(IsNull(oRs("errorline").Value, "")) '6 
        '.Add(IsNull(oRs("errorsuggestion").Value)) '7

        Me.txterrNo.Text = oItem.SubItems(4).Text
        Me.txterrSource.Text = oItem.SubItems(5).Text
        Me.txterrLine.Text = oItem.SubItems(6).Text
        Me.txtErrorlog.Text = oItem.SubItems(3).Text
        Me.txterrSuggest.Text = oItem.SubItems(7).Text

    End Sub

    Sub searchError()
        If txterrSearch.Text = "support1001350" Then
            Me.supportMode = True
            Me.readErrorLog()
            Return
        End If

        If txterrSearch.Text <> "" And txterrSearch.Text.Length < 3 Then Return

        If txterrSearch.Text <> "" Then

            Me.lsvErrorLog.BeginUpdate()

            lsvErrorLog.Items.Clear()

            For Each it As ListViewItem In gerrItemList
                If it IsNot Nothing Then
                    lsvErrorLog.Items.Add(it)
                End If
            Next
            '-----
            'Dim nTotal As Integer = lsvErrorLog.Items.Count
            'Dim counter As Integer = 1
            'Dim foundItem As ListViewItem
            'Dim foundItems As ArrayList = New ArrayList

            'Do
            '    foundItem = lsvErrorLog.FindItemWithText(txterrSearch.Text, True, 0)

            '    If foundItem IsNot Nothing Then
            '        foundItems.Add(foundItem)
            '        lsvErrorLog.Items.Remove(foundItem)
            '        'console.writeline(lsvErrorLog.Items.Count)
            '    End If

            '    clsMarsUI.BusyProgress((counter / nTotal) * 100, "Searching...")
            '    counter += 1
            'Loop Until foundItem Is Nothing

            'clsMarsUI.BusyProgress(, , True)

            'lsvErrorLog.Items.Clear()

            'For Each it As ListViewItem In foundItems
            '    lsvErrorLog.Items.Add(it)
            'Next

            'lsvErrorLog.EndUpdate()
            '------
            If Me.lsvErrorLog.Items.Count > 0 Then
                For Each it As ListViewItem In Me.lsvErrorLog.Items

                    clsMarsUI.BusyProgress((it.Index / lsvErrorLog.Items.Count) * 100, "searching...")

                    Dim search(7) As String

                    For I As Integer = 0 To 7
                        search(I) = it.SubItems(I).Text.ToLower
                    Next

                    Dim match As Boolean = False

                    For I As Integer = 0 To 7
                        If search(I).Contains(txterrSearch.Text.ToLower) = True Then
                            match = True
                        End If
                    Next

                    If match = False Then
                        it.Remove()
                    End If
                Next
            End If
            lsvErrorLog.EndUpdate()

            clsMarsUI.BusyProgress(, , True)
        Else

            Me.lsvErrorLog.BeginUpdate()
            lsvErrorLog.Items.Clear()

            Me.readErrorLog()
            lsvErrorLog.EndUpdate()

        End If

        If Me.lsvErrorLog.Items.Count > 0 Then
            Me.lsvErrorLog.Items(0).Selected = True
        Else
            For Each ctrl As Control In Panel5.Controls
                If TypeOf ctrl Is TextBox Then
                    Dim txt As TextBox = CType(ctrl, TextBox)

                    txt.Text = ""
                End If
            Next
        End If

        lsvErrorLog.Refresh()
    End Sub
    Private Sub txterrSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txterrSearch.TextChanged


        searchError()

    End Sub

    
    Private Sub lsvEmailQueue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvEmailQueue.SelectedIndexChanged

    End Sub

    Private Sub lsvLog_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvLog.SelectedIndexChanged

    End Sub

    Private Sub ScheduleHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ScheduleHistory.Click
        Try
            If tvSingle.Nodes.Count = 0 Then
                Me.optSingleAsc_CheckedChanged(Nothing, Nothing)
            End If
        Catch ex As Exception
            Me.optSingleAsc_CheckedChanged(Nothing, Nothing)
        End Try

    End Sub

    Private Sub btnRefreshRetry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshRetry.Click
        Me.btnRefreshRetry.Enabled = False
        Me.LoadScheduleRetries()
        Me.btnRefreshRetry.Enabled = True
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        Dim arr As ArrayList = New ArrayList

        For Each item As ListViewItem In Me.lsvRetry.SelectedItems
            Dim retryID As Integer = item.Tag
            Dim SQL As String = "DELETE FROM RetryTracker WHERE RetryID =" & retryID

            If clsMarsData.WriteData(SQL, False, False) = True Then
                arr.Add(retryID)
            End If
        Next

        For Each n As Integer In arr
            For Each item As ListViewItem In Me.lsvRetry.Items
                If item.Tag = n Then
                    item.Remove()
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub lsvRetry_ColumnClick(ByVal sender As Object, ByVal e As System.Windows.Forms.ColumnClickEventArgs) Handles lsvRetry.ColumnClick
        Try
            Me.lsvRetry.BeginUpdate()
            Me.retrylogSortBy = e.Column

            Dim sortString As String = " ASC"

            If retrylogsortOrder = Windows.Forms.SortOrder.Ascending Then
                retrylogsortOrder = Windows.Forms.SortOrder.Descending
                sortString = " DESC"
            Else
                retrylogsortOrder = Windows.Forms.SortOrder.Ascending
                sortString = " ASC"
            End If

            clsMarsUI.sortListView(Me.lsvRetry, sortString, e.Column, IIf(e.Column = 1, True, False))

            Me.lsvRetry.EndUpdate()
        Catch : End Try
    End Sub

   
    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            If Me.lsvErrorLog.SelectedItems.Count = 0 Then Return

            Dim txtCopy As New TextBox
            txtCopy.Multiline = True

            txtCopy.Text = "Error #: " & Me.txterrNo.Text & vbCrLf & _
            "Schedule Name: " & Me.lsvErrorLog.SelectedItems(0).SubItems(2).Text & vbCrLf & _
            "Error Source: " & Me.txterrSource.Text & vbCrLf & _
            "Error Line: " & Me.txterrLine.Text & vbCrLf & _
            "Error Suggestion: " & Me.txterrSuggest.Text & vbCrLf & _
            "Error Description: " & Me.txtErrorlog.Text

            txtCopy.SelectAll()

            txtCopy.Copy()
        Catch : End Try
    End Sub

    Private Sub chkGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroup.CheckedChanged
        clsMarsUI.MainUI.SaveRegistry("GroupScheduleMon", Convert.ToInt32(chkGroup.Checked), , , True)
    End Sub

    Private Sub progressTable_ColumnChanged(ByVal sender As Object, ByVal e As System.Data.DataColumnChangeEventArgs) Handles progressTable.ColumnChanged
        'Me.tmScheduleMon.Stop()
        Try
            Dim row As DataRow = e.Row
            Me.lsvProcessing.BeginUpdate()
            If e.Column.ColumnName.ToLower = "status" Then
                Dim procID As Integer = row("processid")

                For Each it As ListViewItem In Me.lsvProcessing.Items
                    If it.SubItems(4).Text = procID Then

                        it.SubItems(3).Text = row("status")

                        '�pdate the duration
                        Try
                            Dim start As Date = it.SubItems(1).Text
                            Dim duration As Double = Now.Subtract(start).TotalMinutes
                            duration = Math.Round(duration, 1)

                            it.SubItems(5).Text = duration
                        Catch : End Try
                    End If
                Next
            End If
        Catch ex As Exception
        Finally
            Me.lsvProcessing.EndUpdate()
        End Try
    End Sub

    Private Sub btnEventHistory_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEventHistory.Click
        Dim report As frmHistoryReport = New frmHistoryReport

        report.viewReportHistory(clsMarsScheduler.enScheduleType.EVENTBASED)
    End Sub

    Private Sub Splitter2_Move(ByVal sender As Object, ByVal e As System.EventArgs) Handles Splitter2.Move
        If Me.IsLoaded = True Then clsMarsUI.MainUI.SaveRegistry("TaskManagerLayOut", Me.GroupBox14.Height)
    End Sub

    Private Sub dgvEmailLog_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmailLog.CellClick
        Try
            Dim I As Integer = 0
            ReDim emailLogRow(I)

            For Each row As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                ReDim Preserve emailLogRow(I)
                Me.emailLogRow(I) = row.Index
                I += 1
            Next
        Catch : End Try
    End Sub

    Private Sub dgvEmailLog_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEmailLog.CellDoubleClick
        Try
            Dim r As DataGridViewRow = Me.dgvEmailLog.Rows(e.RowIndex)
            Dim path As String = r.Cells(6).Value

            Dim convertToMsg As Boolean = Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("ConvertToMsg", 0))

            If path <> "" Then
                Try
                    If convertToMsg = False Then
                        Process.Start(path)
                    Else
                        path = clsMarsMessaging.convertToMsg(path)

                        If path IsNot Nothing Then Process.Start(path)
                    End If
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, 6565)
                End Try
            End If
        Catch : End Try
    End Sub

    Private Sub dgvEmailLog_CellValueNeeded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValueEventArgs) Handles dgvEmailLog.CellValueNeeded
        If e.RowIndex >= 0 AndAlso e.ColumnIndex = 0 Then
            e.Value = My.Resources.mail
        End If
    End Sub

    Private Sub dgvEmailLog_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvEmailLog.KeyUp
        If e.KeyCode = Keys.Delete Or e.KeyCode = Keys.Back Then
            Me.tmEmailLog.Stop()
            For Each r As DataGridViewRow In Me.dgvEmailLog.SelectedRows
                Dim emailID As Integer = r.Cells(1).Value
                Dim filePath As String = r.Cells(6).Value

                If clsMarsData.WriteData("DELETE FROM EmailLog WHERE LogID =" & emailID) = True Then
                    Try
                        If filePath <> "" Then
                            IO.File.Delete(filePath)
                        End If
                    Catch : End Try
                End If
            Next

            Me.LoadEmailLogDGV()
            Me.tmEmailLog.Start()
        End If
    End Sub

  
    'Private Sub emailLogFilterMan_PopupShowing(ByVal sender As Object, ByVal e As DgvFilterPopup.ColumnFilterEventArgs) Handles emailLogFilterMan.PopupShowing
    '    Me.tmEmailLog.Stop()
    '    Me.btnRefreshEmailLog.Visible = True
    'End Sub

    Private Sub btnRefreshEmailLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshEmailLog.Click
        Me.btnRefreshEmailLog.Enabled = False
        Me.LoadEmailLogDGV()
        Me.btnRefreshEmailLog.Enabled = True
    End Sub
End Class
