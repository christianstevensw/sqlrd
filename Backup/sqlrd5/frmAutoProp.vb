Public Class frmAutoProp
    Inherits System.Windows.Forms.Form
    Dim ScheduleID As Integer
    Dim OkayToClose As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents imgTools As System.Windows.Forms.ImageList
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents grpSort As System.Windows.Forms.GroupBox
    Friend WithEvents ucUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents imgAuto As System.Windows.Forms.ImageList
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents tabProperties As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbHistory As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbTasks As DevComponents.DotNetBar.TabItem
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoProp))
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ucUpdate = New sqlrd.ucScheduleUpdate
        Me.txtID = New System.Windows.Forms.TextBox
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdRefresh = New System.Windows.Forms.Button
        Me.grpSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.imgAuto = New System.Windows.Forms.ImageList(Me.components)
        Me.imgTools = New System.Windows.Forms.ImageList(Me.components)
        Me.UcTasks = New sqlrd.ucTasks
        Me.tabProperties = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbSchedule = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbGeneral = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbHistory = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbTasks = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.grpSort.SuspendLayout()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(500, 382)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(412, 382)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(324, 382)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(4, 112)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(424, 129)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(4, 244)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 29
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(4, 260)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(424, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(372, 72)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "...."
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(4, 72)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(344, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(4, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(4, 24)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(424, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Schedule Name"
        '
        'ucUpdate
        '
        Me.ucUpdate.BackColor = System.Drawing.Color.Transparent
        Me.ucUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucUpdate.Location = New System.Drawing.Point(13, 8)
        Me.ucUpdate.m_RepeatUnit = ""
        Me.ucUpdate.Name = "ucUpdate"
        Me.ucUpdate.Size = New System.Drawing.Size(495, 360)
        Me.ucUpdate.TabIndex = 0
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(352, 538)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(100, 21)
        Me.txtID.TabIndex = 1
        Me.txtID.Visible = False
        '
        'cmdClear
        '
        Me.cmdClear.Image = CType(resources.GetObject("cmdClear.Image"), System.Drawing.Image)
        Me.cmdClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(13, 345)
        Me.cmdClear.Name = "cmdClear"
        Me.cmdClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdClear.TabIndex = 3
        Me.cmdClear.Text = "Clear"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Image = CType(resources.GetObject("cmdRefresh.Image"), System.Drawing.Image)
        Me.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRefresh.Location = New System.Drawing.Point(101, 345)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 2
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpSort
        '
        Me.grpSort.BackColor = System.Drawing.Color.Transparent
        Me.grpSort.Controls.Add(Me.optAsc)
        Me.grpSort.Controls.Add(Me.optDesc)
        Me.grpSort.Location = New System.Drawing.Point(13, 16)
        Me.grpSort.Name = "grpSort"
        Me.grpSort.Size = New System.Drawing.Size(272, 48)
        Me.grpSort.TabIndex = 0
        Me.grpSort.TabStop = False
        Me.grpSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 0
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 1
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'tvHistory
        '
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgAuto
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(13, 70)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.tvHistory.Size = New System.Drawing.Size(440, 264)
        Me.tvHistory.TabIndex = 1
        '
        'imgAuto
        '
        Me.imgAuto.ImageStream = CType(resources.GetObject("imgAuto.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgAuto.TransparentColor = System.Drawing.Color.Transparent
        Me.imgAuto.Images.SetKeyName(0, "")
        Me.imgAuto.Images.SetKeyName(1, "")
        Me.imgAuto.Images.SetKeyName(2, "")
        Me.imgAuto.Images.SetKeyName(3, "")
        Me.imgAuto.Images.SetKeyName(4, "")
        Me.imgAuto.Images.SetKeyName(5, "")
        '
        'imgTools
        '
        Me.imgTools.ImageStream = CType(resources.GetObject("imgTools.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTools.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTools.Images.SetKeyName(0, "")
        Me.imgTools.Images.SetKeyName(1, "")
        Me.imgTools.Images.SetKeyName(2, "")
        Me.imgTools.Images.SetKeyName(3, "")
        Me.imgTools.Images.SetKeyName(4, "")
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(0, 0)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(440, 352)
        Me.UcTasks.TabIndex = 0
        '
        'tabProperties
        '
        Me.tabProperties.CanReorderTabs = True
        Me.tabProperties.Controls.Add(Me.TabControlPanel4)
        Me.tabProperties.Controls.Add(Me.TabControlPanel3)
        Me.tabProperties.Controls.Add(Me.TabControlPanel2)
        Me.tabProperties.Controls.Add(Me.TabControlPanel1)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(587, 376)
        Me.tabProperties.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabIndex = 17
        Me.tabProperties.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabProperties.Tabs.Add(Me.tbGeneral)
        Me.tabProperties.Tabs.Add(Me.tbSchedule)
        Me.tabProperties.Tabs.Add(Me.tbHistory)
        Me.tabProperties.Tabs.Add(Me.tbTasks)
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.ucUpdate)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(78, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(509, 376)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbSchedule
        '
        'tbSchedule
        '
        Me.tbSchedule.AttachedControl = Me.TabControlPanel2
        Me.tbSchedule.Image = CType(resources.GetObject("tbSchedule.Image"), System.Drawing.Image)
        Me.tbSchedule.Name = "tbSchedule"
        Me.tbSchedule.Text = "Schedule"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.txtDesc)
        Me.TabControlPanel1.Controls.Add(Me.Label2)
        Me.TabControlPanel1.Controls.Add(Me.Label3)
        Me.TabControlPanel1.Controls.Add(Me.txtName)
        Me.TabControlPanel1.Controls.Add(Me.Label7)
        Me.TabControlPanel1.Controls.Add(Me.Label4)
        Me.TabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.TabControlPanel1.Controls.Add(Me.txtFolder)
        Me.TabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(78, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(509, 376)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbGeneral
        '
        'tbGeneral
        '
        Me.tbGeneral.AttachedControl = Me.TabControlPanel1
        Me.tbGeneral.Image = CType(resources.GetObject("tbGeneral.Image"), System.Drawing.Image)
        Me.tbGeneral.Name = "tbGeneral"
        Me.tbGeneral.Text = "General"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.cmdClear)
        Me.TabControlPanel3.Controls.Add(Me.cmdRefresh)
        Me.TabControlPanel3.Controls.Add(Me.grpSort)
        Me.TabControlPanel3.Controls.Add(Me.tvHistory)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(78, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(509, 376)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbHistory
        '
        'tbHistory
        '
        Me.tbHistory.AttachedControl = Me.TabControlPanel3
        Me.tbHistory.Image = CType(resources.GetObject("tbHistory.Image"), System.Drawing.Image)
        Me.tbHistory.Name = "tbHistory"
        Me.tbHistory.Text = "History"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.UcTasks)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(78, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(509, 376)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbTasks
        '
        'tbTasks
        '
        Me.tbTasks.AttachedControl = Me.TabControlPanel4
        Me.tbTasks.Image = CType(resources.GetObject("tbTasks.Image"), System.Drawing.Image)
        Me.tbTasks.Name = "tbTasks"
        Me.tbTasks.Text = "Tasks"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'frmAutoProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(587, 410)
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtID)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmAutoProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Automation Schedule Properties"
        Me.grpSort.ResumeLayout(False)
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmAutoProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        clsMarsUI.MainUI.InitHistoryImages(tvHistory, My.Resources.document_out)
    End Sub

    Public Sub EditSchedule(ByVal nAutoID As Integer)
        Dim oData As New clsMarsData
        Dim SQL As String

        UcTasks.ShowAfterType = False
        UcTasks.oAuto = True
        UcTasks.lsvTasks.HeaderStyle = ColumnHeaderStyle.None

        SQL = "SELECT * FROM AutomationAttr WHERE AutoID = " & nAutoID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("autoname").Value

            Me.Text = "Automation Schedule Properties - " & txtName.Text

            txtFolder.Tag = oRs("parent").Value
            txtID.Text = oRs("autoid").Value
            oRs.Close()
        End If

        SQL = "SELECT FolderName FROM Folders WHERE FolderID =" & txtFolder.Tag

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtFolder.Text = oRs.Fields(0).Value
        End If

        oRs.Close()

        SQL = "SELECT * FROM ScheduleAttr WHERE AutoID = " & nAutoID

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            ScheduleID = oRs("scheduleid").Value

            Select Case CType(oRs("frequency").Value, String).ToLower
                Case "daily"
                    ucUpdate.optDaily.Checked = True
                Case "weekly"
                    ucUpdate.optWeekly.Checked = True
                Case "week-daily"
                    ucUpdate.optWeekDaily.Checked = True
                Case "monthly"
                    ucUpdate.optMonthly.Checked = True
                Case "yearly"
                    ucUpdate.optYearly.Checked = True
                Case "custom"
                    ucUpdate.optCustom.Checked = True
                    ucUpdate.cmbCustom.Text = oRs("calendarname").Value
                Case "other"
                    ucUpdate.optOther.Checked = True
                Case "none"
                    ucUpdate.optNone.Checked = True
            End Select

            ucUpdate.m_fireOthers = True
            ucUpdate.chkRepeat.Enabled = ucUpdate.m_EnableRepeat(ScheduleID)

            Try
                If oRs("status").Value = 0 Then
                    ucUpdate.chkStatus.Checked = False
                Else
                    ucUpdate.chkStatus.Checked = True
                End If
            Catch
                ucUpdate.chkStatus.Checked = True
            End Try

            Try
                ucUpdate.m_RepeatUnit = IsNull(oRs("repeatunit").Value, "hours")
            Catch ex As Exception
                ucUpdate.m_RepeatUnit = "hours"
            End Try

            txtDesc.Text = IsNull(oRs("description").Value)
            txtKeyWord.Text = IsNull(oRs("keyword").Value)

            UcTasks.ScheduleID = oRs("scheduleid").Value
            ucUpdate.ScheduleID = oRs("scheduleid").Value

            UcTasks.LoadTasks()

            Try
                Dim repeatUntil As String = IsNull(oRs("repeatuntil").Value, Now)

                If repeatUntil.Length > 8 Then
                    ucUpdate.RepeatUntil.Value = ConDateTime(CTimeZ(repeatUntil, dateConvertType.READ))
                Else
                    ucUpdate.RepeatUntil.Value = CTimeZ(Now.Date & " " & repeatUntil, dateConvertType.READ)
                End If
            Catch : End Try

            ucUpdate.NextRun.Value = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
            ucUpdate.EndDate.Value = CTimeZ(oRs("enddate").Value, dateConvertType.READ)
            ucUpdate.RunAt.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & oRs("starttime").Value), dateConvertType.READ)
            ucUpdate.NextRunTime.Value = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
            ucUpdate.chkRepeat.Checked = Convert.ToBoolean(oRs("repeat").Value)
            ucUpdate.cmbRpt.Text = IsNonEnglishRegionRead(oRs("repeatinterval").Value)
            ucUpdate.StartDate.Value = ConDate(CTimeZ(oRs("startdate").Value, dateConvertType.READ))

            ucUpdate.cmbException.Text = IsNull(oRs("exceptioncalendar").Value)

            Try
                ucUpdate.chkException.Checked = Convert.ToBoolean(oRs("useexception").Value)
            Catch
                ucUpdate.chkException.Checked = False
            End Try

            Try
                Dim d As Date

                d = oRs("enddate").Value

                If d.Year >= 3004 Then
                    ucUpdate.EndDate.Enabled = False
                    ucUpdate.chkNoEnd.Checked = True
                End If

            Catch : End Try

            ucUpdate.btnApply = Me.cmdApply
        End If

        ucUpdate.IsLoaded = True

        Me.ShowDialog()
    End Sub


    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        If UcTasks.lsvTasks.Items.Count = 0 Then
            ep.SetError(UcTasks.lsvTasks, "Please add a custom action to the schedule")
            OkayToClose = False
            tabProperties.SelectedTabIndex = 3
            Return
        ElseIf txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter a name for this schedule")
            OkayToClose = False
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf txtFolder.Text.Length = 0 Then
            ep.SetError(txtFolder, "Please select the parent folder for the schedule")
            OkayToClose = False
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text) = False Then
            ep.SetError(txtName, "An Automation schedule with this name already exists in this folder. Please use a different name")
            OkayToClose = False
            txtName.Focus()
            Me.tabProperties.SelectedTabIndex = 0
            Return
        ElseIf Me.ucUpdate.ValidateSchedule(Me.tabProperties, ScheduleID) = False Then
            OkayToClose = False
            Return
        End If

        Dim oData As New clsMarsData

        Dim SQL As String

        SQL = "UPDATE AutomationAttr SET " & _
            "AutoName = '" & SQLPrepare(txtName.Text) & "'," & _
            "Parent = " & txtFolder.Tag & " WHERE AutoID = " & txtID.Text

        clsMarsData.WriteData(SQL)

        Dim nRepeat As String = "0"

        Try
            nRepeat = Convert.ToString(ucUpdate.cmbRpt.Value).Replace(",", ".")
        Catch ex As Exception
            ''console.writeLine(ex.Message)
            nRepeat = "0"
        End Try

        With ucUpdate
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            SQL = "UPDATE ScheduleAttr SET " & _
                "Frequency = '" & .sFrequency & "', " & _
                "EndDate = '" & .m_endDate & "', " & _
                "NextRun = '" & .m_nextRun & "', " & _
                "StartTime = '" & .m_startTime & "', " & _
                "StartDate = '" & .m_startDate & "'," & _
                "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "RepeatInterval = '" & nRepeat & "'," & _
                "Status = " & Convert.ToInt32(.chkStatus.Checked) & "," & _
                "RepeatUntil = '" & .m_repeatUntil & "', " & _
                "Description = '" & SQLPrepare(txtDesc.Text) & "'," & _
                "KeyWord = '" & SQLPrepare(txtKeyWord.Text) & "'," & _
                "CalendarName = '" & SQLPrepare(.cmbCustom.Text) & "', " & _
                "UseException = " & Convert.ToInt32(.chkException.Checked) & "," & _
                "ExceptionCalendar ='" & SQLPrepare(.cmbException.Text) & "', " & _
                "RepeatUnit = '" & ucUpdate.m_RepeatUnit & "' " & _
                "WHERE AutoID = " & txtID.Text
        End With

        clsMarsData.WriteData(SQL)

        UcTasks.CommitDeletions()

        cmdApply.Enabled = False

        OkayToClose = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)

        If OkayToClose = False Then Return

        Close()

        Dim oUI As New clsMarsUI

        oUI.RefreshView(oWindow(nWindowCurrent))

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.EDIT)
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged

        If txtID.Text.Length = 0 Then Return

        grpSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text, " ASC ")

        grpSort.Enabled = True
    End Sub

    Private Sub optDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDesc.CheckedChanged

        If txtID.Text.Length = 0 Then Return

        grpSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text, " DESC ")

        grpSort.Enabled = True
    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim oData As New clsMarsData

            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE AutoID =" & txtID.Text, False)

            _Delay(1)

            cmdRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        If txtID.Text.Length = 0 Then Return

        grpSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        If optDesc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text, " DESC ")
        Else
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text, " ASC ")
        End If

        grpSort.Enabled = True
    End Sub

    Private Sub tabProperties_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "history"

                Dim oSchedule As New clsMarsScheduler
                Dim sOrder As String

                If optAsc.Checked = True Then
                    sOrder = " ASC "
                Else
                    sOrder = " DESC"
                End If

                oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.AUTOMATION, txtID.Text, sOrder)
        End Select
    End Sub
End Class
