Imports Microsoft.Win32
Public Class frmSplash
    Inherits System.Windows.Forms.Form
    Public IsValidated As Boolean = False
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Dim AutoLogin As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lblUserName As System.Windows.Forms.Label
    Friend WithEvents lblEdition As System.Windows.Forms.Label
    Friend WithEvents lblVersion As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblCustomerNo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents lblReg As System.Windows.Forms.Label
    Friend WithEvents tmAutoClose As System.Timers.Timer
    Friend WithEvents chkRem As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSplash))
        Me.lblUserName = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblCustomerNo = New System.Windows.Forms.Label
        Me.lblReg = New System.Windows.Forms.Label
        Me.chkRem = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.lblEdition = New System.Windows.Forms.Label
        Me.lblVersion = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tmAutoClose = New System.Timers.Timer
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        CType(Me.tmAutoClose, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblUserName
        '
        Me.lblUserName.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblUserName.Location = New System.Drawing.Point(3, 0)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(224, 16)
        Me.lblUserName.TabIndex = 0
        Me.lblUserName.Text = "Beta Tester"
        '
        'lblCompany
        '
        Me.lblCompany.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCompany.Location = New System.Drawing.Point(3, 16)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(224, 16)
        Me.lblCompany.TabIndex = 0
        Me.lblCompany.Text = "Beta Tester"
        '
        'lblCustomerNo
        '
        Me.lblCustomerNo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCustomerNo.Location = New System.Drawing.Point(3, 48)
        Me.lblCustomerNo.Name = "lblCustomerNo"
        Me.lblCustomerNo.Size = New System.Drawing.Size(224, 16)
        Me.lblCustomerNo.TabIndex = 0
        Me.lblCustomerNo.Text = "Customer #:"
        '
        'lblReg
        '
        Me.lblReg.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblReg.Location = New System.Drawing.Point(3, 32)
        Me.lblReg.Name = "lblReg"
        Me.lblReg.Size = New System.Drawing.Size(224, 16)
        Me.lblReg.TabIndex = 0
        Me.lblReg.Text = "Beta Tester"
        '
        'chkRem
        '
        Me.chkRem.BackColor = System.Drawing.Color.White
        Me.chkRem.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRem.Location = New System.Drawing.Point(123, 192)
        Me.chkRem.Name = "chkRem"
        Me.chkRem.Size = New System.Drawing.Size(169, 17)
        Me.chkRem.TabIndex = 2
        Me.chkRem.Text = "Remember login"
        Me.chkRem.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(3, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(54, 27)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Password"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(63, 3)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(119, 21)
        Me.txtUserID.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 27)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "UserID"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(63, 30)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(119, 21)
        Me.txtPassword.TabIndex = 1
        '
        'lblEdition
        '
        Me.lblEdition.AutoSize = True
        Me.lblEdition.BackColor = System.Drawing.Color.White
        Me.lblEdition.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblEdition.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblEdition.Location = New System.Drawing.Point(39, 283)
        Me.lblEdition.Name = "lblEdition"
        Me.lblEdition.Size = New System.Drawing.Size(107, 13)
        Me.lblEdition.TabIndex = 6
        Me.lblEdition.Text = "Evaluation Edition"
        Me.lblEdition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblVersion
        '
        Me.lblVersion.BackColor = System.Drawing.Color.White
        Me.lblVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblVersion.Location = New System.Drawing.Point(443, 283)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(192, 12)
        Me.lblVersion.TabIndex = 6
        Me.lblVersion.Text = "Label2"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(410, 16)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "All rights reserved. This product is protected by US and international copyright " & _
            "laws"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 6.0!)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(425, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(167, 16)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Copyright ChristianSteven Software � 2007"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tmAutoClose
        '
        Me.tmAutoClose.Interval = 5000
        Me.tmAutoClose.SynchronizingObject = Me
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.32692!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.67308!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label4, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtUserID, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.txtPassword, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(58, 130)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(208, 54)
        Me.TableLayoutPanel2.TabIndex = 4
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.White
        Me.FlowLayoutPanel1.Controls.Add(Me.lblUserName)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCompany)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblReg)
        Me.FlowLayoutPanel1.Controls.Add(Me.lblCustomerNo)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(42, 43)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(175, 69)
        Me.FlowLayoutPanel1.TabIndex = 13
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.BackColor = System.Drawing.Color.White
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label3, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(39, 316)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 2
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(595, 16)
        Me.TableLayoutPanel4.TabIndex = 17
        '
        'cmdOK
        '
        Me.cmdOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdOK.BackColor = System.Drawing.Color.White
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(123, 215)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(56, 23)
        Me.cmdOK.TabIndex = 3
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.White
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(191, 215)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(57, 23)
        Me.cmdCancel.TabIndex = 4
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(39, 275)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(596, 5)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 14
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.sqlrd.My.Resources.Resources.logo_sarah_sqlrd5
        Me.PictureBox3.Location = New System.Drawing.Point(11, 9)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(660, 355)
        Me.PictureBox3.TabIndex = 16
        Me.PictureBox3.TabStop = False
        '
        'frmSplash
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.BackColor = System.Drawing.Color.Gray
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(672, 365)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.chkRem)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.lblEdition)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.PictureBox3)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSplash"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.tmAutoClose, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    'Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
    '    Get
    '        Const CS_DROPSHADOW As Object = &H20000
    '        Dim cp As CreateParams = MyBase.CreateParams
    '        Dim OSVer As Version = System.Environment.OSVersion.Version

    '        Select Case OSVer.Major
    '            Case 5
    '                If OSVer.Minor > 0 Then
    '                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
    '                End If
    '            Case Is > 5
    '                cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
    '            Case Else
    '        End Select

    '        Return cp
    '    End Get
    'End Property
    Private Sub LoadPic()
        On Error Resume Next
        Dim LastPic As String
        Dim oUI As clsMarsUI = New clsMarsUI

        LastPic = oUI.ReadRegistry("LastPic", "0")

        If LastPic = "0" Then
            PictureBox3.Image = My.Resources.logo_sarah_sqlrd5
            oUI.SaveRegistry("LastPic", "10")
        ElseIf LastPic = "10" Then
            PictureBox3.Image = My.Resources.logo_sarah_sqlrd5
            oUI.SaveRegistry("LastPic", "1")
        Else
            PictureBox3.Image = My.Resources.logo_helen_sqlrd5
            oUI.SaveRegistry("LastPic", "0")
        End If
    End Sub
    Private Sub frmSplash_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Height = NicePanel1.Height
        'Me.Width = NicePanel1.Width

        LoadPic()

        Dim oFile As FileVersionInfo = FileVersionInfo.GetVersionInfo(sAppPath & "sqlrd.exe")

        lblVersion.Text = oFile.FileMajorPart & "." & oFile.FileMinorPart & " " & oFile.Comments

        lblEdition.Text = gsEdition & " Edition"

        FormatForWinXP(Me)

        Dim oUI As New clsMarsUI

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim UnifiedLogin As Boolean

        Try
            UnifiedLogin = oUI.ReadRegistry("UnifiedLogin", 0)
        Catch ex As Exception
            UnifiedLogin = False
        End Try


        oRs = clsMarsData.GetData("SELECT * FROM CRDUsers WHERE AutoLogin = 1")

        Try
            If Not oRs Is Nothing Then
                If oRs.EOF = False Then
                    txtUserID.Text = oRs("userid").Value
                    txtPassword.Text = Decrypt(oRs("password").Value, "qwer][po")
                    chkRem.Checked = True
                    tmAutoClose.Enabled = True
                Else
                    txtUserID.Text = oUI.ReadRegistry("LastUserID", String.Empty)
                End If

                oRs.Close()
            Else
                txtUserID.Text = oUI.ReadRegistry("LastUserID", String.Empty)
            End If
        Catch : End Try

        Dim Custno As String
        Dim RegNo As String
        Dim firstName, lastName As String
        Dim UserCo As String
        Dim oSec As New clsMarsSecurity

        With oUI
            Custno = .ReadRegistry("CustNo", "", , , True)
            firstName = .ReadRegistry("RegFirstName", "", , , True)
            lastName = .ReadRegistry("RegLastName", "", , , True)
            UserCo = .ReadRegistry("RegCo", "", , , True)
            RegNo = .ReadRegistry("RegNo", "", , , True)
        End With

        If clsLicenser.m_licenser.CheckLicense(RegNo, firstName, lastName, UserCo, Custno) = False Then RegNo = "Not Licensed"

        lblCustomerNo.Text = "Customer #: " & Custno
        lblUserName.Text = firstName & " " & lastName
        lblCompany.Text = UserCo
        lblReg.Text = RegNo

        If (Custno.StartsWith("30") = False Or Custno.Length < 7) And _
       (gnEdition = 0 Or gnEdition = 10) Then
            Dim oRes As DialogResult = MessageBox.Show("You have not yet registered your " & _
            "evaluation edition.  Register now and get Free technical and " & _
            "pre-sales support", Application.ProductName, MessageBoxButtons.OKCancel, _
             MessageBoxIcon.Information)

            If oRes = Windows.Forms.DialogResult.OK Then
                clsWebProcess.Start("http://www.christiansteven.com/sql-rd/purchase/register.htm") '.
            End If
        End If

        If gsEdition = "Evaluation" Then
            lblEdition.Text = lblEdition.Text & " (" & nTimeLeft & " Days Left)"
        End If

        If UnifiedLogin = True Then
            txtUserID.Text = Environment.UserDomainName & "\" & Environment.UserName
            txtUserID.Enabled = False
            txtPassword.Enabled = False
            txtPassword.Text = "gibrishish"
            chkRem.Checked = False
            chkRem.Enabled = False
            tmAutoClose.Enabled = True
        End If
    End Sub


    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        IsValidated = False
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim oSec As New clsMarsSecurity
        Dim UnifiedLogin As Boolean
        Dim oUI As New clsMarsUI

        Try
            UnifiedLogin = oUI.ReadRegistry("UnifiedLogin", 0)
        Catch ex As Exception
            UnifiedLogin = False
        End Try

        tmAutoClose.Enabled = False

        If UnifiedLogin = False Then
            If oSec._ValidatePassword(txtUserID.Text, txtPassword.Text) = True Then
                gUser = txtUserID.Text
                CloseInStyle()
                SavePrevUser()
                IsValidated = True
                oMain.tmCheckUpdate.Enabled = True

                Dim oData As New clsMarsData
                Dim SQL As String

                SQL = "UPDATE CRDUsers SET [AutoLogin] =" & Convert.ToInt32(chkRem.Checked) & " WHERE [UserID] = '" & SQLPrepare(Me.txtUserID.Text) & "'"

                clsMarsData.WriteData(SQL, False)

                If chkRem.Checked = True Then
                    SQL = "UPDATE CRDUsers SET [AutoLogin] =" & 0 & " WHERE [UserID] <> '" & SQLPrepare(Me.txtUserID.Text) & "'"

                    clsMarsData.WriteData(SQL, False)
                End If
            Else
                ep.SetError(txtPassword, "Invalid login credentials provided. Please check your username and password.")
                txtPassword.Focus()
                txtPassword.SelectAll()
                IsValidated = False
            End If
        Else
            Dim sUser As String
            Dim oData As New clsMarsData
            Dim oRs As ADODB.Recordset
            Dim switchBack As DialogResult
            Dim sMsg As String = "The current NT user has not been authorized to access SQL-RD. Switch back to SQL-RD aunthentication?"

            sUser = txtUserID.Text

            oRs = clsMarsData.GetData("SELECT * FROM DomainAttr WHERE DomainName ='" & SQLPrepare(sUser) & "'")

            If oRs Is Nothing Then
                switchBack = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                If switchBack = Windows.Forms.DialogResult.Yes Then
                    Dim oAuth As frmRemoteLogin = New frmRemoteLogin

                    If oAuth.AdminCheck = True Then
                        oUI.SaveRegistry("UnifiedLogin", 0, , , True)
                        MessageBox.Show("Please restart SQL-RD in order to log in using SQL-RD aunthentication.", _
                        Application.ProductName, MessageBoxButtons.OK)
                    End If
                End If

                CloseInStyle()
                IsValidated = False
            ElseIf oRs.EOF = True Then
                switchBack = MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

                If switchBack = Windows.Forms.DialogResult.Yes Then
                    Dim oAuth As frmRemoteLogin = New frmRemoteLogin

                    If oAuth.AdminCheck = True Then
                        oUI.SaveRegistry("UnifiedLogin", 0)
                        MessageBox.Show("Please restart SQL-RD in order to log in using SQL-RD aunthentication.", _
                        Application.ProductName, MessageBoxButtons.OK)
                    End If
                End If

                CloseInStyle()
                IsValidated = False

            Else
                gUser = txtUserID.Text
                gRole = oRs("userrole").Value
                IsValidated = True
                CloseInStyle()
                oMain.tmCheckUpdate.Enabled = True
            End If
        End If

    End Sub

    Public Sub CloseInStyle()
        'Dim I As Double = 1
        'Dim nDelay As Double = 0.0625


        'While (Me.Opacity > 0)
        '    Me.Opacity -= nDelay
        '    System.Threading.Thread.Sleep(94)
        'End While

        Me.Close()
    End Sub
    Private Sub SavePrevUser()
        Dim oUI As New clsMarsUI

        oUI.SaveRegistry("LastUserID", txtUserID.Text)
    End Sub

    Private Sub lblEval_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)
        clsWebProcess.Start("http://www.christiansteven.com/purchase_sql-rd.htm") ''
    End Sub

    Private Sub tmAutoClose_Elapsed(ByVal sender As System.Object, ByVal e As System.Timers.ElapsedEventArgs) Handles tmAutoClose.Elapsed
        cmdOK_Click(sender, e)
        tmAutoClose.Enabled = False
    End Sub

    Private Sub txtUserID_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtUserID.GotFocus, txtPassword.GotFocus
        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.BackColor = Color.Yellow
    End Sub

    Private Sub txtUserID_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) _
    Handles txtUserID.LostFocus, txtPassword.LostFocus
        Dim txtBox As TextBox = CType(sender, TextBox)

        txtBox.BackColor = Color.White
    End Sub

    Private Sub txtUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserID.TextChanged
        tmAutoClose.Enabled = False
    End Sub

    Private Sub txtPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPassword.TextChanged
        tmAutoClose.Enabled = False
        ep.SetError(txtPassword, "")
    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim sLink As String

        Try
            sLink = ReadTextFromFile(sAppPath & "link.xml")

            If sLink.Length > 0 Then
                Process.Start(sLink)
            Else
                clsWebProcess.Start("http://www.christiansteven.com/main.htm") ''
            End If
        Catch
            clsWebProcess.Start("http://www.christiansteven.com/main.htm") ''
        End Try
    End Sub


    Private Sub chkRem_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRem.CheckedChanged
        tmAutoClose.Enabled = False
    End Sub

    
    Private Sub PictureBox3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox3.Click

    End Sub
End Class
