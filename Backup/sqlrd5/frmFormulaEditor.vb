Public Class frmFormulaEditor
    Inherits System.Windows.Forms.Form
    Dim UserCancel As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbName As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDefinition As System.Windows.Forms.TextBox
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents UndoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DeleteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents InsertToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ConstantsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents DatabaseFieldToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFormulaEditor))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtDefinition = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DeleteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
        Me.InsertToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ConstantsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripSeparator
        Me.DatabaseFieldToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbName = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdSave = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.mnuInserter.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtDefinition)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cmbName)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 288)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'txtDefinition
        '
        Me.txtDefinition.ContextMenuStrip = Me.mnuInserter
        Me.txtDefinition.Location = New System.Drawing.Point(8, 80)
        Me.txtDefinition.Multiline = True
        Me.txtDefinition.Name = "txtDefinition"
        Me.txtDefinition.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefinition.Size = New System.Drawing.Size(256, 200)
        Me.txtDefinition.TabIndex = 1
        Me.txtDefinition.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.UndoToolStripMenuItem, Me.ToolStripMenuItem1, Me.CutToolStripMenuItem, Me.CopyToolStripMenuItem, Me.PasteToolStripMenuItem, Me.DeleteToolStripMenuItem, Me.ToolStripMenuItem2, Me.InsertToolStripMenuItem, Me.ToolStripMenuItem3, Me.SelectAllToolStripMenuItem})
        Me.mnuInserter.Name = "mnuInserter"
        Me.mnuInserter.Size = New System.Drawing.Size(123, 176)
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.UndoToolStripMenuItem.Text = "Undo"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(119, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.CutToolStripMenuItem.Text = "Cut"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.CopyToolStripMenuItem.Text = "Copy"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.PasteToolStripMenuItem.Text = "Paste"
        '
        'DeleteToolStripMenuItem
        '
        Me.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem"
        Me.DeleteToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.DeleteToolStripMenuItem.Text = "Delete"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(119, 6)
        '
        'InsertToolStripMenuItem
        '
        Me.InsertToolStripMenuItem.Name = "InsertToolStripMenuItem"
        Me.InsertToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.InsertToolStripMenuItem.Text = "Select All"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(119, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConstantsToolStripMenuItem, Me.ToolStripMenuItem4, Me.DatabaseFieldToolStripMenuItem})
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.SelectAllToolStripMenuItem.Text = "Insert"
        '
        'ConstantsToolStripMenuItem
        '
        Me.ConstantsToolStripMenuItem.Name = "ConstantsToolStripMenuItem"
        Me.ConstantsToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.ConstantsToolStripMenuItem.Text = "Constants"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(147, 6)
        '
        'DatabaseFieldToolStripMenuItem
        '
        Me.DatabaseFieldToolStripMenuItem.Name = "DatabaseFieldToolStripMenuItem"
        Me.DatabaseFieldToolStripMenuItem.Size = New System.Drawing.Size(150, 22)
        Me.DatabaseFieldToolStripMenuItem.Text = "Database Field"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Formula Name"
        '
        'cmbName
        '
        Me.cmbName.ItemHeight = 13
        Me.cmbName.Location = New System.Drawing.Point(8, 32)
        Me.cmbName.Name = "cmbName"
        Me.cmbName.Size = New System.Drawing.Size(256, 21)
        Me.cmbName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Definition"
        '
        'cmdSave
        '
        Me.cmdSave.Enabled = False
        Me.cmdSave.Image = CType(resources.GetObject("cmdSave.Image"), System.Drawing.Image)
        Me.cmdSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSave.Location = New System.Drawing.Point(304, 8)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(75, 23)
        Me.cmdSave.TabIndex = 3
        Me.cmdSave.Text = "&Save"
        '
        'cmdCancel
        '
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(304, 40)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdDelete
        '
        Me.cmdDelete.Enabled = False
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(304, 72)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 1
        Me.cmdDelete.Text = "&Delete"
        '
        'frmFormulaEditor
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(386, 296)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdDelete)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmFormulaEditor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Formula Editor"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.mnuInserter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmFormulaEditor_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Function GetName() As String
        Me.ShowDialog()

        Return cmbName.Text
    End Function
    Private Sub cmbName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbName.SelectedIndexChanged
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData
        Dim SQL As String

        SQL = "SELECT * FROM FormulaAttr WHERE FormulaName ='" & _
        SQLPrepare(cmbName.Text) & "'"

        oRs = clsmarsdata.GetData(SQL)

        Try
            If oRs.EOF = False Then
                txtDefinition.Text = oRs("formuladef").Value
            End If

            oRs.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub cmbName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbName.TextChanged
        If cmbName.Text.Length = 0 Then
            cmdSave.Enabled = False
            cmdDelete.Enabled = False
        Else
            cmdSave.Enabled = True
            cmdDelete.Enabled = True
        End If
    End Sub

    Private Sub cmdSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sMsg As String

        If clsMarsData.IsDuplicate("FormulaAttr", "FormulaName" _
        , cmbName.Text, False) = True Then

            SQL = "UPDATE FormulaAttr SET FormulaDef = '" & _
            SQLPrepare(txtDefinition.Text) & "' WHERE " & _
            "FormulaName = '" & SQLPrepare(cmbName.Text) & "'"

            sMsg = "Constant updated successfully"
        Else

            SQL = "INSERT INTO FormulaAttr " & _
            "(FormulaID,FormulaName,FormulaDef) " & _
            "VALUES (" & _
            clsMarsData.CreateDataID("formulaattr", "formulaid") & "," & _
            "'" & SQLPrepare(cmbName.Text) & "'," & _
            "'" & SQLPrepare(txtDefinition.Text) & "')"

            sMsg = "Constant saved successfully"
        End If

        If clsMarsData.WriteData(SQL) = True Then
            cmbName.Items.Add(cmbName.Text)

            MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Information)

            Close()
        End If
    End Sub

    Private Sub txtDefinition_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefinition.TextChanged
        If txtDefinition.Text.Length = 0 Then
            cmdSave.Enabled = False
        Else
            cmdSave.Enabled = True
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oRes As DialogResult

        oRes = MessageBox.Show("Delete the selected formula?", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question)

        If oRes = Windows.Forms.DialogResult.Yes Then
            SQL = "DELETE FROM FormulaAttr WHERE FormulaName ='" & _
            SQLPrepare(cmbName.Text) & "'"

            clsMarsData.WriteData(SQL)

            txtDefinition.Text = String.Empty

            cmbName.Items.Remove(cmbName.Text)
        End If
    End Sub

    Private Sub cmbName_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbName.DropDown
        Dim SQL As String
        Dim oData As New clsMarsData

        cmbName.Items.Clear()

        SQL = "SELECT DISTINCT FormulaName FROM FormulaAttr"

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbName.Items.Add(oRs(0).Value)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub UndoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UndoToolStripMenuItem.Click
        txtDefinition.Undo()
    End Sub

    Private Sub CutToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CutToolStripMenuItem.Click
        txtDefinition.Cut()
    End Sub

    Private Sub CopyToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyToolStripMenuItem.Click
        txtDefinition.Copy()
    End Sub

    Private Sub PasteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PasteToolStripMenuItem.Click
        txtDefinition.Paste()
    End Sub

    Private Sub DeleteToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteToolStripMenuItem.Click
        txtDefinition.SelectedText = ""
    End Sub

    Private Sub InsertToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InsertToolStripMenuItem.Click
        txtDefinition.SelectAll()
    End Sub

    Private Sub ConstantsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConstantsToolStripMenuItem.Click
        Dim inserter As frmInserter = New frmInserter(0)
        inserter.m_HideUserConstants = True
        inserter.GetConstants(Me)
    End Sub

    Private Sub DatabaseFieldToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DatabaseFieldToolStripMenuItem.Click
        Dim oItem As New frmDataItems

        txtDefinition.SelectedText = oItem._GetDataItem(0)
    End Sub
End Class
