Public Class frmViewOptons
    Inherits sqlrd.frmTaskMaster
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem2 As DevComponents.DotNetBar.TabItem
    Friend WithEvents optClassic As System.Windows.Forms.RadioButton
    Friend WithEvents optXP As System.Windows.Forms.RadioButton
    Friend WithEvents optCorporate As System.Windows.Forms.RadioButton
    Friend WithEvents optGlass As System.Windows.Forms.RadioButton
    Friend WithEvents stip As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents lsvDetails As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Dim UserHasChanged As Boolean = False

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Destinations")
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("End Date")
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Execution Time")
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Frequency")
        Dim ListViewItem5 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Last Refreshed")
        Dim ListViewItem6 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Last Result")
        Dim ListViewItem7 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Last Run")
        Dim ListViewItem8 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Next Run")
        Dim ListViewItem9 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Output Formats")
        Dim ListViewItem10 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Repeat Interval")
        Dim ListViewItem11 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Repeat Until")
        Dim ListViewItem12 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Report Path")
        Dim ListViewItem13 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Schedule Description")
        Dim ListViewItem14 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem("Start Date")
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmViewOptons))
        Me.TabControl1 = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.lsvDetails = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.btnDown = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.optClassic = New System.Windows.Forms.RadioButton
        Me.optXP = New System.Windows.Forms.RadioButton
        Me.optCorporate = New System.Windows.Forms.RadioButton
        Me.optGlass = New System.Windows.Forms.RadioButton
        Me.TabItem2 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.stip = New DevComponents.DotNetBar.SuperTooltip
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.CanReorderTabs = True
        Me.TabControl1.Controls.Add(Me.TabControlPanel2)
        Me.TabControl1.Controls.Add(Me.TabControlPanel1)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.TabControl1.SelectedTabIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(296, 250)
        Me.TabControl1.Style = DevComponents.DotNetBar.eTabStripStyle.VS2005Dock
        Me.TabControl1.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.TabControl1.TabIndex = 4
        Me.TabControl1.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.TabControl1.Tabs.Add(Me.TabItem1)
        Me.TabControl1.Tabs.Add(Me.TabItem2)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.lsvDetails)
        Me.TabControlPanel1.Controls.Add(Me.btnDown)
        Me.TabControlPanel1.Controls.Add(Me.btnUp)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(29, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(267, 250)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.TabItem1
        '
        'lsvDetails
        '
        Me.lsvDetails.CheckBoxes = True
        Me.lsvDetails.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvDetails.FullRowSelect = True
        Me.lsvDetails.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvDetails.HideSelection = False
        ListViewItem1.StateImageIndex = 0
        ListViewItem1.Tag = "DestinationType"
        ListViewItem2.StateImageIndex = 0
        ListViewItem2.Tag = "EndDate"
        ListViewItem3.StateImageIndex = 0
        ListViewItem3.Tag = "StartTime"
        ListViewItem4.StateImageIndex = 0
        ListViewItem4.Tag = "Frequency"
        ListViewItem5.StateImageIndex = 0
        ListViewItem5.Tag = "LastRefreshed"
        ListViewItem6.StateImageIndex = 0
        ListViewItem6.Tag = "Success"
        ListViewItem7.StateImageIndex = 0
        ListViewItem7.Tag = "EntryDate"
        ListViewItem8.StateImageIndex = 0
        ListViewItem8.Tag = "NextRun"
        ListViewItem9.StateImageIndex = 0
        ListViewItem9.Tag = "OutputFormat"
        ListViewItem10.StateImageIndex = 0
        ListViewItem10.Tag = "RepeatInterval"
        ListViewItem11.StateImageIndex = 0
        ListViewItem11.Tag = "RepeatUntil"
        ListViewItem12.StateImageIndex = 0
        ListViewItem12.Tag = "DatabasePath"
        ListViewItem13.StateImageIndex = 0
        ListViewItem13.Tag = "Description"
        ListViewItem14.StateImageIndex = 0
        ListViewItem14.Tag = "StartDate"
        Me.lsvDetails.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4, ListViewItem5, ListViewItem6, ListViewItem7, ListViewItem8, ListViewItem9, ListViewItem10, ListViewItem11, ListViewItem12, ListViewItem13, ListViewItem14})
        Me.lsvDetails.Location = New System.Drawing.Point(8, 4)
        Me.lsvDetails.Name = "lsvDetails"
        Me.lsvDetails.Size = New System.Drawing.Size(200, 242)
        Me.lsvDetails.TabIndex = 5
        Me.lsvDetails.UseCompatibleStateImageBehavior = False
        Me.lsvDetails.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Details"
        Me.ColumnHeader1.Width = 167
        '
        'btnDown
        '
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Location = New System.Drawing.Point(214, 33)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(41, 23)
        Me.btnDown.TabIndex = 7
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Location = New System.Drawing.Point(214, 4)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(41, 23)
        Me.btnUp.TabIndex = 6
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel1
        Me.TabItem1.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Columns"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.GroupBox2)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(29, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(267, 250)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(233, Byte), Integer), CType(CType(215, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderColor.Color = System.Drawing.Color.FromArgb(CType(CType(172, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(153, Byte), Integer))
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.TabItem2
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.optClassic)
        Me.GroupBox2.Controls.Add(Me.optXP)
        Me.GroupBox2.Controls.Add(Me.optCorporate)
        Me.GroupBox2.Controls.Add(Me.optGlass)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(247, 221)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Application Theme"
        '
        'optClassic
        '
        Me.optClassic.AutoSize = True
        Me.optClassic.Location = New System.Drawing.Point(8, 89)
        Me.optClassic.Name = "optClassic"
        Me.optClassic.Size = New System.Drawing.Size(57, 17)
        Me.stip.SetSuperTooltip(Me.optClassic, New DevComponents.DotNetBar.SuperTooltipInfo("Classic Theme", "", "", CType(resources.GetObject("optClassic.SuperTooltip"), System.Drawing.Image), Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(400, 300)))
        Me.optClassic.TabIndex = 3
        Me.optClassic.TabStop = True
        Me.optClassic.Text = "Classic"
        Me.optClassic.UseVisualStyleBackColor = True
        '
        'optXP
        '
        Me.optXP.AutoSize = True
        Me.optXP.Location = New System.Drawing.Point(8, 66)
        Me.optXP.Name = "optXP"
        Me.optXP.Size = New System.Drawing.Size(37, 17)
        Me.stip.SetSuperTooltip(Me.optXP, New DevComponents.DotNetBar.SuperTooltipInfo("XP Theme", "", "", CType(resources.GetObject("optXP.SuperTooltip"), System.Drawing.Image), Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(400, 300)))
        Me.optXP.TabIndex = 2
        Me.optXP.TabStop = True
        Me.optXP.Text = "XP"
        Me.optXP.UseVisualStyleBackColor = True
        '
        'optCorporate
        '
        Me.optCorporate.AutoSize = True
        Me.optCorporate.Location = New System.Drawing.Point(8, 43)
        Me.optCorporate.Name = "optCorporate"
        Me.optCorporate.Size = New System.Drawing.Size(74, 17)
        Me.stip.SetSuperTooltip(Me.optCorporate, New DevComponents.DotNetBar.SuperTooltipInfo("Corporate Theme", "", "", CType(resources.GetObject("optCorporate.SuperTooltip"), System.Drawing.Image), Nothing, DevComponents.DotNetBar.eTooltipColor.Gray, True, True, New System.Drawing.Size(400, 300)))
        Me.optCorporate.TabIndex = 1
        Me.optCorporate.TabStop = True
        Me.optCorporate.Text = "Corporate"
        Me.optCorporate.UseVisualStyleBackColor = True
        '
        'optGlass
        '
        Me.optGlass.AutoSize = True
        Me.optGlass.Location = New System.Drawing.Point(8, 20)
        Me.optGlass.Name = "optGlass"
        Me.optGlass.Size = New System.Drawing.Size(50, 17)
        Me.stip.SetSuperTooltip(Me.optGlass, New DevComponents.DotNetBar.SuperTooltipInfo("Glass Theme", "", "Glass Theme", CType(resources.GetObject("optGlass.SuperTooltip"), System.Drawing.Image), CType(resources.GetObject("optGlass.SuperTooltip1"), System.Drawing.Image), DevComponents.DotNetBar.eTooltipColor.PurpleMist, True, True, New System.Drawing.Size(500, 300)))
        Me.optGlass.TabIndex = 0
        Me.optGlass.TabStop = True
        Me.optGlass.Text = "Glass"
        Me.optGlass.UseVisualStyleBackColor = True
        '
        'TabItem2
        '
        Me.TabItem2.AttachedControl = Me.TabControlPanel2
        Me.TabItem2.CloseButtonBounds = New System.Drawing.Rectangle(0, 0, 0, 0)
        Me.TabItem2.Image = CType(resources.GetObject("TabItem2.Image"), System.Drawing.Image)
        Me.TabItem2.Name = "TabItem2"
        Me.TabItem2.Text = "Themes"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.Color.Transparent
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(124, 256)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 28)
        Me.cmdOK.TabIndex = 1
        Me.cmdOK.Text = "&Save"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.Color.Transparent
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(209, 256)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 28)
        Me.cmdCancel.TabIndex = 2
        Me.cmdCancel.Text = "&Close"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'stip
        '
        Me.stip.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.stip.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        '
        'frmViewOptons
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(296, 288)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmViewOptons"
        Me.Text = "View Options"
        CType(Me.TabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmViewOptons_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Dim oUI As New clsMarsUI

        Dim theme As String = oUI.ReadRegistry("AppTheme", "Glass")

        Select Case theme
            Case "Glass"
                optGlass.Checked = True
            Case "Corporate"
                optCorporate.Checked = True
            Case "XP"
                optXP.Checked = True
            Case "Classic"
                optClassic.Checked = True
        End Select
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub ChangeTheme(ByVal oUI As clsMarsUI)
        If optGlass.Checked Then
            oUI.SetAppTheme(clsMarsUI.APPTHEMES.Glass)
        ElseIf optXP.Checked Then
            oUI.SetAppTheme(clsMarsUI.APPTHEMES.XP)
        ElseIf optCorporate.Checked Then
            oUI.SetAppTheme(clsMarsUI.APPTHEMES.Corporate)
        ElseIf optClassic.Checked Then
            oUI.SetAppTheme(clsMarsUI.APPTHEMES.Classic)
        End If
    End Sub
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim sCols As String
        Dim sVals As String
        Dim oUI As New clsMarsUI
        Dim I As Integer = 1

        If UserHasChanged = False Then
            ChangeTheme(oUI)
            Close() : Return
        End If


        clsMarsData.WriteData("DELETE FROM UserColumns WHERE Owner ='" & gUser & "'")

        sCols = "ColumnID,Caption,DataColumn,Owner"

        oUI.BusyProgress(10, "Saving Columns...")

        For Each item As ListViewItem In lsvDetails.Items
            oUI.BusyProgress((I / lsvDetails.Items.Count) * 100, "Processing columns...")

            If item.Checked Then
                sVals = clsMarsData.CreateDataID("usercolumns", "columnid") & "," & _
                "'" & item.Text & "'," & _
                "'" & item.Tag & "'," & _
                "'" & gUser & "'"

                If oData.InsertData("UserColumns", sCols, sVals) = False Then
                    oUI.BusyProgress(, , True)
                    Return
                End If
            End If

            I += 1
        Next


        oUI.BusyProgress(, , True)

        ChangeTheme(oUI)

        oUI.RefreshView(oWindow(nWindowCurrent))

        Close()
    End Sub

    Public Sub SetThemes()
        Me.TabControl1.SelectedTab = Me.TabControl1.Tabs(1)
        Me.Text = "Choose application theme"

        Me.ShowDialog()
    End Sub
    Public Sub ViewOptions(Optional ByVal Themes As Boolean = False)
        Dim SQL As String
        Dim oData As New clsMarsData
        Dim oRs As ADODB.Recordset
        Dim I As Integer = 0

        SQL = "SELECT * FROM UserColumns WHERE Owner ='" & gUser & "' ORDER BY ColumnID"

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            For Each item As ListViewItem In lsvDetails.Items
                If item.Text = oRs("caption").Value Then
                    item.Checked = True

                    MoveListViewItemToPos(item, lsvDetails, I)

                    I += 1
                End If
            Next

            oRs.MoveNext()
        Loop

        oRs.Close()

        If Themes = False Then
            Me.Text = "Choose details..."
        Else
            Me.Text = "Application Themes"
            Me.TabControl1.SelectedTab = Me.TabControl1.Tabs(1)
        End If

        Me.ShowDialog()
    End Sub

    Private Sub CheckChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Me.Visible = True Then UserHasChanged = True
    End Sub


    Private Sub lsvDetails_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvDetails.ItemChecked
        If Me.Visible = True Then UserHasChanged = True
    End Sub

    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        MoveListViewItem(lsvDetails, True)
    End Sub


    Private Sub MoveListViewItemToPos(ByVal listItem As ListViewItem, ByVal lsv As ListView, ByVal newIndex As Integer)
        If listItem Is Nothing Then Return

        Dim value As Integer

        Dim itemList As ListViewItem()
        Dim I As Integer = 0

        ReDim itemList(lsv.Items.Count - 1)

        'copy into an array
        For Each item As ListViewItem In lsv.Items
            itemList(I) = item
            I += 1
        Next

        Dim oldIndex As Integer = listItem.Index

        Dim nextItem As ListViewItem = lsv.Items(newIndex)

        itemList(newIndex) = listItem
        itemList(oldIndex) = nextItem

        lsv.Items.Clear()

        For Each item As ListViewItem In itemList
            lsv.Items.Add(item)
        Next
    End Sub


    Private Function MoveListViewItem(ByVal lsv As ListView, ByVal moveUp As Boolean) As Integer
        If lsv.SelectedItems.Count = 0 Then Return 0

        Dim value As Integer

        If moveUp = False Then
            If lsv.SelectedItems(0).Index = lsv.Items.Count - 1 Then Return 0

            value = 1
        Else
            If lsv.SelectedItems(0).Index = 0 Then Return 0

            value = -1
        End If

        Dim itemList As ListViewItem()
        Dim I As Integer = 0
        Dim newIndex As Integer

        ReDim itemList(lsv.Items.Count - 1)

        'copy into an array
        For Each item As ListViewItem In lsv.Items
            itemList(I) = item
            I += 1
        Next

        newIndex = lsv.SelectedItems(0).Index + value

        Dim selItem As ListViewItem = lsv.SelectedItems(0)
        Dim nextItem As ListViewItem = lsv.Items(selItem.Index + value)


        itemList(selItem.Index + value) = selItem
        itemList(selItem.Index) = nextItem

        lsv.Items.Clear()

        For Each item As ListViewItem In itemList
            lsv.Items.Add(item)

            If item.Index = newIndex Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next

        lsv.Refresh()

        Return newIndex
    End Function
    Private Sub btnDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDown.Click
        MoveListViewItem(lsvDetails, False)
    End Sub
End Class
