Imports sqlrd.clsMarsData

Public Class frmExportWizard
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents grpODBC As System.Windows.Forms.GroupBox
    Friend WithEvents grpDat As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDatPath As System.Windows.Forms.TextBox
    Friend WithEvents cmdTempFolder As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents pnProgress As System.Windows.Forms.Panel
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents tvFolders As System.Windows.Forms.TreeView
    Friend WithEvents optDat As System.Windows.Forms.RadioButton
    Friend WithEvents optODBC As System.Windows.Forms.RadioButton
    Friend WithEvents cmbFolders As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents pgb As System.Windows.Forms.ProgressBar
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmExportWizard))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lblStep = New System.Windows.Forms.Label
        Me.Step1 = New System.Windows.Forms.Panel
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.tvFolders = New System.Windows.Forms.TreeView
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.Step2 = New System.Windows.Forms.Panel
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cmbFolders = New System.Windows.Forms.ComboBox
        Me.grpDat = New System.Windows.Forms.GroupBox
        Me.cmdTempFolder = New System.Windows.Forms.Button
        Me.txtDatPath = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.grpODBC = New System.Windows.Forms.GroupBox
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdTest = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.optDat = New System.Windows.Forms.RadioButton
        Me.optODBC = New System.Windows.Forms.RadioButton
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog
        Me.Step3 = New System.Windows.Forms.Panel
        Me.Label2 = New System.Windows.Forms.Label
        Me.pnProgress = New System.Windows.Forms.Panel
        Me.pgb = New System.Windows.Forms.ProgressBar
        Me.Label3 = New System.Windows.Forms.Label
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.PictureBox3 = New System.Windows.Forms.PictureBox
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.grpDat.SuspendLayout()
        Me.grpODBC.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.pnProgress.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(456, 64)
        Me.Panel1.TabIndex = 0
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(392, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 10
        Me.PictureBox1.TabStop = False
        '
        'lblStep
        '
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(384, 32)
        Me.lblStep.TabIndex = 9
        Me.lblStep.Text = "Step 1: Report Setup"
        '
        'Step1
        '
        Me.Step1.Controls.Add(Me.cmdAdd)
        Me.Step1.Controls.Add(Me.lsvSchedules)
        Me.Step1.Controls.Add(Me.tvFolders)
        Me.Step1.Controls.Add(Me.cmdRemove)
        Me.Step1.Location = New System.Drawing.Point(8, 80)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(440, 328)
        Me.Step1.TabIndex = 0
        '
        'cmdAdd
        '
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(216, 120)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(40, 24)
        Me.cmdAdd.TabIndex = 1
        Me.cmdAdd.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'lsvSchedules
        '
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSchedules.FullRowSelect = True
        Me.lsvSchedules.HideSelection = False
        Me.lsvSchedules.LargeImageList = Me.imgFolders
        Me.lsvSchedules.Location = New System.Drawing.Point(272, 8)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(160, 312)
        Me.lsvSchedules.SmallImageList = Me.imgFolders
        Me.lsvSchedules.TabIndex = 2
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 153
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        '
        'tvFolders
        '
        Me.tvFolders.ImageIndex = 0
        Me.tvFolders.ImageList = Me.imgFolders
        Me.tvFolders.Location = New System.Drawing.Point(8, 8)
        Me.tvFolders.Name = "tvFolders"
        Me.tvFolders.SelectedImageIndex = 0
        Me.tvFolders.Size = New System.Drawing.Size(192, 312)
        Me.tvFolders.TabIndex = 0
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(216, 176)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemove.TabIndex = 3
        Me.cmdRemove.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(368, 424)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 32)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(368, 424)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 32)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(288, 424)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 32)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(208, 424)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 32)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'Step2
        '
        Me.Step2.Controls.Add(Me.GroupBox3)
        Me.Step2.Controls.Add(Me.grpDat)
        Me.Step2.Controls.Add(Me.grpODBC)
        Me.Step2.Controls.Add(Me.cmdTest)
        Me.Step2.Controls.Add(Me.GroupBox5)
        Me.Step2.Location = New System.Drawing.Point(8, 80)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(440, 328)
        Me.Step2.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmbFolders)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 256)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(424, 56)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Destination Folder Name"
        Me.GroupBox3.Visible = False
        '
        'cmbFolders
        '
        Me.cmbFolders.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFolders.Location = New System.Drawing.Point(8, 24)
        Me.cmbFolders.Name = "cmbFolders"
        Me.cmbFolders.Size = New System.Drawing.Size(168, 21)
        Me.cmbFolders.TabIndex = 0
        '
        'grpDat
        '
        Me.grpDat.Controls.Add(Me.cmdTempFolder)
        Me.grpDat.Controls.Add(Me.txtDatPath)
        Me.grpDat.Controls.Add(Me.Label1)
        Me.grpDat.Location = New System.Drawing.Point(8, 64)
        Me.grpDat.Name = "grpDat"
        Me.grpDat.Size = New System.Drawing.Size(424, 152)
        Me.grpDat.TabIndex = 1
        Me.grpDat.TabStop = False
        Me.grpDat.Text = "SQL-RD Location"
        Me.grpDat.Visible = False
        '
        'cmdTempFolder
        '
        Me.cmdTempFolder.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTempFolder.Image = CType(resources.GetObject("cmdTempFolder.Image"), System.Drawing.Image)
        Me.cmdTempFolder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdTempFolder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTempFolder.Location = New System.Drawing.Point(368, 40)
        Me.cmdTempFolder.Name = "cmdTempFolder"
        Me.cmdTempFolder.Size = New System.Drawing.Size(48, 21)
        Me.cmdTempFolder.TabIndex = 1
        Me.cmdTempFolder.Text = "..."
        '
        'txtDatPath
        '
        Me.txtDatPath.Location = New System.Drawing.Point(8, 40)
        Me.txtDatPath.Name = "txtDatPath"
        Me.txtDatPath.Size = New System.Drawing.Size(336, 21)
        Me.txtDatPath.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(200, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the SQL-RD Folder"
        '
        'grpODBC
        '
        Me.grpODBC.Controls.Add(Me.UcDSN)
        Me.grpODBC.Location = New System.Drawing.Point(8, 64)
        Me.grpODBC.Name = "grpODBC"
        Me.grpODBC.Size = New System.Drawing.Size(424, 152)
        Me.grpODBC.TabIndex = 2
        Me.grpODBC.TabStop = False
        Me.grpODBC.Text = "SQL-RD DSN"
        Me.grpODBC.Visible = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 24)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(400, 112)
        Me.UcDSN.TabIndex = 0
        '
        'cmdTest
        '
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.Location = New System.Drawing.Point(183, 224)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdTest.TabIndex = 2
        Me.cmdTest.Text = "Connect"
        Me.cmdTest.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.optDat)
        Me.GroupBox5.Controls.Add(Me.optODBC)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(424, 48)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Select the destination system's connection type"
        '
        'optDat
        '
        Me.optDat.Location = New System.Drawing.Point(16, 16)
        Me.optDat.Name = "optDat"
        Me.optDat.Size = New System.Drawing.Size(104, 24)
        Me.optDat.TabIndex = 0
        Me.optDat.Text = "File System"
        '
        'optODBC
        '
        Me.optODBC.Location = New System.Drawing.Point(184, 16)
        Me.optODBC.Name = "optODBC"
        Me.optODBC.Size = New System.Drawing.Size(184, 24)
        Me.optODBC.TabIndex = 1
        Me.optODBC.Text = "ODBC/SQL Server"
        '
        'Step3
        '
        Me.Step3.Controls.Add(Me.Label2)
        Me.Step3.Controls.Add(Me.pnProgress)
        Me.Step3.Location = New System.Drawing.Point(8, 80)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(440, 328)
        Me.Step3.TabIndex = 29
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(424, 32)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "You are now ready to export the selected schedules. Press 'Finish' to initiate th" & _
            "e exporting process..."
        '
        'pnProgress
        '
        Me.pnProgress.Controls.Add(Me.pgb)
        Me.pnProgress.Controls.Add(Me.Label3)
        Me.pnProgress.Controls.Add(Me.PictureBox2)
        Me.pnProgress.Controls.Add(Me.PictureBox3)
        Me.pnProgress.Location = New System.Drawing.Point(8, 136)
        Me.pnProgress.Name = "pnProgress"
        Me.pnProgress.Size = New System.Drawing.Size(424, 88)
        Me.pnProgress.TabIndex = 1
        Me.pnProgress.Visible = False
        '
        'pgb
        '
        Me.pgb.Location = New System.Drawing.Point(64, 40)
        Me.pgb.Name = "pgb"
        Me.pgb.Size = New System.Drawing.Size(288, 16)
        Me.pgb.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(64, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(296, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Exporting schedules, please wait..."
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(8, 16)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(360, 16)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(56, 50)
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(456, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 30
        '
        'DividerLabel2
        '
        Me.DividerLabel2.ForeColor = System.Drawing.SystemColors.ControlDark
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(0, 411)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(456, 22)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 30
        Me.DividerLabel2.Text = "ChristianSteven Software"
        '
        'frmExportWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(456, 465)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.DividerLabel2)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmExportWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Schedule Export Wizard"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.grpDat.ResumeLayout(False)
        Me.grpDat.PerformLayout()
        Me.grpODBC.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.pnProgress.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim oUI As New clsMarsUI
    Const S1 As String = "Step 1: Select schedules"
    Const S2 As String = "Step 2: Configure Destination System"
    Const S3 As String = "Step 3: Export Schedules"
    Dim nStep As Integer = 1
    Dim RemoteCon As String
    Dim oFolderIDs As ComboBox
    Dim nParent As Integer

    Private Sub frmExportWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        lblStep.Text = S1

        oUI.BuildTree(tvFolders, True, False)

        oUI.InitImages(Me.lsvSchedules)
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False

        Step1.BringToFront()
        tvFolders.Focus()


    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If tvFolders.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode
        Dim oItem As ListViewItem
        Dim sType As String

        oNode = tvFolders.SelectedNode

        sType = oNode.Tag

        sType = sType.Split(":")(0).ToLower

        If sType = "folder" Or sType = "desktop" Or sType = "smartfolder" Then Return

        For Each oItem In lsvSchedules.Items
            If oItem.Text = oNode.Text And oItem.Tag = oNode.Tag Then
                Return
            End If
        Next

        oItem = New ListViewItem

        oItem.Text = oNode.Text
        oItem.Tag = oNode.Tag
        oItem.ImageIndex = oNode.ImageIndex

        lsvSchedules.Items.Add(oItem)
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Exit Sub

        For Each oItem As ListViewItem In lsvSchedules.SelectedItems
            oItem.Remove()
        Next
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If lsvSchedules.Items.Count = 0 Then
                    ep.SetError(lsvSchedules, "Please select at least one schedule to export")
                    Return
                End If
                Step1.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                optDat.Focus()
                lblStep.Text = S2

                cmdBack.Enabled = True
                cmdNext.Enabled = False
            Case 2
                If cmbFolders.Text.Length = 0 Then
                    ep.SetError(cmbFolders, "Please select the destination folder")
                    cmbFolders.Focus()
                    Return
                End If
                Step2.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                lblStep.Text = S3

                cmdNext.Visible = False
                cmdFinish.Visible = True
        End Select

        nStep += 1
    End Sub


    Private Sub optDat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDat.CheckedChanged
        If optDat.Checked = True Then
            grpDat.Visible = True
            cmdTest.Visible = True
            grpODBC.Visible = False

        End If
    End Sub

    Private Sub optODBC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optODBC.CheckedChanged
        If optODBC.Checked = True Then
            grpODBC.Visible = True
            cmdTest.Visible = True
            grpDat.Visible = False

        End If
    End Sub

    Private Sub cmdTempFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTempFolder.Click
        Dim oRes As DialogResult


        With ofd
            .Description = "Please select the destination SQL-RD folder (containing the 'sqlrdlive.dat' file)..."
            .ShowNewFolderButton = True
            oRes = .ShowDialog()

            If oRes = Windows.Forms.DialogResult.Cancel Then
                Exit Sub
            Else
                txtDatPath.Text = .SelectedPath

                If txtDatPath.Text.EndsWith("\") = False Then
                    txtDatPath.Text = txtDatPath.Text & "\"
                End If
            End If
        End With
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        If optDat.Checked = True Then
            Dim sPath As String = txtDatPath.Text & "sqlrdlive.dat"

            RemoteCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
        Else
            With UcDSN
                RemoteCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With
        End If

        Dim TestCon As ADODB.Connection = New ADODB.Connection

        Try
            If optDat.Checked = True Then
                TestCon.Open(RemoteCon)
            Else
                With UcDSN
                    TestCon.Open(.cmbDSN.Text, .txtUserID.Text, .txtPassword.Text)
                End With
            End If

            Dim oRs As ADODB.Recordset = New ADODB.Recordset
            Dim fieldOne As String
            Dim OrderBy As String
            Dim SQL As String = "FolderID FROM Folders "

            If optDat.Checked = False Then
                fieldOne = "CONVERT(VARCHAR(255),FolderName)"
                OrderBy = " ORDER BY CONVERT(VARCHAR(255),FolderName)"
            Else
                fieldOne = "FolderName"
                OrderBy = " ORDER BY FolderName"
            End If

            SQL = "SELECT " & fieldOne & "," & SQL & OrderBy

            oRs.Open(clsMarsData.prepforbinaryCollation(SQL), TestCon, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

            oFolderIDs = New ComboBox

            cmbFolders.Items.Clear()

            Do While oRs.EOF = False
                Dim obj As ComboBox.ObjectCollection = cmbFolders.Items
                Dim inList As Boolean

                inList = False

                For I As Integer = 0 To obj.Count - 1
                    If obj(I) = oRs(0).Value Then
                        inList = True
                        Exit For
                    End If
                Next

                If inList = True Then GoTo SKIP

                cmbFolders.Items.Add(oRs(0).Value)
                oFolderIDs.Items.Add(oRs(1).Value)
SKIP:
                oRs.MoveNext()
            Loop

            oRs.Close()

            TestCon.Close()

            cmdNext.Enabled = True
            GroupBox3.Visible = True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            cmdNext.Enabled = False

        End Try
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 3
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                lblStep.Text = S2
                cmdNext.Enabled = False
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                lblStep.Text = S1
                cmdBack.Enabled = False
                cmdNext.Enabled = True
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click

        pnProgress.Visible = True

        cmdFinish.Enabled = False
        cmdCancel.Enabled = False
        cmdBack.Enabled = False

        Application.DoEvents()

        Dim oItem As ListViewItem
        Dim sType As String
        Dim nID As Integer
        Dim oData As New clsMarsData
        Dim oTables As New ComboBox
        Dim oColumns As New ComboBox
        Dim KeyColumn As String
        Dim ExportTables() As String
        Dim I, x As Integer
        Dim HasFailed As Boolean
        Dim Failures() As String
        Dim errCount As Integer = 0
        Dim exceptList As ArrayList

        x = 0

        If optDat.Checked = True Then
            oData.UpgradeCRDDb(RemoteCon)
            oData.GetTables(oTables, RemoteCon)
        Else
            oData.UpgradeCRDDb(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            oData.GetTables(oTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        Dim oItems As ComboBox.ObjectCollection = oTables.Items

        For Each oItem In lsvSchedules.Items
            x = 0
            sType = oItem.Tag.Split(":")(0)
            nID = oItem.Tag.Split(":")(1)

            If sType.ToLower = "package" Then
                Dim SQL As String = "SELECT ReportID FROM ReportAttr WHERE PackID =" & nID
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        Me.prepareScheduleExport("report", oRs(0).Value, oItem, oItems, HasFailed, errCount)
                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If
            ElseIf sType.ToLower = "report" Then
                Dim SQL As String = "SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & nID
                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        Me.prepareScheduleExport("destination", oRs(0).Value, oItem, oItems, HasFailed, errCount)

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If

                exceptList = New ArrayList

                exceptList.Add("destinationattr")
            End If

            prepareScheduleExport(sType, nID, oItem, oItems, HasFailed, errCount, exceptList)
        Next

        If HasFailed = False Then
            Dim sFinito As String

            sFinito = "The schedule has been exported successfully."

            MessageBox.Show(sFinito, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Close()

            Return
        Else
            cmdFinish.Enabled = True
            cmdCancel.Enabled = True
            cmdBack.Enabled = True
            pnProgress.Visible = False
            pgb.Value = 0

            Dim sMsg As String

            sMsg = "Schedule export completed. Failed to export the following schedules: " & vbCrLf

            For Each s As String In Failures
                sMsg &= s & vbCrLf
            Next

            MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub prepareScheduleExport(ByVal stype As String, ByVal nID As Integer, ByVal oItem As ListViewItem, _
    ByVal oItems As ComboBox.ObjectCollection, ByRef HasFailed As Boolean, ByRef errCount As Integer, Optional ByVal exceptList As ArrayList = Nothing)
        Dim keyColumn As String
        Dim I, x As Integer
        Dim oData As New clsMarsData
        Dim oColumns As New ComboBox
        Dim ExportTables() As String
        Dim failures() As String
        Dim scheduleID As Integer = 0

        Select Case stype.ToLower
            Case "report"
                keyColumn = "reportid"
                scheduleID = clsMarsScheduler.GetScheduleID(nID)
            Case "package"
                keyColumn = "packid"
                scheduleID = clsMarsScheduler.GetScheduleID(, nID)
            Case "automation"
                keyColumn = "autoid"
                scheduleID = clsMarsScheduler.GetScheduleID(, , nID)
            Case "event"
                keyColumn = "eventid"
                scheduleID = nID
            Case "destination"
                keyColumn = "destinationid"
        End Select

        For I = 0 To oItems.Count - 1

            If optDat.Checked = True Then
                oData.GetColumns(oColumns, RemoteCon, oItems(I))
            Else
                oData.GetColumns(oColumns, UcDSN.cmbDSN.Text, oItems(I), UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If

            Dim oItems2 As ComboBox.ObjectCollection = oColumns.Items

            For n As Integer = 0 To oItems2.Count - 1
                Dim sTemp As String = Convert.ToString(oItems2(n))

                If sTemp.ToLower = keyColumn Then
                    ReDim Preserve ExportTables(x)
                    ExportTables(x) = oItems(I)
                    x += 1
                    Exit For
                End If
            Next
        Next

        Application.DoEvents()
        '_Delay(1)

        For n As Integer = 0 To ExportTables.GetUpperBound(0)

            If exceptList IsNot Nothing Then
                If exceptList.Contains(ExportTables(n).ToLower) Then Continue For
            End If


            Label3.Text = "Exporting schedule " & oItem.Text & ": " & ExportTables(n)

            pgb.Value = (n / ExportTables.GetUpperBound(0)) * 100
            Application.DoEvents()


            If _ExportRecord(nID, ExportTables(n), keyColumn) = False Then

                _DeleteRecords(ExportTables, nID, keyColumn)

                HasFailed = True
                ReDim Preserve failures(errCount)

                failures(errCount) = oItem.Text
                Exit For
            End If

        Next

        'export tasks
        If errCount = 0 Then
            Me._ExportRecord(scheduleID, "Tasks", "ScheduleID")
        End If

    End Sub
    Private Sub _DeleteRecords(ByVal sTables() As String, ByVal nID As Integer, ByVal KeyColumn As String)
        On Error Resume Next

        Dim o As ADODB.Connection = New ADODB.Connection

        If optDat.Checked = True Then
            o.Open(RemoteCon)
        Else
            o.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        For Each s As String In sTables
            o.Execute("DELETE FROM " & s & " WHERE " & KeyColumn & " = " & nID)
        Next

    End Sub
    Private Function _ExportRecord(ByVal nID As Integer, ByVal sTable As String, _
    ByVal KeyColumn As String) As Boolean
        Dim oRs As New ADODB.Recordset
        Dim oRemote As ADODB.Connection = New ADODB.Connection
        Dim SQL As String
        Dim sCols As String
        Dim sVals As String

        Try
            If optDat.Checked = True Then
                oRemote.Open(RemoteCon)
            Else
                oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
            End If


            SQL = "SELECT * FROM " & sTable & " WHERE " & KeyColumn & " = " & nID

            oRs = GetData(SQL)

            Dim oField As ADODB.Field

            If Not oRs Is Nothing Then

                Do While oRs.EOF = False

                    sCols = ""
                    sVals = ""

                    For Each oField In oRs.Fields
                        sCols = sCols & "[" & oField.Name & "],"

                        Select Case oField.Type
                            Case ADODB.DataTypeEnum.adInteger
                                If oField.Name.ToLower = "parent" Then
                                    sVals = sVals & nParent & ","
                                Else
                                    sVals = sVals & IsNull(oField.Value, 0) & ","
                                End If
                            Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBTimeStamp
                                sVals = sVals & "'" & ConDateTime(IsNull(oField.Value, Now)) & "',"
                            Case Else
                                If sTable.ToLower = "scheduleattr" And oField.Name.ToLower = "repeatinterval" Then
                                    sVals = sVals & IsNull(oField.Value, "0").ToString.Replace(",", ".") & ","
                                Else
                                    sVals = sVals & "'" & SQLPrepare(IsNull(oField.Value, "")) & "',"
                                End If
                        End Select

                    Next

                    sCols = sCols.Substring(0, sCols.Length - 1)
                    sVals = sVals.Substring(0, sVals.Length - 1)

                    SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & sVals & ")"

                    SQL = clsMarsData.prepforbinaryCollation(SQL)

                    Try
                        oRemote.Execute(SQL)
                    Catch ex1 As Exception
                        If ex1.Message.ToLower.Contains("primary key") = False Then
                            Throw ex1
                        Else
                            If sTable.ToLower <> "reportattr" And sTable.ToLower <> "packageattr" Then
                                Dim newCode As Integer = clsMarsData.CreateDataID
                                Dim newVals As String = ""
                                Dim tempArr As String() = sVals.Split(",")

                                tempArr(0) = newCode

                                For Each s As String In tempArr
                                    newVals &= s & ","
                                Next

                                newVals = newVals.Remove(newVals.Length - 1, 1)

                                SQL = "INSERT INTO " & sTable & "(" & sCols & ") VALUES (" & newVals & ")"

                                SQL = clsMarsData.prepforbinaryCollation(SQL)

                                Try
                                    oRemote.Execute(SQL)
                                Catch : End Try
                            End If
                        End If
                    End Try

                    oRs.MoveNext()
                Loop

                oRs.Close()

                oRemote.Close()
            End If

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function

    Private Sub cmbFolders_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFolders.SelectedIndexChanged
        Dim oRemote As New ADODB.Connection
        Dim oRs As ADODB.Recordset = New ADODB.Recordset

        If optDat.Checked = True Then
            oRemote.Open(RemoteCon)
        Else
            oRemote.Open(UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        End If

        oRs.Open("SELECT FolderID FROM Folders WHERE FolderName LIKE '" & SQLPrepare(cmbFolders.Text) & "'", _
        oRemote, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)

        If oRs.EOF = False Then
            nParent = oRs(0).Value
        Else
            nParent = 0
        End If

        oRs.Close()
        oRemote.Close()
    End Sub
End Class
