Public Class frmUserSchedules
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tvAll As System.Windows.Forms.TreeView
    Friend WithEvents lsvSelected As System.Windows.Forms.ListView
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUserSchedules))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lsvSelected = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tvAll = New System.Windows.Forms.TreeView
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lsvSelected)
        Me.GroupBox1.Location = New System.Drawing.Point(320, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(224, 376)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'lsvSelected
        '
        Me.lsvSelected.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvSelected.Location = New System.Drawing.Point(8, 16)
        Me.lsvSelected.Name = "lsvSelected"
        Me.lsvSelected.Size = New System.Drawing.Size(208, 352)
        Me.lsvSelected.SmallImageList = Me.imgFolders
        Me.lsvSelected.TabIndex = 0
        Me.lsvSelected.UseCompatibleStateImageBehavior = False
        Me.lsvSelected.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Schedule Name"
        Me.ColumnHeader1.Width = 201
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "document_gear.png")
        Me.imgFolders.Images.SetKeyName(8, "document_atoms.png")
        Me.imgFolders.Images.SetKeyName(9, "cube_molecule.png")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tvAll)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(224, 376)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'tvAll
        '
        Me.tvAll.ImageIndex = 0
        Me.tvAll.ImageList = Me.imgFolders
        Me.tvAll.Indent = 19
        Me.tvAll.ItemHeight = 16
        Me.tvAll.Location = New System.Drawing.Point(8, 16)
        Me.tvAll.Name = "tvAll"
        Me.tvAll.SelectedImageIndex = 0
        Me.tvAll.Size = New System.Drawing.Size(208, 352)
        Me.tvAll.TabIndex = 0
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(240, 168)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(72, 32)
        Me.cmdNext.TabIndex = 4
        Me.cmdNext.Text = "&Add"
        Me.cmdNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdBack
        '
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(240, 216)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(72, 32)
        Me.cmdBack.TabIndex = 5
        Me.cmdBack.Text = "&Remove"
        Me.cmdBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.GroupBox1)
        Me.GroupBox3.Controls.Add(Me.GroupBox2)
        Me.GroupBox3.Controls.Add(Me.cmdBack)
        Me.GroupBox3.Controls.Add(Me.cmdNext)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(552, 392)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(400, 400)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 17
        Me.cmdOK.Text = "&OK"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(488, 400)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 18
        Me.cmdCancel.Text = "&Cancel"
        '
        'frmUserSchedules
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(570, 432)
        Me.ControlBox = False
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.GroupBox3)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmUserSchedules"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Schedules"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmUserSchedules_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        tvAll.SelectedNode = tvAll.Nodes(0)

        tvAll.Nodes(0).Expand()
    End Sub

    Public Sub AssignSchedules(ByVal sUserID As String)
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oUI As New clsMarsUI
        Dim nID As Integer
        Dim oRs1 As ADODB.Recordset

        oUI.BuildTree(tvAll, True, False)


        SQL = "SELECT * FROM UserView WHERE UserID = '" & sUserID & "'"

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                If IsNull(oRs("reportid").Value, "0") <> "0" Then
                    nID = oRs("reportid").Value

                    oRs1 = clsmarsdata.GetData("SELECT ReportTitle FROM ReportAttr WHERE ReportID = " & nID)

                    If Not oRs1 Is Nothing Then
                        If oRs1.EOF = False Then
                            Dim olsv As New ListViewItem

                            olsv.Text = oRs1(0).Value
                            olsv.Tag = "Report:" & nID
                            olsv.ImageIndex = 6
                            lsvSelected.Items.Add(olsv)
                        End If
                        oRs1.Close()
                    End If
                ElseIf IsNull(oRs("packid").Value, "0") <> "0" Then
                    nID = oRs("packid").Value

                    oRs1 = clsmarsdata.GetData("SELECT PackageName FROM PackageAttr WHERE PackID = " & nID)

                    If Not oRs1 Is Nothing Then
                        If oRs1.EOF = False Then
                            Dim olsv As New ListViewItem

                            olsv.Text = oRs1(0).Value
                            olsv.Tag = "Package:" & nID
                            olsv.ImageIndex = 3
                            lsvSelected.Items.Add(olsv)
                        End If
                        oRs1.Close()
                    End If
                ElseIf IsNull(oRs("autoid").Value, "0") <> "0" Then
                    nID = oRs("autoid").Value

                    oRs1 = clsmarsdata.GetData("SELECT AutoName FROM AutomationAttr WHERE AutoID = " & nID)

                    If Not oRs1 Is Nothing Then
                        If oRs1.EOF = False Then
                            Dim olsv As New ListViewItem

                            olsv.Text = oRs1(0).Value
                            olsv.Tag = "Automation:" & nID
                            olsv.ImageIndex = 7
                            lsvSelected.Items.Add(olsv)
                        End If
                        oRs1.Close()
                    End If
                ElseIf IsNull(oRs("eventid").Value, "0") <> "0" Then
                    nID = oRs("eventid").Value

                    oRs1 = clsMarsData.GetData("SELECT EventName FROM EventAttr6 WHERE EventID = " & nID)

                    If Not oRs1 Is Nothing Then
                        If oRs1.EOF = False Then
                            Dim olsv As New ListViewItem

                            olsv.Text = oRs1(0).Value
                            olsv.Tag = "Event:" & nID
                            olsv.ImageIndex = 8
                            lsvSelected.Items.Add(olsv)
                        End If
                        oRs1.Close()
                    End If
                ElseIf IsNull(oRs("eventpackid").Value, "0") <> "0" Then
                    nID = oRs("eventpackid").Value

                    oRs1 = clsMarsData.GetData("SELECT PackageName FROM EventPackageAttr WHERE EventPackID = " & nID)

                    If Not oRs1 Is Nothing Then
                        If oRs1.EOF = False Then
                            Dim olsv As New ListViewItem

                            olsv.Text = oRs1(0).Value
                            olsv.Tag = "Event-Package:" & nID
                            olsv.ImageIndex = 9
                            lsvSelected.Items.Add(olsv)
                        End If
                        oRs1.Close()
                    End If
                End If

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        For Each o As TreeNode In tvAll.Nodes
            o.Expand()
        Next

        Me.ShowDialog()

        If UserCancel = True Then Return

        Dim oUser As New clsMarsUsers

        SQL = "DELETE FROM UserView WHERE UserID = '" & sUserID & "'"

        clsMarsData.WriteData(SQL)

        For Each oItem As ListViewItem In lsvSelected.Items
            Select Case oItem.Tag.Split(":")(0)
                Case "Report"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewSingle)
                Case "Automation"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewAutomation)
                Case "Package"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewPackage)
                Case "Folder"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewFolder)
                Case "SmartFolder"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewSmartFolder)
                Case "Event"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewEvent)
                Case "Event-Package"
                    oUser.AssignView(oItem.Tag.Split(":")(1), sUserID, clsMarsUsers.enViewType.ViewEventPackage)
            End Select
        Next
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        If tvAll.SelectedNode Is Nothing Then Return

        If tvAll.SelectedNode.Tag = "Desktop" Then Return

        Dim oNode As TreeNode = tvAll.SelectedNode

        Select Case oNode.Tag.split(":")(0).ToLower
            Case "report"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Report" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 6
                End With

                lsvSelected.Items.Add(olsv)
            Case "automation"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Automation" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 7
                End With

                lsvSelected.Items.Add(olsv)
            Case "package"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Package" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 3
                End With

                lsvSelected.Items.Add(olsv)
            Case "folder"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Folder" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 1
                End With

                lsvSelected.Items.Add(olsv)
            Case "smartfolder"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "SmartFolder" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 5
                End With

                lsvSelected.Items.Add(olsv)
            Case "event"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Event" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 8
                End With

                lsvSelected.Items.Add(olsv)
            Case "event-package"
                For Each oItem As ListViewItem In lsvSelected.Items
                    If oItem.Text = oNode.Text And oItem.Tag.split(":")(0) = _
                    "Event-Package" Then
                        Return
                    End If
                Next

                Dim olsv As New ListViewItem

                With olsv
                    .Text = oNode.Text
                    .Tag = oNode.Tag
                    .ImageIndex = 9
                End With

                lsvSelected.Items.Add(olsv)
        End Select
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        If lsvSelected.SelectedItems.Count = 0 Then Return

        lsvSelected.SelectedItems(0).Remove()

    End Sub

    Private Sub tvAll_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvAll.AfterSelect
        Dim sType As String

        If tvAll.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode = tvAll.SelectedNode

        sType = oNode.Tag.split(":")(0).ToLower

        Select Case sType.ToLower
            Case "folder", "smartfolder", "desktop"
                cmdNext.Enabled = False
            Case Else
                cmdNext.Enabled = True
        End Select
    End Sub
End Class
