Imports DevComponents.DotNetBar
Public Class frmRemoteAdmin
    Inherits sqlrd.frmTaskMaster
    Dim oData As New clsMarsData
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Dim oUI As New clsMarsUI


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents lsvRemotes As System.Windows.Forms.ListView
    Friend WithEvents cmdAdd As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdConnect As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuRemote As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRemoteAdmin))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.lsvRemotes = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.mnuRemote = New System.Windows.Forms.ContextMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdConnect = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(450, 59)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(280, 22)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Remote Administration"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(392, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 47)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'lsvRemotes
        '
        Me.lsvRemotes.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3})
        Me.lsvRemotes.ContextMenu = Me.mnuRemote
        Me.lsvRemotes.FullRowSelect = True
        Me.lsvRemotes.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvRemotes.LargeImageList = Me.ImageList1
        Me.lsvRemotes.Location = New System.Drawing.Point(8, 74)
        Me.lsvRemotes.Name = "lsvRemotes"
        Me.lsvRemotes.Size = New System.Drawing.Size(432, 268)
        Me.lsvRemotes.SmallImageList = Me.ImageList1
        Me.lsvRemotes.TabIndex = 1
        Me.lsvRemotes.UseCompatibleStateImageBehavior = False
        Me.lsvRemotes.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "PC Name"
        Me.ColumnHeader1.Width = 106
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "System Path"
        Me.ColumnHeader2.Width = 200
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Auto Connect"
        Me.ColumnHeader3.Width = 97
        '
        'mnuRemote
        '
        Me.mnuRemote.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.Text = "Auto Connect on Startup"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "")
        '
        'cmdAdd
        '
        Me.cmdAdd.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAdd.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAdd.Location = New System.Drawing.Point(288, 357)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(75, 22)
        Me.cmdAdd.TabIndex = 8
        Me.cmdAdd.Text = "&Add"
        Me.cmdAdd.UseVisualStyleBackColor = False
        '
        'cmdDelete
        '
        Me.cmdDelete.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(368, 357)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(75, 22)
        Me.cmdDelete.TabIndex = 7
        Me.cmdDelete.Text = "&Remove"
        Me.cmdDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdDelete.UseVisualStyleBackColor = False
        '
        'cmdConnect
        '
        Me.cmdConnect.BackColor = System.Drawing.SystemColors.Control
        Me.cmdConnect.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdConnect.Image = CType(resources.GetObject("cmdConnect.Image"), System.Drawing.Image)
        Me.cmdConnect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdConnect.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdConnect.Location = New System.Drawing.Point(8, 357)
        Me.cmdConnect.Name = "cmdConnect"
        Me.cmdConnect.Size = New System.Drawing.Size(75, 22)
        Me.cmdConnect.TabIndex = 7
        Me.cmdConnect.Text = "&Connect"
        Me.cmdConnect.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdConnect.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Location = New System.Drawing.Point(-8, 342)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(472, 7)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(472, 7)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Tag = "3dline"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 59)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(450, 12)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 10
        '
        'frmRemoteAdmin
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(450, 386)
        Me.ControlBox = True
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdAdd)
        Me.Controls.Add(Me.cmdDelete)
        Me.Controls.Add(Me.lsvRemotes)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdConnect)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRemoteAdmin"
        Me.Text = "Select Remote System"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmRemoteAdmin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        LoadAll()
    End Sub

    Private Sub LoadAll()
        Dim oRs As ADODB.Recordset
        Dim SQL As String
        Dim oItem As ListViewItem

        SQL = "SELECT * FROM RemoteSysAttr"

        oRs = clsMarsData.GetData(SQL)

        lsvRemotes.Items.Clear()

        Try
            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.ImageIndex = 0

                oItem.Text = oRs("pcname").Value
                oItem.SubItems.Add(oRs("livepath").Value)

                If IsNull(oRs("autoconnect").Value) = "1" Then
                    oItem.SubItems.Add("1")
                Else
                    oItem.SubItems.Add("0")
                End If

                oItem.Tag = oRs("remoteid").Value

                lsvRemotes.Items.Add(oItem)

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch : End Try
    End Sub

    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        Dim nID As Integer

        If lsvRemotes.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected remote system?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            nID = lsvRemotes.SelectedItems(0).Tag

            Dim SQL As String = "DELETE FROM RemoteSysAttr WHERE RemoteID =" & nID

            clsMarsData.WriteData(SQL)

            lsvRemotes.SelectedItems(0).Remove()

        End If

    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        Dim fbd As New FolderBrowserDialog

        fbd.Description = "Please select the SQL-RD folder on the remote system"

        fbd.ShowNewFolderButton = False
        fbd.ShowDialog()

        Dim Temp As String
        Dim SQL As String
        Dim rs As New ADODB.Recordset
        Dim I As Integer
        Dim NewItem As Integer
        Dim bExists As Boolean
        Dim sRemote As String

        Temp = fbd.SelectedPath

        If Temp.Length = 0 Then Exit Sub

        If Temp.StartsWith("\\") = False Then Temp = _CreateUNC(Temp)

        If Temp.StartsWith("\\") = False Then
            MessageBox.Show("Remote Administration requires the full UNC path ofthe remote system " & vbCrLf & _
            "e.g \\MyServer\C\Program Files\ChristianSteven\SQL-RD", Application.ProductName, MessageBoxButtons.OK, _
            MessageBoxIcon.Exclamation)
            Return
        End If

        sRemote = ConnectRemote(Temp)

        If sRemote.Length = 0 Then Return


        Dim sCols As String
        Dim sVals As String
        Dim nID As Integer = clsMarsData.CreateDataID("remotesysattr", "remoteid")

        sCols = "RemoteID,PCName,LivePath"

        sVals = nID & "," & _
        "'" & SQLPrepare(sRemote) & "'," & _
        "'" & SQLPrepare(Temp) & "'"

        SQL = "INSERT INTO RemoteSysAttr (" & sCols & ") VALUES (" & sVals & ")"

        If clsMarsData.WriteData(SQL) = True Then
            Dim oItem As New ListViewItem

            oItem.Text = sRemote

            oItem.SubItems.Add(Temp)

            oItem.Tag = nID

            lsvRemotes.Items.Add(oItem)
        End If

    End Sub
    Public Function ConnectRemote(ByVal sRemotePath As String, Optional ByVal checkNTUser As Boolean = False) As String
        Dim rs As New ADODB.Recordset
        Dim SQL As String
        Dim Temp As String
        Dim MachineName As String
        Dim cn As New ADODB.Connection
        Dim sHelp As String
        Dim sRemoteDSN As String
        Dim oRemoteReg As New RemoteRegistry.clsRegistry


        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.sa3_RemoteAdmin) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
            Exit Function
        End If

        Try
            MachineName = GetDelimitedWord(sRemotePath, 3, "\")

            sRemoteDSN = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sRemotePath & "\sqlrdlive.dat;Persist Security Info=False"

            oUI.BusyProgress(25, "Connecting to remote system...")

            cn.Mode = ADODB.ConnectModeEnum.adModeShareDenyNone

            cn.Open(sRemoteDSN)

            oUI.BusyProgress(75, "Validating system catalog...")

            SQL = "SELECT TOP 1 * FROM ReportAttr"

            rs.Open(SQL, cn, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            rs.Close()

            rs = Nothing


            oUI.BusyProgress(100, "", True)

            rs = Nothing
            cn.Close()
            cn = Nothing

            Return MachineName
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            oUI.BusyProgress(, , True)
            Return String.Empty
        End Try
    End Function

    Private Sub cmdConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdConnect.Click
        If lsvRemotes.SelectedItems.Count = 0 Then Return

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer = lsvRemotes.SelectedItems(0).Tag
        Dim PCName As String
        Dim sPath As String
        Dim sUser As String
        Dim sPassword As String

        SQL = "SELECT * FROM RemoteSysAttr WHERE RemoteID =" & nID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                PCName = oRs("pcname").Value
                sPath = oRs("livepath").Value
                sUser = IsNull(oRs("username").Value)
                sPassword = _DecryptDBValue(IsNull(oRs("userpassword").Value))
            End If

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Exit Sub
        End Try

        Dim oLogin As New frmRemoteLogin
        Dim unified As Boolean = clsMarsUI.MainUI.ReadRegistry("UnifiedLogin", 0, , sPath & "\sqlrdlive.config", True)


        If oLogin.CheckLogin(sPath & "\sqlrdlive.dat", nID, sUser, sPassword, unified) = False Then
            MessageBox.Show("Login to remote system failed. Please check the provided username and password.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        If ConnectRemote(sPath).Length = 0 Then
            Exit Sub
        End If

        'close all windows
        For Each oForm As frmWindow In oWindow
            oForm.Close()
            oForm.Dispose()
        Next

        nWindowCount = 0

        'close data connect
        oData.CloseMainDataConnection()

        'reset values 
        gPCName = PCName
        gConfigFile = sPath & "\sqlrdlive.config"

        sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & "\sqlrdlive.dat;Persist Security Info=False"

        oData.OpenMainDataConnection()

        oData.UpgradeCRDDb()

        clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

        sAppPath = sPath & "\"

        Try
            oWindow = Nothing

            'nWindowCount += 1
            ReDim oWindow(nWindowCount)
            oWindow(nWindowCount) = New frmWindow
            oWindow(nWindowCount).MdiParent = oMain
            oWindow(nWindowCount).Show()
            oWindow(nWindowCount).Tag = nWindowCount
            oWindow(nWindowCount).Text = "Desktop"
            nWindowCurrent = nWindowCount


            oMain.Text = "SQL-RD " & gsEdition & " Edition - on Machine:\\" & PCName
        Catch ex As Exception
            ''console.writeLine(ex.ToString)
        End Try

        Try
            Dim oBtnConnect As ButtonItem
            Dim oBtnDisconnect As ButtonItem

            oBtnConnect = oMain.DotNetBarManager1.GetItem("item_169")
            oBtnDisconnect = oMain.DotNetBarManager1.GetItem("item_201")

            oBtnConnect.Enabled = False
            oBtnDisconnect.Enabled = True

            Dim oForm As frmWindow = oWindow(nWindowCurrent)

            oBtnConnect = oForm.DotNetBarManager1.GetItem("btnConnect")
            oBtnDisconnect = oForm.DotNetBarManager1.GetItem("btnDisconnect")

            oBtnConnect.Enabled = False
            oBtnDisconnect.Enabled = True

        Catch : End Try

        Me.Close()

    End Sub

    Public Sub Disconnect()
        Dim sPath As String

        gPCName = String.Empty

        'close all windows
        For Each oForm As frmWindow In oWindow
            oForm.Close()
        Next

        nWindowCount = 0

        'save all the data to the remote machine
        clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)

        'close data connect
        oData.CloseMainDataConnection()

        'reset values 
        sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")

        sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

        sAppPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "")

        If sAppPath.Substring(sAppPath.Length - 1, 1) <> "\" Then _
            sAppPath += "\"

        gConfigFile = sAppPath & "sqlrdlive.config"

        sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

        clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

        oData.OpenMainDataConnection()

        Try
            'nWindowCount += 1
            ReDim oWindow(nWindowCount)
            oWindow(nWindowCount) = New frmWindow
            oWindow(nWindowCount).MdiParent = oMain
            oWindow(nWindowCount).Show()
            oWindow(nWindowCount).Tag = nWindowCount
            oWindow(nWindowCount).Text = "Desktop"
            nWindowCurrent = nWindowCount

            oMain.Text = "SQL-RD " & gsEdition & " Edition"
        Catch : End Try

        Try
            Dim oBtnConnect As ButtonItem
            Dim oBtnDisconnect As ButtonItem

            oBtnConnect = oMain.DotNetBarManager1.GetItem("item_169")
            oBtnDisconnect = oMain.DotNetBarManager1.GetItem("item_201")

            oBtnConnect.Enabled = True
            oBtnDisconnect.Enabled = False

        Catch : End Try

    End Sub

    Private Sub MenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem1.Click
        Dim nID As Integer
        Dim nValue As Int16
        Dim SQL As String
        Dim SQL2 As String = ""

        If lsvRemotes.SelectedItems.Count = 0 Then Return

        nValue = lsvRemotes.SelectedItems(0).SubItems(2).Text
        nID = lsvRemotes.SelectedItems(0).Tag

        If nValue = 1 Then
            SQL = "UPDATE RemoteSysAttr SET AutoConnect =0 WHERE RemoteID =" & nID
        Else
            SQL = "UPDATE RemoteSysAttr SET AutoConnect =1 WHERE RemoteID =" & nID
            SQL2 = "UPDATE RemoteSysAttr SET AutoConnect =0  WHERE RemoteID <>" & nID
        End If

        clsMarsData.WriteData(SQL)

        If SQL2.Length > 0 Then clsMarsData.WriteData(SQL2)

        LoadAll()
    End Sub
End Class
