Imports Microsoft.Win32
Imports System.ServiceProcess
Imports System.Net
Imports System.Net.Sockets
Imports ChristianSteven.Licenser.clsLicenser
Imports System.IO
Imports System.Text
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization
Imports ActiveDs


Module MarsGlobal
#Region "Public Constasts"
    Public Const assemblyName As String = "sqlrd.exe"
    Public Const sDSN As String = ""
    Public oApp As Object 'CRAXDRT.Application
    Public greportItem As sqlrd.ReportServer.ReportingService
    Public oMain As frmMain
    Public oWindow() As frmWindow
    Public nWindowCount As Int32 = 0
    Public nWindowCurrent As Int32 = 0
    Public sCon As String
    Public rptFileNames() As String
    Public SMTPErr As String
    Public MailType As gMailType = gMailType.NONE
    Public CurVersion As String
    Public CustID As Integer
    Public sAppPath As String
    Public gErrorDesc As String = ""
    Public gErrorNumber As Long
    Public gErrorLine As Int32
    Public gErrorSource As String = ""
    Public gErrorSuggest As String = ""
    Public gClipboard(1) As String
    Public gVisible As Boolean
    Public ServiceType As String
    Public gsEdition As String
    Public gnEdition As gEdition
    Public nMax As Integer
    Public nTimeLeft As String
    Public gsID(1) As String
    Public gScheduleName As String = ""
    Public gExportedFileName As String = ""
    Public gDoReportParsing As Boolean = False
    'these two are just for dynamic schedule - saves me from passing around too 
    'much
    Public gTables(1) As String
    Public gsCon As String
    '---------------------
    Public sKey As String
    Public gThread As System.Threading.Thread
    Public gUser As String
    Public gRole As String
    Public gCon As ADODB.Connection
    Public gPath As String = ""
    Public gPCName As String = ""
    Public gConType As String = "DAT"
    Public gKeyValue As String = ""
    Public gGroupValue As String
    Public ScheduleStart As Date
    Public gErrorCollection As String = ""
    Public gSystemInfo As Hashtable
    '_________________________________________
    'for parameters in command lines
    Public gParNames() As String
    Public gParValues() As Object
    Public gParent As String = ""
    Public gParentID As Integer = 0
    Public RunEditor As Boolean = True
    'for parameter substitution - it gets ugly
    '----------------------------------------
    Public gRunByEvent As Boolean = False
    Public gEventID As Integer = 0
    Public gSubject As String = ""
    Public gBody As String = ""
    Public gFrom As String = ""
    Public gTo As String = ""
    Public gCc As String = ""
    Public gBcc As String = ""
    Public gImages As ImageList
    '--------------------------------------------
    Public gConfigFile As String
    Public gHelpPath As String
    Public gTimeOffSet As Decimal = 9999
    Public gServiceType As String = ""
    Public ReadOnly PD As Char = Chr(166)
    Public Const QuikSoftLicense As String = "ChristianSteven Software (Single Developer)/13046371F574273253750FB337#3612A2D"
    Public Const QuikSoftSSLLicense As String = "ChristianSteven Software (Single Developer)/13047341F666623200000F00262C5AAE73"
    Public Const gDownloadUrl As String = "http://download.christiansteven.com/sql-rd/updatecheck.txt"
    Public gSysmonInstance As frmSystemMonitor
#End Region
#Region "Global Enums"
    Public Enum gMailType
        NONE = 0
        MAPI = 1
        SMTP = 2
        SQLRDMAIL = 3
        GROUPWISE = 4
    End Enum

    Public Enum gEdition
        EVALUATION = 10
        BRONZE = 1
        SILVER = 2
        GOLD = 3
        STANDARD = 3
        PLATINUM = 4
        ENTERPRISE = 5
        ENTERPRISEPRO = 6
        ENTERPRISEPROPLUS = 7
        CORPORATE = 8
        PREMIUM = 9
        NULL = 99
    End Enum

#End Region

    Dim oLoader As frmLoader
    Dim wbLoading As Boolean = False

    Friend WithEvents wb As New WebBrowser
    Friend WithEvents bgWorker As New System.ComponentModel.BackgroundWorker
    Public Sub XceedLicensing()
        Xceed.Ftp.Licenser.LicenseKey = "FTN33TEWETWAKBNPNNA"
        Xceed.Editors.Licenser.LicenseKey = "EDN22PEW5THAWEJD8CA"
        Xceed.Grid.Licenser.LicenseKey = "GRD35TEW5T7AW45G85A"
        Xceed.Validation.Licenser.LicenseKey = "IVN119MW8TUBKAJL8CA"
    End Sub
    Private Function ReadLinkInfo() As Hashtable

        Try
            Dim oSec As clsMarsSecurity = New clsMarsSecurity
            Dim bytKey As Byte()
            Dim bytIV As Byte()
            Dim tempFile As String = sAppPath & "tempLink.tmp"
            Dim file As String = sAppPath & "link.dll"

            IO.File.Delete(tempFile)

            bytKey = oSec.CreateKey("Sar1532nika")
            bytIV = oSec.CreateIV("Fatsani2476")

            oSec.EncryptOrDecryptFile(file, tempFile, bytKey, bytIV, oSec.CryptoAction.ActionDecrypt)

            Dim dt As Hashtable

            Dim fs As FileStream = New FileStream(tempFile, FileMode.Open, FileAccess.Read)

            Dim bf As BinaryFormatter = New BinaryFormatter

            dt = CType(bf.Deserialize(fs), Hashtable)

            fs.Close()

            IO.File.Delete(tempFile)

            Return dt
        Catch
            Return Nothing
        End Try
    End Function
    Private Sub SaveLinkInfo(ByVal dt As Hashtable)
        Dim tempFile As String = sAppPath & "tempLink.tmp"
        Dim file As String = sAppPath & "link.dll"

        IO.File.Delete(tempFile)
        IO.File.Delete(file)

        Try
            Dim fs As FileStream = New FileStream(tempFile, FileMode.Create, FileAccess.ReadWrite)
            Dim bf As BinaryFormatter = New BinaryFormatter

            bf.Serialize(fs, dt)

            fs.Close()

            Dim bytKey As Byte()
            Dim bytIV As Byte()
            Dim oSec As clsMarsSecurity = New clsMarsSecurity

            bytKey = oSec.CreateKey("Sar1532nika")
            bytIV = oSec.CreateIV("Fatsani2476")

            oSec.EncryptOrDecryptFile(tempFile, file, bytKey, bytIV, oSec.CryptoAction.ActionEncrypt)

            IO.File.Delete(tempFile)
        Catch : End Try
    End Sub

    Public Sub _GetEdition(Optional ByVal SkipSub As Boolean = False, Optional ByVal oLoader As frmLoader = Nothing)
        Dim oProc As Process = New Process
        Dim oUI As New clsMarsUI
        Dim nTemp As Integer
        Dim linkPath As String = sAppPath & "link.exe"
        Dim infoPath As String = sAppPath & "edid.dll"
        Dim reTry As Integer = 0
        Dim overRideDateCheck As Boolean = False

        If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...10%")

StartOver:
        If IO.File.Exists(sAppPath & "link.dll") = True Then
            gSystemInfo = ReadLinkInfo()

            If gSystemInfo IsNot Nothing Then
                Dim infoDate As Date = gSystemInfo.Item("InfoDate")

                If (ConDate(Date.Now) = ConDate(infoDate)) Or overRideDateCheck = True Then
                    nTemp = gSystemInfo.Item("Edition")
                    nTimeLeft = gSystemInfo.Item("TimeLeft")
                Else
                    GoTo ReadLink
                End If
            Else
                GoTo ReadLink
            End If
        Else
ReadLink:
            If IO.File.Exists(linkPath) = False Then
                If RunEditor = True Then
                    MessageBox.Show("Could not locate critical system components. Please reinstall SQL-RD. (Code -1090)", _
                                   Application.ProductName, MessageBoxButtons.OK, _
                                   MessageBoxIcon.Warning)
                Else
                    _ErrorHandle("Could not locate critical system components. Please reinstall SQL-RD.", -1090, Reflection.MethodBase.GetCurrentMethod.Name, _
                    225, , True, True)
                End If


                End

            End If

RETRY:
            If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...30%")
            IO.File.Delete(infoPath)

            Microsoft.VisualBasic.Shell(Chr(34) & linkPath & Chr(34), AppWinStyle.NormalFocus, True)

            Dim nInfo As Integer = 0

            Do While IO.File.Exists(infoPath) = False And nInfo < 10
                Application.DoEvents()
                System.Threading.Thread.Sleep(1000)
                nInfo += 1
            Loop

            If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...40%")

            If IO.File.Exists(infoPath) = False Then
                If reTry < 3 Then
                    reTry += 1
                    System.Threading.Thread.Sleep(1000)
                    GoTo RETRY
                Else
                    If overRideDateCheck = False Then
                        overRideDateCheck = True
                        GoTo StartOver
                    End If
                End If

                If RunEditor = True Then
                    MessageBox.Show("Could not locate critical system components. Please reinstall SQL-RD. (Code -1090)", _
                                   Application.ProductName, MessageBoxButtons.OK, _
                                   MessageBoxIcon.Warning)
                Else
                    _ErrorHandle("Could not locate critical system components. Please reinstall SQL-RD.", -1090, Reflection.MethodBase.GetCurrentMethod.Name, _
                    225, , True, True)
                End If

                End
            End If

            If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...60%")

            Dim info As String = ReadTextFromFile(infoPath).Trim

            gSystemInfo = New Hashtable
            gSystemInfo.Add("InfoDate", ConDate(Date.Now))
            gSystemInfo.Add("Edition", info.Split("|")(0))
            gSystemInfo.Add("TimeLeft", info.Split("|")(1))
            gSystemInfo.Add("IsHP", info.Split("|")(2))
            gSystemInfo.Add("FingerPrint", oUI.ReadRegistry("HardwareFingerPrint", ""))

            nTemp = gSystemInfo.Item("Edition")
            nTimeLeft = gSystemInfo.Item("TimeLeft")
            SaveLinkInfo(gSystemInfo)
        End If

        If nTemp = 0 Then nTemp = 10

        gnEdition = nTemp

        If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...80%")

        Select Case gnEdition
            Case gEdition.EVALUATION
                gsEdition = "Evaluation"
                nMax = 0
            Case gEdition.BRONZE
                gsEdition = "Bronze"
                nMax = 15
            Case gEdition.SILVER
                gsEdition = "Silver"
                nMax = 30
            Case gEdition.GOLD, gEdition.STANDARD
                gsEdition = "Standard"
                nMax = 50
            Case gEdition.PLATINUM
                gsEdition = "Platinum"
                nMax = 0
            Case gEdition.ENTERPRISE
                gsEdition = "Enterprise"
                nMax = 0
            Case gEdition.ENTERPRISEPRO
                gsEdition = "Enterprise Pro"
                nMax = 0
            Case gEdition.ENTERPRISEPROPLUS
                gsEdition = "Enterprise Pro Plus"
                nMax = 0
            Case gEdition.CORPORATE
                gsEdition = "Corporate"
                nMax = 0
            Case Else
                MessageBox.Show("Could not validate system edition (Code -1080)", _
                Application.ProductName, MessageBoxButtons.OK, _
                MessageBoxIcon.Warning)
                End
        End Select


        If IsFeatEnabled(gEdition.NULL, modFeatCodes.g1_Unlimit_Sched) = True Then nMax = 0

        If IsFeatEnabled(gEdition.NULL, featureCodes.ppx_PremuimEdition) = True Then
            gsEdition = "Premium"
        End If

        If oLoader IsNot Nothing Then oLoader.SetMsg("Building  application UX...100%")

        If SkipSub = False Then _CheckHPStatus(gSystemInfo.Item("IsHP"))

    End Sub
    Private Sub _CheckHPStatus(ByVal IsHP As Boolean)
        Try
            Dim oUI As New clsMarsUI
            Dim sMsg As String

            sMsg = "SQL-RD cannot verify your subscription status." & vbCrLf & vbCrLf & _
            "This may be for one of the following reasons.  Please check and rectify:" & vbCrLf & vbCrLf & _
            "1.  The internet connection from this PC is not available and SQL-RD cannot contact the subscription server.  Please check and then start SQL-RD again." & vbCrLf & _
            "2.  The subscription server is temporarily down for maintenance.  Please wait 15 minutes and then try again." & vbCrLf & _
            "3.  Your subscription has lapsed.  Please purchase a new subscription"

            If IsHP = True Then
                If RunEditor = True Then
                    oLoader.SetMsg("Validating  subscription  status,  this  may  take  a  few  moments...")

                    If _CheckMaint() = False Then


                        _ErrorHandle(sMsg, -214760911, "MarsGlobal._CheckMaint", 0)

                        MessageBox.Show(sMsg, _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End
                    End If
                Else
                    Dim d As Date

                    Try
                        d = oUI.ReadRegistry("HyperCheck", Now.Date)
                    Catch ex As Exception
                        d = Now.Date
                    End Try

                    If Now.Date >= d.Date Then
                        If _CheckMaint() = False Then
                            _ErrorHandle(sMsg, -214760911, "MarsGlobal._CheckMaint", 0)
                            End
                        Else
                            d = d.AddDays(42)

                            oUI.SaveRegistry("HyperCheck", ConDate(d))
                        End If
                    End If
                End If
            End If
        Catch : End Try
    End Sub

    Private Function CheckInternet() As Boolean
        Dim o As System.Net.IPHostEntry
        Dim TcpAddress As String


        Try
            o = System.Net.Dns.GetHostByName("www.christiansteven.com")

            For Each s As System.Net.IPAddress In o.AddressList
                TcpAddress = s.ToString
            Next

            Dim t As New TcpClient

            t.Connect(TcpAddress, 80)

            t.Close()

            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function CheckViolation() As Boolean
10:     Try

            Dim oRs As ADODB.Recordset
            Dim SQL As String
            Dim nVal As Integer
            Dim scheduleCount As Integer = 0
            Dim hasViolated As Boolean = False

            'count enabled single schedules
20:         SQL = "SELECT COUNT(*) FROM ReportAttr r INNER JOIN ScheduleAttr s ON r.ReportID = s.ReportID WHERE s.Status = 1"

30:         oRs = clsMarsData.GetData(SQL)

40:         If oRs IsNot Nothing Then
50:             scheduleCount = oRs.Fields(0).Value

60:             oRs.Close()
            End If

            'count packed report where the package is enabled

70:         SQL = "SELECT COUNT(*) " & _
                   "FROM (ReportAttr INNER JOIN PackageAttr ON ReportAttr.PackID = PackageAttr.PackID) " & _
                   "INNER JOIN ScheduleAttr ON PackageAttr.PackID = ScheduleAttr.PackID " & _
                   "WHERE (((ScheduleAttr.Status)=1))"

80:         oRs = clsMarsData.GetData(SQL)

90:         If oRs IsNot Nothing Then
100:            scheduleCount += oRs.Fields(0).Value

110:            oRs.Close()
            End If

            'count enabled autos
120:        SQL = "SELECT COUNT(*) FROM AutomationAttr a INNER JOIN ScheduleAttr s ON a.AutoID = s.AutoID WHERE s.Status = 1"

130:        oRs = clsMarsData.GetData(SQL)

140:        If oRs IsNot Nothing Then
150:            scheduleCount += oRs.Fields(0).Value

160:            oRs.Close()
            End If

            'and events
170:        SQL = "SELECT COUNT(*) FROM EventAttr6 WHERE Status =1"

180:        oRs = clsMarsData.GetData(SQL)

190:        If oRs IsNot Nothing Then
200:            scheduleCount += oRs.Fields(0).Value

210:            oRs.Close()
            End If


220:        If nMax > 0 Then
230:            If scheduleCount > nMax Then
240:                If RunEditor = True Then MessageBox.Show("You have exceeded the number of allowed reports in a " & gnEdition.ToString & " edition(" & nMax & ")." & vbCrLf & _
                          "SQL-RD will disable all your schedules so that you can bring the report count within the allowed limits." & vbCrLf & _
                          "We apologise for any inconviniences caused", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

250:                clsMarsData.WriteData("UPDATE ScheduleAttr SET Status = 0")
260:                clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 0")

270:                hasViolated = True
                End If
            End If

            '280:        If gnEdition < gEdition.GOLD Then
            '290:            SQL = "SELECT COUNT(*) FROM AutomationAttr a INNER JOIN ScheduleAttr s ON a.AutoID = s.AutoID WHERE s.Status = 1"

            '300:            oRs = clsMarsData.GetData(SQL)

            '310:            If oRs IsNot Nothing Then

            '320:                nVal = oRs.Fields(0).Value
            '330:                oRs.Close()
            '                End If

            '340:
            '350:            If nVal > 0 Then
            '360:                If RunEditor = True Then MessageBox.Show("You cannot have automation schedules in this edition." & _
            '                          "All automation schedules will be disabled until you upgrade to a " & _
            '                          "Gold edition or above.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            '370:                clsMarsData.WriteData("UPDATE s SET s.Status = 0 FROM ScheduleAttr s INNER JOIN AutomationAttr a ON s.AutoID = a.AutoID")

            '380:                hasViolated = True
            '                End If
            '            End If

390:        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.s3_EventBasedScheds) = False Then
400:            SQL = "SELECT COUNT(*) FROM EventAttr6 WHERE Status = 1"

410:            oRs = clsMarsData.GetData(SQL)

420:            If oRs IsNot Nothing Then
430:                nVal = oRs.Fields(0).Value
440:                oRs.Close()
                End If

450:

460:            If nVal > 0 Then
470:                If RunEditor = True Then MessageBox.Show("You cannot have event-based schedules in this edition." & _
                          "All event-based schedules will be disabled until you upgrade to an " & _
                          "Enterprise Pro Plus edition or above.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

480:                clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 0")

490:                hasViolated = True
                End If
            End If

500:        Return hasViolated
510:    Catch ex As Exception
520:        Return False
        End Try
    End Function


    Private Function _CheckMaint() As Boolean
        Dim oValue As Boolean
        Dim sCustID As String
        Dim oBj As Object
        Dim oUI As New clsMarsUI

        Try
            'get the custid
            sCustID = oUI.ReadRegistry("CustNo", "")

            If sCustID Is Nothing Then Return False

            If sCustID = "" Then
                sCustID = InputBox("Please enter your customer number", Application.ProductName)
            End If

            'check internet connection
            'If CheckInternet() = False Then
            'Throw New Exception("Cannot establish internet connection")
            'End If

            wb.Navigate("http://www.christiansteven.com/_supp/checksqlrd.cgi") ''

            Do
                Application.DoEvents()
            Loop Until wb.ReadyState = WebBrowserReadyState.Complete

            For Each o As System.Windows.Forms.HtmlElement In wb.Document.All
                If o.Name = "number" Then

                    o.SetAttribute("value", sCustID)
                    Application.DoEvents()
                    Exit For
                End If
            Next

            For Each f As System.Windows.Forms.HtmlElement In wb.Document.Forms
                f.InvokeMember("submit")
            Next

            Do While wbLoading = True
                Application.DoEvents()
            Loop
            'For Each oBj In oIE.Document.All.tags("input")
            '    If oBj.name.ToLower = "number" Then
            '        oBj.Value = sCustID
            '    End If
            'Next

            'For Each oBj In oIE.Document.All.Tags("form")
            '    oBj.Submit()
            'Next

            'Do
            '    Application.DoEvents()
            'Loop Until oIE.Busy = False

            'oHTML = oIE.Document

            Dim s As String = wb.Document.Body.InnerText 'oHTML.body.innerText

            If s.IndexOf("Error") > -1 Then
                oValue = False
            ElseIf s.Length > 50 Then
                oValue = False
            ElseIf s.IndexOf("OK") > -1 Then
                oValue = True
            Else
                oValue = False
            End If

        Catch ex As Exception
            _ErrorHandle("An error was encountered during client validation. (" & ex.Message & ")", _
             -2147645911, Reflection.MethodBase.GetCurrentMethod.Name, 0, "Please make sure you are connected to the internet.")
            oValue = False
        End Try

        Return oValue

    End Function
    Public Sub _CheckAdminExists()
        Dim oData As New clsMarsData
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM CRDUsers WHERE UserRole = 'Administrator' AND " & _
        "UserID <> 'SQLRDAdmin'"

        oRs = clsmarsdata.GetData(SQL)


        If Not oRs Is Nothing Then
            If oRs.EOF = True Then
                'Dim oAdmin As New frmSetupAdmin

                'oAdmin.ShowDialog()
                Dim ostarter As frmStarterWizard = New frmStarterWizard

                ostarter.ShowDialog()
            End If
        Else

            'Dim oAdmin As New frmSetupAdmin

            'oAdmin.ShowDialog()

            Dim ostarter As frmStarterWizard = New frmStarterWizard

            ostarter.ShowDialog()
        End If
    End Sub
    Public Sub _CheckServices()
        Dim oProc As New Process
        Dim sType As String
        Dim oUI As New clsMarsUI
        Dim DoNotCheck, DelayRestart As Boolean
        Dim DelayRestartBy As Integer = 0

        DoNotCheck = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("CheckScheduler", 0)))
        DelayRestart = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DelayRestart", 0)))
        DelayRestartBy = Convert.ToInt32(oUI.ReadRegistry("DelayRestartBy", 0))

        'convert to seconds
        DelayRestartBy = DelayRestartBy * 60

        If DoNotCheck = True Then Exit Sub

        Try
            sType = oUI.ReadRegistry("SQL-RDService", "NONE")

            If sType = "WindowsApp" Then
                For Each oProc In Process.GetProcesses
                    If oProc.ProcessName.ToLower = "sqlrdapp" Then
                        Exit Sub
                    End If
                Next

                If DelayRestart = True Then
                    System.Threading.Thread.Sleep(DelayRestartBy * 1000)
                End If

                If Process.GetProcessesByName("sqlrdapp").GetLength(0) = 0 Then
                    oProc.Start(sAppPath & "sqlrdapp.exe")
                End If

            ElseIf sType = "WindowsService" Then
                Dim oSrv As New ServiceController("SQL-RD")

                If DelayRestart = True Then
                    System.Threading.Thread.Sleep(DelayRestartBy * 1000)
                End If

                If oSrv.Status = ServiceControllerStatus.Stopped Then
                    oSrv.Start()
                End If

                oSrv = New ServiceController("SQL-RD Monitor")

                If oSrv.Status = ServiceControllerStatus.Stopped Then
                    oSrv.Start()
                End If
            End If
        Catch : End Try

        createFolderShare()
    End Sub

    Sub createFolderShare()
        'we will jump in here and set up a share to the christiansteven folder
        Try
            Dim fshare As IADsFileService
            Dim newShare As IADsFileShare

            fshare = GetObject("WinNT://" & Environment.MachineName & "/lanmanserver")

            newShare = fshare.Create("fileshare", "ChristianSteven")

            Dim sPath As String = GetDirectory(Application.StartupPath)

            If sPath.EndsWith("\") Then sPath = sPath.Remove(sPath.Length - 1, 1)

            newShare.Path = sPath
            newShare.SetInfo()
            newShare = Nothing
        Catch : End Try

        'Try
        '    Dim f As New System.Security.Permissions.FileIOPermission(System.Security.Permissions.PermissionState.None)

        '    f.AddPathList(System.Security.Permissions.FileIOPermissionAccess.AllAccess, Environment.MachineName & "\ChristianSteven")

        '    Try
        '        f.Demand()
        '    Catch ex As Exception

        '    End Try
        '    System.Security.Permissions.FileIOPermission()

        '    Dim SD As IADsSecurityDescriptor
        '    Dim sec As New ADsSecurityUtility
        '    Dim Dacl As IADsAccessControlList
        '    Dim newAce As New AccessControlEntry

        '    SD = sec.GetSecurityDescriptor("FILE://\\" & Environment.MachineName & "\ChristianSteven", 0, 0)

        '    Dim Ace As Object = CreateObject("AccessControlEntry")
        '    Dacl = SD.DiscretionaryAcl
        '    newAce.Trustee = "Domain\ChristianSteven"

        '    newAce.AceType = 0
        '    newAce.AceFlags = 3

        '    Dacl.AddAce(newAce)
        '    SD.DiscretionaryAcl = Dacl
        '    sec.SetSecurityDescriptor(SD, 0, 0, 0)


        'Catch : End Try
    End Sub
    Public Sub _FirstRun()
        Dim FirstRun As Boolean
        Dim oUI As New clsMarsUI
        Dim sChar As String = Chr(34)
        Dim oControl As New clsServiceController

        Try
            FirstRun = oUI.ReadRegistry("FirstRun", 1)
        Catch
            FirstRun = True
        End Try

        If FirstRun = False Then Return

        'install the background application
        oUI.BusyProgress(50, "Installing application...")

        oControl.InstallBGScheduler()

        oControl.RemoveNTService()

        oUI.BusyProgress(95, "Cleaning up...")

        'save the settings
        oUI.SaveRegistry("SQL-RDService", "WindowsApp")

        ServiceType = "WindowsApp"

        oControl.StartScheduler()

        oUI.BusyProgress(, , True)

    End Sub


    Public Sub _RestartServices()
        
        Dim controller As clsServiceController = New clsServiceController

        controller.StartScheduler()

    End Sub
    Public Sub _StopServices()
        

        Dim controller As clsServiceController = New clsServiceController

        controller.StopScheduler()

        MessageBox.Show("Please ensure that the SQL-RD Schedule editor is closed before proceeding." & Environment.NewLine & _
        "Click OK to continue", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub
    Public Sub _CreateCrystal()
        
    End Sub
    Private Sub DoCheckReg(ByVal oUI As clsMarsUI)
        Dim sReg As String
        Dim sCompany As String
        Dim sUser As String
        Dim sFirstName As String = ""
        Dim sLastName As String = ""
        Dim CustNo As String = ""

        sReg = oUI.ReadRegistry("RegNo", "")
        sCompany = oUI.ReadRegistry("RegCo", "")
        sUser = oUI.ReadRegistry("RegUser", "")
        sFirstName = oUI.ReadRegistry("RegFirstName", "")
        sLastName = oUI.ReadRegistry("RegLastName", "")
        CustNo = oUI.ReadRegistry("CustNo", "")

        If sReg.Length = 0 Then
            Dim TempKey As String = "Software\ChristianSteven\Crystal Reports Distributor"

            sReg = oUI.ReadRegistry(Registry.LocalMachine, TempKey, "RegNo", "")
            sCompany = oUI.ReadRegistry(Registry.LocalMachine, TempKey, "RegCo", "")
            sUser = oUI.ReadRegistry(Registry.LocalMachine, TempKey, "RegUser", "")
            CustNo = oUI.ReadRegistry(Registry.LocalMachine, TempKey, "CustNo", "")

            If sReg.Length > 0 Then
                oUI.SaveRegistry("RegCo", sCompany)
                oUI.SaveRegistry("RegNo", sReg)
                oUI.SaveRegistry("RegUser", sUser)
                oUI.SaveRegistry("CustNo", CustNo)
            End If

        End If

        Try
            If clsLicenser.m_licenser.CheckLicense(sReg, sFirstName, sLastName, sCompany, CustNo) = False Then
                Dim oReg As New frmAbout

                oReg.ProvideLicense()
            End If
        Catch
            Dim oReg As New frmAbout

            oReg.ProvideLicense()
        End Try
    End Sub
    Sub showerror(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs)
        MsgBox(e.Exception.ToString)
    End Sub


    <STAThread()> Public Sub Main()

        Dim sArgs() As String
        Dim oData As clsMarsData
10:     Dim oUI As clsMarsUI = New clsMarsUI
11:     Dim errorCount As Integer = 0
20:     Dim oTool As New clsSystemTools

RETRY:
        Try
            'add handlers for any unhandled exceptions
            Dim currentDomain As AppDomain = AppDomain.CurrentDomain
            Dim eh As clsUnhandledException = New clsUnhandledException

            AddHandler currentDomain.UnhandledException, AddressOf eh.onUnhandledException
            AddHandler Application.ThreadException, AddressOf eh.onThreadException

            Application.EnableVisualStyles()

            'get command line arguments
30:         sArgs = Environment.GetCommandLineArgs

40:         Try
50:             If sArgs.Length > 1 Then
60:                 If sArgs(1).ToLower.IndexOf("syspath") > -1 Then
70:                     RunEditor = True
80:                 Else
90:                     RunEditor = False
100:                End If
110:            Else
120:                RunEditor = True
130:            End If
            Catch ex As Exception
140:            RunEditor = True
            End Try

            'see if we need to migrate the system before anything else
            Dim migrator As clsMigration = New clsMigration
            migrator.MigrateSystemDBtoLocal()

150:        If RunEditor = True Then
160:            oLoader = New frmLoader
            End If

170:        If RunEditor = True Then
180:            oLoader.SetMsg("Setting  global  variables...")
190:        End If

200:        _SetGlobalVars()

210:        Application.DoEvents()

220:        If RunEditor = True Then

#If CRYSTAL_VER < 12 Then
230:            oLoader.SetMsg("Registering  COM  handlers...")
#Else
230:            oLoader.SetMsg("Loading core interface...")
#End If

240:            _CreateCrystal()
            End If

            'enter license for the Xceed FTP object
300:        'Xceed.Ftp.Licenser.LicenseKey = "FTN20-Y4A17-H4RL5-M4CA"
310:        Application.DoEvents()
            XceedLicensing()
320:        If RunEditor = True Then
330:            oLoader.SetMsg("Connecting  to  SQL-RD  system...")
            End If

340:        oData = New clsMarsData

350:        oData.CreateDB()
360:        Application.DoEvents()

370:        oData.OpenMainDataConnection()
380:        Application.DoEvents()

460:    Catch ex As Exception

461:        Select Case errorCount
                Case 0
462:                clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onClose)
463:                errorCount += 1
464:                GoTo RETRY
                Case 1
465:                clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onOpen)
466:                errorCount += 1
467:                GoTo RETRY
                Case Else
                    If RunEditor = True Then
468:                    frmSystemError.showError(ex, Err.Number, Erl)
                    End If
            End Select
        End Try

        If RunEditor = True Then
470:        Try

                gTimeOffSet = sqlrd.clsMarsTimeZones.m_masterZoneAdjust

471:            errorCount = 0

480:            oLoader.SetMsg("Cleaning  up  system  database...")

490:            clsMarsData.DataItem.CleanDB(, , , , , , , oLoader)

500:            oLoader.SetMsg("Verifying database Integrity...")

510:            clsMarsData.DataItem.UpgradeCRDDb(, , , True, oLoader)

511:            clsMarsData.ValidateSystemStruct()
                clsMarsEvent.RecacheAllDatasets()

520:            bgWorker.RunWorkerAsync()

530:            oTool._CheckSysSize()

540:            oTool._CheckSysIntegrity()

580:            oLoader.SetMsg("Building  application UX...5%")

590:            _GetEdition(, oLoader)

600:            Try
610:                gImages = MarsCommon.InitImages
                Catch : End Try

620:            gVisible = True

630:            If gnEdition <> gEdition.EVALUATION Then
640:                DoCheckReg(oUI)
650:                CheckViolation()
660:            End If

670:            oLoader.Close()
680:            oLoader = Nothing

690:            _CheckAdminExists()

700:            oMain = New frmMain

                Dim oCheck As Threading.Thread

                oCheck = New Threading.Thread(AddressOf _CheckServices)

                oCheck.Start()

710:            '_CheckServices()
            Catch ex As Exception
711:            Select Case errorCount
                    Case 0
713:                    clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onClose)
714:                    errorCount += 1
715:                    GoTo RETRY
                    Case 1
716:                    clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onOpen)
717:                    errorCount += 1
718:                    GoTo RETRY
                    Case Else
719:                    frmSystemError.showError(ex, Err.Number, Erl)
                End Select
            End Try

720:        Application.Run(oMain)

            'clean the database and delete any temporary entries
            Try
721:            clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)

780:            oData.CloseMainDataConnection()

790:            oApp = Nothing

                End
            Catch ex As Exception
                frmSystemError.showError(ex, Err.Number, Erl)
            End Try
        Else

            'DKW: Attach to process before closing MessageBox
            'MsgBox("WAITING!")

            gRole = "Administrator"
            gUser = "SQLRDAdmin"

            SaveTextToFile("**************SQL-RD 6.5 Scheduler Loading*************", sAppPath & "scheduler.debug", , False)

            Select Case sArgs(1).ToLower
                Case "register"
                    IO.File.Delete(sAppPath & "link.dll")

                    Dim oProc As New Process
                    _StopServices()

                    oProc.StartInfo.Arguments = "register"
                    oProc.StartInfo.FileName = sAppPath & "link.exe"
                    oProc.Start()
                    oProc.WaitForExit()

                    _RestartServices()
                Case "regwiz"
                    IO.File.Delete(sAppPath & "link.dll")

                    Dim oSys As New clsSystemTools

                    oSys._ActivateSystem(True, True)
                Case "fixclock"
                    Dim oProc As New Process

                    oProc.StartInfo.Arguments = "fixclock"
                    oProc.StartInfo.FileName = sAppPath & "link.exe"
                    oProc.Start()
                Case "unregister"
                    IO.File.Delete(sAppPath & "link.dll")

                    Dim oProc As New Process

                    oProc.StartInfo.Arguments = "unregister"
                    oProc.StartInfo.FileName = sAppPath & "link.exe"
                    oProc.Start()
                Case "unregwiz"
                    IO.File.Delete(sAppPath & "link.dll")

                    Dim oSys As New clsSystemTools

                    oSys._DeactivateSystem(True)
                Case "systemmonitor"
                    Dim sysMon As frmSystemMonitor = New frmSystemMonitor

                    sysMon.Text = "SQL-RD - System Monitor"
                    sysMon.MenuItem12.Enabled = False

                    RunEditor = True

                    Application.Run(sysMon)
                Case "updatecheck"
                    Dim ocheck As clsSystemTools = New clsSystemTools

                    ocheck.checkforUpdates(gDownloadUrl, True)

                Case Else

                    'Dim sValue As String = ReadTextFromFile(sAppPath & "\edid.dll")

                    '_CheckHPStatus(sValue.Split(":")(2))

                    Dim sIn As String

                    gVisible = False

                    SaveTextToFile(ConDateTime(Now) & ": Initializing main data connection", sAppPath & "scheduler.debug", , True)

                    oData.OpenMainDataConnection()
                    Application.DoEvents()

                    SaveTextToFile(ConDateTime(Now) & ": Confirming user edition", sAppPath & "scheduler.debug", , True)

                    sIn = sArgs(1).ToLower.Trim

                    If sIn.Contains("!xz") Or sIn.Contains("!re") Or sIn.Contains("!xe") Then
                        gnEdition = gEdition.CORPORATE
                    Else
                        _GetEdition(True)
                    End If

                    SaveTextToFile(ConDateTime(Now) & ": Confirming system edition status", sAppPath & "scheduler.debug", , True)

                    If gnEdition <> gEdition.EVALUATION And gnEdition <> gEdition.CORPORATE Then
                        If CheckViolation() = True Then
                            _ErrorHandle("Schedule execution not peformed as the system is currently in violation of the " & _
                            gsEdition & " license. " & vbCrLf & _
                            "Please run the editor to make sure that you have not exceeded number of " & _
                            "schedules allowed in " & gsEdition & " edition.", -7, _
                            Reflection.MethodBase.GetCurrentMethod.Name, 968, , True, True)

                            End
                        End If
                    End If

                    If sIn.StartsWith("oop") Then
                        Dim oop As clsOutofProcessExecutions = New clsOutofProcessExecutions

                        oop.processCommand()
                    ElseIf sIn.StartsWith("-s") Then
                        Dim oCom As New clsCommandLines
                        oCom._Single()
                    ElseIf sIn.StartsWith("-a") Then
                        Dim oCom As New clsCommandLines
                        oCom._Auto()
                    ElseIf sIn.StartsWith("-ep") Then
                        Dim oCom As New clsCommandLines
                        oCom._EventPackage()
                    ElseIf sIn.StartsWith("-e") Then
                        Dim oCom As New clsCommandLines
                        oCom._Event()
                    ElseIf sIn.StartsWith("!xp") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.PACKAGE)
                    ElseIf sIn.StartsWith("!xs") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.REPORT)
                    ElseIf sIn.StartsWith("!xa") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.AUTOMATION)
                    ElseIf sIn.StartsWith("!xe") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.EVENTBASED)
                    ElseIf sIn.StartsWith("!xf") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.SMARTFOLDER)
                    ElseIf sIn.StartsWith("!xz") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.ScheduleThread(sArgs(2), clsMarsScheduler.enScheduleType.EVENTPACKAGE)
                    ElseIf sIn.StartsWith("!xo") Then
                        Dim oSchedule As New clsMarsScheduler

                        oSchedule.OtherScheduleProcesses(False)
                    ElseIf sIn.StartsWith("!re") Then
                        Dim oEvent As clsMarsEvent = New clsMarsEvent

                        oEvent.RunEvents6()
                    ElseIf sIn.StartsWith("!xr") Then
                        Try
                            clsMarsScheduler.globalItem.scheduleRetries()
                        Catch : End Try
                    ElseIf sIn.StartsWith("-p") Then
                        Dim oCom As New clsCommandLines
                        oCom._Package()
                    ElseIf sIn.StartsWith("-x") Then
                        Dim oCom As New clsCommandLines

                        oCom._CreateSingleSchedule()
                    ElseIf sIn.StartsWith("-b") Then

                        sIn = ""

                        For I As Int32 = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
                            sIn &= Environment.GetCommandLineArgs(I) & " "
                        Next

                        sIn = sIn.Trim

                        oTool._BackupSystem(sIn.Split("/")(1), False)
                    ElseIf sIn.StartsWith("-r") Then
                        Dim oTools As New clsSystemTools

                        oTools._RefreshAll()
                    ElseIf sIn.StartsWith("/?") Or sIn.StartsWith("?") Or _
                    sIn.ToLower.StartsWith("help") Or _
                    sIn.ToLower.StartsWith("/help") Then
                        Dim sHelp As String = "To execute a Single Schedules:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -s schedulename=[nameofschedule];sendto=[emailaddress];faxto=[faxnumber];smsto=[cellnumber];showmsg=[yes/no];parameter=[parametername]:[parametervalue];owner=[ownername]" & vbCrLf & vbCrLf & _
                        "Use | to specify range values for parameters e.g. parameter=Order Date:2004-01-01|2004-12-31" & vbCrLf & vbCrLf & _
                        "To execute a Package Schedule:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -p schedulename=[nameofschedule];sendto=[emailaddress];showmsg=[yes/no];owner=[ownername]" & vbCrLf & vbCrLf & _
                        "To execute an Automation Schedule:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -a schedulename=[nameofschedule];showmsg=[yes/no];owner=[ownername]" & vbCrLf & vbCrLf & _
                        "To execute an Event-Based Schedule:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -e schedulename=[nameofschedule];showmsg=[yes/no];owner=[ownername]" & vbCrLf & vbCrLf & _
                        "To execute an Event-Based Package:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -ep schedulename=[nameofschedule];showmsg=[yes/no];owner=[ownername]" & vbCrLf & vbCrLf & _
                        "To perform a system backup:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -b/[pathforbackup]" & vbCrLf & vbCrLf & _
                        "To specify a schedule location:" & vbCrLf & vbCrLf & _
                        "sqlrd.exe syspath=[path to folder]  e.g. sqlrd.exe syspath=c:\mysqlrddata.  By default SQL-RD will use the data held in the folder it was installed in. (Program Files\ChristianSteven\SQL-RD)" & vbCrLf & vbCrLf & _
                        "To refresh all schedules:" & vbCrLf & vbCrLf & _
                        "...\Program Files\ChristianSteven\SQL-RD\sqlrd.exe -r"

                        MessageBox.Show(sHelp, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

                        End
                    Else
                        Dim SysType As String

                        SysType = oUI.ReadRegistry("SystemEnvironment", "")

                        If SysType = "" Then
                            Dim oScheduler As clsMarsScheduler = New clsMarsScheduler

                            SaveTextToFile(ConDateTime(Now) & ": Loading schedules", sAppPath & "scheduler.debug", , True)

                            oScheduler.Schedule()
                        Else
                            Dim sClusterType As String
                            Dim oCluster As New clsClustering
                            sClusterType = oUI.ReadRegistry("ClusterType", "")

                            If sClusterType = "Master" Then

                                Dim oSchedule As clsMarsScheduler = New clsMarsScheduler

                                oCluster.RunCluster()

                                oSchedule.OtherScheduleProcesses()
                            Else
                                oCluster.ObeyCluster()
                            End If

                        End If
                    End If

                    SaveTextToFile(ConDateTime(Now) & ": Closing data connection", sAppPath & "scheduler.debug", , True)

                    clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile)
                    oData.ClearHistory()
                    oData.CloseMainDataConnection()

                    SaveTextToFile("*************Schedule Run Completed**************", sAppPath & "scheduler.debug", , True)

            End Select
        End If
    End Sub


    Private Sub nonPriorityTasks()
        clsMarsData.DataItem.CleanDB()

        clsMarsUI.MainUI.Archive()

        clsMarsData.DataItem.ClearHistory()

        'clear snapshots
        Dim oSnap As New clsMarsSnapshots
        oSnap._ClearSnapshots()

        Dim cmb As New ComboBox

        clsMarsData.DataItem.GetDsns(cmb)

        If cmb.Items.IndexOf("SQL-RD Samples") = -1 Then
            clsMarsData.CreateSampeDSN()
        End If

        'set default values
        Try
            With clsMarsUI.MainUI
                If gConType <> "DAT" Then
                    .SaveRegistry(Registry.LocalMachine, sKey, "AutoCompact", 0)
                End If
            End With
        Catch : End Try

        Dim oSec As New clsMarsSecurity

        oSec._EncryptDBValuesOnce()

        clsMarsData.DataItem.UpdatePackOrder()

        For Each s As String In IO.Directory.GetFiles(sAppPath)
            If s.EndsWith(".debug") Or s.EndsWith("_debug.log") Then
                Try
                    IO.File.Delete(s)
                Catch : End Try
            End If
        Next
        
    End Sub
    Public Sub _SetGlobalVars()
        Try
            Dim sPath As String
            Dim oUI As clsMarsUI = New clsMarsUI
            Dim sMail As String

            sKey = "Software\ChristianSteven\" & Application.ProductName

            If Environment.GetCommandLineArgs.Length = 1 Then
                sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
            Else
                If Environment.GetCommandLineArgs(1).IndexOf("syspath") = -1 Then
                    sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
                Else
                    Dim sTemp As String

                    For I As Integer = 1 To Environment.GetCommandLineArgs.GetUpperBound(0)
                        sTemp &= Environment.GetCommandLineArgs(I) & " "
                    Next

                    sPath = sTemp.Split("=")(1).Trim

                    If sPath.ToLower.IndexOf("sqlrdlive.dat") = -1 Then
                        If sPath.EndsWith("\") = False Then sPath &= "\"

                        sPath &= "sqlrdlive.dat"
                    End If

                    Try
                        If System.IO.File.Exists(sPath) = False Then
                            MessageBox.Show("SQL-RD could not connect to the specified system file. SQL-RD will use the default system file", _
                            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                            sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
                        End If
                    Catch
                        MessageBox.Show("SQL-RD could not connect to the specified system file. SQL-RD will use the default system file", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                        sPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "sqlrdlive.dat")
                    End Try
                End If
            End If


            sAppPath = Application.ExecutablePath.ToLower.Replace(assemblyName, "")

            If sAppPath.EndsWith("\") = False Then sAppPath &= "\"

            gPath = sAppPath & "Samples"

            gConfigFile = sAppPath & "sqlrdlive.config"

            ValidateConfigFile()

            clsMarsUI.TransferSettings()

            clsSettingsManager.Appconfig.populateConfigTable(gConfigFile)

            sMail = oUI.ReadRegistry("MailType", "NONE")

            Select Case sMail
                Case "MAPI"
                    MailType = gMailType.MAPI
                Case "SMTP"
                    MailType = gMailType.SMTP
                Case "NONE"
                    MailType = gMailType.NONE
                Case "SQLRDMAIL"
                    MailType = gMailType.SQLRDMAIL
                Case "GROUPWISE"
                    MailType = gMailType.GROUPWISE
            End Select


            Dim ConType As String

            ConType = oUI.ReadRegistry("ConType", "DAT")

            If ConType = "DAT" Then
                gConType = "DAT"
                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
            Else
                'don't just decrypt it as it may not be encrypted...
                Dim test As String = oUI.ReadRegistry("ConString", "")

                If test.ToLower.Contains("provider") Then
                    oUI.SaveRegistry("ConString", test, True, , True)
                End If

                gConType = ConType '"ODBC"
                sCon = oUI.ReadRegistry("ConString", "", True)
            End If

            If sCon.Length = 0 Then
                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"
            End If

            Try
                CustID = oUI.ReadRegistry("CustNo", "0")
            Catch ex As Exception
                CustID = 0
            End Try

            gHelpPath = sAppPath & "SQL-RD.chm"

            gServiceType = oUI.ReadRegistry("SQL-RDService", "NONE")

            'rename sqlrderror.log
            Try
                If IO.File.Exists(sAppPath & "sqlrderror.legacy") = False And IO.File.Exists(sAppPath & "sqlrderror.log") Then
                    IO.File.Move(sAppPath & "sqlrderror.log", sAppPath & "sqlrderror.legacy")

                    Dim msg As String = "The error log has been moved from a new system file. To view its contents, open SQL-RD and " & _
                    "go to System Monitor -> Error Log." & vbCrLf & _
                    "The contents of the old error log have been copied into sqlrderror.legacy."

                    SaveTextToFile(msg, sAppPath & "sqlrderror.log", , False, False)
                End If
            Catch : End Try
        Catch : End Try
    End Sub
    Private Sub ValidateConfigFile()
        Try
            clsMarsUI.MainUI.SaveRegistry("CheckSum", "19C5299")

            If IO.File.Exists(sAppPath & "configRestore\config_onclose.bak") = True Then
                Dim custNo(1) As String
                custNo(0) = clsMarsUI.MainUI.ReadRegistry("CheckSum", "19C5299")

                Try
                    custNo(1) = clsMarsUI.MainUI.ReadRegistry("CheckSum", "19C5299", , sAppPath & "configRestore\config_onclose.bak")
                Catch
                    custNo(1) = "19C5299"

                    Try
                        IO.File.Delete(sAppPath & "configRestore\config_onclose.bak")
                    Catch : End Try
                End Try

                If custNo(0) <> custNo(1) Then
                    clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onClose)
                End If
            End If
        Catch ex As Exception
            clsSystemTools.RestoreConfigFile(clsSystemTools.configSaveTime.onClose)
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMsg"></param>
    ''' <param name="nNumber"></param>
    ''' <param name="sSource"></param>
    ''' <param name="nLine"></param>
    ''' <param name="sSuggest"></param>
    ''' <param name="Severity">0 is for errors visible to all and 1 for errors only visible to support team</param>
    ''' <remarks></remarks>
    Private Sub LogError(ByVal sMsg As String, ByVal nNumber As Int64, _
    ByVal sSource As String, ByVal nLine As Integer, _
    Optional ByVal sSuggest As String = "", Optional ByVal Severity As Integer = 0)
        Dim oCon As ADODB.Connection = New ADODB.Connection

        Try
            Dim SQL As String
            Dim conString As String

            Dim sPath As String = Application.StartupPath & "\eventlog.dat"

            If IO.File.Exists(sPath) = False Then Return

            conString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sPath & ";Persist Security Info=False"

            oCon.Open(conString)

            Dim cols, vals As String

            cols = "eventid,pcname,schedulename,entrydate,errordesc,errornumber,errorsource,errorline,errorsuggestion,errorseverity"

            Dim oRs As ADODB.Recordset = New ADODB.Recordset
            Dim dataID As Integer = 0

            oRs.Open("SELECT MAX(eventid) FROM EventLogAttr", oCon)

            If oRs.EOF = False Then
                dataID = IsNull(oRs(0).Value, 0) + 1
            End If

            oRs.Close()

            vals = dataID & "," & _
            "'" & SQLPrepare(Environment.MachineName) & "'," & _
            "'" & SQLPrepare(gScheduleName) & "'," & _
            "'" & ConDateTime(Now) & "'," & _
            "'" & SQLPrepare(sMsg) & "'," & _
            nNumber & "," & _
            "'" & sSource & "'," & _
            nLine & "," & _
            "'" & SQLPrepare(sSuggest) & "'," & _
            Severity

            SQL = "INSERT INTO EventLogAttr (" & cols & ") VALUES (" & vals & ")"

            oCon.Execute(SQL)

            oCon.Close()

            oCon = Nothing
            oRs = Nothing
        Catch ex As Exception
            Try
                oCon.Close()
            Catch : End Try
            Return
        End Try
    End Sub
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sMsg"></param>
    ''' <param name="nNumber"></param>
    ''' <param name="sSource"></param>
    ''' <param name="nLine"></param>
    ''' <param name="sSuggest"></param>
    ''' <param name="DoNotEmail"></param>
    ''' <param name="NoDialogue"></param>
    ''' <param name="severity">0 is for errors visible to all and 1 for errors only visible to support team</param>
    ''' <remarks></remarks>
    Public Sub _ErrorHandle(ByVal sMsg As String, ByVal nNumber As Int64, _
    ByVal sSource As String, ByVal nLine As Integer, _
    Optional ByVal sSuggest As String = "", Optional ByVal DoNotEmail As Boolean = False, _
    Optional ByVal NoDialogue As Boolean = False, Optional ByVal severity As Integer = 0)

        Dim sErrorMsg As String

        Try
            Dim sError As String = ""
            Dim sAlertType As String = ""
            Dim oUI As New clsMarsUI

            If sMsg.Length = 0 Then Return

            sErrorMsg = "PC Name: " & Environment.MachineName & vbCrLf & _
            "Schedule Name (if applicable): " & gScheduleName & vbCrLf & _
            "Date: " & Date.Now & Environment.NewLine & _
            "Error Description: " & sMsg & Environment.NewLine & _
            "Error Number: " & nNumber & Environment.NewLine & _
            "Error Source: " & sSource & Environment.NewLine & _
            "Line Number: " & nLine

            sErrorMsg = "-----------------------------------------------------" & Environment.NewLine & _
            sErrorMsg & Environment.NewLine & _
            "-----------------------------------------------------"

            If gVisible = True And NoDialogue = False Then
                Dim oError As frmError = New frmError

                oError.ShowError(nNumber, sSource, sMsg, nLine, sSuggest)
            End If

            Dim ToLog As Integer = 1
            Dim ToMail As Integer = 0
            Dim ToSMS As Integer = 0
            Dim oReg As RegistryKey = Registry.LocalMachine

            ToLog = 1
            ToMail = oUI.ReadRegistry("ErrorByMail", 0)
            ToSMS = oUI.ReadRegistry("ErrorBySMS", 0)

            If ToLog = 1 Then
                'Dim sPath As String = sAppPath & "sqlrderror.log"

                'SaveTextToFile(sErrorMsg, sPath, , True)
                LogError(sMsg, nNumber, sSource, nLine, sSuggest, severity)

            End If

            If ToMail = 1 Then
                If DoNotEmail = True Then Return

                If RunEditor = True Then Return

                Dim sAlertWho As String
                Dim oMsg As New clsMarsMessaging

                sAlertWho = oUI.ReadRegistry("AlertWho", "")

                If sAlertWho.Length = 0 Then Return

                sAlertWho = clsMarsMessaging.ResolveEmailAddress(sAlertWho)

                If sAlertWho.Length > 0 Then
                    If MailType = gMailType.MAPI Then
                        clsMarsMessaging.SendMAPI(sAlertWho, "SQL-RD Error Alert", sErrorMsg, _
                        "", , , , , , False, , , True)
                    ElseIf MailType = gMailType.SMTP Or MailType = gMailType.SQLRDMAIL Then

                        Dim objSender As clsMarsMessaging = New clsMarsMessaging

                        objSender.SendSMTP(sAlertWho, "SQL-RD Error Alert", sErrorMsg, "", , _
                        , , , , , False, , True)
                    ElseIf MailType = gMailType.GROUPWISE Then
                        clsMarsMessaging.SendGROUPWISE(sAlertWho, "", "", "SQL-RD Error Alert", sErrorMsg, "", "Single")
                    End If
                End If
            End If

            If ToSMS = 1 Then

                Dim sAlertWho As String
                Dim oMsg As New clsMarsMessaging

                If RunEditor = True Then Return

                sAlertWho = oUI.ReadRegistry("SMSAlertWho", "")

                If sAlertWho.Length = 0 Then Return

                sAlertWho = clsMarsMessaging.ResolveEmailAddress(sAlertWho)

                If sAlertWho.Length > 0 Then clsMarsMessaging.SendSMS(sAlertWho, sErrorMsg, "")

            End If
        Catch ex As Exception
            Dim sPath As String = sAppPath & "sqlrderror.log"

            SaveTextToFile("Error logging error message (" & sErrorMsg & "): " & vbCrLf & ex.Message, sPath, , True)
        End Try
    End Sub
    Public Sub ThrowUserError(ByVal sMsg As String, Optional ByVal oIcon As System.Windows.Forms.MessageBoxIcon = MessageBoxIcon.Exclamation)
        MessageBox.Show(sMsg, Application.ProductName, MessageBoxButtons.OK, oIcon)
    End Sub

    Public Sub UserError(ByVal sMsg As String, _
    ByVal oErr As System.Windows.Forms.ErrorProvider, _
    ByVal oControl As System.Windows.Forms.Control)
        oErr.SetError(oControl, sMsg)
    End Sub
    Public Sub _Delay(ByVal nHowMuch As Double)

        Dim nDelay As Int64 = nHowMuch * 1000
        AppStatus(True)
        Application.DoEvents()
        System.Threading.Thread.Sleep(nDelay)
        AppStatus(False)
    End Sub

    Public Sub FormatForWinXP(ByVal oControl As Control)

        On Error Resume Next

        Dim ctrlChild As Control
        Dim oFont As Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Dim Bold As Boolean = False
        Dim myColor As System.Drawing.Color = Color.Navy

        If TypeOf oControl Is Form Then
            oControl.Font = oFont
        End If

        Bold = oControl.Font.Bold

        For Each ctrlChild In oControl.Controls
            If TypeOf ctrlChild Is GroupBox Then
                Dim o As GroupBox
                o = ctrlChild

                If o.Tag <> "3dline" Then

                    o.FlatStyle = FlatStyle.Standard
                    o.ForeColor = myColor
                End If

                o.Font = oFont

            ElseIf TypeOf ctrlChild Is CheckBox Then
                Dim o As CheckBox
                o = ctrlChild
                o.FlatStyle = FlatStyle.Standard
                o.ForeColor = myColor
            ElseIf TypeOf ctrlChild Is RadioButton Then
                Dim o As RadioButton
                o = ctrlChild
                o.FlatStyle = FlatStyle.Standard
                o.ForeColor = myColor
            ElseIf TypeOf ctrlChild Is Button Then
                Dim o As Button
                o = ctrlChild
                o.FlatStyle = FlatStyle.Standard
                o.ForeColor = myColor
                o.BackColor = Color.Transparent

                If o.Text.IndexOf("...") > -1 And o.Image IsNot Nothing And o.Text.Length <= 4 Then
                    o.Text = ""
                    o.ImageAlign = ContentAlignment.MiddleCenter
                End If
            ElseIf TypeOf ctrlChild Is TextBox Then

                Dim o As TextBox

                If o.Tag = "ignore" Then Continue For

                o = ctrlChild
                o.ForeColor = myColor
                o.HideSelection = False

                If o.Tag Is Nothing Then
                    o.MaxLength = 50
                ElseIf o.Tag.ToLower <> "memo" Then
                    o.MaxLength = 50
                End If
            ElseIf TypeOf ctrlChild Is Label Then
                Dim o As Label

                o = ctrlChild
                If o.Font.SizeInPoints < 10 And o.Tag <> "memo" Then
                    o.ForeColor = myColor
                    o.Enabled = True

                    If Bold = True Then
                        o.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                    End If
                End If
            ElseIf TypeOf ctrlChild Is ComboBox Then
                Dim o As ComboBox
                o = ctrlChild
                o.ForeColor = myColor

                If o.Tag <> "memo" Then o.MaxLength = 50

                If o.Text.Length = 0 And o.Tag <> "unsorted" Then o.Sorted = True
            ElseIf TypeOf ctrlChild Is TreeView Then
                Dim o As TreeView
                o = ctrlChild
                o.ForeColor = myColor
            ElseIf TypeOf ctrlChild Is ListView Then
                Dim o As ListView
                o = ctrlChild
                o.ForeColor = myColor
            ElseIf TypeOf ctrlChild Is DividerLabel Then
                Return
            Else
                If Bold = True Then
                    ctrlChild.Font = New System.Drawing.Font("Arial", 8, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
                Else
                    ctrlChild.Font = oFont
                End If
            End If

            Application.DoEvents()

            If ctrlChild.Controls.Count > 0 Then
                FormatForWinXP(ctrlChild)
            End If
        Next
    End Sub

    Private Sub wb_Navigated(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatedEventArgs) Handles wb.Navigated
        wbLoading = False
    End Sub


    Private Sub wb_Navigating(ByVal sender As Object, ByVal e As System.Windows.Forms.WebBrowserNavigatingEventArgs) Handles wb.Navigating
        wbLoading = True
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        Try
            nonPriorityTasks()
        Catch : End Try
    End Sub
End Module
