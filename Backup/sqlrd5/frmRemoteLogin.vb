Public Class frmRemoteLogin
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtUserID As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents chkRemember As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRemoteLogin))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtUserID = New System.Windows.Forms.TextBox
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkRemember = New System.Windows.Forms.CheckBox
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(332, 72)
        Me.Panel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(24, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(160, 32)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Remote Login"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(265, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(64, 50)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(321, 60)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Please enter an Administrator user name and password for the remote system"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 153)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(154, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "User ID"
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(168, 151)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(152, 21)
        Me.txtUserID.TabIndex = 3
        '
        'txtPassword
        '
        Me.txtPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtPassword.Location = New System.Drawing.Point(168, 177)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtPassword.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 179)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Password"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(8, 264)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(56, 23)
        Me.cmdOK.TabIndex = 5
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(72, 264)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(56, 23)
        Me.cmdCancel.TabIndex = 6
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Location = New System.Drawing.Point(-8, 248)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(350, 8)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "3dline"
        '
        'chkRemember
        '
        Me.chkRemember.Location = New System.Drawing.Point(8, 224)
        Me.chkRemember.Name = "chkRemember"
        Me.chkRemember.Size = New System.Drawing.Size(208, 24)
        Me.chkRemember.TabIndex = 9
        Me.chkRemember.Text = "Remember Password"
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 72)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(332, 10)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 11
        '
        'frmRemoteLogin
        '
        Me.AcceptButton = Me.cmdOK
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(332, 296)
        Me.ControlBox = False
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.chkRemember)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUserID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label4)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmRemoteLogin"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login to Remote System"
        Me.Panel1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub frmRemoteLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Close()
    End Sub

    Public Function CheckLogin(ByVal sRemoteCon As String, ByVal RemoteID As Integer, _
    Optional ByVal sUser As String = "", Optional ByVal sPassword As String = "", Optional ByVal unifiedLogin As Boolean = False) As Boolean
        Dim SQL As String
        Dim oRs As New ADODB.Recordset
        Dim Valid As Boolean

        If sRemoteCon.Contains("sqlrdlive.dat") Then
            sRemoteCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sRemoteCon & ";Persist Security Info=False"
        End If

        If unifiedLogin = True Then

            SQL = "SELECT * FROM DomainAttr WHERE DomainName ='" & Environment.UserDomainName & "\" & Environment.UserName & "'"
            Try
                oRs.Open(SQL, sRemoteCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                If oRs.EOF = True Then
                    Valid = False
                Else
                    Valid = True
                End If

                oRs.Close()
            Catch ex As Exception
                Valid = False
            End Try

            If Valid = False Then
                If MessageBox.Show("The currently logged-in user does not have rights to access the remote system." & vbCrLf & _
                "Would you like to log-in using MARS authentication?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    Return False
                End If
            Else
                Return True
            End If
        End If

        If sUser.Length > 0 Then
            txtUserID.Text = sUser
            txtPassword.Text = sPassword
            chkRemember.Checked = True
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return False

        Dim StoredPassword As String

        SQL = "SELECT Password, UserRole FROM CRDUsers WHERE UserID = '" & SQLPrepare(txtUserID.Text) & "'"



        oRs.Open(SQL, sRemoteCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

        If oRs.EOF = False Then
            If txtUserID.Text.ToLower = "sqlrdadmin" Then
                StoredPassword = "SQLRDAdmin"
            Else
                StoredPassword = Decrypt(IsNull(oRs(0).Value), "qwer][po")
            End If

            If StoredPassword = txtPassword.Text Then
                Valid = True
            Else
                Valid = False
            End If
        Else
            Valid = False
        End If

            oRs.Close()

            If Valid = True And chkRemember.Checked = True Then
                SQL = "UPDATE RemoteSysAttr SET UserName ='" & SQLPrepare(Me.txtUserID.Text) & "'," & _
                "UserPassword = '" & SQLPrepare(_EncryptDBValue(txtPassword.Text)) & "' " & _
                "WHERE RemoteID =" & RemoteID

                clsMarsData.WriteData(SQL)
            ElseIf Valid = True And chkRemember.Checked = False Then
                SQL = "UPDATE RemoteSysAttr SET UserName =''," & _
                "UserPassword = '' " & _
                "WHERE RemoteID =" & RemoteID

                clsMarsData.WriteData(SQL)
            End If

            Return Valid
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Public Function AdminCheck() As Boolean
        Me.Text = "Aunthenticate user"
        Me.Label1.Text = "Confirmation"
        Label2.Text = "Please provide an  administrative password to confirm the  system change"
        Me.chkRemember.Visible = False

RETRY:
        Me.ShowDialog()

        If UserCancel = True Then Return False

        Dim oSec As clsMarsSecurity = New clsMarsSecurity

        If oSec._ValidatePassword(Me.txtUserID.Text, Me.txtPassword.Text) = False Then
            MessageBox.Show("The username and password you have provided are invalid. Please try again.", _
            Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)

            GoTo Retry
        Else
            Return True
        End If

    End Function

    Public Sub getLoginDetails(ByRef userName As String, ByRef password As String, ByVal sUrl As String)
        Label2.Text = "The server at '" & sUrl & "' requires a username and password."
        Label3.Text = "Domain\User ID"
        Me.chkRemember.Visible = False

        Me.ShowDialog()

        If UserCancel = True Then
            userName = ""
            password = ""
            Return
        Else
            userName = Me.txtUserID.Text
            password = Me.txtPassword.Text
            Return
        End If

    End Sub
End Class
