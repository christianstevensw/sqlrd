#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmRptOptions
    Inherits System.Windows.Forms.Form
    Dim oData As New clsMarsData
    Dim UserCancel As Boolean = False
    Dim oField As TextBox = txtOwnerPassword
    Dim xFormat As String
    Dim ep As New ErrorProvider
    Dim Dynamic As Boolean = False
    Dim eventBased As Boolean = False
    Dim fields As Hashtable
    Dim m_ReportID As Integer = 0
    Dim m_DestinationID As Integer = 0
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents btnPDF As System.Windows.Forms.Button
    Friend WithEvents grpCSV As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCharacter As System.Windows.Forms.TextBox
    Friend WithEvents optCharacter As System.Windows.Forms.RadioButton
    Friend WithEvents optTab As System.Windows.Forms.RadioButton
    Friend WithEvents txtDelimiter As System.Windows.Forms.TextBox
    Friend WithEvents grpHTML As System.Windows.Forms.GroupBox
    Friend WithEvents optAdvancedHTML As System.Windows.Forms.RadioButton
    Friend WithEvents optBasicHTML As System.Windows.Forms.RadioButton
    Friend WithEvents grpTiff As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rbRLE As System.Windows.Forms.RadioButton
    Friend WithEvents rbNone As System.Windows.Forms.RadioButton
    Friend WithEvents rbLZW As System.Windows.Forms.RadioButton
    Friend WithEvents rbFAX4 As System.Windows.Forms.RadioButton
    Friend WithEvents rbFAX3 As System.Windows.Forms.RadioButton
    Friend WithEvents rbCLASSF196 As System.Windows.Forms.RadioButton
    Friend WithEvents rbCLASSF As System.Windows.Forms.RadioButton
    Friend WithEvents rbMAC As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbColor As System.Windows.Forms.ComboBox
    Friend WithEvents chkGrayScale As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents rbPageRange As System.Windows.Forms.RadioButton
    Friend WithEvents rbAllPages As System.Windows.Forms.RadioButton
    Friend WithEvents numFrom As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents numTo As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkNative As System.Windows.Forms.CheckBox
    Friend WithEvents grpAppendFile As System.Windows.Forms.GroupBox
    Friend WithEvents chkAppendFile As System.Windows.Forms.CheckBox
    Friend WithEvents grpXML As System.Windows.Forms.GroupBox
    Friend WithEvents txtXMLTableName As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents optXMLDiffGram As System.Windows.Forms.RadioButton
    Friend WithEvents optXMLIgnoreSchema As System.Windows.Forms.RadioButton
    Friend WithEvents optXMLIncludeSchema As System.Windows.Forms.RadioButton
    Friend WithEvents chkXMLWriteHierachy As System.Windows.Forms.CheckBox
    Friend WithEvents grpCustom As System.Windows.Forms.GroupBox
    Friend WithEvents txtrenderextension As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Public eventID As Integer = 99999

    Private ReadOnly Property m_preserveLinks() As Boolean
        Get
            If Me.optAdvancedHTML.Checked = True Then
                Return True
            Else
                Return False
            End If
        End Get
    End Property
    Private ReadOnly Property m_CSVChar() As String
        Get
            If Me.optCharacter.Checked = True Then
                Return Me.txtCharacter.Text
            Else
                Return "tab"
            End If
        End Get
    End Property

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents grpPDF As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtWatermark As System.Windows.Forms.TextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents txtOwnerPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUserPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopy As System.Windows.Forms.CheckBox
    Friend WithEvents chkEdit As System.Windows.Forms.CheckBox
    Friend WithEvents chkNotes As System.Windows.Forms.CheckBox
    Friend WithEvents chkFill As System.Windows.Forms.CheckBox
    Friend WithEvents chkAccess As System.Windows.Forms.CheckBox
    Friend WithEvents tip As System.Windows.Forms.ToolTip
    Friend WithEvents chkAssemble As System.Windows.Forms.CheckBox
    Friend WithEvents chkFullRes As System.Windows.Forms.CheckBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents tbOptions As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents grpExcel As System.Windows.Forms.GroupBox
    Friend WithEvents chkPDFSecurity As System.Windows.Forms.CheckBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtWorksheetName As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtInfoTitle As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoAuthor As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoSubject As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoKeywords As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtInfoProducer As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents chkExcelPass As System.Windows.Forms.CheckBox
    Friend WithEvents txtExcelPass As System.Windows.Forms.TextBox
    Friend WithEvents tabExcel As System.Windows.Forms.TabControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents tabPDF As System.Windows.Forms.TabControl
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptOptions))
        Me.grpPDF = New System.Windows.Forms.GroupBox
        Me.tabPDF = New System.Windows.Forms.TabControl
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.btnPDF = New System.Windows.Forms.Button
        Me.chkPDFSecurity = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkCopy = New System.Windows.Forms.CheckBox
        Me.chkEdit = New System.Windows.Forms.CheckBox
        Me.chkNotes = New System.Windows.Forms.CheckBox
        Me.chkFill = New System.Windows.Forms.CheckBox
        Me.chkAccess = New System.Windows.Forms.CheckBox
        Me.chkAssemble = New System.Windows.Forms.CheckBox
        Me.chkFullRes = New System.Windows.Forms.CheckBox
        Me.txtOwnerPassword = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.mnuSpell = New System.Windows.Forms.MenuItem
        Me.txtUserPassword = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtWatermark = New System.Windows.Forms.TextBox
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.cmdOK = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.tip = New System.Windows.Forms.ToolTip(Me.components)
        Me.tbOptions = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.grpCSV = New System.Windows.Forms.GroupBox
        Me.grpAppendFile = New System.Windows.Forms.GroupBox
        Me.chkAppendFile = New System.Windows.Forms.CheckBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.txtCharacter = New System.Windows.Forms.TextBox
        Me.optCharacter = New System.Windows.Forms.RadioButton
        Me.optTab = New System.Windows.Forms.RadioButton
        Me.txtDelimiter = New System.Windows.Forms.TextBox
        Me.grpExcel = New System.Windows.Forms.GroupBox
        Me.tabExcel = New System.Windows.Forms.TabControl
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.chkExcelPass = New System.Windows.Forms.CheckBox
        Me.txtWorksheetName = New System.Windows.Forms.TextBox
        Me.txtExcelPass = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.grpXML = New System.Windows.Forms.GroupBox
        Me.txtXMLTableName = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.optXMLDiffGram = New System.Windows.Forms.RadioButton
        Me.optXMLIgnoreSchema = New System.Windows.Forms.RadioButton
        Me.optXMLIncludeSchema = New System.Windows.Forms.RadioButton
        Me.chkXMLWriteHierachy = New System.Windows.Forms.CheckBox
        Me.grpTiff = New System.Windows.Forms.GroupBox
        Me.chkNative = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.lblFrom = New System.Windows.Forms.Label
        Me.numTo = New System.Windows.Forms.NumericUpDown
        Me.numFrom = New System.Windows.Forms.NumericUpDown
        Me.rbPageRange = New System.Windows.Forms.RadioButton
        Me.rbAllPages = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.chkGrayScale = New System.Windows.Forms.CheckBox
        Me.cmbColor = New System.Windows.Forms.ComboBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.rbCLASSF196 = New System.Windows.Forms.RadioButton
        Me.rbCLASSF = New System.Windows.Forms.RadioButton
        Me.rbMAC = New System.Windows.Forms.RadioButton
        Me.rbLZW = New System.Windows.Forms.RadioButton
        Me.rbFAX4 = New System.Windows.Forms.RadioButton
        Me.rbFAX3 = New System.Windows.Forms.RadioButton
        Me.rbRLE = New System.Windows.Forms.RadioButton
        Me.rbNone = New System.Windows.Forms.RadioButton
        Me.grpHTML = New System.Windows.Forms.GroupBox
        Me.optAdvancedHTML = New System.Windows.Forms.RadioButton
        Me.optBasicHTML = New System.Windows.Forms.RadioButton
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtInfoTitle = New System.Windows.Forms.TextBox
        Me.txtInfoAuthor = New System.Windows.Forms.TextBox
        Me.txtInfoSubject = New System.Windows.Forms.TextBox
        Me.txtInfoKeywords = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtInfoProducer = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.grpCustom = New System.Windows.Forms.GroupBox
        Me.txtrenderextension = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.grpPDF.SuspendLayout()
        Me.tabPDF.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tbOptions.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.grpCSV.SuspendLayout()
        Me.grpAppendFile.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.grpExcel.SuspendLayout()
        Me.tabExcel.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.grpXML.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.grpTiff.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.numTo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numFrom, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.grpHTML.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.grpCustom.SuspendLayout()
        Me.SuspendLayout()
        '
        'grpPDF
        '
        Me.grpPDF.Controls.Add(Me.tabPDF)
        Me.grpPDF.Location = New System.Drawing.Point(8, 8)
        Me.grpPDF.Name = "grpPDF"
        Me.grpPDF.Size = New System.Drawing.Size(392, 320)
        Me.grpPDF.TabIndex = 0
        Me.grpPDF.TabStop = False
        Me.grpPDF.Text = "PDF Options"
        Me.grpPDF.Visible = False
        '
        'tabPDF
        '
        Me.tabPDF.Controls.Add(Me.TabPage6)
        Me.tabPDF.Location = New System.Drawing.Point(8, 16)
        Me.tabPDF.Name = "tabPDF"
        Me.tabPDF.SelectedIndex = 0
        Me.tabPDF.Size = New System.Drawing.Size(376, 296)
        Me.tabPDF.TabIndex = 3
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.btnPDF)
        Me.TabPage6.Controls.Add(Me.chkPDFSecurity)
        Me.TabPage6.Controls.Add(Me.GroupBox1)
        Me.TabPage6.Controls.Add(Me.Label2)
        Me.TabPage6.Controls.Add(Me.txtWatermark)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(368, 270)
        Me.TabPage6.TabIndex = 0
        Me.TabPage6.Text = "PDF Security"
        '
        'btnPDF
        '
        Me.btnPDF.Location = New System.Drawing.Point(303, 232)
        Me.btnPDF.Name = "btnPDF"
        Me.btnPDF.Size = New System.Drawing.Size(43, 23)
        Me.btnPDF.TabIndex = 3
        Me.btnPDF.Text = "..."
        Me.btnPDF.UseVisualStyleBackColor = True
        '
        'chkPDFSecurity
        '
        Me.chkPDFSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPDFSecurity.Location = New System.Drawing.Point(8, 8)
        Me.chkPDFSecurity.Name = "chkPDFSecurity"
        Me.chkPDFSecurity.Size = New System.Drawing.Size(336, 24)
        Me.chkPDFSecurity.TabIndex = 0
        Me.chkPDFSecurity.Text = "Enable PDF Options"
        Me.tip.SetToolTip(Me.chkPDFSecurity, "Output can only be read using Adobe Acrobat 5 and above")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox9)
        Me.GroupBox1.Controls.Add(Me.chkPrint)
        Me.GroupBox1.Controls.Add(Me.chkCopy)
        Me.GroupBox1.Controls.Add(Me.chkEdit)
        Me.GroupBox1.Controls.Add(Me.chkNotes)
        Me.GroupBox1.Controls.Add(Me.chkFill)
        Me.GroupBox1.Controls.Add(Me.chkAccess)
        Me.GroupBox1.Controls.Add(Me.chkAssemble)
        Me.GroupBox1.Controls.Add(Me.chkFullRes)
        Me.GroupBox1.Controls.Add(Me.txtOwnerPassword)
        Me.GroupBox1.Controls.Add(Me.txtUserPassword)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Enabled = False
        Me.GroupBox1.Location = New System.Drawing.Point(8, 32)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 176)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "File Permissions"
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(8, 64)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(336, 10)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Tag = "3dline"
        '
        'chkPrint
        '
        Me.chkPrint.Checked = True
        Me.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPrint.Location = New System.Drawing.Point(8, 72)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(88, 24)
        Me.chkPrint.TabIndex = 2
        Me.chkPrint.Text = "Can Print"
        Me.tip.SetToolTip(Me.chkPrint, "Allow the user to print the document")
        '
        'chkCopy
        '
        Me.chkCopy.Checked = True
        Me.chkCopy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCopy.Location = New System.Drawing.Point(8, 96)
        Me.chkCopy.Name = "chkCopy"
        Me.chkCopy.Size = New System.Drawing.Size(88, 24)
        Me.chkCopy.TabIndex = 2
        Me.chkCopy.Text = "Can Copy"
        Me.tip.SetToolTip(Me.chkCopy, "Allows the user to copy text and images from the document")
        '
        'chkEdit
        '
        Me.chkEdit.Checked = True
        Me.chkEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEdit.Location = New System.Drawing.Point(8, 120)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(80, 24)
        Me.chkEdit.TabIndex = 3
        Me.chkEdit.Text = "Can Edit"
        Me.tip.SetToolTip(Me.chkEdit, "Allows the user to edit the document")
        '
        'chkNotes
        '
        Me.chkNotes.Checked = True
        Me.chkNotes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNotes.Location = New System.Drawing.Point(168, 120)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(104, 24)
        Me.chkNotes.TabIndex = 6
        Me.chkNotes.Text = "Can Add Notes"
        Me.tip.SetToolTip(Me.chkNotes, "Allows the user to add annotations")
        '
        'chkFill
        '
        Me.chkFill.Checked = True
        Me.chkFill.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFill.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFill.Location = New System.Drawing.Point(8, 144)
        Me.chkFill.Name = "chkFill"
        Me.chkFill.Size = New System.Drawing.Size(96, 24)
        Me.chkFill.TabIndex = 4
        Me.chkFill.Text = "Can Fill Fields"
        Me.tip.SetToolTip(Me.chkFill, "Allows the user to fill in form fields")
        '
        'chkAccess
        '
        Me.chkAccess.Checked = True
        Me.chkAccess.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAccess.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAccess.Location = New System.Drawing.Point(168, 144)
        Me.chkAccess.Name = "chkAccess"
        Me.chkAccess.Size = New System.Drawing.Size(176, 24)
        Me.chkAccess.TabIndex = 7
        Me.chkAccess.Text = "Can copy accessibility options"
        Me.tip.SetToolTip(Me.chkAccess, "Allow user copying for use with accessibility features")
        '
        'chkAssemble
        '
        Me.chkAssemble.Checked = True
        Me.chkAssemble.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssemble.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssemble.Location = New System.Drawing.Point(168, 72)
        Me.chkAssemble.Name = "chkAssemble"
        Me.chkAssemble.Size = New System.Drawing.Size(96, 24)
        Me.chkAssemble.TabIndex = 6
        Me.chkAssemble.Text = "Can Assemble"
        Me.tip.SetToolTip(Me.chkAssemble, "Allow the user to assemble the document")
        '
        'chkFullRes
        '
        Me.chkFullRes.Checked = True
        Me.chkFullRes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFullRes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFullRes.Location = New System.Drawing.Point(168, 96)
        Me.chkFullRes.Name = "chkFullRes"
        Me.chkFullRes.Size = New System.Drawing.Size(152, 24)
        Me.chkFullRes.TabIndex = 5
        Me.chkFullRes.Text = "Can print in full resolution"
        Me.tip.SetToolTip(Me.chkFullRes, "Allow the user print in full resolution")
        '
        'txtOwnerPassword
        '
        Me.txtOwnerPassword.ContextMenu = Me.mnuInserter
        Me.txtOwnerPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOwnerPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtOwnerPassword.Name = "txtOwnerPassword"
        Me.txtOwnerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOwnerPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtOwnerPassword.TabIndex = 0
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem13, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 10
        Me.MenuItem13.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'txtUserPassword
        '
        Me.txtUserPassword.ContextMenu = Me.mnuInserter
        Me.txtUserPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtUserPassword.Location = New System.Drawing.Point(168, 40)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtUserPassword.Size = New System.Drawing.Size(152, 21)
        Me.txtUserPassword.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(168, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User Password"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Owner Password"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 216)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 16)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Include the following watermark/stamp"
        '
        'txtWatermark
        '
        Me.txtWatermark.ContextMenu = Me.mnuInserter
        Me.txtWatermark.Location = New System.Drawing.Point(8, 232)
        Me.txtWatermark.Name = "txtWatermark"
        Me.txtWatermark.Size = New System.Drawing.Size(289, 21)
        Me.txtWatermark.TabIndex = 2
        '
        'ofd
        '
        Me.ofd.Filter = "JPEG Files|*.jpg|GIF Files|*.gif|Windows Bitmap|*.bmp|All Files|*.*"
        Me.ofd.Title = "Select image file"
        '
        'cmdOK
        '
        Me.cmdOK.BackColor = System.Drawing.SystemColors.Control
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(270, 378)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 0
        Me.cmdOK.Text = "&OK"
        Me.cmdOK.UseVisualStyleBackColor = False
        '
        'cmdCancel
        '
        Me.cmdCancel.BackColor = System.Drawing.SystemColors.Control
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(350, 378)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 1
        Me.cmdCancel.Text = "&Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = False
        '
        'tbOptions
        '
        Me.tbOptions.Controls.Add(Me.TabPage1)
        Me.tbOptions.Controls.Add(Me.TabPage3)
        Me.tbOptions.ItemSize = New System.Drawing.Size(86, 18)
        Me.tbOptions.Location = New System.Drawing.Point(8, 8)
        Me.tbOptions.Name = "tbOptions"
        Me.tbOptions.SelectedIndex = 0
        Me.tbOptions.Size = New System.Drawing.Size(417, 364)
        Me.tbOptions.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grpCustom)
        Me.TabPage1.Controls.Add(Me.grpCSV)
        Me.TabPage1.Controls.Add(Me.grpExcel)
        Me.TabPage1.Controls.Add(Me.grpXML)
        Me.TabPage1.Controls.Add(Me.grpPDF)
        Me.TabPage1.Controls.Add(Me.grpTiff)
        Me.TabPage1.Controls.Add(Me.grpHTML)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(409, 338)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Format Options"
        '
        'grpCSV
        '
        Me.grpCSV.Controls.Add(Me.grpAppendFile)
        Me.grpCSV.Controls.Add(Me.Label6)
        Me.grpCSV.Controls.Add(Me.GroupBox7)
        Me.grpCSV.Controls.Add(Me.txtDelimiter)
        Me.grpCSV.Location = New System.Drawing.Point(8, 8)
        Me.grpCSV.Name = "grpCSV"
        Me.grpCSV.Size = New System.Drawing.Size(392, 279)
        Me.grpCSV.TabIndex = 5
        Me.grpCSV.TabStop = False
        Me.grpCSV.Text = "CSV Options"
        Me.grpCSV.Visible = False
        '
        'grpAppendFile
        '
        Me.grpAppendFile.Controls.Add(Me.chkAppendFile)
        Me.grpAppendFile.Location = New System.Drawing.Point(8, 154)
        Me.grpAppendFile.Name = "grpAppendFile"
        Me.grpAppendFile.Size = New System.Drawing.Size(376, 60)
        Me.grpAppendFile.TabIndex = 4
        Me.grpAppendFile.TabStop = False
        Me.grpAppendFile.Text = "Append to existing file"
        '
        'chkAppendFile
        '
        Me.chkAppendFile.AutoSize = True
        Me.chkAppendFile.Location = New System.Drawing.Point(11, 26)
        Me.chkAppendFile.Name = "chkAppendFile"
        Me.chkAppendFile.Size = New System.Drawing.Size(278, 17)
        Me.chkAppendFile.TabIndex = 1
        Me.chkAppendFile.Text = "Append to file if one already exists (disk destination)"
        Me.chkAppendFile.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(16, 112)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 16)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Delimiter"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txtCharacter)
        Me.GroupBox7.Controls.Add(Me.optCharacter)
        Me.GroupBox7.Controls.Add(Me.optTab)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(376, 80)
        Me.GroupBox7.TabIndex = 0
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Separator"
        '
        'txtCharacter
        '
        Me.txtCharacter.Location = New System.Drawing.Point(136, 16)
        Me.txtCharacter.Name = "txtCharacter"
        Me.txtCharacter.Size = New System.Drawing.Size(72, 21)
        Me.txtCharacter.TabIndex = 1
        Me.txtCharacter.Text = ","
        '
        'optCharacter
        '
        Me.optCharacter.Checked = True
        Me.optCharacter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCharacter.Location = New System.Drawing.Point(8, 16)
        Me.optCharacter.Name = "optCharacter"
        Me.optCharacter.Size = New System.Drawing.Size(104, 24)
        Me.optCharacter.TabIndex = 0
        Me.optCharacter.TabStop = True
        Me.optCharacter.Text = "Character"
        '
        'optTab
        '
        Me.optTab.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optTab.Location = New System.Drawing.Point(8, 48)
        Me.optTab.Name = "optTab"
        Me.optTab.Size = New System.Drawing.Size(104, 24)
        Me.optTab.TabIndex = 0
        Me.optTab.Text = "Tab"
        '
        'txtDelimiter
        '
        Me.txtDelimiter.Location = New System.Drawing.Point(144, 104)
        Me.txtDelimiter.Name = "txtDelimiter"
        Me.txtDelimiter.Size = New System.Drawing.Size(72, 21)
        Me.txtDelimiter.TabIndex = 1
        Me.txtDelimiter.Text = """"
        '
        'grpExcel
        '
        Me.grpExcel.Controls.Add(Me.tabExcel)
        Me.grpExcel.Location = New System.Drawing.Point(8, 8)
        Me.grpExcel.Name = "grpExcel"
        Me.grpExcel.Size = New System.Drawing.Size(392, 320)
        Me.grpExcel.TabIndex = 2
        Me.grpExcel.TabStop = False
        Me.grpExcel.Text = "Excel Options (requires MS Office)"
        Me.grpExcel.Visible = False
        '
        'tabExcel
        '
        Me.tabExcel.Controls.Add(Me.TabPage4)
        Me.tabExcel.ItemSize = New System.Drawing.Size(49, 18)
        Me.tabExcel.Location = New System.Drawing.Point(8, 24)
        Me.tabExcel.Name = "tabExcel"
        Me.tabExcel.SelectedIndex = 0
        Me.tabExcel.Size = New System.Drawing.Size(368, 290)
        Me.tabExcel.TabIndex = 6
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox10)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(360, 264)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "General"
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.chkExcelPass)
        Me.GroupBox10.Controls.Add(Me.txtWorksheetName)
        Me.GroupBox10.Controls.Add(Me.txtExcelPass)
        Me.GroupBox10.Controls.Add(Me.Label10)
        Me.GroupBox10.Location = New System.Drawing.Point(3, 7)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(349, 252)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Workbook Name && Password"
        '
        'chkExcelPass
        '
        Me.chkExcelPass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkExcelPass.Location = New System.Drawing.Point(5, 65)
        Me.chkExcelPass.Name = "chkExcelPass"
        Me.chkExcelPass.Size = New System.Drawing.Size(184, 24)
        Me.chkExcelPass.TabIndex = 1
        Me.chkExcelPass.Text = "Password protect  the workbook"
        '
        'txtWorksheetName
        '
        Me.txtWorksheetName.ContextMenu = Me.mnuInserter
        Me.txtWorksheetName.Location = New System.Drawing.Point(5, 38)
        Me.txtWorksheetName.MaxLength = 30
        Me.txtWorksheetName.Name = "txtWorksheetName"
        Me.txtWorksheetName.Size = New System.Drawing.Size(299, 21)
        Me.txtWorksheetName.TabIndex = 0
        Me.txtWorksheetName.Tag = "memo"
        '
        'txtExcelPass
        '
        Me.txtExcelPass.ContextMenu = Me.mnuInserter
        Me.txtExcelPass.Enabled = False
        Me.txtExcelPass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtExcelPass.Location = New System.Drawing.Point(5, 89)
        Me.txtExcelPass.Name = "txtExcelPass"
        Me.txtExcelPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtExcelPass.Size = New System.Drawing.Size(296, 21)
        Me.txtExcelPass.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(2, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(267, 13)
        Me.Label10.TabIndex = 2
        Me.Label10.Text = "Worksheet Name (leave blank for default i.e.  Sheet1)"
        '
        'grpXML
        '
        Me.grpXML.Controls.Add(Me.txtXMLTableName)
        Me.grpXML.Controls.Add(Me.Label24)
        Me.grpXML.Controls.Add(Me.GroupBox13)
        Me.grpXML.Controls.Add(Me.chkXMLWriteHierachy)
        Me.grpXML.Location = New System.Drawing.Point(8, 8)
        Me.grpXML.Name = "grpXML"
        Me.grpXML.Size = New System.Drawing.Size(392, 209)
        Me.grpXML.TabIndex = 7
        Me.grpXML.TabStop = False
        Me.grpXML.Text = "XML Options"
        Me.grpXML.Visible = False
        '
        'txtXMLTableName
        '
        Me.txtXMLTableName.Location = New System.Drawing.Point(17, 176)
        Me.txtXMLTableName.Name = "txtXMLTableName"
        Me.txtXMLTableName.Size = New System.Drawing.Size(367, 21)
        Me.txtXMLTableName.TabIndex = 3
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(15, 161)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(140, 13)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Data Table Name (Optional)"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.optXMLDiffGram)
        Me.GroupBox13.Controls.Add(Me.optXMLIgnoreSchema)
        Me.GroupBox13.Controls.Add(Me.optXMLIncludeSchema)
        Me.GroupBox13.Location = New System.Drawing.Point(15, 54)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(369, 100)
        Me.GroupBox13.TabIndex = 1
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "XML Mode"
        '
        'optXMLDiffGram
        '
        Me.optXMLDiffGram.AutoSize = True
        Me.optXMLDiffGram.Location = New System.Drawing.Point(15, 66)
        Me.optXMLDiffGram.Name = "optXMLDiffGram"
        Me.optXMLDiffGram.Size = New System.Drawing.Size(88, 17)
        Me.optXMLDiffGram.TabIndex = 0
        Me.optXMLDiffGram.TabStop = True
        Me.optXMLDiffGram.Tag = "2"
        Me.optXMLDiffGram.Text = "Use DiffGram"
        Me.optXMLDiffGram.UseVisualStyleBackColor = True
        '
        'optXMLIgnoreSchema
        '
        Me.optXMLIgnoreSchema.AutoSize = True
        Me.optXMLIgnoreSchema.Location = New System.Drawing.Point(16, 43)
        Me.optXMLIgnoreSchema.Name = "optXMLIgnoreSchema"
        Me.optXMLIgnoreSchema.Size = New System.Drawing.Size(97, 17)
        Me.optXMLIgnoreSchema.TabIndex = 0
        Me.optXMLIgnoreSchema.TabStop = True
        Me.optXMLIgnoreSchema.Tag = "1"
        Me.optXMLIgnoreSchema.Text = "Ignore Schema"
        Me.optXMLIgnoreSchema.UseVisualStyleBackColor = True
        '
        'optXMLIncludeSchema
        '
        Me.optXMLIncludeSchema.AutoSize = True
        Me.optXMLIncludeSchema.Location = New System.Drawing.Point(16, 20)
        Me.optXMLIncludeSchema.Name = "optXMLIncludeSchema"
        Me.optXMLIncludeSchema.Size = New System.Drawing.Size(100, 17)
        Me.optXMLIncludeSchema.TabIndex = 0
        Me.optXMLIncludeSchema.TabStop = True
        Me.optXMLIncludeSchema.Tag = "0"
        Me.optXMLIncludeSchema.Text = "Include Schema"
        Me.optXMLIncludeSchema.UseVisualStyleBackColor = True
        '
        'chkXMLWriteHierachy
        '
        Me.chkXMLWriteHierachy.AutoSize = True
        Me.chkXMLWriteHierachy.Location = New System.Drawing.Point(15, 24)
        Me.chkXMLWriteHierachy.Name = "chkXMLWriteHierachy"
        Me.chkXMLWriteHierachy.Size = New System.Drawing.Size(97, 17)
        Me.chkXMLWriteHierachy.TabIndex = 0
        Me.chkXMLWriteHierachy.Text = "Write Hierachy"
        Me.chkXMLWriteHierachy.UseVisualStyleBackColor = True
        '
        'grpTiff
        '
        Me.grpTiff.Controls.Add(Me.chkNative)
        Me.grpTiff.Controls.Add(Me.GroupBox5)
        Me.grpTiff.Controls.Add(Me.GroupBox4)
        Me.grpTiff.Controls.Add(Me.GroupBox3)
        Me.grpTiff.Location = New System.Drawing.Point(8, 7)
        Me.grpTiff.Name = "grpTiff"
        Me.grpTiff.Size = New System.Drawing.Size(392, 320)
        Me.grpTiff.TabIndex = 6
        Me.grpTiff.TabStop = False
        Me.grpTiff.Visible = False
        '
        'chkNative
        '
        Me.chkNative.AutoSize = True
        Me.chkNative.Checked = True
        Me.chkNative.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNative.Location = New System.Drawing.Point(8, 13)
        Me.chkNative.Name = "chkNative"
        Me.chkNative.Size = New System.Drawing.Size(205, 17)
        Me.chkNative.TabIndex = 0
        Me.chkNative.Text = "Use Reporting Services native format"
        Me.chkNative.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.Label4)
        Me.GroupBox5.Controls.Add(Me.lblFrom)
        Me.GroupBox5.Controls.Add(Me.numTo)
        Me.GroupBox5.Controls.Add(Me.numFrom)
        Me.GroupBox5.Controls.Add(Me.rbPageRange)
        Me.GroupBox5.Controls.Add(Me.rbAllPages)
        Me.GroupBox5.Location = New System.Drawing.Point(186, 196)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(200, 117)
        Me.GroupBox5.TabIndex = 3
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Page Range"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Enabled = False
        Me.Label4.Location = New System.Drawing.Point(112, 82)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(17, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "to"
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.Enabled = False
        Me.lblFrom.Location = New System.Drawing.Point(24, 82)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(31, 13)
        Me.lblFrom.TabIndex = 4
        Me.lblFrom.Text = "From"
        '
        'numTo
        '
        Me.numTo.Enabled = False
        Me.numTo.Location = New System.Drawing.Point(135, 78)
        Me.numTo.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.numTo.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numTo.Name = "numTo"
        Me.numTo.Size = New System.Drawing.Size(53, 21)
        Me.numTo.TabIndex = 3
        Me.numTo.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'numFrom
        '
        Me.numFrom.Enabled = False
        Me.numFrom.Location = New System.Drawing.Point(56, 78)
        Me.numFrom.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.numFrom.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.numFrom.Name = "numFrom"
        Me.numFrom.Size = New System.Drawing.Size(51, 21)
        Me.numFrom.TabIndex = 2
        Me.numFrom.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'rbPageRange
        '
        Me.rbPageRange.AutoSize = True
        Me.rbPageRange.Location = New System.Drawing.Point(7, 53)
        Me.rbPageRange.Name = "rbPageRange"
        Me.rbPageRange.Size = New System.Drawing.Size(83, 17)
        Me.rbPageRange.TabIndex = 1
        Me.rbPageRange.Text = "Page Range"
        Me.rbPageRange.UseVisualStyleBackColor = True
        '
        'rbAllPages
        '
        Me.rbAllPages.AutoSize = True
        Me.rbAllPages.Checked = True
        Me.rbAllPages.Location = New System.Drawing.Point(7, 29)
        Me.rbAllPages.Name = "rbAllPages"
        Me.rbAllPages.Size = New System.Drawing.Size(68, 17)
        Me.rbAllPages.TabIndex = 0
        Me.rbAllPages.TabStop = True
        Me.rbAllPages.Text = "All Pages"
        Me.rbAllPages.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.chkGrayScale)
        Me.GroupBox4.Controls.Add(Me.cmbColor)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 196)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(174, 117)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Color Depth"
        '
        'chkGrayScale
        '
        Me.chkGrayScale.AutoSize = True
        Me.chkGrayScale.Enabled = False
        Me.chkGrayScale.Location = New System.Drawing.Point(14, 67)
        Me.chkGrayScale.Name = "chkGrayScale"
        Me.chkGrayScale.Size = New System.Drawing.Size(152, 17)
        Me.chkGrayScale.TabIndex = 1
        Me.chkGrayScale.Text = "Convert to 8-bit grayscale"
        Me.chkGrayScale.UseVisualStyleBackColor = True
        '
        'cmbColor
        '
        Me.cmbColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColor.FormattingEnabled = True
        Me.cmbColor.Items.AddRange(New Object() {"Black and White", "256 colors", "24-bit color"})
        Me.cmbColor.Location = New System.Drawing.Point(12, 31)
        Me.cmbColor.MaxDropDownItems = 3
        Me.cmbColor.Name = "cmbColor"
        Me.cmbColor.Size = New System.Drawing.Size(148, 21)
        Me.cmbColor.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rbCLASSF196)
        Me.GroupBox3.Controls.Add(Me.rbCLASSF)
        Me.GroupBox3.Controls.Add(Me.rbMAC)
        Me.GroupBox3.Controls.Add(Me.rbLZW)
        Me.GroupBox3.Controls.Add(Me.rbFAX4)
        Me.GroupBox3.Controls.Add(Me.rbFAX3)
        Me.GroupBox3.Controls.Add(Me.rbRLE)
        Me.GroupBox3.Controls.Add(Me.rbNone)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 34)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(380, 159)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Compression"
        '
        'rbCLASSF196
        '
        Me.rbCLASSF196.AutoSize = True
        Me.rbCLASSF196.Location = New System.Drawing.Point(11, 125)
        Me.rbCLASSF196.Name = "rbCLASSF196"
        Me.rbCLASSF196.Size = New System.Drawing.Size(264, 17)
        Me.rbCLASSF196.TabIndex = 7
        Me.rbCLASSF196.Text = "204x196 ClassF TIFF file, Fax machine compatible"
        Me.rbCLASSF196.UseVisualStyleBackColor = True
        '
        'rbCLASSF
        '
        Me.rbCLASSF.AutoSize = True
        Me.rbCLASSF.Location = New System.Drawing.Point(11, 102)
        Me.rbCLASSF.Name = "rbCLASSF"
        Me.rbCLASSF.Size = New System.Drawing.Size(258, 17)
        Me.rbCLASSF.TabIndex = 6
        Me.rbCLASSF.Text = "204x98 ClassF TIFF file, Fax machine compatible"
        Me.rbCLASSF.UseVisualStyleBackColor = True
        '
        'rbMAC
        '
        Me.rbMAC.AutoSize = True
        Me.rbMAC.Location = New System.Drawing.Point(11, 72)
        Me.rbMAC.Name = "rbMAC"
        Me.rbMAC.Size = New System.Drawing.Size(94, 17)
        Me.rbMAC.TabIndex = 2
        Me.rbMAC.Text = "Macintosh RLE"
        Me.rbMAC.UseVisualStyleBackColor = True
        '
        'rbLZW
        '
        Me.rbLZW.AutoSize = True
        Me.rbLZW.Location = New System.Drawing.Point(11, 50)
        Me.rbLZW.Name = "rbLZW"
        Me.rbLZW.Size = New System.Drawing.Size(150, 17)
        Me.rbLZW.TabIndex = 1
        Me.rbLZW.Text = "Lempel-Ziv && Welch (LZW)"
        Me.rbLZW.UseVisualStyleBackColor = True
        '
        'rbFAX4
        '
        Me.rbFAX4.AutoSize = True
        Me.rbFAX4.Location = New System.Drawing.Point(209, 72)
        Me.rbFAX4.Name = "rbFAX4"
        Me.rbFAX4.Size = New System.Drawing.Size(161, 17)
        Me.rbFAX4.TabIndex = 5
        Me.rbFAX4.Text = "CCITT Group 4 fax encoding"
        Me.rbFAX4.UseVisualStyleBackColor = True
        '
        'rbFAX3
        '
        Me.rbFAX3.AutoSize = True
        Me.rbFAX3.Location = New System.Drawing.Point(209, 50)
        Me.rbFAX3.Name = "rbFAX3"
        Me.rbFAX3.Size = New System.Drawing.Size(161, 17)
        Me.rbFAX3.TabIndex = 4
        Me.rbFAX3.Text = "CCITT Group 3 fax encoding"
        Me.rbFAX3.UseVisualStyleBackColor = True
        '
        'rbRLE
        '
        Me.rbRLE.AutoSize = True
        Me.rbRLE.Location = New System.Drawing.Point(209, 25)
        Me.rbRLE.Name = "rbRLE"
        Me.rbRLE.Size = New System.Drawing.Size(163, 17)
        Me.rbRLE.TabIndex = 3
        Me.rbRLE.Text = "CCITT modified Huffman RLE"
        Me.rbRLE.UseVisualStyleBackColor = True
        '
        'rbNone
        '
        Me.rbNone.AutoSize = True
        Me.rbNone.Checked = True
        Me.rbNone.Location = New System.Drawing.Point(11, 25)
        Me.rbNone.Name = "rbNone"
        Me.rbNone.Size = New System.Drawing.Size(137, 17)
        Me.rbNone.TabIndex = 0
        Me.rbNone.TabStop = True
        Me.rbNone.Text = "Uncompressed TIFF file"
        Me.rbNone.UseVisualStyleBackColor = True
        '
        'grpHTML
        '
        Me.grpHTML.Controls.Add(Me.optAdvancedHTML)
        Me.grpHTML.Controls.Add(Me.optBasicHTML)
        Me.grpHTML.Location = New System.Drawing.Point(8, 8)
        Me.grpHTML.Name = "grpHTML"
        Me.grpHTML.Size = New System.Drawing.Size(392, 96)
        Me.grpHTML.TabIndex = 3
        Me.grpHTML.TabStop = False
        Me.grpHTML.Text = "HTML Options"
        Me.grpHTML.Visible = False
        '
        'optAdvancedHTML
        '
        Me.optAdvancedHTML.AutoSize = True
        Me.optAdvancedHTML.Checked = True
        Me.optAdvancedHTML.Location = New System.Drawing.Point(28, 24)
        Me.optAdvancedHTML.Name = "optAdvancedHTML"
        Me.optAdvancedHTML.Size = New System.Drawing.Size(171, 17)
        Me.SuperTooltip1.SetSuperTooltip(Me.optAdvancedHTML, New DevComponents.DotNetBar.SuperTooltipInfo("Advanced HTML Rendering", "", "This HTML rendering method produces outputs that are closer in representation to " & _
                    "the original report and also preserves the links that you have in the report ", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, True, New System.Drawing.Size(250, 124)))
        Me.optAdvancedHTML.TabIndex = 0
        Me.optAdvancedHTML.TabStop = True
        Me.optAdvancedHTML.Text = "Use advanced HTML rendering"
        Me.optAdvancedHTML.UseVisualStyleBackColor = True
        '
        'optBasicHTML
        '
        Me.optBasicHTML.AutoSize = True
        Me.optBasicHTML.Location = New System.Drawing.Point(28, 47)
        Me.optBasicHTML.Name = "optBasicHTML"
        Me.optBasicHTML.Size = New System.Drawing.Size(148, 17)
        Me.optBasicHTML.TabIndex = 0
        Me.optBasicHTML.Text = "Use basic HTML rendering"
        Me.optBasicHTML.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(408, 332)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "File Summary"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox2.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox2.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox2.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.txtInfoProducer)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(392, 320)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Enabled = False
        Me.txtInfoCreated.Location = New System.Drawing.Point(136, 120)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        Me.txtInfoTitle.ContextMenu = Me.mnuInserter
        Me.txtInfoTitle.Location = New System.Drawing.Point(136, 24)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        Me.txtInfoAuthor.ContextMenu = Me.mnuInserter
        Me.txtInfoAuthor.Location = New System.Drawing.Point(136, 48)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        Me.txtInfoSubject.ContextMenu = Me.mnuInserter
        Me.txtInfoSubject.Location = New System.Drawing.Point(136, 96)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        Me.txtInfoKeywords.ContextMenu = Me.mnuInserter
        Me.txtInfoKeywords.Location = New System.Drawing.Point(136, 144)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(232, 168)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 72)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        '
        'Label14
        '
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 144)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        Me.txtInfoProducer.ContextMenu = Me.mnuInserter
        Me.txtInfoProducer.Location = New System.Drawing.Point(136, 72)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoProducer.TabIndex = 20
        '
        'Label16
        '
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = False
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = Nothing
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'grpCustom
        '
        Me.grpCustom.Controls.Add(Me.Label5)
        Me.grpCustom.Controls.Add(Me.txtrenderextension)
        Me.grpCustom.Location = New System.Drawing.Point(8, 8)
        Me.grpCustom.Name = "grpCustom"
        Me.grpCustom.Size = New System.Drawing.Size(392, 75)
        Me.grpCustom.TabIndex = 3
        Me.grpCustom.TabStop = False
        Me.grpCustom.Text = "Custom Format Options"
        Me.grpCustom.Visible = False
        '
        'txtrenderextension
        '
        Me.txtrenderextension.Location = New System.Drawing.Point(15, 45)
        Me.txtrenderextension.Name = "txtrenderextension"
        Me.txtrenderextension.Size = New System.Drawing.Size(309, 21)
        Me.txtrenderextension.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(15, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(253, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Custom Rendering Extension Name (e.g. PDF, TXT)"
        '
        'frmRptOptions
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(432, 409)
        Me.ControlBox = False
        Me.Controls.Add(Me.tbOptions)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frmRptOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Report Options"
        Me.grpPDF.ResumeLayout(False)
        Me.tabPDF.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tbOptions.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.grpCSV.ResumeLayout(False)
        Me.grpCSV.PerformLayout()
        Me.grpAppendFile.ResumeLayout(False)
        Me.grpAppendFile.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.grpExcel.ResumeLayout(False)
        Me.tabExcel.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.grpXML.ResumeLayout(False)
        Me.grpXML.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.grpTiff.ResumeLayout(False)
        Me.grpTiff.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.numTo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numFrom, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.grpHTML.ResumeLayout(False)
        Me.grpHTML.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.grpCustom.ResumeLayout(False)
        Me.grpCustom.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
    '    ofd.ShowDialog()

    '    If ofd.FileName.Length = 0 Then Return

    '    txtWatermark.Text = ofd.FileName
    'End Sub

    Private Sub frmRptOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub PackageOptions(ByVal sFormat As String, Optional ByVal nPackID As Integer = 99999)
        Dim SQL As String
        Dim oRs As ADODB.Recordset

        SQL = "SELECT  * FROM PackageOptions WHERE PackID = " & nPackID

        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                If Convert.ToBoolean(oRs("pdfsecurity").Value) = True Then
                    chkPDFSecurity.Checked = True
                    txtOwnerPassword.Text = IsNull(_DecryptDBValue(oRs("pdfpassword").Value))
                    txtUserPassword.Text = IsNull(_DecryptDBValue(oRs("userpassword").Value))
                    txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                    chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                    chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                    chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                    chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                    chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                    chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                    chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                    chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)

                    txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                    txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                    txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                    txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                    txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                    Try
                        txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                    Catch ex As Exception
                        txtInfoCreated.Value = Now
                    End Try
                End If
            End If

            oRs.Close()
        Catch : End Try

        If sFormat = "PDF" Then
            grpPDF.Visible = True
            grpPDF.BringToFront()

            tbOptions.TabPages(1).Enabled = True
        ElseIf sFormat = "XLS" Then
            grpExcel.Visible = True
            grpExcel.BringToFront()

            tbOptions.TabPages(1).Enabled = True
            tabExcel.TabPages(0).Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If

        Dim sCols As String
        Dim sVals As String

        clsMarsData.WriteData("DELETE FROM PackageOptions WHERE PackID =" & nPackID)

        sCols = "OptionID,PackID,PDFSecurity,PDFPassword,UserPassword,PDFWatermark,CanPrint,CanCopy,CanEdit," & _
        "CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated,InfoProducer,ProtectExcel,ExcelPassword"


        sVals = clsMarsData.CreateDataID("packageoptions", "optionid") & "," & _
        nPackID & "," & _
        Convert.ToInt32(chkPDFSecurity.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtUserPassword.Text)) & "'," & _
        "'" & SQLPrepare(txtWatermark.Text) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        "'" & SQLPrepare(txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(txtInfoProducer.Text) & "'," & _
        Convert.ToInt32(chkExcelPass.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtExcelPass.Text)) & "'"

        SQL = "INSERT INTO PackageOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Public Sub ReportOptions(ByVal sFormat As String, ByVal ReportType As String, _
    Optional ByVal nReportID As Integer = 99999, _
    Optional ByVal nDestinationID As Integer = 99999, Optional ByVal IsDynamic As Boolean = False, _
    Optional ByVal WorkSheetName As String = "")

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim sReportColumn As String

        xFormat = sFormat.ToLower.Trim

        Dynamic = IsDynamic


        If ReportType = "Single" Then
            SQL = "SELECT * FROM ReportOptions WHERE DestinationID = " & nDestinationID
            nReportID = 0
        Else
            SQL = "SELECT * FROM ReportOptions WHERE ReportID =" & nReportID
            nDestinationID = 0
        End If

        Me.m_ReportID = nReportID
        Me.m_DestinationID = nDestinationID


        oRs = clsMarsData.GetData(SQL)

        Try
            If oRs.EOF = False Then
                chkPDFSecurity.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(oRs("pdfsecurity").Value, 0)))
                txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                chkAssemble.Checked = Convert.ToBoolean(oRs("canassemble").Value)
                chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)
                txtWorksheetName.Text = IsNull(oRs("worksheetname").Value)

                '//custom rendering extension
                Try
                    Me.txtrenderextension.Text = IsNull(oRs("rendername").Value, "")
                Catch : End Try

                'ExcelShowGrid -> chkPreserve (for HTML preserving links
                If IsNull(oRs("excelshowgrid").Value, 0) = 1 Then
                    Me.optAdvancedHTML.Checked = True
                Else
                    Me.optBasicHTML.Checked = True
                End If

                If IsNull(oRs("scharacter").Value, "") = "tab" Then
                    optTab.Checked = True
                Else
                    optCharacter.Checked = True
                    txtCharacter.Text = IsNull(oRs("scharacter").Value, ",")
                End If

                txtDelimiter.Text = IsNull(oRs("sdelimiter").Value, "")

                Try
                    chkExcelPass.Checked = Convert.ToBoolean(oRs("protectexcel").Value)
                    txtExcelPass.Text = _DecryptDBValue(oRs("excelpassword").Value)
                Catch : End Try

                'for pdf and excel properties
                txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)
                txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)

                Try
                    txtInfoCreated.Value = CType(oRs("infocreated").Value, Date)
                Catch ex As Exception
                    txtInfoCreated.Value = Now
                End Try

                'for tif properties
                If IsNull(oRs("tcompression").Value) = "0" Or IsNull(oRs("tcompression").Value) = "" Or IsNull(oRs("tcompression").Value) = "Native" Then
                    chkNative.Checked = True
                Else
                    chkNative.Checked = False
                End If
                If IsNull(oRs("tcompression").Value) <> "0" And IsNull(oRs("tcompression").Value) <> "" Then
                    If IsNull(oRs("tcompression").Value) = "None" Then rbNone.Checked = True
                    If IsNull(oRs("tcompression").Value) = "LZW" Then rbLZW.Checked = True
                    If IsNull(oRs("tcompression").Value) = "MAC" Then rbMAC.Checked = True
                    If IsNull(oRs("tcompression").Value) = "RLE" Then rbRLE.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX3" Then rbFAX3.Checked = True
                    If IsNull(oRs("tcompression").Value) = "FAX4" Then rbFAX4.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF" Then rbCLASSF.Checked = True
                    If IsNull(oRs("tcompression").Value) = "CLASSF196" Then rbCLASSF196.Checked = True
                    If IsNull(oRs("tcompression").Value) = "Native" Then chkNative.Checked = True

                    cmbColor.Text = IsNull(oRs("tcolour").Value)
                    If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"

                    If cmbColor.Text = "Black and White" Then
                        chkGrayScale.Checked = Convert.ToBoolean(oRs("tgrey").Value)
                    Else
                        chkGrayScale.Checked = False
                    End If

                    rbPageRange.Checked = Convert.ToBoolean(oRs("tpage").Value)
                    If rbPageRange.Checked = False Then
                        numFrom.Value = 1
                        numTo.Value = 1
                    Else
                        numFrom.Value = oRs("pagefrom").Value
                        numTo.Value = oRs("pageto").Value
                        If numFrom.Value = -1 Then numFrom.Value = 1
                        If numTo.Value = -1 Then numTo.Value = 1
                    End If
                End If

                Try
                    Me.chkAppendFile.Checked = IsNull(oRs("appendtofile").Value, 0)
                Catch : End Try

            Else
                If WorkSheetName.Length > 25 Then
                    txtWorksheetName.Text = WorkSheetName.Substring(0, 25)
                    ep.SetError(txtWorksheetName, "The worksheet name has been truncated to 25 characters")
                Else
                    txtWorksheetName.Text = WorkSheetName
                End If

            End If



            oRs.Close()
        Catch : End Try

        Select Case sFormat.ToLower.Trim
            Case "acrobat format"
                grpPDF.Visible = True
                grpPDF.BringToFront()
                tbOptions.TabPages(1).Enabled = True
                Label13.Text = "Producer"
                txtInfoCreated.Enabled = True
            Case "ms excel - data only"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                tbOptions.TabPages(1).Enabled = True
                Label13.Text = "Company"
            Case "ms excel 97-2000"
                grpExcel.Visible = True
                grpExcel.BringToFront()
                tbOptions.TabPages(1).Enabled = True
                Label13.Text = "Company"
            Case "ms word"
                tbOptions.TabPages(0).Enabled = False
                tbOptions.SelectedTab = tbOptions.TabPages(1)
                tbOptions.TabPages(1).Enabled = True
                Label13.Text = "Company"
            Case "csv"
                tbOptions.TabPages(0).Enabled = True
                tbOptions.SelectedTab = tbOptions.TabPages(0)
                tbOptions.TabPages(1).Enabled = False
                grpCSV.Visible = True
                grpCSV.BringToFront()
            Case "html"
                tbOptions.TabPages(0).Enabled = True
                tbOptions.SelectedTab = tbOptions.TabPages(0)
                tbOptions.TabPages(1).Enabled = False
                grpHTML.Visible = True
                grpHTML.BringToFront()
            Case "tiff"
                tbOptions.TabPages(0).Enabled = True
                tbOptions.SelectedTab = tbOptions.TabPages(0)
                tbOptions.TabPages(1).Enabled = False
                grpTiff.Visible = True
                grpTiff.BringToFront()
                If cmbColor.Text = "" Then cmbColor.Text = "24-bit color"
            Case "custom"
                tbOptions.TabPages(0).Enabled = True
                tbOptions.SelectedTab = tbOptions.TabPages(0)
                tbOptions.TabPages(1).Enabled = False
                Me.grpCustom.Visible = True
                grpCustom.BringToFront()
        End Select

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If


        If ReportType = "Single" Then
            SQL = "DELETE FROM ReportOptions WHERE DestinationID =" & nDestinationID
        Else
            SQL = "DELETE FROM ReportOptions WHERE ReportID =" & nReportID
        End If

        clsMarsData.WriteData(SQL)

        Dim sCols As String
        Dim sVals As String

        sReportColumn = ""

        Dim sCharacter As String

        sCharacter = ""

        'Tiff Options
        Dim sCompression As String
        If rbNone.Checked = True Then
            sCompression = "None"
        ElseIf rbLZW.Checked = True Then
            sCompression = "LZW"
        ElseIf rbMAC.Checked = True Then
            sCompression = "MAC"
        ElseIf rbRLE.Checked = True Then
            sCompression = "RLE"
        ElseIf rbFAX3.Checked = True Then
            sCompression = "FAX3"
        ElseIf rbFAX4.Checked = True Then
            sCompression = "FAX4"
        ElseIf rbCLASSF.Checked = True Then
            sCompression = "CLASSF"
        ElseIf rbCLASSF196.Checked = True Then
            sCompression = "CLASSF196"
        Else : sCompression = "0"
        End If
        If chkNative.Checked = True Then
            sCompression = "Native"
        End If

        If sCompression = "0" Then cmbColor.Text = "0"
        Dim sFrom As Integer
        Dim sTo As Integer
        If rbPageRange.Checked = True Then
            sFrom = numFrom.Value
            sTo = numTo.Value
        Else
            sFrom = -1
            sTo = -1
        End If

        sCols = "OptionID, PDFWaterMark,PDFPassword,DestinationID,UserPassword,CanPrint," & _
        "CanCopy,CanEdit,CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull,ReportID,HTMLMerge," & _
        "ColWidth,ExportHF,PageBreak,ConvertDate,PageFrom,PageTo,sCharacter,sDelimiter,ODBC,TableName," & _
        "LinesPerPage,PDFSecurity,HTMLNavigator,WorkSheetName," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer,ExcelBursting,ExcelBurstColumn," & _
        "ProtectExcel,ExcelPassword,KeepNoFormats,KeepDateFormats,PDFBookmarks,CharPerInch," & _
        "ExcelShowGrid,ExcelPHPF,TCompression,TColour,TGrey,TPage,AppendtoFile,RenderName"

        Dim nID As Integer = clsMarsData.CreateDataID("reportoptions", "reportid")

        sVals = nID & "," & _
        "'" & SQLPrepare(txtWatermark.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(txtOwnerPassword.Text)) & "'," & _
        nDestinationID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtUserPassword.Text)) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        nReportID & "," & _
        0 & "," & _
        "'" & 0 & "'," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        sFrom & "," & _
        sTo & "," & _
        "'" & SQLPrepare(m_CSVChar) & "'," & _
        "'" & SQLPrepare(Me.txtDelimiter.Text) & "'," & _
        "''," & _
        "''," & _
        0 & "," & _
        Convert.ToInt32(Me.chkPDFSecurity.Checked) & "," & _
        0 & "," & _
        "'" & SQLPrepare(Microsoft.VisualBasic.Left(txtWorksheetName.Text, 50)) & "'," & _
        "'" & SQLPrepare(txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(txtInfoProducer.Text) & "'," & _
        0 & "," & _
        "'" & SQLPrepare(sReportColumn) & "'," & _
        Convert.ToInt32(chkExcelPass.Checked) & "," & _
        "'" & SQLPrepare(_EncryptDBValue(txtExcelPass.Text)) & "'," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        0 & "," & _
        Convert.ToInt32(Me.m_preserveLinks) & "," & _
        "'0'" & "," & _
        "'" & sCompression & "'," & _
        "'" & SQLPrepare(cmbColor.Text) & "'," & _
        Convert.ToInt32(chkGrayScale.Checked) & "," & _
        Convert.ToInt32(rbPageRange.Checked) & "," & _
        Convert.ToInt32(Me.chkAppendFile.Checked) & "," & _
        "'" & SQLPrepare(Me.txtrenderextension.Text) & "'"

        SQL = "INSERT INTO ReportOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)
    End Sub

    Public Sub taskOptions(ByVal format As String, ByVal destinationID As Integer)

        TabPage3.Enabled = False

        Select Case format.ToLower
            Case "ms excel 97-2000"
                grpExcel.Visible = True
                grpExcel.BringToFront()
            Case "xml"
                grpXML.Visible = True
                grpXML.BringToFront()
            Case "csv"
                grpCSV.Visible = True
                grpCSV.BringToFront()
                Me.grpAppendFile.Visible = False
        End Select

        Dim SQL As String = "SELECT * FROM ReportOptions WHERE destinationID = " & destinationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs IsNot Nothing AndAlso oRs.EOF = False Then
            '//csv
            Me.txtDelimiter.Text = IsNull(oRs("sdelimiter").Value, Chr(34))

            If IsNull(oRs("scharacter").Value, "") = "tab" Then
                optTab.Checked = True
            Else
                optCharacter.Checked = True
                txtCharacter.Text = IsNull(oRs("scharacter").Value, ",")
            End If

            '//excel
            Me.txtWorksheetName.Text = IsNull(oRs("worksheetname").Value, "")
            Me.txtExcelPass.Text = _DecryptDBValue(IsNull(oRs("excelpassword").Value, ""))

            Try
                chkExcelPass.Checked = Convert.ToBoolean(oRs("protectexcel").Value)
            Catch : End Try

            '//XML
            Select Case IsNull(oRs("xmlmode").Value, 0)
                Case 0
                    Me.optXMLIncludeSchema.Checked = True
                Case 1
                    Me.optXMLIgnoreSchema.Checked = True
                Case 2
                    Me.optXMLDiffGram.Checked = True
            End Select

            Me.chkXMLWriteHierachy.Checked = IsNull(oRs("XMLWriteHierachy").Value, 0)
            Me.txtXMLTableName.Text = IsNull(oRs("xmltablename").Value, "")
        End If

        Me.ShowDialog()

        If UserCancel = True Then Return

        clsMarsData.WriteData("DELETE FROM ReportOptions WHERE destinationID =" & destinationID)

        Dim cols, vals As String
        Dim optionID As Integer = clsMarsData.CreateDataID("reportoptions", "optionid")
        Dim xmlWriteMode As Integer = 0
        Dim sep As String = Me.txtCharacter.Text

        If Me.optTab.Checked Then sep = "tab"

        If Me.optXMLIncludeSchema.Checked Then
            xmlWriteMode = 0
        ElseIf Me.optXMLIgnoreSchema.Checked Then
            xmlWriteMode = 1
        ElseIf Me.optXMLDiffGram.Checked Then
            xmlWriteMode = 2
        End If


        cols = "optionid,destinationid,sdelimiter,scharacter,worksheetname,excelpassword,protectexcel,xmlmode,xmlwritehierachy,xmltablename"

        vals = optionID & "," & _
        destinationID & "," & _
        "'" & SQLPrepare(Me.txtDelimiter.Text) & "'," & _
        "'" & SQLPrepare(sep) & "'," & _
        "'" & SQLPrepare(Me.txtWorksheetName.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtExcelPass.Text)) & "'," & _
        Convert.ToInt32(Me.chkExcelPass.Checked) & "," & _
        xmlWriteMode & "," & _
        Convert.ToInt32(Me.chkXMLWriteHierachy.Checked) & "," & _
        "'" & SQLPrepare(Me.txtXMLTableName.Text) & "'"

        clsMarsData.DataItem.InsertData("ReportOptions", cols, vals, True)

    End Sub
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        'lets validate user input
        Select Case xFormat
            Case "ms excel - data only", "ms excel 97-2000"
                If chkExcelPass.Checked And txtExcelPass.Text.Length = 0 Then
                    ep.SetError(txtExcelPass, "Please provide the workbook password")
                    txtExcelPass.Focus()
                    Return
                End If
        End Select

        Close()
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPDFSecurity.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkPDFSecurity.Checked = False
            Return
        End If

        GroupBox1.Enabled = chkPDFSecurity.Checked

    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        oField.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        oField.SelectAll()
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(eventID)
        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.eventID
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click

        On Error Resume Next

        If Dynamic = False Then
            Dim oItem As New frmDataItems

            oField.SelectedText = oItem._GetDataItem(Me.eventID)
        Else
            Dim oIntruder As New frmInserter(0)

            oIntruder.GetDatabaseField(gTables, gsCon, Me)
        End If

    End Sub

    Private Sub txtOwnerPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOwnerPassword.TextChanged

    End Sub

    Private Sub txtOwnerPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtOwnerPassword.GotFocus
        oField = txtOwnerPassword
    End Sub

    Private Sub txtUserPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUserPassword.TextChanged

    End Sub

    Private Sub txtUserPassword_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUserPassword.GotFocus
        oField = txtUserPassword
    End Sub



    Private Sub txtWatermark_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWatermark.GotFocus
        oField = txtWatermark
    End Sub

    Private Sub txtWorksheetName_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWorksheetName.GotFocus
        oField = txtWorksheetName
    End Sub

    Private Sub txtInfoTitle_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoTitle.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoAuthor_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoAuthor.GotFocus
        oField = sender
    End Sub


    Private Sub txtInfoProducer_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoProducer.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoSubject_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoSubject.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoKeywords_GotFocus(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoKeywords.GotFocus
        oField = sender
    End Sub

    Private Sub txtInfoTitle_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtInfoTitle.TextChanged, txtInfoAuthor.TextChanged, txtInfoKeywords.TextChanged, txtInfoProducer.TextChanged, txtInfoSubject.TextChanged, txtInfoTitle.TextChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            sender.text = String.Empty
            Return
        End If
    End Sub


    Private Sub chkExcelPass_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExcelPass.CheckedChanged
        If chkExcelPass.Checked = True And gnEdition < MarsGlobal.gEdition.ENTERPRISEPROPLUS Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkExcelPass.Checked = False
            Return
        End If

        txtExcelPass.Enabled = chkExcelPass.Checked
    End Sub


    Private Sub txtWorksheetName_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtWorksheetName.KeyPress

        If (Char.IsLetterOrDigit(e.KeyChar) = False) And e.KeyChar <> Chr(8) And _
        e.KeyChar <> Chr(32) Then
            e.Handled = True
        End If
    End Sub

    Private Sub txtWorksheetName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWorksheetName.TextChanged
        ep.SetError(sender, "")
    End Sub


    Private Sub btnPDF_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPDF.Click
        Dim pdfStamper As frmPDFStamp = New frmPDFStamp

        With pdfStamper
            .m_Dynamic = Me.Dynamic
            .m_eventBased = Me.eventBased

            If Me.m_ReportID <> 0 Then
                txtWatermark.Text = .AddStampDtls(m_ReportID, txtWatermark.Text)
            Else
                txtWatermark.Text = .AddStampDtls(m_DestinationID, txtWatermark.Text)
            End If
        End With

    End Sub

    Private Sub txtWatermark_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtWatermark.TextChanged
        If Me.txtWatermark.Text.Length > 0 And IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.pd1_AdvancedPDFPack) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS)
            Me.txtWatermark.Text = String.Empty

        End If
    End Sub

    
    Private Sub cmbColor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColor.SelectedIndexChanged
        If cmbColor.Text = "Black and White" Then
            chkGrayScale.Enabled = True
        Else
            chkGrayScale.Checked = False
            chkGrayScale.Enabled = False
        End If
    End Sub

    Private Sub rbPageRange_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbPageRange.CheckedChanged
        If rbPageRange.Checked = True Then
            lblFrom.Enabled = True
            Label4.Enabled = True
            numFrom.Enabled = True
            numTo.Enabled = True
            rbAllPages.Checked = False
        End If
        If rbPageRange.Checked = False Then
            lblFrom.Enabled = False
            Label4.Enabled = False
            numFrom.Enabled = False
            numTo.Enabled = False
            rbPageRange.Checked = False
        End If
    End Sub

    Private Sub numFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles numFrom.ValueChanged
        numTo.Minimum = numFrom.Value
    End Sub

   
    Private Sub chkNative_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNative.CheckedChanged
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        Else
            GroupBox3.Enabled = True
            GroupBox4.Enabled = True
            GroupBox5.Enabled = True
        End If
    End Sub

    Private Sub rbCLASSF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCLASSF.CheckedChanged
        If rbCLASSF.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbCLASSF196_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCLASSF196.CheckedChanged
        If rbCLASSF196.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbRLE_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbRLE.CheckedChanged
        If rbRLE.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbFAX3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbFAX3.CheckedChanged
        If rbFAX3.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbFAX4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbFAX4.CheckedChanged
        If rbFAX4.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbNone_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbNone.CheckedChanged
        If rbNone.Checked = True Then
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbLZW_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbLZW.CheckedChanged
        If rbLZW.Checked = True Then
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub rbMAC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbMAC.CheckedChanged
        If rbMAC.Checked = True Then
            GroupBox4.Enabled = True
        End If
        If chkNative.Checked = True Then
            GroupBox3.Enabled = False
            GroupBox4.Enabled = False
            GroupBox5.Enabled = False
        End If
    End Sub

    Private Sub GroupBox4_EnabledChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox4.EnabledChanged
        If rbRLE.Checked = True Or rbFAX3.Checked = True Or rbFAX4.Checked = True Or rbCLASSF.Checked = True Or Me.rbCLASSF196.Checked = True Then
            cmbColor.Text = "Black and White"
            chkGrayScale.Checked = True
            GroupBox4.Enabled = False
        End If
        If chkNative.Checked = True Then GroupBox4.Enabled = False

    End Sub
    'Private Sub cmbColor_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColor.SelectedIndexChanged
    '    If cmbColor.Text <> "Black and White" Then
    '        chkGrayScale.Checked = False
    '    Else
    '        chkGrayScale.Checked = True
    '    End If
    'End Sub
End Class
