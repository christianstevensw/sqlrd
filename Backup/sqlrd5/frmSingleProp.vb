Imports Microsoft.Win32

#If CRYSTAL_VER = 8 Then
imports My.Crystal85
#ElseIf CRYSTAL_VER = 9 Then
imports My.Crystal9 
#ElseIf CRYSTAL_VER = 10 Then
imports My.Crystal10 
#ElseIf CRYSTAL_VER = 11 Then
Imports My.Crystal11
#End If

Public Class frmSingleProp
    Inherits System.Windows.Forms.Form
    Dim ReportRestore() As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdDbLoc As System.Windows.Forms.Button
    Friend WithEvents txtDBLoc As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdParameters As System.Windows.Forms.Button
    Friend WithEvents lsvParameters As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtID As System.Windows.Forms.TextBox
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtFormula As System.Windows.Forms.TextBox
    Friend WithEvents imgTools As System.Windows.Forms.ImageList
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmbTable As System.Windows.Forms.ComboBox
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents cmbColumn As System.Windows.Forms.ComboBox
    Friend WithEvents cmbValue As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents ucUpdate As sqlrd.ucScheduleUpdate
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents optStatic As System.Windows.Forms.RadioButton
    Friend WithEvents optDynamic As System.Windows.Forms.RadioButton
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents imgSingle As System.Windows.Forms.ImageList
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkSnapshots As System.Windows.Forms.CheckBox
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader4 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdView As System.Windows.Forms.Button
    Friend WithEvents cmdDelete As System.Windows.Forms.Button
    Friend WithEvents cmdExecute As System.Windows.Forms.Button
    Friend WithEvents lsvSnapshots As System.Windows.Forms.ListView
    Friend WithEvents imgSnap As System.Windows.Forms.ImageList
    Friend WithEvents cmdRequery As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdClear As System.Windows.Forms.Button
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents cmdTest As System.Windows.Forms.Button
    'Friend WithEvents UcBurst As sqlrd.ucBursting
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents chkStaticDest As System.Windows.Forms.CheckBox
    Friend WithEvents btnView As System.Windows.Forms.Button
    Friend WithEvents tabProperties As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbReport As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbOptions As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbOutput As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbHistory As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbExceptions As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbTasks As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbLinking As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSnapshots As DevComponents.DotNetBar.TabItem
    Friend WithEvents grpDynamicTasks As System.Windows.Forms.GroupBox
    Friend WithEvents optOnce As System.Windows.Forms.RadioButton
    Friend WithEvents optAll As System.Windows.Forms.RadioButton
    Friend WithEvents txtUrl As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents mnuDatasources As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ClearToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tvHistory As System.Windows.Forms.TreeView
    Friend WithEvents grSort As System.Windows.Forms.GroupBox
    Friend WithEvents optAsc As System.Windows.Forms.RadioButton
    Friend WithEvents optDesc As System.Windows.Forms.RadioButton
    Friend WithEvents btnRefreshDS As System.Windows.Forms.Button
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmdTest2 As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents bgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents TabControlPanel11 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbDataDriven As DevComponents.DotNetBar.TabItem
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents btnConnect As System.Windows.Forms.Button
    Friend WithEvents UcDSNDD As sqlrd.ucDSN
    Friend WithEvents chkGroupReports As System.Windows.Forms.CheckBox
    Friend WithEvents grpQuery As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuild As System.Windows.Forms.Button
    Friend WithEvents txtQuery As System.Windows.Forms.TextBox
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Friend WithEvents grpAutoResume As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoResume As System.Windows.Forms.CheckBox
    Friend WithEvents txtCacheExpiry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents btnClear As System.Windows.Forms.Button
    Friend WithEvents cmbDDKey As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cmdTestLink As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSingleProp))
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdDbLoc = New System.Windows.Forms.Button
        Me.txtDBLoc = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdRequery = New System.Windows.Forms.Button
        Me.cmdParameters = New System.Windows.Forms.Button
        Me.lsvParameters = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.txtFormula = New System.Windows.Forms.TextBox
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtID = New System.Windows.Forms.TextBox
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdClear = New System.Windows.Forms.Button
        Me.cmdRefresh = New System.Windows.Forms.Button
        Me.imgSingle = New System.Windows.Forms.ImageList(Me.components)
        Me.btnView = New System.Windows.Forms.Button
        Me.chkStaticDest = New System.Windows.Forms.CheckBox
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnClear = New System.Windows.Forms.Button
        Me.Label21 = New System.Windows.Forms.Label
        Me.cmbValue = New System.Windows.Forms.ComboBox
        Me.cmbColumn = New System.Windows.Forms.ComboBox
        Me.cmbTable = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmdTestLink = New System.Windows.Forms.Button
        Me.optStatic = New System.Windows.Forms.RadioButton
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.optDynamic = New System.Windows.Forms.RadioButton
        Me.cmdView = New System.Windows.Forms.Button
        Me.lsvSnapshots = New System.Windows.Forms.ListView
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.imgSnap = New System.Windows.Forms.ImageList(Me.components)
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown
        Me.chkSnapshots = New System.Windows.Forms.CheckBox
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.cmdExecute = New System.Windows.Forms.Button
        Me.cmdTest = New System.Windows.Forms.Button
        Me.imgTools = New System.Windows.Forms.ImageList(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnRefreshDS = New System.Windows.Forms.Button
        Me.tabProperties = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel11 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkGroupReports = New System.Windows.Forms.CheckBox
        Me.grpQuery = New System.Windows.Forms.GroupBox
        Me.cmbDDKey = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.btnBuild = New System.Windows.Forms.Button
        Me.txtQuery = New System.Windows.Forms.TextBox
        Me.btnConnect = New System.Windows.Forms.Button
        Me.UcDSNDD = New sqlrd.ucDSN
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbDataDriven = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.txtUrl = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbGeneral = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpAutoResume = New System.Windows.Forms.GroupBox
        Me.chkAutoResume = New System.Windows.Forms.CheckBox
        Me.txtCacheExpiry = New System.Windows.Forms.NumericUpDown
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmdTest2 = New System.Windows.Forms.Button
        Me.tbReport = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbSnapshots = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpDynamicTasks = New System.Windows.Forms.GroupBox
        Me.optOnce = New System.Windows.Forms.RadioButton
        Me.optAll = New System.Windows.Forms.RadioButton
        Me.oTask = New sqlrd.ucTasks
        Me.tbTasks = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel
        Me.tbLinking = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcDest = New sqlrd.ucDestination
        Me.tbOutput = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lsvDatasources = New System.Windows.Forms.ListView
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.mnuDatasources = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ClearToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.tbOptions = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.tbExceptions = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.ucUpdate = New sqlrd.ucScheduleUpdate
        Me.tbSchedule = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.tvHistory = New System.Windows.Forms.TreeView
        Me.grSort = New System.Windows.Forms.GroupBox
        Me.optAsc = New System.Windows.Forms.RadioButton
        Me.optDesc = New System.Windows.Forms.RadioButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tbHistory = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.bgWorker = New System.ComponentModel.BackgroundWorker
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabProperties.SuspendLayout()
        Me.TabControlPanel11.SuspendLayout()
        Me.grpQuery.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.grpAutoResume.SuspendLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel10.SuspendLayout()
        Me.TabControlPanel8.SuspendLayout()
        Me.grpDynamicTasks.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.mnuDatasources.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.grSort.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtDesc
        '
        Me.HelpProvider1.SetHelpKeyword(Me.txtDesc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDesc.Location = New System.Drawing.Point(4, 204)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.HelpProvider1.SetShowHelp(Me.txtDesc, True)
        Me.txtDesc.Size = New System.Drawing.Size(376, 156)
        Me.txtDesc.TabIndex = 6
        Me.txtDesc.Tag = "memo"
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label7, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label7, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(4, 188)
        Me.Label7.Name = "Label7"
        Me.HelpProvider1.SetShowHelp(Me.Label7, True)
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Description (optional)"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label9, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label9, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(4, 365)
        Me.Label9.Name = "Label9"
        Me.HelpProvider1.SetShowHelp(Me.Label9, True)
        Me.Label9.Size = New System.Drawing.Size(208, 16)
        Me.Label9.TabIndex = 29
        Me.Label9.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtKeyWord, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtKeyWord, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtKeyWord.Location = New System.Drawing.Point(4, 381)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.HelpProvider1.SetShowHelp(Me.txtKeyWord, True)
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 7
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdDbLoc
        '
        Me.cmdDbLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDbLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDbLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDbLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDbLoc.Image = CType(resources.GetObject("cmdDbLoc.Image"), System.Drawing.Image)
        Me.cmdDbLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDbLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDbLoc.Location = New System.Drawing.Point(322, 158)
        Me.cmdDbLoc.Name = "cmdDbLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdDbLoc, True)
        Me.cmdDbLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDbLoc.TabIndex = 5
        Me.cmdDbLoc.Text = "..."
        Me.cmdDbLoc.UseVisualStyleBackColor = False
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtDBLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtDBLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtDBLoc.Location = New System.Drawing.Point(4, 158)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtDBLoc, True)
        Me.txtDBLoc.Size = New System.Drawing.Size(288, 21)
        Me.txtDBLoc.TabIndex = 4
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdLoc, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdLoc, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(324, 67)
        Me.cmdLoc.Name = "cmdLoc"
        Me.HelpProvider1.SetShowHelp(Me.cmdLoc, True)
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtFolder, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtFolder, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtFolder.Location = New System.Drawing.Point(4, 67)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.HelpProvider1.SetShowHelp(Me.txtFolder, True)
        Me.txtFolder.Size = New System.Drawing.Size(288, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label4, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(4, 51)
        Me.Label4.Name = "Label4"
        Me.HelpProvider1.SetShowHelp(Me.Label4, True)
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 18
        Me.Label4.Text = "Create In"
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label3, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(4, 142)
        Me.Label3.Name = "Label3"
        Me.HelpProvider1.SetShowHelp(Me.Label3, True)
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Report Location"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtName, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(4, 27)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label2, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 11)
        Me.Label2.Name = "Label2"
        Me.HelpProvider1.SetShowHelp(Me.Label2, True)
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Schedule Name"
        '
        'cmdRequery
        '
        Me.cmdRequery.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRequery, "Report.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRequery, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRequery.Image = CType(resources.GetObject("cmdRequery.Image"), System.Drawing.Image)
        Me.cmdRequery.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRequery.Location = New System.Drawing.Point(374, 292)
        Me.cmdRequery.Name = "cmdRequery"
        Me.HelpProvider1.SetShowHelp(Me.cmdRequery, True)
        Me.cmdRequery.Size = New System.Drawing.Size(40, 23)
        Me.cmdRequery.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.cmdRequery, "Requery the report file for parameters")
        '
        'cmdParameters
        '
        Me.cmdParameters.BackColor = System.Drawing.SystemColors.Control
        Me.cmdParameters.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdParameters.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.cmdParameters, "Report.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdParameters, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdParameters.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdParameters.Location = New System.Drawing.Point(9, 292)
        Me.cmdParameters.Name = "cmdParameters"
        Me.HelpProvider1.SetShowHelp(Me.cmdParameters, True)
        Me.cmdParameters.Size = New System.Drawing.Size(75, 23)
        Me.cmdParameters.TabIndex = 2
        Me.cmdParameters.Text = "Parameters"
        Me.cmdParameters.UseVisualStyleBackColor = False
        '
        'lsvParameters
        '
        Me.lsvParameters.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader7})
        Me.lsvParameters.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.lsvParameters.ForeColor = System.Drawing.Color.Blue
        Me.lsvParameters.FullRowSelect = True
        Me.lsvParameters.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvParameters, "Report.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvParameters, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvParameters.Location = New System.Drawing.Point(7, 10)
        Me.lsvParameters.Name = "lsvParameters"
        Me.HelpProvider1.SetShowHelp(Me.lsvParameters, True)
        Me.lsvParameters.Size = New System.Drawing.Size(484, 270)
        Me.lsvParameters.TabIndex = 1
        Me.lsvParameters.UseCompatibleStateImageBehavior = False
        Me.lsvParameters.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Parameter Name"
        Me.ColumnHeader1.Width = 147
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Parameter Value"
        Me.ColumnHeader2.Width = 179
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "Multi-Value"
        Me.ColumnHeader7.Width = 70
        '
        'txtFormula
        '
        Me.txtFormula.Location = New System.Drawing.Point(77, 361)
        Me.txtFormula.Name = "txtFormula"
        Me.txtFormula.Size = New System.Drawing.Size(24, 21)
        Me.txtFormula.TabIndex = 25
        Me.txtFormula.Visible = False
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(554, 416)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 49
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(466, 416)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(378, 416)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtID
        '
        Me.txtID.Location = New System.Drawing.Point(45, 361)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(24, 21)
        Me.txtID.TabIndex = 10
        Me.txtID.Visible = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdClear
        '
        Me.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdClear, "History.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdClear, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdClear.Image = CType(resources.GetObject("cmdClear.Image"), System.Drawing.Image)
        Me.cmdClear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClear.Location = New System.Drawing.Point(3, 3)
        Me.cmdClear.Name = "cmdClear"
        Me.HelpProvider1.SetShowHelp(Me.cmdClear, True)
        Me.cmdClear.Size = New System.Drawing.Size(75, 23)
        Me.cmdClear.TabIndex = 0
        Me.cmdClear.Text = "Clear"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdRefresh, "History.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdRefresh, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdRefresh.Image = CType(resources.GetObject("cmdRefresh.Image"), System.Drawing.Image)
        Me.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRefresh.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRefresh.Location = New System.Drawing.Point(86, 3)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.HelpProvider1.SetShowHelp(Me.cmdRefresh, True)
        Me.cmdRefresh.Size = New System.Drawing.Size(75, 23)
        Me.cmdRefresh.TabIndex = 1
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'imgSingle
        '
        Me.imgSingle.ImageStream = CType(resources.GetObject("imgSingle.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgSingle.TransparentColor = System.Drawing.Color.Transparent
        Me.imgSingle.Images.SetKeyName(0, "")
        Me.imgSingle.Images.SetKeyName(1, "")
        Me.imgSingle.Images.SetKeyName(2, "")
        Me.imgSingle.Images.SetKeyName(3, "")
        Me.imgSingle.Images.SetKeyName(4, "")
        Me.imgSingle.Images.SetKeyName(5, "")
        '
        'btnView
        '
        Me.btnView.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.btnView, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnView, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnView.Location = New System.Drawing.Point(386, 41)
        Me.btnView.Name = "btnView"
        Me.HelpProvider1.SetShowHelp(Me.btnView, True)
        Me.btnView.Size = New System.Drawing.Size(37, 16)
        Me.btnView.TabIndex = 4
        Me.btnView.Text = "..."
        '
        'chkStaticDest
        '
        Me.chkStaticDest.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkStaticDest, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkStaticDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkStaticDest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkStaticDest.Location = New System.Drawing.Point(14, 76)
        Me.chkStaticDest.Name = "chkStaticDest"
        Me.HelpProvider1.SetShowHelp(Me.chkStaticDest, True)
        Me.chkStaticDest.Size = New System.Drawing.Size(376, 24)
        Me.chkStaticDest.TabIndex = 5
        Me.chkStaticDest.Text = "Use a static destination for this dynamic schedule"
        Me.chkStaticDest.UseVisualStyleBackColor = False
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.UcDSN)
        Me.GroupBox6.Controls.Add(Me.cmdValidate)
        Me.GroupBox6.Controls.Add(Me.GroupBox2)
        Me.GroupBox6.Location = New System.Drawing.Point(14, 94)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(416, 288)
        Me.GroupBox6.TabIndex = 17
        Me.GroupBox6.TabStop = False
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.Color.Transparent
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.UcDSN, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDSN, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDSN.Location = New System.Drawing.Point(8, 16)
        Me.UcDSN.Name = "UcDSN"
        Me.HelpProvider1.SetShowHelp(Me.UcDSN, True)
        Me.UcDSN.Size = New System.Drawing.Size(384, 112)
        Me.UcDSN.TabIndex = 6
        '
        'cmdValidate
        '
        Me.cmdValidate.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdValidate, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdValidate, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 136)
        Me.cmdValidate.Name = "cmdValidate"
        Me.HelpProvider1.SetShowHelp(Me.cmdValidate, True)
        Me.cmdValidate.Size = New System.Drawing.Size(75, 23)
        Me.cmdValidate.TabIndex = 7
        Me.cmdValidate.Text = "Connect..."
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnClear)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.cmbValue)
        Me.GroupBox2.Controls.Add(Me.cmbColumn)
        Me.GroupBox2.Controls.Add(Me.cmbTable)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.cmdTestLink)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 160)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(400, 112)
        Me.GroupBox2.TabIndex = 15
        Me.GroupBox2.TabStop = False
        '
        'btnClear
        '
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnClear.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnClear.Location = New System.Drawing.Point(277, 81)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(75, 21)
        Me.btnClear.TabIndex = 12
        Me.btnClear.Text = "Clear"
        '
        'Label21
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label21, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label21, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(8, 64)
        Me.Label21.Name = "Label21"
        Me.HelpProvider1.SetShowHelp(Me.Label21, True)
        Me.Label21.Size = New System.Drawing.Size(352, 16)
        Me.Label21.TabIndex = 9
        Me.Label21.Text = "Please select the column that holds the required xxx"
        '
        'cmbValue
        '
        Me.cmbValue.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbValue, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbValue, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbValue.ItemHeight = 13
        Me.cmbValue.Location = New System.Drawing.Point(8, 80)
        Me.cmbValue.Name = "cmbValue"
        Me.HelpProvider1.SetShowHelp(Me.cmbValue, True)
        Me.cmbValue.Size = New System.Drawing.Size(168, 21)
        Me.cmbValue.TabIndex = 10
        '
        'cmbColumn
        '
        Me.cmbColumn.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbColumn, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbColumn, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbColumn.ItemHeight = 13
        Me.cmbColumn.Location = New System.Drawing.Point(184, 32)
        Me.cmbColumn.Name = "cmbColumn"
        Me.HelpProvider1.SetShowHelp(Me.cmbColumn, True)
        Me.cmbColumn.Size = New System.Drawing.Size(168, 21)
        Me.cmbColumn.TabIndex = 9
        '
        'cmbTable
        '
        Me.cmbTable.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.cmbTable, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbTable, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbTable.ItemHeight = 13
        Me.cmbTable.Location = New System.Drawing.Point(8, 32)
        Me.cmbTable.Name = "cmbTable"
        Me.HelpProvider1.SetShowHelp(Me.cmbTable, True)
        Me.cmbTable.Size = New System.Drawing.Size(168, 21)
        Me.cmbTable.TabIndex = 8
        '
        'Label8
        '
        Me.HelpProvider1.SetHelpKeyword(Me.Label8, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label8, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 16)
        Me.Label8.Name = "Label8"
        Me.HelpProvider1.SetShowHelp(Me.Label8, True)
        Me.Label8.Size = New System.Drawing.Size(384, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Please select the table and column that must equal the provided parameter"
        '
        'cmdTestLink
        '
        Me.cmdTestLink.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTestLink, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTestLink, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTestLink.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTestLink.Location = New System.Drawing.Point(184, 80)
        Me.cmdTestLink.Name = "cmdTestLink"
        Me.HelpProvider1.SetShowHelp(Me.cmdTestLink, True)
        Me.cmdTestLink.Size = New System.Drawing.Size(75, 21)
        Me.cmdTestLink.TabIndex = 11
        Me.cmdTestLink.Text = "Test"
        '
        'optStatic
        '
        Me.optStatic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optStatic, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optStatic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optStatic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optStatic.Location = New System.Drawing.Point(14, 32)
        Me.optStatic.Name = "optStatic"
        Me.HelpProvider1.SetShowHelp(Me.optStatic, True)
        Me.optStatic.Size = New System.Drawing.Size(147, 29)
        Me.optStatic.TabIndex = 2
        Me.optStatic.TabStop = True
        Me.optStatic.Text = "Populate from static data"
        Me.optStatic.UseVisualStyleBackColor = False
        '
        'cmbKey
        '
        Me.cmbKey.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.HelpProvider1.SetHelpKeyword(Me.cmbKey, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmbKey, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(95, 4)
        Me.cmbKey.Name = "cmbKey"
        Me.HelpProvider1.SetShowHelp(Me.cmbKey, True)
        Me.cmbKey.Size = New System.Drawing.Size(191, 21)
        Me.cmbKey.TabIndex = 1
        '
        'Label23
        '
        Me.Label23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label23.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label23, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label23, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(11, 4)
        Me.Label23.Name = "Label23"
        Me.HelpProvider1.SetShowHelp(Me.Label23, True)
        Me.Label23.Size = New System.Drawing.Size(100, 23)
        Me.Label23.TabIndex = 11
        Me.Label23.Text = "Key Parameter"
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'optDynamic
        '
        Me.optDynamic.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.optDynamic, "dynamicLinking.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDynamic, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDynamic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDynamic.Location = New System.Drawing.Point(180, 38)
        Me.optDynamic.Name = "optDynamic"
        Me.HelpProvider1.SetShowHelp(Me.optDynamic, True)
        Me.optDynamic.Size = New System.Drawing.Size(194, 23)
        Me.optDynamic.TabIndex = 3
        Me.optDynamic.TabStop = True
        Me.optDynamic.Text = "Populate with data from database"
        Me.optDynamic.UseVisualStyleBackColor = False
        '
        'cmdView
        '
        Me.cmdView.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdView, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdView, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdView.Image = CType(resources.GetObject("cmdView.Image"), System.Drawing.Image)
        Me.cmdView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdView.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdView.Location = New System.Drawing.Point(5, 375)
        Me.cmdView.Name = "cmdView"
        Me.HelpProvider1.SetShowHelp(Me.cmdView, True)
        Me.cmdView.Size = New System.Drawing.Size(75, 23)
        Me.cmdView.TabIndex = 4
        Me.cmdView.Text = "View"
        '
        'lsvSnapshots
        '
        Me.lsvSnapshots.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader3, Me.ColumnHeader4})
        Me.lsvSnapshots.FullRowSelect = True
        Me.lsvSnapshots.GridLines = True
        Me.lsvSnapshots.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.HelpProvider1.SetHelpKeyword(Me.lsvSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvSnapshots.LargeImageList = Me.imgSnap
        Me.lsvSnapshots.Location = New System.Drawing.Point(7, 34)
        Me.lsvSnapshots.Name = "lsvSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.lsvSnapshots, True)
        Me.lsvSnapshots.Size = New System.Drawing.Size(416, 332)
        Me.lsvSnapshots.SmallImageList = Me.imgSnap
        Me.lsvSnapshots.TabIndex = 3
        Me.lsvSnapshots.UseCompatibleStateImageBehavior = False
        Me.lsvSnapshots.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Entry Date"
        Me.ColumnHeader3.Width = 86
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Path"
        Me.ColumnHeader4.Width = 322
        '
        'imgSnap
        '
        Me.imgSnap.ImageStream = CType(resources.GetObject("imgSnap.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgSnap.TransparentColor = System.Drawing.Color.Transparent
        Me.imgSnap.Images.SetKeyName(0, "")
        '
        'Label11
        '
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label11, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label11, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(319, 12)
        Me.Label11.Name = "Label11"
        Me.HelpProvider1.SetShowHelp(Me.Label11, True)
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "Days"
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.HelpProvider1.SetHelpKeyword(Me.txtSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtSnapshots.Location = New System.Drawing.Point(255, 10)
        Me.txtSnapshots.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.txtSnapshots, True)
        Me.txtSnapshots.Size = New System.Drawing.Size(56, 21)
        Me.txtSnapshots.TabIndex = 2
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkSnapshots
        '
        Me.chkSnapshots.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.chkSnapshots, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.chkSnapshots, System.Windows.Forms.HelpNavigator.Topic)
        Me.chkSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkSnapshots.Location = New System.Drawing.Point(7, 8)
        Me.chkSnapshots.Name = "chkSnapshots"
        Me.HelpProvider1.SetShowHelp(Me.chkSnapshots, True)
        Me.chkSnapshots.Size = New System.Drawing.Size(240, 24)
        Me.chkSnapshots.TabIndex = 1
        Me.chkSnapshots.Text = "Enable snapshots and keep snapshots for"
        Me.chkSnapshots.UseVisualStyleBackColor = False
        '
        'cmdDelete
        '
        Me.cmdDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdDelete, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdDelete, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDelete.Location = New System.Drawing.Point(181, 375)
        Me.cmdDelete.Name = "cmdDelete"
        Me.HelpProvider1.SetShowHelp(Me.cmdDelete, True)
        Me.cmdDelete.Size = New System.Drawing.Size(75, 23)
        Me.cmdDelete.TabIndex = 6
        Me.cmdDelete.Text = "Delete"
        '
        'cmdExecute
        '
        Me.cmdExecute.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpKeyword(Me.cmdExecute, "Snapshots.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdExecute, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdExecute.Image = CType(resources.GetObject("cmdExecute.Image"), System.Drawing.Image)
        Me.cmdExecute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdExecute.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdExecute.Location = New System.Drawing.Point(93, 375)
        Me.cmdExecute.Name = "cmdExecute"
        Me.HelpProvider1.SetShowHelp(Me.cmdExecute, True)
        Me.cmdExecute.Size = New System.Drawing.Size(75, 23)
        Me.cmdExecute.TabIndex = 5
        Me.cmdExecute.Text = "Execute"
        Me.cmdExecute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdTest
        '
        Me.cmdTest.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdTest.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTest, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTest, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTest.Image = CType(resources.GetObject("cmdTest.Image"), System.Drawing.Image)
        Me.cmdTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest.Location = New System.Drawing.Point(414, 364)
        Me.cmdTest.Name = "cmdTest"
        Me.HelpProvider1.SetShowHelp(Me.cmdTest, True)
        Me.cmdTest.Size = New System.Drawing.Size(72, 23)
        Me.cmdTest.TabIndex = 2
        Me.cmdTest.Text = "Preview"
        Me.cmdTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdTest.UseVisualStyleBackColor = False
        '
        'imgTools
        '
        Me.imgTools.ImageStream = CType(resources.GetObject("imgTools.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgTools.TransparentColor = System.Drawing.Color.Transparent
        Me.imgTools.Images.SetKeyName(0, "")
        Me.imgTools.Images.SetKeyName(1, "")
        Me.imgTools.Images.SetKeyName(2, "")
        Me.imgTools.Images.SetKeyName(3, "")
        Me.imgTools.Images.SetKeyName(4, "")
        '
        'btnRefreshDS
        '
        Me.btnRefreshDS.BackColor = System.Drawing.SystemColors.Control
        Me.btnRefreshDS.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRefreshDS.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.btnRefreshDS.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.btnRefreshDS, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.btnRefreshDS, System.Windows.Forms.HelpNavigator.Topic)
        Me.btnRefreshDS.Image = CType(resources.GetObject("btnRefreshDS.Image"), System.Drawing.Image)
        Me.btnRefreshDS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnRefreshDS.Location = New System.Drawing.Point(377, 364)
        Me.btnRefreshDS.Name = "btnRefreshDS"
        Me.HelpProvider1.SetShowHelp(Me.btnRefreshDS, True)
        Me.btnRefreshDS.Size = New System.Drawing.Size(32, 23)
        Me.btnRefreshDS.TabIndex = 3
        Me.btnRefreshDS.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ToolTip1.SetToolTip(Me.btnRefreshDS, "Refresh report datasources")
        Me.btnRefreshDS.UseVisualStyleBackColor = False
        '
        'tabProperties
        '
        Me.tabProperties.CanReorderTabs = True
        Me.tabProperties.Controls.Add(Me.TabControlPanel10)
        Me.tabProperties.Controls.Add(Me.TabControlPanel11)
        Me.tabProperties.Controls.Add(Me.TabControlPanel1)
        Me.tabProperties.Controls.Add(Me.TabControlPanel3)
        Me.tabProperties.Controls.Add(Me.TabControlPanel8)
        Me.tabProperties.Controls.Add(Me.TabControlPanel9)
        Me.tabProperties.Controls.Add(Me.TabControlPanel5)
        Me.tabProperties.Controls.Add(Me.TabControlPanel4)
        Me.tabProperties.Controls.Add(Me.TabControlPanel7)
        Me.tabProperties.Controls.Add(Me.TabControlPanel2)
        Me.tabProperties.Controls.Add(Me.TabControlPanel6)
        Me.tabProperties.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabProperties.Location = New System.Drawing.Point(0, 0)
        Me.tabProperties.Name = "tabProperties"
        Me.tabProperties.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabProperties.SelectedTabIndex = 0
        Me.tabProperties.Size = New System.Drawing.Size(641, 410)
        Me.tabProperties.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabProperties.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabProperties.TabIndex = 26
        Me.tabProperties.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabProperties.Tabs.Add(Me.tbGeneral)
        Me.tabProperties.Tabs.Add(Me.tbSchedule)
        Me.tabProperties.Tabs.Add(Me.tbReport)
        Me.tabProperties.Tabs.Add(Me.tbOptions)
        Me.tabProperties.Tabs.Add(Me.tbOutput)
        Me.tabProperties.Tabs.Add(Me.tbExceptions)
        Me.tabProperties.Tabs.Add(Me.tbHistory)
        Me.tabProperties.Tabs.Add(Me.tbTasks)
        Me.tabProperties.Tabs.Add(Me.tbLinking)
        Me.tabProperties.Tabs.Add(Me.tbSnapshots)
        Me.tabProperties.Tabs.Add(Me.tbDataDriven)
        '
        'TabControlPanel11
        '
        Me.TabControlPanel11.Controls.Add(Me.chkGroupReports)
        Me.TabControlPanel11.Controls.Add(Me.grpQuery)
        Me.TabControlPanel11.Controls.Add(Me.btnConnect)
        Me.TabControlPanel11.Controls.Add(Me.UcDSNDD)
        Me.TabControlPanel11.Controls.Add(Me.Label5)
        Me.TabControlPanel11.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel11.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel11.Name = "TabControlPanel11"
        Me.TabControlPanel11.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel11.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel11.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel11.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel11.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel11.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel11.TabIndex = 11
        Me.TabControlPanel11.TabItem = Me.tbDataDriven
        '
        'chkGroupReports
        '
        Me.chkGroupReports.AutoSize = True
        Me.chkGroupReports.BackColor = System.Drawing.Color.Transparent
        Me.chkGroupReports.Location = New System.Drawing.Point(11, 385)
        Me.chkGroupReports.Name = "chkGroupReports"
        Me.chkGroupReports.Size = New System.Drawing.Size(335, 17)
        Me.chkGroupReports.TabIndex = 7
        Me.chkGroupReports.Text = "Group reports together by email address (email destination only)"
        Me.chkGroupReports.UseVisualStyleBackColor = False
        '
        'grpQuery
        '
        Me.grpQuery.BackColor = System.Drawing.Color.Transparent
        Me.grpQuery.Controls.Add(Me.cmbDDKey)
        Me.grpQuery.Controls.Add(Me.Label12)
        Me.grpQuery.Controls.Add(Me.btnBuild)
        Me.grpQuery.Controls.Add(Me.txtQuery)
        Me.grpQuery.Enabled = False
        Me.grpQuery.Location = New System.Drawing.Point(11, 185)
        Me.grpQuery.Name = "grpQuery"
        Me.grpQuery.Size = New System.Drawing.Size(440, 196)
        Me.grpQuery.TabIndex = 6
        Me.grpQuery.TabStop = False
        Me.grpQuery.Text = "Data selection criteria"
        '
        'cmbDDKey
        '
        Me.cmbDDKey.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbDDKey.FormattingEnabled = True
        Me.cmbDDKey.Location = New System.Drawing.Point(86, 164)
        Me.cmbDDKey.Name = "cmbDDKey"
        Me.cmbDDKey.Size = New System.Drawing.Size(171, 21)
        Me.cmbDDKey.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(14, 168)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Key Column"
        '
        'btnBuild
        '
        Me.btnBuild.Location = New System.Drawing.Point(182, 135)
        Me.btnBuild.Name = "btnBuild"
        Me.btnBuild.Size = New System.Drawing.Size(75, 23)
        Me.btnBuild.TabIndex = 1
        Me.btnBuild.Text = "&Build"
        Me.btnBuild.UseVisualStyleBackColor = True
        '
        'txtQuery
        '
        Me.txtQuery.BackColor = System.Drawing.Color.White
        Me.txtQuery.Location = New System.Drawing.Point(9, 19)
        Me.txtQuery.Multiline = True
        Me.txtQuery.Name = "txtQuery"
        Me.txtQuery.ReadOnly = True
        Me.txtQuery.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtQuery.Size = New System.Drawing.Size(410, 110)
        Me.txtQuery.TabIndex = 0
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(193, 144)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 4
        Me.btnConnect.Text = "&Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'UcDSNDD
        '
        Me.UcDSNDD.BackColor = System.Drawing.Color.White
        Me.UcDSNDD.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSNDD.ForeColor = System.Drawing.Color.Navy
        Me.UcDSNDD.Location = New System.Drawing.Point(10, 26)
        Me.UcDSNDD.Name = "UcDSNDD"
        Me.UcDSNDD.Size = New System.Drawing.Size(441, 112)
        Me.UcDSNDD.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(141, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Database Connection Setup"
        '
        'tbDataDriven
        '
        Me.tbDataDriven.AttachedControl = Me.TabControlPanel11
        Me.tbDataDriven.Image = CType(resources.GetObject("tbDataDriven.Image"), System.Drawing.Image)
        Me.tbDataDriven.Name = "tbDataDriven"
        Me.tbDataDriven.Text = "Data-Driver"
        Me.tbDataDriven.Visible = False
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.txtUrl)
        Me.TabControlPanel1.Controls.Add(Me.Label6)
        Me.TabControlPanel1.Controls.Add(Me.txtDesc)
        Me.TabControlPanel1.Controls.Add(Me.Label2)
        Me.TabControlPanel1.Controls.Add(Me.Label7)
        Me.TabControlPanel1.Controls.Add(Me.txtName)
        Me.TabControlPanel1.Controls.Add(Me.Label9)
        Me.TabControlPanel1.Controls.Add(Me.Label3)
        Me.TabControlPanel1.Controls.Add(Me.txtKeyWord)
        Me.TabControlPanel1.Controls.Add(Me.Label4)
        Me.TabControlPanel1.Controls.Add(Me.cmdDbLoc)
        Me.TabControlPanel1.Controls.Add(Me.txtFolder)
        Me.TabControlPanel1.Controls.Add(Me.txtDBLoc)
        Me.TabControlPanel1.Controls.Add(Me.cmdLoc)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbGeneral
        '
        'txtUrl
        '
        Me.txtUrl.BackColor = System.Drawing.Color.White
        Me.txtUrl.ForeColor = System.Drawing.Color.Blue
        Me.HelpProvider1.SetHelpKeyword(Me.txtUrl, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.txtUrl, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtUrl.Location = New System.Drawing.Point(4, 114)
        Me.txtUrl.Name = "txtUrl"
        Me.HelpProvider1.SetShowHelp(Me.txtUrl, True)
        Me.txtUrl.Size = New System.Drawing.Size(376, 21)
        Me.txtUrl.TabIndex = 3
        Me.txtUrl.Tag = "memo"
        Me.txtUrl.Text = "http://myReportServer/ReportServer/"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.HelpProvider1.SetHelpKeyword(Me.Label6, "General.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.Label6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(4, 97)
        Me.Label6.Name = "Label6"
        Me.HelpProvider1.SetShowHelp(Me.Label6, True)
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 32
        Me.Label6.Text = "Report  Server URL"
        '
        'tbGeneral
        '
        Me.tbGeneral.AttachedControl = Me.TabControlPanel1
        Me.tbGeneral.Image = CType(resources.GetObject("tbGeneral.Image"), System.Drawing.Image)
        Me.tbGeneral.Name = "tbGeneral"
        Me.tbGeneral.Text = "General"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.grpAutoResume)
        Me.TabControlPanel3.Controls.Add(Me.cmdTest2)
        Me.TabControlPanel3.Controls.Add(Me.lsvParameters)
        Me.TabControlPanel3.Controls.Add(Me.cmdRequery)
        Me.TabControlPanel3.Controls.Add(Me.txtID)
        Me.TabControlPanel3.Controls.Add(Me.txtFormula)
        Me.TabControlPanel3.Controls.Add(Me.cmdParameters)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbReport
        '
        'grpAutoResume
        '
        Me.grpAutoResume.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoResume.Controls.Add(Me.chkAutoResume)
        Me.grpAutoResume.Controls.Add(Me.txtCacheExpiry)
        Me.grpAutoResume.Controls.Add(Me.Label1)
        Me.grpAutoResume.Controls.Add(Me.Label10)
        Me.grpAutoResume.Location = New System.Drawing.Point(8, 331)
        Me.grpAutoResume.Name = "grpAutoResume"
        Me.grpAutoResume.Size = New System.Drawing.Size(486, 67)
        Me.grpAutoResume.TabIndex = 31
        Me.grpAutoResume.TabStop = False
        '
        'chkAutoResume
        '
        Me.chkAutoResume.AutoSize = True
        Me.chkAutoResume.BackColor = System.Drawing.Color.Transparent
        Me.chkAutoResume.Location = New System.Drawing.Point(2, 13)
        Me.chkAutoResume.Name = "chkAutoResume"
        Me.chkAutoResume.Size = New System.Drawing.Size(268, 17)
        Me.chkAutoResume.TabIndex = 26
        Me.chkAutoResume.Text = "Resume failed/errored schedules with cached data"
        Me.chkAutoResume.UseVisualStyleBackColor = False
        '
        'txtCacheExpiry
        '
        Me.txtCacheExpiry.Enabled = False
        Me.txtCacheExpiry.Location = New System.Drawing.Point(168, 37)
        Me.txtCacheExpiry.Maximum = New Decimal(New Integer() {10080, 0, 0, 0})
        Me.txtCacheExpiry.Name = "txtCacheExpiry"
        Me.txtCacheExpiry.Size = New System.Drawing.Size(38, 21)
        Me.txtCacheExpiry.TabIndex = 29
        Me.txtCacheExpiry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Location = New System.Drawing.Point(-1, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 13)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "Expire dynamic cached data after"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Location = New System.Drawing.Point(208, 41)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "minutes"
        '
        'cmdTest2
        '
        Me.cmdTest2.BackColor = System.Drawing.SystemColors.Control
        Me.cmdTest2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTest2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdTest2.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.cmdTest2, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.cmdTest2, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdTest2.Image = CType(resources.GetObject("cmdTest2.Image"), System.Drawing.Image)
        Me.cmdTest2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdTest2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTest2.Location = New System.Drawing.Point(422, 291)
        Me.cmdTest2.Name = "cmdTest2"
        Me.HelpProvider1.SetShowHelp(Me.cmdTest2, True)
        Me.cmdTest2.Size = New System.Drawing.Size(72, 23)
        Me.cmdTest2.TabIndex = 26
        Me.cmdTest2.Text = "Preview"
        Me.cmdTest2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdTest2.UseVisualStyleBackColor = False
        '
        'tbReport
        '
        Me.tbReport.AttachedControl = Me.TabControlPanel3
        Me.tbReport.Image = CType(resources.GetObject("tbReport.Image"), System.Drawing.Image)
        Me.tbReport.Name = "tbReport"
        Me.tbReport.Text = "Report"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.cmdView)
        Me.TabControlPanel10.Controls.Add(Me.chkSnapshots)
        Me.TabControlPanel10.Controls.Add(Me.lsvSnapshots)
        Me.TabControlPanel10.Controls.Add(Me.cmdExecute)
        Me.TabControlPanel10.Controls.Add(Me.Label11)
        Me.TabControlPanel10.Controls.Add(Me.cmdDelete)
        Me.TabControlPanel10.Controls.Add(Me.txtSnapshots)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel10.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.TabIndex = 10
        Me.TabControlPanel10.TabItem = Me.tbSnapshots
        '
        'tbSnapshots
        '
        Me.tbSnapshots.AttachedControl = Me.TabControlPanel10
        Me.tbSnapshots.Image = CType(resources.GetObject("tbSnapshots.Image"), System.Drawing.Image)
        Me.tbSnapshots.Name = "tbSnapshots"
        Me.tbSnapshots.Text = "Snapshots"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.grpDynamicTasks)
        Me.TabControlPanel8.Controls.Add(Me.oTask)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.TabIndex = 8
        Me.TabControlPanel8.TabItem = Me.tbTasks
        '
        'grpDynamicTasks
        '
        Me.grpDynamicTasks.BackColor = System.Drawing.Color.Transparent
        Me.grpDynamicTasks.Controls.Add(Me.optOnce)
        Me.grpDynamicTasks.Controls.Add(Me.optAll)
        Me.grpDynamicTasks.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.grpDynamicTasks.Enabled = False
        Me.grpDynamicTasks.Location = New System.Drawing.Point(1, 335)
        Me.grpDynamicTasks.Name = "grpDynamicTasks"
        Me.grpDynamicTasks.Size = New System.Drawing.Size(504, 74)
        Me.grpDynamicTasks.TabIndex = 17
        Me.grpDynamicTasks.TabStop = False
        Me.grpDynamicTasks.Text = "Run custom tasks..."
        '
        'optOnce
        '
        Me.optOnce.AutoSize = True
        Me.HelpProvider1.SetHelpKeyword(Me.optOnce, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optOnce, System.Windows.Forms.HelpNavigator.Topic)
        Me.optOnce.Location = New System.Drawing.Point(6, 46)
        Me.optOnce.Name = "optOnce"
        Me.HelpProvider1.SetShowHelp(Me.optOnce, True)
        Me.optOnce.Size = New System.Drawing.Size(162, 17)
        Me.optOnce.TabIndex = 2
        Me.optOnce.TabStop = True
        Me.optOnce.Tag = "1"
        Me.optOnce.Text = "Once for the entire schedule"
        Me.optOnce.UseVisualStyleBackColor = True
        '
        'optAll
        '
        Me.optAll.AutoSize = True
        Me.optAll.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAll, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAll, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAll.Location = New System.Drawing.Point(6, 20)
        Me.optAll.Name = "optAll"
        Me.HelpProvider1.SetShowHelp(Me.optAll, True)
        Me.optAll.Size = New System.Drawing.Size(179, 17)
        Me.optAll.TabIndex = 1
        Me.optAll.TabStop = True
        Me.optAll.Tag = "0"
        Me.optAll.Text = "Once for each generated report"
        Me.optAll.UseVisualStyleBackColor = True
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.Color.Transparent
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Top
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.oTask, "Tasks.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.oTask, System.Windows.Forms.HelpNavigator.Topic)
        Me.oTask.Location = New System.Drawing.Point(1, 1)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.Name = "oTask"
        Me.HelpProvider1.SetShowHelp(Me.oTask, True)
        Me.oTask.Size = New System.Drawing.Size(504, 328)
        Me.oTask.TabIndex = 0
        '
        'tbTasks
        '
        Me.tbTasks.AttachedControl = Me.TabControlPanel8
        Me.tbTasks.Image = CType(resources.GetObject("tbTasks.Image"), System.Drawing.Image)
        Me.tbTasks.Name = "tbTasks"
        Me.tbTasks.Text = "Tasks"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.btnView)
        Me.TabControlPanel9.Controls.Add(Me.GroupBox6)
        Me.TabControlPanel9.Controls.Add(Me.optDynamic)
        Me.TabControlPanel9.Controls.Add(Me.optStatic)
        Me.TabControlPanel9.Controls.Add(Me.cmbKey)
        Me.TabControlPanel9.Controls.Add(Me.Label23)
        Me.TabControlPanel9.Controls.Add(Me.chkStaticDest)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.TabIndex = 9
        Me.TabControlPanel9.TabItem = Me.tbLinking
        '
        'tbLinking
        '
        Me.tbLinking.AttachedControl = Me.TabControlPanel9
        Me.tbLinking.Image = CType(resources.GetObject("tbLinking.Image"), System.Drawing.Image)
        Me.tbLinking.Name = "tbLinking"
        Me.tbLinking.Text = "Linking"
        Me.tbLinking.Visible = False
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.UcDest)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.tbOutput
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcDest, "Output.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcDest, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcDest.Location = New System.Drawing.Point(7, 11)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.HelpProvider1.SetShowHelp(Me.UcDest, True)
        Me.UcDest.Size = New System.Drawing.Size(481, 391)
        Me.UcDest.TabIndex = 0
        '
        'tbOutput
        '
        Me.tbOutput.AttachedControl = Me.TabControlPanel5
        Me.tbOutput.Image = CType(resources.GetObject("tbOutput.Image"), System.Drawing.Image)
        Me.tbOutput.Name = "tbOutput"
        Me.tbOutput.Text = "Output"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.GroupBox1)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbOptions
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.lsvDatasources)
        Me.GroupBox1.Controls.Add(Me.btnRefreshDS)
        Me.GroupBox1.Controls.Add(Me.cmdTest)
        Me.GroupBox1.Location = New System.Drawing.Point(5, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 393)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lsvDatasources.ContextMenuStrip = Me.mnuDatasources
        Me.HelpProvider1.SetHelpKeyword(Me.lsvDatasources, "Report_Options.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.lsvDatasources, System.Windows.Forms.HelpNavigator.Topic)
        Me.lsvDatasources.Location = New System.Drawing.Point(8, 18)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.HelpProvider1.SetShowHelp(Me.lsvDatasources, True)
        Me.lsvDatasources.Size = New System.Drawing.Size(478, 340)
        Me.lsvDatasources.TabIndex = 1
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Datasource Name"
        Me.ColumnHeader5.Width = 274
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Name"
        Me.ColumnHeader6.Width = 116
        '
        'mnuDatasources
        '
        Me.mnuDatasources.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ClearToolStripMenuItem})
        Me.mnuDatasources.Name = "mnuDatasources"
        Me.mnuDatasources.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuDatasources.Size = New System.Drawing.Size(102, 26)
        '
        'ClearToolStripMenuItem
        '
        Me.ClearToolStripMenuItem.Name = "ClearToolStripMenuItem"
        Me.ClearToolStripMenuItem.Size = New System.Drawing.Size(101, 22)
        Me.ClearToolStripMenuItem.Text = "&Clear"
        '
        'tbOptions
        '
        Me.tbOptions.AttachedControl = Me.TabControlPanel4
        Me.tbOptions.Image = CType(resources.GetObject("tbOptions.Image"), System.Drawing.Image)
        Me.tbOptions.Name = "tbOptions"
        Me.tbOptions.Text = "Datasources"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.UcError)
        Me.TabControlPanel7.Controls.Add(Me.UcBlank)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.tbExceptions
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(5, 5)
        Me.UcError.m_showFailOnOne = False
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(496, 65)
        Me.UcError.TabIndex = 5
        '
        'UcBlank
        '
        Me.UcBlank.BackColor = System.Drawing.Color.Transparent
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.UcBlank, "Error_Handling.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.UcBlank, System.Windows.Forms.HelpNavigator.Topic)
        Me.UcBlank.Location = New System.Drawing.Point(4, 76)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.HelpProvider1.SetShowHelp(Me.UcBlank, True)
        Me.UcBlank.Size = New System.Drawing.Size(497, 326)
        Me.UcBlank.TabIndex = 4
        '
        'tbExceptions
        '
        Me.tbExceptions.AttachedControl = Me.TabControlPanel7
        Me.tbExceptions.Image = CType(resources.GetObject("tbExceptions.Image"), System.Drawing.Image)
        Me.tbExceptions.Name = "tbExceptions"
        Me.tbExceptions.Text = "Exception Handling"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.ucUpdate)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbSchedule
        '
        'ucUpdate
        '
        Me.ucUpdate.BackColor = System.Drawing.Color.Transparent
        Me.ucUpdate.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.HelpProvider1.SetHelpKeyword(Me.ucUpdate, "Schedule.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.ucUpdate, System.Windows.Forms.HelpNavigator.Topic)
        Me.ucUpdate.Location = New System.Drawing.Point(7, 8)
        Me.ucUpdate.m_RepeatUnit = ""
        Me.ucUpdate.Name = "ucUpdate"
        Me.HelpProvider1.SetShowHelp(Me.ucUpdate, True)
        Me.ucUpdate.Size = New System.Drawing.Size(494, 360)
        Me.ucUpdate.TabIndex = 0
        '
        'tbSchedule
        '
        Me.tbSchedule.AttachedControl = Me.TabControlPanel2
        Me.tbSchedule.Image = CType(resources.GetObject("tbSchedule.Image"), System.Drawing.Image)
        Me.tbSchedule.Name = "tbSchedule"
        Me.tbSchedule.Text = "Schedule"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.tvHistory)
        Me.TabControlPanel6.Controls.Add(Me.grSort)
        Me.TabControlPanel6.Controls.Add(Me.Panel1)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(135, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(506, 410)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.tbHistory
        '
        'tvHistory
        '
        Me.tvHistory.Dock = System.Windows.Forms.DockStyle.Fill
        Me.HelpProvider1.SetHelpKeyword(Me.tvHistory, "History.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.tvHistory, System.Windows.Forms.HelpNavigator.Topic)
        Me.tvHistory.ImageIndex = 0
        Me.tvHistory.ImageList = Me.imgSingle
        Me.tvHistory.Indent = 19
        Me.tvHistory.ItemHeight = 16
        Me.tvHistory.Location = New System.Drawing.Point(1, 49)
        Me.tvHistory.Name = "tvHistory"
        Me.tvHistory.SelectedImageIndex = 0
        Me.HelpProvider1.SetShowHelp(Me.tvHistory, True)
        Me.tvHistory.Size = New System.Drawing.Size(504, 330)
        Me.tvHistory.TabIndex = 2
        '
        'grSort
        '
        Me.grSort.BackColor = System.Drawing.Color.Transparent
        Me.grSort.Controls.Add(Me.optAsc)
        Me.grSort.Controls.Add(Me.optDesc)
        Me.grSort.Dock = System.Windows.Forms.DockStyle.Top
        Me.grSort.Location = New System.Drawing.Point(1, 1)
        Me.grSort.Name = "grSort"
        Me.grSort.Size = New System.Drawing.Size(504, 48)
        Me.grSort.TabIndex = 0
        Me.grSort.TabStop = False
        Me.grSort.Text = "Sort"
        '
        'optAsc
        '
        Me.optAsc.Checked = True
        Me.HelpProvider1.SetHelpKeyword(Me.optAsc, "History.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optAsc, System.Windows.Forms.HelpNavigator.Topic)
        Me.optAsc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optAsc.Location = New System.Drawing.Point(16, 16)
        Me.optAsc.Name = "optAsc"
        Me.HelpProvider1.SetShowHelp(Me.optAsc, True)
        Me.optAsc.Size = New System.Drawing.Size(120, 24)
        Me.optAsc.TabIndex = 0
        Me.optAsc.TabStop = True
        Me.optAsc.Tag = "ASC"
        Me.optAsc.Text = "Ascending Order"
        '
        'optDesc
        '
        Me.HelpProvider1.SetHelpKeyword(Me.optDesc, "History.htm")
        Me.HelpProvider1.SetHelpNavigator(Me.optDesc, System.Windows.Forms.HelpNavigator.Topic)
        Me.optDesc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDesc.Location = New System.Drawing.Point(144, 16)
        Me.optDesc.Name = "optDesc"
        Me.HelpProvider1.SetShowHelp(Me.optDesc, True)
        Me.optDesc.Size = New System.Drawing.Size(120, 24)
        Me.optDesc.TabIndex = 1
        Me.optDesc.Tag = "DESC"
        Me.optDesc.Text = "Descending Order"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.Controls.Add(Me.cmdClear)
        Me.Panel1.Controls.Add(Me.cmdRefresh)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(1, 379)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(504, 30)
        Me.Panel1.TabIndex = 3
        '
        'tbHistory
        '
        Me.tbHistory.AttachedControl = Me.TabControlPanel6
        Me.tbHistory.Image = CType(resources.GetObject("tbHistory.Image"), System.Drawing.Image)
        Me.tbHistory.Name = "tbHistory"
        Me.tbHistory.Text = "History"
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "data_table.png")
        Me.ImageList1.Images.SetKeyName(1, "key1.ico")
        Me.ImageList1.Images.SetKeyName(2, "view.ico")
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'bgWorker
        '
        Me.bgWorker.WorkerSupportsCancellation = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'frmSingleProp
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(641, 445)
        Me.Controls.Add(Me.tabProperties)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSingleProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Single Schedule Properties"
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabProperties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabProperties.ResumeLayout(False)
        Me.TabControlPanel11.ResumeLayout(False)
        Me.TabControlPanel11.PerformLayout()
        Me.grpQuery.ResumeLayout(False)
        Me.grpQuery.PerformLayout()
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel3.PerformLayout()
        Me.grpAutoResume.ResumeLayout(False)
        Me.grpAutoResume.PerformLayout()
        CType(Me.txtCacheExpiry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel10.ResumeLayout(False)
        Me.TabControlPanel8.ResumeLayout(False)
        Me.grpDynamicTasks.ResumeLayout(False)
        Me.grpDynamicTasks.PerformLayout()
        Me.TabControlPanel9.ResumeLayout(False)
        Me.TabControlPanel5.ResumeLayout(False)
        Me.TabControlPanel4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.mnuDatasources.ResumeLayout(False)
        Me.TabControlPanel7.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        Me.grSort.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oData As clsMarsData = New clsMarsData
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim OkayToClose As Boolean
    Dim IsDynamic As Boolean = False
    Dim IsBursting As Boolean = False
    Dim sLink As String = String.Empty
    Public oRpt As Object 'ReportServer.ReportingService
    Dim sCache As String
    Dim ScheduleID As Integer
    Dim parDefaults As Hashtable
    Dim serverUser As String
    Dim serverPassword As String
    Dim dtParameters As DataTable
    Dim showTooltip As Boolean = True
    Dim owner As String = ""
    Dim IsDataDriven As Boolean = False
    Dim m_serverParametersTable As DataTable

    Private ReadOnly Property m_autoCalcValue() As Decimal
        Get
            Dim val As Decimal = clsMarsScheduler.globalItem.getScheduleDuration(Me.txtID.Text, 0, 0, 0, 0, clsMarsScheduler.enCalcType.MEDIAN, 3)

            Return val

        End Get
    End Property

    Private Sub frmSingleProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        FormatForWinXP(Me)

        bgWorker.RunWorkerAsync()

        clsMarsUI.MainUI.InitHistoryImages(tvHistory, My.Resources.document_chart)

        
    End Sub

    Private ReadOnly Property m_ParametersList() As ArrayList
        Get
            Dim list As ArrayList = New ArrayList

            For Each item As ListViewItem In lsvParameters.Items
                list.Add(item.Text)
            Next

            Return list
        End Get
    End Property
    Public Sub EditSchedule(ByVal nReportID As Int32)

        Me.showTooltip = False

        Dim SQL As String
        Dim Rs As ADODB.Recordset
        Dim rsP As ADODB.Recordset
        Dim sWhere As String
        Dim I As Int32
        Dim sDestination As String = ""
        Dim autoCalc As Boolean

        Try
            sWhere = " WHERE  ReportID = " & nReportID

            SQL = "SELECT * FROM ReportAttr " & sWhere

            Rs = clsMarsData.GetData(SQL)

            If Rs Is Nothing Then Return

            If Rs.EOF = False Then
                txtID.Text = Rs("reportid").Value
                txtDBLoc.Text = Rs("cachepath").Value
                txtUrl.Text = Rs("databasepath").Value
                owner = Rs("owner").Value
                Me.serverUser = IsNull(Rs("rptuserid").Value)
                Me.serverPassword = _DecryptDBValue(IsNull(Rs("rptpassword").Value))
                UcDest.nPackID = 0
                UcDest.nSmartID = 0
                UcDest.nReportID = txtID.Text
                UcDest.LoadAll()
                autoCalc = IsNull(Rs("autocalc").Value, 0)

                txtName.Text = Rs("reporttitle").Value

                UcDest.sReportTitle = txtName.Text

                rsP = clsMarsData.GetData("SELECT FolderName FROM Folders WHERE FolderID = " & Rs("Parent").Value)

                txtFolder.Text = rsP.Fields(0).Value

                txtFolder.Tag = Rs("Parent").Value

                rsP.Close()

                rsP = clsMarsData.GetData("SELECT DestinationType FROM DestinationAttr WHERE " & _
                "ReportID = " & nReportID)

                Try
                    If rsP.EOF = False Then
                        sDestination = rsP.Fields(0).Value
                    End If
                    rsP.Close()

                    Select Case sDestination.ToLower
                        Case "email"
                            Label21.Text = Label21.Text.Replace("xxx", "email address")
                        Case "disk"
                            Label21.Text = Label21.Text.Replace("xxx", "directory")
                        Case "printer"
                            Label21.Text = Label21.Text.Replace("xxx", "printer name")
                        Case "ftp"
                            Label21.Text = Label21.Text.Replace("xxx", "FTP Server")
                    End Select
                Catch : End Try

                Try
                    UcError.cmbRetry.Text = Rs("retry").Value
                    UcError.m_autoFailAfter = IIf(Rs("assumefail").Value = 0, 30, Rs("assumefail").Value)
                    txtFormula.Text = IsNull(Rs("selectionformula").Value)
                    serverUser = Rs("RptUserID").Value
                    serverPassword = _DecryptDBValue(Rs("RptPassword").Value)
                Catch : End Try

                Try
                    chkStaticDest.Checked = Convert.ToBoolean(Rs("staticdest").Value)
                    UcDest.StaticDest = chkStaticDest.Checked
                Catch ex As Exception
                    chkStaticDest.Checked = False
                End Try

                If IsNull(Rs("dynamic").Value, "0") = "-1" Or IsNull(Rs("dynamic").Value, "0") = "1" Then
                    IsDynamic = True
                    Me.Text = "Dynamic Schedule Properties - " & txtName.Text
                    Me.grpAutoResume.Enabled = True
                Else
                    IsDynamic = False
                    Me.Text = "Single Report Schedule Properties - " & txtName.Text
                    Me.grpAutoResume.Enabled = False
                End If

                If IsNull(Rs("dynamictasks").Value, 0) = 0 Then
                    Me.optAll.Checked = True
                Else
                    Me.optOnce.Checked = True
                End If

                If IsNull(Rs("isdatadriven").Value, "0") = "1" Then
                    IsDataDriven = True
                Else
                    IsDataDriven = False
                End If

                UcDest.isDynamic = IsDynamic

                If UcError.cmbAssumeFail.Text > 0 Then
                    UcError.chkAssumeFail.Checked = True
                Else
                    UcError.chkAssumeFail.Checked = True
                    UcError.m_autoFailAfter = 30
                End If

                UcError.chkAutoCalc.Checked = autoCalc

                If autoCalc = True Then
                    UcError.chkAutoCalc.Checked = True
                    UcError.chkAssumeFail.Checked = True
                    UcError.cmbAssumeFail.Enabled = False

                    Dim temp As Decimal = Me.m_autoCalcValue

                    If temp < 1 Then temp = 1

                    Try
                        UcError.m_autoFailAfter = temp
                    Catch : End Try
                End If

                Try
                    UcBlank.chkBlankReport.Checked = Convert.ToBoolean(Rs("checkblank").Value)
                Catch : End Try

                UcError.txtRetryInterval.Value = IsNull(Rs("retryinterval").Value, 0)

            End If

            Rs.Close()

            'get the blank report attributes
            If UcBlank.chkBlankReport.Checked = True Then
                Rs = clsMarsData.GetData("SELECT * FROM BlankReportAlert " & sWhere)

                Try
                    If Rs.EOF = False Then
                        With UcBlank
                            .txtAlertTo.Text = Rs("to").Value
                            .txtBlankMsg.Text = Rs("msg").Value
                            .txtSubject.Text = IsNull(Rs("subject").Value)

                            Select Case Rs("type").Value
                                Case "Alert"
                                    .optAlert.Checked = True
                                Case "AlertandReport"
                                    .optAlertandReport.Checked = True
                                Case "Ignore"
                                    .optIgnore.Checked = True
                            End Select


                            Dim query As String = IsNull(Rs("customquerydetails").Value)

                            If query.Length > 0 Then
                                '.chkCustomQuery.Checked = True

                                .m_customDSN = query.Split("|")(0)
                                .m_customUserID = query.Split("|")(1)
                                .m_customPassword = _DecryptDBValue(query.Split("|")(2))
                                .txtQuery.Text = query.Split("|")(3)
                            End If
                        End With

                        Rs.Close()
                    End If
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, "frmSingleProp.EditSchedule", _
                    _GetLineNumber(ex.StackTrace))
                End Try
            End If

            SQL = "SELECT * FROM ScheduleAttr " & sWhere

            Rs = clsMarsData.GetData(SQL)

            If Rs Is Nothing Then Return

            If Rs.EOF = False Then
                ScheduleID = Rs("scheduleid").Value

                Select Case Rs("frequency").Value.ToLower
                    Case "daily"
                        ucUpdate.optDaily.Checked = True
                    Case "weekly"
                        ucUpdate.optWeekly.Checked = True
                    Case "week-daily"
                        ucUpdate.optWeekDaily.Checked = True
                    Case "monthly"
                        ucUpdate.optMonthly.Checked = True
                    Case "yearly"
                        ucUpdate.optYearly.Checked = True
                    Case "custom"
                        ucUpdate.optCustom.Checked = True
                        ucUpdate.cmbCustom.Text = IsNull(Rs("calendarname").Value)
                    Case "other"
                        ucUpdate.optOther.Checked = True
                    Case "none"
                        ucUpdate.optNone.Checked = True
                End Select

                ucUpdate.m_fireOthers = True
                ucUpdate.chkRepeat.Enabled = ucUpdate.m_EnableRepeat(ScheduleID)

                Try
                    If Rs("status").Value = 1 Then
                        ucUpdate.chkStatus.Checked = True
                    Else
                        ucUpdate.chkStatus.Checked = False
                    End If
                Catch ex As Exception
                    ucUpdate.chkStatus.Checked = True
                End Try

                Try
                    ucUpdate.m_RepeatUnit = IsNull(Rs("repeatunit").Value, "hours")
                Catch ex As Exception
                    ucUpdate.m_RepeatUnit = "hours"
                End Try

                oTask.ScheduleID = Rs("scheduleid").Value
                ucUpdate.ScheduleID = Rs("scheduleid").Value
                ucUpdate.btnApply = Me.cmdApply

                oTask.LoadTasks()

                Try
                    Dim d As Date

                    d = Rs("enddate").Value

                    If d.Year >= 3004 Then
                        ucUpdate.EndDate.Enabled = False
                        ucUpdate.chkNoEnd.Checked = True
                    End If

                Catch : End Try

                Dim rptUntil As String = ""

                Try
                    rptUntil = IsNull(Rs("repeatuntil").Value, Now)

                    If rptUntil.Length > 8 Then
                        ucUpdate.RepeatUntil.Value = CTimeZ(ConDateTime(rptUntil), dateConvertType.READ)
                    Else
                        ucUpdate.RepeatUntil.Value = CTimeZ(Now.Date & " " & rptUntil, dateConvertType.READ)
                    End If
                Catch ex As Exception
                    Try
                        ucUpdate.RepeatUntil.Value = CTimeZ(Date.Now.Date & " " & ConTime(rptUntil), dateConvertType.READ)
                    Catch : End Try
                End Try

                ucUpdate.NextRun.Value = CTimeZ(Rs("nextrun").Value, dateConvertType.READ)
                ucUpdate.EndDate.Value = CTimeZ(Rs("enddate").Value, dateConvertType.READ)
                ucUpdate.RunAt.Value = CTimeZ(Convert.ToDateTime(Date.Now.Date & " " & Rs("starttime").Value), dateConvertType.READ)
                ucUpdate.NextRunTime.Value = CTimeZ(Rs("nextrun").Value, dateConvertType.READ)
                ucUpdate.chkRepeat.Checked = Convert.ToBoolean(Rs("repeat").Value)
                ucUpdate.cmbRpt.Text = IsNonEnglishRegionRead(Rs("repeatinterval").Value)
                ucUpdate.StartDate.Value = CTimeZ(ConDate(Rs("startdate").Value), dateConvertType.READ)

                ucUpdate.cmbException.Text = IsNull(Rs("exceptioncalendar").Value)

                Try
                    ucUpdate.chkException.Checked = Convert.ToBoolean(Rs("useexception").Value)
                Catch
                    ucUpdate.chkException.Checked = False
                End Try
            End If

            If ucUpdate.cmbRpt.Text = "" Then ucUpdate.cmbRpt.Text = 0.25

            txtDesc.Text = IsNull(Rs("description").Value)
            txtKeyWord.Text = IsNull(Rs("keyword").Value)

            Rs.Close()

            'add parameters
            SQL = "SELECT * FROM ReportParameter WHERE ReportID = " & nReportID

            Rs = clsMarsData.GetData(SQL)


            Try
                Do While Rs.EOF = False
                    With lsvParameters
                        Dim oItem As ListViewItem = New ListViewItem

                        oItem.Text = Rs("parname").Value
                        oItem.Tag = Rs("partype").Value

                        oItem.SubItems.Add(Rs("parvalue").Value)
                        oItem.SubItems.Add(IsNull(Rs("multivalue").Value, "false"))

                        .Items.Add(oItem)
                    End With

                    Rs.MoveNext()
                Loop

                Rs.Close()
            Catch : End Try

            UcBlank.m_ParametersList = Me.m_ParametersList
            'add report datasources
            Rs = clsMarsData.GetData("SELECT * FROM ReportDatasource WHERE ReportID =" & nReportID)

            If Rs IsNot Nothing Then
                Do While Rs.EOF = False
                    Dim item As ListViewItem = lsvDatasources.Items.Add(Rs("datasourcename").Value)

                    item.SubItems.Add(Rs("rptuserid").Value)

                    item.ImageIndex = 0
                    item.Tag = Rs("datasourceid").Value

                    Rs.MoveNext()
                Loop

                Rs.Close()
            End If

            'add dynamic linking is necessary

            If IsDynamic = True Then

                grpDynamicTasks.Enabled = True

                tabProperties.Tabs(8).Visible = True
                tabProperties.Tabs(9).Visible = False

                'cmdParameters.Enabled = True 
                'cmdRequery.Enabled = False

                SQL = "SELECT * FROM DynamicLink WHERE ReportID =" & nReportID

                Rs = clsMarsData.GetData(SQL)

                If Not Rs Is Nothing Then
                    If Rs.EOF = False Then
                        With UcDSN
                            .cmbDSN.Text = Convert.ToString(Rs("constring").Value).Split("|")(0)
                            .txtUserID.Text = Convert.ToString(Rs("constring").Value).Split("|")(1)
                            .txtPassword.Text = Convert.ToString(Rs("constring").Value).Split("|")(2)
                            gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
                        End With


                        cmbTable.Text = Convert.ToString(Rs("keycolumn").Value).Split(".")(0)
                        cmbColumn.Text = Convert.ToString(Rs("keycolumn").Value).Split(".")(1)

                        Dim valueColumn As String = IsNull(Rs("valuecolumn").Value)
                        Dim valueTable As String = ""

                        Try
                            valueTable = valueColumn.Split(".")(0)
                        Catch ex As Exception
                            valueTable = cmbTable.Text
                        End Try

                        If valueTable = cmbTable.Text Then
                            Try
                                cmbValue.Text = Convert.ToString(Rs("valuecolumn").Value).Split(".")(1)
                            Catch
                                cmbValue.Text = Convert.ToString(Rs("valuecolumn").Value)
                            End Try
                        Else
                            cmbValue.Text = "<Advanced>"
                        End If

                        gTables(0) = cmbTable.Text

                        If IsNull(Rs("table2").Value).Length > 0 Then _
                            gTables(1) = Rs("table2").Value

                    End If
                    Rs.Close()

                    Rs = clsMarsData.GetData("SELECT KeyParameter,AutoResume,ExpireCacheAfter FROM DynamicAttr WHERE " & _
                    "ReportID = " & nReportID)

                    For Each oItem As ListViewItem In lsvParameters.Items
                        cmbKey.Items.Add(oItem.Text)
                    Next

                    If Not Rs Is Nothing Then
                        If Rs.EOF = False Then
                            cmbKey.Text = Rs("keyparameter").Value
                            Me.chkAutoResume.Checked = Convert.ToBoolean(Convert.ToInt32(IsNull(Rs("autoresume").Value, 0)))
                            Me.txtCacheExpiry.Value = IsNull(Rs("expirecacheafter").Value, 0)
                        End If
                    End If

                    'lets not remove the key parameter from the parameter list and also set its type
                    For Each oitem As ListViewItem In lsvParameters.Items
                        If oitem.Text = cmbKey.Text Then

                            Try
                                If oitem.SubItems(1).Text.StartsWith("[") And oitem.SubItems(1).Text.EndsWith("]") Then
                                    optDynamic.Checked = True
                                Else
                                    optStatic.Checked = True
                                End If
                            Catch
                                optStatic.Checked = True
                            End Try

                            'oitem.Remove()
                        End If
                    Next
                End If

            End If

            'add data-driven schedule data
            Try
                If IsDataDriven = True Then
                    UcError.m_showFailOnOne = True
                    grpDynamicTasks.Enabled = True
                    Me.UcDest.isDataDriven = True

                    Me.txtUrl.ContextMenu = Me.mnuInserter

                    Text = "Data-Driven Schedule Properties"

                    Rs = clsMarsData.GetData("SELECT * FROM DataDrivenAttr WHERE ReportID =" & nReportID)

                    If Rs IsNot Nothing Then
                        If Rs.EOF = False Then
                            tbDataDriven.Visible = True
                            tabProperties.Tabs(9).Visible = False

                            Dim sQuery As String
                            Dim conString As String

                            sQuery = Rs("sqlquery").Value
                            conString = Rs("constring").Value

                            txtQuery.Text = sQuery

                            Try
                                UcError.chkFailonOne.Checked = IsNull(Rs("failonone").Value, 0)
                            Catch ex As Exception
                                UcError.chkFailonOne.Checked = False
                            End Try

                            clsMarsReport.populateDataDrivenCache(sQuery, conString, True)

                            Dim DSN, sUser, sPassword As String

                            DSN = conString.Split("|")(0)
                            sUser = conString.Split("|")(1)
                            sPassword = _DecryptDBValue(conString.Split("|")(2))

                            With UcDSNDD
                                .cmbDSN.Text = DSN
                                .txtUserID.Text = sUser
                                .txtPassword.Text = sPassword
                            End With

                            Try
                                Me.chkGroupReports.Checked = IsNull(Rs("groupreports").Value, 1)
                            Catch
                                Me.chkGroupReports.Checked = True
                            End Try

                            Dim rsDD As ADODB.Recordset = New ADODB.Recordset
                            Dim ddCon As ADODB.Connection = New ADODB.Connection

                            ddCon.Open(DSN, sUser, sPassword)

                            rsDD.Open(sQuery, ddCon)

                            Me.cmbDDKey.Items.Clear()

                            Dim z As Integer = 0

                            frmRDScheduleWizard.m_DataFields = New Hashtable

                            For Each fld As ADODB.Field In rsDD.Fields
                                frmRDScheduleWizard.m_DataFields.Add(z, fld.Name)
                                Me.cmbDDKey.Items.Add(fld.Name)
                                z += 1
                            Next

                            Try
                                Me.cmbDDKey.Text = IsNull(Rs("keycolumn").Value)
                            Catch : End Try

                            rsDD.Close()

                            ddCon.Close()
                            ddCon = Nothing
                        End If

                        Rs.Close()
                    End If
                End If
            Catch : End Try

            Rs = clsMarsData.GetData("SELECT * FROM ReportSnapshots WHERE ReportID =" & nReportID)

            Try
                If Rs.EOF = False Then
                    chkSnapshots.Checked = True
                    txtSnapshots.Value = Rs("keepsnap").Value
                End If

                Rs.Close()
            Catch : End Try

            UcDest.m_ParameterList = Me.m_ParametersList

            ucUpdate.IsLoaded = True

            Me.showTooltip = True
            cmdOK.Enabled = True


            Try
                'set up auto-complete data
                With Me.txtUrl
                    .AutoCompleteMode = AutoCompleteMode.SuggestAppend
                    .AutoCompleteSource = AutoCompleteSource.CustomSource
                    .AutoCompleteCustomSource = getUrlHistory()
                End With

                Me.ShowDialog()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub GetParameterAvailableValues()
        Try
            oRpt = New ReportServer.ReportingService

            oUI.BusyProgress(10, "Connecting to server...")

            oRpt.Url = clsMarsParser.Parser.ParseString(txtUrl.Text)

            If serverUser = "" Then
                oRpt.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials
            Else
                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential(serverUser, serverPassword)

                oRpt.Credentials = cred
            End If

            Dim sRDLPath As String = sAppPath & txtName.Text & ".rdl"
            Dim repDef() As Byte

            oUI.BusyProgress(40, "Getting server definition...")

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLoc.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            oUI.BusyProgress(70, "Loading values...")

            clsMarsReport._GetParameterValues(sRDLPath)

            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            parDefaults = New Hashtable

            oUI.BusyProgress(90, "Populating parameters...")

            If sPars IsNot Nothing Then
                For Each s As String In sPars

                    parDefaults.Add(s.Split("|")(0), sParDefaults(I))

                    I += 1
                Next
            End If

            UcDest.m_ParameterList = Me.m_ParametersList

            oUI.BusyProgress(, , True)
        Catch
            oUI.BusyProgress(, , True)
        End Try

    End Sub
    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDbLoc.Click
        Try
            Dim srsVersion As String = clsMarsReport.m_serverVersion(txtUrl.Text, serverUser, serverPassword)

            If srsVersion = "2007" Then
                oRpt = New ReportServer_2005.ReportingService2005
            Else
                oRpt = New ReportServer.ReportingService
            End If
            'Dim oServerLogin As frmRemoteLogin = New frmRemoteLogin
            'Dim sReturn As String() = oServerLogin.ServerLogin(ServerUser, ServerPassword)
            'Dim cred As System.Net.NetworkCredential

            'we check to see if we should put reportserver2005.asmx in the URL
            If srsVersion = "2007" Then txtUrl.Text = clsMarsParser.Parser.fixASMXfor2008(txtUrl.Text)

            oRpt.Url = clsMarsParser.Parser.ParseString(txtUrl.Text)

            'If sReturn Is Nothing Then
            'oRpt.Credentials = System.Net.CredentialCache.DefaultCredentials
            'Else
            'ServerUser = sReturn(0)
            'ServerPassword = sReturn(1)

            'cred = New System.Net.NetworkCredential(ServerUser, ServerPassword)

            'oRpt.Credentials = cred
            'End If

            Dim sPath As String

            Dim oServer As frmReportServer = New frmReportServer

            oServer.m_ReadOnly = True

            sPath = oServer.newGetReports(oRpt, clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text)

            'If Me.txtDBLoc.Text.Length = 0 Then

            '    showTooltip = False

            '    Dim oServerBrowse As frmReportServer = New frmReportServer

            '    AppStatus(True)

            '    sPath = oServerBrowse._GetReport(txtUrl.Text, cred)
            'Else
            '    sPath = txtDBLoc.Text
            'End If

            If sPath IsNot Nothing Then
                txtDBLoc.Text = sPath
                txtName.Text = sPath.Split("/")(sPath.Split("/").GetUpperBound(0))
                cmdOK.Enabled = True

                If txtDBLoc.Text.StartsWith("/") = False Then
                    txtDBLoc.Text = "/" & txtDBLoc.Text
                End If
            Else
                Return
            End If

            Dim sRDLPath As String = clsMarsReport.m_rdltempPath & txtDBLoc.Text & ".rdl"
            Dim repDef() As Byte

            If IO.File.Exists(sRDLPath) = True Then
                IO.File.Delete(sRDLPath)
            End If

            Dim fsReport As New System.IO.FileStream(sRDLPath, IO.FileMode.Create)

            repDef = oRpt.GetReportDefinition(txtDBLoc.Text)

            fsReport.Write(repDef, 0, repDef.Length)

            fsReport.Close()

            'get the parameters
            Dim sPars() As String
            Dim sParDefaults As String()
            Dim I As Integer = 0

            sPars = clsMarsReport._GetParameterFields(sRDLPath)
            sParDefaults = clsMarsReport._GetParameterValues(sRDLPath)

            lsvParameters.Items.Clear()

            parDefaults = New Hashtable

            If sPars IsNot Nothing Then
                For Each s As String In sPars
                    Dim oItem As ListViewItem = lsvParameters.Items.Add(s.Split("|")(0))

                    oItem.Tag = s.Split("|")(1)

                    Try
                        oItem.SubItems(1).Text = ""
                    Catch ex As Exception
                        oItem.SubItems.Add("")
                    End Try

                    Try
                        oItem.SubItems(2).Text = s.Split("|")(2)
                    Catch
                        oItem.SubItems.Add(s.Split("|")(2))
                    End Try

                    parDefaults.Add(oItem.Text, sParDefaults(I))

                    I += 1
                Next
            End If

            'ucDest.m_ParameterList = Me.m_ParametersList

            lsvDatasources.Items.Clear()

            Dim sData As ArrayList = New ArrayList

            sData = clsMarsReport._GetDatasources(sRDLPath)

            If sData IsNot Nothing Then
                lsvDatasources.Items.Clear()

                For Each s As Object In sData
                    Dim oItem As ListViewItem = lsvDatasources.Items.Add(s)

                    oItem.SubItems.Add("default")

                    oItem.ImageIndex = 0
                    oItem.Tag = 0
                Next
            End If

            'txtDesc.Text = clsMarsReport._GetReportDescription(sRDLPath)

            IO.File.Delete(sRDLPath)

            txtName.Focus()
            txtName.SelectAll()

            cmdOK.Enabled = True

            'save the Url as it has been validated
            logUrlHistory(Me.txtUrl.Text)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace), _
            "Please check your report server's web service URL and try again")

            If ex.Message.ToLower.Contains("cannot be found") Then
                txtName.Text = ""
                txtName.Text = ""
                'txtDesc.Text = ""
            End If
        End Try
    End Sub


    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        Try
            Dim SQL As String
            Dim sWhere As String
            Dim sPrinters As String = ""
            Dim nRepeat As Double

            'validate user entries
            If txtName.Text = "" Then
                tabProperties.SelectedTabIndex = 0
                ep.SetError(txtName, "Please enter the name of the schedule")
                OkayToClose = False
                txtName.Focus()
                Exit Sub
            ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, IsDynamic, IsDataDriven) = False Then
                tabProperties.SelectedTabIndex = 0
                ep.SetError(txtName, "A single schedule with this name already exists in this folder. Please use a different name.")
                OkayToClose = False
                txtName.Focus()
                Return
            ElseIf txtFolder.Text = "" Then
                tabProperties.SelectedTabIndex = 0
                ep.SetError(txtFolder, "Please select the containing folder")
                txtFolder.Focus()
                OkayToClose = False
                Exit Sub
            ElseIf txtDBLoc.Text = "" Then
                tabProperties.SelectedTabIndex = 0
                ep.SetError(txtDBLoc, "Please select the MS Access database")
                txtDBLoc.Focus()
                OkayToClose = False
                Exit Sub

            ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Text = "" Then
                tabProperties.SelectedTabIndex = 5
                ep.SetError(UcError.cmbAssumeFail, "Please select the amount before a schedule is assumed as failed")
                UcError.cmbAssumeFail.Focus()
                OkayToClose = False
                Exit Sub
            ElseIf UcBlank.ValidateEntries = False Then
                'If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                'UcBlank.txtAlertTo.Text = "" Then
                '    tabProperties.SelectedTabIndex = 5
                '    UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please specify the blank report alert recipients")
                '    UcBlank.txtAlertTo.Focus()
                '    OkayToClose = False
                '    Exit Sub
                'End If
                OkayToClose = False
                Return
            ElseIf UcDest.lsvDestination.Items.Count = 0 Then
                UcDest.ep.SetError(UcDest.lsvDestination, "Please add at least one destination")
                tabProperties.SelectedTabIndex = 4
                OkayToClose = False
                Return
            ElseIf UcDest.lsvDestination.CheckedItems.Count = 0 Then
                UcDest.ep.SetError(UcDest.lsvDestination, "Please enable at least one destination")
                tabProperties.SelectedTabIndex = 4
                OkayToClose = False
                Return
            ElseIf UcError.chkAssumeFail.Checked = True And UcError.cmbAssumeFail.Value = 0 Then
                ep.SetError(UcError.cmbAssumeFail, "Please provide a value greater than zero")
                tabProperties.SelectedTabIndex = 5
                UcError.cmbAssumeFail.Focus()
                OkayToClose = False
                Return
            ElseIf Me.ucUpdate.ValidateSchedule(Me.tabProperties, Me.ScheduleID) = False Then
                OkayToClose = False
                Return
            ElseIf IsDynamic = True Then
                If Me.chkStaticDest.Checked = False Then
                    If cmbTable.Text.Length = 0 Then
                        ep.SetError(cmbTable, "Please select the table that holds the value that must match the key parameter")
                        tabProperties.SelectedTab = Me.tbLinking
                        cmbTable.Focus()
                        Return
                    ElseIf cmbColumn.Text.Length = 0 Then
                        ep.SetError(cmbColumn, "Please select the column that holds the value that must match the key parameter")
                        tabProperties.SelectedTab = Me.tbLinking
                        cmbColumn.Focus()
                        Return
                    ElseIf cmbValue.Text.Length = 0 Then
                        ep.SetError(cmbValue, "Please select the column that will provide the report destination")
                        tabProperties.SelectedTab = Me.tbLinking
                        cmbValue.Focus()
                        Return
                    ElseIf cmbValue.Text = "<Advanced>" And sLink = String.Empty Then
                        ep.SetError(cmbValue, "Please set up the query for the value")
                        tabProperties.SelectedTab = Me.tbLinking
                        cmbValue.Focus()
                        Return
                    ElseIf UcDSN._Validate = False Then
                        tabProperties.SelectedTab = Me.tbLinking
                        UcDSN.cmbDSN.Focus()
                    End If
                End If
            End If

            sWhere = " WHERE (PackID = 0 OR PackID IS NULL) and ReportID = " & txtID.Text

            Dim dynamicTasks As Integer

            If optAll.Checked Then '//run once for each
                dynamicTasks = 0
            Else
                dynamicTasks = 1 '//run once for all
            End If


            SQL = "UPDATE ReportAttr SET " & _
                "DatabasePath = '" & clsMarsParser.Parser.ParseString(txtUrl.Text).Replace("'", "''") & "', " & _
                "ReportName = '" & txtName.Text.Replace("'", "''") & "', " & _
                "CachePath = '" & SQLPrepare(txtDBLoc.Text) & "'," & _
                "Parent = " & txtFolder.Tag & ", " & _
                "ReportTitle = '" & txtName.Text.Replace("'", "''") & "', " & _
                "retry = " & UcError.cmbRetry.Value & "," & _
                "AssumeFail = " & UcError.m_autoFailAfter & "," & _
                "CheckBlank = " & Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
                "SelectionFormula = '" & txtFormula.Text.Replace("'", "''") & "'," & _
                "RptUserID = '" & SQLPrepare(Me.serverUser) & "'," & _
                "RptPassword = '" & SQLPrepare(_EncryptDBValue(Me.serverPassword)) & "'," & _
                "RptServer = ''," & _
                "RptDatabase = ''," & _
                "UseLogin = " & 0 & "," & _
                "UseSavedData = " & 0 & ", " & _
                "AutoRefresh = " & 0 & "," & _
                "StaticDest = " & Convert.ToInt32(Me.chkStaticDest.Checked) & "," & _
                "DynamicTasks = " & dynamicTasks & "," & _
                "RptDatabaseType ='', " & _
                "AutoCalc =" & Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
                "RetryInterval =" & UcError.txtRetryInterval.Value & " "

            SQL &= sWhere

            clsMarsData.WriteData(SQL)

            'delete destinations if any have been marked to be deleted
            UcDest.CommitDeletions()

            If ucUpdate.chkRepeat.Checked = False Then
                nRepeat = 0
            Else
                nRepeat = ucUpdate.cmbRpt.Text
            End If


            With ucUpdate
                If .chkNoEnd.Checked = True Then
                    Dim d As Date

                    'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                    d = New Date(3004, Now.Month, Now.Day)

                    .EndDate.Value = d
                End If

                SQL = "UPDATE ScheduleAttr SET " & _
                    "Frequency = '" & .sFrequency & "', " & _
                    "EndDate = '" & .m_endDate & "', " & _
                    "StartDate = '" & .m_startDate & "'," & _
                    "NextRun = '" & .m_nextRun & "', " & _
                    "StartTime = '" & .m_startTime & "', " & _
                    "Repeat = " & Convert.ToInt32(.chkRepeat.Checked) & "," & _
                    "RepeatInterval = '" & nRepeat & "'," & _
                    "Status = " & Convert.ToInt32(.chkStatus.Checked) & "," & _
                    "RepeatUntil = '" & .m_repeatUntil & "', " & _
                    "Description = '" & SQLPrepare(txtDesc.Text) & "'," & _
                    "KeyWord = '" & SQLPrepare(txtKeyWord.Text) & "', " & _
                    "CalendarName = '" & SQLPrepare(.cmbCustom.Text) & "', " & _
                    "UseException = " & Convert.ToInt32(.chkException.Checked) & "," & _
                    "ExceptionCalendar ='" & SQLPrepare(.cmbException.Text) & "', " & _
                    "RepeatUnit ='" & .m_RepeatUnit & "' "
            End With

            SQL &= sWhere

            clsMarsData.WriteData(SQL)

            'save the blank report attributes

            clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE ReportID = " & txtID.Text)

            If UcBlank.chkBlankReport.Checked = True Then
                Dim sBlankType As String = "Ignore"

                If UcBlank.optAlert.Checked = True Then
                    sBlankType = "Alert"
                ElseIf UcBlank.optAlertandReport.Checked = True Then
                    sBlankType = "AlertandReport"
                ElseIf UcBlank.optIgnore.Checked = True Then
                    sBlankType = "Ignore"
                End If

                SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,ReportID,Subject,CustomQueryDetails) VALUES (" & _
                clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
                "'" & sBlankType & "'," & _
                "'" & UcBlank.txtAlertTo.Text.Replace("'", "''") & "'," & _
                "'" & UcBlank.txtBlankMsg.Text.Replace("'", "''") & "'," & _
                txtID.Text & "," & _
                "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
                "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

                clsMarsData.WriteData(SQL)
            End If

            If chkSnapshots.Checked = True Then
                clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE ReportID =" & txtID.Text)

                SQL = "INSERT INTO ReportSnapshots (SnapID,ReportID,KeepSnap) "

                SQL &= "VALUES (" & clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
                txtID.Text & "," & _
                txtSnapshots.Value & ")"

                clsMarsData.WriteData(SQL)
            Else
                clsMarsData.WriteData("DELETE FROM ReportSnapshots WHERE ReportID =" & txtID.Text)

                Dim rsd As ADODB.Recordset

                rsd = clsMarsData.GetData("SELECT SnapPath FROM SnapshotsAttr WHERE ReportID =" & txtID.Text)

                Try
                    Do While rsd.EOF = False
                        Try
                            System.IO.File.Delete(rsd("snappath").Value)
                        Catch : End Try

                        rsd.MoveNext()
                    Loop

                    rsd.Close()
                Catch : End Try

                clsMarsData.WriteData("DELETE FROM SnapshotsAttr WHERE ReportID =" & txtID.Text)
            End If

            'for dynamic data
            Dim ValueCol As String
            Dim LinkSQL As String

            If cmbValue.Text.ToLower = "<advanced>" Then
                ValueCol = "<Advanced>"
            Else
                ValueCol = cmbTable.Text & "." & cmbValue.Text
            End If

            SQL = "UPDATE DynamicAttr SET " & _
            "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "', " & _
            "Autoresume = " & Convert.ToInt32(Me.chkAutoResume.Checked) & "," & _
            "ExpireCacheAfter = " & Me.txtCacheExpiry.Value & _
            " WHERE ReportID =" & txtID.Text

            clsMarsData.WriteData(SQL)

            If ValueCol <> "<Advanced>" Then
                LinkSQL = "SELECT DISTINCT " & cmbTable.Text & "." & cmbValue.Text & " FROM " & cmbTable.Text

                SQL = "UPDATE DynamicLink SET " & _
                "KeyColumn ='" & SQLPrepare(cmbTable.Text & "." & cmbColumn.Text) & "'," & _
                "KeyParameter ='" & SQLPrepare(cmbKey.Text) & "'," & _
                "LinkSQL ='" & SQLPrepare(LinkSQL) & "'," & _
                "ConString ='" & SQLPrepare(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text & "|") & "'," & _
                "ValueColumn ='" & SQLPrepare(ValueCol) & "' WHERE " & _
                "ReportID =" & txtID.Text

                clsMarsData.WriteData(SQL)
            End If

            For Each item As ListViewItem In lsvDatasources.Items
                SQL = "SELECT * FROM ReportDatasource WHERE ReportID = " & txtID.Text & " AND " & _
                "DatasourceName = '" & SQLPrepare(item.Text) & "'"

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    If oRs.EOF = True Then
                        Dim sCols As String = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"
                        Dim sVals As String = clsMarsData.CreateDataID("reportdatasource", "datasourceid") & "," & _
                        txtID.Text & "," & _
                        "'" & SQLPrepare(item.Text) & "'," & _
                        "'default'," & _
                        "'<default>'"

                        SQL = "INSERT INTO ReportDatasource (" & sCols & ") VALUES (" & sVals & ")"

                        clsMarsData.WriteData(SQL)
                    End If

                    oRs.Close()
                End If
            Next

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            If IsDataDriven = True Then
                If Me.cmbDDKey.Text = "" Then
                    ep.SetError(Me.cmbDDKey, "Please select the key column")
                    Me.cmbDDKey.Focus()
                    Me.tabProperties.SelectedTab = Me.tbDataDriven
                    Return
                End If

                'check if the selected key column contains unique values only. Going to do this by reading from the data cache
                For Each row As DataRow In clsMarsReport.m_dataDrivenCache.Rows
                    Dim dupKey As String = IsNull(row(cmbDDKey.Text), "")
                    Dim rows() As DataRow

                    'use select to get all the rows where the key column matches the dupKey
                    rows = clsMarsReport.m_dataDrivenCache.Select(Me.cmbDDKey.Text & " = '" & dupKey & "'")

                    If rows IsNot Nothing Then
                        If rows.Length > 1 Then
                            ep.SetError(cmbDDKey, "The selected key column contains duplicate values of '" & dupKey & "'. Please select a column that contains UNIQUE values only.")
                            cmbDDKey.Focus()
                            Me.tabProperties.SelectedTab = Me.tbDataDriven
                            Return
                        End If
                    End If
                Next

                SQL = "UPDATE DataDrivenAttr SET " & _
                "SQLQuery = '" & SQLPrepare(txtQuery.Text) & "'," & _
                "ConString = '" & SQLPrepare(Me.UcDSNDD.m_conString) & "', " & _
                "FailOnOne = " & Convert.ToInt32(UcError.chkFailonOne.Checked) & ", " & _
                "GroupReports = " & Convert.ToInt32(Me.chkGroupReports.Checked) & ", " & _
                "KeyColumn = '" & SQLPrepare(Me.cmbDDKey.Text) & "' " & _
                "WHERE ReportID =" & txtID.Text

                clsMarsData.WriteData(SQL)
            End If

            Me.oTask.CommitDeletions()

            cmdApply.Enabled = False

            OkayToClose = True

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, _
            "frmSingleProp.cmdApply_Click", _GetLineNumber(ex.StackTrace))
            OkayToClose = False
        End Try
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDBLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles txtDBLoc.TextChanged
        If txtDBLoc.Text.Length > 0 And showTooltip = True Then
            Dim info As New DevComponents.DotNetBar.SuperTooltipInfo
            Dim msg As String

            msg = "Please be aware that by entering the report path manually, SQL-RD will not " & _
            "connect to your report server to list your reports. " & vbCrLf & _
            "If you would like SQL-RD to " & _
            "connect to the report server and list your reports, remove all text from this field."

            With info
                .Color = DevComponents.DotNetBar.eTooltipColor.Lemon
                .CustomSize = New System.Drawing.Size(400, 100)
                .BodyText = msg
                .HeaderText = "For your information:"
                .HeaderVisible = True
                .FooterVisible = False
            End With

            Me.SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, info)

            SuperTooltip1.ShowTooltip(Me.cmdDbLoc)

            Me.cmdDbLoc.Image = Me.ImageList1.Images(1)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = False
        ElseIf Me.txtDBLoc.Text.Length = 0 Then
            SuperTooltip1.SetSuperTooltip(Me.cmdDbLoc, Nothing)

            Me.cmdDbLoc.Image = ImageList1.Images(2)
            Me.cmdDbLoc.ImageAlign = ContentAlignment.MiddleCenter

            showTooltip = True
        End If

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub



    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        Dim SQL As String
        Dim lsv As ListViewItem

        cmdApply_Click(sender, e)

        If OkayToClose = False Then Exit Sub

        Try
            If bgWorker.IsBusy = True Then bgWorker.CancelAsync()
        Catch : End Try

        Dim oUI As New clsMarsUI

        oUI.RefreshView(oWindow(nWindowCurrent))

        If IsDynamic = True Then
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.DYNAMIC, clsMarsAudit.AuditAction.EDIT)
        ElseIf IsBursting = True Then
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.BURST, clsMarsAudit.AuditAction.EDIT)
        Else
            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.SINGLES, clsMarsAudit.AuditAction.EDIT)
        End If

        Me.Close()
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmbTable.Enabled = True
            cmbColumn.Enabled = True
            cmbValue.Enabled = True

            With UcDSN
                gsCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & .txtPassword.Text
            End With

            oData.GetTables(cmbTable, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)

            oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
                UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

            cmbValue.Items.Add("<Advanced>")
            cmbValue.Sorted = True
        Else
            _ErrorHandle("Could not connect to the selected data source", -2147636225, "ucDSN._Validate(frmDynamicSchedule.cmdValidate_Click)", 1912)
            cmbTable.Enabled = False
            cmbColumn.Enabled = False
            cmbValue.Enabled = False
        End If
    End Sub


    Private Sub cmbTable_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTable.SelectedIndexChanged
        oData.GetColumns(cmbColumn, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)


        oData.GetColumns(cmbValue, UcDSN.cmbDSN.Text, cmbTable.Text, _
        UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)

        cmbValue.Items.Add("<Advanced>")

        cmbValue.Sorted = True
    End Sub

    Private Sub cmbValue_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbValue.SelectedIndexChanged
        If cmbValue.Text.ToLower = "<advanced>" Then
            Dim oForm As New frmDynamicAdvanced

            oForm._AdvancedDynamicEdit(txtID.Text, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
            UcDSN.txtPassword.Text, cmbTable.Text & "." & cmbColumn.Text, cmbKey.Text)
        End If
    End Sub

    Private Sub cmdParameters_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                .Item("ParType") = item.Tag
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, parDefaults, txtID.Text, txtUrl.Text, Me.m_serverParametersTable, Me.txtDBLoc.Text, serverUser, serverPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub

    Private Sub optStatic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optStatic.Click
        Dim sVal As String = ""
        Try
10:         If cmbKey.Text.Length = 0 Then
20:             ep.SetError(cmbKey, "Please select the key parameter")
30:             cmbKey.Focus()
40:             Return
            End If

50:         Dim oGet As New frmKeyParameter

60:         For Each oItem As ListViewItem In lsvParameters.Items
70:             If oItem.Text = cmbKey.Text Then
80:                 Try
90:                     sVal = oItem.SubItems(1).Text
                    Catch : End Try
100:                Exit For
                End If
110:        Next

120:        Try
130:            clsMarsReport.GetReportParameters(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, False, parDefaults)
            Catch : End Try

140:        sVal = oGet._GetParameterValue(cmbKey.Text, sVal, parDefaults(cmbKey.Text), Me.m_serverParametersTable)

150:        If sVal.Length = 0 Then Return

160:        For Each oItem As ListViewItem In lsvParameters.Items
170:            If oItem.Text = cmbKey.Text Then

180:                clsMarsData.WriteData("UPDATE ReportParameter SET ParValue = '" & SQLPrepare(sVal) & "' WHERE " & _
          "ReportID = " & txtID.Text & " AND ParName LIKE  '" & SQLPrepare(cmbKey.Text) & "'")

190:                Try
200:                    oItem.SubItems(1).Text = sVal
210:                    Exit For
220:                Catch
230:                    oItem.SubItems.Add(sVal)
240:                    Exit For
                    End Try
                End If
250:        Next

260:        clsMarsData.WriteData("DELETE FROM DynamicAttr WHERE ReportID=" & txtID.Text)

            Dim sCols As String
            Dim sVals As String
            Dim SQL As String

270:        sCols = "DynamicID,KeyParameter,KeyColumn,ConString," & _
          "Query,ReportID"

280:        sVals = clsMarsData.CreateDataID("dynamicattr", "dynamicid") & "," & _
          "'" & SQLPrepare(cmbKey.Text) & "'," & _
          "'" & SQLPrepare(sVal) & "'," & _
          "'_'," & _
          "'_'," & _
          txtID.Text

290:        SQL = "INSERT INTO DynamicAttr(" & sCols & ") VALUES (" & sVals & ")"

300:        clsMarsData.WriteData(SQL)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
        End Try
    End Sub


    Private Sub optDynamic_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDynamic.Click
        Dim oPar As New frmDynamicParameter
        Dim sParameter As String

        If cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the key parameter")
            cmbKey.Focus()
            Return
        End If

        sParameter = oPar.AddParameterDynamicQuery(cmbKey.Text, "report", txtID.Text)

        If sParameter.Length = 0 Then Return

        For Each oItem As ListViewItem In lsvParameters.Items
            If oItem.Text = cmbKey.Text Then
                oItem.SubItems(1).Text = sParameter

                clsMarsData.WriteData("UPDATE ReportParameter SET ParValue = '" & SQLPrepare(sParameter) & "' WHERE " & _
                "ReportID = " & txtID.Text & " AND ParName LIKE  '" & SQLPrepare(cmbKey.Text) & "'")

                Exit For
            End If
        Next
    End Sub

    Private Sub lsvParameters_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvParameters.DoubleClick
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text
                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try

                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try

                .Item("ParType") = item.Tag
            End With

            dtParameters.Rows.Add(Row)
        Next

        dtParameters = oPar.SetParameters(dtParameters, parDefaults, txtID.Text, lsvParameters.SelectedItems(0).Text, m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, serverUser, serverPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next

    End Sub


    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            Try
                Dim nID As Integer

                nID = oItem.Tag

                If clsMarsData.WriteData _
                ("DELETE FROM SnapshotsAttr WHERE AttrID =" & nID) = True Then

                    System.IO.File.Delete(oItem.SubItems(1).Text)

                    oItem.Remove()
                End If
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, _
                "frmSingleProp.cmdDelete_Click", _GetLineNumber(ex.StackTrace))
            End Try
        Next
    End Sub

    Private Sub cmdView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdView.Click
        Dim oProc As Process

        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            Try
                oProc = New Process

                oProc.StartInfo.FileName = oItem.SubItems(1).Text

                oProc.Start()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, _
                "frmSingleProp.cmdView_Click", _GetLineNumber(ex.StackTrace))
            End Try
        Next
    End Sub

    Private Sub ShowSnapshots()

        Dim oRs As ADODB.Recordset
        Dim SQL As String

        If lsvSnapshots.Items.Count > 0 Then Return

        If chkSnapshots.Checked = True Then
            SQL = "SELECT * FROM SnapshotsAttr WHERE ReportID = " & txtID.Text

            oRs = clsMarsData.GetData(SQL)

            Try
                Do While oRs.EOF = False

                    Dim oItem As New ListViewItem

                    oItem.Text = oRs("datecreated").Value
                    oItem.SubItems.Add(oRs("snappath").Value)

                    oItem.Tag = oRs("attrid").Value
                    oItem.ImageIndex = 0

                    lsvSnapshots.Items.Add(oItem)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            Catch ex As Exception
                _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                _GetLineNumber(ex.StackTrace))
            End Try
        End If
    End Sub

    Private Sub chkSnapshots_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshots.CheckedChanged
        If chkSnapshots.Checked = True Then
            If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.sa8_Snapshots) Then
                _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
                chkSnapshots.Checked = False
                Return
            End If
        End If

        txtSnapshots.Enabled = chkSnapshots.Checked
    End Sub

    Private Sub cmdExecute_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExecute.Click
        Dim oSnap As New clsMarsSnapshots

        For Each oItem As ListViewItem In lsvSnapshots.SelectedItems
            oSnap.ExecuteSnapshot(txtID.Text, oItem.SubItems(1).Text)
        Next

    End Sub



    Private Sub cmdTestRpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles cmdTest.Click
        clsMarsReport.oReport.ViewReport(txtID.Text, True, serverUser, serverPassword, True)
    End Sub



    Private Sub cmdRequery_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRequery.Click
        If MessageBox.Show("Requery the report for parameters? This will remove the current parameters and respective values", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            clsMarsData.WriteData("DELETE FROM ReportParameter WHERE ReportID =" & txtID.Text)

            clsMarsReport.GetReportParameters(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, True, parDefaults, lsvParameters)

            UcBlank.m_ParametersList = New ArrayList

            For Each it As ListViewItem In Me.lsvParameters.Items
                UcBlank.m_ParametersList.Add(it.Text)
            Next
        End If

    End Sub

    Private Sub cmdClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClear.Click
        If MessageBox.Show("Delete all history for this schedule?", Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim oData As New clsMarsData

            clsMarsData.WriteData("DELETE FROM ScheduleHistory WHERE ReportID =" & txtID.Text, False)

            clsMarsData.WriteData("DELETE FROM HistoryDetailAttr WHERE HistoryID NOT IN (SELECT HistoryID FROM ScheduleHistory) AND HistoryID NOT IN (SELECT HistoryID FROM EventHistory)")

            _Delay(1)

            Dim res As DialogResult = MessageBox.Show("Would you like to remove this schedule's report duration information?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If res = Windows.Forms.DialogResult.Yes Then
                clsMarsData.WriteData("DELETE FROM ReportDuration WHERE ReportID =" & txtID.Text)
                clsMarsData.WriteData("DELETE FROM ReportDurationTracker WHERE ReportID =" & txtID.Text)
            End If

            cmdRefresh_Click(sender, e)
        End If
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        If txtID.Text.Length = 0 Then Return

        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        If optDesc.Checked = True Then
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, " DESC ")
        Else
            oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, " ASC ")
        End If

        grSort.Enabled = True
    End Sub

    Private Sub chkStaticDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkStaticDest.CheckedChanged
        GroupBox6.Enabled = Not chkStaticDest.Checked

        UcDest.StaticDest = chkStaticDest.Checked

    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        If optStatic.Checked = True Then
            optStatic_Click(sender, e)
        ElseIf optDynamic.Checked = True Then
            optDynamic_Click(sender, e)
        End If
    End Sub



    Private Sub cmdTestLink_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTestLink.Click

        Dim SQL As String
        Dim ParValue As String
        Dim LinkSQL As String
        Dim LinkCon As String
        Dim oRs As ADODB.Recordset
        Dim sDSN As String
        Dim sUser As String
        Dim sPassword As String

        ParValue = InputBox("Please provide a sample value for the parameter", Application.ProductName)

        If ParValue.Length = 0 Then Return

        Try
            Dim oLinkCon As New ADODB.Connection

            If cmbValue.Text = "<Advanced>" Then
                SQL = "SELECT * FROM DynamicLink WHERE ReportID= " & txtID.Text

                oRs = clsMarsData.GetData(SQL)

                If oRs.EOF = False Then
                    LinkSQL = oRs("linksql").Value
                    LinkCon = oRs("constring").Value
                End If

                sDSN = LinkCon.Split("|")(0)
                sUser = LinkCon.Split("|")(1)
                sPassword = LinkCon.Split("|")(2)

                oLinkCon.Open(sDSN, sUser, sPassword)

                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, oRs("keycolumn").Value, ParValue)

                If LinkSQL.ToLower.IndexOf("where") > -1 Then
                    LinkSQL &= " AND " & oRs("keycolumn").Value & " = " & ParValue
                Else
                    LinkSQL &= " WHERE " & oRs("keycolumn").Value & " = " & ParValue
                End If

                oRs.Close()
            Else
                sDSN = UcDSN.cmbDSN.Text
                sUser = UcDSN.txtUserID.Text
                sPassword = UcDSN.txtPassword.Text

                oLinkCon.Open(sDSN, sUser, sPassword)

                LinkSQL = "SELECT " & cmbValue.Text & " FROM " & cmbTable.Text


                ParValue = clsMarsData.ConvertToFieldType(LinkSQL, oLinkCon, cmbColumn.Text, ParValue)

                LinkSQL &= " WHERE " & cmbColumn.Text & " = " & ParValue

            End If

            oRs = New ADODB.Recordset

            oRs.Open(LinkSQL, oLinkCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

            Dim oResults As New frmDBResults

            oResults._ShowResults(oRs)

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace), LinkSQL)
        End Try

    End Sub


    Private Sub tabProperties_SelectedTabChanged(ByVal sender As Object, ByVal e As DevComponents.DotNetBar.TabStripTabChangedEventArgs) Handles tabProperties.SelectedTabChanged
        Select Case e.NewTab.Text.ToLower
            Case "history"
                Me.cmdRefresh_Click(sender, e)
            Case "snapshots"
                ShowSnapshots()
            Case "datasources"
                'add datasources from the report that have default credentials in case user wants to set them up
                'clsMarsReport.GetReportDatasources(txtUrl.Text, serverUser, serverPassword, txtDBLoc.Text, txtName.Text, True, Me.lsvDatasources)
        End Select
    End Sub

    Private Sub lsvDatasources_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvDatasources.DoubleClick
        Dim oSet As New frmSetTableLogin
        Dim oItem As ListViewItem
        Dim sValues() As String

        If lsvDatasources.SelectedItems.Count = 0 Then Return

        oItem = lsvDatasources.SelectedItems(0)

        sValues = oSet.SaveLoginInfo(txtID.Text, oItem.Text, oItem.Tag)

        If Not sValues Is Nothing Then
            oItem.SubItems(1).Text = sValues(0)
            oItem.Tag = sValues(1)
        End If
    End Sub


    Private Sub ClearToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearToolStripMenuItem.Click
        If lsvDatasources.SelectedItems.Count = 0 Then Return
        Dim oData As New clsMarsData

        Dim oItem As ListViewItem = lsvDatasources.SelectedItems(0)

        Dim nDataID As Integer = oItem.Tag

        If nDataID > 0 Then
            If clsMarsData.WriteData("DELETE FROM ReportDatasource WHERE DatasourceID =" & nDataID) = True Then
                oItem.SubItems(1).Text = "default"
            End If
        End If
    End Sub
    Private Sub lsvDatasources_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvDatasources.KeyUp
        If e.KeyCode = Keys.Enter Then
            Me.lsvDatasources_DoubleClick(sender, e)
        End If
    End Sub

    Private Sub cmdParameters_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdParameters.Click
        Dim I As Integer = 1

        If lsvParameters.Items.Count = 0 Then
            MessageBox.Show("This report does not have any parameters", Application.ProductName, _
            MessageBoxButtons.OK, MessageBoxIcon.Information)
            Return
        End If

        Dim oPar As New frmParameters

        Dim Row As DataRow

        dtParameters = New DataTable("Parameters")

        With dtParameters
            .Columns.Add("ParName")
            .Columns.Add("ParValue")
            .Columns.Add("ParType")
            .Columns.Add("MultiValue")
        End With

        For Each item As ListViewItem In lsvParameters.Items
            Row = dtParameters.NewRow

            With Row
                .Item("ParName") = item.Text

                Try
                    .Item("ParValue") = item.SubItems(1).Text
                Catch
                    .Item("ParValue") = String.Empty
                End Try


                Try
                    .Item("MultiValue") = item.SubItems(2).Text
                Catch
                    .Item("MultiValue") = "false"
                End Try

                .Item("ParType") = item.Tag
            End With

            dtParameters.Rows.Add(Row)
        Next

        Try
            clsMarsReport.GetReportParameters(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, False, parDefaults)
        Catch : End Try

        dtParameters = oPar.SetParameters(dtParameters, parDefaults, txtID.Text, , m_serverParametersTable, txtUrl.Text, Me.txtDBLoc.Text, serverUser, serverPassword)

        If dtParameters Is Nothing Then Return

        lsvParameters.Items.Clear()

        For Each dataRow As DataRow In dtParameters.Rows
            Dim oItem As ListViewItem = New ListViewItem

            oItem.Text = dataRow.Item("ParName")

            Try
                oItem.SubItems(1).Text = dataRow.Item("ParValue")
            Catch
                oItem.SubItems.Add(dataRow.Item("ParValue"))
            End Try

            Try
                oItem.SubItems(2).Text = dataRow.Item("MultiValue")
            Catch ex As Exception
                oItem.SubItems.Add(dataRow.Item("MultiValue"))
            End Try

            oItem.Tag = dataRow.Item("ParType")

            lsvParameters.Items.Add(oItem)

        Next
    End Sub

    Private Sub optAsc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAsc.CheckedChanged
        If txtID.Text.Length = 0 Then Return

        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, " ASC ")

        grSort.Enabled = True
    End Sub

    Private Sub optDesc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles optDesc.CheckedChanged
        If txtID.Text.Length = 0 Then Return

        grSort.Enabled = False

        Dim oSchedule As New clsMarsScheduler

        oSchedule.DrawScheduleHistory(tvHistory, clsMarsScheduler.enScheduleType.REPORT, txtID.Text, " DESC ")

        grSort.Enabled = True
    End Sub

    Private Sub btnRefreshDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshDS.Click
        clsMarsReport.GetReportDatasources(clsMarsParser.Parser.ParseString(txtUrl.Text), serverUser, serverPassword, txtDBLoc.Text, txtName.Text, True, Me.lsvDatasources)
    End Sub

    Private Sub tvHistory_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvHistory.AfterSelect

    End Sub

    Private Sub cmdTest2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest2.Click
        cmdTestRpt_Click(Nothing, Nothing)
    End Sub

    Private Sub bgWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgWorker.DoWork
        Try
            m_serverParametersTable = clsMarsReport.oReport.getserverReportParameters(Me.txtUrl.Text, Me.txtDBLoc.Text, serverUser, serverPassword, "")

            Me.GetParameterAvailableValues()
        Catch
            oUI.BusyProgress(, , True)
        End Try
    End Sub

    Private Sub bgWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgWorker.RunWorkerCompleted

    End Sub

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        grpQuery.Enabled = UcDSNDD._Validate
    End Sub

    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim queryBuilder As frmDynamicParameter = New frmDynamicParameter

        Dim sVals(1) As String
        Dim sCon As String = ""
        Dim temp As String = txtQuery.Text

        If UcDSNDD.cmbDSN.Text.Length > 0 Then
            sCon = UcDSNDD.cmbDSN.Text & "|" & UcDSNDD.txtUserID.Text & "|" & UcDSNDD.txtPassword.Text & "|"
        End If

        queryBuilder.m_showStar = True
        queryBuilder.Label1.Text = "Please select the table that holds the required data"

        sVals = queryBuilder.ReturnSQLQuery(sCon, txtQuery.Text)

        If sVals Is Nothing Then Return

        sCon = sVals(0)
        txtQuery.Text = sVals(1)

        UcDSNDD.cmbDSN.Text = sCon.Split("|")(0)
        UcDSNDD.txtUserID.Text = sCon.Split("|")(1)
        UcDSNDD.txtPassword.Text = sCon.Split("|")(2)

        Dim oRs As ADODB.Recordset = New ADODB.Recordset
        Dim oCon As ADODB.Connection = New ADODB.Connection

        oCon.Open(UcDSNDD.cmbDSN.Text, UcDSNDD.txtUserID.Text, UcDSNDD.txtPassword.Text)

        oRs.Open(txtQuery.Text, oCon, ADODB.CursorTypeEnum.adOpenStatic)

        Me.cmbDDKey.Items.Clear()

        Dim I As Integer = 0

        frmRDScheduleWizard.m_DataFields = New Hashtable

        For Each fld As ADODB.Field In oRs.Fields
            frmRDScheduleWizard.m_DataFields.Add(I, fld.Name)
            Me.cmbDDKey.Items.Add(fld.Name)
            I += 1
        Next

        Dim tmpCon As String = ""

        With UcDSNDD
            tmpCon = .cmbDSN.Text & "|" & .txtUserID.Text & "|" & _EncryptDBValue(.txtPassword.Text)
        End With

        If txtQuery.Text.ToLower <> temp.ToLower Then
            clsMarsReport.populateDataDrivenCache(txtQuery.Text, tmpCon, True)
        End If

        oRs.Close()
        oCon.Close()

        oRs = Nothing
        oCon = Nothing
    End Sub

    Private Sub chkAutoResume_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoResume.CheckedChanged
        txtCacheExpiry.Enabled = Me.chkAutoResume.Checked
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Undo()
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            Dim txt As TextBox = CType(Me.ActiveControl, TextBox)

            txt.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As New frmInserter(99999)
        oInsert.m_EventBased = False
        oInsert.GetConstants(Me)
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        Dim res As DialogResult = MessageBox.Show("Are you sure you would like to clear all the Dynamic linking information for this schedule?", _
        Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If res = Windows.Forms.DialogResult.Yes Then
            clsMarsData.WriteData("DELETE FROM DynamicLink WHERE ReportID=" & Me.txtID.Text)

            With UcDSN
                .cmbDSN.Text = ""
                .txtPassword.Text = ""
                .txtUserID.Text = ""
            End With

            Me.cmbTable.Text = ""
            cmbValue.Text = ""
            cmbColumn.Text = ""
        End If
    End Sub

    Private Sub chkGroupReports_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkGroupReports.CheckedChanged
        Me.UcDest.disableEmbed = chkGroupReports.Checked
    End Sub

    Private Sub lsvParameters_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvParameters.SelectedIndexChanged

    End Sub

    Private Sub cmbDDKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDDKey.SelectedIndexChanged
        ep.SetError(Me.cmbDDKey, "")
        cmdApply.Enabled = True
    End Sub
End Class
