Public Class frmAutoScheduleWizard
    Inherits System.Windows.Forms.Form
    Dim nStep As Integer = 0
    Dim oData As New clsMarsData
    Const S1 As String = "Step 1: Schedule Name"
    Const S2 As String = "Step 2: Schedule Setup"
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Const S3 As String = "Step 3: Custom Tasks"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAutoScheduleWizard))
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Step2 = New System.Windows.Forms.Panel
        Me.ucSet = New sqlrd.ucScheduleSet
        Me.Step3 = New System.Windows.Forms.Panel
        Me.UcTasks = New sqlrd.ucTasks
        Me.lblStep = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Footer1 = New WizardFooter.Footer
        Me.DividerLabel1 = New sqlrd.DividerLabel
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(360, 436)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(75, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(280, 436)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(200, 436)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(75, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(384, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 48)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.SystemColors.Control
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label3)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Location = New System.Drawing.Point(0, 75)
        Me.Step1.Name = "Step1"
        Me.Step1.Size = New System.Drawing.Size(440, 341)
        Me.Step1.TabIndex = 14
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(8, 120)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(360, 88)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 104)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 26
        Me.Label3.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 216)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 25
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 232)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(360, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(312, 80)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "...."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 80)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(288, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(360, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Schedule Name"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.ucSet)
        Me.Step2.Location = New System.Drawing.Point(0, 75)
        Me.Step2.Name = "Step2"
        Me.Step2.Size = New System.Drawing.Size(440, 341)
        Me.Step2.TabIndex = 15
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(8, 4)
        Me.ucSet.m_RepeatUnit = "hours"
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(424, 333)
        Me.ucSet.TabIndex = 0
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.SystemColors.Control
        Me.Step3.Controls.Add(Me.UcTasks)
        Me.Step3.Location = New System.Drawing.Point(0, 75)
        Me.Step3.Name = "Step3"
        Me.Step3.Size = New System.Drawing.Size(440, 341)
        Me.Step3.TabIndex = 16
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.SystemColors.Control
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(8, 8)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(400, 320)
        Me.UcTasks.TabIndex = 0
        '
        'lblStep
        '
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(336, 32)
        Me.lblStep.TabIndex = 4
        Me.lblStep.Text = "Step 3: Custom Actions"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(360, 436)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(75, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(442, 64)
        Me.Panel1.TabIndex = 18
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 418)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(456, 16)
        Me.Footer1.TabIndex = 21
        Me.Footer1.TabStop = False
        '
        'DividerLabel1
        '
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 64)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(442, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 22
        '
        'frmAutoScheduleWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(442, 468)
        Me.ControlBox = False
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmAutoScheduleWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Automation Schedule Wizard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmAutoScheduleWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        FormatForWinXP(Me)
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        txtName.Focus()
        Step1.BringToFront()
        lblStep.Text = S1

        clsMarsData.DataItem.CleanDB()

        UcTasks.ShowAfterType = False
        UcTasks.oAuto = True
        UcTasks.lsvTasks.HeaderStyle = ColumnHeaderStyle.None

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 0
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please set a name for this schedule")
                    txtName.Focus()
                    Return
                ElseIf txtFolder.Text.Length = 0 Then
                    ep.SetError(txtFolder, "Please select the parent folder for the schedule")
                    txtFolder.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.AUTOMATION) = False Then
                    ep.SetError(txtName, "An automation schedule " & _
                    "already exist with this name in this folder")
                    txtName.Focus()
                    Return
                End If

                cmdBack.Enabled = True
                Step1.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                ucSet.Focus()
                lblStep.Text = S2
            Case 1
                'If ucSet.StartDate.Value > ucSet.EndDate.Value Then
                '    ep.SetError(ucSet.EndDate, "Please select a valid date range")
                '    Return
                'ElseIf ucSet.chkRepeat.Checked = True Then
                '    If ucSet.cmbRpt.Text = 0 Then
                '        ep.SetError(ucSet.cmbRpt, "Please select the repeat interval")
                '        Return
                '    ElseIf ucSet.RunAt.Value > ucSet.RepeatUntil.Value Then
                '        ep.SetError(ucSet.RunAt, "The execution time cannot be after the repeat until time")
                '        Return
                '    End If
                'ElseIf ucSet.optCustom.Checked = True And _
                '    (ucSet.cmbCustom.Text.Length = 0 Or _
                '    ucSet.cmbCustom.Text = "[New]") Then
                '    ep.SetError(ucSet.cmbCustom, "Please select a valid calendar")
                '    ucSet.cmbCustom.Focus()
                '    Return
                'ElseIf ucSet.chkException.Checked And (ucSet.cmbException.Text.Length = 0 Or ucSet.cmbException.Text = "[New...]") Then
                '    ep.SetError(ucSet.cmbException, "Please select an exception calendar")
                '    ucSet.cmbException.Focus()
                '    Return
                'End If

                If ucSet.ValidateSchedule = False Then
                    Return
                End If
                Step2.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                cmdNext.Visible = False
                cmdFinish.Visible = True
                UcTasks.Focus()
                lblStep.Text = S3
            Case 2

        End Select

        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        ep.SetError(txtFolder, "")
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 2
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                cmdNext.Visible = True
                cmdFinish.Visible = False
                lblStep.Text = S2
            Case 1
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                cmdBack.Enabled = False
                lblStep.Text = S1
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdFinish.Click

        If UcTasks.lsvTasks.Items.Count = 0 Then
            ep.SetError(UcTasks, "Please add a custom action")
            Return
        End If

        cmdFinish.Enabled = False

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim ExecuteOk As Boolean
        Dim nID As Integer
        Dim AutoID As Integer

        AutoID = clsMarsData.CreateDataID("automationattr", "autoid")

        sCols = "AutoID, AutoName, Parent,Owner"

        sVals = AutoID & ", '" & SQLPrepare(txtName.Text) & "'," & _
            txtFolder.Tag & "," & _
            "'" & gUser & "'"

        SQL = "INSERT INTO AutomationAttr (" & sCols & ") VALUES (" & sVals & ")"

        ExecuteOk = clsMarsData.WriteData(SQL)


        If ExecuteOk = False Then GoTo RollbackTrans

        Dim ScheduleID As Int64 = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

        sCols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,AutoID," & _
                "Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit"

        With ucSet
            If .chkNoEnd.Checked = True Then
                Dim d As Date

                'd = New Date(Now.Year + 1000, Now.Month, Now.Day)
                d = New Date(3004, Now.Month, Now.Day)

                .EndDate.Value = d
            End If

            sVals = ScheduleID & "," & _
                    "'" & .sFrequency & "'," & _
                    "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & ConDate(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                    "'" & CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkRepeat.Checked) & "," & _
                    "'" & Convert.ToString(.cmbRpt.Value).Replace(",", ".") & "'," & _
                    "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                    Convert.ToInt32(.chkStatus.Checked) & "," & _
                    AutoID & "," & _
                    "'" & SQLPrepare(txtDesc.Text) & "'," & _
                    "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                    "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                    Convert.ToInt32(.chkException.Checked) & "," & _
                    "'" & SQLPrepare(.cmbException.Text) & "'," & _
                    "'" & ucSet.m_RepeatUnit & "'"
        End With

        SQL = "INSERT INTO ScheduleAttr (" & sCols & ") VALUES (" & sVals & ")"

        ExecuteOk = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
            "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")


        If ExecuteOk = False Then GoTo RollbackTrans

        UcTasks.CommitDeletions()

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        If gUser.ToLower <> "admin" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nID, gUser, clsMarsUsers.enViewType.ViewAutomation)
        End If

        Close()

        Dim oUI As New clsMarsUI

        oUI.RefreshView(oWindow(nWindowCurrent))

        Try
            Dim nCount As Integer
            nCount = txtFolder.Text.Split("\").GetUpperBound(0)

            Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

            oUI.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))
        Catch

        End Try

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.AUTOMATION, clsMarsAudit.AuditAction.CREATE)
        Return
RollbackTrans:
        clsMarsData.WriteData("DELETE FROM AutomationAttr WHERE AutoID =" & nID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE AutoID =" & nID)
        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID =" & ScheduleID)
        clsMarsData.WriteData("DELETE FROM ScheduleOptions WHERE ScheduleID =" & ScheduleID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click

        oData.CleanDB()

        Me.Close()
    End Sub
End Class
