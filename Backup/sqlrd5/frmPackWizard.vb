Imports Microsoft.Win32
Public Class frmPackWizard
    Inherits System.Windows.Forms.Form
    Dim nStep As Int32
    Dim sFrequency As String
    Dim oErr As clsMarsUI = New clsMarsUI
    Dim oUI As New clsMarsUI
    Dim oField As TextBox

    Const S1 As String = "Step 1: Package Setup"
    Const S2 As String = "Step 2: Schedule Setup"
    Const S3 As String = "Step 3: Destination Setup"
    Const S4 As String = "Step 4: Add Reports"
    Const S5 As String = "Step 5: Exception Handling"
    Dim HasCancelled As Boolean = True
    Friend WithEvents DividerLabel1 As sqlrd.DividerLabel
    Friend WithEvents chkSnapshot As System.Windows.Forms.CheckBox
    Friend WithEvents txtSnapshots As System.Windows.Forms.NumericUpDown
    Friend WithEvents DividerLabel3 As sqlrd.DividerLabel
    Friend WithEvents DividerLabel2 As sqlrd.DividerLabel
    Friend WithEvents txtAdjustStamp As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSignature As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAttachment As System.Windows.Forms.MenuItem
    Friend WithEvents UcError As sqlrd.ucErrorHandler
    Const S6 As String = "Step 6: Custom Tasks"
    Friend WithEvents DividerLabel4 As sqlrd.DividerLabel
    Friend WithEvents chkMultithreaded As System.Windows.Forms.CheckBox
    Dim serverUrl As String = ""
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdFinish As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrProv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdLoc As System.Windows.Forms.Button
    Friend WithEvents txtFolder As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Step1 As System.Windows.Forms.Panel
    Friend WithEvents Step2 As System.Windows.Forms.Panel
    Friend WithEvents Step3 As System.Windows.Forms.Panel
    Friend WithEvents Step4 As System.Windows.Forms.Panel
    Friend WithEvents Step5 As System.Windows.Forms.Panel
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents lsvReports As System.Windows.Forms.ListView
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents Format As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddReport As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveReport As System.Windows.Forms.Button
    Friend WithEvents Step6 As System.Windows.Forms.Panel
    Friend WithEvents oTask As sqlrd.ucTasks
    Friend WithEvents lblStep As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ucSet As sqlrd.ucScheduleSet
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents cmdEditReport As System.Windows.Forms.Button
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents chkMergePDF As System.Windows.Forms.CheckBox
    Friend WithEvents chkMergeXL As System.Windows.Forms.CheckBox
    Friend WithEvents txtMergePDF As System.Windows.Forms.TextBox
    Friend WithEvents txtMergeXL As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbDateTime As System.Windows.Forms.ComboBox
    Friend WithEvents chkDTStamp As System.Windows.Forms.CheckBox
    Friend WithEvents cmdUp As System.Windows.Forms.Button
    Friend WithEvents cmdDown As System.Windows.Forms.Button
    Friend WithEvents Footer1 As WizardFooter.Footer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPackWizard))
        Me.cmdFinish = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdBack = New System.Windows.Forms.Button
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.ErrProv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdLoc = New System.Windows.Forms.Button
        Me.txtFolder = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblStep = New System.Windows.Forms.Label
        Me.Step1 = New System.Windows.Forms.Panel
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.Step2 = New System.Windows.Forms.Panel
        Me.ucSet = New sqlrd.ucScheduleSet
        Me.Step3 = New System.Windows.Forms.Panel
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.txtAdjustStamp = New System.Windows.Forms.NumericUpDown
        Me.Label26 = New System.Windows.Forms.Label
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.chkDTStamp = New System.Windows.Forms.CheckBox
        Me.UcDest = New sqlrd.ucDestination
        Me.Step4 = New System.Windows.Forms.Panel
        Me.DividerLabel4 = New sqlrd.DividerLabel
        Me.chkMultithreaded = New System.Windows.Forms.CheckBox
        Me.DividerLabel3 = New sqlrd.DividerLabel
        Me.DividerLabel2 = New sqlrd.DividerLabel
        Me.chkSnapshot = New System.Windows.Forms.CheckBox
        Me.txtSnapshots = New System.Windows.Forms.NumericUpDown
        Me.chkMergeXL = New System.Windows.Forms.CheckBox
        Me.txtMergeXL = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmdUp = New System.Windows.Forms.Button
        Me.txtMergePDF = New System.Windows.Forms.TextBox
        Me.cmdDown = New System.Windows.Forms.Button
        Me.chkMergePDF = New System.Windows.Forms.CheckBox
        Me.cmdAddReport = New System.Windows.Forms.Button
        Me.lsvReports = New System.Windows.Forms.ListView
        Me.ReportName = New System.Windows.Forms.ColumnHeader
        Me.Format = New System.Windows.Forms.ColumnHeader
        Me.cmdRemoveReport = New System.Windows.Forms.Button
        Me.cmdEditReport = New System.Windows.Forms.Button
        Me.Step5 = New System.Windows.Forms.Panel
        Me.UcError = New sqlrd.ucErrorHandler
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.cmdNext = New System.Windows.Forms.Button
        Me.Step6 = New System.Windows.Forms.Panel
        Me.oTask = New sqlrd.ucTasks
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.Footer1 = New WizardFooter.Footer
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.DividerLabel1 = New sqlrd.DividerLabel
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem
        Me.mnuDefMsg = New System.Windows.Forms.MenuItem
        Me.mnuSignature = New System.Windows.Forms.MenuItem
        Me.mnuAttachment = New System.Windows.Forms.MenuItem
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step1.SuspendLayout()
        Me.Step2.SuspendLayout()
        Me.Step3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step4.SuspendLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Step5.SuspendLayout()
        Me.Step6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdFinish
        '
        Me.cmdFinish.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdFinish.Image = CType(resources.GetObject("cmdFinish.Image"), System.Drawing.Image)
        Me.cmdFinish.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdFinish.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdFinish.Location = New System.Drawing.Point(376, 463)
        Me.cmdFinish.Name = "cmdFinish"
        Me.cmdFinish.Size = New System.Drawing.Size(73, 25)
        Me.cmdFinish.TabIndex = 50
        Me.cmdFinish.Text = "&Finish"
        Me.cmdFinish.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.PictureBox1.Location = New System.Drawing.Point(400, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(48, 56)
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(288, 463)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(73, 25)
        Me.cmdCancel.TabIndex = 48
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(200, 463)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(73, 25)
        Me.cmdBack.TabIndex = 47
        Me.cmdBack.Text = "&Back"
        '
        'ofd
        '
        Me.ofd.DefaultExt = "*.mdb"
        Me.ofd.Filter = "MS Access Database|*.mdb|All Files|*.*"
        '
        'ErrProv
        '
        Me.ErrProv.ContainerControl = Me
        Me.ErrProv.Icon = CType(resources.GetObject("ErrProv.Icon"), System.Drawing.Icon)
        '
        'cmdLoc
        '
        Me.cmdLoc.BackColor = System.Drawing.SystemColors.Control
        Me.cmdLoc.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdLoc.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.cmdLoc.ForeColor = System.Drawing.Color.Navy
        Me.cmdLoc.Image = CType(resources.GetObject("cmdLoc.Image"), System.Drawing.Image)
        Me.cmdLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdLoc.Location = New System.Drawing.Point(328, 80)
        Me.cmdLoc.Name = "cmdLoc"
        Me.cmdLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdLoc.TabIndex = 2
        Me.cmdLoc.Text = "..."
        Me.ToolTip1.SetToolTip(Me.cmdLoc, "Browse")
        Me.cmdLoc.UseVisualStyleBackColor = False
        '
        'txtFolder
        '
        Me.txtFolder.BackColor = System.Drawing.Color.White
        Me.txtFolder.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtFolder.ForeColor = System.Drawing.Color.Blue
        Me.txtFolder.Location = New System.Drawing.Point(8, 80)
        Me.txtFolder.Name = "txtFolder"
        Me.txtFolder.ReadOnly = True
        Me.txtFolder.Size = New System.Drawing.Size(296, 21)
        Me.txtFolder.TabIndex = 1
        Me.txtFolder.Tag = "1"
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label4.ForeColor = System.Drawing.Color.Navy
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(208, 16)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Create In"
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 32)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(376, 21)
        Me.txtName.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Label2.ForeColor = System.Drawing.Color.Navy
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Package Name"
        '
        'lblStep
        '
        Me.lblStep.BackColor = System.Drawing.Color.Transparent
        Me.lblStep.Font = New System.Drawing.Font("Tahoma", 15.75!)
        Me.lblStep.ForeColor = System.Drawing.Color.Navy
        Me.lblStep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStep.Location = New System.Drawing.Point(8, 16)
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(416, 32)
        Me.lblStep.TabIndex = 2
        Me.lblStep.Text = "Step 1: Package Setup"
        '
        'Step1
        '
        Me.Step1.BackColor = System.Drawing.SystemColors.Control
        Me.Step1.Controls.Add(Me.txtDesc)
        Me.Step1.Controls.Add(Me.Label1)
        Me.Step1.Controls.Add(Me.Label7)
        Me.Step1.Controls.Add(Me.txtKeyWord)
        Me.Step1.Controls.Add(Me.cmdLoc)
        Me.Step1.Controls.Add(Me.txtFolder)
        Me.Step1.Controls.Add(Me.Label4)
        Me.Step1.Controls.Add(Me.txtName)
        Me.Step1.Controls.Add(Me.Label2)
        Me.Step1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.Step1.ForeColor = System.Drawing.Color.Navy
        Me.HelpProvider1.SetHelpKeyword(Me.Step1, "Packaged_Reports_Schedule.htm#Step1")
        Me.HelpProvider1.SetHelpNavigator(Me.Step1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step1.Location = New System.Drawing.Point(0, 72)
        Me.Step1.Name = "Step1"
        Me.HelpProvider1.SetShowHelp(Me.Step1, True)
        Me.Step1.Size = New System.Drawing.Size(456, 368)
        Me.Step1.TabIndex = 3
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(8, 128)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(376, 167)
        Me.txtDesc.TabIndex = 3
        Me.txtDesc.Tag = "memo"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(208, 16)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Description (optional)"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 304)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 17
        Me.Label7.Text = "Keyword (optional)"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(8, 320)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 4
        Me.txtKeyWord.Tag = "memo"
        '
        'Step2
        '
        Me.Step2.BackColor = System.Drawing.SystemColors.Control
        Me.Step2.Controls.Add(Me.ucSet)
        Me.HelpProvider1.SetHelpKeyword(Me.Step2, "Packaged_Reports_Schedule.htm#Step2")
        Me.HelpProvider1.SetHelpNavigator(Me.Step2, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step2.Location = New System.Drawing.Point(0, 72)
        Me.Step2.Name = "Step2"
        Me.HelpProvider1.SetShowHelp(Me.Step2, True)
        Me.Step2.Size = New System.Drawing.Size(456, 368)
        Me.Step2.TabIndex = 14
        Me.Step2.Visible = False
        '
        'ucSet
        '
        Me.ucSet.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucSet.Location = New System.Drawing.Point(6, 3)
        Me.ucSet.m_RepeatUnit = "hours"
        Me.ucSet.Name = "ucSet"
        Me.ucSet.Size = New System.Drawing.Size(440, 362)
        Me.ucSet.TabIndex = 1
        '
        'Step3
        '
        Me.Step3.BackColor = System.Drawing.SystemColors.Control
        Me.Step3.Controls.Add(Me.GroupBox5)
        Me.Step3.Controls.Add(Me.UcDest)
        Me.HelpProvider1.SetHelpKeyword(Me.Step3, "Packaged_Reports_Schedule.htm#Step3")
        Me.HelpProvider1.SetHelpNavigator(Me.Step3, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step3.Location = New System.Drawing.Point(0, 72)
        Me.Step3.Name = "Step3"
        Me.HelpProvider1.SetShowHelp(Me.Step3, True)
        Me.Step3.Size = New System.Drawing.Size(456, 368)
        Me.Step3.TabIndex = 15
        Me.Step3.Visible = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtAdjustStamp)
        Me.GroupBox5.Controls.Add(Me.Label26)
        Me.GroupBox5.Controls.Add(Me.cmbDateTime)
        Me.GroupBox5.Controls.Add(Me.chkDTStamp)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 280)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(440, 78)
        Me.GroupBox5.TabIndex = 1
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Date/Time Stamp"
        '
        'txtAdjustStamp
        '
        Me.txtAdjustStamp.Enabled = False
        Me.txtAdjustStamp.Location = New System.Drawing.Point(232, 48)
        Me.txtAdjustStamp.Maximum = New Decimal(New Integer() {365, 0, 0, 0})
        Me.txtAdjustStamp.Minimum = New Decimal(New Integer() {365, 0, 0, -2147483648})
        Me.txtAdjustStamp.Name = "txtAdjustStamp"
        Me.txtAdjustStamp.Size = New System.Drawing.Size(49, 21)
        Me.txtAdjustStamp.TabIndex = 2
        Me.txtAdjustStamp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(15, 52)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(168, 13)
        Me.Label26.TabIndex = 21
        Me.Label26.Text = "Adjust date/time stamp by (days)"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Enabled = False
        Me.cmbDateTime.ForeColor = System.Drawing.Color.Blue
        Me.cmbDateTime.ItemHeight = 13
        Me.cmbDateTime.Items.AddRange(New Object() {"ddhh", "ddMM", "ddMMyy", "ddMMyyhhmm", "ddMMyyhhmms", "ddMMyyyy", "ddMMyyyyhhmm", "ddMMyyyyhhmmss", "hhmm", "hhmmss", "MMddyy", "MMddyyhhmm", "MMddyyhhmmss", "MMddyyyy", "MMddyyyyhhmm", "MMddyyyyhhmmss", "yyMMdd", "yyMMddhhmm", "yyMMddhhmmss", "yyyyMMdd", "yyyyMMddhhmm", "yyyyMMddhhmmss", "MMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(232, 16)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(152, 21)
        Me.cmbDateTime.TabIndex = 1
        '
        'chkDTStamp
        '
        Me.chkDTStamp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkDTStamp.Location = New System.Drawing.Point(16, 16)
        Me.chkDTStamp.Name = "chkDTStamp"
        Me.chkDTStamp.Size = New System.Drawing.Size(160, 24)
        Me.chkDTStamp.TabIndex = 0
        Me.chkDTStamp.Text = "Append date/time stamp"
        '
        'UcDest
        '
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(8, 8)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(441, 266)
        Me.UcDest.TabIndex = 0
        '
        'Step4
        '
        Me.Step4.BackColor = System.Drawing.SystemColors.Control
        Me.Step4.Controls.Add(Me.DividerLabel4)
        Me.Step4.Controls.Add(Me.chkMultithreaded)
        Me.Step4.Controls.Add(Me.DividerLabel3)
        Me.Step4.Controls.Add(Me.DividerLabel2)
        Me.Step4.Controls.Add(Me.chkSnapshot)
        Me.Step4.Controls.Add(Me.txtSnapshots)
        Me.Step4.Controls.Add(Me.chkMergeXL)
        Me.Step4.Controls.Add(Me.txtMergeXL)
        Me.Step4.Controls.Add(Me.Label3)
        Me.Step4.Controls.Add(Me.Label5)
        Me.Step4.Controls.Add(Me.cmdUp)
        Me.Step4.Controls.Add(Me.txtMergePDF)
        Me.Step4.Controls.Add(Me.cmdDown)
        Me.Step4.Controls.Add(Me.chkMergePDF)
        Me.Step4.Controls.Add(Me.cmdAddReport)
        Me.Step4.Controls.Add(Me.lsvReports)
        Me.Step4.Controls.Add(Me.cmdRemoveReport)
        Me.Step4.Controls.Add(Me.cmdEditReport)
        Me.HelpProvider1.SetHelpKeyword(Me.Step4, "Packaged_Reports_Schedule.htm#Step4")
        Me.HelpProvider1.SetHelpNavigator(Me.Step4, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step4.Location = New System.Drawing.Point(0, 72)
        Me.Step4.Name = "Step4"
        Me.HelpProvider1.SetShowHelp(Me.Step4, True)
        Me.Step4.Size = New System.Drawing.Size(456, 368)
        Me.Step4.TabIndex = 16
        Me.Step4.Visible = False
        '
        'DividerLabel4
        '
        Me.DividerLabel4.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel4.Location = New System.Drawing.Point(8, 323)
        Me.DividerLabel4.Name = "DividerLabel4"
        Me.DividerLabel4.Size = New System.Drawing.Size(436, 13)
        Me.DividerLabel4.Spacing = 0
        Me.DividerLabel4.TabIndex = 15
        Me.DividerLabel4.Text = "Multi-Threading"
        '
        'chkMultithreaded
        '
        Me.chkMultithreaded.AutoSize = True
        Me.chkMultithreaded.Location = New System.Drawing.Point(8, 341)
        Me.chkMultithreaded.Name = "chkMultithreaded"
        Me.chkMultithreaded.Size = New System.Drawing.Size(163, 17)
        Me.chkMultithreaded.TabIndex = 14
        Me.chkMultithreaded.Text = "Run Package Multi-Threaded"
        Me.chkMultithreaded.UseVisualStyleBackColor = True
        '
        'DividerLabel3
        '
        Me.DividerLabel3.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel3.Location = New System.Drawing.Point(8, 276)
        Me.DividerLabel3.Name = "DividerLabel3"
        Me.DividerLabel3.Size = New System.Drawing.Size(436, 13)
        Me.DividerLabel3.Spacing = 0
        Me.DividerLabel3.TabIndex = 12
        Me.DividerLabel3.Text = "Snapshots"
        '
        'DividerLabel2
        '
        Me.DividerLabel2.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel2.Location = New System.Drawing.Point(8, 190)
        Me.DividerLabel2.Name = "DividerLabel2"
        Me.DividerLabel2.Size = New System.Drawing.Size(438, 13)
        Me.DividerLabel2.Spacing = 0
        Me.DividerLabel2.TabIndex = 12
        Me.DividerLabel2.Text = "Merging"
        '
        'chkSnapshot
        '
        Me.chkSnapshot.AutoSize = True
        Me.chkSnapshot.Location = New System.Drawing.Point(8, 297)
        Me.chkSnapshot.Name = "chkSnapshot"
        Me.chkSnapshot.Size = New System.Drawing.Size(208, 17)
        Me.chkSnapshot.TabIndex = 10
        Me.chkSnapshot.Text = "Enable snapshots and keep for (days)"
        Me.chkSnapshot.UseVisualStyleBackColor = True
        '
        'txtSnapshots
        '
        Me.txtSnapshots.Enabled = False
        Me.txtSnapshots.Location = New System.Drawing.Point(240, 295)
        Me.txtSnapshots.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSnapshots.Name = "txtSnapshots"
        Me.txtSnapshots.Size = New System.Drawing.Size(32, 21)
        Me.txtSnapshots.TabIndex = 11
        Me.txtSnapshots.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSnapshots.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkMergeXL
        '
        Me.chkMergeXL.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergeXL.Location = New System.Drawing.Point(8, 206)
        Me.chkMergeXL.Name = "chkMergeXL"
        Me.chkMergeXL.Size = New System.Drawing.Size(263, 32)
        Me.chkMergeXL.TabIndex = 6
        Me.chkMergeXL.Text = "Merge all Excel outputs into a single workbook"
        Me.ToolTip1.SetToolTip(Me.chkMergeXL, "All reports in Excel format will be merged into a single Excel workbook")
        '
        'txtMergeXL
        '
        Me.txtMergeXL.Enabled = False
        Me.txtMergeXL.Location = New System.Drawing.Point(277, 212)
        Me.txtMergeXL.Name = "txtMergeXL"
        Me.txtMergeXL.Size = New System.Drawing.Size(128, 21)
        Me.txtMergeXL.TabIndex = 7
        Me.txtMergeXL.Tag = "memo"
        Me.ToolTip1.SetToolTip(Me.txtMergeXL, "Enter the resulting PDF file name")
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(414, 252)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = ".pdf"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(414, 214)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 16)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = ".xls"
        '
        'cmdUp
        '
        Me.cmdUp.BackColor = System.Drawing.SystemColors.Control
        Me.cmdUp.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(376, 125)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(75, 24)
        Me.cmdUp.TabIndex = 4
        Me.cmdUp.Text = "&Up"
        Me.cmdUp.UseVisualStyleBackColor = False
        '
        'txtMergePDF
        '
        Me.txtMergePDF.Enabled = False
        Me.txtMergePDF.Location = New System.Drawing.Point(277, 250)
        Me.txtMergePDF.Name = "txtMergePDF"
        Me.txtMergePDF.Size = New System.Drawing.Size(128, 21)
        Me.txtMergePDF.TabIndex = 9
        Me.txtMergePDF.Tag = "memo"
        Me.ToolTip1.SetToolTip(Me.txtMergePDF, "Enter the resulting PDF file name")
        '
        'cmdDown
        '
        Me.cmdDown.BackColor = System.Drawing.SystemColors.Control
        Me.cmdDown.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(376, 155)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(75, 24)
        Me.cmdDown.TabIndex = 5
        Me.cmdDown.Text = "&Down"
        Me.cmdDown.UseVisualStyleBackColor = False
        '
        'chkMergePDF
        '
        Me.chkMergePDF.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkMergePDF.Location = New System.Drawing.Point(8, 244)
        Me.chkMergePDF.Name = "chkMergePDF"
        Me.chkMergePDF.Size = New System.Drawing.Size(263, 32)
        Me.chkMergePDF.TabIndex = 8
        Me.chkMergePDF.Text = "Merge all PDF outputs into a single PDF file"
        Me.ToolTip1.SetToolTip(Me.chkMergePDF, "All reports in PDF format will be merged into a single PDF file")
        '
        'cmdAddReport
        '
        Me.cmdAddReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdAddReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddReport.Image = CType(resources.GetObject("cmdAddReport.Image"), System.Drawing.Image)
        Me.cmdAddReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(376, 8)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 0
        Me.cmdAddReport.Text = "&Add"
        Me.cmdAddReport.UseVisualStyleBackColor = False
        '
        'lsvReports
        '
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName, Me.Format})
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HideSelection = False
        Me.lsvReports.Location = New System.Drawing.Point(8, 8)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.lsvReports.Size = New System.Drawing.Size(360, 171)
        Me.lsvReports.TabIndex = 1
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 200
        '
        'Format
        '
        Me.Format.Text = "Format"
        Me.Format.Width = 150
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdRemoveReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveReport.Image = CType(resources.GetObject("cmdRemoveReport.Image"), System.Drawing.Image)
        Me.cmdRemoveReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(376, 72)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 3
        Me.cmdRemoveReport.Text = "&Remove"
        Me.cmdRemoveReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveReport.UseVisualStyleBackColor = False
        '
        'cmdEditReport
        '
        Me.cmdEditReport.BackColor = System.Drawing.SystemColors.Control
        Me.cmdEditReport.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdEditReport.Image = CType(resources.GetObject("cmdEditReport.Image"), System.Drawing.Image)
        Me.cmdEditReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(376, 40)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdEditReport.TabIndex = 2
        Me.cmdEditReport.Text = "&Edit"
        Me.cmdEditReport.UseVisualStyleBackColor = False
        '
        'Step5
        '
        Me.Step5.BackColor = System.Drawing.SystemColors.Control
        Me.Step5.Controls.Add(Me.UcError)
        Me.Step5.Controls.Add(Me.UcBlank)
        Me.HelpProvider1.SetHelpKeyword(Me.Step5, "Packaged_Reports_Schedule.htm#Step5")
        Me.HelpProvider1.SetHelpNavigator(Me.Step5, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step5.Location = New System.Drawing.Point(0, 72)
        Me.Step5.Name = "Step5"
        Me.HelpProvider1.SetShowHelp(Me.Step5, True)
        Me.Step5.Size = New System.Drawing.Size(456, 368)
        Me.Step5.TabIndex = 17
        Me.Step5.Visible = False
        '
        'UcError
        '
        Me.UcError.BackColor = System.Drawing.Color.Transparent
        Me.UcError.Location = New System.Drawing.Point(8, 6)
        Me.UcError.m_showFailOnOne = True
        Me.UcError.Name = "UcError"
        Me.UcError.Size = New System.Drawing.Size(440, 86)
        Me.UcError.TabIndex = 5
        '
        'UcBlank
        '
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlank.Location = New System.Drawing.Point(8, 98)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(440, 270)
        Me.UcBlank.TabIndex = 4
        '
        'cmdNext
        '
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(376, 463)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(73, 25)
        Me.cmdNext.TabIndex = 49
        Me.cmdNext.Text = "&Next"
        '
        'Step6
        '
        Me.Step6.BackColor = System.Drawing.SystemColors.Control
        Me.Step6.Controls.Add(Me.oTask)
        Me.HelpProvider1.SetHelpKeyword(Me.Step6, "Packaged_Reports_Schedule.htm#Step6")
        Me.HelpProvider1.SetHelpNavigator(Me.Step6, System.Windows.Forms.HelpNavigator.Topic)
        Me.Step6.Location = New System.Drawing.Point(0, 72)
        Me.Step6.Name = "Step6"
        Me.HelpProvider1.SetShowHelp(Me.Step6, True)
        Me.Step6.Size = New System.Drawing.Size(456, 368)
        Me.Step6.TabIndex = 20
        Me.Step6.Visible = False
        '
        'oTask
        '
        Me.oTask.BackColor = System.Drawing.SystemColors.Control
        Me.oTask.Dock = System.Windows.Forms.DockStyle.Fill
        Me.oTask.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.oTask.ForeColor = System.Drawing.Color.Navy
        Me.oTask.Location = New System.Drawing.Point(0, 0)
        Me.oTask.m_defaultTaks = False
        Me.oTask.m_eventBased = False
        Me.oTask.m_eventID = 99999
        Me.oTask.Name = "oTask"
        Me.oTask.Size = New System.Drawing.Size(456, 368)
        Me.oTask.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.Panel1.BackgroundImage = Global.sqlrd.My.Resources.Resources.strip
        Me.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Controls.Add(Me.lblStep)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(458, 62)
        Me.Panel1.TabIndex = 21
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'Footer1
        '
        Me.Footer1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Footer1.Location = New System.Drawing.Point(0, 446)
        Me.Footer1.Name = "Footer1"
        Me.Footer1.Size = New System.Drawing.Size(472, 16)
        Me.Footer1.TabIndex = 1
        Me.Footer1.TabStop = False
        '
        'DividerLabel1
        '
        Me.DividerLabel1.BackColor = System.Drawing.Color.Transparent
        Me.DividerLabel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.DividerLabel1.LineStyle = System.Windows.Forms.Border3DStyle.Etched
        Me.DividerLabel1.Location = New System.Drawing.Point(0, 62)
        Me.DividerLabel1.Name = "DividerLabel1"
        Me.DividerLabel1.Size = New System.Drawing.Size(458, 13)
        Me.DividerLabel1.Spacing = 0
        Me.DividerLabel1.TabIndex = 25
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuDefMsg, Me.mnuSignature, Me.mnuAttachment})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuDefMsg
        '
        Me.mnuDefMsg.Index = 5
        Me.mnuDefMsg.Text = "Default Message"
        '
        'mnuSignature
        '
        Me.mnuSignature.Index = 6
        Me.mnuSignature.Text = "Default Signature"
        '
        'mnuAttachment
        '
        Me.mnuAttachment.Index = 7
        Me.mnuAttachment.Text = "Default Attachment"
        '
        'frmPackWizard
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(458, 492)
        Me.Controls.Add(Me.Step4)
        Me.Controls.Add(Me.Step3)
        Me.Controls.Add(Me.Step5)
        Me.Controls.Add(Me.Step6)
        Me.Controls.Add(Me.Step2)
        Me.Controls.Add(Me.Step1)
        Me.Controls.Add(Me.DividerLabel1)
        Me.Controls.Add(Me.Footer1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdFinish)
        Me.Controls.Add(Me.cmdNext)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdBack)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.HelpButton = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPackWizard"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Packaged Schedule Wizard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrProv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step1.ResumeLayout(False)
        Me.Step1.PerformLayout()
        Me.Step2.ResumeLayout(False)
        Me.Step3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.txtAdjustStamp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step4.ResumeLayout(False)
        Me.Step4.PerformLayout()
        CType(Me.txtSnapshots, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Step5.ResumeLayout(False)
        Me.Step6.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub cmdLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLoc.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim sFolder(1) As String

        sFolder = oFolders.GetFolder

        If sFolder(0) <> "" And sFolder(0) <> "Desktop" Then
            txtFolder.Text = sFolder(0)
            txtFolder.Tag = sFolder(1)
        End If
    End Sub

    Private Sub frmPackWizard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If HasCancelled = True Then
            cmdCancel_Click(Nothing, Nothing)
        End If
    End Sub



    Private Sub frmPackWizard_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        HelpProvider1.HelpNamespace = gHelpPath
        If _CheckScheduleCount() = False Then Return

        Dim I As Int32 = 0

        nStep = 1

        ucSet.cmbRpt.Text = 0

        UcDest.isPackage = True

        ucSet.EndDate.Value = Now.AddYears(100)

        FormatForWinXP(Me)
        Step1.Visible = True
        Step2.Visible = False
        Step3.Visible = False
        Step4.Visible = False
        Step5.Visible = False
        Step6.Visible = False
        Step1.BringToFront()
        txtName.Focus()

        lblStep.Text = S1

        If MailType <> MarsGlobal.gMailType.MAPI Then
            mnuMAPI.Enabled = False
        End If

        If gParentID > 0 And gParent.Length > 0 Then
            txtFolder.Text = gParent
            txtFolder.Tag = gParentID
        End If

        Dim oData As New clsMarsData

        oData.CleanDB()

        Me.txtMergePDF.ContextMenu = Me.mnuInserter
        Me.txtMergeXL.ContextMenu = Me.mnuInserter

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            chkMultithreaded.Enabled = False
        End If

    End Sub


    Private Sub txtFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFolder.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub


    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 6
                Step6.Visible = False
                Step5.Visible = True
                Step5.BringToFront()
                lblStep.Text = S5
                cmdNext.Visible = True
                cmdFinish.Visible = False
                Me.AcceptButton = cmdNext
            Case 5
                Step5.Visible = False
                Step4.Visible = True
                Step4.BringToFront()
                lblStep.Text = S4

            Case 4
                Step4.Visible = False
                Step3.Visible = True
                Step3.BringToFront()
                lblStep.Text = S3
            Case 3
                Step3.Visible = False
                Step2.Visible = True
                Step2.BringToFront()
                lblStep.Text = S2
            Case 2
                Step2.Visible = False
                Step1.Visible = True
                Step1.BringToFront()
                cmdBack.Enabled = False
                lblStep.Text = S1
                txtName.Focus()
            Case Else
                Exit Sub
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click

        Dim oData As New clsMarsData

        Select Case nStep
            Case 1
                If txtName.Text = "" Then
                    ErrProv.SetError(txtName, "Please provide a name for this schedule")
                    txtName.Focus()
                    Exit Sub
                ElseIf txtFolder.Text = "" Then
                    ErrProv.SetError(cmdLoc, "Please select the destination folder")
                    cmdLoc.Focus()
                    Exit Sub
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtFolder.Tag, clsMarsScheduler.enScheduleType.PACKAGE) = False Then
                    ErrProv.SetError(txtName, "A package schedule " & _
                    "already exist with this name")
                    txtName.Focus()
                    Return
                End If

                Step1.Visible = False
                Step2.Visible = True
                lblStep.Text = S2
                cmdBack.Enabled = True
            Case 2
                'If ucSet.StartDate.Value > ucSet.EndDate.Value Then
                '    ErrProv.SetError(ucSet.StartDate, "Please select a valid date range")
                '    ucSet.StartDate.Focus()
                '    Exit Sub
                'ElseIf ucSet.chkRepeat.CheckState = CheckState.Checked Then
                '    If ucSet.cmbRpt.Text = "" Then
                '        ErrProv.SetError(ucSet.cmbRpt, "Please set the repeat interval")
                '        ucSet.cmbRpt.Focus()
                '        Exit Sub
                '    ElseIf ucSet.RepeatUntil.Value < ucSet.RunAt.Value Then
                '        ErrProv.SetError(ucSet.RepeatUntil, "The 'repeat until' time cannot be before the execution time")
                '        ucSet.RepeatUntil.Focus()
                '        Exit Sub
                '    End If
                'ElseIf ucSet.optCustom.Checked = True And _
                '    (ucSet.cmbCustom.Text.Length = 0 Or _
                '    ucSet.cmbCustom.Text = "[New...]") Then
                '    ErrProv.SetError(ucSet.cmbCustom, "Please select a valid calendar")
                '    ucSet.cmbCustom.Focus()
                '    Return
                'ElseIf ucSet.chkException.Checked And ucSet.cmbException.Text.Length = 0 Then
                '    ErrProv.SetError(ucSet.cmbException, "Please select an exception calendar")
                '    ucSet.cmbException.Focus()
                '    Return
                'End If

                If ucSet.ValidateSchedule = False Then
                    Return
                End If

                Step2.Visible = False
                Step3.Visible = True
                lblStep.Text = S3
            Case 3
                If UcDest.lsvDestination.Items.Count = 0 Or UcDest.lsvDestination.CheckedItems.Count = 0 Then
                    ErrProv.SetError(UcDest.lsvDestination, "Please add a destination")
                    Return
                ElseIf Me.chkDTStamp.Checked And Me.cmbDateTime.Text = "" Then
                    ErrProv.SetError(Me.cmbDateTime, "Please specify the date/time format for the stamp")
                    Me.cmbDateTime.Focus()
                    Return
                End If

                Step3.Visible = False
                Step4.Visible = True
                cmdAddReport.Focus()
                lblStep.Text = S4
            Case 4
                If chkMergePDF.Checked = True And txtMergePDF.Text.Length = 0 Then
                    ErrProv.SetError(txtMergePDF, "Please enter the name of the resulting PDF file")
                    txtMergePDF.Focus()
                    Return
                ElseIf chkMergeXL.Checked = True And txtMergeXL.Text.Length = 0 Then
                    ErrProv.SetError(txtMergeXL, "Please enter the name of the resulting Excel file")
                    txtMergeXL.Focus()
                    Return
                ElseIf chkDTStamp.Checked = True And cmbDateTime.Text.Length = 0 Then
                    ErrProv.SetError(cmbDateTime, "Please select the datetime stamp")
                    cmbDateTime.Focus()
                    Return
                End If

                Step4.Visible = False
                Step5.Visible = True
                UcError.cmbRetry.Focus()
                lblStep.Text = S5
            Case 5
                'If UcBlank.chkBlankReport.Checked = True Then
                '    If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                '    UcBlank.txtAlertTo.Text.Length = 0 Then
                '        UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please enter the alert recipient")
                '        UcBlank.txtAlertTo.Focus()
                '        Return
                '    End If
                'End If
                If UcBlank.ValidateEntries = False Then Return

                Step5.Visible = False
                Step6.Visible = True
                lblStep.Text = S6
                cmdNext.Visible = False
                cmdFinish.Visible = True
                Me.AcceptButton = cmdFinish
            Case Else
                Exit Sub
        End Select
        nStep += 1
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        oErr.ResetError(sender, ErrProv)
    End Sub

    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        Dim sReturn(2) As String
        Dim oNewReport As New frmPackedReport

        oNewReport = New frmPackedReport

        If serverUrl <> "" Then oNewReport.m_serverUrl = serverUrl

        sReturn = oNewReport.AddReport(lsvReports.Items.Count + 1)

        Me.serverUrl = oNewReport.m_serverUrl

        If sReturn(0) = "" Then Exit Sub

        Dim lsvItem As ListViewItem = New ListViewItem
        Dim lsvSub As ListViewItem.ListViewSubItem = New ListViewItem.ListViewSubItem

        lsvItem.Text = sReturn(0)
        lsvItem.Tag = sReturn(1)
        lsvSub.Text = sReturn(2)

        lsvItem.SubItems.Add(lsvSub)

        lsvReports.Items.Add(lsvItem)
    End Sub

    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim oData As clsMarsData = New clsMarsData
        Dim SQL As String
        Dim nPackID As Integer
        Dim WriteSuccess As Boolean
        Dim sPrinters As String = ""
        Dim lsv As ListViewItem
        Dim I As Integer = 1
        'save the package

        oErr.BusyProgress(10, "Saving package data...")

        cmdFinish.Enabled = False

        nPackID = clsMarsData.CreateDataID("PackageAttr", "PackID")

        HasCancelled = False

        SQL = "INSERT INTO PackageAttr(PackID,PackageName,Parent,Retry,AssumeFail," & _
            "CheckBlank,Owner,FailOnOne,MergePDF,MergeXL,MergePDFName,MergeXLName,DateTimeStamp,StampFormat,Dynamic," & _
            "AdjustPackageStamp,AutoCalc,RetryInterval,Multithreaded) VALUES(" & _
            nPackID & "," & _
            "'" & txtName.Text & "'," & _
            txtFolder.Tag & "," & _
            UcError.cmbRetry.Value & "," & _
            UcError.m_autoFailAfter & "," & _
            Convert.ToInt32(UcBlank.chkBlankReport.Checked) & "," & _
            "'" & gUser & "'," & _
            Convert.ToInt32(UcError.chkFailonOne.Checked) & "," & _
            Convert.ToInt32(chkMergePDF.Checked) & "," & _
            Convert.ToInt32(chkMergeXL.Checked) & "," & _
            "'" & SQLPrepare(txtMergePDF.Text) & "'," & _
            "'" & SQLPrepare(txtMergeXL.Text) & "'," & _
            Convert.ToInt32(chkDTStamp.Checked) & "," & _
            "'" & SQLPrepare(cmbDateTime.Text) & "',0," & _
            txtAdjustStamp.Value & "," & _
            Convert.ToInt32(UcError.chkAutoCalc.Checked) & "," & _
            UcError.txtRetryInterval.Value & "," & _
            Convert.ToInt32(chkMultithreaded.Checked) & ")"

        If clsMarsData.WriteData(SQL) = False Then Exit Sub

        oErr.BusyProgress(30, "Saving schedule data...")

        'save the schedule
        Dim ScheduleID As Int64 = clsMarsData.CreateDataID("ScheduleAttr", "ScheduleID")

        With ucSet
            SQL = "INSERT INTO ScheduleAttr(ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                "StartTime,Repeat,RepeatInterval,RepeatUntil,Status," & _
                "PackID,Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit) " & _
                "VALUES(" & _
                ScheduleID & "," & _
                "'" & .sFrequency & "'," & _
                "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                "'" & ConDate(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                "'" & CTimeZ(.RunAt.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.chkRepeat.Checked) & "," & _
                "'" & Convert.ToString(.cmbRpt.Text).Replace(",", ".") & "'," & _
                "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                Convert.ToInt32(.chkStatus.Checked) & "," & _
                nPackID & "," & _
                "'" & SQLPrepare(txtDesc.Text) & "'," & _
                "'" & SQLPrepare(txtKeyWord.Text) & "'," & _
                "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                Convert.ToInt32(.chkException.Checked) & "," & _
                "'" & SQLPrepare(.cmbException.Text) & "'," & _
                "'" & ucSet.m_RepeatUnit & "')"
        End With

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ScheduleOptions SET " & _
            "ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999")

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        'save the destination
        UcDest.CommitDeletions()

        oErr.BusyProgress(50, "Saving destination data...")

        SQL = "UPDATE DestinationAttr SET ReportID =0, SmartID = 0, PackID = " & nPackID & " WHERE PackID = 99999"

        WriteSuccess = clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE PackageOptions SET PackID =" & nPackID & " WHERE PackID = 99999")

        If WriteSuccess = False Then
            clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
            clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
            Exit Sub
        End If

        oErr.BusyProgress(50, "Saving printers...")

        'save the tasks
        oTask.CommitDeletions()

        SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

        clsMarsData.WriteData(SQL)

        'save the blank report alert information
        If UcBlank.chkBlankReport.Checked = True Then
            Dim sBlankType As String

            If UcBlank.optAlert.Checked = True Then
                sBlankType = "Alert"
            ElseIf UcBlank.optAlertandReport.Checked = True Then
                sBlankType = "AlertandReport"
            ElseIf UcBlank.optIgnore.Checked = True Then
                sBlankType = "Ignore"
            End If

            oErr.BusyProgress(70, "Saving error handling data...")

            SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg,PackID,Subject,CustomQueryDetails) VALUES (" & _
            clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
            "'" & sBlankType & "'," & _
            "'" & UcBlank.txtAlertTo.Text & "'," & _
            "'" & UcBlank.txtBlankMsg.Text & "'," & _
            nPackID & "," & _
            "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
            "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

            clsMarsData.WriteData(SQL)
        End If

        oErr.BusyProgress(90, "Saving reports data...")

        'update the report for this package
        SQL = "UPDATE ReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"

        If clsMarsData.WriteData(SQL) = False Then GoTo RollBackTransaction

        SQL = "UPDATE PackagedReportAttr SET PackID = " & nPackID & " WHERE PackID = 99999"

        clsMarsData.WriteData(SQL)

        clsMarsData.WriteData("UPDATE ReportOptions SET DestinationID =0 WHERE DestinationID =99999")

        'sort out snapshots for the package
        If chkSnapshot.Checked = True Then
            Dim sCols, sVals As String

            sCols = "SnapID,PackID,KeepSnap"

            sVals = clsMarsData.CreateDataID("reportsnapshots", "snapid") & "," & _
            nPackID & "," & _
            txtSnapshots.Value

            SQL = "INSERT INTO ReportSnapshots (" & sCols & ") VALUES " & _
            "(" & sVals & ")"

            clsMarsData.WriteData(SQL)
        End If
        If gRole.ToLower <> "administrator" Then
            Dim oUser As New clsMarsUsers

            oUser.AssignView(nPackID, gUser, clsMarsUsers.enViewType.ViewPackage)
        End If

        oErr.BusyProgress(, , True)

        On Error Resume Next

        oErr.RefreshView(oWindow(nWindowCurrent))

        On Error Resume Next

        Dim nCount As Integer

        nCount = txtFolder.Text.Split("\").GetUpperBound(0)

        Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

        oErr.FindNode("Folder:" & txtFolder.Tag, oTree, oTree.Nodes(0))
        oWindow(nWindowCurrent).lsvWindow.Focus()

        For Each item As ListViewItem In oWindow(nWindowCurrent).lsvWindow.Items
            If item.Text = txtName.Text Then
                item.Selected = True
                item.Focused = True
            Else
                item.Selected = False
                item.Focused = False
            End If
        Next

        clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.PACKAGE, clsMarsAudit.AuditAction.CREATE)

        Me.Close()
        Exit Sub
RollBackTransaction:
        oErr.BusyProgress(, , True)
        clsMarsData.WriteData("DELETE FROM PackageAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM ScheduleAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE PackID = " & nPackID)
        clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE PackID =" & nPackID)
        cmdFinish.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Dim oData As clsMarsData = New clsMarsData
        HasCancelled = False
        oData.CleanDB()

        Dim SQL As String
        Try
            For Each oitem As ListViewItem In lsvReports.Items
                SQL = "DELETE FROM ReportParameter WHERE ReportID =" & oitem.Tag

                clsMarsData.WriteData(SQL)
            Next
        Catch
        End Try

        Me.Close()
    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As ListViewItem = lsvReports.SelectedItems(0)

        nReportID = lsv.Tag

        Dim SQL As String = "DELETE FROM ReportAttr WHERE PackID = 99999 AND " & _
                "ReportID =" & nReportID

        clsMarsData.WriteData(SQL)

        SQL = "DELETE FROM ReportParameter WHERE ReportID=" & nReportID

        clsMarsData.WriteData(SQL)

        lsv.Remove()
    End Sub

    

    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmPackedReport
        Dim sVals() As String

        sVals = oEdit.EditReport(lsvReports.SelectedItems(0).Tag)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)

            oItem.SubItems(1).Text = sVals(1)

            lsvReports.Refresh()
        End If


    End Sub

    Private Sub lsvReports_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvReports.SelectedIndexChanged

    End Sub

    Private Sub lsvReports_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvReports.DoubleClick
        cmdEditReport_Click(sender, e)
    End Sub

    Private Sub chkMergePDF_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergePDF.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.pd1_AdvancedPDFPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergePDF.Checked = False
            Return
        End If

        txtMergePDF.Enabled = chkMergePDF.Checked

        If chkMergePDF.Checked = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("PDF")
        End If

    End Sub

    Private Sub chkMergeXL_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMergeXL.CheckedChanged
        If Not IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.xl1_AdvancedXLPack) Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkMergeXL.Checked = False
            Return
        End If

        txtMergeXL.Enabled = chkMergeXL.Checked

        If chkMergeXL.Checked = True Then
            Dim oOptions As New frmRptOptions

            oOptions.PackageOptions("XLS")
        End If
    End Sub

    Private Sub txtMergePDF_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergePDF.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub txtMergeXL_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMergeXL.GotFocus, txtMergePDF.GotFocus
        Try
            oField = CType(sender, TextBox)
        Catch : End Try
    End Sub

    Private Sub txtMergeXL_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMergeXL.TextChanged
        ErrProv.SetError(sender, String.Empty)
    End Sub

    Private Sub chkDTStamp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTStamp.CheckedChanged
        cmbDateTime.Enabled = chkDTStamp.Checked
        txtAdjustStamp.Enabled = chkDTStamp.Checked

        If chkDTStamp.Checked = False Then
            cmbDateTime.Text = String.Empty
            txtAdjustStamp.Value = 0
        Else
            Dim oUI As New clsMarsUI

            cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

        End If
    End Sub


    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved up
        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & NewID & " WHERE ReportID = " & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = 99999 ORDER BY PackOrderID"


        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)
        Dim SelTag As Object = olsv.Tag

        If olsv.Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim OldID, NewID As Integer

        NewID = olsv2.Text.Split(":")(0)
        OldID = olsv.Text.Split(":")(0)
        Dim NewName As String

        Dim SQL As String
        Dim oData As New clsMarsData

        'update the item being moved down

        NewName = NewID & ":" & olsv.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID =" & NewID & " WHERE ReportID =" & olsv.Tag

        clsMarsData.WriteData(SQL)

        olsv.Text = NewName

        'update the one being moved down
        NewName = OldID & ":" & olsv2.Text.Split(":")(1)

        SQL = "UPDATE ReportAttr SET ReportTitle ='" & SQLPrepare(NewName) & "', " & _
        "PackOrderID = " & OldID & " WHERE ReportID =" & olsv2.Tag

        clsMarsData.WriteData(SQL)

        olsv2.Text = NewName

        SQL = "SELECT * FROM ReportAttr INNER JOIN PackagedReportAttr ON " & _
        "ReportAttr.ReportID = PackagedReportAttr.ReportID WHERE ReportAttr.PackID = 99999 ORDER BY PackOrderID"



        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        lsvReports.Items.Clear()

        Do While oRs.EOF = False
            olsv = New ListViewItem

            olsv.Text = oRs("reporttitle").Value
            olsv.Tag = oRs(0).Value

            olsv.SubItems.Add(oRs("outputformat").Value)

            lsvReports.Items.Add(olsv)
            oRs.MoveNext()
        Loop

        oRs.Close()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next
    End Sub

    Private Sub chkSnapshot_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSnapshot.CheckedChanged
        If Not (IsFeatEnabled(gEdition.ENTERPRISEPRO, featureCodes.sa8_Snapshots)) And chkSnapshot.Checked = True Then
            _NeedUpgrade(gEdition.ENTERPRISEPRO)
            chkSnapshot.Checked = False
            Return
        End If

        txtSnapshots.Enabled = chkSnapshot.Checked
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next
        oField.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next
        oField.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next
        oField.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next
        oField.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(0)
        oInsert.m_EventBased = False
        oInsert.m_EventID = 0
        oInsert.m_ParameterList = Nothing
        'oField.SelectedText = oInsert.GetConstants(Me)
        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next
        Dim oItem As New frmDataItems

        oItem.m_ParameterList = Nothing
        oField.SelectedText = oItem._GetDataItem(0)
    End Sub

    Private Sub chkMultithreaded_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMultithreaded.CheckedChanged
        If Me.chkMultithreaded.Checked And IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False Then
            _NeedUpgrade(gEdition.ENTERPRISEPROPLUS, "Multi-Threading")
            Me.chkMultithreaded.Checked = False
        End If
    End Sub
End Class
