Imports Microsoft.Win32
Imports System.IO
Imports System.ServiceProcess
Imports ActiveDs
Imports System.Net.Sockets
Imports System.Windows.Forms

Public Class frmOptions
    Inherits System.Windows.Forms.Form
    Dim oUI As clsMarsUI = New clsMarsUI
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oReg As RegistryKey = Registry.LocalMachine
    Dim sKey As String = "Software\ChristianSteven\" & Application.ProductName
    Dim WithEvents oSMTP As New Quiksoft.EasyMail.SMTP.SMTP
    Dim UpdateAutoCompact As Boolean = False
    Dim ShowCompactMsg As Boolean = False
    Dim isLoaded As Boolean = False
    Dim skipValidation As Boolean = False
    Dim m_tempSMTPValues As Hashtable

    Friend WithEvents tabOptions As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbMessaging As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbScheduling As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbHouseKeeping As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbUserDefaults As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbDestinations As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel7 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbCluster As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel8 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbAuditTrail As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel9 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbPaths As DevComponents.DotNetBar.TabItem
    Friend WithEvents chkUniversal As System.Windows.Forms.CheckBox
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cmbPollInt As System.Windows.Forms.NumericUpDown
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents cmbSMTPTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents TabControlPanel10 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents txtRefresh As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkDTRefresh As System.Windows.Forms.CheckBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cmbPriority As System.Windows.Forms.ComboBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents grpGroupwise As System.Windows.Forms.GroupBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtgwUser As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtgwPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtgwProxy As System.Windows.Forms.TextBox
    Friend WithEvents txtgwPOIP As System.Windows.Forms.TextBox
    Friend WithEvents txtgwPOPort As System.Windows.Forms.TextBox
    Friend WithEvents txtThreadCount As System.Windows.Forms.NumericUpDown
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents chkUNC As System.Windows.Forms.CheckBox
    Friend WithEvents chkOutofProcess As System.Windows.Forms.CheckBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cmbPollIntEB As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents cmbPriorityEB As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents chkUseRelTime As System.Windows.Forms.CheckBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents txtSMTPPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtSMSInit As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents chkDisableCRParsing As System.Windows.Forms.CheckBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents btnSentMessages As System.Windows.Forms.Button
    Friend WithEvents txtSentMessages As System.Windows.Forms.TextBox
    Friend WithEvents btnMAPIMore As System.Windows.Forms.Button
    Friend WithEvents chkDelayRestart As System.Windows.Forms.CheckBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txtDelayBy As System.Windows.Forms.NumericUpDown
    Dim sCrystalKey As String = ""
    Friend WithEvents btnSMTPAdvanced As System.Windows.Forms.Button
    Friend WithEvents chkConvertToMsg As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvServers As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader7 As System.Windows.Forms.ColumnHeader
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents chkShowHiddenParameters As System.Windows.Forms.CheckBox
    Dim m_scheduleStatus As clsServiceController.enum_svcStatus
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents grpSMTP As System.Windows.Forms.GroupBox
    Friend WithEvents grpMAPI As System.Windows.Forms.GroupBox
    Friend WithEvents cmdMailTest As System.Windows.Forms.Button
    Friend WithEvents cmdClearSettings As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents chkThreading As System.Windows.Forms.CheckBox
    Friend WithEvents txtErrorAlertWho As System.Windows.Forms.TextBox
    Friend WithEvents optErrorLog As System.Windows.Forms.CheckBox
    Friend WithEvents optErrorMail As System.Windows.Forms.CheckBox
    Friend WithEvents txtCustomerNo As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTPUserID As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTPPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTPServer As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTPSenderAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtSMTPSenderName As System.Windows.Forms.TextBox
    Friend WithEvents txtMAPIPassword As System.Windows.Forms.TextBox
    Friend WithEvents cmbMAPIProfile As System.Windows.Forms.ComboBox
    Friend WithEvents chkEmailRetry As System.Windows.Forms.CheckBox
    Friend WithEvents cmbMailType As System.Windows.Forms.ComboBox
    Friend WithEvents cmdServiceApply As System.Windows.Forms.Button
    Friend WithEvents txtWinDomain As System.Windows.Forms.TextBox
    Friend WithEvents txtWinUser As System.Windows.Forms.TextBox
    Friend WithEvents txtWinPassword As System.Windows.Forms.TextBox
    Friend WithEvents optNoScheduling As System.Windows.Forms.RadioButton
    Friend WithEvents optBackgroundApp As System.Windows.Forms.RadioButton
    Friend WithEvents optNTService As System.Windows.Forms.RadioButton
    Friend WithEvents cmdServiceStart As System.Windows.Forms.Button
    Friend WithEvents cmdRegister As System.Windows.Forms.Button
    Friend WithEvents lsvComponents As System.Windows.Forms.ListView
    Friend WithEvents txtDefaultEmail As System.Windows.Forms.TextBox
    Friend WithEvents grpNTService As System.Windows.Forms.GroupBox
    Friend WithEvents grpServices As System.Windows.Forms.GroupBox
    Friend WithEvents ofd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtSig As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtDefSubject As System.Windows.Forms.TextBox
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents cmdSpell As System.Windows.Forms.Button
    Friend WithEvents txtArchive As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkArchive As System.Windows.Forms.CheckBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents UcLogin As sqlrd.ucLoginDetails
    Friend WithEvents grpCRDMail As System.Windows.Forms.GroupBox
    Friend WithEvents txtCRDSender As System.Windows.Forms.TextBox
    Friend WithEvents txtCRDAddress As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cmdActivate As System.Windows.Forms.Button
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents chkCheckService As System.Windows.Forms.CheckBox
    Friend WithEvents chkCluster As System.Windows.Forms.CheckBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents txtSlaveTimeout As System.Windows.Forms.NumericUpDown
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddReport As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveReport As System.Windows.Forms.Button
    Friend WithEvents lsvSlaves As System.Windows.Forms.ListView
    Friend WithEvents grpSlaves As System.Windows.Forms.GroupBox
    Friend WithEvents cmbClusterType As System.Windows.Forms.ComboBox
    Friend WithEvents grpMaster As System.Windows.Forms.GroupBox
    Friend WithEvents txtMaster As System.Windows.Forms.TextBox
    Friend WithEvents cmdMaster As System.Windows.Forms.Button
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents cmdSMTPMore As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cmdDBLoc As System.Windows.Forms.Button
    Friend WithEvents txtDefAttach As System.Windows.Forms.TextBox
    Friend WithEvents chkCompact As System.Windows.Forms.CheckBox
    Friend WithEvents dtAutoCompact As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents chkAutoCompact As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents txtReportLoc As System.Windows.Forms.TextBox
    Friend WithEvents cmdRptLoc As System.Windows.Forms.Button
    Friend WithEvents tabMessaging As System.Windows.Forms.TabControl
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents cmbSMSDevice As System.Windows.Forms.ComboBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage6 As TabPage
    Friend WithEvents TabPage8 As TabPage
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label

    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtSMSNumber As System.Windows.Forms.TextBox
    Friend WithEvents cmbSMSProtocol As System.Windows.Forms.ComboBox
    Friend WithEvents txtSMSPassword As System.Windows.Forms.TextBox
    Friend WithEvents optUseGSM As System.Windows.Forms.RadioButton
    Friend WithEvents cmbSMSFormat As System.Windows.Forms.ComboBox
    Friend WithEvents cmbSMSSpeed As System.Windows.Forms.ComboBox
    Friend WithEvents optUseSMSC As System.Windows.Forms.RadioButton
    Friend WithEvents txtSMSSender As System.Windows.Forms.TextBox
    Friend WithEvents chkUnicode As System.Windows.Forms.CheckBox
    Friend WithEvents grpSMSC As System.Windows.Forms.GroupBox
    Friend WithEvents optErrorSMS As System.Windows.Forms.CheckBox
    Friend WithEvents cmdAlertWho As System.Windows.Forms.Button
    Friend WithEvents cmdAlertSMS As System.Windows.Forms.Button
    Friend WithEvents txtAlertSMS As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents cmbEncoding As System.Windows.Forms.ComboBox
    Friend WithEvents txtReportCache As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCachedData As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents txtSnapShots As System.Windows.Forms.TextBox
    Friend WithEvents cmdReportCache As System.Windows.Forms.Button
    Friend WithEvents cmdCachedData As System.Windows.Forms.Button
    Friend WithEvents cmdSnapshots As System.Windows.Forms.Button
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents txtTempFolder As TextBox
    Friend WithEvents cmdTempFolder As Button
    Friend WithEvents Label38 As Label
    Friend WithEvents cmdAdd As Button
    Friend WithEvents lsvPaths As ListView
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents cmdDelete As Button
    Friend WithEvents GroupBox13 As GroupBox
    Friend WithEvents cmbDateTime As ComboBox
    Friend WithEvents Label44 As Label
    Friend WithEvents cmdTest As Button
    Friend WithEvents ucDSN As sqlrd.ucDSN
    Friend WithEvents chkAudit As CheckBox


    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptions))
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Collaboration Data Objects"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Compression Support"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem3 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Extended MAPI Support"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Dim ListViewItem4 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"MAPI Profiles Enumerator"}, -1, System.Drawing.Color.Blue, System.Drawing.SystemColors.Window, New System.Drawing.Font("Tahoma", 8.25!))
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.ofd = New System.Windows.Forms.FolderBrowserDialog
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.tabOptions = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox17 = New System.Windows.Forms.GroupBox
        Me.chkShowHiddenParameters = New System.Windows.Forms.CheckBox
        Me.GroupBox15 = New System.Windows.Forms.GroupBox
        Me.chkUseRelTime = New System.Windows.Forms.CheckBox
        Me.chkOutofProcess = New System.Windows.Forms.CheckBox
        Me.chkUNC = New System.Windows.Forms.CheckBox
        Me.txtRefresh = New System.Windows.Forms.NumericUpDown
        Me.chkDTRefresh = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtCustomerNo = New System.Windows.Forms.TextBox
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.chkCompact = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cmdAlertWho = New System.Windows.Forms.Button
        Me.txtErrorAlertWho = New System.Windows.Forms.TextBox
        Me.optErrorMail = New System.Windows.Forms.CheckBox
        Me.optErrorSMS = New System.Windows.Forms.CheckBox
        Me.cmdAlertSMS = New System.Windows.Forms.Button
        Me.txtAlertSMS = New System.Windows.Forms.TextBox
        Me.tbGeneral = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cmdDBLoc = New System.Windows.Forms.Button
        Me.Label20 = New System.Windows.Forms.Label
        Me.txtDefSubject = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.txtDefaultEmail = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtSig = New System.Windows.Forms.TextBox
        Me.cmdSpell = New System.Windows.Forms.Button
        Me.Label31 = New System.Windows.Forms.Label
        Me.txtDefAttach = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.UcLogin = New sqlrd.ucLoginDetails
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.lsvServers = New System.Windows.Forms.ListView
        Me.ColumnHeader7 = New System.Windows.Forms.ColumnHeader
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.cmbDateTime = New System.Windows.Forms.ComboBox
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.cmdRptLoc = New System.Windows.Forms.Button
        Me.txtReportLoc = New System.Windows.Forms.TextBox
        Me.tbUserDefaults = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel9 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.chkConvertToMsg = New System.Windows.Forms.CheckBox
        Me.txtSentMessages = New System.Windows.Forms.TextBox
        Me.btnSentMessages = New System.Windows.Forms.Button
        Me.Label52 = New System.Windows.Forms.Label
        Me.cmdReportCache = New System.Windows.Forms.Button
        Me.txtReportCache = New System.Windows.Forms.TextBox
        Me.txtTempFolder = New System.Windows.Forms.TextBox
        Me.cmdTempFolder = New System.Windows.Forms.Button
        Me.Label42 = New System.Windows.Forms.Label
        Me.txtCachedData = New System.Windows.Forms.TextBox
        Me.cmdSnapshots = New System.Windows.Forms.Button
        Me.cmdCachedData = New System.Windows.Forms.Button
        Me.txtSnapShots = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label43 = New System.Windows.Forms.Label
        Me.tbPaths = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox16 = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.cmbPollIntEB = New System.Windows.Forms.NumericUpDown
        Me.cmbPriority = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.cmbPollInt = New System.Windows.Forms.NumericUpDown
        Me.Label47 = New System.Windows.Forms.Label
        Me.Label48 = New System.Windows.Forms.Label
        Me.cmbPriorityEB = New System.Windows.Forms.ComboBox
        Me.chkUniversal = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.Label53 = New System.Windows.Forms.Label
        Me.txtDelayBy = New System.Windows.Forms.NumericUpDown
        Me.chkDelayRestart = New System.Windows.Forms.CheckBox
        Me.chkCheckService = New System.Windows.Forms.CheckBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.txtThreadCount = New System.Windows.Forms.NumericUpDown
        Me.chkThreading = New System.Windows.Forms.CheckBox
        Me.cmdServiceApply = New System.Windows.Forms.Button
        Me.grpServices = New System.Windows.Forms.GroupBox
        Me.grpNTService = New System.Windows.Forms.GroupBox
        Me.txtWinDomain = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtWinUser = New System.Windows.Forms.TextBox
        Me.txtWinPassword = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.optNoScheduling = New System.Windows.Forms.RadioButton
        Me.optBackgroundApp = New System.Windows.Forms.RadioButton
        Me.optNTService = New System.Windows.Forms.RadioButton
        Me.cmdServiceStart = New System.Windows.Forms.Button
        Me.tbScheduling = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel8 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.Label44 = New System.Windows.Forms.Label
        Me.cmdTest = New System.Windows.Forms.Button
        Me.ucDSN = New sqlrd.ucDSN
        Me.chkAudit = New System.Windows.Forms.CheckBox
        Me.tbAuditTrail = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.tabMessaging = New System.Windows.Forms.TabControl
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.grpSMTP = New System.Windows.Forms.GroupBox
        Me.btnSMTPAdvanced = New System.Windows.Forms.Button
        Me.txtSMTPPort = New System.Windows.Forms.NumericUpDown
        Me.cmbSMTPTimeout = New System.Windows.Forms.NumericUpDown
        Me.cmdSMTPMore = New System.Windows.Forms.Button
        Me.Label19 = New System.Windows.Forms.Label
        Me.txtSMTPUserID = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtSMTPPassword = New System.Windows.Forms.TextBox
        Me.Label49 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtSMTPServer = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtSMTPSenderAddress = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.txtSMTPSenderName = New System.Windows.Forms.TextBox
        Me.grpMAPI = New System.Windows.Forms.GroupBox
        Me.btnMAPIMore = New System.Windows.Forms.Button
        Me.txtMAPIPassword = New System.Windows.Forms.TextBox
        Me.cmbMAPIProfile = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.grpGroupwise = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label28 = New System.Windows.Forms.Label
        Me.txtgwUser = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label41 = New System.Windows.Forms.Label
        Me.Label45 = New System.Windows.Forms.Label
        Me.Label46 = New System.Windows.Forms.Label
        Me.txtgwPassword = New System.Windows.Forms.TextBox
        Me.txtgwProxy = New System.Windows.Forms.TextBox
        Me.txtgwPOIP = New System.Windows.Forms.TextBox
        Me.txtgwPOPort = New System.Windows.Forms.TextBox
        Me.grpCRDMail = New System.Windows.Forms.GroupBox
        Me.cmdActivate = New System.Windows.Forms.Button
        Me.Label23 = New System.Windows.Forms.Label
        Me.txtCRDAddress = New System.Windows.Forms.TextBox
        Me.txtCRDSender = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.cmbEncoding = New System.Windows.Forms.ComboBox
        Me.Label39 = New System.Windows.Forms.Label
        Me.cmbMailType = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.chkEmailRetry = New System.Windows.Forms.CheckBox
        Me.cmdMailTest = New System.Windows.Forms.Button
        Me.cmdClearSettings = New System.Windows.Forms.Button
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.Label51 = New System.Windows.Forms.Label
        Me.Label50 = New System.Windows.Forms.Label
        Me.txtSMSInit = New System.Windows.Forms.TextBox
        Me.chkUnicode = New System.Windows.Forms.CheckBox
        Me.grpSMSC = New System.Windows.Forms.GroupBox
        Me.txtSMSNumber = New System.Windows.Forms.TextBox
        Me.Label36 = New System.Windows.Forms.Label
        Me.cmbSMSProtocol = New System.Windows.Forms.ComboBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.txtSMSPassword = New System.Windows.Forms.TextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.txtSMSSender = New System.Windows.Forms.TextBox
        Me.optUseGSM = New System.Windows.Forms.RadioButton
        Me.cmbSMSDevice = New System.Windows.Forms.ComboBox
        Me.Label33 = New System.Windows.Forms.Label
        Me.cmbSMSFormat = New System.Windows.Forms.ComboBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.cmbSMSSpeed = New System.Windows.Forms.ComboBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.optUseSMSC = New System.Windows.Forms.RadioButton
        Me.tbMessaging = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel10 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcTasks = New sqlrd.ucTasks
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel7 = New DevComponents.DotNetBar.TabControlPanel
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.grpSlaves = New System.Windows.Forms.GroupBox
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.cmdAddReport = New System.Windows.Forms.Button
        Me.cmdRemoveReport = New System.Windows.Forms.Button
        Me.lsvSlaves = New System.Windows.Forms.ListView
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.txtSlaveTimeout = New System.Windows.Forms.NumericUpDown
        Me.grpMaster = New System.Windows.Forms.GroupBox
        Me.cmdMaster = New System.Windows.Forms.Button
        Me.txtMaster = New System.Windows.Forms.TextBox
        Me.cmbClusterType = New System.Windows.Forms.ComboBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.chkCluster = New System.Windows.Forms.CheckBox
        Me.tbCluster = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.TabControl2 = New System.Windows.Forms.TabControl
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.lsvComponents = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.cmdRegister = New System.Windows.Forms.Button
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.chkArchive = New System.Windows.Forms.CheckBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.txtArchive = New System.Windows.Forms.NumericUpDown
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.chkAutoCompact = New System.Windows.Forms.CheckBox
        Me.dtAutoCompact = New System.Windows.Forms.DateTimePicker
        Me.Label32 = New System.Windows.Forms.Label
        Me.TabPage8 = New System.Windows.Forms.TabPage
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cmdAdd = New System.Windows.Forms.Button
        Me.lsvPaths = New System.Windows.Forms.ListView
        Me.ColumnHeader4 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.cmdDelete = New System.Windows.Forms.Button
        Me.tbHouseKeeping = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.UcDest = New sqlrd.ucDestination
        Me.tbDestinations = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.chkDisableCRParsing = New System.Windows.Forms.CheckBox
        Me.optErrorLog = New System.Windows.Forms.CheckBox
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown
        CType(Me.tabOptions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabOptions.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        CType(Me.txtRefresh, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.TabControlPanel9.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.cmbPollIntEB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbPollInt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.txtDelayBy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtThreadCount, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpServices.SuspendLayout()
        Me.grpNTService.SuspendLayout()
        Me.TabControlPanel8.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.tabMessaging.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.grpSMTP.SuspendLayout()
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpMAPI.SuspendLayout()
        Me.grpGroupwise.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.grpCRDMail.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.grpSMSC.SuspendLayout()
        Me.TabControlPanel10.SuspendLayout()
        Me.TabControlPanel7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.grpSlaves.SuspendLayout()
        CType(Me.txtSlaveTimeout, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpMaster.SuspendLayout()
        Me.TabControlPanel4.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        CType(Me.txtArchive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox8.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(589, 436)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 4
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(501, 436)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(413, 436)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = False
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'tabOptions
        '
        Me.tabOptions.CanReorderTabs = False
        Me.tabOptions.ColorScheme.TabBackground = System.Drawing.SystemColors.Control
        Me.tabOptions.ColorScheme.TabBackgroundGradientAngle = 0
        Me.tabOptions.ColorScheme.TabItemBackground2 = System.Drawing.Color.White
        Me.tabOptions.ColorScheme.TabPanelBackground2 = System.Drawing.Color.White
        Me.tabOptions.Controls.Add(Me.TabControlPanel1)
        Me.tabOptions.Controls.Add(Me.TabControlPanel5)
        Me.tabOptions.Controls.Add(Me.TabControlPanel9)
        Me.tabOptions.Controls.Add(Me.TabControlPanel3)
        Me.tabOptions.Controls.Add(Me.TabControlPanel8)
        Me.tabOptions.Controls.Add(Me.TabControlPanel2)
        Me.tabOptions.Controls.Add(Me.TabControlPanel10)
        Me.tabOptions.Controls.Add(Me.TabControlPanel7)
        Me.tabOptions.Controls.Add(Me.TabControlPanel4)
        Me.tabOptions.Controls.Add(Me.TabControlPanel6)
        Me.tabOptions.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabOptions.Location = New System.Drawing.Point(0, 0)
        Me.tabOptions.Name = "tabOptions"
        Me.tabOptions.SelectedTabFont = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabOptions.SelectedTabIndex = 0
        Me.tabOptions.Size = New System.Drawing.Size(676, 430)
        Me.tabOptions.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabOptions.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabOptions.TabIndex = 10
        Me.tabOptions.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabOptions.Tabs.Add(Me.tbGeneral)
        Me.tabOptions.Tabs.Add(Me.tbMessaging)
        Me.tabOptions.Tabs.Add(Me.tbScheduling)
        Me.tabOptions.Tabs.Add(Me.tbHouseKeeping)
        Me.tabOptions.Tabs.Add(Me.tbUserDefaults)
        Me.tabOptions.Tabs.Add(Me.tbDestinations)
        Me.tabOptions.Tabs.Add(Me.tbCluster)
        Me.tabOptions.Tabs.Add(Me.tbPaths)
        Me.tabOptions.Tabs.Add(Me.tbAuditTrail)
        Me.tabOptions.Tabs.Add(Me.TabItem1)
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.GroupBox17)
        Me.TabControlPanel1.Controls.Add(Me.GroupBox15)
        Me.TabControlPanel1.Controls.Add(Me.GroupBox7)
        Me.TabControlPanel1.Controls.Add(Me.GroupBox1)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbGeneral
        '
        'GroupBox17
        '
        Me.GroupBox17.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox17.Controls.Add(Me.chkShowHiddenParameters)
        Me.GroupBox17.Location = New System.Drawing.Point(11, 313)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(481, 100)
        Me.GroupBox17.TabIndex = 8
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "Reports"
        '
        'chkShowHiddenParameters
        '
        Me.chkShowHiddenParameters.AutoSize = True
        Me.chkShowHiddenParameters.Location = New System.Drawing.Point(8, 26)
        Me.chkShowHiddenParameters.Name = "chkShowHiddenParameters"
        Me.chkShowHiddenParameters.Size = New System.Drawing.Size(178, 17)
        Me.chkShowHiddenParameters.TabIndex = 0
        Me.chkShowHiddenParameters.Text = "Show hidden report parameters"
        Me.chkShowHiddenParameters.UseVisualStyleBackColor = True
        '
        'GroupBox15
        '
        Me.GroupBox15.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox15.Controls.Add(Me.chkUseRelTime)
        Me.GroupBox15.Controls.Add(Me.chkOutofProcess)
        Me.GroupBox15.Controls.Add(Me.chkUNC)
        Me.GroupBox15.Controls.Add(Me.txtRefresh)
        Me.GroupBox15.Controls.Add(Me.chkDTRefresh)
        Me.GroupBox15.Controls.Add(Me.Label3)
        Me.GroupBox15.Controls.Add(Me.txtCustomerNo)
        Me.GroupBox15.Location = New System.Drawing.Point(11, 4)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(477, 163)
        Me.GroupBox15.TabIndex = 7
        Me.GroupBox15.TabStop = False
        Me.GroupBox15.Text = "User Experience"
        '
        'chkUseRelTime
        '
        Me.chkUseRelTime.AutoSize = True
        Me.chkUseRelTime.Location = New System.Drawing.Point(7, 120)
        Me.chkUseRelTime.Name = "chkUseRelTime"
        Me.chkUseRelTime.Size = New System.Drawing.Size(111, 17)
        Me.chkUseRelTime.TabIndex = 4
        Me.chkUseRelTime.Text = "Use Relative Time"
        Me.chkUseRelTime.UseVisualStyleBackColor = True
        '
        'chkOutofProcess
        '
        Me.chkOutofProcess.Location = New System.Drawing.Point(7, 98)
        Me.chkOutofProcess.Name = "chkOutofProcess"
        Me.chkOutofProcess.Size = New System.Drawing.Size(389, 17)
        Me.chkOutofProcess.TabIndex = 7
        Me.chkOutofProcess.Text = "Use the Process Watcher for manual executions"
        Me.chkOutofProcess.UseVisualStyleBackColor = True
        '
        'chkUNC
        '
        Me.chkUNC.AutoSize = True
        Me.chkUNC.Checked = True
        Me.chkUNC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkUNC.Location = New System.Drawing.Point(7, 76)
        Me.chkUNC.Name = "chkUNC"
        Me.chkUNC.Size = New System.Drawing.Size(192, 17)
        Me.chkUNC.TabIndex = 5
        Me.chkUNC.Text = "Only convert remote paths to UNC"
        Me.chkUNC.UseVisualStyleBackColor = True
        '
        'txtRefresh
        '
        Me.txtRefresh.Enabled = False
        Me.txtRefresh.Location = New System.Drawing.Point(239, 52)
        Me.txtRefresh.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me.txtRefresh.Minimum = New Decimal(New Integer() {60, 0, 0, 0})
        Me.txtRefresh.Name = "txtRefresh"
        Me.txtRefresh.Size = New System.Drawing.Size(40, 21)
        Me.txtRefresh.TabIndex = 4
        Me.txtRefresh.Value = New Decimal(New Integer() {60, 0, 0, 0})
        '
        'chkDTRefresh
        '
        Me.chkDTRefresh.AutoSize = True
        Me.chkDTRefresh.Location = New System.Drawing.Point(7, 54)
        Me.chkDTRefresh.Name = "chkDTRefresh"
        Me.chkDTRefresh.Size = New System.Drawing.Size(232, 17)
        Me.chkDTRefresh.TabIndex = 2
        Me.chkDTRefresh.Text = "Automatically refresh desktop every (secs)"
        Me.chkDTRefresh.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(5, 23)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Customer #"
        '
        'txtCustomerNo
        '
        Me.txtCustomerNo.ForeColor = System.Drawing.Color.Blue
        Me.txtCustomerNo.Location = New System.Drawing.Point(84, 20)
        Me.txtCustomerNo.Name = "txtCustomerNo"
        Me.txtCustomerNo.Size = New System.Drawing.Size(195, 21)
        Me.txtCustomerNo.TabIndex = 1
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Controls.Add(Me.chkCompact)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 254)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(478, 56)
        Me.GroupBox7.TabIndex = 6
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Misc"
        '
        'chkCompact
        '
        Me.chkCompact.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCompact.Location = New System.Drawing.Point(8, 20)
        Me.chkCompact.Name = "chkCompact"
        Me.chkCompact.Size = New System.Drawing.Size(456, 24)
        Me.chkCompact.TabIndex = 1
        Me.chkCompact.Text = "Do not prompt me to compact the system when it grows over 50MB"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.cmdAlertWho)
        Me.GroupBox1.Controls.Add(Me.txtErrorAlertWho)
        Me.GroupBox1.Controls.Add(Me.optErrorMail)
        Me.GroupBox1.Controls.Add(Me.optErrorSMS)
        Me.GroupBox1.Controls.Add(Me.cmdAlertSMS)
        Me.GroupBox1.Controls.Add(Me.txtAlertSMS)
        Me.GroupBox1.Location = New System.Drawing.Point(11, 169)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(477, 72)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Error Handling"
        '
        'cmdAlertWho
        '
        Me.cmdAlertWho.Enabled = False
        Me.cmdAlertWho.Image = CType(resources.GetObject("cmdAlertWho.Image"), System.Drawing.Image)
        Me.cmdAlertWho.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAlertWho.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAlertWho.Location = New System.Drawing.Point(168, 21)
        Me.cmdAlertWho.Name = "cmdAlertWho"
        Me.cmdAlertWho.Size = New System.Drawing.Size(48, 23)
        Me.cmdAlertWho.TabIndex = 2
        Me.cmdAlertWho.Text = "To..."
        Me.cmdAlertWho.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtErrorAlertWho
        '
        Me.txtErrorAlertWho.Enabled = False
        Me.txtErrorAlertWho.ForeColor = System.Drawing.Color.Blue
        Me.txtErrorAlertWho.Location = New System.Drawing.Point(224, 21)
        Me.txtErrorAlertWho.Name = "txtErrorAlertWho"
        Me.txtErrorAlertWho.Size = New System.Drawing.Size(232, 21)
        Me.txtErrorAlertWho.TabIndex = 3
        '
        'optErrorMail
        '
        Me.optErrorMail.AutoSize = True
        Me.optErrorMail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorMail.Location = New System.Drawing.Point(8, 20)
        Me.optErrorMail.Name = "optErrorMail"
        Me.optErrorMail.Size = New System.Drawing.Size(92, 17)
        Me.optErrorMail.TabIndex = 1
        Me.optErrorMail.Text = "Send an email"
        '
        'optErrorSMS
        '
        Me.optErrorSMS.AutoSize = True
        Me.optErrorSMS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorSMS.Location = New System.Drawing.Point(8, 44)
        Me.optErrorSMS.Name = "optErrorSMS"
        Me.optErrorSMS.Size = New System.Drawing.Size(133, 17)
        Me.optErrorSMS.TabIndex = 4
        Me.optErrorSMS.Text = "Send an SMS Message"
        '
        'cmdAlertSMS
        '
        Me.cmdAlertSMS.Enabled = False
        Me.cmdAlertSMS.Image = CType(resources.GetObject("cmdAlertSMS.Image"), System.Drawing.Image)
        Me.cmdAlertSMS.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAlertSMS.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAlertSMS.Location = New System.Drawing.Point(168, 45)
        Me.cmdAlertSMS.Name = "cmdAlertSMS"
        Me.cmdAlertSMS.Size = New System.Drawing.Size(48, 23)
        Me.cmdAlertSMS.TabIndex = 5
        Me.cmdAlertSMS.Text = "To..."
        Me.cmdAlertSMS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAlertSMS
        '
        Me.txtAlertSMS.Enabled = False
        Me.txtAlertSMS.ForeColor = System.Drawing.Color.Blue
        Me.txtAlertSMS.Location = New System.Drawing.Point(224, 46)
        Me.txtAlertSMS.Name = "txtAlertSMS"
        Me.txtAlertSMS.Size = New System.Drawing.Size(232, 21)
        Me.txtAlertSMS.TabIndex = 6
        '
        'tbGeneral
        '
        Me.tbGeneral.AttachedControl = Me.TabControlPanel1
        Me.tbGeneral.Image = CType(resources.GetObject("tbGeneral.Image"), System.Drawing.Image)
        Me.tbGeneral.Name = "tbGeneral"
        Me.tbGeneral.Text = "General"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.TabControl1)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.tbUserDefaults
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.ItemSize = New System.Drawing.Size(105, 18)
        Me.TabControl1.Location = New System.Drawing.Point(8, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(488, 392)
        Me.TabControl1.TabIndex = 5
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(480, 366)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Messaging Defaults"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cmdDBLoc)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Controls.Add(Me.txtDefSubject)
        Me.GroupBox3.Controls.Add(Me.Label18)
        Me.GroupBox3.Controls.Add(Me.txtDefaultEmail)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtSig)
        Me.GroupBox3.Controls.Add(Me.cmdSpell)
        Me.GroupBox3.Controls.Add(Me.Label31)
        Me.GroupBox3.Controls.Add(Me.txtDefAttach)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(464, 352)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'cmdDBLoc
        '
        Me.cmdDBLoc.Image = CType(resources.GetObject("cmdDBLoc.Image"), System.Drawing.Image)
        Me.cmdDBLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdDBLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDBLoc.Location = New System.Drawing.Point(392, 72)
        Me.cmdDBLoc.Name = "cmdDBLoc"
        Me.cmdDBLoc.Size = New System.Drawing.Size(56, 21)
        Me.cmdDBLoc.TabIndex = 3
        Me.cmdDBLoc.Text = "..."
        '
        'Label20
        '
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(8, 17)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(216, 16)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Default Email Subject"
        '
        'txtDefSubject
        '
        Me.txtDefSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtDefSubject.Location = New System.Drawing.Point(8, 33)
        Me.txtDefSubject.Name = "txtDefSubject"
        Me.txtDefSubject.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefSubject.Size = New System.Drawing.Size(440, 21)
        Me.txtDefSubject.TabIndex = 0
        Me.txtDefSubject.Tag = "memo"
        '
        'Label18
        '
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(8, 96)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(216, 16)
        Me.Label18.TabIndex = 0
        Me.Label18.Text = "Default Email Message"
        '
        'txtDefaultEmail
        '
        Me.txtDefaultEmail.ForeColor = System.Drawing.Color.Blue
        Me.txtDefaultEmail.Location = New System.Drawing.Point(8, 112)
        Me.txtDefaultEmail.Multiline = True
        Me.txtDefaultEmail.Name = "txtDefaultEmail"
        Me.txtDefaultEmail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefaultEmail.Size = New System.Drawing.Size(440, 120)
        Me.txtDefaultEmail.TabIndex = 0
        Me.txtDefaultEmail.Tag = "memo"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 240)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(216, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Default Email Signature"
        '
        'txtSig
        '
        Me.txtSig.ForeColor = System.Drawing.Color.Blue
        Me.txtSig.Location = New System.Drawing.Point(8, 256)
        Me.txtSig.Multiline = True
        Me.txtSig.Name = "txtSig"
        Me.txtSig.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtSig.Size = New System.Drawing.Size(440, 56)
        Me.txtSig.TabIndex = 0
        Me.txtSig.Tag = "memo"
        '
        'cmdSpell
        '
        Me.cmdSpell.Image = CType(resources.GetObject("cmdSpell.Image"), System.Drawing.Image)
        Me.cmdSpell.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSpell.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSpell.Location = New System.Drawing.Point(8, 320)
        Me.cmdSpell.Name = "cmdSpell"
        Me.cmdSpell.Size = New System.Drawing.Size(88, 23)
        Me.cmdSpell.TabIndex = 2
        Me.cmdSpell.Text = "Spell Check"
        Me.cmdSpell.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label31
        '
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(8, 56)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(216, 16)
        Me.Label31.TabIndex = 0
        Me.Label31.Text = "Default Attachment"
        '
        'txtDefAttach
        '
        Me.txtDefAttach.BackColor = System.Drawing.Color.White
        Me.txtDefAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtDefAttach.Location = New System.Drawing.Point(8, 72)
        Me.txtDefAttach.Name = "txtDefAttach"
        Me.txtDefAttach.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDefAttach.Size = New System.Drawing.Size(376, 21)
        Me.txtDefAttach.TabIndex = 0
        Me.txtDefAttach.Tag = "memo"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.UcLogin)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(480, 366)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Database Defaults"
        '
        'UcLogin
        '
        Me.UcLogin.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcLogin.Location = New System.Drawing.Point(8, 8)
        Me.UcLogin.Name = "UcLogin"
        Me.UcLogin.Size = New System.Drawing.Size(416, 128)
        Me.UcLogin.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox5)
        Me.TabPage3.Controls.Add(Me.GroupBox13)
        Me.TabPage3.Controls.Add(Me.GroupBox10)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(480, 366)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Miscellaneous"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lsvServers)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 72)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(461, 214)
        Me.GroupBox5.TabIndex = 2
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Previous Servers"
        '
        'lsvServers
        '
        Me.lsvServers.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader7})
        Me.lsvServers.FullRowSelect = True
        Me.lsvServers.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvServers.HideSelection = False
        Me.lsvServers.Location = New System.Drawing.Point(14, 15)
        Me.lsvServers.Name = "lsvServers"
        Me.lsvServers.Size = New System.Drawing.Size(433, 193)
        Me.SuperTooltip1.SetSuperTooltip(Me.lsvServers, New DevComponents.DotNetBar.SuperTooltipInfo("Previous Report Servers", "", "Items in this list are automagically added by SQL-RD as you create schedules. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "T" & _
                    "o remove an item, simply select it and press the 'Delete' button on your keyboar" & _
                    "d.", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, True, False, New System.Drawing.Size(0, 0)))
        Me.lsvServers.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.lsvServers, "Items in this list are automagically added by SQL-RD as you create schedules. ")
        Me.lsvServers.UseCompatibleStateImageBehavior = False
        Me.lsvServers.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Width = 426
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.cmbDateTime)
        Me.GroupBox13.Location = New System.Drawing.Point(10, 303)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(464, 56)
        Me.GroupBox13.TabIndex = 1
        Me.GroupBox13.TabStop = False
        Me.GroupBox13.Text = "Default Date/Time Stamp"
        '
        'cmbDateTime
        '
        Me.cmbDateTime.Items.AddRange(New Object() {"ddHH", "ddMM", "ddMMyy", "ddMMyyHHmm", "ddMMyyHHmms", "ddMMyyyy", "ddMMyyyyHHmm", "ddMMyyyyHHmmss", "HHmm", "HHmmss", "MMddyy", "MMddyyHHmm", "MMddyyHHmmss", "MMddyyyy", "MMddyyyyHHmm", "MMddyyyyHHmmss", "yyMMdd", "yyMMddHHmm", "yyMMddHHmmss", "yyyyMMdd", "yyyyMMddHHmm", "yyyyMMddHHmmss", "MMMMM"})
        Me.cmbDateTime.Location = New System.Drawing.Point(8, 24)
        Me.cmbDateTime.Name = "cmbDateTime"
        Me.cmbDateTime.Size = New System.Drawing.Size(208, 21)
        Me.cmbDateTime.TabIndex = 0
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.cmdRptLoc)
        Me.GroupBox10.Controls.Add(Me.txtReportLoc)
        Me.GroupBox10.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(464, 56)
        Me.GroupBox10.TabIndex = 0
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Default Report Location"
        '
        'cmdRptLoc
        '
        Me.cmdRptLoc.Image = CType(resources.GetObject("cmdRptLoc.Image"), System.Drawing.Image)
        Me.cmdRptLoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRptLoc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRptLoc.Location = New System.Drawing.Point(400, 24)
        Me.cmdRptLoc.Name = "cmdRptLoc"
        Me.cmdRptLoc.Size = New System.Drawing.Size(52, 21)
        Me.cmdRptLoc.TabIndex = 4
        Me.cmdRptLoc.Text = "..."
        Me.cmdRptLoc.Visible = False
        '
        'txtReportLoc
        '
        Me.txtReportLoc.Location = New System.Drawing.Point(8, 24)
        Me.txtReportLoc.Name = "txtReportLoc"
        Me.txtReportLoc.Size = New System.Drawing.Size(376, 21)
        Me.txtReportLoc.TabIndex = 1
        '
        'tbUserDefaults
        '
        Me.tbUserDefaults.AttachedControl = Me.TabControlPanel5
        Me.tbUserDefaults.Image = CType(resources.GetObject("tbUserDefaults.Image"), System.Drawing.Image)
        Me.tbUserDefaults.Name = "tbUserDefaults"
        Me.tbUserDefaults.Text = "User Defaults"
        '
        'TabControlPanel9
        '
        Me.TabControlPanel9.Controls.Add(Me.GroupBox14)
        Me.TabControlPanel9.Controls.Add(Me.cmdReportCache)
        Me.TabControlPanel9.Controls.Add(Me.txtReportCache)
        Me.TabControlPanel9.Controls.Add(Me.txtTempFolder)
        Me.TabControlPanel9.Controls.Add(Me.cmdTempFolder)
        Me.TabControlPanel9.Controls.Add(Me.Label42)
        Me.TabControlPanel9.Controls.Add(Me.txtCachedData)
        Me.TabControlPanel9.Controls.Add(Me.cmdSnapshots)
        Me.TabControlPanel9.Controls.Add(Me.cmdCachedData)
        Me.TabControlPanel9.Controls.Add(Me.txtSnapShots)
        Me.TabControlPanel9.Controls.Add(Me.Label17)
        Me.TabControlPanel9.Controls.Add(Me.Label16)
        Me.TabControlPanel9.Controls.Add(Me.Label43)
        Me.TabControlPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel9.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel9.Name = "TabControlPanel9"
        Me.TabControlPanel9.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel9.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel9.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel9.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel9.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel9.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel9.TabIndex = 9
        Me.TabControlPanel9.TabItem = Me.tbPaths
        '
        'GroupBox14
        '
        Me.GroupBox14.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox14.Controls.Add(Me.chkConvertToMsg)
        Me.GroupBox14.Controls.Add(Me.txtSentMessages)
        Me.GroupBox14.Controls.Add(Me.btnSentMessages)
        Me.GroupBox14.Controls.Add(Me.Label52)
        Me.GroupBox14.Location = New System.Drawing.Point(4, 145)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(520, 84)
        Me.GroupBox14.TabIndex = 9
        Me.GroupBox14.TabStop = False
        '
        'chkConvertToMsg
        '
        Me.chkConvertToMsg.AutoSize = True
        Me.chkConvertToMsg.BackColor = System.Drawing.Color.Transparent
        Me.chkConvertToMsg.Location = New System.Drawing.Point(5, 59)
        Me.chkConvertToMsg.Name = "chkConvertToMsg"
        Me.chkConvertToMsg.Size = New System.Drawing.Size(291, 17)
        Me.chkConvertToMsg.TabIndex = 8
        Me.chkConvertToMsg.Text = "Convert files to ""MSG"" format for viewing in MS Outlook"
        Me.chkConvertToMsg.UseVisualStyleBackColor = False
        '
        'txtSentMessages
        '
        Me.txtSentMessages.Location = New System.Drawing.Point(6, 32)
        Me.txtSentMessages.Name = "txtSentMessages"
        Me.txtSentMessages.Size = New System.Drawing.Size(424, 21)
        Me.txtSentMessages.TabIndex = 6
        '
        'btnSentMessages
        '
        Me.btnSentMessages.Image = CType(resources.GetObject("btnSentMessages.Image"), System.Drawing.Image)
        Me.btnSentMessages.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnSentMessages.Location = New System.Drawing.Point(436, 30)
        Me.btnSentMessages.Name = "btnSentMessages"
        Me.btnSentMessages.Size = New System.Drawing.Size(52, 21)
        Me.btnSentMessages.TabIndex = 7
        Me.btnSentMessages.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label52
        '
        Me.Label52.BackColor = System.Drawing.Color.Transparent
        Me.Label52.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label52.Location = New System.Drawing.Point(3, 16)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(216, 16)
        Me.Label52.TabIndex = 0
        Me.Label52.Text = "Sent messages Repository (SMTP)"
        '
        'cmdReportCache
        '
        Me.cmdReportCache.Image = CType(resources.GetObject("cmdReportCache.Image"), System.Drawing.Image)
        Me.cmdReportCache.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdReportCache.Location = New System.Drawing.Point(435, 332)
        Me.cmdReportCache.Name = "cmdReportCache"
        Me.cmdReportCache.Size = New System.Drawing.Size(52, 21)
        Me.cmdReportCache.TabIndex = 7
        Me.cmdReportCache.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdReportCache.Visible = False
        '
        'txtReportCache
        '
        Me.txtReportCache.Location = New System.Drawing.Point(4, 332)
        Me.txtReportCache.Name = "txtReportCache"
        Me.txtReportCache.Size = New System.Drawing.Size(424, 21)
        Me.txtReportCache.TabIndex = 6
        Me.txtReportCache.Visible = False
        '
        'txtTempFolder
        '
        Me.txtTempFolder.Location = New System.Drawing.Point(5, 121)
        Me.txtTempFolder.Name = "txtTempFolder"
        Me.txtTempFolder.Size = New System.Drawing.Size(424, 21)
        Me.txtTempFolder.TabIndex = 4
        '
        'cmdTempFolder
        '
        Me.cmdTempFolder.Image = CType(resources.GetObject("cmdTempFolder.Image"), System.Drawing.Image)
        Me.cmdTempFolder.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTempFolder.Location = New System.Drawing.Point(437, 122)
        Me.cmdTempFolder.Name = "cmdTempFolder"
        Me.cmdTempFolder.Size = New System.Drawing.Size(52, 21)
        Me.cmdTempFolder.TabIndex = 5
        Me.cmdTempFolder.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label42
        '
        Me.Label42.BackColor = System.Drawing.Color.Transparent
        Me.Label42.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label42.Location = New System.Drawing.Point(5, 12)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(216, 16)
        Me.Label42.TabIndex = 0
        Me.Label42.Text = "Cached Data Folder"
        '
        'txtCachedData
        '
        Me.txtCachedData.Location = New System.Drawing.Point(5, 28)
        Me.txtCachedData.Name = "txtCachedData"
        Me.txtCachedData.Size = New System.Drawing.Size(424, 21)
        Me.txtCachedData.TabIndex = 0
        '
        'cmdSnapshots
        '
        Me.cmdSnapshots.Image = CType(resources.GetObject("cmdSnapshots.Image"), System.Drawing.Image)
        Me.cmdSnapshots.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSnapshots.Location = New System.Drawing.Point(437, 76)
        Me.cmdSnapshots.Name = "cmdSnapshots"
        Me.cmdSnapshots.Size = New System.Drawing.Size(52, 21)
        Me.cmdSnapshots.TabIndex = 3
        Me.cmdSnapshots.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdCachedData
        '
        Me.cmdCachedData.Image = CType(resources.GetObject("cmdCachedData.Image"), System.Drawing.Image)
        Me.cmdCachedData.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCachedData.Location = New System.Drawing.Point(437, 28)
        Me.cmdCachedData.Name = "cmdCachedData"
        Me.cmdCachedData.Size = New System.Drawing.Size(52, 21)
        Me.cmdCachedData.TabIndex = 1
        Me.cmdCachedData.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtSnapShots
        '
        Me.txtSnapShots.Location = New System.Drawing.Point(5, 76)
        Me.txtSnapShots.Name = "txtSnapShots"
        Me.txtSnapShots.Size = New System.Drawing.Size(424, 21)
        Me.txtSnapShots.TabIndex = 2
        '
        'Label17
        '
        Me.Label17.BackColor = System.Drawing.Color.Transparent
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(1, 317)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(216, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Cached Reports folder"
        Me.Label17.Visible = False
        '
        'Label16
        '
        Me.Label16.BackColor = System.Drawing.Color.Transparent
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(3, 105)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(216, 16)
        Me.Label16.TabIndex = 0
        Me.Label16.Text = "Temporary output folder"
        '
        'Label43
        '
        Me.Label43.BackColor = System.Drawing.Color.Transparent
        Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label43.Location = New System.Drawing.Point(5, 60)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(216, 16)
        Me.Label43.TabIndex = 0
        Me.Label43.Text = "Snapshots Folder"
        '
        'tbPaths
        '
        Me.tbPaths.AttachedControl = Me.TabControlPanel9
        Me.tbPaths.Image = CType(resources.GetObject("tbPaths.Image"), System.Drawing.Image)
        Me.tbPaths.Name = "tbPaths"
        Me.tbPaths.Text = "System Paths"
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.GroupBox16)
        Me.TabControlPanel3.Controls.Add(Me.chkUniversal)
        Me.TabControlPanel3.Controls.Add(Me.GroupBox2)
        Me.TabControlPanel3.Controls.Add(Me.cmdServiceApply)
        Me.TabControlPanel3.Controls.Add(Me.grpServices)
        Me.TabControlPanel3.Controls.Add(Me.cmdServiceStart)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbScheduling
        '
        'GroupBox16
        '
        Me.GroupBox16.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox16.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox16.Location = New System.Drawing.Point(8, 222)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(414, 88)
        Me.GroupBox16.TabIndex = 4
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "Polling Intervals (Seconds)"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPollIntEB, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPriority, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label25, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPollInt, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label47, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label48, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.cmbPriorityEB, 3, 1)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(8, 17)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(399, 62)
        Me.TableLayoutPanel2.TabIndex = 12
        '
        'cmbPollIntEB
        '
        Me.cmbPollIntEB.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbPollIntEB.Location = New System.Drawing.Point(118, 34)
        Me.cmbPollIntEB.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.cmbPollIntEB.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbPollIntEB.Name = "cmbPollIntEB"
        Me.cmbPollIntEB.Size = New System.Drawing.Size(55, 21)
        Me.cmbPollIntEB.TabIndex = 2
        Me.cmbPollIntEB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbPollIntEB.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmbPriority
        '
        Me.cmbPriority.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriority.FormattingEnabled = True
        Me.cmbPriority.Items.AddRange(New Object() {"Real Time", "High", "Above Normal", "Normal", "Below Normal", "Low"})
        Me.cmbPriority.Location = New System.Drawing.Point(259, 3)
        Me.cmbPriority.Name = "cmbPriority"
        Me.cmbPriority.Size = New System.Drawing.Size(109, 21)
        Me.cmbPriority.TabIndex = 1
        Me.cmbPriority.Tag = "unsorted"
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(3, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(109, 31)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Standard"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label25.BackColor = System.Drawing.Color.Transparent
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(179, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(74, 31)
        Me.Label25.TabIndex = 4
        Me.Label25.Text = "CPU Priority"
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbPollInt
        '
        Me.cmbPollInt.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmbPollInt.Location = New System.Drawing.Point(118, 3)
        Me.cmbPollInt.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.cmbPollInt.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbPollInt.Name = "cmbPollInt"
        Me.cmbPollInt.Size = New System.Drawing.Size(55, 21)
        Me.cmbPollInt.TabIndex = 0
        Me.cmbPollInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbPollInt.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'Label47
        '
        Me.Label47.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label47.Location = New System.Drawing.Point(3, 31)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(109, 31)
        Me.Label47.TabIndex = 0
        Me.Label47.Text = "Event-Based"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label48
        '
        Me.Label48.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label48.BackColor = System.Drawing.Color.Transparent
        Me.Label48.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label48.Location = New System.Drawing.Point(179, 31)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(74, 31)
        Me.Label48.TabIndex = 4
        Me.Label48.Text = "CPU Priority"
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmbPriorityEB
        '
        Me.cmbPriorityEB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPriorityEB.FormattingEnabled = True
        Me.cmbPriorityEB.Items.AddRange(New Object() {"Real Time", "High", "Above Normal", "Normal", "Below Normal", "Low"})
        Me.cmbPriorityEB.Location = New System.Drawing.Point(259, 34)
        Me.cmbPriorityEB.Name = "cmbPriorityEB"
        Me.cmbPriorityEB.Size = New System.Drawing.Size(109, 21)
        Me.cmbPriorityEB.TabIndex = 3
        Me.cmbPriorityEB.Tag = "unsorted"
        '
        'chkUniversal
        '
        Me.chkUniversal.AutoSize = True
        Me.chkUniversal.BackColor = System.Drawing.Color.Transparent
        Me.chkUniversal.Location = New System.Drawing.Point(20, 253)
        Me.chkUniversal.Name = "chkUniversal"
        Me.chkUniversal.Size = New System.Drawing.Size(197, 17)
        Me.chkUniversal.TabIndex = 55
        Me.chkUniversal.TabStop = False
        Me.chkUniversal.Text = "Scheduler should use Universal time"
        Me.chkUniversal.UseVisualStyleBackColor = False
        Me.chkUniversal.Visible = False
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.Label53)
        Me.GroupBox2.Controls.Add(Me.txtDelayBy)
        Me.GroupBox2.Controls.Add(Me.chkDelayRestart)
        Me.GroupBox2.Controls.Add(Me.chkCheckService)
        Me.GroupBox2.Controls.Add(Me.Label21)
        Me.GroupBox2.Controls.Add(Me.txtThreadCount)
        Me.GroupBox2.Controls.Add(Me.chkThreading)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 313)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(416, 117)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Miscellaneous"
        '
        'Label53
        '
        Me.Label53.Location = New System.Drawing.Point(344, 78)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(44, 21)
        Me.Label53.TabIndex = 6
        Me.Label53.Text = "minutes"
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtDelayBy
        '
        Me.txtDelayBy.Enabled = False
        Me.txtDelayBy.Location = New System.Drawing.Point(295, 78)
        Me.txtDelayBy.Maximum = New Decimal(New Integer() {9999, 0, 0, 0})
        Me.txtDelayBy.Name = "txtDelayBy"
        Me.txtDelayBy.Size = New System.Drawing.Size(44, 21)
        Me.txtDelayBy.TabIndex = 5
        Me.txtDelayBy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtDelayBy.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'chkDelayRestart
        '
        Me.chkDelayRestart.AutoSize = True
        Me.chkDelayRestart.Location = New System.Drawing.Point(14, 80)
        Me.chkDelayRestart.Name = "chkDelayRestart"
        Me.chkDelayRestart.Size = New System.Drawing.Size(284, 17)
        Me.chkDelayRestart.TabIndex = 4
        Me.chkDelayRestart.Text = "On editor start-up, delay restarting the scheduler for "
        Me.chkDelayRestart.UseVisualStyleBackColor = True
        '
        'chkCheckService
        '
        Me.chkCheckService.BackColor = System.Drawing.Color.Transparent
        Me.chkCheckService.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCheckService.Location = New System.Drawing.Point(14, 50)
        Me.chkCheckService.Name = "chkCheckService"
        Me.chkCheckService.Size = New System.Drawing.Size(399, 24)
        Me.chkCheckService.TabIndex = 0
        Me.chkCheckService.Text = "Do not check and restart the scheduler on editor start-up (not recommended)"
        Me.chkCheckService.UseVisualStyleBackColor = False
        '
        'Label21
        '
        Me.Label21.Location = New System.Drawing.Point(343, 21)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(44, 21)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "threads"
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtThreadCount
        '
        Me.txtThreadCount.Enabled = False
        Me.txtThreadCount.Location = New System.Drawing.Point(295, 23)
        Me.txtThreadCount.Maximum = New Decimal(New Integer() {8, 0, 0, 0})
        Me.txtThreadCount.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtThreadCount.Name = "txtThreadCount"
        Me.txtThreadCount.Size = New System.Drawing.Size(44, 21)
        Me.txtThreadCount.TabIndex = 1
        Me.txtThreadCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtThreadCount.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        'chkThreading
        '
        Me.chkThreading.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkThreading.Location = New System.Drawing.Point(14, 21)
        Me.chkThreading.Name = "chkThreading"
        Me.chkThreading.Size = New System.Drawing.Size(281, 24)
        Me.chkThreading.TabIndex = 0
        Me.chkThreading.Text = "Enable Multi-Threading technology and use up to "
        '
        'cmdServiceApply
        '
        Me.cmdServiceApply.Enabled = False
        Me.cmdServiceApply.Image = CType(resources.GetObject("cmdServiceApply.Image"), System.Drawing.Image)
        Me.cmdServiceApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdServiceApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdServiceApply.Location = New System.Drawing.Point(427, 10)
        Me.cmdServiceApply.Name = "cmdServiceApply"
        Me.cmdServiceApply.Size = New System.Drawing.Size(96, 23)
        Me.cmdServiceApply.TabIndex = 7
        Me.cmdServiceApply.Text = "Apply Setting"
        Me.cmdServiceApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'grpServices
        '
        Me.grpServices.BackColor = System.Drawing.Color.Transparent
        Me.grpServices.Controls.Add(Me.grpNTService)
        Me.grpServices.Controls.Add(Me.optNoScheduling)
        Me.grpServices.Controls.Add(Me.optBackgroundApp)
        Me.grpServices.Controls.Add(Me.optNTService)
        Me.grpServices.Location = New System.Drawing.Point(9, -2)
        Me.grpServices.Name = "grpServices"
        Me.grpServices.Size = New System.Drawing.Size(413, 218)
        Me.grpServices.TabIndex = 0
        Me.grpServices.TabStop = False
        '
        'grpNTService
        '
        Me.grpNTService.Controls.Add(Me.txtWinDomain)
        Me.grpNTService.Controls.Add(Me.Label13)
        Me.grpNTService.Controls.Add(Me.Label14)
        Me.grpNTService.Controls.Add(Me.txtWinUser)
        Me.grpNTService.Controls.Add(Me.txtWinPassword)
        Me.grpNTService.Controls.Add(Me.Label15)
        Me.grpNTService.Location = New System.Drawing.Point(15, 93)
        Me.grpNTService.Name = "grpNTService"
        Me.grpNTService.Size = New System.Drawing.Size(380, 114)
        Me.grpNTService.TabIndex = 3
        Me.grpNTService.TabStop = False
        Me.grpNTService.Text = "NT Service Logon Credentials"
        '
        'txtWinDomain
        '
        Me.txtWinDomain.ForeColor = System.Drawing.Color.Blue
        Me.txtWinDomain.Location = New System.Drawing.Point(132, 22)
        Me.txtWinDomain.Name = "txtWinDomain"
        Me.txtWinDomain.Size = New System.Drawing.Size(228, 21)
        Me.txtWinDomain.TabIndex = 0
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 24)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "Windows Domain"
        '
        'Label14
        '
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 55)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(116, 16)
        Me.Label14.TabIndex = 0
        Me.Label14.Text = "Windows User Name"
        '
        'txtWinUser
        '
        Me.txtWinUser.ForeColor = System.Drawing.Color.Blue
        Me.txtWinUser.Location = New System.Drawing.Point(132, 53)
        Me.txtWinUser.Name = "txtWinUser"
        Me.txtWinUser.Size = New System.Drawing.Size(228, 21)
        Me.txtWinUser.TabIndex = 1
        '
        'txtWinPassword
        '
        Me.txtWinPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtWinPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtWinPassword.Location = New System.Drawing.Point(132, 84)
        Me.txtWinPassword.Name = "txtWinPassword"
        Me.txtWinPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtWinPassword.Size = New System.Drawing.Size(228, 21)
        Me.txtWinPassword.TabIndex = 2
        '
        'Label15
        '
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 86)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(120, 16)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "Windows Password"
        '
        'optNoScheduling
        '
        Me.optNoScheduling.Checked = True
        Me.optNoScheduling.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNoScheduling.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNoScheduling.Location = New System.Drawing.Point(8, 16)
        Me.optNoScheduling.Name = "optNoScheduling"
        Me.optNoScheduling.Size = New System.Drawing.Size(352, 24)
        Me.optNoScheduling.TabIndex = 0
        Me.optNoScheduling.TabStop = True
        Me.optNoScheduling.Text = "No Scheduling required"
        '
        'optBackgroundApp
        '
        Me.optBackgroundApp.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optBackgroundApp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.optBackgroundApp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optBackgroundApp.Location = New System.Drawing.Point(8, 41)
        Me.optBackgroundApp.Name = "optBackgroundApp"
        Me.optBackgroundApp.Size = New System.Drawing.Size(352, 24)
        Me.optBackgroundApp.TabIndex = 1
        Me.optBackgroundApp.Text = "Use background scheduling application (recommended)"
        '
        'optNTService
        '
        Me.optNTService.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.optNTService.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNTService.Location = New System.Drawing.Point(8, 66)
        Me.optNTService.Name = "optNTService"
        Me.optNTService.Size = New System.Drawing.Size(352, 24)
        Me.optNTService.TabIndex = 2
        Me.optNTService.Text = "Use NT Service (advanced users on Windows NT, 2000 or later)"
        '
        'cmdServiceStart
        '
        Me.cmdServiceStart.Enabled = False
        Me.cmdServiceStart.Image = CType(resources.GetObject("cmdServiceStart.Image"), System.Drawing.Image)
        Me.cmdServiceStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdServiceStart.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdServiceStart.Location = New System.Drawing.Point(427, 39)
        Me.cmdServiceStart.Name = "cmdServiceStart"
        Me.cmdServiceStart.Size = New System.Drawing.Size(96, 23)
        Me.cmdServiceStart.TabIndex = 8
        Me.cmdServiceStart.Text = "Start Service"
        Me.cmdServiceStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tbScheduling
        '
        Me.tbScheduling.AttachedControl = Me.TabControlPanel3
        Me.tbScheduling.Image = CType(resources.GetObject("tbScheduling.Image"), System.Drawing.Image)
        Me.tbScheduling.Name = "tbScheduling"
        Me.tbScheduling.Text = "Scheduler"
        '
        'TabControlPanel8
        '
        Me.TabControlPanel8.Controls.Add(Me.GroupBox12)
        Me.TabControlPanel8.Controls.Add(Me.chkAudit)
        Me.TabControlPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel8.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel8.Name = "TabControlPanel8"
        Me.TabControlPanel8.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel8.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel8.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel8.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel8.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel8.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel8.TabIndex = 8
        Me.TabControlPanel8.TabItem = Me.tbAuditTrail
        '
        'GroupBox12
        '
        Me.GroupBox12.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox12.Controls.Add(Me.Label44)
        Me.GroupBox12.Controls.Add(Me.cmdTest)
        Me.GroupBox12.Controls.Add(Me.ucDSN)
        Me.GroupBox12.Enabled = False
        Me.GroupBox12.Location = New System.Drawing.Point(5, 40)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(488, 368)
        Me.GroupBox12.TabIndex = 2
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "Audit Trail Database Setup"
        '
        'Label44
        '
        Me.Label44.Location = New System.Drawing.Point(8, 32)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(456, 16)
        Me.Label44.TabIndex = 0
        Me.Label44.Text = "Please provide the Data Source Name that will be used to store Audit trail data"
        '
        'cmdTest
        '
        Me.cmdTest.Location = New System.Drawing.Point(200, 176)
        Me.cmdTest.Name = "cmdTest"
        Me.cmdTest.Size = New System.Drawing.Size(75, 23)
        Me.cmdTest.TabIndex = 1
        Me.cmdTest.Text = "Connect"
        '
        'ucDSN
        '
        Me.ucDSN.BackColor = System.Drawing.Color.Transparent
        Me.ucDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucDSN.ForeColor = System.Drawing.Color.Navy
        Me.ucDSN.Location = New System.Drawing.Point(8, 56)
        Me.ucDSN.Name = "ucDSN"
        Me.ucDSN.Size = New System.Drawing.Size(472, 112)
        Me.ucDSN.TabIndex = 0
        '
        'chkAudit
        '
        Me.chkAudit.BackColor = System.Drawing.Color.Transparent
        Me.chkAudit.Location = New System.Drawing.Point(5, 9)
        Me.chkAudit.Name = "chkAudit"
        Me.chkAudit.Size = New System.Drawing.Size(240, 24)
        Me.chkAudit.TabIndex = 0
        Me.chkAudit.Text = "Enable audit trial"
        Me.chkAudit.UseVisualStyleBackColor = False
        '
        'tbAuditTrail
        '
        Me.tbAuditTrail.AttachedControl = Me.TabControlPanel8
        Me.tbAuditTrail.Image = CType(resources.GetObject("tbAuditTrail.Image"), System.Drawing.Image)
        Me.tbAuditTrail.Name = "tbAuditTrail"
        Me.tbAuditTrail.Text = "Audit Trail"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.tabMessaging)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbMessaging
        '
        'tabMessaging
        '
        Me.tabMessaging.Controls.Add(Me.TabPage4)
        Me.tabMessaging.Controls.Add(Me.TabPage5)
        Me.tabMessaging.ItemSize = New System.Drawing.Size(42, 18)
        Me.tabMessaging.Location = New System.Drawing.Point(8, 8)
        Me.tabMessaging.Name = "tabMessaging"
        Me.tabMessaging.SelectedIndex = 0
        Me.tabMessaging.Size = New System.Drawing.Size(480, 392)
        Me.tabMessaging.TabIndex = 8
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.grpSMTP)
        Me.TabPage4.Controls.Add(Me.grpMAPI)
        Me.TabPage4.Controls.Add(Me.grpGroupwise)
        Me.TabPage4.Controls.Add(Me.grpCRDMail)
        Me.TabPage4.Controls.Add(Me.GroupBox11)
        Me.TabPage4.Controls.Add(Me.cmbMailType)
        Me.TabPage4.Controls.Add(Me.Label5)
        Me.TabPage4.Controls.Add(Me.chkEmailRetry)
        Me.TabPage4.Controls.Add(Me.cmdMailTest)
        Me.TabPage4.Controls.Add(Me.cmdClearSettings)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(472, 366)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Email"
        '
        'grpSMTP
        '
        Me.grpSMTP.Controls.Add(Me.btnSMTPAdvanced)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPort)
        Me.grpSMTP.Controls.Add(Me.cmbSMTPTimeout)
        Me.grpSMTP.Controls.Add(Me.cmdSMTPMore)
        Me.grpSMTP.Controls.Add(Me.Label19)
        Me.grpSMTP.Controls.Add(Me.txtSMTPUserID)
        Me.grpSMTP.Controls.Add(Me.Label8)
        Me.grpSMTP.Controls.Add(Me.Label9)
        Me.grpSMTP.Controls.Add(Me.txtSMTPPassword)
        Me.grpSMTP.Controls.Add(Me.Label49)
        Me.grpSMTP.Controls.Add(Me.Label10)
        Me.grpSMTP.Controls.Add(Me.txtSMTPServer)
        Me.grpSMTP.Controls.Add(Me.Label11)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderAddress)
        Me.grpSMTP.Controls.Add(Me.Label12)
        Me.grpSMTP.Controls.Add(Me.txtSMTPSenderName)
        Me.grpSMTP.Location = New System.Drawing.Point(8, 40)
        Me.grpSMTP.Name = "grpSMTP"
        Me.grpSMTP.Size = New System.Drawing.Size(424, 216)
        Me.grpSMTP.TabIndex = 2
        Me.grpSMTP.TabStop = False
        Me.grpSMTP.Text = "SMTP Mail"
        '
        'btnSMTPAdvanced
        '
        Me.btnSMTPAdvanced.Location = New System.Drawing.Point(230, 184)
        Me.btnSMTPAdvanced.Name = "btnSMTPAdvanced"
        Me.btnSMTPAdvanced.Size = New System.Drawing.Size(66, 21)
        Me.btnSMTPAdvanced.TabIndex = 9
        Me.btnSMTPAdvanced.Text = "Advanced"
        Me.btnSMTPAdvanced.UseVisualStyleBackColor = True
        '
        'txtSMTPPort
        '
        Me.txtSMTPPort.Location = New System.Drawing.Point(310, 88)
        Me.txtSMTPPort.Maximum = New Decimal(New Integer() {80000, 0, 0, 0})
        Me.txtSMTPPort.Name = "txtSMTPPort"
        Me.txtSMTPPort.Size = New System.Drawing.Size(58, 21)
        Me.txtSMTPPort.TabIndex = 8
        Me.txtSMTPPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSMTPPort.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'cmbSMTPTimeout
        '
        Me.cmbSMTPTimeout.Location = New System.Drawing.Point(136, 184)
        Me.cmbSMTPTimeout.Maximum = New Decimal(New Integer() {360, 0, 0, 0})
        Me.cmbSMTPTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbSMTPTimeout.Name = "cmbSMTPTimeout"
        Me.cmbSMTPTimeout.Size = New System.Drawing.Size(65, 21)
        Me.cmbSMTPTimeout.TabIndex = 7
        Me.cmbSMTPTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbSMTPTimeout.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'cmdSMTPMore
        '
        Me.cmdSMTPMore.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSMTPMore.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSMTPMore.Location = New System.Drawing.Point(302, 184)
        Me.cmdSMTPMore.Name = "cmdSMTPMore"
        Me.cmdSMTPMore.Size = New System.Drawing.Size(66, 21)
        Me.cmdSMTPMore.TabIndex = 6
        Me.cmdSMTPMore.Text = "Add more"
        '
        'Label19
        '
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(8, 186)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(96, 16)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "SMTP Timeout"
        '
        'txtSMTPUserID
        '
        Me.txtSMTPUserID.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPUserID.Location = New System.Drawing.Point(136, 24)
        Me.txtSMTPUserID.Name = "txtSMTPUserID"
        Me.txtSMTPUserID.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPUserID.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(8, 24)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 16)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "User ID"
        '
        'Label9
        '
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(8, 56)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(56, 16)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "Password"
        '
        'txtSMTPPassword
        '
        Me.txtSMTPPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMTPPassword.Location = New System.Drawing.Point(136, 56)
        Me.txtSMTPPassword.Name = "txtSMTPPassword"
        Me.txtSMTPPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMTPPassword.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPPassword.TabIndex = 1
        '
        'Label49
        '
        Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label49.Location = New System.Drawing.Point(261, 90)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(43, 16)
        Me.Label49.TabIndex = 0
        Me.Label49.Text = "Port"
        '
        'Label10
        '
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(8, 88)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 16)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Server Name"
        '
        'txtSMTPServer
        '
        Me.txtSMTPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPServer.Location = New System.Drawing.Point(136, 88)
        Me.txtSMTPServer.Name = "txtSMTPServer"
        Me.txtSMTPServer.Size = New System.Drawing.Size(120, 21)
        Me.txtSMTPServer.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 120)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(96, 16)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Sender Address"
        '
        'txtSMTPSenderAddress
        '
        Me.txtSMTPSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderAddress.Location = New System.Drawing.Point(136, 120)
        Me.txtSMTPSenderAddress.Name = "txtSMTPSenderAddress"
        Me.txtSMTPSenderAddress.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPSenderAddress.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 152)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 16)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "Sender Name"
        '
        'txtSMTPSenderName
        '
        Me.txtSMTPSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSMTPSenderName.Location = New System.Drawing.Point(136, 152)
        Me.txtSMTPSenderName.Name = "txtSMTPSenderName"
        Me.txtSMTPSenderName.Size = New System.Drawing.Size(232, 21)
        Me.txtSMTPSenderName.TabIndex = 4
        '
        'grpMAPI
        '
        Me.grpMAPI.Controls.Add(Me.btnMAPIMore)
        Me.grpMAPI.Controls.Add(Me.txtMAPIPassword)
        Me.grpMAPI.Controls.Add(Me.cmbMAPIProfile)
        Me.grpMAPI.Controls.Add(Me.Label6)
        Me.grpMAPI.Controls.Add(Me.Label7)
        Me.grpMAPI.Location = New System.Drawing.Point(8, 40)
        Me.grpMAPI.Name = "grpMAPI"
        Me.grpMAPI.Size = New System.Drawing.Size(424, 184)
        Me.grpMAPI.TabIndex = 2
        Me.grpMAPI.TabStop = False
        Me.grpMAPI.Text = "MAPI Mail"
        '
        'btnMAPIMore
        '
        Me.btnMAPIMore.Location = New System.Drawing.Point(310, 116)
        Me.btnMAPIMore.Name = "btnMAPIMore"
        Me.btnMAPIMore.Size = New System.Drawing.Size(58, 23)
        Me.btnMAPIMore.TabIndex = 2
        Me.btnMAPIMore.Text = "More..."
        Me.btnMAPIMore.UseVisualStyleBackColor = True
        '
        'txtMAPIPassword
        '
        Me.txtMAPIPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtMAPIPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtMAPIPassword.Location = New System.Drawing.Point(8, 88)
        Me.txtMAPIPassword.Name = "txtMAPIPassword"
        Me.txtMAPIPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtMAPIPassword.Size = New System.Drawing.Size(360, 21)
        Me.txtMAPIPassword.TabIndex = 1
        '
        'cmbMAPIProfile
        '
        Me.cmbMAPIProfile.ForeColor = System.Drawing.Color.Blue
        Me.cmbMAPIProfile.ItemHeight = 13
        Me.cmbMAPIProfile.Location = New System.Drawing.Point(8, 40)
        Me.cmbMAPIProfile.Name = "cmbMAPIProfile"
        Me.cmbMAPIProfile.Size = New System.Drawing.Size(360, 21)
        Me.cmbMAPIProfile.TabIndex = 0
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(8, 24)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 16)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "MAPI Profile"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 16)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Password"
        '
        'grpGroupwise
        '
        Me.grpGroupwise.Controls.Add(Me.TableLayoutPanel1)
        Me.grpGroupwise.Location = New System.Drawing.Point(8, 40)
        Me.grpGroupwise.Name = "grpGroupwise"
        Me.grpGroupwise.Size = New System.Drawing.Size(424, 216)
        Me.grpGroupwise.TabIndex = 11
        Me.grpGroupwise.TabStop = False
        Me.grpGroupwise.Text = "Groupwise"
        Me.grpGroupwise.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.Label28, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwUser, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label29, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label41, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label45, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label46, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPassword, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwProxy, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPOIP, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtgwPOPort, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(22, 30)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(344, 135)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'Label28
        '
        Me.Label28.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(3, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(43, 27)
        Me.Label28.TabIndex = 0
        Me.Label28.Text = "User ID"
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtgwUser
        '
        Me.txtgwUser.Location = New System.Drawing.Point(90, 3)
        Me.txtgwUser.Name = "txtgwUser"
        Me.txtgwUser.Size = New System.Drawing.Size(219, 21)
        Me.txtgwUser.TabIndex = 1
        '
        'Label29
        '
        Me.Label29.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(3, 27)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(53, 27)
        Me.Label29.TabIndex = 0
        Me.Label29.Text = "Password"
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label41
        '
        Me.Label41.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label41.AutoSize = True
        Me.Label41.Location = New System.Drawing.Point(3, 54)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(49, 27)
        Me.Label41.TabIndex = 0
        Me.Label41.Text = "Proxy ID"
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label45
        '
        Me.Label45.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label45.AutoSize = True
        Me.Label45.Location = New System.Drawing.Point(3, 81)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(81, 27)
        Me.Label45.TabIndex = 0
        Me.Label45.Text = "Server Address"
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label46
        '
        Me.Label46.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label46.AutoSize = True
        Me.Label46.Location = New System.Drawing.Point(3, 108)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(62, 27)
        Me.Label46.TabIndex = 0
        Me.Label46.Text = "Server Port"
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtgwPassword
        '
        Me.txtgwPassword.Location = New System.Drawing.Point(90, 30)
        Me.txtgwPassword.Name = "txtgwPassword"
        Me.txtgwPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtgwPassword.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPassword.TabIndex = 1
        '
        'txtgwProxy
        '
        Me.txtgwProxy.Location = New System.Drawing.Point(90, 57)
        Me.txtgwProxy.Name = "txtgwProxy"
        Me.txtgwProxy.Size = New System.Drawing.Size(219, 21)
        Me.txtgwProxy.TabIndex = 1
        '
        'txtgwPOIP
        '
        Me.txtgwPOIP.Location = New System.Drawing.Point(90, 84)
        Me.txtgwPOIP.Name = "txtgwPOIP"
        Me.txtgwPOIP.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPOIP.TabIndex = 1
        '
        'txtgwPOPort
        '
        Me.txtgwPOPort.Location = New System.Drawing.Point(90, 111)
        Me.txtgwPOPort.Name = "txtgwPOPort"
        Me.txtgwPOPort.Size = New System.Drawing.Size(219, 21)
        Me.txtgwPOPort.TabIndex = 1
        '
        'grpCRDMail
        '
        Me.grpCRDMail.Controls.Add(Me.cmdActivate)
        Me.grpCRDMail.Controls.Add(Me.Label23)
        Me.grpCRDMail.Controls.Add(Me.txtCRDAddress)
        Me.grpCRDMail.Controls.Add(Me.txtCRDSender)
        Me.grpCRDMail.Controls.Add(Me.Label24)
        Me.grpCRDMail.Location = New System.Drawing.Point(8, 40)
        Me.grpCRDMail.Name = "grpCRDMail"
        Me.grpCRDMail.Size = New System.Drawing.Size(424, 216)
        Me.grpCRDMail.TabIndex = 4
        Me.grpCRDMail.TabStop = False
        Me.grpCRDMail.Text = "SQL-RD Mail"
        Me.grpCRDMail.Visible = False
        '
        'cmdActivate
        '
        Me.cmdActivate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdActivate.Location = New System.Drawing.Point(264, 88)
        Me.cmdActivate.Name = "cmdActivate"
        Me.cmdActivate.Size = New System.Drawing.Size(104, 23)
        Me.cmdActivate.TabIndex = 3
        Me.cmdActivate.Text = "Activate Account"
        '
        'Label23
        '
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(8, 24)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(100, 16)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Sender Name"
        '
        'txtCRDAddress
        '
        Me.txtCRDAddress.Location = New System.Drawing.Point(136, 54)
        Me.txtCRDAddress.Name = "txtCRDAddress"
        Me.txtCRDAddress.Size = New System.Drawing.Size(232, 21)
        Me.txtCRDAddress.TabIndex = 1
        '
        'txtCRDSender
        '
        Me.txtCRDSender.Location = New System.Drawing.Point(136, 24)
        Me.txtCRDSender.Name = "txtCRDSender"
        Me.txtCRDSender.Size = New System.Drawing.Size(232, 21)
        Me.txtCRDSender.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(8, 56)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(100, 16)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "Sender Address"
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.cmbEncoding)
        Me.GroupBox11.Controls.Add(Me.Label39)
        Me.GroupBox11.Location = New System.Drawing.Point(8, 296)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(416, 64)
        Me.GroupBox11.TabIndex = 5
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "Text Character Encoding"
        '
        'cmbEncoding
        '
        Me.cmbEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbEncoding.ItemHeight = 13
        Me.cmbEncoding.Items.AddRange(New Object() {"US-ASCII", "ISO-2022-JP"})
        Me.cmbEncoding.Location = New System.Drawing.Point(136, 32)
        Me.cmbEncoding.Name = "cmbEncoding"
        Me.cmbEncoding.Size = New System.Drawing.Size(224, 21)
        Me.cmbEncoding.TabIndex = 0
        '
        'Label39
        '
        Me.Label39.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label39.Location = New System.Drawing.Point(8, 32)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(100, 16)
        Me.Label39.TabIndex = 2
        Me.Label39.Text = "Encoding Type"
        '
        'cmbMailType
        '
        Me.cmbMailType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailType.ForeColor = System.Drawing.Color.Blue
        Me.cmbMailType.ItemHeight = 13
        Me.cmbMailType.Items.AddRange(New Object() {"MAPI", "SMTP", "SQLRDMAIL", "GROUPWISE", "NONE"})
        Me.cmbMailType.Location = New System.Drawing.Point(144, 8)
        Me.cmbMailType.Name = "cmbMailType"
        Me.cmbMailType.Size = New System.Drawing.Size(232, 21)
        Me.cmbMailType.TabIndex = 0
        Me.cmbMailType.Tag = "unsorted"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(8, 10)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(100, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Mail Type"
        '
        'chkEmailRetry
        '
        Me.chkEmailRetry.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkEmailRetry.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEmailRetry.Location = New System.Drawing.Point(8, 264)
        Me.chkEmailRetry.Name = "chkEmailRetry"
        Me.chkEmailRetry.Size = New System.Drawing.Size(152, 24)
        Me.chkEmailRetry.TabIndex = 1
        Me.chkEmailRetry.Text = "Try re-sending failed emails"
        '
        'cmdMailTest
        '
        Me.cmdMailTest.Image = CType(resources.GetObject("cmdMailTest.Image"), System.Drawing.Image)
        Me.cmdMailTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdMailTest.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMailTest.Location = New System.Drawing.Point(328, 264)
        Me.cmdMailTest.Name = "cmdMailTest"
        Me.cmdMailTest.Size = New System.Drawing.Size(104, 23)
        Me.cmdMailTest.TabIndex = 3
        Me.cmdMailTest.Text = "Test Settings"
        Me.cmdMailTest.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdClearSettings
        '
        Me.cmdClearSettings.Image = CType(resources.GetObject("cmdClearSettings.Image"), System.Drawing.Image)
        Me.cmdClearSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdClearSettings.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdClearSettings.Location = New System.Drawing.Point(208, 264)
        Me.cmdClearSettings.Name = "cmdClearSettings"
        Me.cmdClearSettings.Size = New System.Drawing.Size(104, 23)
        Me.cmdClearSettings.TabIndex = 2
        Me.cmdClearSettings.Text = "Clear Settings"
        Me.cmdClearSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.Label51)
        Me.TabPage5.Controls.Add(Me.Label50)
        Me.TabPage5.Controls.Add(Me.txtSMSInit)
        Me.TabPage5.Controls.Add(Me.chkUnicode)
        Me.TabPage5.Controls.Add(Me.grpSMSC)
        Me.TabPage5.Controls.Add(Me.optUseGSM)
        Me.TabPage5.Controls.Add(Me.cmbSMSDevice)
        Me.TabPage5.Controls.Add(Me.Label33)
        Me.TabPage5.Controls.Add(Me.cmbSMSFormat)
        Me.TabPage5.Controls.Add(Me.Label34)
        Me.TabPage5.Controls.Add(Me.cmbSMSSpeed)
        Me.TabPage5.Controls.Add(Me.Label35)
        Me.TabPage5.Controls.Add(Me.optUseSMSC)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(472, 366)
        Me.TabPage5.TabIndex = 1
        Me.TabPage5.Text = "SMS Text"
        '
        'Label51
        '
        Me.Label51.Font = New System.Drawing.Font("Tahoma", 8.25!, CType((System.Drawing.FontStyle.Italic Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle))
        Me.Label51.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label51.Location = New System.Drawing.Point(395, 47)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(41, 16)
        Me.SuperTooltip1.SetSuperTooltip(Me.Label51, New DevComponents.DotNetBar.SuperTooltipInfo("", "", resources.GetString("Label51.SuperTooltip"), Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.Label51.TabIndex = 9
        Me.Label51.Text = "Help"
        '
        'Label50
        '
        Me.Label50.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label50.Location = New System.Drawing.Point(87, 48)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(101, 14)
        Me.Label50.TabIndex = 8
        Me.Label50.Text = "Initialization String"
        '
        'txtSMSInit
        '
        Me.txtSMSInit.Location = New System.Drawing.Point(188, 44)
        Me.txtSMSInit.Name = "txtSMSInit"
        Me.txtSMSInit.Size = New System.Drawing.Size(206, 21)
        Me.txtSMSInit.TabIndex = 7
        Me.txtSMSInit.Text = "AT+MS=V22B"
        '
        'chkUnicode
        '
        Me.chkUnicode.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkUnicode.Location = New System.Drawing.Point(88, 312)
        Me.chkUnicode.Name = "chkUnicode"
        Me.chkUnicode.Size = New System.Drawing.Size(360, 32)
        Me.chkUnicode.TabIndex = 6
        Me.chkUnicode.Text = "Use Unicode (to support Arabic, Chinese and other wide style character sets"
        '
        'grpSMSC
        '
        Me.grpSMSC.Controls.Add(Me.txtSMSNumber)
        Me.grpSMSC.Controls.Add(Me.Label36)
        Me.grpSMSC.Controls.Add(Me.cmbSMSProtocol)
        Me.grpSMSC.Controls.Add(Me.Label37)
        Me.grpSMSC.Controls.Add(Me.Label38)
        Me.grpSMSC.Controls.Add(Me.txtSMSPassword)
        Me.grpSMSC.Controls.Add(Me.Label40)
        Me.grpSMSC.Controls.Add(Me.txtSMSSender)
        Me.grpSMSC.Location = New System.Drawing.Point(88, 160)
        Me.grpSMSC.Name = "grpSMSC"
        Me.grpSMSC.Size = New System.Drawing.Size(304, 144)
        Me.grpSMSC.TabIndex = 5
        Me.grpSMSC.TabStop = False
        Me.grpSMSC.Text = "SMSC Details"
        '
        'txtSMSNumber
        '
        Me.txtSMSNumber.Location = New System.Drawing.Point(120, 24)
        Me.txtSMSNumber.Name = "txtSMSNumber"
        Me.txtSMSNumber.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSNumber.TabIndex = 4
        '
        'Label36
        '
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(8, 24)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(88, 16)
        Me.Label36.TabIndex = 0
        Me.Label36.Text = "SMSC Number "
        '
        'cmbSMSProtocol
        '
        Me.cmbSMSProtocol.ItemHeight = 13
        Me.cmbSMSProtocol.Items.AddRange(New Object() {"TAP", "UCP"})
        Me.cmbSMSProtocol.Location = New System.Drawing.Point(120, 56)
        Me.cmbSMSProtocol.Name = "cmbSMSProtocol"
        Me.cmbSMSProtocol.Size = New System.Drawing.Size(160, 21)
        Me.cmbSMSProtocol.TabIndex = 1
        '
        'Label37
        '
        Me.Label37.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label37.Location = New System.Drawing.Point(8, 56)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(96, 16)
        Me.Label37.TabIndex = 0
        Me.Label37.Text = "Provider Protocol"
        '
        'Label38
        '
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(8, 80)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(104, 16)
        Me.Label38.TabIndex = 0
        Me.Label38.Text = "Password (optional)"
        '
        'txtSMSPassword
        '
        Me.txtSMSPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtSMSPassword.Location = New System.Drawing.Point(120, 80)
        Me.txtSMSPassword.Name = "txtSMSPassword"
        Me.txtSMSPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtSMSPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSPassword.TabIndex = 4
        '
        'Label40
        '
        Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label40.Location = New System.Drawing.Point(8, 117)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(104, 16)
        Me.Label40.TabIndex = 0
        Me.Label40.Text = "Sender Number"
        '
        'txtSMSSender
        '
        Me.txtSMSSender.Location = New System.Drawing.Point(120, 112)
        Me.txtSMSSender.Name = "txtSMSSender"
        Me.txtSMSSender.Size = New System.Drawing.Size(160, 21)
        Me.txtSMSSender.TabIndex = 4
        '
        'optUseGSM
        '
        Me.optUseGSM.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optUseGSM.Location = New System.Drawing.Point(88, 112)
        Me.optUseGSM.Name = "optUseGSM"
        Me.optUseGSM.Size = New System.Drawing.Size(312, 24)
        Me.optUseGSM.TabIndex = 2
        Me.optUseGSM.Text = "Send through GSM phone or SMS device"
        '
        'cmbSMSDevice
        '
        Me.cmbSMSDevice.ItemHeight = 13
        Me.cmbSMSDevice.Location = New System.Drawing.Point(88, 16)
        Me.cmbSMSDevice.Name = "cmbSMSDevice"
        Me.cmbSMSDevice.Size = New System.Drawing.Size(306, 21)
        Me.cmbSMSDevice.TabIndex = 1
        '
        'Label33
        '
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(45, 19)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(41, 16)
        Me.Label33.TabIndex = 0
        Me.Label33.Text = "Device"
        '
        'cmbSMSFormat
        '
        Me.cmbSMSFormat.ItemHeight = 13
        Me.cmbSMSFormat.Items.AddRange(New Object() {"Default", "8,n,1", "7,e,1"})
        Me.cmbSMSFormat.Location = New System.Drawing.Point(88, 80)
        Me.cmbSMSFormat.Name = "cmbSMSFormat"
        Me.cmbSMSFormat.Size = New System.Drawing.Size(96, 21)
        Me.cmbSMSFormat.TabIndex = 1
        '
        'Label34
        '
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(14, 83)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(72, 16)
        Me.Label34.TabIndex = 0
        Me.Label34.Text = "Data Format"
        '
        'cmbSMSSpeed
        '
        Me.cmbSMSSpeed.ItemHeight = 13
        Me.cmbSMSSpeed.Items.AddRange(New Object() {"Default", "1200", "2400", "9600", "19200", "38400", "57600", "115200"})
        Me.cmbSMSSpeed.Location = New System.Drawing.Point(240, 80)
        Me.cmbSMSSpeed.Name = "cmbSMSSpeed"
        Me.cmbSMSSpeed.Size = New System.Drawing.Size(104, 21)
        Me.cmbSMSSpeed.TabIndex = 1
        '
        'Label35
        '
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(197, 83)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(40, 16)
        Me.Label35.TabIndex = 0
        Me.Label35.Text = "Speed"
        '
        'optUseSMSC
        '
        Me.optUseSMSC.Checked = True
        Me.optUseSMSC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optUseSMSC.Location = New System.Drawing.Point(88, 136)
        Me.optUseSMSC.Name = "optUseSMSC"
        Me.optUseSMSC.Size = New System.Drawing.Size(312, 24)
        Me.optUseSMSC.TabIndex = 2
        Me.optUseSMSC.TabStop = True
        Me.optUseSMSC.Text = "Send through service provider"
        '
        'tbMessaging
        '
        Me.tbMessaging.AttachedControl = Me.TabControlPanel2
        Me.tbMessaging.Image = CType(resources.GetObject("tbMessaging.Image"), System.Drawing.Image)
        Me.tbMessaging.Name = "tbMessaging"
        Me.tbMessaging.Text = "Messaging"
        '
        'TabControlPanel10
        '
        Me.TabControlPanel10.Controls.Add(Me.UcTasks)
        Me.TabControlPanel10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel10.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel10.Name = "TabControlPanel10"
        Me.TabControlPanel10.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel10.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel10.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel10.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel10.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel10.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel10.TabIndex = 10
        Me.TabControlPanel10.TabItem = Me.TabItem1
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(1, 1)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(533, 428)
        Me.UcTasks.TabIndex = 0
        '
        'TabItem1
        '
        Me.TabItem1.AttachedControl = Me.TabControlPanel10
        Me.TabItem1.Image = CType(resources.GetObject("TabItem1.Image"), System.Drawing.Image)
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "Default Tasks"
        '
        'TabControlPanel7
        '
        Me.TabControlPanel7.Controls.Add(Me.GroupBox6)
        Me.TabControlPanel7.Controls.Add(Me.chkCluster)
        Me.TabControlPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel7.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel7.Name = "TabControlPanel7"
        Me.TabControlPanel7.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel7.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel7.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel7.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel7.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel7.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel7.TabIndex = 7
        Me.TabControlPanel7.TabItem = Me.tbCluster
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox6.Controls.Add(Me.grpSlaves)
        Me.GroupBox6.Controls.Add(Me.grpMaster)
        Me.GroupBox6.Controls.Add(Me.cmbClusterType)
        Me.GroupBox6.Controls.Add(Me.Label26)
        Me.GroupBox6.Enabled = False
        Me.GroupBox6.Location = New System.Drawing.Point(5, 39)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(488, 360)
        Me.GroupBox6.TabIndex = 2
        Me.GroupBox6.TabStop = False
        '
        'grpSlaves
        '
        Me.grpSlaves.Controls.Add(Me.txtPort)
        Me.grpSlaves.Controls.Add(Me.Label30)
        Me.grpSlaves.Controls.Add(Me.cmdAddReport)
        Me.grpSlaves.Controls.Add(Me.cmdRemoveReport)
        Me.grpSlaves.Controls.Add(Me.lsvSlaves)
        Me.grpSlaves.Controls.Add(Me.Label1)
        Me.grpSlaves.Controls.Add(Me.Label27)
        Me.grpSlaves.Controls.Add(Me.txtSlaveTimeout)
        Me.grpSlaves.Location = New System.Drawing.Point(8, 64)
        Me.grpSlaves.Name = "grpSlaves"
        Me.grpSlaves.Size = New System.Drawing.Size(472, 288)
        Me.grpSlaves.TabIndex = 3
        Me.grpSlaves.TabStop = False
        Me.grpSlaves.Text = "Slaves"
        Me.grpSlaves.Visible = False
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(176, 21)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(55, 21)
        Me.txtPort.TabIndex = 0
        Me.txtPort.Text = "2908"
        '
        'Label30
        '
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(238, 52)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(96, 16)
        Me.Label30.TabIndex = 13
        Me.Label30.Text = "Seconds"
        '
        'cmdAddReport
        '
        Me.cmdAddReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddReport.Image = CType(resources.GetObject("cmdAddReport.Image"), System.Drawing.Image)
        Me.cmdAddReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(392, 88)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdAddReport.TabIndex = 2
        Me.cmdAddReport.Text = "&Add"
        Me.cmdAddReport.UseVisualStyleBackColor = False
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveReport.Image = CType(resources.GetObject("cmdRemoveReport.Image"), System.Drawing.Image)
        Me.cmdRemoveReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(392, 120)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(75, 24)
        Me.cmdRemoveReport.TabIndex = 3
        Me.cmdRemoveReport.Text = "&Remove"
        Me.cmdRemoveReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveReport.UseVisualStyleBackColor = False
        '
        'lsvSlaves
        '
        Me.lsvSlaves.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader6})
        Me.lsvSlaves.FullRowSelect = True
        Me.lsvSlaves.Location = New System.Drawing.Point(8, 88)
        Me.lsvSlaves.Name = "lsvSlaves"
        Me.lsvSlaves.Size = New System.Drawing.Size(376, 192)
        Me.lsvSlaves.TabIndex = 2
        Me.lsvSlaves.UseCompatibleStateImageBehavior = False
        Me.lsvSlaves.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Slave"
        Me.ColumnHeader2.Width = 100
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Path"
        Me.ColumnHeader3.Width = 195
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "IP Address"
        Me.ColumnHeader6.Width = 75
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Slave Timeout"
        '
        'Label27
        '
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(8, 24)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(96, 16)
        Me.Label27.TabIndex = 1
        Me.Label27.Text = "TCP/IP Port"
        '
        'txtSlaveTimeout
        '
        Me.txtSlaveTimeout.Location = New System.Drawing.Point(176, 50)
        Me.txtSlaveTimeout.Maximum = New Decimal(New Integer() {86400, 0, 0, 0})
        Me.txtSlaveTimeout.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtSlaveTimeout.Name = "txtSlaveTimeout"
        Me.txtSlaveTimeout.Size = New System.Drawing.Size(56, 21)
        Me.txtSlaveTimeout.TabIndex = 1
        Me.txtSlaveTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtSlaveTimeout.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'grpMaster
        '
        Me.grpMaster.Controls.Add(Me.cmdMaster)
        Me.grpMaster.Controls.Add(Me.txtMaster)
        Me.grpMaster.Location = New System.Drawing.Point(8, 64)
        Me.grpMaster.Name = "grpMaster"
        Me.grpMaster.Size = New System.Drawing.Size(472, 72)
        Me.grpMaster.TabIndex = 4
        Me.grpMaster.TabStop = False
        Me.grpMaster.Text = "Master"
        Me.grpMaster.Visible = False
        '
        'cmdMaster
        '
        Me.cmdMaster.BackColor = System.Drawing.SystemColors.Control
        Me.cmdMaster.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdMaster.Image = CType(resources.GetObject("cmdMaster.Image"), System.Drawing.Image)
        Me.cmdMaster.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdMaster.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMaster.Location = New System.Drawing.Point(408, 32)
        Me.cmdMaster.Name = "cmdMaster"
        Me.cmdMaster.Size = New System.Drawing.Size(56, 21)
        Me.cmdMaster.TabIndex = 3
        Me.cmdMaster.Text = "..."
        Me.cmdMaster.UseVisualStyleBackColor = False
        '
        'txtMaster
        '
        Me.txtMaster.Location = New System.Drawing.Point(8, 32)
        Me.txtMaster.Name = "txtMaster"
        Me.txtMaster.Size = New System.Drawing.Size(376, 21)
        Me.txtMaster.TabIndex = 0
        '
        'cmbClusterType
        '
        Me.cmbClusterType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbClusterType.ItemHeight = 13
        Me.cmbClusterType.Items.AddRange(New Object() {"Master", "Slave"})
        Me.cmbClusterType.Location = New System.Drawing.Point(8, 32)
        Me.cmbClusterType.Name = "cmbClusterType"
        Me.cmbClusterType.Size = New System.Drawing.Size(248, 21)
        Me.cmbClusterType.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label26.Location = New System.Drawing.Point(8, 16)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(256, 16)
        Me.Label26.TabIndex = 1
        Me.Label26.Text = "In this cluster, this machine will have the role of"
        '
        'chkCluster
        '
        Me.chkCluster.BackColor = System.Drawing.Color.Transparent
        Me.chkCluster.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCluster.Location = New System.Drawing.Point(4, 7)
        Me.chkCluster.Name = "chkCluster"
        Me.chkCluster.Size = New System.Drawing.Size(440, 24)
        Me.chkCluster.TabIndex = 0
        Me.chkCluster.Text = "Enable SQL-RD Clustering on this machine"
        Me.chkCluster.UseVisualStyleBackColor = False
        '
        'tbCluster
        '
        Me.tbCluster.AttachedControl = Me.TabControlPanel7
        Me.tbCluster.Image = CType(resources.GetObject("tbCluster.Image"), System.Drawing.Image)
        Me.tbCluster.Name = "tbCluster"
        Me.tbCluster.Text = "Clustering"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.TabControl2)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbHouseKeeping
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage6)
        Me.TabControl2.Controls.Add(Me.TabPage8)
        Me.TabControl2.Location = New System.Drawing.Point(6, 7)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(480, 392)
        Me.TabControl2.TabIndex = 10
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.lsvComponents)
        Me.TabPage6.Controls.Add(Me.cmdRegister)
        Me.TabPage6.Controls.Add(Me.GroupBox9)
        Me.TabPage6.Controls.Add(Me.GroupBox8)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(472, 366)
        Me.TabPage6.TabIndex = 0
        Me.TabPage6.Text = "Components"
        '
        'lsvComponents
        '
        Me.lsvComponents.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvComponents.ForeColor = System.Drawing.Color.Blue
        Me.lsvComponents.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lsvComponents.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2, ListViewItem3, ListViewItem4})
        Me.lsvComponents.Location = New System.Drawing.Point(8, 8)
        Me.lsvComponents.Name = "lsvComponents"
        Me.lsvComponents.Size = New System.Drawing.Size(456, 160)
        Me.lsvComponents.TabIndex = 0
        Me.lsvComponents.UseCompatibleStateImageBehavior = False
        Me.lsvComponents.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 250
        '
        'cmdRegister
        '
        Me.cmdRegister.Image = CType(resources.GetObject("cmdRegister.Image"), System.Drawing.Image)
        Me.cmdRegister.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRegister.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRegister.Location = New System.Drawing.Point(336, 176)
        Me.cmdRegister.Name = "cmdRegister"
        Me.cmdRegister.Size = New System.Drawing.Size(128, 23)
        Me.cmdRegister.TabIndex = 1
        Me.cmdRegister.Text = "Register Component"
        Me.cmdRegister.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.chkArchive)
        Me.GroupBox9.Controls.Add(Me.Label22)
        Me.GroupBox9.Controls.Add(Me.txtArchive)
        Me.GroupBox9.Location = New System.Drawing.Point(8, 208)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(456, 48)
        Me.GroupBox9.TabIndex = 13
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Archiving"
        '
        'chkArchive
        '
        Me.chkArchive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkArchive.Location = New System.Drawing.Point(8, 16)
        Me.chkArchive.Name = "chkArchive"
        Me.chkArchive.Size = New System.Drawing.Size(264, 24)
        Me.chkArchive.TabIndex = 7
        Me.chkArchive.Text = "Archive schedules that have been disabled for"
        '
        'Label22
        '
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(344, 19)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(40, 14)
        Me.Label22.TabIndex = 6
        Me.Label22.Text = "Days"
        '
        'txtArchive
        '
        Me.txtArchive.Enabled = False
        Me.txtArchive.Location = New System.Drawing.Point(272, 16)
        Me.txtArchive.Maximum = New Decimal(New Integer() {366, 0, 0, 0})
        Me.txtArchive.Name = "txtArchive"
        Me.txtArchive.Size = New System.Drawing.Size(48, 21)
        Me.txtArchive.TabIndex = 8
        Me.txtArchive.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.chkAutoCompact)
        Me.GroupBox8.Controls.Add(Me.dtAutoCompact)
        Me.GroupBox8.Controls.Add(Me.Label32)
        Me.GroupBox8.Location = New System.Drawing.Point(8, 264)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(456, 80)
        Me.GroupBox8.TabIndex = 12
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Auto-Compact"
        '
        'chkAutoCompact
        '
        Me.chkAutoCompact.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAutoCompact.Location = New System.Drawing.Point(8, 16)
        Me.chkAutoCompact.Name = "chkAutoCompact"
        Me.chkAutoCompact.Size = New System.Drawing.Size(232, 24)
        Me.chkAutoCompact.TabIndex = 9
        Me.chkAutoCompact.Text = "Auto-compact and repair system files at :"
        '
        'dtAutoCompact
        '
        Me.dtAutoCompact.Enabled = False
        Me.dtAutoCompact.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtAutoCompact.Location = New System.Drawing.Point(248, 16)
        Me.dtAutoCompact.Name = "dtAutoCompact"
        Me.dtAutoCompact.ShowUpDown = True
        Me.dtAutoCompact.Size = New System.Drawing.Size(72, 21)
        Me.dtAutoCompact.TabIndex = 10
        Me.dtAutoCompact.Value = New Date(2005, 1, 7, 18, 30, 0, 0)
        '
        'Label32
        '
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(8, 40)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(424, 32)
        Me.Label32.TabIndex = 11
        Me.Label32.Text = "CAUTION: Auto-compact reqires that the SQL-RD editor is closed. The system will c" & _
            "lose itself down at this time."
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.GroupBox4)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Size = New System.Drawing.Size(472, 366)
        Me.TabPage8.TabIndex = 2
        Me.TabPage8.Text = "Folder Housekeeping"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cmdAdd)
        Me.GroupBox4.Controls.Add(Me.lsvPaths)
        Me.GroupBox4.Controls.Add(Me.cmdDelete)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 5)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(456, 200)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = ""
        Me.GroupBox4.Text = "Folder  Housekeeping"
        '
        'cmdAdd
        '
        Me.cmdAdd.Image = CType(resources.GetObject("cmdAdd.Image"), System.Drawing.Image)
        Me.cmdAdd.Location = New System.Drawing.Point(400, 16)
        Me.cmdAdd.Name = "cmdAdd"
        Me.cmdAdd.Size = New System.Drawing.Size(48, 23)
        Me.cmdAdd.TabIndex = 1
        '
        'lsvPaths
        '
        Me.lsvPaths.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader4, Me.ColumnHeader5})
        Me.lsvPaths.FullRowSelect = True
        Me.lsvPaths.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvPaths.HideSelection = False
        Me.lsvPaths.Location = New System.Drawing.Point(8, 16)
        Me.lsvPaths.Name = "lsvPaths"
        Me.lsvPaths.Size = New System.Drawing.Size(376, 176)
        Me.lsvPaths.TabIndex = 0
        Me.lsvPaths.UseCompatibleStateImageBehavior = False
        Me.lsvPaths.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Path"
        Me.ColumnHeader4.Width = 228
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Delete items older than..."
        Me.ColumnHeader5.Width = 143
        '
        'cmdDelete
        '
        Me.cmdDelete.Image = CType(resources.GetObject("cmdDelete.Image"), System.Drawing.Image)
        Me.cmdDelete.Location = New System.Drawing.Point(400, 48)
        Me.cmdDelete.Name = "cmdDelete"
        Me.cmdDelete.Size = New System.Drawing.Size(48, 23)
        Me.cmdDelete.TabIndex = 1
        '
        'tbHouseKeeping
        '
        Me.tbHouseKeeping.AttachedControl = Me.TabControlPanel4
        Me.tbHouseKeeping.Image = CType(resources.GetObject("tbHouseKeeping.Image"), System.Drawing.Image)
        Me.tbHouseKeeping.Name = "tbHouseKeeping"
        Me.tbHouseKeeping.Text = "House Keeping"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.UcDest)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(141, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(535, 430)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.SystemColors.Control
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.White
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.tbDestinations
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(8, 14)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(488, 392)
        Me.UcDest.TabIndex = 0
        '
        'tbDestinations
        '
        Me.tbDestinations.AttachedControl = Me.TabControlPanel6
        Me.tbDestinations.Image = CType(resources.GetObject("tbDestinations.Image"), System.Drawing.Image)
        Me.tbDestinations.Name = "tbDestinations"
        Me.tbDestinations.Text = "Default Destinations"
        '
        'chkDisableCRParsing
        '
        Me.chkDisableCRParsing.AutoSize = True
        Me.chkDisableCRParsing.Location = New System.Drawing.Point(195, 436)
        Me.chkDisableCRParsing.Name = "chkDisableCRParsing"
        Me.chkDisableCRParsing.Size = New System.Drawing.Size(204, 17)
        Me.chkDisableCRParsing.TabIndex = 2
        Me.chkDisableCRParsing.Text = "Disable Crystal Reports fields parsing"
        Me.ToolTip1.SetToolTip(Me.chkDisableCRParsing, "Disables the ability to insert Crystal Reports fields into emails etc. Having thi" & _
                "s option checked can speed up report exporting.")
        Me.chkDisableCRParsing.UseVisualStyleBackColor = True
        Me.chkDisableCRParsing.Visible = False
        '
        'optErrorLog
        '
        Me.optErrorLog.Checked = True
        Me.optErrorLog.CheckState = System.Windows.Forms.CheckState.Checked
        Me.optErrorLog.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optErrorLog.Location = New System.Drawing.Point(70, 436)
        Me.optErrorLog.Name = "optErrorLog"
        Me.optErrorLog.Size = New System.Drawing.Size(160, 24)
        Me.optErrorLog.TabIndex = 0
        Me.optErrorLog.Text = "Write to error log file"
        Me.optErrorLog.Visible = False
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        Me.SuperTooltip1.TooltipDuration = 30
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(310, 88)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {80000, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(58, 20)
        Me.NumericUpDown1.TabIndex = 8
        Me.NumericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.NumericUpDown1.Value = New Decimal(New Integer() {25, 0, 0, 0})
        '
        'frmOptions
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(676, 463)
        Me.Controls.Add(Me.chkDisableCRParsing)
        Me.Controls.Add(Me.tabOptions)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.optErrorLog)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Options"
        CType(Me.tabOptions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabOptions.ResumeLayout(False)
        Me.TabControlPanel1.ResumeLayout(False)
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        CType(Me.txtRefresh, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TabControlPanel5.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.TabControlPanel9.ResumeLayout(False)
        Me.TabControlPanel9.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel3.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.cmbPollIntEB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbPollInt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.txtDelayBy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtThreadCount, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpServices.ResumeLayout(False)
        Me.grpNTService.ResumeLayout(False)
        Me.grpNTService.PerformLayout()
        Me.TabControlPanel8.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.TabControlPanel2.ResumeLayout(False)
        Me.tabMessaging.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.grpSMTP.ResumeLayout(False)
        Me.grpSMTP.PerformLayout()
        CType(Me.txtSMTPPort, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbSMTPTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpMAPI.ResumeLayout(False)
        Me.grpMAPI.PerformLayout()
        Me.grpGroupwise.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.grpCRDMail.ResumeLayout(False)
        Me.grpCRDMail.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.grpSMSC.ResumeLayout(False)
        Me.grpSMSC.PerformLayout()
        Me.TabControlPanel10.ResumeLayout(False)
        Me.TabControlPanel7.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.grpSlaves.ResumeLayout(False)
        Me.grpSlaves.PerformLayout()
        CType(Me.txtSlaveTimeout, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpMaster.ResumeLayout(False)
        Me.grpMaster.PerformLayout()
        Me.TabControlPanel4.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.GroupBox9.ResumeLayout(False)
        CType(Me.txtArchive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox8.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Private Sub frmOptions_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        On Error Resume Next
        Dim I As Integer = 0
        Dim oSec As New clsMarsSecurity
        FormatForWinXP(Me)


        UcDest.DefaultDestinations = True

        With UcTasks
            .ScheduleID = 2777
            .oAuto = False
            .ShowAfterType = False
            .LoadTasks()
            .m_eventBased = False
            .DefaultTaskToolStripMenuItem.Visible = False
            .m_defaultTaks = True
        End With

        If gConType <> "DAT" Then
            chkCompact.Enabled = False
            GroupBox8.Enabled = False
        End If

        cmbPollInt.Text = oUI.ReadRegistry("Poll", 30)
        cmbSMTPTimeout.Text = oUI.ReadRegistry("SMTPTimeout", 30)
        chkThreading.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDThreading", 0)))
        chkCompact.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotAutoCompact", 0)))
        chkOutofProcess.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("RunOutOfProcess", 1)))
        chkDTRefresh.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DTRefresh", 0)))
        txtRefresh.Value = oUI.ReadRegistry("DTRefreshInterval", "180")

        chkUNC.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotUNC", 1)))
        Me.chkDisableCRParsing.Checked = False 'Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DoNotParseReportFields", 0)))

        Me.chkShowHiddenParameters.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("ShowHiddenParameters", 0)))

        Dim sErrorHandle As String = oUI.ReadRegistry("ErrorHandle", "")

        If sErrorHandle.Length > 0 Then
            Select Case sErrorHandle
                Case "Log"
                    optErrorLog.Checked = True
                Case "Email"
                    optErrorMail.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case "Both"
                    optErrorMail.Checked = True
                    optErrorLog.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case "SMS"
                    optErrorSMS.Checked = True
                    txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
                Case Else
                    optErrorLog.Checked = True
            End Select

            txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")

            'oUI._DeleteRegistry(oReg, sKey, "ErrorHandle")
        Else
            optErrorLog.Checked = oUI.ReadRegistry("ErrorByLog", 1)
            optErrorMail.Checked = oUI.ReadRegistry("ErrorByMail", 0)
            optErrorSMS.Checked = oUI.ReadRegistry("ErrorBySMS", 0)

            txtErrorAlertWho.Text = oUI.ReadRegistry("AlertWho", "")
            txtAlertSMS.Text = oUI.ReadRegistry("SMSAlertWho", "")
        End If


        'NT Scheduler values

        'try the new options
        Select Case oUI.ReadRegistry("MailType", "NONE")
            Case "NONE"
                cmbMailType.Text = "NONE"
                grpSMTP.Visible = False
                grpMAPI.Visible = False
            Case "MAPI"
                cmbMailType.Text = "MAPI"
                grpMAPI.Visible = True
                grpMAPI.BringToFront()
            Case "SMTP"
                cmbMailType.Text = "SMTP"
                grpSMTP.Visible = True
                grpSMTP.BringToFront()
            Case "GROUPWISE"
                cmbMailType.Text = "GROUPWISE"
                grpGroupwise.Visible = True
                grpGroupwise.BringToFront()
            Case "SQLRDMAIL"
                cmbMailType.Text = "SQLRDMAIL"

                Dim isActivated As Boolean = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("SQL-RDMailActivation", "0")))

                If isActivated = True Then
                    txtCRDAddress.ReadOnly = False
                    txtCRDSender.ReadOnly = False
                Else
                    txtCRDAddress.ReadOnly = True
                    txtCRDSender.ReadOnly = True
                End If
        End Select

        chkEmailRetry.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("EmailRetry", 0)))

        Select Case oUI.ReadRegistry("SQL-RDService", "NONE")
            Case "NONE"
                optNoScheduling.Checked = True
                GroupBox16.Enabled = False
                GroupBox2.Enabled = False
                chkCheckService.Checked = False
            Case "WindowsApp"
                optBackgroundApp.Checked = True
                chkCheckService.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("CheckScheduler", 0)))
            Case "WindowsNT"
                optNTService.Checked = True
                chkCheckService.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("CheckScheduler", 0)))

                With oUI
                    txtWinUser.Text = .ReadRegistry("NTLoginName", "")
                    txtWinPassword.Text = .ReadRegistry("NTLoginPassword", "", True)
                    txtWinDomain.Text = .ReadRegistry("NTDomain", "")
                End With
        End Select

        Me.m_scheduleStatus = clsServiceController.itemGlobal.m_serviceStatus

        chkUniversal.Checked = True
        cmbPriority.SelectedIndex = cmbPriority.Items.IndexOf(oUI.ReadRegistry("CPU Priority", "Normal"))
        txtThreadCount.Value = oUI.ReadRegistry("ThreadCount", 4)
        chkUseRelTime.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("Use Relative Time", 0)))
        Me.chkDelayRestart.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("DelayRestart", 0)))
        Me.txtDelayBy.Value = Convert.ToInt32(oUI.ReadRegistry("DelayRestartBy", 0))

        'If gConType = "DAT" Then
        '    chkUseRelTime.Checked = False
        '    chkUseRelTime.Enabled = False
        'End If

        Me.cmbPollIntEB.Value = oUI.ReadRegistry("PollEB", cmbPollInt.Value)
        Me.cmbPriorityEB.Text = oUI.ReadRegistry("CPU Priority EB", "Normal")

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, featureCodes.e1_BasicEventsPack) = False Then
            cmbPollIntEB.Enabled = False
            cmbPriorityEB.Enabled = False
        Else
            cmbPollIntEB.Enabled = True
            cmbPriorityEB.Enabled = True
        End If

        txtCustomerNo.Text = oUI.ReadRegistry("CustNo", "")
        txtTempFolder.Text = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

        'cmbAgedValue.Text = oUI._ReadRegistry(oReg, sKey, "AgedValue", "28")
        'cmbAgedUnit.Text = oUI._ReadRegistry(oReg, sKey, "AgedUnit", "Days")

        txtDefaultEmail.Text = oUI.ReadTextFile(sAppPath & "defmsg.sqlrd")
        txtSig.Text = oUI.ReadTextFile(sAppPath & "defsig.sqlrd")
        txtDefSubject.Text = oUI.ReadRegistry("DefSubject", "")
        txtDefAttach.Text = oUI.ReadRegistry("DefAttach", "")

        cmdApply.Enabled = False
        cmdOK.Enabled = True
        cmdServiceApply.Enabled = False
        cmdServiceStart.Enabled = False

        'messaging
        txtSMTPUserID.Text = oUI.ReadRegistry("SMTPUserID", "")
        txtSMTPPassword.Text = oUI.ReadRegistry("SMTPPassword", "", True)
        txtSMTPServer.Text = oUI.ReadRegistry("SMTPServer", "")
        txtSMTPSenderAddress.Text = oUI.ReadRegistry("SMTPSenderAddress", "")
        txtSMTPSenderName.Text = oUI.ReadRegistry("SMTPSenderName", "")
        txtSig.Text = oUI.ReadTextFile(sAppPath & "defsig.sqlrd")
        cmbMAPIProfile.Text = oUI.ReadRegistry("MAPIProfile", "")
        txtMAPIPassword.Text = oUI.ReadRegistry("MAPIPassword", "", True)

        txtCRDSender.Text = oUI.ReadRegistry("SQL-RDSender", "")
        txtCRDAddress.Text = oUI.ReadRegistry("SQL-RDAddress", "")

        cmbEncoding.Text = oUI.ReadRegistry("CharSetEncoding", "US-ASCII")

        'sms options
        With oUI
            cmbSMSDevice.Text = .ReadRegistry("SMSDevice", "")
            cmbSMSFormat.Text = .ReadRegistry("SMSDataFormat", "8,n,1")
            cmbSMSSpeed.Text = .ReadRegistry("SMSSpeed", "9600")
            txtSMSInit.Text = .ReadRegistry("SMSInit", "AT+MS=V22B")

            If .ReadRegistry("SMSDeviceType", "SMSC") = "GSM" Then
                optUseGSM.Checked = True
            Else
                optUseSMSC.Checked = True
            End If

            txtSMSNumber.Text = .ReadRegistry("SMSCNumber", "")
            cmbSMSProtocol.Text = .ReadRegistry("SMSCProtocol", "TAP")
            txtSMSPassword.Text = .ReadRegistry("SMSCPassword", "", True)
            txtSMSSender.Text = .ReadRegistry("SMSSender", "")
            chkUnicode.Checked = .ReadRegistry("UseUnicode", 0)
        End With

        chkArchive.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("Archive", 0)))
        txtArchive.Value = oUI.ReadRegistry("ArchiveAfter", 0)

        With UcLogin
            .txtUserID.Text = oUI.ReadRegistry("DefDBUser", "", True)
            .txtPassword.Text = oUI.ReadRegistry("DefPassword", "", True)
        End With


        'audit trail
        chkAudit.Checked = oUI.ReadRegistry("AuditTrail", 0)

        If chkAudit.Checked = True Then
            ucDSN.cmbDSN.Text = oUI.ReadRegistry("AuditDSN", "")
            ucDSN.txtUserID.Text = oUI.ReadRegistry("AuditUserName", "")
            ucDSN.txtPassword.Text = oUI.ReadRegistry("AuditPassword", "", True)
        End If

        'for excel options

        txtReportCache.Text = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

        Me.txtSentMessages.Text = oUI.ReadRegistry("SentMessagesFolder", sAppPath & "Sent Messages\")

        'autocompact
        If gConType = "DAT" Then
            chkAutoCompact.Checked = Convert.ToBoolean(Convert.ToInt32(oUI.ReadRegistry("AutoCompact", 0)))
            dtAutoCompact.Value = oUI.ReadRegistry("CompactDue", Now.Date & " 18:30:00")
        Else
            chkAutoCompact.Checked = False
            dtAutoCompact.Value = Now.Date & " 18:30:00"
        End If

        ShowCompactMsg = True

        UcDest.nPackID = 2777
        UcDest.nReportID = 2777
        UcDest.nSmartID = 2777
        UcDest.cmdImport.Visible = False
        UcDest.m_CanDisable = False
        UcDest.LoadAll()

        'user defaults
        UcLogin.txtPassword.Text = oUI.ReadRegistry("DefPassword", "", True)
        txtReportLoc.Text = oUI.ReadRegistry("ReportsLoc", "http://sqlrdsamples.christiansteven.com/reportserver/reportservice.asmx")
        cmbDateTime.Text = oUI.ReadRegistry("DefDateTimeStamp", "")

        Dim cacheFile As String = sAppPath & "url.dat"
        Dim cache As String = IsNull(ReadTextFromFile(cacheFile))

        For Each s As String In cache.Split("|")
            If s <> "" Then
                lsvServers.Items.Add(s)
            End If
        Next

        lsvServers.Sorting = SortOrder.Ascending
        lsvServers.Sort()

        'clustering
        If oUI.ReadRegistry("SystemEnvironment", "") = "SQL-RD Cluster" Then
            chkCluster.Checked = True
        Else
            chkCluster.Checked = False
        End If

        If chkCluster.Checked = True Then
            cmbClusterType.Text = oUI.ReadRegistry("ClusterType", "")

            Select Case cmbClusterType.Text
                Case "Master"
                    txtSlaveTimeout.Value = oUI.ReadRegistry("SlaveTimeout", 25)
                    txtPort.Text = oUI.ReadRegistry("ClusterPort", 25)

                    Dim oData As New clsMarsData
                    Dim oItem As ListViewItem
                    Dim oRs As ADODB.Recordset

                    oRs = clsMarsData.GetData("SELECT * FROM Slaves")

                    Do While oRs.EOF = False
                        oItem = lsvSlaves.Items.Add(oRs("slavename").Value)

                        oItem.SubItems.Add(oRs("slavepath").Value)
                        oItem.SubItems.Add(oRs("ipaddress").Value)

                        oItem.Tag = oRs("slaveid").Value

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                Case "Slave"
                    txtMaster.Text = oUI.ReadRegistry("ClusterMaster", "")
            End Select
        End If

        Me._LoadPaths()

        'system paths
        txtCachedData.Text = oUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Data\")
        txtSnapShots.Text = oUI.ReadRegistry("SnapshotsPath", sAppPath & "Snapshots\")
        Me.chkConvertToMsg.Checked = Convert.ToInt32(oUI.ReadRegistry("ConvertToMsg", 0))

        chkEmailRetry.Enabled = True
        cmdOK.Enabled = True

        isLoaded = True
    End Sub

    Private Sub _LoadPaths()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim oItem As ListViewItem

        'transfer from registry into table
        Dim sPath As String
        Dim sAge As String
        Dim sUnit As String

        sPath = oUI.ReadRegistry(oReg, sKey, "TempFolder", "")
        sAge = oUI.ReadRegistry(oReg, sKey, "AgedValue", "")
        sUnit = oUI.ReadRegistry(oReg, sKey, "AgedUnit", "")

        If sUnit.Length > 0 Then
            SQL = "INSERT INTO HouseKeepingPaths (EntryID,FolderPath,KeepFor,KeepUnit) VALUES (" & _
            clsMarsData.CreateDataID("housekeepingpaths", "entryid") & "," & _
            "'" & SQLPrepare(sPath) & "'," & _
            sAge & "," & _
            "'" & sUnit & "')"

            If clsMarsData.WriteData(SQL) = True Then
                oUI.DeleteRegistry(oReg, sKey, "AgedValue")
                oUI.DeleteRegistry(oReg, sKey, "AgedUnit")
            End If
        End If

        SQL = "SELECT * FROM HouseKeepingPaths"

        oRs = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing Then
            lsvPaths.Items.Clear()

            Do While oRs.EOF = False
                oItem = New ListViewItem

                oItem.Tag = oRs("entryid").Value
                oItem.Text = oRs("folderpath").Value
                oItem.SubItems.Add(oRs("keepfor").Value & " " & oRs("keepunit").Value)

                lsvPaths.Items.Add(oItem)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub
    Private Sub cmbMailType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMailType.SelectedIndexChanged
        On Error Resume Next
        Dim oSec As clsMarsSecurity = New clsMarsSecurity
        cmdApply.Enabled = True

        oUI.ResetError(sender, ep)

        Select Case cmbMailType.Text
            Case "SMTP"
                grpSMTP.Visible = True
                grpMAPI.Visible = False
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False

                grpSMTP.BringToFront()
                txtSMTPUserID.Text = oUI.ReadRegistry("SMTPUserID", "")
                txtSMTPPassword.Text = oUI.ReadRegistry("SMTPPassword", "", True)
                txtSMTPServer.Text = oUI.ReadRegistry("SMTPServer", "")
                txtSMTPSenderAddress.Text = oUI.ReadRegistry("SMTPSenderAddress", "")
                txtSMTPSenderName.Text = oUI.ReadRegistry("SMTPSenderName", "")
                txtSMTPPort.Value = oUI.ReadRegistry("SMTPPort", 25)

                chkEmailRetry.Enabled = True
                cmdClearSettings.Enabled = True
                cmdMailTest.Enabled = True
                GroupBox11.Enabled = True
            Case "MAPI"
                grpMAPI.Visible = True
                grpSMTP.Visible = False
                grpMAPI.BringToFront()
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False
                cmbMAPIProfile.Text = oUI.ReadRegistry("MAPIProfile", "")
                txtMAPIPassword.Text = oUI.ReadRegistry("MAPIPassword", "", True)

                chkEmailRetry.Enabled = True
                cmdClearSettings.Enabled = True
                cmdMailTest.Enabled = True
                GroupBox11.Enabled = True
            Case "SQLRDMAIL"
                txtCRDSender.Text = oUI.ReadRegistry("SQL-RDSender", "")
                txtCRDAddress.Text = oUI.ReadRegistry("SQL-RDAddress", "")
                cmdMailTest.Enabled = True
                cmdApply.Enabled = False
                cmdOK.Enabled = False
                cmdActivate.Visible = True

                grpCRDMail.BringToFront()
                grpCRDMail.Visible = True

                grpMAPI.Visible = False
                grpSMTP.Visible = False
                grpGroupwise.Visible = False

                If gnEdition = MarsGlobal.gEdition.EVALUATION Then
                    txtCRDAddress.Text = "evaluation@crystalreportsdistributor.com"
                    txtCRDSender.Text = "SQL-RD Evaluation"
                    txtCRDAddress.ReadOnly = True
                    txtCRDSender.ReadOnly = True
                End If

                cmdClearSettings.Visible = True
                chkEmailRetry.Visible = True
                GroupBox11.Enabled = True
            Case "GROUPWISE"
                Me.txtgwUser.Text = oUI.ReadRegistry("GWUser", "")
                Me.txtgwPassword.Text = oUI.ReadRegistry("GWPassword", "", True)
                Me.txtgwProxy.Text = oUI.ReadRegistry("GWProxy", "")
                Me.txtgwPOIP.Text = oUI.ReadRegistry("GWPOIP", "")
                Me.txtgwPOPort.Text = oUI.ReadRegistry("GWPOPort", "")

                grpGroupwise.Visible = True
                grpGroupwise.BringToFront()

                cmdClearSettings.Visible = True
                chkEmailRetry.Visible = True
                GroupBox11.Enabled = True

                grpMAPI.Visible = False
                grpSMTP.Visible = False
                grpCRDMail.Visible = False
            Case "NONE"
                grpSMTP.Visible = False
                grpMAPI.Visible = False
                grpCRDMail.Visible = False
                grpGroupwise.Visible = False
                chkEmailRetry.Enabled = False
                chkEmailRetry.Checked = False
                cmdClearSettings.Enabled = False
                cmdMailTest.Enabled = False
                GroupBox11.Enabled = False
        End Select
    End Sub

    Private Sub cmbPollInt_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        On Error Resume Next
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub txtCustomerNo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCustomerNo.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkThreading_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkThreading.CheckedChanged
        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.sa1_MultiThreading) = False And chkThreading.Checked Then
            If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            chkThreading.Checked = False
        End If

        cmdApply.Enabled = True

        txtThreadCount.Enabled = chkThreading.Checked

        If chkThreading.Checked = False Then
            txtThreadCount.Minimum = 1
            txtThreadCount.Value = 1
        Else
            txtThreadCount.Value = 2
            txtThreadCount.Minimum = 2
        End If
    End Sub

    Private Sub optErrorLog_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorLog.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub optErrorMail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorMail.CheckedChanged
        On Error Resume Next
        cmdApply.Enabled = True

        txtErrorAlertWho.Enabled = optErrorMail.Checked
        cmdAlertWho.Enabled = optErrorMail.Checked
    End Sub

    Private Sub chkEmailRetry_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEmailRetry.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtTempFolder_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTempFolder.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub



    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        On Error Resume Next
        Dim sErrorHandle As String
        Dim nMailType As gMailType
        Dim oSec As clsMarsSecurity = New clsMarsSecurity

        If skipValidation = False Then
            'validate user entry
            If cmbPollInt.Text = "" Then
                tabOptions.SelectedTabIndex = 2
                ep.SetError(cmbPollInt, "Please enter the polling interval")
                cmbPollInt.Focus()
                Exit Sub
            ElseIf optErrorMail.Checked = True And txtErrorAlertWho.Text = "" Then
                tabOptions.SelectedTabIndex = 0
                ep.SetError(txtErrorAlertWho, "Please enter the email address for the error alerts")
                txtErrorAlertWho.Focus()
                Exit Sub
            ElseIf optErrorSMS.Checked = True And txtAlertSMS.Text.Length = 0 Then
                tabOptions.SelectedTabIndex = 0
                ep.SetError(txtAlertSMS, "Please enter the cellphone number for the error alerts")
                txtAlertSMS.Focus()
                Exit Sub
            ElseIf cmbMailType.Text = "" Then
                tabOptions.SelectedTabIndex = 1
                ep.SetError(cmbMailType, "Please select the mail type")
                cmbMailType.Focus()
                Exit Sub
            ElseIf cmbMailType.Text = "MAPI" Then
                If cmbMAPIProfile.Text = "" Then
                    tabOptions.SelectedTabIndex = 1
                    ep.SetError(cmbMAPIProfile, "Please enter the MAPI profile")
                    cmbMAPIProfile.Focus()
                    Exit Sub
                ElseIf txtMAPIPassword.Text = "" Then
                    tabOptions.SelectedTabIndex = 1
                    ep.SetError(txtMAPIPassword, "Please enter your Windows NT password here")
                    txtMAPIPassword.Focus()
                End If
            ElseIf cmbMailType.Text = "SMTP" Then
                If txtSMTPServer.Text = "" Then
                    tabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPServer, "Please enter the SMTP Server name here")
                    txtSMTPServer.Focus()
                    Exit Sub
                ElseIf txtSMTPSenderAddress.Text = "" Then
                    tabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPSenderAddress, "Please enter your sender address")
                    txtSMTPSenderAddress.Focus()
                    Exit Sub
                ElseIf txtSMTPSenderName.Text = "" Then
                    tabOptions.SelectedTabIndex = 1
                    ep.SetError(txtSMTPSenderName, "Please enter your sender name")
                    txtSMTPSenderName.Focus()
                    Exit Sub
                End If
            ElseIf txtTempFolder.Text = "" Then
                tabOptions.SelectedTabIndex = 7
                ep.SetError(txtTempFolder, "Please specify the temporary output folder")
                txtTempFolder.Focus()
                Exit Sub

            ElseIf chkAudit.Checked = True Then
                If ucDSN._Validate = False Then
                    tabOptions.SelectedTabIndex = 8
                    Exit Sub
                End If
            End If

            If chkCluster.Checked = True Then
                If cmbClusterType.Text.Length = 0 Then
                    ep.SetError(cmbClusterType, "Please select the role this machine will play in the cluster")
                    cmbClusterType.Focus()
                    tabOptions.SelectedTabIndex = 6
                    Return
                ElseIf cmbClusterType.Text = "Master" And lsvSlaves.Items.Count = 0 Then
                    ep.SetError(lsvSlaves, "Please add some slaves to the cluster")
                    tabOptions.SelectedTabIndex = 6
                    Return
                ElseIf cmbClusterType.Text = "Slave" And txtMaster.Text.Length = 0 Then
                    ep.SetError(txtMaster, "Please select the path to the Master's system file")
                    txtMaster.Focus()
                    tabOptions.SelectedTabIndex = 6
                    Return
                End If
            End If

            If optErrorLog.Checked = True Then
                sErrorHandle = "Log"
            ElseIf optErrorMail.Checked = True Then
                sErrorHandle = "Email"
            ElseIf optErrorSMS.Checked = True Then
                sErrorHandle = "SMS"
            Else
                sErrorHandle = "Both"
            End If

            If cmbMailType.Text.ToLower = "none" Then
                nMailType = MarsGlobal.gMailType.NONE
                MailType = MarsGlobal.gMailType.NONE
            ElseIf cmbMailType.Text.ToLower = "mapi" Then
                nMailType = MarsGlobal.gMailType.MAPI
                MailType = MarsGlobal.gMailType.MAPI
            ElseIf cmbMailType.Text.ToLower = "smtp" Then
                nMailType = MarsGlobal.gMailType.SMTP
                MailType = MarsGlobal.gMailType.SMTP
            ElseIf cmbMailType.Text.ToLower = "sqlrdmail" Then
                nMailType = MarsGlobal.gMailType.SQLRDMAIL
                MailType = MarsGlobal.gMailType.SQLRDMAIL
            ElseIf cmbMailType.Text.ToLower = "groupwise" Then
                nMailType = gMailType.GROUPWISE
                MailType = gMailType.GROUPWISE
            End If
        End If

        Dim SMTPManager As Boolean

        If MailType = gMailType.SMTP Then
            SMTPManager = True
        Else
            SMTPManager = False
        End If

        For Each oForm As frmWindow In oWindow
            Dim oBtn As DevComponents.DotNetBar.ButtonItem

            oBtn = oForm.DotNetBarManager1.GetItem("btnSMTPManager")

            oBtn.Enabled = SMTPManager
        Next

        With oUI
            'general page
            .SaveRegistry("Poll", cmbPollInt.Text)
            .SaveRegistry("CustNo", txtCustomerNo.Text)
            .SaveRegistry("SQL-RDThreading", Convert.ToInt32(chkThreading.Checked))
            .SaveRegistry("AlertWho", txtErrorAlertWho.Text)
            .SaveRegistry("DoNotAutoCompact", Convert.ToInt32(chkCompact.Checked))
            .SaveRegistry("RunOutOfProcess", Convert.ToInt32(chkOutofProcess.Checked))

            .SaveRegistry("DTRefresh", Convert.ToInt32(chkDTRefresh.Checked))
            .SaveRegistry("DTRefreshInterval", txtRefresh.Value)
            .SaveRegistry("DoNotUNC", Convert.ToInt32(chkUNC.Checked))
            .SaveRegistry("DoNotParseReportFields", Convert.ToInt32(chkDisableCRParsing.Checked))

            'hidden parameters setting
            .SaveRegistry("ShowHiddenParameters", Convert.ToInt32(Me.chkShowHiddenParameters.Checked))

            If chkDTRefresh.Checked Then
                For Each oform As frmWindow In oWindow
                    oform.tmRefresh.Interval = txtRefresh.Value * 1000
                    oform.tmRefresh.Enabled = True
                    oform.tmRefresh.Start()
                Next
            Else
                For Each oform As frmWindow In oWindow
                    oform.tmRefresh.Enabled = False
                Next
            End If

            'new error handling
            .SaveRegistry("ErrorByLog", Convert.ToInt32(optErrorLog.Checked))
            .SaveRegistry("ErrorByMail", Convert.ToInt32(optErrorMail.Checked))
            .SaveRegistry("ErrorBySMS", Convert.ToInt32(optErrorSMS.Checked))
            .SaveRegistry("SMSAlertWho", txtAlertSMS.Text)

            CustID = txtCustomerNo.Text

            'messaging page
            .SaveRegistry("MailType", cmbMailType.Text)
            .SaveRegistry("MAPIProfile", cmbMAPIProfile.Text)
            .SaveRegistry("MAPIPassword", txtMAPIPassword.Text, True)

            If .ReadRegistry("MAPIType", "") = "" Then
                .SaveRegistry("MAPIType", "Single")
            End If

            .SaveRegistry("EmailRetry", Convert.ToString(Convert.ToInt32(chkEmailRetry.Checked)))
            .SaveRegistry("SMTPUserID", txtSMTPUserID.Text)
            .SaveRegistry("SMTPPassword", txtSMTPPassword.Text, True)
            .SaveRegistry("SMTPServer", txtSMTPServer.Text)
            .SaveRegistry("SMTPSenderAddress", txtSMTPSenderAddress.Text)
            .SaveRegistry("SMTPSenderName", txtSMTPSenderName.Text)
            .SaveRegistry("SMTPTimeout", cmbSMTPTimeout.Text)
            .SaveRegistry("SMTPPort", Me.txtSMTPPort.Value)

            .SaveRegistry("SQL-RDSender", txtCRDSender.Text)
            .SaveRegistry("SQL-RDAddress", txtCRDAddress.Text)

            .SaveRegistry("GWUser", Me.txtgwUser.Text)
            .SaveRegistry("GWPassword", Me.txtgwPassword.Text, True)
            .SaveRegistry("GWProxy", Me.txtgwProxy.Text)
            .SaveRegistry("GWPOIP", Me.txtgwPOIP.Text)
            .SaveRegistry("GWPOPort", Me.txtgwPOPort.Text)

            .SaveRegistry("CharSetEncoding", cmbEncoding.Text)

            'sms options
            .SaveRegistry("SMSDevice", cmbSMSDevice.Text)
            .SaveRegistry("SMSDataFormat", cmbSMSFormat.Text)
            .SaveRegistry("SMSSpeed", cmbSMSSpeed.Text)
            .SaveRegistry("SMSSInit", txtSMSInit.Text)

            If optUseGSM.Checked = True Then
                .SaveRegistry("SMSDeviceType", "GSM")
            Else
                .SaveRegistry("SMSDeviceType", "SMSC")
            End If

            .SaveRegistry("SMSCNumber", txtSMSNumber.Text)
            .SaveRegistry("SMSCProtocol", cmbSMSProtocol.Text)
            .SaveRegistry("SMSCPassword", txtSMSPassword.Text, True)
            .SaveRegistry("SMSSender", txtSMSSender.Text)
            .SaveRegistry("UseUnicode", Convert.ToInt32(chkUnicode.Checked))

            'scheduler
            .SaveRegistry("CheckScheduler", Convert.ToInt32(chkCheckService.Checked))
            .SaveRegistry("UniversalTime", Convert.ToInt32(chkUniversal.Checked))
            .SaveRegistry("CPU Priority", cmbPriority.Text)
            .SaveRegistry("ThreadCount", txtThreadCount.Value)
            .SaveRegistry("PollEB", Me.cmbPollIntEB.Value)
            .SaveRegistry("CPU Priority EB", Me.cmbPriorityEB.Text)
            .SaveRegistry("Use Relative Time", Convert.ToInt32(chkUseRelTime.Checked))
            .SaveRegistry("DelayRestart", Convert.ToInt32(Me.chkDelayRestart.Checked))
            .SaveRegistry("DelayRestartBy", Me.txtDelayBy.Value)

            'housekeeping

            .SaveRegistry("TempFolder", _CreateUNC(txtTempFolder.Text))

            If UpdateAutoCompact = True Then
                If chkAutoCompact.Checked = True Then
                    .SaveRegistry("AutoCompact", 1)
                Else
                    .SaveRegistry("AutoCompact", 0)
                End If

                Dim CompactDue As String

                CompactDue = ConDateTime(Now.Date & " " & dtAutoCompact.Text)

                .SaveRegistry("CompactDue", CompactDue)
            End If

            'user defaults
            SaveTextToFile(txtDefaultEmail.Text, sAppPath & "defmsg.sqlrd", , , False)
            SaveTextToFile(txtSig.Text, sAppPath & "defsig.sqlrd", , , False)
            .SaveRegistry("DefSubject", txtDefSubject.Text)
            .SaveRegistry("DefAttach", txtDefAttach.Text)
            .SaveRegistry("Archive", Convert.ToInt32(chkArchive.Checked))
            .SaveRegistry("ArchiveAfter", txtArchive.Value)
            .SaveRegistry("DefDateTimeStamp", cmbDateTime.Text)
            .SaveRegistry("DefDBUser", Me.UcLogin.txtUserID.Text)
            .SaveRegistry("DefPassword", Me.UcLogin.txtPassword.Text)

            .SaveRegistry("ReportsLoc", txtReportLoc.Text)

            Dim urloutput As String = ""

            For Each item As ListViewItem In lsvServers.Items
                If item.Text <> "" Then
                    urloutput &= item.Text & "|"
                End If
            Next

            SaveTextToFile(urloutput, sAppPath & "url.dat", , False, False)

            'security page
            .SaveRegistry("AuditTrail", Convert.ToInt32(chkAudit.Checked))


            If chkAudit.Checked = True Then
                .SaveRegistry("AuditDSN", ucDSN.cmbDSN.Text)
                .SaveRegistry("AuditUserName", ucDSN.txtUserID.Text)
                .SaveRegistry("AuditPassword", ucDSN.txtPassword.Text, True)
            End If

            'system path
            .SaveRegistry("CachedDataPath", txtCachedData.Text)
            .SaveRegistry("SnapshotsPath", txtSnapShots.Text)
            .SaveRegistry("CachePath", Me.txtReportCache.Text)
            .SaveRegistry("SentMessagesFolder", Me.txtSentMessages.Text)
            .SaveRegistry("ConvertToMsg", Convert.ToInt32(Me.chkConvertToMsg.Checked))
        End With

        cmdApply.Enabled = False

        'for clustering, don't be scarred
        If chkCluster.Checked = True Then
            oUI.BusyProgress(90, "Setting up clustering...")

            Me.SetupCluster()
        Else
            RemoveCluster()
        End If

        oUI.BusyProgress(, , True)


    End Sub


    Private Sub RemoveCluster()
        Try
            oUI.DeleteRegistry("SystemEnvironment", gConfigFile)

            oUI.DeleteRegistry("ClusterType", gConfigFile)

            'remove my self from the master
            If txtMaster.Text <> "" Then
                Dim cnAdd As New ADODB.Connection

                Dim SQL As String = "DELETE FROM slaves WHERE slavename = '" & Environment.MachineName & "'"

                cnAdd.Open("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & txtMaster.Text & "\sqlrdlive.dat;Persist Security Info=False")

                cnAdd.Execute(SQL)

                cnAdd.Close()

                oUI.DeleteRegistry("ClusterMaster")
            Else
                Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM Slaves")

                If oRs Is Nothing Then Return

                Do While oRs.EOF = False
                    Dim slavePath As String = oRs("slavepath").Value

                    If slavePath.EndsWith("\") = False Then slavePath &= "\"

                    slavePath &= "sqlrdlive.config"

                    oUI.DeleteRegistry("SystemEnvironment", slavePath)

                    oUI.DeleteRegistry("ClusterType", slavePath)

                    oRs.MoveNext()
                Loop

                oRs.Close()

            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub SetupCluster()
        If cmbClusterType.Text = "Master" Then
            Dim fshare As IADsFileService
            Dim newShare As IADsFileShare
            Dim slaveName As String
            Dim slaveIP As String
            Dim slavePath As String
            Dim slaveConfig As String
            Dim SQL As String

            'share the sqlrd folder

            oUI.BusyProgress(75, "Creating share of SQL-RD folder", False)
            AppStatus(True)

            On Error GoTo trap

            fshare = GetObject("WinNT://" & Environment.MachineName & "/lanmanserver")
            newShare = fshare.Create("fileshare", "SQL-RD")
            newShare.Path = Environment.CurrentDirectory
            newShare.SetInfo()
            newShare = Nothing

trap:
            AppStatus(False)

            With oUI
                .SaveRegistry("SystemEnvironment", "SQL-RD Cluster")

                .SaveRegistry("ClusterType", cmbClusterType.Text)

                .SaveRegistry("SlaveTimeout", txtSlaveTimeout.Text)

                .SaveRegistry("ClusterPort", txtPort.Text)

                'save the slaves
                clsMarsData.WriteData("DELETE FROM Slaves")

                For Each oItem As ListViewItem In lsvSlaves.Items
                    slaveName = oItem.Text
                    slavePath = oItem.SubItems(1).Text
                    slaveIP = oItem.SubItems(2).Text
                    slaveConfig = slavePath & "sqlrdlive.config"

                    If slavePath.Length > 0 Then
                        SQL = "INSERT INTO Slaves(SlaveID,SlaveName, SlavePath, IPAddress) VALUES (" & _
                        clsMarsData.CreateDataID("slaves", "slaveid") & "," & _
                        "'" & SQLPrepare(slaveName) & "'," & _
                        "'" & SQLPrepare(slavePath) & "'," & _
                        "'" & SQLPrepare(slaveIP) & "')"

                        gPCName = slaveName

                        If .SaveRegistry("SystemEnvironment", "SQL-RD Cluster", , slaveConfig, True) = True Then

                            .SaveRegistry("ClusterType", "Slave", , slaveConfig, True)

                            .SaveRegistry("ClusterMaster", "\\" & Environment.MachineName & "\SQL-RD", , slaveConfig, True)

                            clsMarsData.WriteData(SQL)
                        End If
                    End If
                    gPCName = String.Empty
                Next
            End With

            'SaveMailSettingsToSlaves(slaveConfig)

        ElseIf cmbClusterType.Text = "Slave" Then

            'save system type to a cluster

            With oUI
                .SaveRegistry("SystemEnvironment", "SQL-RD Cluster")

                'set the cluster role to be a slave
                .SaveRegistry("ClusterType", "Slave")
                .SaveRegistry("ClusterMaster", txtMaster.Text)
            End With
        End If
    End Sub
    Public Sub SaveMailSettingsToSlaves(ByVal slaveConfig As String)
        Dim I As Integer
        Dim Slave As String
        Dim oItem As ListViewItem
        Dim oSec As New clsMarsSecurity
        Dim sErrorHandle As String

        Try
            For Each oItem In lsvSlaves.Items
                Slave = oItem.Text

                gPCName = Slave

                oUI.SaveRegistry("PrintPage", "25", , slaveConfig)

                oUI.SaveRegistry("AlertWho", txtErrorAlertWho.Text, , slaveConfig)

                oUI.SaveRegistry("MailType", cmbMailType.Text, , slaveConfig)

                oUI.SaveRegistry("SMTPServer", txtSMTPServer.Text, , slaveConfig)

                oUI.SaveRegistry("SMTPSenderName", txtSMTPSenderName.Text, , slaveConfig)

                oUI.SaveRegistry("ClusterPort", txtPort.Text, , slaveConfig)

                Select Case MailType
                    Case MarsGlobal.gMailType.MAPI
                        oUI.SaveRegistry("MAPIProfile", cmbMAPIProfile.Text, , slaveConfig)
                        oUI.SaveRegistry("MAPIPassword", txtMAPIPassword.Text, True, slaveConfig)
                    Case "SMTP"
                        oUI.SaveRegistry("SMTPUserID", txtSMTPUserID.Text, , slaveConfig)
                        oUI.SaveRegistry("SMTPPassword", txtSMTPPassword.Text, True, slaveConfig)
                        oUI.SaveRegistry("SMTPSenderAddress", txtSMTPSenderAddress.Text, , slaveConfig)
                        oUI.SaveRegistry("SMTPTimeout", cmbSMTPTimeout.Text, , slaveConfig)
                    Case "SQL-RD Mail"
                        oUI.SaveRegistry("SQL-RDSender", txtCRDSender.Text, , slaveConfig)
                        oUI.SaveRegistry("SQL-RDAddress", txtCRDAddress.Text, , slaveConfig)
                End Select

                oUI.SaveRegistry("ErrorByLog", Convert.ToInt32(optErrorLog.Checked), , slaveConfig)
                oUI.SaveRegistry("ErrorByMail", Convert.ToInt32(optErrorMail.Checked), , slaveConfig)
                oUI.SaveRegistry("ErrorBySMS", Convert.ToInt32(optErrorSMS.Checked), , slaveConfig)

            Next
        Catch : End Try

        gPCName = String.Empty

    End Sub
    Private Sub cmdMailTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMailTest.Click

        Dim sErr As String
        Dim vbCrLf = Environment.NewLine

        Select Case cmbMailType.Text.ToLower
            Case "mapi"
                Dim sTestAddress As String

                sTestAddress = InputBox("Please enter an email address to send to", _
               Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                oUI.SaveRegistry("TestSMTPAddress", _
                sTestAddress)

                If sTestAddress = "" Then Exit Sub

                Dim UserID As String = cmbMAPIProfile.Text
                Dim Password As String = Me.txtMAPIPassword.Text
                Dim MAPIType As String = clsMarsUI.MainUI.ReadRegistry("MAPIType", "")
                Dim sAlias As String = clsMarsUI.MainUI.ReadRegistry("MAPIAlias", "")
                Dim sxServer As String = clsMarsUI.MainUI.ReadRegistry("MAPIServer", "")

                Try
                    Dim cdoSession As Object = CreateObject("Redemption.RDOSession") ' MAPI.Session = New MAPI.Session

                    If MAPIType = "Single" Then
                        cdoSession.Logon(UserID, Password, False, True)
                    Else
                        cdoSession.LogonExchangeMailbox(sAlias, sxServer)
                    End If

                    Dim Outbox As Object = cdoSession.GetDefaultFolder(4)
                    Dim cdoMessage As Object = Outbox.Items.Add

                    With cdoMessage
                        .To = sTestAddress
                        .Body = "This is a test MAPI email sent by SQL-RD"
                        .Subject = "Test MAPI Message from SQL-RD"

                        Dim Utils As Redemption.MAPIUtils = New Redemption.MAPIUtils

                        .Recipients.ResolveAll()
                        .Save()
                        .Send()

                        Utils.DeliverNow()
                        Utils.Cleanup()
                        Utils = Nothing
                    End With

                    cdoSession = Nothing
                    cdoMessage = Nothing

                    MessageBox.Show("MAPI Mail test succeeded.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    cmdOK.Enabled = True
                    cmdApply.Enabled = True
                    chkEmailRetry.Enabled = True
                    'cdoSession.Logoff()
                Catch ex As Exception
                    'If Err.Number = 429 Or Err.Number = 91 Then
                    '    sErr = ex.Message '& 'vbCrLf & "Error initialising Collaborative Data Objects (CDO)." & vbCrLf & _
                    '    '"CDO might not be installed correctly on your pc." & vbCrLf & _
                    '    '"You can install CDO from your Microsoft Office as follows:" & vbCrLf & _
                    '    '"1. Run your Office Setup" & vbCrLf & _
                    '    '"2. Select 'Add or Remove Features'..." & vbCrLf & _
                    '    '"3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & vbCrLf & _
                    '    '"4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & vbCrLf & _
                    '    '"5. Click the 'Update' button"
                    'ElseIf Err.Number = -2147221231 Then
                    '    sErr = "Logging on to MAPI system failed. Please check the provided credentials"
                    'ElseIf Err.Number = 5 Then
                    '    sErr = "CRD cannot access the internet." & vbCrLf & _
                    '    "Please ensure that this PC is connected to the internet." & vbCrLf & _
                    '    "Please check your firewall and ensure that you have given full access to SQL-RD."
                    'Else
                    sErr = "MAPI Mail test failed with the following error: " & ex.Message & " [" & Err.Number & "]"
                    'End If

                    MessageBox.Show(sErr, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End Try
            Case "smtp", "sqlrdmail"
                Dim sTestAddress As String
                Dim ok As Boolean

                If cmbMailType.Text.ToLower = "smtp" Then
                    MailType = gMailType.SMTP
                Else
                    MailType = gMailType.SQLRDMAIL
                End If

                skipValidation = True

                m_tempSMTPValues = New Hashtable

                'lets save the current settings in case the user cancels changing the SMTP settings
                With m_tempSMTPValues
                    .Add("server", clsMarsUI.MainUI.ReadRegistry("SMTPServer", ""))
                    .Add("user", clsMarsUI.MainUI.ReadRegistry("SMTPUserID", ""))
                    .Add("password", clsMarsUI.MainUI.ReadRegistry("SMTPPassword", ""))
                    .Add("port", clsMarsUI.MainUI.ReadRegistry("SMTPPort", 25))
                    .Add("sender", clsMarsUI.MainUI.ReadRegistry("SMTPSenderName", ""))
                    .Add("senderaddress", clsMarsUI.MainUI.ReadRegistry("SMTPSenderAddress", ""))
                    .Add("timeout", clsMarsUI.MainUI.ReadRegistry("SMTPTimeout", 30))
                    .Add("sqlrdsender", clsMarsUI.MainUI.ReadRegistry("SQL-RDSender", ""))
                    .Add("sqlrdaddress", clsMarsUI.MainUI.ReadRegistry("SQL-RDAddress", ""))
                End With

                cmdApply_Click(Nothing, Nothing)

                sTestAddress = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                skipValidation = False

                oUI.SaveRegistry("TestSMTPAddress", _
                sTestAddress)

                If sTestAddress = "" Or sTestAddress.IndexOf("@") = -1 Then Exit Sub


                Dim objSender As clsMarsMessaging = New clsMarsMessaging

                ok = objSender.SendSMTP(sTestAddress, "Test SMTP Email From SQL-RD", "This is a test SMTP email sent by SQL-RD", _
                "Single", , , , , , , , , , , , , , , , , True)

                If ok = False Then
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, gErrorSuggest, True, False)
                End If

                oUI.BusyProgress(100, , True)

                If ok = True Then
                    MessageBox.Show("Email sent successfully!", Application.ProductName, MessageBoxButtons.OK, _
                    MessageBoxIcon.Information)

                    cmdApply.Enabled = True
                    cmdOK.Enabled = True
                    chkEmailRetry.Enabled = True

                End If
            Case "groupwise"
                Dim testAddress As String = InputBox("Please enter an email address to send to", _
                Application.ProductName, oUI.ReadRegistry("TestSMTPAddress", ""))

                oUI.SaveRegistry("TestSMTPAddress", testAddress)

                If testAddress = "" Or testAddress.IndexOf("@") = -1 Then Exit Sub

                If oMsg.SendGROUPWISE(testAddress, "", "", "Test GROUPWISE Mail from SQL-RD", "Test GROUPWISE email sent by SQL-RD", "", "Single") = True Then
                    MessageBox.Show("Test completed. If you have not received any errors then the email has been sent successfully", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    _ErrorHandle(gErrorDesc, gErrorNumber, gErrorSource, gErrorLine, "Please check the provided information and try again")
                End If
        End Select
    End Sub

    Private Sub cmdServiceApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdServiceApply.Click
        On Error Resume Next
        Dim oSec As clsMarsSecurity = New clsMarsSecurity
        Dim sCommand As String
        Dim sChar As String = Chr(34)
        Dim srv As New ServiceController("SQL-RD")
        Dim srvmon As New ServiceController("SQL-RD Monitor")
        Dim sParameter As String
        Dim oControl As New clsServiceController

        If optNTService.Checked = True Then
            If txtWinDomain.Text.Length = 0 Then
                ep.SetError(txtWinDomain, "Please enter your Windows domain name. If your pc is " & _
                "not a member of a domain then please enter your pc name")
                txtWinDomain.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            ElseIf Me.txtWinUser.Text.Length = 0 Then
                ep.SetError(txtWinUser, "Please enter your WIndows user id")
                txtWinUser.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            ElseIf Me.txtWinPassword.Text.Length = 0 Then
                ep.SetError(txtWinPassword, "Please enter your Windows password")
                txtWinPassword.Focus()
                cmdApply.Enabled = True
                cmdOK.Enabled = True
                Return
            End If

            Dim loggedinUser As String = Environment.UserDomainName & "\" & Environment.UserName

            If loggedinUser.ToLower <> (txtWinDomain.Text & "\" & txtWinUser.Text).ToLower Then
                Dim warning As String = "The specified user is not the same as the currently logged in user. It is advisable to install the NT service to run " & _
                "under the logged in user." & vbCrLf & _
                "Would you like to abort the NT service installation so that you may log out and log back in as the specified user?"

                Dim yesorno As Windows.Forms.DialogResult = MessageBox.Show(warning, Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)

                If yesorno = Windows.Forms.DialogResult.Yes Then Return

            End If

            Dim checkList As frmNTCheckList = New frmNTCheckList

            If checkList.ShowCheckList() = False Then
                Return
            End If

            If oControl.InstallNTScheduler(txtWinDomain.Text, txtWinUser.Text, _
            txtWinPassword.Text, cmbMailType.Text) = True Then
                cmdServiceApply.Enabled = False
                cmdServiceStart.Enabled = True

                txtReportCache.Text = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

                txtCachedData.Text = oUI.ReadRegistry("CachedDataPath", sAppPath & "Cached Date\")

                txtSnapShots.Text = oUI.ReadRegistry("SnapshotsPath", sAppPath & "Snapshots\")

                txtTempFolder.Text = oUI.ReadRegistry("TempFolder", sAppPath & "Output\")

                gServiceType = "WindowsNT"
            End If

            chkCheckService.Enabled = True
        ElseIf optBackgroundApp.Checked = True Then

            oControl.InstallBGScheduler()

            cmdServiceStart.Enabled = True

            oUI.BusyProgress(, , True)

            cmdServiceApply.Enabled = False

            chkCheckService.Enabled = True

            gServiceType = "WindowsApp"
        ElseIf optNoScheduling.Checked = True Then

            Dim Response As DialogResult

            Response = MessageBox.Show("Having this option selected means no reports will be sent automatically, continue?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

            If Response = DialogResult.Yes Then

                oUI.BusyProgress(25, "Removing services...")

                If Environment.OSVersion.Platform.ToString.ToLower = "win32nt" Then
                    oControl.RemoveNTService()
                End If

                oControl.RemoveBAS()

                oUI.BusyProgress(95, "Cleaning up...")

                oUI.BusyProgress(, , True)

                cmdServiceStart.Enabled = False

                oUI.SaveRegistry("SQL-RDService", "NONE", , , True)

                ServiceType = "NONE"

                cmdServiceStart.Enabled = False
                cmdServiceApply.Enabled = False
                chkCheckService.Enabled = False

                gServiceType = "NONE"
            Else
                optNoScheduling.Checked = False

                Select Case ServiceType
                    Case "WindowsNT"
                        optNTService.Checked = True
                    Case "WindowsApp"
                        optBackgroundApp.Checked = True
                    Case "NONE"
                        optNoScheduling.Checked = True
                End Select

                cmdServiceApply.Enabled = False
                AppStatus(False)
                Exit Sub
            End If
        End If

        cmdApply.Enabled = True
        cmdOK.Enabled = True
    End Sub

    Private Sub cmdTempFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTempFolder.Click
        On Error Resume Next

        With ofd
            .Description = "Please select the temporary output folder..."
            .ShowNewFolderButton = True
            .ShowDialog()

            If .SelectedPath = "" Then
                Exit Sub
            Else
                txtTempFolder.Text = .SelectedPath

                If txtTempFolder.Text.EndsWith("\") = False Then
                    txtTempFolder.Text &= "\"
                End If
            End If
        End With
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdApply_Click(sender, e)

        clsSettingsManager.Appconfig.savetoConfigFile(gConfigFile, False)

        If Me.m_scheduleStatus = clsServiceController.enum_svcStatus.RUNNING Then
            oUI.BusyProgress(50, "Restarting scheduler...")

            Dim oSvc As clsServiceController = New clsServiceController

            oSvc.StopScheduler()

            _Delay(1.5)

            oUI.BusyProgress(90, "Cleaning up...", True)

            oSvc.StartScheduler()
        End If

        Me.Close()
    End Sub

    Private Sub txtErrorAlertWho_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtErrorAlertWho.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSMTPUserID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPUserID.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub txtSMTPPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPPassword.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub txtSMTPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPServer.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub txtSMTPSenderAddress_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPSenderAddress.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub txtSMTPSenderName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPSenderName.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub cmbSMTPTimeout_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub cmbMAPIProfile_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbMAPIProfile.SelectedIndexChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub txtMAPIPassword_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMAPIPassword.TextChanged
        oUI.ResetError(sender, ep)
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub optNTService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNTService.CheckedChanged
        If optNTService.Checked = True Then
            grpNTService.Enabled = True
        End If
        cmdServiceApply.Enabled = True

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub optBackgroundApp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optBackgroundApp.CheckedChanged
        If optBackgroundApp.Checked = True Then
            grpNTService.Enabled = False
        End If
        cmdServiceApply.Enabled = True

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub optNoScheduling_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optNoScheduling.CheckedChanged
        cmdServiceApply.Enabled = True

        If optNoScheduling.Checked = True Then
            grpNTService.Enabled = False
            chkEmailRetry.Enabled = False
            GroupBox16.Enabled = False
            GroupBox2.Enabled = False

        Else
            If cmbMailType.Text <> "NONE" Then
                chkEmailRetry.Enabled = True
            End If

            If Me.optNTService.Checked = True Then
                grpNTService.Enabled = True
            End If
            GroupBox16.Enabled = True
            GroupBox2.Enabled = True

        End If

        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub


    Private Sub cmdServiceStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdServiceStart.Click
        On Error Resume Next
        Dim I As Integer

        If optNTService.Checked = True Then
            Dim srv As New ServiceController("SQL-RD")
            Dim srvmon As New ServiceController("SQL-RD Monitor")

            If srvmon.Status <> ServiceControllerStatus.Running Then
                srvmon.Start()
            End If

            If srv.Status <> ServiceControllerStatus.Running Then
                srv.Start()
            End If

            For I = 1 To 100 Step 20
                oUI.BusyProgress(I, "Starting service...")
            Next

            Process.Start(sAppPath & "ServiceMonitor.exe")

        ElseIf optBackgroundApp.Checked = True Then
            Dim oProc As Process = New Process

            oProc.StartInfo.FileName = sAppPath & "sqlrdapp.exe"

            For I = 1 To 100 Step 20
                oUI.BusyProgress(I, "Starting service...")
            Next

            oProc.Start()

            Process.Start(sAppPath & "sqlrdappmon.exe")
        End If


        oUI.BusyProgress(, , True)

        cmdServiceStart.Enabled = False

    End Sub

    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click
        Dim oUI As New clsMarsMessaging

        oUI.OutlookAddresses(txtErrorAlertWho)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click
        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        oPicker.cmdBcc.Enabled = False
        oPicker.cmdCc.Enabled = False
        oPicker.txtBcc.Enabled = False
        oPicker.txtCc.Enabled = False

        sValues = oPicker.PickContact("to")

        If sValues Is Nothing Then Return

        If txtErrorAlertWho.Text.EndsWith(";") = False And txtErrorAlertWho.Text.Length > 0 _
        Then txtErrorAlertWho.Text &= ";"

        Try
            txtErrorAlertWho.Text &= sValues(0)
        Catch
        End Try
    End Sub

    Private Sub cmdAlertWho_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAlertWho.MouseUp
        If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False
        mnuContacts.Show(cmdAlertWho, New Point(e.X, e.Y))
    End Sub

    Private Sub cmdRegister_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRegister.Click
        If lsvComponents.SelectedItems.Count = 0 Then Return

        Dim sPath As String = sAppPath
        Dim oProc As New Process

        sPath = sPath.ToLower.Replace("sqlrd", "common")

        If lsvComponents.SelectedItems.Count = 0 Then Return

        Try
            Select Case lsvComponents.SelectedItems(0).Text.ToLower
                Case "collaboration data objects"
                    'Dim cdoSession As MAPI.Session
                    'Dim cdoVer As String

                    'cdoSession = New MAPI.Session

                    Try
                        'cdoVer = cdoSession.Version

                        MessageBox.Show("Collaboration Data Objects Test successful." & _
                        Environment.NewLine & _
                        "Installed version is cdo " & 1.2, Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Catch
                        MessageBox.Show("Error initialising Collaborative Data Objects (CDO)." & Environment.NewLine & _
                        "CDO might not be installed correctly on your pc." & Environment.NewLine & _
                        "You can install CDO from your Microsoft Office as follows:" & Environment.NewLine & _
                        "1. Run your Office Setup" & Environment.NewLine & _
                        "2. Select 'Add or Remove Features'..." & Environment.NewLine & _
                        "3. In the next screen, Open the 'Microsoft Outlook for Windows' node" & Environment.NewLine & _
                        "4. Open 'Collaboration Data Objects' and select 'Run from My Computer'" & Environment.NewLine & _
                        "5. Click the 'Update' button", Application.ProductName, _
                        MessageBoxButtons.OK, _
                        MessageBoxIcon.Exclamation)
                    End Try
                Case "compression support"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "dzactx.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "extended mapi support"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "redemption.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "mapi profiles enumerator"
                    oProc.StartInfo.Arguments = Chr(34) & sPath & "profman.dll" & Chr(34)
                    oProc.StartInfo.FileName = "regsvr32.exe"
                    oProc.Start()
                Case "sqlrd - crystal connector"
                    Try
                        Dim oRes As DialogResult

                        oRes = MessageBox.Show("This action will shut down SQL-RD and the scheduler so that " & _
                        "the files in use can be accessed. Proceed?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                        If oRes = DialogResult.Yes Then
                            Process.Start(sAppPath & "crfr.exe")
                        End If
                    Catch ex As Exception
                        _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
                        _GetLineNumber(ex.StackTrace))
                    End Try
            End Select
        Catch : End Try
    End Sub

    Private Sub txtDefSubject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefSubject.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbMAPIProfile_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbMAPIProfile.DropDown
        Try
            Dim oProfs As New ProfMan.Profiles

            cmbMAPIProfile.Items.Clear()

            For I As Integer = 1 To oProfs.Count
                cmbMAPIProfile.Items.Add(oProfs.Item(I).Name)
            Next
        Catch
        End Try

    End Sub

    Private Sub cmdSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSpell.Click
        Dim oBox(2) As TextBox

        oBox(0) = txtDefSubject
        oBox(1) = txtDefaultEmail
        oBox(2) = txtSig

        With Speller
            .Modal = True
            .ModalOwner = Me
            .TextBoxBasesToCheck = oBox
            .Check()
        End With
    End Sub

    Private Sub chkArchive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkArchive.CheckedChanged
        txtArchive.Enabled = chkArchive.Checked
        cmdApply.Enabled = True
    End Sub


    Private Sub chkMaintainColAlign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkUseFormat_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkKeepImages_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub oSMTP_SMTPStatus(ByVal TotalSent As Long, ByVal TotalSize As Long) Handles oSMTP.SMTPStatus
        Dim Percent As Double

        Percent = (TotalSent / TotalSize) * 100

        oUI.BusyProgress(Percent, "Sending email...")
    End Sub

    Private Sub cmdActivate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdActivate.Click
        Dim Msg As String

        If gnEdition = MarsGlobal.gEdition.EVALUATION Then
            Msg = "The account cannot be changed in the Evaluation edition. The preset " & vbCrLf & _
                "account is fully functional during the evaluation period. You can test " & vbCrLf & _
                "it by clicking 'Test Settings', and it can be used to email scheduled reports"

            MessageBox.Show(Msg, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            clsWebProcess.Start("http://www.christiansteven.com/sql-rd/members/members.htm")

            oUI.SaveRegistry("SQL-RDMailActivation", 1)
            txtCRDAddress.ReadOnly = False
            txtCRDSender.ReadOnly = False
        End If
    End Sub

    Private Sub cmdClearSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdClearSettings.Click
        Dim oRes As DialogResult

        oRes = MessageBox.Show("Clear the select mail settings? This cannot be undone", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If oRes = DialogResult.No Then Return

        Select Case cmbMailType.Text.ToLower
            Case "mapi"
                With oUI
                    .SaveRegistry("MAPIProfile", String.Empty)
                    .SaveRegistry("MAPIPassword", "")
                    .SaveRegistry("MAPIAlias", String.Empty)
                    .SaveRegistry("MAPIType", String.Empty)
                    .SaveRegistry("MAPIDomain", String.Empty)
                    .SaveRegistry("MAPIServer", String.Empty)
                    .SaveRegistry("MAPIUser", String.Empty)

                    Me.cmbMAPIProfile.Text = String.Empty
                    Me.txtMAPIPassword.Text = String.Empty
                End With
            Case "smtp"
                With oUI
                    .SaveRegistry("SMTPPassword", String.Empty)
                    .SaveRegistry("SMTPProfile", String.Empty)
                    .SaveRegistry("SMTPSenderAddress", String.Empty)
                    .SaveRegistry("SMTPSenderName", String.Empty)
                    .SaveRegistry("SMTPServer", String.Empty)
                    .SaveRegistry("SMTPTimeout", String.Empty)
                    .SaveRegistry("SMTPUserID", String.Empty)

                    Me.txtSMTPPassword.Text = String.Empty
                    Me.txtSMTPSenderAddress.Text = String.Empty
                    Me.txtSMTPServer.Text = String.Empty
                    Me.txtSMTPUserID.Text = String.Empty
                    Me.txtSMTPSenderName.Text = String.Empty
                    Me.cmbSMTPTimeout.Text = 0
                End With
            Case "sqlrdmail"
                With oUI
                    .SaveRegistry("SQL-RDAddress", String.Empty)
                    .SaveRegistry("SQL-RDSender", String.Empty)

                    txtCRDAddress.Text = String.Empty
                    txtCRDSender.Text = String.Empty
                End With
            Case "groupwise"
                With oUI
                    .SaveRegistry("GWUser", String.Empty)
                    .SaveRegistry("GWPassword", String.Empty, True)
                    .SaveRegistry("GWProxy", String.Empty)
                    .SaveRegistry("GWPOIP", String.Empty)
                    .SaveRegistry("GWPOPort", String.Empty)
                End With
        End Select
    End Sub



    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        Try
            Dim Slave As String
            Dim I As Integer
            Dim Duplicate As Boolean
            Dim sCon As String
            Dim rs As New ADODB.Recordset
            Dim SQL As String
            Dim sAlive As Boolean
            Dim strSlave As String
            Dim SlaveName As String
            Dim fbd As New FolderBrowserDialog
            Dim SlaveIP As String
            Dim tcp As TcpClient = New TcpClient


            fbd.Description = "Please select the path to the slave's SQL-RD folder"

            fbd.ShowDialog()

            If fbd.SelectedPath.StartsWith("\\") = False Then
                MessageBox.Show("You cannot connect using mapped drives. " & vbCrLf & _
                "Please select shared folder on a PC on the network so that the path is returned in UNC format.", Application.ProductName, _
                 MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If


            If fbd.SelectedPath.Length = 0 Then Return

            Slave = fbd.SelectedPath

            If Slave.EndsWith("\") = False Then Slave &= "\"



            If IO.File.Exists(Slave & "sqlrdlive.dat") = False Or IO.File.Exists(Slave & assemblyName) = False Then
                MessageBox.Show("The folder you selected is not a valid SQL-RD folder. " & vbCrLf & _
                    "Please reinstall SQL-RD on the slave if the problem persists", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                oUI.BusyProgress(10, "Establishing connection to slave...")


                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Slave & "\sqlrdlive.dat;Persist Security Info=False"

                oUI.BusyProgress(25, "Configuring slave...")

                SlaveName = Slave.Split("\")(2).ToUpper

                gPCName = SlaveName

                Dim hostEntry As System.Net.IPHostEntry = System.Net.Dns.GetHostEntry(SlaveName)

                For Each s As System.Net.IPAddress In hostEntry.AddressList
                    SlaveIP = s.ToString
                Next

                strSlave = oUI.ReadRegistry("Poll", "", , Slave & "\sqlrdlive.config", True)

                gPCName = String.Empty

                If strSlave = "" Then
                    oUI.BusyProgress(100, "", True)
                    MessageBox.Show("Could not read slave's registry. Please make sure that you " & vbCrLf & _
                        "have enough access privilidges to the slave and that the " & vbCrLf & _
                        "Remote Registry service is running on the slave", Application.ProductName, _
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If

                For Each oItem As ListViewItem In lsvSlaves.Items
                    If SlaveName = oItem.Text Then
                        MessageBox.Show("The selected computer has already been added as a slave to the cluster.", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                Next

                oUI.BusyProgress(40, "Waiting for reply...")

                _Delay(1)

                SQL = "SELECT TOP 10 * FROM ReportAttr"

                oUI.BusyProgress(60, "Attempting to read from slave's system file...")

                Try
                    rs.Open(SQL, sCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    oUI.BusyProgress(90, "Slave contacted successfully. Closing connection...")
                    sAlive = True
                    rs.Close()
                Catch
                    oUI.BusyProgress(90, "Failed to read slave's system file...")
                    sAlive = False
                End Try

                oUI.BusyProgress(100, "", True)

                If SlaveName = Environment.MachineName.ToUpper Then
                    MessageBox.Show("The master cannot be set as a slave in the same cluster", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                ElseIf sAlive = False Then
                    MessageBox.Show("Could not read from slave's system file. Make sure you have read/write access to the slave", _
                    Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    Dim oItem As ListViewItem = lsvSlaves.Items.Add(SlaveName)

                    oItem.SubItems.Add(Slave)
                    oItem.SubItems.Add(SlaveIP)

                End If
            End If
        Catch ex As Exception
            gPCName = String.Empty
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

        cmdApply.Enabled = True

    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        On Error Resume Next
        Dim SlaveName As String
        Dim SlavePath As String

        If lsvSlaves.SelectedItems.Count = 0 Then Exit Sub

        SlaveName = lsvSlaves.SelectedItems(0).Text
        SlavePath = lsvSlaves.SelectedItems(0).SubItems(1).Text

        Dim slaveConfig As String = SlavePath & "\sqlrdlive.config"

        gPCName = SlaveName

        oUI.DeleteRegistry("ClusterType", slaveConfig)
        oUI.DeleteRegistry("SystemEnvironment", slaveConfig)
        oUI.DeleteRegistry("ClusterMaster", slaveConfig)

        lsvSlaves.SelectedItems(0).Remove()

        gPCName = String.Empty

        clsMarsData.WriteData("DELETE FROM Slaves WHERE SlaveID =" & lsvSlaves.SelectedItems(0).Tag)

        cmdApply.Enabled = True
    End Sub

    Private Sub chkCluster_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCluster.CheckedChanged
        If chkCluster.Checked = True Then
            If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.sa2_Clustering) = False Then
                If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.CORPORATE)
                chkCluster.Checked = False
                Return
            ElseIf gPCName.Length > 0 Then
                MessageBox.Show("You cannot setup clustering while in remote administration. Please disconnect " & _
                "and try again.")
                chkCluster.Checked = False
                Return
            End If
        End If

        GroupBox6.Enabled = chkCluster.Checked

        cmdApply.Enabled = True
    End Sub


    Private Sub cmbClusterType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbClusterType.SelectedIndexChanged
        If cmbClusterType.Text = "Master" Then
            grpSlaves.Visible = True
            grpMaster.Visible = False
        Else
            grpSlaves.Visible = False
            grpMaster.Visible = True
        End If

        cmdApply.Enabled = True
    End Sub

    Private Sub txtSlaveTimeout_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSlaveTimeout.ValueChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSlavePoll_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdMaster_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMaster.Click
        Try
            Dim sMaster As String
            Dim sCon As String
            Dim rs As New ADODB.Recordset
            Dim sAlive As Boolean
            Dim isMaster As String
            Dim fbd As New FolderBrowserDialog

            fbd.Description = "Please select the path to the Master's system files!"

            fbd.ShowDialog()

            If fbd.SelectedPath.Length = 0 Then Return

            sMaster = fbd.SelectedPath

            If sMaster.EndsWith("\") = False Then sMaster &= "\"

            If IO.File.Exists(sMaster & "\sqlrdlive.dat") = False Or IO.File.Exists(sMaster & assemblyName) = False Then
                MessageBox.Show("The folder you selected is not a valid SQL-RD folder. " & vbCrLf & _
                    "Please reinstall SQL-RD on the master if the problem persists", Application.ProductName, _
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                oUI.BusyProgress(10, "Checking the status of the sqlrd instance...")

                isMaster = sMaster.Split("\")(2).ToUpper

                gPCName = isMaster

                isMaster = oUI.ReadRegistry("ClusterType", "")

                gPCName = String.Empty

                If isMaster <> "Master" Then
                    oUI.BusyProgress(100, "", True)
                    MessageBox.Show("The selected machine has not been configured as SQL-RD Cluster master." & vbCrLf & _
                        "Please set up this machine as a master before proceeding.", _
                        Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If

                oUI.BusyProgress(25, "Establishing connection to master...")

                _Delay(1)

                sCon = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sMaster & "\sqlrdlive.dat;Persist Security Info=False"

                oUI.BusyProgress(40, "Waiting for reply...")

                _Delay(1)

                Dim SQL As String = "SELECT TOP 10 * FROM ReportAttr"

                oUI.BusyProgress(60, "Attempting to read from master's system file...")

                Try
                    rs.Open(SQL, sCon, ADODB.CursorTypeEnum.adOpenForwardOnly, ADODB.LockTypeEnum.adLockReadOnly)

                    oUI.BusyProgress(90, "Master contacted successfully. Closing connection...")

                    sAlive = True
                    rs.Close()
                Catch
                    oUI.BusyProgress(90, "Failed to read master's system file...")
                    sAlive = False
                End Try

                rs = Nothing

                oUI.BusyProgress(100, "", True)

                txtMaster.Text = sMaster
            End If

        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try

    End Sub



    Private Sub cmdSMTPMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSMTPMore.Click
        If IsFeatEnabled(gEdition.ENTERPRISEPRO, modFeatCodes.m1_MultipleSMTPServer) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPRO)
            Return
        End If

        Dim oMore As New frmSMTPServers

        oMore.IsDialog = True

        oMore.ShowDialog(Me)
    End Sub

    Private Sub cmdDBLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDBLoc.Click
        Dim ofd As New OpenFileDialog

        With ofd
            .Title = "Please select the file to set as the default attachment"
            .ShowDialog()
            .Multiselect = False

            If .FileName.Length = 0 Then Return

            txtDefAttach.Text = .FileName
        End With


    End Sub

    Private Sub chkAutoCompact_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoCompact.CheckedChanged
        dtAutoCompact.Enabled = chkAutoCompact.Checked


        If ShowCompactMsg = True And isLoaded = True Then
            UpdateAutoCompact = True

            If chkAutoCompact.Checked = True Then
                MessageBox.Show("The SQL-RD scheduler will need to shutdown the editor in order to carry out " & _
                "the auto-compact. This will be done at the selected time and will occur without warning.", _
                Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If

    End Sub


    Private Sub cmdRptLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRptLoc.Click
        With ofd
            .Description = "Please select the path to your Crystal Reports"
            .ShowNewFolderButton = False

            .ShowDialog()

            If .SelectedPath.Length > 0 Then
                txtReportLoc.Text = _CreateUNC(.SelectedPath)
            End If
        End With
    End Sub

    Private Sub optUseSMSC_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optUseSMSC.CheckedChanged
        grpSMSC.Enabled = optUseSMSC.Checked
    End Sub

    Private Sub cmbSMSDevice_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSDevice.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSDevice_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMSDevice.DropDown
        Try
            If cmbSMSDevice.Items.Count > 0 Then Return

            Dim oSMS As New ASMSCTRLLib.SMSC

            oSMS.Activate("157C2-9A5A9-57FFB")

            Dim i As Integer

            For i = 0 To oSMS.GetDeviceCount - 1
                cmbSMSDevice.Items.Add(oSMS.GetDevice(i))    ' Add Devices
            Next

            'show the COM ports
            With cmbSMSDevice.Items
                .Add("COM1")
                .Add("COM2")
                .Add("COM3")
                .Add("COM4")
            End With
        Catch
            'show the COM ports
            With cmbSMSDevice.Items
                .Add("COM1")
                .Add("COM2")
                .Add("COM3")
                .Add("COM4")
            End With
        End Try
    End Sub

    Private Sub optErrorSMS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optErrorSMS.CheckedChanged
        On Error Resume Next
        cmdApply.Enabled = True

        txtAlertSMS.Enabled = optErrorSMS.Checked
        cmdAlertSMS.Enabled = optErrorSMS.Checked
    End Sub

    Private Sub cmdAlertWho_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlertWho.Click

    End Sub

    Private Sub cmdAlertSMS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAlertSMS.Click
        Dim oPick As New frmPickContact
        Dim sValues() As String

        oPick.sMode = "SMS"

        sValues = oPick.PickContact("to")

        If sValues Is Nothing Then Return

        If txtAlertSMS.Text.EndsWith(";") = False And txtAlertSMS.Text.Length > 0 _
        Then txtAlertSMS.Text &= ";"

        Try
            txtAlertSMS.Text &= sValues(0)
        Catch : End Try
    End Sub

    Private Sub txtAlertSMS_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtAlertSMS.TextChanged
        cmdApply.Enabled = True
        oUI.ResetError(sender, ep)
    End Sub

    Private Sub dtAutoCompact_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtAutoCompact.ValueChanged
        If isLoaded = True Then UpdateAutoCompact = True
        cmdApply.Enabled = True
    End Sub

    Private Sub chkAudit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAudit.CheckedChanged
        If chkAudit.Checked = True And gnEdition < MarsGlobal.gEdition.ENTERPRISE Then
            If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            chkAudit.Checked = False
            Return
        End If

        GroupBox12.Enabled = chkAudit.Checked

        cmdApply.Enabled = True
    End Sub

    Private Sub cmdTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTest.Click
        If ucDSN._Validate = False Then
            cmdApply.Enabled = False
            cmdOK.Enabled = False
        Else

            cmdApply.Enabled = True
            cmdOK.Enabled = True

            Dim oCon As New ADODB.Connection
            Dim SQL As String

            oUI.BusyProgress(50, "Opening connection...")

            oCon.Open(ucDSN.cmbDSN.Text, ucDSN.txtUserID.Text, ucDSN.txtPassword.Text)

            oUI.BusyProgress(85, "Creating table...")

            SQL = "CREATE TABLE CRDAuditTrail (" & _
            "AuditID        INTEGER     PRIMARY KEY," & _
            "EntryDate      DATETIME," & _
            "UserID         VARCHAR(55)," & _
            "ScheduleType   VARCHAR(55)," & _
            "ScheduleName   VARCHAR(55)," & _
            "AuditAction    VARCHAR(55))"

            Try
                oCon.Execute(SQL)
            Catch : End Try

            oUI.BusyProgress(95, "Cleaning up...")

            oCon.Close()

            oUI.BusyProgress(100, "", True)

            MessageBox.Show("Database connection established", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub cmdAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAdd.Click
        If lsvPaths.Items.Count >= 1 Then
            If IsFeatEnabled(gEdition.PLATINUM, modFeatCodes.sa7_FolderHousekeeping) = False Then
                If isLoaded = True Then _NeedUpgrade(MarsGlobal.gEdition.PLATINUM)
                Return
            End If
        End If

        Dim oKeeper As New frmAddHouseKeeper

        oKeeper._AddPath()

        Me._LoadPaths()
    End Sub

    Private Sub lsvPaths_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvPaths.SelectedIndexChanged

    End Sub

    Private Sub lsvPaths_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lsvPaths.DoubleClick
        If lsvPaths.SelectedItems.Count = 0 Then Return

        Dim oKeeper As New frmAddHouseKeeper

        oKeeper._EditPath(lsvPaths.SelectedItems(0).Tag)

        _LoadPaths()
    End Sub



    Private Sub cmdDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDelete.Click
        If lsvPaths.SelectedItems.Count = 0 Then Return

        If MessageBox.Show("Delete the selected path(s)?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each oItem As ListViewItem In lsvPaths.SelectedItems
                clsMarsData.WriteData("DELETE FROM HouseKeepingPaths WHERE EntryID =" & oItem.Tag)

                oItem.Remove()
            Next
        End If
    End Sub


    Private Sub cmdReportCache_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReportCache.Click
        Try
            Dim oRes As DialogResult

            With ofd
                .Description = "Please select the path to the cache folder"
                .ShowNewFolderButton = True
                oRes = .ShowDialog()
            End With

            If oRes = DialogResult.Cancel Then Return

            txtReportCache.Text = _CreateUNC(ofd.SelectedPath)

            Dim saveCursor As Cursor = Cursor.Current

            Try
                If txtReportCache.Text.EndsWith("\") = False Then txtReportCache.Text &= "\"

                If MessageBox.Show("Move the existing cached reports and update the schedules?", Application.ProductName, MessageBoxButtons.YesNo, _
                 MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Cursor.Current = Cursors.WaitCursor
                    MoveCache()
                End If
            Finally
                Cursor.Current = saveCursor
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub



    Private Sub MoveCache()
        Dim CurrentPath As String
        Dim sFile As String
        Dim nID As Integer
        Dim s As String

        Try
            CurrentPath = oUI.ReadRegistry("CachePath", sAppPath & "Cache\")

            Dim NewPath As String = txtReportCache.Text

            If IO.Directory.Exists(NewPath) = False Then
                IO.Directory.CreateDirectory(NewPath)
            End If

            'update reportattr and copy the files
            Dim oRs As ADODB.Recordset
            Dim SQL As String

            SQL = "SELECT * FROM ReportAttr"

            oRs = clsMarsData.GetData(SQL)

            If Not oRs Is Nothing Then
                Do While oRs.EOF = False
                    CurrentPath = oRs("cachepath").Value

                    sFile = ExtractFileName(CurrentPath)

                    Dim sCache As String = NewPath & sFile

                    nID = oRs("reportid").Value

                    Try
                        IO.File.Copy(oRs("cachepath").Value, sCache, True)
                    Catch
                        Try
                            IO.File.Copy(oRs("databasepath").Value, sCache, True)
                        Catch : End Try
                    End Try

                    SQL = "UPDATE ReportAttr SET CachePath = '" & SQLPrepare(sCache) & "' WHERE ReportID =" & nID

                    clsMarsData.WriteData(SQL)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If

            cmdApply.Enabled = True
            cmdOK.Enabled = True

            oUI.SaveRegistry("CachePath", NewPath)
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub MoveFolder(ByVal folderPath As String, ByVal destination As String)
        Dim folderName As String = ExtractFileName(folderPath)

        If destination.EndsWith("\") = False Then destination &= "\"

        If IO.Directory.Exists(destination & folderName) = False Then
            IO.Directory.CreateDirectory(destination & folderName)
        End If

        'My.Computer.FileSystem.mo()
    End Sub

    Private Sub cmdCachedData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCachedData.Click
        Try
            ofd.Description = "Please select the folder to store cached data for event-based schedule"

            Dim temp As String = txtCachedData.Text

            If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtCachedData.Text = _CreateUNC(ofd.SelectedPath)

                If txtCachedData.Text.EndsWith("\") = False Then txtCachedData.Text &= "\"

                If MessageBox.Show("Move existing cached data to this location?", Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For Each s As String In IO.Directory.GetDirectories(temp)
                        My.Computer.FileSystem.MoveDirectory(s, txtCachedData.Text & ExtractFileName(s), True)
                        'IO.Directory.Move(s, txtCachedData.Text & ExtractFileName(s))
                    Next

                    For Each s As String In IO.Directory.GetFiles(temp)
                        IO.File.Move(s, txtCachedData.Text & ExtractFileName(s))
                    Next

                    oUI.SaveRegistry("CachedDataPath", txtCachedData.Text)

                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmdSnapshots_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSnapshots.Click
        Try
            ofd.Description = "Please select the folder to store schedule snapshots"

            Dim temp As String = txtSnapShots.Text

            If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
                txtSnapShots.Text = _CreateUNC(ofd.SelectedPath)

                If txtSnapShots.Text.EndsWith("\") = False Then txtSnapShots.Text &= "\"

                If MessageBox.Show("Move existing snapshots to this location?", Application.ProductName, MessageBoxButtons.YesNo, _
                MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    For Each s As String In IO.Directory.GetDirectories(temp)
                        IO.Directory.Move(s, txtSnapShots.Text & ExtractFileName(s))
                    Next

                    For Each s As String In IO.Directory.GetFiles(temp)
                        Dim newFile As String = txtSnapShots.Text & ExtractFileName(s)

                        Dim SQL As String

                        SQL = "UPDATE SnapshotsAttr SET SnapPath = '" & SQLPrepare(newFile) & "' WHERE SnapPath = '" & SQLPrepare(s) & "'"

                        If clsMarsData.WriteData(SQL) = True Then
                            IO.File.Move(s, newFile)
                        Else
                            Exit For
                        End If
                    Next

                    oUI.SaveRegistry("SnapshotsPath", txtSnapShots.Text)
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub


    Private Sub chkDTRefresh_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDTRefresh.CheckedChanged
        txtRefresh.Enabled = chkDTRefresh.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbPriority_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPriority.SelectedIndexChanged, cmbPriorityEB.SelectedIndexChanged
        If isLoaded = True And Me.optNoScheduling.Checked = False Then
            ep.SetError(cmbPriority, "You will need to restart the scheduler for this change to take effect")
        End If
        cmdApply.Enabled = True
    End Sub

    Private Sub txtgwUser_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtgwUser.TextChanged, txtgwPassword.TextChanged, txtgwPOIP.TextChanged, txtgwPOPort.TextChanged, txtgwProxy.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtRefresh_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtRefresh.ValueChanged, txtRefresh.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkIgnoreSel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        cmdApply.Enabled = True
    End Sub

    Private Sub chkCompact_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCompact.CheckedChanged
        cmdApply.Enabled = True
    End Sub


    Private Sub cmbSMSFormat_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSFormat.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSSpeed_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbSMSSpeed.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbSMSProtocol_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMSProtocol.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSMSNumber_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSMSNumber.TextChanged, txtSMSPassword.TextChanged, txtSMSSender.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub chkUnicode_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkUnicode.CheckedChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbEncoding_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbEncoding.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbPollInt_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPollInt.ValueChanged, cmbPollInt.TextChanged
        cmdApply.Enabled = True
        If isLoaded = True Then
            ep.SetError(cmbPollInt, "You must restart the scheduler for the changes to take effect")
        End If
    End Sub

    Private Sub chkCheckService_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCheckService.CheckedChanged
        cmdApply.Enabled = True

        If chkCheckService.Checked = True Then
            Me.chkDelayRestart.Enabled = False
            Me.chkDelayRestart.Checked = False
        Else
            Me.chkDelayRestart.Enabled = True
        End If
    End Sub

    Private Sub txtArchive_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArchive.ValueChanged
        cmdApply.Enabled = True
    End Sub


    Private Sub txtDefAttach_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefAttach.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtDefaultEmail_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDefaultEmail.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSig_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSig.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtReportLoc_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportLoc.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub cmbDateTime_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbDateTime.SelectedIndexChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtCachedData_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCachedData.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtSnapShots_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSnapShots.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtReportCache_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtReportCache.TextChanged, txtSentMessages.TextChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtThreadCount_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtThreadCount.ValueChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub txtWinDomain_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtWinDomain.LostFocus
        txtWinDomain.Text = txtWinDomain.Text.ToUpper
    End Sub

    Private Sub txtWinDomain_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtWinDomain.TextChanged, txtWinPassword.TextChanged, txtWinUser.TextChanged
        ep.SetError(sender, "")
        cmdApply.Enabled = False
        cmdOK.Enabled = False
    End Sub

    Private Sub chkOutofProcess_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkOutofProcess.CheckedChanged

    End Sub

    Private Sub txtSMTPPort_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSMTPPort.ValueChanged
        cmdApply.Enabled = False
        cmdOK.Enabled = False
        chkEmailRetry.Enabled = False
    End Sub

    Private Sub btnSentMessages_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSentMessages.Click
        Try
            Dim oRes As DialogResult

            With ofd
                .Description = "Please select the path for the sent messages repository"
                .ShowNewFolderButton = True
                oRes = .ShowDialog()
            End With

            If oRes = DialogResult.Cancel Then Return

            Me.txtSentMessages.Text = _CreateUNC(ofd.SelectedPath)

            Dim saveCursor As Cursor = Cursor.Current

            Try
                If txtSentMessages.Text.EndsWith("\") = False Then txtSentMessages.Text &= "\"
            Finally
                Cursor.Current = saveCursor
            End Try
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub cmbPollIntEB_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPollIntEB.ValueChanged
        cmdApply.Enabled = True

        If isLoaded = True Then
            ep.SetError(cmbPollInt, "You must restart the scheduler for the changes to take effect")
        End If
    End Sub


    Private Sub btnMAPIMore_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMAPIMore.Click
        Dim oMAPI As frmMAPIType = New frmMAPIType

        oMAPI.MAPISetup()
    End Sub

    Private Sub chkDelayRestart_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDelayRestart.CheckedChanged
        txtDelayBy.Enabled = chkDelayRestart.Checked

        cmdApply.Enabled = True
    End Sub

    Private Sub txtDelayBy_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDelayBy.ValueChanged
        cmdApply.Enabled = True
    End Sub

    Private Sub btnSMTPAdvanced_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMTPAdvanced.Click
        Dim oSMTP As frmSMTPAdvanced = New frmSMTPAdvanced

        oSMTP.SMTPAdvanced(False)
    End Sub

    Private Sub chkConvertToMsg_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConvertToMsg.CheckedChanged
        If Me.chkConvertToMsg.Checked = True And isLoaded = True Then
            Try
                Dim ou As Object = CreateObject("Outlook.Application")

                ou = Nothing
            Catch ex As Exception
                Me.chkConvertToMsg.Checked = False

                Me.ep.SetError(Me.chkConvertToMsg, "Could not initialize MS Outlook. Please make sure that Outlook is installed on this machine.")
            End Try
        Else
            ep.SetError(Me.chkConvertToMsg, "")
        End If
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        If Me.m_tempSMTPValues IsNot Nothing Then
            With clsMarsUI.MainUI
                .SaveRegistry("SMTPServer", Me.m_tempSMTPValues.Item("server"), , , True)
                .SaveRegistry("SMTPUserID", Me.m_tempSMTPValues.Item("user"), , , True)
                .SaveRegistry("SMTPPassword", Me.m_tempSMTPValues.Item("password"), , , True)
                .SaveRegistry("SMTPPort", Me.m_tempSMTPValues.Item("port"), , , True)
                .SaveRegistry("SMTPSenderName", Me.m_tempSMTPValues.Item("sender"), , , True)
                .SaveRegistry("SMTPSenderAddress", Me.m_tempSMTPValues.Item("senderaddress"), , , True)
                .SaveRegistry("SMTPTimeout", Me.m_tempSMTPValues.Item("timeout"), , , True)
                .SaveRegistry("SQL-RDSender", Me.m_tempSMTPValues.Item("sqlrdsender"), , , True)
                .SaveRegistry("SQL-RDAddress", Me.m_tempSMTPValues.Item("sqlrdaddress"), , , True)
            End With

            
        End If
    End Sub

    Private Sub lsvServers_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lsvServers.KeyDown
        If e.KeyCode = Keys.Delete Then
            For Each item As ListViewItem In lsvServers.SelectedItems
                item.Remove()
            Next
        End If
    End Sub

    Private Sub lsvServers_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvServers.SelectedIndexChanged

    End Sub
End Class
