Public Class frmTaskFTPCreateDirectory
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean = True
    'Dim oFtp As ucFTPBrowser
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDirectory As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPath As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents HelpProvider1 As System.Windows.Forms.HelpProvider
    Friend WithEvents UcFTPDetails1 As sqlrd.ucFTPDetails
    Public m_eventID As Integer = 99999
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskFTPCreateDirectory))
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.txtDirectory = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblPath = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.HelpProvider1 = New System.Windows.Forms.HelpProvider
        Me.UcFTPDetails1 = New sqlrd.ucFTPDetails
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpNavigator(Me.cmdCancel, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(378, 344)
        Me.cmdCancel.Name = "cmdCancel"
        Me.HelpProvider1.SetShowHelp(Me.cmdCancel, True)
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.HelpProvider1.SetHelpNavigator(Me.cmdOK, System.Windows.Forms.HelpNavigator.Topic)
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(290, 344)
        Me.cmdOK.Name = "cmdOK"
        Me.HelpProvider1.SetShowHelp(Me.cmdOK, True)
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.HelpProvider1.SetHelpNavigator(Me.txtName, System.Windows.Forms.HelpNavigator.Topic)
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.HelpProvider1.SetShowHelp(Me.txtName, True)
        Me.txtName.Size = New System.Drawing.Size(445, 20)
        Me.txtName.TabIndex = 0
        '
        'Label1
        '
        Me.HelpProvider1.SetHelpNavigator(Me.Label1, System.Windows.Forms.HelpNavigator.Topic)
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.HelpProvider1.SetShowHelp(Me.Label1, True)
        Me.Label1.Size = New System.Drawing.Size(100, 22)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.HelpProvider1.SetHelpNavigator(Me.GroupBox1, System.Windows.Forms.HelpNavigator.Topic)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.HelpProvider1.SetShowHelp(Me.GroupBox1, True)
        Me.GroupBox1.Size = New System.Drawing.Size(445, 293)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.UcFTPDetails1)
        Me.GroupBox2.Controls.Add(Me.txtDirectory)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.lblPath)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.txtFile)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(429, 280)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        '
        'txtDirectory
        '
        Me.txtDirectory.Location = New System.Drawing.Point(118, 251)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(300, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtDirectory, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "For root folder, leave empty or enter a forward slash." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You can right-click and i" & _
                    "nsert constants and database values." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Remember:  Most FTP servers are case sen" & _
                    "sitive!", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtDirectory.TabIndex = 8
        Me.txtDirectory.Tag = "memo"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 254)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 15)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "New Directory Name"
        '
        'lblPath
        '
        Me.lblPath.BackColor = System.Drawing.SystemColors.Window
        Me.lblPath.Location = New System.Drawing.Point(7, 217)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(364, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.lblPath, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "For root folder, leave empty or enter a forward slash." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You can right-click and i" & _
                    "nsert constants and database values." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Remember:  Most FTP servers are case sen" & _
                    "sitive!", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.lblPath.TabIndex = 6
        Me.lblPath.Tag = "memo"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(4, 202)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(124, 21)
        Me.Label6.TabIndex = 51
        Me.Label6.Text = "Base FTP Directory"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(378, 216)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 20)
        Me.Button1.TabIndex = 7
        Me.Button1.Text = "..."
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFile
        '
        Me.txtFile.BackColor = System.Drawing.Color.White
        Me.txtFile.Location = New System.Drawing.Point(25, 188)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(10, 20)
        Me.txtFile.TabIndex = 52
        Me.txtFile.TabStop = False
        Me.txtFile.Visible = False
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'UcFTPDetails1
        '
        Me.UcFTPDetails1.BackColor = System.Drawing.SystemColors.Control
        Me.UcFTPDetails1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcFTPDetails1.Location = New System.Drawing.Point(7, 10)
        Me.UcFTPDetails1.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
            "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;"
        Me.UcFTPDetails1.m_ftpSuccess = False
        Me.UcFTPDetails1.Name = "UcFTPDetails1"
        Me.UcFTPDetails1.Size = New System.Drawing.Size(411, 190)
        Me.UcFTPDetails1.TabIndex = 53
        '
        'frmTaskFTPCreateDirectory
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(460, 374)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskFTPCreateDirectory"
        Me.Text = "FTP Create Directory"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region



    Private Sub frmTaskFTPCreateDirectory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If oFtp Is Nothing Then
        '    oFtp = New ucFTPBrowser

        '    oFtp.Left = 8
        '    oFtp.Top = 16

        '    oFtp.Visible = True

        '    Page1.Controls.Add(oFtp)
        'End If


        FormatForWinXP(Me)
        HelpProvider1.HelpNamespace = gHelpPath
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.HelpButton = True
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
        Me.ControlBox = True
        txtName.Focus()

       
        Me.lblPath.ContextMenu = Me.mnuInserter
        Me.txtDirectory.ContextMenu = Me.mnuInserter
        Me.txtFile.ContextMenu = Me.mnuInserter
        Me.UcFTPDetails1.m_ShowBrowse = False
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        
        ElseIf txtDirectory.Text.Length = 0 Then
            ep.SetError(txtDirectory, "Please enter the new directory name")
            txtDirectory.Focus()
            Return

        ElseIf Me.UcFTPDetails1.validateFtpInfo = False Then
            Return
        End If

        UserCancel = False
        Me.Close()

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub
    Public Sub AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID

        sCols = "TaskID,ScheduleID,TaskType,TaskName,FTPServer,FTPPort,FTPUser," & _
        "FTPPassword,FTPDirectory,ProgramPath,FTPType,FTPPassive,OrderID,FtpOptions"

        If lblPath.Text.StartsWith("/") = False Then
            lblPath.Text = "/" & lblPath.Text
        End If
        sVals = nID & "," & _
            ScheduleID & "," & _
            "'FTPCreateDirectory'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
            "" & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
            "'" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(lblPath.Text) & "'," & _
            "'" & SQLPrepare(txtDirectory.Text) & "'," & _
            "'" & UcFTPDetails1.cmbFTPType.Text & "'," & _
            Convert.ToInt32(UcFTPDetails1.chkPassive.Checked) & "," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            "'" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"


        SQL = "INSERT INTO TASKS (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)

        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFile As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtDirectory.Text = oRs("programpath").Value



            UcFTPDetails1.txtFTPServer.Text = oRs("ftpserver").Value
            UcFTPDetails1.txtPort.Text = oRs("ftpport").Value
            UcFTPDetails1.txtUserName.Text = oRs("ftpuser").Value
            UcFTPDetails1.txtPassword.Text = oRs("ftppassword").Value
            lblPath.Text = oRs("ftpdirectory").Value
            UcFTPDetails1.cmbFTPType.Text = IsNull(oRs("ftptype").Value, "FTP")

            Try
                UcFTPDetails1.chkPassive.Checked = IsNull(oRs("ftppassive").Value, 0)
            Catch : End Try

            Try
                Me.UcFTPDetails1.m_ftpOptions = IsNull(oRs("ftpoptions").Value)
            Catch : End Try

        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Return

        'sFile = oFtp.sFTPItem



        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
        "FTPServer = '" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
        "FTPPort = " & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
        "FTPuser = '" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
        "FTPPassword = '" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
        "FTPDirectory = '" & SQLPrepare(lblPath.Text) & "'," & _
        "ProgramPath = '" & SQLPrepare(txtDirectory.Text) & "'," & _
        "FTPType ='" & UcFTPDetails1.cmbFTPType.Text & "'," & _
        "FTPPassive =" & Convert.ToInt32(UcFTPDetails1.chkPassive.Checked) & "," & _
        "FtpOptions ='" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"


        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oBrowse As frmFTPBrowser2 = New frmFTPBrowser2
        Dim sReturn As String()


        sReturn = oBrowse.GetFtpInfo(Me.UcFTPDetails1.ConnectFTP)

        If sReturn Is Nothing Then

            Return
        Else
            lblPath.Text = sReturn(0)
            txtFile.Text = sReturn(1)
        End If

    End Sub
End Class
