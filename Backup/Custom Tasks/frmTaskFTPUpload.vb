Public Class frmTaskFTPUpload
    Inherits sqlrd.frmTaskMaster
    Dim nStep As Int32 = 0
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFile As System.Windows.Forms.TextBox
    Friend WithEvents lblPath As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtSourceFile As System.Windows.Forms.TextBox
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents chkRecreateFolderStructure As System.Windows.Forms.CheckBox
    Friend WithEvents chkRecursive As System.Windows.Forms.CheckBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Dim UserCancel As Boolean = True
    Friend WithEvents UcFTPDetails1 As sqlrd.ucFTPDetails
    Public m_eventID As Integer = 99999
    'Dim oFtp As ucFTPBrowser
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()
        'Xceed.Ftp.Licenser.LicenseKey = "FTN10-Y4T6U-4BUKN-MAJA"
        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents UcFTPBrowser1 As sqlrd.ucFTPBrowser
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvFiles As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdAddFiles As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend FtpClient1 As Xceed.Ftp.FtpClient = New Xceed.Ftp.FtpClient
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskFTPUpload))
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Page2 = New System.Windows.Forms.GroupBox
        Me.lsvFiles = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddFiles = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.Page3 = New System.Windows.Forms.GroupBox
        Me.chkRecreateFolderStructure = New System.Windows.Forms.CheckBox
        Me.chkRecursive = New System.Windows.Forms.CheckBox
        Me.txtSourceFile = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Page1 = New System.Windows.Forms.GroupBox
        Me.rbAdvanced = New System.Windows.Forms.RadioButton
        Me.rbSimple = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.lblPath = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtFile = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.UcFTPDetails1 = New sqlrd.ucFTPDetails
        Me.Page2.SuspendLayout()
        Me.Page3.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Page1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 22)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Task Name"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(424, 20)
        Me.txtName.TabIndex = 0
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvFiles)
        Me.Page2.Controls.Add(Me.cmdAddFiles)
        Me.Page2.Controls.Add(Me.cmdRemove)
        Me.Page2.Location = New System.Drawing.Point(6, 44)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(408, 135)
        Me.Page2.TabIndex = 2
        Me.Page2.TabStop = False
        '
        'lsvFiles
        '
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.HideSelection = False
        Me.lsvFiles.LabelEdit = True
        Me.lsvFiles.Location = New System.Drawing.Point(8, 15)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(394, 87)
        Me.lsvFiles.TabIndex = 1
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 379
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(6, 108)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 22)
        Me.cmdAddFiles.TabIndex = 0
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(54, 108)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 22)
        Me.cmdRemove.TabIndex = 2
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.chkRecreateFolderStructure)
        Me.Page3.Controls.Add(Me.chkRecursive)
        Me.Page3.Controls.Add(Me.txtSourceFile)
        Me.Page3.Controls.Add(Me.Label8)
        Me.Page3.Controls.Add(Me.cmdBrowse)
        Me.Page3.Location = New System.Drawing.Point(6, 44)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(418, 135)
        Me.Page3.TabIndex = 2
        Me.Page3.TabStop = False
        '
        'chkRecreateFolderStructure
        '
        Me.chkRecreateFolderStructure.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecreateFolderStructure.Location = New System.Drawing.Point(22, 92)
        Me.chkRecreateFolderStructure.Name = "chkRecreateFolderStructure"
        Me.chkRecreateFolderStructure.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecreateFolderStructure, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Recreates the folder structure. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Useful when using wildcards.  e.g. *.* will c" & _
                    "opy all the files and subfolders to the FTP server and recreate the folder struc" & _
                    "ture.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecreateFolderStructure.TabIndex = 3
        Me.chkRecreateFolderStructure.Text = "Recreate Folder Structure"
        '
        'chkRecursive
        '
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(22, 71)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecursive, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Searches the folder and all subfolder." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Select ""Recreated Folder Structure"" to " & _
                    "reproduce the folder structure so that multiple files of the same name are not s" & _
                    "impy overwritten.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecursive.TabIndex = 2
        Me.chkRecursive.Text = "Recursive"
        '
        'txtSourceFile
        '
        Me.txtSourceFile.BackColor = System.Drawing.Color.White
        Me.txtSourceFile.Location = New System.Drawing.Point(22, 45)
        Me.txtSourceFile.Name = "txtSourceFile"
        Me.txtSourceFile.Size = New System.Drawing.Size(344, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtSourceFile, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert Constants and database values.  You can also use w" & _
                    "ildcards e.g. my*abc.*", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtSourceFile.TabIndex = 0
        Me.txtSourceFile.Tag = "Memo"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(15, 31)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 52
        Me.Label8.Text = "File(s)"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(372, 45)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "..."
        Me.cmdBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(365, 508)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 49
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(277, 508)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 50
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.Page3)
        Me.Page1.Controls.Add(Me.Page2)
        Me.Page1.Controls.Add(Me.rbAdvanced)
        Me.Page1.Controls.Add(Me.rbSimple)
        Me.Page1.Controls.Add(Me.GroupBox2)
        Me.Page1.Location = New System.Drawing.Point(0, 3)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(432, 453)
        Me.Page1.TabIndex = 1
        Me.Page1.TabStop = False
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.Location = New System.Drawing.Point(85, 20)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(74, 17)
        Me.SuperTooltip1.SetSuperTooltip(Me.rbAdvanced, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Use wildcards or insert constants (or both) to select the source files.", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.rbAdvanced.TabIndex = 1
        Me.rbAdvanced.TabStop = True
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = True
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.Location = New System.Drawing.Point(14, 20)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(56, 17)
        Me.SuperTooltip1.SetSuperTooltip(Me.rbSimple, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Select files to upload", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.rbSimple.TabIndex = 0
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.UcFTPDetails1)
        Me.GroupBox2.Controls.Add(Me.lblPath)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Button1)
        Me.GroupBox2.Controls.Add(Me.txtFile)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 185)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(420, 262)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Upload to..."
        '
        'lblPath
        '
        Me.lblPath.BackColor = System.Drawing.SystemColors.Window
        Me.lblPath.Location = New System.Drawing.Point(6, 234)
        Me.lblPath.Name = "lblPath"
        Me.lblPath.Size = New System.Drawing.Size(362, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.lblPath, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "For root folder, leave empty or enter a forward slash." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You can right-click and i" & _
                    "nsert constants and database values." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Remember: Most FTP servers are case sens" & _
                    "itive!", Nothing, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.lblPath.TabIndex = 5
        Me.lblPath.Tag = "Memo"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.SystemColors.Control
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(6, 221)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 51
        Me.Label6.Text = "FTP Directory"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(374, 233)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 20)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "..."
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtFile
        '
        Me.txtFile.BackColor = System.Drawing.Color.White
        Me.txtFile.Location = New System.Drawing.Point(36, 145)
        Me.txtFile.Name = "txtFile"
        Me.txtFile.Size = New System.Drawing.Size(10, 20)
        Me.txtFile.TabIndex = 52
        Me.txtFile.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(432, 457)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", Global.sqlrd.My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0))
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'UcFTPDetails1
        '
        Me.UcFTPDetails1.BackColor = System.Drawing.SystemColors.Control
        Me.UcFTPDetails1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UcFTPDetails1.Location = New System.Drawing.Point(6, 19)
        Me.UcFTPDetails1.m_ftpOptions = "UsePFX=0;UsePVK=0;CertFile=;KeyFile=;CertName=;CertPassword=;UseProxy=0;ProxyServ" & _
            "er=;ProxyUser=;ProxyPassword=;ProxyPort=21;"
        Me.UcFTPDetails1.m_ftpSuccess = False
        Me.UcFTPDetails1.Name = "UcFTPDetails1"
        Me.UcFTPDetails1.Size = New System.Drawing.Size(411, 190)
        Me.UcFTPDetails1.TabIndex = 53
        '
        'frmTaskFTPUpload
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(451, 539)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskFTPUpload"
        Me.Text = "FTP Upload files"
        Me.Page2.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page3.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Page1.ResumeLayout(False)
        Me.Page1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region




    Private Sub frmTaskFTPUpload_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'If oFtp Is Nothing Then
        '    oFtp = New ucFTPBrowser

        '    oFtp.Left = 8
        '    oFtp.Top = 16

        '    oFtp.Visible = True

        '    Page1.Controls.Add(oFtp)
        'End If
        'Page1.BringToFront()
        FormatForWinXP(Me)
        txtName.Focus()
       
        Me.lblPath.ContextMenu = Me.mnuInserter
        Me.txtSourceFile.ContextMenu = Me.mnuInserter
        Me.UcFTPDetails1.m_ShowBrowse = False
    End Sub

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click
        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            sFile = _CreateUNC(sFile)
            lsv = lsvFiles.Items.Add(sFile)
            lsv.ImageIndex = 0
        Next

        ep.SetError(lsvFiles, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub


    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please enter a name for this task")
            txtName.Focus()
            Return
        ElseIf Me.txtSourceFile.Text.Length = 0 And Me.rbAdvanced.Checked = True Then
            Page3.BringToFront()
            ep.SetError(txtSourceFile, "Please select the files to upload")
            txtSourceFile.Focus()
            Return
        ElseIf lsvFiles.Items.Count = 0 And Me.rbSimple.Checked = True Then
            Page2.BringToFront()
            ep.SetError(lsvFiles, "Please select the files to upload")
            cmdAddFiles.Focus()
            Return
        ElseIf Me.UcFTPDetails1.validateFtpInfo = False Then
            Return
        End If

        UserCancel = False
        Me.Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.rbSimple.Checked = True
        Me.rbAdvanced.Checked = False
        Me.Page2.BringToFront()

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID

        If Me.rbSimple.Checked = True Then
            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If
        'note:  cc = recursive, bcc = recreate folder structure
        sCols = "TaskID,ScheduleID,TaskType,TaskName,FTPServer,FTPPort,FTPUser," & _
        "FTPPassword,FTPDirectory,Filelist,FTPType,FTPPassive,OrderID,CC,Bcc,FtpOptions"
        If lblPath.Text.StartsWith("/") = False Then
            lblPath.Text = "/" & lblPath.Text
        End If

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'FTPUpload'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
            "" & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
            "'" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
            "'" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
            "'" & SQLPrepare(lblPath.Text) & "'," & _
            "'" & SQLPrepare(sFiles) & "'," & _
            "'" & UcFTPDetails1.cmbFTPType.Text & "'," & _
             Convert.ToInt32(Me.UcFTPDetails1.chkPassive.Checked) & "," & _
            oTask.GetOrderID(ScheduleID) & "," & _
            "'" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
            "'" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
            "'" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"


        SQL = "INSERT INTO TASKS (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem



        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value
            If sFiles.Contains("|") = True Then

                Me.rbSimple.Checked = True
                Me.rbAdvanced.Checked = False
                Me.Page2.BringToFront()

                For Each sVal In sFiles.Split("|")
                    If sVal.Length > 0 Then lsvFiles.Items.Add(sVal)
                Next
            Else
                Me.rbSimple.Checked = False
                Me.rbAdvanced.Checked = True
                Me.Page3.BringToFront()
                Me.txtSourceFile.Text = sFiles
            End If

            UcFTPDetails1.txtFTPServer.Text = oRs("ftpserver").Value
            UcFTPDetails1.txtPort.Text = oRs("ftpport").Value
            UcFTPDetails1.txtUserName.Text = oRs("ftpuser").Value
            UcFTPDetails1.txtPassword.Text = oRs("ftppassword").Value
            lblPath.Text = oRs("ftpdirectory").Value
            UcFTPDetails1.cmbFTPType.Text = IsNull(oRs("ftptype").Value, "FTP")
            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))
            chkRecreateFolderStructure.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0))))

            Try
                UcFTPDetails1.chkPassive.Checked = IsNull(oRs("ftppassive").Value, 0)
            Catch : End Try

            Try
                Me.UcFTPDetails1.m_ftpOptions = IsNull(oRs("ftpoptions").Value)
            Catch :End Try
        End If

        oRs.Close()


        Me.ShowDialog()

        If UserCancel = True Then Return

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If
        SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
        "FTPServer = '" & SQLPrepare(UcFTPDetails1.txtFTPServer.Text) & "'," & _
        "FTPPort = " & SQLPrepare(UcFTPDetails1.txtPort.Text) & "," & _
        "FTPuser = '" & SQLPrepare(UcFTPDetails1.txtUserName.Text) & "'," & _
        "FTPPassword = '" & SQLPrepare(UcFTPDetails1.txtPassword.Text) & "'," & _
        "FTPDirectory = '" & SQLPrepare(lblPath.Text) & "'," & _
        "FileList = '" & sFiles & "'," & _
        "FTPType ='" & UcFTPDetails1.cmbFTPType.Text & "'" & "," & _
        "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
        "Bcc = '" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'," & _
        "FTPPassive =" & Convert.ToInt32(UcFTPDetails1.chkPassive.Checked) & "," & _
        "FtpOptions ='" & SQLPrepare(Me.UcFTPDetails1.m_ftpOptions) & "'"


        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub



    Private Sub frmTaskFTPUpload_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oBrowse As frmFTPBrowser2 = New frmFTPBrowser2
        Dim sReturn As String()

        sReturn = oBrowse.GetFtpInfo(Me.UcFTPDetails1.ConnectFTP)

        If sReturn Is Nothing Then

            Return
        Else
            lblPath.Text = sReturn(0)
            txtFile.Text = sReturn(1)
        End If
    End Sub

    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            Page3.Visible = False
            Page2.Visible = True
            Page2.BringToFront()
            rbAdvanced.Checked = False
        Else
            Page2.Visible = False
            Page3.Visible = True
            Page3.BringToFront()
            rbAdvanced.Checked = True
        End If
    End Sub

    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            Page2.Visible = False
            Page3.Visible = True
            Page3.BringToFront()
            rbSimple.Checked = False
        Else
            Page2.Visible = True
            Page3.Visible = False
            Page2.BringToFront()
            rbSimple.Checked = True
        End If
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        Dim sFD As OpenFileDialog = New OpenFileDialog
        sFD.Title = "Select file to upload"

        If sFD.ShowDialog <> Windows.Forms.DialogResult.Cancel Then
            Me.txtSourceFile.Text = _CreateUNC(sFD.FileName)
        End If
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

End Class
