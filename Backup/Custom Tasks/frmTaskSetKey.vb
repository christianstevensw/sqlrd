Imports Microsoft.Win32
Imports Microsoft.Win32.Registry
Public Class frmTaskSetKey
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmbKey As System.Windows.Forms.ComboBox
    Friend WithEvents txtValue As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ucReg As sqlrd.ucRegistryBrowser
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskSetKey))
        Me.ucReg = New sqlrd.ucRegistryBrowser
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtValue = New System.Windows.Forms.TextBox
        Me.cmbKey = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ucReg
        '
        Me.ucReg.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucReg.Location = New System.Drawing.Point(8, 15)
        Me.ucReg.Name = "ucReg"
        Me.ucReg.Size = New System.Drawing.Size(440, 273)
        Me.ucReg.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(392, 400)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 19
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(304, 400)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 18
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(448, 20)
        Me.txtName.TabIndex = 17
        Me.txtName.Text = ""
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtValue)
        Me.GroupBox1.Controls.Add(Me.cmbKey)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.ucReg)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(456, 340)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'txtValue
        '
        Me.txtValue.Location = New System.Drawing.Point(240, 312)
        Me.txtValue.Name = "txtValue"
        Me.txtValue.Size = New System.Drawing.Size(200, 20)
        Me.txtValue.TabIndex = 3
        Me.txtValue.Text = ""
        '
        'cmbKey
        '
        Me.cmbKey.ItemHeight = 13
        Me.cmbKey.Location = New System.Drawing.Point(16, 312)
        Me.cmbKey.Name = "cmbKey"
        Me.cmbKey.Size = New System.Drawing.Size(208, 21)
        Me.cmbKey.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 296)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 21)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Registry Key"
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(240, 296)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 21)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Registry Value"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'frmTaskSetKey
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(474, 432)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskSetKey"
        Me.Text = "Set Registry Key"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmbKey_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbKey.SelectedIndexChanged
        On Error Resume Next
        Dim oUI As New clsMarsUI
        Dim sRegKey As String
        Dim sParent As String
        Dim sTemp As String

        sTemp = cmbKey.Text

        sParent = ucReg.lblPath.Text.Split("\")(0).ToLower



        Select Case sParent
            Case "hkey_local_machine"
                sRegKey = ucReg.lblPath.Text.ToLower
                sRegKey = sRegKey.Replace("hkey_local_machine\", String.Empty)
                txtValue.Text = oUI.ReadRegistry(Registry.LocalMachine, sRegKey, cmbKey.Text, "")

            Case "hkey_current_user"
                sRegKey = ucReg.lblPath.Text.ToLower
                sRegKey = sRegKey.Replace("hkey_current_user\", String.Empty)
                txtValue.Text = oUI.ReadRegistry(Registry.CurrentUser, sRegKey, cmbKey.Text, "")

        End Select

        ep.SetError(sender, "")
    End Sub

    Private Sub cmbKey_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbKey.DropDown
        Try
            Dim sParent As String
            Dim oReg As RegistryKey
            Dim sRegKey As String

            sParent = ucReg.lblPath.Text.Split("\")(0).ToLower

            cmbKey.Items.Clear()


            Select Case sParent
                Case "hkey_local_machine"
                    sRegKey = ucReg.lblPath.Text.ToLower
                    sRegKey = sRegKey.Replace("hkey_local_machine\", String.Empty)

                    oReg = Registry.LocalMachine.OpenSubKey(sRegKey, False)

                    For Each s As String In oReg.GetValueNames()
                        cmbKey.Items.Add(s)
                    Next
                Case "hkey_current_user"
                    sRegKey = ucReg.lblPath.Text.ToLower
                    sRegKey = sRegKey.Replace("hkey_current_user\", String.Empty)

                    oReg = Registry.LocalMachine.OpenSubKey(sRegKey, False)

                    For Each s As String In oReg.GetValueNames()
                        cmbKey.Items.Add(s)
                    Next
            End Select
            Return
        Catch ex As Exception
            If Err.Number = 91 Then
                Return
            End If

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Sub frmTaskSetKey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub
    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")


        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,ProgramParameters,FileList,OrderID"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'RegistrySetKey'," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & SQLPrepare(ucReg.lblPath.Text) & "'," & _
        "'" & SQLPrepare(cmbKey.Text) & "'," & _
        "'" & SQLPrepare(txtValue.Text) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi

    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtValue.Text = oRs("filelist").Value
            ucReg.lblPath.Text = oRs("programpath").Value
            cmbKey.Text = oRs("programparameters").Value
        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Return


        sVal = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
            "ProgramParameters = '" & SQLPrepare(cmbKey.Text) & "'," & _
            "FileList = '" & SQLPrepare(txtValue.Text) & "'," & _
            "ProgramPath = '" & SQLPrepare(ucReg.lblPath.Text) & "'"

        SQL = "UPDATE Tasks SET " & sVal & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Return
        ElseIf cmbKey.Text.Length = 0 Then
            ep.SetError(cmbKey, "Please select the registry value to set")
            cmbKey.Focus()
            Return
        ElseIf ucReg.lblPath.Text.Length = 0 Then
            ep.SetError(ucReg, "Please select the registry path")
            Return
        End If

        Me.Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub
End Class
