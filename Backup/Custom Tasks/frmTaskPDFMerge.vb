Public Class frmTaskPDFMerge
    Inherits System.Windows.Forms.Form
    Dim UserCancel As Boolean = False
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvFiles As System.Windows.Forms.ListView
    Friend WithEvents cmdAddFiles As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents MyTip As System.Windows.Forms.ToolTip
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents imgFiles As System.Windows.Forms.ImageList
    Friend WithEvents fbd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents chkReplace As System.Windows.Forms.CheckBox
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    Friend WithEvents mnuAdder As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFolder As System.Windows.Forms.MenuItem
    Friend WithEvents fbg As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents grpPDF As System.Windows.Forms.GroupBox
    Friend WithEvents chkPDFSecurity As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkPrint As System.Windows.Forms.CheckBox
    Friend WithEvents chkCopy As System.Windows.Forms.CheckBox
    Friend WithEvents chkEdit As System.Windows.Forms.CheckBox
    Friend WithEvents chkNotes As System.Windows.Forms.CheckBox
    Friend WithEvents chkFill As System.Windows.Forms.CheckBox
    Friend WithEvents chkAccess As System.Windows.Forms.CheckBox
    Friend WithEvents chkAssemble As System.Windows.Forms.CheckBox
    Friend WithEvents chkFullRes As System.Windows.Forms.CheckBox
    Friend WithEvents txtOwnerPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUserPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtWatermark As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtInfoCreated As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtInfoTitle As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoAuthor As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoSubject As System.Windows.Forms.TextBox
    Friend WithEvents txtInfoKeywords As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtInfoProducer As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskPDFMerge))
        Me.cmdOK = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chkReplace = New System.Windows.Forms.CheckBox
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.lsvFiles = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddFiles = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.grpPDF = New System.Windows.Forms.GroupBox
        Me.chkPDFSecurity = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkPrint = New System.Windows.Forms.CheckBox
        Me.chkCopy = New System.Windows.Forms.CheckBox
        Me.chkEdit = New System.Windows.Forms.CheckBox
        Me.chkNotes = New System.Windows.Forms.CheckBox
        Me.chkFill = New System.Windows.Forms.CheckBox
        Me.chkAccess = New System.Windows.Forms.CheckBox
        Me.chkAssemble = New System.Windows.Forms.CheckBox
        Me.chkFullRes = New System.Windows.Forms.CheckBox
        Me.txtOwnerPassword = New System.Windows.Forms.TextBox
        Me.txtUserPassword = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtWatermark = New System.Windows.Forms.TextBox
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.txtInfoCreated = New System.Windows.Forms.DateTimePicker
        Me.Label11 = New System.Windows.Forms.Label
        Me.txtInfoTitle = New System.Windows.Forms.TextBox
        Me.txtInfoAuthor = New System.Windows.Forms.TextBox
        Me.txtInfoSubject = New System.Windows.Forms.TextBox
        Me.txtInfoKeywords = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.txtInfoProducer = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.MyTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.imgFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.fbd = New System.Windows.Forms.FolderBrowserDialog
        Me.sfd = New System.Windows.Forms.SaveFileDialog
        Me.mnuAdder = New System.Windows.Forms.ContextMenu
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuFolder = New System.Windows.Forms.MenuItem
        Me.fbg = New System.Windows.Forms.FolderBrowserDialog
        Me.GroupBox1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.grpPDF.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(344, 424)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 24)
        Me.cmdOK.TabIndex = 20
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkReplace)
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.lsvFiles)
        Me.GroupBox1.Controls.Add(Me.cmdAddFiles)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 236)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        '
        'chkReplace
        '
        Me.chkReplace.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkReplace.Location = New System.Drawing.Point(8, 208)
        Me.chkReplace.Name = "chkReplace"
        Me.chkReplace.Size = New System.Drawing.Size(360, 24)
        Me.chkReplace.TabIndex = 18
        Me.chkReplace.Text = "Replace existing files at destination"
        Me.MyTip.SetToolTip(Me.chkReplace, "Replace existing files at the destination")
        '
        'cmdBrowse
        '
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(416, 184)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 21)
        Me.cmdBrowse.TabIndex = 17
        Me.cmdBrowse.Text = "..."
        Me.cmdBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.MyTip.SetToolTip(Me.cmdBrowse, "Browse to the destination directory")
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 168)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Destination File"
        '
        'txtPath
        '
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 184)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(392, 21)
        Me.txtPath.TabIndex = 15
        Me.MyTip.SetToolTip(Me.txtPath, "Destination to copy the files to")
        '
        'lsvFiles
        '
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.Location = New System.Drawing.Point(8, 17)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(392, 147)
        Me.lsvFiles.TabIndex = 6
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 380
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(416, 17)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 25)
        Me.cmdAddFiles.TabIndex = 4
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(416, 52)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 24)
        Me.cmdRemove.TabIndex = 5
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 26)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(472, 21)
        Me.txtName.TabIndex = 19
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(424, 424)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 24)
        Me.cmdCancel.TabIndex = 21
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 17)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Task Name"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.ItemSize = New System.Drawing.Size(49, 18)
        Me.TabControl1.Location = New System.Drawing.Point(8, 56)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(488, 360)
        Me.TabControl1.TabIndex = 23
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(480, 334)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grpPDF)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(480, 334)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "PDF Security"
        '
        'grpPDF
        '
        Me.grpPDF.Controls.Add(Me.chkPDFSecurity)
        Me.grpPDF.Controls.Add(Me.GroupBox2)
        Me.grpPDF.Controls.Add(Me.Label5)
        Me.grpPDF.Controls.Add(Me.txtWatermark)
        Me.grpPDF.Location = New System.Drawing.Point(8, 8)
        Me.grpPDF.Name = "grpPDF"
        Me.grpPDF.Size = New System.Drawing.Size(392, 312)
        Me.grpPDF.TabIndex = 24
        Me.grpPDF.TabStop = False
        Me.grpPDF.Text = "PDF Options"
        '
        'chkPDFSecurity
        '
        Me.chkPDFSecurity.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPDFSecurity.Location = New System.Drawing.Point(8, 16)
        Me.chkPDFSecurity.Name = "chkPDFSecurity"
        Me.chkPDFSecurity.Size = New System.Drawing.Size(336, 24)
        Me.chkPDFSecurity.TabIndex = 0
        Me.chkPDFSecurity.Text = "Enable PDF Options "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkPrint)
        Me.GroupBox2.Controls.Add(Me.chkCopy)
        Me.GroupBox2.Controls.Add(Me.chkEdit)
        Me.GroupBox2.Controls.Add(Me.chkNotes)
        Me.GroupBox2.Controls.Add(Me.chkFill)
        Me.GroupBox2.Controls.Add(Me.chkAccess)
        Me.GroupBox2.Controls.Add(Me.chkAssemble)
        Me.GroupBox2.Controls.Add(Me.chkFullRes)
        Me.GroupBox2.Controls.Add(Me.txtOwnerPassword)
        Me.GroupBox2.Controls.Add(Me.txtUserPassword)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.GroupBox9)
        Me.GroupBox2.Enabled = False
        Me.GroupBox2.Location = New System.Drawing.Point(8, 40)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(376, 216)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "File Permissions"
        '
        'chkPrint
        '
        Me.chkPrint.Checked = True
        Me.chkPrint.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkPrint.Location = New System.Drawing.Point(8, 88)
        Me.chkPrint.Name = "chkPrint"
        Me.chkPrint.Size = New System.Drawing.Size(176, 24)
        Me.chkPrint.TabIndex = 2
        Me.chkPrint.Text = "Can Print"
        '
        'chkCopy
        '
        Me.chkCopy.Checked = True
        Me.chkCopy.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCopy.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkCopy.Location = New System.Drawing.Point(8, 120)
        Me.chkCopy.Name = "chkCopy"
        Me.chkCopy.Size = New System.Drawing.Size(176, 24)
        Me.chkCopy.TabIndex = 3
        Me.chkCopy.Text = "Can Copy"
        '
        'chkEdit
        '
        Me.chkEdit.Checked = True
        Me.chkEdit.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEdit.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkEdit.Location = New System.Drawing.Point(8, 152)
        Me.chkEdit.Name = "chkEdit"
        Me.chkEdit.Size = New System.Drawing.Size(176, 24)
        Me.chkEdit.TabIndex = 4
        Me.chkEdit.Text = "Can Edit"
        '
        'chkNotes
        '
        Me.chkNotes.Checked = True
        Me.chkNotes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkNotes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNotes.Location = New System.Drawing.Point(184, 152)
        Me.chkNotes.Name = "chkNotes"
        Me.chkNotes.Size = New System.Drawing.Size(176, 24)
        Me.chkNotes.TabIndex = 8
        Me.chkNotes.Text = "Can Add Notes"
        '
        'chkFill
        '
        Me.chkFill.Checked = True
        Me.chkFill.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFill.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFill.Location = New System.Drawing.Point(8, 184)
        Me.chkFill.Name = "chkFill"
        Me.chkFill.Size = New System.Drawing.Size(176, 24)
        Me.chkFill.TabIndex = 5
        Me.chkFill.Text = "Can Fill Fields"
        '
        'chkAccess
        '
        Me.chkAccess.Checked = True
        Me.chkAccess.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAccess.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAccess.Location = New System.Drawing.Point(184, 184)
        Me.chkAccess.Name = "chkAccess"
        Me.chkAccess.Size = New System.Drawing.Size(176, 24)
        Me.chkAccess.TabIndex = 9
        Me.chkAccess.Text = "Can copy accessibility options"
        '
        'chkAssemble
        '
        Me.chkAssemble.Checked = True
        Me.chkAssemble.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssemble.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssemble.Location = New System.Drawing.Point(184, 88)
        Me.chkAssemble.Name = "chkAssemble"
        Me.chkAssemble.Size = New System.Drawing.Size(176, 24)
        Me.chkAssemble.TabIndex = 6
        Me.chkAssemble.Text = "Can Assemble"
        '
        'chkFullRes
        '
        Me.chkFullRes.Checked = True
        Me.chkFullRes.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFullRes.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFullRes.Location = New System.Drawing.Point(184, 120)
        Me.chkFullRes.Name = "chkFullRes"
        Me.chkFullRes.Size = New System.Drawing.Size(176, 24)
        Me.chkFullRes.TabIndex = 7
        Me.chkFullRes.Text = "Can print in full resolution"
        '
        'txtOwnerPassword
        '
        Me.txtOwnerPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtOwnerPassword.Location = New System.Drawing.Point(8, 40)
        Me.txtOwnerPassword.Name = "txtOwnerPassword"
        Me.txtOwnerPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtOwnerPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtOwnerPassword.TabIndex = 0
        '
        'txtUserPassword
        '
        Me.txtUserPassword.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtUserPassword.Location = New System.Drawing.Point(184, 40)
        Me.txtUserPassword.Name = "txtUserPassword"
        Me.txtUserPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtUserPassword.Size = New System.Drawing.Size(160, 21)
        Me.txtUserPassword.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(184, 24)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "User Password"
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(112, 16)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Owner Password"
        '
        'GroupBox9
        '
        Me.GroupBox9.Location = New System.Drawing.Point(8, 72)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(360, 8)
        Me.GroupBox9.TabIndex = 2
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Tag = "3dline"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(16, 264)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(256, 16)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Include the following watermark"
        '
        'txtWatermark
        '
        Me.txtWatermark.Location = New System.Drawing.Point(16, 280)
        Me.txtWatermark.Name = "txtWatermark"
        Me.txtWatermark.Size = New System.Drawing.Size(360, 21)
        Me.txtWatermark.TabIndex = 16
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(480, 334)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "PDF File Summary"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtInfoCreated)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.txtInfoTitle)
        Me.GroupBox3.Controls.Add(Me.txtInfoAuthor)
        Me.GroupBox3.Controls.Add(Me.txtInfoSubject)
        Me.GroupBox3.Controls.Add(Me.txtInfoKeywords)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.txtInfoProducer)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(392, 320)
        Me.GroupBox3.TabIndex = 24
        Me.GroupBox3.TabStop = False
        '
        'txtInfoCreated
        '
        Me.txtInfoCreated.Location = New System.Drawing.Point(136, 120)
        Me.txtInfoCreated.Name = "txtInfoCreated"
        Me.txtInfoCreated.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoCreated.TabIndex = 26
        '
        'Label11
        '
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(8, 24)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(100, 16)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Title"
        '
        'txtInfoTitle
        '
        Me.txtInfoTitle.Location = New System.Drawing.Point(136, 24)
        Me.txtInfoTitle.Name = "txtInfoTitle"
        Me.txtInfoTitle.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoTitle.TabIndex = 16
        '
        'txtInfoAuthor
        '
        Me.txtInfoAuthor.Location = New System.Drawing.Point(136, 48)
        Me.txtInfoAuthor.Name = "txtInfoAuthor"
        Me.txtInfoAuthor.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoAuthor.TabIndex = 17
        '
        'txtInfoSubject
        '
        Me.txtInfoSubject.Location = New System.Drawing.Point(136, 96)
        Me.txtInfoSubject.Name = "txtInfoSubject"
        Me.txtInfoSubject.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoSubject.TabIndex = 25
        '
        'txtInfoKeywords
        '
        Me.txtInfoKeywords.Location = New System.Drawing.Point(136, 144)
        Me.txtInfoKeywords.Multiline = True
        Me.txtInfoKeywords.Name = "txtInfoKeywords"
        Me.txtInfoKeywords.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtInfoKeywords.Size = New System.Drawing.Size(232, 168)
        Me.txtInfoKeywords.TabIndex = 27
        Me.txtInfoKeywords.Tag = "memo"
        '
        'Label12
        '
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(8, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(100, 16)
        Me.Label12.TabIndex = 22
        Me.Label12.Text = "Author"
        '
        'Label13
        '
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(8, 72)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 16)
        Me.Label13.TabIndex = 19
        Me.Label13.Text = "Producer"
        '
        'Label14
        '
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(8, 144)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(112, 16)
        Me.Label14.TabIndex = 18
        Me.Label14.Text = "Keywords/Comments"
        '
        'Label15
        '
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(8, 120)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(100, 16)
        Me.Label15.TabIndex = 21
        Me.Label15.Text = "Date Created"
        '
        'txtInfoProducer
        '
        Me.txtInfoProducer.Location = New System.Drawing.Point(136, 72)
        Me.txtInfoProducer.Name = "txtInfoProducer"
        Me.txtInfoProducer.Size = New System.Drawing.Size(232, 21)
        Me.txtInfoProducer.TabIndex = 20
        '
        'Label16
        '
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(8, 96)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(100, 16)
        Me.Label16.TabIndex = 24
        Me.Label16.Text = "Subject"
        '
        'ofd
        '
        Me.ofd.Filter = "PDF Files|*.pdf|All Files|*.*"
        Me.ofd.Title = "Please select PDF Files to merge"
        '
        'imgFiles
        '
        Me.imgFiles.ImageStream = CType(resources.GetObject("imgFiles.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFiles.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFiles.Images.SetKeyName(0, "")
        '
        'sfd
        '
        Me.sfd.Filter = "PDF Files|*.pdf"
        Me.sfd.OverwritePrompt = False
        Me.sfd.Title = "Please specify the merged file"
        '
        'mnuAdder
        '
        Me.mnuAdder.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuFolder})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.Text = "PDF File(s)"
        '
        'mnuFolder
        '
        Me.mnuFolder.Index = 1
        Me.mnuFolder.Text = "Folder"
        '
        'fbg
        '
        Me.fbg.Description = "Please select the folder containing PDF Files"
        Me.fbg.ShowNewFolderButton = False
        '
        'frmTaskPDFMerge
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.ClientSize = New System.Drawing.Size(506, 456)
        Me.ControlBox = False
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ForeColor = System.Drawing.Color.Navy
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmTaskPDFMerge"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Merge PDF Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.grpPDF.ResumeLayout(False)
        Me.grpPDF.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf lsvFiles.Items.Count = 0 Then
            ep.SetError(lsvFiles, "Please select the file(s) to copy")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf txtPath.Text = "" Then
            ep.SetError(txtPath, "Please specify the merged PDF file name")
            txtPath.Focus()
            Exit Sub
        End If

        Me.Close()
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With sfd
            .AddExtension = True
            .DefaultExt = "pdf"
            .ShowDialog()

            If .FileName.Length = 0 Then Return

            txtPath.Text = .FileName
        End With
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nTaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        For Each lsv In lsvFiles.Items
            sFiles &= lsv.Text & "|"
        Next


        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,ReplaceFiles,OrderID"

        sVals = nTaskID & "," & _
        ScheduleID & "," & _
        "'MergePDF'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        Convert.ToInt32(chkReplace.Checked) & "," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsmarsdata.WriteData(SQL)

        sCols = "OptionID,TaskID,PDFPassword,PDFWaterMark,UserPassword,CanPrint," & _
        "CanCopy,CanEdit,CanNotes,CanFill,CanAccess,CanAssemble,CanPrintFull," & _
        "PDFSecurity," & _
        "InfoTitle,InfoAuthor,InfoSubject,InfoKeyWords,InfoCreated," & _
        "InfoProducer"

        sVals = clsMarsData.CreateDataID("taskoptions", "optionid") & "," & nTaskID & "," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtOwnerPassword.Text)) & "'," & _
        "'" & SQLPrepare(Me.txtWatermark.Text) & "'," & _
        "'" & SQLPrepare(_EncryptDBValue(Me.txtUserPassword.Text)) & "'," & _
        Convert.ToInt32(chkPrint.Checked) & "," & _
        Convert.ToInt32(chkCopy.Checked) & "," & _
        Convert.ToInt32(chkEdit.Checked) & "," & _
        Convert.ToInt32(chkNotes.Checked) & "," & _
        Convert.ToInt32(chkFill.Checked) & "," & _
        Convert.ToInt32(chkAccess.Checked) & "," & _
        Convert.ToInt32(chkAssemble.Checked) & "," & _
        Convert.ToInt32(chkFullRes.Checked) & "," & _
        Convert.ToInt32(chkPDFSecurity.Checked) & "," & _
        "'" & SQLPrepare(Me.txtInfoTitle.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoAuthor.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtInfoKeywords.Text) & "'," & _
        "'" & ConDateTime(txtInfoCreated.Value) & "'," & _
        "'" & SQLPrepare(Me.txtInfoProducer.Text) & "'"

        SQL = "INSERT INTO TaskOptions (" & sCols & ") VALUES (" & sVals & ")"

        clsmarsdata.WriteData(SQL)
        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
        'basi
    End Function

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            For Each sVal In sFiles.Split("|")
                If sval.Length > 0 Then lsvFiles.Items.Add(sVal)
            Next

            txtPath.Text = oRs("programpath").Value

            chkReplace.Checked = oRs("replacefiles").Value

        End If

        oRs.Close()

        Try
            oRs = clsmarsdata.GetData("SELECT * FROM TaskOptions WHERE TaskID = " & nTaskID)

            If Not oRs Is Nothing Then
                If oRs.EOF = False Then

                    With Me
                        .txtOwnerPassword.Text = _DecryptDBValue(IsNull(oRs("pdfpassword").Value))
                        .txtWatermark.Text = IsNull(oRs("pdfwatermark").Value)
                        .txtUserPassword.Text = _DecryptDBValue(IsNull(oRs("userpassword").Value))
                        .chkPrint.Checked = Convert.ToBoolean(oRs("canprint").Value)
                        .chkCopy.Checked = Convert.ToBoolean(oRs("cancopy").Value)
                        .chkEdit.Checked = Convert.ToBoolean(oRs("canedit").Value)
                        .chkNotes.Checked = Convert.ToBoolean(oRs("cannotes").Value)
                        .chkFill.Checked = Convert.ToBoolean(oRs("canfill").Value)
                        .chkAccess.Checked = Convert.ToBoolean(oRs("canaccess").Value)
                        .chkFullRes.Checked = Convert.ToBoolean(oRs("canprintfull").Value)
                        .chkPDFSecurity.Checked = Convert.ToBoolean(oRs("PDFsecurity").Value)
                        .txtInfoTitle.Text = IsNull(oRs("infotitle").Value)
                        .txtInfoAuthor.Text = IsNull(oRs("infoauthor").Value)
                        .txtInfoSubject.Text = IsNull(oRs("infosubject").Value)
                        .txtInfoKeywords.Text = IsNull(oRs("infokeywords").Value)

                        Try
                            .txtInfoCreated.Value = Convert.ToDateTime(oRs("infocreated").Value)
                        Catch ex As Exception
                            .txtInfoCreated.Value = Now
                        End Try

                        .txtInfoProducer.Text = IsNull(oRs("infoproducer").Value)
                    End With
                End If
            End If
        Catch : End Try

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""

        For Each lsv In lsvFiles.Items
            sFiles &= lsv.Text & "|"
        Next

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'," & _
            "ReplaceFiles = " & Convert.ToInt32(chkReplace.Checked)

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        With Me
            SQL = "PDFPassword = '" & SQLPrepare(_EncryptDBValue(.txtOwnerPassword.Text)) & "'," & _
            "PDFWaterMark = '" & SQLPrepare(.txtWatermark.Text) & "'," & _
            "UserPassword = '" & SQLPrepare(_EncryptDBValue(.txtUserPassword.Text)) & "'," & _
            "CanPrint = " & Convert.ToInt32(.chkPrint.Checked) & "," & _
            "CanCopy = " & Convert.ToInt32(.chkCopy.Checked) & "," & _
            "CanEdit = " & Convert.ToInt32(.chkEdit.Checked) & "," & _
            "CanNotes = " & Convert.ToInt32(.chkNotes.Checked) & "," & _
            "CanFill = " & Convert.ToInt32(.chkFill.Checked) & "," & _
            "CanAccess = " & Convert.ToInt32(.chkAccess.Checked) & "," & _
            "CanAssemble = " & Convert.ToInt32(.chkAssemble.Checked) & "," & _
            "CanPrintFull = " & Convert.ToInt32(.chkFullRes.Checked) & "," & _
            "PDFSecurity = " & Convert.ToInt32(.chkPDFSecurity.Checked) & "," & _
            "InfoTitle = '" & SQLPrepare(.txtInfoTitle.Text) & "'," & _
            "InfoAuthor = '" & SQLPrepare(.txtInfoAuthor.Text) & "'," & _
            "InfoSubject = '" & SQLPrepare(.txtInfoSubject.Text) & "'," & _
            "InfoKeyWords = '" & SQLPrepare(.txtInfoKeywords.Text) & "'," & _
            "InfoCreated = '" & ConDateTime(.txtInfoCreated.Value) & "'," & _
            "InfoProducer = '" & SQLPrepare(.txtInfoProducer.Text) & "'"
        End With

        SQL = "UPDATE TaskOptions SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub

    Private Sub cmdAddFiles_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdAddFiles.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        mnuAdder.Show(cmdAddFiles, New Point(e.X, e.Y))

    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            If sFile.Length > 0 Then
                sFile = _CreateUNC(sFile)
                lsv = lsvFiles.Items.Add(sFile)
                lsv.ImageIndex = 0
            End If
        Next

        ep.SetError(lsvFiles, "")
    End Sub

    Private Sub mnuFolder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFolder.Click
        Dim lsv As ListViewItem
        Dim sPath As String

        With fbg
            .ShowDialog()

            If .SelectedPath.Length = 0 Then Return

            sPath = _CreateUNC(.SelectedPath)

            If sPath.EndsWith("\") = False Then
                sPath &= "\"
            End If

            lsv = lsvFiles.Items.Add(sPath & "*.pdf")

            lsv.ImageIndex = 0
        End With
    End Sub

    Private Sub frmTaskPDFMerge_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub chkPDFSecurity_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPDFSecurity.CheckedChanged
        GroupBox2.Enabled = chkPDFSecurity.Checked
    End Sub

   
    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click

    End Sub
End Class
