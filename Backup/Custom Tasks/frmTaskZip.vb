Public Class frmTaskZip
    Inherits frmTaskMaster
    Dim UserCancel As Boolean = True
    Friend WithEvents SuperTooltip1 As DevComponents.DotNetBar.SuperTooltip
    Public m_eventID As Integer = 99999

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsvFiles As System.Windows.Forms.ListView
    Friend WithEvents cmdAddFiles As System.Windows.Forms.Button
    Friend WithEvents cmdRemove As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents imgFiles As System.Windows.Forms.ImageList
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents grpAdvanced As System.Windows.Forms.Panel
    Friend WithEvents chkRecreateFolderStructure As System.Windows.Forms.CheckBox
    Friend WithEvents chkRecursive As System.Windows.Forms.CheckBox
    Friend WithEvents txtSourceFile As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cmdBrowseSource As System.Windows.Forms.Button
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents rbAdvanced As System.Windows.Forms.RadioButton
    Friend WithEvents rbSimple As System.Windows.Forms.RadioButton
    Friend WithEvents sfd As System.Windows.Forms.SaveFileDialog
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskZip))
        Me.cmdOK = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.grpAdvanced = New System.Windows.Forms.Panel
        Me.chkRecreateFolderStructure = New System.Windows.Forms.CheckBox
        Me.chkRecursive = New System.Windows.Forms.CheckBox
        Me.txtSourceFile = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cmdBrowseSource = New System.Windows.Forms.Button
        Me.lsvFiles = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.imgFiles = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdAddFiles = New System.Windows.Forms.Button
        Me.cmdRemove = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.sfd = New System.Windows.Forms.SaveFileDialog
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.rbAdvanced = New System.Windows.Forms.RadioButton
        Me.rbSimple = New System.Windows.Forms.RadioButton
        Me.SuperTooltip1 = New DevComponents.DotNetBar.SuperTooltip
        Me.GroupBox1.SuspendLayout()
        Me.grpAdvanced.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(312, 289)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 49
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grpAdvanced)
        Me.GroupBox1.Controls.Add(Me.lsvFiles)
        Me.GroupBox1.Controls.Add(Me.cmdAddFiles)
        Me.GroupBox1.Controls.Add(Me.cmdRemove)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 148)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select files to compress"
        '
        'grpAdvanced
        '
        Me.grpAdvanced.Controls.Add(Me.chkRecreateFolderStructure)
        Me.grpAdvanced.Controls.Add(Me.chkRecursive)
        Me.grpAdvanced.Controls.Add(Me.txtSourceFile)
        Me.grpAdvanced.Controls.Add(Me.Label8)
        Me.grpAdvanced.Controls.Add(Me.cmdBrowseSource)
        Me.grpAdvanced.Location = New System.Drawing.Point(8, 12)
        Me.grpAdvanced.Name = "grpAdvanced"
        Me.grpAdvanced.Size = New System.Drawing.Size(448, 129)
        Me.grpAdvanced.TabIndex = 0
        Me.grpAdvanced.TabStop = True
        '
        'chkRecreateFolderStructure
        '
        Me.chkRecreateFolderStructure.Enabled = False
        Me.chkRecreateFolderStructure.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecreateFolderStructure.Location = New System.Drawing.Point(14, 79)
        Me.chkRecreateFolderStructure.Name = "chkRecreateFolderStructure"
        Me.chkRecreateFolderStructure.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecreateFolderStructure, New DevComponents.DotNetBar.SuperTooltipInfo("", "", resources.GetString("chkRecreateFolderStructure.SuperTooltip"), My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecreateFolderStructure.TabIndex = 3
        Me.chkRecreateFolderStructure.Text = "Maintain Folder Structure"
        '
        'chkRecursive
        '
        Me.chkRecursive.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkRecursive.Location = New System.Drawing.Point(14, 58)
        Me.chkRecursive.Name = "chkRecursive"
        Me.chkRecursive.Size = New System.Drawing.Size(200, 23)
        Me.SuperTooltip1.SetSuperTooltip(Me.chkRecursive, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "Search the folder and all its subfolders.", My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.chkRecursive.TabIndex = 2
        Me.chkRecursive.Text = "Recursive"
        '
        'txtSourceFile
        '
        Me.txtSourceFile.BackColor = System.Drawing.Color.White
        Me.txtSourceFile.Location = New System.Drawing.Point(14, 32)
        Me.txtSourceFile.Name = "txtSourceFile"
        Me.txtSourceFile.Size = New System.Drawing.Size(319, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtSourceFile, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert Constants and database values.  You can also use w" & _
                    "ildcards e.g. my*abc.*", My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtSourceFile.TabIndex = 0
        Me.txtSourceFile.Tag = "Memo"
        '
        'Label8
        '
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(7, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(96, 16)
        Me.Label8.TabIndex = 57
        Me.Label8.Text = "File(s)"
        '
        'cmdBrowseSource
        '
        Me.cmdBrowseSource.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowseSource.Image = CType(resources.GetObject("cmdBrowseSource.Image"), System.Drawing.Image)
        Me.cmdBrowseSource.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowseSource.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowseSource.Location = New System.Drawing.Point(344, 31)
        Me.cmdBrowseSource.Name = "cmdBrowseSource"
        Me.cmdBrowseSource.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowseSource.TabIndex = 1
        Me.cmdBrowseSource.Text = "..."
        Me.cmdBrowseSource.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lsvFiles
        '
        Me.lsvFiles.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvFiles.ForeColor = System.Drawing.Color.Blue
        Me.lsvFiles.FullRowSelect = True
        Me.lsvFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvFiles.LabelEdit = True
        Me.lsvFiles.LargeImageList = Me.imgFiles
        Me.lsvFiles.Location = New System.Drawing.Point(8, 15)
        Me.lsvFiles.Name = "lsvFiles"
        Me.lsvFiles.Size = New System.Drawing.Size(392, 126)
        Me.lsvFiles.SmallImageList = Me.imgFiles
        Me.lsvFiles.TabIndex = 1
        Me.lsvFiles.UseCompatibleStateImageBehavior = False
        Me.lsvFiles.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Files"
        Me.ColumnHeader1.Width = 380
        '
        'imgFiles
        '
        Me.imgFiles.ImageStream = CType(resources.GetObject("imgFiles.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFiles.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFiles.Images.SetKeyName(0, "")
        '
        'cmdAddFiles
        '
        Me.cmdAddFiles.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddFiles.Image = CType(resources.GetObject("cmdAddFiles.Image"), System.Drawing.Image)
        Me.cmdAddFiles.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddFiles.Location = New System.Drawing.Point(416, 15)
        Me.cmdAddFiles.Name = "cmdAddFiles"
        Me.cmdAddFiles.Size = New System.Drawing.Size(40, 21)
        Me.cmdAddFiles.TabIndex = 0
        '
        'cmdRemove
        '
        Me.cmdRemove.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemove.Image = CType(resources.GetObject("cmdRemove.Image"), System.Drawing.Image)
        Me.cmdRemove.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemove.Location = New System.Drawing.Point(416, 45)
        Me.cmdRemove.Name = "cmdRemove"
        Me.cmdRemove.Size = New System.Drawing.Size(40, 21)
        Me.cmdRemove.TabIndex = 2
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(456, 20)
        Me.txtName.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(400, 289)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 50
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "Task Name"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdBrowse)
        Me.GroupBox2.Controls.Add(Me.txtPath)
        Me.GroupBox2.Location = New System.Drawing.Point(8, 230)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(464, 52)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Output zip file"
        '
        'cmdBrowse
        '
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(416, 24)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 20)
        Me.cmdBrowse.TabIndex = 1
        Me.cmdBrowse.Text = "..."
        Me.cmdBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPath
        '
        Me.txtPath.ForeColor = System.Drawing.Color.Blue
        Me.txtPath.Location = New System.Drawing.Point(8, 22)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(392, 20)
        Me.SuperTooltip1.SetSuperTooltip(Me.txtPath, New DevComponents.DotNetBar.SuperTooltipInfo("", "", "You can right-click and insert constants and database values.", My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0)))
        Me.txtPath.TabIndex = 0
        Me.txtPath.Tag = "Memo"
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'sfd
        '
        Me.sfd.Filter = "Zip Files|*.zip"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'rbAdvanced
        '
        Me.rbAdvanced.AutoSize = True
        Me.rbAdvanced.Location = New System.Drawing.Point(79, 51)
        Me.rbAdvanced.Name = "rbAdvanced"
        Me.rbAdvanced.Size = New System.Drawing.Size(74, 17)
        Me.rbAdvanced.TabIndex = 2
        Me.rbAdvanced.Text = "Advanced"
        Me.rbAdvanced.UseVisualStyleBackColor = True
        '
        'rbSimple
        '
        Me.rbSimple.AutoSize = True
        Me.rbSimple.Location = New System.Drawing.Point(8, 51)
        Me.rbSimple.Name = "rbSimple"
        Me.rbSimple.Size = New System.Drawing.Size(56, 17)
        Me.rbSimple.TabIndex = 1
        Me.rbSimple.TabStop = True
        Me.rbSimple.Text = "Simple"
        Me.rbSimple.UseVisualStyleBackColor = True
        '
        'SuperTooltip1
        '
        Me.SuperTooltip1.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperTooltip1.DefaultTooltipSettings = New DevComponents.DotNetBar.SuperTooltipInfo("", "", "", My.Resources.Resources.lightbulb_on, Nothing, DevComponents.DotNetBar.eTooltipColor.Lemon, False, False, New System.Drawing.Size(0, 0))
        Me.SuperTooltip1.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperTooltip1.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'frmTaskZip
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(479, 315)
        Me.ControlBox = True
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.rbAdvanced)
        Me.Controls.Add(Me.rbSimple)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MinimizeBox = False
        Me.Name = "frmTaskZip"
        Me.Text = "Zip Files"
        Me.GroupBox1.ResumeLayout(False)
        Me.grpAdvanced.ResumeLayout(False)
        Me.grpAdvanced.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdAddFiles_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddFiles.Click
        Dim sFiles() As String
        Dim sFile As String
        Dim lsv As ListViewItem

        With ofd
            .Multiselect = True
            .ShowDialog()
        End With

        sFiles = ofd.FileNames

        For Each sFile In sFiles
            lsv = lsvFiles.Items.Add(_CreateUNC(sFile))
            lsv.ImageIndex = 0
        Next

        ep.SetError(lsvFiles, "")
    End Sub

    Private Sub cmdRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemove.Click
        Dim lsv As ListViewItem

        For Each lsv In lsvFiles.SelectedItems
            lsv.Remove()
        Next
    End Sub

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        With sfd
            .CheckPathExists = True
            .OverwritePrompt = True
            .Title = "Select the destination zip file"
            .ShowDialog()

            If .FileName.Length = 0 Then Return

            txtPath.Text = _CreateUNC(.FileName)
        End With

    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter the name of the task")
            txtName.Focus()
            Exit Sub
        ElseIf rbSimple.Checked = True And lsvFiles.Items.Count = 0 Then
            ep.SetError(lsvFiles, "Please select the file(s) to zip")
            cmdAddFiles.Focus()
            Exit Sub
        ElseIf rbAdvanced.Checked = True And txtSourceFile.Text = "" Then
            ep.SetError(txtSourceFile, "Please select the file(s) to zip")
            txtSourceFile.Focus()
            Exit Sub
        ElseIf txtPath.Text.Length = 0 Then
            ep.SetError(txtPath, "Please select the destination zip file")
            txtPath.Focus()
            Return
        End If
        UserCancel = False
        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
   Optional ByVal ShowAfter As Boolean = True)
        rbSimple.Checked = True
        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sFiles As String
        Dim lsv As ListViewItem
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,OrderID,CC,Bcc"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'ZipFiles'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtPath.Text.Replace("'", "''") & "'," & _
        "'" & sFiles.Replace("'", "''") & "'," & _
        oTask.GetOrderID(ScheduleID) & "," & _
        Convert.ToInt32(chkRecursive.Checked) & "," & _
        Convert.ToInt32(chkRecreateFolderStructure.Checked)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Function

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String
        Dim sFiles As String
        Dim lsv As ListViewItem

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value

            sFiles = oRs("filelist").Value

            If sFiles.Contains("|") = True Then

                Me.rbSimple.Checked = True

                For Each sVal In sFiles.Split("|")
                    If sVal.Length > 0 Then lsvFiles.Items.Add(sVal)
                Next
            Else
                txtSourceFile.Text = sFiles
                Me.rbAdvanced.Checked = True
            End If

            txtPath.Text = oRs("programpath").Value
            chkRecursive.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("CC").Value), 0))))
            chkRecreateFolderStructure.Checked = Convert.ToBoolean(Convert.ToInt32((IsNull((oRs("Bcc").Value), 0))))



        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sFiles = ""
        If Me.rbSimple.Checked = True Then

            For Each lsv In lsvFiles.Items
                sFiles &= lsv.Text & "|"
            Next
        Else
            sFiles = SQLPrepare(txtSourceFile.Text)
        End If


        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtPath.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & sFiles.Replace("'", "''") & "'" & "," & _
            "CC = '" & Convert.ToInt32(chkRecursive.Checked) & "'," & _
            "Bcc = '" & Convert.ToInt32(chkRecreateFolderStructure.Checked) & "'"


        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub

    Private Sub frmTaskZip_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Me.MinimizeBox = False
        Me.MaximizeBox = False
        Me.HelpButton = False
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.FixedDialog
        Me.ControlBox = True
        txtName.Focus()
        txtSourceFile.ContextMenu = Me.mnuInserter
        txtPath.ContextMenu = Me.mnuInserter

    End Sub


    Private Sub rbAdvanced_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbAdvanced.CheckedChanged
        If rbAdvanced.Checked = True Then
            rbSimple.Checked = False
            lsvFiles.Enabled = False
            cmdAddFiles.Enabled = False
            cmdRemove.Enabled = False
            grpAdvanced.Enabled = True
            grpAdvanced.Visible = True
        End If
    End Sub

    Private Sub rbSimple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbSimple.CheckedChanged
        If rbSimple.Checked = True Then
            rbAdvanced.Checked = False
            lsvFiles.Enabled = True
            cmdAddFiles.Enabled = True
            cmdRemove.Enabled = True
            grpAdvanced.Enabled = False
            grpAdvanced.Visible = False
        End If
    End Sub

    Private Sub cmdBrowseSource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowseSource.Click
        With ofd
            .Multiselect = False
            .ShowDialog()
        End With

        txtSourceFile.Text = _CreateUNC(ofd.FileName)
    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub chkRecursive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRecursive.CheckedChanged
        If chkRecursive.Checked = True Then
            Me.chkRecreateFolderStructure.Enabled = True
            Me.chkRecreateFolderStructure.Checked = True
        Else
            Me.chkRecreateFolderStructure.Checked = False
            Me.chkRecreateFolderStructure.Enabled = False
        End If
    End Sub
End Class
