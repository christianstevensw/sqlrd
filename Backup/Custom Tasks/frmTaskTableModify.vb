Public Class frmTaskTableModify
    Inherits sqlrd.frmTaskMaster
    Dim nStep As Integer = 1
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmbTables As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbColumnType As System.Windows.Forms.ComboBox
    Friend WithEvents txtColumnSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmbModifyType As System.Windows.Forms.ComboBox
    Friend WithEvents cmbColumnName As System.Windows.Forms.ComboBox
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents cmdSkip As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskTableModify))
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Page3 = New System.Windows.Forms.GroupBox
        Me.txtFinal = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Page2 = New System.Windows.Forms.GroupBox
        Me.txtColumnSize = New System.Windows.Forms.NumericUpDown
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbModifyType = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmbTables = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmbColumnName = New System.Windows.Forms.ComboBox
        Me.cmbColumnType = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Page1 = New System.Windows.Forms.GroupBox
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdBack = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdSkip = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Page1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(8, 15)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(416, 20)
        Me.txtName.TabIndex = 28
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 37)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 253)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.txtFinal)
        Me.Page3.Controls.Add(Me.Label7)
        Me.Page3.Location = New System.Drawing.Point(16, 7)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(416, 208)
        Me.Page3.TabIndex = 8
        Me.Page3.TabStop = False
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(8, 30)
        Me.txtFinal.Multiline = True
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFinal.Size = New System.Drawing.Size(392, 171)
        Me.txtFinal.TabIndex = 5
        Me.txtFinal.Tag = "memo"
        '
        'Label7
        '
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(8, 15)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(136, 15)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "Finalised Query"
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.txtColumnSize)
        Me.Page2.Controls.Add(Me.Label4)
        Me.Page2.Controls.Add(Me.cmbModifyType)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.cmbTables)
        Me.Page2.Controls.Add(Me.Label1)
        Me.Page2.Controls.Add(Me.cmbColumnName)
        Me.Page2.Controls.Add(Me.cmbColumnType)
        Me.Page2.Controls.Add(Me.Label5)
        Me.Page2.Controls.Add(Me.Label6)
        Me.Page2.Location = New System.Drawing.Point(16, 7)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(416, 208)
        Me.Page2.TabIndex = 7
        Me.Page2.TabStop = False
        '
        'txtColumnSize
        '
        Me.txtColumnSize.Enabled = False
        Me.txtColumnSize.Location = New System.Drawing.Point(320, 119)
        Me.txtColumnSize.Name = "txtColumnSize"
        Me.txtColumnSize.Size = New System.Drawing.Size(88, 20)
        Me.txtColumnSize.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(8, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(88, 15)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Column Name"
        '
        'cmbModifyType
        '
        Me.cmbModifyType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbModifyType.Enabled = False
        Me.cmbModifyType.ItemHeight = 13
        Me.cmbModifyType.Items.AddRange(New Object() {"ADD COLUMN", "DROP COLUMN"})
        Me.cmbModifyType.Location = New System.Drawing.Point(8, 74)
        Me.cmbModifyType.Name = "cmbModifyType"
        Me.cmbModifyType.Size = New System.Drawing.Size(216, 21)
        Me.cmbModifyType.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(216, 22)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "How would like to modify the table"
        '
        'cmbTables
        '
        Me.cmbTables.ItemHeight = 13
        Me.cmbTables.Location = New System.Drawing.Point(8, 30)
        Me.cmbTables.Name = "cmbTables"
        Me.cmbTables.Size = New System.Drawing.Size(216, 21)
        Me.cmbTables.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(168, 21)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Please select the table to modify"
        '
        'cmbColumnName
        '
        Me.cmbColumnName.ItemHeight = 13
        Me.cmbColumnName.Items.AddRange(New Object() {"ADD COLUMN", "DROP COLUMN"})
        Me.cmbColumnName.Location = New System.Drawing.Point(8, 119)
        Me.cmbColumnName.Name = "cmbColumnName"
        Me.cmbColumnName.Size = New System.Drawing.Size(216, 21)
        Me.cmbColumnName.TabIndex = 3
        '
        'cmbColumnType
        '
        Me.cmbColumnType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbColumnType.Enabled = False
        Me.cmbColumnType.ItemHeight = 13
        Me.cmbColumnType.Items.AddRange(New Object() {"NUMBER", "DECIMAL", "TEXT", "MEMO", "DATETIME"})
        Me.cmbColumnType.Location = New System.Drawing.Point(232, 119)
        Me.cmbColumnType.Name = "cmbColumnType"
        Me.cmbColumnType.Size = New System.Drawing.Size(80, 21)
        Me.cmbColumnType.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(232, 104)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(72, 15)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Type"
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(320, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(72, 15)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Size"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(16, 7)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(416, 208)
        Me.Page1.TabIndex = 2
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 119)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 7)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(392, 112)
        Me.UcDSN.TabIndex = 0
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(16, 223)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 21)
        Me.cmdBack.TabIndex = 6
        Me.cmdBack.Text = "&Back"
        Me.cmdBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(376, 223)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 21)
        Me.cmdNext.TabIndex = 5
        Me.cmdNext.Text = "&Next"
        Me.cmdNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(373, 297)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 31
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(288, 297)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 30
        Me.cmdOK.Text = "&OK"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdSkip
        '
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSkip.Image = CType(resources.GetObject("cmdSkip.Image"), System.Drawing.Image)
        Me.cmdSkip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(8, 297)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 21)
        Me.cmdSkip.TabIndex = 32
        Me.cmdSkip.Text = "&Skip"
        '
        'frmTaskTableModify
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(458, 326)
        Me.Controls.Add(Me.cmdSkip)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label3)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskTableModify"
        Me.Text = "Modify Table"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page3.PerformLayout()
        Me.Page2.ResumeLayout(False)
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Page1.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmbModifyType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbModifyType.SelectedIndexChanged
        If cmbModifyType.Text = "ADD COLUMN" Then
            cmbColumnType.Enabled = True
        Else
            cmbColumnType.Enabled = False
            txtColumnSize.Enabled = False
        End If
    End Sub

    

    Private Sub cmbColumnName_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbColumnName.DropDown
        If cmbModifyType.Text = "DROP COLUMN" Then
            Dim oData As clsMarsData = New clsMarsData

            oData.GetColumns(cmbColumnName, UcDSN.cmbDSN.Text, cmbTables.Text, UcDSN.txtUserID.Text, UcDSN.txtPassword.Text)
        Else
            cmbColumnName.Items.Clear()
        End If
    End Sub

    Private Sub cmbColumnType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumnType.SelectedIndexChanged
        If cmbColumnType.Text = "TEXT" Then
            txtColumnSize.Enabled = True
        Else
            txtColumnSize.Enabled = False
        End If

    End Sub

    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
                Select Case cmbModifyType.Text
                    Case "ADD COLUMN"
                        If cmbColumnName.Text = "" Then
                            ep.SetError(cmbColumnName, "Please enter the column name")
                            cmbColumnName.Focus()
                            Return
                        ElseIf cmbColumnType.Text = "" Then
                            ep.SetError(cmbColumnType, "Please select the column type")
                            cmbColumnType.Focus()
                            Return
                        ElseIf txtColumnSize.Text = "0" And cmbColumnType.Text = "TEXT" Then
                            ep.SetError(txtColumnSize, "Please select the column size")
                            txtColumnSize.Focus()
                            Return
                        End If
                    Case "DROP COLUMN"
                        If cmbColumnName.Text = "" Then
                            ep.SetError(cmbColumnName, "Please enter the column name")
                            cmbColumnName.Focus()
                            Return
                        End If
                End Select

                Page3.BringToFront()

                cmdNext.Enabled = False
                cmdOK.Enabled = True

                If cmbModifyType.Text = "ADD COLUMN" Then
                    Dim sDataType As String

                    Select Case cmbColumnType.Text
                        Case "TEXT"
                            sDataType = "VARCHAR"
                        Case "NUMBER"
                            sDataType = "INTEGER"
                        Case "DECIMAL"
                            sDataType = "FLOAT"
                        Case "MEMO"
                            sDataType = "TEXT"
                    End Select

                    If sDataType = "VARCHAR" Then
                        txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " ADD COLUMN " & cmbColumnName.Text & " " & sDataType & "(" & txtColumnSize.Text & ")"
                    Else
                        txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " ADD COLUMN " & cmbColumnName.Text & " " & sDataType
                    End If
                Else
                    txtFinal.Text = "ALTER TABLE " & cmbTables.Text & " DROP COLUMN " & cmbColumnName.Text
                End If

                txtFinal.Text = txtFinal.Text.ToUpper

        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Select Case nStep
            Case 3
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub
    Public Sub AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sWhere As String
        Dim sValues As String
        Dim sModifyType As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'Connection String -> ProgramParameters
        'Table Name -> ProgramPath 
        'Modify Type -> WindowStyle
        'Columns to add/delete (pipe delimited) -> SendTo
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,WindowStyle,SendTo,Msg,OrderID"

        sValues = cmbColumnName.Text & "|" & cmbColumnType.Text & "|" & txtColumnSize.Text & "|"

        If cmbModifyType.Text = "ADD COLUMN" Then
            sModifyType = "DBModifyTable_Add"
        Else
            sModifyType = "DBModifyTable_Drop"
        End If

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'" & sModifyType & "'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(cmbTables.Text) & "'," & _
            "'" & SQLPrepare(cmbModifyType.Text) & "'," & _
            "'" & SQLPrepare(sValues) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsmarsdata.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String
        Dim sWhere As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        'Connection String -> ProgramParameters
        'Table Name -> ProgramPath 
        'Modify Type -> WindowStyle
        'Columns to add/delete (pipe delimited) -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            cmbTables.Text = oRs("programpath").Value

            sInserts = oRs("sendto").Value

            cmbColumnName.Text = GetDelimitedWord(sInserts, 1, "|")

            cmbColumnType.Text = GetDelimitedWord(sInserts, 2, "|")

            txtColumnSize.Text = GetDelimitedWord(sInserts, 3, "|")


            cmbModifyType.Text = oRs("windowstyle").Value

            txtFinal.Text = oRs("msg").Value

            oRs.Close()

            Me.ShowDialog()

            If UserCancel = True Then Return

            sInserts = cmbColumnName.Text & "|" & cmbColumnType.Text & "|" & txtColumnSize.Text & "|"

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            'Connection String -> ProgramParameters
            'Table Name -> ProgramPath 
            'Modify Type -> WindowStyle
            'Columns to add/delete (pipe delimited) -> SendTo
            'Final SQL -> Msg

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "ProgramPath = '" & SQLPrepare(cmbTables.Text) & "'," & _
                "WindowStyle = '" & SQLPrepare(cmbModifyType.Text) & "'," & _
                "SendTo = '" & SQLPrepare(sInserts) & "'," & _
                "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsmarsdata.WriteData(SQL)

            'for editing a task, set its run type
            '_Delay(2)

            Dim oRunWhen As New frmTaskRunWhen

            If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
        End If
    End Sub

    Private Sub frmTaskTableModify_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page1.BringToFront()
        FormatForWinXP(Me)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdNext.Enabled = True

            Dim oData As clsMarsData = New clsMarsData

            oData.GetTables(cmbTables, UcDSN.cmbDSN.Text, UcDSN.txtUserID.Text, _
                UcDSN.txtPassword.Text)
            cmdSkip.Enabled = True
        Else
            cmdNext.Enabled = False
        End If
    End Sub



    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter the name for the task")
            Return
        ElseIf txtFinal.Text = "" Then
            ep.SetError(txtFinal, "Please select the table to modify")
            Return
        End If

        Me.Close()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, "")
    End Sub

    Private Sub txtFinal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFinal.TextChanged
        ep.SetError(txtFinal, "")
    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page3.BringToFront()
        cmdNext.Enabled = False
        nStep = 3
        cmdOK.Enabled = True
    End Sub
End Class
