Public Class frmTaskRunSchedule
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents tvMars As System.Windows.Forms.TreeView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskRunSchedule))
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tvMars = New System.Windows.Forms.TreeView
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(392, 371)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 22)
        Me.cmdCancel.TabIndex = 19
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(304, 371)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 22)
        Me.cmdOK.TabIndex = 18
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(440, 20)
        Me.txtName.TabIndex = 17
        Me.txtName.Text = ""
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.tvMars)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 45)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(456, 319)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(8, 290)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(432, 20)
        Me.txtPath.TabIndex = 2
        Me.txtPath.Text = ""
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(440, 15)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Please select the schedule to run"
        '
        'tvMars
        '
        Me.tvMars.ImageList = Me.imgFolders
        Me.tvMars.Indent = 19
        Me.tvMars.ItemHeight = 16
        Me.tvMars.Location = New System.Drawing.Point(8, 30)
        Me.tvMars.Name = "tvMars"
        Me.tvMars.Size = New System.Drawing.Size(432, 252)
        Me.tvMars.TabIndex = 0
        '
        'imgFolders
        '
        Me.imgFolders.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgFolders.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'frmTaskRunSchedule
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(474, 401)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskRunSchedule"
        Me.Text = "Run a schedule"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskRunSchedule_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub
    Public Sub AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim oUI As New clsMarsUI

        AppStatus(True)
        oUI.BuildTree(tvMars, True)
        tvMars.Nodes(0).Expand()
        tvMars.SelectedNode = tvMars.Nodes(0)
        AppStatus(False)

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sObject As String
        Dim TaskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        Dim nID As Integer

        'Subject -> schedule type
        'PauseInt -> Report or Package ID
        'ProgramPath -> Schedule Name

        sCols = "TaskID, ScheduleID,TaskType,TaskName,Subject,PauseInt,ProgramPath,OrderID"

        sObject = GetDelimitedWord(tvMars.SelectedNode.Tag, 1, ":")
        nID = GetDelimitedWord(tvMars.SelectedNode.Tag, 2, ":")

        sVals = TaskID & "," & _
        ScheduleID & "," & _
        "'RunSchedule'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & sObject & "'," & _
        "" & nID & "," & _
        "'" & SQLPrepare(tvMars.SelectedNode.FullPath) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(TaskID)
        'basi
    End Sub
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oUI As New clsMarsUI
        Dim nID As Integer
        Dim sObject As String
        Dim sPath As String


        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        oUI.BuildTree(tvMars, True)

        If Not oRs Is Nothing And oRs.EOF = False Then

            nID = oRs("pauseint").Value
            sObject = oRs("subject").Value

            txtName.Text = oRs("taskname").Value

            sPath = oRs("programpath").Value

            Try
                txtPath.Text = tvMars.SelectedNode.FullPath
            Catch
                txtPath.Text = sPath
            End Try

            oUI.FindNode(sObject & ":" & nID, tvMars, tvMars.Nodes(0))
        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        sObject = GetDelimitedWord(tvMars.SelectedNode.Tag, 1, ":")
        nID = GetDelimitedWord(tvMars.SelectedNode.Tag, 2, ":")

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & SQLPrepare(tvMars.SelectedNode.FullPath) & "'," & _
            "Subject = '" & sObject & "'," & _
            "PauseInt = " & nID

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        'for editing a task, set its run type


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If tvMars.SelectedNode Is Nothing Then
            ep.SetError(tvMars, "Please select the schedule to execute")
            tvMars.Focus()
            Return
        ElseIf txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please provide a name for the task")
            txtName.Focus()
            Return
        ElseIf tvMars.SelectedNode.Tag.split(":")(0).ToLower = "folder" Or _
        tvMars.SelectedNode.Tag.split(":")(0).ToLower = "desktop" Then
            ep.SetError(tvMars, "Please select the schedule to execute")
            tvMars.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Private Sub tvMars_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles tvMars.AfterSelect
        ep.SetError(tvMars, "")
        txtPath.Text = tvMars.SelectedNode.FullPath
        tvMars.SelectedNode.Expand()
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, "")
    End Sub

    
End Class
