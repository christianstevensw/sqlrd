Public Class frmTaskFolderMove
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Friend WithEvents optMove As System.Windows.Forms.RadioButton
    Friend WithEvents optRename As System.Windows.Forms.RadioButton
    Public m_eventID As Integer = 99999
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdBrowse As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents fbd As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtOld As System.Windows.Forms.TextBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents txtNew As System.Windows.Forms.TextBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskFolderMove))
        Me.cmdBrowse = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmdOK = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.txtOld = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtNew = New System.Windows.Forms.TextBox
        Me.fbd = New System.Windows.Forms.FolderBrowserDialog
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.optRename = New System.Windows.Forms.RadioButton
        Me.optMove = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdBrowse
        '
        Me.cmdBrowse.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBrowse.Image = CType(resources.GetObject("cmdBrowse.Image"), System.Drawing.Image)
        Me.cmdBrowse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBrowse.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBrowse.Location = New System.Drawing.Point(416, 19)
        Me.cmdBrowse.Name = "cmdBrowse"
        Me.cmdBrowse.Size = New System.Drawing.Size(40, 19)
        Me.cmdBrowse.TabIndex = 20
        Me.cmdBrowse.Text = "..."
        Me.cmdBrowse.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(464, 20)
        Me.txtName.TabIndex = 14
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(312, 192)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 16
        Me.cmdOK.Text = "&OK"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "Task Name"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 4)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 15)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Select Folder"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(397, 192)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 17
        Me.cmdCancel.Text = "&Cancel"
        '
        'txtOld
        '
        Me.txtOld.ForeColor = System.Drawing.Color.Blue
        Me.txtOld.Location = New System.Drawing.Point(8, 19)
        Me.txtOld.Name = "txtOld"
        Me.txtOld.Size = New System.Drawing.Size(392, 20)
        Me.txtOld.TabIndex = 15
        Me.txtOld.Tag = "Memo"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdBrowse)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtOld)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtNew)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 81)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 101)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button1.Image = CType(resources.GetObject("Button1.Image"), System.Drawing.Image)
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button1.Location = New System.Drawing.Point(416, 63)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(40, 20)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "..."
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "Destination  Folder"
        '
        'txtNew
        '
        Me.txtNew.ForeColor = System.Drawing.Color.Blue
        Me.txtNew.Location = New System.Drawing.Point(8, 63)
        Me.txtNew.Name = "txtNew"
        Me.txtNew.Size = New System.Drawing.Size(392, 20)
        Me.txtNew.TabIndex = 15
        Me.txtNew.Tag = "Memo"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'optRename
        '
        Me.optRename.AutoSize = True
        Me.optRename.Location = New System.Drawing.Point(83, 55)
        Me.optRename.Name = "optRename"
        Me.optRename.Size = New System.Drawing.Size(65, 17)
        Me.optRename.TabIndex = 22
        Me.optRename.TabStop = True
        Me.optRename.Text = "Rename"
        Me.optRename.UseVisualStyleBackColor = True
        '
        'optMove
        '
        Me.optMove.AutoSize = True
        Me.optMove.Location = New System.Drawing.Point(14, 55)
        Me.optMove.Name = "optMove"
        Me.optMove.Size = New System.Drawing.Size(52, 17)
        Me.optMove.TabIndex = 23
        Me.optMove.TabStop = True
        Me.optMove.Text = "Move"
        Me.optMove.UseVisualStyleBackColor = True
        '
        'frmTaskFolderMove
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(482, 224)
        Me.Controls.Add(Me.optMove)
        Me.Controls.Add(Me.optRename)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskFolderMove"
        Me.Text = "Rename/Move Folder"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBrowse.Click
        fbd.Description = "Select the folder to move"
        fbd.ShowDialog()

        Dim sTemp As String = fbd.SelectedPath

        If sTemp = "" Then Exit Sub

        If sTemp.Substring(sTemp.Length - 1, 1) <> "\" Then sTemp &= "\"

        txtOld.Text = _CreateUNC(sTemp)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        fbd.Description = "Select the destination folder"
        fbd.ShowDialog()

        Dim sTemp As String = fbd.SelectedPath

        If sTemp = "" Then Exit Sub

        If sTemp.Substring(sTemp.Length - 1, 1) <> "\" Then sTemp &= "\"

        txtNew.Text = _CreateUNC(sTemp)
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf txtOld.Text = "" Then
            ep.SetError(txtOld, "Please select the folder to move")
            txtOld.Focus()
            Exit Sub
        ElseIf txtNew.Text = "" Then
            ep.SetError(txtNew, "Please specify the destination folder")
            txtNew.Focus()
            Exit Sub
        ElseIf optRename.Checked = True And txtNew.Text.Contains("\") = True Then
            ep.SetError(txtNew, "Please enter a name, not a path")
            txtNew.Focus()
            Exit Sub
        End If
        If optMove.Checked = True And txtNew.Text.Contains("\") = True And txtNew.Text.EndsWith("\") = False Then
            txtNew.Text = txtNew.Text & "\"
        End If

        Me.Close()

    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Public Sub AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'MoveFolder'," & _
            "'" & txtName.Text.Replace("'", "''") & "'," & _
            "'" & txtOld.Text.Replace("'", "''") & "'," & _
            "'" & txtNew.Text.Replace("'", "''") & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi

    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtOld.Text = oRs("programpath").Value
            txtNew.Text = oRs("filelist").Value
        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & txtOld.Text.Replace("'", "''") & "'," & _
            "Filelist = '" & txtNew.Text.Replace("'", "''") & "'"

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsmarsdata.WriteData(SQL)

        'for editing a task, set its run type
        '_Delay(2)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub
    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtOld_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtOld.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtNew_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNew.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub frmTaskFolderMove_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Me.txtOld.ContextMenu = Me.mnuInserter
        Me.txtNew.ContextMenu = Me.mnuInserter
        If Me.optMove.Checked = False And Me.optRename.Checked = False Then
            Me.optMove.Checked = True
            Me.Button1.Enabled = True
        End If
        If Me.txtNew.Text.Contains("\") = True Then
            Me.optMove.Checked = True
            Me.optRename.Checked = False
            Me.Button1.Enabled = True
        End If
        If Me.txtNew.Text.Contains("\") = False Then
            Me.optRename.Checked = True
            Me.optMove.Checked = False
            Me.Button1.Enabled = False
        End If
        If Me.optRename.Checked = True Then
            Me.Button1.Enabled = False
            Me.Label3.Text = "New Name"
        End If


    End Sub
    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Undo()
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Cut()
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Copy()
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.Paste()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = ""
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        On Error Resume Next

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectAll()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        Dim box As TextBox

        box = CType(Me.ActiveControl, TextBox)

        box.SelectedText = oData._GetDataItem(Me.m_eventID)
    End Sub

    Private Sub optRename_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optRename.Click
        If Me.optRename.Checked = True Then
            Me.Button1.Enabled = False
            Me.Label3.Text = "New Name"

        End If
    End Sub

    Private Sub optMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles optMove.Click
        If Me.optMove.Checked = True Then
            Me.Button1.Enabled = True
            Me.Label3.Text = "Destination Folder"

        End If
    End Sub
End Class
