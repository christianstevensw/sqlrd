Public Class frmTaskAddKey
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ucReg As sqlrd.ucRegistryBrowser
    Friend WithEvents txtKey As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents MyTip As System.Windows.Forms.ToolTip
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskAddKey))
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtKey = New System.Windows.Forms.TextBox
        Me.ucReg = New sqlrd.ucRegistryBrowser
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.MyTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(448, 20)
        Me.txtName.TabIndex = 13
        Me.txtName.Text = ""
        Me.MyTip.SetToolTip(Me.txtName, "Please enter the name for the task")
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(400, 400)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 15
        Me.cmdCancel.Text = "&Cancel"
        Me.MyTip.SetToolTip(Me.cmdCancel, "Exit without saving the task")
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(312, 400)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 14
        Me.cmdOK.Text = "&OK"
        Me.MyTip.SetToolTip(Me.cmdOK, "Save this task")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtKey)
        Me.GroupBox1.Controls.Add(Me.ucReg)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(464, 340)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Please select the parent key"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(16, 296)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(240, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "New Key Name"
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(16, 312)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(424, 20)
        Me.txtKey.TabIndex = 1
        Me.txtKey.Text = ""
        Me.MyTip.SetToolTip(Me.txtKey, "Enter the new registry key name")
        '
        'ucReg
        '
        Me.ucReg.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.ucReg.Location = New System.Drawing.Point(8, 15)
        Me.ucReg.Name = "ucReg"
        Me.ucReg.Size = New System.Drawing.Size(448, 273)
        Me.ucReg.TabIndex = 0
        Me.MyTip.SetToolTip(Me.ucReg, "Select the registry keys from the drill down")
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'frmTaskAddKey
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(482, 432)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskAddKey"
        Me.Text = "Create Registry Key"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskAddKey_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramPath,FileList,OrderID"

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'RegistryCreateKey'," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & SQLPrepare(ucReg.lblPath.Text) & "'," & _
        "'" & SQLPrepare(txtKey.Text) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi

    End Sub
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sVal As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtKey.Text = oRs("filelist").Value
            ucReg.lblPath.Text = oRs("programpath").Value
        End If

        oRs.Close()

        Me.ShowDialog()

        If UserCancel = True Then Return


        sVal = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
            "FileList = '" & SQLPrepare(txtKey.Text) & "'," & _
            "ProgramPath = '" & SQLPrepare(ucReg.lblPath.Text) & "'"

        SQL = "UPDATE Tasks SET " & sVal & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type


        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub
    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please provide the name for the task")
            txtName.Focus()
            Return
        ElseIf txtKey.Text.Length = 0 Then
            ep.SetError(txtKey, "Please enter the name of the new key")
            txtKey.Focus()
            Return
        ElseIf ucReg.tvRegistry.SelectedNode Is Nothing Then
            ep.SetError(ucReg, "Please select the node to add the key into")
            Return
        ElseIf ucReg.tvRegistry.SelectedNode.Text = "My Computer" Then
            ep.SetError(ucReg, "Please select a valid key")
            Return

        End If

        Me.Close()
    End Sub
End Class
