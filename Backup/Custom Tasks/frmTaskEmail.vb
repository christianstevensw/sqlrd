Public Class frmTaskEmail
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Dim oMsg As clsMarsMessaging = New clsMarsMessaging
    Dim oField As TextBox
    Dim sField As String = "to"
    Friend WithEvents mnuConstants As System.Windows.Forms.MenuItem
    Dim eventBased As Boolean = False
    Friend WithEvents cmbSMTPServer As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents pnSMTP As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents SuperT As DevComponents.DotNetBar.SuperTooltip
    Friend WithEvents grpSender As System.Windows.Forms.GroupBox
    Friend WithEvents txtSenderAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtSenderName As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Dim eventID As Integer = 99999

    Public Property m_eventBased() As Boolean
        Get
            Return eventBased
        End Get
        Set(ByVal value As Boolean)
            eventBased = value
        End Set
    End Property

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdTo As System.Windows.Forms.Button
    Friend WithEvents txtTo As System.Windows.Forms.TextBox
    Friend WithEvents txtCC As System.Windows.Forms.TextBox
    Friend WithEvents cmdCC As System.Windows.Forms.Button
    Friend WithEvents txtBcc As System.Windows.Forms.TextBox
    Friend WithEvents cmdBCC As System.Windows.Forms.Button
    Friend WithEvents cmdAttach As System.Windows.Forms.Button
    Friend WithEvents txtAttach As System.Windows.Forms.TextBox
    Friend WithEvents txtMsg As System.Windows.Forms.TextBox
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents ofd As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtSubject As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents mnuContacts As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuMAPI As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMARS As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDB As System.Windows.Forms.MenuItem
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents cmdInsert As System.Windows.Forms.Button
    Friend WithEvents mnuSubInsert As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem15 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem16 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem18 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuMsg As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSig As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDefSubject As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents cmbMailFormat As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Speller As Keyoti.RapidSpell.RapidSpellDialog
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSpell As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAttach As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTaskEmail))
        Me.cmdOK = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.pnSMTP = New System.Windows.Forms.FlowLayoutPanel
        Me.Label4 = New System.Windows.Forms.Label
        Me.cmbSMTPServer = New System.Windows.Forms.ComboBox
        Me.cmbMailFormat = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdTo = New System.Windows.Forms.Button
        Me.txtTo = New System.Windows.Forms.TextBox
        Me.txtCC = New System.Windows.Forms.TextBox
        Me.cmdCC = New System.Windows.Forms.Button
        Me.txtBcc = New System.Windows.Forms.TextBox
        Me.cmdBCC = New System.Windows.Forms.Button
        Me.cmdAttach = New System.Windows.Forms.Button
        Me.txtAttach = New System.Windows.Forms.TextBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.mnuDefSubject = New System.Windows.Forms.MenuItem
        Me.mnuMsg = New System.Windows.Forms.MenuItem
        Me.mnuSig = New System.Windows.Forms.MenuItem
        Me.mnuAttach = New System.Windows.Forms.MenuItem
        Me.MenuItem12 = New System.Windows.Forms.MenuItem
        Me.mnuSpell = New System.Windows.Forms.MenuItem
        Me.txtMsg = New System.Windows.Forms.TextBox
        Me.txtSubject = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmdInsert = New System.Windows.Forms.Button
        Me.ofd = New System.Windows.Forms.OpenFileDialog
        Me.mnuContacts = New System.Windows.Forms.ContextMenu
        Me.mnuMAPI = New System.Windows.Forms.MenuItem
        Me.mnuMARS = New System.Windows.Forms.MenuItem
        Me.mnuDB = New System.Windows.Forms.MenuItem
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuConstants = New System.Windows.Forms.MenuItem
        Me.mnuSubInsert = New System.Windows.Forms.ContextMenu
        Me.MenuItem15 = New System.Windows.Forms.MenuItem
        Me.MenuItem16 = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.MenuItem18 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.MenuItem11 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.Speller = New Keyoti.RapidSpell.RapidSpellDialog(Me.components)
        Me.SuperT = New DevComponents.DotNetBar.SuperTooltip
        Me.grpSender = New System.Windows.Forms.GroupBox
        Me.txtSenderAddress = New System.Windows.Forms.TextBox
        Me.txtSenderName = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.pnSMTP.SuspendLayout()
        CType(Me.ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpSender.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(301, 452)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 22)
        Me.cmdOK.TabIndex = 2
        Me.cmdOK.Text = "&OK"
        '
        'txtName
        '
        Me.txtName.ForeColor = System.Drawing.Color.Blue
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(456, 21)
        Me.txtName.TabIndex = 0
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(389, 452)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 22)
        Me.cmdCancel.TabIndex = 3
        Me.cmdCancel.Text = "&Cancel"
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 15)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Task Name"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grpSender)
        Me.GroupBox1.Controls.Add(Me.pnSMTP)
        Me.GroupBox1.Controls.Add(Me.cmbMailFormat)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cmdTo)
        Me.GroupBox1.Controls.Add(Me.txtTo)
        Me.GroupBox1.Controls.Add(Me.txtCC)
        Me.GroupBox1.Controls.Add(Me.cmdCC)
        Me.GroupBox1.Controls.Add(Me.txtBcc)
        Me.GroupBox1.Controls.Add(Me.cmdBCC)
        Me.GroupBox1.Controls.Add(Me.cmdAttach)
        Me.GroupBox1.Controls.Add(Me.txtAttach)
        Me.GroupBox1.Controls.Add(Me.txtMsg)
        Me.GroupBox1.Controls.Add(Me.txtSubject)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(456, 394)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'pnSMTP
        '
        Me.pnSMTP.Controls.Add(Me.Label4)
        Me.pnSMTP.Controls.Add(Me.cmbSMTPServer)
        Me.pnSMTP.Location = New System.Drawing.Point(228, 293)
        Me.pnSMTP.Name = "pnSMTP"
        Me.pnSMTP.Size = New System.Drawing.Size(212, 26)
        Me.pnSMTP.TabIndex = 25
        Me.pnSMTP.Visible = False
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 24)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "SMTP Server"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmbSMTPServer
        '
        Me.cmbSMTPServer.ItemHeight = 13
        Me.cmbSMTPServer.Location = New System.Drawing.Point(85, 3)
        Me.cmbSMTPServer.Name = "cmbSMTPServer"
        Me.cmbSMTPServer.Size = New System.Drawing.Size(121, 21)
        Me.cmbSMTPServer.TabIndex = 1
        '
        'cmbMailFormat
        '
        Me.cmbMailFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMailFormat.ItemHeight = 13
        Me.cmbMailFormat.Items.AddRange(New Object() {"TEXT", "HTML"})
        Me.cmbMailFormat.Location = New System.Drawing.Point(80, 294)
        Me.cmbMailFormat.Name = "cmbMailFormat"
        Me.cmbMailFormat.Size = New System.Drawing.Size(121, 21)
        Me.cmbMailFormat.TabIndex = 10
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 297)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 15)
        Me.Label3.TabIndex = 27
        Me.Label3.Text = "Mail Format"
        '
        'cmdTo
        '
        Me.cmdTo.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdTo.Image = CType(resources.GetObject("cmdTo.Image"), System.Drawing.Image)
        Me.cmdTo.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdTo.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdTo.Location = New System.Drawing.Point(8, 15)
        Me.cmdTo.Name = "cmdTo"
        Me.cmdTo.Size = New System.Drawing.Size(64, 21)
        Me.cmdTo.TabIndex = 0
        Me.cmdTo.Text = "To..."
        Me.cmdTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtTo
        '
        Me.txtTo.ForeColor = System.Drawing.Color.Blue
        Me.txtTo.Location = New System.Drawing.Point(80, 15)
        Me.txtTo.Name = "txtTo"
        Me.txtTo.Size = New System.Drawing.Size(360, 21)
        Me.txtTo.TabIndex = 1
        Me.txtTo.Tag = "memo"
        '
        'txtCC
        '
        Me.txtCC.ForeColor = System.Drawing.Color.Blue
        Me.txtCC.Location = New System.Drawing.Point(80, 45)
        Me.txtCC.Name = "txtCC"
        Me.txtCC.Size = New System.Drawing.Size(360, 21)
        Me.txtCC.TabIndex = 3
        Me.txtCC.Tag = "memo"
        '
        'cmdCC
        '
        Me.cmdCC.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCC.Image = CType(resources.GetObject("cmdCC.Image"), System.Drawing.Image)
        Me.cmdCC.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCC.Location = New System.Drawing.Point(8, 45)
        Me.cmdCC.Name = "cmdCC"
        Me.cmdCC.Size = New System.Drawing.Size(64, 21)
        Me.cmdCC.TabIndex = 2
        Me.cmdCC.Text = "Cc..."
        Me.cmdCC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtBcc
        '
        Me.txtBcc.ForeColor = System.Drawing.Color.Blue
        Me.txtBcc.Location = New System.Drawing.Point(80, 74)
        Me.txtBcc.Name = "txtBcc"
        Me.txtBcc.Size = New System.Drawing.Size(360, 21)
        Me.txtBcc.TabIndex = 5
        Me.txtBcc.Tag = "memo"
        '
        'cmdBCC
        '
        Me.cmdBCC.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBCC.Image = CType(resources.GetObject("cmdBCC.Image"), System.Drawing.Image)
        Me.cmdBCC.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdBCC.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBCC.Location = New System.Drawing.Point(8, 74)
        Me.cmdBCC.Name = "cmdBCC"
        Me.cmdBCC.Size = New System.Drawing.Size(64, 22)
        Me.cmdBCC.TabIndex = 4
        Me.cmdBCC.Text = "Bcc..."
        Me.cmdBCC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cmdAttach
        '
        Me.cmdAttach.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAttach.Image = CType(resources.GetObject("cmdAttach.Image"), System.Drawing.Image)
        Me.cmdAttach.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAttach.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAttach.Location = New System.Drawing.Point(8, 104)
        Me.cmdAttach.Name = "cmdAttach"
        Me.cmdAttach.Size = New System.Drawing.Size(64, 21)
        Me.cmdAttach.TabIndex = 6
        Me.cmdAttach.Text = "Attach"
        Me.cmdAttach.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtAttach
        '
        Me.txtAttach.ContextMenu = Me.mnuInserter
        Me.txtAttach.ForeColor = System.Drawing.Color.Blue
        Me.txtAttach.Location = New System.Drawing.Point(80, 104)
        Me.txtAttach.Name = "txtAttach"
        Me.txtAttach.Size = New System.Drawing.Size(360, 21)
        Me.txtAttach.TabIndex = 7
        Me.txtAttach.Tag = "memo"
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1, Me.MenuItem12, Me.mnuSpell})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase, Me.MenuItem5, Me.mnuDefSubject, Me.mnuMsg, Me.mnuSig, Me.mnuAttach})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "-"
        '
        'mnuDefSubject
        '
        Me.mnuDefSubject.Index = 4
        Me.mnuDefSubject.Text = "Default Subject"
        '
        'mnuMsg
        '
        Me.mnuMsg.Index = 5
        Me.mnuMsg.Text = "Default Message"
        '
        'mnuSig
        '
        Me.mnuSig.Index = 6
        Me.mnuSig.Text = "Default Signature"
        '
        'mnuAttach
        '
        Me.mnuAttach.Index = 7
        Me.mnuAttach.Text = "Default Attachment"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 10
        Me.MenuItem12.Text = "-"
        '
        'mnuSpell
        '
        Me.mnuSpell.Index = 11
        Me.mnuSpell.Text = "Spell Check"
        '
        'txtMsg
        '
        Me.txtMsg.ContextMenu = Me.mnuInserter
        Me.txtMsg.ForeColor = System.Drawing.Color.Blue
        Me.txtMsg.Location = New System.Drawing.Point(8, 163)
        Me.txtMsg.Multiline = True
        Me.txtMsg.Name = "txtMsg"
        Me.txtMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMsg.Size = New System.Drawing.Size(432, 124)
        Me.txtMsg.TabIndex = 9
        Me.txtMsg.Tag = "memo"
        '
        'txtSubject
        '
        Me.txtSubject.ContextMenu = Me.mnuInserter
        Me.txtSubject.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSubject.ForeColor = System.Drawing.Color.Blue
        Me.txtSubject.Location = New System.Drawing.Point(80, 134)
        Me.txtSubject.Name = "txtSubject"
        Me.txtSubject.Size = New System.Drawing.Size(360, 21)
        Me.txtSubject.TabIndex = 8
        Me.txtSubject.Tag = "memo"
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 136)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(48, 14)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Subject"
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdInsert
        '
        Me.cmdInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdInsert.Image = CType(resources.GetObject("cmdInsert.Image"), System.Drawing.Image)
        Me.cmdInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdInsert.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdInsert.Location = New System.Drawing.Point(5, 452)
        Me.cmdInsert.Name = "cmdInsert"
        Me.cmdInsert.Size = New System.Drawing.Size(24, 22)
        Me.cmdInsert.TabIndex = 24
        Me.cmdInsert.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ofd
        '
        Me.ofd.Filter = "All Files|*.*"
        Me.ofd.Multiselect = True
        '
        'mnuContacts
        '
        Me.mnuContacts.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuMAPI, Me.mnuMARS, Me.mnuDB, Me.mnuFile, Me.mnuConstants})
        '
        'mnuMAPI
        '
        Me.mnuMAPI.Index = 0
        Me.mnuMAPI.Text = "MAPI Address Book"
        '
        'mnuMARS
        '
        Me.mnuMARS.Index = 1
        Me.mnuMARS.Text = "SQL-RD Address Book"
        '
        'mnuDB
        '
        Me.mnuDB.Index = 2
        Me.mnuDB.Text = "Database Source"
        '
        'mnuFile
        '
        Me.mnuFile.Index = 3
        Me.mnuFile.Text = "Text File"
        '
        'mnuConstants
        '
        Me.mnuConstants.Index = 4
        Me.mnuConstants.Text = "Constants"
        '
        'mnuSubInsert
        '
        Me.mnuSubInsert.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem15})
        '
        'MenuItem15
        '
        Me.MenuItem15.Index = 0
        Me.MenuItem15.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem16, Me.MenuItem17, Me.MenuItem18, Me.MenuItem8, Me.MenuItem11, Me.MenuItem9, Me.MenuItem10, Me.MenuItem13})
        Me.MenuItem15.Text = "Insert"
        '
        'MenuItem16
        '
        Me.MenuItem16.Index = 0
        Me.MenuItem16.Text = "Constants"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 1
        Me.MenuItem17.Text = "-"
        '
        'MenuItem18
        '
        Me.MenuItem18.Index = 2
        Me.MenuItem18.Text = "Database Field"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 3
        Me.MenuItem8.Text = "-"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 4
        Me.MenuItem11.Text = "Default Subject"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 5
        Me.MenuItem9.Text = "Default Message"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "Default Signature "
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 7
        Me.MenuItem13.Text = "Default Attachment"
        '
        'Speller
        '
        Me.Speller.AllowAnyCase = False
        Me.Speller.AllowMixedCase = False
        Me.Speller.AlwaysShowDialog = True
        Me.Speller.BackColor = System.Drawing.Color.Empty
        Me.Speller.CheckCompoundWords = False
        Me.Speller.ConsiderationRange = 80
        Me.Speller.ControlBox = True
        Me.Speller.DictFilePath = Nothing
        Me.Speller.FindCapitalizedSuggestions = False
        Me.Speller.ForeColor = System.Drawing.Color.Empty
        Me.Speller.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
        Me.Speller.GUILanguage = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Icon = CType(resources.GetObject("Speller.Icon"), System.Drawing.Icon)
        Me.Speller.IgnoreCapitalizedWords = False
        Me.Speller.IgnoreURLsAndEmailAddresses = True
        Me.Speller.IgnoreWordsWithDigits = True
        Me.Speller.IgnoreXML = False
        Me.Speller.IncludeUserDictionaryInSuggestions = False
        Me.Speller.LanguageParser = Keyoti.RapidSpell.LanguageType.ENGLISH
        Me.Speller.Location = New System.Drawing.Point(300, 200)
        Me.Speller.LookIntoHyphenatedText = True
        Me.Speller.MdiParent = Nothing
        Me.Speller.MinimizeBox = True
        Me.Speller.Modal = False
        Me.Speller.ModalAutoDispose = True
        Me.Speller.ModalOwner = Nothing
        Me.Speller.QueryTextBoxMultiline = True
        Me.Speller.SeparateHyphenWords = False
        Me.Speller.ShowFinishedBoxAtEnd = True
        Me.Speller.ShowInTaskbar = False
        Me.Speller.SizeGripStyle = System.Windows.Forms.SizeGripStyle.[Auto]
        Me.Speller.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Speller.SuggestionsMethod = Keyoti.RapidSpell.SuggestionsMethodType.HashingSuggestions
        Me.Speller.SuggestSplitWords = True
        Me.Speller.TextBoxBaseToCheck = Nothing
        Me.Speller.Texts.AddButtonText = "Add"
        Me.Speller.Texts.CancelButtonText = "Cancel"
        Me.Speller.Texts.ChangeAllButtonText = "Change All"
        Me.Speller.Texts.ChangeButtonText = "Change"
        Me.Speller.Texts.CheckCompletePopUpText = "The spelling check is complete."
        Me.Speller.Texts.CheckCompletePopUpTitle = "Spelling"
        Me.Speller.Texts.CheckingSpellingText = "Checking Document..."
        Me.Speller.Texts.DialogTitleText = "Spelling"
        Me.Speller.Texts.FindingSuggestionsLabelText = "Finding Suggestions..."
        Me.Speller.Texts.FindSuggestionsLabelText = "Find Suggestions?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpText = "Finished checking selection, would you like to check the rest of the text?"
        Me.Speller.Texts.FinishedCheckingSelectionPopUpTitle = "Finished Checking Selection"
        Me.Speller.Texts.IgnoreAllButtonText = "Ignore All"
        Me.Speller.Texts.IgnoreButtonText = "Ignore"
        Me.Speller.Texts.InDictionaryLabelText = "In Dictionary:"
        Me.Speller.Texts.NoSuggestionsText = "No Suggestions."
        Me.Speller.Texts.NotInDictionaryLabelText = "Not In Dictionary:"
        Me.Speller.Texts.RemoveDuplicateWordText = "Remove duplicate word"
        Me.Speller.Texts.ResumeButtonText = "Resume"
        Me.Speller.Texts.SuggestionsLabelText = "Suggestions:"
        Me.Speller.ThirdPartyTextComponentToCheck = Nothing
        Me.Speller.TopLevel = True
        Me.Speller.TopMost = False
        Me.Speller.UserDictionaryFile = Nothing
        Me.Speller.V2Parser = True
        Me.Speller.WarnDuplicates = True
        '
        'SuperT
        '
        Me.SuperT.DefaultFont = New System.Drawing.Font("Tahoma", 8.25!)
        Me.SuperT.LicenseKey = "F962CEC7-CD8F-4911-A9E9-CAB39962FC1F"
        Me.SuperT.MinimumTooltipSize = New System.Drawing.Size(150, 50)
        '
        'grpSender
        '
        Me.grpSender.Controls.Add(Me.txtSenderAddress)
        Me.grpSender.Controls.Add(Me.txtSenderName)
        Me.grpSender.Controls.Add(Me.Label18)
        Me.grpSender.Controls.Add(Me.Label17)
        Me.grpSender.Location = New System.Drawing.Point(11, 321)
        Me.grpSender.Name = "grpSender"
        Me.grpSender.Size = New System.Drawing.Size(439, 66)
        Me.grpSender.TabIndex = 11
        Me.grpSender.TabStop = False
        Me.grpSender.Text = "Customize sender details (optional)"
        '
        'txtSenderAddress
        '
        Me.txtSenderAddress.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderAddress.Location = New System.Drawing.Point(83, 41)
        Me.txtSenderAddress.Name = "txtSenderAddress"
        Me.txtSenderAddress.Size = New System.Drawing.Size(340, 21)
        Me.txtSenderAddress.TabIndex = 1
        '
        'txtSenderName
        '
        Me.txtSenderName.ForeColor = System.Drawing.Color.Blue
        Me.txtSenderName.Location = New System.Drawing.Point(83, 14)
        Me.txtSenderName.Name = "txtSenderName"
        Me.txtSenderName.Size = New System.Drawing.Size(340, 21)
        Me.txtSenderName.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(4, 43)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(50, 13)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Address:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(6, 17)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(38, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Name:"
        '
        'frmTaskEmail
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 14)
        Me.CancelButton = Me.cmdCancel
        Me.ClientSize = New System.Drawing.Size(474, 477)
        Me.Controls.Add(Me.cmdInsert)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmTaskEmail"
        Me.Text = "Send an Email"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.pnSMTP.ResumeLayout(False)
        CType(Me.ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpSender.ResumeLayout(False)
        Me.grpSender.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
        Me.Close()
    End Sub

    Private Sub cmdAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAttach.Click
        ofd.Multiselect = True
        ofd.Title = "Select files to attach"
        ofd.ShowDialog()

        If ofd.FileNames.GetLength(0) = 0 Then Return

        If txtAttach.Text.EndsWith(";") = False And txtAttach.Text.Length > 0 Then _
        txtAttach.Text &= ";"

        For Each s As String In ofd.FileNames
            txtAttach.Text &= s & ";"
        Next

    End Sub


    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" And txtName.Visible = True Then
            ep.SetError(txtName, "Please provide a name for this task")
            txtName.Focus()
            Exit Sub
        ElseIf txtTo.Text = "" Then
            ep.SetError(txtTo, "Please enter the email recipient(s)")
            txtTo.Focus()
            Exit Sub
            'Else
            '    For Each s As String In txtAttach.Text.Split(";")
            '        If s.Length > 0 Then
            '            If (s.IndexOf("*") = -1) And (s.Contains("<[") = False) Then
            '                If IO.File.Exists(s) = False Then
            '                    ep.SetError(txtAttach, "The file '" & s & "' does not exist. Please make sure the file exists and try again.")
            '                    txtAttach.Focus()
            '                    Return
            '                End If
            '            End If
            '        End If

            '    Next
        End If

        Me.Close()
    End Sub

    Public Function AddTask(Optional ByVal ScheduleID As Integer = 99999, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.cmbSMTPServer.Text = "Default"
        Me.cmbMailFormat.SelectedIndex = 1

        If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
            Me.grpSender.Enabled = True
        Else
            Me.grpSender.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Exit Function

        Dim SQL As String
        Dim sCols As String
        Dim sVals As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        sCols = "TaskID,ScheduleID,TaskType,TaskName," & _
        "SendTo,Cc,Bcc,Msg,FileList,Subject,ProgramPath,PrinterName,SenderName,SenderAddress,OrderID"

        'program path used for mail format
        'printername used for smtpserver

        sVals = nID & "," & _
        ScheduleID & "," & _
        "'SendMail'," & _
        "'" & txtName.Text.Replace("'", "''") & "'," & _
        "'" & txtTo.Text.Replace("'", "''") & "'," & _
        "'" & txtCC.Text.Replace("'", "''") & "'," & _
        "'" & txtBcc.Text.Replace("'", "''") & "'," & _
        "'" & txtMsg.Text.Replace("'", "''") & "'," & _
        "'" & txtAttach.Text.Replace("'", "''") & "'," & _
        "'" & txtSubject.Text.Replace("'", "''") & "'," & _
        "'" & cmbMailFormat.Text & "'," & _
        "'" & SQLPrepare(cmbSMTPServer.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSenderName.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSenderAddress.Text) & "'," & _
        oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks (" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        _Delay(0.5)
        'set when the task will be executed

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Function
    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            txtTo.Text = oRs("sendto").Value
            txtCC.Text = oRs("cc").Value
            txtBcc.Text = oRs("bcc").Value
            txtAttach.Text = oRs("filelist").Value
            txtSubject.Text = oRs("subject").Value
            txtMsg.Text = oRs("msg").Value
            cmbSMTPServer.Text = IsNull(oRs("printername").Value, "Default")
            cmbMailFormat.Text = IsNull(oRs("programpath").Value, "TEXT")

            Try
                Me.txtSenderName.Text = IsNull(oRs("sendername").Value)
                Me.txtSenderAddress.Text = IsNull(oRs("senderaddress").Value)
            Catch :End Try
        End If

        oRs.Close()

        If MailType = gMailType.SQLRDMAIL Or MailType = gMailType.SMTP Then
            Me.grpSender.Enabled = True
        Else
            Me.grpSender.Enabled = False
        End If

        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        SQL = "TaskName = '" & txtName.Text.Replace("'", "''") & "'," & _
            "SendTo = '" & txtTo.Text.Replace("'", "''") & "'," & _
            "CC = '" & txtCC.Text.Replace("'", "''") & "'," & _
            "Bcc = '" & txtBcc.Text.Replace("'", "''") & "'," & _
            "FileList ='" & txtAttach.Text.Replace("'", "''") & "'," & _
            "Subject = '" & txtSubject.Text.Replace("'", "''") & "'," & _
            "Msg = '" & txtMsg.Text.Replace("'", "''") & "'," & _
            "ProgramPath = '" & cmbMailFormat.Text & "'," & _
            "PrinterName ='" & SQLPrepare(cmbSMTPServer.Text) & "', " & _
            "SenderName ='" & SQLPrepare(Me.txtSenderName.Text) & "', " & _
            "SenderAddress ='" & SQLPrepare(Me.txtSenderAddress.Text) & "' "

        SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

        clsMarsData.WriteData(SQL)

        'for editing a task, set its run type

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
    End Sub
    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub txtTo_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles _
    txtTo.GotFocus, txtCC.GotFocus, txtBcc.GotFocus
        oField = CType(sender, TextBox)
    End Sub

    Private Sub txtTo_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTo.TextChanged
        ep.SetError(sender, "")
    End Sub


    Private Sub frmTaskEmail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        If MailType <> MarsGlobal.gMailType.MAPI Then mnuMAPI.Enabled = False

        If MailType = MarsGlobal.gMailType.MAPI Or MailType = gMailType.GROUPWISE Then
            cmbMailFormat.Text = "TEXT"
            cmbMailFormat.Enabled = False
        ElseIf MailType = gMailType.SMTP Then
            pnSMTP.Visible = True
        End If

        Me.txtTo.ContextMenu = Me.mnuInserter
        Me.txtCC.ContextMenu = Me.mnuInserter
        Me.txtBcc.ContextMenu = Me.mnuInserter
        Me.txtSenderName.ContextMenu = Me.mnuInserter
        Me.txtSenderAddress.ContextMenu = Me.mnuInserter
    End Sub

    Private Sub cmdTo_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdTo.MouseUp

        If e.Button <> MouseButtons.Left Then Return

        oField = txtTo
        sField = "to"

        mnuContacts.Show(cmdTo, New Point(e.X, e.Y))

    End Sub

    Private Sub cmdCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtCC
        sField = "cc"
        mnuContacts.Show(cmdCC, New Point(e.X, e.Y))
    End Sub

    Private Sub cmdBCC_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdBCC.MouseUp
        If e.Button <> MouseButtons.Left Then Return

        oField = txtBcc
        sField = "bcc"
        mnuContacts.Show(cmdBCC, New Point(e.X, e.Y))

    End Sub


    Private Sub mnuMAPI_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMAPI.Click

        oMsg.OutlookAddresses(oField)
    End Sub

    Private Sub mnuMARS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMARS.Click
        

        Dim oPicker As New frmPickContact
        Dim sValues(2) As String

        sValues = oPicker.PickContact(sField)

        If sValues Is Nothing Then Return

        If txtTo.Text.EndsWith(";") = False And txtTo.Text.Length > 0 _
        Then txtTo.Text &= ";"

        Try
            txtTo.Text &= sValues(0)
        Catch
        End Try

        If txtCC.Text.EndsWith(";") = False And txtCC.Text.Length > 0 _
        Then txtCC.Text &= ";"

        Try
            txtCC.Text &= sValues(1)
        Catch
        End Try

        If txtBcc.Text.EndsWith(";") = False And txtBcc.Text.Length > 0 _
        Then txtBcc.Text &= ";"

        Try
            txtBcc.Text &= sValues(2)
        Catch
        End Try
    End Sub

    Private Sub mnuFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuFile.Click

        If IsFeatEnabled(gEdition.ENTERPRISEPROPLUS, modFeatCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISEPROPLUS)
            Return
        End If

        ofd.Title = "Please select the file to read email addresses from"

        ofd.ShowDialog()

        If ofd.FileName.Length > 0 Then
            If oField.Text.EndsWith(";") = False And oField.Text.Length > 0 Then _
            oField.Text &= ";"

            oField.Text &= "<[f]" & ofd.FileName & ">;"
        End If
    End Sub

    Private Sub mnuDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDB.Click
        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) = False Then
            _NeedUpgrade(MarsGlobal.gEdition.ENTERPRISE)
            Return
        End If

        Dim oItem As New frmDataItems
        Dim sTemp As String

        sTemp = oItem._GetDataItem(Me.m_eventID)

        If sTemp.Length > 0 Then
            If oField.Text.Length > 0 And oField.Text.EndsWith(";") = False Then oField.Text &= ";"
            oField.Text &= sTemp & ";"
        End If
    End Sub

    Private Sub txtSubject_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSubject.TextChanged

    End Sub

    Private Sub txtSubject_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSubject.GotFocus
        oField = txtSubject
    End Sub

    Private Sub txtSubject_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtSubject.MouseUp
        oField = txtSubject
    End Sub

    Private Sub txtMsg_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMsg.TextChanged

    End Sub

    Private Sub txtMsg_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMsg.GotFocus
        oField = txtMsg
    End Sub

    Private Sub txtMsg_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtMsg.MouseUp
        oField = txtMsg
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch
        End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch
        End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch
        End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch
        End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        On Error Resume Next
        oField.SelectedText = String.Empty
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch
        End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        On Error Resume Next
        Dim oInsert As New frmInserter(Me.m_eventID)
        oInsert.m_EventBased = Me.m_eventBased
        'oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oItem As New frmDataItems
        oItem.m_EventID = Me.m_eventID

        oField.SelectedText = oItem._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub cmdInsert_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles cmdInsert.MouseUp
        mnuSubInsert.Show(cmdInsert, New Point(e.X, e.Y))
    End Sub


    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem16.Click
        MenuItem2_Click(sender, e)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem18.Click
        mnuDatabase_Click(sender, e)
    End Sub

    Private Sub cmdInsert_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInsert.Click

    End Sub

    Private Sub mnuMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMsg.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub mnuSig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSig.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defmsg.sqlrd")
        Catch : End Try
    End Sub

    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click
        Try
            oField.SelectedText = ReadTextFromFile(sAppPath & "defsig.sqlrd")
        Catch : End Try
    End Sub

   

    Private Sub mnuDefSubject_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDefSubject.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub MenuItem11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem11.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefSubject", "")
    End Sub

    Private Sub mnuSpell_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSpell.Click
        With Speller
            .TextBoxBaseToCheck = oField
            .Modal = True
            .ModalOwner = Me
            .Check()
        End With
    End Sub

    Private Sub mnuAttach_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
    mnuAttach.Click, MenuItem13.Click
        Dim oUI As New clsMarsUI
        oField.SelectedText = oUI.ReadRegistry( _
         "DefAttach", "")
    End Sub

    Private Sub cmdTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTo.Click

    End Sub

    Private Sub txtAttach_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAttach.GotFocus
        If IsFeatEnabled(gEdition.ENTERPRISE, featureCodes.m2_MailingLists) = True Then
            oField = txtAttach
        End If
    End Sub

    Private Sub mnuConstants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuConstants.Click
        Dim oInsert As New frmInserter(Me.m_eventID)

        oInsert.m_EventBased = Me.m_eventBased
        oInsert.m_EventID = Me.m_eventID

        oInsert.GetConstants(Me)
    End Sub

    Private Sub cmbSMTPServer_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbSMTPServer.DropDown
        Dim oRs As ADODB.Recordset
        Dim SQL As String

        SQL = "SELECT * FROM SMTPServers"

        cmbSMTPServer.Items.Clear()
        cmbSMTPServer.Items.Add("Default")

        oRs = clsMarsData.GetData(SQL)

        Do While oRs.EOF = False
            cmbSMTPServer.Items.Add(oRs("smtpname").Value)
            oRs.MoveNext()
        Loop

        oRs.Close()
    End Sub

    Public Sub AddForwardMail(Optional ByVal conditionID As Integer = 99999)
        Dim SQL As String

        Me.cmbSMTPServer.Text = "Default"
        Me.cmbMailFormat.SelectedIndex = 1
        Label1.Visible = False
        Me.txtName.Visible = False
        GroupBox1.Location = New Point(8, 7)
        Me.Text = "Forward Email"

        'tooltips
        Dim info As New DevComponents.DotNetBar.SuperTooltipInfo

        info.HeaderText = "Forward Email"
        info.BodyText = "Modifying this field will replace the value in the originating mail"
        info.HeaderVisible = True

        SuperT.SetSuperTooltip(Me.txtSubject, info)

        'reload info
        SQL = "SELECT * FROM ForwardMailAttr WHERE ConditionID =" & conditionID

        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        If oRs Is Nothing Then
            Application.DoEvents()
        ElseIf oRs.EOF = True Then
            oRs.Close()
        Else
            txtTo.Text = IsNull(oRs("sendto").Value)
            txtCC.Text = IsNull(oRs("sendcc").Value)
            txtBcc.Text = IsNull(oRs("sendbcc").Value)
            txtSubject.Text = IsNull(oRs("mailsubject").Value)
            txtMsg.Text = IsNull(oRs("mailbody").Value)
            txtAttach.Text = IsNull(oRs("attachments").Value)
            Me.cmbMailFormat.Text = IsNull(oRs("mailformat").Value, "TEXT")
            Me.cmbSMTPServer.Text = IsNull(oRs("mailserver").Value, "Default")
            oRs.Close()
        End If

        Me.ShowDialog()

        If UserCancel = True Then
            Return
        End If


        Dim cols As String
        Dim vals As String

        SQL = "DELETE FROM ForwardMailAttr WHERE ConditionID =" & conditionID

        clsMarsData.WriteData(SQL)

        cols = "MailID,ConditionID,SendTo,SendCC,SendBCC,MailSubject,MailBody,Attachments,MailFormat,MailServer"

        vals = clsMarsData.CreateDataID("forwardmailattr", "mailid") & "," & _
        conditionID & "," & _
        "'" & SQLPrepare(Me.txtTo.Text) & "'," & _
        "'" & SQLPrepare(Me.txtCC.Text) & "'," & _
        "'" & SQLPrepare(Me.txtBcc.Text) & "'," & _
        "'" & SQLPrepare(Me.txtSubject.Text) & "'," & _
        "'" & SQLPrepare(Me.txtMsg.Text) & "'," & _
        "'" & SQLPrepare(Me.txtAttach.Text) & "'," & _
        "'" & SQLPrepare(Me.cmbMailFormat.Text) & "'," & _
        "'" & SQLPrepare(Me.cmbSMTPServer.Text) & "' "

        clsMarsData.DataItem.InsertData("ForwardMailAttr", cols, vals)

    End Sub
  
    Private Sub mnuInserter_Popup(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuInserter.Popup
        Dim textBox As Control = mnuInserter.SourceControl

        Select Case textBox.Name
            Case txtTo.Name, txtCC.Name, txtBcc.Name
                Me.MenuItem12.Visible = False
                Me.mnuSpell.Visible = False
                Me.MenuItem5.Visible = False
                Me.mnuDefSubject.Visible = False
                Me.mnuMsg.Visible = False
                Me.mnuSig.Visible = False
                Me.mnuAttach.Visible = False
            Case Else
                Me.MenuItem12.Visible = True
                Me.mnuSpell.Visible = True
                Me.MenuItem5.Visible = True
                Me.mnuDefSubject.Visible = True
                Me.mnuMsg.Visible = True
                Me.mnuSig.Visible = True
                Me.mnuAttach.Visible = True
        End Select
    End Sub

    Private Sub txtSenderAddress_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSenderAddress.GotFocus, txtSenderName.GotFocus
        Me.oField = CType(sender, TextBox)
    End Sub
End Class
