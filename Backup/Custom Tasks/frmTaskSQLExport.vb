Public Class frmTaskSQLExport
    Dim ep As ErrorProvider = New ErrorProvider
    Dim userCancel As Boolean = False

    Private Sub btnConnect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnect.Click
        Dim isValidated As Boolean = UcDSN._Validate
        Me.btnBuild.Enabled = isValidated

        If isValidated = True And txtQuery.Text = "" Then
            btnBuild_Click(Nothing, Nothing)
        End If
    End Sub

    Public Sub AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        UcDest.m_IsQuery = True
        Dim taskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")
        UcDest.nReportID = taskID
        UcDest.nPackID = 0
        UcDest.nSmartID = 0
        UcDest.disableEmbed = True

        Me.ShowDialog()

        If userCancel = True Then Exit Sub

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sValues As String
        
        'Connection String -> ProgramParameters
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,Msg,OrderID"

        sVals = taskID & "," & _
            ScheduleID & "," & _
            "'DBExport'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(txtQuery.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(taskID)
        'basi
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        'Connection String -> ProgramParameters
        'New Table Name -> ProgramPath 
        'Columns for Table -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing AndAlso oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            txtQuery.Text = oRs("msg").Value

            oRs.Close()

            '//load the destinations
            UcDest.nReportID = nTaskID
            UcDest.nPackID = 0
            UcDest.nSmartID = 0
            UcDest.m_IsQuery = True
            UcDest.LoadAll()
            UcDest.disableEmbed = True

            Me.ShowDialog()

            If userCancel = True Then Return

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "Msg = '" & SQLPrepare(txtQuery.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)

            'for editing a task, set its run type
            ' _Delay(2)

            Dim oRunWhen As New frmTaskRunWhen

            If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
        End If
    End Sub
    Private Sub btnBuild_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuild.Click
        Dim getSQL As frmDynamicParameter = New frmDynamicParameter

        Dim results() As String = getSQL.ReturnSQLQuery(UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & UcDSN.txtPassword.Text, txtQuery.Text)

        If results IsNot Nothing Then
            Dim connection As String = results(0)

            With UcDSN
                .cmbDSN.Text = connection.Split("|")(0)
                .txtUserID.Text = connection.Split("|")(1)
                .txtPassword.Text = connection.Split("|")(2)
            End With

            txtQuery.Text = results(1)
        End If

    End Sub

    Private Sub frmTaskSQLExport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
        Me.UcDest.m_IsQuery = True
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf txtQuery.Text = "" Then
            ep.SetError(txtQuery, "Please configure the query to use")
            btnBuild.Focus()
            Return
        ElseIf UcDSN._Validate = False Then
            Return
        ElseIf Me.UcDest.destinationCount = 0 Then
            ep.SetError(Me.UcDest.lsvDestination, "Please add a destination for the query export")
            Me.tabExport.SelectedTab = TabPage2
            Return
        End If

        Me.UcDest.CommitDeletions()

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        userCancel = True
        Close()
    End Sub

    Private Sub txtQuery_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtQuery.TextChanged
        ep.SetError(txtQuery, "")
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, "")
    End Sub
End Class
