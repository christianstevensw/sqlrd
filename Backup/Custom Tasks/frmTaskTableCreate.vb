Public Class frmTaskTableCreate
    Inherits sqlrd.frmTaskMaster
    Dim UserCancel As Boolean
    Dim nStep As Integer = 1

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents UcDSN As sqlrd.ucDSN
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents Page1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdBack As System.Windows.Forms.Button
    Friend WithEvents cmdNext As System.Windows.Forms.Button
    Friend WithEvents Page2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTable As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtColumnName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cmbColumnType As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtColumnSize As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmdAddWhere As System.Windows.Forms.Button
    Friend WithEvents cmdRemoveWhere As System.Windows.Forms.Button
    Friend WithEvents lsvColumns As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents Page3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtFinal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cmdSkip As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmTaskTableCreate))
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Page3 = New System.Windows.Forms.GroupBox
        Me.txtFinal = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.Page2 = New System.Windows.Forms.GroupBox
        Me.lsvColumns = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.cmdAddWhere = New System.Windows.Forms.Button
        Me.cmdRemoveWhere = New System.Windows.Forms.Button
        Me.txtColumnSize = New System.Windows.Forms.NumericUpDown
        Me.cmbColumnType = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtTable = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtColumnName = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Page1 = New System.Windows.Forms.GroupBox
        Me.cmdValidate = New System.Windows.Forms.Button
        Me.UcDSN = New sqlrd.ucDSN
        Me.cmdBack = New System.Windows.Forms.Button
        Me.cmdNext = New System.Windows.Forms.Button
        Me.ep = New System.Windows.Forms.ErrorProvider
        Me.cmdSkip = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.Page3.SuspendLayout()
        Me.Page2.SuspendLayout()
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Page1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(8, 22)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(400, 20)
        Me.txtName.TabIndex = 26
        Me.txtName.Text = ""
        '
        'Label3
        '
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(8, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 15)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Task Name"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.Image = CType(resources.GetObject("cmdCancel.Image"), System.Drawing.Image)
        Me.cmdCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(333, 364)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 21)
        Me.cmdCancel.TabIndex = 24
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.Image = CType(resources.GetObject("cmdOK.Image"), System.Drawing.Image)
        Me.cmdOK.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(248, 364)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 21)
        Me.cmdOK.TabIndex = 23
        Me.cmdOK.Text = "&OK"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Page3)
        Me.GroupBox1.Controls.Add(Me.Page2)
        Me.GroupBox1.Controls.Add(Me.Page1)
        Me.GroupBox1.Controls.Add(Me.cmdBack)
        Me.GroupBox1.Controls.Add(Me.cmdNext)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(408, 305)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        '
        'Page3
        '
        Me.Page3.Controls.Add(Me.txtFinal)
        Me.Page3.Controls.Add(Me.Label6)
        Me.Page3.Location = New System.Drawing.Point(8, 7)
        Me.Page3.Name = "Page3"
        Me.Page3.Size = New System.Drawing.Size(392, 260)
        Me.Page3.TabIndex = 6
        Me.Page3.TabStop = False
        '
        'txtFinal
        '
        Me.txtFinal.Location = New System.Drawing.Point(4, 30)
        Me.txtFinal.Multiline = True
        Me.txtFinal.Name = "txtFinal"
        Me.txtFinal.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtFinal.Size = New System.Drawing.Size(384, 223)
        Me.txtFinal.TabIndex = 3
        Me.txtFinal.Tag = "memo"
        Me.txtFinal.Text = ""
        '
        'Label6
        '
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(4, 15)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 15)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "Finalised Query"
        '
        'Page2
        '
        Me.Page2.Controls.Add(Me.lsvColumns)
        Me.Page2.Controls.Add(Me.cmdAddWhere)
        Me.Page2.Controls.Add(Me.cmdRemoveWhere)
        Me.Page2.Controls.Add(Me.txtColumnSize)
        Me.Page2.Controls.Add(Me.cmbColumnType)
        Me.Page2.Controls.Add(Me.Label2)
        Me.Page2.Controls.Add(Me.txtTable)
        Me.Page2.Controls.Add(Me.Label1)
        Me.Page2.Controls.Add(Me.txtColumnName)
        Me.Page2.Controls.Add(Me.Label4)
        Me.Page2.Controls.Add(Me.Label5)
        Me.Page2.Location = New System.Drawing.Point(8, 7)
        Me.Page2.Name = "Page2"
        Me.Page2.Size = New System.Drawing.Size(392, 260)
        Me.Page2.TabIndex = 5
        Me.Page2.TabStop = False
        '
        'lsvColumns
        '
        Me.lsvColumns.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.lsvColumns.Location = New System.Drawing.Point(8, 126)
        Me.lsvColumns.Name = "lsvColumns"
        Me.lsvColumns.Size = New System.Drawing.Size(360, 127)
        Me.lsvColumns.TabIndex = 13
        Me.lsvColumns.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Column Headers"
        Me.ColumnHeader1.Width = 355
        '
        'cmdAddWhere
        '
        Me.cmdAddWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdAddWhere.Image = CType(resources.GetObject("cmdAddWhere.Image"), System.Drawing.Image)
        Me.cmdAddWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddWhere.Location = New System.Drawing.Point(344, 97)
        Me.cmdAddWhere.Name = "cmdAddWhere"
        Me.cmdAddWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdAddWhere.TabIndex = 12
        Me.cmdAddWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cmdRemoveWhere
        '
        Me.cmdRemoveWhere.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdRemoveWhere.Image = CType(resources.GetObject("cmdRemoveWhere.Image"), System.Drawing.Image)
        Me.cmdRemoveWhere.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveWhere.Location = New System.Drawing.Point(248, 97)
        Me.cmdRemoveWhere.Name = "cmdRemoveWhere"
        Me.cmdRemoveWhere.Size = New System.Drawing.Size(40, 22)
        Me.cmdRemoveWhere.TabIndex = 11
        Me.cmdRemoveWhere.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtColumnSize
        '
        Me.txtColumnSize.Location = New System.Drawing.Point(296, 74)
        Me.txtColumnSize.Name = "txtColumnSize"
        Me.txtColumnSize.Size = New System.Drawing.Size(88, 20)
        Me.txtColumnSize.TabIndex = 4
        '
        'cmbColumnType
        '
        Me.cmbColumnType.ItemHeight = 13
        Me.cmbColumnType.Items.AddRange(New Object() {"NUMBER", "DECIMAL", "TEXT", "MEMO", "DATETIME"})
        Me.cmbColumnType.Location = New System.Drawing.Point(168, 74)
        Me.cmbColumnType.Name = "cmbColumnType"
        Me.cmbColumnType.Size = New System.Drawing.Size(121, 21)
        Me.cmbColumnType.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(8, 59)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 15)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Column Name"
        '
        'txtTable
        '
        Me.txtTable.Location = New System.Drawing.Point(8, 30)
        Me.txtTable.Name = "txtTable"
        Me.txtTable.Size = New System.Drawing.Size(376, 20)
        Me.txtTable.TabIndex = 1
        Me.txtTable.Text = ""
        '
        'Label1
        '
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(8, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(216, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Table Name"
        '
        'txtColumnName
        '
        Me.txtColumnName.Location = New System.Drawing.Point(8, 74)
        Me.txtColumnName.Name = "txtColumnName"
        Me.txtColumnName.Size = New System.Drawing.Size(144, 20)
        Me.txtColumnName.TabIndex = 1
        Me.txtColumnName.Text = ""
        '
        'Label4
        '
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(168, 59)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 15)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Column Type"
        '
        'Label5
        '
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(296, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 15)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Column Size"
        '
        'Page1
        '
        Me.Page1.Controls.Add(Me.cmdValidate)
        Me.Page1.Controls.Add(Me.UcDSN)
        Me.Page1.Location = New System.Drawing.Point(8, 7)
        Me.Page1.Name = "Page1"
        Me.Page1.Size = New System.Drawing.Size(392, 260)
        Me.Page1.TabIndex = 2
        Me.Page1.TabStop = False
        '
        'cmdValidate
        '
        Me.cmdValidate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdValidate.Location = New System.Drawing.Point(168, 141)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(75, 21)
        Me.cmdValidate.TabIndex = 1
        Me.cmdValidate.Text = "Connect..."
        '
        'UcDSN
        '
        Me.UcDSN.BackColor = System.Drawing.SystemColors.Control
        Me.UcDSN.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDSN.ForeColor = System.Drawing.Color.Navy
        Me.UcDSN.Location = New System.Drawing.Point(8, 22)
        Me.UcDSN.Name = "UcDSN"
        Me.UcDSN.Size = New System.Drawing.Size(368, 119)
        Me.UcDSN.TabIndex = 0
        '
        'cmdBack
        '
        Me.cmdBack.Enabled = False
        Me.cmdBack.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdBack.Image = CType(resources.GetObject("cmdBack.Image"), System.Drawing.Image)
        Me.cmdBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdBack.Location = New System.Drawing.Point(8, 275)
        Me.cmdBack.Name = "cmdBack"
        Me.cmdBack.Size = New System.Drawing.Size(56, 21)
        Me.cmdBack.TabIndex = 4
        Me.cmdBack.Text = "&Back"
        Me.cmdBack.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cmdNext
        '
        Me.cmdNext.Enabled = False
        Me.cmdNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdNext.Image = CType(resources.GetObject("cmdNext.Image"), System.Drawing.Image)
        Me.cmdNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdNext.Location = New System.Drawing.Point(344, 275)
        Me.cmdNext.Name = "cmdNext"
        Me.cmdNext.Size = New System.Drawing.Size(56, 21)
        Me.cmdNext.TabIndex = 3
        Me.cmdNext.Text = "&Next"
        Me.cmdNext.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ep
        '
        Me.ep.ContainerControl = Me
        Me.ep.Icon = CType(resources.GetObject("ep.Icon"), System.Drawing.Icon)
        '
        'cmdSkip
        '
        Me.cmdSkip.Enabled = False
        Me.cmdSkip.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdSkip.Image = CType(resources.GetObject("cmdSkip.Image"), System.Drawing.Image)
        Me.cmdSkip.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdSkip.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdSkip.Location = New System.Drawing.Point(8, 364)
        Me.cmdSkip.Name = "cmdSkip"
        Me.cmdSkip.Size = New System.Drawing.Size(75, 21)
        Me.cmdSkip.TabIndex = 28
        Me.cmdSkip.Text = "&Skip"
        '
        'frmTaskTableCreate
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(426, 393)
        Me.Controls.Add(Me.cmdSkip)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Name = "frmTaskTableCreate"
        Me.Text = "Create  a new table"
        Me.GroupBox1.ResumeLayout(False)
        Me.Page3.ResumeLayout(False)
        Me.Page2.ResumeLayout(False)
        CType(Me.txtColumnSize, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Page1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmTaskTableCreate_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Page1.BringToFront()
        FormatForWinXP(Me)
    End Sub

    Private Sub cmbColumnType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbColumnType.SelectedIndexChanged
        If cmbColumnType.Text = "TEXT" Then
            txtColumnSize.Enabled = True
            txtColumnSize.Text = 50
        Else
            txtColumnSize.Enabled = False
            txtColumnSize.Text = 0
        End If

        ep.SetError(cmbColumnType, "")
    End Sub

    Private Sub cmdAddWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddWhere.Click

        Dim sName As String

        If cmbColumnType.Text = "TEXT" And txtColumnSize.Text = "0" Then
            ep.SetError(txtColumnSize, "Please set the correct column size")
            txtColumnSize.Focus()
            Return
        ElseIf txtColumnName.Text = "" Then
            ep.SetError(txtColumnName, "Please enter the column name")
            txtColumnName.Focus()
            Return
        ElseIf cmbColumnType.Text = "" Then
            ep.SetError(cmbColumnType, "Please select the column")
            cmbColumnType.Focus()
            Return
        Else
            For Each lsv As ListViewItem In lsvColumns.Items
                If txtColumnName.Text.ToLower = lsv.Text.ToLower Then
                    ep.SetError(txtColumnName, "The column name already exists in the table definition")
                    txtColumnName.Focus()
                    Return
                End If
            Next
        End If

        If txtColumnName.Text.IndexOf(" ") > 0 Then
            sName = "[" & txtColumnName.Text.ToUpper & "]"
        Else
            sName = txtColumnName.Text.ToUpper
        End If

        Select Case cmbColumnType.Text
            Case "TEXT"
                lsvColumns.Items.Add(sName & " VARCHAR(" & txtColumnSize.Text & ")")
            Case "NUMBER"
                lsvColumns.Items.Add(sName & " INTEGER")
            Case "DECIMAL"
                lsvColumns.Items.Add(sName & " FLOAT")
            Case "DATETIME"
                lsvColumns.Items.Add(sName & " DATETIME")
            Case "MEMO"
                lsvColumns.Items.Add(sName & " TEXT")
        End Select
    End Sub

    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text = "" Then
            ep.SetError(txtName, "Please enter the task name")
            txtName.Focus()
            Return
        ElseIf txtFinal.Text = "" Then
            ep.SetError(txtFinal, "Please build the query")
            txtFinal.Focus()
            Return
        End If

        Me.Close()
    End Sub

    Private Sub cmdRemoveWhere_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveWhere.Click
        If lsvColumns.SelectedItems.Count = 0 Then Return

        Dim lsv As ListViewItem = lsvColumns.SelectedItems(0)

        lsv.Remove()
    End Sub

    Public Sub AddTask(ByVal ScheduleID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Me.ShowDialog()

        If UserCancel = True Then Exit Sub

        Dim SQL As String
        Dim sVals As String
        Dim sCols As String
        Dim oData As clsMarsData = New clsMarsData
        Dim oTask As clsMarsTask = New clsMarsTask
        Dim sCon As String
        Dim sValues As String
        Dim nID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

        'Connection String -> ProgramParameters
        'New Table Name -> ProgramPath 
        'Columns for Table -> SendTo
        'Final SQL -> Msg

        sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
            UcDSN.txtPassword.Text & "|"

        For Each lsv As ListViewItem In lsvColumns.Items
            If lsv.Text <> "" Then sValues &= lsv.Text & "|"
        Next

        sCols = "TaskID,ScheduleID,TaskType,TaskName,ProgramParameters,ProgramPath,SendTo,Msg,OrderID"

        sVals = nID & "," & _
            ScheduleID & "," & _
            "'DBCreateTable'," & _
            "'" & SQLPrepare(txtName.Text) & "'," & _
            "'" & SQLPrepare(sCon) & "'," & _
            "'" & SQLPrepare(txtTable.Text) & "'," & _
            "'" & SQLPrepare(sValues) & "'," & _
            "'" & SQLPrepare(txtFinal.Text) & "'," & _
            oTask.GetOrderID(ScheduleID)

        SQL = "INSERT INTO Tasks(" & sCols & ") VALUES (" & sVals & ")"

        clsMarsData.WriteData(SQL)

        'set when the task will be executed
        _Delay(0.5)

        Dim oRunWhen As New frmTaskRunWhen

        If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nID)
        'basi
    End Sub

    Public Sub EditTask(ByVal nTaskID As Integer, _
    Optional ByVal ShowAfter As Boolean = True)
        Dim SQL As String
        Dim oData As clsMarsData = New clsMarsData
        Dim sCon As String
        Dim sInserts As String

        SQL = "SELECT * FROM Tasks WHERE TaskID = " & nTaskID

        Dim oRs As ADODB.Recordset = clsmarsdata.GetData(SQL)

        'Connection String -> ProgramParameters
        'New Table Name -> ProgramPath 
        'Columns for Table -> SendTo
        'Final SQL -> Msg

        If Not oRs Is Nothing And oRs.EOF = False Then
            txtName.Text = oRs("taskname").Value
            sCon = oRs("programparameters").Value

            UcDSN.cmbDSN.Text = GetDelimitedWord(sCon, 1, "|")
            UcDSN.txtUserID.Text = GetDelimitedWord(sCon, 2, "|")
            UcDSN.txtPassword.Text = GetDelimitedWord(sCon, 3, "|")

            txtTable.Text = oRs("programpath").Value

            sInserts = oRs("sendto").Value

            For Each s As String In sInserts.Split("|")
                If s.Length > 0 Then
                    Dim lsv As ListViewItem = lsvColumns.Items.Add(s)
                End If
            Next

            txtFinal.Text = oRs("msg").Value

            oRs.Close()

            Me.ShowDialog()

            If UserCancel = True Then Return

            sInserts = ""

            sCon = UcDSN.cmbDSN.Text & "|" & UcDSN.txtUserID.Text & "|" & _
                UcDSN.txtPassword.Text & "|"

            For Each lsv As ListViewItem In lsvColumns.Items
                If lsv.Text <> "" Then sInserts &= lsv.Text & "|"
            Next

            SQL = "TaskName = '" & SQLPrepare(txtName.Text) & "'," & _
                "ProgramParameters = '" & SQLPrepare(sCon) & "'," & _
                "ProgramPath = '" & SQLPrepare(txtTable.Text) & "'," & _
                "SendTo = '" & SQLPrepare(sInserts) & "'," & _
                "Msg = '" & SQLPrepare(txtFinal.Text) & "'"

            SQL = "UPDATE Tasks SET " & SQL & " WHERE TaskID = " & nTaskID

            clsMarsData.WriteData(SQL)

            'for editing a task, set its run type
            ' _Delay(2)

            Dim oRunWhen As New frmTaskRunWhen

            If ShowAfter = True Then oRunWhen.SetTaskRunWhen(nTaskID)
        End If
    End Sub


    Private Sub cmdNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtFinal.Text.Length > 0 Then
                    If MessageBox.Show("Going through the steps will overwrite your custom query. " & Environment.NewLine & _
                    "Would you like to edit your query directly now?", Application.ProductName, MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                        cmdSkip_Click(sender, e)
                        Return
                    End If
                End If

                cmdBack.Enabled = True
                Page2.BringToFront()
            Case 2
                If lsvColumns.Items.Count = 0 Then
                    ep.SetError(lsvColumns, "Please add the table definitions")
                    Return
                End If

                Page3.BringToFront()

                cmdNext.Enabled = False
                cmdOK.Enabled = True

                txtFinal.Text = "CREATE TABLE " & txtTable.Text & " ("

                For Each lsv As ListViewItem In lsvColumns.Items
                    txtFinal.Text &= lsv.Text & ","
                Next

                txtFinal.Text = txtFinal.Text.Substring(0, txtFinal.Text.Length - 1)

                txtFinal.Text &= ")"

                txtFinal.Text = txtFinal.Text.ToUpper

        End Select

        nStep += 1
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdBack.Click

        Select Case nStep
            Case 3
                cmdNext.Enabled = True
                cmdOK.Enabled = False
                Page2.BringToFront()
            Case 2
                Page1.BringToFront()
                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        UserCancel = True
    End Sub

    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged
        ep.SetError(txtName, "")
    End Sub

    Private Sub txtColumnName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtColumnName.TextChanged
        ep.SetError(txtColumnName, "")
    End Sub

    Private Sub txtColumnSize_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtColumnSize.ValueChanged
        ep.SetError(txtColumnSize, "")
    End Sub

    Private Sub txtFinal_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFinal.TextChanged
        ep.SetError(txtFinal, "")
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        If UcDSN._Validate = True Then
            cmdNext.Enabled = True
            cmdSkip.Enabled = True
        Else
            cmdNext.Enabled = False
        End If
    End Sub

    Private Sub cmdSkip_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdSkip.Click
        cmdBack.Enabled = True
        Page3.BringToFront()
        cmdNext.Enabled = False
        nStep = 3
        cmdOK.Enabled = True
    End Sub
End Class
