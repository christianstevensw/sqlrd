

Public Class frmSMTPAdvanced
    Dim err As New ErrorProvider
    Dim m_userCancel As Boolean = True

    Private Sub chkRequiresPop_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRequiresPop.CheckedChanged
        Me.grpPop.Enabled = Me.chkRequiresPop.Checked

        Me.btnOK.Enabled = Not Me.chkRequiresPop.Checked
    End Sub

    Private Sub frmSMTPAdvanced_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        If Me.txtPOPServer.Text = "" Then
            err.SetError(Me.txtPOPServer, "Please enter the POP3 server address")
            Me.txtPOPServer.Focus()
            Return
        ElseIf Me.txtPopUser.Text = "" Then
            err.SetError(Me.txtPopUser, "Please enter the POP3 UserID")
            Me.txtPopUser.Focus()
            Return
        ElseIf Me.txtPopPassword.Text = "" Then
            err.SetError(Me.txtPopPassword, "Please provide the POP3 server password")
            Me.txtPopPassword.Focus()
            Return
        ElseIf Me.txtPopPort.Text = "" Then
            err.SetError(Me.txtPopPort, "Please enter the POP3 server port")
            Me.txtPopPort.Focus()
            Return
        End If

        If clsMarsMessaging.Pop3Login(Me.txtPopUser.Text, Me.txtPopPassword.Text, Me.txtPOPServer.Text, _
        Me.txtPopPort.Text, Me.chkPOPSSL.Checked) = False Then
            err.SetError(Me.txtPOPServer, "Could not log into the POP3 server. Please check the address and credentials provided.")
            Me.txtPOPServer.Focus()
        Else
            Me.btnOK.Enabled = True
            err.SetError(Me.txtPopPassword, "")
            err.SetError(Me.txtPopPort, "")
            err.SetError(Me.txtPOPServer, "")
            err.SetError(Me.txtPopPort, "")
        End If
        
    End Sub

    Public Function SMTPAdvanced(ByVal backupServer As Boolean, Optional ByRef tblValues As DataTable = Nothing) As Boolean

        If backupServer = False Then
            With clsMarsUI.MainUI
                Me.chkRequiresPop.Checked = Convert.ToInt32(.ReadRegistry("RequirePOPLogin", 0))
                Me.txtPOPServer.Text = .ReadRegistry("SMTPPOPServer", "")
                Me.txtPopUser.Text = .ReadRegistry("SMTPPOPUser", "")
                Me.txtPopPassword.Text = .ReadRegistry("SMTPPOPPassword", "", True)
                Me.txtPopPort.Text = .ReadRegistry("SMTPPOPPort", 110)
                Me.chkPOPSSL.Checked = Convert.ToInt32(.ReadRegistry("SMTPPOPSSL", 0))
                Me.chkSmtpSSL.Checked = Convert.ToInt32(.ReadRegistry("SMTPSSL", 0))
                Me.chkSmtpStartTLS.Checked = Convert.ToInt32(.ReadRegistry("SMTPStartTLS", 0))
                Me.cmbSmtpAuthMode.Text = .ReadRegistry("SMTPAuthMode", "LOGIN")
            End With
        Else
            If tblValues IsNot Nothing Then
                Dim row As DataRow = tblValues.Rows(0)

                Me.chkRequiresPop.Checked = row("smtppoplogin")
                Me.txtPOPServer.Text = row("smtppopserver")
                Me.txtPopUser.Text = row("smtppopuser")
                Me.txtPopPassword.Text = row("smtppoppassword")
                Me.txtPopPort.Text = row("smtppopport")
                Me.chkPOPSSL.Checked = row("smtppopssl")
                Me.chkSmtpSSL.Checked = row("smtpssl")
                Me.chkSmtpStartTLS.Checked = row("smtpstarttls")
                Me.cmbSmtpAuthMode.Text = row("smtpauthmode")
            End If
        End If

        Me.ShowDialog()

        If Me.m_userCancel = True Then
            Return False
        End If

        If backupServer = False Then
            With clsMarsUI.MainUI
                .SaveRegistry("RequirePOPLogin", Convert.ToInt32(Me.chkRequiresPop.Checked))
                .SaveRegistry("SMTPPOPServer", Me.txtPOPServer.Text)
                .SaveRegistry("SMTPPOPUser", Me.txtPopUser.Text)
                .SaveRegistry("SMTPPOPPassword", Me.txtPopPassword.Text, True)
                .SaveRegistry("SMTPPOPPort", Me.txtPopPort.Text)
                .SaveRegistry("SMTPPOPSSL", Convert.ToInt32(Me.chkPOPSSL.Checked))
                .SaveRegistry("SMTPSSL", Convert.ToInt32(Me.chkSmtpSSL.Checked))
                .SaveRegistry("SMTPStartTLS", Convert.ToInt32(Me.chkSmtpStartTLS.Checked))
                .SaveRegistry("SMTPAuthMode", Me.cmbSmtpAuthMode.Text)
            End With

            Return True
        Else
            tblValues = New DataTable

            With tblValues.Columns
                .Add("smtppoplogin")
                .Add("smtppopserver")
                .Add("smtppopuser")
                .Add("smtppoppassword")
                .Add("smtppopport")
                .Add("smtppopssl")
                .Add("smtpssl")
                .Add("smtpstarttls")
                .Add("smtpauthmode")
            End With

            Dim irow As DataRow = tblValues.Rows.Add

            irow("smtppoplogin") = Convert.ToInt32(Me.chkRequiresPop.Checked)
            irow("smtppopserver") = Me.txtPOPServer.Text
            irow("smtppopuser") = Me.txtPopUser.Text
            irow("smtppoppassword") = Me.txtPopPassword.Text
            irow("smtppopport") = Me.txtPopPort.Text
            irow("smtppopssl") = Convert.ToInt32(Me.chkPOPSSL.Checked)
            irow("smtpssl") = Convert.ToInt32(Me.chkSmtpSSL.Checked)
            irow("smtpstarttls") = Convert.ToInt32(Me.chkSmtpStartTLS.Checked)
            irow("smtpauthmode") = Me.cmbSmtpAuthMode.Text
            Return True
        End If
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Close()
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Me.m_userCancel = False

        Close()
    End Sub

    Private Sub txtPOPServer_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtPOPServer.TextChanged, _
     txtPopPassword.TextChanged, txtPopPort.TextChanged, txtPopUser.TextChanged
        err.SetError(sender, "")

    End Sub

    Private Sub chkPOPSSL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPOPSSL.CheckedChanged
        Me.btnOK.Enabled = False
    End Sub

   
    Private Sub cmbSmtpAuthMode_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cmbSmtpAuthMode.Validating
        If Me.cmbSmtpAuthMode.Text <> "NONE" And Me.cmbSmtpAuthMode.Text <> "LOGIN" And Me.cmbSmtpAuthMode.Text <> "PLAIN" And _
        Me.cmbSmtpAuthMode.Text <> "CRAM-MD5" And Me.cmbSmtpAuthMode.Text <> "NTLM" Then
            Me.cmbSmtpAuthMode.Text = "LOGIN"
        End If
    End Sub
End Class