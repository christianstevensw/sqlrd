Public Class ucErrorHandler

    Public Property m_showFailOnOne() As Boolean
        Get
            Return Me.chkFailonOne.Visible
        End Get
        Set(ByVal value As Boolean)
            Me.chkFailonOne.Visible = value
        End Set
    End Property

    Public Property m_autoFailAfter(Optional ByVal forceWrite As Boolean = False) As String
        Get
            If forceWrite = False Then
                Return IsNonEnglishRegionRead(Me.cmbAssumeFail.Value)
            Else
                Return IsNonEnglishRegionWrite(Me.cmbAssumeFail.Value)
            End If
        End Get

        Set(ByVal value As String)
            Me.cmbAssumeFail.Value = IsNonEnglishRegionWrite(value)
        End Set
    End Property

    Private Sub chkAutoCalc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoCalc.CheckedChanged
        cmbAssumeFail.Enabled = Not (chkAutoCalc.Checked)
    End Sub

    
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.cmbAssumeFail.Maximum = Decimal.MaxValue
    End Sub
End Class
