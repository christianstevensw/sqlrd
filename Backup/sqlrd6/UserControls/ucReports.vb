Imports sqlrd.clsMarsUI


Public Class ucReports

    Dim MasterID As Integer = 99999

    Public Property m_MasterID() As Integer
        Get
            Return MasterID
        End Get
        Set(ByVal value As Integer)
            MasterID = value
        End Set
    End Property

    Public Sub LoadReports()
        Dim SQL As String
        Dim nPackID As Integer = Me.m_MasterID
        Dim oRs As ADODB.Recordset

        SQL = "SELECT * FROM " & _
            "ReportAttr WHERE PackID =" & nPackID

        SQL &= " ORDER BY PackOrderID"

        oRs = clsMarsData.GetData(SQL)


        Dim oListItem As ListViewItem

        If Not oRs Is Nothing Then

            lsvReports.Items.Clear()

            Do While oRs.EOF = False
                oListItem = lsvReports.Items.Add(oRs("reporttitle").Value)

                oListItem.Tag = oRs("reportid").Value

                oListItem.ImageIndex = 0

                oRs.MoveNext()
            Loop
        End If

        oRs.Close()
    End Sub
    Private Sub cmdAddReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAddReport.Click
        Dim sReturn(2) As String
        Dim oNewReport As frmPackedReport

        oNewReport = New frmPackedReport

        oNewReport.m_EventReport = True
        oNewReport.m_eventID = Me.m_MasterID

        sReturn = oNewReport.AddReport(lsvReports.Items.Count + 1, Me.m_MasterID)

        If sReturn(0) = "" Then Exit Sub

        Me.LoadReports()

    End Sub

    Private Sub cmdEditReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdEditReport.Click, lsvReports.DoubleClick
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim oEdit As New frmPackedReport
        Dim sVals() As String

        oEdit.m_EventReport = True

        Dim reportID As Integer = lsvReports.SelectedItems(0).Tag

        sVals = oEdit.EditReport(reportID)

        If Not sVals Is Nothing Then
            Dim oItem As ListViewItem = lsvReports.SelectedItems(0)

            oItem.Text = sVals(0)

            'oItem.SubItems(1).Text = sVals(1)

            lsvReports.Refresh()
        End If
    End Sub

    Private Sub cmdRemoveReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdRemoveReport.Click
        If lsvReports.SelectedItems.Count = 0 Then Exit Sub

        Dim oData As clsMarsData = New clsMarsData

        Dim nReportID As Integer

        Dim lsv As ListViewItem = lsvReports.SelectedItems(0)

        nReportID = lsv.Tag

        Dim oReport As New clsMarsReport

        clsMarsUI.MainUI.DeleteReport(nReportID)

        lsv.Remove()
    End Sub

    Private Sub cmdUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUp.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        If lsvReports.SelectedItems(0).Index = 0 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)

        If olsv.Index = 0 Then Return

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index - 1)

        Dim SQL As String

        Dim MoverID As Integer
        Dim MoverPos As Integer

        Dim MovedID As Integer
        Dim MovedPos As Integer

        MoverID = olsv.Tag
        MoverPos = olsv.Index

        MovedID = olsv2.Tag
        MovedPos = olsv2.Index

        Dim SelTag As String = olsv.Tag

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MovedPos & " WHERE ReportID =" & MoverID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MoverPos & " WHERE ReportID =" & MovedID

        clsMarsData.WriteData(SQL)

        Me.LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    Private Sub cmdDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDown.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        If lsvReports.SelectedItems(0).Index = lsvReports.Items.Count - 1 Then Return

        Dim olsv As ListViewItem = lsvReports.SelectedItems(0)

        Dim olsv2 As ListViewItem = lsvReports.Items(olsv.Index + 1)

        Dim SQL As String

        Dim MoverID As Integer
        Dim MoverPos As Integer

        Dim MovedID As Integer
        Dim MovedPos As Integer

        MoverID = olsv.Tag
        MoverPos = olsv.Index

        MovedID = olsv2.Tag
        MovedPos = olsv2.Index

        Dim SelTag As String = olsv.Tag

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MovedPos & " WHERE ReportID =" & MoverID

        clsMarsData.WriteData(SQL)

        SQL = "UPDATE ReportAttr SET PackOrderID =" & MoverPos & " WHERE ReportID =" & MovedID

        clsMarsData.WriteData(SQL)

        Me.LoadReports()

        For Each olsv In lsvReports.Items
            If olsv.Tag = SelTag Then
                olsv.Selected = True
                Exit For
            End If
        Next

    End Sub

    
    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        If lsvReports.SelectedItems.Count = 0 Then Return

        Dim result As ArrayList = New ArrayList

        For Each item As ListViewItem In lsvReports.SelectedItems
            Dim reportid As Integer = item.Tag

            result.Add(clsMarsReport.oReport.RefreshReport(reportid))
        Next

        If result.IndexOf(False) = -1 Then
            MessageBox.Show("Report(s) refreshed successfully", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class
