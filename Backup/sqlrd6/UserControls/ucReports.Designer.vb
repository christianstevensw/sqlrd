<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucReports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucReports))
        Me.cmdUp = New System.Windows.Forms.Button
        Me.cmdDown = New System.Windows.Forms.Button
        Me.cmdAddReport = New System.Windows.Forms.Button
        Me.lsvReports = New System.Windows.Forms.ListView
        Me.ReportName = New System.Windows.Forms.ColumnHeader
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.cmdRemoveReport = New System.Windows.Forms.Button
        Me.cmdEditReport = New System.Windows.Forms.Button
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cmdUp
        '
        Me.cmdUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdUp.BackColor = System.Drawing.Color.Transparent
        Me.cmdUp.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdUp.Image = CType(resources.GetObject("cmdUp.Image"), System.Drawing.Image)
        Me.cmdUp.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdUp.Location = New System.Drawing.Point(385, 208)
        Me.cmdUp.Name = "cmdUp"
        Me.cmdUp.Size = New System.Drawing.Size(80, 24)
        Me.cmdUp.TabIndex = 15
        Me.cmdUp.UseVisualStyleBackColor = False
        '
        'cmdDown
        '
        Me.cmdDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdDown.BackColor = System.Drawing.Color.Transparent
        Me.cmdDown.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdDown.Image = CType(resources.GetObject("cmdDown.Image"), System.Drawing.Image)
        Me.cmdDown.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDown.Location = New System.Drawing.Point(385, 240)
        Me.cmdDown.Name = "cmdDown"
        Me.cmdDown.Size = New System.Drawing.Size(80, 24)
        Me.cmdDown.TabIndex = 14
        Me.cmdDown.UseVisualStyleBackColor = False
        '
        'cmdAddReport
        '
        Me.cmdAddReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdAddReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdAddReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdAddReport.Image = CType(resources.GetObject("cmdAddReport.Image"), System.Drawing.Image)
        Me.cmdAddReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdAddReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdAddReport.Location = New System.Drawing.Point(385, 3)
        Me.cmdAddReport.Name = "cmdAddReport"
        Me.cmdAddReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdAddReport.TabIndex = 13
        Me.cmdAddReport.Text = "&Add"
        Me.cmdAddReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdAddReport.UseVisualStyleBackColor = False
        '
        'lsvReports
        '
        Me.lsvReports.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvReports.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ReportName})
        Me.lsvReports.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsvReports.ForeColor = System.Drawing.Color.Blue
        Me.lsvReports.FullRowSelect = True
        Me.lsvReports.HideSelection = False
        Me.lsvReports.LargeImageList = Me.ImageList1
        Me.lsvReports.Location = New System.Drawing.Point(3, 3)
        Me.lsvReports.MultiSelect = False
        Me.lsvReports.Name = "lsvReports"
        Me.lsvReports.Size = New System.Drawing.Size(376, 262)
        Me.lsvReports.SmallImageList = Me.ImageList1
        Me.lsvReports.TabIndex = 10
        Me.lsvReports.UseCompatibleStateImageBehavior = False
        Me.lsvReports.View = System.Windows.Forms.View.Details
        '
        'ReportName
        '
        Me.ReportName.Text = "Report Name"
        Me.ReportName.Width = 350
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "document_chart.ico")
        '
        'cmdRemoveReport
        '
        Me.cmdRemoveReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdRemoveReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdRemoveReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdRemoveReport.Image = CType(resources.GetObject("cmdRemoveReport.Image"), System.Drawing.Image)
        Me.cmdRemoveReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdRemoveReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdRemoveReport.Location = New System.Drawing.Point(385, 67)
        Me.cmdRemoveReport.Name = "cmdRemoveReport"
        Me.cmdRemoveReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdRemoveReport.TabIndex = 12
        Me.cmdRemoveReport.Text = "&Delete"
        Me.cmdRemoveReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdRemoveReport.UseVisualStyleBackColor = False
        '
        'cmdEditReport
        '
        Me.cmdEditReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdEditReport.BackColor = System.Drawing.Color.Transparent
        Me.cmdEditReport.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdEditReport.Image = CType(resources.GetObject("cmdEditReport.Image"), System.Drawing.Image)
        Me.cmdEditReport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdEditReport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdEditReport.Location = New System.Drawing.Point(385, 35)
        Me.cmdEditReport.Name = "cmdEditReport"
        Me.cmdEditReport.Size = New System.Drawing.Size(80, 24)
        Me.cmdEditReport.TabIndex = 11
        Me.cmdEditReport.Text = "&Edit"
        Me.cmdEditReport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdEditReport.UseVisualStyleBackColor = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(385, 97)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(80, 23)
        Me.btnRefresh.TabIndex = 16
        Me.btnRefresh.Text = "&Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'ucReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.cmdUp)
        Me.Controls.Add(Me.cmdDown)
        Me.Controls.Add(Me.cmdAddReport)
        Me.Controls.Add(Me.lsvReports)
        Me.Controls.Add(Me.cmdRemoveReport)
        Me.Controls.Add(Me.cmdEditReport)
        Me.Name = "ucReports"
        Me.Size = New System.Drawing.Size(468, 298)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdUp As System.Windows.Forms.Button
    Friend WithEvents cmdDown As System.Windows.Forms.Button
    Friend WithEvents cmdAddReport As System.Windows.Forms.Button
    Friend WithEvents lsvReports As System.Windows.Forms.ListView
    Friend WithEvents ReportName As System.Windows.Forms.ColumnHeader
    Friend WithEvents cmdRemoveReport As System.Windows.Forms.Button
    Friend WithEvents cmdEditReport As System.Windows.Forms.Button
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents btnRefresh As System.Windows.Forms.Button

End Class
