Public Class ucFTPDetails
    Dim ep As ErrorProvider = New ErrorProvider
    Dim ftpSuccess As Boolean
    Dim oField As TextBox
    Dim showBrowse As Boolean = True

    Public m_eventID As Integer = 99999

    Public Property m_ShowBrowse() As Boolean
        Get
            Return showBrowse
        End Get
        Set(ByVal value As Boolean)
            showBrowse = value

            Me.txtDirectory.Visible = value
            Me.btnBrowseFTP.Visible = value
            Label6.Visible = value
        End Set
    End Property
    Public Property m_ftpSuccess() As Boolean
        Get
            Return ftpSuccess
        End Get
        Set(ByVal value As Boolean)
            ftpSuccess = value
        End Set
    End Property

    Public Property m_ftpOptions() As String
        Get
            'use the following template
            'UsePFX;UsePVK;CertFile;KeyFile;CertName;CertPassword;UseProxy;ProxyServer;ProxyUser;ProxyPassword;ProxyPort
            Dim value As String

            value &= "UsePFX=" & Convert.ToInt32(Me.optPFX.Checked) & ";"

            value &= "UsePVK=" & Convert.ToInt32(Me.optPVK.Checked) & ";"

            value &= "CertFile=" & Me.txtCertFile.Text & ";"

            value &= "KeyFile=" & Me.txtPrivateKeyFile.Text & ";"

            value &= "CertName=" & Me.txtCertName.Text & ";"

            value &= "CertPassword=" & _EncryptDBValue(Me.txtCertPassword.Text) & ";"

            value &= "UseProxy=" & Convert.ToInt32(Me.chkProxy.Checked) & ";"

            value &= "ProxyServer=" & Me.txtProxyServer.Text & ";"

            value &= "ProxyUser=" & Me.txtProxyUserName.Text & ";"

            value &= "ProxyPassword=" & _EncryptDBValue(Me.txtProxyPassword.Text) & ";"

            value &= "ProxyPort=" & Me.txtProxyPort.Value & ";"

            Return value
        End Get

        Set(ByVal value As String)
            For Each entry As String In value.Split(";")
                If entry = "" Or entry Is Nothing Then Continue For

                Dim propName As String = entry.Split("=")(0)
                Dim propValue As String = entry.Split("=")(1)

                Select Case propName.ToLower
                    Case "usepfx"
                        Me.optPFX.Checked = Convert.ToInt32(propValue)
                    Case "usepvk"
                        Me.optPVK.Checked = Convert.ToInt32(propValue)
                    Case "certfile"
                        Me.txtCertFile.Text = propValue
                    Case "keyfile"
                        Me.txtPrivateKeyFile.Text = propValue
                    Case "certname"
                        Me.txtCertName.Text = propValue
                    Case "certpassword"
                        Me.txtCertPassword.Text = _DecryptDBValue(propValue)
                    Case "useproxy"
                        Me.chkProxy.Checked = Convert.ToInt32(propValue)
                    Case "proxyserver"
                        Me.txtProxyServer.Text = propValue
                    Case "proxyuser"
                        Me.txtProxyUserName.Text = propValue
                    Case "proxypassword"
                        Me.txtProxyPassword.Text = _DecryptDBValue(propValue)
                    Case "proxyport"
                        Me.txtProxyPort.Value = propValue
                End Select
            Next
        End Set

    End Property

    Public Function validateFtpInfo(Optional ByVal IsDynamic As Boolean = False) As Boolean
        If Me.txtFTPServer.Text = "" And IsDynamic = False Then
            ep.SetError(Me.txtFTPServer, "Missing server information...")
            Me.txtFTPServer.Focus()
            Return False
        ElseIf Me.txtUserName.Text = "" Then
            ep.SetError(Me.txtUserName, "Please enter the username")
            txtUserName.Focus()
            Return False
        ElseIf Me.txtPassword.Text = "" Then
            ep.SetError(Me.txtPassword, "Please enter the password")
            Me.txtPassword.Focus()
            Return False
        ElseIf Me.chkProxy.Checked And Me.txtProxyServer.Text = "" Then
            ep.SetError(Me.txtProxyServer, "Please enter the Proxy server")
            Me.txtProxyServer.Focus()
            Return False
        End If

        Return True
    End Function
    Public Sub ResetAll()
        Try
            For Each page As TabPage In Me.tbFTP.TabPages
                For Each ctrl As Control In page.Controls

                    If TypeOf ctrl Is TextBox Then
                        Dim txt As TextBox = CType(ctrl, TextBox)

                        txt.Text = ""
                    ElseIf TypeOf ctrl Is CheckBox Then
                        Dim chk As CheckBox = CType(ctrl, CheckBox)

                        chk.Checked = False
                    ElseIf TypeOf ctrl Is NumericUpDown Then
                        Dim nm As NumericUpDown = CType(ctrl, NumericUpDown)

                        nm.Value = 21
                    End If
                Next
            Next
            cmbFTPType.SelectedIndex = 0
        Catch : End Try
    End Sub
    Public Function ConnectSFTP() As Rebex.Net.Sftp
        Try
            Dim ftp As Rebex.Net.Sftp = New Rebex.Net.Sftp

            ftp.Connect(txtFTPServer.Text)

            If chkProxy.Checked Then
                Dim cred As System.Net.NetworkCredential = New System.Net.NetworkCredential
                cred.UserName = Me.txtProxyUserName.Text
                cred.Password = Me.txtProxyPassword.Text

                ftp.Proxy = New Rebex.Net.Proxy(Rebex.Net.ProxyType.HttpConnect, Rebex.Net.ProxyAuthentication.Basic, Me.txtProxyServer.Text, _
                Me.txtProxyPort.Value, cred)
            End If

            ftp.Login(txtUserName.Text, txtPassword.Text)

            Return ftp
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl, "Please check the provided credentials and server address")
            Return Nothing
        End Try
    End Function
    Public Function ConnectFTP() As Object 'Chilkat.Ftp2
        Try
            Dim ftp As Chilkat.Ftp2 = New Chilkat.Ftp2

            Dim ok As Boolean = ftp.UnlockComponent("CHRISTFTP_QZc6MEfg5UnY")

            Select Case cmbFTPType.Text
                Case "FTP - SSL 3.1 (TLS)"
                    ftp.AuthTls = True
                    ftp.Ssl = False
                Case "FTP - SSL 3.0"
                    ftp.Ssl = True
                    ftp.AuthTls = False
                Case "FTP - SSL 3.0 (Implicit)"
                    ftp.Ssl = True
                    ftp.AuthTls = False
                Case "FTP - SSH (FTPS)"
                    Return ConnectSFTP()
            End Select

            ftp.Hostname = txtFTPServer.Text
            ftp.Username = txtUserName.Text
            ftp.Password = txtPassword.Text
            ftp.Passive = Me.chkPassive.Checked
            ftp.Port = Me.txtPort.Value

            If Me.optPVK.Checked And Me.txtCertFile.Text <> "" Then
                Dim certStore As New Chilkat.CertStore()
                Dim password As String = Me.txtCertPassword.Text

                '  Load the certs from a PFX into an in-memory certificate store:
                Dim success As Boolean = certStore.LoadPfxFile(Me.txtCertFile.Text, password)

                If (success <> True) Then
                    Throw New Exception(certStore.LastErrorText)
                End If

                '  Find the exact cert we'll use:
                Dim cert As Chilkat.Cert
                cert = certStore.FindCertBySubject(Me.txtCertName.Text)

                If (cert Is Nothing) Then
                    Throw New Exception("Certificate '" & txtCertName.Text & "' not found!")

                End If

                '  Use this certificate for our secure (SSL/TLS) connection:
                ftp.SetSslClientCert(cert)
            ElseIf Me.optPFX.Checked And Me.txtCertFile.Text <> "" Then
                Dim cert As New Chilkat.Cert()

                '  LoadFromFile will load either PEM and DER formatted files.
                '  It automatically recognizes the file format based on the
                '  file contents.

                Dim success As Boolean = cert.LoadFromFile(Me.txtCertFile.Text)

                If (success <> True) Then
                    Throw New Exception(cert.LastErrorText)
                End If

                Dim password As String = Me.txtCertPassword.Text

                Dim pvk As New Chilkat.PrivateKey()
                success = pvk.LoadPvkFile(Me.txtPrivateKeyFile.Text, password)

                If (success <> True) Then
                    Throw New Exception(pvk.LastErrorText)
                End If

                '  Import the private key to a Windows key container and link
                '  it to the certificate.  (It's OK if the key is already
                '  imported and present in the key container...)
                Dim bForSigning As Boolean
                Dim bForKeyExchange As Boolean
                Dim bMachineKeyset As Boolean
                Dim bNeedPrivateKeyAccess As Boolean
                Dim keyContainerName As String

                '  Choose anything for the key container name.
                keyContainerName = "MyCertForFtp"

                '  We'll import the key to our logged-on user keyset rather
                '  than the machine keyset:
                bMachineKeyset = False

                bNeedPrivateKeyAccess = True

                '  Create a key container and import the private key.
                Dim keyContainer As New Chilkat.KeyContainer()
                success = keyContainer.OpenContainer(keyContainerName, bNeedPrivateKeyAccess, bMachineKeyset)

                If (success <> True) Then
                    success = keyContainer.CreateContainer(keyContainerName, bMachineKeyset)
                End If

                If (success <> True) Then
                    Throw New Exception(keyContainer.LastErrorText)
                End If

                '  Import the private key into the key container.
                '  We're using the key for key exchange, not signing:
                bForKeyExchange = True
                success = keyContainer.ImportPrivateKey(pvk, bForKeyExchange)

                If (success <> True) Then
                    Throw New Exception(keyContainer.LastErrorText)
                End If

                '  Link the cert with the private key in the key container.
                bForSigning = False
                success = cert.LinkPrivateKey(keyContainerName, bMachineKeyset, bForSigning)

                If (success <> True) Then
                    Throw New Exception(cert.LastErrorText)
                End If

                '  The cert now has access to a private key and is ready to be
                '  used...

                '  Use this certificate for our secure (SSL/TLS) connection:
                ftp.SetSslClientCert(cert)
            End If

            If Me.chkProxy.Checked = True And Me.txtProxyServer.Text <> "" Then
                ftp.ProxyHostname = txtProxyServer.Text
                ftp.ProxyPort = txtProxyPort.Value

                '  Note: Your FTP Proxy server may or may not require authentication.
                If Me.txtProxyUserName.Text <> "" Then
                    ftp.ProxyUsername = txtProxyUserName.Text
                    ftp.ProxyPassword = txtProxyPassword.Text
                End If
                '  The ProxyMethod should be an integer value between 1 and 5.
                '  If you know your FTP proxy server's authentication scheme,
                '  you may set it directly.  To determine the ProxyMethod,
                '  call DetermineProxyMethod.  A return value of -1 indicates a failure.
                '  A return value of 0 indicates that nothing worked.
                '  A return value of 1-5 indicates the ProxyMethod that was
                '  successful, and this should be the value used for the ProxyMethod
                '  property.

                Dim pMethod As Long
                pMethod = ftp.DetermineProxyMethod()

                If pMethod = -1 Then
                    ep.SetError(btnVerify, "Could not determine Proxy method. Will continue to connect...")
                End If
            End If

            Dim retry As Integer = 0

            Do
                ok = ftp.Connect

                If ok = False Then System.Threading.Thread.Sleep(2000)

                retry += 1
            Loop Until ok = True Or retry >= 3

            If ftp.AuthTls = True Then ftp.ClearControlChannel()

            If ok = False Then
                Throw New Exception(ftp.LastErrorText)
            Else
                Me.m_ftpSuccess = True
            End If

            Return ftp
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, Erl)
            Return Nothing
        End Try
    End Function
    Private Sub btnVerify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnVerify.Click
        Try
            ep.SetError(btnVerify, "")

            If cmbFTPType.Text = "FTP - SSH (FTPS)" Then
                Dim ftp As Rebex.Net.Sftp = Me.ConnectFTP

                If Me.txtDirectory.Text = "" Then Me.txtDirectory.Text = ftp.GetCurrentDirectory

                ftp.Disconnect()
            Else
                Dim ftp As Chilkat.Ftp2 = Me.ConnectFTP()

                If Me.txtDirectory.Text = "" Then Me.txtDirectory.Text = ftp.GetCurrentRemoteDir

                ftp.Disconnect()
            End If

            MessageBox.Show("Connection tested successfully!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            ep.SetError(btnVerify, ex.Message)
        End Try
    End Sub

    Private Sub cmbFTPType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbFTPType.SelectedIndexChanged
        If cmbFTPType.Text = "FTP" Then
            Me.grpCerts.Enabled = False
            Me.optPFX.Enabled = False
            Me.optPVK.Enabled = False
            Me.txtPort.Value = 21
        ElseIf cmbFTPType.Text = "FTP - SSL 3.0 (Implicit)" Then
            Me.txtPort.Value = 990
        ElseIf cmbFTPType.Text = "FTP - SSH (FTPS)" Then
            Me.txtPort.Value = 22
        Else
            Me.grpCerts.Enabled = True
            Me.optPFX.Enabled = True
            Me.optPVK.Enabled = True
        End If
    End Sub

    Private Sub optPFX_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optPFX.CheckedChanged, optPVK.CheckedChanged
        If optPFX.Checked = True Then
            Me.txtPrivateKeyFile.Enabled = False
            Me.btnBrowse2.Enabled = False
            Me.grpCerts.Enabled = True
        ElseIf Me.optPVK.Checked Then
            Me.txtPrivateKeyFile.Enabled = True
            Me.btnBrowse2.Enabled = True
            Me.grpCerts.Enabled = True
        End If


    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the Certificate File..."

        If Me.optPFX.Checked Then
            ofd.Filter = "Certificate|*.pfx|All|*.*"
        Else
            ofd.Filter = "Certificate|*.crt|All|*.*"
        End If

        ofd.Multiselect = False

        If ofd.ShowDialog <> DialogResult.Cancel Then
            Me.txtCertFile.Text = ofd.FileName
        End If
    End Sub

    Private Sub btnBrowse2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse2.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        ofd.Title = "Please select the Certificate File..."

        ofd.Filter = "Certificate|*.pvk|All|*.*"

        ofd.Multiselect = False

        If ofd.ShowDialog <> DialogResult.Cancel Then
            Me.txtPrivateKeyFile.Text = ofd.FileName
        End If
    End Sub

    Private Sub chkProxy_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkProxy.CheckedChanged
        Me.grpProxy.Enabled = Me.chkProxy.Checked
    End Sub

    Private Sub mnuUndo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuUndo.Click
        Try
            oField.Undo()
        Catch :End Try
    End Sub

    Private Sub txtFTPServer_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFTPServer.GotFocus, txtCertFile.GotFocus, _
     txtCertName.GotFocus, txtCertPassword.GotFocus, txtDirectory.GotFocus, txtFTPServer.GotFocus, txtPassword.GotFocus, _
      txtPrivateKeyFile.GotFocus, txtProxyPassword.GotFocus, txtProxyServer.GotFocus, txtProxyUserName.GotFocus, _
     txtUserName.GotFocus

        Try
            oField = CType(sender, TextBox)

            oField.ContextMenu = Me.mnuInserter
        Catch : End Try
    End Sub

    Private Sub mnuCut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCut.Click
        Try
            oField.Cut()
        Catch : End Try
    End Sub

    Private Sub mnuCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCopy.Click
        Try
            oField.Copy()
        Catch : End Try
    End Sub

    Private Sub mnuPaste_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPaste.Click
        Try
            oField.Paste()
        Catch : End Try
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Try
            oField.SelectedText = ""
        Catch : End Try
    End Sub

    Private Sub mnuSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectAll.Click
        Try
            oField.SelectAll()
        Catch : End Try
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Try
            Dim oInsert As frmInserter = New frmInserter(Me.m_eventID)

            oField.SelectedText = oInsert.GetConstants()
        Catch : End Try
    End Sub

    Private Sub mnuDatabase_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDatabase.Click
        On Error Resume Next

        Dim oData As New frmDataItems

        'Dim box As TextBox

        'box = CType(Me.ActiveControl, TextBox)

        oField.SelectedText = oData._GetDataItem(Me.m_eventID)

    End Sub

    Private Sub txtFTPServer_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFTPServer.TextChanged, txtPassword.TextChanged, _
    txtUserName.TextChanged, txtProxyServer.TextChanged
        ep.SetError(sender, "")
    End Sub

    Private Sub btnBrowseFTP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseFTP.Click
        Dim oFtp As frmFTPBrowser2 = New frmFTPBrowser2

        Dim sReturn As String() = oFtp.GetFtpInfo(Me.ConnectFTP)

        If sReturn IsNot Nothing Then
            Me.txtDirectory.Text = sReturn(0)
        End If
    End Sub
End Class
