<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucFTPDetails
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.btnBrowseFTP = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtFTPServer = New System.Windows.Forms.TextBox
        Me.txtUserName = New System.Windows.Forms.TextBox
        Me.txtDirectory = New System.Windows.Forms.TextBox
        Me.cmbFTPType = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtPassword = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.txtPort = New System.Windows.Forms.NumericUpDown
        Me.btnVerify = New System.Windows.Forms.Button
        Me.chkPassive = New System.Windows.Forms.CheckBox
        Me.tbFTP = New System.Windows.Forms.TabControl
        Me.tbpGeneral = New System.Windows.Forms.TabPage
        Me.tbpCerts = New System.Windows.Forms.TabPage
        Me.optPVK = New System.Windows.Forms.RadioButton
        Me.grpCerts = New System.Windows.Forms.TableLayoutPanel
        Me.btnBrowse2 = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtCertFile = New System.Windows.Forms.TextBox
        Me.btnBrowse = New System.Windows.Forms.Button
        Me.Label9 = New System.Windows.Forms.Label
        Me.txtCertPassword = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.txtCertName = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.txtPrivateKeyFile = New System.Windows.Forms.TextBox
        Me.optPFX = New System.Windows.Forms.RadioButton
        Me.tbpProxy = New System.Windows.Forms.TabPage
        Me.grpProxy = New System.Windows.Forms.TableLayoutPanel
        Me.txtProxyPort = New System.Windows.Forms.NumericUpDown
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.txtProxyServer = New System.Windows.Forms.TextBox
        Me.txtProxyUserName = New System.Windows.Forms.TextBox
        Me.txtProxyPassword = New System.Windows.Forms.TextBox
        Me.chkProxy = New System.Windows.Forms.CheckBox
        Me.mnuInserter = New System.Windows.Forms.ContextMenu
        Me.mnuUndo = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.mnuCut = New System.Windows.Forms.MenuItem
        Me.mnuCopy = New System.Windows.Forms.MenuItem
        Me.mnuPaste = New System.Windows.Forms.MenuItem
        Me.mnuDelete = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.mnuSelectAll = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.mnuDatabase = New System.Windows.Forms.MenuItem
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.txtPort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbFTP.SuspendLayout()
        Me.tbpGeneral.SuspendLayout()
        Me.tbpCerts.SuspendLayout()
        Me.grpCerts.SuspendLayout()
        Me.tbpProxy.SuspendLayout()
        Me.grpProxy.SuspendLayout()
        CType(Me.txtProxyPort, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.4026!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.02597!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.88312!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.42857!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnBrowseFTP, 3, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFTPServer, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtUserName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtDirectory, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.cmbFTPType, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPassword, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPort, 3, 3)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 6)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(385, 110)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnBrowseFTP
        '
        Me.btnBrowseFTP.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowseFTP.Location = New System.Drawing.Point(314, 84)
        Me.btnBrowseFTP.Name = "btnBrowseFTP"
        Me.btnBrowseFTP.Size = New System.Drawing.Size(68, 23)
        Me.btnBrowseFTP.TabIndex = 6
        Me.btnBrowseFTP.Text = "&Browse"
        Me.btnBrowseFTP.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 27)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "FTP Server"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.Location = New System.Drawing.Point(3, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 27)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "User Name"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label4.Location = New System.Drawing.Point(3, 54)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(61, 27)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "FTP Type"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label6
        '
        Me.Label6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.Location = New System.Drawing.Point(3, 81)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 31)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "Remote Directory"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtFTPServer
        '
        Me.txtFTPServer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtFTPServer, 3)
        Me.txtFTPServer.Location = New System.Drawing.Point(70, 3)
        Me.txtFTPServer.Name = "txtFTPServer"
        Me.txtFTPServer.Size = New System.Drawing.Size(312, 21)
        Me.txtFTPServer.TabIndex = 0
        '
        'txtUserName
        '
        Me.txtUserName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUserName.Location = New System.Drawing.Point(70, 30)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(125, 21)
        Me.txtUserName.TabIndex = 1
        '
        'txtDirectory
        '
        Me.txtDirectory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.SetColumnSpan(Me.txtDirectory, 2)
        Me.txtDirectory.Location = New System.Drawing.Point(70, 84)
        Me.txtDirectory.Name = "txtDirectory"
        Me.txtDirectory.Size = New System.Drawing.Size(190, 21)
        Me.txtDirectory.TabIndex = 5
        '
        'cmbFTPType
        '
        Me.cmbFTPType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbFTPType.FormattingEnabled = True
        Me.cmbFTPType.Items.AddRange(New Object() {"FTP", "FTP - SSL 3.1 (TLS)", "FTP - SSL 3.0", "FTP - SSL 3.0 (Implicit)", "FTP - SSH (FTPS)"})
        Me.cmbFTPType.Location = New System.Drawing.Point(70, 57)
        Me.cmbFTPType.Name = "cmbFTPType"
        Me.cmbFTPType.Size = New System.Drawing.Size(125, 21)
        Me.cmbFTPType.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.Location = New System.Drawing.Point(201, 27)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 27)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPassword
        '
        Me.txtPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPassword.Location = New System.Drawing.Point(266, 30)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPassword.Size = New System.Drawing.Size(116, 21)
        Me.txtPassword.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Location = New System.Drawing.Point(201, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 27)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "Port"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(266, 57)
        Me.txtPort.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(68, 21)
        Me.txtPort.TabIndex = 4
        Me.txtPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtPort.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'btnVerify
        '
        Me.btnVerify.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVerify.Location = New System.Drawing.Point(320, 121)
        Me.btnVerify.Name = "btnVerify"
        Me.btnVerify.Size = New System.Drawing.Size(68, 23)
        Me.btnVerify.TabIndex = 1
        Me.btnVerify.Text = "&Verify"
        Me.btnVerify.UseVisualStyleBackColor = True
        '
        'chkPassive
        '
        Me.chkPassive.AutoSize = True
        Me.chkPassive.Checked = True
        Me.chkPassive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPassive.Location = New System.Drawing.Point(76, 125)
        Me.chkPassive.Name = "chkPassive"
        Me.chkPassive.Size = New System.Drawing.Size(91, 17)
        Me.chkPassive.TabIndex = 0
        Me.chkPassive.Text = "Passive Mode"
        Me.chkPassive.UseVisualStyleBackColor = True
        '
        'tbFTP
        '
        Me.tbFTP.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tbFTP.Controls.Add(Me.tbpGeneral)
        Me.tbFTP.Controls.Add(Me.tbpCerts)
        Me.tbFTP.Controls.Add(Me.tbpProxy)
        Me.tbFTP.Location = New System.Drawing.Point(3, 3)
        Me.tbFTP.Name = "tbFTP"
        Me.tbFTP.SelectedIndex = 0
        Me.tbFTP.Size = New System.Drawing.Size(405, 181)
        Me.tbFTP.TabIndex = 2
        '
        'tbpGeneral
        '
        Me.tbpGeneral.Controls.Add(Me.TableLayoutPanel1)
        Me.tbpGeneral.Controls.Add(Me.btnVerify)
        Me.tbpGeneral.Controls.Add(Me.chkPassive)
        Me.tbpGeneral.Location = New System.Drawing.Point(4, 22)
        Me.tbpGeneral.Name = "tbpGeneral"
        Me.tbpGeneral.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpGeneral.Size = New System.Drawing.Size(397, 155)
        Me.tbpGeneral.TabIndex = 0
        Me.tbpGeneral.Text = "General"
        Me.tbpGeneral.UseVisualStyleBackColor = True
        '
        'tbpCerts
        '
        Me.tbpCerts.Controls.Add(Me.optPVK)
        Me.tbpCerts.Controls.Add(Me.grpCerts)
        Me.tbpCerts.Controls.Add(Me.optPFX)
        Me.tbpCerts.Location = New System.Drawing.Point(4, 22)
        Me.tbpCerts.Name = "tbpCerts"
        Me.tbpCerts.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpCerts.Size = New System.Drawing.Size(397, 155)
        Me.tbpCerts.TabIndex = 1
        Me.tbpCerts.Text = "Certificate Settings"
        Me.tbpCerts.UseVisualStyleBackColor = True
        '
        'optPVK
        '
        Me.optPVK.AutoSize = True
        Me.optPVK.Location = New System.Drawing.Point(171, 6)
        Me.optPVK.Name = "optPVK"
        Me.optPVK.Size = New System.Drawing.Size(133, 17)
        Me.optPVK.TabIndex = 1
        Me.optPVK.TabStop = True
        Me.optPVK.Text = "Use Private Certificate"
        Me.optPVK.UseVisualStyleBackColor = True
        '
        'grpCerts
        '
        Me.grpCerts.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpCerts.ColumnCount = 3
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.47945!))
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.17809!))
        Me.grpCerts.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.34247!))
        Me.grpCerts.Controls.Add(Me.btnBrowse2, 2, 1)
        Me.grpCerts.Controls.Add(Me.Label7, 0, 0)
        Me.grpCerts.Controls.Add(Me.txtCertFile, 1, 0)
        Me.grpCerts.Controls.Add(Me.btnBrowse, 2, 0)
        Me.grpCerts.Controls.Add(Me.Label9, 0, 3)
        Me.grpCerts.Controls.Add(Me.txtCertPassword, 1, 3)
        Me.grpCerts.Controls.Add(Me.Label8, 0, 2)
        Me.grpCerts.Controls.Add(Me.txtCertName, 1, 2)
        Me.grpCerts.Controls.Add(Me.Label10, 0, 1)
        Me.grpCerts.Controls.Add(Me.txtPrivateKeyFile, 1, 1)
        Me.grpCerts.Location = New System.Drawing.Point(6, 29)
        Me.grpCerts.Name = "grpCerts"
        Me.grpCerts.RowCount = 4
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpCerts.Size = New System.Drawing.Size(364, 100)
        Me.grpCerts.TabIndex = 3
        '
        'btnBrowse2
        '
        Me.btnBrowse2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse2.Location = New System.Drawing.Point(324, 28)
        Me.btnBrowse2.Name = "btnBrowse2"
        Me.btnBrowse2.Size = New System.Drawing.Size(37, 19)
        Me.btnBrowse2.TabIndex = 1
        Me.btnBrowse2.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.Location = New System.Drawing.Point(3, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(86, 25)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "Certificate File"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCertFile
        '
        Me.txtCertFile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtCertFile.Location = New System.Drawing.Point(95, 3)
        Me.txtCertFile.Name = "txtCertFile"
        Me.txtCertFile.Size = New System.Drawing.Size(209, 21)
        Me.txtCertFile.TabIndex = 0
        '
        'btnBrowse
        '
        Me.btnBrowse.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBrowse.Location = New System.Drawing.Point(324, 3)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(37, 19)
        Me.btnBrowse.TabIndex = 0
        Me.btnBrowse.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label9.Location = New System.Drawing.Point(3, 75)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 25)
        Me.Label9.TabIndex = 2
        Me.Label9.Text = "Password"
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCertPassword
        '
        Me.txtCertPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpCerts.SetColumnSpan(Me.txtCertPassword, 2)
        Me.txtCertPassword.Location = New System.Drawing.Point(95, 78)
        Me.txtCertPassword.Name = "txtCertPassword"
        Me.txtCertPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtCertPassword.Size = New System.Drawing.Size(266, 21)
        Me.txtCertPassword.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.Location = New System.Drawing.Point(3, 50)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(86, 25)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "Certificate Name"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtCertName
        '
        Me.txtCertName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpCerts.SetColumnSpan(Me.txtCertName, 2)
        Me.txtCertName.Location = New System.Drawing.Point(95, 53)
        Me.txtCertName.Name = "txtCertName"
        Me.txtCertName.Size = New System.Drawing.Size(266, 21)
        Me.txtCertName.TabIndex = 2
        '
        'Label10
        '
        Me.Label10.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label10.Location = New System.Drawing.Point(3, 25)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(86, 25)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "Private Key File"
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPrivateKeyFile
        '
        Me.txtPrivateKeyFile.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPrivateKeyFile.Location = New System.Drawing.Point(95, 28)
        Me.txtPrivateKeyFile.Name = "txtPrivateKeyFile"
        Me.txtPrivateKeyFile.Size = New System.Drawing.Size(209, 21)
        Me.txtPrivateKeyFile.TabIndex = 1
        '
        'optPFX
        '
        Me.optPFX.AutoSize = True
        Me.optPFX.Location = New System.Drawing.Point(6, 6)
        Me.optPFX.Name = "optPFX"
        Me.optPFX.Size = New System.Drawing.Size(155, 17)
        Me.optPFX.TabIndex = 0
        Me.optPFX.TabStop = True
        Me.optPFX.Text = "Use Client Certificate (PFX)"
        Me.optPFX.UseVisualStyleBackColor = True
        '
        'tbpProxy
        '
        Me.tbpProxy.Controls.Add(Me.grpProxy)
        Me.tbpProxy.Controls.Add(Me.chkProxy)
        Me.tbpProxy.Location = New System.Drawing.Point(4, 22)
        Me.tbpProxy.Name = "tbpProxy"
        Me.tbpProxy.Padding = New System.Windows.Forms.Padding(3)
        Me.tbpProxy.Size = New System.Drawing.Size(397, 155)
        Me.tbpProxy.TabIndex = 2
        Me.tbpProxy.Text = "Proxy Server"
        Me.tbpProxy.UseVisualStyleBackColor = True
        '
        'grpProxy
        '
        Me.grpProxy.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grpProxy.ColumnCount = 2
        Me.grpProxy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.88119!))
        Me.grpProxy.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.11881!))
        Me.grpProxy.Controls.Add(Me.txtProxyPort, 1, 3)
        Me.grpProxy.Controls.Add(Me.Label11, 0, 0)
        Me.grpProxy.Controls.Add(Me.Label12, 0, 1)
        Me.grpProxy.Controls.Add(Me.Label13, 0, 2)
        Me.grpProxy.Controls.Add(Me.Label14, 0, 3)
        Me.grpProxy.Controls.Add(Me.txtProxyServer, 1, 0)
        Me.grpProxy.Controls.Add(Me.txtProxyUserName, 1, 1)
        Me.grpProxy.Controls.Add(Me.txtProxyPassword, 1, 2)
        Me.grpProxy.Enabled = False
        Me.grpProxy.Location = New System.Drawing.Point(6, 29)
        Me.grpProxy.Name = "grpProxy"
        Me.grpProxy.RowCount = 4
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.grpProxy.Size = New System.Drawing.Size(385, 100)
        Me.grpProxy.TabIndex = 3
        '
        'txtProxyPort
        '
        Me.txtProxyPort.Location = New System.Drawing.Point(144, 78)
        Me.txtProxyPort.Maximum = New Decimal(New Integer() {999999, 0, 0, 0})
        Me.txtProxyPort.Name = "txtProxyPort"
        Me.txtProxyPort.Size = New System.Drawing.Size(68, 21)
        Me.txtProxyPort.TabIndex = 3
        Me.txtProxyPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtProxyPort.Value = New Decimal(New Integer() {21, 0, 0, 0})
        '
        'Label11
        '
        Me.Label11.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(135, 25)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "Proxy Server"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(3, 25)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(135, 25)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "User Name (optional)"
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(3, 50)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(135, 25)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Password (optional)"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 75)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(135, 25)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Port"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtProxyServer
        '
        Me.txtProxyServer.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyServer.Location = New System.Drawing.Point(144, 3)
        Me.txtProxyServer.Name = "txtProxyServer"
        Me.txtProxyServer.Size = New System.Drawing.Size(238, 21)
        Me.txtProxyServer.TabIndex = 0
        '
        'txtProxyUserName
        '
        Me.txtProxyUserName.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyUserName.Location = New System.Drawing.Point(144, 28)
        Me.txtProxyUserName.Name = "txtProxyUserName"
        Me.txtProxyUserName.Size = New System.Drawing.Size(238, 21)
        Me.txtProxyUserName.TabIndex = 1
        '
        'txtProxyPassword
        '
        Me.txtProxyPassword.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtProxyPassword.Location = New System.Drawing.Point(144, 53)
        Me.txtProxyPassword.Name = "txtProxyPassword"
        Me.txtProxyPassword.Size = New System.Drawing.Size(238, 21)
        Me.txtProxyPassword.TabIndex = 2
        '
        'chkProxy
        '
        Me.chkProxy.AutoSize = True
        Me.chkProxy.Location = New System.Drawing.Point(6, 6)
        Me.chkProxy.Name = "chkProxy"
        Me.chkProxy.Size = New System.Drawing.Size(110, 17)
        Me.chkProxy.TabIndex = 0
        Me.chkProxy.Text = "Use Proxy Server"
        Me.chkProxy.UseVisualStyleBackColor = True
        '
        'mnuInserter
        '
        Me.mnuInserter.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuUndo, Me.MenuItem3, Me.mnuCut, Me.mnuCopy, Me.mnuPaste, Me.mnuDelete, Me.MenuItem6, Me.mnuSelectAll, Me.MenuItem7, Me.MenuItem1})
        '
        'mnuUndo
        '
        Me.mnuUndo.Index = 0
        Me.mnuUndo.Text = "Undo"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "-"
        '
        'mnuCut
        '
        Me.mnuCut.Index = 2
        Me.mnuCut.Text = "Cut"
        '
        'mnuCopy
        '
        Me.mnuCopy.Index = 3
        Me.mnuCopy.Text = "Copy"
        '
        'mnuPaste
        '
        Me.mnuPaste.Index = 4
        Me.mnuPaste.Text = "Paste"
        '
        'mnuDelete
        '
        Me.mnuDelete.Index = 5
        Me.mnuDelete.Text = "Delete"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 6
        Me.MenuItem6.Text = "-"
        '
        'mnuSelectAll
        '
        Me.mnuSelectAll.Index = 7
        Me.mnuSelectAll.Text = "Select All"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 8
        Me.MenuItem7.Text = "-"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 9
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem4, Me.mnuDatabase})
        Me.MenuItem1.Text = "Insert"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Constants"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 1
        Me.MenuItem4.Text = "-"
        '
        'mnuDatabase
        '
        Me.mnuDatabase.Index = 2
        Me.mnuDatabase.Text = "Database Field"
        '
        'ucFTPDetails
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.Controls.Add(Me.tbFTP)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucFTPDetails"
        Me.Size = New System.Drawing.Size(411, 190)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.txtPort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbFTP.ResumeLayout(False)
        Me.tbpGeneral.ResumeLayout(False)
        Me.tbpGeneral.PerformLayout()
        Me.tbpCerts.ResumeLayout(False)
        Me.tbpCerts.PerformLayout()
        Me.grpCerts.ResumeLayout(False)
        Me.grpCerts.PerformLayout()
        Me.tbpProxy.ResumeLayout(False)
        Me.tbpProxy.PerformLayout()
        Me.grpProxy.ResumeLayout(False)
        Me.grpProxy.PerformLayout()
        CType(Me.txtProxyPort, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtFTPServer As System.Windows.Forms.TextBox
    Friend WithEvents txtUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtDirectory As System.Windows.Forms.TextBox
    Friend WithEvents cmbFTPType As System.Windows.Forms.ComboBox
    Friend WithEvents txtPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents btnVerify As System.Windows.Forms.Button
    Friend WithEvents chkPassive As System.Windows.Forms.CheckBox
    Friend WithEvents tbFTP As System.Windows.Forms.TabControl
    Friend WithEvents tbpGeneral As System.Windows.Forms.TabPage
    Friend WithEvents tbpCerts As System.Windows.Forms.TabPage
    Friend WithEvents optPVK As System.Windows.Forms.RadioButton
    Friend WithEvents optPFX As System.Windows.Forms.RadioButton
    Friend WithEvents grpCerts As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtCertFile As System.Windows.Forms.TextBox
    Friend WithEvents txtCertName As System.Windows.Forms.TextBox
    Friend WithEvents txtCertPassword As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtPrivateKeyFile As System.Windows.Forms.TextBox
    Friend WithEvents btnBrowse2 As System.Windows.Forms.Button
    Friend WithEvents tbpProxy As System.Windows.Forms.TabPage
    Friend WithEvents chkProxy As System.Windows.Forms.CheckBox
    Friend WithEvents grpProxy As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtProxyPort As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtProxyServer As System.Windows.Forms.TextBox
    Friend WithEvents txtProxyUserName As System.Windows.Forms.TextBox
    Friend WithEvents txtProxyPassword As System.Windows.Forms.TextBox
    Friend WithEvents mnuInserter As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuUndo As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCut As System.Windows.Forms.MenuItem
    Friend WithEvents mnuCopy As System.Windows.Forms.MenuItem
    Friend WithEvents mnuPaste As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuDatabase As System.Windows.Forms.MenuItem
    Friend WithEvents btnBrowseFTP As System.Windows.Forms.Button

End Class
