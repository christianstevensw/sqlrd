<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucExistingReports
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucExistingReports))
        Me.pnExisting = New System.Windows.Forms.Panel
        Me.lsvSchedules = New System.Windows.Forms.ListView
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.ExpandableSplitter1 = New DevComponents.DotNetBar.ExpandableSplitter
        Me.tvSchedules = New System.Windows.Forms.TreeView
        Me.imgFolders = New System.Windows.Forms.ImageList(Me.components)
        Me.mnuParameters = New System.Windows.Forms.ContextMenu
        Me.mnuParameter = New System.Windows.Forms.MenuItem
        Me.pnExisting.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnExisting
        '
        Me.pnExisting.BackColor = System.Drawing.Color.Transparent
        Me.pnExisting.Controls.Add(Me.lsvSchedules)
        Me.pnExisting.Controls.Add(Me.Panel1)
        Me.pnExisting.Controls.Add(Me.ExpandableSplitter1)
        Me.pnExisting.Controls.Add(Me.tvSchedules)
        Me.pnExisting.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnExisting.Location = New System.Drawing.Point(0, 0)
        Me.pnExisting.Name = "pnExisting"
        Me.pnExisting.Size = New System.Drawing.Size(570, 368)
        Me.pnExisting.TabIndex = 8
        '
        'lsvSchedules
        '
        Me.lsvSchedules.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader2})
        Me.lsvSchedules.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lsvSchedules.Location = New System.Drawing.Point(227, 0)
        Me.lsvSchedules.Name = "lsvSchedules"
        Me.lsvSchedules.Size = New System.Drawing.Size(343, 368)
        Me.lsvSchedules.TabIndex = 0
        Me.lsvSchedules.UseCompatibleStateImageBehavior = False
        Me.lsvSchedules.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Schedules"
        Me.ColumnHeader2.Width = 175
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnRemove)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(179, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(48, 368)
        Me.Panel1.TabIndex = 3
        '
        'btnRemove
        '
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(6, 140)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(35, 23)
        Me.btnRemove.TabIndex = 0
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(6, 99)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(35, 23)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'ExpandableSplitter1
        '
        Me.ExpandableSplitter1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ExpandableSplitter1.BackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.ExpandableControl = Me.tvSchedules
        Me.ExpandableSplitter1.ExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.ExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.ExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.ExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.GripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.GripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.GripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.HotBackColor = System.Drawing.Color.FromArgb(CType(CType(204, Byte), Integer), CType(CType(229, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotBackColor2 = System.Drawing.Color.Empty
        Me.ExpandableSplitter1.HotBackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.None
        Me.ExpandableSplitter1.HotBackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemCheckedBackground
        Me.ExpandableSplitter1.HotExpandFillColor = System.Drawing.Color.FromArgb(CType(CType(163, Byte), Integer), CType(CType(209, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ExpandableSplitter1.HotExpandFillColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBackground
        Me.ExpandableSplitter1.HotExpandLineColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotExpandLineColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripDarkColor = System.Drawing.SystemColors.Highlight
        Me.ExpandableSplitter1.HotGripDarkColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.ItemPressedBorder
        Me.ExpandableSplitter1.HotGripLightColor = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.ExpandableSplitter1.HotGripLightColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.MenuBackground
        Me.ExpandableSplitter1.Location = New System.Drawing.Point(176, 0)
        Me.ExpandableSplitter1.Name = "ExpandableSplitter1"
        Me.ExpandableSplitter1.Size = New System.Drawing.Size(3, 368)
        Me.ExpandableSplitter1.Style = DevComponents.DotNetBar.eSplitterStyle.Mozilla
        Me.ExpandableSplitter1.TabIndex = 2
        Me.ExpandableSplitter1.TabStop = False
        '
        'tvSchedules
        '
        Me.tvSchedules.Dock = System.Windows.Forms.DockStyle.Left
        Me.tvSchedules.ImageIndex = 0
        Me.tvSchedules.ImageList = Me.imgFolders
        Me.tvSchedules.Location = New System.Drawing.Point(0, 0)
        Me.tvSchedules.Name = "tvSchedules"
        Me.tvSchedules.SelectedImageIndex = 0
        Me.tvSchedules.Size = New System.Drawing.Size(176, 368)
        Me.tvSchedules.TabIndex = 1
        '
        'imgFolders
        '
        Me.imgFolders.ImageStream = CType(resources.GetObject("imgFolders.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imgFolders.TransparentColor = System.Drawing.Color.Transparent
        Me.imgFolders.Images.SetKeyName(0, "")
        Me.imgFolders.Images.SetKeyName(1, "")
        Me.imgFolders.Images.SetKeyName(2, "")
        Me.imgFolders.Images.SetKeyName(3, "")
        Me.imgFolders.Images.SetKeyName(4, "")
        Me.imgFolders.Images.SetKeyName(5, "")
        Me.imgFolders.Images.SetKeyName(6, "")
        Me.imgFolders.Images.SetKeyName(7, "")
        Me.imgFolders.Images.SetKeyName(8, "")
        Me.imgFolders.Images.SetKeyName(9, "")
        Me.imgFolders.Images.SetKeyName(10, "")
        Me.imgFolders.Images.SetKeyName(11, "")
        Me.imgFolders.Images.SetKeyName(12, "")
        '
        'mnuParameters
        '
        Me.mnuParameters.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuParameter})
        '
        'mnuParameter
        '
        Me.mnuParameter.Index = 0
        Me.mnuParameter.Text = "Parameter Substitution"
        '
        'ucExistingReports
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.pnExisting)
        Me.Name = "ucExistingReports"
        Me.Size = New System.Drawing.Size(570, 368)
        Me.pnExisting.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnExisting As System.Windows.Forms.Panel
    Friend WithEvents lsvSchedules As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents ExpandableSplitter1 As DevComponents.DotNetBar.ExpandableSplitter
    Friend WithEvents tvSchedules As System.Windows.Forms.TreeView
    Friend WithEvents imgFolders As System.Windows.Forms.ImageList
    Friend WithEvents mnuParameters As System.Windows.Forms.ContextMenu
    Friend WithEvents mnuParameter As System.Windows.Forms.MenuItem

End Class
