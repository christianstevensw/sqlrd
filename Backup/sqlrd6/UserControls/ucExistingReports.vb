Public Class ucExistingReports

    Dim eventID As Integer = 99999
    Public parentID As Integer = 0
    Friend imgList As ImageList

    Public Property m_eventID() As Integer
        Get
            Return eventID
        End Get
        Set(ByVal value As Integer)
            eventID = value
        End Set
    End Property
    Private Sub InitImages()
        imgList = New ImageList()

        Dim sz As Size = New Size(16, 16)

        imgList.ImageSize = sz
        imgList.ColorDepth = ColorDepth.Depth32Bit
        imgList.TransparentColor = Color.Transparent

        With imgList.Images
            .Add(My.Resources.home) '0
            .Add(My.Resources.folder_closed) '1
            .Add(My.Resources.folder) '2
            .Add(My.Resources.box_closed) '3
            .Add(My.Resources.box) '4
            .Add(My.Resources.folder_cubes) '5
            .Add(My.Resources.document_chart) '6
            .Add(My.Resources.document_gear) '7
            .Add(My.Resources.folder_closed1) '8
            .Add(My.Resources.box_closed_bw) '9
            .Add(My.Resources.box_bw) '10
            .Add(My.Resources.document_pulse) '11
            .Add(My.Resources.document_attachment) '12
            .Add(My.Resources.document_atoms) '13
            .Add(My.Resources.box_new) '14
            .Add(My.Resources.box_new_bw) '15
        End With

    End Sub
    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tvSchedules.SelectedNode Is Nothing Then Return

        Dim oNode As TreeNode
        Dim oItem As ListViewItem
        Dim sType As String
        Dim ID As Integer

        Me.InitImages()

        Me.lsvSchedules.LargeImageList = imgList
        Me.lsvSchedules.SmallImageList = imgList

        oNode = tvSchedules.SelectedNode

        sType = oNode.Tag
        ID = sType.Split(":")(1)
        sType = sType.Split(":")(0).ToLower

        If sType = "folder" Or sType = "desktop" Or sType = "smartfolder" Then Return

        For Each oItem In lsvSchedules.Items
            If oItem.Text = oNode.Text And oItem.Tag = oNode.Tag Or ID = parentID Then
                Return
            End If
        Next

        oItem = New ListViewItem

        oItem.Text = oNode.Text
        oItem.Tag = oNode.Tag
        oItem.ImageIndex = oNode.ImageIndex

        lsvSchedules.Items.Add(oItem)
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Exit Sub

        For Each oItem As ListViewItem In lsvSchedules.SelectedItems
            clsMarsData.WriteData("DELETE FROM EventSubAttr WHERE ReportID=" & oItem.Tag.split(":")(1), False)
            oItem.Remove()
        Next
    End Sub

    Public Sub LoadSelectedReports()
        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nImage As Integer = 6

        Me.InitImages()

        Me.lsvSchedules.LargeImageList = imgList
        Me.lsvSchedules.SmallImageList = imgList

        SQL = "SELECT * FROM EventSchedule WHERE EventID =" & eventID

        oRs = clsMarsData.GetData(SQL)

        Dim ColName As String
        Dim TableName As String
        Dim KeyCol As String

        lsvSchedules.Items.Clear()

        Do While oRs.EOF = False
            Select Case oRs("scheduletype").Value.ToLower
                Case "report"
                    ColName = "ReportTitle"
                    TableName = "ReportAttr"
                    KeyCol = "ReportID"
                    nImage = 6
                Case "package"
                    ColName = "PackageName"
                    TableName = "PackageAttr"
                    KeyCol = "PackID"
                    nImage = 3
                Case "automation"
                    ColName = "AutoName"
                    TableName = "AutomationAttr"
                    KeyCol = "AutoID"
                    nImage = 7
            End Select

            SQL = "SELECT " & ColName & "," & KeyCol & " FROM " & _
            TableName & " WHERE " & _
            KeyCol & "=" & oRs("scheduleid").Value

            Dim rsP As ADODB.Recordset

            rsP = clsMarsData.GetData(SQL)

            If rsP.EOF = False Then

                Dim oItem As New ListViewItem

                oItem.Text = rsP.Fields(0).Value
                oItem.ImageIndex = nImage
                oItem.Tag = oRs("scheduletype").Value & ":" & oRs("scheduleid").Value

                Me.lsvSchedules.Items.Add(oItem)

            End If

            rsP.Close()
            oRs.MoveNext()
        Loop
    End Sub

    Private Sub ucExistingReports_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.InitImages()

        Me.lsvSchedules.LargeImageList = imgList
        Me.lsvSchedules.SmallImageList = imgList
    End Sub

    Private Sub mnuParameter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuParameter.Click
        Dim sType As String
        Dim nID As Integer

        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim oItem As ListViewItem

        oItem = lsvSchedules.SelectedItems(0)

        sType = oItem.Tag.Split(":")(0)
        nID = oItem.Tag.Split(":")(1)

        If sType.ToLower <> "report" Then Return

        Dim oSub As frmEventSub = New frmEventSub
        oSub.m_eventID = eventID
        oSub.SetSubstitution(nID, eventID)
    End Sub
    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub
End Class
