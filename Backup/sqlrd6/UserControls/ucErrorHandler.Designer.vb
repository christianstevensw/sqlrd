<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucErrorHandler
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.chkFailonOne = New System.Windows.Forms.CheckBox
        Me.txtRetryInterval = New System.Windows.Forms.NumericUpDown
        Me.chkAutoCalc = New System.Windows.Forms.CheckBox
        Me.chkAssumeFail = New System.Windows.Forms.CheckBox
        Me.cmbAssumeFail = New System.Windows.Forms.NumericUpDown
        Me.cmbRetry = New System.Windows.Forms.NumericUpDown
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.txtRetryInterval, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbAssumeFail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cmbRetry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 4
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.Controls.Add(Me.chkAssumeFail, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.chkFailonOne, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(2, 59)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(429, 86)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'chkFailonOne
        '
        Me.chkFailonOne.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkFailonOne.BackColor = System.Drawing.Color.Transparent
        Me.chkFailonOne.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkFailonOne.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkFailonOne.Location = New System.Drawing.Point(3, 3)
        Me.chkFailonOne.Name = "chkFailonOne"
        Me.chkFailonOne.Size = New System.Drawing.Size(293, 22)
        Me.chkFailonOne.TabIndex = 12
        Me.chkFailonOne.Text = "Fail whole schedule if any report fails"
        Me.chkFailonOne.UseVisualStyleBackColor = False
        Me.chkFailonOne.Visible = False
        '
        'txtRetryInterval
        '
        Me.txtRetryInterval.Location = New System.Drawing.Point(209, 39)
        Me.txtRetryInterval.Maximum = New Decimal(New Integer() {20164, 0, 0, 0})
        Me.txtRetryInterval.Name = "txtRetryInterval"
        Me.txtRetryInterval.Size = New System.Drawing.Size(56, 20)
        Me.txtRetryInterval.TabIndex = 16
        Me.txtRetryInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.ToolTip1.SetToolTip(Me.txtRetryInterval, "Leave as 0 to have the schedule retry immediately")
        '
        'chkAutoCalc
        '
        Me.chkAutoCalc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAutoCalc.AutoSize = True
        Me.chkAutoCalc.BackColor = System.Drawing.Color.Transparent
        Me.chkAutoCalc.Checked = True
        Me.chkAutoCalc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAutoCalc.Location = New System.Drawing.Point(328, 16)
        Me.chkAutoCalc.Name = "chkAutoCalc"
        Me.chkAutoCalc.Size = New System.Drawing.Size(94, 17)
        Me.chkAutoCalc.TabIndex = 14
        Me.chkAutoCalc.Text = "Auto-calculate"
        Me.chkAutoCalc.UseVisualStyleBackColor = False
        '
        'chkAssumeFail
        '
        Me.chkAssumeFail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkAssumeFail.BackColor = System.Drawing.Color.Transparent
        Me.chkAssumeFail.Checked = True
        Me.chkAssumeFail.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssumeFail.Enabled = False
        Me.chkAssumeFail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssumeFail.Location = New System.Drawing.Point(3, 31)
        Me.chkAssumeFail.Name = "chkAssumeFail"
        Me.chkAssumeFail.Size = New System.Drawing.Size(195, 22)
        Me.chkAssumeFail.TabIndex = 3
        Me.chkAssumeFail.Text = "Fail if not completed in (mins)"
        Me.chkAssumeFail.UseVisualStyleBackColor = False
        '
        'cmbAssumeFail
        '
        Me.cmbAssumeFail.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmbAssumeFail.DecimalPlaces = 2
        Me.cmbAssumeFail.Enabled = False
        Me.cmbAssumeFail.Increment = New Decimal(New Integer() {5, 0, 0, 65536})
        Me.cmbAssumeFail.Location = New System.Drawing.Point(209, 13)
        Me.cmbAssumeFail.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.cmbAssumeFail.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbAssumeFail.Name = "cmbAssumeFail"
        Me.cmbAssumeFail.Size = New System.Drawing.Size(56, 20)
        Me.cmbAssumeFail.TabIndex = 13
        Me.cmbAssumeFail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbAssumeFail.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'cmbRetry
        '
        Me.cmbRetry.Location = New System.Drawing.Point(329, 39)
        Me.cmbRetry.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbRetry.Name = "cmbRetry"
        Me.cmbRetry.Size = New System.Drawing.Size(52, 20)
        Me.cmbRetry.TabIndex = 5
        Me.cmbRetry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbRetry.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbRetry)
        Me.GroupBox1.Controls.Add(Me.txtRetryInterval)
        Me.GroupBox1.Controls.Add(Me.cmbAssumeFail)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label24)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chkAutoCalc)
        Me.GroupBox1.Location = New System.Drawing.Point(0, -6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(424, 64)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(1, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(182, 20)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Treat as ""error"" if not completed in"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(265, 13)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(33, 20)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "mins."
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label24
        '
        Me.Label24.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(1, 39)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(204, 20)
        Me.Label24.TabIndex = 23
        Me.Label24.Text = "On error, retry  executing schedule every"
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(265, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 20)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "mins up to"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(381, 39)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(42, 20)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "times."
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ucErrorHandler
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Transparent
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "ucErrorHandler"
        Me.Size = New System.Drawing.Size(430, 86)
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.txtRetryInterval, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbAssumeFail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cmbRetry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents cmbRetry As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkAssumeFail As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoCalc As System.Windows.Forms.CheckBox
    Friend WithEvents cmbAssumeFail As System.Windows.Forms.NumericUpDown
    Friend WithEvents chkFailonOne As System.Windows.Forms.CheckBox
    Friend WithEvents txtRetryInterval As System.Windows.Forms.NumericUpDown
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
