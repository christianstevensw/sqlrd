<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucConditions
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ucConditions))
        Me.TabItem1 = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.lsvConditions = New System.Windows.Forms.ListView
        Me.ColumnHeader1 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader3 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader2 = New System.Windows.Forms.ColumnHeader
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnDown = New System.Windows.Forms.Button
        Me.btnUp = New System.Windows.Forms.Button
        Me.mnuType = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ConditionTypeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.DatabaseRecordExistsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator
        Me.FileExistsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.FileHasBeenModifiedToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripSeparator
        Me.ProcessExistsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.WindowIsPresentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripSeparator
        Me.UnreadMailIsPresentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.btnRefresh = New System.Windows.Forms.Button
        Me.mnuType.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabItem1
        '
        Me.TabItem1.Name = "TabItem1"
        Me.TabItem1.Text = "TabItem1"
        '
        'lsvConditions
        '
        Me.lsvConditions.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lsvConditions.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader3, Me.ColumnHeader2})
        Me.lsvConditions.FullRowSelect = True
        Me.lsvConditions.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lsvConditions.HideSelection = False
        Me.lsvConditions.Location = New System.Drawing.Point(6, 3)
        Me.lsvConditions.MultiSelect = False
        Me.lsvConditions.Name = "lsvConditions"
        Me.lsvConditions.Size = New System.Drawing.Size(357, 291)
        Me.lsvConditions.TabIndex = 0
        Me.lsvConditions.UseCompatibleStateImageBehavior = False
        Me.lsvConditions.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Name"
        Me.ColumnHeader1.Width = 130
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Condition"
        Me.ColumnHeader3.Width = 150
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Run If"
        '
        'btnDelete
        '
        Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDelete.Image = CType(resources.GetObject("btnDelete.Image"), System.Drawing.Image)
        Me.btnDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDelete.Location = New System.Drawing.Point(369, 61)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(76, 23)
        Me.btnDelete.TabIndex = 3
        Me.btnDelete.Text = "&Delete"
        Me.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAdd.Location = New System.Drawing.Point(369, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(76, 23)
        Me.btnAdd.TabIndex = 1
        Me.btnAdd.Text = "&Add"
        Me.btnAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnEdit.Image = CType(resources.GetObject("btnEdit.Image"), System.Drawing.Image)
        Me.btnEdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEdit.Location = New System.Drawing.Point(369, 32)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(76, 23)
        Me.btnEdit.TabIndex = 2
        Me.btnEdit.Text = "&Edit"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnDown
        '
        Me.btnDown.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDown.Image = CType(resources.GetObject("btnDown.Image"), System.Drawing.Image)
        Me.btnDown.Location = New System.Drawing.Point(369, 271)
        Me.btnDown.Name = "btnDown"
        Me.btnDown.Size = New System.Drawing.Size(76, 23)
        Me.btnDown.TabIndex = 5
        Me.btnDown.UseVisualStyleBackColor = True
        '
        'btnUp
        '
        Me.btnUp.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUp.Image = CType(resources.GetObject("btnUp.Image"), System.Drawing.Image)
        Me.btnUp.Location = New System.Drawing.Point(369, 242)
        Me.btnUp.Name = "btnUp"
        Me.btnUp.Size = New System.Drawing.Size(76, 23)
        Me.btnUp.TabIndex = 4
        Me.btnUp.UseVisualStyleBackColor = True
        '
        'mnuType
        '
        Me.mnuType.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConditionTypeToolStripMenuItem, Me.ToolStripSeparator1, Me.DatabaseRecordExistsToolStripMenuItem, Me.DatabaseRecordHasBeenModifiedToolStripMenuItem, Me.ToolStripMenuItem1, Me.FileExistsToolStripMenuItem, Me.FileHasBeenModifiedToolStripMenuItem, Me.ToolStripMenuItem2, Me.ProcessExistsToolStripMenuItem, Me.WindowIsPresentToolStripMenuItem, Me.ToolStripMenuItem3, Me.UnreadMailIsPresentToolStripMenuItem})
        Me.mnuType.Name = "mnuType"
        Me.mnuType.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.mnuType.Size = New System.Drawing.Size(264, 204)
        '
        'ConditionTypeToolStripMenuItem
        '
        Me.ConditionTypeToolStripMenuItem.Enabled = False
        Me.ConditionTypeToolStripMenuItem.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.ConditionTypeToolStripMenuItem.Name = "ConditionTypeToolStripMenuItem"
        Me.ConditionTypeToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.ConditionTypeToolStripMenuItem.Text = "Condition Type"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(260, 6)
        '
        'DatabaseRecordExistsToolStripMenuItem
        '
        Me.DatabaseRecordExistsToolStripMenuItem.Image = CType(resources.GetObject("DatabaseRecordExistsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatabaseRecordExistsToolStripMenuItem.Name = "DatabaseRecordExistsToolStripMenuItem"
        Me.DatabaseRecordExistsToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.DatabaseRecordExistsToolStripMenuItem.Text = "Database Record Exists"
        '
        'DatabaseRecordHasBeenModifiedToolStripMenuItem
        '
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Image = CType(resources.GetObject("DatabaseRecordHasBeenModifiedToolStripMenuItem.Image"), System.Drawing.Image)
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Name = "DatabaseRecordHasBeenModifiedToolStripMenuItem"
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.DatabaseRecordHasBeenModifiedToolStripMenuItem.Text = "Database Record has been Modified"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(260, 6)
        '
        'FileExistsToolStripMenuItem
        '
        Me.FileExistsToolStripMenuItem.Image = CType(resources.GetObject("FileExistsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FileExistsToolStripMenuItem.Name = "FileExistsToolStripMenuItem"
        Me.FileExistsToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.FileExistsToolStripMenuItem.Text = "File Exists"
        '
        'FileHasBeenModifiedToolStripMenuItem
        '
        Me.FileHasBeenModifiedToolStripMenuItem.Image = CType(resources.GetObject("FileHasBeenModifiedToolStripMenuItem.Image"), System.Drawing.Image)
        Me.FileHasBeenModifiedToolStripMenuItem.Name = "FileHasBeenModifiedToolStripMenuItem"
        Me.FileHasBeenModifiedToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.FileHasBeenModifiedToolStripMenuItem.Text = "File has been Modified"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(260, 6)
        '
        'ProcessExistsToolStripMenuItem
        '
        Me.ProcessExistsToolStripMenuItem.Image = CType(resources.GetObject("ProcessExistsToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ProcessExistsToolStripMenuItem.Name = "ProcessExistsToolStripMenuItem"
        Me.ProcessExistsToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.ProcessExistsToolStripMenuItem.Text = "Process Exists"
        '
        'WindowIsPresentToolStripMenuItem
        '
        Me.WindowIsPresentToolStripMenuItem.Image = CType(resources.GetObject("WindowIsPresentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.WindowIsPresentToolStripMenuItem.Name = "WindowIsPresentToolStripMenuItem"
        Me.WindowIsPresentToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.WindowIsPresentToolStripMenuItem.Text = "Window is present"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(260, 6)
        '
        'UnreadMailIsPresentToolStripMenuItem
        '
        Me.UnreadMailIsPresentToolStripMenuItem.Image = CType(resources.GetObject("UnreadMailIsPresentToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UnreadMailIsPresentToolStripMenuItem.Name = "UnreadMailIsPresentToolStripMenuItem"
        Me.UnreadMailIsPresentToolStripMenuItem.Size = New System.Drawing.Size(263, 22)
        Me.UnreadMailIsPresentToolStripMenuItem.Text = "Unread mail is present"
        '
        'btnRefresh
        '
        Me.btnRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRefresh.Enabled = False
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(369, 90)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(76, 23)
        Me.btnRefresh.TabIndex = 7
        Me.btnRefresh.Text = "&Refresh"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'ucConditions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnUp)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.btnDown)
        Me.Controls.Add(Me.lsvConditions)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "ucConditions"
        Me.Size = New System.Drawing.Size(452, 323)
        Me.mnuType.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabItem1 As DevComponents.DotNetBar.TabItem
    Friend WithEvents lsvConditions As System.Windows.Forms.ListView
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnDown As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader1 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader2 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnUp As System.Windows.Forms.Button
    Friend WithEvents ColumnHeader3 As System.Windows.Forms.ColumnHeader
    Friend WithEvents mnuType As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DatabaseRecordExistsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DatabaseRecordHasBeenModifiedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileExistsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileHasBeenModifiedToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcessExistsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UnreadMailIsPresentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WindowIsPresentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ConditionTypeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnRefresh As System.Windows.Forms.Button

End Class
