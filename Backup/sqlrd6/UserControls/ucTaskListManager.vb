Public Class ucTaskListManager
    Public Sub LoadDefaultTasks()

        lsvTasks.Items.Clear()

        If System.IO.Directory.Exists(sAppPath & "Tasks") = False Then
            System.IO.Directory.CreateDirectory(sAppPath & "Tasks")
        End If

        For Each sFile As String In System.IO.Directory.GetFiles(sAppPath & "Tasks")
            Dim oItem As New ListViewItem

            oItem.Text = ExtractFileName(sFile).Replace(".xml", String.Empty)

            oItem.ImageIndex = 0
            oItem.Tag = sFile
            lsvTasks.Items.Add(oItem)
        Next
    End Sub

    Private Sub lsvTasks_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsvTasks.SelectedIndexChanged
        LoadTasks()
    End Sub
    Public Sub LoadTasks()

        If lsvDetails.Items.Count > 0 Then lsvDetails.Items.Clear()
        Dim nTaskID As Integer
        Dim oRs As ADODB.Recordset

        If lsvTasks.SelectedItems.Count = 0 Then Return

        oRs = clsMarsData.GetXML(lsvTasks.SelectedItems(0).Tag)

        Dim lsv As ListViewItem
        Dim sTaskType As String

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                lsv = New ListViewItem

                sTaskType = IsNull(oRs("tasktype").Value)

                nTaskID = oRs("taskid").Value

                lsv.Tag = sTaskType & ":" & nTaskID

                lsv.Text = oRs("taskname").Value

                Select Case sTaskType.ToLower
                    Case "copyfile"
                        lsv.ImageIndex = 5
                    Case "deletefile"
                        lsv.ImageIndex = 7
                    Case "sendmail"
                        lsv.ImageIndex = 13
                    Case "createfolder"
                        lsv.ImageIndex = 9
                    Case "movefolder"
                        lsv.ImageIndex = 11
                    Case "pause"
                        lsv.ImageIndex = 14
                    Case "print"
                        lsv.ImageIndex = 2
                    Case "program"
                        lsv.ImageIndex = 1
                    Case "renamefile"
                        lsv.ImageIndex = 6
                    Case "webbrowse"
                        lsv.ImageIndex = 15
                    Case "writefile"
                        lsv.ImageIndex = 8
                    Case "sqlscript"
                        lsv.ImageIndex = 22
                    Case "sqlproc"
                        lsv.ImageIndex = 21
                    Case "dbupdate"
                        lsv.ImageIndex = 35
                    Case "dbinsert"
                        lsv.ImageIndex = 33
                    Case "dbdelete"
                        lsv.ImageIndex = 34
                    Case "dbcreatetable"
                        lsv.ImageIndex = 23
                    Case "dbdeletetable"
                        lsv.ImageIndex = 24
                    Case "dbmodifytable_add"
                        lsv.ImageIndex = 25
                    Case "dbmodifytable_drop"
                        lsv.ImageIndex = 26
                    Case "ftpupload"
                        lsv.ImageIndex = 29
                    Case "ftpdownload"
                        lsv.ImageIndex = 30
                    Case "ftpdeletefile", "ftpdeletedirectory"
                        lsv.ImageIndex = 31
                    Case "ftpcreatedirectory"
                        lsv.ImageIndex = 32
                    Case "registrycreatekey"
                        lsv.ImageIndex = 39
                    Case "registrydeletekey"
                        lsv.ImageIndex = 38
                    Case "registrysetkey"
                        lsv.ImageIndex = 37
                    Case "runschedule"
                        lsv.ImageIndex = 40
                    Case "zipfiles"
                        lsv.ImageIndex = 41
                    Case "unzipfile"
                        lsv.ImageIndex = 42
                    Case "mergepdf"
                        lsv.ImageIndex = 43
                End Select

                With lsv.SubItems
                    .Add(IsNull(oRs("runwhen").Value))
                    .Add(IsNull(oRs("aftertype").Value))
                End With

                lsvDetails.Items.Add(lsv)

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub
    Private Sub mnuRename_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRename.Click
        If lsvTasks.SelectedItems.Count = 0 Then Return

        lsvTasks.SelectedItems(0).BeginEdit()
    End Sub

    Private Sub lsvTasks_AfterLabelEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.LabelEditEventArgs) Handles lsvTasks.AfterLabelEdit

        If e.Label.Length = 0 Then e.CancelEdit = True : Return

        Dim oItem As ListViewItem = lsvTasks.SelectedItems(0)

        System.IO.File.Move(oItem.Tag, sAppPath & "Tasks\" & e.Label & ".xml")

    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        Dim oRes As DialogResult

        If lsvTasks.SelectedItems.Count = 0 Then Return

        oRes = MessageBox.Show("Delete the selected task list?", _
        Application.ProductName, MessageBoxButtons.YesNo, _
        MessageBoxIcon.Question)

        If oRes = DialogResult.Yes Then
            Dim oItem As ListViewItem = lsvTasks.SelectedItems(0)

            System.IO.File.Delete(oItem.Tag)

            oItem.Remove()
        End If
    End Sub

    
End Class
