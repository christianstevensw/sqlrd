Public Class ucExecutionPath

    Public Property m_ExecutionFlow() As String
        Get
            If optAfter.Checked Then
                Return "AFTER"
            ElseIf optBefore.Checked Then
                Return "BEFORE"
            ElseIf optBoth.Checked Then
                Return "BOTH"
            End If
        End Get

        Set(ByVal value As String)
            Select Case value
                Case "AFTER"
                    optAfter.Checked = True
                Case "BEFORE"
                    optBefore.Checked = True
                Case "BOTH"
                    optBoth.Checked = True
            End Select
        End Set
    End Property
    
    Private Sub optAfter_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAfter.CheckedChanged
        If optAfter.Checked Then
            optBefore.Checked = False
            optBoth.Checked = False
        End If
    End Sub

    Private Sub optBefore_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optBefore.CheckedChanged
        If optBefore.Checked Then
            optAfter.Checked = False
            optBoth.Checked = False
        End If
    End Sub

    Private Sub optBoth_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optBoth.CheckedChanged
        If optBoth.Checked Then
            optAfter.Checked = False
            optBefore.Checked = False
        End If
    End Sub
End Class
