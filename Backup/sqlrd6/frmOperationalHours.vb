Public Class frmOperationalHours
    Dim allChecks As CheckBox()
    Dim allOpens As DateTimePicker()
    Dim allCloses As DateTimePicker()
    Dim allPanels As FlowLayoutPanel()
    Dim ep As New ErrorProvider
    Dim userCancel As Boolean = True
    Dim currentMode As mode
    Dim m_OperationID As Integer = 0

    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Enum mode As Integer
        ADD = 0
        EDIT = 1
    End Enum
    Private Sub txtName_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtName.TextChanged

        If txtName.Text.Length > 0 Then
            cmdOK.Enabled = True
        Else
            cmdOK.Enabled = False
        End If

        ep.SetError(txtName, "")
    End Sub

    Private Sub frmOperationalHours_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        If allChecks Is Nothing Then Return

        For Each chk As CheckBox In allChecks
            chk.Checked = chkAll.Checked
        Next

        ep.SetError(chkAll, "")
    End Sub

    Private Sub dtopenall_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtopenall.ValueChanged
        If allOpens Is Nothing Then Return
        Dim I As Integer = 0

        For Each dt As DateTimePicker In allOpens
            If allChecks(I).Checked = True Then
                dt.Value = dtopenall.Value
            End If

            I += 1
        Next
    End Sub

    Private Sub dtcloseall_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtcloseall.ValueChanged
        If allCloses Is Nothing Then Return
        Dim I As Integer = 0

        For Each dt As DateTimePicker In allCloses
            If allChecks(I).Checked = True Then
                dt.Value = dtcloseall.Value
            End If

            I += 1
        Next
    End Sub

    Private Sub chkMonday_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMonday.CheckedChanged, _
    chkTuesday.CheckedChanged, chkWednesday.CheckedChanged, chkThursday.CheckedChanged, chkFriday.CheckedChanged, chkSaturday.CheckedChanged, _
    chkSunday.CheckedChanged

        If allChecks Is Nothing Then Return

        Dim chk As CheckBox = CType(sender, CheckBox)
        Dim loc As Integer = allChecks.IndexOf(allChecks, chk)

        If loc > -1 Then
            Dim pn As FlowLayoutPanel = allPanels(loc)

            pn.Enabled = chk.Checked
        End If

    End Sub

    Public Function AddOperationalHours() As String
        allChecks = New CheckBox() {chkMonday, chkTuesday, chkWednesday, chkThursday, chkFriday, chkSaturday, chkSunday}
        allOpens = New DateTimePicker() {dtopenmonday, dtopentuesday, dtopenwednesday, dtopenthursday, dtopenfriday, dtopensaturday, dtopensunday}
        allCloses = New DateTimePicker() {dtclosemonday, dtclosetuesday, dtclosewednesday, dtclosethursday, dtclosefriday, dtclosesaturday, dtclosesunday}
        allPanels = New FlowLayoutPanel() {pnMonday, pnTuesday, pnWednesday, pnThursday, pnFriday, pnSaturday, pnSunday}

        For Each dt As DateTimePicker In allOpens
            dt.Value = ConDate(Now) & " 00:00:00"
        Next

        For Each dt As DateTimePicker In allCloses
            dt.Value = ConDate(Now) & " 23:59:59"
        Next

        Me.dtcloseall.Value = ConDate(Now) & " 23:59:59"
        Me.dtopenall.Value = ConDate(Now) & " 00:00:00"

        Me.currentMode = mode.ADD

        Me.ShowDialog()

        If userCancel = True Then Return ""

        Dim SQL As String
        Dim cols As String
        Dim vals As String

        Dim OperationID As Integer = clsMarsData.CreateDataID("operationattr", "operationid")

        cols = "OperationID, OperationName"

        vals = OperationID & ",'" & SQLPrepare(txtName.Text) & "'"



        If clsMarsData.DataItem.InsertData("OperationAttr", cols, vals) = True Then
            cols = "DayID, OperationID, DayName, DayIndex, OpenAt, CloseAt"



            For Each chk As CheckBox In allChecks
                If chk.Checked Then
                    Dim DayID As Integer = clsMarsData.CreateDataID("operationdays", "dayid")
                    Dim OpenAt As String = ConTime(allOpens(chk.Tag).Value)
                    Dim CloseAt As String = ConTime(allCloses(chk.Tag).Value)

                    vals = DayID & "," & _
                    OperationID & "," & _
                    "'" & chk.Text & "'," & _
                    chk.Tag & "," & _
                    "'" & CTimeZ(OpenAt, dateConvertType.WRITE) & "'," & _
                    "'" & CTimeZ(CloseAt, dateConvertType.WRITE) & "'"

                    clsMarsData.DataItem.InsertData("OperationDays", cols, vals)
                End If
            Next

            Return txtName.Text
        Else
            Return ""
        End If
    End Function

    Public Function EditOperationalHours(ByVal OperationID As Integer) As Boolean
        allChecks = New CheckBox() {chkMonday, chkTuesday, chkWednesday, chkThursday, chkFriday, chkSaturday, chkSunday}
        allOpens = New DateTimePicker() {dtopenmonday, dtopentuesday, dtopenwednesday, dtopenthursday, dtopenfriday, dtopensaturday, dtopensunday}
        allCloses = New DateTimePicker() {dtclosemonday, dtclosetuesday, dtclosewednesday, dtclosethursday, dtclosefriday, dtclosesaturday, dtclosesunday}
        allPanels = New FlowLayoutPanel() {pnMonday, pnTuesday, pnWednesday, pnThursday, pnFriday, pnSaturday, pnSunday}

        Dim SQL As String = "SELECT * FROM OperationAttr  o INNER JOIN OperationDays d " & _
        "ON o.OperationID = d.OperationID WHERE o.OperationID =" & OperationID
        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Me.m_OperationID = OperationID

        If oRs IsNot Nothing Then
            Do While oRs.EOF = False
                txtName.Text = oRs("operationname").Value

                Dim index As Integer = oRs("dayindex").Value

                allChecks(index).Checked = True

                Dim openAt, closeAt As Date

                openAt = CTimeZ(ConDate(Now) & " " & oRs("openat").Value, dateConvertType.READ)
                closeAt = CTimeZ(ConDate(Now) & " " & oRs("closeat").Value, dateConvertType.READ)

                allOpens(index).Value = openAt
                allCloses(index).Value = closeAt

                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

        Me.currentMode = mode.EDIT
        'chkAll.CheckState = CheckState.Indeterminate

        Me.ShowDialog()

        If userCancel = True Then Return False

        SQL = "UPDATE OperationAttr SET OperationName ='" & txtName.Text & "' WHERE OperationID =" & OperationID

        If clsMarsData.WriteData(SQL) = True Then

            clsMarsData.WriteData("DELETE FROM OperationDays WHERE OperationID =" & OperationID)

            Dim cols As String = "DayID, OperationID, DayName, DayIndex, OpenAt, CloseAt"

            For Each chk As CheckBox In allChecks
                If chk.Checked Then
                    Dim DayID As Long = clsMarsData.CreateDataID("operationdays", "dayid")
                    Dim OpenAt As String = ConTime(allOpens(chk.Tag).Value)
                    Dim CloseAt As String = ConTime(allCloses(chk.Tag).Value)

                    Dim vals As String = DayID & "," & _
                    OperationID & "," & _
                    "'" & chk.Text & "'," & _
                    chk.Tag & "," & _
                    "'" & CTimeZ(OpenAt, dateConvertType.WRITE) & "'," & _
                    "'" & CTimeZ(CloseAt, dateConvertType.WRITE) & "'"

                    If clsMarsData.DataItem.InsertData("OperationDays", cols, vals) = False Then
                        Return False
                    End If
                End If
            Next

            Return True
        End If

    End Function
    
    Private Sub cmdOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        If txtName.Text.Length = 0 Then
            ep.SetError(txtName, "Please provide a name for these Operational Hours")
            txtName.Focus()
            Return
        ElseIf Me.currentMode = mode.ADD Then
            If clsMarsData.IsDuplicate("OperationAttr", "OperationName", txtName.Text, False) = True Then
                ep.SetError(txtName, "An object with this name already exists")
                txtName.Focus()
                txtName.SelectAll()
                Return
            End If
        ElseIf Me.currentMode = mode.EDIT Then
            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT * FROM OperationAttr WHERE " & _
            "OperationName ='" & SQLPrepare(txtName.Text) & "' AND OperationID <> " & Me.m_OperationID)

            If oRs IsNot Nothing Then
                If oRs.EOF = False Then
                    ep.SetError(txtName, "An object with this name already exists")
                    txtName.Focus()
                    txtName.SelectAll()
                    Return
                End If

                oRs.Close()
            End If
        End If

        Dim count As Integer = 0

        For Each chk As CheckBox In allChecks
            If chk.Checked = True And chk.Name <> "chkAll" Then
                count += 1
                Exit For
            End If
        Next

        If count = 0 Then
            ep.SetError(chkAll, "Please select at least one day of the week")
            chkAll.Focus()
            Return
        End If


        Me.userCancel = False

        Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Close()
    End Sub
End Class