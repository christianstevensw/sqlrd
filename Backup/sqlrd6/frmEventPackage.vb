Public Class frmEventPackage

    Dim nStep As Integer = 1
    Dim ep As New ErrorProvider

    Private ReadOnly Property m_Step(ByVal stepValue As Integer) As String
        Get
            Select Case stepValue
                Case 1
                    Return "Step 1: Schedule Details"
                Case 2
                    Return "Step 2: Schedule Setup"
                Case 3
                    Return "Step 3: Add Schedules"
                Case 4
                    Return "Step 4: Custom Tasks"
                Case 5
                    Return "Step 5: Execution Flow"
            End Select
        End Get
    End Property
    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Dim oFolders As frmFolders = New frmFolders
        Dim folderDetails() As String

        folderDetails = oFolders.GetFolder

        txtLocation.Text = folderDetails(0)
        txtLocation.Tag = folderDetails(1)
    End Sub

    Private Sub frmEventPackage_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsFeatEnabled(gEdition.CORPORATE, modFeatCodes.s3_EventBasedScheds) = False Then
            _NeedUpgrade(gEdition.CORPORATE)
            Close()
            Return
        End If

        FormatForWinXP(Me)

        Me.lblStep.Text = Me.m_Step(1)

        Step1.BringToFront()

        UcTasks.ShowAfterType = False
        UcTasks.lsvTasks.HeaderStyle = ColumnHeaderStyle.None

        If gParentID > 0 And gParent.Length > 0 Then
            txtLocation.Text = gParent
            txtLocation.Tag = gParentID
        End If
    End Sub

    Private Sub cmdNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNext.Click
        Select Case nStep
            Case 1
                If txtName.Text.Length = 0 Then
                    ep.SetError(txtName, "Please enter the schedule name")
                    txtName.Focus()
                    Return
                ElseIf txtLocation.Text.Length = 0 Then
                    ep.SetError(txtLocation, "Please select the schedule location")
                    txtLocation.Focus()
                    Return
                ElseIf clsMarsUI.candoRename(txtName.Text, Me.txtLocation.Tag, clsMarsScheduler.enScheduleType.EVENTPACKAGE) = False Then
                    ep.SetError(txtName, "An Event-Based Package with this name already exists in this folder")
                    txtName.Focus()
                    Return
                End If

                Step2.BringToFront()
                lblStep.Text = Me.m_Step(nStep + 1)

                cmdBack.Enabled = True
            Case 2
                If ucSet.ValidateSchedule = True Then
                    Step3.BringToFront()
                    lblStep.Text = Me.m_Step(nStep + 1)
                End If
            Case 3

                Step4.BringToFront()
                lblStep.Text = Me.m_Step(nStep + 1)

            Case 4
                Step5.BringToFront()

                If Me.UcTasks.lsvTasks.Items.Count = 0 Then
                    ucPath.Enabled = False
                Else
                    ucPath.Enabled = True
                End If

                lblStep.Text = Me.m_Step(nStep + 1)

                cmdFinish.Visible = True
                cmdNext.Visible = False
        End Select

        nStep += 1

    End Sub

    Private Sub cmdBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBack.Click
        Select Case nStep
            Case 5
                Step4.BringToFront()

                lblStep.Text = Me.m_Step(nStep - 1)

                cmdFinish.Visible = False
                cmdNext.Visible = True
            Case 4
                Step3.BringToFront()
                lblStep.Text = m_Step(nStep - 1)

            Case 3
                Step2.BringToFront()
                lblStep.Text = m_Step(nStep - 1)
            Case 2
                Step1.BringToFront()
                lblStep.Text = m_Step(nStep - 1)

                cmdBack.Enabled = False
        End Select

        nStep -= 1
    End Sub
    Private Sub txtLocation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles txtLocation.TextChanged, txtName.TextChanged
        Dim oText As TextBox = CType(sender, TextBox)

        If oText.Text.Length = 0 Then
            cmdNext.Enabled = False
        Else
            cmdNext.Enabled = True
        End If
    End Sub

   
    Private Sub btnAdd_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles btnAdd.MouseClick
        Me.mnuAdd.Show(Me.btnAdd, New Point(e.X, e.Y))
    End Sub

    
    Private Sub mnuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim oEvent As New frmEventWizard6
        Dim eventDtls As Hashtable
        Dim eventName As String = ""
        Dim eventID As Integer = 0
        Dim eventStatus As String

        oEvent.txtLocation.Tag = "99999"
        oEvent.txtLocation.Text = "\Package"
        oEvent.txtLocation.Enabled = False
        oEvent.btnBrowse.Enabled = False
        eventDtls = oEvent.AddSchedule(lsvSchedules.Items.Count)

        If eventDtls IsNot Nothing Then
            Dim lsv As ListViewItem = Me.lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & eventDtls.Item("Name"))
            lsv.Tag = eventDtls.Item("ID")
            lsv.ImageIndex = 0

            If eventDtls.Item("Status") = "Disabled" Then
                lsv.Checked = False
            Else
                lsv.Checked = True
            End If
        End If
    End Sub

    Private Sub mnuExisting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExisting.Click
        Dim values As Hashtable
        Dim getEvents As frmFolders = New frmFolders

        getEvents.m_eventOnly = True
        values = getEvents.GetEventBasedSchedule

        If values IsNot Nothing Then
            Dim lsv As ListViewItem = lsvSchedules.Items.Add(lsvSchedules.Items.Count + 1 & ":" & values.Item("Name"))
            lsv.Tag = values.Item("ID")
            lsv.ImageIndex = 0

            clsMarsData.WriteData("UPDATE EventAttr6 SET Status = 1 WHERE EventID =" & lsv.Tag)

            lsv.Checked = True

        End If

    End Sub

    Private Sub cmdFinish_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFinish.Click
        Dim SQL As String
        Dim PackID As Integer = clsMarsData.CreateDataID("eventpackageattr", "eventpackid")
        Dim cols As String
        Dim vals As String

        cols = "EventPackID,Parent,PackageName,ExecutionFlow,Owner"

        vals = PackID & "," & _
        txtLocation.Tag & "," & _
        "'" & SQLPrepare(txtName.Text) & "'," & _
        "'" & Me.ucPath.m_ExecutionFlow & "'," & _
        "'" & gUser & "'"
        
        If clsMarsData.DataItem.InsertData("EventPackageAttr", cols, vals) = True Then
            For Each lsv As ListViewItem In lsvSchedules.Items
                Dim EventID As Integer = lsv.Tag

                SQL = "UPDATE EventAttr6 SET PackID =" & PackID & "," & _
                "Parent = " & PackID & "," & _
                "PackOrderID =" & lsv.Index & " WHERE EventID =" & EventID

                clsMarsData.WriteData(SQL)
            Next

            If ucSet.chkNoEnd.Checked = True Then ucSet.EndDate.Value = Now.AddYears(1000)

            Dim ScheduleID As Integer = clsMarsData.CreateDataID("scheduleattr", "scheduleid")

            cols = "ScheduleID,Frequency,StartDate,EndDate,NextRun," & _
                    "StartTime,Repeat,RepeatInterval,RepeatUntil,Status,EventPackID," & _
                    "Description,KeyWord,CalendarName,UseException,ExceptionCalendar,RepeatUnit"

            With ucSet
                vals = ScheduleID & "," & _
                        "'" & .sFrequency & "'," & _
                        "'" & ConDateTime(CTimeZ(.StartDate.Value, dateConvertType.WRITE)) & "'," & _
                        "'" & ConDateTime(CTimeZ(.EndDate.Value, dateConvertType.WRITE)) & "'," & _
                        "'" & ConDate(CTimeZ(.StartDate.Value.Date, dateConvertType.WRITE)) & " " & ConTime(CTimeZ(.RunAt.Value, dateConvertType.WRITE)) & "'," & _
                        "'" & ConTime(.RunAt.Value.ToShortTimeString) & "'," & _
                        Convert.ToInt32(.chkRepeat.Checked) & "," & _
                        "'" & Convert.ToString(.cmbRpt.Value).Replace(",", ".") & "'," & _
                        "'" & CTimeZ(.RepeatUntil.Value.ToShortTimeString, dateConvertType.WRITE) & "'," & _
                        Convert.ToInt32(.chkStatus.Checked) & "," & _
                        PackID & "," & _
                        "'" & SQLPrepare(txtDescription.Text) & "'," & _
                        "'" & SQLPrepare(txtKeyword.Text) & "'," & _
                        "'" & SQLPrepare(.cmbCustom.Text) & "'," & _
                        Convert.ToInt32(.chkException.Checked) & "," & _
                        "'" & SQLPrepare(.cmbException.Text) & "'," & _
                        "'" & ucSet.m_RepeatUnit & "'"
            End With

            SQL = "INSERT INTO ScheduleAttr (" & cols & ") VALUES (" & vals & ")"

            clsMarsData.WriteData(SQL)

            UcTasks.CommitDeletions()

            SQL = "UPDATE Tasks SET ScheduleID = " & ScheduleID & " WHERE ScheduleID = 99999"

            clsMarsData.WriteData(SQL)

            Close()

            clsMarsUI.MainUI.RefreshView(oWindow(nWindowCurrent))

            On Error Resume Next
            Dim nCount As Integer

            nCount = txtLocation.Text.Split("\").GetUpperBound(0)

            Dim oTree As TreeView = oWindow(nWindowCurrent).tvFolders

            clsMarsUI.MainUI.FindNode("Folder:" & txtLocation.Tag, oTree, oTree.Nodes(0))

            oWindow(nWindowCurrent).lsvWindow.Focus()

            For Each item As ListViewItem In oWindow(nWindowCurrent).lsvWindow.Items
                If item.Text = txtName.Text Then
                    item.Selected = True
                    item.Focused = True
                Else
                    item.Selected = False
                    item.Focused = False
                End If
            Next

            clsMarsAudit._LogAudit(txtName.Text, clsMarsAudit.ScheduleType.EVENTPACKAGE, clsMarsAudit.AuditAction.CREATE)

        End If
    End Sub

    
    Private Sub btnUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUp.Click
        MoveListViewItem(Me.lsvSchedules, True)
    End Sub

    Private Function MoveListViewItem(ByVal lsv As ListView, ByVal moveUp As Boolean) As Integer
        If lsv.SelectedItems.Count = 0 Then Return 0

        Dim value As Integer

        If moveUp = False Then
            If lsv.SelectedItems(0).Index = lsv.Items.Count - 1 Then Return 0

            value = 1
        Else
            If lsv.SelectedItems(0).Index = 0 Then Return 0

            value = -1
        End If

        Dim itemList As ListViewItem()
        Dim I As Integer = 0
        Dim newIndex As Integer

        ReDim itemList(lsv.Items.Count - 1)

        'copy into an array
        For Each item As ListViewItem In lsv.Items
            itemList(I) = item
            I += 1
        Next

        newIndex = lsv.SelectedItems(0).Index + value

        Dim selItem As ListViewItem = lsv.SelectedItems(0)
        Dim nextItem As ListViewItem = lsv.Items(selItem.Index + value)

        Dim selPrefix As String = lsv.SelectedItems(0).Text.Split(":")(0)
        Dim nextPrefix As String = lsv.Items(selItem.Index + value).Text.Split(":")(0)

        selItem.Text = nextPrefix & ":" & selItem.Text.Split(":")(1)
        nextItem.Text = selPrefix & ":" & nextItem.Text.Split(":")(1)

        itemList(selItem.Index + value) = selItem
        itemList(selItem.Index) = nextItem

        lsv.Items.Clear()

        For Each item As ListViewItem In itemList
            lsv.Items.Add(item)

            If item.Index = newIndex Then
                item.Selected = True
            Else
                item.Selected = False
            End If
        Next

        lsv.Refresh()

        Return newIndex
    End Function

    Private Sub btnDown_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDown.Click
        MoveListViewItem(Me.lsvSchedules, False)
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If lsvSchedules.SelectedItems.Count = 0 Then Return

        Dim frmEventProp As New frmEventProp

        frmEventProp.EditSchedule(lsvSchedules.SelectedItems(0).Tag)
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            clsMarsData.DataItem.CleanDB()

            Dim oRs As ADODB.Recordset = clsMarsData.GetData("SELECT EventID FROM EventAttr6 WHERE Parent =99999")

            If oRs IsNot Nothing Then
                Do While oRs.EOF = False
                    clsMarsEvent.DeleteEvent(oRs("eventid").Value)

                    oRs.MoveNext()
                Loop

                oRs.Close()
            End If
        Catch : End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Dim Res As DialogResult
        Dim oEvent As clsMarsEvent = New clsMarsEvent

        Res = MessageBox.Show("Would you like to delete the selected event-based schedule(s)?" & vbCrLf & _
        "Click YES to permanently delete the schedule, NO to simply remove it from this package or CANCEL to leave everything unchanged", _
        Application.ProductName, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question)


        Select Case Res
            Case Windows.Forms.DialogResult.Yes
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    oEvent.DeleteEvent(eventID)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.No
                For Each item As ListViewItem In lsvSchedules.SelectedItems
                    Dim eventID As Integer = item.Tag

                    Dim SQL As String = "UPDATE EventAttr6 SET PackID =0, Parent =" & txtLocation.Tag & " WHERE EventID =" & eventID

                    clsMarsData.WriteData(SQL)

                    item.Remove()
                Next
            Case Windows.Forms.DialogResult.Cancel
                Return
        End Select

    End Sub

   
End Class