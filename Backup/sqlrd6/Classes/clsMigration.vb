Public Class clsMigration
    Private Shared result As Boolean = False

    Public Shared Property m_Result() As Boolean
        Get
            Return result
        End Get
        Set(ByVal value As Boolean)
            result = value
        End Set
    End Property
    Public Shared ReadOnly Property m_datConString() As String
        Get
            Return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sAppPath & "sqlrdlive.dat" & ";Persist Security Info=False"
        End Get
    End Property
    Public Shared ReadOnly Property m_localConString() As String
        Get
            Return "Provider=SQLOLEDB.1;Password=cssAdmin12345$;Persist Security Info=True;User ID=sa;Initial Catalog=SQL-RD;Data Source=" & Environment.MachineName & "\SQL-RD" 'Data Source=" & instanceName & ";Initial Catalog=MARS;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
        End Get
    End Property

    Public Shared ReadOnly Property m_localConStringdotNET() As String
        Get
            Return "Data Source=" & Environment.MachineName & "\SQL-RD;Initial Catalog=SQL-RD;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
        End Get
    End Property

    Public Shared ReadOnly Property m_DatabaseService() As String
        Get
            Return "SQL Server (SQL-RD)"
        End Get
    End Property
    Public Shared Function MigrateSystem(ByVal dsnName As String, ByVal userName As String, _
    ByVal password As String, Optional ByVal exceptTables As ArrayList = Nothing) As Boolean
        If CreateSystemTables(dsnName, userName, password) = True Then
            m_Result = CopySystemData(dsnName, userName, password, exceptTables)
            Return m_Result
        End If
        Return False
    End Function
    Private Shared Function CreateSystemTables(ByVal dsnName As String, ByVal userName As String, ByVal password As String) As Boolean
        Try
            Dim cmb As ComboBox = New ComboBox
            Dim owner As String = ""

            Dim connectionString As String = ""

            If gConType = "DAT" Then
                connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sAppPath & "sqlrdlive.dat" & ";Persist Security Info=False"
                owner = "dbo."
            Else
                connectionString = clsMarsUI.MainUI.ReadRegistry("ConString", connectionString, True)
            End If

            clsMarsData.DataItem.GetTables(cmb, connectionString, False)

            'loop through all the tables
            Dim obj As ComboBox.ObjectCollection = cmb.Items

            Dim oCon As ADODB.Connection = New ADODB.Connection

            oCon.Open(dsnName, userName, password)

            For i As Integer = 0 To obj.Count - 1

                Dim tableName As String = obj.Item(i).ToString

                clsMarsUI.MainUI.BusyProgress((i / obj.Count) * 100, "Creating table: " & tableName)

                Try
                    oCon.Execute("DROP TABLE [" & tableName & "]")

                    '_Delay(1)
                Catch : End Try

                If tableName.StartsWith("[") = False Then
                    tableName = "[" & tableName & "]"
                End If

                Dim oRs As ADODB.Recordset = clsMarsData.GetData(clsMarsData.DataItem.prepforbinaryCollation("SELECT TOP 1 * FROM " & tableName))

                If oRs Is Nothing Then
                    Continue For
                End If

                tableName = owner & tableName

                Dim SQL As String = "CREATE TABLE " & tableName & " ( "

                For Each fld As ADODB.Field In oRs.Fields
                    Dim columnName As String = fld.Name
                    Dim columnType As ADODB.DataTypeEnum = fld.Type
                    Dim columnSize As Integer = fld.DefinedSize
                    Dim columnDef As String = ""

                    If columnName.StartsWith("[") = False Then
                        columnName = "[" & columnName & "]"
                    End If

                    Select Case columnType
                        Case ADODB.DataTypeEnum.adBigInt, ADODB.DataTypeEnum.adDecimal, _
                        ADODB.DataTypeEnum.adDouble, ADODB.DataTypeEnum.adInteger, ADODB.DataTypeEnum.adNumeric, _
                        ADODB.DataTypeEnum.adSingle, ADODB.DataTypeEnum.adSmallInt, ADODB.DataTypeEnum.adTinyInt, _
                        ADODB.DataTypeEnum.adVarNumeric
                            columnDef = columnName & " INTEGER,"
                        Case ADODB.DataTypeEnum.adDate, ADODB.DataTypeEnum.adDBDate, ADODB.DataTypeEnum.adDBTime, ADODB.DataTypeEnum.adDBTimeStamp
                            columnDef = columnName & " DATETIME,"
                        Case ADODB.DataTypeEnum.adWChar, ADODB.DataTypeEnum.adLongVarChar
                            columnDef = columnName & " TEXT,"
                        Case Else
                            If columnSize > 0 And columnSize < 8000 Then
                                columnDef = columnName & " VARCHAR(" & columnSize & "),"
                            Else
                                columnDef = columnName & " TEXT,"
                            End If
                    End Select

                    SQL &= columnDef
                Next

                SQL = SQL.Substring(0, SQL.Length - 1)

                SQL &= ")"

                '                'console.writeLine(SQL)

                oCon.Execute(clsMarsData.DataItem.prepforbinaryCollation(SQL))

NextTable:
            Next

            oCon.Close()

            Return True
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
            Return False
        End Try
    End Function

    Private Shared Function CopySystemData(ByVal dsnName As String, ByVal userName As String, ByVal password As String, _
    Optional ByVal exceptTables As ArrayList = Nothing) As Boolean
        Dim cmb As New ComboBox
        Dim values As String
        Dim owner As String = ""

        Dim connectionString As String = ""

        If gConType = "DAT" Then
            connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & sAppPath & "sqlrdlive.dat" & ";Persist Security Info=False"
        Else
            connectionString = clsMarsUI.MainUI.ReadRegistry("ConString", connectionString, True)
            owner = "dbo."
        End If

        clsMarsData.DataItem.GetTables(cmb, connectionString, False)

        Dim obj As ComboBox.ObjectCollection = cmb.Items

        Dim oCon As New ADODB.Connection

        oCon.Open(dsnName, userName, password)

        Try
            If exceptTables IsNot Nothing Then
                Dim tmpArray As ArrayList = New ArrayList

                For Each s As String In exceptTables
                    For Each t As String In s.Split(",")
                        If t <> "" Then
                            If tmpArray.Contains(t) = False Then tmpArray.Add(t.Trim)
                        End If
                    Next
                Next

                exceptTables = tmpArray.Clone
            End If
        Catch : End Try

        For i As Integer = 0 To obj.Count - 1
            'lets see if we should skip this table
            Try
                If exceptTables IsNot Nothing Then
                    If exceptTables.Contains(obj.Item(i).ToString.ToLower) Then
                        Continue For
                    End If
                End If
            Catch : End Try

            Dim SQL As String = clsMarsData.DataItem.prepforbinaryCollation("SELECT * FROM " & owner & "[" & obj.Item(i).ToString & "]")
            Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL, ADODB.CursorTypeEnum.adOpenStatic)
            Dim columns As String = ""
            Dim curRecord As Integer = 1

            clsMarsUI.MainUI.BusyProgress((i / obj.Count) * 100, "Copying table: " & obj.Item(i).ToString)

            For Each fld As ADODB.Field In oRs.Fields
                columns &= "[" & fld.Name & "],"
            Next

            columns = columns.Substring(0, columns.Length - 1)

            Do While oRs.EOF = False

                If oMain.m_UserCancel = True Then
                    oMain.m_UserCancel = False

                    If MessageBox.Show("Cancel data export?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                        Return False
                    End If
                End If

                Try
                    clsMarsUI.MainUI.BusyProgress((curRecord / oRs.RecordCount) * 100, curRecord & " of " & oRs.RecordCount & " records (" & obj.Item(i).ToString & ")")
                Catch : End Try

                values = String.Empty

                For Each s As String In columns.Split(",")
                    If s.Length > 0 Then
                        Dim field As String = s.Replace("[", "").Replace("]", "")
                        Dim query As String = clsMarsData.DataItem.prepforbinaryCollation("SELECT * FROM " & obj.Item(i).ToString)
                        values &= clsMarsData.DataItem.ConvertToFieldType(query, gCon, field, oRs(field).Value) & ","
                    End If
                Next

                'For Each field As ADODB.Field In oRs.Fields
                '    values &= clsMarsData.DataItem.ConvertToFieldType(SQL, gCon, field.Name, field.Value) & ","
                'Next

                values = values.Substring(0, values.Length - 1)

                SQL = "INSERT INTO " & obj.Item(i) & " (" & columns & ") VALUES (" & values & ")"

                Try
                    oCon.Execute(clsMarsData.DataItem.prepforbinaryCollation(SQL))
                Catch ex As Exception
                    'console.writeLine(ex.Message & vbCrLf & SQL)
                End Try

                oRs.MoveNext()

                curRecord += 1

                Application.DoEvents()
            Loop

            oRs.Close()

        Next
        oCon.Close()

        clsMarsUI.MainUI.BusyProgress(, , True)
        Return True
    End Function

    Public Function MigrateSystemDBtoLocal()
        Try
            If RunEditor = False Then Return Nothing

            Dim configFile As String = Application.StartupPath & "\sqlrdlive.config"

            Dim skipMigration As Boolean = Convert.ToBoolean(Convert.ToInt32(clsMarsUI.MainUI.ReadRegistry("SkipMigration", 0, , configFile, True)))

            If skipMigration = True Then Return Nothing

            Dim conType As String = clsMarsUI.MainUI.ReadRegistry("ConType", "DAT", , configFile, True)

            Dim resultFile As String = Application.StartupPath & "\dmg.result"

            Try
                If IO.File.Exists(resultFile) = True Then
                    IO.File.Delete(resultFile)
                End If
            Catch : End Try

            If conType = "DAT" Then
                gConfigFile = configFile

                '//check if there is another migrator running
                If Process.GetProcessesByName("dmg").Length > 0 Then
                    MessageBox.Show("There is already another SQL-RD Database Configuration Wizard running. Please try running SQL-RD once that one has completed and exited.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    End
                End If

                '//stop the scheduler if its running
                clsServiceController.itemGlobal.StopScheduler()

                '//kill any other crds that may be running
                Dim myID As Integer = Process.GetCurrentProcess.Id

                For Each p As Process In Process.GetProcessesByName("sqlrd")
                    If p.Id <> myID Then
                        p.Kill()
                    End If
                Next

                Dim pm As Process = New Process

                pm.StartInfo.FileName = Application.StartupPath & "\dmg.exe"
                pm.StartInfo.UseShellExecute = True
                pm.Start()

                pm.WaitForExit()

                Dim migrated As Boolean = False

                Dim result As String = "false"

                Try
                    If IO.File.Exists(Application.StartupPath & "\dmg.result") Then
                        result = ReadTextFromFile(Application.StartupPath & "\dmg.result")
                    End If
                Catch : End Try

                If String.Compare(result.Trim, "true", True) = 0 Then
                    migrated = True
                Else
                    migrated = False
                End If

                If migrated = True Then
                    clsMarsUI.MainUI.SaveRegistry("ConType", "LOCAL", , configFile, True)

                    Dim workStation As String = Environment.MachineName
                    Dim instanceName As String = workStation & "\SQL-RD"
                    Dim conString As String = Me.m_localConString

                    clsMarsUI.MainUI.SaveRegistry("ConString", conString, True, configFile, True)
                Else
                    MessageBox.Show("SQL-RD did not configure your database and will now exit.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Warning)

                    End
                End If
            End If
        Catch : End Try

    End Function
End Class
