Public Class clsUnhandledException

    Public Sub onUnhandledException(ByVal sender As Object, ByVal e As UnhandledExceptionEventArgs)
        If RunEditor = True Then
            Dim ex As Exception = e.ExceptionObject

            Dim frmU As frmUnhandledException = New frmUnhandledException

            frmU.showError(ex)
        Else
            Dim ex As Exception = e.ExceptionObject

            _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
        End If
    End Sub
    Public Sub onThreadException(ByVal sender As Object, ByVal e As Threading.ThreadExceptionEventArgs)
        If RunEditor = True Then
            Dim ex As Exception = e.Exception

            Dim frmU As frmUnhandledException = New frmUnhandledException

            frmU.showError(ex)
        Else
            Dim ex As Exception = e.Exception

            _ErrorHandle(ex.ToString, Err.Number, ex.Source, 0, , True, True, 1)
        End If
    End Sub
End Class
