Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports System.Text


Public Class clsListener
    Dim listenPort As Integer = 2908
    Dim recvdData As String = ""
    Dim tcpAddress As String
    Dim tcpMsg As String

    Public Property m_listenPort() As Integer
        Get
            Return listenPort
        End Get
        Set(ByVal value As Integer)
            listenPort = value
        End Set
    End Property

    Public Property m_recievedData() As String
        Set(ByVal value As String)
            recvdData = value
        End Set
        Get
            Return recvdData
        End Get
    End Property

    Public Property m_TcpAddress() As String
        Get
            Return tcpAddress
        End Get
        Set(ByVal value As String)
            tcpAddress = value
        End Set
    End Property

    Public Property m_tcpMsg() As String
        Get
            Return tcpMsg
        End Get
        Set(ByVal value As String)
            tcpMsg = value
        End Set
    End Property

    Public Sub TcpSend()
        Dim o As New TcpClient
        Try
            o.Connect(tcpAddress, listenPort)

            o.SendTimeout = clsMarsUI.MainUI.ReadRegistry("SlaveTimeout", 90)

            Dim networkStream As NetworkStream = o.GetStream()

            If networkStream.CanWrite And networkStream.CanRead Then
                ' Do a simple write.
                Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(tcpMsg)

                networkStream.Write(sendBytes, 0, sendBytes.Length)
                ' Read the NetworkStream into a byte buffer.
                Dim bytes(o.ReceiveBufferSize) As Byte
                networkStream.Read(bytes, 0, CInt(o.ReceiveBufferSize))
                ' Output the data received from the host to the console.
                Dim returndata As String = Encoding.ASCII.GetString(bytes)

                ''console.writeLine(("Host returned: " + returndata))

                networkStream.Close(30)
            Else
                If Not networkStream.CanRead Then
                    Throw New Exception("cannot not write data to this stream")
                    o.Close()
                Else
                    If Not networkStream.CanWrite Then
                        Throw New Exception("cannot read data from this stream")
                        o.Close()
                    End If
                End If
            End If
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub
    Public Sub Listen()

        Dim tcpListener As New TcpListener(IPAddress.Any, listenPort)

        tcpListener.Start()

        'Accept the pending client connection and return             'a TcpClient initialized for communication. 
        Dim tcpClient As TcpClient = tcpListener.AcceptTcpClient()

        Try
            Dim networkStream As NetworkStream = tcpClient.GetStream()
            ' Read the stream into a byte array
            Dim bytes(tcpClient.ReceiveBufferSize) As Byte
            networkStream.Read(bytes, 0, CInt(tcpClient.ReceiveBufferSize))
            ' Return the data received from the client to the console.
            Dim clientdata As String = Encoding.ASCII.GetString(bytes)

            Dim responseString As String = "OK"
            Dim sendBytes As [Byte]() = Encoding.ASCII.GetBytes(responseString)
            networkStream.Write(sendBytes, 0, sendBytes.Length)

            recvdData = clientdata

            If recvdData.IndexOf(";") > -1 Then

                Dim dataThread As New Thread(AddressOf ProcessData)

                dataThread.Start()

            End If

            'Any communication with the remote client using the TcpClient can go here.
            'Close TcpListener and TcpClient.

            tcpClient.Close()
            tcpListener.Stop()

            Dim oT As New System.Threading.Thread(AddressOf Listen)

            oT.Start()
        Catch ex As Exception
            tcpClient.Close()
            tcpListener.Stop()

            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Dim oT As New System.Threading.Thread(AddressOf Listen)

            oT.Start()
        End Try
    End Sub

    Public Sub ProcessData()
        Try
            Dim type As String
            Dim id As Integer
            Dim thread As Process
            Dim data As String = Me.m_recievedData
            Dim exePath As String = My.Application.Info.DirectoryPath

            If exePath.EndsWith("\") = False Then exePath &= "\"

            type = data.Split(";")(0)
            id = data.Split(";")(1)

            Select Case type.ToLower
                Case "report"

                    If clsMarsData.IsDuplicate("ThreadManager", "ReportID", id, False) = False Then
                        thread = New Process
                        thread.StartInfo.Arguments = "!xs " & id
                        thread.StartInfo.FileName = exePath & assemblyName
                        thread.Start()
                    End If

                Case "package"
                    If clsMarsData.IsDuplicate("ThreadManager", "PackID", id, False) = False Then
                        thread = New Process
                        thread.StartInfo.Arguments = "!xp " & id
                        thread.StartInfo.FileName = exePath & assemblyName
                        thread.Start()
                    End If
                Case "automation"
                    If clsMarsData.IsDuplicate("ThreadManager", "AutoID", id, False) = False Then
                        thread = New Process
                        thread.StartInfo.Arguments = "!xa " & id
                        thread.StartInfo.FileName = exePath & assemblyName
                        thread.Start()
                    End If
            End Select
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _
            _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

End Class


