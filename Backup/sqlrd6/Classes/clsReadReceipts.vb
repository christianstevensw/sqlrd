Imports MAPI
Imports sqlrd.clsMarsUI

Public Class clsReadReceipts

    Public Shared Sub ProcessReceipts()
        Try
            Dim SQL As String
            Dim oRs As ADODB.Recordset

            SQL = "SELECT * FROM ReadReceiptsAttr"

            oRs = clsMarsData.GetData(SQL)

            If oRs Is Nothing Then Return

            Do While oRs.EOF = False
                Dim destinationID As Integer
                Dim waitFor As Double
                Dim repeat As Boolean = False
                Dim repeatEvery As Double
                Dim receiptID As Integer
                Dim readOnes() As Integer
                Dim I As Integer = 0
                Dim logID As Integer

                destinationID = oRs("destinationid").Value
                receiptID = oRs("receiptid").Value
                waitFor = oRs("waitlength").Value

                Try
                    repeat = Convert.ToBoolean(oRs("repeat").Value)
                Catch : End Try

                repeatEvery = oRs("repeatevery").Value

                'check if read receipt exists
                Dim oRs1 As ADODB.Recordset

                SQL = "SELECT * FROM ReadReceiptsLog WHERE DestinationID =" & destinationID & " AND RRStatus = 'Pending'"

                oRs1 = clsMarsData.GetData(SQL)

                If oRs1 Is Nothing Then Continue Do

                I = 0

                ReDim readOnes(I)

                Do While oRs1.EOF = False
                    Dim emailSubject As String
                    Dim entryDate As Date
                    Dim sentTo As String
                    Dim resultTable As Hashtable = New Hashtable

                    logID = oRs1("logid").Value
                    emailSubject = oRs1("emailsubject").Value
                    entryDate = oRs1("nextcheck").Value
                    sentTo = oRs1("sentto").Value


                    If clsReadReceipts.ReceiptExists(emailSubject, sentTo) Then
                        ReDim Preserve readOnes(I)

                        readOnes(I) = logID

                        I += 1
                    Else
                        Dim waitedFor As Double

                        waitedFor = Now.Subtract(entryDate).TotalMinutes

                        If waitedFor > waitFor Then
                            Dim oTask As clsMarsTask = New clsMarsTask

                            oTask.ProcessTasks(receiptID, "NONE")
                        End If

                        clsMarsData.WriteData("UPDATE ReadReceiptsLog SET LastCheck = '" & ConDateTime(Now) & "', NextCheck ='" & ConDateTime(Now.AddMinutes(repeatEvery)) & "' WHERE LogID =" & logID)
                    End If

                    oRs1.MoveNext()
                Loop

                For Each n As Integer In readOnes
                    If n > 0 Then clsMarsData.WriteData("UPDATE ReadReceiptsLog SET RRStatus = 'Received', " & _
                    "LastCheck ='" & ConDateTime(Now) & "' WHERE LogID=" & n)
                Next

                oRs1.Close()

                oRs.MoveNext()
            Loop

            oRs.Close()
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
        End Try
    End Sub

    Private Shared Function ReceiptExists(ByVal subject As String, ByVal sentTo As String) As Boolean
        Try
            Dim cdoSession As Object 'Session

            'create a mail session object
            cdoSession = CreateObject("Redemption.RDOSession") 'New Session
            Dim cdoFolders As Object 'Folders
            'Dim cdoFolder As Folder
            Dim cdoMessage As Object 'Message

            Dim sMode As String = MainUI.ReadRegistry("SQL-RDService", "")

            Dim sArgs() As String = Environment.GetCommandLineArgs

            Dim userid As String = MainUI.ReadRegistry("MAPIProfile", "")

            Dim password As String = MainUI.ReadRegistry("MAPIPassword", "", True)

            Dim mapiType As String = MainUI.ReadRegistry("MAPIType", "")

            Dim sAlias As String = MainUI.ReadRegistry("MAPIAlias", "")

            Dim sxServer As String = MainUI.ReadRegistry("MAPIServer", "")

            Dim EmailRetry As String = Convert.ToBoolean(Convert.ToInt32(MainUI.ReadRegistry("EmailRetry", 0)))

            If mapiType = "Single" Then
                cdoSession.Logon(userid, password, False, True)
            Else
                cdoSession.LogonExchangeMailbox(sAlias, sxServer) ', , , True, , _
                'NoMail:=True, _
                '   ProfileInfo:=sxServer & vbLf & sAlias)
            End If

            'create a folder object in the session's inbox
            cdoFolders = cdoSession.GetDefaultFolder(6).Folders 'cdoSession.Inbox.Folders
            'loop through all the folders
            'For I As Integer = 1 To cdoFolders.Count
            'cdoFolder = cdoFolders.Item(I)

            Dim inbox As Object = cdoSession.GetDefaultFolder(6)

            For Each cdoMessage In inbox.Items 'cdoSession.Inbox.messages
                Dim oSafe As Redemption.SafeMailItem = New Redemption.SafeMailItem

                Dim mailSubject As String
                Dim mailSender, mailSenderAddress As String
                Dim addressEntry As Redemption.AddressEntry

                oSafe.Item = cdoMessage

                mailSubject = oSafe.subject
                addressEntry = oSafe.Sender
                mailSender = addressEntry.Name
                mailSenderAddress = addressEntry.SMTPAddress

                'console.writeLine(mailSender & " (" & mailSenderAddress & ") :" & mailSubject)

                If LCase(mailSubject) = "read: " & LCase(subject) Then

                    If String.Compare(mailSender, sentTo, True) = 0 Then
                        Return True
                    ElseIf String.Compare(mailSenderAddress, sentTo, True) = 0 Then
                        Return True
                    End If
                End If

                Try
                    If LCase(mailSubject) = "read: " & LCase(subject) And (LCase(mailSender) = LCase(sentTo) Or LCase(mailSenderAddress) = LCase(sentTo)) Then
                        Return True
                    End If
                Catch : End Try

                oSafe = Nothing
            Next

            ' Next

            cdoFolders = Nothing
            cdoSession = Nothing

            Return False
        Catch ex As Exception
            _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))

            Return False
        End Try

    End Function
End Class
