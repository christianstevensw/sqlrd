Imports DevComponents.DotNetBar
Imports Xceed.Grid
Imports Xceed.Grid.Editors
Imports Xceed.Grid.Viewers
Imports Xceed.Grid.Collections
Imports Xceed.Editors
Imports Xceed.Grid.Reporting
Public Class frmHistoryReport

    Public Sub systemDumpReport(ByVal SQL As String, ByVal scheduleType As String)
        historyGrid.Clear()

        historyGrid.FixedHeaderRows.Add(New Xceed.Grid.GroupByRow)
        historyGrid.FixedHeaderRows.Add(New Xceed.Grid.ColumnManagerRow)
        historyGrid.ApplyStyleSheet(StyleSheet.Default)



        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

        Dim oTable As DataTable = New DataTable(scheduleType)

        Dim I As Integer = 0

        For Each field As ADODB.Field In oRs.Fields
            Try
                oTable.Columns.Add(field.Name)
            Catch ex As Exception
                oTable.Columns.Add(field.Name & "_" & I)
                I += 1
            End Try
        Next

        Do While oRs.EOF = False
            I = 0
            Dim row As System.Data.DataRow = oTable.Rows.Add()

            For Each field As ADODB.Field In oRs.Fields
                row(I) = oRs(I).Value
                I += 1
            Next

            oRs.MoveNext()
        Loop

        oRs.Close()

        Dim ds As DataSet = New DataSet("Dump_Set")

        ds.Tables.Add(oTable)

        historyGrid.BeginInit()
        historyGrid.DataSource = ds
        historyGrid.DataMember = scheduleType

        historyGrid.EndInit()

        Me.Text = "System Data: " & scheduleType
        Me.MdiParent = oMain
        Me.Show()

    End Sub
    Public Sub viewReportHistory(ByVal scheduleType As clsMarsScheduler.enScheduleType, Optional ByVal nID As Integer = 0)
        clsMarsScheduler.globalItem.DrawScheduleHistory6(Me.historyGrid, scheduleType, nID)

        Select Case scheduleType
            Case clsMarsScheduler.enScheduleType.AUTOMATION
                Me.Text = "Automation Schedule History"
            Case clsMarsScheduler.enScheduleType.EVENTBASED
                Me.Text = "Event-Based Schedule History"
            Case clsMarsScheduler.enScheduleType.EVENTPACKAGE
                Me.Text = "Event-Based Package History"
            Case clsMarsScheduler.enScheduleType.PACKAGE
                Me.Text = "Package Schedule History"
            Case clsMarsScheduler.enScheduleType.REPORT
                Me.Text = "Single Schedule History"
            Case clsMarsScheduler.enScheduleType.SMARTFOLDER
                Me.Text = "Smart Folder History"
        End Select

        Me.MdiParent = oMain
        Me.Show()
    End Sub

    Private Sub frmHistoryReport_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub dnbman_ItemClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dnbman.ItemClick
        Dim oItem As DevComponents.DotNetBar.BaseItem

        oItem = CType(sender, BaseItem)

        Select Case oItem.Name.ToLower
            Case "btnpreview"
                Dim report As New Report(historyGrid)

                report.ReportStyleSheet = Xceed.Grid.Reporting.ReportStyleSheet.Contemporary

                report.PrintPreview()

            Case "btnprint"
                Dim report As New Report(historyGrid)

                report.ReportStyleSheet = Xceed.Grid.Reporting.ReportStyleSheet.Contemporary

                report.Print(True)
            Case "btnreport"
                Dim reportForm As New GenerateReportForm(historyGrid)

                reportForm.ShowDialog()
            Case "btnsave"
                Dim report As New Report(historyGrid)

                Dim sfd As New SaveFileDialog

                Try
                    With sfd
                        .CheckFileExists = False
                        .DefaultExt = "pdf"
                        .Filter = "XML File|*.xml|HTML File|*.html|JPEG Image|*.jpg|Acrobat Format|*.pdf|TIFF Image|*.tif"
                        .Title = "Export Smart Folder Report..."

                        If .ShowDialog() <> Windows.Forms.DialogResult.Cancel Then
                            Dim format As Xceed.Grid.Reporting.ExportFormat

                            report.ReportStyleSheet = Xceed.Grid.Reporting.ReportStyleSheet.Contemporary

                            Dim expset As Xceed.Grid.Reporting.ExportSettings = New Xceed.Grid.Reporting.ExportSettings

                            'expset.Landscape = True
                            expset.Title = Me.Text

                            Select Case .FilterIndex
                                Case 1
                                    Dim oSet As DataSet
                                    oSet = historyGrid.DataSource

                                    oSet.WriteXml(.FileName)
                                Case 2
                                    report.Export(.FileName, ExportFormat.Html, True, True, expset)
                                Case 3
                                    report.Export(.FileName, ExportFormat.Jpeg, True, True, expset)
                                Case 4
                                    report.Export(.FileName, ExportFormat.Pdf, True, True, expset)
                                Case 5
                                    report.Export(.FileName, ExportFormat.Tiff, True, True, expset)
                            End Select

                            MessageBox.Show("Report exported successfully!", Application.ProductName, _
                                                    MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End With
                Catch ex As Exception
                    _ErrorHandle(ex.Message, Err.Number, Reflection.MethodBase.GetCurrentMethod.Name, _GetLineNumber(ex.StackTrace))
                End Try
            Case "btnchooser"
                Dim chooser As frmFieldChooser = New frmFieldChooser

                chooser.chooseFields(historyGrid, Me)
        End Select
    End Sub

End Class