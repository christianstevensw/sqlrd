Imports sqlrd.clsMarsUI
Public Class frmCollectiveProp
    Dim scheduleID As Integer = 0
    Dim nextRun As Date
    Dim m_schedulesTable As DataTable
    Dim ep As ErrorProvider = New ErrorProvider
    Dim canClose As Boolean = False
    Private ReadOnly markerID As Integer = clsMarsData.CreateDataID("reportattr", "reportid")

    Private ReadOnly Property m_Frequency() As String
        Get
            If optDaily.Checked = True Then
                Return "Daily"
            ElseIf optWeekly.Checked = True Then
                Return "Weekly"
            ElseIf Me.optWeekDaily.Checked = True Then
                Return "Week-Daily"
            ElseIf Me.optMonthly.Checked = True Then
                Return "Monthly"
            ElseIf Me.optCustom.Checked = True Then
                Return "Custom"
            ElseIf optYearly.Checked = True Then
                Return "Yearly"
            ElseIf optNone.Checked = True Then
                Return "None"
            Else
                Return ""
            End If
        End Get
    End Property
    Private Sub chkReportLocation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReportLocation.CheckedChanged
        Me.grpReportLocation.Enabled = Me.chkReportLocation.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkDesc_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDesc.CheckedChanged
        Me.grpDesc.Enabled = Me.chkDesc.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkKeyword_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkKeyword.CheckedChanged
        Me.grpKeyword.Enabled = Me.chkKeyword.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkFrequency_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFrequency.CheckedChanged
        Me.grpFrequency.Enabled = Me.chkFrequency.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkExceptionCal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkExceptionCal.CheckedChanged
        Me.grpExceptionCal.Enabled = Me.chkExceptionCal.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkScheduleTime_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkScheduleTime.CheckedChanged
        Me.grpScheduleTime.Enabled = Me.chkScheduleTime.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkEndDate_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEndDate.CheckedChanged
        Me.grpEndDate.Enabled = Me.chkEndDate.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkRepeatEvery_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRepeatEvery.CheckedChanged
        Me.grpRepeat.Enabled = Me.chkRepeatEvery.Checked
        cmdApply.Enabled = True
    End Sub


    Private Sub chkLoginReq_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLoginReq.CheckedChanged
        Me.grpLoginReq.Enabled = Me.chkLoginReq.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkDest_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDest.CheckedChanged
        Me.UcDest.Enabled = chkDest.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkRetry_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRetry.CheckedChanged
        Me.grpRetry.Enabled = Me.chkRetry.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkAutoFail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAutoFail.CheckedChanged
        Me.grpAutoFail.Enabled = Me.chkAutoFail.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkBlankReport_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBlankReport.CheckedChanged
        Me.grpBlankReports.Enabled = Me.chkBlankReport.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkTasks_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTasks.CheckedChanged
        Me.UcTasks.Enabled = Me.chkTasks.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub cmdDbLoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim I As Int32

        Dim sDBLoc As String
        Dim ofd As FolderBrowserDialog = New FolderBrowserDialog


        ofd.Description = "Select the path to your reports"

        If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
            sDBLoc = _CreateUNC(ofd.SelectedPath)

            If sDBLoc.EndsWith("\") = False Then sDBLoc &= "\"
        End If

        txtDBLoc.Text = sDBLoc

    End Sub

    Private Sub cmdDaily_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdDaily.Click
        Dim oForm As New frmScheduleOptions

        nextRun = oForm.DailyOptions(scheduleID)

    End Sub

    Private Sub cmdWeekly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdWeekly.Click
        Dim oForm As New frmScheduleOptions

        oForm.WeeklyOptions(scheduleID, nextRun)

    End Sub

    Private Sub cmdMonthly_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMonthly.Click

        Dim oForm As New frmScheduleOptions

        oForm.MonthlyOptions(scheduleID, nextRun)

    End Sub

    Private Sub optCustom_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optCustom.CheckedChanged
        If optCustom.Checked = True Then
            cmbCustom.Enabled = True
        End If
    End Sub

    Private Sub optDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optDaily.CheckedChanged
        Me.cmdDaily.Enabled = optDaily.Checked
    End Sub

    Private Sub optWeekly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optWeekly.CheckedChanged
        Me.cmdWeekly.Enabled = optWeekly.Checked
    End Sub

    Private Sub optMonthly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optMonthly.CheckedChanged
        Me.cmdMonthly.Enabled = optMonthly.Checked
    End Sub

    Private Sub chkException_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkException.CheckedChanged
        If chkException.Checked And gnEdition < gEdition.GOLD Then
            _NeedUpgrade(gEdition.GOLD)
            chkException.Checked = False
            Return
        End If

        cmbException.Enabled = chkException.Checked
    End Sub

    Private Sub cmbCustom_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCustom.DropDown
        cmbCustom.Items.Clear()
        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbCustom.Items.Add("[New...]")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbCustom.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If
    End Sub

    Private Sub cmbException_DropDown(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbException.DropDown
        Me.cmbException.Items.Clear()

        Dim oRs As ADODB.Recordset
        Dim oData As New clsMarsData

        oRs = clsMarsData.GetData("SELECT DISTINCT CalendarName FROM CalendarAttr ORDER BY CalendarName")

        cmbException.Items.Add("[New...]")

        If Not oRs Is Nothing Then
            Do While oRs.EOF = False
                cmbException.Items.Add(oRs.Fields(0).Value)
                oRs.MoveNext()
            Loop

            oRs.Close()
        End If

    End Sub

    Private Sub cmbException_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbException.SelectedIndexChanged
        If cmbException.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar
                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbException_DropDown(sender, e)
                    cmbException.SelectedIndex = cmbException.Items.IndexOf(temp)
                End If
            End If
        End If

    End Sub

    Private Sub cmbCustom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbCustom.SelectedIndexChanged
        If cmbCustom.Text = "[New...]" Then
            If clsMarsSecurity._HasGroupAccess("Custom Calendar") = True Then
                Dim oForm As New frmAddCalendar

                Dim temp As String = oForm.AddCalendar("")

                If temp <> "" Then
                    cmbCustom_DropDown(sender, e)
                    cmbCustom.SelectedIndex = cmbCustom.Items.IndexOf(temp)
                End If
            End If
        Else
            Dim oS As New clsMarsScheduler

            nextRun = clsMarsScheduler.globalItem.GetCustomNextRunDate(cmbCustom.Text, Now.Date)

            EndDate.Value = oS.GetCustomEndDate(cmbCustom.Text)
        End If
    End Sub

    Public Function editSchedules(ByVal schedulesTable As DataTable)
        Me.m_schedulesTable = schedulesTable

        Me.ShowDialog()
    End Function

    Private Sub saveDestinations()
        Dim cols As String
        Dim vals As String
        Dim SQL As String
        Dim nID As Integer

        cols = clsMarsData.DataItem.GetColumns("DestinationAttr")

        For Each row As DataRow In m_schedulesTable.Rows
            Dim destID As Integer = clsMarsData.CreateDataID("destinationattr", "destinationid")
            nID = row("ID")

            Select Case row("scheduleType").ToString.ToLower
                Case "report"
                    vals = cols.ToLower
                    vals = vals.Replace("[destinationid]", destID)
                    vals = vals.Replace("[reportid]", nID)
                    vals = vals.Replace("[packid]", 0)

                    SQL = "INSERT INTO DestinationAttr (" & cols & ") " & _
                    "SELECT " & vals & " FROM DestinationAttr WHERE ReportID =" & Me.markerID

                    If clsMarsData.WriteData(SQL) = True Then
                        Dim optCols As String = ""
                        Dim optVals As String = ""

                        'save report format options
                        optCols = clsMarsData.DataItem.GetColumns("ReportOptions")

                        SQL = "SELECT * FROM ReportOptions WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & Me.markerID & ")"

                        Dim oRs As ADODB.Recordset = clsMarsData.GetData(SQL)

                        If oRs IsNot Nothing Then
                            Do While oRs.EOF = False

                                For Each s As String In optCols.Split(",")
                                    If s.Length = 0 Then Continue For

                                    s = s.Replace("[", "").Replace("]", "")

                                    If s.ToLower = "reportid" Then
                                        optVals &= "0,"
                                    ElseIf s.ToLower = "destinationid" Then
                                        optVals &= destID & ","
                                    ElseIf s.ToLower = "optionid" Then
                                        optVals &= clsMarsData.CreateDataID("reportoptions", "optionid") & ","
                                    Else
                                        Dim temp = IsNull(oRs(s).Value)

                                        Try
                                            Int32.Parse(temp)
                                        Catch ex As Exception
                                            temp = "'" & SQLPrepare(temp) & "'"
                                        End Try

                                        optVals &= temp & ","
                                    End If
                                Next

                                optVals = optVals.Substring(0, (optVals.Length - 1))

                                SQL = "INSERT INTO ReportOptions (" & optCols & ") VALUES (" & optVals & ")"

                                clsMarsData.WriteData(SQL)

                                oRs.MoveNext()
                            Loop

                            oRs.Close()

                            'save printer attr
                            optCols = clsMarsData.DataItem.GetColumns("PrinterAttr")

                            SQL = "SELECT * FROM PrinterAttr WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & Me.markerID & ")"

                            oRs = clsMarsData.GetData(SQL)

                            If oRs IsNot Nothing Then
                                Do While oRs.EOF = False

                                    For Each s As String In optCols.Split(",")
                                        If s.Length = 0 Then Continue For

                                        s = s.Replace("[", "").Replace("]", "")

                                        If s.ToLower = "destinationid" Then
                                            optVals &= destID & ","
                                        ElseIf s.ToLower = "printerid" Then
                                            optVals &= clsMarsData.CreateDataID("printerattr", "printerid") & ","
                                        Else
                                            Dim temp = IsNull(oRs(s).Value)

                                            Try
                                                Int32.Parse(temp)
                                            Catch ex As Exception
                                                temp = "'" & SQLPrepare(temp) & "'"
                                            End Try

                                            optVals &= temp & ","
                                        End If
                                    Next

                                    optVals = optVals.Substring(0, (optVals.Length - 1))

                                    SQL = "INSERT INTO PrinterAttr (" & optCols & ") VALUES (" & optVals & ")"

                                    clsMarsData.WriteData(SQL)

                                    oRs.MoveNext()
                                Loop

                                oRs.Close()
                            End If
                        End If
                    End If
                Case "package"
                    vals = cols.ToLower
                    vals = vals.Replace("[destinationid]", destID)
                    vals = vals.Replace("[reportid]", 0)
                    vals = vals.Replace("[packid]", nID)
                    vals = vals.Replace("[outputformat]", "'Package'")

                    SQL = "INSERT INTO DestinationAttr (" & cols & ") " & _
                    "SELECT " & vals & " FROM DestinationAttr WHERE ReportID =" & Me.markerID

                    If clsMarsData.WriteData(SQL) = True Then
                        'save printer attr

                        Dim optCols As String = clsMarsData.DataItem.GetColumns("PrinterAttr")
                        Dim optVals As String = ""
                        Dim oRs As ADODB.Recordset

                        SQL = "SELECT * FROM PrinterAttr WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & Me.markerID & ")"

                        oRs = clsMarsData.GetData(SQL)

                        If oRs IsNot Nothing Then
                            Do While oRs.EOF = False

                                For Each s As String In optCols.Split(",")
                                    If s.Length = 0 Then Continue For

                                    s = s.Replace("[", "").Replace("]", "")

                                    If s.ToLower = "destinationid" Then
                                        optVals &= destID & ","
                                    ElseIf s.ToLower = "printerid" Then
                                        optVals &= clsMarsData.CreateDataID("printerattr", "printerid") & ","
                                    Else
                                        Dim temp = IsNull(oRs(s).Value)

                                        Try
                                            Int32.Parse(temp)
                                        Catch ex As Exception
                                            temp = "'" & SQLPrepare(temp) & "'"
                                        End Try

                                        optVals &= temp & ","
                                    End If
                                Next

                                optVals = optVals.Substring(0, (optVals.Length - 1))

                                SQL = "INSERT INTO PrinterAttr (" & optCols & ") VALUES (" & optVals & ")"

                                clsMarsData.WriteData(SQL)

                                oRs.MoveNext()
                            Loop

                            oRs.Close()
                        End If
                    End If
                Case Else
                    Continue For
            End Select
        Next

        clsMarsData.WriteData("DELETE FROM ReportOptions WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & markerID & ")")
        clsMarsData.WriteData("DELETE FROM PrinterAttr WHERE DestinationID IN (SELECT DestinationID FROM DestinationAttr WHERE ReportID =" & markerID & ")")
        clsMarsData.WriteData("DELETE FROM DestinationAttr WHERE ReportID =" & Me.markerID)

    End Sub
    Private Function saveDatasources()
        Dim SQL As String
        Dim nID As Integer
        Dim scheduleID As Integer
        Dim oRs As ADODB.Recordset
        Dim cols, vals As String

        cols = "DatasourceID,ReportID,DatasourceName,RptUserID,RptPassword"

        For Each row As DataRow In Me.m_schedulesTable.Rows
            nID = row("ID")

            Select Case row("scheduleType").ToString.ToLower
                Case "report", "packed report", "package"

                    Dim temp As ArrayList

                    If row("scheduleType").ToString.ToLower = "package" Then
                        temp = clsMarsReport.getPackagedReports(nID)
                    Else
                        temp = New ArrayList

                        temp.Add(nID)
                    End If

                    If temp IsNot Nothing Then
                        For Each nReportID As Integer In temp
                            For Each lsv As ListViewItem In Me.lsvDatasources.Items
                                Dim dsName As String = lsv.Text

                                SQL = "SELECT * FROM ReportDatasource WHERE DatasourceName ='" & SQLPrepare(dsName) & "' AND ReportID =" & nReportID

                                oRs = clsMarsData.GetData(SQL)

                                If oRs IsNot Nothing Then
                                    If oRs.EOF = False Then 'a ds already exists with this name
                                        SQL = "UPDATE ReportDatasource SET " & _
                                        "RptUserID = '" & SQLPrepare(lsv.SubItems(1).Text) & "'," & _
                                        "RptPassword = '" & SQLPrepare(_EncryptDBValue(lsv.Tag)) & "' " & _
                                        "WHERE DatasourceName ='" & SQLPrepare(lsv.Text) & "' AND " & _
                                        "ReportID = " & nReportID
                                    Else 'one doesnt exist so we must add it
INSERT:
                                        vals = clsMarsData.CreateDataID("ReportDatasource", "DatasourceID") & "," & _
                                        nReportID & "," & _
                                        "'" & SQLPrepare(lsv.Text) & "'," & _
                                        "'" & SQLPrepare(lsv.SubItems(1).Text) & "'," & _
                                        "'" & SQLPrepare(_EncryptDBValue(lsv.Tag)) & "' "

                                        clsMarsData.DataItem.InsertData("ReportDatasource", cols, vals)
                                    End If
                                Else
                                    GoTo INSERT
                                End If
                            Next
                        Next
                    End If
            End Select
        Next
    End Function
    Private Function saveTasks()
        Dim SQL As String
        Dim nID As Integer
        Dim scheduleID As Integer
        Dim oRs As ADODB.Recordset
        Dim cols As String = clsMarsData.DataItem.GetColumns("Tasks")
        Dim vals As String = ""

        For Each row As DataRow In m_schedulesTable.Rows
            scheduleID = row("scheduleid")

            If scheduleID = 0 Then
                Continue For
            Else

                Dim taskID As Integer = clsMarsData.CreateDataID("tasks", "taskid")

                SQL = "SELECT * FROM Tasks WHERE scheduleID =" & Me.markerID

                oRs = clsMarsData.GetData(SQL)

                If oRs IsNot Nothing Then
                    Do While oRs.EOF = False
                        vals = ""

                        For Each s As String In cols.Split(",")
                            If s.Length = 0 Then Continue For

                            s = s.Replace("[", "").Replace("]", "")

                            If s.ToLower = "scheduleid" Then
                                vals &= scheduleID & ","
                            ElseIf s.ToLower = "taskid" Then
                                vals &= clsMarsData.CreateDataID("tasks", "taskid") & ","
                            Else
                                Dim temp = IsNull(oRs(s).Value)

                                Try
                                    Int32.Parse(temp)
                                Catch ex As Exception
                                    temp = "'" & SQLPrepare(temp) & "'"
                                End Try

                                vals &= temp & ","
                            End If

                        Next

                        vals = vals.Substring(0, (vals.Length - 1))

                        SQL = "INSERT INTO Tasks (" & cols & ") VALUES (" & vals & ")"

                        clsMarsData.WriteData(SQL)

                        oRs.MoveNext()
                    Loop

                    oRs.Close()
                End If
            End If
        Next

        clsMarsData.WriteData("DELETE FROM Tasks WHERE ScheduleID =" & markerID)
    End Function
    Private Sub cmdOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdOK.Click
        cmdOK.Enabled = False

        cmdApply_Click(sender, e)
        
        If Me.chkDest.Checked Then
            'destination
            If UcDest.lsvDestination.Items.Count = 0 Then
                ep.SetError(UcDest.lsvDestination, "Please add a destination output")
                UcDest.cmdAdd.Focus()
                Me.tabCollective.SelectedTab = Me.tbOutput
                Return
            Else
                Me.saveDestinations()
            End If
        End If

        If Me.chkTasks.Checked Then
            If UcTasks.lsvTasks.Items.Count = 0 Then
                ep.SetError(UcTasks.lsvTasks, "Please add at least one task")
                UcDest.cmdAdd.Focus()
                Me.tabCollective.SelectedTab = Me.tbTasks
                Return
            Else
                Me.saveTasks()
            End If
        End If

        If Me.chkLoginReq.Checked Then
            If Me.lsvDatasources.Items.Count = 0 Then
                ep.SetError(Me.lsvDatasources, "Please add at least one data source")
                Me.lsvDatasources.Focus()
                Me.tabCollective.SelectedTab = Me.tbReportOptions
                Return
            Else
                Me.saveDatasources()
            End If
        End If
        If canClose = True Then Close()

        Me.m_schedulesTable.Dispose()
        Me.m_schedulesTable = Nothing
        cmdOK.Enabled = True

        clsMarsUI.MainUI.RefreshView(oWindow(nWindowCurrent))
    End Sub

    Private Sub cmdApply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdApply.Click
        cmdApply.Enabled = False

        Dim SQL As String
        Dim oRs As ADODB.Recordset
        Dim nID As Integer
        Dim scheduleID As Integer
        Dim reportIDs As ArrayList = New ArrayList

        '<schedulesTable>
        '<ID>Integer</ID>
        '<ScheduleType>String</ScheduleType>
        '<ScheduleID>Integer</ScheduleID>
        '</schedulesTable>

        'schedule types 
        'report
        'package
        'automation
        'event
        'event-package
        'packaged report

        MainUI.BusyProgress(5, "Saving schedule data...")

        For Each row As DataRow In m_schedulesTable.Rows
            Dim type As String = row("scheduleType")

            nID = row("ID")

            Select Case type.ToLower
                Case "report"
                    scheduleID = clsMarsScheduler.GetScheduleID(nID)
                Case "package"
                    scheduleID = clsMarsScheduler.GetScheduleID(, nID)
                Case "automation"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , nID)
                Case "event-package"
                    scheduleID = clsMarsScheduler.GetScheduleID(, , , , nID)
                Case "packed report"
                    Dim oID As ADODB.Recordset = clsMarsData.GetData("SELECT PackID FROM ReportAttr WHERE ReportID =" & nID)

                    If oID IsNot Nothing Then
                        If oID.EOF = False Then
                            nID = oID(0).Value
                        End If

                        oID.Close()
                    End If

                    scheduleID = clsMarsScheduler.GetScheduleID(, nID)
                Case Else
                    scheduleID = 0
            End Select

            row("scheduleID") = scheduleID
        Next

        MainUI.BusyProgress(10, "Saving schedule data...")

        If chkReportLocation.Checked = True Then
            'code for updating report location for all single based schedules
            If txtDBLoc.Text = "" Then
                tabCollective.SelectedTab = Me.tbGeneral
                ep.SetError(txtDBLoc, "Please enter the report server address")
                txtDBLoc.Focus()
                Return
            End If

            reportIDs = New ArrayList

            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduletype").ToString.ToLower
                    Case "report", "packed report"

                        reportIDs.Add(nID)

                    Case "package"
                        Dim temp As ArrayList = clsMarsReport.getPackagedReports(nID)

                        If temp IsNot Nothing Then
                            For Each n As Integer In temp
                                reportIDs.Add(n)
                            Next
                        End If
                End Select
            Next

            For Each n As Integer In reportIDs
                nID = n

                SQL = "UPDATE ReportAttr SET DatabasePath ='" & SQLPrepare(txtDBLoc.Text) & "' WHERE ReportID =" & nID

                clsMarsData.WriteData(SQL)
                    
            Next

        End If

        MainUI.BusyProgress(15, "Saving schedule data...")

        If Me.chkDesc.Checked = True Then
            'update desc
            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                If row("scheduletype").ToString.ToLower = "event" Then
                    SQL = "UPDATE EventAttr6 SET Description ='" & SQLPrepare(txtDesc.Text) & "' WHERE EventID =" & row("ID")

                    clsMarsData.WriteData(SQL)
                Else
                    SQL = "UPDATE ScheduleAttr SET Description ='" & SQLPrepare(txtDesc.Text) & "' WHERE ScheduleID =" & scheduleID

                    clsMarsData.WriteData(SQL)
                End If

            Next
        End If

        MainUI.BusyProgress(20, "Saving schedule data...")

        If Me.chkKeyword.Checked = True Then
            'update keyword
            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                If row("scheduletype").ToString.ToLower = "event" Then
                    SQL = "UPDATE EventAttr6 SET Keyword ='" & SQLPrepare(txtKeyWord.Text) & "' WHERE EventID =" & row("ID")

                    clsMarsData.WriteData(SQL)
                Else
                    SQL = "UPDATE ScheduleAttr SET Keyword ='" & SQLPrepare(txtKeyWord.Text) & "' WHERE ScheduleID =" & scheduleID

                    clsMarsData.WriteData(SQL)
                End If

            Next
        End If

        MainUI.BusyProgress(25, "Saving schedule data...")

        If Me.chkFrequency.Checked = True Then
            'frequency
            If Me.m_Frequency = "" Then
                tabCollective.SelectedTab = Me.tbSchedule
                ep.SetError(optDaily, "Please choose the schedule frequency")
                Return
            ElseIf optCustom.Checked And cmbCustom.Text = "" Then
                ep.SetError(cmbCustom, "Please select the custom calendar to use")
                cmbCustom.Focus()
                Me.tabCollective.SelectedTab = tbSchedule
                Return
            End If

            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                SQL = "UPDATE ScheduleAttr SET Frequency ='" & Me.m_Frequency & "' WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(30, "Saving schedule data...")

        If Me.chkExceptionCal.Checked Then
            ''exception cal
            If Me.cmbException.Text = "" Then
                ep.SetError(Me.cmbException, "Please select the exception calendar to use")
                Me.tabCollective.SelectedTab = tbSchedule
                Return
            End If

            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                SQL = "UPDATE ScheduleAttr SET UseException =" & Convert.ToInt32(Me.chkException.Checked) & ", " & _
                "ExceptionCalendar ='" & SQLPrepare(Me.cmbException.Text) & "' WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(35, "Saving schedule data...")

        If Me.chkNextRun.Checked Then
            Dim nextRun As Date

            For Each row As DataRow In Me.m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                oRs = clsMarsData.GetData("SELECT StartTime FROM ScheduleAttr WHERE ScheduleID =" & scheduleID)

                If oRs IsNot Nothing Then
                    If oRs.EOF = False Then
                        nextRun = ConDate(Me.dtNextRun.Value) & " " & CTimeZ(oRs("starttime").Value, dateConvertType.READ)
                    End If

                    oRs.Close()
                End If

                SQL = "UPDATE ScheduleAttr SET NextRun ='" & ConDateTime(CTimeZ(nextRun, dateConvertType.WRITE)) & "' WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(35, "Saving schedule data...")

        If Me.chkScheduleTime.Checked Then
            Dim nextRun As Date
            'schedule nextrun and start time
            For Each row As DataRow In m_schedulesTable.Rows

                scheduleID = row("scheduleid")

                oRs = clsMarsData.GetData("SELECT NextRun FROM ScheduleAttr WHERE ScheduleID =" & scheduleID)

                If oRs IsNot Nothing Then
                    If oRs.EOF = False Then
                        nextRun = CTimeZ(oRs("nextrun").Value, dateConvertType.READ)
                    End If

                    oRs.Close()
                End If

                Dim sNextRun As String = ConDate(nextRun.Date) & " " & ConTime(CTimeZ(Me.RunAt.Value, dateConvertType.WRITE))

                SQL = "UPDATE ScheduleAttr SET StartTime ='" & ConTime(Me.RunAt.Value) & "', " & _
                "NextRun ='" & sNextRun & "' WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(40, "Saving schedule data...")

        If Me.chkEndDate.Checked Then
            'end date
            Dim sEndDate As String

            If Me.chkNoEnd.Checked Then
                sEndDate = ConDate(Date.Now.AddYears(1000))
            Else
                sEndDate = ConDate(Me.EndDate.Value)
            End If

            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                SQL = "UPDATE ScheduleAttr SET EndDate ='" & sEndDate & "' " & _
                " WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next

        End If

        MainUI.BusyProgress(45, "Saving schedule data...")

        If Me.chkRepeatEvery.Checked Then
            'repeat every
            For Each row As DataRow In m_schedulesTable.Rows
                scheduleID = row("scheduleid")

                SQL = "UPDATE ScheduleAttr SET " & _
                "Repeat = 1," & _
                "RepeatInterval =" & cmbRpt.Value & "," & _
                "RepeatUntil = '" & ConTime(CTimeZ(RepeatUntil.Value, dateConvertType.WRITE)) & "'," & _
                "RepeatUnit = '" & cmbUnit.Text & "' WHERE ScheduleID =" & scheduleID

                clsMarsData.WriteData(SQL)
            Next
        End If

        reportIDs = New ArrayList

        For Each row As DataRow In m_schedulesTable.Rows
            Select Case row("scheduletype").ToString.ToLower
                Case "report", "packed report"
                    reportIDs.Add(row("ID"))
                Case "package"
                    Dim temp As ArrayList = clsMarsReport.getPackagedReports(nID)

                    If temp IsNot Nothing Then
                        For Each n As Integer In temp
                            reportIDs.Add(n)
                        Next
                    End If
            End Select
        Next

        MainUI.BusyProgress(50, "Saving data sources...")

        

        MainUI.BusyProgress(70, "Saving schedule data...")

        If Me.chkRetry.Checked Then
            'retry
            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduleType").ToString.ToLower
                    Case "report"
                        SQL = "UPDATE ReportAttr SET Retry = " & cmbRetry.Value & " WHERE ReportID =" & nID
                    Case "package"
                        SQL = "UPDATE PackageAttr SET Retry =" & cmbRetry.Value & " WHERE PackID =" & nID
                    Case Else
                        Continue For
                End Select

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(75, "Saving schedule data...")

        If Me.chkAutoFail.Checked Then
            'autofail
            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduleType").ToString.ToLower
                    Case "report"
                        SQL = "UPDATE ReportAttr SET AssumeFail = " & cmbRetry.Value & " WHERE ReportID =" & nID
                    Case "package"
                        SQL = "UPDATE PackageAttr SET AssumeFail =" & cmbRetry.Value & " WHERE PackID =" & nID
                    Case "event"
                        SQL = "UPDATE PackageAttr SET AutoFailAfter =" & cmbRetry.Value & " WHERE EventID =" & nID
                    Case Else
                        Continue For
                End Select

                clsMarsData.WriteData(SQL)
            Next
        End If

        If Me.chkAutoCalcm.Checked Then
            'auto-calc autofail
            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduleType").ToString.ToLower
                    Case "report"
                        SQL = "UPDATE ReportAttr SET AutoCalc = " & Convert.ToInt32(Me.chkAutoCalc.Checked) & " WHERE ReportID = " & nID
                    Case "package"
                        SQL = "UPDATE PackageAttr SET AutoCalc = " & Convert.ToInt32(Me.chkAutoCalc.Checked) & " WHERE PackID = " & nID
                    Case "event"
                        SQL = "UPDATE EventAttr6 SET AutoCalc = " & Convert.ToInt32(Me.chkAutoCalc.Checked) & " WHERE EventID = " & nID
                    Case Else
                        Continue For
                End Select

                clsMarsData.WriteData(SQL)
            Next
        End If


        If Me.chkRetryAfterm.Checked Then
            'retry interval
            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduleType").ToString.ToLower
                    Case "report"
                        SQL = "UPDATE ReportAttr SET RetryInterval = " & Me.txtRetryAfter.Value & " WHERE ReportID = " & nID
                    Case "package"
                        SQL = "UPDATE PackageAttr SET RetryInterval = " & Me.txtRetryAfter.Value & " WHERE PackID = " & nID
                    Case "event"
                        SQL = "UPDATE EventAttr6 SET RetryInterval = " & Me.txtRetryAfter.Value & " WHERE EventID = " & nID
                    Case Else
                        Continue For
                End Select

                clsMarsData.WriteData(SQL)
            Next
        End If

        MainUI.BusyProgress(80, "Saving schedule data...")

        If Me.chkBlankReport.Checked Then
            'blank report
            If (UcBlank.optAlert.Checked = True Or UcBlank.optAlertandReport.Checked = True) And _
                UcBlank.txtAlertTo.Text = "" Then
                Me.tabCollective.SelectedTab = Me.tbException
                UcBlank.ep.SetError(UcBlank.txtAlertTo, "Please specify the blank report alert recipients")
                UcBlank.txtAlertTo.Focus()
                Return
            End If

            Dim keyCol As String

            For Each row As DataRow In m_schedulesTable.Rows
                nID = row("ID")

                Select Case row("scheduleType").ToString.ToLower
                    Case "report"
                        SQL = "UPDATE ReportAttr SET CheckBlank = " & Convert.ToInt32(UcBlank.chkBlankReport.Checked) & " WHERE ReportID =" & nID
                        keyCol = "ReportID"
                    Case "package"
                        SQL = "UPDATE PackageAttr SET CheckBlank =" & Convert.ToInt32(UcBlank.chkBlankReport.Checked) & " WHERE PackID =" & nID
                        keyCol = "PackID"
                    Case Else
                        Continue For
                End Select

                clsMarsData.WriteData(SQL)

                clsMarsData.WriteData("DELETE FROM BlankReportAlert WHERE " & keyCol & " = " & nID)

                If UcBlank.chkBlankReport.Checked = True Then
                    Dim sBlankType As String

                    If UcBlank.optAlert.Checked = True Then
                        sBlankType = "Alert"
                    ElseIf UcBlank.optAlertandReport.Checked = True Then
                        sBlankType = "AlertandReport"
                    ElseIf UcBlank.optIgnore.Checked = True Then
                        sBlankType = "Ignore"
                    End If

                    SQL = "INSERT INTO BlankReportAlert(BlankID,[Type],[To],Msg," & keyCol & ",Subject,CustomQueryDetails) VALUES (" & _
                    clsMarsData.CreateDataID("blankreportalert", "blankid") & "," & _
                    "'" & sBlankType & "'," & _
                    "'" & SQLPrepare(UcBlank.txtAlertTo.Text) & "'," & _
                    "'" & SQLPrepare(UcBlank.txtBlankMsg.Text) & "'," & _
                    nID & "," & _
                    "'" & SQLPrepare(UcBlank.txtSubject.Text) & "'," & _
                    "'" & SQLPrepare(UcBlank.m_CustomBlankQuery) & "')"

                    clsMarsData.WriteData(SQL)
                End If
            Next
        End If

        MainUI.BusyProgress(85, "Saving schedule data...")

        If Me.chkTasks.Checked Then
            'custom tasks
        End If

        canClose = True

        MainUI.BusyProgress(, "Saving schedule data...", True)

        cmdApply.Enabled = True
    End Sub

    Private Sub frmCollectiveProp_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)

        Me.UcDest.nReportID = Me.markerID
        Me.UcDest.nPackID = 0
        Me.UcDest.nSmartID = 0

        Me.UcTasks.ScheduleID = Me.markerID
    End Sub

    Private Sub chkNextRun_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkNextRun.CheckedChanged
        Me.grpNextRunDate.Enabled = chkNextRun.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkAutoCalcm_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAutoCalcm.CheckedChanged
        grpAutoCalc.Enabled = chkAutoCalcm.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub chkRetryAfterm_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkRetryAfterm.CheckedChanged
        Me.grpRetryAfter.Enabled = Me.chkRetryAfterm.Checked
        cmdApply.Enabled = True
    End Sub

    Private Sub btnAddDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddDS.Click
        Dim result As Hashtable
        Dim oSet As frmSetTableLogin = New frmSetTableLogin

        result = oSet.getDatasourceDetails(False)

        If result IsNot Nothing Then
            Dim lsv As ListViewItem
            Dim dsName As String = result.Item("Name")
            Dim UserID As String = result.Item("UserID")
            Dim Password As String = result.Item("Password")

            Dim exists As Boolean = False

            For Each lsv In Me.lsvDatasources.Items
                If lsv.Text.ToLower = dsName.ToLower Then
                    exists = True
                    Exit For
                End If
            Next

            If exists = False Then
                lsv = New ListViewItem(dsName)
                lsv.SubItems.Add(UserID)
                lsv.Tag = Password

                Me.lsvDatasources.Items.Add(lsv)
            Else
                Dim res As DialogResult = MessageBox.Show("Update the existing '" & dsName & "'?", Application.ProductName, _
                MessageBoxButtons.YesNo, MessageBoxIcon.Question)

                If res = Windows.Forms.DialogResult.Yes Then
                    For Each lsv In Me.lsvDatasources.Items
                        If lsv.Text.ToLower = dsName.ToLower Then
                            lsv.SubItems(1).Text = UserID
                            lsv.Tag = Password
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub btnEditDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditDS.Click
        If Me.lsvDatasources.SelectedItems.Count = 0 Then Return

        Dim result As Hashtable
        Dim oSet As frmSetTableLogin = New frmSetTableLogin
        Dim lsv As ListViewItem = Me.lsvDatasources.SelectedItems(0)

        result = oSet.getDatasourceDetails(True, lsv.Text, lsv.SubItems(1).Text, lsv.Tag)

        If result IsNot Nothing Then
            Dim dsName As String = result.Item("Name")
            Dim UserID As String = result.Item("UserID")
            Dim Password As String = result.Item("Password")

            lsv.SubItems(1).Text = UserID
            lsv.Tag = Password
        End If
    End Sub

    Private Sub btnDeleteDS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteDS.Click
        If Me.lsvDatasources.SelectedItems.Count = 0 Then Return

        Dim res As DialogResult = MessageBox.Show("Remove the selected datasources?", Application.ProductName, _
        MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

        If res = Windows.Forms.DialogResult.Yes Then
            For Each lsv As ListViewItem In Me.lsvDatasources.SelectedItems
                lsv.Remove()
            Next
        End If
    End Sub
End Class