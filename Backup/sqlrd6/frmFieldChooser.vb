Imports System.IO
Imports System.Text
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Runtime.InteropServices
Imports System.Runtime.Serialization

Public Class frmFieldChooser
    Dim userCancel As Boolean = True
    Dim m_grid As Xceed.Grid.GridControl
    Dim isLoaded As Boolean = False
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        Get
            Const CS_DROPSHADOW As Object = &H20000
            Dim cp As CreateParams = MyBase.CreateParams
            Dim OSVer As Version = System.Environment.OSVersion.Version

            Select Case OSVer.Major
                Case 5
                    If OSVer.Minor > 0 Then
                        cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                    End If
                Case Is > 5
                    cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
                Case Else
            End Select

            Return cp
        End Get
    End Property

    Private Sub frmFieldChooser_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Public Sub chooseFields(ByVal grid As Xceed.Grid.GridControl, ByVal owner As Form)

        Me.m_grid = grid

        lsvFields.Items.Clear()

        For Each col As Xceed.Grid.Column In grid.Columns
            Dim item As ListViewItem = New ListViewItem

            item.Text = col.Title
            item.Checked = col.Visible

            lsvFields.Items.Add(item)
        Next

        'For Each item As ListViewItem In lsvFields.Items
        '    item.Checked = grid.Columns(item.Text).Visible
        'Next

        Me.Owner = owner
        Me.Show()

        isLoaded = True
    End Sub

    Private Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        userCancel = False

        Close()
    End Sub

    Private Sub lsvFields_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lsvFields.ItemChecked
        If isLoaded = False Then Return

        For Each item As ListViewItem In lsvFields.Items
            Try
                m_grid.Columns(item.Text).Visible = item.Checked
            Catch : End Try
        Next
    End Sub

    Private Sub chkSelect_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        If isLoaded = False Then Return

        chkSelect.Enabled = False
        lsvFields.Enabled = False

        For Each item As ListViewItem In lsvFields.Items
            item.Checked = chkSelect.Checked
        Next

        chkSelect.Enabled = True
        lsvFields.Enabled = True
    End Sub

    Private Sub SaveColumnsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveColumnsToolStripMenuItem.Click
        Dim sfd As New SaveFileDialog

        sfd.Title = "Please select the file to save to"
        sfd.Filter = "SQX File (*.sqx)|*.sqx"

        Dim dt As DataTable = New DataTable("Columns")

        dt.Columns.Add("ColumnName")
        dt.Columns.Add("Selected")
        dt.Columns.Add("Width")

        Dim row As DataRow

        For Each item As ListViewItem In Me.lsvFields.Items
            row = dt.Rows.Add

            row("ColumnName") = item.Text
            row("Selected") = Convert.ToInt32(item.Checked)
            row("Width") = Me.m_grid.Columns(item.Text).Width
        Next

        If sfd.ShowDialog = Windows.Forms.DialogResult.OK Then
            If IO.File.Exists(sfd.FileName) = True Then
                IO.File.Delete(sfd.FileName)
            End If

            Try
                Dim fs As FileStream = New FileStream(sfd.FileName, FileMode.Create, FileAccess.ReadWrite)
                Dim bf As BinaryFormatter = New BinaryFormatter

                bf.Serialize(fs, dt)

                fs.Close()
            Catch : End Try
        End If
    End Sub

    Private Sub LoadColumnSelectionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadColumnSelectionToolStripMenuItem.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog

        ofd.Title = "Please select file containing the definition"
        ofd.Filter = "SQX File (*.sqx)|*.sqx"

        If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim dt As DataTable

            Dim fs As FileStream = New FileStream(ofd.FileName, FileMode.Open, FileAccess.Read)

            Dim bf As BinaryFormatter = New BinaryFormatter

            dt = CType(bf.Deserialize(fs), DataTable)

            fs.Close()

            For Each row As DataRow In dt.Rows
                Dim itemName As String = row("ColumnName")

                For Each item As ListViewItem In lsvFields.Items
                    If item.Text = itemName Then

                        item.Checked = Convert.ToBoolean(Convert.ToInt32(row("Selected")))
                        Me.m_grid.Columns(itemName).Width = Convert.ToInt32(row("Width"))
                    End If
                Next
            Next
        End If
    End Sub
End Class