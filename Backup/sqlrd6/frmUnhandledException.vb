Public Class frmUnhandledException

    Private Sub frmUnhandledException_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FormatForWinXP(Me)
    End Sub

    Private Sub btnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Try
            Me.txtStackTrace.SelectAll()
            txtStackTrace.Copy()
            Me.txtStackTrace.SelectionStart = 0
            Me.txtStackTrace.SelectionLength = 0
        Catch : End Try
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        _ErrorHandle(Me.txtStackTrace.Text, 0, "Unhandled Exception", 0, , True, True)
        Close()
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Dim result As DialogResult = MessageBox.Show("Are you sure you would like to exit " & Application.ProductName & "?", Application.ProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Question)

        If result = Windows.Forms.DialogResult.Yes Then
            Application.Exit()
        End If
    End Sub


    Public Sub showError(ByVal ex As Exception)
        Try
            Me.txtStackTrace.Text = ex.ToString
            Me.txtStackTrace.SelectionStart = 0
            Me.txtStackTrace.SelectionLength = 0
            Label1.Text = Application.ProductName & " - Application Domain Error"

            Me.ShowDialog()
        Catch : End Try
    End Sub
End Class