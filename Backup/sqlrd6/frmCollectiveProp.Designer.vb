<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCollectiveProp
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCollectiveProp))
        Me.tabCollective = New DevComponents.DotNetBar.TabControl
        Me.TabControlPanel3 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkLoginReq = New System.Windows.Forms.CheckBox
        Me.grpLoginReq = New System.Windows.Forms.GroupBox
        Me.btnDeleteDS = New System.Windows.Forms.Button
        Me.btnEditDS = New System.Windows.Forms.Button
        Me.btnAddDS = New System.Windows.Forms.Button
        Me.lsvDatasources = New System.Windows.Forms.ListView
        Me.ColumnHeader5 = New System.Windows.Forms.ColumnHeader
        Me.ColumnHeader6 = New System.Windows.Forms.ColumnHeader
        Me.tbReportOptions = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel6 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkTasks = New System.Windows.Forms.CheckBox
        Me.UcTasks = New sqlrd.ucTasks
        Me.tbTasks = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel5 = New DevComponents.DotNetBar.TabControlPanel
        Me.grpAutoCalc = New System.Windows.Forms.GroupBox
        Me.chkAutoCalc = New System.Windows.Forms.CheckBox
        Me.chkAutoCalcm = New System.Windows.Forms.CheckBox
        Me.chkAutoFail = New System.Windows.Forms.CheckBox
        Me.grpRetryAfter = New System.Windows.Forms.GroupBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.txtRetryAfter = New System.Windows.Forms.NumericUpDown
        Me.chkBlankReport = New System.Windows.Forms.CheckBox
        Me.chkRetry = New System.Windows.Forms.CheckBox
        Me.grpBlankReports = New System.Windows.Forms.GroupBox
        Me.UcBlank = New sqlrd.ucBlankAlert
        Me.chkRetryAfterm = New System.Windows.Forms.CheckBox
        Me.grpAutoFail = New System.Windows.Forms.GroupBox
        Me.chkAssumeFail = New System.Windows.Forms.CheckBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbAssumeFail = New System.Windows.Forms.NumericUpDown
        Me.grpRetry = New System.Windows.Forms.GroupBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.cmbRetry = New System.Windows.Forms.NumericUpDown
        Me.tbException = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel4 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkDest = New System.Windows.Forms.CheckBox
        Me.UcDest = New sqlrd.ucDestination
        Me.tbOutput = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel1 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkKeyword = New System.Windows.Forms.CheckBox
        Me.chkDesc = New System.Windows.Forms.CheckBox
        Me.chkReportLocation = New System.Windows.Forms.CheckBox
        Me.grpDesc = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.txtDesc = New System.Windows.Forms.TextBox
        Me.grpReportLocation = New System.Windows.Forms.GroupBox
        Me.txtDBLoc = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.grpKeyword = New System.Windows.Forms.GroupBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtKeyWord = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tbGeneral = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.TabControlPanel2 = New DevComponents.DotNetBar.TabControlPanel
        Me.chkNextRun = New System.Windows.Forms.CheckBox
        Me.grpNextRunDate = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtNextRun = New System.Windows.Forms.DateTimePicker
        Me.chkScheduleTime = New System.Windows.Forms.CheckBox
        Me.chkEndDate = New System.Windows.Forms.CheckBox
        Me.chkRepeatEvery = New System.Windows.Forms.CheckBox
        Me.chkExceptionCal = New System.Windows.Forms.CheckBox
        Me.chkFrequency = New System.Windows.Forms.CheckBox
        Me.grpRepeat = New System.Windows.Forms.GroupBox
        Me.pnlRepeat = New System.Windows.Forms.Panel
        Me.cmbUnit = New System.Windows.Forms.ComboBox
        Me.RepeatUntil = New System.Windows.Forms.DateTimePicker
        Me.cmbRpt = New System.Windows.Forms.NumericUpDown
        Me.Label13 = New System.Windows.Forms.Label
        Me.grpEndDate = New System.Windows.Forms.GroupBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.EndDate = New System.Windows.Forms.DateTimePicker
        Me.chkNoEnd = New System.Windows.Forms.CheckBox
        Me.grpScheduleTime = New System.Windows.Forms.GroupBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.RunAt = New System.Windows.Forms.DateTimePicker
        Me.grpExceptionCal = New System.Windows.Forms.GroupBox
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.cmbException = New System.Windows.Forms.ComboBox
        Me.chkException = New System.Windows.Forms.CheckBox
        Me.grpFrequency = New System.Windows.Forms.GroupBox
        Me.cmdMonthly = New System.Windows.Forms.Button
        Me.cmbCustom = New System.Windows.Forms.ComboBox
        Me.optDaily = New System.Windows.Forms.RadioButton
        Me.optWeekDaily = New System.Windows.Forms.RadioButton
        Me.optWeekly = New System.Windows.Forms.RadioButton
        Me.optMonthly = New System.Windows.Forms.RadioButton
        Me.optYearly = New System.Windows.Forms.RadioButton
        Me.optNone = New System.Windows.Forms.RadioButton
        Me.optCustom = New System.Windows.Forms.RadioButton
        Me.cmdDaily = New System.Windows.Forms.Button
        Me.cmdWeekly = New System.Windows.Forms.Button
        Me.tbSchedule = New DevComponents.DotNetBar.TabItem(Me.components)
        Me.cmdApply = New System.Windows.Forms.Button
        Me.cmdCancel = New System.Windows.Forms.Button
        Me.cmdOK = New System.Windows.Forms.Button
        CType(Me.tabCollective, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabCollective.SuspendLayout()
        Me.TabControlPanel3.SuspendLayout()
        Me.grpLoginReq.SuspendLayout()
        Me.TabControlPanel6.SuspendLayout()
        Me.TabControlPanel5.SuspendLayout()
        Me.grpAutoCalc.SuspendLayout()
        Me.grpRetryAfter.SuspendLayout()
        CType(Me.txtRetryAfter, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpBlankReports.SuspendLayout()
        Me.grpAutoFail.SuspendLayout()
        CType(Me.cmbAssumeFail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpRetry.SuspendLayout()
        CType(Me.cmbRetry, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlPanel4.SuspendLayout()
        Me.TabControlPanel1.SuspendLayout()
        Me.grpDesc.SuspendLayout()
        Me.grpReportLocation.SuspendLayout()
        Me.grpKeyword.SuspendLayout()
        Me.TabControlPanel2.SuspendLayout()
        Me.grpNextRunDate.SuspendLayout()
        Me.grpRepeat.SuspendLayout()
        Me.pnlRepeat.SuspendLayout()
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpEndDate.SuspendLayout()
        Me.grpScheduleTime.SuspendLayout()
        Me.grpExceptionCal.SuspendLayout()
        Me.grpFrequency.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabCollective
        '
        Me.tabCollective.CanReorderTabs = True
        Me.tabCollective.Controls.Add(Me.TabControlPanel1)
        Me.tabCollective.Controls.Add(Me.TabControlPanel3)
        Me.tabCollective.Controls.Add(Me.TabControlPanel6)
        Me.tabCollective.Controls.Add(Me.TabControlPanel5)
        Me.tabCollective.Controls.Add(Me.TabControlPanel4)
        Me.tabCollective.Controls.Add(Me.TabControlPanel2)
        Me.tabCollective.Dock = System.Windows.Forms.DockStyle.Top
        Me.tabCollective.Location = New System.Drawing.Point(0, 0)
        Me.tabCollective.Name = "tabCollective"
        Me.tabCollective.SelectedTabFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold)
        Me.tabCollective.SelectedTabIndex = 0
        Me.tabCollective.Size = New System.Drawing.Size(641, 446)
        Me.tabCollective.Style = DevComponents.DotNetBar.eTabStripStyle.SimulatedTheme
        Me.tabCollective.TabAlignment = DevComponents.DotNetBar.eTabStripAlignment.Left
        Me.tabCollective.TabIndex = 0
        Me.tabCollective.TabLayoutType = DevComponents.DotNetBar.eTabLayoutType.FixedWithNavigationBox
        Me.tabCollective.Tabs.Add(Me.tbGeneral)
        Me.tabCollective.Tabs.Add(Me.tbSchedule)
        Me.tabCollective.Tabs.Add(Me.tbReportOptions)
        Me.tabCollective.Tabs.Add(Me.tbOutput)
        Me.tabCollective.Tabs.Add(Me.tbException)
        Me.tabCollective.Tabs.Add(Me.tbTasks)
        '
        'TabControlPanel3
        '
        Me.TabControlPanel3.Controls.Add(Me.chkLoginReq)
        Me.TabControlPanel3.Controls.Add(Me.grpLoginReq)
        Me.TabControlPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel3.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel3.Name = "TabControlPanel3"
        Me.TabControlPanel3.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel3.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel3.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel3.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel3.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel3.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel3.TabIndex = 3
        Me.TabControlPanel3.TabItem = Me.tbReportOptions
        '
        'chkLoginReq
        '
        Me.chkLoginReq.AutoSize = True
        Me.chkLoginReq.Location = New System.Drawing.Point(26, 218)
        Me.chkLoginReq.Name = "chkLoginReq"
        Me.chkLoginReq.Size = New System.Drawing.Size(15, 14)
        Me.chkLoginReq.TabIndex = 33
        Me.chkLoginReq.UseVisualStyleBackColor = True
        '
        'grpLoginReq
        '
        Me.grpLoginReq.BackColor = System.Drawing.Color.Transparent
        Me.grpLoginReq.Controls.Add(Me.btnDeleteDS)
        Me.grpLoginReq.Controls.Add(Me.btnEditDS)
        Me.grpLoginReq.Controls.Add(Me.btnAddDS)
        Me.grpLoginReq.Controls.Add(Me.lsvDatasources)
        Me.grpLoginReq.Enabled = False
        Me.grpLoginReq.Location = New System.Drawing.Point(63, 9)
        Me.grpLoginReq.Name = "grpLoginReq"
        Me.grpLoginReq.Size = New System.Drawing.Size(432, 433)
        Me.grpLoginReq.TabIndex = 9
        Me.grpLoginReq.TabStop = False
        '
        'btnDeleteDS
        '
        Me.btnDeleteDS.Image = CType(resources.GetObject("btnDeleteDS.Image"), System.Drawing.Image)
        Me.btnDeleteDS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteDS.Location = New System.Drawing.Point(348, 66)
        Me.btnDeleteDS.Name = "btnDeleteDS"
        Me.btnDeleteDS.Size = New System.Drawing.Size(73, 23)
        Me.btnDeleteDS.TabIndex = 5
        Me.btnDeleteDS.Text = "Delete"
        Me.btnDeleteDS.UseVisualStyleBackColor = True
        '
        'btnEditDS
        '
        Me.btnEditDS.Image = CType(resources.GetObject("btnEditDS.Image"), System.Drawing.Image)
        Me.btnEditDS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditDS.Location = New System.Drawing.Point(348, 41)
        Me.btnEditDS.Name = "btnEditDS"
        Me.btnEditDS.Size = New System.Drawing.Size(73, 23)
        Me.btnEditDS.TabIndex = 4
        Me.btnEditDS.Text = "Edit"
        Me.btnEditDS.UseVisualStyleBackColor = True
        '
        'btnAddDS
        '
        Me.btnAddDS.Image = CType(resources.GetObject("btnAddDS.Image"), System.Drawing.Image)
        Me.btnAddDS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAddDS.Location = New System.Drawing.Point(348, 16)
        Me.btnAddDS.Name = "btnAddDS"
        Me.btnAddDS.Size = New System.Drawing.Size(73, 23)
        Me.btnAddDS.TabIndex = 3
        Me.btnAddDS.Text = "Add"
        Me.btnAddDS.UseVisualStyleBackColor = True
        '
        'lsvDatasources
        '
        Me.lsvDatasources.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader5, Me.ColumnHeader6})
        Me.lsvDatasources.Dock = System.Windows.Forms.DockStyle.Left
        Me.lsvDatasources.Location = New System.Drawing.Point(3, 17)
        Me.lsvDatasources.Name = "lsvDatasources"
        Me.lsvDatasources.Size = New System.Drawing.Size(339, 413)
        Me.lsvDatasources.TabIndex = 2
        Me.lsvDatasources.UseCompatibleStateImageBehavior = False
        Me.lsvDatasources.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Datasource Name"
        Me.ColumnHeader5.Width = 174
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "Name"
        Me.ColumnHeader6.Width = 116
        '
        'tbReportOptions
        '
        Me.tbReportOptions.AttachedControl = Me.TabControlPanel3
        Me.tbReportOptions.Image = CType(resources.GetObject("tbReportOptions.Image"), System.Drawing.Image)
        Me.tbReportOptions.Name = "tbReportOptions"
        Me.tbReportOptions.Text = "Datasources"
        '
        'TabControlPanel6
        '
        Me.TabControlPanel6.Controls.Add(Me.chkTasks)
        Me.TabControlPanel6.Controls.Add(Me.UcTasks)
        Me.TabControlPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel6.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel6.Name = "TabControlPanel6"
        Me.TabControlPanel6.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel6.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel6.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel6.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel6.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel6.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel6.TabIndex = 6
        Me.TabControlPanel6.TabItem = Me.tbTasks
        '
        'chkTasks
        '
        Me.chkTasks.AutoSize = True
        Me.chkTasks.Location = New System.Drawing.Point(26, 219)
        Me.chkTasks.Name = "chkTasks"
        Me.chkTasks.Size = New System.Drawing.Size(15, 14)
        Me.chkTasks.TabIndex = 48
        Me.chkTasks.UseVisualStyleBackColor = True
        '
        'UcTasks
        '
        Me.UcTasks.BackColor = System.Drawing.Color.Transparent
        Me.UcTasks.Dock = System.Windows.Forms.DockStyle.Right
        Me.UcTasks.Enabled = False
        Me.UcTasks.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcTasks.ForeColor = System.Drawing.Color.Navy
        Me.UcTasks.Location = New System.Drawing.Point(63, 1)
        Me.UcTasks.m_defaultTaks = False
        Me.UcTasks.m_eventBased = False
        Me.UcTasks.m_eventID = 99999
        Me.UcTasks.Name = "UcTasks"
        Me.UcTasks.Size = New System.Drawing.Size(451, 444)
        Me.UcTasks.TabIndex = 49
        '
        'tbTasks
        '
        Me.tbTasks.AttachedControl = Me.TabControlPanel6
        Me.tbTasks.Image = CType(resources.GetObject("tbTasks.Image"), System.Drawing.Image)
        Me.tbTasks.Name = "tbTasks"
        Me.tbTasks.Text = "Tasks"
        '
        'TabControlPanel5
        '
        Me.TabControlPanel5.Controls.Add(Me.grpAutoCalc)
        Me.TabControlPanel5.Controls.Add(Me.chkAutoCalcm)
        Me.TabControlPanel5.Controls.Add(Me.chkAutoFail)
        Me.TabControlPanel5.Controls.Add(Me.grpRetryAfter)
        Me.TabControlPanel5.Controls.Add(Me.chkBlankReport)
        Me.TabControlPanel5.Controls.Add(Me.chkRetry)
        Me.TabControlPanel5.Controls.Add(Me.grpBlankReports)
        Me.TabControlPanel5.Controls.Add(Me.chkRetryAfterm)
        Me.TabControlPanel5.Controls.Add(Me.grpAutoFail)
        Me.TabControlPanel5.Controls.Add(Me.grpRetry)
        Me.TabControlPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel5.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel5.Name = "TabControlPanel5"
        Me.TabControlPanel5.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel5.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel5.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel5.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel5.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel5.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel5.TabIndex = 5
        Me.TabControlPanel5.TabItem = Me.tbException
        '
        'grpAutoCalc
        '
        Me.grpAutoCalc.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoCalc.Controls.Add(Me.chkAutoCalc)
        Me.grpAutoCalc.Enabled = False
        Me.grpAutoCalc.Location = New System.Drawing.Point(62, 150)
        Me.grpAutoCalc.Name = "grpAutoCalc"
        Me.grpAutoCalc.Size = New System.Drawing.Size(449, 44)
        Me.grpAutoCalc.TabIndex = 47
        Me.grpAutoCalc.TabStop = False
        '
        'chkAutoCalc
        '
        Me.chkAutoCalc.AutoSize = True
        Me.chkAutoCalc.Location = New System.Drawing.Point(8, 18)
        Me.chkAutoCalc.Name = "chkAutoCalc"
        Me.chkAutoCalc.Size = New System.Drawing.Size(222, 17)
        Me.chkAutoCalc.TabIndex = 0
        Me.chkAutoCalc.Text = "Auto-calculate schedule auto-fail interval"
        Me.chkAutoCalc.UseVisualStyleBackColor = True
        '
        'chkAutoCalcm
        '
        Me.chkAutoCalcm.AutoSize = True
        Me.chkAutoCalcm.Location = New System.Drawing.Point(26, 170)
        Me.chkAutoCalcm.Name = "chkAutoCalcm"
        Me.chkAutoCalcm.Size = New System.Drawing.Size(15, 14)
        Me.chkAutoCalcm.TabIndex = 46
        Me.chkAutoCalcm.UseVisualStyleBackColor = True
        '
        'chkAutoFail
        '
        Me.chkAutoFail.AutoSize = True
        Me.chkAutoFail.Location = New System.Drawing.Point(26, 72)
        Me.chkAutoFail.Name = "chkAutoFail"
        Me.chkAutoFail.Size = New System.Drawing.Size(15, 14)
        Me.chkAutoFail.TabIndex = 43
        Me.chkAutoFail.UseVisualStyleBackColor = True
        '
        'grpRetryAfter
        '
        Me.grpRetryAfter.BackColor = System.Drawing.Color.Transparent
        Me.grpRetryAfter.Controls.Add(Me.Label24)
        Me.grpRetryAfter.Controls.Add(Me.Label6)
        Me.grpRetryAfter.Controls.Add(Me.txtRetryAfter)
        Me.grpRetryAfter.Enabled = False
        Me.grpRetryAfter.Location = New System.Drawing.Point(63, 104)
        Me.grpRetryAfter.Name = "grpRetryAfter"
        Me.grpRetryAfter.Size = New System.Drawing.Size(448, 47)
        Me.grpRetryAfter.TabIndex = 53
        Me.grpRetryAfter.TabStop = False
        '
        'Label24
        '
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(3, 20)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(317, 16)
        Me.Label24.TabIndex = 12
        Me.Label24.Text = "If the schedule fails, retry running the schedule every"
        '
        'Label6
        '
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(387, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 14)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "minutes"
        '
        'txtRetryAfter
        '
        Me.txtRetryAfter.Location = New System.Drawing.Point(322, 18)
        Me.txtRetryAfter.Maximum = New Decimal(New Integer() {20164, 0, 0, 0})
        Me.txtRetryAfter.Name = "txtRetryAfter"
        Me.txtRetryAfter.Size = New System.Drawing.Size(62, 21)
        Me.txtRetryAfter.TabIndex = 42
        Me.txtRetryAfter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtRetryAfter.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'chkBlankReport
        '
        Me.chkBlankReport.AutoSize = True
        Me.chkBlankReport.Location = New System.Drawing.Point(26, 316)
        Me.chkBlankReport.Name = "chkBlankReport"
        Me.chkBlankReport.Size = New System.Drawing.Size(15, 14)
        Me.chkBlankReport.TabIndex = 46
        Me.chkBlankReport.UseVisualStyleBackColor = True
        '
        'chkRetry
        '
        Me.chkRetry.AutoSize = True
        Me.chkRetry.Location = New System.Drawing.Point(26, 25)
        Me.chkRetry.Name = "chkRetry"
        Me.chkRetry.Size = New System.Drawing.Size(15, 14)
        Me.chkRetry.TabIndex = 41
        Me.chkRetry.UseVisualStyleBackColor = True
        '
        'grpBlankReports
        '
        Me.grpBlankReports.BackColor = System.Drawing.Color.Transparent
        Me.grpBlankReports.Controls.Add(Me.UcBlank)
        Me.grpBlankReports.Enabled = False
        Me.grpBlankReports.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.grpBlankReports.Location = New System.Drawing.Point(63, 191)
        Me.grpBlankReports.Name = "grpBlankReports"
        Me.grpBlankReports.Size = New System.Drawing.Size(448, 254)
        Me.grpBlankReports.TabIndex = 14
        Me.grpBlankReports.TabStop = False
        '
        'UcBlank
        '
        Me.UcBlank.BackColor = System.Drawing.Color.Transparent
        Me.UcBlank.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcBlank.Location = New System.Drawing.Point(7, 13)
        Me.UcBlank.m_customDSN = ""
        Me.UcBlank.m_customPassword = ""
        Me.UcBlank.m_customUserID = ""
        Me.UcBlank.m_ParametersList = Nothing
        Me.UcBlank.Name = "UcBlank"
        Me.UcBlank.Size = New System.Drawing.Size(436, 225)
        Me.UcBlank.TabIndex = 47
        '
        'chkRetryAfterm
        '
        Me.chkRetryAfterm.AutoSize = True
        Me.chkRetryAfterm.Location = New System.Drawing.Point(26, 124)
        Me.chkRetryAfterm.Name = "chkRetryAfterm"
        Me.chkRetryAfterm.Size = New System.Drawing.Size(15, 14)
        Me.chkRetryAfterm.TabIndex = 46
        Me.chkRetryAfterm.UseVisualStyleBackColor = True
        '
        'grpAutoFail
        '
        Me.grpAutoFail.BackColor = System.Drawing.Color.Transparent
        Me.grpAutoFail.Controls.Add(Me.chkAssumeFail)
        Me.grpAutoFail.Controls.Add(Me.Label5)
        Me.grpAutoFail.Controls.Add(Me.cmbAssumeFail)
        Me.grpAutoFail.Enabled = False
        Me.grpAutoFail.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.grpAutoFail.Location = New System.Drawing.Point(63, 51)
        Me.grpAutoFail.Name = "grpAutoFail"
        Me.grpAutoFail.Size = New System.Drawing.Size(449, 50)
        Me.grpAutoFail.TabIndex = 13
        Me.grpAutoFail.TabStop = False
        '
        'chkAssumeFail
        '
        Me.chkAssumeFail.BackColor = System.Drawing.Color.Transparent
        Me.chkAssumeFail.Checked = True
        Me.chkAssumeFail.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkAssumeFail.Enabled = False
        Me.chkAssumeFail.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkAssumeFail.Location = New System.Drawing.Point(8, 16)
        Me.chkAssumeFail.Name = "chkAssumeFail"
        Me.chkAssumeFail.Size = New System.Drawing.Size(232, 24)
        Me.chkAssumeFail.TabIndex = 44
        Me.chkAssumeFail.Text = "Treat as failed if not completed in"
        Me.chkAssumeFail.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(387, 21)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(56, 14)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "minutes"
        '
        'cmbAssumeFail
        '
        Me.cmbAssumeFail.Location = New System.Drawing.Point(322, 18)
        Me.cmbAssumeFail.Maximum = New Decimal(New Integer() {99999, 0, 0, 0})
        Me.cmbAssumeFail.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbAssumeFail.Name = "cmbAssumeFail"
        Me.cmbAssumeFail.Size = New System.Drawing.Size(64, 21)
        Me.cmbAssumeFail.TabIndex = 45
        Me.cmbAssumeFail.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbAssumeFail.Value = New Decimal(New Integer() {30, 0, 0, 0})
        '
        'grpRetry
        '
        Me.grpRetry.BackColor = System.Drawing.Color.Transparent
        Me.grpRetry.Controls.Add(Me.Label4)
        Me.grpRetry.Controls.Add(Me.Label10)
        Me.grpRetry.Controls.Add(Me.cmbRetry)
        Me.grpRetry.Enabled = False
        Me.grpRetry.ForeColor = System.Drawing.SystemColors.InactiveCaptionText
        Me.grpRetry.Location = New System.Drawing.Point(63, 1)
        Me.grpRetry.Name = "grpRetry"
        Me.grpRetry.Size = New System.Drawing.Size(449, 49)
        Me.grpRetry.TabIndex = 12
        Me.grpRetry.TabStop = False
        '
        'Label4
        '
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(3, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(227, 16)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "On error, retry running the schedule up to "
        '
        'Label10
        '
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(387, 21)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(56, 14)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "times"
        '
        'cmbRetry
        '
        Me.cmbRetry.Location = New System.Drawing.Point(322, 18)
        Me.cmbRetry.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.cmbRetry.Name = "cmbRetry"
        Me.cmbRetry.Size = New System.Drawing.Size(64, 21)
        Me.cmbRetry.TabIndex = 42
        Me.cmbRetry.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbRetry.Value = New Decimal(New Integer() {3, 0, 0, 0})
        '
        'tbException
        '
        Me.tbException.AttachedControl = Me.TabControlPanel5
        Me.tbException.Image = CType(resources.GetObject("tbException.Image"), System.Drawing.Image)
        Me.tbException.Name = "tbException"
        Me.tbException.Text = "Exception Handling"
        '
        'TabControlPanel4
        '
        Me.TabControlPanel4.Controls.Add(Me.chkDest)
        Me.TabControlPanel4.Controls.Add(Me.UcDest)
        Me.TabControlPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel4.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel4.Name = "TabControlPanel4"
        Me.TabControlPanel4.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel4.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel4.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel4.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel4.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel4.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel4.TabIndex = 4
        Me.TabControlPanel4.TabItem = Me.tbOutput
        '
        'chkDest
        '
        Me.chkDest.AutoSize = True
        Me.chkDest.Location = New System.Drawing.Point(26, 203)
        Me.chkDest.Name = "chkDest"
        Me.chkDest.Size = New System.Drawing.Size(15, 14)
        Me.chkDest.TabIndex = 39
        Me.chkDest.UseVisualStyleBackColor = True
        '
        'UcDest
        '
        Me.UcDest.BackColor = System.Drawing.Color.Transparent
        Me.UcDest.Dock = System.Windows.Forms.DockStyle.Right
        Me.UcDest.Enabled = False
        Me.UcDest.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.UcDest.Location = New System.Drawing.Point(76, 1)
        Me.UcDest.m_CanDisable = True
        Me.UcDest.m_DelayDelete = False
        Me.UcDest.m_eventBased = False
        Me.UcDest.m_ExcelBurst = False
        Me.UcDest.m_IsDynamic = False
        Me.UcDest.m_isPackage = False
        Me.UcDest.m_StaticDest = False
        Me.UcDest.Name = "UcDest"
        Me.UcDest.Size = New System.Drawing.Size(438, 444)
        Me.UcDest.TabIndex = 40
        '
        'tbOutput
        '
        Me.tbOutput.AttachedControl = Me.TabControlPanel4
        Me.tbOutput.Image = CType(resources.GetObject("tbOutput.Image"), System.Drawing.Image)
        Me.tbOutput.Name = "tbOutput"
        Me.tbOutput.Text = "Output"
        '
        'TabControlPanel1
        '
        Me.TabControlPanel1.Controls.Add(Me.chkKeyword)
        Me.TabControlPanel1.Controls.Add(Me.chkDesc)
        Me.TabControlPanel1.Controls.Add(Me.chkReportLocation)
        Me.TabControlPanel1.Controls.Add(Me.grpDesc)
        Me.TabControlPanel1.Controls.Add(Me.grpReportLocation)
        Me.TabControlPanel1.Controls.Add(Me.grpKeyword)
        Me.TabControlPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel1.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel1.Name = "TabControlPanel1"
        Me.TabControlPanel1.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel1.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel1.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel1.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel1.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel1.TabIndex = 1
        Me.TabControlPanel1.TabItem = Me.tbGeneral
        '
        'chkKeyword
        '
        Me.chkKeyword.AutoSize = True
        Me.chkKeyword.Location = New System.Drawing.Point(26, 358)
        Me.chkKeyword.Name = "chkKeyword"
        Me.chkKeyword.Size = New System.Drawing.Size(15, 14)
        Me.chkKeyword.TabIndex = 5
        Me.chkKeyword.UseVisualStyleBackColor = True
        '
        'chkDesc
        '
        Me.chkDesc.AutoSize = True
        Me.chkDesc.Location = New System.Drawing.Point(26, 199)
        Me.chkDesc.Name = "chkDesc"
        Me.chkDesc.Size = New System.Drawing.Size(15, 14)
        Me.chkDesc.TabIndex = 3
        Me.chkDesc.UseVisualStyleBackColor = True
        '
        'chkReportLocation
        '
        Me.chkReportLocation.AutoSize = True
        Me.chkReportLocation.Location = New System.Drawing.Point(26, 36)
        Me.chkReportLocation.Name = "chkReportLocation"
        Me.chkReportLocation.Size = New System.Drawing.Size(15, 14)
        Me.chkReportLocation.TabIndex = 0
        Me.chkReportLocation.UseVisualStyleBackColor = True
        '
        'grpDesc
        '
        Me.grpDesc.BackColor = System.Drawing.Color.Transparent
        Me.grpDesc.Controls.Add(Me.Label7)
        Me.grpDesc.Controls.Add(Me.txtDesc)
        Me.grpDesc.Enabled = False
        Me.grpDesc.Location = New System.Drawing.Point(63, 71)
        Me.grpDesc.Name = "grpDesc"
        Me.grpDesc.Size = New System.Drawing.Size(427, 245)
        Me.grpDesc.TabIndex = 1
        Me.grpDesc.TabStop = False
        '
        'Label7
        '
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(4, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(208, 16)
        Me.Label7.TabIndex = 37
        Me.Label7.Text = "Description (optional)"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(7, 36)
        Me.txtDesc.Multiline = True
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtDesc.Size = New System.Drawing.Size(376, 203)
        Me.txtDesc.TabIndex = 4
        Me.txtDesc.Tag = "memo"
        '
        'grpReportLocation
        '
        Me.grpReportLocation.BackColor = System.Drawing.Color.Transparent
        Me.grpReportLocation.Controls.Add(Me.txtDBLoc)
        Me.grpReportLocation.Controls.Add(Me.Label3)
        Me.grpReportLocation.Enabled = False
        Me.grpReportLocation.Location = New System.Drawing.Point(63, 3)
        Me.grpReportLocation.Name = "grpReportLocation"
        Me.grpReportLocation.Size = New System.Drawing.Size(427, 62)
        Me.grpReportLocation.TabIndex = 0
        Me.grpReportLocation.TabStop = False
        '
        'txtDBLoc
        '
        Me.txtDBLoc.BackColor = System.Drawing.Color.White
        Me.txtDBLoc.ForeColor = System.Drawing.Color.Blue
        Me.txtDBLoc.Location = New System.Drawing.Point(10, 33)
        Me.txtDBLoc.Name = "txtDBLoc"
        Me.txtDBLoc.Size = New System.Drawing.Size(376, 21)
        Me.txtDBLoc.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 17)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(208, 16)
        Me.Label3.TabIndex = 35
        Me.Label3.Text = "Report Server"
        '
        'grpKeyword
        '
        Me.grpKeyword.BackColor = System.Drawing.Color.Transparent
        Me.grpKeyword.Controls.Add(Me.TextBox2)
        Me.grpKeyword.Controls.Add(Me.Button2)
        Me.grpKeyword.Controls.Add(Me.Label2)
        Me.grpKeyword.Controls.Add(Me.txtKeyWord)
        Me.grpKeyword.Controls.Add(Me.Label9)
        Me.grpKeyword.Enabled = False
        Me.grpKeyword.Location = New System.Drawing.Point(63, 322)
        Me.grpKeyword.Name = "grpKeyword"
        Me.grpKeyword.Size = New System.Drawing.Size(427, 65)
        Me.grpKeyword.TabIndex = 2
        Me.grpKeyword.TabStop = False
        '
        'TextBox2
        '
        Me.TextBox2.BackColor = System.Drawing.Color.White
        Me.TextBox2.ForeColor = System.Drawing.Color.Blue
        Me.TextBox2.Location = New System.Drawing.Point(0, -297)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(288, 21)
        Me.TextBox2.TabIndex = 31
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.Control
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Button2.Location = New System.Drawing.Point(320, -297)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(56, 21)
        Me.Button2.TabIndex = 32
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(0, -313)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(208, 16)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Report Location"
        '
        'txtKeyWord
        '
        Me.txtKeyWord.BackColor = System.Drawing.Color.White
        Me.txtKeyWord.ForeColor = System.Drawing.Color.Blue
        Me.txtKeyWord.Location = New System.Drawing.Point(10, 36)
        Me.txtKeyWord.Name = "txtKeyWord"
        Me.txtKeyWord.Size = New System.Drawing.Size(376, 21)
        Me.txtKeyWord.TabIndex = 6
        Me.txtKeyWord.Tag = "memo"
        '
        'Label9
        '
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(7, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(208, 16)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Keyword (optional)"
        '
        'tbGeneral
        '
        Me.tbGeneral.AttachedControl = Me.TabControlPanel1
        Me.tbGeneral.Image = CType(resources.GetObject("tbGeneral.Image"), System.Drawing.Image)
        Me.tbGeneral.Name = "tbGeneral"
        Me.tbGeneral.Text = "General"
        '
        'TabControlPanel2
        '
        Me.TabControlPanel2.Controls.Add(Me.chkNextRun)
        Me.TabControlPanel2.Controls.Add(Me.grpNextRunDate)
        Me.TabControlPanel2.Controls.Add(Me.chkScheduleTime)
        Me.TabControlPanel2.Controls.Add(Me.chkEndDate)
        Me.TabControlPanel2.Controls.Add(Me.chkRepeatEvery)
        Me.TabControlPanel2.Controls.Add(Me.chkExceptionCal)
        Me.TabControlPanel2.Controls.Add(Me.chkFrequency)
        Me.TabControlPanel2.Controls.Add(Me.grpRepeat)
        Me.TabControlPanel2.Controls.Add(Me.grpEndDate)
        Me.TabControlPanel2.Controls.Add(Me.grpScheduleTime)
        Me.TabControlPanel2.Controls.Add(Me.grpExceptionCal)
        Me.TabControlPanel2.Controls.Add(Me.grpFrequency)
        Me.TabControlPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControlPanel2.Location = New System.Drawing.Point(126, 0)
        Me.TabControlPanel2.Name = "TabControlPanel2"
        Me.TabControlPanel2.Padding = New System.Windows.Forms.Padding(1)
        Me.TabControlPanel2.Size = New System.Drawing.Size(515, 446)
        Me.TabControlPanel2.Style.BackColor1.Color = System.Drawing.Color.FromArgb(CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(252, Byte), Integer))
        Me.TabControlPanel2.Style.BackColor2.Color = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer), CType(CType(253, Byte), Integer))
        Me.TabControlPanel2.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine
        Me.TabControlPanel2.Style.BorderSide = CType(((DevComponents.DotNetBar.eBorderSide.Right Or DevComponents.DotNetBar.eBorderSide.Top) _
                    Or DevComponents.DotNetBar.eBorderSide.Bottom), DevComponents.DotNetBar.eBorderSide)
        Me.TabControlPanel2.TabIndex = 2
        Me.TabControlPanel2.TabItem = Me.tbSchedule
        '
        'chkNextRun
        '
        Me.chkNextRun.AutoSize = True
        Me.chkNextRun.Location = New System.Drawing.Point(26, 157)
        Me.chkNextRun.Name = "chkNextRun"
        Me.chkNextRun.Size = New System.Drawing.Size(15, 14)
        Me.chkNextRun.TabIndex = 19
        Me.chkNextRun.UseVisualStyleBackColor = True
        '
        'grpNextRunDate
        '
        Me.grpNextRunDate.BackColor = System.Drawing.Color.Transparent
        Me.grpNextRunDate.Controls.Add(Me.Label1)
        Me.grpNextRunDate.Controls.Add(Me.dtNextRun)
        Me.grpNextRunDate.Enabled = False
        Me.grpNextRunDate.Location = New System.Drawing.Point(63, 144)
        Me.grpNextRunDate.Name = "grpNextRunDate"
        Me.grpNextRunDate.Size = New System.Drawing.Size(432, 41)
        Me.grpNextRunDate.TabIndex = 4
        Me.grpNextRunDate.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(6, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Next Run Date"
        '
        'dtNextRun
        '
        Me.dtNextRun.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtNextRun.Location = New System.Drawing.Point(120, 13)
        Me.dtNextRun.Name = "dtNextRun"
        Me.dtNextRun.Size = New System.Drawing.Size(88, 21)
        Me.dtNextRun.TabIndex = 20
        '
        'chkScheduleTime
        '
        Me.chkScheduleTime.AutoSize = True
        Me.chkScheduleTime.Location = New System.Drawing.Point(26, 255)
        Me.chkScheduleTime.Name = "chkScheduleTime"
        Me.chkScheduleTime.Size = New System.Drawing.Size(15, 14)
        Me.chkScheduleTime.TabIndex = 24
        Me.chkScheduleTime.UseVisualStyleBackColor = True
        '
        'chkEndDate
        '
        Me.chkEndDate.AutoSize = True
        Me.chkEndDate.Location = New System.Drawing.Point(26, 298)
        Me.chkEndDate.Name = "chkEndDate"
        Me.chkEndDate.Size = New System.Drawing.Size(15, 14)
        Me.chkEndDate.TabIndex = 26
        Me.chkEndDate.UseVisualStyleBackColor = True
        '
        'chkRepeatEvery
        '
        Me.chkRepeatEvery.AutoSize = True
        Me.chkRepeatEvery.Location = New System.Drawing.Point(26, 345)
        Me.chkRepeatEvery.Name = "chkRepeatEvery"
        Me.chkRepeatEvery.Size = New System.Drawing.Size(15, 14)
        Me.chkRepeatEvery.TabIndex = 29
        Me.chkRepeatEvery.UseVisualStyleBackColor = True
        '
        'chkExceptionCal
        '
        Me.chkExceptionCal.AutoSize = True
        Me.chkExceptionCal.Location = New System.Drawing.Point(26, 205)
        Me.chkExceptionCal.Name = "chkExceptionCal"
        Me.chkExceptionCal.Size = New System.Drawing.Size(15, 14)
        Me.chkExceptionCal.TabIndex = 21
        Me.chkExceptionCal.UseVisualStyleBackColor = True
        '
        'chkFrequency
        '
        Me.chkFrequency.AutoSize = True
        Me.chkFrequency.Location = New System.Drawing.Point(26, 71)
        Me.chkFrequency.Name = "chkFrequency"
        Me.chkFrequency.Size = New System.Drawing.Size(15, 14)
        Me.chkFrequency.TabIndex = 7
        Me.chkFrequency.UseVisualStyleBackColor = True
        '
        'grpRepeat
        '
        Me.grpRepeat.BackColor = System.Drawing.Color.Transparent
        Me.grpRepeat.Controls.Add(Me.pnlRepeat)
        Me.grpRepeat.Enabled = False
        Me.grpRepeat.Location = New System.Drawing.Point(63, 323)
        Me.grpRepeat.Name = "grpRepeat"
        Me.grpRepeat.Size = New System.Drawing.Size(432, 50)
        Me.grpRepeat.TabIndex = 8
        Me.grpRepeat.TabStop = False
        '
        'pnlRepeat
        '
        Me.pnlRepeat.Controls.Add(Me.cmbUnit)
        Me.pnlRepeat.Controls.Add(Me.RepeatUntil)
        Me.pnlRepeat.Controls.Add(Me.cmbRpt)
        Me.pnlRepeat.Controls.Add(Me.Label13)
        Me.pnlRepeat.Location = New System.Drawing.Point(116, 13)
        Me.pnlRepeat.Name = "pnlRepeat"
        Me.pnlRepeat.Size = New System.Drawing.Size(278, 30)
        Me.pnlRepeat.TabIndex = 18
        '
        'cmbUnit
        '
        Me.cmbUnit.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbUnit.FormattingEnabled = True
        Me.cmbUnit.Items.AddRange(New Object() {"hours", "minutes"})
        Me.cmbUnit.Location = New System.Drawing.Point(70, 4)
        Me.cmbUnit.Name = "cmbUnit"
        Me.cmbUnit.Size = New System.Drawing.Size(74, 21)
        Me.cmbUnit.TabIndex = 31
        '
        'RepeatUntil
        '
        Me.RepeatUntil.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RepeatUntil.Location = New System.Drawing.Point(182, 5)
        Me.RepeatUntil.Name = "RepeatUntil"
        Me.RepeatUntil.ShowUpDown = True
        Me.RepeatUntil.Size = New System.Drawing.Size(88, 21)
        Me.RepeatUntil.TabIndex = 32
        '
        'cmbRpt
        '
        Me.cmbRpt.DecimalPlaces = 2
        Me.cmbRpt.Increment = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Location = New System.Drawing.Point(6, 5)
        Me.cmbRpt.Minimum = New Decimal(New Integer() {25, 0, 0, 131072})
        Me.cmbRpt.Name = "cmbRpt"
        Me.cmbRpt.Size = New System.Drawing.Size(56, 21)
        Me.cmbRpt.TabIndex = 30
        Me.cmbRpt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.cmbRpt.Value = New Decimal(New Integer() {25, 0, 0, 131072})
        '
        'Label13
        '
        Me.Label13.Enabled = False
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(150, 4)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(33, 21)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "until"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'grpEndDate
        '
        Me.grpEndDate.BackColor = System.Drawing.Color.Transparent
        Me.grpEndDate.Controls.Add(Me.Label11)
        Me.grpEndDate.Controls.Add(Me.EndDate)
        Me.grpEndDate.Controls.Add(Me.chkNoEnd)
        Me.grpEndDate.Enabled = False
        Me.grpEndDate.Location = New System.Drawing.Point(63, 281)
        Me.grpEndDate.Name = "grpEndDate"
        Me.grpEndDate.Size = New System.Drawing.Size(432, 41)
        Me.grpEndDate.TabIndex = 7
        Me.grpEndDate.TabStop = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(6, 17)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(51, 13)
        Me.Label11.TabIndex = 6
        Me.Label11.Text = "End Date"
        '
        'EndDate
        '
        Me.EndDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EndDate.Location = New System.Drawing.Point(120, 14)
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Size = New System.Drawing.Size(88, 21)
        Me.EndDate.TabIndex = 27
        '
        'chkNoEnd
        '
        Me.chkNoEnd.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.chkNoEnd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkNoEnd.Location = New System.Drawing.Point(243, 12)
        Me.chkNoEnd.Name = "chkNoEnd"
        Me.chkNoEnd.Size = New System.Drawing.Size(98, 24)
        Me.chkNoEnd.TabIndex = 28
        Me.chkNoEnd.Text = "No End Date"
        '
        'grpScheduleTime
        '
        Me.grpScheduleTime.BackColor = System.Drawing.Color.Transparent
        Me.grpScheduleTime.Controls.Add(Me.Label12)
        Me.grpScheduleTime.Controls.Add(Me.RunAt)
        Me.grpScheduleTime.Enabled = False
        Me.grpScheduleTime.Location = New System.Drawing.Point(63, 239)
        Me.grpScheduleTime.Name = "grpScheduleTime"
        Me.grpScheduleTime.Size = New System.Drawing.Size(432, 41)
        Me.grpScheduleTime.TabIndex = 6
        Me.grpScheduleTime.TabStop = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(6, 17)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(75, 13)
        Me.Label12.TabIndex = 6
        Me.Label12.Text = "Schedule Time"
        '
        'RunAt
        '
        Me.RunAt.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.RunAt.Location = New System.Drawing.Point(120, 14)
        Me.RunAt.Name = "RunAt"
        Me.RunAt.ShowUpDown = True
        Me.RunAt.Size = New System.Drawing.Size(88, 21)
        Me.RunAt.TabIndex = 25
        '
        'grpExceptionCal
        '
        Me.grpExceptionCal.BackColor = System.Drawing.Color.Transparent
        Me.grpExceptionCal.Controls.Add(Me.GroupBox7)
        Me.grpExceptionCal.Controls.Add(Me.cmbException)
        Me.grpExceptionCal.Controls.Add(Me.chkException)
        Me.grpExceptionCal.Enabled = False
        Me.grpExceptionCal.Location = New System.Drawing.Point(63, 186)
        Me.grpExceptionCal.Name = "grpExceptionCal"
        Me.grpExceptionCal.Size = New System.Drawing.Size(432, 52)
        Me.grpExceptionCal.TabIndex = 5
        Me.grpExceptionCal.TabStop = False
        '
        'GroupBox7
        '
        Me.GroupBox7.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox7.Location = New System.Drawing.Point(0, 58)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(200, 94)
        Me.GroupBox7.TabIndex = 20
        Me.GroupBox7.TabStop = False
        '
        'cmbException
        '
        Me.cmbException.Enabled = False
        Me.cmbException.FormattingEnabled = True
        Me.cmbException.Location = New System.Drawing.Point(142, 18)
        Me.cmbException.Name = "cmbException"
        Me.cmbException.Size = New System.Drawing.Size(170, 21)
        Me.cmbException.TabIndex = 23
        '
        'chkException
        '
        Me.chkException.AutoSize = True
        Me.chkException.Location = New System.Drawing.Point(6, 20)
        Me.chkException.Name = "chkException"
        Me.chkException.Size = New System.Drawing.Size(138, 17)
        Me.chkException.TabIndex = 22
        Me.chkException.Text = "Use exception calendar"
        Me.chkException.UseVisualStyleBackColor = True
        '
        'grpFrequency
        '
        Me.grpFrequency.BackColor = System.Drawing.Color.Transparent
        Me.grpFrequency.Controls.Add(Me.cmdMonthly)
        Me.grpFrequency.Controls.Add(Me.cmbCustom)
        Me.grpFrequency.Controls.Add(Me.optDaily)
        Me.grpFrequency.Controls.Add(Me.optWeekDaily)
        Me.grpFrequency.Controls.Add(Me.optWeekly)
        Me.grpFrequency.Controls.Add(Me.optMonthly)
        Me.grpFrequency.Controls.Add(Me.optYearly)
        Me.grpFrequency.Controls.Add(Me.optNone)
        Me.grpFrequency.Controls.Add(Me.optCustom)
        Me.grpFrequency.Controls.Add(Me.cmdDaily)
        Me.grpFrequency.Controls.Add(Me.cmdWeekly)
        Me.grpFrequency.Enabled = False
        Me.grpFrequency.Location = New System.Drawing.Point(63, 3)
        Me.grpFrequency.Name = "grpFrequency"
        Me.grpFrequency.Size = New System.Drawing.Size(432, 141)
        Me.grpFrequency.TabIndex = 3
        Me.grpFrequency.TabStop = False
        '
        'cmdMonthly
        '
        Me.cmdMonthly.Enabled = False
        Me.cmdMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdMonthly.Location = New System.Drawing.Point(312, 20)
        Me.cmdMonthly.Name = "cmdMonthly"
        Me.cmdMonthly.Size = New System.Drawing.Size(40, 16)
        Me.cmdMonthly.TabIndex = 16
        Me.cmdMonthly.Text = "..."
        '
        'cmbCustom
        '
        Me.cmbCustom.Enabled = False
        Me.cmbCustom.ItemHeight = 13
        Me.cmbCustom.Location = New System.Drawing.Point(142, 112)
        Me.cmbCustom.Name = "cmbCustom"
        Me.cmbCustom.Size = New System.Drawing.Size(170, 21)
        Me.cmbCustom.TabIndex = 14
        '
        'optDaily
        '
        Me.optDaily.Checked = True
        Me.optDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optDaily.Location = New System.Drawing.Point(8, 16)
        Me.optDaily.Name = "optDaily"
        Me.optDaily.Size = New System.Drawing.Size(91, 24)
        Me.optDaily.TabIndex = 8
        Me.optDaily.TabStop = True
        Me.optDaily.Text = "Every Day"
        '
        'optWeekDaily
        '
        Me.optWeekDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekDaily.Location = New System.Drawing.Point(6, 46)
        Me.optWeekDaily.Name = "optWeekDaily"
        Me.optWeekDaily.Size = New System.Drawing.Size(112, 24)
        Me.optWeekDaily.TabIndex = 10
        Me.optWeekDaily.Text = "Every Week Day"
        '
        'optWeekly
        '
        Me.optWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optWeekly.Location = New System.Drawing.Point(8, 80)
        Me.optWeekly.Name = "optWeekly"
        Me.optWeekly.Size = New System.Drawing.Size(104, 24)
        Me.optWeekly.TabIndex = 11
        Me.optWeekly.Text = "Every Week"
        '
        'optMonthly
        '
        Me.optMonthly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optMonthly.Location = New System.Drawing.Point(208, 16)
        Me.optMonthly.Name = "optMonthly"
        Me.optMonthly.Size = New System.Drawing.Size(104, 24)
        Me.optMonthly.TabIndex = 15
        Me.optMonthly.Text = "Every Month"
        '
        'optYearly
        '
        Me.optYearly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optYearly.Location = New System.Drawing.Point(208, 48)
        Me.optYearly.Name = "optYearly"
        Me.optYearly.Size = New System.Drawing.Size(104, 24)
        Me.optYearly.TabIndex = 17
        Me.optYearly.Text = "Every Year"
        '
        'optNone
        '
        Me.optNone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optNone.Location = New System.Drawing.Point(208, 80)
        Me.optNone.Name = "optNone"
        Me.optNone.Size = New System.Drawing.Size(104, 24)
        Me.optNone.TabIndex = 18
        Me.optNone.Text = "None"
        '
        'optCustom
        '
        Me.optCustom.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.optCustom.Location = New System.Drawing.Point(8, 112)
        Me.optCustom.Name = "optCustom"
        Me.optCustom.Size = New System.Drawing.Size(112, 24)
        Me.optCustom.TabIndex = 13
        Me.optCustom.Text = "Custom Calendar"
        '
        'cmdDaily
        '
        Me.cmdDaily.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdDaily.Location = New System.Drawing.Point(120, 20)
        Me.cmdDaily.Name = "cmdDaily"
        Me.cmdDaily.Size = New System.Drawing.Size(40, 16)
        Me.cmdDaily.TabIndex = 9
        Me.cmdDaily.Text = "..."
        '
        'cmdWeekly
        '
        Me.cmdWeekly.Enabled = False
        Me.cmdWeekly.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdWeekly.Location = New System.Drawing.Point(120, 84)
        Me.cmdWeekly.Name = "cmdWeekly"
        Me.cmdWeekly.Size = New System.Drawing.Size(40, 16)
        Me.cmdWeekly.TabIndex = 12
        Me.cmdWeekly.Text = "..."
        '
        'tbSchedule
        '
        Me.tbSchedule.AttachedControl = Me.TabControlPanel2
        Me.tbSchedule.Image = CType(resources.GetObject("tbSchedule.Image"), System.Drawing.Image)
        Me.tbSchedule.Name = "tbSchedule"
        Me.tbSchedule.Text = "Schedule"
        '
        'cmdApply
        '
        Me.cmdApply.Enabled = False
        Me.cmdApply.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdApply.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdApply.Location = New System.Drawing.Point(554, 452)
        Me.cmdApply.Name = "cmdApply"
        Me.cmdApply.Size = New System.Drawing.Size(75, 23)
        Me.cmdApply.TabIndex = 50
        Me.cmdApply.Text = "&Apply"
        '
        'cmdCancel
        '
        Me.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cmdCancel.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdCancel.Location = New System.Drawing.Point(466, 452)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(75, 23)
        Me.cmdCancel.TabIndex = 52
        Me.cmdCancel.Text = "&Cancel"
        '
        'cmdOK
        '
        Me.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmdOK.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cmdOK.Location = New System.Drawing.Point(378, 452)
        Me.cmdOK.Name = "cmdOK"
        Me.cmdOK.Size = New System.Drawing.Size(75, 23)
        Me.cmdOK.TabIndex = 51
        Me.cmdOK.Text = "&OK"
        '
        'frmCollectiveProp
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 482)
        Me.Controls.Add(Me.cmdApply)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdOK)
        Me.Controls.Add(Me.tabCollective)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCollectiveProp"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Edit Properties"
        CType(Me.tabCollective, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabCollective.ResumeLayout(False)
        Me.TabControlPanel3.ResumeLayout(False)
        Me.TabControlPanel3.PerformLayout()
        Me.grpLoginReq.ResumeLayout(False)
        Me.TabControlPanel6.ResumeLayout(False)
        Me.TabControlPanel6.PerformLayout()
        Me.TabControlPanel5.ResumeLayout(False)
        Me.TabControlPanel5.PerformLayout()
        Me.grpAutoCalc.ResumeLayout(False)
        Me.grpAutoCalc.PerformLayout()
        Me.grpRetryAfter.ResumeLayout(False)
        CType(Me.txtRetryAfter, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpBlankReports.ResumeLayout(False)
        Me.grpAutoFail.ResumeLayout(False)
        CType(Me.cmbAssumeFail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpRetry.ResumeLayout(False)
        CType(Me.cmbRetry, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlPanel4.ResumeLayout(False)
        Me.TabControlPanel4.PerformLayout()
        Me.TabControlPanel1.ResumeLayout(False)
        Me.TabControlPanel1.PerformLayout()
        Me.grpDesc.ResumeLayout(False)
        Me.grpDesc.PerformLayout()
        Me.grpReportLocation.ResumeLayout(False)
        Me.grpReportLocation.PerformLayout()
        Me.grpKeyword.ResumeLayout(False)
        Me.grpKeyword.PerformLayout()
        Me.TabControlPanel2.ResumeLayout(False)
        Me.TabControlPanel2.PerformLayout()
        Me.grpNextRunDate.ResumeLayout(False)
        Me.grpNextRunDate.PerformLayout()
        Me.grpRepeat.ResumeLayout(False)
        Me.pnlRepeat.ResumeLayout(False)
        CType(Me.cmbRpt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpEndDate.ResumeLayout(False)
        Me.grpEndDate.PerformLayout()
        Me.grpScheduleTime.ResumeLayout(False)
        Me.grpScheduleTime.PerformLayout()
        Me.grpExceptionCal.ResumeLayout(False)
        Me.grpExceptionCal.PerformLayout()
        Me.grpFrequency.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabCollective As DevComponents.DotNetBar.TabControl
    Friend WithEvents TabControlPanel1 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbGeneral As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel2 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbSchedule As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel3 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbReportOptions As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel4 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbOutput As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel5 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbException As DevComponents.DotNetBar.TabItem
    Friend WithEvents TabControlPanel6 As DevComponents.DotNetBar.TabControlPanel
    Friend WithEvents tbTasks As DevComponents.DotNetBar.TabItem
    Friend WithEvents UcDest As sqlrd.ucDestination
    Friend WithEvents UcTasks As sqlrd.ucTasks
    Friend WithEvents txtDesc As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtKeyWord As System.Windows.Forms.TextBox
    Friend WithEvents txtDBLoc As System.Windows.Forms.TextBox
    Friend WithEvents grpKeyword As System.Windows.Forms.GroupBox
    Friend WithEvents grpReportLocation As System.Windows.Forms.GroupBox
    Friend WithEvents grpDesc As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkReportLocation As System.Windows.Forms.CheckBox
    Friend WithEvents chkKeyword As System.Windows.Forms.CheckBox
    Friend WithEvents chkDesc As System.Windows.Forms.CheckBox
    Friend WithEvents grpFrequency As System.Windows.Forms.GroupBox
    Friend WithEvents cmdMonthly As System.Windows.Forms.Button
    Friend WithEvents cmbCustom As System.Windows.Forms.ComboBox
    Friend WithEvents optDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekDaily As System.Windows.Forms.RadioButton
    Friend WithEvents optWeekly As System.Windows.Forms.RadioButton
    Friend WithEvents optMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents optYearly As System.Windows.Forms.RadioButton
    Friend WithEvents optNone As System.Windows.Forms.RadioButton
    Friend WithEvents optCustom As System.Windows.Forms.RadioButton
    Friend WithEvents cmdDaily As System.Windows.Forms.Button
    Friend WithEvents cmdWeekly As System.Windows.Forms.Button
    Friend WithEvents grpExceptionCal As System.Windows.Forms.GroupBox
    Friend WithEvents cmbException As System.Windows.Forms.ComboBox
    Friend WithEvents chkException As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkNoEnd As System.Windows.Forms.CheckBox
    Friend WithEvents EndDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents RunAt As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents pnlRepeat As System.Windows.Forms.Panel
    Friend WithEvents cmbUnit As System.Windows.Forms.ComboBox
    Friend WithEvents RepeatUntil As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbRpt As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents grpScheduleTime As System.Windows.Forms.GroupBox
    Friend WithEvents grpRepeat As System.Windows.Forms.GroupBox
    Friend WithEvents grpEndDate As System.Windows.Forms.GroupBox
    Friend WithEvents chkExceptionCal As System.Windows.Forms.CheckBox
    Friend WithEvents chkFrequency As System.Windows.Forms.CheckBox
    Friend WithEvents chkScheduleTime As System.Windows.Forms.CheckBox
    Friend WithEvents chkEndDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkRepeatEvery As System.Windows.Forms.CheckBox
    Friend WithEvents grpLoginReq As System.Windows.Forms.GroupBox
    Friend WithEvents chkLoginReq As System.Windows.Forms.CheckBox
    Friend WithEvents chkDest As System.Windows.Forms.CheckBox
    Friend WithEvents grpRetry As System.Windows.Forms.GroupBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents UcBlank As sqlrd.ucBlankAlert
    Friend WithEvents cmbAssumeFail As System.Windows.Forms.NumericUpDown
    Friend WithEvents cmbRetry As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents chkAssumeFail As System.Windows.Forms.CheckBox
    Friend WithEvents grpBlankReports As System.Windows.Forms.GroupBox
    Friend WithEvents grpAutoFail As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoFail As System.Windows.Forms.CheckBox
    Friend WithEvents chkBlankReport As System.Windows.Forms.CheckBox
    Friend WithEvents chkRetry As System.Windows.Forms.CheckBox
    Friend WithEvents chkTasks As System.Windows.Forms.CheckBox
    Friend WithEvents cmdApply As System.Windows.Forms.Button
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdOK As System.Windows.Forms.Button
    Friend WithEvents grpNextRunDate As System.Windows.Forms.GroupBox
    Friend WithEvents chkNextRun As System.Windows.Forms.CheckBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtNextRun As System.Windows.Forms.DateTimePicker
    Friend WithEvents grpAutoCalc As System.Windows.Forms.GroupBox
    Friend WithEvents chkAutoCalc As System.Windows.Forms.CheckBox
    Friend WithEvents chkAutoCalcm As System.Windows.Forms.CheckBox
    Friend WithEvents grpRetryAfter As System.Windows.Forms.GroupBox
    Friend WithEvents chkRetryAfterm As System.Windows.Forms.CheckBox
    Friend WithEvents txtRetryAfter As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lsvDatasources As System.Windows.Forms.ListView
    Friend WithEvents ColumnHeader5 As System.Windows.Forms.ColumnHeader
    Friend WithEvents ColumnHeader6 As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnDeleteDS As System.Windows.Forms.Button
    Friend WithEvents btnEditDS As System.Windows.Forms.Button
    Friend WithEvents btnAddDS As System.Windows.Forms.Button
End Class
