Public Class frmWebBrowser
    Public Sub viewPage(ByVal sUrl As String, Optional ByVal dialog As Boolean = True, Optional ByVal title As String = "Movie Player")
        Me.Text = title

        Me.wb.Navigate(sUrl)

        If dialog = True Then
            Me.ShowDialog()
        Else
            Me.Show()
        End If
    End Sub

    Private Sub frmWebBrowser_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        'console.writeLine(Me.Width & "," & Me.Height)
    End Sub
End Class