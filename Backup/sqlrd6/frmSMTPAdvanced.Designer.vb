<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSMTPAdvanced
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSMTPAdvanced))
        Me.chkRequiresPop = New System.Windows.Forms.CheckBox
        Me.grpPop = New System.Windows.Forms.GroupBox
        Me.btnTest = New System.Windows.Forms.Button
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.txtPOPServer = New System.Windows.Forms.TextBox
        Me.txtPopUser = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.txtPopPassword = New System.Windows.Forms.TextBox
        Me.txtPopPort = New System.Windows.Forms.MaskedTextBox
        Me.chkPOPSSL = New System.Windows.Forms.CheckBox
        Me.btnCancel = New System.Windows.Forms.Button
        Me.btnOK = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.Label5 = New System.Windows.Forms.Label
        Me.cmbSmtpAuthMode = New System.Windows.Forms.ComboBox
        Me.chkSmtpStartTLS = New System.Windows.Forms.CheckBox
        Me.chkSmtpSSL = New System.Windows.Forms.CheckBox
        Me.grpPop.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkRequiresPop
        '
        Me.chkRequiresPop.AutoSize = True
        Me.chkRequiresPop.Location = New System.Drawing.Point(6, 6)
        Me.chkRequiresPop.Name = "chkRequiresPop"
        Me.chkRequiresPop.Size = New System.Drawing.Size(224, 17)
        Me.chkRequiresPop.TabIndex = 0
        Me.chkRequiresPop.Text = "SMTP Server requires POP aunthetication"
        Me.chkRequiresPop.UseVisualStyleBackColor = True
        '
        'grpPop
        '
        Me.grpPop.Controls.Add(Me.btnTest)
        Me.grpPop.Controls.Add(Me.TableLayoutPanel1)
        Me.grpPop.Enabled = False
        Me.grpPop.Location = New System.Drawing.Point(6, 29)
        Me.grpPop.Name = "grpPop"
        Me.grpPop.Size = New System.Drawing.Size(363, 180)
        Me.grpPop.TabIndex = 1
        Me.grpPop.TabStop = False
        Me.grpPop.Text = "POP Details"
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(150, 149)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 0
        Me.btnTest.Text = "Verify"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.19277!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.80723!))
        Me.TableLayoutPanel1.Controls.Add(Me.txtPOPServer, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopUser, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopPassword, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtPopPort, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.chkPOPSSL, 1, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(25, 19)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(332, 124)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'txtPOPServer
        '
        Me.txtPOPServer.ForeColor = System.Drawing.Color.Blue
        Me.txtPOPServer.Location = New System.Drawing.Point(79, 3)
        Me.txtPOPServer.Name = "txtPOPServer"
        Me.txtPOPServer.Size = New System.Drawing.Size(226, 21)
        Me.txtPOPServer.TabIndex = 0
        '
        'txtPopUser
        '
        Me.txtPopUser.ForeColor = System.Drawing.Color.Blue
        Me.txtPopUser.Location = New System.Drawing.Point(79, 27)
        Me.txtPopUser.Name = "txtPopUser"
        Me.txtPopUser.Size = New System.Drawing.Size(226, 21)
        Me.txtPopUser.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "POP Server"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 24)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "User Name"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label3
        '
        Me.Label3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 48)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 24)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Password"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label4
        '
        Me.Label4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(27, 24)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Port"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPopPassword
        '
        Me.txtPopPassword.ForeColor = System.Drawing.Color.Blue
        Me.txtPopPassword.Location = New System.Drawing.Point(79, 51)
        Me.txtPopPassword.Name = "txtPopPassword"
        Me.txtPopPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.txtPopPassword.Size = New System.Drawing.Size(226, 21)
        Me.txtPopPassword.TabIndex = 2
        '
        'txtPopPort
        '
        Me.txtPopPort.ForeColor = System.Drawing.Color.Blue
        Me.txtPopPort.Location = New System.Drawing.Point(79, 75)
        Me.txtPopPort.Mask = "00000"
        Me.txtPopPort.Name = "txtPopPort"
        Me.txtPopPort.PromptChar = Global.Microsoft.VisualBasic.ChrW(32)
        Me.txtPopPort.Size = New System.Drawing.Size(44, 21)
        Me.txtPopPort.TabIndex = 3
        Me.txtPopPort.Text = "110"
        Me.txtPopPort.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals
        Me.txtPopPort.ValidatingType = GetType(Integer)
        '
        'chkPOPSSL
        '
        Me.chkPOPSSL.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.chkPOPSSL.AutoSize = True
        Me.chkPOPSSL.Location = New System.Drawing.Point(79, 99)
        Me.chkPOPSSL.Name = "chkPOPSSL"
        Me.chkPOPSSL.Size = New System.Drawing.Size(64, 22)
        Me.chkPOPSSL.TabIndex = 4
        Me.chkPOPSSL.Text = "Use SSL"
        Me.chkPOPSSL.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Image = CType(resources.GetObject("btnCancel.Image"), System.Drawing.Image)
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(311, 251)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 3
        Me.btnCancel.Text = "&Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnOK
        '
        Me.btnOK.Image = CType(resources.GetObject("btnOK.Image"), System.Drawing.Image)
        Me.btnOK.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnOK.Location = New System.Drawing.Point(230, 251)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.Size = New System.Drawing.Size(75, 23)
        Me.btnOK.TabIndex = 2
        Me.btnOK.Text = "&OK"
        Me.btnOK.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(2, 4)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(384, 241)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.chkRequiresPop)
        Me.TabPage1.Controls.Add(Me.grpPop)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(376, 215)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "POP Aunthentication"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.cmbSmtpAuthMode)
        Me.TabPage2.Controls.Add(Me.chkSmtpStartTLS)
        Me.TabPage2.Controls.Add(Me.chkSmtpSSL)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(376, 215)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "SMTP Security"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 71)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(122, 13)
        Me.Label5.TabIndex = 3
        Me.Label5.Text = "Aunthentication Method"
        '
        'cmbSmtpAuthMode
        '
        Me.cmbSmtpAuthMode.FormattingEnabled = True
        Me.cmbSmtpAuthMode.Items.AddRange(New Object() {"NONE", "LOGIN", "PLAIN", "CRAM-MD5", "NTLM"})
        Me.cmbSmtpAuthMode.Location = New System.Drawing.Point(132, 68)
        Me.cmbSmtpAuthMode.Name = "cmbSmtpAuthMode"
        Me.cmbSmtpAuthMode.Size = New System.Drawing.Size(121, 21)
        Me.cmbSmtpAuthMode.TabIndex = 2
        Me.cmbSmtpAuthMode.Text = "LOGIN"
        '
        'chkSmtpStartTLS
        '
        Me.chkSmtpStartTLS.AutoSize = True
        Me.chkSmtpStartTLS.Location = New System.Drawing.Point(6, 40)
        Me.chkSmtpStartTLS.Name = "chkSmtpStartTLS"
        Me.chkSmtpStartTLS.Size = New System.Drawing.Size(99, 17)
        Me.chkSmtpStartTLS.TabIndex = 1
        Me.chkSmtpStartTLS.Text = "Use ""Start TLS"""
        Me.chkSmtpStartTLS.UseVisualStyleBackColor = True
        '
        'chkSmtpSSL
        '
        Me.chkSmtpSSL.AutoSize = True
        Me.chkSmtpSSL.Location = New System.Drawing.Point(6, 17)
        Me.chkSmtpSSL.Name = "chkSmtpSSL"
        Me.chkSmtpSSL.Size = New System.Drawing.Size(121, 17)
        Me.chkSmtpSSL.TabIndex = 0
        Me.chkSmtpSSL.Text = "Use SSL Connection"
        Me.chkSmtpSSL.UseVisualStyleBackColor = True
        '
        'frmSMTPAdvanced
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(391, 278)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnOK)
        Me.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmSMTPAdvanced"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Advanced..."
        Me.grpPop.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents chkRequiresPop As System.Windows.Forms.CheckBox
    Friend WithEvents grpPop As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtPOPServer As System.Windows.Forms.TextBox
    Friend WithEvents txtPopUser As System.Windows.Forms.TextBox
    Friend WithEvents txtPopPassword As System.Windows.Forms.TextBox
    Friend WithEvents btnTest As System.Windows.Forms.Button
    Friend WithEvents txtPopPort As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents chkPOPSSL As System.Windows.Forms.CheckBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cmbSmtpAuthMode As System.Windows.Forms.ComboBox
    Friend WithEvents chkSmtpStartTLS As System.Windows.Forms.CheckBox
    Friend WithEvents chkSmtpSSL As System.Windows.Forms.CheckBox
End Class
